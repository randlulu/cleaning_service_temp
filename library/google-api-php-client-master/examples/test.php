<?php 
require_once realpath(dirname(__FILE__) . '/../autoload.php');
$client = new Google_Client();
// Replace this with your application name.
$client->setApplicationName("Client_Library_Examples");
// Replace this with the service you are using.
$service = new Google_Service_Calendar($client);

// This file location should point to the private key file.
//echo dirname(__FILE__) . '/../Tile Cleaners-83d152f9fa40.p12';
$key = file_get_contents('Tile Cleaners-83d152f9fa40.p12');
$cred = new Google_Auth_AssertionCredentials(
  'calendar',
  array('https://www.googleapis.com/auth/calendar'),
  $key
);
$cred->sub = "abusalem.islam1988@gmail.com";
$client->setAssertionCredentials($cred);
//echo $client;
print_r($client);




$event = new Google_Service_Calendar_Event();
$event->setSummary('Appointment');
$event->setLocation('Somewhere');
$start = new Google_Service_Calendar_EventDateTime();
$start->setDateTime('2011-06-03T10:00:00.000-07:00');
$event->setStart($start);
$end = new Google_Service_Calendar_EventDateTime();
$end->setDateTime('2011-06-03T10:25:00.000-07:00');
$event->setEnd($end);
$attendee1 = new Google_Service_Calendar_EventAttendee();
$attendee1->setEmail('attendeeEmail');
// ...
$attendees = array($attendee1,
                   // ...
                  );
$event->attendees = $attendees;
$createdEvent = $service->events->insert('primary', $event);

echo $createdEvent->getId();

?>
    