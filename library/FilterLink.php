<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FilterLink
 */
class FilterLink {

    private $url;
    private $filters;
    private $clear;
    private $query_string;

    function __construct($params = array()) {
        $frontController = Zend_Controller_Front::getInstance();
        $requestUrl = $frontController->getBaseUrl() . (isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '');

        $this->set_url(isset($params['url']) ? $params['url'] : $requestUrl);
        $this->set_filters(isset($params['filters']) ? $params['filters'] : array());
        $this->set_clear(isset($params['clear']) ? $params['clear'] : false);

        $this->handle_query_string();
    }

    public function set_url($url) {
        $this->url = $url;
    }

    public function get_url() {
        return $this->url;
    }

    public function set_filters($filters) {
        $this->filters = $filters;
    }

    public function get_filters() {
        return $this->filters;
    }
    
    public function set_clear($clear) {
        $this->clear = $clear;
    }

    public function get_clear() {
        return $this->clear;
    }

    public function set_query_string($query_string) {
        $this->query_string = $query_string;
    }

    public function get_query_string() {
        return $this->query_string;
    }

    public function handle_query_string() {

        $url = explode('?', $this->url);

        $matches = array();
        preg_match("/^[?].*/", $this->url, $matches);

        if (empty($matches)) {
            $this->set_url($url[0]);
        }

        $query_string_array = array();

        if (!$this->clear AND (isset($url[1]) AND $url[1])) {

            parse_str($url[1], $query_string_array);

            if ($query_string_array) {
                foreach ($this->filters as $key => $value) {
                    if (isset($query_string_array[$key])) {
                        if (is_array($value) && is_array($query_string_array[$key])) {
                            $query_string_array[$key] = array_merge(array_filter($query_string_array[$key]), array_filter($value));
                        } else {
                            $query_string_array[$key] = $value;
                        }
                    } else {
                        $query_string_array[$key] = $value;
                    }
                }
            } else {
                $query_string_array = $this->filters;
            }
        } else {
            $query_string_array = $this->filters;
        }

        $query_string = http_build_query(array_filter($query_string_array));
        $this->set_query_string($query_string);
    }

    public static function generate_url($filters) {
        $fl = new FilterLink(array('filters' => $filters));

        if (isset($fl->query_string) AND $fl->query_string) {
            return $fl->url . '?' . $fl->query_string;
        } else {
            return $fl->url;
        }
    }

    public static function generate_link($label, $filters, $selected = false, $clear = false , $url = null) {
        $fl = new FilterLink(array('filters' => $filters, 'clear' => $clear , 'url'=>$url));

        $url = '';
        if (isset($fl->query_string) AND $fl->query_string) {
            $url = $fl->url . '?' . $fl->query_string;
        } else {
            $url = $fl->url;
        }

        $class = 'filter_link';
        if ($selected) {
            $class = 'selected_filter_link';
        }
        
        $title = $label;
        $link = "<a href='{$url}' class='{$class}' >{$title}</a>";
        return $link;
    }

}

/* End of file FilterLink.php */