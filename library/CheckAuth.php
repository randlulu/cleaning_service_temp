<?php

class CheckAuth {

    private static $loggedUser;
    private static $cityId;
    private static $countryId;
    private static $companySession;
    private static $dashboardSession;
    private static $ss;
    private static $loggedEmployee;

    public static function checkPermission($credentials = array(), $message = "You Don't Have Permission", $router_url = "Login") {

        if (!self::checkCredential($credentials)) {
            $flashMessenger = new Zend_Controller_Action_Helper_FlashMessenger();
            $flashMessenger->addMessage(array('type' => 'error', 'message' => $message));

            $redirect = new Zend_Controller_Action_Helper_Redirector();
            $router = Zend_Controller_Front::getInstance()->getRouter();
            $redirect->gotoUrl($router->assemble(array(), $router_url));
        }
    }

    public static function checkCredential($credentials = array()) {

        self::checkLoggedIn();

        $loggedUser = self::getLoggedUser();

        if ($loggedUser) {
            $modelAuthCredential = new Model_AuthCredential();
            foreach ($credentials as $credential) {
                //get the credintial id by the credintial name
                $authority = $modelAuthCredential->getByCredentialName($credential);

                /* created by mohammed mkheamar for credential plan */


                $arrPlanCred = array();
                $loggedUser = self::getLoggedUser();
                $userCompaniesModel = new Model_UserCompanies();
                $company = $userCompaniesModel->getCompaniesByUserId($loggedUser['user_id']);
                $accountModel = new Model_Account();
                $Account = $accountModel->getByCompanyId($company['company_id']);

                if ($Account) {
                    $planID = $Account['plan_id'];
                    $planModel = new Model_Plan();
                    $planObj = $planModel->getById($planID);
                    $planCredModel = new Model_PlanTypeCredential();
                    $planCredObjs = $planCredModel->getByPlanTypeId($planObj['plan_type_id']);
                    foreach ($planCredObjs as $planCredObj) {
                        $arrPlanCred [] = $planCredObj['auth_credential_id'];
                    }
                    //////////////////////////////////////////////////////////////////////////////////////////////////////

                    $loggedUser = self::getLoggedUser();
                    $idsCred = array();

                    if ($loggedUser) {
                        $modelAuthCredential = new Model_AuthCredential();
                        foreach ($credentials as $credential) {
                            //get the credintial id by the credintial name
                            $authority = $modelAuthCredential->getByCredentialName($credential);
                            $authority_2 = $modelAuthCredential->getByCredentialName($credential);
                            $i = 0;
                            $authority_cred = 'empty';

                            while ($authority_2['parent_id'] != 0 and $i < 20) {
                                $i++;
                                if ($authority_2['parent_id'] == 0) {
                                    $authority_cred = $authority_2["credential_id"];
                                } else {
                                    $authority_2 = $modelAuthCredential->getById($authority_2['parent_id']);
                                    $authority_cred = $authority_2["credential_id"];
                                }
                            }


                            if (!in_array($authority_cred, $arrPlanCred)) {
                                return false;
                            }
                        }
                    }
                }


                /* end credential plan */



                /*                 * *********** *
                 * TMP CODE
                 * ************ */


                if (!$authority) {
                    $parent = $modelAuthCredential->getByCredentialName('general');
                    if (!$parent) {
                        $data = array(
                            'credential_name' => 'general',
                            'is_hidden' => 1,
                            'parent_id' => 0
                        );
                        $modelAuthCredential->insert($data);
                        $parent = $modelAuthCredential->getByCredentialName('general');
                    }
                    $data = array(
                        'credential_name' => $credential,
                        'is_hidden' => 0,
                        'parent_id' => $parent['credential_id']
                    );

                    $new_credential_id = $modelAuthCredential->insert($data);

                    $modelAuthRole = new Model_AuthRole();
                    $super_admin_role_id = $modelAuthRole->getRoleIdByName('super_admin');



                    $modelAuthRoleCredential = new Model_AuthRoleCredential();
                    $authRoleCredential = $modelAuthRoleCredential->getByRoleIdAndCredentialId($super_admin_role_id, $new_credential_id);


                    if (!$authRoleCredential) {
                        $data = array(
                            'role_id' => $super_admin_role_id,
                            'credential_id' => $new_credential_id
                        );
                        $modelAuthRoleCredential->insert($data);
                    }
                }
                /*                 * *********** *
                 * TMP CODE
                 * ************ */

                //check if the user has Permission to this credintial
                if (empty($loggedUser['credentials'][$authority['credential_id']])) {
                    return false;
                }
            }

            return true;
        }
        return false;
    }

    public static function checkPermissionOr($credentials = array(), $message = "You Don't Have Permission", $router_url = "Login") {

        if (!self::checkCredentialOr($credentials)) {
            $flashMessenger = new Zend_Controller_Action_Helper_FlashMessenger();
            $flashMessenger->addMessage(array('type' => 'error', 'message' => $message));

            $redirect = new Zend_Controller_Action_Helper_Redirector();
            $router = Zend_Controller_Front::getInstance()->getRouter();

            $redirect->gotoUrl($router->assemble(array(), $router_url));
        }
    }

    public static function checkCredentialOr($credentials = array()) {

        self::checkLoggedIn();

        $loggedUser = self::getLoggedUser();
        if ($loggedUser) {
            $modelAuthCredential = new Model_AuthCredential();
            foreach ($credentials as $credential) {
                //get the credintial id by the credintial name
                $authority = $modelAuthCredential->getByCredentialName($credential);

                /*                 * *********** *
                 * TMP CODE
                 * ************ */


                if (!$authority) {
                    $parent = $modelAuthCredential->getByCredentialName('general');
                    if (!$parent) {
                        $data = array(
                            'credential_name' => 'general',
                            'is_hidden' => 1,
                            'parent_id' => 0
                        );
                        $modelAuthCredential->insert($data);
                        $parent = $modelAuthCredential->getByCredentialName('general');
                    }
                    $data = array(
                        'credential_name' => $credential,
                        'is_hidden' => 0,
                        'parent_id' => $parent['credential_id']
                    );

                    $new_credential_id = $modelAuthCredential->insert($data);

                    $modelAuthRole = new Model_AuthRole();
                    $super_admin_role_id = $modelAuthRole->getRoleIdByName('super_admin');

                    $modelAuthRoleCredential = new Model_AuthRoleCredential();
                    $authRoleCredential = $modelAuthRoleCredential->getByRoleIdAndCredentialId($super_admin_role_id, $new_credential_id);

                    if (!$authRoleCredential) {
                        $data = array(
                            'role_id' => $super_admin_role_id,
                            'credential_id' => $new_credential_id
                        );
                        $modelAuthRoleCredential->insert($data);
                    }
                }
                /*                 * *********** *
                 * TMP CODE
                 * ************ */

                //check if the user has Permission to this credintial

                if (!empty($loggedUser['credentials'][$authority['credential_id']])) {
                    return true;
                }
            }

            return false;
        }
        return false;
    }

    public static function getLoggedUser() {
        // check if the $loggeduser has value
        if (self::$loggedUser) {
            return self::$loggedUser;
        }

        if (Zend_Auth::getInstance()->hasIdentity()) {
            // save the user login information  inside $loggeduser as array
            $userParams = array();
            $loggedUser = Zend_Auth::getInstance()->getIdentity();
            foreach ($loggedUser as $key => $value) {
                $userParams[$key] = $value;
            }
            // get all the credential id that belong to the role
            self::$loggedUser = $userParams;
            $roleCredentialModel = new Model_AuthRoleCredential();

			if('customer' == CheckAuth::getRoleName()){
			 self::$loggedUser['active'] = 1;
			}
            $allUserCredential = $roleCredentialModel->getAllowCredentialByUserRoleId(self::$loggedUser['role_id'],self::$loggedUser['active']);

            foreach ($allUserCredential as $userCredential) {
                self::$loggedUser['credentials'][$userCredential['credential_id']] = $userCredential['credential_id'];
            }

            $userCompaniesModel = new Model_UserCompanies;
            $company = $userCompaniesModel->getCompaniesByUserId($loggedUser->user_id);

            self::$loggedUser['company_id'] = $company['company_id'];
        }

        return self::$loggedUser;
    }

    public static function afterlogin($with_redirect = true, $os = 'web') {

        $loggedUser = Zend_Auth::getInstance()->getIdentity();



        $params = array(
            'last_login' => time(),
        );


        $usersModel = new Model_User();
        $usersModel->updateById($loggedUser->user_id, $params);

        //// add new record to user_login_history
        $data = array(
            'user_id' => $loggedUser->user_id,
            'login_time' => time()
        );

        if ($os == 'web' || $os == 'for_signUp') {
            $modelUserLoginHistory = new Model_UserLoginHistory();
            $historyId = $modelUserLoginHistory->insert($data);
        }

        if ($os == 'web') {
            $userLoginModel = new Model_UserLogin();
            $userLoginExists = $userLoginModel->getByUserId($loggedUser->user_id);
            $userLogin = array(
                'user_ip' => $_SERVER['REMOTE_ADDR'],
                'user_id' => $loggedUser->user_id,
            );
            if (!empty($userLoginExists)) {
                if ($userLoginExists['user_ip'] != $_SERVER['REMOTE_ADDR']) {
                    $userLoginModel->deleteById($userLoginExists['user_login_id']);

                    $userLoginModel->insert($userLogin);
                }
            } else {
                $userLoginModel->insert($userLogin);
            }
        }


        if (!self::getCompanySession()) {

            $modelUserCompanies = new Model_UserCompanies();
            $userCompanies = $modelUserCompanies->getCompaniesByUserId($loggedUser->user_id);
            $companyId = isset($userCompanies['company_id']) ? $userCompanies['company_id'] : 0;
            self::setCompanySession($companyId);
        }

        $session = new Zend_Session_Namespace();
        $modelCities = new Model_Cities;
        if ('contractor' == self::getRoleName()) {
            $session->city_id = $loggedUser->city_id;

            $city = $modelCities->getById($loggedUser->city_id);
            $session->country_id = $city['country_id'];
        } else {
            $companySession = self::getCompanySession();

            $modelCompanies = new Model_Companies();
            $company = $modelCompanies->getById($companySession);

            $session->city_id = $company['city_id'];
            $city = $modelCities->getById($company['city_id']);
            $session->country_id = $city['country_id'];
        }
        self::checkAccountCompletion($loggedUser->user_id);

        $go_to = $session->go_to;
        $session->go_to = '';

        $redirect = new Zend_Controller_Action_Helper_Redirector();
        if ($os == 'web') {
            $userInfo = $usersModel->getById($loggedUser->user_id);
            if ('contractor' == self::getRoleName() && $userInfo['profile_completeness'] <= 80) {
                $redirect->gotoUrl('/completeProfileVideo');
            }
        }

        if ($with_redirect) {
            if ($go_to) {
                $redirect->gotoUrl($go_to);
            } else {
                $modelAuthRole = new Model_AuthRole();
                $authRole = $modelAuthRole->getById($loggedUser->role_id);
                $defaultPage = $authRole['default_page'];
                $redirect->gotoUrl($defaultPage);
            }
        }
    }

    /*     * *** Check Employee Permission ****IBM */

    public static function checkEmployeePermission($message = "You Don't Have Permission", $router_url = "employeeLogin") {

        self::checkEmployeeLoggedIn();

        $loggedEmployee = self::getLoggedEmployee();

        // if (!self::checkCredential($credentials)) {
        //     $flashMessenger = new Zend_Controller_Action_Helper_FlashMessenger();
        //     $flashMessenger->addMessage(array('type' => 'error', 'message' => $message));
        //     $redirect = new Zend_Controller_Action_Helper_Redirector();
        //     $router = Zend_Controller_Front::getInstance()->getRouter();
        //     $redirect->gotoUrl($router->assemble(array(), $router_url));
        // }
    }

    /*     * ***Get Logged Employee *****IBM */

    public static function getLoggedEmployee() {
        // check if the $loggeduser has value
        if (self::$loggedEmployee) {
            return self::$loggedEmployee;
        }


        if (Zend_Auth::getInstance()->hasIdentity()) {
            // save the user login information  inside $loggedEmployee as array
            $empParams = array();
            $loggedEmployee = Zend_Auth::getInstance()->getIdentity();

            foreach ($loggedEmployee as $key => $value) {
                $empParams[$key] = $value;
            }
            // get all the credential id that belong to the role
            self::$loggedEmployee = $empParams;
            // $roleCredentialModel = new Model_AuthRoleCredential();
            // $allUserCredential = $roleCredentialModel->getAllowCredentialByUserRoleId(self::$loggedUser['role_id']);
            // foreach ($allUserCredential as $userCredential) {
            //     self::$loggedUser['credentials'][$userCredential['credential_id']] = $userCredential['credential_id'];
            // }
        }

        return self::$loggedEmployee;
    }

    /*     * *****After employee login*****IBM */

    public static function afterEmployeeLogin($with_redirect = true, $os = 'web', $employee = 1) {
        $loggedEmployee = Zend_Auth::getInstance()->getIdentity();

        $params = array(
            'last_login' => time()
        );

        // $employeeModel = new Model_Employee();
        // $employeeModel->updateById($loggedEmployee->employee_id, $params);
        //// add new record to user_login_history
        // $data = array(
        //     'employee_id'    => $loggedEmployee->employee_id,
        //     'login_time' => time()
        // );
        // if($os == 'web'){
        //   $modelUserLoginHistory = new Model_UserLoginHistory();
        //   $historyId = $modelUserLoginHistory->insert($data);
        // }

        $session = new Zend_Session_Namespace();

        $go_to = $session->go_to;
        $session->go_to = '';

        $redirect = new Zend_Controller_Action_Helper_Redirector();

        if ($with_redirect) {
            if ($go_to) {
                $redirect->gotoUrl($go_to);
            } else {
                $redirect->gotoUrl('subscription/account');
            }
        }
    }

    /*     * **** Employee logout ******IBM */

   

    /*     * *** Check Employee Login *****IBM */

    public static function checkEmployeeLoggedIn() {
        //checked if the Employee has logged in
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            //add message to the session at the last request
            $flashMessenger = new Zend_Controller_Action_Helper_FlashMessenger();
            $flashMessenger->addMessage(array('type' => 'error', 'message' => "You must be logged in"));

            $session = new Zend_Session_Namespace();
            //save the last url request in the session
            $session->go_to = (isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '');

            $redirect = new Zend_Controller_Action_Helper_Redirector();
            $router = Zend_Controller_Front::getInstance()->getRouter();

            $redirect->gotoUrl($router->assemble(array(), 'employeeLogin'));
        } elseif (Zend_Auth::getInstance()->hasIdentity()) {
            $logged = Zend_Auth::getInstance()->getIdentity();

            if (!isset($logged->is_employee) && $logged->is_employee != 'octopus') {

                $flashMessenger = new Zend_Controller_Action_Helper_FlashMessenger();
                $flashMessenger->addMessage(array('type' => 'error', 'message' => "You must be logged in as an Octopuspro employee !"));

                $session = new Zend_Session_Namespace();
                //save the last url request in the session
                $session->go_to = (isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '');
                self::logout();

                $redirect = new Zend_Controller_Action_Helper_Redirector();
                $router = Zend_Controller_Front::getInstance()->getRouter();
                $redirect->gotoUrl($router->assemble(array(), 'employeeLogin'));
            }
        }
    }

    /*     * **** End ***** */

    public static function logout() {
        Zend_Auth::getInstance()->clearIdentity();
        self::$loggedUser = null;

        $session = new Zend_Session_Namespace();

        $session->city_id = null;
        self::$cityId = null;

        $session->country_id = null;
        self::$countryId = null;

        $session->company_session = null;
        self::$companySession = null;

        $session->dashboard_session = null;
        self::$dashboardSession = null;
    }

    public static function getCityId() {
        if (self::$cityId) {
            return self::$cityId;
        }

        $session = new Zend_Session_Namespace();
        $cityId = $session->city_id;
        if ($cityId) {
            self::$cityId = $cityId;
        }
        return self::$cityId;
    }

    public static function getCountryId() {
        if (self::$countryId) {
            return self::$countryId;
        }

        $session = new Zend_Session_Namespace();
        $countryId = $session->country_id;
        if ($countryId) {
            self::$countryId = $countryId;
        }
        return self::$countryId;
    }

    /*
     * check_logged_in
     * @return true/false
     */

    public static function checkLoggedIn() {
        //checked if the user has logged in
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            //add message to the session at the last request
            $flashMessenger = new Zend_Controller_Action_Helper_FlashMessenger();
            $flashMessenger->addMessage(array('type' => 'error', 'message' => "You must be logged in"));

            $session = new Zend_Session_Namespace();
            //save the last url request in the session
            $session->go_to = (isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '');

            $redirect = new Zend_Controller_Action_Helper_Redirector();
            $router = Zend_Controller_Front::getInstance()->getRouter();
            $redirect->gotoUrl($router->assemble(array(), 'Login'));
        }
        /*         * ***** Check session if logged as employee ********IBM */ elseif (Zend_Auth::getInstance()->hasIdentity()) {
            $logged = Zend_Auth::getInstance()->getIdentity();
            if (isset($logged->is_employee) && $logged->is_employee == 'octopus') {

                $flashMessenger = new Zend_Controller_Action_Helper_FlashMessenger();
                $flashMessenger->addMessage(array('type' => 'error', 'message' => "You must be logged in !"));

                $session = new Zend_Session_Namespace();
                //save the last url request in the session
                $session->go_to = (isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '');
                self::employeeLogout();

                $redirect = new Zend_Controller_Action_Helper_Redirector();
                $router = Zend_Controller_Front::getInstance()->getRouter();
                $redirect->gotoUrl($router->assemble(array(), 'Login'));
            }
        }
    }

    public static function isLoggedIn() {
        //checked if the user has logged in
        if (Zend_Auth::getInstance()->hasIdentity()) {
            $isLogged = true;
        } else {

            $isLogged = false;
        }
        return $isLogged;
    }

    public static function getRoleName() {
        $loggedUser = Zend_Auth::getInstance()->getIdentity();

        if ($loggedUser) {
            $modelAuthRole = new Model_AuthRole();
            return $modelAuthRole->getRoleName($loggedUser->role_id);
        }
        return '';
    }

    /**
     * get_company_dropdown
     *
     * @return string
     */
    public static function getCompanyDropdown() {
        $loggedUser = self::getLoggedUser();
        if ($loggedUser) {
            $modelUserCompanies = new Model_UserCompanies();
            $userCompanies = $modelUserCompanies->getCompaniesByUserId($loggedUser['user_id']);
            if (self::checkCredential(array('canHandleAllCompanies')) || (count($userCompanies) > 1)) {

                //
                //get all companies
                //
                    $companies = array();
                $modelCompanies = new Model_Companies();
                if (self::checkCredential(array('canHandleAllCompanies'))) {
                    $companies = $modelCompanies->getAll();
                } elseif ($userCompanies) {
                    $companies = $userCompanies;
                }

                //
                // drow field
                //
                    $dropdown_field = '';

                if ($companies) {
                    //  $dropdown_field .= '<select id="company_session" class="header_company_select" >';
                    $companyId = self::getCompanySession();
                    foreach ($companies as $company) {
                        //  $selected = ((!empty($companyId) AND $company['company_id'] == $companyId) ? 'selected = "selected"' : '');
                        $dropdown_field .= '<li role="presentation"><input type="hidden" value="' . $company['company_id'] . '"/><a role="menuitem" tabindex="-1" href="#">' . $company['company_name'] . '</a></li>';
                    }
                    //$dropdown_field .= '</select>';
                }
                return $dropdown_field;
            } else {
                $companyName = isset($userCompanies[0]['company_name']) ? $userCompanies[0]['company_name'] : '';
                return "<span id=\"company_name\">{$companyName}</span>";
            }
        }
    }

    /**
     * set_company_session
     *
     * @param type $companySession
     */
    public static function setCompanySession($companySession) {
        $session = new Zend_Session_Namespace();
        $session->company_session = $companySession;
    }

    /**
     * getCompanySession
     *
     * @return type
     */
    public static function getCompanySession() {
        if (self::$companySession) {
            return self::$companySession;
        }

        $session = new Zend_Session_Namespace();
        $companySession = $session->company_session;

        if ($companySession) {
            self::$companySession = $companySession;
        }
        return self::$companySession;
    }

    /**
     * get_DashboardStatus_dropdown
     *
     * @return string
     */
    public static function getDashboardStatusDropdown() {

        $loggedUser = self::getLoggedUser();
        if ($loggedUser) {
            if (CheckAuth::checkCredential(array('canSeeAllBookingStatistics'))) {

                //dashboard_status for drop down menu

                $select = new Zend_Form_Element_Select('dashboard_status');
                $select->setDecorators(array('ViewHelper'));
                $select->setValue(CheckAuth::getDashboardStatusSession());
                $select->setAttrib('onchange', "select_dashboard_status();");
                $select->setAttrib('class', "form-control");
                $select->addMultiOption('all_dashboard', 'All Dashboard');
                $select->addMultiOption('my_dashboard', 'My Dashboard');
                return $select;
            } else {
                return false;
            }
        }
    }

    /**
     * set_DashboardStatus_session
     *
     * @param type $dashboardSession
     */
    public static function setDashboardStatusSession($dashboardSession) {
        $session = new Zend_Session_Namespace();
        $session->dashboard_session = $dashboardSession;
    }

    /**
     * getDashboardStatusSession
     *
     * @return type
     */
    public static function getDashboardStatusSession() {
        if (self::$dashboardSession) {
            return self::$dashboardSession;
        }

        $session = new Zend_Session_Namespace();
        $dashboardSession = $session->dashboard_session;
        if ($dashboardSession) {
            self::$dashboardSession = $dashboardSession;
        }
        return self::$dashboardSession;
    }

    /**
     * getGeneralContractor
     *
     * @return type
     */
    public static function getGeneralContractor() {
        $modelUser = new Model_User();
        $modelCompanies = new Model_Companies();

        $companyId = self::getCompanySession();
        $company = $modelCompanies->getById($companyId);
        $companyName = trim($company['company_name']);

        return $modelUser->getByUserCode(sha1("General {$companyName}"));
    }

    public static function checkIfCanHandelAllCompany($type, $id) {

        if (self::checkCredential(array('canHandleAllCompanies'))) {
            return true;
        }

        $companyId = self::getCompanySession();

        switch ($type) {

            case 'inquiry':
                $modelInquiry = new Model_Inquiry();
                $inquiry = $modelInquiry->getById($id);

                if ($inquiry['company_id'] == $companyId) {
                    return true;
                }
                break;

            case 'booking':
                $modelBooking = new Model_Booking();
                $booking = $modelBooking->getById($id);

                if ($booking['company_id'] == $companyId) {
                    return true;
                }
                break;

            case 'estimate':
                $modelBookingEstimate = new Model_BookingEstimate();
                $modelBooking = new Model_Booking();

                $bookingEstimate = $modelBookingEstimate->getById($id);
                $booking = $modelBooking->getById($bookingEstimate['booking_id']);
                if ($booking['company_id'] == $companyId) {
                    return true;
                }
                break;

            case 'invoice':
                $modelBookingInvoice = new Model_BookingInvoice();
                $modelBooking = new Model_Booking();

                $bookingInvoice = $modelBookingInvoice->getById($id);
                $booking = $modelBooking->getById($bookingInvoice['booking_id']);
                if ($booking['company_id'] == $companyId) {
                    return true;
                }
                break;

            case 'complaint':
                $modelComplaint = new Model_Complaint();
                $modelBooking = new Model_Booking();

                $complaint = $modelComplaint->getById($id);
                $booking = $modelBooking->getById($complaint['booking_id']);
                if ($booking['company_id'] == $companyId) {
                    return true;
                }
                break;

            case 'customer':
                $modelCustomer = new Model_Customer();

                $customer = $modelCustomer->getById($id);
                if ($customer['company_id'] == $companyId) {
                    return true;
                }
                break;

            case 'report':
                $modelReport = new Model_Report();

                $report = $modelReport->getById($id);
                if ($report['company_id'] == $companyId) {
                    return true;
                }
                break;

            case 'user':
                $modelUserCompanies = new Model_UserCompanies();

                $UserCompanies = $modelUserCompanies->getByUserId($id);
                if ($UserCompanies['company_id'] == $companyId) {
                    return true;
                }
                break;

            case 'user_info':
                $modelUserInfo = new Model_UserInfo();
                $modelUserCompanies = new Model_UserCompanies();

                $userInfo = $modelUserInfo->getById($id);
                $UserCompanies = $modelUserCompanies->getByUserId($userInfo['user_id']);
                if ($UserCompanies['company_id'] == $companyId) {
                    return true;
                }
                break;

            case 'contractor_info':
                $modelContractorInfo = new Model_ContractorInfo();
                $modelUserCompanies = new Model_UserCompanies();

                $contractorInfo = $modelContractorInfo->getById($id);
                $UserCompanies = $modelUserCompanies->getByUserId($contractorInfo['contractor_id']);

                if ($UserCompanies['company_id'] == $companyId) {
                    return true;
                }
                break;

            case 'contractor_owner':
                $modelContractorOwner = new Model_ContractorOwner();
                $modelContractorInfo = new Model_ContractorInfo();
                $modelUserCompanies = new Model_UserCompanies();

                $contractorOwner = $modelContractorOwner->getById($id);
                $contractorInfo = $modelContractorInfo->getById($contractorOwner['contractor_info_id']);
                $UserCompanies = $modelUserCompanies->getByUserId($contractorInfo['contractor_id']);

                if ($UserCompanies['company_id'] == $companyId) {
                    return true;
                }
                break;

            case 'contractor_employee':
                $modelContractorEmployee = new Model_ContractorEmployee();
                $modelContractorInfo = new Model_ContractorInfo();
                $modelUserCompanies = new Model_UserCompanies();

                $contractorEmployee = $modelContractorEmployee->getById($id);
                $contractorInfo = $modelContractorInfo->getById($contractorEmployee['contractor_info_id']);
                $UserCompanies = $modelUserCompanies->getByUserId($contractorInfo['contractor_id']);

                if ($UserCompanies['company_id'] == $companyId) {
                    return true;
                }
                break;

            case 'contractorVehicle':
                $modelContractorVehicle = new Model_ContractorVehicle();
                $modelContractorInfo = new Model_ContractorInfo();
                $modelUserCompanies = new Model_UserCompanies();

                $contractorVehicle = $modelContractorVehicle->getById($id);
                $contractorInfo = $modelContractorInfo->getById($contractorVehicle['contractor_info_id']);
                $UserCompanies = $modelUserCompanies->getByUserId($contractorInfo['contractor_id']);

                if ($UserCompanies['company_id'] == $companyId) {
                    return true;
                }
                break;

            case 'contractor_service':
                $modelContractorService = new Model_ContractorService();
                $modelUserCompanies = new Model_UserCompanies();

                $contractorService = $modelContractorService->getById($id);
                $UserCompanies = $modelUserCompanies->getByUserId($contractorService['contractor_id']);

                if ($UserCompanies['company_id'] == $companyId) {
                    return true;
                }
                break;

            case 'contractor_service_availability':
                $modelContractorServiceAvailability = new Model_ContractorServiceAvailability();
                $modelContractorService = new Model_ContractorService();
                $modelUserCompanies = new Model_UserCompanies();

                $contractorServiceAvailability = $modelContractorServiceAvailability->getById($id);
                $contractorService = $modelContractorService->getById($contractorServiceAvailability['contractor_service_id']);
                $UserCompanies = $modelUserCompanies->getByUserId($contractorService['contractor_id']);

                if ($UserCompanies['company_id'] == $companyId) {
                    return true;
                }
                break;

            case 'service':
                $modelServices = new Model_Services();
                $services = $modelServices->getById($id);

                if ($services['company_id'] == $companyId) {
                    return true;
                }
                break;

            case 'attribute':
                $modelAttributes = new Model_Attributes();
                $attributes = $modelAttributes->getById($id);

                if ($attributes['company_id'] == $companyId) {
                    return true;
                }
                break;

            case 'service_attribute':
                $modelServiceAttribute = new Model_ServiceAttribute();
                $modelAttributes = new Model_Attributes();

                $serviceAttribute = $modelServiceAttribute->getById($id);
                $attributes = $modelAttributes->getById($serviceAttribute['attribute_id']);

                if ($attributes['company_id'] == $companyId || $attributes['company_id'] == 0) {
                    return true;
                }
                break;

            case 'product':
                $modelProduct = new Model_Product();
                $product = $modelProduct->getById($id);

                if ($product['company_id'] == $companyId) {
                    return true;
                }
                break;

            case 'payment_type':
                $modelPaymentType = new Model_PaymentType();
                $paymentType = $modelPaymentType->getById($id);

                if ($paymentType['company_id'] == $companyId) {
                    return true;
                }
                break;

            case 'label':
                $modelLabel = new Model_Label();
                $label = $modelLabel->getById($id);

                if ($label['company_id'] == $companyId) {
                    return true;
                }
                break;

            case 'customer_contact_label':
                $modelCustomerContactLabel = new Model_CustomerContactLabel();
                $customerContactLabel = $modelCustomerContactLabel->getById($id);

                if ($customerContactLabel['company_id'] == $companyId) {
                    return true;
                }
                break;

            case 'inquiry_type':
                $modelInquiryType = new Model_InquiryType();
                $inquiryType = $modelInquiryType->getById($id);

                if ($inquiryType['company_id'] == $companyId) {
                    return true;
                }
                break;

            case 'inquiry_type_attribute':
                $modelInquiryTypeAttribute = new Model_InquiryTypeAttribute();
                $modelInquiryType = new Model_InquiryType();

                $inquiryTypeAttribute = $modelInquiryTypeAttribute->getById($id);
                $inquiryType = $modelInquiryType->getById($inquiryTypeAttribute['inquiry_type_id']);

                if ($inquiryType['company_id'] == $companyId) {
                    return true;
                }
                break;

            case 'inquiry_required_type':
                $modelInquiryRequiredType = new Model_InquiryRequiredType();
                $inquiryRequiredType = $modelInquiryRequiredType->getById($id);

                if ($inquiryRequiredType['company_id'] == $companyId) {
                    return true;
                }
                break;

            case 'due_date':
                $modelDueDate = new Model_DueDate();
                $inquiryRequiredType = $modelDueDate->getById($id);

                if ($inquiryRequiredType['company_id'] == $companyId) {
                    return true;
                }
                break;

            case 'customer_type':
                $modelCustomerType = new Model_CustomerType();
                $customerType = $modelCustomerType->getById($id);

                if ($customerType['company_id'] == $companyId) {
                    return true;
                }
                break;

            case 'complaint_type':
                $modelComplaintType = new Model_ComplaintType();
                $complaintType = $modelComplaintType->getById($id);

                if ($complaintType['company_id'] == $companyId) {
                    return true;
                }
                break;

            case 'company_invoice_note':
                $modelCompanyInvoiceNote = new Model_CompanyInvoiceNote();
                $companyInvoiceNote = $modelCompanyInvoiceNote->getById($id);

                if ($companyInvoiceNote['company_id'] == $companyId) {
                    return true;
                }
                break;
        }

        return false;
    }

    public static function redirectCustomer($defaultPage = null) {

        $loggedUser = Zend_Auth::getInstance()->getIdentity();
        if (!self::getCompanySession()) {
            $companyId = isset($loggedUser->company_id) ? $loggedUser->company_id : 0;
            self::setCompanySession($companyId);
        }

        $session = new Zend_Session_Namespace();
        $modelCities = new Model_Cities;
        if ('customer' == self::getRoleName()) {
            $session->city_id = $loggedUser->city_id;

            $city = $modelCities->getById($loggedUser->city_id);
            $session->country_id = $city['country_id'];
        }
        $session->go_to = $defaultPage;
        $go_to = $session->go_to;

        $redirect = new Zend_Controller_Action_Helper_Redirector();

        if ($go_to) {
            $redirect->gotoUrl($go_to);
        } else if ($defaultPage) {
            $redirect->gotoUrl("/booking/view/13426");
        }
    }

    /*
      created by mohammed Mukhaimer
      this function  to check after  every login
      if this user  has create account subscription befor ?
      if yes he create account subscription i want to check
      the status of his account subscription
      and if status of his account
      before to be  active  i will redirect him to complete account information





     */

    public static function checkAccountCompletion($id) {

        $accountModel = new Model_Account();
        $account = $accountModel->getByCreatedBy($id);   // check if user has create account

        $redirect = new Zend_Controller_Action_Helper_Redirector();


        if ($account) {
            /*

              if($account['account_status'] == 'Suspended' ){

              self::logout();
              exit(' account suspended') ;
              }
             */
            // check if he complete company information of his account
            // i will fetch row of company which has id and check data 

            $companiesObj = new Model_Companies();
            $company = $companiesObj->getById($account['company_id']);

            ///// check if he not end company information  redirect 
            /////  to panel with focus in to form complete company information
            if ($company['company_business_name'] == '' || $company['company_name'] == '') {
                $redirect->gotoUrl("/panel-first-login");
            }

            $usersObj = new Model_User();
            $user = $usersObj->getById($id);

            if ($user['first_name'] == '' || $user['last_name'] == '') {
                $redirect->gotoUrl("/panel-first-login");
            }

            if ($account['account_status'] == 'Non-subscriber') {
                $redirect->gotoUrl("/panel-first-login");
            }
        } else {

            $userCompaniesModel = new Model_UserCompanies();
            $companyID = $userCompaniesModel->getCompaniesByUserId($id);
            $account = $accountModel->getByCompanyId($companyID['company_id']);

            //  var_dump($account ); exit;

            if ($account['account_status'] == 'Suspended') {
                self::logout();
                exit(' account suspended');
            }
        }


        ///////////////  Now check this user with any company to any account ///////////////////
        ////////////////// and check if  the account belong to is suspended or not /////////////
        ////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////
    }

    /*
      public function checkPlanCredential(){

      return true ;


      self::checkLoggedIn();
      $user = CheckAuth::getLoggedUser();
      $userCompaniesModel = new Model_UserCompanies();
      $company =  $userCompaniesModel->getCompaniesByUserId($user['user_id']);

      $accountModel = new Model_Account() ;

      $belongToAccount = $accountModel -> getByCompanyId($company['company_id']);




      }

     */

    public static function checkAccountSteps($id) {

        $accountModel = new Model_Account();
        $account = $accountModel->getByCreatedBy($id);   // check if user has create account

        $redirect = new Zend_Controller_Action_Helper_Redirector();
        if ($account) {
            // check if he complete company information of his account
            // i will fetch row of company which has id and check data 
            $companiesObj = new Model_Companies();
            $company = $companiesObj->getById($account['company_id']);

            ///// check if he not end company information  redirect 
            /////  to panel with focus in to form complete company information
            if ($company['company_business_name'] == '' || $company['company_name'] == '') {
                return 1;
            } else if ($company['company_website'] == '' || $company['company_website'] == '') {
                return 2;
            } else if ($company['company_street_no'] == '' || $company['company_street_no'] == '') {
                return 3;
            }

            $usersObj = new Model_User();
            $user = $usersObj->getById($id);
            if ($user['first_name'] == '' || $user['last_name'] == '') {
                return 4;
            }
            if ($account['account_status'] == 'Non-subscriber') {
                return 5;
            }
            return 6;
        } else {
            return 700;
        }
    }
    
    //login by mohamed for employee session
    public static function sec_session_start($login_string,$employee_id,$email,$username,$company_name,$is_employee,$avatar,$displayName) {

        $session_name = 'employee_session';
        session_name($session_name);
        $secure = true;
        $httponly = true;
        $session = new Zend_Session_Namespace();
        $session->login_string = $login_string;
        $session->employee_id = $employee_id;
        $session->email = $email;
        $session->username = $username;
        $session->company_name = $company_name;
        $session->is_employee = $is_employee;
        $session->avatar = $avatar;
        $session->displayName = $displayName;
        if (ini_set('session.use_only_cookies', 1) === FALSE) {
            $redirect = new Zend_Controller_Action_Helper_Redirector();
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => 'Could not initiate a safe session (ini_set)'));
            $this->_redirect($this->router->assemble(array(), 'employeeLogin'));
            exit();
        }
     
        $cookieParams = session_get_cookie_params();
        session_set_cookie_params($cookieParams["lifetime"], $cookieParams["path"], $cookieParams["domain"], $secure, $httponly);
     
    }

    public static function login_check() {

        $redirect = new Zend_Controller_Action_Helper_Redirector();
        $flashMessenger = new Zend_Controller_Action_Helper_FlashMessenger();
        $router = Zend_Controller_Front::getInstance()->getRouter();
         $session = new Zend_Session_Namespace();
         
        if (isset($session->employee_id, $session->username, $session->login_string, $session->email)) {

            $employee_id  = $session->employee_id;
            $login_string = $session->login_string;
            $user_browser = $_SERVER['HTTP_USER_AGENT'];

            $employeeobj = new Model_Employee();
            $employeeData = $employeeobj->getById($employee_id);
            if (!empty($employeeData)) {
                $login_check = hash('sha512', $employeeData['password'] . $user_browser);
                if (strcmp($login_check, $login_string) !=0) {
                     Zend_Auth::getInstance()->clearIdentity();
                    $flashMessenger->addMessage(array('type' => 'error', 'message' => "You must be logged in"));
                    $redirect->gotoUrl($router->assemble(array(), 'employeeLogin'));
                }
            } else {
                 Zend_Auth::getInstance()->clearIdentity();
                $flashMessenger->addMessage(array('type' => 'error', 'message' => "You must be logged in"));
                $redirect->gotoUrl($router->assemble(array(), 'employeeLogin'));
            }
        } else {
            Zend_Auth::getInstance()->clearIdentity();
            $flashMessenger->addMessage(array('type' => 'error', 'message' => "You must be logged in"));
            $redirect->gotoUrl($router->assemble(array(), 'employeeLogin'));
        }
    }

    public static function employeeLogout() {
        Zend_Auth::getInstance()->clearIdentity();
        $params = session_get_cookie_params();
        setcookie(session_name(), '', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
        session_destroy();
    }

}
