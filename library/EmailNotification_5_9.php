<?php

class EmailNotification {

    public static function sendEmail($params = array(), $template_name = '', $template_params = array(), $email_log = array(), $companyId = 1) {

        //smtp auth
        $config = array('auth' => 'login',
            'username' => get_config('smtp_username', array('email_notification')),
            'password' => get_config('smtp_password', array('email_notification'))
        );


        $transport = new Zend_Mail_Transport_Smtp(get_config('smtp_domain', array('email_notification')), $config);

        $mail = new Zend_Mail('UTF-8');

        /*if (!$companyId) {
            $companyId = CheckAuth::getCompanySession();
        }*/
            $loggedUser = CheckAuth::getLoggedUser();
          if($loggedUser and !$companyId){
              $companyId = CheckAuth::getCompanySession();
          }
        

        $modelCompanies = new Model_Companies();
        $company = $modelCompanies->getById($companyId);
		$company_name = '';
		if ($companyId == 1) {
		 $company_name = isset($company['company_name']) && $company['company_name'] ? $company['company_name'] : 'Tile Cleaners Pty Ltd';
		}

        
         if (isset($params['from'])) {
            $mail->setFrom($params['from'],$company_name);
        } else {
            if ($companyId == 1) {
            $company_enquiries_email = isset($company['company_enquiries_email']) && $company['company_enquiries_email'] ? $company['company_enquiries_email'] : get_config('default_email', array('email_notification'));
            
            $mail->setFrom($company_enquiries_email, $company_name);
             } else{
            $mail->setFrom('support@octopuspro.com','OctopusPro');
            }
        } 

        if (isset($params['to']) && $params['to']) {

            if (!is_array($params['to'])) {
                $params['to'] = explode(',', $params['to']);
            }
            foreach ($params['to'] as $to) {
                $to = trim($to);
                if (!empty($to) && filter_var($to, FILTER_VALIDATE_EMAIL)) {
                    $mail->addTo($to);
                }
            }
        }

        if (isset($params['cc']) && $params['cc']) {
            if (!is_array($params['cc'])) {
                $params['cc'] = explode(',', $params['cc']);
            }
            foreach ($params['cc'] as $cc) {
                $cc = trim($cc);
                if (!empty($cc) && filter_var($cc, FILTER_VALIDATE_EMAIL)) {
                    $mail->addCc($cc);
                }
            }
        }

        if (isset($params['reply']) && $params['reply']) {
            $replyEmail = isset($params['reply']['email']) ? trim($params['reply']['email']) : '';
            $replyName = isset($params['reply']['name']) ? trim($params['reply']['name']) : '';

            if (!empty($replyEmail) && filter_var($replyEmail, FILTER_VALIDATE_EMAIL)) {
                $mail->setReplyTo($replyEmail, $replyName);
            }
        }

        $modelEmailTemplate = new Model_EmailTemplate();
        $emailTemplate = $modelEmailTemplate->getEmailTemplate($template_name, $template_params, $companyId);
		
		//var_dump($emailTemplate) ; exit ; 
        

        $body = $emailTemplate['body'];
        $subject = $emailTemplate['subject'];
		
		/*if($walaaFlag){
			$senderId = 53;
			$recieverId = 22265;
			$modelDiscussionSeq = new Model_DiscussionSeq();
			$newEmailTrackingLogId = $modelDiscussionSeq->updateComplaintDiscussionSeq();
			$tracker = 'https://' . $_SERVER['HTTP_HOST'] . dirname( $_SERVER['REQUEST_URI'] ) . 'dropboxTest/emailTracking/' . $newEmailTrackingLogId . '/' . $senderId . '/' . $recieverId . '/' . $subject;
			$body .= '<img border="0" src="'.$tracker.'" width="1" height="1" />';
		}*/

        $templateData = array();
        $templateData['signature_logo'] = $company['company_signature_logo'];
        $templateData['layout'] = isset($params['layout']) ? $params['layout'] : 'email_template';

        if ($body) {
            $templateData['body'] = $body;
            $templateData['subject'] = $subject;

            $body = self::getEmailLayout($templateData);
            $subject = $subject;
        } else {
            $templateData['body'] = isset($params['body']) ? $params['body'] : '';
            $templateData['subject'] = isset($params['subject']) ? $params['subject'] : '';

            $body = self::getEmailLayout($templateData);
            $subject = isset($params['subject']) ? $params['subject'] : '';
        }
        
       

        $mail->setSubject($subject);
        $mail->setBodyHtml($body, 'UTF-8');
        // $mail->type = 'text/html';
		
		
        //By Walaa
		if (!isset($params['attachment'])) {
			$attachments = $modelEmailTemplate->getAllAttachmentByNameAsString($template_name);
			if($attachments){
				$params['attachment'] = $attachments;
			}
		}
		//end by walaa
        if (isset($params['attachment'])) {
            $attachment = explode(',', $params['attachment']);
            foreach ($attachment as $key => $attach_value) {
                if (file_exists($attach_value)) {
                    $at_file = file_get_contents($attach_value);
                    $at = $mail->createAttachment($at_file);
                    $at->type = 'application/pdf';
                    $at->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
                    $at->encoding = Zend_Mime::ENCODING_BASE64;

                    if (isset($params['attachment_name'])) {
                        $at->filename = $params['attachment_name'];
                    } else {
                        $at->filename = self::getFileName($attach_value);
                    } 
                }
            }
            // send attachment
        }

        $loggedUser = CheckAuth::getLoggedUser();
        $emailLog = array(
            'created' => time(),
            'to' => implode(',', $params['to']),
            'body' => $body,
            'subject' => $subject,
            'user_id' => isset($loggedUser['user_id']) && $loggedUser['user_id'] ? $loggedUser['user_id'] : 0,
            'reference_id' => isset($email_log['reference_id']) && $email_log['reference_id'] ? $email_log['reference_id'] : 0,
            'cronjob_history_id' => isset($email_log['cronjob_history_id']) && $email_log['cronjob_history_id'] ? $email_log['cronjob_history_id'] : 0,
            'type' => isset($email_log['type']) && $email_log['type'] ? $email_log['type'] : 'common'
        );

        if (isset($params['attachment']) && file_exists($params['attachment'])) {
            // save  attachment path 
            $emailLog['attachment_path'] = $params['attachment'];
        }

        $modelEmailLog = new Model_EmailLog(); 
       $modelEmailLog->insert($emailLog);
		

        //return file_put_contents('/tmp/' . time() . '-' . $to . '.html', $body);
        return $mail->send($transport);
        //return $mail->send();
    }

    public static function sendMarketingEmail($params = array(), $template_name = '', $template_params = array(), $email_log = array(), $companyId = 0) {

        //smtp auth
        $config = array('auth' => 'login',
            'username' => get_config('smtp_username', array('email_notification')),
            'password' => get_config('smtp_password', array('email_notification'))
        );


        $transport = new Zend_Mail_Transport_Smtp(get_config('smtp_domain', array('email_notification')), $config);

        $mail = new Zend_Mail('UTF-8');

        if (!$companyId) {
            $companyId = CheckAuth::getCompanySession();
        }

        $modelCompanies = new Model_Companies();
        $company = $modelCompanies->getById($companyId);



        if (isset($params['from'])) {
            $mail->setFrom($params['from']);
        } else {

            $company_enquiries_email = isset($company['company_enquiries_email']) && $company['company_enquiries_email'] ? $company['company_enquiries_email'] : get_config('default_email', array('email_notification'));

            $company_name = isset($company['company_name']) && $company['company_name'] ? $company['company_name'] : 'Tile Cleaners Pty Ltd';
            $mail->setFrom($company_enquiries_email, $company_name);
        }

        if (isset($params['to']) && $params['to']) {

            if (!is_array($params['to'])) {
                $params['to'] = explode(',', $params['to']);
            }
            foreach ($params['to'] as $to) {
                $to = trim($to);
                if (!empty($to) && filter_var($to, FILTER_VALIDATE_EMAIL)) {
                    $mail->addTo($to);
                }
            }
        }

        if (isset($params['cc']) && $params['cc']) {
            if (!is_array($params['cc'])) {
                $params['cc'] = explode(',', $params['cc']);
            }
            foreach ($params['cc'] as $cc) {
                $cc = trim($cc);
                if (!empty($cc) && filter_var($cc, FILTER_VALIDATE_EMAIL)) {
                    $mail->addCc($cc);
                }
            }
        }

        if (isset($params['reply']) && $params['reply']) {
            $replyEmail = isset($params['reply']['email']) ? trim($params['reply']['email']) : '';
            $replyName = isset($params['reply']['name']) ? trim($params['reply']['name']) : '';

            if (!empty($replyEmail) && filter_var($replyEmail, FILTER_VALIDATE_EMAIL)) {
                $mail->setReplyTo($replyEmail, $replyName);
            }
        }

        $modelEmailTemplate = new Model_EmailTemplate();
        $emailTemplate = $modelEmailTemplate->getEmailTemplate($template_name, $template_params, $companyId);

        $body = $emailTemplate['body'];
        $subject = $emailTemplate['subject'];

        $templateData = array();
        $templateData['signature_logo'] = $company['company_signature_logo'];
        $templateData['layout'] = isset($params['layout']) ? $params['layout'] : 'email_template';

        if ($body) {
            $templateData['body'] = $body;
            $templateData['subject'] = $subject;

            $body = self::getEmailLayout($templateData);
            $subject = $subject;
        } else {
            $templateData['body'] = isset($params['body']) ? $params['body'] : '';
            $templateData['subject'] = isset($params['subject']) ? $params['subject'] : '';

            $body = self::getEmailLayout($templateData);
            $subject = isset($params['subject']) ? $params['subject'] : '';
        }


        $mail->setSubject($subject);
        $mail->setBodyHtml($body, 'UTF-8');
        // $mail->type = 'text/html';

        if (isset($params['attachment']) && file_exists($params['attachment'])) {
            // send attachment
            $at_file = file_get_contents($params['attachment']);
            $at = $mail->createAttachment($at_file);
            $at->type = 'application/pdf';
            $at->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
            $at->encoding = Zend_Mime::ENCODING_BASE64;

            if (isset($params['attachment_name'])) {
                $at->filename = $params['attachment_name'];
            } else {
                $at->filename = self::getFileName($params['attachment']);
            }
        }

        $loggedUser = CheckAuth::getLoggedUser();
        $marketingEmailLog = array(
            'created' => time(),
            'mailing_list_id' => isset($email_log['mailing_list_id']) && $email_log['mailing_list_id'] ? $email_log['mailing_list_id'] : 0,
            'body' => $body,
            'subject' => $subject,
            'user_id' => isset($loggedUser['user_id']) && $loggedUser['user_id'] ? $loggedUser['user_id'] : 0,
            'email_template_id' => isset($email_log['email_template_id']) && $email_log['email_template_id'] ? $email_log['email_template_id'] : 0
        );



        $modelMarketingEmailLog = new Model_MarketingEmailLog();
        $modelMarketingEmailLog->insert($marketingEmailLog);

        //return file_put_contents('/tmp/' . time() . '-' . $to . '.html', $body);
        return $mail->send($transport);
        //return $mail->send();
    }

    public static function getFileName($file) {
        $parts = explode('/', $file);
        $fileName = $parts[count($parts) - 1];
        return $fileName;
    }

    public static function getEmailLayout($templateData) {
        $view = new Zend_View();
        $view->setScriptPath(APPLICATION_PATH . '/layouts/email');
        $view->templateData = $templateData;
        return $view->render($templateData['layout'] . '.phtml');
    }

    public static function validation($params, &$error_mesages = array()) {

        $isValid = true;

        //required valdation
        $requiredFields = array('to', 'subject');
        $paramsKeys = array_keys($params);

        foreach ($requiredFields as $requiredField) {
            if (!in_array($requiredField, $paramsKeys)) {
                $error_mesages[$requiredField] = 'Required Field';
                $isValid = false;
            }
        }

        if ($isValid) {
            foreach ($params as $key => $value) {
                if (in_array($key, $requiredFields)) {
                    if (empty($value)) {
                        $error_mesages[$key] = 'Required Field';
                        $isValid = false;
                    }
                }
            }


            //email valdation
            $emailValidation = array('to', 'cc');
            foreach ($params as $key => $value) {
                if (in_array($key, $emailValidation)) {
                    if (!empty($value)) {
                        if (!is_array($value)) {
                            $emails = explode(',', $value);
                        } else {
                            $emails = $value;
                        }
                        foreach ($emails as $email) {
                            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                                $error_mesages[$key] = 'Email not valid';
                                $isValid = false;
                            }
                        }
                    }
                }
            }
        }

        return $isValid;
    }

}
