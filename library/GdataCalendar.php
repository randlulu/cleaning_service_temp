<?php

class GdataCalendar extends Zend_Gdata_Calendar {

    public function __construct($client = null, $applicationId = 'MyCompany-MyApp-1.0') {
        parent::__construct($client, $applicationId);
    }

    public function updateEvent($client, $eventId, $event) {
        $eventOld = $this->getEvent($client, $eventId);

        if ($eventOld) {
            $eventOld->title = $event->title;
            $eventOld->content = $event->content;
            $eventOld->where = $event->where;
            $eventOld->when = $event->when;
            try {
                $eventOld->save();
                return $eventOld;
            } catch (Zend_Gdata_App_Exception $e) {
                //var_dump($e);
                return null;
            }
        } else {
            return null;
        }
    }

    public function deleteEvent($client, $eventId) {
        $eventOld = $this->getEvent($client, $eventId);

        if ($eventOld) {
            try {
                $eventOld->delete();
            } catch (Zend_Gdata_App_Exception $e) {
                //var_dump($e);
                return null;
            }
        } else {
            return null;
        }
    }

    public function getEvent($client, $eventId) {
        $gdataCal = new Zend_Gdata_Calendar($client);
        $query = $gdataCal->newEventQuery();
        $query->setUser('default');
        $query->setVisibility('private');
        $query->setProjection('full');
        $query->setEvent($eventId);

        try {
            $eventEntry = $gdataCal->getCalendarEventEntry($query);
            return $eventEntry;
        } catch (Zend_Gdata_App_Exception $e) {
            //var_dump($e);
            return null;
        }
    }

    function getEventByDateRange($client, $startDate = '2007-05-01', $endDate = '2007-08-01') {
        $gdataCal = new Zend_Gdata_Calendar($client);
        $query = $gdataCal->newEventQuery();
        $query->setUser('default');
        $query->setVisibility('private');
        $query->setProjection('full');
        $query->setOrderby('starttime');
        $query->setStartMin($startDate);
        $query->setStartMax($endDate);
        
        try {
            $eventFeed = $gdataCal->getCalendarEventFeed($query);
            return $eventFeed;
        } catch (Zend_Gdata_App_Exception $e) {
            //var_dump($e);
            return null;
        }
    }

}