<?php

class MailingServer {

    public static function sendEmailsDataToMailingServer($post_arr = array()) {
		if(!empty($post_arr)){
			$str = serialize($post_arr);
			$parms = array('str'=>$str);
				
				
			echo 'ret val '.self::do_post_request('http://test.tilecleaners.com.au/webservice/receive-emails-parameters',$parms);
			//////after sending emails save them in email_log with 'out-live' status
			
			$loggedUser = CheckAuth::getLoggedUser();
			$modelEmailTemplate = new Model_EmailTemplate();
			foreach($post_arr as $arr){
				$body = 'No body';
				$subject = 'No Subject';
				////prepare email content 
				$template_params = $arr['template_params'];
				$email_log = $arr['email_log'];
				$params = $arr['data'];
				$template_name = $arr['template_name'];
				
				if(isset($arr['template_name']) && $arr['template_name']){
					$template_name = $arr['template_name'];
					$emailTemplate = $modelEmailTemplate->getEmailTemplate($template_name, $template_params, $params['companyId']);

					$body = $emailTemplate['body'];
					$subject = $emailTemplate['subject'];
				}
				//////
				$email_log = $arr['email_log'];
				$params = $arr['data'];
			
				$emailLog = array(
					'created' => time(),
					'to' => isset($params['to']) && $params['to'] ? $params['to'] : 'No to',
					'body' => isset($params['body']) && $params['body'] ? $params['body'] : $body,
					'subject' => isset($params['subject']) && $params['subject'] ? $params['subject'] : $subject,
					'user_id' => isset($loggedUser['user_id']) && $loggedUser['user_id'] ? $loggedUser['user_id'] : 0,
					'reference_id' => isset($email_log['reference_id']) && $email_log['reference_id'] ? $email_log['reference_id'] : 0,
					'cronjob_history_id' => isset($email_log['cronjob_history_id']) && $email_log['cronjob_history_id'] ? $email_log['cronjob_history_id'] : 0,
					'type' => isset($email_log['type']) && $email_log['type'] ? $email_log['type'] : 'common',
					'status' => 'out-live'
				);
				$modelEmailLog = new Model_EmailLog();
				$modelEmailLog->insert($emailLog);
				
				////save record in email_log_temp
				$emailLogTemp = array(
					'created' => time(),
					'reference_id' => isset($email_log['reference_id']) && $email_log['reference_id'] ? $email_log['reference_id'] : 0,
					'cronjob_history_id' => isset($email_log['cronjob_history_id']) && $email_log['cronjob_history_id'] ? $email_log['cronjob_history_id'] : 0,
					'status' => 'out-live'
				);
				$modelEmailLogTemp = new Model_EmailLogTemp();
				$modelEmailLogTemp->insert($emailLogTemp);
				
			}
			
			
			
		}
		return;
        
	}

    
	public function do_post_request($url, $params = array(), $head = array()) {
		//
		// request body
		//
		$body = array();
		foreach ($params as $param => $value) {
			$body[] = urlencode($param) . '=' . urlencode($value);
		}
		$body = implode('&', $body);

		//
		// request headers
		//
		$head[] = "Content-Type: application/x-www-form-urlencoded; charset=UTF-8";
		$head[] = "Content-Length: " . strlen($body);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $head);
		//echo 'request '.$body;
		$output = curl_exec($ch);
		curl_close($ch);
		return $output;
	}


}
