<?php

class MobileNotification {

    //public static function notify($booking_id, $case, $filters = null) {
    public static function notify($booking_id = 0, $case, $filters = null, $sms_id = 0) {

        $model_booking = new Model_Booking();
        $model_Mongo = new Model_Mongo();
        $model_user = new Model_User();
        $modelIosUserNotificationSetting = new Model_IosUserNotificationSetting();
        $model_contractorServiceBooking = new Model_ContractorServiceBooking();
        $model_bookingAddress = new Model_BookingAddress();
        $model_bookingHistory = new Model_BookingHistory();
        $model_bookingStatus = new Model_BookingStatus();
        $model_complaint = new Model_Complaint();
        $model_payment = new Model_Payment();
        $model_bookingReminder = new Model_BookingReminder();
        $model_bookingContractorPayment = new Model_BookingContractorPayment();
        $model_bookingInvoice = new Model_BookingInvoice();
        $model_bookingEstimate = new Model_BookingEstimate();
        $model_bookingLog = new Model_BookingLog();
        $model_services = new Model_Services();
        $model_image = new Model_Image();
        $model_paymentToContractors = new Model_PaymentToContractors();
        $model_invoiceDiscussion = new Model_InvoiceDiscussion();
        $model_estimateDiscussion = new Model_EstimateDiscussion();
        $modelSmsHistory = new Model_SmsHistorty();
        $modelCustomer = new Model_Customer();
        $sendSmsNotificationToSystem = 0;

        $item_type = "booking";
        if (isset($filters['invoice_id']) && isset($filters['discussion_id'])) {
            $invoice = $model_bookingInvoice->getById($filters['invoice_id']);
            $invoiceNum = 'Invoice <b style="color:#69bdaa;">' . $invoice['invoice_num'] . '</b>';
            $booking_id = $invoice['booking_id'];
            $item_id = $invoice['id'];
            $item_type = "invoice";
        } else if (isset($filters['estimate_id']) && isset($filters['discussion_id'])) {
            $estimate = $model_bookingEstimate->getById($filters['estimate_id']);
            $estimateNum = 'Estimate <b style="color:#69bdaa;">' . $estimate['estimate_num'] . '</b>';
            $booking_id = $estimate['booking_id'];
            $item_id = $estimate['id'];
            $item_type = "estimate";
        } else if (isset($filters['complaint_id']) && isset($filters['discussion_id'])) {
            $complaint = $model_complaint->getById($filters['complaint_id']);
            $booking_id = $complaint['booking_id'];
            $item_type = "complaint";
            $item_id = $complaint['complaint_id'];
        }
        $booking = $model_booking->getById($booking_id);

        $booking_address = $model_bookingAddress->getByBookingId($booking_id);
        $booking_user = $model_user->getById($booking['created_by']);
        $loggedUser = CheckAuth::getLoggedUser();
        $timeStamp = strtotime($booking['booking_start']);
        $day = date("l", $timeStamp);
        $time = date("H:ia", $timeStamp);
        $month_day = date("M d,", $timeStamp);
        $thumbnail = array();
        $userRole = 0;
        $created_date = date("M d,", $booking['created']);
        $created_time = date("H:ia", $booking['created']);
        $booking_status = $model_bookingStatus->getById($booking['status_id']);
        $bookingAddress = '<i class="fa fa-map-marker"></i> <b><i>' . $booking_address['street_address'] . ' ' . $booking_address['suburb'] . ' ' . $booking_address['postcode'] . '</i></b>';
        $pureBookingAddress = $booking_address['street_address'] . ' ' . $booking_address['suburb'] . ' ' . $booking_address['postcode'];
        //echo "after getting booking";
        $bookingNum = 'Booking <b style="color:#69bdaa;">' . $booking['booking_num'] . '</b>';
        $booking_date = '<span style="color:#69bdaa;">' . $day . ', ' . $month_day . ' ' . $time . '</span>';
        $pureBookingDate = $day . ', ' . $month_day . ' ' . $time;
        $notification_user = $booking_user['username'];
        if ($case == 'new booking') {
            $notification_text = 'New ' . $bookingNum . ' ' . $bookingAddress . ' added to your calendar for ' . $booking_date;
            $iosNotification = "New " . $booking['booking_num'] . " " . $pureBookingAddress . " added to your calendar for " . $pureBookingDate;
        } else if ($case == 'booking moved') {
            $bookingLogs = $model_bookingLog->getLogNotificationByBookingId($booking_id);
            $notification_text = $bookingNum . ' ' . $bookingAddress . ' ' . $bookingLogs['booking_moved'];
            $iosNotification = $booking['booking_num'] . ' ' . $pureBookingAddress . ' ' . $bookingLogs['booking_moved'];
        }
        /*         * ************************************ */ 
        else if ($case == 'new sms') {
            $sendSmsNotificationToSystem = 1;
            $smsInfo = $modelSmsHistory->getById($sms_id);
            $mobileFormat = $modelSmsHistory->getMobileFormat($smsInfo['from']);
            $userInfo = $model_user->getByMobile($mobileFormat);
            //$id=$smsInfo['id'];
            if ($userInfo) {
                $userId = $userInfo['user_id'];
                $userName = $userInfo['username'];
            } else {
                //user_name  user_id  notification_text  date_sent  read  sms_id  seen title
                $customerInfo = $modelCustomer->getByMobile($mobileFormat);
                $userId = $customerInfo['customer_id'];
                $userName = $customerInfo['first_name'];
            }
            $notification_text = 'You have received new message from :<b>' . $userName . '</b>, message :<i>' . $smsInfo['message'].'</i>';
            $iosNotification = 'You have received new message from :' . $userName . ', message :' . $smsInfo['message'];
        }
        /*         * ********************************* */
        else if ($case == 'booking cancelled') {
            $bookingLogs = $model_bookingLog->getLogNotificationByBookingId($booking_id);
            if (isset($filters['service_id'])) {
                $services = $model_services->getById($filters['service_id']);
                $notification_text = '' . $bookingNum . ' ' . $bookingAddress . ' for ' . $booking_date . ' has been modified, service "<b>' . $services['service_name'] . '</b>" has been removed';
                $iosNotification = '' . $booking['booking_num'] . ' ' . $pureBookingAddress . ' for ' . $pureBookingDate . ' has been modified, service "' . $services['service_name'] . '" has been removed';
            } else {
                $notification_text = $bookingNum . ' ' . $bookingAddress . ' for ' . $booking_date . ' has been cancelled';
                $iosNotification = $booking['booking_num'] . ' ' . $pureBookingAddress . ' for ' . $pureBookingDate . ' has been cancelled';
            }
        } else if ($case == 'booking on hold') {
            $bookingLogs = $model_bookingLog->getLogNotificationByBookingId($booking_id);

            $notification_text = $bookingNum . ' ' . $bookingAddress . ' for ' . $booking_date . ' has been on hold';
            $iosNotification = $booking['booking_num'] . ' ' . $pureBookingAddress . ' for ' . $pureBookingDate . ' has been on hold';
        } else if ($case == 'booking address changed') {
            $bookingLogs = $model_bookingLog->getLogNotificationByBookingId($booking_id);

            $notification_text = 'Address has changed for ' . $bookingNum . ' ' . $bookingLogs['address_log'] . ' for ' . $booking_date;
            $iosNotification = 'Address has changed for ' . $booking['booking_num'] . ' ' . $bookingLogs['address_log'] . ' for ' . $pureBookingDate;
        } else if ($case == 'new discussion') {

            $bookingDiscussionObj = new Model_BookingDiscussion();
            $booking_discussion = $bookingDiscussionObj->getById($filters['discussion_id']);

            $notification_text = $loggedUser['username'] . ' has posted on the discussion board for ' . $bookingNum . ' ' . $bookingAddress . ' for ' . $booking_date . ' " ' . $booking_discussion['user_message'] . ' "';
            $iosNotification = $loggedUser['username'] . ' has posted on the discussion board for ' . $booking['booking_num'] . ' ' . $pureBookingAddress . ' for ' . $pureBookingDate . ' " ' . $booking_discussion['user_message'] . ' "';


            /* $bookingDiscussionObj = new Model_BookingDiscussion();
              $booking_discussion = $bookingDiscussionObj->getLastBookingDiscussionByBookingId($booking_id);
              $notification_text = $loggedUser['username'] . ' has posted on the discussion board for ' . $bookingNum . ' ' . $bookingAddress . ' for ' . $booking_date . ' " ' . $value['user_message'] . ' "';
              $iosNotification = $loggedUser['username'] . ' has posted on the discussion board for ' . $booking['booking_num'] . ' ' . $pureBookingAddress . ' for ' . $pureBookingDate . ' " ' . $value['user_message'] . ' "'; */
        } else if ($case == 'inquiry discussion') {
            $model_inquiry = new Model_Inquiry();
            $model_inquiryAddress = new Model_InquiryAddress();
            $model_inquiryDiscussion = new Model_InquiryDiscussion();

            $inquiry = $model_inquiry->getById($filters['inquiry_id']);
            $inquiryAddress = $model_inquiryAddress->getByInquiryId($filters['inquiry_id']);
            $inquiryDiscussion = $model_inquiryDiscussion->getById($filters['discussion_id']);
            $inquiryNum = 'Inquiry <b style="color:#69bdaa;">' . $inquiry['inquiry_num'] . '</b>';
            $inquiry_Address = '<i class="fa fa-map-marker"></i> <b><i>' . $inquiryAddress['street_address'] . ' ' . $inquiryAddress['suburb'] . ' ' . $inquiryAddress['postcode'] . '</i></b>';
            $pureInquiryAddress = $inquiryAddress['street_address'] . ' ' . $inquiryAddress['suburb'] . ' ' . $inquiryAddress['postcode'];
            $created_date = date("M d,", $inquiry['created']);
            $created_time = date("H:ia", $inquiry['created']);
            $item_id = $filters['inquiry_id'];
//            $timeStamp = strtotime($inquiry['created']);
            $inquiry_date = '<span style="color:#69bdaa;">' . date('l, M d, H:ia', $inquiry['created']) . '</span>';
            $pureInquiryDate = date('l, M d, H:ia', $inquiry['created']);

            $notification_text = '<b>' . $loggedUser['username'] . '</b> has posted on the discussion board for ' . $inquiryNum . ' ' . $inquiry_Address . ' for ' . $inquiry_date . ' " ' . $inquiryDiscussion['user_message'] . ' "';
            $iosNotification = $loggedUser['username'] . ' has posted on the discussion board for Inquiry ' . $inquiry['inquiry_num'] . ' ' . $pureInquiryAddress . ' for ' . $pureInquiryDate . ' " ' . $inquiryDiscussion['user_message'] . ' "';
        } else if ($case == 'estimate discussion') {

            $estimateDiscussion = $model_estimateDiscussion->getById($filters['discussion_id']);
            $notification_text = '<b>' . $loggedUser['username'] . '</b> has posted on the discussion board for ' . $estimateNum . ' ' . $bookingAddress . ' from ' . $booking_date . ' " ' . $estimateDiscussion['user_message'] . ' "';
            $iosNotification = $loggedUser['username'] . ' has posted on the discussion board for Estimate ' . $estimate['estimate_num'] . ' ' . $pureBookingAddress . ' from ' . $pureBookingDate . ' " ' . $estimateDiscussion['user_message'] . ' "';
        } else if ($case == 'complaint discussion') {
            $complaint = $model_complaint->getById($filters['complaint_id']);
            $model_complaintDiscussion = new Model_complaintDiscussion();
            $complaintDiscussion = $model_complaintDiscussion->getById($filters['discussion_id']);
            $complaintNum = 'Complaint <b style="color:#69bdaa;">' . $complaint['complaint_num'] . '</b>';

            $notification_text = '<b>' . $loggedUser['username'] . '</b> has posted on the discussion board for  ' . $complaintNum . ' from ' . date("l, M d, H:ia", $complaint['created']) . ' " ' . $complaintDiscussion['user_message'] . ' "';
            $iosNotification = $loggedUser['username'] . ' has posted on the discussion board for Complaint ' . $complaint['complaint_num'] . ' from ' . date("l, M d, H:ia", $complaint['created']) . ' " ' . $complaintDiscussion['user_message'] . ' "';
        } else if ($case == 'invoice discussion') {
            $modelInvoiceDiscussion = new Model_InvoiceDiscussion();
            $invoiceDiscussion = $modelInvoiceDiscussion->getById($filters['discussion_id']);

            $notification_text = '<b>' . $loggedUser['username'] . '</b> has posted on the discussion board from ' . $invoiceNum . ' ' . $bookingAddress . ' from ' . $booking_date . ' " ' . $invoiceDiscussion['user_message'] . ' "';
            $iosNotification = $loggedUser['username'] . ' has posted on the discussion board for Invoice ' . $invoice['invoice_num'] . ' ' . $pureBookingAddress . ' from ' . $pureBookingDate . ' " ' . $invoiceDiscussion['user_message'] . ' "';
        } else if ($case == 'new complaint') {
            $complaint = $model_complaint->getLastComplaintByBookingId($booking_id);
            $item_id = $complaint['complaint_id'];
            $created_date = date("M d,", $complaint['created']);
            $created_time = date("H:ia", $complaint['created']);
            $comp_date_time = date("l", $complaint['created']) . ' ' . date("H:i a", $complaint['created']) . ' ' . date("M d", $complaint['created']);
            $notification_text = $bookingNum . ' ' . $bookingAddress . ' ' . $booking_status['name'] . ' on ' . $day . ', ' . $month_day . ' has a complaint placed by ' . $loggedUser['username'] . ' on ' . $comp_date_time . ' " ' . $complaint['comment'] . ' " ';
            $iosNotification = $booking['booking_num'] . ' ' . $pureBookingAddress . ' ' . $booking_status['name'] . ' on ' . $day . ', ' . $month_day . ' has a complaint placed by ' . $loggedUser['username'] . ' on ' . $comp_date_time . ' " ' . $complaint['comment'] . ' " ';
        } else if ($case == 'booking paid') {
            $payment = $model_payment->getLastBookingPaymentByBookingId($booking_id);
            $notification_text = '$' . $payment['amount'] . ' ' . $payment['payment_type'] . ' Payment approved for
                    ' . $bookingNum . ' ' . $bookingAddress . ' ' . $booking_status['name'] . ' ' . $booking_date;
            $iosNotification = '$' . $payment['amount'] . ' ' . $payment['payment_type'] . ' Payment approved for ' . $booking['booking_num'] . ' ' . $pureBookingAddress . ' ' . $booking_status['name'] . ' ' . $pureBookingDate;
        } else if ($case == 'booking confirmation') {
            $booking_remiders = $model_bookingReminder->getLastConfirmationByBookingId($booking_id);
            $notification_text = $bookingNum . ' ' . $bookingAddress . ' for ' . $booking_date . ' has been confirmed with ' . $loggedUser['username'];
            $iosNotification = $booking['booking_num'] . ' ' . $pureBookingAddress . ' for ' . $pureBookingDate . ' has been confirmed with ' . $loggedUser['username'];
        } else if ($case == 'booking update approved') {
            $notification_text = 'Update for ' . $bookingNum . ' ' . $bookingAddress . ' ' . $booking_status['name'] . ' on ' . $booking_date . ' has been approved ';
            $iosNotification = 'Update for ' . $booking['booking_num'] . ' ' . $pureBookingAddress . ' ' . $booking_status['name'] . ' on ' . $pureBookingDate . ' has been approved ';
        } else if ($case == 'payment made to contractor') {
            $bookingContractorPayment = $model_bookingContractorPayment->getBybookingIdAndContractorId($booking_id, $filters['contractor_id']);
            $notification_text = $bookingContractorPayment['contractor_invoice_num'] . ' Invoice has been paid . The Amount of $' . $bookingContractorPayment['amount_paid'] . ' was transferred to your bank account ';
            $iosNotification = $bookingContractorPayment['contractor_invoice_num'] . ' Invoice has been paid . The Amount of $' . $bookingContractorPayment['amount_paid'] . ' was transferred to your bank account ';
        } else if ($case == 'invoice email to client') {
            $item_type = 'invoice';
            $invoice = $model_bookingInvoice->getByBookingId($booking_id);
            $notification_text = $invoice['invoice_num'] . ' ' . $bookingAddress . ' ' . $booking_status['name'] . ' ' . $booking_date . ' has been emailed to client';
            $iosNotification = $invoice['invoice_num'] . ' ' . $pureBookingAddress . ' ' . $booking_status['name'] . ' ' . $pureBookingDate . ' has been emailed to client';
        } else if ($case == 'estimate email to client') {
            $item_type = 'estimate';
            $estimate = $model_bookingEstimate->getByBookingId($booking_id);
            $notification_text = $estimate['estimate_num'] . ' ' . $bookingAddress . ' ' . $booking_status['name'] . ' ' . $booking_date . ' has been emailed to client';
            $iosNotification = $estimate['estimate_num'] . ' ' . $pureBookingAddress . ' ' . $booking_status['name'] . ' ' . $pureBookingDate . ' has been emailed to client';
        } else if ($case == 'booking has new photos') {
            $filter = array();
            $image = $model_image->getAll($booking_id, 'booking', 'ii.item_image_id DESC', $filter, null, $filters['counter']);
            $notification_text = $bookingNum . ' ' . $bookingAddress . ' for ' . $booking_date . ' has ' . $filters['counter'] . ' new photos added <br/>';
            $iosNotification = $booking['booking_num'] . ' ' . $pureBookingAddress . ' for ' . $pureBookingDate . ' has ' . $filters['counter'] . ' new photos added <br/>';
            foreach ($image as $key => $value) {
                $thumbnail[$key] = $value['thumbnail_path'];
                $created_by = $model_user->getById($value['created_by']);
            }
        } else if ($case == 'booking service changed') {
            $notification_text = 'Service details were changed for ' . $bookingNum . ' ' . $bookingAddress . ' for ' . $booking_date;
            $iosNotification = 'Service details were changed for ' . $booking['booking_num'] . ' ' . $pureBookingAddress . ' for ' . $pureBookingDate;
        } else if ($case == 'new contractor discussion') {
            $item_type = "contractor";
            $modelContractorDiscussionMongo = new Model_ContractorDiscussionMongo();
            $contractorDiscussion = $modelContractorDiscussionMongo->getById($filters['discussion_id']);
            foreach ($contractorDiscussion as $key => $value) {

                $posted_user = $model_user->getById($value['user_id']);
                $contractor = $model_user->getById($value['contractor_id']);
                $notification_user = $contractor['user_id'];
                $notification_text = '<b>' . $posted_user['display_name'] . '</b> has posted on ' . $value['contractor_name'] . ' discussion board: " ' . $value['user_message'] . ' ."' . date('l,M d H:ia', $value['created']);
                $iosNotification = $posted_user['display_name'] . ' has posted on ' . $value['contractor_name'] . ' discussion board: " ' . $value['user_message'] . ' ."' . date('l,M d H:ia', $value['created']);
            }
        } else if ($case == 'booking deleted') {
            $notification_text = 'Booking ' . $bookingNum . ' ' . $bookingAddress . ' for ' . $booking_date . ' has been deleted by <b>' . $loggedUser['username'] . '</b>';
            $iosNotification = $booking['booking_num'] . ' ' . $pureBookingAddress . ' for ' . $pureBookingDate . ' has been deleted by ' . $loggedUser['username'];
        } else if ($case == 'booking deleted forever') {
            $notification_text = 'Booking ' . $bookingNum . ' ' . $bookingAddress . ' for ' . $booking_date . ' has been deleted forever by <b>' . $loggedUser['username'] . '</b>';
            $iosNotification = $booking['booking_num'] . ' ' . $pureBookingAddress . ' for ' . $pureBookingDate . ' has been deleted forever by ' . $loggedUser['username'];
        } else if ($case == 'contractor changed') {
            $notification_text = 'New Booking ' . $bookingNum . ' ' . $bookingAddress . ' added to your calendar for ' . $booking_date;
            $iosNotification = "New Booking " . $booking['booking_num'] . " " . $pureBookingAddress . " added to your calendar for " . $pureBookingDate;
        } else if ($case == 'new service') {
            $model_services = new Model_Services();
            $services = $model_services->getById($filters['service_id']);
            $notification_text = 'New Service "<b>' . $services['service_name'] . '</b>" added to booking ' . $bookingNum . '  on your calendar for ' . $booking_date;
            $iosNotification = 'New Service "' . $services['service_name'] . '" added to booking ' . $booking['booking_num'] . ' on your calendar for ' . $pureBookingDate;
        } else if ($case == 'service cancelled') {
            $notification_text = 'Service "<b>' . $services['service_name'] . '</b>" has been removed from your booking ' . $bookingNum . ' ' . $bookingAddress . ' for ' . $booking_date . ' has been cancelled';
            $iosNotification = 'Service ' . $services['service_name'] . ' ' . $booking['booking_num'] . ' has been removed from your booking ' . $pureBookingAddress . ' for ' . $pureBookingDate . ' has been cancelled';
        }
        
        $show = $model_Mongo->getShowByTitle($case);
        if($show == null){
            $show = "";
        }
        $contractors = array();
        if ($booking_id != 0 && !isset($filters['new_contractor_id'])) {
            $contractors = $model_contractorServiceBooking->getContractorIdsByBookingId($booking_id);
        } else if (isset($filters['contractor_id']) || isset($filters['new_contractor_id'])) {
            $contractor_id = (isset($filters['contractor_id']) ? $filters['contractor_id'] : (isset($filters['new_contractor_id']) ? $filters['new_contractor_id'] : 0));
            $contractor = $model_user->getById($contractor_id);
            $contractor['contractor_id'] = $contractor_id;
            $contractors[] = $contractor;
        }

        if (isset($filters['notify_contractor']) && $filters['notify_contractor'] == 0) {
            foreach ($contractors as &$contractor) {
                $contractor['contractor_id'] = 0;
            }
        }

        if (isset($filters['notidy_multiple']) && $filters['notidy_multiple'] && $filters['notidy_multiple'] != 'null') {
            $notifyUsers = $filters['notidy_multiple'];
        } else {
            $notifyUsers = "";
        }
        
        $company_id = CheckAuth::getCompanySession();

        if ($sendSmsNotificationToSystem) {
            //user_name  user_id  notification_text  date_sent  read  sms_id  seen title
            
            $doc = array(
                'user_name' => $userName,
                'user_id' => $userId,
                'notification_text' => $notification_text,
                'date_sent' => time(),
                'read' => "",
                'sms_id' => (int) $sms_id,
                'seen' => "",
                'title' => "$case",
                'show'=>$show,
                'company_id'=> (int) $company_id 
            );
            $id = $model_Mongo->insertNotification($doc);
            if ($id) {
                echo "affected doc is:" . $id;
            }
            exit;
        }

      
        

        foreach ($contractors as $contractor) {

            $sendNotificationToContractors = 1;
            if ($sendNotificationToContractors) {

                $doc = array(
                    'contractor_mobile_info_id' => $contractor['contractor_id'],
                    'contractor_id' => (int) $contractor['contractor_id'],
                    'contractor_name' => $contractor['username'],
                    'notification_text' => $notification_text,
                    'iosNotification' => $iosNotification,
                    'date_sent' => time(),
                    'notification_user' => $notification_user,
                    'created' => $created_date . ' at ' . $created_time,
                    'read' => "",
                    'booking_id' => $booking_id,
                    'title' => $case,
                    'seen' => "",
                    'thumbnails' => $thumbnail,
                    'show' => $show,
                    'created_by' => (int) $loggedUser['user_id'],
                    'item_id' => isset($item_id) ? $item_id : 0,
                    'item_type' => $item_type,
                    'notify_others' => $notifyUsers,
                     'company_id'=> (int) $company_id 
                        //'notify_customers' => $notifyCustomers
                );
                

                $model_Mongo->insertNotification($doc);

                //SEND PUSH NOTIFICATION
                $modelAuthRoleCredential = new Model_AuthRoleCredential();
                $model_ContractorMobileInfo = new Model_ContractorMobileInfo();
                $credential = $modelAuthRoleCredential->getByRoleId(1);
//                if ($_SERVER['REMOTE_ADDR'] == '176.106.46.142') {
//                    var_dump($credential);
//                }
                if (!empty($credential)) {
                    $contractorMobiles = $model_ContractorMobileInfo->getByContractorIdAndOs($contractor['contractor_id'], 'android');
                    $devicetoken = array();
                    if (!empty($contractorMobiles)) {
                        foreach ($contractorMobiles as $contractorMobile) {
                            $devicetoken[] = $contractorMobile['devicetoken'];
                        }
                    }
                    if (!empty($devicetoken)) {
                        $extra = array(
                            'booking_id' => $booking_id,
                            'type' => $case
                        );
                        // $test = new Model_GCMPushMessage('AIzaSyDlZZ6U8GlPfVpXDGeam3mNYMk1hd-kfIk');
                        //New API key from Hussam added by islam
                        $test = new Model_GCMPushMessage('AIzaSyCOZw75Spbsi0flk0S7IoyM73Hcv8dd-4o');

                        $test->setDevices($devicetoken);
                        $test->send($notification_text, $extra);
                    }
                    //FOR IOS
                    $contractorMobiles = $model_ContractorMobileInfo->getByContractorIdAndOs($contractor['contractor_id'], 'ios');

                    $devicetoken = array();
                    if (!empty($contractorMobiles)) {
                        $modelIOSPushMessage = new Model_IOSPushMessage('aps_dis.pem', '');
                        foreach ($contractorMobiles as $contractorMobile) {
                            if (!empty($contractorMobile['devicetoken'])) {
                                $extra = array(
                                    'booking_id' => $booking_id,
                                    'type' => $case
                                );
                                $modelIOSPushMessage->send($iosNotification, $contractorMobile['devicetoken'], $extra);
                            }
                        }
                        $modelIOSPushMessage->close();
                    }
                }


                //End By Islam
            }
        }

        $model_Mongo->closeConnection();
        return;
    }

}
