<?php

class BreadCrumbs {

    private static $title;
    private static $link;
    private static $level = 0;
    private static $bCrumbs = array();
    private static $home = 'Home';
    private static $modules = array();
    private static $separator = '::';
    private static $homeLink = '/';
    private static $className = 'breadcrumb';

    public static function setLevel($level, $title = '', $link = '') {
        self::$title = $title;
        self::$link = $link ? $link : $_SERVER['REQUEST_URI'];
        self::$level = $level;
    }

    public static function getLevel() {
        return self::$level;
    }

    public static function getLinkLevel() {
        return '<li><a href="' . self::$link . '">' . self::$title . '</a></li>';
    }

    public static function setBCrumbs($bCrumbs = array()) {
        ksort($bCrumbs);
        self::$bCrumbs = $bCrumbs;
    }

    public static function getBCrumbs() {
        return self::$bCrumbs;
    }

    public static function setHomeLink($home, $homeLink = '/') {
        self::$home = $home;
        self::$homeLink = $homeLink;
    }

    public static function getHome() {
        return '<li><a href="' . self::$homeLink . '">' . self::$home . '</a></li>';
    }

    public static function setSeparator($separator) {
        self::$separator = $separator;
    }

    public static function getSeparator() {
        return self::$separator;
    }

    public static function setClassName($className) {
        self::$className = $className;
    }

    public static function getClassName() {
        return self::$className;
    }

    public static function setModule($module) {
        self::$modules[] = $module;
    }

    public static function setModules($modules = array()) {
        self::$modules = $modules;
    }

    public static function getModules() {
        return self::$modules;
    }

    public static function allawModule() {
        $cFront = Zend_Controller_Front::getInstance();
        $module = $cFront->getRequest()->getModuleName();

        if (in_array($module, self::getModules())) {
            return true;
        }
        return false;
    }

    public static function breadCrumb() {

        if (!self::allawModule()) {
            self::resetBreadCrumb();
            return '';
        }

        self::breadCrumbBuilder();

        $bCrumbs = '<ol class="' . self::getClassName() . '">';
        $bCrumbs .= implode('<li class="' . self::getClassName() . '_separator" >' . self::getSeparator() . '</li> ', self::getBCrumbs());
        $bCrumbs .= '</ol>';
        return $bCrumbs;
    }

    public static function breadCrumbBuilder() {

        if (empty($_SESSION['breadCrumb'][0])) {
            $_SESSION['breadCrumb'][0] = self::getHome();
        }

        if (self::getLevel()) {
            $_SESSION['breadCrumb'][self::getLevel()] = self::getLinkLevel();
        }

        self::back();
        self::setBCrumbs(self::getSessionBreadCrumb());
    }

    public static function resetBreadCrumb() {
        unset($_SESSION['breadCrumb']);
    }

    public static function back() {
        $breadCrumbs = self::getSessionBreadCrumb();

        foreach ($breadCrumbs AS $key => $value) {
            if ($key > self::getLevel()) {
                unset($_SESSION['breadCrumb'][$key]);
            }
        }
    }

    public static function getSessionBreadCrumb() {
        $breadCrumbs = isset($_SESSION['breadCrumb']) ? $_SESSION['breadCrumb'] : array();
        ksort($breadCrumbs);
        return $breadCrumbs;
    }

}