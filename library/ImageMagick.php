<?php

class ImageMagick {

    public static function create_thumbnail($source, $dest, $destWith=0, $destHeight=0) {

        if (file_exists($source)) {
            $source = escapeshellarg($source);
            $dest = escapeshellarg($dest);

            exec("convert {$source} -resize {$destWith}x{$destHeight}^ -gravity center -extent {$destWith}x{$destHeight} {$dest}");
        }
    }

    public static function scale_image($source, $dest, $destWith=0, $destHeight=0) {
        if (file_exists($source)) {
            $source = escapeshellarg($source);
            $dest = escapeshellarg($dest);
            if ($destHeight) {
                exec("convert {$source} -resize {$destWith}x{$destHeight} {$dest}");
            } else {
                exec("convert {$source} -resize {$destWith} {$dest}");
            }
        }
    }

    public static function resize_image($source, $dest, $destWith=0, $destHeight=0) {
        if (file_exists($source)) {
            $source = escapeshellarg($source);
            $dest = escapeshellarg($dest);
            exec("convert {$source} -resize {$destWith}x{$destHeight}\! {$dest}");
        }
    }
	
	public static function compress_image($source, $dest ) {
        if (file_exists($source)) {
            $source = escapeshellarg($source);
            $dest = escapeshellarg($dest);
            exec("convert {$source} -compress JPEG -quality 50 {$dest}");
        }
    }

    public static function convert($source, $dest) {
        exec("convert {$source} {$dest}");
    }
	
	public static function convertFile($source, $dest) {
        //exec("convert -density 600 {$source} -colorspace RGB -resample 300 {$dest}");
		exec("convert '{$source}' -colorspace RGB -geometry 200 '{$dest}'");

    }

}