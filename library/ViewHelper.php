<?php

class ViewHelper {

    private static $styleSheet = array();
    private static $styleSheetLast = array();
    private static $javaScript = array();
    private static $javaScriptLast = array();
    private static $urlStyleSheet = array();
    private static $urlJavaScript = array();
    private static $verision = 1;
    private static $baseUrl = "";

    /**
     * set Style Sheet
     * 
     * @param string $style_path 
     */
    public static function setStyleSheet($style_path, $last = false) {

        $key = self::getPathKey($style_path);

        if (!$last) {
            unset(self::$styleSheetLast[$key]);
            self::$styleSheet[$key] = $style_path;
        } else {
            unset(self::$styleSheet[$key]);
            self::$styleSheetLast[$key] = $style_path;
        }
    }

    /**
     * get Style Sheet
     * 
     * @return array 
     */
    public static function getStyleSheet() {
        return self::$styleSheet;
    }

    /**
     * get StyleSheet Last
     * 
     * @return array 
     */
    public static function getStyleSheetLast() {
        return self::$styleSheetLast;
    }

    /**
     * set Javascript
     * 
     * @param string $js_path 
     */
    public static function setJavaScript($js_path, $last = false) {

        $key = self::getPathKey($js_path);

        if (!$last) {
            unset(self::$javaScriptLast[$key]);
            self::$javaScript[$key] = $js_path;
        } else {
            unset(self::$javaScript[$key]);
            self::$javaScriptLast[$key] = $js_path;
        }
    }

    /**
     * get Javascript
     * 
     * @return array 
     */
    public static function getJavaScript() {
        return self::$javaScript;
    }

    /**
     * get Javascript Last
     * 
     * @return array 
     */
    public static function getJavaScriptLast() {
        return self::$javaScriptLast;
    }

    /**
     * set Url Style Sheet
     * 
     * @param string $style_path 
     */
    public static function setUrlStyleSheet($style_url) {

        self::$urlStyleSheet[] = $style_url;
    }

    /**
     * get Style Sheet
     * 
     * @return array 
     */
    public static function getUrlStyleSheet() {
        return self::$urlStyleSheet;
    }

    /**
     * set Javascript
     * 
     * @param string $js_path 
     */
    public static function setUrlJavaScript($js_url) {

        self::$urlJavaScript[] = $js_url;
    }

    /**
     * get Javascript
     * 
     * @return array 
     */
    public static function getUrlJavaScript() {
        return self::$urlJavaScript;
    }

    /**
     * set verision
     * 
     * @param int $verision 
     */
    public static function setVerision($verision) {
        self::$verision = $verision;
    }

    /**
     * get verision
     * 
     * @return int 
     */
    public static function getVerision() {
        return self::$verision;
    }

    /**
     * set baseUrl
     * 
     * @param string $baseUrl
     */
    public static function setBaseUrl($baseUrl) {
        self::$baseUrl = $baseUrl;
    }

    /**
     * get baseUrl
     * 
     * @return string 
     */
    public static function getBaseUrl() {
        return self::$baseUrl;
    }

    /**
     * get Path Key
     * 
     * @param string $path
     * @return string 
     */
    public static function getPathKey($path) {

        $path = basename($path);

        $pathKey = preg_replace('#[\:\;\"\'\|\<\,\>\.\?\`\~\!\@\#\$\%\^\&\*\(\)\_\-\+\=\{\[\}\]\/\t\n]#', ' ', trim($path));
        $pathKey = preg_replace('#[\s]+#', '_', trim($pathKey));
        $pathKey = strtolower($pathKey);

        return $pathKey;
    }

    public static function renderHtmlStyleSheet() {

        $allStyleSheet = self::getStyleSheet();
        $allUrlStyleSheet = self::getUrlStyleSheet();
        $allStyleSheetLast = self::getStyleSheetLast();
        $verision = self::getVerision();
        $baseUrl = self::getBaseUrl();

        $total = count($allStyleSheet);
        $counter = 0;
        foreach ($allStyleSheet as $styleSheet) {
            $counter++;
            $breakLine = ($counter != $total) || (count($allUrlStyleSheet) > 0) || (count($allStyleSheetLast) > 0) ? "\n\t" : "\n";
            echo '<link type="text/css" rel="stylesheet" media="screen" href="' . $baseUrl . $styleSheet . '?v=' . $verision . '"/>' . $breakLine;
        }

        if ($allUrlStyleSheet) {
            $total = count($allUrlStyleSheet);
            $counter = 0;
            foreach ($allUrlStyleSheet as $urlStyleSheet) {
                $counter++;
                $breakLine = $counter != $total || (count($allStyleSheetLast) > 0) ? "\n\t" : "\n";
                echo '<link type="text/css" rel="stylesheet" media="screen" href="' . $urlStyleSheet . '"/>' . $breakLine;
            }
        }

        if ($allStyleSheetLast) {
            $total = count($allStyleSheetLast);
            $counter = 0;
            foreach ($allStyleSheetLast as $styleSheet) {
                $counter++;
                $breakLine = ($counter != $total) ? "\n\t" : "\n";
                echo '<link type="text/css" rel="stylesheet" media="screen" href="' . $baseUrl . $styleSheet . '?v=' . $verision . '"/>' . $breakLine;
            }
        }
    }

    public static function renderHtmlJavaScript() {

        $allJavaScript = self::getJavaScript();
        $allJavaScriptLast = self::getJavaScriptLast();
        $allUrlJavaScript = self::getUrlJavaScript();
        $verision = self::getVerision();
        $baseUrl = self::getBaseUrl();

        $total = count($allJavaScript);
        $counter = 0;
        foreach ($allJavaScript as $javaScript) {
            $counter++;
            $breakLine = ($counter != $total) || (count($allUrlJavaScript) > 0) || (count($allJavaScriptLast) > 0) ? "\n\t" : "\n";
            echo '<script src="' . $baseUrl . $javaScript . '?v=' . $verision . '" type="text/javascript"></script>' . $breakLine;
        }

        if ($allUrlJavaScript) {
            $total = count($allUrlJavaScript);
            $counter = 0;
            foreach ($allUrlJavaScript as $urlJavaScript) {
                $counter++;
                $breakLine = $counter != $total || (count($allJavaScriptLast) > 0) ? "\n\t" : "\n";
                echo '<script src="' . $urlJavaScript . '" type="text/javascript"></script>' . $breakLine;
            }
        }

        if ($allJavaScriptLast) {
            $total = count($allJavaScriptLast);
            $counter = 0;
            foreach ($allJavaScriptLast as $javaScript) {
                $counter++;
                $breakLine = ($counter != $total) ? "\n\t" : "\n";
                echo '<script src="' . $baseUrl . $javaScript . '?v=' . $verision . '" type="text/javascript"></script>' . $breakLine;
            }
        }
    }

}

