<?php

class UserSessionForMobile{

  
  public static function openNewSession($accessToken){
			$modelIosUser = new Model_IosUser();
			//$user = $modelIosUser->getByUserInfoById($iosUserId);
			//echo 'accessToken   '.$accessToken;
			$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
			//echo 'tessssssssssssssssst';
			//print_r($user);
			//echo 'is empty '.empty($user);
			if(!empty($user)){
				$email = $user['email1'];
				$password = $user['password'];
				$authrezed = $this->getAuthrezed();
				$authrezed->setIdentity($email);
				$authrezed->setCredential($password);

				$auth = Zend_Auth::getInstance();
				$authrezedResult = $auth->authenticate($authrezed);
				if ($authrezedResult->isValid()) {

					$identity = $authrezed->getResultRowObject();

					$authStorge = $auth->getStorage();
					$authStorge->write($identity);

					CheckAuth::afterlogin(false,'app');
					//CheckAuth::afterlogin(false);
					
					$this->iosLoggedUser = $user['id'];
					
					return 1;
				}
				else{
					return 0;
				}
				
			}
			else{
				return 0;
				
			}
	
	}
	
	public static function getAuthrezed() {
        $modelAuthRole = new Model_AuthRole();
		$contractorRoleId = $modelAuthRole->getRoleIdByName('contractor');
		
        $authrezed = new Zend_Auth_Adapter_DbTable(Zend_Db_Table::getDefaultAdapter());
        $authrezed->setTableName('user')
                ->setIdentityColumn('email1')
                ->setCredentialColumn('password')
                ->setCredentialTreatment("? AND active = 'TRUE' And role_id={$contractorRoleId}");

        return $authrezed;
    }
	
	
	public static function checkAccessTokenOfLoggedUser($accessToken){
			$modelIosUser = new Model_IosUser();
			//$user = $modelIosUser->getByUserInfoById($iosUserId);
			$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
			$loggedUser = CheckAuth::getLoggedUser();
			if($user['user_id'] == $loggedUser['user_id']){
				return 1;
			}
			return 0;
			
	}

}

?>