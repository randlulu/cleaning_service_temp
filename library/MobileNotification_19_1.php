<?php

class MobileNotification {

    public static function notify($booking_id, $case, $contractor_id = 0, $counter = 0) {
        $model_booking = new Model_Booking();
        $model_Mongo = new Model_Mongo();
        $model_user = new Model_User();
        $modelIosUserNotificationSetting = new Model_IosUserNotificationSetting();
        $model_contractorServiceBooking = new Model_ContractorServiceBooking();
        $model_bookingAddress = new Model_BookingAddress();
        $model_bookingHistory = new Model_BookingHistory();
        $model_bookingStatus = new Model_BookingStatus();
        $model_complaint = new Model_Complaint();
        $model_payment = new Model_Payment();
        $model_bookingReminder = new Model_BookingReminder();
        $model_bookingContractorPayment = new Model_BookingContractorPayment();
        $model_bookingInvoice = new Model_BookingInvoice();
        $model_bookingEstimate = new Model_BookingEstimate();
        $model_bookingLog = new Model_BookingLog();
        $model_image = new Model_Image();
        $booking = $model_booking->getById($booking_id);
        $booking_address = $model_bookingAddress->getByBookingId($booking_id);
        $booking_user = $model_user->getById($booking['created_by']);
        $loggedUser = CheckAuth::getLoggedUser();
        $timeStamp = strtotime($booking['booking_start']);
        $day = date("l", $timeStamp);
        $time = date("H:i a", $timeStamp);
        $month_day = date("M d", $timeStamp);
        $thumbnail = array();
        $userRole = 0;
        $created_date = date("M d", $booking['created']);
        $created_time = date("H:i a", $booking['created']);
        $booking_status = $model_bookingStatus->getById($booking['status_id']);
        $bookingAddress = '<i class="fa fa-map-marker"></i> <b><i>' . $booking_address['unit_lot_number'] . ' ' . $booking_address['street_number'] . ' , ' . $booking_address['street_address'] . ' ' . $booking_address['suburb'] . ' ' . $booking_address['state'] . ' ' . $booking_address['postcode'] . '</i></b>';
        //echo "after getting booking";
        $bookingNum = '<b style="color:#69bdaa;">' . $booking['booking_num'] . '</b>';
        $booking_date = '<span style="color:#69bdaa;">' . $day . ', ' . $month_day . ' ' . $time . '</span>';
        
        if ($case == 'new booking') {

            $notification_text = 'New Booking  
			 ' . $bookingNum . ' ' . $bookingAddress . ' added to your calendar for ' . $booking_date;
        } else if ($case == 'booking moved') {
            $bookingLogs = $model_bookingLog->getLogNotificationByBookingId($booking_id);
            $notification_text = $bookingNum . ' ' . $bookingAddress . ' ' . $bookingLogs['booking_moved'];
        } else if ($case == 'booking cancelled') {
            $bookingLogs = $model_bookingLog->getLogNotificationByBookingId($booking_id);

            $notification_text = $bookingNum . ' ' . $bookingAddress . ' for ' . $booking_date . ' has been ' . $bookingLogs['status_id'];
        } else if ($case == 'booking on hold') {
            $bookingLogs = $model_bookingLog->getLogNotificationByBookingId($booking_id);

            $notification_text = $bookingNum . ' ' . $bookingAddress . ' for ' . $booking_date . ' has been ' . $bookingLogs['status_id'];
        } else if ($case == 'booking address changed') {
            $bookingLogs = $model_bookingLog->getLogNotificationByBookingId($booking_id);

            $notification_text = 'Address has changed for ' . $bookingNum . ' ' . $bookingLogs['address_log'] . ' for ' . $booking_date;
        } else if ($case == 'new discussion') {
            $bookingDiscussionObj = new Model_BookingDiscussion();
            $booking_discussion = $bookingDiscussionObj->getLastBookingDiscussionByBookingId($booking_id);
            $notification_text = $loggedUser['username'] . ' has posted on the discussion board for ' . $bookingNum . ' ' . $bookingAddress . ' for ' . $booking_date . ' " ' . $booking_discussion['user_message'] . ' "';
        } else if ($case == 'new complaint') {
            //echo 'te '.$status_id;

            $complaint = $model_complaint->getLastComplaintByBookingId($booking_id);
            $comp_date_time = date("l", $complaint['created']) . ' ' . date("H:i a", $complaint['created']) . ' ' . date("M d", $complaint['created']);
            $notification_text = $bookingNum . ' ' . $bookingAddress . ' ' . $booking_status['name'] . ' on ' . $day . ', ' . $month_day . ' has a complaint placed by ' . $loggedUser['username'] . ' on ' . $comp_date_time . ' " ' . $complaint['comment'] . ' " ';
        } else if ($case == 'booking paid') {
            $payment = $model_payment->getLastBookingPaymentByBookingId($booking_id);
            $notification_text = '$' . $payment['amount'] . ' ' . $payment['payment_type'] . ' Payment approved for 
			' . $bookingNum . ' ' . $bookingAddress . ' ' . $booking_status['name'] . ' ' . $booking_date;
        } else if ($case == 'booking confirmation') {
            $booking_remiders = $model_bookingReminder->getLastConfirmationByBookingId($booking_id);
            $notification_text = $bookingNum . ' ' . $bookingAddress . ' for ' . $booking_date . ' has been confirmed with ' . $loggedUser['username'];
        } else if ($case == 'booking update approved') {
            $notification_text = 'Update for ' . $bookingNum . ' ' . $bookingAddress . ' ' . $booking_status['name'] . ' on ' . $booking_date . ' has been approved ';
        } else if ($case == 'payment made to contractor') {
            $bookingContractorPayment = $model_bookingContractorPayment->getBybookingIdAndContractorId($booking_id, $contractor_id);
            //var_dump($bookingContractorPayment);
            //exit;
            $notification_text = $bookingContractorPayment['contractor_invoice_num'] . ' Invoice has been paid . The Amount of $' . $bookingContractorPayment['amount_paid'] . ' was transferred to your bank account ';
        } else if ($case == 'invoice email to client') {
            $invoice = $model_bookingInvoice->getByBookingId($booking_id);
            $notification_text = $invoice['invoice_num'] . ' ' . $bookingAddress . ' ' . $booking_status['name'] . ' ' . $booking_date . ' has been emailed to client';
        } else if ($case == 'estimate email to client') {
            $estimate = $model_bookingEstimate->getByBookingId($booking_id);
            $notification_text = $estimate['estimate_num'] . ' ' . $bookingAddress . ' ' . $booking_status['name'] . ' ' . $booking_date . ' has been emailed to client';
        } else if ($case == 'booking has new photos') {
            $filter = array();
            $image = $model_image->getAll($booking_id, 'booking', 'ii.item_image_id DESC', $filter, null, $counter);
            $notification_text = $bookingNum . ' ' . $bookingAddress . ' for ' . $booking_date . ' has ' . $counter . ' new photos added <br/>';
            foreach ($image as $key => $value) {
                $thumbnail[$key] = $value['thumbnail_path'];
                $created_by = $model_user->getById($value['created_by']);
            }
        } else if ($case == 'booking service changed') {
            $notification_text = 'Service details were changed for ' . $bookingNum . ' ' . $bookingAddress . ' for ' . $booking_date;
        }


        $contractors = $model_contractorServiceBooking->getContractorIdsByBookingId($booking_id);
        $show = $model_Mongo->getShowByTitle($case);
        foreach ($contractors as $contractor) {
            ///check if the contractor needs to receive notification in this case
            /* $sendNotificationToContractors = $modelIosUserNotificationSetting->getBySettingNameAndUserId($contractor['contractor_id'],$settingName);
             */

            $sendNotificationToContractors = 1;
            if ($sendNotificationToContractors) {

                $doc = array(
                    'contractor_mobile_info_id' => $contractor['contractor_id'],
                    'contractor_id' => $contractor['contractor_id'],
                    'contractor_name' => $contractor['username'],
                    'notification_text' => $notification_text,
                    'date_sent' => time(),
                    'notification_user' => $booking_user['username'],
                    'created' => $created_date . ' at ' . $created_time,
                    'read' => "",
                    'booking_id' => $booking_id,
                    'title' => $case,
                    'seen' => "",
                    'thumbnails' => $thumbnail,
                    'show'=>$show,
                    'created_by'=>$loggedUser['user_id']
                );

                $model_Mongo->insertNotification($doc);
				
				//SEND PUSH NOTIFICATION
				$model_ContractorMobileInfo = new Model_ContractorMobileInfo();
				$contractorMobiles = $model_ContractorMobileInfo->getByContractorIdAndOs($contractor['contractor_id'],'android');
				
				$devicetoken = array();
				if(!empty($contractorMobiles)){
					foreach($contractorMobiles as $contractorMobile){
						$devicetoken[] = $contractorMobile['devicetoken'];
					}
				}
				if(!empty($devicetoken)){
					$test = new Model_GCMPushMessage('AIzaSyDlZZ6U8GlPfVpXDGeam3mNYMk1hd-kfIk');
					$test->setDevices($devicetoken); 
					$test->send($notification_text);
				}
				//End By Islam
				
            }
        }

        $model_Mongo->closeConnection();
        return;
    }

}
