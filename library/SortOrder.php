<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SortOrder
 */
class SortOrder {

    private $url;
    private $method;
    private $sort;
    private $method_qs;
    private $sort_qs;
    private $method_array;
    private $query_string;
    private $qs_sort;
    private $qs_method;

    function __construct($params = array()) {
        $frontController = Zend_Controller_Front::getInstance();
        $requestUrl = $frontController->getBaseUrl() . (isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '');

        $this->set_url(isset($params['url']) ? $params['url'] : $requestUrl);
        $this->set_method_array(array('asc', 'desc'));
        $this->set_sort(isset($params['sort']) ? $params['sort'] : 'id');
        $this->set_sort_qs(isset($params['sort_qs']) ? $params['sort_qs'] : 'sort');
        $this->set_method(isset($params['method']) ? $params['method'] : 'asc');
        $this->set_method_qs(isset($params['method_qs']) ? $params['method_qs'] : 'method');

        $this->handle_query_string();
    }

    public function set_url($url) {
        $this->url = $url;
    }

    public function get_url() {
        return $this->url;
    }

    public function set_method($method) {
        if (in_array($method, $this->method_array)) {
            $this->method = $method;
        } else {
            $this->method = $this->method_array[0];
        }
    }

    public function get_method() {
        return $this->method;
    }

    public function set_method_qs($method_qs) {
        $this->method_qs = $method_qs;
    }

    public function get_method_qs() {
        return $this->method_qs;
    }

    public function set_sort($sort) {
        $this->sort = $sort;
    }

    public function get_sort() {
        return $this->sort;
    }

    public function set_sort_qs($sort_qs) {
        $this->sort_qs = $sort_qs;
    }

    public function get_sort_qs() {
        return $this->sort_qs;
    }

    public function set_method_array($method_array) {
        $this->method_array = $method_array;
    }

    public function get_method_array() {
        return $this->method_array;
    }

    public function set_query_string($query_string) {
        $this->query_string = $query_string;
    }

    public function get_query_string() {
        return $this->query_string;
    }

    public function set_qs_method($qs_method) {
        $this->qs_method = $qs_method;
    }

    public function get_qs_method() {
        return $this->qs_method;
    }

    public function set_qs_sort($qs_sort) {
        $this->qs_sort = $qs_sort;
    }

    public function get_qs_sort() {
        return $this->qs_sort;
    }

    public function handle_query_string() {
        $url = explode('?', $this->url);
        $matches = array();
        preg_match("/^[?].*/", $this->url, $matches);
        if (empty($matches)) {
            $this->set_url($url[0]);
        }
        if (isset($url[1]) AND $url[1]) {
            parse_str($url[1], $query_string_array);
            if (isset($query_string_array[$this->sort_qs])) {
                $this->qs_sort = $query_string_array[$this->sort_qs];
                unset($query_string_array[$this->sort_qs]);
            }
            if (isset($query_string_array[$this->method_qs])) {
                $this->qs_method = $query_string_array[$this->method_qs];
                unset($query_string_array[$this->method_qs]);
            }
            $query_string = http_build_query($query_string_array);
            $this->set_query_string($query_string);
        }
    }

    public function generate_url() {
        $methodOrder = '';
        $methodOrder = $this->sort_qs . '=' . $this->sort . '&' . $this->method_qs . '=' . $this->method;
        if (isset($this->query_string) AND $this->query_string) {
            return $this->url . '?' . $this->query_string . $methodOrder;
        } else {
            return $this->url . '?' . $methodOrder;
        }
    }

    public static function generate($sort, $method = 'asc') {
        $so = new SortOrder();
        $so->set_sort($sort);
        $so->set_method($method);
        if ($so->get_qs_sort() == $sort AND $so->get_qs_method() == $method) {
            if ($method == 'asc') {
                $so->set_method('desc');
            } else {
                $so->set_method('asc');
            }
        }
        $methodOrder = '';
        $methodOrder = $so->sort_qs . '=' . $so->sort . '&' . $so->method_qs . '=' . $so->method;
        if (isset($so->query_string) AND $so->query_string) {
            return $so->url . '?' . $so->query_string . '&' . $methodOrder;
        } else {
            return $so->url . '?' . $methodOrder;
        }
    }

    public static function generate_link($label, $sort, $method = 'asc') {
        $so = new SortOrder();
        $so->set_sort($sort);
        $so->set_method($method);
        if ($so->get_qs_sort() == $sort AND $so->get_qs_method() == $method) {
            if ($method == 'asc') {
                $so->set_method('desc');
            } else {
                $so->set_method('asc');
            }
        }
        $methodOrder = '';
        $methodOrder = $so->sort_qs . '=' . $so->sort . '&' . $so->method_qs . '=' . $so->method;
        $url = '';
        if (isset($so->query_string) AND $so->query_string) {
            $url = $so->url . '?' . $so->query_string . '&' . $methodOrder;
        } else {
            $url = $so->url . '?' . $methodOrder;
        }

        if ($so->method == 'asc') {
            $img = '<img src="/pic/down_arrow.png" border="0"/>';
        } else {
            $img = '<img src="/pic/up_arrow.png" border="0"/>';
        }

        $title = $label;
        $link = "<a href='{$url}' class='sort_link' >{$title}</a> {$img}";
        return $link;
    }

}

/* End of file SortOrder.php */