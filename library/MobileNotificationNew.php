<?php

class MobileNotificationNew {

    //public static function notify($booking_id, $case, $filters = null) {
    public static function notify($case = null, $filters = null) {

        $model_booking = new Model_Booking();
        $model_MongoTest = new Model_MongoTest();
        $model_user = new Model_User();
        $modelIosUserNotificationSetting = new Model_IosUserNotificationSetting();
        $model_contractorServiceBooking = new Model_ContractorServiceBooking();
        $model_bookingAddress = new Model_BookingAddress();
        $model_bookingHistory = new Model_BookingHistory();
        $model_bookingStatus = new Model_BookingStatus();
        $model_complaint = new Model_Complaint();
        $model_payment = new Model_Payment();
        $model_bookingReminder = new Model_BookingReminder();
        $model_bookingContractorPayment = new Model_BookingContractorPayment();
        $model_bookingInvoice = new Model_BookingInvoice();
        $model_bookingEstimate = new Model_BookingEstimate();
        $model_bookingLog = new Model_BookingLog();
        $model_services = new Model_Services();
        $model_image = new Model_Image();
        $model_paymentToContractors = new Model_PaymentToContractors();
        $model_invoiceDiscussion = new Model_InvoiceDiscussion();
        $model_estimateDiscussion = new Model_EstimateDiscussion();
        $modelSmsHistory = new Model_SmsHistorty();
        $modelCustomer = new Model_Customer();
        $model_inquiry = new Model_Inquiry();
        $model_inquiryAddress = new Model_InquiryAddress();
        $twilioaccountInfoObj = new Model_TwilioAccountInfo();
        $model_services = new Model_Services();
        $model_inquiryDiscussion = new Model_InquiryDiscussion();
        $model_credential = new Model_AuthCredential();
        $model_authRoleCredential = new Model_AuthRoleCredential();
        $loggedUser = CheckAuth::getLoggedUser();
        $thumbnail = array();
        $sendSmsNotificationToSystem = 0;
        $customer_name = '';
        $router = Zend_Controller_Front::getInstance()->getRouter();
        $contractors = array();
        $notifed_users = array();
        $model_authRole = new Model_AuthRole();
        $contractorRoleId = $model_authRole->getRoleIdByName('contractor');
        if (isset($filters)) {
            // prepate common data
            $item_type = isset($filters['item_type']) ? $filters['item_type'] : 'others';
            $item_id = isset($filters['item_id']) ? $filters['item_id'] : 0;
            $created_by = $loggedUser['user_id'];
            $created_by_role_id = $loggedUser['role_id'];
            $created_by_name = (!empty($loggedUser['display_name']) ? $loggedUser['display_name'] : $loggedUser['username']);
            $date_sent = time();
            $notification_type = 'notification';
            $credential_name = getCredentialFormat($case);
            // preapare rest of notification data by item_type and case type
            if ($filters['item_type'] == 'booking') {
                $contractors = $model_contractorServiceBooking->getContractorIdsByBookingId($item_id);
                //here all cases that related of booking
                //get data for booking and handle it to use on notification text by specific format
                $notification_type = 'notification';
                $booking = $model_booking->getById($filters['item_id']);
                $booking_address = $model_bookingAddress->getByBookingId($booking['booking_id']);
                //get start date and convert it on specifice formate
                $timeStamp = strtotime($booking['booking_start']);
                $day = date("l", $timeStamp);
                $time = date("H:ia", $timeStamp);
                $month_day = date("M d,", $timeStamp);
                $booking_date = '<span style="color:#69bdaa;">' . $day . ', ' . $month_day . ' ' . $time . '</span>';
                $pureBookingDate = $day . ', ' . $month_day . ' ' . $time;
                //

                $booking_status = $model_bookingStatus->getById($booking['status_id']);
                // get booking address for text of notifications
                $bookingAddress = '<i class="fa fa-map-marker"></i> <b><i>' . $booking_address['street_address'] . ' ' . $booking_address['suburb'] . ' ' . $booking_address['postcode'] . '</i></b>';
                $pureBookingAddress = $booking_address['street_address'] . ' ' . $booking_address['suburb'] . ' ' . $booking_address['postcode'];
                //
                $bookingNum = 'Booking <b style="color:#69bdaa;">' . $booking['booking_num'] . '</b>';
                // now we will check all cases of booking to prepare notification text 
                // case new booking 
                if ($case == 'new booking') {
                    $notification_text = 'New ' . $bookingNum . ' ' . $bookingAddress . ' added to your calendar for ' . $booking_date;
                    $iosNotification = "New " . $booking['booking_num'] . " " . $pureBookingAddress . " added to your calendar for " . $pureBookingDate;
                }
                // case booking moved
                else if ($case == 'booking moved') {
                    // still have problem
                    $bookingLogs = $model_bookingLog->getLogNotificationByBookingId($item_id);

                    $notification_text = $bookingNum . ' ' . $bookingAddress . ' ' . $bookingLogs['booking_moved'];
                    $iosNotification = $booking['booking_num'] . ' ' . $pureBookingAddress . ' ' . $bookingLogs['booking_moved'];
                }
                //case request access
                if ($case == 'request access') {
                    //admin
                    $accept = $router->assemble(array('id' => $item_id, 'name' => $loggedUser['display_name']), 'acceptRequest');
                    $reject = $router->assemble(array('id' => $item_id, 'name' => $loggedUser['display_name']), 'rejectRequest');
                    $notification_text = $loggedUser['display_name'] . ' ' . 'is requesting access to edit ' . $bookingNum . ' ' . $bookingAddress . ' ' . $booking_status['name'] . ' ' . $booking_date . '<br>' . '<strong><a class="btn btn-warning btn-xs" href="' . $accept . '">' . "Accept" . '<strong></a>' . '  OR  ' . '<strong><a class="btn btn-warning btn-xs" href="' . $reject . '">' . "Reject" . '</strong></a>';
                    $iosNotification = $loggedUser['display_name'] . ' ' . 'is requesting access to edit ' . $bookingNum . ' ' . $bookingAddress . ' ' . $booking_status['name'] . ' ' . $booking_date . '<br>' . '<strong><a href="' . $accept . '">' . "Accept" . '<strong></a>' . '  OR  ' . '<strong><a href="' . $reject . '">' . "Reject" . '</strong></a>';
                    $RoleCredentials = $model_authRoleCredential->getRolesByCredentialName($credential_name);
                    foreach ($RoleCredentials as $key => $value) {
                        if ($value['role_id'] != $contractorRoleId) {
                            $users = $model_user->getActiveUserByRoleId($value['role_id']);
                            $notifed_users[] = $users;
                        }
                    }
                }
                //case accept request
                else if ($case == 'accept request') {
                    //contractor
                    $notification_text = $loggedUser['display_name'] . ' ' . 'has accepted your request to edit ' . $bookingNum . ' ' . $bookingAddress . ' ' . $booking_status['name'] . ' ' . $booking_date;
                    $iosNotification = $loggedUser['display_name'] . ' ' . 'is accepted your request to edit this ' . $bookingNum . ' ' . $bookingAddress . ' ' . $booking_status['name'] . ' ' . $booking_date;
                }
                //case reject request
                else if ($case == 'reject request') {
                    //contractor
                    $notification_text = $loggedUser['display_name'] . ' ' . 'has rejected your request to edit ' . $bookingNum . ' ' . $bookingAddress . ' ' . $booking_status['name'] . ' ' . $booking_date;
                    $iosNotification = $loggedUser['display_name'] . ' ' . 'rejected your request to edit this ' . $bookingNum . ' ' . $bookingAddress . ' ' . $booking_status['name'] . ' ' . $booking_date;
                }
                // case to approve edit
                else if ($case == 'to approve edit') {
                    //admin
                    $approve = $router->assemble(array('id' => $item_id), 'toApproveEdit');

                    $notification_text = $loggedUser['display_name'] . ' ' . 'made changes to ' . $bookingNum . ' ' . $bookingAddress . ' ' . $booking_status['name'] . ' ' . $booking_date . '<br>' . '<strong><a class="btn btn-warning btn-xs" href="' . $approve . '">' . "Approve changes" . '<strong></a>';
                    $iosNotification = $loggedUser['display_name'] . ' ' . 'do changes in this ' . $bookingNum . ' ' . $bookingAddress . ' ' . $booking_status['name'] . ' ' . $booking_date . '<br>' . '<strong><a href="' . $approve . '">' . "Approve changes" . '<strong></a>';
                    $RoleCredentials = $model_authRoleCredential->getRolesByCredentialName($credential_name);
                    foreach ($RoleCredentials as $key => $value) {
                        if ($value['role_id'] != $contractorRoleId) {
                            $users = $model_user->getActiveUserByRoleId($value['role_id']);
                            $notifed_users[] = $users;
                        }
                    }
                }
                // case approve edit
                else if ($case == 'approve edit') {
                    //contractor
                    $notification_text = $loggedUser['display_name'] . ' ' . 'approve your changes for ' . $bookingNum . ' ' . $bookingAddress . ' ' . $booking_status['name'] . ' ' . $booking_date;
                    $iosNotification = $loggedUser['display_name'] . ' ' . 'approve your changes for this ' . $bookingNum . ' ' . $bookingAddress . ' ' . $booking_status['name'] . ' ' . $booking_date;
                }
                //case booking cancelled
                else if ($case == 'booking cancelled') {
                    $bookingLogs = $model_bookingLog->getLogNotificationByBookingId($item_id);
                    if (isset($filters['service_id'])) {
                        $services = $model_services->getById($filters['service_id']);
                        $notification_text = '' . $bookingNum . ' ' . $bookingAddress . ' for ' . $booking_date . ' has been modified, service "<b>' . $services['service_name'] . '</b>" has been removed';
                        $iosNotification = '' . $booking['booking_num'] . ' ' . $pureBookingAddress . ' for ' . $pureBookingDate . ' has been modified, service "' . $services['service_name'] . '" has been removed';
                    } else {
                        $notification_text = $bookingNum . ' ' . $bookingAddress . ' for ' . $booking_date . ' has been cancelled';
                        $iosNotification = $booking['booking_num'] . ' ' . $pureBookingAddress . ' for ' . $pureBookingDate . ' has been cancelled';
                    }
                }
                // case booking changed
                else if ($case == 'booking changed') {
                    $bookingLogs = $model_bookingLog->getLogNotificationByBookingId($item_id);
                    if (isset($filters['service_id'])) {
                        $services = $model_services->getById($filters['service_id']);
                        $notification_text = '' . $bookingNum . ' ' . $bookingAddress . ' for ' . $booking_date . ' has been modified, service "<b>' . $services['service_name'] . '</b>" has been removed';
                        $iosNotification = '' . $booking['booking_num'] . ' ' . $pureBookingAddress . ' for ' . $pureBookingDate . ' has been modified, service "' . $services['service_name'] . '" has been removed';
                    } else {
                        $notification_text = $bookingNum . ' ' . $bookingAddress . ' for ' . $booking_date . ' has been modified';
                        $iosNotification = $booking['booking_num'] . ' ' . $pureBookingAddress . ' for ' . $pureBookingDate . ' has been modified';
                    }
                }
                // case booking on hold
                else if ($case == 'booking on hold') {
                    $bookingLogs = $model_bookingLog->getLogNotificationByBookingId($item_id);

                    $notification_text = $bookingNum . ' ' . $bookingAddress . ' for ' . $booking_date . ' has been on hold';
                    $iosNotification = $booking['booking_num'] . ' ' . $pureBookingAddress . ' for ' . $pureBookingDate . ' has been on hold';
                }
                //case booking address changed
                else if ($case == 'booking address changed') {
                    $bookingLogs = $model_bookingLog->getLogNotificationByBookingId($item_id);

                    $notification_text = 'Address has changed for ' . $bookingNum . ' ' . $bookingLogs['address_log'] . ' for ' . $booking_date;
                    $iosNotification = 'Address has changed for ' . $booking['booking_num'] . ' ' . $bookingLogs['address_log'] . ' for ' . $pureBookingDate;
                }
                // case booking discussio
                else if ($case == 'booking discussion for notification') {

                    $bookingDiscussionObj = new Model_BookingDiscussion();
                    $booking_discussion = $bookingDiscussionObj->getById($filters['discussion_id']);
                    $notification_text = $loggedUser['username'] . ' has posted on the discussion board for ' . $bookingNum . ' ' . $bookingAddress . ' for ' . $booking_date . ' " ' . $booking_discussion['user_message'] . ' "';
                    $iosNotification = $loggedUser['username'] . ' has posted on the discussion board for ' . $booking['booking_num'] . ' ' . $pureBookingAddress . ' for ' . $pureBookingDate . ' " ' . $booking_discussion['user_message'] . ' "';
                    if (isset($filters['notify_multiple']) && !empty($filters['notify_multiple'])) {
                        foreach ($filters['notify_multiple'] as $key => $user) {
                            $other_users = $model_user->getById((int) $user['user_id']);
                            $notifed_users[$key][] = $other_users;
                        }
                    }
                    if (isset($filters['visibility']) && !empty($filters['visibility'])) {
                        if ($filters['visibility'] != '1') {
                            $notifed_users[] = $contractors;
                        } else {
                            $contractors = array();
                        }
                    }
                }

                //case booking paid
                else if ($case == 'booking paid') {
                    $payment = $model_payment->getLastBookingPaymentByBookingId($item_id);
                    $notification_text = '$' . $payment['amount'] . ' ' . $payment['payment_type'] . ' Payment approved for
                    ' . $bookingNum . ' ' . $bookingAddress . ' ' . $booking_status['name'] . ' ' . $booking_date;
                    $iosNotification = '$' . $payment['amount'] . ' ' . $payment['payment_type'] . ' Payment approved for ' . $booking['booking_num'] . ' ' . $pureBookingAddress . ' ' . $booking_status['name'] . ' ' . $pureBookingDate;
                }
                //case booking confirmation
                else if ($case == 'booking confirmation') {
                    $booking_remiders = $model_bookingReminder->getLastConfirmationByBookingId($item_id);
                    $notification_text = $bookingNum . ' ' . $bookingAddress . ' for ' . $booking_date . ' has been confirmed with ' . $loggedUser['username'];
                    $iosNotification = $booking['booking_num'] . ' ' . $pureBookingAddress . ' for ' . $pureBookingDate . ' has been confirmed with ' . $loggedUser['username'];
                }
                // case booking update approved
                else if ($case == 'booking update approved') {
                    $notification_text = 'Update for ' . $bookingNum . ' ' . $bookingAddress . ' ' . $booking_status['name'] . ' on ' . $booking_date . ' has been approved ';
                    $iosNotification = 'Update for ' . $booking['booking_num'] . ' ' . $pureBookingAddress . ' ' . $booking_status['name'] . ' on ' . $pureBookingDate . ' has been approved ';
                }
                //case booking has new photos
                else if ($case == 'booking has new photos') {
                    $filter = array();
                    $image = $model_image->getAll($item_id, $item_type, 'ii.item_image_id DESC', $filter, null, $filters['counter']);

                    $notification_text = $bookingNum . ' ' . $bookingAddress . ' for ' . $booking_date . ' has ' . $filters['counter'] . ' new photos added <br/>';
                    $iosNotification = $booking['booking_num'] . ' ' . $pureBookingAddress . ' for ' . $pureBookingDate . ' has ' . $filters['counter'] . ' new photos added <br/>';
                    foreach ($image as $key => $value) {
                        $thumbnail[$key] = $value['thumbnail_path'];
                    }
                    $RoleCredentials = $model_authRoleCredential->getRolesByCredentialName($credential_name);
                    foreach ($RoleCredentials as $key => $value) {
                        $users = $model_user->getActiveUserByRoleId($value['role_id']);
                        $notifed_users[] = $users;
                    }
                }
                //case booking service changed
                else if ($case == 'booking service changed') {
                    $notification_text = 'Service details were changed for ' . $bookingNum . ' ' . $bookingAddress . ' for ' . $booking_date;
                    $iosNotification = 'Service details were changed for ' . $booking['booking_num'] . ' ' . $pureBookingAddress . ' for ' . $pureBookingDate;
                }
                // case booking deleted
                else if ($case == 'booking deleted') {
                    $notification_text = 'Booking ' . $bookingNum . ' ' . $bookingAddress . ' for ' . $booking_date . ' has been deleted by <b>' . $loggedUser['username'] . '</b>';
                    $iosNotification = $booking['booking_num'] . ' ' . $pureBookingAddress . ' for ' . $pureBookingDate . ' has been deleted by ' . $loggedUser['username'];
                }
                // case booking deleted forever
                else if ($case == 'booking deleted forever') {
                    $notification_text = 'Booking ' . $bookingNum . ' ' . $bookingAddress . ' for ' . $booking_date . ' has been deleted forever by <b>' . $loggedUser['username'] . '</b>';
                    $iosNotification = $booking['booking_num'] . ' ' . $pureBookingAddress . ' for ' . $pureBookingDate . ' has been deleted forever by ' . $loggedUser['username'];
                }
                // case new service
                else if ($case == 'new service') {
                    $services = $model_services->getById($filters['service_id']);
                    $notification_text = 'New Service "<b>' . $services['service_name'] . '</b>" added to booking ' . $bookingNum . '  on your calendar for ' . $booking_date;
                    $iosNotification = 'New Service "' . $services['service_name'] . '" added to booking ' . $booking['booking_num'] . ' on your calendar for ' . $pureBookingDate;
                }
                // case service cancelled
                else if ($case == 'service cancelled') {
                    $services = $model_services->getById($filters['service_id']);
                    $notification_text = 'Service "<b>' . $services['service_name'] . '</b>" has been removed from your booking ' . $bookingNum . ' ' . $bookingAddress . ' for ' . $booking_date . ' has been cancelled';
                    $iosNotification = 'Service ' . $services['service_name'] . ' ' . $booking['booking_num'] . ' has been removed from your booking ' . $pureBookingAddress . ' for ' . $pureBookingDate . ' has been cancelled';
                }
                // case contractor rejected service
                //Rand
                else if ($case == 'contractor rejected service') {
                    $bookingLogs = $model_bookingLog->getLogNotificationByBookingId($item_id);
                    if (isset($filters['service_id'])) {
                        $services = $model_services->getById($filters['service_id']);
                        $notification_text = '' . $loggedUser['display_name'] . ' has rejected service ' . $services['service_name'] . ' for ' . $bookingNum . ' ' . $bookingAddress . ' for ' . $booking_date . '</b>';
                        $iosNotification = '' . $loggedUser['display_name'] . ' has rejected service ' . $services['service_name'] . ' for ' . $booking['booking_num'] . ' ' . $pureBookingAddress . ' for ' . $pureBookingDate;
                    } else {
                        $notification_text = '' . $loggedUser['display_name'] . ' has rejected a service for ' . $bookingNum . ' ' . $bookingAddress . ' for ' . $booking_date . '</b>';
                        $iosNotification = '' . $loggedUser['display_name'] . ' has rejected a service for ' . $booking['booking_num'] . ' ' . $pureBookingAddress . ' for ' . $pureBookingDate;
                    }

                    $RoleCredentials = $model_authRoleCredential->getRolesByCredentialName($credential_name);
                    foreach ($RoleCredentials as $key => $value) {
                        if ($value['role_id'] != $contractorRoleId) {
                            $users = $model_user->getActiveUserByRoleId($value['role_id']);
                            $notifed_users[] = $users;
                        }
                    }
                } else if ($case == 'customer upload photos') {
                    $customer = $modelCustomer->getById($created_by);
                    $customer_name = 'Customer <b style="color:#69bdaa;">' . $customer['first_name'] . ' ' . $customer['last_name'] . '</b>';


                    $newFilter = array();
                    $image = $model_image->getAll($item_id, $item_type, 'ii.item_image_id DESC', $newFilter, null, $filters['counter']);
                    $notification_text = $bookingNum . ' ' . $bookingAddress . ' for ' . $booking_date . ' has ' . $filters['counter'] . ' new photos added By ' . $customer_name . '<br/>';
                    $iosNotification = 'Booking ' . $booking['booking_num'] . ' ' . $pureBookingAddress . ' for ' . $pureBookingDate . ' has ' . $filters['counter'] . ' new photos added By ' . $customer['first_name'] . ' ' . $customer['last_name'] . '<br/>';
                    foreach ($image as $key => $value) {
                        $thumbnail[$key] = $value['thumbnail_path'];
                    }

                    $RoleCredentials = $model_authRoleCredential->getRolesByCredentialName($credential_name);
                    foreach ($RoleCredentials as $key => $value) {
                        $users = $model_user->getActiveUserByRoleId($value['role_id']);
                        $notifed_users[] = $users;
                    }
                }



                if (isset($filters['new_contractor_id'])) {
                    $contractor = $model_user->getById($filters['new_contractor_id']);
                    $notifed_users[][] = $contractor;
                } else if (!empty($contractors) && empty($notifed_users)) {
                    $notifed_users[] = $contractors;
                }

//                var_dump($notification_text);
//                exit;
            } else if ($filters['item_type'] == 'estimate') {

                //here all cases that related of booking
                //get data for booking and handle it to use on notification text by specific format
                $notification_type = 'notification';
                $estimate = $model_bookingEstimate->getById($item_id);
                $booking = $model_booking->getById($estimate['booking_id']);
                $contractors = $model_contractorServiceBooking->getContractorIdsByBookingId($booking['booking_id']);
                $booking_address = $model_bookingAddress->getByBookingId($booking['booking_id']);
                $estimateNum = 'Estimate <b style="color:#69bdaa;">' . $estimate['estimate_num'] . '</b>';

                //get start date and convert it on specifice formate
                $timeStamp = strtotime($booking['booking_start']);
                $day = date("l", $timeStamp);
                $time = date("H:ia", $timeStamp);
                $month_day = date("M d,", $timeStamp);
                $booking_date = '<span style="color:#69bdaa;">' . $day . ', ' . $month_day . ' ' . $time . '</span>';
                $pureBookingDate = $day . ', ' . $month_day . ' ' . $time;
                //
                $booking_status = $model_bookingStatus->getById($booking['status_id']);
                // get booking address for text of notifications
                $bookingAddress = '<i class="fa fa-map-marker"></i> <b><i>' . $booking_address['street_address'] . ' ' . $booking_address['suburb'] . ' ' . $booking_address['postcode'] . '</i></b>';
                $pureBookingAddress = $booking_address['street_address'] . ' ' . $booking_address['suburb'] . ' ' . $booking_address['postcode'];
                //
                //$bookingNum = 'Booking <b style="color:#69bdaa;">' . $booking['booking_num'] . '</b>';
                //here all cases that related of estimate
                // case estimate discussion
                if ($case == 'estimate discussion for notification') {
                    $notification_type = 'discussion';
                    $estimateDiscussion = $model_estimateDiscussion->getById($filters['discussion_id']);
                    $notification_text = '<b>' . $created_by_name . '</b> has posted on the discussion board for ' . $estimateNum . ' ' . $bookingAddress . ' from ' . $booking_date . ' " ' . $estimateDiscussion['user_message'] . ' "';
                    $iosNotification = $created_by_name . ' has posted on the discussion board for Estimate ' . $estimate['estimate_num'] . ' ' . $pureBookingAddress . ' from ' . $pureBookingDate . ' " ' . $estimateDiscussion['user_message'] . ' "';

                    if (isset($filters['notify_multiple']) && !empty($filters['notify_multiple'])) {
                        foreach ($filters['notify_multiple'] as $key => $user) {
                            $other_users = $model_user->getById((int) $user['user_id']);
                            $notifed_users[$key][] = $other_users;
                        }
                    }
                    if (isset($filters['visibility']) && !empty($filters['visibility'])) {
                        if ($filters['visibility'] != '1') {
                            $notifed_users[] = $contractors;
                        } else {
                            $contractors = array();
                        }
                    }
                } else if ($case == 'estimate email to client') {
                    $notification_text = $estimateNum . ' ' . $bookingAddress . ' ' . $booking_status['name'] . ' ' . $booking_date . ' has been emailed to client';
                    $iosNotification = 'Estimate ' . $estimate['estimate_num'] . ' ' . $pureBookingAddress . ' ' . $booking_status['name'] . ' ' . $pureBookingDate . ' has been emailed to client';
                } else if ($case == 'customer upload photos') {
                    $customer = $modelCustomer->getById($created_by);
                    $customer_name = 'Customer <b style="color:#69bdaa;">' . $customer['first_name'] . ' ' . $customer['last_name'] . '</b>';
                    $newFilter = array();
                    $image = $model_image->getAll($item_id, $item_type, 'ii.item_image_id DESC', $newFilter, null, $filters['counter']);
                    $notification_text = $estimateNum . ' ' . $bookingAddress . ' for ' . $booking_date . ' has ' . $filters['counter'] . ' new photos added By ' . $customer_name . '<br/>';
                    $iosNotification = 'Estimate ' . $estimate['estimate_num'] . ' ' . $pureBookingAddress . ' for ' . $pureBookingDate . ' has ' . $filters['counter'] . ' new photos added By ' . $customer['first_name'] . ' ' . $customer['last_name'] . '<br/>';

                    foreach ($image as $key => $value) {
                        $thumbnail[$key] = $value['thumbnail_path'];
                    }
                } else if ($case == 'booking has new photos') {
                    $filter = array();
                    $image = $model_image->getAll($item_id, $item_type, 'ii.item_image_id DESC', $filter, null, $filters['counter']);

                    $notification_text = $estimateNum . ' ' . $bookingAddress . ' for ' . $booking_date . ' has ' . $filters['counter'] . ' new photos added <br/>';
                    $iosNotification = 'Estimate ' . $estimate['estimate_num'] . ' ' . $pureBookingAddress . ' for ' . $pureBookingDate . ' has ' . $filters['counter'] . ' new photos added <br/>';
                    foreach ($image as $key => $value) {
                        $thumbnail[$key] = $value['thumbnail_path'];
                    }
                    $RoleCredentials = $model_authRoleCredential->getRolesByCredentialName($credential_name);
                    foreach ($RoleCredentials as $key => $value) {
                        $users = $model_user->getActiveUserByRoleId($value['role_id']);
                        $notifed_users[] = $users;
                    }
                }
                if (!empty($contractors) && empty($notifed_users)) {
                    $notifed_users[] = $contractors;
                }
//                var_dump($notification_text);
//                exit;
            } else if ($filters['item_type'] == 'invoice') {

                $invoice = $model_bookingInvoice->getById($item_id);
                $invoiceNum = 'Invoice <b style="color:#69bdaa;">' . $invoice['invoice_num'] . '</b>';

                $booking = $model_booking->getById($invoice['booking_id']);

                $contractors = $model_contractorServiceBooking->getContractorIdsByBookingId($booking['booking_id']);
                $booking_address = $model_bookingAddress->getByBookingId($booking['booking_id']);
                //get start date and convert it on specifice formate
                $timeStamp = strtotime($booking['booking_start']);
                $day = date("l", $timeStamp);
                $time = date("H:ia", $timeStamp);
                $month_day = date("M d,", $timeStamp);
                $booking_date = '<span style="color:#69bdaa;">' . $day . ', ' . $month_day . ' ' . $time . '</span>';
                $pureBookingDate = $day . ', ' . $month_day . ' ' . $time;
                //

                $booking_status = $model_bookingStatus->getById($booking['status_id']);
                // get booking address for text of notifications
                $bookingAddress = '<i class="fa fa-map-marker"></i> <b><i>' . $booking_address['street_address'] . ' ' . $booking_address['suburb'] . ' ' . $booking_address['postcode'] . '</i></b>';
                $pureBookingAddress = $booking_address['street_address'] . ' ' . $booking_address['suburb'] . ' ' . $booking_address['postcode'];
                //
                $bookingNum = 'Booking <b style="color:#69bdaa;">' . $booking['booking_num'] . '</b>';

                if ($case == 'invoice discussion for notification') {
                    $notification_type = 'discussion';
                    $modelInvoiceDiscussion = new Model_InvoiceDiscussion();
                    $invoiceDiscussion = $modelInvoiceDiscussion->getById($filters['discussion_id']);

                    $notification_text = '<b>' . $created_by_name . '</b> has posted on the discussion board from ' . $invoiceNum . ' ' . $bookingAddress . ' from ' . $booking_date . ' " ' . $invoiceDiscussion['user_message'] . ' "';
                    $iosNotification = $created_by_name . ' has posted on the discussion board for Invoice ' . $invoice['invoice_num'] . ' ' . $pureBookingAddress . ' from ' . $pureBookingDate . ' " ' . $invoiceDiscussion['user_message'] . ' "';

                    if (isset($filters['notify_multiple']) && !empty($filters['notify_multiple'])) {
                        foreach ($filters['notify_multiple'] as $key => $user) {
                            $other_users = $model_user->getById((int) $user['user_id']);
                            $notifed_users[$key][] = $other_users;
                        }
                    }
                    if (isset($filters['visibility']) && !empty($filters['visibility'])) {
                        if ($filters['visibility'] != '1') {
                            $notifed_users[] = $contractors;
                        } else {
                            $contractors = array();
                        }
                    }
                } else if ($case == 'invoice email to client') {
                    $invoice = $model_bookingInvoice->getByBookingId($booking['booking_id']);
                    $notification_text = $invoiceNum . ' ' . $bookingAddress . ' ' . $booking_status['name'] . ' ' . $booking_date . ' has been emailed to client';
                    $iosNotification = 'Invoice ' . $invoice['invoice_num'] . ' ' . $pureBookingAddress . ' ' . $booking_status['name'] . ' ' . $pureBookingDate . ' has been emailed to client';
                }


                if (!empty($contractors) && empty($notifed_users)) {
                    $notifed_users[] = $contractors;
                }

                //here all cases that related of invoice
            } else if ($filters['item_type'] == 'inquiry') {
                $inquiry = $model_inquiry->getById($item_id);
                $inquiryAddress = $model_inquiryAddress->getByInquiryId($item_id);

                $inquiryNum = 'Inquiry <b style="color:#69bdaa;">' . $inquiry['inquiry_num'] . '</b>';
                $inquiry_Address = '<i class="fa fa-map-marker"></i> <b><i>' . $inquiryAddress['street_address'] . ' ' . $inquiryAddress['suburb'] . ' ' . $inquiryAddress['postcode'] . '</i></b>';
                $pureInquiryAddress = $inquiryAddress['street_address'] . ' ' . $inquiryAddress['suburb'] . ' ' . $inquiryAddress['postcode'];
                $created_date = date("M d,", $inquiry['created']);
                $created_time = date("H:ia", $inquiry['created']);
                $inquiry_date = '<span style="color:#69bdaa;">' . date('l, M d, H:ia', $inquiry['created']) . '</span>';
                $pureInquiryDate = date('l, M d, H:ia', $inquiry['created']);
                //here all cases that related of inquiry
                if ($case == 'inquiry discussion for notification') {
                    $credential_name = 'inquiryDiscussionForNotification';
                    $notification_type = 'discussion';
                    $inquiryDiscussion = $model_inquiryDiscussion->getById($filters['discussion_id']);
                    $notification_text = '<b>' . $created_by_name . '</b> has posted on the discussion board for ' . $inquiryNum . ' ' . $inquiry_Address . ' for ' . $inquiry_date . ' " ' . $inquiryDiscussion['user_message'] . ' "';
                    $iosNotification = $created_by_name . ' has posted on the discussion board for Inquiry ' . $inquiry['inquiry_num'] . ' ' . $pureInquiryAddress . ' for ' . $pureInquiryDate . ' " ' . $inquiryDiscussion['user_message'] . ' "';

                    if (isset($filters['notify_multiple']) && !empty($filters['notify_multiple'])) {
                        foreach ($filters['notify_multiple'] as $key => $user) {
                            $other_users = $model_user->getById((int) $user['user_id']);
                            $notifed_users[$key][] = $other_users;
                        }
                    }
                } else if ($case == 'customer upload photos') {
                    $credential_name = 'customerUploadPhotos';
                    $customer = $modelCustomer->getById($created_by);
                    $customer_name = 'Customer <b style="color:#69bdaa;">' . $customer['first_name'] . ' ' . $customer['last_name'] . '</b>';
                    $newFilter = array();
                    $image = $model_image->getAll($item_id, $item_type, 'ii.item_image_id DESC', $newFilter, null, $filters['counter']);
                    $notification_text = $inquiryNum . ' ' . $inquiry_Address . ' for ' . $inquiry_date . ' has ' . $filters['counter'] . ' new photos added By ' . $customer_name . '<br/>';
                    $iosNotification = $inquiry['inquiry_num'] . ' ' . $pureInquiryAddress . ' for ' . $pureInquiryDate . ' has ' . $filters['counter'] . ' new photos added By ' . $customer['first_name'] . ' ' . $customer['last_name'] . '<br/>';
                    foreach ($image as $key => $value) {
                        $thumbnail[$key] = $value['thumbnail_path'];
                    }
                }

                if (empty($notifed_users) && !isset($filters['notify_multiple']) && empty($filters['notify_multiple'])) {
                    $RoleCredentials = $model_authRoleCredential->getRolesByCredentialName($credential_name);
                    foreach ($RoleCredentials as $key => $value) {
                        if ($value['role_id'] != $contractorRoleId) {
                            $users = $model_user->getActiveUserByRoleId($value['role_id']);
                            $notifed_users[] = $users;
                        }
                    }
                }
//                var_dump($notifed_users);
//                exit;
            } else if ($filters['item_type'] == 'contractor') {

                //here all cases that related of contactor
                if ($case == 'payment made to contractor') {
                    $user = $model_user->getById($item_id);
                    $bookingContractorPayment = $model_bookingContractorPayment->getBybookingIdAndContractorId($filters['booking_id'], $item_id);
                    $notification_text = '<b>' . $bookingContractorPayment['contractor_invoice_num'] . '</b> Invoice has been paid . The Amount of $' . $bookingContractorPayment['amount_paid'] . ' was transferred to your bank account ';
                    $iosNotification = $bookingContractorPayment['contractor_invoice_num'] . ' Invoice has been paid . The Amount of $' . $bookingContractorPayment['amount_paid'] . ' was transferred to your bank account ';
                    $notifed_users[][] = $user;
                } else if ($case == 'new contractor discussion') {
                    $notification_type = 'discussion';
                    $modelContractorDiscussionMongo = new Model_ContractorDiscussionMongo();
                    $contractorDiscussion = $modelContractorDiscussionMongo->getById($filters['discussion_id']);
                    foreach ($contractorDiscussion as $key => $value) {
                        //$posted_user = $model_user->getById($value['user_id']);
                        $contractors = $model_user->getById($value['contractor_id']);
                        $notifed_users[$key][] = $contractors;
                        $notification_text = '<b>' . $created_by_name . '</b> has posted on ' . $value['contractor_name'] . ' discussion board: " ' . $value['user_message'] . ' ."' . date('l,M d H:ia', $value['created']);
                        $iosNotification = $created_by_name . ' has posted on ' . $value['contractor_name'] . ' discussion board: " ' . $value['user_message'] . ' ."' . date('l,M d H:ia', $value['created']);
                    }

                    if (isset($filters['notify_multiple']) && !empty($filters['notify_multiple'])) {
                        foreach ($filters['notify_multiple'] as $key => $user) {
                            $other_users = $model_user->getById((int) $user['user_id']);
                            $notifed_users[$key][] = $other_users;
                        }
                    }
                    if (isset($filters['visibility']) && !empty($filters['visibility'])) {
                        if ($filters['visibility'] != '1') {
                            $notifed_users[] = $contractors;
                        }
                    }
                }


//                exit;
            } else if ($filters['item_type'] == 'complaint') {
                //here all cases that related of complaint
//                $contractors = $model_contractorServiceBooking->getContractorIdsByBookingId($booking['booking_id']);

                $complaint = $model_complaint->getById($item_id);
                $booking_id = $complaint['booking_id'];
                $booking = $model_booking->getById($booking_id);
                $booking_address = $model_bookingAddress->getByBookingId($booking['booking_id']);
                //get start date and convert it on specifice formate
                $timeStamp = strtotime($booking['booking_start']);
                $day = date("l", $timeStamp);
                $time = date("H:ia", $timeStamp);
                $month_day = date("M d,", $timeStamp);
                $booking_date = '<span style="color:#69bdaa;">' . $day . ', ' . $month_day . ' ' . $time . '</span>';
                $pureBookingDate = $day . ', ' . $month_day . ' ' . $time;
                //

                $booking_status = $model_bookingStatus->getById($booking['status_id']);
                // get booking address for text of notifications
                $bookingAddress = '<i class="fa fa-map-marker"></i> <b><i>' . $booking_address['street_address'] . ' ' . $booking_address['suburb'] . ' ' . $booking_address['postcode'] . '</i></b>';
                $pureBookingAddress = $booking_address['street_address'] . ' ' . $booking_address['suburb'] . ' ' . $booking_address['postcode'];
                //
                $bookingNum = 'Booking <b style="color:#69bdaa;">' . $booking['booking_num'] . '</b>';
                $created_date = date("M d,", $complaint['created']);
                $created_time = date("H:ia", $complaint['created']);
                $comp_date_time = date("l", $complaint['created']) . ' ' . date("H:i a", $complaint['created']) . ' ' . date("M d", $complaint['created']);
                $contractors = $model_contractorServiceBooking->getContractorIdsByBookingId($complaint['booking_id']);
                if ($case == 'complaint discussion for notification') {
                    $notification_type = 'discussion';

                    $model_complaintDiscussion = new Model_ComplaintDiscussion();
                    $complaintDiscussion = $model_complaintDiscussion->getById($filters['discussion_id']);
                    $complaintNum = 'Complaint <b style="color:#69bdaa;">' . $complaint['complaint_num'] . '</b>';
                    $notification_text = '<b>' . $created_by_name . '</b> has posted on the discussion board for  ' . $complaintNum . ' from ' . date("l, M d, H:ia", $complaint['created']) . ' " ' . $complaintDiscussion['user_message'] . ' "';
                    $iosNotification = $created_by_name . ' has posted on the discussion board for Complaint ' . $complaint['complaint_num'] . ' from ' . date("l, M d, H:ia", $complaint['created']) . ' " ' . $complaintDiscussion['user_message'] . ' "';
                    if (isset($filters['notify_multiple']) && !empty($filters['notify_multiple'])) {
                        foreach ($filters['notify_multiple'] as $key => $user) {
                            $other_users = $model_user->getById((int) $user['user_id']);
                            $notifed_users[$key][] = $other_users;
                        }
                    }
                    if (isset($filters['visibility']) && !empty($filters['visibility'])) {
                        if ($filters['visibility'] != '1') {
                            $notifed_users[] = $contractors;
                        } else {
                            $contractors = array();
                        }
                    }
                } else if ($case == 'new complaint') {
                    $notification_text = $bookingNum . ' ' . $bookingAddress . ' ' . $booking_status['name'] . ' on ' . $day . ', ' . $month_day . ' has a complaint placed by ' . $created_by_name . ' on ' . $comp_date_time . ' " ' . $complaint['comment'] . ' " ';
                    $iosNotification = $booking['booking_num'] . ' ' . $pureBookingAddress . ' ' . $booking_status['name'] . ' on ' . $day . ', ' . $month_day . ' has a complaint placed by ' . $created_by_name . ' on ' . $comp_date_time . ' " ' . $complaint['comment'] . ' " ';
                }

                if (!empty($contractors) && empty($notifed_users)) {
                    $notifed_users[] = $contractors;
                }
            } else if ($filters['item_type'] == 'customer') {
                //here all cases that related of customer
            } else if ($filters['item_type'] == 'others') {
                if ($case == 'new sms') {
                    $notification_type = 'sms';
                    $credential_name = 'newSms';
                    $sendSmsNotificationToSystem = 1;
                    $smsInfo = $modelSmsHistory->getById($item_id);
                    $mobileFormat = $modelSmsHistory->getMobileFormat($smsInfo['from']);
                    $userInfo = $model_user->getByMobile($mobileFormat);
                    //$id=$smsInfo['id'];
                    if ($userInfo) {
                        $userId = $userInfo['user_id'];
                        $userName = $userInfo['username'];
                    } else {
                        //user_name  user_id  notification_text  date_sent  read  sms_id  seen title
                        $customerInfo = $modelCustomer->getByMobile($mobileFormat);
                        $userId = $customerInfo['customer_id'];
                        $userName = $customerInfo['first_name'];
                    }

                    /*                     * ******************* */
                    $reason_id = "";
                    $reason_id = "";
                    if (isset($filters['for_booking'])) {
                        $contractors = $model_contractorServiceBooking->getContractorIdsByBookingId($smsInfo['reason_id']);
                        if (!empty($contractors)) {
                            $notifed_users[] = $contractors;
                        }
                    } else {
                        $RoleCredentials = $model_authRoleCredential->getRolesByCredentialName($credential_name);
                        foreach ($RoleCredentials as $key => $value) {
                            if ($value['role_id'] != $contractorRoleId) {
                                $users = $model_user->getActiveUserByRoleId($value['role_id']);
                                $notifed_users[] = $users;
                            }
                        }
                    }

                    $notification_text = 'You have received new message from : <b>' . $userName . '</b>, message :<i>' . $smsInfo['message'] . '</i>';
                    $iosNotification = 'You have received new message from : ' . $userName . ', message :' . $smsInfo['message'];



//                    exit;
                } else if ($case == 'subscriber phone request') {
                    $notification_type = 'notification';
                    $twilioInfoNumber = $twilioaccountInfoObj->getById($item_id);
                    $router = Zend_Controller_Front::getInstance()->getRouter();
                    $purchase = $router->assemble(array(), 'account') . '?twilioAdd=1&companyID=' . $filters['company_id'] . '&fromNotification=1';
                    $notification_text = '<b>' . $created_by_name . '</b> the owner of <b>' . $filters['company_name'] . '</b> company has Requested Twilio Service where capability is <b>' . $twilioInfoNumber['capability'] . '</b> and type is <b>' . $twilioInfoNumber['type'] . '</b> and Country Code is <b>' . $twilioInfoNumber['CountryCode'] . '<br>' . '<strong><a class="btn btn-primary btn-xs" onclick="window.location.reload();" href="' . $purchase . '">' . "Purchase" . '<strong></a></b>';
                    $iosNotification = '<b>' . $created_by_name . '</b> the owner of <b>' . $filters['company_name'] . '</b> company has Requested Twilio Service where capability is <b>' . $twilioInfoNumber['capability'] . '</b> and type is <b>' . $twilioInfoNumber['type'] . '</b> and Country Code is <b>' . $twilioInfoNumber['CountryCode'] . '<br>' . '<strong><a class="btn btn-primary btn-xs" onclick="window.location.reload();" href="' . $purchase . '">' . "Purchase" . '<strong></a></b>';
                }
            }
        }

        $company_id = CheckAuth::getCompanySession();
        $case_credential = $model_credential->getByCredentialName(getCredentialFormat($case));

        foreach ($notifed_users as $key => $notify_user_by_contractor) {
            foreach ($notify_user_by_contractor as $key => $notify_user) {
                $notified_user_id = isset($notify_user['contractor_id']) ? $notify_user['contractor_id'] : $notify_user['user_id'];
                $model_notificationSetting = new Model_NotificationSetting();
                $show = 0;
                $notification_settings = $model_notificationSetting->getByUserIdAndCredentialId($notified_user_id, $case_credential['credential_id']);
                if ((int) $notify_user['role_id'] == $contractorRoleId) {
                    $show = 1;
                } else if (!empty($notification_settings)) {
                    $show = 1;
                }
                $doc = array(
                    'case' => $case,
                    'item_type' => $item_type,
                    'item_id' => (int) $item_id,
                    'created_by' => (int) $created_by,
                    'created_by_role_id' => (int) $created_by_role_id,
                    'created_by_name' => $created_by_name,
                    'date_sent' => $date_sent,
                    'notify_user_id' => (int) $notified_user_id,
                    'notify_user_name' => (!empty($notify_user['display_name']) ? $notify_user['display_name'] : $notify_user['username']),
                    'notify_role_id' => (int) $notify_user['role_id'],
                    'read' => 0,
                    'seen' => 0,
                    'notification_text' => $notification_text,
                    'Ios_text' => $iosNotification,
                    'notification_type' => $notification_type,
                    'thumbnails' => $thumbnail,
                    'credential_id' => (int) $case_credential['credential_id'],
                    'show' => (int) $show,
                    'company_id' => (int) $company_id
                );

                $model_MongoTest->insertNotification($doc);
                //SEND PUSH NOTIFICATION
                $modelAuthRoleCredential = new Model_AuthRoleCredential();
                $model_ContractorMobileInfo = new Model_ContractorMobileInfo();

//                if ((int) $notify_user['role_id'] == $contractorRoleId) {
//                    $credential = $modelAuthRoleCredential->getByRoleId((int) $notify_user['role_id']);
//                    if (!empty($credential)) {
//                        if (!in_array($case, array('to approve edit', 'request access'))) {
//                            $contractorMobiles = $model_ContractorMobileInfo->getByContractorIdAndOs($notified_user_id, 'android');
//                            $devicetoken = array();
//                            if (!empty($contractorMobiles)) {
//                                foreach ($contractorMobiles as $contractorMobile) {
//                                    $devicetoken[] = $contractorMobile['devicetoken'];
//                                }
//                            }
//                            if (!empty($devicetoken)) {
//                                $extra = array(
//                                    'booking_id' => $item_id,
//                                    'type' => $case
//                                );
//                                // $test = new Model_GCMPushMessage('AIzaSyDlZZ6U8GlPfVpXDGeam3mNYMk1hd-kfIk');
//                                //New API key from Hussam added by islam
//                                $test = new Model_GCMPushMessage('AIzaSyCOZw75Spbsi0flk0S7IoyM73Hcv8dd-4o');
//
//                                $test->setDevices($devicetoken);
//                                // $test->send($notification_text, $extra);
//                            }
//                            //FOR IOS
//                            $contractorMobiles = $model_ContractorMobileInfo->getByContractorIdAndOs($notified_user_id, 'ios');
//                            $devicetoken = array();
//                            if (!empty($contractorMobiles)) {
//                                $modelIOSPushMessage = new Model_IOSPushMessage('aps_dis.pem', '');
//                                foreach ($contractorMobiles as $contractorMobile) {
//
//                                    if (!empty($contractorMobile['devicetoken'])) {
//                                        $extra = array(
//                                            'booking_id' => $item_id,
//                                            'type' => $case
//                                        );
//                                        // $modelIOSPushMessage->send($iosNotification, $contractorMobile['devicetoken'], $extra);
//                                    }
//                                }
//                                $modelIOSPushMessage->close();
//                            }
//                        }
//                    }
//                }
            }
        }
        //  $model_MongoTest->closeConnection();
        return;
    }

}
