
<html>
<?php

date_default_timezone_set('Australia/Sydney');
session_start();
//unset($_SESSION['new_booking']);
if( isset($_SESSION['new_booking']['current_tab']) ) {
  $current_tab = $_SESSION['new_booking']['current_tab'];
  $prev_tab = $_SESSION['new_booking']['prev_tab'];
 }else{
  $_SESSION['new_booking']['current_tab'] = 'select-postCode';
  $_SESSION['new_booking']['prev_tab'] = '';
  $current_tab = $_SESSION['new_booking']['current_tab'];
  $prev_tab = $_SESSION['new_booking']['prev_tab'];
 }
 
clearBookingSession();
?>

<head>
        <title>Cleaning Service</title>
        <meta charset="utf-8">
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
		<link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/new-booking.css" rel="stylesheet">
		<link href="css/calendar.css" type="text/css" rel="stylesheet" />
        <link href="css/datepicker3.css" type="text/css" rel="stylesheet" />
        <link href="css/dropzone.css" type="text/css" rel="stylesheet" />
		<link href="css/summernote.css" type="text/css" rel="stylesheet" />
		<link href="css/formValidation.min.css" type="text/css" rel="stylesheet" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    </head>
    <body>
        <div class="loader" style="height:100%">
            <img src="pic/loading-4.gif">
        </div>
        <div class="header">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 logo_with_menu" >
                        <div class="menu" style="display:none">
                            <ul>
                                <li><a href="#">HELP</a></li>
                                <li><a href="#">LOGIN</a></li>
                            </ul>
                        </div>
                        <div class="logo "><img src="pic/tile-cleaners.gif"/></div>
                    </div>
                    <div class="col-md-8 header_steps">
                        <div class="all-services-menu main_circle_box <?= ($current_tab == 'all-services' || $current_tab == 'select-postCode') ? 'active': ''; ?> " id="all-services-menu">
                            <a href="#" class="pull-left added">
                                <div class="circle">
                                    <i class="fa fa-pencil icons fa-2x" aria-hidden="true"></i>
                                </div>
                                <span>Service</span>
                            </a>
                            <ul class="dotted_circles">
                                <li></li> 
                                <li></li> 
                                <li></li> 
                            </ul>
                        </div>

                        <div class="Quote-details-menu main_circle_box" id="Quote-details-menu">
                            <a href="#" class="pull-left">
                                <div class="circle">
                                    <i class="fa fa-check icons fa-2x" aria-hidden="true"></i>
                                </div>
                                <span>Quote</span>
                            </a>
                            <ul class="dotted_circles">
                                <li></li> 
                                <li></li> 
                                <li></li> 
                            </ul>
                        </div>
                        <div class="availability-menu main_circle_box vailablity_circle" id="availability-menu">
                            <a href="#" class="pull-left">
                                <div class="circle">
                                    <i class="fa fa-check icons fa-2x" aria-hidden="true"></i>
                                </div>
                                <span>Availability</span>
                            </a>
                            <ul class="dotted_circles">
                                <li></li> 
                                <li></li> 
                                <li></li> 
                            </ul>
                        </div>
                        <div class="Contact-Details-menu main_circle_box " id="Contact-Details-menu">
                            <a href="#" class="pull-left">
                                <div class="circle">
                                    <i class="fa fa-check icons fa-2x" aria-hidden="true"></i>
                                </div>
                                <span>Details</span>
                            </a>
                            <ul class="dotted_circles">
                                <li></li> 
                                <li></li> 
                                <li></li> 
                            </ul>
                        </div>
                        <div class="confirm-booking-menu main_circle_box " id="confirm-booking-menu">
                            <a href="#" class="pull-left">
                                <div class="circle">
                                    <i class="fa fa-check icons fa-2x" aria-hidden="true"></i>
                                </div>
                                <span>Confirm</span>
                            </a>

                        </div>
                    </div>
                </div>

            </div>
        </div>





        <div class="content">
		

            <div class="container">
                <form type="POST"  id="new-booking" role="form" onsubmit="ajax_submit('webservice/add-booking');"
                      data-eway-encrypt-key="jj0v08RhXr5swodwNEHftys/7P7R2+LMBXU8iRCZ9voNuHVSuXrvJ6KmQ5c05lnzF563khRotBGYw8KmZzQuMlD6wf3d48wYq3hNWqd8ambURC5dWP1lDN9XkhPo2aH9EZpFmVU5/yvkt+Tvd0SF2BAorTmwFfq1JDbNspYQ05NLb4ct/jcEk78LTYaPRA/9dXtbzgYm/pechVNLIsbZmBIQSYtBIYaAtRxJIZtSduVkwE1Kb/I5EihI/TXIOLKJCwSHcb+Ma0dxDL3rF+dTyDvF7afVAX5G7O7MpQaA/F0D4rslX9b8Qawn4iCHGCgD1yXjB4lioplhxkOeAHD3Jw=="
                      >
					<input type="hidden" name="total_services" id="total_services" value="<?= isset($_SESSION['new_booking']['total_services'])? $_SESSION['new_booking']['total_services'] : ''?>"/>
					<input type="hidden" name="total_price" id="total_price" value="<?= isset($_SESSION['new_booking']['total_price'])? $_SESSION['new_booking']['total_price'] : ''?>"/>
					
                    <div id="select-postCode" style="display:<?= ($current_tab == 'select-postCode') ? 'block': 'none'; ?>">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box">
                                    <div class="confirm-box box-title main-box">
                                        <div class="row">
                                            <div class="col-md-5 col-sm-5">
                                                <div class="thank-you"><img src="pic/tile-cleaner.png"/></div>
                                            </div>

                                            <div class="col-md-7 left-col col-sm-7">				 				  
                                                <p class="welcome-text">
                                                    Hey! Welcome to tile cleaners!<br/>
                                                    To get our available services <br/>
                                                    Please enter your postcode <br/>
                                                </p>
                                                <div class="form-group col-md-9 margin-input">
                                                    <input type="text" value="<?= isset($_SESSION['new_booking']['customer-postcode'])? $_SESSION['new_booking']['customer-postcode'] : ''?>" placeholder = "Postcode" class="form-control" style="border-radius:55px" id="customer-postcode" name="customer-postcode"/>
                                                </div>

                                                <div class="login">
                                                    <a class="btn btn-info" href="javascript:;" id="get-services">Get Services</a>
                                                </div>


                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="all-services" style="visibility:<?= ($current_tab == 'all-services') ? 'visible': 'hidden'; ?>">
                        <div class="row">
                            <div class="col-md-12">

                                <div class="box main-box">
                                    <div class="box-title">
                                        <h3>
                                            Choose which <b>Services you require</b></h3>
                                    </div>
                                    <div class="box-content">
									  <div class="service-title"></div>
                                        <div class="all-services">
										   <div class="services_list" >
										    
											
											
										   </div>
                                            
                                        </div>
										<div class="services-Details" style="display:none;position:relative"></div>

                                    </div>
                                    <div class="box-footer">
                                        <div onclick="goTo('all-services', 'Quote-details')">
                                            <h3>To continue please click the arrow</h3>
                                            <a href="javascript:;" class="next-step" ><i class="fa fa-chevron-right  fa-4x" aria-hidden="true"></i></a>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div id="Contact-Details" style="visibility:hidden">
                        <div class="row">
                            <div class="col-md-12">

                                <div class="box main-box">
                                    <div class="box-title">
                                        <div class="row"><h3>
                                                <div class="col-md-7 col-sm-7">Complete <b>Contact Details</b></div>
                                                <div class="col-md-5 additional-icon col-sm-5"><span onclick="addSecondaryContact();">ADD ADDITIONAL CONTACT &nbsp; <a id="add-contact" class="plus" href="javascript:;" style="text-decoration: none;">
                                                            <i class="fa fa-plus"></i> </a></span></div></h3>
                                        </div>	 			 
                                    </div>


                                    <div class="box-content">
                                        <div class="row primary-contact">
                                            <div class="col-md-7">
                                                <h4> Customer Type : </h4>
                                                 <div class="row">
												   <div class="col-md-12 customer_types">
												    <div class="input-group">
													  <span class="input-group-addon" id="Residential">Residential</span>
													  <span class="input-group-addon" id="Commercial">Commercial</span>
													  <input type="hidden" value="<?= isset($_SESSION['new_booking']['customer_type_name'])? $_SESSION['new_booking']['customer_type_name'] : ''?>" name="customer_type_name" id="customer_type_name"/>
													</div>
													<div class="customer_types_list"></div>
													
												   </div>
												 </div>
                                            </div>
											<div class="customer_business_name">
											 <div class="col-md-7">
                                                <h4> Business Name : </h4>
                                                 <div class="row">
												   <div class="col-md-12">
												    <input type="text" name="customer_business_name" id="customer_business_name" value="<?= isset($_SESSION['new_booking']['customer_business_name'])? $_SESSION['new_booking']['customer_business_name'] : ''?>" class="form-control required" placeholder="Business Name">
												   </div>
												 </div>
                                            </div>
											</div>
                                        </div>
                                        <div class="primary-contact contact-sub-container">
                                            <h4>Primary Contact : </h4>
                                            <div class="row">
                                                <div class="col-md-7">
                                                    <div class="row">				 
                                                        <div class="col-md-6 form-group"><input type="text" name="first_name" id="first_name" value="<?= isset($_SESSION['new_booking']['first_name'])? $_SESSION['new_booking']['first_name'] : ''?>" class="form-control required" placeholder="First Name"></div>				 
                                                        <div class="col-md-6 form-group"><input type="text" name="last_name" id="last_name" value="<?= isset($_SESSION['new_booking']['last_name'])? $_SESSION['new_booking']['last_name'] : ''?>" class="form-control" placeholder="Last Name"></div>				 
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-7">
                                                    <div class="row">				 
                                                        <div class="col-md-6 form-group">
                                                            <input type="text" name="mobile1" id="mobile1" value="<?= isset($_SESSION['new_booking']['mobile1'])? $_SESSION['new_booking']['mobile1'] : ''?>" class="form-control required matched_mobile_formate" placeholder="Mobile" maxlength="10"></div>				 
                                                        <div class="col-md-6 form-group">
                                                            <input type="text" name="phone1" id="phone1" value="<?= isset($_SESSION['new_booking']['phone1'])? $_SESSION['new_booking']['phone1'] : ''?>" class="form-control" placeholder="Home Number"></div>			 
                                                    </div>
                                                </div>
                                                <div class="col-md-5 additional-icon" onclick="$('#contact-number_2').show();">
                                                    Add ADDITIONAL NUMBER
                                                    <a class=" plus" href="javascript:;" style="text-decoration: none;">
                                                        <i class="fa fa-plus"></i> </a>	
                                                </div>
                                            </div>

                                            <div class="row" id="contact-number_2" style="display:none">
                                                <div class="col-md-7">
                                                    <div class="row">				 
                                                        <div class="col-md-6 form-group">
                                                            <input type="text" name="mobile2" id="mobile2" value="<?= isset($_SESSION['new_booking']['mobile2'])? $_SESSION['new_booking']['mobile2'] : ''?>" class="form-control matched_mobile_formate" placeholder="Mobile" maxlength="10"></div>				 
                                                        <div class="col-md-6 form-group">
                                                            <input type="text" name="phone2" id="phone2" value="<?= isset($_SESSION['new_booking']['phone2'])? $_SESSION['new_booking']['phone2'] : ''?>" class="form-control" placeholder="Home Number"></div>				 
                                                    </div>
                                                </div>
                                                <div class="col-md-5" style="margin-bottom:15px;">
                                                    <a class="plus" href="javascript:;" style="text-decoration: none;" onclick="$('#contact-number_3').show();">
                                                        <i class="fa fa-plus"></i> </a>
                                                    <a class=" plus" href="javascript:;" style="text-decoration: none;" onclick="$('#contact-number_2').hide();$('#mobile2').val('');$('#phone2').val('');"><i class="fa fa-minus" aria-hidden="true"></i> </a>
                                                </div>
                                            </div>


                                            <div class="row" id="contact-number_3" style="display:none">
                                                <div class="col-md-7">
                                                    <div class="row">				 
                                                        <div class="col-md-6 form-group">
                                                            <input type="text" name="mobile3" id="mobile3" value="<?= isset($_SESSION['new_booking']['mobile3'])? $_SESSION['new_booking']['mobile3'] : ''?>" class="form-control matched_mobile_formate" placeholder="Mobile" maxlength="10"></div>				 
                                                        <div class="col-md-6 form-group">
                                                            <input type="text" name="phone3" id="phone3" value="<?= isset($_SESSION['new_booking']['phone3'])? $_SESSION['new_booking']['phone3'] : ''?>" class="form-control" placeholder="Home Number"></div> 
                                                    </div>
                                                </div>
                                                <div class="col-md-5" style="margin-bottom:15px;">
                                                    <a class="plus" href="javascript:;" style="text-decoration: none;" onclick="$('#contact-number_3').hide();$('#mobile3').val('');$('#phone3').val('');">
                                                        <i class="fa fa-minus"></i> </a>
                                                </div>
                                            </div>


                                            <div class="row">
                                                <div class="col-md-7">
                                                    <div class="form-group">
                                                        <input type="text" name="email1" id="email1" value="<?= isset($_SESSION['new_booking']['email1'])? $_SESSION['new_booking']['email1'] : ''?>" class="form-control required" placeholder="email">			   				  
                                                    </div>
                                                </div>
                                                <div class="col-md-5 additional-icon" onclick="$('#email_2').show();">
                                                    ADD ADDITIONAL EMAIL
                                                    <a class="plus" href="javascript:;" style="text-decoration: none;" ><i class="fa fa-plus"></i> </a>
                                                </div>
                                            </div>

                                            <div class="row" id="email_2" style="display:none">
                                                <div class="col-md-7">
                                                    <div class="form-group">
                                                        <input type="text" name="email2" id="email2" value="<?= isset($_SESSION['new_booking']['email2'])? $_SESSION['new_booking']['email2'] : ''?>" class="form-control" placeholder="email">			  				  
                                                    </div>
                                                </div>
                                                <div class="col-md-5" style="margin-bottom:15px;">
                                                    <a class="plus" href="javascript:;" style="text-decoration: none;"  onclick="$('#email_3').show();"><i class="fa fa-plus"></i></a>
                                                    <a class="plus" href="javascript:;" style="text-decoration: none;" onclick="$('#email_2').hide();$('#email2').val('');"><i class="fa fa-minus" aria-hidden="true"></i> </a>
                                                </div>
                                            </div>

                                            <div class="row" id="email_3" style="display:none">
                                                <div class="col-md-7">
                                                    <div class="form-group" >
                                                        <input type="text" name="email3" id="email3" value="<?= isset($_SESSION['new_booking']['email3'])? $_SESSION['new_booking']['email3'] : ''?>" class="form-control" placeholder="email">			  				  
                                                    </div>
                                                </div>
                                                <div class="col-md-5" style="margin-bottom:15px;">
                                                    <a class="plus" href="javascript:;" style="text-decoration: none;" onclick="$('#email_3').hide();$('#email3').val('');"><i class="fa fa-minus" aria-hidden="true"></i> </a>
                                                </div>
                                            </div>


                                        </div>

                                        <div class="secondary-contact contact-sub-container" style="display:none" id="secondary-contact-box1">
                                            <a class="delete-sub-container" href="javascript:;" onclick="ClearSecondaryContact('#secondary-contact-box1')">
                                                <i class="fa fa-times"></i> </a>
                                            <h4>Secondary Contact : </h4>

                                            <div class="row">
                                                <div class="col-md-7">
                                                    <div class="row">				 
                                                        <div class="col-md-6 form-group">
                                                            <input type="text" name="first_name_0" id="first_name_0" value="<?= isset($_SESSION['new_booking']['first_name_0'])? $_SESSION['new_booking']['first_name_0'] : ''?>" class="form-control" placeholder="First Name"></div>				 
                                                        <div class="col-md-6 form-group">
                                                            <input type="text" name="last_name_0" id="last_name_0" value="<?= isset($_SESSION['new_booking']['last_name_0'])? $_SESSION['new_booking']['last_name_0'] : ''?>" class="form-control" placeholder="Last Name"></div>				 
                                                    </div>
                                                </div>
                                            </div>				 

                                            <div class="row">
                                                <div class="col-md-7">
                                                    <div class="row">				 
                                                        <div class="col-md-6 form-group">
                                                            <input type="text" name="mobile1_0" id="mobile1_0" value="<?= isset($_SESSION['new_booking']['mobile1_0'])? $_SESSION['new_booking']['mobile1_0'] : ''?>" class="form-control matched_mobile_formate" placeholder="Mobile" maxlength="10"></div>				 
                                                        <div class="col-md-6 form-group">
                                                            <input type="text" name="phone1_0" id="phone1_0" value="<?= isset($_SESSION['new_booking']['phone1_0'])? $_SESSION['new_booking']['phone1_0'] : ''?>" class="form-control" placeholder="Home Number"></div>			 
                                                    </div>

                                                </div>
                                                <div class="col-md-5 additional-icon" onclick="$('#contact-number1_2').show();">
                                                    Add ADDITIONAL NUMBER
                                                    <a class="plus" href="javascript:;" style="text-decoration: none;">
                                                        <i class="fa fa-plus"></i> </a>	
                                                </div>
                                            </div>

                                            <div class="row" id="contact-number1_2" style="display:none">
                                                <div class="col-md-7">
                                                    <div class="row">				 
                                                        <div class="col-md-6 form-group">
                                                            <input type="text" name="mobile2_0" id="mobile2_0" value="<?= isset($_SESSION['new_booking']['mobile2_0'])? $_SESSION['new_booking']['mobile2_0'] : ''?>" class="form-control matched_mobile_formate" placeholder="Mobile" maxlength="10"></div>				 
                                                        <div class="col-md-6 form-group">
                                                            <input type="text" name="phone2_0" id="phone2_0" value="<?= isset($_SESSION['new_booking']['phone2_0'])? $_SESSION['new_booking']['phone2_0'] : ''?>" class="form-control" placeholder="Home Number"></div>				 
                                                    </div>
                                                </div>
                                                <div class="col-md-5" style="margin-bottom:15px;">
                                                    <a class="plus" href="javascript:;" style="text-decoration: none;" onclick="$('#contact-number1_3').show();">
                                                        <i class="fa fa-plus"></i> </a>
                                                    <a class="plus" href="javascript:;" style="text-decoration: none;" onclick="$('#contact-number1_2').hide();$('#mobile2_0').val('');$('#phone2_0').val('');"><i class="fa fa-minus" aria-hidden="true"></i> </a>
                                                </div>
                                            </div>

                                            <div class="row" id="contact-number1_3" style="display:none">
                                                <div class="col-md-7">
                                                    <div class="row" >				 
                                                        <div class="col-md-6 form-group">
                                                            <input type="text" name="mobile3_0" id="mobile3_0" value="<?= isset($_SESSION['new_booking']['mobile3_0'])? $_SESSION['new_booking']['mobile3_0'] : ''?>" class="form-control matched_mobile_formate" placeholder="Mobile" maxlength="10"></div>				 
                                                        <div class="col-md-6 form-group">
                                                            <input type="text" name="phone3_0" id="phone3_0" value="<?= isset($_SESSION['new_booking']['phone3_0'])? $_SESSION['new_booking']['phone3_0'] : ''?>" class="form-control" placeholder="Home Number"></div>
                                                    </div>
                                                </div>
                                                <div class="col-md-5" style="margin-bottom:15px;">
                                                    <a class="plus" href="javascript:;" style="text-decoration: none;" onclick="$('#contact-number1_3').hide();$('#mobile3_0').val('');$('#phone3_0').val('');">
                                                        <i class="fa fa-minus"></i> </a>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-7">
                                                    <div class="form-group">
                                                        <input type="text" name="email1_0" id="email1_0" value="<?= isset($_SESSION['new_booking']['email1_0'])? $_SESSION['new_booking']['email1_0'] : ''?>" class="form-control" placeholder="email">			  			 
                                                    </div>
                                                </div>
                                                <div class="col-md-5 additional-icon">
                                                    ADD ADDITIONAL EMAIL
                                                    <a class="plus" href="javascript:;" style="text-decoration: none;" onclick="$('#email1_2').show();"><i class="fa fa-plus"></i> </a>
                                                </div>
                                            </div>


                                            <div class="row" id="email1_2" style="display:none">
                                                <div class="col-md-7">
                                                    <div class="form-group">				
                                                        <input type="text" name="email2_0" id="email2_0" value="<?= isset($_SESSION['new_booking']['email2_0'])? $_SESSION['new_booking']['email2_0'] : ''?>" class="form-control" placeholder="email">			  	 
                                                    </div>
                                                </div>
                                                <div class="col-md-5" style="margin-bottom:15px;">
                                                    <a class="plus" href="javascript:;" style="text-decoration: none;"  onclick="$('#email1_3').show();"><i class="fa fa-plus"></i> </a>
                                                    <a class="plus" href="javascript:;" style="text-decoration: none;" onclick="$('#email1_2').hide();$('#email2_0').val('');"><i class="fa fa-minus" aria-hidden="true"></i> </a>
                                                </div>
                                            </div>


                                            <div class="row" id="email1_3" style="display:none">
                                                <div class="col-md-7">
                                                    <div class="form-group">
                                                        <input type="text" name="email3_0" id="email3_0" value="<?= isset($_SESSION['new_booking']['email3_0'])? $_SESSION['new_booking']['email3_0'] : ''?>" class="form-control" placeholder="email">			  
                                                    </div>
                                                </div>
                                                <div class="col-md-5" style="margin-bottom:15px;">
                                                    <a class=" plus" href="javascript:;" style="text-decoration: none;" onclick="$('#email1_3').hide();$('#email3_0').val('');"><i class="fa fa-minus" aria-hidden="true"></i> </a>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="secondary-contact contact-sub-container" style="display:none" id="secondary-contact-box2">
                                            <a class="delete-sub-container" href="javascript:;" onclick="ClearSecondaryContact('#secondary-contact-box2')">
                                                <i class="fa fa-times"></i> </a>

                                            <h4>Secondary Contact : </h4>

                                            <div class="row">
                                                <div class="col-md-7">
                                                    <div class="row">				 
                                                        <div class="col-md-6 form-group">
                                                            <input type="text" name="first_name_1" id="first_name_1" value="<?= isset($_SESSION['new_booking']['first_name_1'])? $_SESSION['new_booking']['first_name_1'] : ''?>" class="form-control" placeholder="First Name"></div>				 
                                                        <div class="col-md-6 form-group">
                                                            <input type="text" name="last_name_1" id="last_name_1" value="<?= isset($_SESSION['new_booking']['last_name_1'])? $_SESSION['new_booking']['last_name_1'] : ''?>" class="form-control" placeholder="Last Name"></div>				 
                                                    </div>
                                                </div>
                                            </div>				 

                                            <div class="row">
                                                <div class="col-md-7">
                                                    <div class="row">				 
                                                        <div class="col-md-6 form-group">
                                                            <input type="text" name="mobile1_1" id="mobile1_1" value="<?= isset($_SESSION['new_booking']['mobile1_1'])? $_SESSION['new_booking']['mobile1_1'] : ''?>" class="form-control matched_mobile_formate" placeholder="Mobile" maxlength="10"></div>				 
                                                        <div class="col-md-6 form-group">
                                                            <input type="text" name="phone1_1" id="phone1_1" value="<?= isset($_SESSION['new_booking']['phone1_1'])? $_SESSION['new_booking']['phone1_1'] : ''?>" class="form-control" placeholder="Home Number"></div>			 
                                                    </div>

                                                </div>
                                                <div class="col-md-5 additional-icon" onclick="$('#contact-number2_2').show();">
                                                    Add ADDITIONAL NUMBER
                                                    <a class=" plus" href="javascript:;" style="text-decoration: none;" >
                                                        <i class="fa fa-plus"></i> </a>	
                                                </div>
                                            </div>

                                            <div class="row" id="contact-number2_2" style="display:none">
                                                <div class="col-md-7">
                                                    <div class="row">				 
                                                        <div class="col-md-6 form-group">
                                                            <input type="text" name="mobile2_1" id="mobile2_1" value="<?= isset($_SESSION['new_booking']['mobile2_1'])? $_SESSION['new_booking']['mobile2_1'] : ''?>" class="form-control matched_mobile_formate" placeholder="Mobile" maxlength="10"></div>				 
                                                        <div class="col-md-6 form-group">
                                                            <input type="text" name="phone2_1" id="phone2_1" value="<?= isset($_SESSION['new_booking']['phone2_1'])? $_SESSION['new_booking']['phone2_1'] : ''?>" class="form-control" placeholder="Home Number"></div>				 
                                                    </div>
                                                </div>
                                                <div class="col-md-5" style="margin-bottom:15px;">
                                                    <a class=" plus" href="javascript:;" style="text-decoration: none;" onclick="$('#contact-number2_3').show();">
                                                        <i class="fa fa-plus"></i> </a>
                                                    <a class=" plus" href="javascript:;" style="text-decoration: none;" onclick="$('#contact-number2_2').hide();$('#mobile2_2').val('');$('#phone2_2').val('');"><i class="fa fa-minus" aria-hidden="true"></i> </a>
                                                </div>
                                            </div>

                                            <div class="row" id="contact-number2_3" style="display:none">
                                                <div class="col-md-7">
                                                    <div class="row" >				 
                                                        <div class="col-md-6 form-group">
                                                            <input type="text" name="mobile3_1" id="mobile3_1" value="<?= isset($_SESSION['new_booking']['mobile3_1'])? $_SESSION['new_booking']['mobile3_1'] : ''?>" class="form-control matched_mobile_formate" placeholder="Mobile" maxlength="10"></div>				 
                                                        <div class="col-md-6 form-group">
                                                            <input type="text" name="phone3_1" id="phone3_1" value="<?= isset($_SESSION['new_booking']['phone3_1'])? $_SESSION['new_booking']['phone3_1'] : ''?>" class="form-control" placeholder="Home Number"></div>
                                                    </div>
                                                </div>
                                                <div class="col-md-5" style="margin-bottom:15px;">
                                                    <a class=" plus" href="javascript:;" style="text-decoration: none;" onclick="$('#contact-number2_3').hide();$('#mobile3_1').val('');$('#phone3_1').val('');">
                                                        <i class="fa fa-minus"></i> </a>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-7">
                                                    <div class="form-group">
                                                        <input type="text" name="email1_1" id="email1_1" value="<?= isset($_SESSION['new_booking']['email1_1'])? $_SESSION['new_booking']['email1_1'] : ''?>" class="form-control" placeholder="email">			  			 
                                                    </div>
                                                </div>
                                                <div class="col-md-5 additional-icon" onclick="$('#email2_2').show();">
                                                    ADD ADDITIONAL EMAIL
                                                    <a class=" plus" href="javascript:;" style="text-decoration: none;"><i class="fa fa-plus"></i> </a>
                                                </div>
                                            </div>
											
											


                                            <div class="row" id="email2_2" style="display:none">
                                                <div class="col-md-7">
                                                    <div class="form-group">				
                                                        <input type="text" name="email2_1" id="email2_1" value="<?= isset($_SESSION['new_booking']['email2_1'])? $_SESSION['new_booking']['email2_1'] : ''?>" class="form-control" placeholder="email">			  	 
                                                    </div>
                                                </div>
                                                <div class="col-md-5" style="margin-bottom:15px;">
                                                    <a class=" plus" href="javascript:;" style="text-decoration: none;"  onclick="$('#email2_3').show();"><i class="fa fa-plus"></i> </a>
                                                    <a class=" plus" href="javascript:;" style="text-decoration: none;" onclick="$('#email2_2').hide();$('#email2_0').val('');"><i class="fa fa-minus" aria-hidden="true"></i> </a>
                                                </div>
                                            </div>


                                            <div class="row" id="email2_3" style="display:none">
                                                <div class="col-md-7">
                                                    <div class="form-group">
                                                        <input type="text" name="email3_1" id="email3_1" value="<?= isset($_SESSION['new_booking']['email3_1'])? $_SESSION['new_booking']['email3_1'] : ''?>" class="form-control" placeholder="email">			  
                                                    </div>
                                                </div>
                                                <div class="col-md-5" style="margin-bottom:15px;">
                                                    <a class=" plus" href="javascript:;" style="text-decoration: none;" onclick="$('#email2_3').hide();$('#email3_1').val('');"><i class="fa fa-minus" aria-hidden="true"></i> </a>
                                                </div>
                                            </div>

                                        </div>


                                        <div class="booking-address contact-sub-container">
                                            <h4>Booking Address : </h4>
                                            <div class="row">
                                                <div class="col-md-7">
                                                    <div class="row">				 
                                                        <div class="col-md-6 form-group">
                                                            <input type="text" name="unit_number" id="unit_number" value="<?= isset($_SESSION['new_booking']['unit_number'])? $_SESSION['new_booking']['unit_number'] : ''?>" class="form-control" placeholder="Unit Number"></div>				 
                                                        <div class="col-md-6 form-group">
                                                            <input type="text" name="street_number" id="street_number" value="<?= isset($_SESSION['new_booking']['street_number'])? $_SESSION['new_booking']['street_number'] : ''?>" class="form-control required" placeholder="Street Number"></div>				 
                                                    </div>

                                                    <div class="form-group">
                                                        <input type="text" name="street_name" id="street_name" value="<?= isset($_SESSION['new_booking']['street_name'])? $_SESSION['new_booking']['street_name'] : ''?>" class="form-control required" placeholder="Street Name">
                                                    </div>

                                                    <div class="row">				 
                                                        <div class="col-md-6 form-group">
                                                            <input type="text" name="suburb" id="suburb" value="<?= isset($_SESSION['new_booking']['suburb'])? $_SESSION['new_booking']['suburb'] : ''?>" class="form-control required" placeholder="suburb"></div>				 
                                                        <div class="col-md-6 form-group" id="postCode-container">
                                                            <input type="text" name="postCode" id="postCode" value="<?= isset($_SESSION['new_booking']['postCode'])? $_SESSION['new_booking']['postCode'] : ''?>" class="form-control required" placeholder="postCode"></div>				 
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="customer-address contact-sub-container">
                                            <h4>Mailing Address : </h4>
                                            <div class="row">
                                                <div class="col-md-7">
                                                    <div class="form-coontrol">
                                                        <label class="label-control"><input type="checkbox" name="is_same" id="is_same" <?= isset($_SESSION['new_booking']['is_same']) && $_SESSION['new_booking']['is_same'] ? 'checked="checked"' : ''?> > Same as booking address </label>
                                                    </div>
                                                    <div class="row">				 
                                                        <div class="col-md-6 form-group">
                                                            <input type="text" name="customer_unitLotNumber" id="customer_unitLotNumber" value="<?= isset($_SESSION['new_booking']['customer_unitLotNumber']) ? $_SESSION['new_booking']['customer_unitLotNumber']: ''?>" class="form-control" placeholder="Unit Number"></div>				 
                                                        <div class="col-md-6 form-group">
                                                            <input type="text" name="customer_streetNumber" id="customer_streetNumber" value="<?= isset($_SESSION['new_booking']['customer_streetNumber']) ? $_SESSION['new_booking']['customer_streetNumber']: ''?>" class="form-control required" placeholder="Street Number"></div>				 
                                                    </div>

                                                    <div class="form-group">
                                                        <input type="text" name="customer_streetAddress" id="customer_streetAddress" value="<?= isset($_SESSION['new_booking']['customer_streetAddress']) ? $_SESSION['new_booking']['customer_streetAddress']: ''?>" class="form-control required" placeholder="Street Name">
                                                    </div>

                                                    <div class="row">				 
                                                        <div class="col-md-6 form-group">
                                                            <input type="text" name="customer_suburb" id="customer_suburb" value="<?= isset($_SESSION['new_booking']['customer_suburb']) ? $_SESSION['new_booking']['customer_suburb']: ''?>" class="form-control required" placeholder="suburb"></div>
                                                        <div class="col-md-6 form-group">
                                                            <input type="text" name="customer_postCode" id="customer_postCode" value="<?= isset($_SESSION['new_booking']['customer_postCode']) ? $_SESSION['new_booking']['customer_postCode']: ''?>" class="form-control required" placeholder="postCode"></div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>




                                    </div>
                                    <div class="box-footer">
                                        <div onclick="goTo('Contact-Details', 'confirm-booking')">
                                            <h3>To continue please click the arrow</h3>
                                            <a href="javascript:;" class="next-step" ><i class="fa fa-chevron-right  fa-4x" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div> 

                            </div>
                        </div>
                    </div> 

                    <div id="Quote-details" style="visibility:hidden">

                        <div class="row">
                            <div class="col-md-12">

                                <div class="box main-box">
                                    <div class="box-title">
                                        <div class="row"><h3>
                                                <div class="col-md-7 col-sm-7">Thanks, Here's your <b>Quote</b></div>
                                                <div class="col-md-5 additional-icon col-sm-7"><span id="additional_service">ADD ADDITIONAL SERVICE &nbsp;&nbsp;&nbsp; <a class=" plus" href="javascript:;" >
                                                            <i class="fa fa-plus"></i> </a></span></div></h3>
                                        </div>	 			 
                                    </div>
                                    <div class="box-content">
                                        <ul>
                                        </ul>

                                    </div>
                                    <div class="box-content2">
                                        <div class="row">

                                            <div class="col-md-4 email-quote" style="display:none">
                                                <a class="btn btn-info" href="javascript:;">Email me this <b>Quote</b></a>
                                            </div>

                                            <div class="col-md-12">
                                                <input type="hidden" value="" name="total_qoute" id="total_qoute"/>
                                                <input type="hidden" value="" name="gst" id="gst"/>
                                                <input type="hidden" value="" name="sub_total" id="sub_total"/>
                                                <div class="total-details"><span style="font-size: 15px;">(PRICE MAY VARY ON THE DAY DUE TO THE RODUCTS USED )</span><br/>
                                                    <div class="total-amount-details"><span style="font-size:14px;">Sub Total</span><span id="sub_total_value"></span></span> <br/> GST(10%):<span id="gst_value"></span><br/>
                                                        <span style="font-size:14px;">TOTAL</span> <b style="font-size:26px;"><span id="total_value"></span></b>
                                                        <div style="font-size:14px;">INC GST</div></div>	
                                                </div>
                                            </div>



                                        </div>

                                    </div>

                                    <div class="box-footer">
                                        <div onclick="goTo('Quote-details', 'availability')">
                                            <h3>To continue please click the arrow</h3>
                                            <a href="javascript:;" class="next-step" ><i class="fa fa-chevron-right  fa-4x" aria-hidden="true"></i></a></div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
					

                    <div id="availability" style="visibility:hidden">
                        <input type="hidden" name="booking_start" id="booking_start" value="<?= isset($_SESSION['new_booking']['booking_start'])? $_SESSION['new_booking']['booking_start'] : ''?>"/>
                        <input type="hidden" name="booking_end" id="booking_end" value="<?= isset($_SESSION['new_booking']['booking_end'])? $_SESSION['new_booking']['booking_end'] : ''?>"/>
                        <input type="hidden" name="top_position" id="top_position" value="<?= isset($_SESSION['new_booking']['top_position'])? $_SESSION['new_booking']['top_position'] : 0?>"/>
                        <input type="hidden" name="booking_time" id="booking_time" value="<?= isset($_SESSION['new_booking']['booking_time'])? $_SESSION['new_booking']['booking_time'] : 0?>"/>
						<input type="hidden" name="selected_date" id="selected_date" value="<?= isset($_SESSION['new_booking']['selected_date'])? $_SESSION['new_booking']['selected_date'] : ''?>"/>
                        <div class="row">
                            <div class="col-md-12">

                                <div class="box main-box">
                                    <div class="box-title">
                                        <div class="row"><h3>Select your <b>Appointment time</b></h3>
                                        </div>	 			 
                                    </div>
                                    <div class="box-content">

                                    </div>

                                    <div class="box-footer">
                                        <div onclick="goTo('availability', 'Contact-Details')">
                                            <h3>To continue please click the arrow</h3>
                                            <a href="javascript:;" class="next-step" ><i class="fa fa-chevron-right  fa-4x" aria-hidden="true"></i></a></div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>


                    <div id="confirm-booking" style="visibility:hidden">
                        <div class="row">
                            <div class="col-md-8">

                                <div class="box main-box">
                                    <div class="box-title">
                                        <h3>Confirm <b>Booking</b></h3>
                                    </div>
                                    <div class="box-content">
                                        <div class="msgs"></div>
                                        <div class="customer-contact summary"><span></span> <a class=" plus" href="javascript:;" style="text-decoration: none; float:right" onclick="goTo('confirm-booking', 'Contact-Details')"><i class="fa fa-pencil"></i> </a></div>
                                        <div class="booking-start summary"><span></span> <a class=" plus" href="javascript:;" style="text-decoration: none;float:right" onclick="goTo('confirm-booking', 'availability')"><i class="fa fa-pencil"></i> </a> </div>
                                        <div class="booking-address summary"><span></span> <a class=" plus" href="javascript:;" style="text-decoration: none;float:right" onclick="goTo('confirm-booking', 'Contact-Details')"><i class="fa fa-pencil"></i> </a></div>

                                        <div class="payment-details">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="form-control payment-types" style="background:#F2F3F7;height:44px">
                                                            <a><img src="pic/visa.png" alt="Visa Card"></a>
                                                            <a><img src="pic/master-card.png" alt="Master Card" ></a>
                                                            <a><img src="pic/american.png" alt="American Exoress Card"></a>
                                                            <a><img src="pic/discover.png" alt="Discover Card"></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
											
											<div id="payment-form">
											  <div class="row">			
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <input  id="card_number" value="" class="form-control" placeholder="Card number" type="text" name="card_number">
                                                        <input  id="eway_card_number" value=""  type="hidden"  name="eway_card_number">
                                                    </div>
                                                </div>
                                            </div>
											</div>
                                           

                                            <div class="row">			
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" name="card_name" placeholder="Card holder's name">
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row">				 
                                                        <div class="col-md-6 form-group">
                                                            <label>Expires</label>
                                                            <input type="text" placeholder="MM/YY" class="form-control datepicker" value="" id="expiry_date" name="expiry_date"></div>				 
                                                        <div class="col-md-6">
                                                            <label>CVN</label>
                                                            <div class="input-group date">
                                                                <input  id="cvn" value="" class="form-control" placeholder="3digits" type="text"  maxlength="3">
                                                                <input  id="eway_cvn" value=""  placeholder="3digits" type="hidden"  name="eway_cvn">
                                                                <span class="input-group-addon"><i class="fa fa-credit-card" aria-hidden="true"></i></span>
                                                            </div>
                                                        </div>				 
                                                    </div>
                                                </div>
                                            </div>



                                        </div>

                                    </div>
                                    <div class="box-footer">
                                        <div onclick="submit_form('#new-booking')">
                                            <h3>To Confirm please click here</h3>
                                            <a href="javascript:;" class="next-step"><i class="fa fa-check   fa-4x" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div> 

                            </div>
                            <div class="col-md-4">
                                <div class="box box-Qoute" id="booking_services">
                                    <div class="box-title">
                                        <h3>Quote</h3>
                                    </div>
                                    <div class="box-content">
                                        <ul></ul>
                                    </div>
                                    <div class="divider"></div>
                                    <div class="box-content2">
                                        <h5>TOTAL</h5>
                                        <div class="total-details">

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <input type="hidden" value="web" name="request_type"/>
                </form>

                <div id="confirmed-booking" style="visibility:hidden">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="box">
                                <div class="confirm-box box-title">
                                    <div class="row">
                                        <div class="col-md-6 left-col">				 
                                            <div class="thank-you"><img src="pic/thank-you.png"/></div>
                                            <p>
                                                Your Booking is confirmed.<br/>
                                                To make changes or track the job<br/> 
                                                please log in with the booking number
                                            </p>
                                            <div class="bok-num">BOK-1646</div>
                                            <div class="login">
											    <div class="row">
													<div class="col-md-4 col-md-offset-2">
													  <a class="btn btn-info pay-link" href="javascript:;" >Pay Booking</a>
													</div>
													<div class="col-md-4">
													  <a class="btn btn-info login-link" href="newbooking_login.php" >LOG IN</a>
													</div>
												  </div>
                                            </div>
                                            <p>
                                                If you need to change the contact details for this job, 
                                                please call us back ASAP on 0423 011 301 or contact support
                                            </p>
                                        </div>
                                        <div class="col-md-6 right-col">
                                            <div class="need-to-know">
                                                <h4>WHAT YOU NEED TO KNOW!</h4>
                                                <p>					
                                                    Please move all furniture and other items 
                                                    off the floor so it is ready for us to start 
                                                    when we arrive. 
                                                </p>
                                                <p>
                                                    *Please note that payment must be made on 
                                                    the day of the job by either credit card or EFT. <br/>
                                                    No cash payments accepted.
                                                </p>
                                                <p>
                                                    *If you don't go ahead with the job on the day, <br/>
                                                    a call out fee of $60 + GST applies.
                                                </p>
                                                For more info click below
                                                </p>
                                            </div>
                                            <div class="faqs">
                                                <a class="btn btn-info" href="javascript:;" target-booking="">FAQs</a>
                                            </div>
                                            <div class="download-app">
                                                <p>
                                                    Download our Tilecleaners App to recieve discounts
                                                    rewards and to manage your booking
                                                </p>
                                                <ul>
                                                    <li class="float-l"><a target="_blank" href="https://itunes.apple.com/us/app/tilecleaners/id921943286?mt=8"><img src="pic/app-store.png"/></a></li>
                                                    <li class="float-l"><a target="_blank" href="https://play.google.com/store/apps/details?id=au.tilecleaners.app"><img src="pic/android-store.png"/></a></li>
                                                    <div style="clear:both"></div>
                                                </ul>				   
                                            </div>

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12" style="padding-right:0px" id="faqs-content">
                                            <div class="faqs-body"></div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>

                </div>
				
				<div id="pay-booking" style="visibility:hidden">
				  <div class="row">
				    <div class="col-md-8">
					  <div class="box main-box">
                                    <div class="box-title">
                                        <h3>Pay <b>Booking</b></h3>
                                    </div>
                                    <div class="box-content">
                                        <div class="pay-msgs"></div>
                                        <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="form-control payment-types" style="background:#F2F3F7;height:44px">
                                                            <a><img src="pic/visa.png" alt="Visa Card"></a>
                                                            <a><img src="pic/master-card.png" alt="Master Card"></a>
                                                            <a><img src="pic/american.png" alt="American Exoress Card"></a>
                                                            <a><img src="pic/discover.png" alt="Discover Card"></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
										<form type="POST"  id="pay-new-booking"  onsubmit="add_payment('webservice/add-payment-by-customer')return false;" data-eway-encrypt-key="jj0v08RhXr5swodwNEHftys/7P7R2+LMBXU8iRCZ9voNuHVSuXrvJ6KmQ5c05lnzF563khRotBGYw8KmZzQuMlD6wf3d48wYq3hNWqd8ambURC5dWP1lDN9XkhPo2aH9EZpFmVU5/yvkt+Tvd0SF2BAorTmwFfq1JDbNspYQ05NLb4ct/jcEk78LTYaPRA/9dXtbzgYm/pechVNLIsbZmBIQSYtBIYaAtRxJIZtSduVkwE1Kb/I5EihI/TXIOLKJCwSHcb+Ma0dxDL3rF+dTyDvF7afVAX5G7O7MpQaA/F0D4rslX9b8Qawn4iCHGCgD1yXjB4lioplhxkOeAHD3Jw=="
							            >
										<input type="hidden" name="booking_id" id="booking_id"/>
                                        <div class="payment-details">
                                            <div class="row">			
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <input id="customer_eway_card_number" value="" type="hidden" name="customer_eway_card_number">
														<input id="customer_card_number" value="" class="form-control required" placeholder="Card number" type="text"  name="customer_card_number"/>
                                                        
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">			
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control required" name="card_name" placeholder="Card holder's name">
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row">				 
                                                        <div class="col-md-6 form-group">
                                                            <label>Expires</label>
                                                            <input type="text" placeholder="MM/YY" class="form-control datepicker required" value="" id="card_expiry_date" name="card_expiry_date"></div>				 
                                                        <div class="col-md-6">
                                                            <label>CVN</label>
                                                            <div class="input-group date">
                                                                <input id="customer_cvn" value="" class="form-control required" placeholder="3digits" type="text" maxlength="3"/>
                                                                <input id="customer_eway_cvn" value="" placeholder="3digits" type="hidden" name="customer_eway_cvn"/>
                                                                <span class="input-group-addon"><i class="fa fa-credit-card" aria-hidden="true"></i></span>
                                                            </div>
                                                        </div>				 
                                                    </div>
                                                </div>
                                            </div>



                                        </div>
										
										<div class="form-group">
                                            <label>PAYMENT AMOUNT</label>									
                                                <div class="input-group">
                                                 <input type="text" class="form-control required decimal" name="total_amount" id="total_amount" value=""><span class="input-group-addon">AUD</span>
                                                </div>
									    </div>
									 </form>	

                                    </div>
                                    <div class="box-footer">
                                        <div onclick="submit_form('#pay-new-booking')">
                                            <h3>To Pay please click here</h3>
                                            <a href="javascript:;" class="next-step"><i class="fa fa-check   fa-4x" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>
					
					</div>
				    <div class="col-md-4">
					  <div class="box box-Qoute" id="all_booking_services">
                        <div class="box-title">
                            <h3>Quote</h3>
                        </div>
                        <div class="box-content">
                            <ul>
							
							</ul>
                        </div>
                        <div class="divider"></div>
                            <div class="box-content2">
                                <h5>TOTAL</h5>
                            <div class="total-details"></div>
                           </div>
						  </div>
					</div>
				  </div>
				</div>



            </div>
        </div>


        <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false" >
            <div class="modal-dialog">


                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header box-title">
					   <button type="button" class="close" data-dismiss="modal" style="color:#fff;opacity:1">x</button>
                        <h3 class="modal-title">Select your <b>Surface</b></h3>
                    </div>
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer">
                       
                        <div class="box-footer">
                            <h3>To continue please click the arrow</h3>
                            <a href="javascript:;" class="next-step"><i class="fa fa-chevron-right  fa-4x" aria-hidden="true"></i></a>
                        </div>
                    </div>


                </div>

            </div>
        </div>

        <div id="errorModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title error"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> &nbsp; Error</h4>
                    </div>
                    <div class="modal-body">
                        <p></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
                    </div>
                </div>

            </div>
        </div>
		
		<div id="uploadImageModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
					    <button type="button" class="close" data-dismiss="modal" style="color:#fff;opacity:1">x</button>
                        <h3 class="modal-title box-title">Upload Service Images</h3>
						
                    </div>
					<form method="POST" id="upload-service-images" enctype="multipart/form-data" action="">
                    <div class="modal-body"> 
						<div class="row">
						<div class="form-group">
							<label class="col-md-3">Upload Image(s) :</label>
							<div class="col-md-9">
									<div id="dropzonePreview" class="dropzone-previews form-control dz-clickable" style="margin-bottom:20px">
										<div class="dz-message"></div>
									</div>
							</div>
						</div>
						</div>
						<div class="row">
						 <div class="fom-group">
						    <label class="col-md-3">Image Tag : </label>
							<div class="col-md-9">
									<select name="image_label" class="form-control" id="image_label">
									 <option value="">Select One</option>
									</select>
							</div>
						 </div>
						</div>
						<input type="hidden" name="service_id" id="selected_service_id"/>
						<input type="hidden" name="request_type" value="1"/>
					    
                    </div>
                    <div class="modal-footer">
                        <input type="submit" class="btn btn-default" value="Upload"/>
                    </div>
					</form>	
                </div>

            </div>
        </div>
		
		


    </body>
</html>
<script src="js/jquery-2.1.1.js" type="text/javascript"></script> 
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/validator.min.js" type="text/javascript"></script>
<script src="js/calendar.js" type="text/javascript"></script>
<script src="js/jquery.form.js" type="text/javascript"></script>
<script src="js/bootstrap-datepicker.js" type="text/javascript"></script>
<script src="js/date.format.js" type="text/javascript"></script>
<script src="plugins/dropzone/dropzone.js" type="text/javascript"></script>
<script src="https://secure.ewaypayments.com/scripts/eCrypt.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
<script src="js/summernote.min.js" type="text/javascript"></script>
<script src="js/formValidation/formValidation.js" type="text/javascript"></script>
<script src="js/formValidation/bootstrap.min.js" type="text/javascript"></script>
<script>
    var current_tab = '<?= $current_tab?>';
    var prev_tab = '<?= $prev_tab?>';
	var Saved_Session  = <?php echo json_encode($_SESSION['new_booking'] );?>;
	var selected_service = new Array();
	var total_qoute = new Array();
	var total_min_qoute = new Array();
	var choose_service = 0;
	var booking_time = 0 ;
	var valid = false; 
	var card_number_valid = false; 
	for (var key in Saved_Session) 
		{											  
		if (key.indexOf("services_") == 0)
			{ 
				service_id = key.split("_")[1];
				clone = key.split("_")[2];
				if(parseInt(clone)){
				 service = service_id+'_'+clone;
				}else{
				 service = service_id;
				}
				
				selected_service[service] = service;
				total_qoute[service] = Saved_Session[key]['price'];
				total_min_qoute[service] = Saved_Session[key]['service']['min_price'];
				choose_service++;
			}
			
			if(key == 'booking_time'){
			 booking_time = parseInt(Saved_Session[key]);
			}
			
			if(key == 'booking_start'){
				 selected_date = Saved_Session[key];
			}
			
			
		}
	
   

	function attributeSlider(obj){
	       
	        var Current = 0;
			var move = 0;
            var MaxNext = (obj.find(".slider_container li").size()) - 3;
            var Width = 163;
			var left_icon = obj.find('.slider_container .Left');
			var right_icon = obj.find('.slider_container .Right');
			left_icon.click(function(){
			  if (Current != MaxNext) {
                        Current++;
                        obj.find(".slider_container ul").animate({ marginLeft: -1 * Current * Width }, 1000, "easeOutExpo");
						
                    }
			});
			right_icon.click(function(){
			  if (Current != 0) {
                        Current--;
                        obj.find(".slider_container ul").animate({ marginLeft: -1 * Current * Width }, 1000, "easeOutExpo");
                    }
			});
			
			/*$('#myModal').on('click','.slider_container .Left,.slider_container .Right',function (){
			  
			  if ($(this).hasClass("Left")) {
                    if (Current != MaxNext) {
                        Current++;
                        obj.find(".slider_container ul").animate({ marginLeft: -1 * Current * Width }, 1000, "easeOutExpo");
						
                    }
                }
                else {
                    if (Current != 0) {
                        Current--;
                        obj.find(".slider_container ul").animate({ marginLeft: -1 * Current * Width }, 1000, "easeOutExpo");
                    }
                }
                return false;
			
			});*/
	 
	 
	}

    


                                            var gst_tax = 0.10;
                                            var total_gst_tax = <?php echo (1 + 0.10); ?>;
                                            var current_service_id = 0;
                                            

                                            function countServicePrice(){}
                                            function ClearSecondaryContact(parent) {

                                                $(parent).hide();
                                                $(parent + ' input[type=text]').val('');
                                                if ($(parent + ' :input').hasClass('required')) {
                                                    $(parent + ' :input').removeClass('required');
                                                }
                                            }

                                            function addRequiredField(parent) {
                                                if ('#secondary-contact-box2' == parent) {
                                                    $(parent + ' #first_name_1').addClass('required');
                                                    $(parent + ' #mobile1_1').addClass('required');
                                                }
                                                if ('#secondary-contact-box1' == parent) {
                                                    $(parent + ' #first_name_0').addClass('required');
                                                    $(parent + ' #mobile1_0').addClass('required');
                                                }
                                            }


                                            $(document).ready(function () {
											
											    
												
											    $('#Quote-details').on('click','#uploadImages',function(){
												  
												  var service_id = $(this).attr('service');
												  $('#upload-service-images #selected_service_id').val(service_id);
												   $("#image_label").html('');
												   $.ajax({
												        url: "webservice/get-image-tags",
										                dataType:"JSON",
  												        success: function(result){
														 $.each(result.ImageTypes, function(key,val) {
															$("#image_label").append('<option value="'+val.id+'">'+val.name+'</option>');
													     });
													   }
													});
												  
												});
												
												$('#is_same').prop('checked', true);
												$('.customer-address input[type=text]').prop('disabled', true);
                                                $('.customer-address input[type=text]').removeClass('required');
                                                $('.customer-address .errspan').remove();
												$('.customer-address input[type=text]').hide(); 
												
											
											    var postMaxSize = '<?php echo str_replace('M', '', ini_get('post_max_size')); ?>';
												var totalSizes = 0;
												var photoDropzone = new Dropzone("#upload-service-images", {
													url:'webservice/upload-image-new-booking',
													autoProcessQueue: false,
													uploadMultiple: true,
													parallelUploads: 10,
													maxFiles: 100,
													maxFilesize: 10,
													acceptedFiles: ".jpeg,.jpg,.png,.gif",
													previewsContainer: '#dropzonePreview',
													clickable: '#dropzonePreview',
													addRemoveLinks: true,
													// The setting up of the dropzone
													init: function () {
														var myDropzone = this;

														var submitButton = document.querySelector('#upload-service-images input[type=submit]');
														myDropzone = this; // closure		
														submitButton.addEventListener("click", function (e) {

															e.preventDefault();
															e.stopPropagation();
															if (myDropzone.getQueuedFiles().length === 0) {
																alert('please select at least one image');
															} else {
																//$('#button_loop').html('<img src="/pic/loading-4.gif">');
																$('#loading_img').show();
																$('#save_btn').hide();
																myDropzone.processQueue();
															}
														});

														this.on("successmultiple", function (files, response) {		
														var obj = jQuery.parseJSON(response)					
															 $.each(obj.files, function(key,val){															     
																$("#Quote-details").append('<input type="hidden" value="'+val+'" name="image_'+obj.service_id+'[]"/>');
															});
															
															this.removeAllFiles();
															$('#uploadImageModal').modal('hide');
														});
														this.on("errormultiple", function (files, response) {
															console.log(response);
															//$('#button_loop').html('<input type="submit" value="Save" class="btn btn-primary">');
															$('#loading_img').hide();
															$('#save_btn').show();
														});

														this.on("addedfile", function (file) {
															var fileSize = file.size;
															var MegaSize = fileSize / (1024 * 1024);
															totalSizes = totalSizes + MegaSize;
															if (totalSizes > postMaxSize) {
																this.removeFile(file);
																alert('Total files size is too big , Max total file size (30M)');
															}

														});

														this.on("removedfile", function (file) {
															var fileSize = file.size;
															var MegaSize = fileSize / (1024 * 1024);
															totalSizes = totalSizes - MegaSize;
															console.log(totalSizes);
														});



													}
												});
											
											   $('#Contact-Details .matched_mobile_formate').each(function () {
												   
												   $(this).focusin(function(){
													 var mobile = $(this).val();
													 if(mobile == ''){
													    $(this).val('04');
													 }
													});
													
													$(this).focusout(function(){
													 var mobile = $(this).val();
													 if(mobile == '04'){
													    $(this).val('');
													 }
													});
												  
                                                });


                                                if ($('.reponsive-menu').is(':visible')) {
                                                    var container = $('.container');
                                                    container.removeClass('container');
                                                    container.addClass('col-md-10 col-xs-9 col-sm-10');
                                                }

                                                $("#expiry_date, #card_expiry_date").datepicker({
                                                    format: "mm/yyyy",
                                                    viewMode: "months",
                                                    minViewMode: "months",
                                                });


                                                /*$.ajax({
                                                 type: "POST",
                                                 url: 'webservice/add-booking',
                                                 data:$('#new-booking').serialize(),
                                                 dataType: "text",
                                                 success: function(data) {
                                                 console.log('here');
                                                 data = data.split('{');
                                                 var new_data = '{' + data[data.length -1];
                                                 var obj = JSON.parse(new_data);
                                                 console.log(obj.msg);			
                                                 
                                                 },error:function(data){}
                                                 });*/
												 
												 
												 function getServicesForPostCode(from_session){
												    from_session = typeof from_session !== 'undefined' ? from_session : 0;
													var $postcode = $('#customer-postcode').val();
                                                    if ($postcode){
													  currentObject = $('#get-services').parent('.login');
                                                      $('#get-services').parent('.login').html('<img src="pic/loading-4.gif">');
                                                        $.ajax({
                                                            type: "POST",
                                                            url: 'webservice/contractor-services',
                                                            data: {'postCode': $postcode, 'from_web': 1,'from_session':from_session,'company_name':'Tile Cleaners Pty Ltd'},
                                                            dataType: "JSON",
                                                            success: function (data) {
															
															  if (data.msg == 'success') {
															    if((!from_session) || (from_session && current_tab == 'select-postCode')){
																    $('#select-postCode').css('visibility','hidden');
                                                                    $('#all-services').css('visibility','visible');
																	
																}
                                                                    
																	
                                                                    $('#all-services ul').html('');
                                                                    $('#postCode').val($postcode);
                                                                    $('#customer_postCode').val($postcode);
                                                                    if (data.result.length > 0) {
                                                                        $('#all-services .services_list').html('');
																		$('.service-title').html('');
																		$('.service-title').html('<h3>There are '+data.result.length+' Services available in your Area</h3>');
                                                                        for (var i = 0; i < data.result.length; i++) {
																		   description = data.result[i].description
																		   description = description.replace(/"/g, '\'');
                                                                          
																		   $('#all-services .services_list').append('<div class="col-md-4 img-container" onclick="serviceDesc(this ,\'' + data.result[i].service_id + '\' , \'' + data.result[i].defualt_image_large + '\'); return false;" service_id="' + data.result[i].service_id + '"> <div class="img-container" ><img src="'+data.result[i].defualt_image_compressed+'"/></div><h4 class="ellipsis">'+data.result[i].service_name+'</h4> <input type="hidden" value="' + data.result[i].min_price + '" id="min_' + data.result[i].service_id + '"/><input type="hidden" value="' + data.result[i].estimate_hours + '" id="estimate_hours_' + data.result[i].service_id + '"/><input type="hidden" id="service_desc_' + data.result[i].service_id + '" value="' + description + '"/></div>');
																		   


																		   /*$('#all-services ul').append('<li onclick="serviceDesc(this ,\'' + data.result[i].service_id + '\' , \'' + data.result[i].defualt_image_large + '\'); return false;" service_id="' + data.result[i].service_id + '" ><div class="row service-item" style="background: url(' + data.result[i].defualt_image_large + ') no-repeat;position: relative;height: 200px;background-size: cover;    margin: 0px;" ><a onclick="serviceDesc(this.parent() ,\'' + data.result[i].service_id + '\' , \'' + data.result[i].defualt_image_large + '\'); return false;" href="javascript:;" style="width:40px;height:40px;border-radius:50%;background: #01b5f0;position: absolute;right: 10px;top: 30%;display: block;text-align: center;"><i class="fa fa-chevron-right fa-2x" style="color:white;line-height: 43px;" aria-hidden="true"></i></a><h3 style="position: absolute;bottom: 0;left: 0; width: 100%;background: rgba(0, 0, 0, 0.6);color: white;margin: 0px;padding: 20px;">' + data.result[i].service_name + '</h3></div><input type="hidden" value="' + data.result[i].min_price + '" id="min_' + data.result[i].service_id + '"/><input type="hidden" value="' + data.result[i].estimate_hours + '" id="estimate_hours_' + data.result[i].service_id + '"/> \n\
                                                                            <input type="hidden" id="service_desc_' + data.result[i].service_id + '" value="' + data.result[i].description + '"/></li> ');*/
                                                                        }
                                                                    }
                                                                } else if (data.type == 'error') {

                                                                    currentObject.html('<a class="btn btn-info" href="javascript:;" id="get-services">Get Services</a>');
																	if(!($('.alert_box').length)){
                                                                      $('#select-postCode .welcome-text').prepend('<div class="alert_box"><div class="alert alert-danger alert-dismissible fade in col-md-12" role="alert" style="margin-top: 5px;"><button  style="color:#fff;opacity:1" type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">x</span></button><span>' + data.msg + '</span></div></div>');
																	}
                                                                }
															
															}
														});	
													
													}
													
												 
												 }





                                                $('#select-postCode').on('click','#get-services',function(){
												   getServicesForPostCode();
												});
												

												
											
											
                                                /*$('.all-services').on('click', 'li', function () {
                                                    var service_id = $(this).attr('service_id');
                                                    $service_name = $(this).find('h3').text();
                                                    var clone = 0;
                                                    var service = service_id;
                                                    while (selected_service[service]) {
                                                        clone++;
                                                        service = service_id + '_' + clone;
                                                    }
                                                    selected_service[service] = service;
                                                    $('.loader').show();

                                                    if (service_id) {
                                                        $.ajax({
                                                            type: "POST",
                                                            url: "common/drow_attribute",
                                                            data: "service_id=" + service_id + "&clone=" + clone + "&type=new-booking",
                                                            dataType: "JSON",
                                                            success: function (data) {
                                                                $('.loader').hide();
                                                                $('#myModal .modal-title').html($service_name);
                                                                $('#myModal .close').remove();
                                                                $('#myModal .modal-body').html(data.second);
                                                                $('#myModal .modal-body').prepend('<input type="hidden" value="' + clone + '" id="clone" name="clone"/><input type="hidden" value="' + service_id + '" id="current_service_id" name="service_id"/>');
                                                                $('#myModal .modal-body').prepend('<div style="display:none">' + data.first + '</div>');
                                                                $('#myModal .modal-footer').find('.box-footer').attr('id', 'next-attribute');
                                                                $('.select-value').remove();
                                                                $("#myModal").modal('show');
                                                                choose_service++;
                                                                //console.log(msg);
                                                            }
                                                        });
                                                    }
                                                });*/
                                                     
												$('#myModal').on('click', '.slider_container li', function () {
                                                    var text = $(this).find('.option-text').text();
                                                    var id = $(this).find('.option-text').attr('id');
													$('.slider_container li').css('border-color','#ccc');
													$(this).css('border-color','#01b5f0');
													
													$(this).closest('.slider_container').find('input').val(id);
													if($('#myModal').find('.modal-footer .select-value').length ){
													 
													  $('.select-value').html(text);
													}else{
													
													  $('#myModal .modal-footer').prepend('<div class="select-value">'+text+'</div>');
													}
                                                });	 
												
												

                                                $('#myModal').on('click', '#closePopup', function () {
                                                    $('#myModal').modal('hide');
                                                });


                                                $('#myModal').on('click', '#next-attribute', function () {
                                                    $current = $('.services_attributes_with_background:visible');
                                                    $next = $('.services_attributes_with_background:visible').next('.services_attributes_with_background');
                                                    if ($current.find(':input').val() == '' && !($current.find(':input').hasClass('comment_input'))) {
                                                        if (!($current.find('.error').length > 0)) {
                                                            $current.prepend('<div class="col-md-9 col-md-offset-3 error">Required Field</div>');
                                                        }
                                                    } else {
                                                        if ($next.length > 0) {
                                                            $attribute_name = $('.services_attributes_with_background:visible').next().find('label').text();
                                                            $attribute_value = $('.services_attributes_with_background:visible').next().find('.col-md-12').html();
															
                                                            /*if ($attribute_name == 'Description') {
															   
                                                                //$new_value = $attribute_value.replace("textarea", "div");
																//$new_value = $('<div/>').html($new_value).text();
                                                                //$new_value = $new_value.replace("form-control", " ");
                                                                $('.services_attributes_with_background:visible').next().remove();
                                                            }else{
															    $('.services_attributes_with_background:visible').next().css('display', 'block');
															}*/

                                                            $('.services_attributes_with_background:visible').next().css('display', 'block');
															$('#myModal .modal-footer .select-value').remove();
                                                            															
															attributeSlider($next);
                                                            $current.css('display', 'none');

                                                            /*var attributes_count = $('.services_attributes_with_background').length - 1;
                                                            if ($('.services_attributes_with_background:eq(' + attributes_count + ')').is(':visible')) {
                                                                $('<div class="form-group"><label class="control-label col-md-3">Comments</label><div class="col-md-9"><textarea name="customer_comment"  class="form-control" rows="5" cols="80"></textarea></div></div><div style="clear:both"></div>').insertAfter('.services_attributes_with_background:eq(' + attributes_count + ')');
                                                            }*/




                                                        } else {
                                                            
                                                            drawSelectedService();
                                                            $("#myModal").modal('hide');
                                                            //$('#all-services').hide();
                                                            $('#all-services').css('visibility','hidden');
                                                            $('#all-services .services-Details').css('visibility','hidden');
                                                            //$('#Quote-details').show();
                                                            $('#Quote-details').css('visibility','visible');
                                                            goTo('all-services', 'Quote-details');
															
                                                            //$('.box-footer > div').attr('onclick', "goTo('Quote-details', 'availability')");
                                                        }
                                                    }

                                                });

                                                $('#additional_service').click(function () {
                                                    //$('#Quote-details').hide();
                                                    $('#Quote-details').css('visibility', 'hidden');
                                                    //$('#all-services').show();
                                                    $('#all-services').css('visibility', 'visible');
                                                    $('#all-services .all-services').show();
                                                    $('.service-title').show();
                                                    $('#all-services .services-Details').hide();
													$('#all-services-menu').click();
													
													$('#all-services .box-footer > div').attr('onclick', "goTo('all-services', 'Quote-details')");

                                                });

                                                $('#myModal').on('click', '.select-options li', function () {

                                                    $('.select-options li').removeClass('selected-value');
                                                    $(this).addClass('selected-value');
                                                    $attribute_name = $(this).attr('attribute_name');
                                                    $attribute_type = $(this).attr('attribute_type');
                                                    $attribute_value_id = $(this).attr('attribute_value_id');
                                                    $attribute_image = $(this).find('.option-image img').attr('src');
                                                    $attribute_text = $(this).find('.option-text').html();

                                                    if ($attribute_type == 'Floor') {
                                                        $newContent = '<img src="' + $attribute_image + '" width="100%" height="100%"/>' + '<span class="floor-name">' + $attribute_text + '</span>' + '<input type="hidden" name="' + $attribute_name + '" value="' + $attribute_value_id + '" id="' + $attribute_name + '">';
                                                    } else {
                                                        $newContent = '<input type="hidden" name="' + $attribute_name + '" value="' + $attribute_value_id + '" id="' + $attribute_name + '">' + $attribute_text;
                                                    }
                                                    $('#myModal .select-value').html('You have selected:' + $attribute_text);
                                                    $('.' + $attribute_name).html($newContent);
                                                    $('#' + $attribute_name + '_summary').html($attribute_text);

                                                });


                                                $("#postCode-container").on("input", function () {
                                                    $new_value = $('#postCode').val();
                                                    $pre_value = $('#customer-postcode').val();
                                                    if ($new_value != $pre_value) {
                                                        $('#errorModal .modal-title').html('<i class="fa fa-question-circle" aria-hidden="true"></i> Confirm');
                                                        $('#errorModal .modal-body').html('To change the postcode you need to restart your booking process , Are you sure you want to continue ?');
                                                        $('#errorModal .modal-footer').html('<button type="button" class="btn btn-default" data-dismiss="modal" id="restart-newbooking">Yes</button><button type="button" class="btn btn-primary" data-dismiss="modal" id="cancel-change">No</button>');
                                                        $('#errorModal').modal('show');
                                                    }
                                                });



                                                $("#errorModal").on("click", "#restart-newbooking", function () {
                                                    window.location.reload();
                                                });

                                                $("#errorModal").on("click", "#cancel-change", function () {
                                                    $('#postCode').val($('#customer-postcode').val());
                                                    $('#errorModal').modal('hide');
                                                });


                                                $('.header_steps .main_circle_box').click(function () {

                                                    current_id = $(this).attr('id');
													if (!($(this).hasClass('finished'))){ 
                                                    active_id = $('.active').attr('id');
                                                    current_container = current_id.split("-menu")[0];
                                                    active_container = active_id.split("-menu")[0];
													
                                                    if ($(this).hasClass('marked')) {
                                                        $('#' + current_id).removeClass('marked');
                                                        $('#' + current_id).addClass('active added');
                                                        $('#' + current_id + ' a i').removeClass('fa fa-check icons fa-2x');
                                                        $('#' + current_id + ' a i').addClass('fa fa-pencil icons fa-2x');
                                                        if ($('#' + active_id).hasClass('added')) {
                                                            
                                                            $('#' + active_id).removeClass('added');
                                                            $('#' + active_id).addClass('marked added');
                                                            $('#' + active_id + ' a i').removeClass('fa fa-pencil icons fa-2x');
                                                            $('#' + active_id + ' a i').addClass('fa fa-check icons fa-2x');
															$('.' + active_id).removeClass("active");
                                                            $('#' + active_id + ' a i').removeClass('fa fa-pencil icons fa-2x');
                                                            $('#' + active_id + ' a i').addClass('fa fa-check icons fa-2x');
                                                        } else {
                                                            $('.' + active_id).removeClass("active");
                                                            $('#' + active_id + ' a i').removeClass('fa fa-pencil icons fa-2x');
                                                            $('#' + active_id + ' a i').addClass('fa fa-check icons fa-2x');
                                                        }

                                                        if (active_container == 'confirm-booking') {
                                                            //$('#confirmed-booking').hide();
                                                            $('#confirmed-booking').css('visibility', 'hidden');
                                                        }
														
														if(current_container == 'all-services'){
														   $('#all-services .services-Details').hide();
														   $('#all-services .services-Details').css('visibility', 'hidden');
														   $('#all-services .all-services').show();
														   $('#all-services .all-services').css('visibility', 'visible');
														   $('#all-services .box-footer > div').attr('onclick',"goTo('all-services', 'Quote-details')");
														}else{
														   $('#all-services .services-Details').hide();
														   $('#all-services .services-Details').css('visibility', 'hidden');
														   $('#all-services .all-services').hide();
														   $('#all-services .all-services').css('visibility', 'hidden');
														}

                                                        //$('#' + active_container).hide();
                                                        $('#' + active_container).css('visibility', 'hidden');
                                                        //$('#' + current_container).show();
                                                        $('#' + current_container).css('visibility', 'visible');
                                                        
                                                     }
													}
                                                });


                                                $('#is_same').click(function () {
                                                    if ($(this).is(":checked")) {
                                                        $('.customer-address input[type=text]').prop('disabled', true);
                                                        $('.customer-address input[type=text]').removeClass('required');
                                                        $('.customer-address .errspan').remove();
														$('.customer-address input[type=text]').hide();
                                                    } else {
                                                        $('.customer-address input[type=text]').prop('disabled', false);
                                                        $('#customer_streetNumber , #customer_streetAddress , #customer_suburb').addClass('required');
														$('.customer-address input[type=text]').show();
                                                    }
                                                });

                                               /* $('.customer_types_container').on('change', '#customer_type_id', function () {
                                                    selected_text = $(this).find("option:selected").text();
                                                    if (selected_text == 'Residential') {
                                                        $('.customer-address').hide();
                                                        $('.customer-address input[type=text]').removeClass('required');
                                                    } else {
                                                        $('.customer-address').show();
                                                        $('.customer-address input[type=text]').addClass('required');
														$('#customer_streetNumber , #customer_streetAddress , #customer_suburb').addClass('required');
                                                    }
                                                });*/


                                                $('.faqs a').click(function () {
                                                    var booking_id = $(this).attr('target-booking');
                                                    $.ajax({
                                                        type: "POST",
                                                        url: "webservice/get-faqs",
                                                        data: {'booking_id': booking_id},
                                                        dataType: "JSON",
                                                        success: function (data) {
                                                            $('#faqs-content .faqs-body').html(data);
                                                            $('#faqs-content .faqs-body').show();
                                                        }
                                                    });

                                                });


                                                /*$('#myModal').on('change', '.list_attribute', function () {
                                                    attribute_value_id = $(this).val();
                                                    var obj = $(this);
                                                    $.ajax({
                                                        type: "POST",
                                                        url: "common/get-attribute-values",
                                                        dataType: "json",
                                                        data: {'attribute_value_id': attribute_value_id},
                                                        success: function (data) {
                                                            if (obj.next('.attribute_viewer').length > 0) {
                                                                obj.next('.attribute_viewer').find('img').attr('src', data.default_image);
                                                            } else {
                                                                obj.after('<div style="text-align:center;margin-top:10px" class="attribute_viewer"><img src="' + data.default_image + '" width="100%" height="200px"/></div>');
                                                            }

                                                        }
                                                    });
                                                });*/
												
												
												$('#Residential').click(function(){
												  
												    $(this).css('background-color','#01b5f0');
												    $(this).css('color','#fff');
												    $('#Commercial').css('background-color','#eee');
												    $('#Commercial').css('color','#555');
												    $('.customer-address').hide();
												    $('.customer_types_list').hide();
												    $('.customer_business_name').hide();
													$('#customer_type_name').val('Residential');
													$('.customer-address input[type=text]').val('');
												    $('.customer-address input[type=text] , .customer_business_name input[type=text] , .customer_types_list').removeClass('required');
                                                    $('.customer-address .errspan').remove();
												});
												
												
												$('#Commercial').click(function(){
												  
												    $(this).css('background-color','#01b5f0');
												    $(this).css('color','#fff');
													$('.customer_types_list').show();
													$('#customer_type_name').val('Commercial');
												    $('#Residential').css('background-color','#eee');
												    $('#Residential').css('color','#555');
												    $('.customer-address').show();
													//$('#customer_streetNumber , #customer_streetAddress , #customer_suburb').addClass('required');
													if (!($('#Contact-Details .customer_types_list option').length > 0)) {
                                                        $('.customer_types_list').html('<img src="pic/loading-4.gif">');
                                                        $.ajax({
                                                            type: "POST",
                                                            url: 'webservice/get-customer-types',
                                                            dataType: "JSON",
															data:{'exclude':'Residential'},
                                                            success: function (data) {
                                                                if (data.result) {
                                                                    customer_type_html = '<select name="customer_type_id" class="customer_types_list form-control required"  id="customer_type_id"><option value="">Select custmer type</option>';
                                                                    
																	for (var i = 0; i < data.result.length; i++) {
																	    var selected = '';
																		if(data.selected == data.result[i].customer_type_id){
																		 selected = 'selected';
																		}
                                                                        customer_type_html += '<option value="' + data.result[i].customer_type_id + '"'+selected+'>'+ data.result[i].customer_type + '</option>';
                                                                    }
                                                                    customer_type_html += '</select>';
                                                                    $('.customer_types .customer_types_list').html(customer_type_html);
																	$('.customer_business_name').show();
																	$('#customer_business_name').addClass('required');
                                                                }
                                                            }
                                                        });
                                                    }else{
													 $('.customer_business_name').show();
													 $('#customer_business_name').addClass('required');
													}													
												});
												
												
												/// to retreive data from  session
																						
											if(current_tab){
											 getServicesForPostCode(1);
											 var draw_availability = 0;
											 var selected_date = '';
                                             for (var key in Saved_Session) 
												{												  
													if (key.indexOf("services_") == 0)
													{ 
													    service_id = key.split("_")[1];
														clone = key.split("_")[2];
														clone = typeof clone !== 'undefined' ? clone : 0													  
													    drawSelectedService(service_id,clone,1); //add this line
													}
													if(key == 'booking_start'){
													 draw_availability = 1;
													 selected_date = Saved_Session[key];
													}
													
												}
																																													 
											 if($('#customer_type_name').val()){
											   $('#'+$('#customer_type_name').val()).click();
											 }	
                                             if(current_tab != 'availability' && draw_availability){
											     console.log(selected_date);												 
												  if(selected_date){
												    date = selected_date.split("-");
													year = date[0];
													month = date[1];
													getCalendarContent(month,year);
												  }else{
												    getCalendarContent();
												  }
												  
												}											 
											    
											}
											
											if(prev_tab){                                             											
											  goTo(prev_tab,current_tab,1);
											}

                                            });




                                            function getServicePrice(event, parentId, attribute_name) {

                                                $newPrice = $(event).val();
                                                var service = parentId;
                                                var service_id = parentId;
                                                if (service.indexOf("_") > -1) {
                                                    service_id = parentId.split("_")[0];
                                                }
                                                var min_price = $('#min_' + service_id).val();
                                                var consider_min_price = 1;

                                                $('#' + attribute_name).val($newPrice);



                                                $.ajax({
                                                    type: "POST",
                                                    url: "common/count_service_price",
                                                    data: $('#' + parentId + ' :input').serialize() + "&service_id=" + service_id,
                                                    success: function (msg) {

                                                        /*if (parseFloat(msg) < parseFloat(min_price) && consider_min_price == 1) {
                                                            total_qoute[service] = parseFloat(min_price);
                                                        } else {
                                                            total_qoute[service] = parseFloat(msg);
                                                        }*/
														
														total_qoute[service] = parseFloat(msg.price);
														
														/////
														
														
														
														
														total_min_qoute[service] = min_price;
														var sub_qoute = 0;
														for (x in total_qoute) {														
                                                            sub_qoute = sub_qoute + total_qoute[x];
                                                         }
														 
														var min_sub_qoute = 0;
														for (x in total_min_qoute) {														
                                                            min_sub_qoute = min_sub_qoute + total_min_qoute[x];
                                                         }
														 
														
														min_sub_qoute =  Math.max.apply(null,
														Object.keys(total_min_qoute).map(function(e) {
																return total_min_qoute[e];
														}));
														 
														if(sub_qoute < min_sub_qoute){														
														  sub_qoute = min_sub_qoute;														
														} 
														
													
                                                        $('#sub_total').val(sub_qoute.toFixed(2));
                                                        $('#sub_total_value').html(' $' + sub_qoute.toFixed(2));

                                                        var total_discount = 0;
                                                        var call_out_fee = 0;

                                                        var total = ((sub_qoute + call_out_fee) - total_discount);
                                                        $('#total_qoute').val((total * total_gst_tax).toFixed(2));
                                                        $('#total_value').html(' $' + (total * total_gst_tax).toFixed(2));
                                                        $('#gst').val((total * gst_tax).toFixed(2));
                                                        $('#gst_value').html('$' + (total * gst_tax).toFixed(2));

                                                        $newContent = '<input type="hidden" name="' + attribute_name + '" value="' + $newPrice + '" id="' + attribute_name + '">' + $newPrice + ' m2<span><i class="fa fa-sort-desc" aria-hidden="true"></i></span>';

                                                        $('.' + attribute_name).html($newContent);
                                                        $('#' + attribute_name + '_summary').html($newPrice + ' m2');
                                                        $('.' + parentId + '_total_price').html('$' + (total_qoute[service] * total_gst_tax).toFixed(2));
														
														if (clone) {
														   price_attribute = service_id+''+msg.price_attribute_id + '_' + clone;
														} else {
															price_attribute = service_id+''+msg.price_attribute_id;
														}
														var price = msg.unit_price;
														price = price.toString();
														if(price.indexOf('per m2') == -1){
														  price = parseFloat(price).toFixed(2)
														  price = price+' per m2';
														}
														
														$('#attribute_'+price_attribute).val(price);



                                                    }
                                                });
                                            }


                                            function drawSelectedService(service_id ,clone, from_session , save_changes ) {
											    
												 var service_id = typeof service_id !== 'undefined' ? service_id : $('#myModal #current_service_id').val();
												 from_session = typeof from_session !== 'undefined' ? from_session : 0;
												 save_changes = typeof save_changes !== 'undefined' ? save_changes : 0;
                                                 var clone = typeof clone !== 'undefined' ? clone : $('#myModal #clone').val(); 
                                                //var service_id = $('#myModal #current_service_id').val();

                                                var service = 0;

                                                if (parseInt(clone)) {
                                                    service = service_id + '_' + clone;
                                                } else {
                                                    service = service_id;
                                                }
												
												if(save_changes){
												 data = $('#'+service+' :input').serialize();
												}else{
												 data = $('#myModal :input').serialize();
												}
														
														
                                                
												$.ajax({
                                                    type: "POST",
                                                    url: "common/count_service_price",
                                                    dataType: "json",
                                                    data: data + '&with_draw=true&from_session='+from_session+'&service_id='+service_id+'&clone='+clone,
                                                    success: function (msg) {

													
													if(!save_changes){
                                                        $('#Quote-details ul').prepend(msg.data);
														if(msg.extra_charge){
														 total_qoute[service] = parseFloat(msg.price) + parseFloat(msg.extra_charge);
														}else{
														 total_qoute[service] = parseFloat(msg.price) 
														}
														
														
                                                        
														total_min_qoute[service] = msg.min_price;
	
                                                        var sub_qoute = 0;
														for (x in total_qoute) {														
                                                            sub_qoute = sub_qoute + total_qoute[x];
                                                         }
														 
														var min_sub_qoute = 0;
														min_sub_qoute =  Math.max.apply(null,
														Object.keys(total_min_qoute).map(function(e) {
																return total_min_qoute[e];
														}));
														
														if(sub_qoute < min_sub_qoute){
														   sub_qoute = min_sub_qoute;
														} 
														 
														
												
                                                        $('#sub_total').val(sub_qoute.toFixed(2));
                                                        $('#sub_total_value').html(' $' + sub_qoute.toFixed(2));

                                                        var total_discount = 0;
                                                        var call_out_fee = 0;

                                                        var total = ((sub_qoute + call_out_fee) - total_discount);
                                                        $('#total_qoute').val((total * total_gst_tax).toFixed(2));
                                                        $('#total_value').html(' $' + (total * total_gst_tax).toFixed(2));
                                                        $('#gst').val((total * gst_tax).toFixed(2));
                                                        $('#gst_value').html('$' + (total * gst_tax).toFixed(2));
														
													}
													
													//booking_time = parseInt($('#booking_time').val());
													 if(!from_session){
														 estimated_time = parseInt($('#estimate_hours_'+service_id).val());
														 booking_time = booking_time + estimated_time;
														 $('#booking_time').val(booking_time);
														}
														
                                                    }
                                                });

                                            }





                                            function confirmDeleteService(service) {

                                                $('#errorModal .modal-title').html('<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Warning');
                                                $('#errorModal .modal-body').html('Are you sure you want to delete this service ?');
                                                $('#errorModal .modal-footer').html('<button type="button" class="btn btn-default" data-dismiss="modal" onclick="removeService(\'' + service + '\')">Yes</button><button type="button" class="btn btn-primary" data-dismiss="modal">No</button>');
                                                $('#errorModal').modal('show');

                                            }

                                            function removeService(service) {


                                                $('#' + service).remove();
                                                selected_service[service] = '';
                                                total_qoute[service] = 0;
												delete total_min_qoute[service];
                                                
                                                var sub_qoute = 0;
                                                for (x in total_qoute) {
                                                    sub_qoute = sub_qoute + total_qoute[x];
                                                }
												
												min_sub_qoute =  Math.max.apply(null,
												Object.keys(total_min_qoute).map(function(e) {
													return total_min_qoute[e];
												}));
												
												if(sub_qoute < min_sub_qoute){														
													sub_qoute = min_sub_qoute;														
												} 

                                                var total_discount = 0;
                                                var call_out_fee = 0;

                                                var total = ((sub_qoute + call_out_fee) - total_discount);
                                                $('#total_qoute').val((total * total_gst_tax).toFixed(2));
                                                $('#gst').val((total * gst_tax).toFixed(2));
                                                $('#total_value').html('$ ' + (total * total_gst_tax).toFixed(2));
                                                $('#gst_value').html('$' + (total * gst_tax).toFixed(2));
                                                $('#sub_total').val('$' + sub_qoute.toFixed(2));
                                                $('#sub_total_value').html(' $' + sub_qoute.toFixed(2));
                                                choose_service--;
												saveDataInSession('Quote-details',service);
                                            }


                                            function changeValue(attribute_id, attribute_name) {


                                                if (attribute_id) {
                                                    attribute_value = $('#' + attribute_name).val();
                                                    $.ajax({
                                                        type: "POST",
                                                        url: "common/get-attribute-values",
                                                        dataType: "json",
                                                        data: {'attribute_id': attribute_id},
                                                        success: function (data) {

                                                            if ($('#myModal .modal-footer .select-value').length > 0) {
                                                                $('#myModal .modal-footer .select-value').remove();
                                                            }
                                                            $('#myModal .modal-header .close').remove();
                                                            $('#myModal .modal-header').prepend('<button type="button" class="close" data-dismiss="modal" style="color:#fff;opacity:1">&times;</button>');
                                                            if (data.is_list) {
                                                                $('#myModal .modal-title').html('Select your <b>' + data.attribute.attribute_name + '</b>');
                                                                options = '<div class="select-options"><ul>';
                                                                attribute_list_value = 0;
                                                                $.each(data.listOptions, function (index, option) {
                                                                    selected = '';
                                                                    if (index == attribute_value) {
                                                                        attribute_list_value = option.value;
                                                                        selected = 'selected-value';
                                                                    }
                                                                    options += '<li attribute_type = "' + data.attribute.attribute_name + '"class="' + selected + '"attribute_value_id ="' + index + '" attribute_name="' + attribute_name + '"><div class="option-image"><img src="' + option.default_image + '" width="110px" height="100px"></div><div class="option-text ellipsis">' + option.value + '</div></li>';
                                                                });
                                                                options += '</div></ul>';
                                                                $('#myModal .modal-body').html(options);
                                                                $('#myModal .modal-footer').prepend('<div class="select-value">You have selected:' + attribute_list_value + '</div>');
                                                            } else {
                                                                $parentId = $('.' + attribute_name).parents('li').attr('id');
                                                                onChange = '';
                                                                if (data.attribute.attribute_variable_name == 'quantity') {
                                                                    onChange = 'onchange="getServicePrice(this,\'' + $parentId + '\' , \'' + attribute_name + '\')"';
                                                                }
                                                                $('#myModal .modal-title').html('Change your <b>' + data.attribute.attribute_name + '</b>');
                                                                $('#myModal .modal-body').html('<div  class="form-group services_attributes_with_background"><label class="control-label col-md-2">' + data.attribute.attribute_name + '</label><div class="col-md-10"><input type="text" class="form-control" value="' + attribute_value + '"  ' + onChange + '/></div><div style="clear:both"></div></div>');
                                                            }

                                                            $('#myModal .box-footer').attr("id", "closePopup");
                                                            $('#myModal .modal-footer').show();
															$("#myModal").modal('show');
															
                                                        }
                                                    });
                                                }
                                            }

                                            function getAttributes(service_id) {

                                                var $service_name = $('#all-services .services-Details').find('.service_name_' + service_id).text();
                                                var clone = 0;
                                                var service = service_id;
                                                while (selected_service[service]) {
                                                    clone++;
                                                    service = service_id + '_' + clone;
                                                }

                                                selected_service[service] = service;
                                                $('.loader').show();
                                                if (service_id) {
                                                    $.ajax({
                                                        type: "POST",
                                                        url: "common/drow_attribute",
                                                        data: "service_id=" + service_id + "&clone=" + clone + "&type=new-booking",
                                                        dataType: "JSON",
                                                        success: function (data) {
                                                            $('.loader').hide();
                                                            $('#myModal .modal-title').html($service_name);
                                                            //$('#myModal .close').remove();
															var attributes = data.second;
															var attributes = $(attributes.bold()); 
															attributes.find('script').remove();
															attributes = attributes.html();
                                                            $('#myModal .modal-body').html(attributes);
                                                            $('#myModal .modal-body').append('<div class="form-group services_attributes_with_background" style="display:none"><label class="control-label col-md-3">Comments</label><div class="col-md-9"><textarea name="customer_comment"  class="form-control comment_input" rows="5" cols="80"></textarea></div></div><div style="clear:both"></div>');
															
                                                            $('#myModal .modal-body').prepend('<input type="hidden" value="' + clone + '" id="clone" name="clone"/><input type="hidden" value="' + service_id + '" id="current_service_id" name="service_id"/>');
                                                            $('#myModal .modal-body').prepend('<div style="display:none">' + data.first + '</div>');
                                                            $('#myModal .modal-footer').find('.box-footer').attr('id', 'next-attribute');
                                                            $('.select-value').remove();
                                                            $("#myModal").modal('show');
                                                            $("#myModal .modal-footer").show();
															
                                                            choose_service++;
															 //$("#lightSlider").lightSlider(); 
                                                            //console.log(msg);
                                                        }
                                                    });
                                                }


                                            }
                                            function goTo(from, to,from_session) {
											    from_session = typeof from_session !== 'undefined' ? from_session : 0;

                                                if (from == 'all-services' || from == 'Quote-details') {
                                                    if (choose_service == 0) {
                                                        $('#errorModal .modal-body').html('Please choose your service to continue');
                                                        $('#errorModal .modal-footer').html('<button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>');
                                                        $('#errorModal').modal('show');
                                                        return;
                                                    }
													
													$('#all-services .all-services').css('visibility','hidden');
													$('#all-services .services-Details').css('visibility','hidden');
                                                }

                                                if (from == 'availability') {
                                                    if (($('#booking_start').val() == '') || ($('#booking_end').val() == '')) {
                                                        $('#errorModal .modal-body').html('Please select specific time range');
                                                        $('#errorModal .modal-footer').html('<button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>');
                                                        $('#errorModal').modal('show');
                                                        return;
                                                    } else {
													    var saved_booking_date = $('#booking_start').val();
														if(saved_booking_date){
														  booking_date = saved_booking_date;
														}

                                                        if (booking_date == today) {
                                                            var d = new Date(); // for now
                                                            var h = d.getHours(); // => 9
                                                            var m = d.getMinutes(); // =>  30
                                                            var s = d.getSeconds(); // => 51
                                                            if (h < 10) {
                                                                h = '0' + h;
                                                            }
                                                            if (m < 10) {
                                                                m = '0' + m;
                                                            }
                                                            var currentTime = h + ':' + m;
                                                            var booking_start_time = $('#booking_start').val();
                                                            booking_start_time = booking_start_time.split(' ')[1];
                                                            if (currentTime > booking_start_time) {
                                                                $('#errorModal .modal-body').html('You can\'t make a booking for a date in the past!');
                                                                $('#errorModal .modal-footer').html('<button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>');
                                                                $('#errorModal').modal('show');
                                                                $('#booking_start').val('');
                                                                $('#booking_end').val('');
                                                                return;
                                                            }
                                                        }
                                                    }
                                                }

                                                if (from == 'Contact-Details') {
                                                    is_valid = validateContactForm();
                                                    if (!(is_valid)) {
                                                        return;
                                                    }
                                                }

                                                if (to == 'Contact-Details') {
                                                    $('#Residential').click();
                                                }
												
												


                                                $('.' + from + '-menu').removeClass("active");
                                                $('.' + from + '-menu').addClass('marked added');
                                                $('.' + from + '-menu a i').removeClass('fa fa-pencil icons fa-2x');
                                                $('.' + from + '-menu a i').addClass('fa fa-check icons fa-2x');
                                                // $('.' + to + '-menu a').removeClass();
                                                $('.' + to + '-menu').addClass('active');
                                                $('.' + to + '-menu a i').removeClass('fa fa-check icons fa-2x');
                                                $('.' + to + '-menu a i').addClass('fa fa-pencil icons fa-2x');
                                                //$('#' + from).hide();
                                                $('#' + from).css('visibility','hidden');
                                                //$('#' + to).show();
                                                $('#' + to).css('visibility','visible');
                                                if (to == 'availability' && from != 'confirm-booking') {
                                                    $('.loader').show();
													 getCalendarContent();
                                                    if(selected_date){
												    date = selected_date.split("-");
													year = date[0];
													month = date[1];
													getCalendarContent(month,year);
												  }else{
												    getCalendarContent();
												  }
                                                }
												
												if(from == 'all-services .services-Details'){
 												  $('#all-services .box-footer > div').attr('onclick',"goTo('all-services', 'Quote-details')");
												  $('.service-title').show();
												  $('#'+to).show();
												  $('#'+from).hide();
												}

                                                if (to == 'confirm-booking') {
												  
												   drawTotalQuote(from_session);
												  
                                                    
                                                }
												
												if(!from_session){
												 if(from == 'Quote-details'){
												     $('input[name^="services"]').each(function() {
													 var service = $(this).val();
													 var service_id = service;
													 clone = 0;
													if(service.indexOf("_") > -1){
													  service_id = service.split("_")[0];
													  clone = service.split("_")[1];													  
													}
													
													drawSelectedService(service_id,clone,0,1);
													
												   });
												 }
												 
												   saveDataInSession(from);
												  
												}
												
												if(from_session){
                                                $('.' + from + '-menu').prevAll().removeClass("active");
                                                $('.' + from + '-menu').prevAll().addClass('marked added');
                                                $('.' + from + '-menu a i').prevAll().addClass('fa fa-check icons fa-2x');
                                                $('.' + from + '-menu a i').prevAll().removeClass('fa fa-pencil icons fa-2x');
                                                }

                                            }
											
											
											
                                          


                                            function ajax_submit(url) {

                                                if ($('#card_number').val() != '') {
                                                    $('#eway_card_number').val(eCrypt.encryptValue($('#card_number').val()));
                                                    $('#eway_cvn').val(eCrypt.encryptValue($('#cvn').val()));
                                                }else{
												 card_number_valid = true;
												}
												if(card_number_valid){
												$('.loader').show();
                                                var options = {
                                                    target: '#output',
                                                    type: 'post',
                                                    url: url,
                                                    dataType: 'json',
                                                    success: function (data) {
                                                        //data = data.split('{');
                                                        //var new_data = '{' + data[data.length - 1];
                                                        var obj = JSON.parse(data);
                                                        if (obj.type == 'success') {
                                                            //$('#confirm-booking').hide();
                                                            $('#confirm-booking').css('visibility','hidden');
                                                            //$('#confirmed-booking').show();
                                                            $('#confirmed-booking').css('visibility','visible');
                                                            $('#confirmed-booking .bok-num').html(obj.result);
                                                            $('#confirmed-booking .login .login-link').attr('href','newbooking_login.php?num='+obj.result);
                                                            $('#confirmed-booking .login .pay-link').attr('onclick','payBooking('+obj.booking_id+')');
                                                            $('#booking_id').val(obj.booking_id);
                                                            $('#confirmed-booking .faqs a').attr('target-booking', obj.booking_id);
															$('.header_steps .main_circle_box').addClass('finished');
                                                        } else if (obj.type == 'error') {
                                                            $('.loader').hide();
                                                            $(".msgs").html('<div class="alert alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button" style="color:#fff;opacity:1">x</button>' + obj.msg + '</div>');
                                                        }

                                                        $('.loader').hide();
                                                    }
                                                };
                                                $('#new-booking').ajaxSubmit(options);
                                               }
											}

                                            function drawTotalQuote(from_session) {
                                                $('#booking_services .box-content ul').html('');
                                                $('#all_booking_services .box-content ul').html('');
												if(from_session){
												  total_details = $('#total_price').val();
												  service_details = $('#total_services').val();
												  
												  $('#booking_services .box-content ul').html(service_details);
												  $('#all_booking_services .box-content ul').html(service_details);
												  $('#booking_services .total-details').html(total_details);
												  $('#all_booking_services .total-details').html(total_details);
												}else{
												  $('input[name^="services"]').each(function () {
                                                    var service_id = $(this).val();
													console.log(service_id);
                                                    service_name = $('#' + service_id).find('.subtitle-service-name').html();
                                                    service_attributes = $('#' + service_id).find('.service_attributes').html();
                                                    service_amount = $('#' + service_id).find('.service_amount').html();
                                                    $service_details = '<li class="service-details"><div class="row"><div class="col-md-8"><h4>' + service_name + '</h4><span class="service-attributes">' + service_attributes + '</span></div><div class="col-md-4">' + service_amount + '</div> </div></li>';
                                                    $('#booking_services .box-content ul').append($service_details);
                                                    $('#all_booking_services .box-content ul').append($service_details);
													
                                                 });
												 total_details = $('.total-amount-details').html();
                                                 $('#booking_services .total-details').html(total_details);
                                                 $('#all_booking_services .total-details').html(total_details);
												}
                                                
                                                
                                                $customer_info = $('#first_name').val() + ' ' + $('#last_name').val() + ' ' + $('#mobile1').val();
                                                $booking_address = $('#unit_number').val() + "/" + $('#street_number').val() + ' ' + $('#street_name').val() + ' ' + $('#suburb').val() + ' ' + $('#postCode').val();
                                                $('#confirm-booking .customer-contact span').html($customer_info);
                                                $('#confirm-booking .booking-address span').html($booking_address);
                                                $booking_start = $('#booking_start').val();
                                                $booking_end = $('#booking_end').val();
                                                $booking_date = getBookingDateFormating($booking_start, $booking_end, 1);
                                                $('#confirm-booking .booking-start span').html($booking_date);
                                            }

                                            function getBookingDateFormating($booking_start, $booking_end, $with_time) {

                                                var $booking_start = new Date($booking_start);
                                                var $booking_end = new Date($booking_end);
                                                //var start_date = $booking_start.toString("dddd MMMM yyyy");											
												var Full_start_date = dateFormat($booking_start,'ddd mmm dd yyyy h:MM TT');
												var start_date = dateFormat($booking_start,'ddd mmm dd yyyy');
                                                var Full_end_time = dateFormat($booking_end,'h:MM TT');
                                                if ($with_time) {
                                                    return Full_start_date + ' to ' + Full_end_time;
                                                } else {
                                                    return start_date + ' ';
                                                }


                                            }

                                            function validateContactForm() {
                                                $empty_field = true;
                                                $('#Contact-Details .required').each(function () {
                                                    if ($(this).val() == '') {
                                                        $empty_field = false;
                                                        $(this).after('<span class="fa fa-info-circle errspan"></span>');
                                                    } else if ($(this).next().length > 0 && $(this).val() != '') {
                                                        $(this).next().remove();
                                                    }
                                                });
												
												$('#Contact-Details .matched_mobile_formate').each(function () {
												   
												   if ($(this).val() != '') {
												    pattern = /^04[\d]{8}$/;
													number = $(this).val();
													$check_pattern = pattern.test(number);
													if($check_pattern){
													  if ($(this).next().length > 0 && $(this).val() != '') {
                                                        $(this).next().remove();
                                                     }
													}else{
													  $empty_field = false;
													  $(this).after('<span class="fa fa-info-circle errspan"></span>');
													}
												   }
												  
                                                });
												
												
												
												
												
                                                return $empty_field;
                                            }


                                            function submit_form(form_id) {                                               
                                                $(form_id).submit();
                                            }

                                            function addSecondaryContact() {

                                                if ($('.secondary-contact:eq(0)').is(':visible')) {
                                                    $('.secondary-contact:eq(1)').show();
                                                    addRequiredField('#secondary-contact-box2');
                                                } else {
                                                    $('.secondary-contact:eq(0)').show();
                                                    addRequiredField('#secondary-contact-box1');
                                                }

                                            }
                                            
											function serviceDesc(that, id, img) {
                                                var serviceName = $(that).find('h4').html();
                                                var serviceDesc = $('#service_desc_' + id).val();
                                                var estimate_hours = $('#estimate_hours_' + id).val();
                                                $('#all-services .all-services').hide();
												//image = "<img style='width:100%;height:300px; object-fit: cover;' src='" + img + "'/>";
												image = '';
												$('.loader').show();
												$.ajax({
													type: "POST",
													dataType : "JSON",
													data:{'service_id':id},
													url: 'settings/services/get-service-images',
													success: function(data) {
													if(data.length > 0){
													SliderSize = 0;
													var links = '';
													 image = '<div class="slider"><ul>';
													 $.each(data, function(key,value){													
														var type = value.type.split("/");
														var parts = value.path.split("public");														
														if(type['0'] == 'image'){
														   if(SliderSize == 0){
														    links += "<a href='javascript:;' class='active'><i class='fa fa-circle' aria-hidden='true'></i></a> ";
														   }else{
														   links += "<a href='javascript:;'><i class='fa fa-circle' aria-hidden='true'></i></a>";
														   }														   
														   image += '<li><img src="'+parts[1]+'" style="object-fit: cover;"/></li>';
														   SliderSize++;
														}
													  });
													  image += '</ul><div class="SliderNav" style="position:absolute;bottom:10px;width:100%">'+links+'</div></div>';

                                                        current = 0;
														$('#all-services').on('click','.SliderNav a',function(){
														  if (!$(this).hasClass("active")) {
																var index = $(".SliderNav a").index(this);
																current = index;
																$(".slider li:visible").fadeOut();
																$(".slider li").eq(index).fadeIn();
																$(".SliderNav a").removeClass("active");
																$(this).addClass("active");
															}												
														});	
																												
														SliderTimer = setInterval(SliderClick, 5000);

														
													}else{
													 image = "<img style='width:100%;height:300px; object-fit: cover;' src='" + img + "'/>";
													 }
													 
													 $('#all-services .services-Details').html("<a href='javascript:;' style='width:40px;height:40px;border-radius:50%;background: #01b5f0;margin-bottom:15px;display: block;text-align: center;position:absolute;top:15px;left:15px;z-index:1' onclick=\"goTo('all-services .services-Details', 'all-services .all-services')\"><i class='fa fa-chevron-left fa-2x' style='color:white;line-height: 43px;' aria-hidden='true'></i></a><div>"+image+"<div style='clear:both'></div><h3 class='service_name_" + id + "' style='float:left'>" + serviceName + "</h3><div class='faq-button'  data-toggle='modal' data-target='#myModal' onclick='popup_faq(\""+id+"\" , \""+serviceName+"\"); return false;'>FAQ's<a class='faq-icon' href='#'><i class='fa fa-question  fa-2x' aria-hidden='true' style='margin-top: 5px;color: #fff;'></i></a></div><div onclick='getAttributes(" + id + ")' class='book-service'><b>Book</b> this Service<a href='javascript:;' class='next-step' style='width: 60px;height: 60px;right: -7px;top: -5px;'><i class='fa fa-chevron-right  fa-2x' aria-hidden='true' style='margin-top: 9px;'></i></a></div><div style='clear:both'></div><p>" + serviceDesc + "</p><br/>\n\ <h4>Time Estimate</h4> <span>An average job takes around " + estimate_hours + " hrs</span></div>");
													 $('.loader').hide();
													 													 													 
													}
												 });
												 
												 
												 $('.service-title').hide();
												 $('#all-services .services-Details').show();
												 $('#all-services .services-Details').css('visibility','visible');
                                                 $('#all-services .box-footer > div').attr('onclick', 'getAttributes(' + id + ')');
												 
                                            }
											
											function SliderClick() {
												var SliderSize = $(".slider li").size();
												$(".SliderNav a").eq(current++).click();
												if (current == SliderSize)
													current = 0;
											}
											
											
											function payBooking(booking_id){
											  $.ajax({
												        url: "booking/get-booking-details",
														type:"POST",
														data:{'booking_id':booking_id},
														dataType:"JSON",
  												        success: function(result){
                                                            console.log(result);
                                                            if(result.bookingCustomer.customer_token_id){
															  $('#pay-booking .payment-details').remove();
															}
															 
															$('#confirmed-booking').css('visibility','hidden');
															$('#pay-booking').css('visibility','visible');

													   }
													});
											}
											
											function add_payment(url){
											$empty_field = true;
											$('#pay-new-booking .required').each(function () {
                                                    if ($(this).val() == '') {
                                                        $empty_field = false;
                                                        $(this).after('<span class="fa fa-info-circle errspan"></span>');
                                                    } else if ($(this).next('.errspan').length > 0 && $(this).val() != '') {
                                                        $(this).next('.errspan').remove();
                                                    }
                                                });
											 if ($('#customer_card_number').val()) {
												    pattern = /^[\d]{16}$/;
													number = $('#customer_card_number').val();
													$check_pattern = pattern.test(number);
													if($check_pattern){
													  if ($('#customer_card_number').next('.errspan').length > 0 && $('#customer_card_number').val() != '') {
                                                        $('#customer_card_number').next('.errspan').remove();
                                                     }
													 $('#customer_eway_card_number').val(eCrypt.encryptValue($('#customer_card_number').val()));
                                                     $('#customer_eway_cvn').val(eCrypt.encryptValue($('#customer_cvn').val()));
													}else{
													  $empty_field = false;
													  $('#customer_card_number').after('<span class="fa fa-info-circle errspan"></span>');
													  console.log('test1');   
													}
												   }
												  
												   
											if($empty_field && valid){	
											$('.loader').show();
										    
											var options = {
                                                    target: '#output',
                                                    type: 'post',
                                                    url: url,
                                                    dataType: 'json',
                                                    success: function (data) {
                                                        $('.loader').hide();
														console.log(data);
                                                    }
                                                };
                                                $('#pay-new-booking').ajaxSubmit(options);											
										    }
										  }
											
											function saveDataInSession(current_selected_tab , service_index){
											service_index = typeof service_index !== 'undefined' ? service_index : 0;
											data = $('#new-booking').serialize()+'&current_tab='+current_selected_tab;
											if(current_selected_tab == 'Quote-details'){
											 var service_details = '';
											 $('input[name^="services"]').each(function () {
                                                    var service_id = $(this).val();
                                                    service_name = $('#' + service_id).find('.subtitle-service-name').html();
                                                    service_attributes = $('#' + service_id).find('.service_attributes').html();
                                                    service_amount = $('#' + service_id).find('.service_amount').html();
                                                    service_details += '<li class="service-details"><div class="row"><div class="col-md-8"><h4>' + service_name + '</h4><span class="service-attributes">' + service_attributes + '</span></div><div class="col-md-4">' + service_amount + '</div> </div></li>';
													service_details = escape(service_details);
                                                    service_details = $("<div/>").html(service_details).text();
													

													
                                                });
                                                var total_details = $('.total-amount-details').html();
												total_details = escape(total_details);
												console.log(service_index);
												if(service_index){
												  data = 'current_tab='+current_selected_tab+'&remove_service=1&service_index='+service_index+'&total_services='+service_details+'&total_price='+total_details;
												}else{
												  data = $('#new-booking').serialize()+'&current_tab='+current_selected_tab+'&total_services='+service_details+'&total_price='+total_details;
												}
												
											}
											
												
											  $.ajax({
												        url: "default/save-booking-in-session",
														type:"POST",
														data:data,
  												        success: function(result){														 
													   }
													});
											 
											
											}
											
											function popup_faq(id , service_name){
                                                
                                                 $('#myModal .modal-title').html(service_name);
												  $('#myModal .modal-body').html('<img src="pic/loading-4.gif"/>');
                                                    $.ajax({
                                                        type: "POST",
                                                        url: "webservice/get-faqs",
                                                        data: {service_id: id},
                                                        dataType: "JSON",
                                                        success: function (data) {
                                                           $('#myModal .modal-body').html(data);
                                                        }
                                                    });

                                               
                                                 
                                                 $('#myModal .modal-footer').hide();                                               
                                                 $("#myModal").modal('show');
                                                 
                                              
                                            }
											
											
										




</script>


<script>
//card number validation 
$(document).ready(function() {
    $('#pay-new-booking').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            customer_card_number: {
                validators: {
                    creditCard: {
                        message: 'The credit card number is not valid'
                    }
                }
            }
        }
    }).on('success.validator.fv', function(e, data) {
            if (data.field === 'customer_card_number' && data.validator === 'creditCard') {
                valid = true;
            }
        }).on('err.field.fv', function(e, data) {
            if (data.field === 'customer_card_number') {
              valid = false;
            }
        });
		
		$('#payment-form').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            card_number: {
                validators: {
                    creditCard: {
                        message: 'The credit card number is not valid'
                    }
                }
            }
        }
    }).on('success.validator.fv', function(e, data) {
            if (data.field === 'card_number' && data.validator === 'creditCard') {
                card_number_valid = true;
            }
        }).on('err.field.fv', function(e, data) {
            if (data.field === 'card_number') {
              card_number_valid = false;
            }
        });
});
</script>


<?php 
  
function clearBookingSession(){
$expireAfter = 2 * 60;
//Check to see if our "last action" session
//variable has been set.
if(isset($_SESSION['last_action'])){
    //Figure out how many seconds have passed
    //since the user was last active.
    $secondsInactive = time() - $_SESSION['last_action'];
    //Convert our minutes into seconds.
    $expireAfterSeconds = $expireAfter * 60; 
    //Check to see if they have been inactive for too long.
    if($secondsInactive >= $expireAfterSeconds){
        //User has been inactive for too long.
        //Kill their session.
        unset($_SESSION['new_booking']);
    }
}
//Assign the current timestamp as the user's
//latest activity
$_SESSION['last_action'] = time();
}

?>
