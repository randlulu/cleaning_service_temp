<?php

//if ('212.118.23.131' == $_SERVER['REMOTE_ADDR']) {
//      error_reporting(E_ALL);
//	ini_set('display_errors', 'On');
//}

//set timezone = Australia/Sydney
date_default_timezone_set('Australia/Sydney');

$timezone = date('H') - gmdate('H');
if (date('d') != gmdate('d')) {
    $timezone = (date('H') + 24) - gmdate('H');
}
define('TIMEZONE', '+' . $timezone);

// Define path to application directory
defined('APPLICATION_PATH')
        || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));


/////////////////////////////////////////////////////
// Define path to WKPDF_PATH directory
defined('WKPDF_PATH')
        || define('WKPDF_PATH', realpath(dirname(__FILE__) .'/../public/WKPDF/'));

// Define path to PDF_FILE_PATH directory
defined('PDF_FILE_PATH')
        || define('PDF_FILE_PATH', realpath(dirname(__FILE__) .'/../public/WKPDF/pdf_files/'));
		
// Define path to PDF_FILE_PATH directory
defined('TEMP_WKPDF_PATH')
        || define('TEMP_WKPDF_PATH', realpath(dirname(__FILE__) .'/../public/WKPDF/tmp/'));
/////////////////////////////////////////////////////		

// Define path to uploads directory
defined('UPLOADS_PATH')
        || define('UPLOADS_PATH', realpath(dirname(__FILE__) . '/uploads'));

// Define path to Cron directory
defined('CRON_PATH')
        || define('CRON_PATH', realpath(dirname(__FILE__) . '/../cron'));

// Define application environment
defined('APPLICATION_ENV')
        || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// Define path to cache directory
defined('CACHE_PATH')
        || define('CACHE_PATH', realpath(dirname(__FILE__) . '/cache'));	
//defined('FONTS_PATH')
//        || define('FONTS_PATH', realpath(dirname(__FILE__) . '/cache'));	
		
// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
            realpath(APPLICATION_PATH . '/../library'), 
            get_include_path(),
        )));
		
/*/By Rand
defined('APPLICATION_NAME')
        || define('APPLICATION_NAME', 'Tile Cleaners App');
defined('CLIENT_SECRET_PATH')
        || define('CLIENT_SECRET_PATH', APPLICATION_PATH . '/client_secret.json');
/*defined('SCOPES')
        || define('SCOPES', implode(' ', array(
			  Google_Service_Calendar::CALENDAR)
			));*/
/** Zend_Application */
require_once 'Zend/Application.php';

// Create application, bootstrap, and run
$application = new Zend_Application(
                APPLICATION_ENV,
                APPLICATION_PATH . '/configs/application.ini'
);
if (!defined('DONT_RUN_APP') || DONT_RUN_APP == false) {
    $application->bootstrap()->run();
}


        
	   
	   
	   
	   
	   
	   