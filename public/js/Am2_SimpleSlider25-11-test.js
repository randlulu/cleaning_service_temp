﻿(function ($) {

    jQuery.fn.Am2_SimpleSlider = function () {
		if(!($('#image-modal').length))
		{
		
        $div1 = $('<div class="modal fade" id="image-modal"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button><h4 class="modal-title">Images</h4></div><div class="modal-body row"><img class="loading-pic" src="/pic/image-loader.gif"/><div class="product-image1 col-md-8"><a id="nav-btn-next" class="gallery-nav-btns nav-btn next" style="display: none;"></a> <a id="nav-btn-prev" class="nav-btn prev gallery-nav-btns" style="display: none;"></a></div> <div class="product-information1 col-md-4"><p class="product-desc"></p></div></div></div></div></div>').appendTo('body');
		}
		
		
		var current;
		var currentScale = 1;
		var zoomDelta = 2;
		var degree = 0;
		var startX, startY,SliderTimer, isDown = false;
		$('#image-modal').on('hidden.bs.modal', function () {
          clearInterval(SliderTimer);
        });
		
		
        
        //on image click
	
        $(this).find('.viewImage , .viewImageTab').click(function () {
            //$('.product-gallery-popup').fadeIn(500);
            //$('.modal').fadeIn(500);
            //$('body').css({ 'overflow': 'hidden' });
			
			
			clickedObject = $(this);
			
			$Current = $(this).parents('li');
			current = $(this).parents('li');
			
			
            $PreviousElm = $(this).parents('li').prev();
            $nextElm = $(this).parents('li').next();
            
			
			$('#image-modal').on('show.bs.modal', function () {
			
			//$('#image-modal .modal-body .product-image1 img').css('max-width', '100%' );

			
			$(this).find('.modal-content , .modal-dialog').css({
              width:'auto' , //probably not needed
			  margin:'10px'
            });
          });
		  
		    console.log($(this).parents('li').find('img').attr('original'));
		    console.log($(this).closest( "li" ).find('img').attr('original'));
		    console.log($(this).parents('li').attr('number'));
		    console.log($(this).parents('li').attr('count'));
		  
			$('#image-modal').modal('show');
			$('#image-modal .modal-title ').html('Photo '+ $(this).parents('li').attr('number')+'/'+$(this).parents('li').attr('count'));
            if($('#gallery-img')) $('#gallery-img').remove();
			var img = $('<img id="gallery-img" src="'+$(this).closest('li').find('img').attr('original')+'" alt="" "/>');
			if(!($('.loading-pic').length > 0 ))$('#image-modal .modal-body').prepend('<img class="loading-pic" src="/pic/image-loader.gif"/>');
			if($('.image-icons')) $('.image-icons').remove();
			if($('canvas')) $('canvas').remove();
			$('.product-image1').css('background','none');		
            $('.gallery-nav-btns').hide();
            $('#image-modal .modal-body .product-information1 p').html('');
			var OriginalImage = $(this);
			clearInterval(SliderTimer);
			var loaded = 0 ; 
			img.load(function() {
				$('.loading-pic').remove();
				$('#image-modal .modal-body .product-image1').append('<canvas id="canvas" width="'+OriginalImage.parents('li').find('img').attr('imagewidth')+'" height="'+OriginalImage.parents('li').find('img').attr('imageheight')+'" ></canvas>');
				$('#image-modal .modal-body .product-image1').prepend(img);	
				FirstRotate();
                loaded = 1;	
				if (!($nextElm.length === 0) && loaded == 1){
				 SliderTimer = setInterval(SliderClick, 10000);
				}else{
				 SliderTimer = 0;
				}	
			$('.product-image1').css('background','#F8F9FA');	
            $('#image-modal .modal-body .product-information1 p').html(OriginalImage.parents('li').find('.content').html());
			if($('#image_num').length){ $('#image_num').remove(); }
            $('#image-modal .modal-body .product-information1').prepend('<input type="hidden" value = "'+OriginalImage.parents('li').index()+'" id="image_num"/>');
			if ($PreviousElm.length === 0) { $('.nav-btn.prev').css({ 'display': 'none' }); }
            else { $('.nav-btn.prev').css({ 'display': 'block' }); }
            if ($nextElm.length === 0) { $('.nav-btn.next').css({ 'display': 'none' }); }
            else { $('.nav-btn.next').css({ 'display': 'block' }); }
							
			});	
            $('#image-modal .modal-body .product-image1 ').css('max-height', $(this).parents('li').find('img').attr('imageheight'));
            
			
            
            
			
			
			
        });
		
		
		$("#image-modal").hover(function () {
		       if(SliderTimer){
			    console.log('inside if');
                clearInterval(SliderTimer);
			 }	
            }, function () {
			  if(SliderTimer){
                SliderTimer = setInterval(SliderClick, 10000);
			 }	
            });
		
		
		 function SliderClick() {		 
			  ForSliding();
        }
		
		
		
		 function ForSliding(){
		 
		   
		
		    if(current.next().length == 0){
			  current = $('#productContent li').eq(0);
			
			 }else{
			  current = current.next();
			 
			 }
			 
			$PreviousElm = current.prev();
            $nextElm = current.next();
			$('#image-modal .modal-title ').html('Photo '+ current.attr('number')+'/'+current.attr('count'));
		    $('#image-modal .modal-body .product-image1 img').clearQueue().animate({ opacity: '0' }, 0);
			$('#image-modal .modal-body .product-image1 img').remove();
			$('#image-modal .modal-body .product-image1 ').css('max-height', current.find('img').attr('height'));
			if(!($('.loading-pic').length > 0 ))$('#image-modal .modal-body ').prepend('<img class="loading-pic" src="/pic/image-loader.gif"/>'); 
			 if($('.image-icons')) $('.image-icons').remove();
			 if($('canvas')) $('canvas').remove();
			 $('.product-image1').css('background','none');
             $('.gallery-nav-btns').hide();
             $('#image-modal .modal-body .product-information1 p').html('');
			 var img = $('<img id="gallery-img" src="'+current.find('img').attr('original')+'" alt=""  style="opacity:0;"/>');
			 clearInterval(SliderTimer);
			 var loaded = 0 ; 
				img.load(function() {
					$('.loading-pic').remove();
					$('#image-modal .modal-body .product-image1').prepend(img);	
					$('#image-modal .modal-body .product-image1').append('<canvas id="canvas" width="'+current.find('img').attr('imagewidth')+'" height="'+current.find('img').attr('imageheight')+'"></canvas>');					
					FirstRotate();	

                    loaded = 1;	
					if (loaded == 1 && SliderTimer){
					 console.log('loaded');
					 SliderTimer = setInterval(SliderClick, 10000);
					}
					if($('#image_num').length){ $('#image_num').remove(); }
                    $('#image-modal .modal-body .product-information1').prepend('<input type="hidden" value = "'+current.index()+'" id="image_num"/>');		
			        $('#image-modal .modal-body .product-information1 p').html(current.find('.content').html());
					if ($PreviousElm.length === 0) { $('.nav-btn.prev').css({ 'display': 'none' }); }
					else { $('.nav-btn.prev').css({ 'display': 'block' }); }
					if ($nextElm.length === 0) { $('.nav-btn.next').css({ 'display': 'none' }); }
					else { $('.nav-btn.next').css({ 'display': 'block' }); }
					$('.product-image1').css('background','#F8F9FA');
				});
			$('#image-modal .modal-body .product-image1 img').animate({ opacity: '1' }, 500);
            
			 
		}
		
	//on Next click
        $('.nav-btn.next').click(function () {
            $NewCurrent = $nextElm;
            $PreviousElm = $NewCurrent.prev();
            $nextElm = $NewCurrent.next();
			

			$('#image-modal .modal-title ').html('Photo '+ $NewCurrent.attr('number')+'/'+$NewCurrent.attr('count'));
            $('#image-modal .modal-body .product-image1 img').clearQueue().animate({ opacity: '0' }, 0);
			$('#image-modal .modal-body .product-image1 img').remove();
			$('#image-modal .modal-body .product-image1 ').css('max-height', $NewCurrent.find('img').attr('imageheight'));
			if(!($('.loading-pic').length > 0 ))$('#image-modal .modal-body').prepend('<img class="loading-pic" src="/pic/image-loader.gif"/>');
			if($('.image-icons')) $('.image-icons').remove();
			if($('canvas')) $('canvas').remove();
			$('.product-image1').css('background','none');		
            $('.gallery-nav-btns').hide();
            $('#image-modal .modal-body .product-information1 p').html('');
			
			var img = $('<img id="gallery-img" src="'+$NewCurrent.find('img').attr('original')+'" alt=""  style="opacity:0;"/>');
			img.load(function() {
				$('.loading-pic').remove();
				$('#image-modal .modal-body .product-image1').prepend(img);	
				$('#image-modal .modal-body .product-image1').append('<canvas id="canvas" width="'+$NewCurrent.find('img').attr('imagewidth')+'" height="'+$NewCurrent.find('img').attr('imageheight')+'"></canvas>');
				FirstRotate();	
                if ($PreviousElm.length === 0) { $('.nav-btn.prev').css({ 'display': 'none' }); }
				else { $('.nav-btn.prev').css({ 'display': 'block' }); }
				if ($nextElm.length === 0) { $('.nav-btn.next').css({ 'display': 'none' }); }
				else { $('.nav-btn.next').css({ 'display': 'block' }); }	
                $('.product-image1').css('background','#F8F9FA');				
			});
			$('#image-modal .modal-body .product-image1 img').animate({ opacity: '1' }, 500);
            if($('#image_num').length){ $('#image_num').remove(); }
            $('#image-modal .modal-body .product-information1').prepend('<input type="hidden" value = "'+$NewCurrent.index()+'" id="image_num"/>');
			
          
            
            $('#image-modal .modal-body .product-information1 p').html($NewCurrent.find('.content').html());
            
        });
		
		
		
		//on Prev click
        $('.prev').click(function () {
            $NewCurrent = $PreviousElm;
            $PreviousElm = $NewCurrent.prev();
            $nextElm = $NewCurrent.next();

			
			$('#image-modal .modal-title ').html('Photo '+ $NewCurrent.attr('number')+'/'+$NewCurrent.attr('count'));
			$('#image-modal .modal-body .product-image1 img').clearQueue().animate({ opacity: '0' }, 0);
			$('#image-modal .modal-body .product-image1 img').remove();
			$('#image-modal .modal-body .product-image1 ').css('max-height', $NewCurrent.find('img').attr('imageheight'));
			if($('.image-icons')) $('.image-icons').remove();
			if(!($('.loading-pic').length > 0 ))$('#image-modal .modal-body ').prepend('<img class="loading-pic" src="/pic/image-loader.gif"/>');
			if($('canvas')) $('canvas').remove();
			var img = $('<img id="gallery-img" src="'+$NewCurrent.find('img').attr('original')+'" alt=""  style="opacity:0;"/>');
			$('.product-image1').css('background','none');		
            $('.gallery-nav-btns').hide();
            $('#image-modal .modal-body .product-information1 p').html('');
			img.load(function() {
				$('.loading-pic').remove();
				$('#image-modal .modal-body .product-image1').prepend(img);	
				$('#image-modal .modal-body .product-image1').append('<canvas id="canvas" width="'+$NewCurrent.find('img').attr('imagewidth')+'" height="'+$NewCurrent.find('img').attr('imageheight')+'"></canvas>');
				FirstRotate();		
                if ($PreviousElm.length === 0) { $('.nav-btn.prev').css({ 'display': 'none' }); }
				else { $('.nav-btn.prev').css({ 'display': 'block' }); }
				if ($nextElm.length === 0) { $('.nav-btn.next').css({ 'display': 'none' }); }
				else { $('.nav-btn.next').css({ 'display': 'block' }); }	
                $('.product-image1').css('background','#F8F9FA');				
			});
			$('#image-modal .modal-body .product-image1 img').animate({ opacity: '1' }, 500);
			
			if($('#image_num').length){ $('#image_num').remove(); }
            $('#image-modal .modal-body .product-information1').prepend('<input type="hidden" value = "'+$NewCurrent.index()+'" id="image_num"/>');
			
            $('#image-modal .modal-body .product-information1 p').html($NewCurrent.find('.content').html());
            
        });
		
		
		$('#image-modal').on('click','#zoomIn',function(){
	     currentScale += zoomDelta;
		 img = document.getElementById('gallery-img');
	     canvas = document.getElementById('canvas');
		 rotateImage(img);
	  });
	  
	  $('#image-modal').on('click','#zoomOut',function(){
	     if(currentScale > 1){
		 currentScale -= zoomDelta;
		 var img = document.getElementById('gallery-img');
	     canvas = document.getElementById('canvas');
		 var size = getCanvasSize(degree,img);
			width = size.cw;
			height = size.ch;
			canvas.setAttribute('width', width);
			canvas.setAttribute('height', height);
		    rotateImage(img);
		 }
	  });
	  
	 
	   $('#image-modal').on('click','#rotateImage',function(){
         var img = document.getElementById('gallery-img');
	     canvas = document.getElementById('canvas');
          if(degree == 360){
		    degree = 0;
		  }
         degree += 90;
		 var size = getCanvasSize(degree,img);
			width = size.cw;
			height = size.ch;
			canvas.setAttribute('width', width);
			canvas.setAttribute('height', height);
    	 rotateImage(img);
	  });
	  
	  $('#image-modal').on('click','#undorotateImage',function(){
         img = document.getElementById('gallery-img');
	     canvas = document.getElementById('canvas');
		 if(degree == 0){
		  degree = 360;
		 }
		 degree -= 90;
		 var size = getCanvasSize(degree,img);
		 width = size.cw;
		 height = size.ch;
		 canvas.setAttribute('width', width);
		 canvas.setAttribute('height', height);
    	 rotateImage(img);
	  });
		
        //Close Popup
        $('.cross,.popup-overlay').click(function () {
            $('.product-gallery-popup').fadeOut(500);
            $('body').css({ 'overflow': 'initial' });
        });

        //Key Events
        $(document).on('keyup', function (e) {
            e.preventDefault();
            //Close popup on esc
            if (e.keyCode === 27) { $('.product-gallery-popup').fadeOut(500); $('body').css({ 'overflow': 'initial' }); }
            //Next Img On Right Arrow Click
            if (e.keyCode === 39) { NextProduct(); }
            //Prev Img on Left Arrow Click
            if (e.keyCode === 37) { PrevProduct(); }
        });

        function NextProduct() {
            if ($nextElm.length === 1) {
                $NewCurrent = $nextElm;
                $PreviousElm = $NewCurrent.prev();
                $nextElm = $NewCurrent.next();

                 $('#image-modal .modal-title ').html('Photo '+ $NewCurrent.attr('number')+'/'+$NewCurrent.attr('count'));
				 $('#image-modal .modal-body .product-image1 img').clearQueue().animate({ opacity: '0' }, 0);
			     $('#image-modal .modal-body .product-image1 img').remove();
				 //$('#image-modal .modal-body .product-image1 img').css('max-width', '100%' );
				 $('#image-modal .modal-body .product-image1 ').css('max-height', $NewCurrent.find('img').attr('imageheight'));
			     if($('.image-icons')) $('.image-icons').remove();
				 if($('canvas')) $('canvas').remove();
				 if(!($('.loading-pic').length > 0 ))$('#image-modal .modal-body ').prepend('<img class="loading-pic" src="/pic/image-loader.gif"/>');
				  var img = $('<img id="gallery-img" src="'+$NewCurrent.find('img').attr('original')+'" alt=""  style="opacity:0;"/>');
				  $('.product-image1').css('background','none');		
                  $('.gallery-nav-btns').hide();
                  $('#image-modal .modal-body .product-information1 p').html('');
			     img.load(function() {
				 $('.loading-pic').remove();
				 $('#image-modal .modal-body .product-image1').prepend(img);	
				 $('#image-modal .modal-body .product-image1').append('<canvas id="canvas" width="'+$NewCurrent.find('img').attr('imagewidth')+'" height="'+$NewCurrent.find('img').attr('imageheight')+'"></canvas>');
				  FirstRotate();	
                  if ($PreviousElm.length === 0) { $('.nav-btn.prev').css({ 'display': 'none' }); }
                else { $('.nav-btn.prev').css({ 'display': 'block' }); }
                if ($nextElm.length === 0) { $('.nav-btn.next').css({ 'display': 'none' }); }
                else { $('.nav-btn.next').css({ 'display': 'block' }); }
                $('.product-image1').css('background','#F8F9FA');					
			     });
			     $('#image-modal .modal-body .product-image1 img').animate({ opacity: '1' }, 500);
				
				if($('#image_num').length){ $('#image_num').remove(); }
                $('#image-modal .modal-body .product-information1').prepend('<input type="hidden" value = "'+$NewCurrent.index()+'" id="image_num"/>');
				
                $('#image-modal .modal-body .product-information1 p').html($NewCurrent.find('.content').html());
                
            }

        }

        function PrevProduct() {
            if ($PreviousElm.length === 1) {
                $NewCurrent = $PreviousElm;
                $PreviousElm = $NewCurrent.prev();
                $nextElm = $NewCurrent.next();
                $('#image-modal .modal-body .product-image1 img').clearQueue().animate({ opacity: '0' }, 0).attr('src', $NewCurrent.find('img').attr('original')).animate({ opacity: '1' }, 500);
				
			
                $('#image-modal .modal-title ').html('Photo '+ $NewCurrent.attr('number')+'/'+$NewCurrent.attr('count'));
				if($('#image_num').length){ $('#image_num').remove(); }
                //$('#image-modal .modal-body .product-information1').prepend('<input type="hidden" value = "'+$NewCurrent.index()+'" id="image_num"/>');
				 $('#image-modal .modal-body .product-image1 img').clearQueue().animate({ opacity: '0' }, 0);
				 $('#image-modal .modal-body .product-image1 ').css('max-height', $NewCurrent.find('img').attr('imageheight'));
				 
			     $('#image-modal .modal-body .product-image1 img').remove();
			      if($('#gallery-img')) $('#gallery-img').remove();
				 if($('.image-icons')) $('.image-icons').remove();
				 if($('canvas')) $('canvas').remove();
				 $('.product-image1').css('background','none');		
                 $('.gallery-nav-btns').hide();
                 $('#image-modal .modal-body .product-information1 p').html('');
				 if(!($('.loading-pic').length > 0 ))$('#image-modal .modal-body').prepend('<img class="loading-pic" src="/pic/image-loader.gif"/>');
				 var img = $('<img id="gallery-img" src="'+$NewCurrent.find('img').attr('original')+'" alt=""  style="opacity:0;"/>');
				img.load(function() {
					$('.loading-pic').remove();
					$('#image-modal .modal-body .product-image1').prepend(img);	
					$('#image-modal .modal-body .product-image1').append('<canvas id="canvas" width="'+$NewCurrent.find('img').attr('imagewidth')+'" height="'+$NewCurrent.find('img').attr('imageheight')+'"></canvas>');
					FirstRotate();	
                    $('#image-modal .modal-body .product-information1 p').html($NewCurrent.find('.content').html());
					if ($PreviousElm.length === 0) { $('.nav-btn.prev').css({ 'display': 'none' }); }
					else { $('.nav-btn.prev').css({ 'display': 'block' }); }
					if ($nextElm.length === 0) { $('.nav-btn.next').css({ 'display': 'none' }); }
					else { $('.nav-btn.next').css({ 'display': 'block' }); }	
                    $('.product-image1').css('background','#F8F9FA');						
				});
			     $('#image-modal .modal-body .product-image1 img').animate({ opacity: '1' }, 500);
		        
				
                
            }

        }
		
		function getCanvasSize(degree,img){		  
		var cw = img.width, ch = img.height;
		 switch(degree){
            case 90:
                cw = img.height;
                ch = img.width;
                break;
            case 270:
                cw = img.height;
                ch = img.width;
                break;
           }
		   
		   console.log(cw);
		   console.log(ch);
	      
		  return {
				cw: cw,
				ch: ch
			};		
		}
    
	function FirstRotate(){		  
		
		    img = document.getElementById('gallery-img');
			canvas = document.getElementById('canvas');
			var cContext = canvas.getContext('2d');
			currentScale = 1;
			zoomDelta = 0.1;
			degree = 0;
			isDown = false;
			startX = 0;
			startY = 0;
			
			$('#image-modal .product-image1').append('<div class="image-icons" style="width:'+img.width+';margin:auto;max-width:90%"><a href="javascript:;" prev ="" next= "" class="rotate270" id="undorotateImage" style="margin-left:10px"><i class="fa fa-undo"></i></a><a href="javascript:;" class="rotate90" id="rotateImage" style="margin-left:10px" prev ="" next= ""><i class="fa fa-repeat"></i></a> <a href="javascript:;" id="zoomIn" style="margin-left:10px" prev ="" next= ""><i class="fa fa-search-plus fa-4"></i></a><a href="javascript:;" id="zoomOut" style="margin-left:10px" prev ="" next= ""><i class="fa fa-search-minus fa-4" ></i></a></div>');
			
			
			 if(!canvas || !canvas.getContext){
			   canvas.parentNode.removeChild(canvas);
		   } else {
			   img.style.position = 'absolute';
			   img.style.visibility = 'hidden';
		   }
		   
		   canvas.onmousedown = function (e) {
			var pos = getMousePos(canvas, e);
			startX = pos.x;
			startY = pos.y;
			isDown = true;
		}

		canvas.onmousemove = function (e) {
			if (isDown === true) {
				var pos = getMousePos(canvas, e);
				var x = pos.x;
				var y = pos.y;	
                cContext.clearRect(0, 0, cContext.canvas.width * currentScale, cContext.canvas.height * currentScale);
                cContext.translate(x - startX, y - startY);					
				rotateImage(img);
				startX = x;
				startY = y;
			}
		}
		canvas.onmouseup = function (e) {
			isDown = false;
		}
		
        if(isDown == false){		  
		  rotateImage(img);
		}
		
		}
		
		function getMousePos(canvas, evt) {
		var rect = canvas.getBoundingClientRect();
		return {
			x: evt.clientX - rect.left,
			y: evt.clientY - rect.top
		};
     }	
	 
	 
	 function rotateImage(img)
	{
	    
        if(document.getElementById('canvas')){
           var cContext = canvas.getContext('2d');
           var cw = img.width, ch = img.height, cx = 0, cy = 0;
           
           //   Calculate new canvas size and x/y coorditates for image
           switch(degree){
                case 90:
                    cw = img.height;
                    ch = img.width;
                    cy = img.height * (-1);
                    break;
                case 180:
                    cx = img.width * (-1);
                    cy = img.height * (-1);
                    break;
                case 270:
                    cw = img.height;
                    ch = img.width;
                    cx = img.width * (-1);
                    break;
           }


			cContext.clearRect(0, 0, cContext.canvas.width, cContext.canvas.height);
            cContext.save();		
			cContext.scale(currentScale, currentScale);
			cContext.rotate(degree * Math.PI / 180);
			cContext.drawImage(img, cx, cy);
			cContext.restore();
			
        } else {
            //  Use DXImageTransform.Microsoft.BasicImage filter for MSIE
            switch(degree){
                case 0: image.style.filter = 'progid:DXImageTransform.Microsoft.BasicImage(rotation=0)'; break;
                case 90: image.style.filter = 'progid:DXImageTransform.Microsoft.BasicImage(rotation=1)'; break;
                case 180: image.style.filter = 'progid:DXImageTransform.Microsoft.BasicImage(rotation=2)'; break;
                case 270: image.style.filter = 'progid:DXImageTransform.Microsoft.BasicImage(rotation=3)'; break;
            }
        }
	}
		
	};	
} (jQuery));