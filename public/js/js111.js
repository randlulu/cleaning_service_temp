//checked all checkboxs

$(document).ready(function(){
    $("#checkboxall").click(function(){
        var checked_status = this.checked;
        $("input[type='checkbox']").each(function(){
            this.checked = checked_status;
        });
    });
});

function alert_dialog_message(message,title){
        
    title = typeof(title) != 'undefined' ? title : '';
                    
    $('body').append(
        '<div id="dialog-message" title="'+title+'">'+
        '<p>'+message+'</p>'+
        '</div>');

    $("#dialog-message").dialog({
        resizable: false,
        modal: true,
        buttons: [{
            text: "Ok",
            click: function() {
                $(this).dialog("close");
            }
        }],
        close: function() {
            $(this).remove();
        }
    });
}

function confirm_message(message,confirm_return,title,dialog_button){
    
    title = typeof(title) != 'undefined' ? title : '';
    if(!(title == '')){
	  $("#confirm .modal-title").html(title);
	}	

    $("#confirm .modal-body").html(message);	
    $("#confirm").modal("show");
    
$('#okBtn').on('click', function(evt) {
	confirm_return(true);
  $("#confirm").modal("hide");
});	

}

//Show Popup Window to assigned height and width
function showPopupWindow(url,height,width,caption,get_parms) {
    //default caption
    caption = typeof(caption) != 'undefined' ? caption : "";
    get_parms = typeof(get_parms) != 'undefined' && get_parms ? "&"+get_parms : "";

    tb_show(caption, url+'?height='+height+'&width='+width+get_parms);
}

function showPopupWindowWithGoTo(url,go_to,height,width,caption,get_parms) {
    //default caption
    caption = typeof(caption) != 'undefined' ? caption : "";
    get_parms = typeof(get_parms) != 'undefined' && get_parms ? "&"+get_parms : "";
    tb_show(caption, url+'?height='+height+'&width='+width+get_parms,go_to);
}

function resize(){
    var frame = document.getElementById("right_side");
    var bodyheight = document.getElementById("body"); 
    var windowheight = window.innerheight;
    alert(bodyheight.style.height);
    if ( htmlheight < windowheight ) {
        document.body.style.height = windowheight + "px";
        frame.style.height = windowheight + "px"; 
    } else {
        document.body.style.height = htmlheight + "px";
        frame.style.height = htmlheight + "px"; 
    }
}

function do_submit(url,form,content){
	
    $('#button_cont').html('<img src="/pic/loading-4.gif">');
    $.ajax({
        type: "POST",
        url: url,
        data: $(form).serialize(),
        success: function(msg) {
		
            if(msg == 1) {
                window.location.reload();
            } else {
                $(content).html(msg);
            }
        }
    });
}

function do_upload(form_id,content){

    $(content).append('<iframe id="iframe_upload_target" name="iframe_upload_target" src="#" style="display: none; width:0; height:0; border:0px solid #fff;"></iframe>');

    $('#button_cont').html('<img src="/pic/loading-4.gif">');

    $('#'+form_id).attr('target', 'iframe_upload_target');
    $('iframe[name=iframe_upload_target]').load(function(){
        var ret = frames["iframe_upload_target"].document.getElementsByTagName("body")[0].innerHTML;

        if(ret == '1'){
            window.location.reload();
        } else {
            var ret_2 = $(this).contents().text();
            if(ret_2 == '1'){
                window.location.reload();
            } else {
                $(content).html(ret);
            }
        }
    });
}

function submit_label(url,form){
    $('#button_cont').html('<img src="/pic/loading-4.gif">');
    $.ajax({
        type: "POST",
        url: url,
        data: $(form).serialize(),
        dataType : "json",
        success: function(data) {
            $('#label_'+data.entityId).html('');
            if(data.result){
                $.each(data.labels, function(index , label) {
                    $('#label_'+data.entityId).append('<div class="label_box">'+label+'</div>');
                });
            }
           $('#popupi').modal('hide');
        }
    });
}

function filter_label(url,form){
    $('#button_cont').html('<img src="/pic/loading-4.gif">');
    $.ajax({
        type: "POST",
        url: url,
        data: $(form).serialize(),
        success: function(response_url) {
            window.location = response_url;
        }
    });
} 
 
//function that will display the address
function getAddress(customer_id,container_id,url){
    if($(customer_id).val()){
        $(container_id).css('display','block');
        $(container_id).html('<img src="/pic/loading-4.gif">'); 
        $.ajax({
            type: "POST",
            url: url,
            data: "customer_id="+$(customer_id).val(),
            success: function(msg) {
                $(container_id).html(msg);
            }
        });
    }else{
        $(container_id).css('display','none');
    }
}

$(function(){
    // BUTTON
    $('.fg-button').hover(
        function(){
            $(this).removeClass('ui-state-default').addClass('ui-state-focus');
        },
        function(){
            $(this).removeClass('ui-state-focus').addClass('ui-state-default');
        }
        );
});

function confirmDelete(url,message){

    message = typeof(message) != 'undefined' ? message : "Are you sure you want to delete this ?";

    if(confirm(message)){
        window.location = url;
    }
}



function select_dashboard_status(){
    var dashboard_status = $('#dashboard_status').val();
    if(dashboard_status){
        $.ajax({
            type: "POST",
            url: "/ajax/setDashboardStatusSession",
            data: "dashboard_status="+dashboard_status,
            success: function(result) {
                if(result == '1'){
                    window.location.reload();
                }
            }
        });
    }
}


function save_as_xls(){
var html = $('#data_table').html();
	

    $('body').append(
        '<form id="save_as_xls" style="display: none;">'+
        '<input name="html1"/>'+
		'<input name="html2"/>'+
		'<input name="html3"/>'+
		'<input name="html4"/>'+
        '</form>'
        );

    $('#save_as_xls').append($('#search_form').html());
    $('#save_as_xls').attr('action',"/ajax/saveAsXls");
    $('#save_as_xls').attr('method',"POST");
	
	var encodeHtml=encodeURIComponent(html);
	var middle=(encodeHtml.length/4);
	var s1 = encodeHtml.substr(0, middle);
	var s2 = encodeHtml.substr(middle + 1, middle);
	var s3 = encodeHtml.substr(2*middle+ 1, middle);
	var s4 = encodeHtml.substr(3*middle+ 1);
	
	$('input[name=html1]').val(s1);
	$('input[name=html2]').val(s2);
	$('input[name=html3]').val(s3);
	$('input[name=html4]').val(s4);
	$('#save_as_xls').submit();
}

function setMessage(message,type){
    $.ajax({
        type: "POST",
        url: "/common/setMessage",
        data: "message="+message+"&type="+type,
        success: function(msg) {
            return true;
        }
    });
}

/*
(function( $ ) {
    $.widget( "ui.combobox", {
        _create: function() {
            var self = this,
            select = this.element.hide(),
            selected = select.children( ":selected" ),
            value = selected.val() ? selected.text() : "";
            var input = this.input = $( "<input>" )
            .insertAfter( select )
            .val( value )
            .autocomplete({
                delay: 0,
                minLength: 0,
                source: function( request, response ) {
                    var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
                    response( select.children( "option" ).map(function() {
                        var text = $( this ).text();
                        if ( this.value && ( !request.term || matcher.test(text) ) )
                            return {
                                label: text.replace(
                                    new RegExp(
                                        "(?![^&;]+;)(?!<[^<>]*)(" +
                                        $.ui.autocomplete.escapeRegex(request.term) +
                                        ")(?![^<>]*>)(?![^&;]+;)", "gi"
                                        ), "<strong>$1</strong>" ),
                                value: text,
                                option: this
                            };
                    }) );
                },
                select: function( event, ui ) {
                    ui.item.option.selected = true;
                    self._trigger( "selected", event, {
                        item: ui.item.option
                    });
                },
                change: function( event, ui ) {
                    if ( !ui.item ) {
                        var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( $(this).val() ) + "$", "i" ),
                        valid = false;
                        select.children( "option" ).each(function() {
                            if ( $( this ).text().match( matcher ) ) {
                                this.selected = valid = true;
                                return false;
                            }
                        });
                        if ( !valid ) {
                            // remove invalid value, as it didn't match anything
                            $( this ).val( "" );
                            select.val( "" );
                            input.data( "autocomplete" ).term = "";
                            return false;
                        }
                    }
                }
            })
            .addClass( "ui-widget ui-widget-content ui-corner-left" );

            input.data( "autocomplete" )._renderItem = function( ul, item ) {
                return $( "<li></li>" )
                .data( "item.autocomplete", item )
                .append( "<a>" + item.label + "</a>" )
                .appendTo( ul );
            };

            this.button = $( "<button type='button'>&nbsp;</button>" )
            .attr( "tabIndex", -1 )
            .attr( "title", "Show All Items" )
            .insertAfter( input )
            .button({
                icons: {
                    primary: "ui-icon-triangle-1-s"
                },
                text: false
            })
            .removeClass( "ui-corner-all" )
            .addClass( "ui-corner-right ui-button-icon" )
            .click(function() {
                // close if already visible
                if ( input.autocomplete( "widget" ).is( ":visible" ) ) {
                    input.autocomplete( "close" );
                    return;
                }

                // work around a bug (likely same cause as #5265)
                $( this ).blur();

                // pass empty string as value to search for, displaying all results
                input.autocomplete( "search", "" );
                input.focus();
            });
        },

        destroy: function() {
            this.input.remove();
            this.button.remove();
            this.element.show();
            $.Widget.prototype.destroy.call( this );
        }
    });
})( jQuery ); */

//add Customer
function AddEditCustomer(url) {
    $(document).ready(function() {
        popupi('Add Customer',url+'?asJson=1');
    });
}


// customer search
function customer_search(url){
    $(document).ready(function() {
        popupi('customer_search',url);
    });
}



//editcustomer
function editCustomer(){
		var customer_id = $('#customer_id').val();
		
		if(customer_id){
			var url = '/customer/edit/'+customer_id;
			popupi('Edit cutomer',url+'?asJson=1');
    }
}

function showEditCustomer(){
    var customer_id = $('#customer_id').val();
    if(customer_id){
        $("#edit_customer").show();
    } else {
        $("#edit_customer").hide();
    }
}

function getRelatedAddress(query_url){

 
    query_url = typeof(query_url) != 'undefined' ? query_url : '';

    $('#related_address').show();
    $('#related_address').html('<img src="/pic/loading-4.gif">');
    $.ajax({
        type: "POST",
        url: "/ajax/getRelatedAddress",
        data: $("#fmEdit").serialize()+query_url,
        success: function(result) {
            $('#content_modal_popup').html(result);
        }
    });
}


function submit_reminder(url,form_id,type){

    type = typeof(type) != 'undefined' ? type : 'inquiry';
    var name = (type == 'inquiry') ? 'Add Note' : 'Confirm Booking';

    $('#button_cont').html('<img src="/pic/loading-4.gif">');
	
    $.ajax({
        type: "POST",
        url: url,
        data: $(form_id).serialize(),
        dataType: "json",
        success: function(data) {
            if ($('#reminder_'+data.id).length) {
                $('#reminder_'+data.id).html('');
                $.each(data.reminders, function(index,value) {
                    $('#reminder_'+data.id).append('<div style="font-size: 12px;">'+
                        '<a href="javascript:;" style="color:black;" data-toggle="modal" data-target="#popupi" onclick="popupi(\''+name+'\',\''+value.url+ '\');" class="details_info_opt">' +value.reminder+'</a> '+
                        '<a href="javascript:;" onclick="delete_reminder(\''+value.remove_url+'\',\''+type+'\');"><img height="10" width="10" src="/pic/remove.png" border="0" ></a>'+
                        '</div>'
                        );
                });
                $('#reminder_'+data.id).append(' <div style = "font-size: 12px;">'+
                    '<a href="javascript:;" data-toggle="modal" data-target="#popupi"  onclick="popupi(\''+name+'\',\''+ data.add_url+'\');" class="details_info_opt">'+name+'</a>'+
                    '</div>');
                	//console.log("salim quta");
						$("#popupi").modal('hide');
						
            } else {
                window.location.reload();
            }
        }
    });
}

function submit_contact_history(url,form_id){
    $('#button_cont').html('<img src="/pic/loading-4.gif">');
    $.ajax({
        type: "POST",
        url: url,
        data: $(form_id).serialize(),
        dataType: "json",
        success: function(data) {
            if ($('#contact_history_'+data.id).length) {
                $('#contact_history_'+data.id).html('');
                $.each(data.reminders, function(index,value) {
                    $('#contact_history_'+data.id).append('<div style="font-size: 12px;">'+
                        '<a href="javascript:;" style="color:black;" data-toggle="modal" data-target="#popupi" onclick="popupi(\'Add Note\',\''+value.url+ '\');" class="details_info_opt">' +value.reminder+'</a> '+
                        '<a href="javascript:;" onclick="delete_contact_history(\''+value.remove_url+'\');"><img height="10" width="10" src="/pic/remove.png" border="0" ></a>'+
                        '</div>'
                        );
                });
                $('#contact_history_'+data.id).append(' <div style = "font-size: 12px;">'+
                    '<a href="javascript:;" data-toggle="modal" data-target="#popupi" onclick="popupi(\'Add Note\',\''+ data.add_url+'\');" class="details_info_opt">Add Note</a>'+
                    '</div>');
                $('#popupi').modal('hide');
            } else {
                window.location.reload();
            }
        }
    });
}

function delete_contact_history(url){
    if(confirm('Are you sure you want to delete this Booking ?')){
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            success: function(data) {
                if ($('#contact_history_'+data.id).length) {
                    $('#contact_history_'+data.id).html('');
                    $.each(data.reminders, function(index,value) {
                        $('#contact_history_'+data.id).append('<div style="font-size: 12px;">'+
                            '<a href="javascript:;" style="color:black;" data-toggle="modal" data-target="#popupi" onclick="popupi(\'Add Note\',\''+value.url+ '\');" class="details_info_opt">' +value.reminder+'</a> '+
                            '<a href="javascript:;"  onclick="delete_contact_history(\''+value.remove_url +'\');"><img height="10" width="10" src="/pic/remove.png" border="0" ></a>'+
                            '</div>');
                    });
                    $('#contact_history_'+data.id).append(' <div style = "font-size: 12px;">'+
                        '<a href="javascript:;" data-toggle="modal" data-target="#popupi" onclick="popupi(\'Add Note\',\''+ data.add_url+'\');" class="details_info_opt">Add Note</a>'+
                        '</div>');
					
                   // tb_remove();
                } else {
                    window.location.reload();
                }
            }
        });
    }
}

function delete_reminder(url,type){

    type = typeof(type) != 'undefined' ? type : 'inquiry';
    var name = (type == 'inquiry') ? 'Add Note' : 'Confirm Booking';

    if(confirm('Are you sure you want to delete this '+name+' ?')){
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            success: function(data) {
                if ($('#reminder_'+data.id).length) {
                    $('#reminder_'+data.id).html('');
                    $.each(data.reminders, function(index,value) {
                        $('#reminder_'+data.id).append('<div style="font-size: 12px;">'+
                            '<a href="javascript:;" style="color:black;"  data-toggle="modal" data-target="#popupi" onclick="popupi(\''+name+'\',\''+value.url+ '\');" class="details_info_opt">' +value.reminder+'</a> '+
                            '<a href="javascript:;" onclick="delete_reminder(\''+value.remove_url +'\',\''+type+'\');"><img height="10" width="10" src="/pic/remove.png" border="0" ></a>'+
                            '</div>');
                    });
					
                    $('#reminder_'+data.id).append(' <div style = "font-size: 12px;">'+
                        '<a href="javascript:;" data-toggle="modal" data-target="#popupi" onclick="popupi(\''+name+'\',\''+ data.add_url+'\');" class="details_info_opt">'+name+'</a>'+
                        '</div>');
                    tb_remove();
                } else {
                    window.location.reload();
                }
            }
        });
    }
}
 
function NearestBooking(){

    var booking_id = $('#booking_id').val();
    var city_id = $('#city_id').val();
    var stpartdate = $('#stpartdate').val();
    var stparttime = $('#stparttime').val();

    var unit_lot_number = encodeURIComponent($('[name=unit_lot_number]').val());
    var street_number = encodeURIComponent($('[name=street_number]').val());
    var street_address= encodeURIComponent($('[name=street_address]').val());
    var suburb = encodeURIComponent($('[name=suburb]').val());
    var postcode = encodeURIComponent($('[name=postcode]').val());
    var state = encodeURIComponent($('[name=state]').val());

    var url = '/ajax/getRelatedAddress/?height=500&width=900';

    if(booking_id){
        url = url+'&booking_id='+booking_id;
    }
    if(city_id){
        url = url+'&city_id='+city_id;
    }
    if(stpartdate){
        url = url+'&stpartdate='+stpartdate;
    }
    if(stparttime){
        url = url+'&stparttime='+stparttime;
    }
    if(unit_lot_number){
        url = url+'&unit_lot_number='+unit_lot_number;
    }
    if(street_number){
        url = url+'&street_number='+street_number;
    }
    if(street_address){
        url = url+'&street_address='+street_address;
    }
    if(suburb){
        url = url+'&suburb='+suburb;
    }
    if(postcode){
        url = url+'&postcode='+postcode;
    }
    if(state){
        url = url+'&state='+state;
    }
 
    tb_show("Nearest Bookings", url);
}

function find_availability(){

    var url = '/ajax/findAvailability/?height=500&width=1000';

    var booking_id = $('#booking_id').val();
    var city_id = $('#city_id').val();
    var stpartdate = $('#stpartdate').val();

    $('[name=services\\[\\]]').each(function(index,element) {
        url = url+'&services[]='+$(element).val();
    });

    if(booking_id){
        url = url+'&booking_id='+booking_id;
    }
    if(city_id){
        url = url+'&city_id='+city_id;
    }
    if(stpartdate){
        url = url+'&stpartdate='+stpartdate;
    }

   ajaxpage(url, 'availability_modal');
	
	//tb_show("Find Availability", url);
}

function toggle_right_side(){
    $('.right_side').toggle();

    var width = parseInt($('.right_side').css('width')) + 2;

    if($('.right_side').css('display') == 'none'){
        $('#toggle_right_side').css({
            right: '0px'
        });
    } else {
        $('#toggle_right_side').css({
            right: width+'px'
        });
    }
}

function fltr(url,id){
    id = $('#'+id).val();
    window.location = url+id;
}

