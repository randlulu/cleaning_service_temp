// JavaScript Document
 function doSubmit(url){
        $('#button_cont').html('<img src="/pic/loading-4.gif">');
        $.ajax({
            type: "POST",
            url: url,
            data: $('#customer_form').serialize(),
            success: function(data) {
                try {
                    var json = eval('('+data+')');
                    if(json.asJson == '1'){
                        $('#customer_id').val(json.customer_id);
                        $(".ui-autocomplete-input").val(json.customer_name);
                        
                        //tb_remove
                        try {
                            if(json.asBookingAddress == '1'){
                                //get Booking Address if in Booking
                                try{
                                    getBookingAddress();
                                } catch (e) {}
                            
                                //get Inquiry Address if in Inquiry
                                try{
                                    getInquiryAddress();
                                } catch (e) {}
                            }
                            
                            showEditCustomer();
                            tb_remove();
                        } catch (e) {
                            tb_remove();
                        }
                        
                    }else {
                        window.location.reload();
                    }
                } catch (e) {
                    $('#TB_ajaxContent').html(data);
                }
            }
        });
        
    }
   
       function getCustomerContact(){
        contact_counter = $('#contact_counter').val();
        $('#more_contact').html('<img src="/pic/loading-4.gif">');
        $('#contact_label_block').css('display','block');
        $.ajax({
            type: "POST",
            url:  "<?php echo $this->url(array(),'getCustomerContact'); ?>",
            data: "contact_counter="+contact_counter,
            success: function(data) {
                $('#contact_label_block').append(data);
                $('#contact_counter').val(contact_counter);
                $('#more_contact').html('<a href="javascript:;" id="more_contact_link" onclick="getCustomerContact()">More Contact</a>');
            }
        });
        contact_counter++;
    }
    
    function getState(){
        if($('#customer_country_id').val()){
            $('#customer_state_block').css('display','table-row');
            $('#customer_state_cont').html('<img src="/pic/loading-4.gif">');
            $.ajax({
                type: "POST",
                url:  "<?php echo $this->url(array(), 'dropDownState'); ?>",
                data: "country_id="+$('#customer_country_id').val()+"&name=customer_state",
                success: function(msg) {
                    $('#customer_state_block').css('display','table-row');
                    $('#customer_state_cont').html(msg);
                }
            });
        }else{
            $('#customer_state_block').css('display','none');
            $('#customer_city_block').css('display','none');
        }
    }
    
    function getCities(){
        if($('#customer_state').val()){
            $('#customer_city_block').css('display','table-row');
            $('#customer_city_cont').html('<img src="/pic/loading-4.gif">');
            $.ajax({
                type: "POST",
                url:  "<?php echo $this->url(array(), 'dropDownCity'); ?>",
                data: "country_id="+$('#customer_country_id').val()+"&state="+$('#customer_state').val()+"&name=customer_city_id",
                success: function(msg) {
                    $('#customer_city_block').css('display','table-row');
                    $('#customer_city_cont').html(msg);
                }
            });
        }else{
            $('#customer_city_block').css('display','none');
        }
    }


  
    function customerSearch(){
        var emailQuery = $('#email1').val();
        var mobileQuery = $('#mobile1').val();
        var phoneQuery = $('#phone1').val();
        var asjson = $('#asJson').val();
        if (emailQuery.length >2 || mobileQuery.length >2 || phoneQuery.length >2 ){
            $.ajax({
                type: "POST",
                url: "<?php echo $this->escape($this->url(array(), 'customerLike')); ?>",
                data: "customer-email="+$('#email1').val()+"&customer-mobile="+$('#mobile1').val()+"&customer-phone="+$('#phone1').val()+"&asJson="+asjson,
                success: function(msg) {
                    $('#customerResult').html(msg);
                }
            });
        }else{
            $('#customerResult').html('');
        }
    }
	
	
	 