  var hours = new Array();
  var unavailiable_starts = new Array(); 
  var serviceTime = 0;
  var divHeight = 0;
  var booking_date;
  var today;
  $(document).ready(function(){
    
	var mY = 0;
	var clicking = false;
 $('#availability').on('mousedown touchstart','.service-time',function(e){
 
    clicking = true;    
    //console.log(clicking);	
	
 });
 
  $('#availability').on('mousemove','.service-time',function(e){
    
    //console.log('web');
    if(clicking == false) return;
	var direction = '';
	
    if (e.pageY < mY) {
        direction = 'From Bottom';
    } else {
        direction = 'From Top';
    }
	// alert(direction);
	 //alert(e.pageY);

	 
	//console.log(direction);
	//console.log(e);
	
	mY = e.pageY;
	serviceBoxMovement(e,$('.calendar-times'),direction,'web');	
 });

 $('#availability').on('touchmove','.service-time',function(e){
 
    //console.log('mobile');
	event.preventDefault();
	console.log(clicking);
    if(clicking == false) return;
	var direction = '';
	
	console.log('position:'+e.originalEvent.touches[0].pageY);
	console.log('mY:'+mY);
	
    if (e.originalEvent.touches[0].pageY < mY) {
        direction = 'From Bottom';
    } else {
        direction = 'From Top';
    }
	
	// alert(direction);
	 //alert(e.pageY);

	 
	//console.log(direction);
	//console.log(e);
	//console.log(e.currentTarget.offsetTop);
	
	mY = e.originalEvent.touches[0].pageY;
	serviceBoxMovement(e,$('.calendar-times'),direction,'mobile');	
 });

 $('#availability').on('mouseup','.service-time',function(e){
 	getBookingStartAndEnd();
	clicking = false;
	//console.log('web_up');
 });
 
 $('#availability').on('touchend','.service-time',function(e){
 	getBookingStartAndEnd();
 });
 
  $('#availability').on('mouseover','.service-time',function(){
	$(this).css('background-color','#2958C0'); 
  });
  
  $('#availability').on('mouseout','.service-time',function(){
	$(this).css('background-color','#0db7ee'); 
	clicking = false;
  });	
	
	
 /*$('#availability').on('click','.service-time',function(){

    //$(".service-time").animate({"top": ($('.calendar-times').scrollTop()) + "px"},0);	
    
    var data = $._data( $('#availability .calendar-times')[0], 'events' );
	if(data['mousemove'] ==  undefined ){
	  $('.calendar-times').mousemove(function (e) { 
		  serviceBoxMovement(e,$('.calendar-times'));

		});
	}else{
	  $('.calendar-times').unbind('mousemove');
	  getBookingStartAndEnd();
	}
	
});*/


 
   
  });
  
  
  function getCalendarContent(month,year){
  
    $('#availability .box-content').html('<div class="loading"><img src="pic/loading-4.gif"></div>');
	$.ajax({
                type: "POST",
				data:{'year':year,'month':month},
                url: 'webservice/get-calendar-content',
                success: function (msg) {
                    $('#availability .box-content').html(msg);	
                     //$('#availability').find('.calendar-times').scroll( scrollingFunction );					
                    getHoursOfDay();
					var selected_date = $('#selected_date').val();
					if(selected_date){
					  var cdate = selected_date;
					}else{
					  	var currentDate = new Date();
						var day = currentDate.getDate();
						var month = currentDate.getMonth() + 1;
						var year = currentDate.getFullYear();
						if(month < 10){
							month = '0' + month;
						}
						if(day < 10){
							day = '0' + day;
						}
						var cdate = year + '-' + month + '-' + day;
						cdate = cdate.trim(); 
					}
					
					//console.log(cdate);
					$('body').find('.dates li').each(function( index ) {
						var currentListItemdate = ($(this).attr('date-target')).trim();
						console.log(currentListItemdate);
						console.log(cdate);
						
						if(currentListItemdate == cdate){ 
						    console.log('here..');
							$(this).click();								
							//$(this).touchstart();								
						}
					});
                    //drawUnavailableTime();
					

                    //getTopPostion();					
                }
            });
  }
 
function scrollingFunction( event ) {
    $(".service-time").animate({"top": ($('.calendar-times').scrollTop()) + "px"},0);	
}
 
 function getHoursOfDay(){
    for (var i =0 ; i<24 ; i++){	
	var hour = i + ' AM';
	 if (i >= 12){
	   hour = i -12;
	   if( hour == 0 ){
	     hour = '12';
	   }
	   hour = hour + ' PM';       
	 }
	 
	 if( i == 0){
	  hour = '12 AM';
	 }
	 hours[i] = hour
	 $('.calendar-times ul').append('<li>'+hour+'</li>');	 
	}
	
	if($('input[name^="services"]').val()){
	serviceTime = 0;  
	$('input[name^="services"]').each(function() {
	     var service = $(this).val();
		 var service_id = service;
		 
		if(service.indexOf("_") > -1){
		  service_id = service.split("_")[0];
		  
		}
		serviceTime += parseInt($('#estimate_hours_'+service_id).val());
		
	});
	
	console.log(serviceTime);
	
	}else{
	  serviceTime = $('#booking_time').val();
	}
	divHeight = $(".calendar-times ul li").outerHeight() * serviceTime;
	$('.calendar-times').append('<div class="service-time" style="height:'+divHeight+';top:0px"></div>');

	

	
  }
  
  

 
 
 $.fn.isBound = function(type, fn) {
    var data = this.data('events')[type];

    if (data === undefined || data.length === 0) {
        return false;
    }

    return (-1 !== $.inArray(fn, data));
};


function serviceBoxMovement(e,obj,direction,type){
   var collision_flg = 0 ;
   var y = 0;
   var top = 0;
   if(type == 'mobile'){
     y =  e.originalEvent.touches[0].pageY - obj.offset().top;
   }else{
    y = e.pageY - obj.offset().top;
   }
   
   //console.log('type'+type);
   //console.log('y:'+y);
    if((y) >= 15){
	  $('.unavailable-time').each(function(){
	   
		if(collision($(".service-time"),$(this))){
		 console.log('here in if');
		 var top = getTopPostion(direction);
		
		 $(".service-time").css('top',top);
		 collision_flg = 1;		 
		}
	  });
	  
	  if(!collision_flg){
      var calendar_fullHeight = $('.calendar-times')[0].scrollHeight;
	  top = y + $('.calendar-times').scrollTop() - 15;
	  //console.log('top:'+top);
     if((top + divHeight) >= calendar_fullHeight ){
		 top = $('.service-time').position().top + $('.calendar-times').scrollTop();
		 $(".service-time").css('top',top);
	  }else{
	    //console.log('add');
	    $('.service-time').animate({
			'top': top  + 'px'
		}, 0);
	  } 
 
		
	  }
	}

}
  
 /*$('.calendar-times').mousemove(function (e) {
    serviceBoxMovement(e,$('.calendar-times'));

});*/

function moveUp(){
  var top = 0;
  var calendar_fullHeight = $('.calendar-times')[0].scrollHeight;
  var unavailable_size =  $('.unavailable-time').length;
  unavailable_size = parseInt(unavailable_size);
   if(unavailable_size > 1){
  for(var i = unavailable_size-1 ; i > 0 ; i--){
     //console.log(i);
     currentElement = $('.unavailable-time').eq(i);
	 //console.log(currentElement.position().top);
     prevElement = $('.unavailable-time').eq(i -1);
     space = (currentElement.position().top ) - (prevElement.position().top + prevElement.outerHeight(true));
	 if (space < divHeight){
	   nextElementTop = prevElement.position().top + $('.calendar-times').scrollTop() - divHeight;
	   top = nextElementTop -10;
	   //console.log(top);
	 }else{
	    nextElementTop = currentElement.position().top + $('.calendar-times').scrollTop() - divHeight;
		top = nextElementTop - 10; 
	 }
	 
	 if((calendar_fullHeight - top + 10) < divHeight ){
		 top = $('.calendar-times').scrollTop();
	  }
   }
  }else{
    
	nextElementTop = $(this).position().top + $('.calendar-times').scrollTop() - divHeight;
    top = nextElementTop - 10; 
  
  }
  
  return top;
}

function moveDown(){

 var calendar_fullHeight = $('.calendar-times')[0].scrollHeight; 
 var top = 0; 
 var unavailable_size =  $('.unavailable-time').length;
 unavailable_size = parseInt(unavailable_size);
 if(unavailable_size > 1){
  for (var i = 0; i < unavailable_size -1 ; i++){
    //console.log('infor');
     currentElement = $('.unavailable-time').eq(i);
     nextElement = $('.unavailable-time').eq(i +1);
     space = (nextElement.position().top ) - (currentElement.position().top + currentElement.outerHeight(true));
	 if (space < divHeight){
	    nextElementTop = nextElement.position().top + $('.calendar-times').scrollTop() + nextElement.outerHeight(true);
		top = nextElementTop + 10;
	 }else{
	   //console.log('in else');
	    nextElementTop = currentElement.position().top + $('.calendar-times').scrollTop() + currentElement.outerHeight(true);
	    top = nextElementTop +10; 
	 }
	 
	
	 
	 if((calendar_fullHeight - top - 10) < divHeight ){
		 top = $('.calendar-times').scrollTop();
	  }
	  
	
  }
  }else{
  
   nextElementTop = $(this).position().top + $('.calendar-times').scrollTop() + divHeight;
   top = nextElementTop + 10; 
  }
  
  //console.log(top);
  return top;

}

 function getTopPostion(direction){
  var top = 0;
  if(direction == 'From Bottom'){
      
	top =  moveUp();
    
  }else{
  top =  moveDown();
  }
  
  if(top > 807){
   top = 807;
  }
 
 /*
 
 $('.unavailable-time').each(function(index, el){
    
	 unavailable_length = $('.unavailable-time').length;
	 
	 last_element = parseInt(unavailable_length) - 1;
	   if(index != last_element) {
        nexElement = $('.unavailable-time').eq(index + 1);
		space = (nexElement.position().top ) - ($(this).position().top + $(this).outerHeight(true));
		top = $(this).position().top + $('.calendar-times').scrollTop() + $(this).outerHeight(true) + 10;
        if (space < divHeight){
		  
		  if(direction == 'From Bottom'){
		     nextElementTop = nexElement.position().top + $('.calendar-times').scrollTop() - divHeight;
			 
		     top = nextElementTop -10;
		  }else if(direction == 'From Top'){
		     nextElementTop = nexElement.position().top + $('.calendar-times').scrollTop() + nexElement.outerHeight(true);
		     top = nextElementTop + 10;
		  }
		 
		  
		}else{
		   if(direction == 'From Bottom'){
		     nextElementTop = $(this).position().top + $('.calendar-times').scrollTop() - divHeight;
		     top = nextElementTop - 10; 
		 
			}else{
			  nextElementTop = $(this).position().top + $('.calendar-times').scrollTop() + $(this).outerHeight(true);
			  top = nextElementTop +10; 
			  
			}
		
		}
		 
		calendar_fullHeight = $('.calendar-times')[0].scrollHeight;
		
		if((calendar_fullHeight - top) < divHeight ){
		 top = $('.calendar-times').scrollTop();
		 
		} 
		
    }else{
	    
	    
	    if(direction == 'From Bottom'){
		  nextElementTop = $(this).position().top + $('.calendar-times').scrollTop() - divHeight;
		  top = nextElementTop - 10; 
		 
		}else{
		  nextElementTop = $(this).position().top + $('.calendar-times').scrollTop() + $(this).outerHeight(true);
		  top = nextElementTop +10; 
		 
		}
	    calendar_fullHeight = $('.calendar-times')[0].scrollHeight;
		
		if((calendar_fullHeight - top) < divHeight ){
		 top = $('.calendar-times').scrollTop();
		
		}
		
	}
	
  });	
  
  //console.log(direction);*/
  return top;

}

 


function getBookingStartAndEnd(){

  var startTime  = $(".service-time").position().top +  $('.calendar-times').scrollTop() ;
  $('#top_position').val(startTime); 
  var serviceHeight = $(".calendar-times ul li").outerHeight();
  
   /*$('.unavailable-time').each(function(index, el){
		if(collision($(".service-time"),$(this))){
		 var top = getTopPostion();
		 $(".service-time").css('top',top);	 
		}
	  });*/
    
  startTime = startTime / serviceHeight ;
  decimal = Math.floor(startTime);
  var floatNum = startTime - decimal; 
  Hour = convertHour(hours[decimal]);
  min = Math.floor(floatNum * 60);
  min = (min < 10) ? '0'+min : min;
  // booking sart time
  startTime = Hour+':'+min;
  endHour = parseInt(Hour) + Math.floor(serviceTime);
  endMin = parseInt(min) + ( serviceTime - Math.floor(serviceTime)) * 60;
  endMin = Math.floor(endMin);
  endMin = (endMin < 10) ? '0'+endMin : endMin;
  if(endMin >= 60){
   endMin = (endMin == 60) ? 00 : endMin - 60;
   endHour = endHour +1;
  }
  console.log('5_'+endMin);
  // booking end time
  endTime = endHour+':'+endMin; 
  $('#booking_start').val(booking_date+' '+startTime);
  $('#booking_end').val(booking_date+' '+endTime);   
    
   
}

 function collision($div1, $div2) {
      var x1 = $div1.offset().left;
      var y1 = $div1.offset().top;
      var h1 = $div1.outerHeight(true);
      var w1 = $div1.outerWidth(true);
      var b1 = y1 + h1;
      var r1 = x1 + w1;
      var x2 = $div2.offset().left;
      var y2 = $div2.offset().top;
      var h2 = $div2.outerHeight(true);
      var w2 = $div2.outerWidth(true);
      var b2 = y2 + h2;
      var r2 = x2 + w2;
        
      if (b1 < y2 || y1 > b2 || r1 < x2 || x1 > r2) return false;
      return true;
    }

function convertHour(hourFromArray){
 
  hourArray = hourFromArray;
  hour = hourArray.split(" ");
  if(hour[1] == "PM" && parseInt(hour[0])<12) hour[0] = parseInt(hour[0])+12;
  if(hour[1] == "AM" && parseInt(hour[0])==12) hour[0] = parseInt(hour[0])-12;
  sHours = hour[0];
  if(parseInt(hour[0])<10) sHours = "0" + hour[0];
  return sHours;

}


function drawUnavailableTime(data){ 
var obj = data;
//var string = '{"result":[{"start_time":"04:32","end_time":"06:32"}]}';
//var obj = JSON.parse(string);

var serviceHeight = $(".calendar-times ul li").outerHeight();
$('.calendar-times .unavailable-time').remove();
 for(var i = 0 ; i<obj.result.length; i++){
    startTime = obj.result[i].start_time;
	startHour = startTime.split(":")[0];
	startMin = startTime.split(":")[1];  
    hourPoint = parseInt(startHour) * serviceHeight;
	minPoint = (startMin / 60) * serviceHeight;
	
    // top position of unavailable box
	startPoint =  hourPoint + minPoint;
	// height of unavailable box
	
	unavailiable_starts[i] = startPoint;
   	
	endTime = obj.result[i].end_time;
	endHour = endTime.split(":")[0];
	endMin = endTime.split(":")[1];
	
	var start_time_tosec = HMStoSec1(startTime);
	var end_time_tosec = HMStoSec1(endTime);
	var diff = end_time_tosec - start_time_tosec;
	var diff_hours = convertHours(diff);
	var diff_minutes = getRemainingMinutes(diff);
	diff_minutes = (diff_minutes == 60) ? "00" : diff_minutes;


	
	box_height = parseInt(diff_hours) * serviceHeight +  (parseInt(diff_minutes)/60) * serviceHeight;
	
	
	
	
	$('.calendar-times').append('<div class="unavailable-time" data-sort="'+Math.floor(startPoint)+'" style="height:'+Math.floor(box_height)+'; top:'+Math.floor(startPoint)+'"></div>');
	
 }
 
    var list = $('.calendar-times');
	var listItems = list.find('.unavailable-time').sort(function(a,b){ return $(a).attr('data-sort') - $(b).attr('data-sort'); });
	$('.calendar-times').find('.unavailable-time').remove();
	list.prepend(listItems);
   
   unavailiable_starts.sort();


}


var secondsPerMinute = 60;
var minutesPerHour = 60;

function convertSecondsToHHMM(intSecondsToConvert) {

var hours = convertHours(intSecondsToConvert);
var minutes = getRemainingMinutes(intSecondsToConvert);
minutes = (minutes == 60) ? "00" : minutes;
return hours+":"+minutes;
}

function convertHours(intSeconds) {
var minutes = convertMinutes(intSeconds);
var hours = Math.floor(minutes/60);
return hours;
}
function convertMinutes(intSeconds) {
return Math.floor(intSeconds/60);
}

function getRemainingMinutes(intSeconds) {
var intTotalMinutes = convertMinutes(intSeconds);
return (intTotalMinutes%60);
}

function HMStoSec1(T) { // h:m:s
 var A = T.split(/\D+/) ; return (A[0]*60 + +A[1])*60  
}


$('body').on('click touchstart','.dates li',function(){
	var clicked_date = $(this);
	today = $('.today').attr('date-target');
	booking_date = clicked_date.attr("date-target");
	if(booking_date != ''){
	if(!(booking_date < today)){
	
	if(!$(this).hasClass('mask')){	
     $('.dates li').removeClass('shadowAndBackground');
     $(this).addClass('shadowAndBackground');
   }
   //$('#booking_start').val('');
   //$('#booking_end').val('');
	
	current_date = getBookingDateFormating(booking_date,booking_date,0);
	//$('#current_date').html(current_date);
	//var sent_date = '2016-07-07';
	var services = new Array();
	var service_counter = 0;
	$('input[name^="services"]').each(function() {
	    service = $(this).val();
		if(!(service.indexOf("_") > -1)){
          services[service_counter] = service;
		  service_counter++;
		}
	});
	
	var postCode = $('#customer-postcode').val();
	
	$.ajax({
		type: "POST",
		url: 'webservice/get-contractors-with-unavailable-time',
		data:{'booking_date':booking_date, 'service_ids':services, 'postCode':postCode,'is_array':1},
		dataType :"JSON",
		success: function(data) {			
			drawUnavailableTime(data);	
            $('.loader').hide();
			var top_position = $('#top_position').val();
            $('.service-time').css('top',top_position);			
		}
	});
	
	booking_date = clicked_date.attr("date-target");
	
	}else{	  
	  $('#errorModal .modal-body').html('You cloudn\'t select past date for new booking');
	  $('#errorModal').modal('show');
	}
  }
});
	