// Custom scripts
$(document).ready(function () {

    // MetsiMenu
    $('#side-menu').metisMenu();

    // Collapse ibox function
    $('.collapse-link').click( function() {
        var ibox = $(this).closest('div.ibox');
        var button = $(this).find('i');
        var content = ibox.find('div.ibox-content');
        content.slideToggle(200);
        button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
        ibox.toggleClass('').toggleClass('border-bottom');
        setTimeout(function () {
            ibox.resize();
            ibox.find('[id^=map-]').resize();
        }, 50);
    });

    // Close ibox function
    $('.close-link').click( function() {
        var content = $(this).closest('div.ibox');
        content.remove();
    });

    // Small todo handler
    $('.check-link').click( function(){
        var button = $(this).find('i');
        var label = $(this).next('span');
        button.toggleClass('fa-check-square').toggleClass('fa-square-o');
        label.toggleClass('todo-completed');
        return false;
    });

    // Append config box / Only for demo purpose
  

    // minimalize menu
    $('.navbar-minimalize').click(function () {
        $("body").toggleClass("mini-navbar");
        SmoothlyMenu();
    })

    // tooltips
    $('.tooltip-demo').tooltip({
        selector: "[data-toggle=tooltip]",
        container: "body"
    })

    // Full height of sidebar
    function fix_height() {
        var heightWithoutNavbar = $("body > #wrapper").height() - 61;
        $(".sidebard-panel").css("min-height", heightWithoutNavbar + "px");
    }
    fix_height();

    // Fixed Sidebar
    // unComment this only whe you have a fixed-sidebar
            //    $(window).bind("load", function() {
            //        if($("body").hasClass('fixed-sidebar')) {
            //            $('.sidebar-collapse').slimScroll({
            //                height: 'auto',
            //                railOpacity: 0.9,
            //            });
            //        }
            //    })

    $(window).bind("load resize click scroll", function() {
        if(!$("body").hasClass('body-small')) {
            fix_height();
        }
    })

    $("[data-toggle=popover]")
        .popover();


});
 // for bootstrap modal popup add by Abdallah
 // change title and put png waiting 
  function pass_header_to_modal(header){

           $('#modal_title').html(header);
            $('#content_modal_popup').html('<img src="/pic/ajax-loader.gif"/>');
       }
	     function pass_header_to_modal2(header){

           $('#modal_title2').html(header);
            $('#content_modal_popup2').html('<img src="/pic/ajax-loader.gif"/>');
       }
	   
     // popup content 
   function popupi(title,url){
    pass_header_to_modal(title);
	$('#content_modal_popup').load(url);

    } 
	function fpopupi(title,url){
		
    pass_header_to_modal(title);
	$('#content_modal_popup').load(url);

    }
	
	  function popupi_with_parm(title,url,prameters){
		
        pass_header_to_modal(title);
        var get_parms = typeof(prameters) != 'undefined' && prameters ? "&"+prameters : "";
        $('#content_modal_popup').load(url,get_parms);
    }
		  function fn_popupi_with_parm(title,url,prameters){
		
        pass_header_to_modal2(title);
        var get_parms = typeof(prameters) != 'undefined' && prameters ? "&"+prameters : "";
        $('#content_modal_popup2').load(url,get_parms);
    }
	
    // nearest booking and find availability add by Abdallah 
 


 function NearestBooking2(title){
	  $('#popupi').addClass('n-width');
	   pass_header_to_modal(title);
	
    var booking_id = $('#booking_id').val();
    var city_id = $('#city_id').val();
    var stpartdate = $('#stpartdate').val();
    var stparttime = $('#stparttime').val();

    var unit_lot_number = encodeURIComponent($('[name=unit_lot_number]').val());
    var street_number = encodeURIComponent($('[name=street_number]').val());
    var street_address= encodeURIComponent($('[name=street_address]').val());
    var suburb = encodeURIComponent($('[name=suburb]').val());
    var postcode = encodeURIComponent($('[name=postcode]').val());
    var state = encodeURIComponent($('[name=state]').val());

    var url = '/ajax/getRelatedAddress/?height=500&width=900';

    if(booking_id){
        url = url+'&booking_id='+booking_id;
    }
    if(city_id){
        url = url+'&city_id='+city_id;
    }
    if(stpartdate){
        url = url+'&stpartdate='+stpartdate;
    }
    if(stparttime){
        url = url+'&stparttime='+stparttime;
    }
    if(unit_lot_number){
        url = url+'&unit_lot_number='+unit_lot_number;
    }
    if(street_number){
        url = url+'&street_number='+street_number;
    }
    if(street_address){
        url = url+'&street_address='+street_address;
    }
    if(suburb){
        url = url+'&suburb='+suburb;
    }
    if(postcode){
        url = url+'&postcode='+postcode;
    }
    if(state){
        url = url+'&state='+state;
    }
 
    $('#content_modal_popup').load(url);
	  
	  
	  
	  }
  function find_availability2(title){
	 $('#popupi').addClass('fa-width');
	    pass_header_to_modal(title);

	var url = '/ajax/findAvailability/';

    var booking_id = $('#booking_id').val();
    var city_id = $('#city_id').val();
    var stpartdate = $('#stpartdate').val();

    $('[name=services\\[\\]]').each(function(index,element) {
        url = url+'?services[]='+$(element).val();
    });

    if(booking_id){
        url = url+'?booking_id='+booking_id;
    }
    if(city_id){
        url = url+'?city_id='+city_id;
    }
    if(stpartdate){
        url = url+'?stpartdate='+stpartdate;
    }
		   
     $('#content_modal_popup').load(url);
		
  }
// For demo purpose - animation css script
function animationHover(element, animation){
    element = $(element);
    element.hover(
        function() {
            element.addClass('animated ' + animation);
        },
        function(){
            //wait for animation to finish before removing classes
            window.setTimeout( function(){
                element.removeClass('animated ' + animation);
            }, 2000);
        });
}

// Minimalize menu when screen is less than 768px
$(function() {
    $(window).bind("load resize", function() {
        if ($(this).width() < 769) {
            $('body').addClass('body-small')
        } else {
            $('body').removeClass('body-small')
        }
    })
})

function SmoothlyMenu() {
    if (!$('body').hasClass('mini-navbar') || $('body').hasClass('body-small')) {
        // Hide menu in order to smoothly turn on when maximize menu
        $('#side-menu').hide();
        // For smoothly turn on menu
        setTimeout(
            function () {
                $('#side-menu').fadeIn(500);
            }, 100);
    } else if ($('body').hasClass('fixed-sidebar')){
        $('#side-menu').hide();
        setTimeout(
            function () {
                $('#side-menu').fadeIn(500);
            }, 300);
    } else {
        // Remove all inline style from jquery fadeIn function to reset menu state
        $('#side-menu').removeAttr('style');
    }
}

// Dragable panels
function WinMove() {
    $("div.ibox").not('.no-drop')
        .draggable({
            revert: true,
            zIndex: 2000,
            cursor: "move",
            handle: '.ibox-title',
            opacity: 0.8,
            drag: function(){
                var finalOffset = $(this).offset();
                var finalxPos = finalOffset.left;
                var finalyPos = finalOffset.top;
                // Add div with above id to see position of panel
                $('#posX').text('Final X: ' + finalxPos);
                $('#posY').text('Final Y: ' + finalyPos);
            },
        })
        .droppable({
            tolerance: 'pointer',
            drop: function (event, ui) {
                var draggable = ui.draggable;
                var droppable = $(this);
                var dragPos = draggable.position();
                var dropPos = droppable.position();
                draggable.swap(droppable);
                setTimeout(function () {
                    var dropmap = droppable.find('[id^=map-]');
                    var dragmap = draggable.find('[id^=map-]');
                    if (dragmap.length > 0 || dropmap.length > 0) {
                        dragmap.resize();
                        dropmap.resize();
                    }
                    else {
                        draggable.resize();
                        droppable.resize();
                    }
                }, 50);
                setTimeout(function () {
                    draggable.find('[id^=map-]').resize();
                    droppable.find('[id^=map-]').resize();
                }, 250);
            }
        });
}
jQuery.fn.swap = function (b) {
    b = jQuery(b)[0];
    var a = this[0];
    var t = a.parentNode.insertBefore(document.createTextNode(''), a);
    b.parentNode.insertBefore(a, b);
    t.parentNode.insertBefore(b, t);
    t.parentNode.removeChild(t);
    return this;
};
