/**
 * The nav stuff
 */
 $(document).ready(function(e) {

	
	'use strict';

	var body = document.body;
	var mask=document.createElement("div");
			mask.className = "mask";
	
		
		
		var pushMenuLeft = document.querySelector( ".push-menu-left" );
		
		var activeNav;
	;



	/* push menu left */
	$('.toggle-push-left').click(function(){
		
		classie.add( body, "pml-open" );
		document.body.appendChild(mask);
		activeNav = "pml-open";
		
		});
	
		

	

	/* hide active menu if mask is clicked */
	mask.addEventListener( "click", function(){
		classie.remove( body, activeNav );
		activeNav = "";
		document.body.removeChild(mask);
	} );

	/* hide active menu if close menu button is clicked */
	[].slice.call(document.querySelectorAll(".close-menu")).forEach(function(el,i){
		el.addEventListener( "click", function(){
			classie.remove( body, activeNav );
			activeNav = "";
			document.body.removeChild(mask);
		} );
	});



	});