//checked all checkboxs

$(document).ready(function(){
   /* $("#checkboxall").click(function(){
        var checked_status = this.checked;
        $("input[type='checkbox']").each(function(){
            this.checked = checked_status;
        });
    });*/
	$("body").on("click", "#checkboxall", function(){
        var checked_status = this.checked;
        $("input[type='checkbox']").each(function(){
            this.checked = checked_status;
        });
    });
	
	
	$('.gallery-img').mouseenter(function(e) {
     $(this).find('.viewImage').addClass('over');
     $(this).find('.icons-left,.icons-right').show();

  });
  
  $('.gallery-img').mouseleave(function(e) {
    $(this).find('.viewImage').removeClass('over');
    $(this).find('.icons-left,.icons-right').hide();
  });
  
  $('body').on('mouseenter', '.product-image1',function(e) {
    $(this).find('.gallery-nav-btns').animate({ opacity: '1' }, 500);
  });
  
  $('body').on('mouseleave', '.product-image1',function(e) {
     $(this).find('.gallery-nav-btns').animate({ opacity: '0' }, 500);
  });
  
  
  function confirmImageDelete(deleteLink){
   var deletCommentFlag  = 0;
   
   if(!($('#deleteComment').length))
  {
        $div1 = $('<div id="deleteComment" class="modal fade"><div class="modal-dialog"><div class="modal-content"> <div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h4 class="modal-title">Confirmation Message</h4></div><div class="modal-body"> <p>Are you sure you want to continue?</p> </div><div class="modal-footer"><a type="button" class="btn btn-danger" id="okBtn">OK</a><a type="button" class="btn btn-default" data-dismiss="modal">Cancel</a></div></div></div></div>').appendTo('body');
  }
   
   $("#confirm .modal-title").html('Delete Image');
   $("#confirm .modal-body").html('Are you sure you want to delete this Image Attachment?'); 
   $('#confirm').modal('show');
   $('#confirm .modal-footer').show();
   
   $('body').on('click','#confirm #okBtn',function(){

     $comments = deleteLink.parents('.gallery-img').find('.discussions ul li').length;
  
        if($comments){
     $('#confirm').modal('hide');
     $('#deleteComment').modal('show');
     $("#deleteComment .modal-title").html('Delete Image Comments');
     $("#deleteComment .modal-body").html('Are you sure you want to delete this Image Comments?'); 
     $("#deleteComment a:eq(0)").text('Yes'); 
     $("#deleteComment a:eq(1)").text('No');
     var _href = deleteLink.attr("href");  
     $('#deleteComment a:eq(0)').attr("href", _href); 
     $('#deleteComment a:eq(1)').attr("href", _href); 
  }else{
    var _href = deleteLink.attr("href"); 
    window.location.href = _href;
  }
   });
   
   var deletCommentFlag = 'false';
   $('body').on('click','#deleteComment #okBtn',function(){
        var deletCommentFlag = 'true';
     $('#deleteComment').modal('hide');
        var _href = $(this).attr("href");  
     $(this).attr("href", _href + '?deleteComments='+deletCommentFlag); 
   });
   
   $('body').on('click','#deleteComment a:eq(1)',function(){
        var deletCommentFlag = 'false';
     $('#deleteComment').modal('hide');

   });

 
   return false;
 }
  
  
});

function alert_dialog_message(message,title){
        
    title = typeof(title) != 'undefined' ? title : '';
                    
    $('body').append(
        '<div id="dialog-message" title="'+title+'">'+
        '<p>'+message+'</p>'+
        '</div>');

    $("#dialog-message").dialog({
        resizable: false,
        modal: true,
        buttons: [{
            text: "Ok",
            click: function() {
                $(this).dialog("close");
            }
        }],
        close: function() {
            $(this).remove();
        }
    });
}

function confirm_message(message,confirm_return,title,dialog_button){
    console.log(title);
	console.log(message);
	
    title = typeof(title) != 'undefined' ? title : '';
    if(!(title == '')){
	  $("#confirm .modal-title").html(title);
	  $("#confirm #cancelBtn").html('No');
	  $("#confirm #okBtn").html('Yes');
	}	
	
    $("#confirm .modal-body").html(message);	
    $("#confirm").modal("show");
    
$('#okBtn').click(function(){
	
  $("#confirm").modal("hide");
});	
}

//Show Popup Window to assigned height and width
function showPopupWindow(url,height,width,caption,get_parms) {
    //default caption
    caption = typeof(caption) != 'undefined' ? caption : "";
    get_parms = typeof(get_parms) != 'undefined' && get_parms ? "&"+get_parms : "";

    tb_show(caption, url+'?height='+height+'&width='+width+get_parms);
}

function showPopupWindowWithGoTo(url,go_to,height,width,caption,get_parms) {
    //default caption
    caption = typeof(caption) != 'undefined' ? caption : "";
    get_parms = typeof(get_parms) != 'undefined' && get_parms ? "&"+get_parms : "";
    tb_show(caption, url+'?height='+height+'&width='+width+get_parms,go_to);
}

function resize(){
    var frame = document.getElementById("right_side");
    var bodyheight = document.getElementById("body"); 
    var windowheight = window.innerheight;
    alert(bodyheight.style.height);
    if ( htmlheight < windowheight ) {
        document.body.style.height = windowheight + "px";
        frame.style.height = windowheight + "px"; 
    } else {
        document.body.style.height = htmlheight + "px";
        frame.style.height = htmlheight + "px"; 
    }
}

function do_submit(url,form,content){
    $('#button_cont').html('<img src="/pic/loading-4.gif">');
    console.log("serialize " + $(form).serialize())
    $.ajax({
        type: "POST",
        url: url,
        data: $(form).serialize(),
        success: function(msg) {

            if(msg == 1) {
                window.location.reload();
            } else {
                $(content).html(msg);
            }
        }
    });
}

function do_upload(form_id,content){
	
    $(content).append('<iframe id="iframe_upload_target" name="iframe_upload_target" src="#" style="display: none; width:0; height:0; border:0px solid #fff;"></iframe>');

    $('#button_cont').html('<img src="/pic/loading-4.gif">');
	
    $('#'+form_id).attr('target', 'iframe_upload_target');
    $('iframe[name=iframe_upload_target]').load(function(){
        var ret = frames["iframe_upload_target"].document.getElementsByTagName("body")[0].innerHTML;

        if(ret == '1'){
			//alert('Pass');
            window.location.reload();
        } else {
            var ret_2 = $(this).contents().text();
            if(ret_2 == '1'){
				//alert('Error');
                window.location.reload();
            } else {
                $(content).html(ret);
            }
        }
    });
}

function submit_label(url,form){
    $('#button_cont').html('<img src="/pic/loading-4.gif">');
    $.ajax({
        type: "POST",
        url: url,
        data: $(form).serialize(),
        dataType : "json",
        success: function(data) {
            $('#label_'+data.entityId).html('');
            if(data.result){
                $.each(data.labels, function(index , label) {
                    $('#label_'+data.entityId).append('<li>'+label+'</li>');
                });
            }
           $('#popupi').modal('hide');
        }
    });
}

function filter_label(url,form){
    $('#button_cont').html('<img src="/pic/loading-4.gif">');
    $.ajax({
        type: "POST",
        url: url,
        data: $(form).serialize(),
        success: function(response_url) {
            window.location = response_url;
        }
    });
} 
 
//function that will display the address
function getAddress(customer_id,container_id,url){
    if($(customer_id).val()){
        $(container_id).css('display','block');
        $(container_id).html('<img src="/pic/loading-4.gif">'); 
        $.ajax({
            type: "POST",
            url: url,
            data: "customer_id="+$(customer_id).val(),
            success: function(msg) {
                $(container_id).html(msg);
            }
        });
    }else{
        $(container_id).css('display','none');
    }
}

$(function(){
    // BUTTON
    $('.fg-button').hover(
        function(){
            $(this).removeClass('ui-state-default').addClass('ui-state-focus');
        },
        function(){
            $(this).removeClass('ui-state-focus').addClass('ui-state-default');
        }
        );
});

function confirmDelete(url,message){

    message = typeof(message) != 'undefined' ? message : "Are you sure you want to delete this ?";

    if(confirm(message)){
        window.location = url;
    }
}



function select_dashboard_status(){
    var dashboard_status = $('#dashboard_status').val();
    if(dashboard_status){
        $.ajax({
            type: "POST",
            url: "/ajax/setDashboardStatusSession",
            data: "dashboard_status="+dashboard_status,
            success: function(result) {
                if(result == '1'){
                    window.location.reload();
                }
            }
        });
    }
}


function save_as_xls(){
var html = $('#data_table').html();
	

    $('body').append(
        '<form id="save_as_xls" style="display: none;">'+
        '<input name="html1"/>'+
		'<input name="html2"/>'+
		'<input name="html3"/>'+
		'<input name="html4"/>'+
        '</form>'
        );

    $('#save_as_xls').append($('#search_form').html());
    $('#save_as_xls').attr('action',"/ajax/saveAsXls");
    $('#save_as_xls').attr('method',"POST");
	
	var encodeHtml=encodeURIComponent(html);
	var middle=(encodeHtml.length/4);
	var s1 = encodeHtml.substr(0, middle);
	var s2 = encodeHtml.substr(middle + 1, middle);
	var s3 = encodeHtml.substr(2*middle+ 1, middle);
	var s4 = encodeHtml.substr(3*middle+ 1);
	
	$('input[name=html1]').val(s1);
	$('input[name=html2]').val(s2);
	$('input[name=html3]').val(s3);
	$('input[name=html4]').val(s4);
	$('#save_as_xls').submit();
}

function setMessage(message,type){
    $.ajax({
        type: "POST",
        url: "/common/setMessage",
        data: "message="+message+"&type="+type,
        success: function(msg) {
            return true;
        }
    });
}

/*
(function( $ ) {
    $.widget( "ui.combobox", {
        _create: function() {
            var self = this,
            select = this.element.hide(),
            selected = select.children( ":selected" ),
            value = selected.val() ? selected.text() : "";
            var input = this.input = $( "<input>" )
            .insertAfter( select )
            .val( value )
            .autocomplete({
                delay: 0,
                minLength: 0,
                source: function( request, response ) {
                    var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
                    response( select.children( "option" ).map(function() {
                        var text = $( this ).text();
                        if ( this.value && ( !request.term || matcher.test(text) ) )
                            return {
                                label: text.replace(
                                    new RegExp(
                                        "(?![^&;]+;)(?!<[^<>]*)(" +
                                        $.ui.autocomplete.escapeRegex(request.term) +
                                        ")(?![^<>]*>)(?![^&;]+;)", "gi"
                                        ), "<strong>$1</strong>" ),
                                value: text,
                                option: this
                            };
                    }) );
                },
                select: function( event, ui ) {
                    ui.item.option.selected = true;
                    self._trigger( "selected", event, {
                        item: ui.item.option
                    });
                },
                change: function( event, ui ) {
                    if ( !ui.item ) {
                        var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( $(this).val() ) + "$", "i" ),
                        valid = false;
                        select.children( "option" ).each(function() {
                            if ( $( this ).text().match( matcher ) ) {
                                this.selected = valid = true;
                                return false;
                            }
                        });
                        if ( !valid ) {
                            // remove invalid value, as it didn't match anything
                            $( this ).val( "" );
                            select.val( "" );
                            input.data( "autocomplete" ).term = "";
                            return false;
                        }
                    }
                }
            })
            .addClass( "ui-widget ui-widget-content ui-corner-left" );

            input.data( "autocomplete" )._renderItem = function( ul, item ) {
                return $( "<li></li>" )
                .data( "item.autocomplete", item )
                .append( "<a>" + item.label + "</a>" )
                .appendTo( ul );
            };

            this.button = $( "<button type='button'>&nbsp;</button>" )
            .attr( "tabIndex", -1 )
            .attr( "title", "Show All Items" )
            .insertAfter( input )
            .button({
                icons: {
                    primary: "ui-icon-triangle-1-s"
                },
                text: false
            })
            .removeClass( "ui-corner-all" )
            .addClass( "ui-corner-right ui-button-icon" )
            .click(function() {
                // close if already visible
                if ( input.autocomplete( "widget" ).is( ":visible" ) ) {
                    input.autocomplete( "close" );
                    return;
                }

                // work around a bug (likely same cause as #5265)
                $( this ).blur();

                // pass empty string as value to search for, displaying all results
                input.autocomplete( "search", "" );
                input.focus();
            });
        },

        destroy: function() {
            this.input.remove();
            this.button.remove();
            this.element.show();
            $.Widget.prototype.destroy.call( this );
        }
    });
})( jQuery ); */

//add Customer
function AddEditCustomer(url) {
    popupi('Add Customer',url+'?asJson=1'); 
	console.log('add customer');
}


// customer search
function customer_search(url){
    $(document).ready(function() {
        popupi('customer_search',url);
    });
}



//editcustomer
function editCustomer(){
		var customer_id = $('#customer_id').val();
		
		if(customer_id){
			var url = '/customer/edit/'+customer_id;
			popupi('Edit Customer',url+'?asJson=1');
    }
}

function showEditCustomer(){
    var customer_id = $('#customer_id').val();
    if(customer_id){
        $("#edit_customer").show();
    } else {
        $("#edit_customer").hide();
    }
}

function getRelatedAddress(query_url){

 
    query_url = typeof(query_url) != 'undefined' ? query_url : '';

    $('#related_address').show();
    $('#related_address').html('<img src="/pic/loading-4.gif">');
    $.ajax({
        type: "POST",
        url: "/ajax/getRelatedAddress",
        data: $("#fmEdit").serialize()+query_url,
        success: function(result) {
            $('#content_modal_popup').html(result);
        }
    });
}


function submit_reminder(url,form_id,type){

    type = typeof(type) != 'undefined' ? type : 'inquiry';
    var name = (type == 'inquiry') ? 'Add Note' : 'Confirm Booking';

    $('#button_cont').html('<img src="/pic/loading-4.gif">');
	
    $.ajax({
        type: "POST",
        url: url,
        data: $(form_id).serialize(),
        dataType: "json",
        success: function(data) {
            if ($('#reminder_'+data.id).length) {
                $('#reminder_'+data.id).html('');
                $.each(data.reminders, function(index,value) {
                    $('#reminder_'+data.id).append('<div >'+
                        '<a href="javascript:;" style="color:black;" data-toggle="modal" data-target="#popupi" onclick="popupi(\''+name+'\',\''+value.url+ '\');" class="details_info_opt"><i class="fa fa-thumbs-up ">&nbsp;</i>' +value.reminder+'</a> '+
                        '<a href="javascript:;" onclick="delete_reminder(\''+value.remove_url+'\',\''+type+'\');"><img height="10" width="10" src="/pic/remove.png" border="0" ></a>'+
                        '</div>'
                        );
                });
                $('#reminder_'+data.id).append(' <div>'+
                    '<a href="javascript:;" data-toggle="modal" data-target="#popupi"  onclick="popupi(\''+name+'\',\''+ data.add_url+'\');" class="details_info_opt btn btn-white"><i class="fa fa-thumbs-up ">&nbsp;</i>'+name+'</a>'+
                    '</div>');
                	//console.log("salim quta");
						$("#popupi").modal('hide');
						
            } else {
			                 
				if(name == 'Confirm Booking'){
				  window.location = window.location.href.split("?")[0]+'?type=confirmed';
				}else{
				  window.location.reload();
				} 
				
            }
        }
    });
}

function submit_contact_history(url,form_id){
    $('#button_cont').html('<img src="/pic/loading-4.gif">');
    $.ajax({
        type: "POST",
        url: url,
        data: $(form_id).serialize(),
        dataType: "json",
        success: function(data) {
            if ($('#contact_history_'+data.id).length) {
                $('#contact_history_'+data.id).html('');
                $.each(data.reminders, function(index,value) {
                    $('#contact_history_'+data.id).append('<div style="font-size: 12px;">'+
                        '<a href="javascript:;" style="color:black;" data-toggle="modal" data-target="#popupi" onclick="popupi(\'Add Note\',\''+value.url+ '\');" class="details_info_opt">' +value.reminder+'</a> '+
                        '<a href="javascript:;" onclick="delete_contact_history(\''+value.remove_url+'\');"><img height="10" width="10" src="/pic/remove.png" border="0" ></a>'+
                        '</div>'
                        );
                });
                $('#contact_history_'+data.id).append(' <div style = "font-size: 12px;">'+
                    '<a href="javascript:;" data-toggle="modal" data-target="#popupi" onclick="popupi(\'Add Note\',\''+ data.add_url+'\');" class="details_info_opt">Add Note</a>'+
                    '</div>');
                $('#popupi').modal('hide');
            } else {
                window.location.reload();
            }
        }
    });
}

function delete_contact_history(url){
    if(confirm('Are you sure you want to delete this Booking ?')){
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            success: function(data) {
                if ($('#contact_history_'+data.id).length) {
                    $('#contact_history_'+data.id).html('');
                    $.each(data.reminders, function(index,value) {
                        $('#contact_history_'+data.id).append('<div style="font-size: 12px;">'+
                            '<a href="javascript:;" style="color:black;" data-toggle="modal" data-target="#popupi" onclick="popupi(\'Add Note\',\''+value.url+ '\');" class="details_info_opt">' +value.reminder+'</a> '+
                            '<a href="javascript:;"  onclick="delete_contact_history(\''+value.remove_url +'\');"><img height="10" width="10" src="/pic/remove.png" border="0" ></a>'+
                            '</div>');
                    });
                    $('#contact_history_'+data.id).append(' <div style = "font-size: 12px;">'+
                        '<a href="javascript:;" data-toggle="modal" data-target="#popupi" onclick="popupi(\'Add Note\',\''+ data.add_url+'\');" class="details_info_opt">Add Note</a>'+
                        '</div>');
					
                   // tb_remove();
                } else {
                    window.location.reload();
                }
            }
        });
    }
}

function delete_reminder(url,type){

    type = typeof(type) != 'undefined' ? type : 'inquiry';
    var name = (type == 'inquiry') ? 'Add Note' : 'Confirm Booking';

    if(confirm('Are you sure you want to delete this '+name+' ?')){
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            success: function(data) {
                if ($('#reminder_'+data.id).length) {
                    $('#reminder_'+data.id).html('');
                    $.each(data.reminders, function(index,value) {
                        $('#reminder_'+data.id).append('<div>'+
                            '<a href="javascript:;" style="color:black;"  data-toggle="modal" data-target="#popupi" onclick="popupi(\''+name+'\',\''+value.url+ '\');" class="details_info_opt"><i class="fa fa-thumbs-up ">&nbsp;</i>' +value.reminder+'</a> '+
                            '<a href="javascript:;" onclick="delete_reminder(\''+value.remove_url +'\',\''+type+'\');"><img height="10" width="10" src="/pic/remove.png" border="0" ></a>'+
                            '</div>');
                    });
					
                    $('#reminder_'+data.id).append(' <div>'+
                        '<a href="javascript:;" data-toggle="modal" data-target="#popupi" onclick="popupi(\''+name+'\',\''+ data.add_url+'\');" class="details_info_opt btn btn-white"><i class="fa fa-thumbs-up">&nbsp;</i>'+name+'</a>'+
                        '</div>');
                    tb_remove();
                } else {
                    window.location.reload();
                }
            }
        });
    }
}
 
function NearestBooking(){

    var booking_id = $('#booking_id').val();
    var city_id = $('#city_id').val();
    var stpartdate = $('#stpartdate').val();
    var stparttime = $('#stparttime').val();

    var unit_lot_number = encodeURIComponent($('[name=unit_lot_number]').val());
    var street_number = encodeURIComponent($('[name=street_number]').val());
    var street_address= encodeURIComponent($('[name=street_address]').val());
    var suburb = encodeURIComponent($('[name=suburb]').val());
    var postcode = encodeURIComponent($('[name=postcode]').val());
    var state = encodeURIComponent($('[name=state]').val());

    var url = '/ajax/getRelatedAddress/?height=500&width=900';

    if(booking_id){
        url = url+'&booking_id='+booking_id;
    }
    if(city_id){
        url = url+'&city_id='+city_id;
    }
    if(stpartdate){
        url = url+'&stpartdate='+stpartdate;
    }
    if(stparttime){
        url = url+'&stparttime='+stparttime;
    }
    if(unit_lot_number){
        url = url+'&unit_lot_number='+unit_lot_number;
    }
    if(street_number){
        url = url+'&street_number='+street_number;
    }
    if(street_address){
        url = url+'&street_address='+street_address;
    }
    if(suburb){
        url = url+'&suburb='+suburb;
    }
    if(postcode){
        url = url+'&postcode='+postcode;
    }
    if(state){
        url = url+'&state='+state;
    }
 
    tb_show("Nearest Bookings", url);
}

function find_availability(){

    var url = '/ajax/findAvailability/?height=500&width=1000';

    var booking_id = $('#booking_id').val();
    var city_id = $('#city_id').val();
    var stpartdate = $('#stpartdate').val();

    $('[name=services\\[\\]]').each(function(index,element) {
        url = url+'&services[]='+$(element).val();
    });

    if(booking_id){
        url = url+'&booking_id='+booking_id;
    }
    if(city_id){
        url = url+'&city_id='+city_id;
    }
    if(stpartdate){
        url = url+'&stpartdate='+stpartdate;
    }

   ajaxpage(url, 'availability_modal');
	
	//tb_show("Find Availability", url);
}

function toggle_right_side(){
    $('.right_side').toggle();

    var width = parseInt($('.right_side').css('width')) + 2;

    if($('.right_side').css('display') == 'none'){
        $('#toggle_right_side').css({
            right: '0px'
        });
    } else {
        $('#toggle_right_side').css({
            right: width+'px'
        });
    }
}

function fltr(url,id){
    id = $('#'+id).val();
    window.location = url+id;
}

function fltr_page(url,id){
    id = $('#'+id).val();
    page = '&page_name='+$('select option:selected').text();
    window.location = url+id+page;
}

function fltr2(url,id1,id2){

    filter1_value = $('#'+id1).val();
    filter2_value = $('#'+id2).val();
    var filter = "";
    if(filter1_value && filter2_value){
        filter = filter + "fltr["+id1+"]="+filter1_value+"&fltr["+id2+"]="+filter2_value;
    }else{
        if(filter1_value){
            filter = filter + "fltr["+id1+"]="+filter1_value;
        }
        if(filter2_value){
            filter = filter + "fltr["+id2+"]="+filter2_value;
        }

    }

        if(!filter2_value && !filter1_value){
            filter = "";
        }
		
		console.log(url.indexOf("?"));
		
		if(url.indexOf("?") < 0 ){
		  window.location = url+'?'+filter;
		}else{
		  window.location = url+'&'+filter;
		}
    
        
   
}

function getMobileOperatingSystem() {
    var userAgent = navigator.userAgent || navigator.vendor || window.opera;

    if (/android/i.test(userAgent)) {
        window.location = "https://play.google.com/store/apps/details?id=au.tilecleaners.app";
    }
    else if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
        window.location = "https://itunes.apple.com/nz/app/tilecleaners/id921943286?mt=8";
    }
    else{
        window.close();
    }

}


function blockUser(url, user_id) {
    var confirmMsg = $('.blocked_'+user_id).text();
        if (confirm('Are you sure you want to' + confirmMsg + ' this user ?')) {
            $.ajax({
                url: url,
                dataType: "JSON",
                data: { user_id : user_id},
                success: function (result) {
                    $('.alert_box').html('<div class="alert alert-success alert-dismissible fade in col-md-12" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><span class="typeprice"> ' + result.msg + '</span></div>');
                    if(result.blocked){
                        $('.blocked_'+user_id).html('<span></span>');
                        $('.blocked_'+user_id).html('<span><i class="fa fa-minus-circle" aria-hidden="true"></i> Unblock</span>');

                    }
                    else{
                        $('.blocked_'+user_id).html('<span></span>');
                        $('.blocked_'+user_id).html('<span><i class="fa fa-minus-circle" aria-hidden="true"></i> Block</span>');
                    }
                }
            });
        }
    }

function toggle_blocked(url, user_id){
        $.ajax({
            url: url,
            dataType: "JSON",
            data: { user_id : user_id},
            success: function (result) {
                //alert('change success ');

            }
        });
    }
	
/*function dateFormat_js_to_js(date_format,date_value)
	{
		
		new_date = $.datepicker.formatDate(date_format , new Date(date_value));
		
		return new_date;
	}*/
	
	function dateFormat_js_to_js(date_format, date_value){
		/*new_date = $.datepicker.formatDate(date_format, new Date(date_value));
		return new_date;*/
		var finalDate = moment(new Date(date_value).getTime()).format(date_format);
		return finalDate;
	}

/*function timeFormat_js_to_Momentjs(format, time_value ,type = 'time'){//'HH:mm' format used previously in system
		if(type == 'time')
		{
			var timeParts = time_value.split(" ");
			if(timeParts.length > 2){
				timeParts.splice(timeParts.length - 1, 1);
				timeParts = timeParts.join(" ");
			}else
			{
				timeParts = time_value;
			}
			
		var date = new Date();
		var currentMonth = date.getMonth()+1;
        var currentDate = date.getDate();
        var currentYear = date.getFullYear();
		
		time_value = currentMonth + '/'  + currentDate + '/' + currentYear + ' ' + timeParts;
		}
		//console.log('new Date(time_value)' + time_value + ' ' + new Date(time_value));
		var finalDate = moment(new Date(time_value).getTime()).format(format);
		return finalDate;
	}*/ 
	
	function dateTimeFormat_js_to_Momentjs(format, value){
		var finalDate = moment(new Date(value).getTime()).format(format);
		return finalDate;
	}		
	
	function timeFormat_js_to_Momentjs(format, time_value, type){
		type = 'time';
		if(type == 'time'){
			var timeParts = time_value.split(" ");
			if(timeParts.length > 2){
				timeParts.splice(timeParts.length - 1, 1);
				timeParts = timeParts.join(" ");
			}else{
				timeParts = time_value;
			}
			var date = new Date();
			var currentMonth = date.getMonth() + 1;
			var currentDate = date.getDate();
			var currentYear = date.getFullYear();
			
			time_value = currentMonth + '/' + currentDate + '/' + currentYear + ' ' + timeParts;
		}
		var finalDate = moment(new Date(time_value).getTime()).format(format);
		return finalDate;
	}
	
	function adjust_time_format(time_value, time_zone_string){
		var check_seconds = time_value.split(":").length - 1;
		if(check_seconds == 2){
			var last_index_of_colon = time_value.lastIndexOf(":");
			time_value = time_value.substr(0, last_index_of_colon + 1) + '00' + time_value.substr(last_index_of_colon + 3);
		}
		time_value = time_value + '' + time_zone_string;
		return time_value;
	}
	
	/*function adjust_time_format(time_value,timeZone_string)
	{
		var check_seconds = (time_value.split(":").length - 1);
		if(check_seconds == 2){
			var last_index_of_colon = time_value.lastIndexOf(":");
			time_value = time_value.substr(0, last_index_of_colon + 1) + '00' + time_value.substr(last_index_of_colon + 3);
		}  
		
		time_value = time_value+''+timeZone_string;
		return time_value;
	}
	
	/*function adjust_time_format(time_value,timeZone_string)
	{
		 
		var timeFormatPart = time_value.split(" ");
		//var timeParts = timeFormatPart[timeIndex].split(":");//walaa
		var timeParts = timeFormatPart[0].split(":");//rand
        var new_value = time_value;
		if(timeParts.length == 3)
		{
			timeParts[2] = '00';
			var joinTimeParts = timeParts.join(":");
											
			//timeFormatPart.splice(timeIndex, 1);//walaa
				timeFormatPart.splice(0, 1);	//rand							
			var joinTimeFormatPart = timeFormatPart.join(" ");
			var newTime = joinTimeParts + " " + joinTimeFormatPart;
				//console.log("test: " + newTime);
			new_value = newTime;
													
		}
		newValue = new_value+''+timeZone_string;
		return newValue;
		
	}*/
