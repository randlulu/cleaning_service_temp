﻿(function ($) {

    jQuery.fn.Am2_SimpleSlider = function () {
		if(!($('#image-modal').length))
		{
		
        $div1 = $('<div class="modal fade" id="image-modal"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button><h4 class="modal-title">Images</h4></div><div class="modal-body"><div class="product-image1"><img id="gallery-img" src="" alt="" /> <div class="gallery-nav-btns"> <a id="nav-btn-next" class="nav-btn next" style="display: block;"></a> <a id="nav-btn-prev" class="nav-btn prev" style="display: none;"></a></div> </div> <div class="product-information1"><p class="product-desc"></p></div></div></div></div></div>').appendTo('body');
		}
		
		
		var current;
		$('#image-modal').on('hidden.bs.modal', function () {
          clearInterval(SliderTimer);
        });
		
		
        
        //on image click
	
        $(this).find('.viewImage').click(function () {
            //$('.product-gallery-popup').fadeIn(500);
            //$('.modal').fadeIn(500);
            //$('body').css({ 'overflow': 'hidden' });
			
			
			clickedObject = $(this);
			
			$('#image-modal').on('show.bs.modal', function () {
			
			$('#image-modal .modal-body .product-image1 img').css('max-width', '90%' );
			$('#image-modal .modal-body .product-image1 img').css('max-height', '70%' );

			
			$(this).find('.modal-content , .modal-dialog').css({
              width:'auto' , //probably not needed
			  margin:'10px'
            });
          });
		  
			$('#image-modal').modal('show');
			$('#image-modal .modal-title ').html('Photo '+ $(this).parents('li').attr('number')+'/'+$(this).parents('li').attr('count'));
            $('#image-modal .modal-body .product-image1 img').attr('src', $(this).parents('li').find('img').attr('original'));
            $('#image-modal .modal-body .product-image1 ').css('max-height', $(this).parents('li').find('img').attr('imageheight'));
            
			
            $('#image-modal .modal-body .product-information1 p').html($(this).parents('li').find('.content').html());
			if($('#image_num').length){ $('#image_num').remove(); }
            $('#image-modal .modal-body .product-information1').prepend('<input type="hidden" value = "'+$(this).parents('li').index()+'" id="image_num"/>');
			
            $Current = $(this).parents('li');
			current = $(this).parents('li');
			
			
            $PreviousElm = $(this).parents('li').prev();
            $nextElm = $(this).parents('li').next();
            if ($PreviousElm.length === 0) { $('.nav-btn.prev').css({ 'display': 'none' }); }
            else { $('.nav-btn.prev').css({ 'display': 'block' }); }
            if ($nextElm.length === 0) { $('.nav-btn.next').css({ 'display': 'none' }); }
            else { $('.nav-btn.next').css({ 'display': 'block' }); }
			
			if (!($nextElm.length === 0)){
			 SliderTimer = setInterval(SliderClick, 5000);
			}else{
			 SliderTimer = 0;
			}
			
        });
		
		
		$("#image-modal").hover(function () {
		       if(SliderTimer){
                clearInterval(SliderTimer);
			 }	
            }, function () {
			  if(SliderTimer){
                SliderTimer = setInterval(SliderClick, 5000);
			 }	
            });
		
		
		 function SliderClick() {		 
			  ForSliding();
        }
		
		
		
		 function ForSliding(){
		 
		   
		
		    if(current.next().length == 0){
			  current = $('#productContent li').eq(0);
			
			 }else{
			  current = current.next();
			 
			 }
			 
			$PreviousElm = current.prev();
            $nextElm = current.next();
			$('#image-modal .modal-title ').html('Photo '+ current.attr('number')+'/'+current.attr('count'));
		    $('#image-modal .modal-body .product-image1 img').clearQueue().animate({ opacity: '0' }, 0);
			$('#image-modal .modal-body .product-image1 img').remove();
			$('#image-modal .modal-body .product-image1 ').css('max-height', current.find('img').attr('height'));
			$('#image-modal .modal-body .product-image1').prepend('<img id="gallery-img" src="'+current.find('img').attr('original')+'" alt=""  style="opacity:0;max-width:90%;max-height:70%"/>');
			$('#image-modal .modal-body .product-image1 img').animate({ opacity: '1' }, 500);
            if($('#image_num').length){ $('#image_num').remove(); }
            $('#image-modal .modal-body .product-information1').prepend('<input type="hidden" value = "'+current.index()+'" id="image_num"/>');
			
			 $('#image-modal .modal-body .product-information1 p').html(current.find('.content').html());
			 
			 if ($PreviousElm.length === 0) { $('.nav-btn.prev').css({ 'display': 'none' }); }
            else { $('.nav-btn.prev').css({ 'display': 'block' }); }
            if ($nextElm.length === 0) { $('.nav-btn.next').css({ 'display': 'none' }); }
            else { $('.nav-btn.next').css({ 'display': 'block' }); }
		}
		
	//on Next click
        $('.nav-btn.next').click(function () {
            $NewCurrent = $nextElm;
            $PreviousElm = $NewCurrent.prev();
            $nextElm = $NewCurrent.next();
			

			$('#image-modal .modal-title ').html('Photo '+ $NewCurrent.attr('number')+'/'+$NewCurrent.attr('count'));
            $('#image-modal .modal-body .product-image1 img').clearQueue().animate({ opacity: '0' }, 0);
			$('#image-modal .modal-body .product-image1 img').remove();
			$('#image-modal .modal-body .product-image1 ').css('max-height', $NewCurrent.find('img').attr('imageheight'));
			$('#image-modal .modal-body .product-image1').prepend('<img id="gallery-img" src="'+$NewCurrent.find('img').attr('original')+'" alt=""  style="opacity:0;max-width:90%;max-height:70%"/>');
			$('#image-modal .modal-body .product-image1 img').animate({ opacity: '1' }, 500);
            if($('#image_num').length){ $('#image_num').remove(); }
            $('#image-modal .modal-body .product-information1').prepend('<input type="hidden" value = "'+$NewCurrent.index()+'" id="image_num"/>');
			
          
            
            $('#image-modal .modal-body .product-information1 p').html($NewCurrent.find('.content').html());
            if ($PreviousElm.length === 0) { $('.nav-btn.prev').css({ 'display': 'none' }); }
            else { $('.nav-btn.prev').css({ 'display': 'block' }); }
            if ($nextElm.length === 0) { $('.nav-btn.next').css({ 'display': 'none' }); }
            else { $('.nav-btn.next').css({ 'display': 'block' }); }
        });
		
		
		
		//on Prev click
        $('.prev').click(function () {
            $NewCurrent = $PreviousElm;
            $PreviousElm = $NewCurrent.prev();
            $nextElm = $NewCurrent.next();

			
			$('#image-modal .modal-title ').html('Photo '+ $NewCurrent.attr('number')+'/'+$NewCurrent.attr('count'));
			$('#image-modal .modal-body .product-image1 img').clearQueue().animate({ opacity: '0' }, 0);
			$('#image-modal .modal-body .product-image1 img').remove();
			$('#image-modal .modal-body .product-image1 ').css('max-height', $NewCurrent.find('img').attr('imageheight'));
			$('#image-modal .modal-body .product-image1').prepend('<img id="gallery-img" src="'+$NewCurrent.find('img').attr('original')+'" alt=""  style="opacity:0;max-width:90%;max-height:70%"/>');
			$('#image-modal .modal-body .product-image1 img').animate({ opacity: '1' }, 500);
			
			if($('#image_num').length){ $('#image_num').remove(); }
            $('#image-modal .modal-body .product-information1').prepend('<input type="hidden" value = "'+$NewCurrent.index()+'" id="image_num"/>');
			
            $('#image-modal .modal-body .product-information1 p').html($NewCurrent.find('.content').html());
            if ($PreviousElm.length === 0) { $('.nav-btn.prev').css({ 'display': 'none' }); }
            else { $('.nav-btn.prev').css({ 'display': 'block' }); }
            if ($nextElm.length === 0) { $('.nav-btn.next').css({ 'display': 'none' }); }
            else { $('.nav-btn.next').css({ 'display': 'block' }); }
        });
        //Close Popup
        $('.cross,.popup-overlay').click(function () {
            $('.product-gallery-popup').fadeOut(500);
            $('body').css({ 'overflow': 'initial' });
        });

        //Key Events
        $(document).on('keyup', function (e) {
            e.preventDefault();
            //Close popup on esc
            if (e.keyCode === 27) { $('.product-gallery-popup').fadeOut(500); $('body').css({ 'overflow': 'initial' }); }
            //Next Img On Right Arrow Click
            if (e.keyCode === 39) { NextProduct(); }
            //Prev Img on Left Arrow Click
            if (e.keyCode === 37) { PrevProduct(); }
        });

        function NextProduct() {
            if ($nextElm.length === 1) {
                $NewCurrent = $nextElm;
                $PreviousElm = $NewCurrent.prev();
                $nextElm = $NewCurrent.next();

                 $('#image-modal .modal-title ').html('Photo '+ $NewCurrent.attr('number')+'/'+$NewCurrent.attr('count'));
				 $('#image-modal .modal-body .product-image1 img').clearQueue().animate({ opacity: '0' }, 0);
			     $('#image-modal .modal-body .product-image1 img').remove();
				 $('#image-modal .modal-body .product-image1 img').css('max-width', '90%' );
				 $('#image-modal .modal-body .product-image1 img').css('max-height', '70%' );
				 $('#image-modal .modal-body .product-image1 ').css('max-height', $NewCurrent.find('img').attr('imageheight'));
			     $('#image-modal .modal-body .product-image1').prepend('<img id="gallery-img" src="'+$NewCurrent.find('img').attr('original')+'" alt=""  style="opacity:0;max-width:90%;max-height:70%"/>');
			     $('#image-modal .modal-body .product-image1 img').animate({ opacity: '1' }, 500);
				
				if($('#image_num').length){ $('#image_num').remove(); }
                $('#image-modal .modal-body .product-information1').prepend('<input type="hidden" value = "'+$NewCurrent.index()+'" id="image_num"/>');
				
                $('#image-modal .modal-body .product-information1 p').html($NewCurrent.find('.content').html());
                if ($PreviousElm.length === 0) { $('.nav-btn.prev').css({ 'display': 'none' }); }
                else { $('.nav-btn.prev').css({ 'display': 'block' }); }
                if ($nextElm.length === 0) { $('.nav-btn.next').css({ 'display': 'none' }); }
                else { $('.nav-btn.next').css({ 'display': 'block' }); }
            }

        }

        function PrevProduct() {
            if ($PreviousElm.length === 1) {
                $NewCurrent = $PreviousElm;
                $PreviousElm = $NewCurrent.prev();
                $nextElm = $NewCurrent.next();
                $('#image-modal .modal-body .product-image1 img').clearQueue().animate({ opacity: '0' }, 0).attr('src', $NewCurrent.find('img').attr('original')).animate({ opacity: '1' }, 500);
				
			
                $('#image-modal .modal-title ').html('Photo '+ $NewCurrent.attr('number')+'/'+$NewCurrent.attr('count'));
				if($('#image_num').length){ $('#image_num').remove(); }
                //$('#image-modal .modal-body .product-information1').prepend('<input type="hidden" value = "'+$NewCurrent.index()+'" id="image_num"/>');
				 $('#image-modal .modal-body .product-image1 img').clearQueue().animate({ opacity: '0' }, 0);
				 $('#image-modal .modal-body .product-image1 ').css('max-height', $NewCurrent.find('img').attr('imageheight'));
				 
			     $('#image-modal .modal-body .product-image1 img').remove();
			     $('#image-modal .modal-body .product-image1').prepend('<img id="gallery-img" src="'+$NewCurrent.find('img').attr('original')+'" alt=""  style="opacity:0;max-width:90%;max-height:70%"/>');
			     $('#image-modal .modal-body .product-image1 img').animate({ opacity: '1' }, 500);
		        
				
                $('#image-modal .modal-body .product-information1 p').html($NewCurrent.find('.content').html());
                if ($PreviousElm.length === 0) { $('.nav-btn.prev').css({ 'display': 'none' }); }
                else { $('.nav-btn.prev').css({ 'display': 'block' }); }
                if ($nextElm.length === 0) { $('.nav-btn.next').css({ 'display': 'none' }); }
                else { $('.nav-btn.next').css({ 'display': 'block' }); }
            }

        }
		
	};	
} (jQuery));