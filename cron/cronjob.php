<?php

define("DONT_RUN_APP", true);
defined('PUBLIC_PATH') || define('PUBLIC_PATH', realpath(dirname(__FILE__) . '/../public'));
require(realpath(PUBLIC_PATH . '/index.php'));
$application->bootstrap();

//error_reporting(E_ALL);
//ini_set('display_errors','On');

//Change Invoice To Overdue
$modelBookingInvoice = new Model_BookingInvoice();
$modelBookingInvoice->cronJobChangeToOverdue();

//Change Invoice To Open
$modelBookingInvoice->cronJobChangeToOpen();

//Change Invoice To Close
$modelBookingInvoice->cronJobChangeToClose();

//Change TO DO and TO VISIT To Awating Update
$modelBooking = new Model_Booking();
$modelBooking->cronJobChangeBookingToAwatingUpdate(); 

//Change IN PROGRESS TO AWAITING UPDATE, WHEN ALL DATES IS PAST, OR THERE IS ANY DATE IS PAST AND IS_VISITED STATUS IS UNKNOWN
$modelBooking = new Model_Booking();
$modelBooking->cronJobChangeInProgressBookingToAwaitingUpdate();
 
//Update Full Text Search
//$modelUpdateFullTextSearch = new Model_UpdateFullTextSearch();
//$modelUpdateFullTextSearch->cronJobUpdateFullTextSearch();