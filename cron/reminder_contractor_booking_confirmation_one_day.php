<?php

define("DONT_RUN_APP", true);
defined("PUBLIC_PATH") || define("PUBLIC_PATH", realpath(dirname(__FILE__) . "/../public"));
require(realpath(PUBLIC_PATH . "/index.php"));
$application->bootstrap();


//send reminder email to the contractor one day ahead of booking start date
 $modelBooking = new Model_Booking();
 $modelBooking->cronJobReminderContractorBookingConfirmationOneDay(); 

$ModelBooking = new Model_Booking();
$ModelBooking->cronJobSendSmsOnBookingTmorrow();