<?php

define("DONT_RUN_APP", true);
defined('PUBLIC_PATH') || define('PUBLIC_PATH', realpath(dirname(__FILE__) . '/../public'));
require(realpath(PUBLIC_PATH . '/index.php'));
$application->bootstrap();

echo "\n";
echo "\n";
echo 'Customers';
echo "\n";
$modelCustomer = new Model_Customer();
$customers = $modelCustomer->fetchAll()->toArray();
foreach ($customers as $customer){
    $db_params = array();
    $db_params['phone1'] = preparer_number($customer['phone1']);
    $db_params['phone2'] = preparer_number($customer['phone2']);
    $db_params['phone3'] = preparer_number($customer['phone3']);
    $db_params['mobile1'] = preparer_number($customer['mobile1']);
    $db_params['mobile2'] = preparer_number($customer['mobile2']);
    $db_params['mobile3'] = preparer_number($customer['mobile3']);
    
    $modelCustomer->updateById($customer['customer_id'], $db_params);
    echo ".";
}

echo "\n";
echo "\n";
echo "Users";
echo "\n";
$modelUser = new Model_User();
$users = $modelUser->fetchAll()->toArray();
foreach ($users as $user){
    $db_params = array();
    $db_params['phone1'] = preparer_number($user['phone1']);
    $db_params['phone2'] = preparer_number($user['phone2']);
    $db_params['phone3'] = preparer_number($user['phone3']);
    $db_params['mobile1'] = preparer_number($user['mobile1']);
    $db_params['mobile2'] = preparer_number($user['mobile2']);
    $db_params['mobile3'] = preparer_number($user['mobile3']);
    
    $modelUser->updateById($user['user_id'], $db_params);
    echo ".";
}

echo "\n";
echo "\n";
echo "Companies";
echo "\n";
$modelCompanies = new Model_Companies();
$companies = $modelCompanies->fetchAll()->toArray();
foreach ($companies as $company){
    $db_params = array();
    $db_params['company_phone1'] = preparer_number($company['company_phone1']);
    $db_params['company_phone2'] = preparer_number($company['company_phone2']);
    $db_params['company_phone3'] = preparer_number($company['company_phone3']);
    $db_params['company_mobile1'] = preparer_number($company['company_mobile1']);
    $db_params['company_mobile2'] = preparer_number($company['company_mobile2']);
    $db_params['company_mobile3'] = preparer_number($company['company_mobile3']);
    
    $modelCompanies->updateById($company['company_id'], $db_params);
    echo ".";
}

echo "\n";
echo "\n";
echo "done";
echo "\n";
echo "\n";