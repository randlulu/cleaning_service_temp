<?php
//by Ibraheem Qanah
function do_post_request($url, $params = array(), $head = array()) {
    //
    // request body
    //
    $body = array();
    foreach ($params as $param => $value) {
        $body[] = urlencode($param) . '=' . urlencode($value);
    }
    $body = implode('&', $body);

    //
    // request headers
    //
    $head[] = "Content-Type: application/x-www-form-urlencoded; charset=UTF-8";
    $head[] = "Content-Length: " . strlen($body);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $head);

    $output = curl_exec($ch);
    curl_close($ch);
    return $output;
}

$url = "http://www.alltilescleaning.com.au/formmail.php";
$params = array(
    'name' => 'Sandra Jon',
    'email' => 'sandrajon@yahoo.com',
    'phone' => '0431518995',
    'comment' => 'we need about 60 meters of tiles restored please'
);
$head = array('Referer: http://www.alltilescleaning.com.au/');

echo do_post_request($url, $params, $head);