<?php

define("DONT_RUN_APP", true);
defined('PUBLIC_PATH') || define('PUBLIC_PATH', realpath(dirname(__FILE__) . '/../public'));
require(realpath(PUBLIC_PATH . '/index.php'));
$application->bootstrap();

echo "\n";
echo "\n";
echo 'Booking';
echo "\n";
$modelBooking = new Model_Booking();
$bookings = $modelBooking->fetchAll()->toArray();
foreach ($bookings as $booking) {
    $db_params = array();
    $db_params['sub_total'] = round($booking['sub_total'], 2);
    $db_params['gst'] = round($booking['gst'], 2);
    $db_params['qoute'] = round($booking['qoute'], 2);
    $db_params['call_out_fee'] = round($booking['call_out_fee'], 2);
    $db_params['total_discount'] = round($booking['total_discount'], 2);
    $db_params['total_discount_temp'] = round($booking['total_discount_temp'], 2);
    $db_params['paid_amount'] = round($booking['paid_amount'], 2);

    if ($db_params['qoute'] < 0) {
        $db_params['sub_total'] = 0;
        $db_params['gst'] = 0;
        $db_params['qoute'] = 0;
        $db_params['call_out_fee'] = 0;
        $db_params['total_discount'] = 0;
        $db_params['total_discount_temp'] = 0;
        $db_params['paid_amount'] = 0;
    }

    $modelBooking->updateById($booking['booking_id'], $db_params);
    echo ".";
}

echo "\n";
echo "\n";
echo "Payments";
echo "\n";
$modelPayment = new Model_Payment();
$payments = $modelPayment->fetchAll()->toArray();
foreach ($payments as $payment) {
    $db_params = array();
    $db_params['amount'] = round($payment['amount'], 2);
    $db_params['amount_withheld'] = round($payment['amount_withheld'], 2);
    $db_params['bank_charges'] = round($payment['bank_charges'], 2);

    $modelPayment->updateById($payment['payment_id'], $db_params);
    echo ".";
}

//chick all paid_amount
$modelBookingInvoice = new Model_BookingInvoice();
$modelBookingInvoice->cronJobChickAllPaidAmount();

echo "\n";
echo "\n";
echo "done";
echo "\n";
echo "\n";