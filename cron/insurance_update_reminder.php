<?php

define("DONT_RUN_APP", true);
defined("PUBLIC_PATH") || define("PUBLIC_PATH", realpath(dirname(__FILE__) . "/../public"));
require(realpath(PUBLIC_PATH . "/index.php"));
$application->bootstrap();


//send reminder Email to contractor every Friday
$Model_ContractorInsurance = new Model_ContractorInsurance();
$Model_ContractorInsurance->insuranceUpdateReminderBeforeTwoWeekCronJob();