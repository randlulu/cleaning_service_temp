<?php

define("DONT_RUN_APP", true);
defined('PUBLIC_PATH') || define('PUBLIC_PATH', realpath(dirname(__FILE__) . '/../public'));
require(realpath(PUBLIC_PATH . '/index.php'));
$application->bootstrap();

ini_set('memory_limit', '-1');

echo "\n";
echo "\n";
echo 'Booking';
echo "\n";
$modelBooking = new Model_Booking();
$bookings = $modelBooking->fetchAll()->toArray();
foreach ($bookings as $booking) {
    $modelBooking->updateFullTextSearch($booking['booking_id']);
    echo ".";
}

echo "\n";
echo "\n";
echo 'Estimate';
echo "\n";
$modelBookingEstimate = new Model_BookingEstimate();
$bookingEstimates = $modelBookingEstimate->fetchAll()->toArray();
foreach ($bookingEstimates as $bookingEstimate) {
    $modelBookingEstimate->updateFullTextSearch($bookingEstimate['id']);
    echo ".";
}

echo "\n";
echo "\n";
echo 'Invoice';
echo "\n";
$modelBookingInvoice = new Model_BookingInvoice();
$bookingInvoices = $modelBookingInvoice->fetchAll()->toArray();
foreach ($bookingInvoices as $bookingInvoice) {
    $modelBookingInvoice->updateFullTextSearch($bookingInvoice['id']);
    echo ".";
}

echo "\n";
echo "\n";
echo 'Complaint';
echo "\n";
$modelComplaint = new Model_Complaint();
$complaints = $modelComplaint->fetchAll()->toArray();
foreach ($complaints as $complaint) {
    $modelComplaint->updateFullTextSearch($complaint['complaint_id']);
    echo ".";
}

echo "\n";
echo "\n";
echo 'Inquiry';
echo "\n";
$modelInquiry = new Model_Inquiry();
$inquirys = $modelInquiry->fetchAll()->toArray();
foreach ($inquirys as $inquiry) {
    $modelInquiry->updateFullTextSearch($inquiry['inquiry_id']);
    echo ".";
}

echo "\n";
echo "\n";
echo 'Customer';
echo "\n";
$modelCustomer = new Model_Customer();
$customers = $modelCustomer->fetchAll()->toArray();
foreach ($customers as $customer) {
    $modelCustomer->updateFullTextSearch($customer['customer_id']);
    echo ".";
}

echo "\n";
echo "\n";
echo "done";
echo "\n";
echo "\n";