<?php

define("DONT_RUN_APP", true);
defined('PUBLIC_PATH') || define('PUBLIC_PATH', realpath(dirname(__FILE__) . '/../public'));
require(realpath(PUBLIC_PATH . '/index.php'));
$application->bootstrap();


$modelMigration = new Model_Migration();
$modelOriginalInquiry = new Model_OriginalInquiry();
$modelCities = new Model_Cities();
$modelAttributeListValue = new Model_AttributeListValue();
$modelServices = new Model_Services();
$modelUser = new Model_User();
$modelCustomerType = new Model_CustomerType();
$modelCustomer = new Model_Customer();
$modelInquiryType = new Model_InquiryType();
$modelInquiry = new Model_Inquiry();
$modelInquiryService = new Model_InquiryService();
$modelServiceAttribute = new Model_ServiceAttribute();
$modelInquiryServiceAttributeValue = new Model_InquiryServiceAttributeValue();
$modelCompanies = new Model_Companies();
$modelInquiryPropertyType = new Model_InquiryPropertyType();
$modelInquiryRequiredType = new Model_InquiryRequiredType();

$inquiries = $modelMigration->getAll();


$companyName = "Tile Cleaners Pty Ltd";
$company = $modelCompanies->getByName($companyName);

$total_counter = count($inquiries);
$success_counter = 0;
$failure_counter = 0;
$failures = array();

foreach ($inquiries as $inquiry) {

    // get request parameters
    $ip_address = $inquiry['ip_address'];
    $page_url = $inquiry['page_url'];
    $floor = $inquiry['floor'];
    $service = $inquiry['service'];
    $cityName = $inquiry['city'];
    $property_type = $inquiry['property_type'];
    $name = $inquiry['name'];
    $email = trim($inquiry['email']);
    $mobile = trim($inquiry['mobile']);
    $phone = trim($inquiry['phone']);
    $address = $inquiry['address'];
    $postcode = $inquiry['postcode'];
    $required = $inquiry['required'];
    $comment = $inquiry['comment'];
    $keyword_referer = $inquiry['keyword_referer'];
    $ref_no = $inquiry['ref_no'];
    $date_posted = $inquiry['date_posted'];

    $originalInquiry = array(
        'ip_address' => $ip_address,
        'page_url' => $page_url,
        'floor' => $floor,
        'service' => $service,
        'city' => $cityName,
        'property_type' => $property_type,
        'name' => $name,
        'email' => $email,
        'mobile' => $mobile,
        'phone' => $phone,
        'address' => $address,
        'postcode' => $postcode,
        'required' => $required,
        'comment' => $comment,
        'keyword_referer' => $keyword_referer,
        'ref_no' => $ref_no,
        'date_posted' => $date_posted
    );

    $originalInquiryId = $modelOriginalInquiry->insert($originalInquiry);

    //insert city
    $city = $modelCities->getByName($cityName);
    if (empty($city)) {
        $failures[] = $inquiry['id'];
        $failure_counter++;
        continue;
    }

    $service = $modelServices->getByServiceName($service);
    if (empty($service)) {
        $failures[] = $inquiry['id'];
        $failure_counter++;
        continue;
    }

    $success_counter++;

    $customerPropertyType = $modelCustomerType->getByTypeName($property_type);

    $commercial = $modelCustomerType->getByTypeName('Commercial');
    $residential = $modelCustomerType->getByTypeName('Residential ');
    if ($customerPropertyType['customer_type_id'] == $commercial['customer_type_id']) {
        $customerTypeId = $commercial['customer_type_id'];
    } else {
        $customerTypeId = $residential['customer_type_id'];
    }

    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $tmp = uniqid('inq');
        $email = "tmp_{$tmp}@update_this_email.tmp";
    }

    $data = array(
        'title' => 'mr',
        'first_name' => $name,
        'city_id' => $city['city_id'],
        'street_address' => $address,
        'postcode' => $postcode,
        'email1' => $email,
        'phone1' => $phone,
        'mobile1' => $mobile,
        'full_name_search' => 'mr.' . $name,
        'created' => strtotime($inquiry['date_posted']),
        'customer_type_id' => $customerTypeId,
        'company_id' => $company['company_id']
    );
    $newCustomerId = $modelCustomer->insert($data);

    $inquiryRequiredType = $modelInquiryRequiredType->getByType($required);
    $inquiryPropertyType = $modelInquiryPropertyType->getByType($property_type);
    $inquiryType = $modelInquiryType->getByType('Website');
    $user = $modelUser->getByUserCode(sha1("General {$companyName}"));

    $data = array(
        'title' => "from {$companyName}",
        'inquiry_type_id' => $inquiryType['inquiry_type_id'],
        'comment' => $comment,
        'city_id' => $city['city_id'],
        'customer_id' => $newCustomerId,
        'created' => strtotime($inquiry['date_posted']),
        'user_id' => $user['user_id'],
        'full_text_search' => "from {$companyName} " . $comment,
        'original_inquiry_id' => $originalInquiryId,
        'company_id' => $company['company_id'],
        'property_type_id' => (isset($inquiryPropertyType['id']) ? $inquiryPropertyType['id'] : 0),
        'required_type_id' => (isset($inquiryRequiredType['id']) ? $inquiryRequiredType['id'] : 0)
    );
    $inquiryId = $modelInquiry->insert($data, $company['company_id']);

    if ($inquiryId) {

        /*
         * add Inquiry Address
         */
        $db_params = array();
        $db_params['inquiry_id'] = $inquiryId;
        $db_params['street_address'] = $address;
        $db_params['suburb'] = '';
        $db_params['state'] = '';
        $db_params['unit_lot_number'] = '';
        $db_params['street_number'] = '';
        $db_params['postcode'] = $postcode;
        $db_params['po_box'] = '';
        $modelInquiryAddress = new Model_InquiryAddress();
        $modelInquiryAddress->insert($db_params);


        /*
         * add service to inquiry
         */
        $params = array(
            'service_id' => $service['service_id'],
            'inquiry_id' => $inquiryId,
            'clone' => 0
        );
        $modelInquiryService->insert($params);


        /*
         * add floor to inquiry
         */
        $attributeValue = $modelAttributeListValue->getByAttributeValue($floor);
        if ($attributeValue) {
            $serviceAttribute = $modelServiceAttribute->getByAttributeIdAndServiceId($attributeValue['attribute_id'], $service['service_id']);
            if ($serviceAttribute) {
                $data = array(
                    'service_attribute_id' => $serviceAttribute['service_attribute_id'],
                    'inquiry_id' => $inquiryId,
                    'value' => $attributeValue['attribute_value_id'],
                    'clone' => 0
                );
                $modelInquiryServiceAttributeValue->insert($data);
            }
        }
    }
    echo '.';
}

echo "\n";
echo '----------------------------------' . "\n";
echo 'done' . "\n";
echo 'total = ' . $total_counter . "\n";
echo 'success = ' . $success_counter . "\n";
echo 'failure = ' . $failure_counter . "\n";
echo '----------------------------------' . "\n";
echo "\n";
echo "\n";
echo 'failure inquiry' . "\n";
echo "\n";
foreach ($failures as $failure) {
    echo 'inquiry id = ' . $failure . "\n";
}
echo '----------------------------------' . "\n";
echo "\n";