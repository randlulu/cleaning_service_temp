<?php

define("DONT_RUN_APP", true);
defined("PUBLIC_PATH") || define("PUBLIC_PATH", realpath(dirname(__FILE__) . "/../public"));
require(realpath(PUBLIC_PATH . "/index.php"));
$application->bootstrap();


//Send Inquiry Contact Attempt
$modelInquiry = new Model_Inquiry();
$modelInquiry->cronJobSendInquiryContactAttempt();