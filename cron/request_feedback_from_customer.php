<?php

define("DONT_RUN_APP", true);
defined("PUBLIC_PATH") || define("PUBLIC_PATH", realpath(dirname(__FILE__) . "/../public"));
require(realpath(PUBLIC_PATH . "/index.php"));
$application->bootstrap();


// Send Request Feedback to Customer for all Booking have feedback status
$modelBooking = new Model_Booking();
$modelBooking->cronJobSendRequestFeedback();