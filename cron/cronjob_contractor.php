<?php

define("DONT_RUN_APP", true);
defined('PUBLIC_PATH') || define('PUBLIC_PATH', realpath(dirname(__FILE__) . '/../public'));
require(realpath(PUBLIC_PATH . '/index.php'));
$application->bootstrap();

$modelPayment = new Model_Payment();
$payments = $modelPayment->fetchAll()->toArray();

echo "\n";
echo "\n";
echo 'start';
echo "\n";
$modelContractorServiceBooking = new Model_ContractorServiceBooking();
foreach ($payments as $payment) {
    $contractorServiceBookings = $modelContractorServiceBooking->getByBookingId($payment['booking_id']);
    if (count($contractorServiceBookings) == 1) {
        $modelPayment->updateById($payment['payment_id'], array('contractor_id' => $contractorServiceBookings[0]['contractor_id']));
        echo ".";
    } else {
        $contractor_ids = array();
        foreach ($contractorServiceBookings as $contractorServiceBooking) {
            $contractor_ids[$contractorServiceBooking['contractor_id']] = $contractorServiceBooking['contractor_id'];
        }
        if (count($contractor_ids) == 1) {
            $modelPayment->updateById($payment['payment_id'], array('contractor_id' => current($contractor_ids)));
            echo ".";
        } else {
            echo "\n";
            echo "payment_id : " . $payment['payment_id'];
            echo "\n";
            echo "booking_id : " . $payment['booking_id'];
            echo "\n";
        }
    }
}
echo "\n";
echo "\n";
echo "done";
echo "\n";
echo "\n";