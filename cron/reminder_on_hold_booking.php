<?php

define("DONT_RUN_APP", true);
defined("PUBLIC_PATH") || define("PUBLIC_PATH", realpath(dirname(__FILE__) . "/../public"));
require(realpath(PUBLIC_PATH . "/index.php"));
$application->bootstrap();


//Reminder On Hold Booking
$modelBooking = new Model_Booking();
$modelBooking->cronJobReminderOnHoldBooking();