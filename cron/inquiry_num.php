<?php

define("DONT_RUN_APP", true);
defined('PUBLIC_PATH') || define('PUBLIC_PATH', realpath(dirname(__FILE__) . '/../public'));
require(realpath(PUBLIC_PATH . '/index.php'));
$application->bootstrap();

$modelUpdateFullTextSearch = new Model_UpdateFullTextSearch();
$modelInquiry = new Model_Inquiry();

echo "\n";
echo "\n";
echo 'Inquiry';
echo "\n";

$select = $modelInquiry->getAdapter()->select();
$select->from('inquiry');
$select->order("inquiry_id asc");
$inquirys = $modelInquiry->getAdapter()->fetchAll($select);

foreach ($inquirys as $inquiry) {
    if ($inquiry['inquiry_num'] != 'INQ-' . $inquiry['count']) {
        $data = array();
        $data['inquiry_num'] = 'INQ-' . $inquiry['count'];
        $modelInquiry->update($data, "inquiry_id = '{$inquiry['inquiry_id']}'");

        $modelUpdateFullTextSearch->updateFullTextSearchFlag('inquiry', $inquiry['inquiry_id'], false);
        echo ".";
    }
}

echo "\n";
echo "\n";
echo "done";
echo "\n";
echo "\n";