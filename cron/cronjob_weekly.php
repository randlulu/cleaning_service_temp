<?php

define("DONT_RUN_APP", true);
defined('PUBLIC_PATH') || define('PUBLIC_PATH', realpath(dirname(__FILE__) . '/../public'));
require(realpath(PUBLIC_PATH . '/index.php'));
$application->bootstrap();

//Reset Overdue Invoice Reminder
$modelBookingInvoice = new Model_BookingInvoice();
$modelBookingInvoice->cronJobResetOverdueInvoiceReminder();

//Reset On Hold Booking Reminder
$modelBooking = new Model_Booking();
$modelBooking->cronJobResetOnHoldBookingReminder();

//Reset Tentative Booking Reminder
$modelBooking = new Model_Booking();
$modelBooking->cronJobResetTentativeBookingReminder();

//Reset Estimates Reminder
$modelBookingEstimate = new Model_BookingEstimate();
$modelBookingEstimate->cronJobResetQuotedBookingReminder();

//Count Duplicate
$modelCustomer = new Model_Customer();
$modelCustomer->cronJobCountDuplicate();

//reminde the contractor that have profile score < 80%
//$modelUser = new Model_User();
//$modelUser->cronJobProfileCompletenessReminder();