<?php

define("DONT_RUN_APP", true);
defined('PUBLIC_PATH') || define('PUBLIC_PATH', realpath(dirname(__FILE__) . '/../public'));
require(realpath(PUBLIC_PATH . '/index.php'));
$application->bootstrap();

$modelBooking = new Model_Booking();
$modelBookingLog = new Model_BookingLog();

$modelBookingAddress = new Model_BookingAddress();
$modelBookingAddressLog = new Model_BookingAddressLog();

$modelBookingInvoice = new Model_BookingInvoice();
$modelBookingInvoiceLog = new Model_BookingInvoiceLog();

$modelBookingEstimate = new Model_BookingEstimate();
$modelBookingEstimateLog = new Model_BookingEstimateLog();

$modelContractorServiceBooking = new Model_ContractorServiceBooking();
$modelContractorServiceBookingLog = new Model_ContractorServiceBookingLog();

$modelServiceAttributeValue = new Model_ServiceAttributeValue();
$modelServiceAttributeValueLog = new Model_ServiceAttributeValueLog();

$modelBookingHistory = new Model_BookingHistory();
$select = $modelBookingHistory->getAdapter()->select();
$select->from('booking_history', array('booking_id', 'user_id'));
$select->order(array('booking_id', 'user_id'));
$select->distinct();

$historys = $modelBookingHistory->getAdapter()->fetchAll($select);

foreach ($historys as $history) {

    $booking = $modelBooking->getById($history['booking_id']);
    if ($booking) {
        $modelBookingLog->addBookingLog($history['booking_id'], $history['user_id']);

        $bookingAddress = $modelBookingAddress->getByBookingId($history['booking_id']);
        if ($bookingAddress) {
            $modelBookingAddressLog->addBookingAddressLog($bookingAddress['booking_address_id'], $history['user_id']);
        }

        $bookingInvoice = $modelBookingInvoice->getByBookingId($history['booking_id']);
        if ($bookingInvoice) {
            $modelBookingInvoiceLog->addBookingInvoiceLog($bookingInvoice['id'], $history['user_id']);
        }

        $bookingEstimate = $modelBookingEstimate->getByBookingId($history['booking_id']);
        if ($bookingEstimate) {
            $modelBookingEstimateLog->addBookingEstimateLog($bookingEstimate['id'], $history['user_id']);
        }

        $bookingContractorServiceBookings = $modelContractorServiceBooking->getByBookingId($history['booking_id']);
        if ($bookingContractorServiceBookings) {
            foreach ($bookingContractorServiceBookings as $bookingContractorServiceBooking) {
                $modelContractorServiceBookingLog->addContractorServiceBookingLog($bookingContractorServiceBooking['id'], $history['user_id']);
            }
        }

        $serviceAttributeValues = $modelServiceAttributeValue->getByBookingIdWithoutExtraField($history['booking_id']);
        if ($serviceAttributeValues) {
            foreach ($serviceAttributeValues as $serviceAttributeValue) {
                $modelServiceAttributeValueLog->addServiceAttributeValueLog($serviceAttributeValue['service_attribute_value_id'], $history['user_id']);
            }
        }
    }

    echo '.';
}
