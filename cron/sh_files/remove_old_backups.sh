month=`date +'%m'`;
year=`date +'%Y'`;

if [ $month -eq 1 ] || [ $month -eq 01 ]
then
        last_month=12;
        year=`expr $year - 1`;
else
        last_month=`expr $month - 1`;
	last_month=`printf "%02d\n" $last_month`;
fi

##find "/home/ubuntu/database_backup/" -name "cleaning_service-$year-$last_month*" -exec ls {} \;
find "/home/ubuntu/database_backup/" -name "cleaning_service-$year-$last_month*" -exec rm -f {} \;
