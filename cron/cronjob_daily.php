<?php

//error_reporting(E_ALL);
//ini_set('display_errors','On');

define("DONT_RUN_APP", true);
defined("PUBLIC_PATH") || define("PUBLIC_PATH", realpath(dirname(__FILE__) . "/../public"));
require(realpath(PUBLIC_PATH . "/index.php"));
$application->bootstrap();

//change customers data
//$c = new Model_Customer();
//$c->updateAllPhoneAndMobileNumbers();
//exit;  

//Reset Send Inquiry Contact Attempt
$modelInquiry = new Model_Inquiry();
$modelInquiry->cronJobResetSendInquiryContactAttempt();

//Resend Full booking to Contractor Google Calendar
$modelGoogleCalendarEvent = new Model_GoogleCalendarEvent();
$modelGoogleCalendarEvent->cronJobCheckGmailBookingFullEvent();

//Reset Daily Overdue (30) Invoice Reminder
$modelBookingInvoice = new Model_BookingInvoice();
$modelBookingInvoice->cronJobResetDailyOverdueInvoiceReminder();

//Daily Report
$modelDailyReport = new Model_DailyReport();
$modelDailyReport->cronJobDailyReport();

// Daily Complaint Remindar 
$modelComplaint = new Model_Complaint();
$modelComplaint->ReminderComplaintToContractor();

// Daily Get Sms Information 
$modelBooking = new Model_Booking();
$modelBooking->cronJobGetSmsInfo();


    /*
     //send sms message for tomorrow booking for customer and contractor//cronJobGetSmsMessagInfo
     $modelBooking = new Model_Booking();
     $modelBooking->cronJobSendSmsOnBookingTmorrow();
     
     //////////////////////////////////
     
     //get sms message information
     $modelBooking = new Model_Booking();
     $modelBooking->cronJobGetSmsMessagInfo();
     
     /////////////////////////////////////
     
     //resend failure message for users//
     $modelBooking = new Model_Booking();
     $modelBooking->cronJobResendSmsFailedMessag();
    */
