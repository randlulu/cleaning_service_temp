<?php

class Customer_IndexController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'customers';
    }

    /**
     * Items list action
     */
    public function indexAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('customerList'));
		
		/*if($_SERVER['REMOTE_ADDR'] == '176.106.46.142'){
		 $modelCustomer = new Model_Customer();
		 $modelCustomer->cronJobFiveStarReasonsToChooseTileCleaners();

		}*/

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'customer_id');
        $sortingMethod = $this->request->getParam('method', 'desc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());
		$is_first_time = $this->request->getParam('is_first_time');
		$page_number = $this->request->getParam('page_number');
		$isDeleted = $this->request->getParam('is_deleted', 0);
		

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }
		
		if($isDeleted){
		   $filters['is_deleted'] = 1;
		}

        //
        // init pager and articles model object
        //
		
        /*$pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];*/

        //
        // get data list
        //
        $customerObj = new Model_Customer();
        $loggedUser = CheckAuth::getLoggedUser();

        //check if loged user is contractor
        if (!CheckAuth::checkCredential(array('CanSeeAllBooking'))) {
            if (CheckAuth::checkCredential(array('CanSeeOnlyHisBooking'))) {
                if (CheckAuth::checkCredential(array('CanSeeAssignedBooking'))) {
                    $filters['created_by'] = $loggedUser['user_id'];
                }
            }
        }
		
		 if ($this->request->isPost()) {
            if(isset($page_number)){
			  $perPage = 15;
			  $currentPage = $page_number +1;
		    }
			if(isset($filters['is_deleted'])){
			 $isDeleted = $filters['is_deleted'];
			}
			$data = $customerObj->getAll($filters, "{$orderBy} {$sortingMethod}", $pager, 0, $perPage, $currentPage);
			$result = array();
			 $this->view->isDeleted = $isDeleted;
		     $this->view->data = $data;
			 $this->view->is_first_time = $is_first_time;
             $result['data'] = $this->view->render('index/draw-node.phtml');
			 
			 if($data){
			   $result['is_last_request'] = 0;
			  }else{
			   $result['is_last_request'] = 1;
			  }
			  
		      echo json_encode($result);
				
			exit;
		  }

        //$this->view->data = $customerObj->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);
        //
        // set view params
        //
        //$this->view->currentPage = $currentPage;
        //$this->view->perPage = $pager->perPage;
        //$this->view->pageLinks = $pager->getPager();
        
		$this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
        $this->view->sub_menu = "customer";
    }

    /**
     * Add new item action
     */
    public function addAction() {	
	    //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('customerAdd'));

        //
        //getting the customer type id to configure commercial customer
        //
        $modelCustomerType = new Model_CustomerType();
//        $customerType = $modelCustomerType->getByTypeName("Commercial");
//        $commercialId = $customerType['customer_type_id'];
//        $this->view->commercialId = $commercialId;

        $residential = $modelCustomerType->getByTypeName("Residential");
        $residentialId = $residential['customer_type_id'];
        $this->view->residentialId = $residentialId;
        //
        // get request parameters
        //
        $title = $this->request->getParam('title');
        $firstName = $this->request->getParam('first_name');
        $lastName = $this->request->getParam('last_name');
		$title2 = $this->request->getParam('title2');
        $firstName2 = $this->request->getParam('first_name2');
        $lastName2 = $this->request->getParam('last_name2');
		$title3 = $this->request->getParam('title3');
        $firstName3 = $this->request->getParam('first_name3');
        $lastName3 = $this->request->getParam('last_name3');
        $customerTypeId = $this->request->getParam('customer_type_id');
        $businessName = $this->request->getParam('customer_business_name');
        $unitLotNumber = $this->request->getParam('unit_lot_number');
        $streetNumber = $this->request->getParam('street_number');
        $streetAddress = $this->request->getParam('street_address');
        $suburb = $this->request->getParam('suburb');
        $state = $this->request->getParam('customer_state');
        $postcode = $this->request->getParam('postcode');
        $po_box = $this->request->getParam('po_box');
        $cityId = $this->request->getParam('customer_city_id');
        $countryId = $this->request->getParam('customer_country_id');
        $email1 = $this->request->getParam('email1');
        $email2 = $this->request->getParam('email2');
        $email3 = $this->request->getParam('email3');
		
		$internation_key = $this->request->getParam('international_key');
        $mobile_key = $this->request->getParam('mobile_key');
        $phone_key = $this->request->getParam('phone_key');
		
        $mobile1 = $this->request->getParam('mobile1');
        $mobile2 = $this->request->getParam('mobile2');
        $mobile3 = $this->request->getParam('mobile3');
        $phone1 = $this->request->getParam('phone1');
        $phone2 = $this->request->getParam('phone2');
        $phone3 = $this->request->getParam('phone3');
        $fax = $this->request->getParam('fax');
        $asJson = $this->request->getParam('asJson', 0);
        $contacts_counter = $this->request->getParam('contact_counter', 0);
        $customer_contacts = $this->request->getParam('contacts', array());
        $asBookingAddress = $this->request->getParam('as_booking_address', 0);
		
		/********Check if user put phone or mobile Number? and insert full format***********IBM*/
        if(isset($mobile1) && $mobile1 != ""){
            $mobile1_key = substr($mobile_key, 1, 1);
            $mobile1 = $internation_key . $mobile1_key . $mobile1;
        }
         if(isset($mobile2) && $mobile2 != ""){
            $mobile2_key = substr($mobile_key, 1, 1);
            $mobile2 = $internation_key . $mobile2_key . $mobile2;
        }
        if(isset($mobile3) && $mobile3 != ""){
            $mobile3_key = substr($mobile_key, 1, 1);
            $mobile3 = $internation_key . $mobile3_key . $mobile3;
        }
        if(isset($phone1) && $phone1 != ""){
            $phone1_key = substr($phone_key, 1, 1);
            $phone1 = $internation_key . $phone1_key . $phone1;

        }
        if(isset($phone2) && $phone2 != ""){
            $phone2_key = substr($phone_key, 1, 1);
            $phone2 = $internation_key . $phone2_key . $phone2;
        }
        if(isset($phone3) && $phone3 != ""){
            $phone3_key = substr($phone_key, 1, 1);
            $phone3 = $internation_key . $phone3_key . $phone3;
        }
      
        /*******End***********/

        if (get_config('remove_white_spacing')) {
            $mobile1 = preparer_number($mobile1);
            $mobile2 = preparer_number($mobile2);
            $mobile3 = preparer_number($mobile3);
            $phone1 = preparer_number($phone1);
            $phone2 = preparer_number($phone2);
            $phone3 = preparer_number($phone3);
        }

        $haveBookingAddress = $modelCustomerType->getCustomerTypeHaveBookingAddressAsArray();
        $this->view->haveBookingAddress = $haveBookingAddress;
        //
        // init action form
        //
        $options = array(
            'country_id' => $countryId,
            'customer_business_name' => $businessName,
            'state' => $state,
            'asJson' => $asJson,
            'residentialId' => $residentialId
        );
        $form = new Customer_Form_Customer($options);

        // add customer contact if exist
        if ($customer_contacts) {
            foreach ($customer_contacts as $key => $customer_contact) {

                $formObjects = $this->drawContactLabel($key, $asobject = true);
                $form->addElements($formObjects);
            }

            $this->view->customer_contacts = $customer_contacts;
            $this->view->contacts_counter = $contacts_counter;
        }


        //
        // handling the insertion process
        //
       
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $customerObj = new Model_Customer();
                $loggedUser = CheckAuth::getLoggedUser();
                $company_id = CheckAuth::getCompanySession();
                $db_params = array(
                    'title' => $title,
                    'first_name' => $firstName,
                    'last_name' => $lastName,
					'title2' => $title2,
                    'first_name2' => $firstName2,
                    'last_name2' => $lastName2,
					'title3' => $title3,
                    'first_name3' => $firstName3,
                    'last_name3' => $lastName3,
                    'city_id' => $cityId,
                    'email1' => $email1,
                    'email2' => $email2,
                    'email3' => $email3,
                    'mobile1' => $mobile1,
                    'mobile2' => $mobile2,
                    'mobile3' => $mobile3,
                    'phone1' => $phone1,
                    'phone2' => $phone2,
                    'phone3' => $phone3,
                    'fax' => $fax,
                    'customer_type_id' => $customerTypeId,
                    'unit_lot_number' => $unitLotNumber,
                    'street_number' => $streetNumber,
                    'street_address' => $streetAddress,
                    'suburb' => $suburb,
                    'state' => $state,
                    'postcode' => $postcode,
                    'po_box' => $po_box,
                    'full_name_search' => $title . '.' . $firstName . ' ' . $lastName,
                    'created_by' => $loggedUser['user_id'],
                    'created' => time(),
                    'company_id' => $company_id
                );

                $newCustomerId = $customerObj->insert($db_params);

                // insert customer contact
                if ($newCustomerId) {
                    $customerContact = new Model_CustomerContact();
                    foreach ($customer_contacts as $key => $customer_contact) {
                        $data = array(
                            'customer_id' => $newCustomerId,
                            'customer_contact_label_id' => $customer_contact["contact_label_{$key}"],
                            'contact' => $customer_contact["info_{$key}"],
                        );
                        $customerContact->insert($data);
                    }
                }
                $modelCustomerCommercialInfo = new Model_CustomerCommercialInfo();
                $modelCustomerCommercialInfo->setCustomerCommercialInfo($newCustomerId, $customerTypeId, $businessName);


                //if it was from booking it returns the new customer ID to select on drop down
                $data = array(
                    'customer_id' => $newCustomerId,
					'city_id' => $cityId,
					'state' => $state,
                    'customer_name' => get_customer_name($db_params),
                    'asJson' => $asJson,
                    'asBookingAddress' => $asBookingAddress
                );
                echo json_encode($data);
                exit;
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('index/add_edit.phtml');
        exit;
    }

    public function deleteAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('customerDelete'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
		$currentPage = $this->request->getParam('currentPage', 1);
        if ($id) {
            $ids[] = $id;
        }
        $modelCustomerCommercialInfo = new Model_CustomerCommercialInfo();
        $customerObj = new Model_Customer();
        foreach ($ids as $id) {
            if (CheckAuth::checkIfCanHandelAllCompany('customer', $id)) {
                $data = array(
                    'is_deleted' => 1
                );
                $customerObj->updateById($id, $data);
                $modelCustomerCommercialInfo->deleteByCustomerId($id);
            }
        }
        $this->_redirect($this->router->assemble(array(), 'customerList').'?page='.$currentPage);
    }

    public function undeleteAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('customerUndelete'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);

        if (!CheckAuth::checkIfCanHandelAllCompany('customer', $id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this booking"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $customerObj = new Model_Customer();
        $data = array(
            'is_deleted' => 0
        );
        $success = $customerObj->updateById($id, $data);

        if ($success) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Customer details have been restored"));
        } else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Failed to restore customer details"));
        }

        $this->_redirect($this->router->assemble(array(), 'customerList'));
    }

    public function editAction() {

        //
        // check Auth for logged user
        //
		
		
		
        CheckAuth::checkPermission(array('customerEdit'));

        //
        //getting the customer type id to configure commercial customer
        //
        $modelCustomerType = new Model_CustomerType();

//        $customerType = $modelCustomerType->getByTypeName("Commercial");
//        $commercialId = $customerType['customer_type_id'];
//        $this->view->commercialId = $commercialId;

        $residential = $modelCustomerType->getByTypeName("Residential");
        $residentialId = $residential['customer_type_id'];
        $this->view->residentialId = $residentialId;
        //
        // get request parameters
        //
        $title = $this->request->getParam('title');
        $firstName = $this->request->getParam('first_name');
        $lastName = $this->request->getParam('last_name');
		$title2 = $this->request->getParam('title2');
        $firstName2 = $this->request->getParam('first_name2');
        $lastName2 = $this->request->getParam('last_name2');
		$title3 = $this->request->getParam('title3');
        $firstName3 = $this->request->getParam('first_name3');
        $lastName3 = $this->request->getParam('last_name3');
        $customerTypeId = $this->request->getParam('customer_type_id');
        $businessName = $this->request->getParam('customer_business_name');
        $unitLotNumber = $this->request->getParam('unit_lot_number');
        $streetNumber = $this->request->getParam('street_number');
        $streetAddress = $this->request->getParam('street_address');
        $suburb = $this->request->getParam('suburb');
        $state = $this->request->getParam('customer_state');
        $postcode = $this->request->getParam('postcode');
        $po_box = $this->request->getParam('po_box');
        $cityId = $this->request->getParam('customer_city_id');
        $countryId = $this->request->getParam('customer_country_id');
        $email1 = $this->request->getParam('email1');
        $email2 = $this->request->getParam('email2');
        $email3 = $this->request->getParam('email3');
		$internation_key = $this->request->getParam('international_key');
        $mobile_key = $this->request->getParam('mobile_key');
        $phone_key = $this->request->getParam('phone_key');
		
        $mobile1 = $this->request->getParam('mobile1');
        $mobile2 = $this->request->getParam('mobile2');
        $mobile3 = $this->request->getParam('mobile3');
        $phone1 = $this->request->getParam('phone1');
        $phone2 = $this->request->getParam('phone2');
        $phone3 = $this->request->getParam('phone3');
        $fax = $this->request->getParam('fax');
        $id = $this->request->getParam('id');
        $asJson = $this->request->getParam('asJson', 0);
        $newCustomerContacts = $this->request->getParam('contacts', array());
        $contacts_counter = $this->request->getParam('contact_counter', 0);
        $asBookingAddress = $this->request->getParam('as_booking_address', 0);
		
		/********Check if user put phone or mobile Number? and insert full format***********IBM*/
        if(isset($mobile1) && $mobile1 != ""){
            $mobile1_key = substr($mobile_key, 1, 1);
            $mobile1 = $internation_key . $mobile1_key . $mobile1;
        }
         if(isset($mobile2) && $mobile2 != ""){
            $mobile2_key = substr($mobile_key, 1, 1);
            $mobile2 = $internation_key . $mobile2_key . $mobile2;
        }
        if(isset($mobile3) && $mobile3 != ""){
            $mobile3_key = substr($mobile_key, 1, 1);
            $mobile3 = $internation_key . $mobile3_key . $mobile3;
        } 
        if(isset($phone1) && $phone1 != ""){
            $phone1_key = substr($phone_key, 1, 1);
            $phone1 = $internation_key . $phone1_key . $phone1;

        }
        if(isset($phone2) && $phone2 != ""){
            $phone2_key = substr($phone_key, 1, 1);
            $phone2 = $internation_key . $phone2_key . $phone2;
        }
        if(isset($phone3) && $phone3 != ""){
            $phone3_key = substr($phone_key, 1, 1);
            $phone3 = $internation_key . $phone3_key . $phone3;
        }
      
        /*******End***********/

        if (get_config('remove_white_spacing')) {
            $mobile1 = preparer_number($mobile1);
            $mobile2 = preparer_number($mobile2);
            $mobile3 = preparer_number($mobile3);
            $phone1 = preparer_number($phone1);
            $phone2 = preparer_number($phone2);
            $phone3 = preparer_number($phone3);
        }

        //
        // validation
        //
        if (!CheckAuth::checkIfCanHandelAllCompany('customer', $id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this customer"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $customerObj = new Model_Customer();
        $modelCustomerCommercialInfo = new Model_CustomerCommercialInfo();

        $customer = $customerObj->getById($id);


        if (!$customer) {
            $this->_redirect($this->router->assemble(array(), 'settingsCustomerList'));
            return;
        }


        $haveBookingAddress = $modelCustomerType->getCustomerTypeHaveBookingAddressAsArray();
        $this->view->haveBookingAddress = $haveBookingAddress;

        $customerCommercial = $modelCustomerCommercialInfo->getByCustomerId($id);
        $commercialBusinessName = $customerCommercial['business_name'];

        $customerType = $modelCustomerType->getById($customer['customer_type_id']);

        //
        // init action form
        //
        $options = array(
            'commercial_business_name' => $commercialBusinessName,
            'residentialId' => $residentialId,
            'asJson' => $asJson,
            'mode' => 'update',
            'customer' => $customer,
            'state' => $state,
            'country_id' => $countryId,
            'as_booking_address' => $customerType['as_booking_address']
        );
        $form = new Customer_Form_Customer($options);


        //   get customer contact

        $modelCustomerContact = new Model_CustomerContact();
        $customer_contacts = $modelCustomerContact->getByCustomerId($id);
        $counter = 0;

        // add  customer contact  from old data(database) or new(isPost)
        if ($this->request->isPost()) {
            foreach ($newCustomerContacts as $key => $newCustomerContact) {
                $formObjects = $this->drawContactLabel($key, $asobject = true, $newCustomerContact);
                $form->addElements($formObjects);
                $counter++;
            }
            $this->view->customer_contacts = $newCustomerContacts;
        } else {
            foreach ($customer_contacts as $key => $customerContact) {
                $formObjects = $this->drawContactLabel($key, $asobject = true, $customerContact);
                $form->addElements($formObjects);
                $counter++;
            }
            $this->view->customer_contacts = $customer_contacts;
        }
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $db_params = array(
                    'title' => $title,
                    'first_name' => $firstName,
                    'last_name' => $lastName,
			'title2' => $title2,
                    'first_name2' => $firstName2,
                    'last_name2' => $lastName2,
					'title3' => $title3,
                    'first_name3' => $firstName3,
                    'last_name3' => $lastName3,
                    'city_id' => $cityId,
                    'email1' => $email1,
                    'email2' => $email2,
                    'email3' => $email3,
                    'mobile1' => $mobile1,
                    'mobile2' => $mobile2,
                    'mobile3' => $mobile3,
                    'phone1' => $phone1,
                    'phone2' => $phone2,
                    'phone3' => $phone3,
                    'fax' => $fax,
                    'customer_type_id' => $customerTypeId,
                    'unit_lot_number' => $unitLotNumber,
                    'street_number' => $streetNumber,
                    'street_address' => $streetAddress,
                    'suburb' => $suburb,
                    'state' => $state,
                    'postcode' => $postcode,
                    'po_box' => $po_box,
                    'full_name_search' => $title . '.' . $firstName . ' ' . $lastName
                );


                $customerObj->updateById($id, $db_params);
                $modelCustomerContact = new Model_CustomerContact();
                $modelCustomerContact->deleteByCustomerId($id);
                if ($newCustomerContacts) {
                    foreach ($newCustomerContacts as $key => $newCustomerContact) {
                        $new_data = array(
                            'customer_id' => $id,
                            'customer_contact_label_id' => $newCustomerContact["contact_label_{$key}"],
                            'contact' => $newCustomerContact["info_{$key}"],
                        );
                        $modelCustomerContact->insert($new_data);
                    }
                }
                $modelCustomerCommercialInfo->setCustomerCommercialInfo($id, $customerTypeId, $businessName);

                //if it was from booking it returns the new customer ID to select on drop down
                $data = array(
                    'customer_id' => $id,
					'city_id' => $cityId,
					'state' => $state,
                    'customer_name' => get_customer_name($db_params),
                    'asJson' => $asJson,
                    'asBookingAddress' => $asBookingAddress,
					'customer_email1' => $email1,                    
                    'customer_mobile1' => $mobile1,                    
                    'customer_phone1' => $phone1,                    
                    'customer_fax' => $fax,
					'customer_po_box' => $po_box,
					'customer_address' => get_line_address($db_params)
                );
                //'customer_name' => ($title ? ucfirst($title) . '.' : '') . ucwords($firstName) . ($lastName ? ' ' . ucwords($lastName) : ''),

                echo json_encode($data);
                exit;
            }
        }


        $this->view->form = $form;

        if ($contacts_counter) {
            $this->view->contacts_counter = $contacts_counter;
        } else {
            $this->view->contacts_counter = $counter;
        }

        //
        // render views
        //
        echo $this->view->render('index/add_edit.phtml');
        exit;
    }

    public function viewAction() {

        $customerId = $this->request->getParam('id', 0);

        //
        // load Model
        //
        $modelCustomer = new Model_Customer();
        $modelCustomerNotes = new Model_CustomerNotes();
        $modelBooking = new Model_Booking();
        $modelInquiry = new Model_Inquiry();
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelBookingEstimate = new Model_BookingEstimate();

        $customer = $modelCustomer->getById($customerId);
        $modelCustomer->fill($customer, array('customer_commercial_info', 'customer_contacts'));
        $this->view->customer_commercial_info = $customer['customer_commercial_info'];
        $this->view->customerContacts = $customer['customer_contacts'];

        if (!CheckAuth::checkIfCanHandelAllCompany('customer', $customerId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this Customer"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        if (!$customer) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Customer not found"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $checkIfCanSeeCustomer = $modelCustomer->checkIfCanSeeCustomer($customerId);
        if (!$checkIfCanSeeCustomer) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this Customer"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // get all Inquiry Status and send to view
        //   
        $inquiriesStatus = $modelInquiry->getEnamStatus();
        foreach ($inquiriesStatus as &$inquiryStatus) {
            $filters = array();
            $filters['customer_id'] = $customerId;
            $filters['inquiry_status'] = $inquiryStatus;

            $inquiryStatus = array(
                'name' => ucfirst($inquiryStatus),
                'total' => $modelInquiry->getInquiryCount($filters)
            );
        }
        $this->view->inquiriesStatus = $inquiriesStatus;

        //
        // get all Booking Statuses and send to view
        //
        $modelBookingStatus = new Model_BookingStatus();
        $bookingStatuses = $modelBookingStatus->getAll();

        foreach ($bookingStatuses as &$bookingStatus) {
            $filters = array();
            $filters['customer_id'] = $customerId;
            $filters['status_id'] = $bookingStatus['booking_status_id'];
            $filters['status'] = $bookingStatus['booking_status_id'];
           $filters['withoutEstimateStatus'] = "1";
//		if($_SERVER['REMOTE_ADDR'] == '176.106.46.142'){
//                    var_dump($filters);
//                }	
            $bookingStatus['total'] = $modelBooking->getBookingCount($filters);
            $bookingStatus['Ammount'] = number_format($modelBooking->total($filters), 2);
        }
		
		
   
        $this->view->bookingStatuses = $bookingStatuses;

        //
        // get all Estimates Type and send to view
        //   
        $modelBookingEstimate = new Model_BookingEstimate();
        $estimatesType = $modelBookingEstimate->getEnamEstimateType();

        foreach ($estimatesType as &$estimateType) {
            $filters = array();
            $filters['customer_id'] = $customerId;
            $filters['estimate_type'] = $estimateType;
			//by islam
            $filters['estimate_is_deleted'] = 'no'; 

            $estimateType = array(
                'name' => ucfirst($estimateType),
                'total' => $modelBooking->getBookingCount($filters),
                'Ammount' => number_format($modelBooking->total($filters), 2),
                'balance' => number_format($modelBooking->total($filters, 'paid_amount'), 2)
            );
        }
        $this->view->estimatesType = $estimatesType;

        //
        // get all Invoices Type and send to view
        //
        $modelBookingInvoice = new Model_BookingInvoice();
        $invoicesType = $modelBookingInvoice->getEnamInvoiceType();

        foreach ($invoicesType as &$invoiceType) {
            $filters = array();
            $filters['customer_id'] = $customerId;
            $filters['invoice_type'] = $invoiceType;

            $invoiceType = array(
                'name' => ucfirst($invoiceType),
                'total' => $modelBooking->getBookingCount($filters),
                'Ammount' => number_format($modelBooking->total($filters), 2),
                'balance' => number_format($modelBooking->total($filters, 'paid_amount'), 2)
            );
        }
        $this->view->invoicesType = $invoicesType;


        //  get all Email Customer History
        $filters = array();
        $filters['to'] = $customer['email1'];

        $modelEmailLog = new Model_EmailLog();
        if (!empty($customer['email1'])) {
            $emailLog = $modelEmailLog->getAll($filters, 'email_log_id  desc');
        } else {
            $emailLog = array();
        }
        $this->view->emailLog = $emailLog;

        $customerNotes = $modelCustomerNotes->getByCustomerId($customerId);
        $this->view->customerNotes = $customerNotes['notes'];

        $this->view->canSeeCustomer = $checkIfCanSeeCustomer;
        $this->view->customer = $customer;
    }
///////////by islam view Bussiness details all inquires, booking and estimates
	public function viewBusinessAction() {

        $businessName = $this->request->getParam('name', 0);
		$this->view->businessName=$businessName;
        //
        // load Model
        //
		$modelCustomerCommericalInfo= new Model_CustomerCommercialInfo();
        $modelCustomer = new Model_Customer();
        $modelCustomerNotes = new Model_CustomerNotes();
        $modelBooking = new Model_Booking();
        $modelInquiry = new Model_Inquiry();
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelBookingEstimate = new Model_BookingEstimate();
		
		$customerIds=$modelCustomerCommericalInfo->getCustomerIdsByBusinessName($businessName);
		$this->view->customerIds=$customerIds;
		$customerIdsArr=array();
		
		foreach($customerIds as $customerId){
			$customerIdsArr[]=$customerId['customer_id'];
		}
		
        //
        // get all Inquiry Status and send to view
        //   
        $inquiriesStatus = $modelInquiry->getEnamStatus();
        foreach ($inquiriesStatus as &$inquiryStatus) {
            $filters = array();
            $filters['customer_ids'] = $customerIdsArr;
			
            $filters['inquiry_status'] = $inquiryStatus;

            $inquiryStatus = array(
                'name' => ucfirst($inquiryStatus),
                'total' => $modelInquiry->getInquiryCount($filters)
            );
        }
        $this->view->inquiriesStatus = $inquiriesStatus;

        //
        // get all Booking Statuses and send to view
        //
        $modelBookingStatus = new Model_BookingStatus();
        $bookingStatuses = $modelBookingStatus->getAll();

        foreach ($bookingStatuses as &$bookingStatus) {
            $filters = array();
            $filters['customer_ids'] = $customerIdsArr;
            $filters['status_id'] = $bookingStatus['booking_status_id'];
            $filters['status'] = $bookingStatus['booking_status_id'];
            $bookingStatus['total'] = $modelBooking->getBookingCount($filters);
            $bookingStatus['Ammount'] = number_format($modelBooking->total($filters), 2);
        }
        $this->view->bookingStatuses = $bookingStatuses;

        //
        // get all Estimates Type and send to view
        //   
        $modelBookingEstimate = new Model_BookingEstimate();
        $estimatesType = $modelBookingEstimate->getEnamEstimateType();

        foreach ($estimatesType as &$estimateType) {
            $filters = array();
            $filters['customer_ids'] = $customerIdsArr;
            $filters['estimate_type'] = $estimateType;

            $estimateType = array(
                'name' => ucfirst($estimateType),
                'total' => $modelBooking->getBookingCount($filters),
                'Ammount' => number_format($modelBooking->total($filters), 2),
                'balance' => number_format($modelBooking->total($filters, 'paid_amount'), 2)
            );
        }
        $this->view->estimatesType = $estimatesType;

        //
        // get all Invoices Type and send to view
        //
        $modelBookingInvoice = new Model_BookingInvoice();
        $invoicesType = $modelBookingInvoice->getEnamInvoiceType();
		
		
        foreach ($invoicesType as &$invoiceType) {
            $filters = array();
			
            $filters['customer_ids'] = $customerIdsArr;
            $filters['invoice_type'] = $invoiceType;

            $invoiceType = array(
                'name' => ucfirst($invoiceType),
                'total' => $modelBooking->getBookingCount($filters),
                'Ammount' => number_format($modelBooking->total($filters), 2),
                'balance' => number_format($modelBooking->total($filters, 'paid_amount'), 2)
            );
        }
		
        $this->view->invoicesType = $invoicesType;


        //  get all Email Customer History
        /*$filters = array();
        $filters['to'] = $customer['email1'];

        $modelEmailLog = new Model_EmailLog();
        if (!empty($customer['email1'])) {
            $emailLog = $modelEmailLog->getAll($filters, 'email_log_id  desc');
        } else {
            $emailLog = array();
        }
        $this->view->emailLog = $emailLog;

        $customerNotes = $modelCustomerNotes->getByCustomerId($customerId);
        $this->view->customerNotes = $customerNotes['notes'];

        $this->view->canSeeCustomer = $checkIfCanSeeCustomer;
        $this->view->customer = $customer;*/
		//}
    }



////////////////end
    public function locationAction() {

        $customerId = $this->request->getParam('id', 0);

        if (!CheckAuth::checkIfCanHandelAllCompany('customer', $customerId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this customer"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $modelCustomer = new Model_Customer();
        $customer = $modelCustomer->getById($customerId);

        if (!$customer) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Customer not found"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }
        if (!$modelCustomer->checkIfCanSeeLocation($customerId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this Customer Location"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }

        $line_address = get_line_address($customer);
        $is_address = false;

        if (empty($line_address)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Customer Does not have address"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        } else {
            $MAP_OBJECT = new GoogleMapAPI();

            $MAP_OBJECT->setHeight(500);
            $MAP_OBJECT->setWidth('100%');
            $MAP_OBJECT->setMapType('map');

            //$MAP_OBJECT->addDirections($address, $link, 'map_directions', true);
            $MAP_OBJECT->addMarkerByAddress($line_address);

            $this->view->MAP_OBJECT = $MAP_OBJECT;
            $this->view->line_address = $line_address;
            $is_address = true;
        }

        $this->view->is_address = $is_address;
        $this->view->customer = $customer;
    }

    public function bookingLocationAction() {

        $bookingId = $this->request->getParam('id', 0);

        if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this booking"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $modelBooking = new Model_Booking();
        $booking = $modelBooking->getById($bookingId);

        if (!$booking) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Booking not found"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }
        if (!$modelBooking->checkIfCanSeeLocation($bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this Booking Location"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }

        $modelBookingAddress = new Model_BookingAddress();
        $bookingAddress = $modelBookingAddress->getByBookingId($bookingId);

        if ($bookingAddress) {

            $line_address = get_line_address($bookingAddress);
            $is_address = false;

            if (empty($line_address)) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "There are no Address for this Booking"));
                $this->_redirect($this->router->assemble(array(), 'Login'));
            } else {
                $MAP_OBJECT = new GoogleMapAPI();

                $MAP_OBJECT->setHeight(500);
                $MAP_OBJECT->setWidth('100%');
                $MAP_OBJECT->setMapType('map');

                //$MAP_OBJECT->addDirections($address, $link, 'map_directions', true);
                $MAP_OBJECT->addMarkerByAddress($line_address);

                $this->view->MAP_OBJECT = $MAP_OBJECT;
                $is_address = true;
            }
            $this->view->sub_menu = "";
            $this->view->is_sub_menu = 1;
            $this->view->is_address = $is_address;
            $this->view->booking = $booking;
            $this->view->line_address = $line_address;
        } else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "There are no Address for this Booking"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }
    }

    public function inquiryLocationAction() {

        $inquiryId = $this->request->getParam('id', 0);

        if (!CheckAuth::checkIfCanHandelAllCompany('inquiry', $inquiryId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this inquiry "));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $modelInquiry = new Model_Inquiry();
        $inquiry = $modelInquiry->getById($inquiryId);

        if (!$inquiry) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Inquiry not found"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }
        if (!$modelInquiry->checkIfCanSeeLocation($inquiryId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this Inquiry Location"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }

        $modelInquiryAddress = new Model_InquiryAddress();

        $inquiryAddress = $modelInquiryAddress->getByInquiryId($inquiryId);

        if (!$inquiryAddress) {
            $modelCustomer = new Model_Customer();
            $inquiryAddress = $modelCustomer->getById($inquiry['customer_id']);
        }

        if ($inquiryAddress) {

            $line_address = get_line_address($inquiryAddress);
            $is_address = false;

            if (empty($line_address)) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "There are no Address for this Inquiry"));
                $this->_redirect($this->router->assemble(array(), 'inquiry'));
            } else {
                $MAP_OBJECT = new GoogleMapAPI();

                $MAP_OBJECT->setHeight(500);
                $MAP_OBJECT->setWidth('100%');
                $MAP_OBJECT->setMapType('map');

                //$MAP_OBJECT->addDirections($address, $link, 'map_directions', true);
                $MAP_OBJECT->addMarkerByAddress($line_address);

                $this->view->MAP_OBJECT = $MAP_OBJECT;
                $is_address = true;
            }
            $this->view->sub_menu = "";
            $this->view->is_sub_menu = 1;
            $this->view->is_address = $is_address;
            $this->view->inquiry = $inquiry;
            $this->view->line_address = $line_address;
        } else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "There are no Address for this Inquiry"));
            $this->_redirect($this->router->assemble(array(), 'inquiry'));
        }
    }

    public function showDeletedAction() {
        //
        //authinticat
        //
        CheckAuth::checkPermission(array('showDeletedCustomer'));

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'customer_id');
        $sortingMethod = $this->request->getParam('method', 'desc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        $this->view->sub_menu = 'deleted_customer';
        //
        //get all deleted customers
        //
            $filters['is_deleted'] = 1;



        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        //
        // get data list
        //
        $customerObj = new Model_Customer();
        $this->view->data = $customerObj->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
    }

    public function deleteForeverAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('showDeletedCustomer', 'deleteCustomerForever'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
        if ($id) {
            $ids[] = $id;
        }

        $customerObj = new Model_Customer();
        foreach ($ids as $id) {
            if (CheckAuth::checkIfCanHandelAllCompany('customer', $id)) {
                $customerObj->deleteById($id);
            }
        }

        $this->_redirect($this->router->assemble(array(), 'showDeletedCustomer'));
    }

    public function sendEmailAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('sendEmail'));

//        $session = new Zend_Session_Namespace();
//        if (empty($session->go_to)) {
//            $session->go_to = (isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '');
//        }

        $customerId = $this->request->getParam('id', 0);
        $type = $this->request->getParam('type', 'customer');
        $referenceId = $this->request->getParam('reference_id', 0);

        if (!CheckAuth::checkIfCanHandelAllCompany('customer', $customerId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this customer"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $modelCustomer = new Model_Customer();
        $customer = $modelCustomer->getById($customerId);

        if (!$customer) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Customer not found"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }
        if (!$modelCustomer->checkIfCanSeeCustomer($customerId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this Customer"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }

        $to = array();
        if ($customer['email1']) {
            $to[] = $customer['email1'];
        }
        if ($customer['email2']) {
            $to[] = $customer['email2'];
        }
        if ($customer['email2']) {
            $to[] = $customer['email3'];
        }
        $to = implode(',', $to);

        if ($this->request->isPost()) {
            $dir = get_config('attachment');
            $subdir = date('Y/m/d/');
            $fullDir = $dir . '/' . $subdir;

            if (!is_dir($fullDir)) {
                mkdir($fullDir, 0777, true);
            }

            $to = $this->request->getParam('to');
            $cc = $this->request->getParam('cc');
            $subject = $this->request->getParam('subject');
            $body = $this->request->getParam('body');
            $cannedResponsesValue = $this->request->getParam('canned-responses');


            $params = array(
                'to' => $to,
                'cc' => $cc,
                'body' => $body,
                'subject' => $subject
            );

            if ($_FILES) {
                $file_name = array();
                $file_tmp_name = array();
                foreach ($_FILES['file'] as $key => $file) {

                    if ($key == 'name') {
                        foreach ($file as $key => $value) {
                            $file_name[] = $value;
                        }
                    } else if ($key == 'tmp_name') {
                        foreach ($file as $key => $value) {
                            $file_tmp_name[] = $value;
                        }
                    }
                }

                $paths = array();
                foreach ($file_name as $key => $file_name_value) {
                    if (move_uploaded_file($file_tmp_name[$key], $fullDir . basename($file_name_value))) {
                        $paths[] = $fullDir . $file_name_value;
                    }
                }
                //var_dump($paths);
                $params['attachment'] = implode(',', $paths);
            }
            $email_log = array(
                'type' => $type,
                'reference_id' => $referenceId
            );

            $error_mesages = array();
            if (EmailNotification::validation($params, $error_mesages)) {
                $success = EmailNotification::sendEmail($params, '', array(), $email_log);
                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Email sent successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Email not sent"));
                }

                echo 1;
                exit;
            }
            $this->view->error_mesages = $error_mesages;
        }

        $modelCannedResponses = new Model_CannedResponses();
        $cannedResponses = $modelCannedResponses->getAll();
        $this->view->cannedResponses = $cannedResponses;

        $this->view->to = $to;
        $this->view->cc = isset($cc) ? $cc : '';
        $this->view->subject = isset($subject) ? $subject : '';
        $this->view->body = isset($body) ? $body : '';
        $this->view->customer_id = $customerId;
        $this->view->type = $type;
        $this->view->reference_id = $referenceId;
        $this->view->cannedResponsesValue = isset($cannedResponsesValue) ? $cannedResponsesValue : 0;

        echo $this->view->render('index/send-email.phtml');
        exit;
    }

    public function customerNotesAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('canAddCustomerNote'));


        $customerId = $this->request->getParam('id', 0);
        $notes = $this->request->getParam('notes', '');

        if (!CheckAuth::checkIfCanHandelAllCompany('customer', $customerId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this customer"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        //load the modules
        //
        $modelCustomerNotes = new Model_CustomerNotes();
        $modelCustomer = new Model_Customer();

        if (!empty($notes)) {
            $customerNotes = $notes;
        } else {
            $customerNotes = $modelCustomerNotes->getByCustomerId($customerId);
        }

        $customer = $modelCustomer->getById($customerId);

        $this->view->customerNotes = $customerNotes;
        //
        // init action form
        //
        $options = array(
            'customer' => $customer,
            'customerNotes' => $customerNotes
        );

        //
        //check of the form validity
        //
        $form = new Customer_Form_CustomerNotes($options);
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                //get logged user 
                $loggedUser = CheckAuth::getLoggedUser();

                //data to be saved
                $data = array(
                    'customer_id' => $customerId,
                    'notes' => $notes,
                    'created_by' => $loggedUser['user_id']
                );
                $success = $modelCustomerNotes->save($data, $customerId);
            }

            if ($success) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "The Note have been saved."));
            } else {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "There was no change on the Note."));
            }
            $this->_redirect($this->router->assemble(array('id' => $customerId), 'customerView'));
        }
        $this->view->form = $form;

        echo $this->view->render('index/customer-notes.phtml');
        exit;
    }

    public function customerInfoAsJsonAction() {
        $customerId = $this->request->getParam('id', 0);

        if (!CheckAuth::checkIfCanHandelAllCompany('customer', $customerId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this customer"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        $modelCustomer = new Model_Customer();
        $customer = $modelCustomer->getById($customerId);

        echo json_encode(array('success' => 1, 'customer' => $customer));
        exit;
    }

    /**
     * Add new item action
     */
    public function getCustomerContactAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('customerAdd'));

        $contact_counter = $this->request->getParam('contact_counter', 1);

        echo $this->drawContactLabel($contact_counter);
        exit;
    }

    public function drawContactLabel($contact_counter, $asobject = false, $customerContacts = '') {

        $formObjects = array();
        $label_val = (isset($customerContacts['customer_contact_label_id']) ? $customerContacts['customer_contact_label_id'] : '');
        $info_val = (isset($customerContacts['contact']) ? $customerContacts['contact'] : '');


        $contactLabel = new Zend_Form_Element_Select("contact_label_{$contact_counter}");
        $contactLabel->setBelongsTo("contacts[{$contact_counter}]")
                ->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'form-control'))
                ->setRequired()
                ->setErrorMessages(array('Required' => 'Required Label'))
                ->setValue(!empty($label_val) ? $label_val : '');

        $modelCustomerContactLabel = new Model_CustomerContactLabel();
        $contactLabel->addMultiOption('', 'Select');
        $contactLabel->addMultiOptions($modelCustomerContactLabel->getContactLabelAsArray());
        $formObjects["contact_label_{$contact_counter}"] = $contactLabel;

        $info = new Zend_Form_Element_Text("info_{$contact_counter}");
        $info->setBelongsTo("contacts[{$contact_counter}]")
                ->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'form-control'))
                ->setRequired()
                ->setValue(!empty($info_val) ? $info_val : '');

        $formObjects["info_{$contact_counter}"] = $info;

        if ($asobject) {
            return $formObjects;
        } else {
            $this->view->contact_counter = $contact_counter;
            $this->view->formObjects = $formObjects;
            return $this->view->render('index/get-customer-contact.phtml');
        }
    }

}