<?php

class Customer_DuplicateCustomerController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'customers';
        $this->view->sub_menu = "duplicate_customer";
        
        $pageName = $this->request->getParam('page_name', '');
        $this->view->page_title = (!empty($pageName))?$pageName." - Customers":"Customers";
    }

    public function findDuplicateCustomerAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('findDuplicateCustomer'));

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'customer_id');
        $sortingMethod = $this->request->getParam('method', 'desc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = 15;
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        //
        // get data list
        //
        $modelCustomer = new Model_Customer();
        $loggedUser = CheckAuth::getLoggedUser();
        

        //check if loged user is contractor
        if (!CheckAuth::checkCredential(array('CanSeeAllBooking'))) {
            if (CheckAuth::checkCredential(array('CanSeeOnlyHisBooking'))) {
                if (CheckAuth::checkCredential(array('CanSeeAssignedBooking'))) {
                    $filters['created_by'] = $loggedUser['user_id'];
                }
            }
        }

        $customers = $modelCustomer->findDuplicate($filters, "{$orderBy} {$sortingMethod}", $pager);
        $modelCustomer->fills($customers,array('duplicate_count'));
        $this->view->data = $customers;
        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
    }

    public function fixDuplicateCustomerAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('fixDuplicateCustomer'));

        //
        // get request parameters
        //
        $customerId = $this->request->getParam('id');
        $title = $this->request->getParam('title');
        $firstName = $this->request->getParam('first_name');
        $lastName = $this->request->getParam('last_name');
        $customerTypeId = $this->request->getParam('customer_type_id');
        $businessName = $this->request->getParam('customer_business_name');
        $unitLotNumber = $this->request->getParam('unit_lot_number');
        $streetNumber = $this->request->getParam('street_number');
        $streetAddress = $this->request->getParam('street_address');
        $suburb = $this->request->getParam('suburb');
        $state = $this->request->getParam('customer_state');
        $postcode = $this->request->getParam('postcode');
        $po_box = $this->request->getParam('po_box');
        $cityId = $this->request->getParam('customer_city_id');
        $countryId = $this->request->getParam('customer_country_id');
        $email1 = $this->request->getParam('email1');
        $email2 = $this->request->getParam('email2');
        $email3 = $this->request->getParam('email3');
        $mobile1 = $this->request->getParam('mobile1');
        $mobile2 = $this->request->getParam('mobile2');
        $mobile3 = $this->request->getParam('mobile3');
        $phone1 = $this->request->getParam('phone1');
        $phone2 = $this->request->getParam('phone2');
        $phone3 = $this->request->getParam('phone3');
        $fax = $this->request->getParam('fax');
        $customer_ids = $this->request->getParam('customer_ids', array());
        $this->view->customer_ids = $customer_ids;

        if (get_config('remove_white_spacing')) {
            $mobile1 = preparer_number($mobile1);
            $mobile2 = preparer_number($mobile2);
            $mobile3 = preparer_number($mobile3);
            $phone1 = preparer_number($phone1);
            $phone2 = preparer_number($phone2);
            $phone3 = preparer_number($phone3);
        }

        //load model
        $modelCustomer = new Model_Customer();
        $modelCustomerCommercialInfo = new Model_CustomerCommercialInfo();
        $modelBooking = new Model_Booking();
        $modelCustomerNotes = new Model_CustomerNotes();
        $modelInquiry = new Model_Inquiry();
        $modelPayment = new Model_Payment();
        //
        //getting the customer type id to configure commercial customer
        //
        $modelCustomerType = new Model_CustomerType();
//        $customerType = $modelCustomerType->getByTypeName("Commercial");
//        $commercialId = $customerType['customer_type_id'];
//        $this->view->commercialId = $commercialId;

        $residential = $modelCustomerType->getByTypeName("Residential");
        $residentialId = $residential['customer_type_id'];
        $this->view->residentialId = $residentialId;


        $customers = $modelCustomer->getAllDuplicateCustomer($customerId);
        
        if(count($customers) > 100){
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Error In customer"));
            $this->_redirect(isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : $this->router->assemble(array(), 'findDuplicateCustomer'));
        }
        
        $modelCustomer->fills($customers, array('customer_commercial_info', 'customer_city'));

        $options = array(
            'country_id' => $countryId,
            'customer_business_name' => $businessName,
            'state' => $state,
            'residentialId' => $residentialId
        );
        $form = new Customer_Form_Customer($options);

        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                //select the oldest customer
                $customerId = min($customer_ids);

                $loggedUser = CheckAuth::getLoggedUser();
                $company_id = CheckAuth::getCompanySession();
                $db_params = array(
                    'title' => $title,
                    'first_name' => $firstName,
                    'last_name' => $lastName,
                    'city_id' => $cityId,
                    'email1' => $email1,
                    'email2' => $email2,
                    'email3' => $email3,
                    'mobile1' => $mobile1,
                    'mobile2' => $mobile2,
                    'mobile3' => $mobile3,
                    'phone1' => $phone1,
                    'phone2' => $phone2,
                    'phone3' => $phone3,
                    'fax' => $fax,
                    'customer_type_id' => $customerTypeId,
                    'unit_lot_number' => $unitLotNumber,
                    'street_number' => $streetNumber,
                    'street_address' => $streetAddress,
                    'suburb' => $suburb,
                    'state' => $state,
                    'postcode' => $postcode,
                    'po_box' => $po_box,
                    'full_name_search' => $title . '.' . $firstName . ' ' . $lastName,
                    'created_by' => $loggedUser['user_id'],
                    'company_id' => $company_id
                );

                $modelCustomer->updateById($customerId, $db_params);
                $modelCustomerCommercialInfo->setCustomerCommercialInfo($customerId, $customerTypeId, $businessName);

                unset($customer_ids[$customerId]);

                foreach ($customer_ids as $customer_id) {

                    $data = array(
                        'customer_id' => $customerId
                    );
					//by islam, we override update function in booking and inquiry models
					//we removed the cache before update the record 
                    $modelBooking->update($data, "customer_id = '{$customer_id}'");
                    $modelCustomerNotes->update($data, "customer_id = '{$customer_id}'");
                    $modelInquiry->update($data, "customer_id = '{$customer_id}'");
                    $modelPayment->update($data, "customer_id = '{$customer_id}'");

                    $modelCustomer->delete("customer_id = '{$customer_id}'");
                    $modelCustomerCommercialInfo->delete("customer_id = '{$customer_id}'");
                }

                $duplicates = $modelCustomer->getAllDuplicateCustomer(0, $db_params);
                $data = array();
                $data['duplicate'] = count($duplicates);
                $modelCustomer->updateById($customerId, $data);

                $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Duplicate customer merged"));
                $this->_redirect($this->router->assemble(array('id' => $customerId), 'customerView'));
            }
        }

        $this->view->customers = $customers;
        $this->view->form = $form;
    }

}