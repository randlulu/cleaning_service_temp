<?php

class Customer_Form_Customer extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('Customer');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $customer = (isset($options['customer']) ? $options['customer'] : '');
        $optionState = (isset($options['state']) ? $options['state'] : '');
        $countryId = (isset($options['country_id']) ? $options['country_id'] : 0);
        $as_json = (isset($options['asJson']) ? $options['asJson'] : 0);
        $commercialBusinessName = (isset($options['commercial_business_name']) ? $options['commercial_business_name'] :'');
        $residentialId = (isset($options['residentialId']) ? $options['residentialId'] : 0);
        $getBusinessName = (isset($getBusinessName) ? $getBusinessName : '');
        $asBookingAddressVal = (isset($options['as_booking_address']) && !empty($options['as_booking_address']) ? $options['as_booking_address'] : 0);

        $router = Zend_Controller_Front::getInstance()->getRouter();
        $request = Zend_Controller_Front::getInstance()->getRequest();


        $title = new Zend_Form_Element_Select('title');
        $title->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                //->setErrorMessages(array('Required' => 'Please enter the customer title'))
                ->setAttribs(array('class' => 'select_field form-control'))
                ->setValue((!empty($customer['title']) ? $customer['title'] : ''));
        $title->addMultiOption('', 'Title');
        $title->addMultiOption('dr', 'Dr.');
        $title->addMultiOption('mr', 'Mr.');
        $title->addMultiOption('mrs', 'Mrs.');
        $title->addMultiOption('miss', 'Miss.');
        $title->addMultiOption('ms', 'Ms.');


        $firstName = new Zend_Form_Element_Text('first_name');
        $firstName->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setErrorMessages(array('Required' => 'Please enter the First name'))
                ->setAttribs(array('class' => 'text_field form-control'))
                ->setValue((!empty($customer['first_name']) ? $customer['first_name'] : ''));


        $lastName = new Zend_Form_Element_Text('last_name');
        $lastName->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                //->setErrorMessages(array('Required' => 'Please enter the Last name'))
                ->setAttribs(array('class' => 'text_field form-control'))
                ->setValue((!empty($customer['last_name']) ? $customer['last_name'] : ''));
		$firstName2 = new Zend_Form_Element_Text('first_name2');
        $firstName2->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'text_field form-control'))
                ->setValue((!empty($customer['first_name2']) ? $customer['first_name2'] : ''));	
				
        $lastName2 = new Zend_Form_Element_Text('last_name2');
        $lastName2->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                //->setErrorMessages(array('Required' => 'Please enter the Last name'))
                ->setAttribs(array('class' => 'text_field form-control'))
                ->setValue((!empty($customer['last_name2']) ? $customer['last_name2'] : ''));	
				
		
		$title2 = new Zend_Form_Element_Select('title2');
        $title2->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                //->setErrorMessages(array('Required' => 'Please enter the customer title'))
                ->setAttribs(array('class' => 'select_field form-control'))
                ->setValue((!empty($customer['title2']) ? $customer['title2'] : ''));
        $title2->addMultiOption('', 'Title');
        $title2->addMultiOption('dr', 'Dr.');
        $title2->addMultiOption('mr', 'Mr.');
        $title2->addMultiOption('mrs', 'Mrs.');
        $title2->addMultiOption('miss', 'Miss.');
        $title2->addMultiOption('ms', 'Ms.');
		
		$firstName3 = new Zend_Form_Element_Text('first_name3');
        $firstName3->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'text_field form-control'))
                ->setValue((!empty($customer['first_name3']) ? $customer['first_name3'] : ''));	
				
        $lastName3 = new Zend_Form_Element_Text('last_name3');
        $lastName3->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                //->setErrorMessages(array('Required' => 'Please enter the Last name'))
                ->setAttribs(array('class' => 'text_field form-control'))
                ->setValue((!empty($customer['last_name3']) ? $customer['last_name3'] : ''));				

				
		$title3 = new Zend_Form_Element_Select('title3');
        $title3->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                //->setErrorMessages(array('Required' => 'Please enter the customer title'))
                ->setAttribs(array('class' => 'select_field form-control'))
                ->setValue((!empty($customer['title3']) ? $customer['title3'] : ''));
        $title3->addMultiOption('', 'Title');
        $title3->addMultiOption('dr', 'Dr.');
        $title3->addMultiOption('mr', 'Mr.');
        $title3->addMultiOption('mrs', 'Mrs.');
        $title3->addMultiOption('miss', 'Miss.');
        $title3->addMultiOption('ms', 'Ms.');		

        $customerTypeId = new Zend_Form_Element_Select('customer_type_id');
        $customerTypeId->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => ' form-control', 'onchange' => 'getCommercialBusinessName();'))
                ->setValue((!empty($customer['customer_type_id']) ? $customer['customer_type_id'] : ''))
                ->setErrorMessages(array('Required' => 'Please select the customer type'));
        $modelCustomerType = new Model_CustomerType();
        $customerTypeId->addMultiOption('', 'Select');
        $customerTypeId->addMultiOptions($modelCustomerType->getCustomerTypeAsArray());


        $customerBusinessName = new Zend_Form_Element_Text('customer_business_name');
        $customerBusinessName->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($commercialBusinessName) ? $commercialBusinessName : ''));

        $customer_type_id = $request->getParam('customer_type_id');
        if ($customer_type_id != $residentialId) {
            $customerBusinessName->setRequired();
            $customerBusinessName->setErrorMessages(array('Required' => 'Please select the customer business name'));
        }


        $email1 = new Zend_Form_Element_Text('email1');
        $email1->setDecorators(array('ViewHelper'))
        ->addDecorator('Errors', array('class' => 'errors'));
        //->setRequired()
        //->setErrorMessages(array('Required' => 'Please enter the email'))
        if (CheckAuth::getRoleName() != 'customer') {
        $email1->setAttribs(array('class' => 'text_field form-control', 'onkeyup' => "customerSearch();"));
        }else{
        $email1->setAttribs(array('class' => 'text_field form-control'));

        }
        $email1->setValue((!empty($customer['email1']) ? $customer['email1'] : ''));


        $email2 = new Zend_Form_Element_Text('email2');
        $email2->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'text_field form-control'))
                ->setValue((!empty($customer['email2']) ? $customer['email2'] : ''));


        $email3 = new Zend_Form_Element_Text('email3');
        $email3->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'text_field form-control'))
                ->setValue((!empty($customer['email3']) ? $customer['email3'] : ''));


        $mobile1 = new Zend_Form_Element_Text('mobile1');
        $mobile1->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setErrorMessages(array('Required' => 'Please enter the mobile1'))
                ->setAttribs(array('class' => 'text_field form-control', 'onkeyup' => "customerSearch();"))
                ->setValue((!empty($customer['mobile1']) ? substr($customer['mobile1'] , -8,8) : ''));

        $mobile2 = new Zend_Form_Element_Text('mobile2');
        $mobile2->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'text_field form-control'))
                ->setValue((!empty($customer['mobile2']) ? substr($customer['mobile2'] , -8,8) : ''));


        $mobile3 = new Zend_Form_Element_Text('mobile3');
        $mobile3->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'text_field form-control'))
                ->setValue((!empty($customer['mobile3']) ? substr($customer['mobile3'] , -8,8) : ''));


        $phone1 = new Zend_Form_Element_Text('phone1');
        $phone1->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'text_field form-control', 'onkeyup' => "customerSearch();"))
                ->setValue((!empty($customer['phone1']) ? substr($customer['phone1'] , -8,8) : ''));


        $phone2 = new Zend_Form_Element_Text('phone2');
        $phone2->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'text_field form-control'))
                ->setValue((!empty($customer['phone2']) ? substr($customer['phone2'] , -8,8) : ''));


        $phone3 = new Zend_Form_Element_Text('phone3');
        $phone3->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'text_field form-control'))
                 ->setValue((!empty($customer['phone3']) ? substr($customer['phone3'] , -8,8) : ''));
		
		 $city_obj = new Model_Cities();
            $city = $city_obj->getById((!empty($customer['city_id']) ? $customer['city_id'] : CheckAuth::getCityId()));
        /**********By IBM****************************/
        $international_key = new Zend_Form_Element_Text('international_key');
        $international_key->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'text_field form-control'))
                ->setAttribs(array('readonly' => 'true'))
                ->setValue('0061');
                // ->addMultiOption('', '0061');
        $mobile_key = new Zend_Form_Element_Text('mobile_key');
        $mobile_key->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'text_field form-control'))
                ->setAttribs(array('readonly' => 'true'))
                ->setValue(('04'));
        $phone_key = new Zend_Form_Element_Text('phone_key');
        $phone_key->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'text_field form-control'))
                ->setAttribs(array('readonly' => 'true'))
                ->setValue((!empty($city['phone_key']) ? $city['phone_key'] : ''));
		/**********End****************************/



        $fax = new Zend_Form_Element_Text('fax');
        $fax->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'text_field form-control'))
                ->setValue((!empty($customer['fax']) ? $customer['fax'] : ''));


        $unitLotNumber = new Zend_Form_Element_Text('unit_lot_number');
        $unitLotNumber->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'text_field form-control','placeHolder'=>'Unit/Lot No'))
                ->setValue((!empty($customer['unit_lot_number']) ? $customer['unit_lot_number'] : ''));


        $streetNumber = new Zend_Form_Element_Text('street_number');
        $streetNumber->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
//                ->setRequired()
//                ->setErrorMessages(array('Required' => 'Please enter the street number'))
                ->setAttribs(array('class' => 'text_field form-control','placeHolder'=>'Street No'))
                ->setValue((!empty($customer['street_number']) ? $customer['street_number'] : ''));


        $streetAddress = new Zend_Form_Element_Text('street_address');
        $streetAddress->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
//                ->setRequired()
//                ->setErrorMessages(array('Required' => 'Please enter the street address'))
                ->setAttribs(array('class' => 'text_field form-control','placeHolder'=>'Street Name'))
                ->setValue((!empty($customer['street_address']) ? $customer['street_address'] : ''));


        $suburb = new Zend_Form_Element_Text('suburb');
        $suburb->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
//                ->setRequired()
//                ->setErrorMessages(array('Required' => 'Please enter the Suburb'))
                ->setAttribs(array('class' => 'text_field form-control','placeHolder'=>'Suburb'))
                ->setValue((!empty($customer['suburb']) ? $customer['suburb'] : ''));


        $postcode = new Zend_Form_Element_Text('postcode');
        $postcode->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
//                ->setRequired()
//                ->setErrorMessages(array('Required' => 'Please enter the Postcode'))
                ->setAttribs(array('class' => 'text_field form-control','placeHolder'=>'Postcode'))
                ->setValue((!empty($customer['postcode']) ? $customer['postcode'] : ''));

        //
        //get country & city for ajax
        //
        $city_obj = new Model_Cities();
        $city = $city_obj->getById((!empty($customer['city_id']) ? $customer['city_id'] : CheckAuth::getCityId()));

        $country_id = new Zend_Form_Element_Select('customer_country_id');
        $country_id->removeDecorator('HtmlTag')
                ->removeDecorator('Label')
                ->setAttribs(array('class' => 'form-control', 'onchange' => 'getState();' , 'id'=>'customer_country_id'))
                ->setRequired()
                ->setValue((!empty($city['country_id']) ? $city['country_id'] : ''))
                ->addDecorator('Errors', array('class' => 'errors'));

        $table = new Model_Countries();
        $country_id->addMultiOption('','Select');
        foreach ($table->getCountriesAsArray() as $c) {
            $country_id->addMultiOption($c['id'], $c['name']);
        }

        $state = new Zend_Form_Element_Select('customer_state');
        $state->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control', 'onchange' => 'getCities();'))
                ->setValue((!empty($city['state']) ? $city['state'] : ''));
        $state->addMultiOption('', 'Select');
        $state->addMultiOptions($city_obj->getStateByCountryId((!empty($countryId) ? $countryId : $city['country_id'])));

        $city_id = new Zend_Form_Element_Select('customer_city_id');
        $city_id->removeDecorator('HtmlTag')
                ->removeDecorator('Label')
                ->setAttribs(array('class' => 'select_field form-control'))
                ->setRequired()
                ->setValue((!empty($city['city_id']) ? $city['city_id'] : ''))
                ->addDecorator('Errors', array('class' => 'errors'));

        $city_id->addMultiOption('','Select');
        $city_id->addMultiOptions($city_obj->getCitiesByCountryIdAndState((!empty($countryId) ? $countryId : $city['country_id']), (!empty($optionState) ? $optionState : $city['state']), true));


        $poBox = new Zend_Form_Element_Text('po_box');
        $poBox->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                //->setErrorMessages(array('Required' => 'Please enter the PO Box'))
                ->setAttribs(array('class' => 'text_field form-control'))
                ->setValue((!empty($customer['po_box']) ? $customer['po_box'] : ''));


        $asBookingAddress = new Zend_Form_Element_Checkbox('as_booking_address');
        $asBookingAddress->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'checkbox_field'))
                ->setValue($asBookingAddressVal);


        $asJson = new Zend_Form_Element_Hidden('asJson');
        $asJson->setDecorators(array('ViewHelper'))
                ->setValue($as_json);

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));


        $this->addElements(array($title, $firstName, $lastName, $title2, $firstName2, $lastName2, $title3, $firstName3, $lastName3, $asJson, $customerTypeId, $customerBusinessName, $email1, $email2, $email3,$international_key,$mobile_key,$phone_key , $mobile1, $mobile2, $mobile3, $phone1, $phone2, $phone3, $fax, $unitLotNumber, $streetNumber, $streetAddress, $state, $suburb, $country_id, $city_id, $postcode, $poBox, $asBookingAddress, $button));

        $this->setMethod('post');
        if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $customer['customer_id']), 'customerEdit'));
        } else {
            $this->setAction($router->assemble(array(), 'customerAdd'));
        }
    }

}

