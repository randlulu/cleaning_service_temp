<?php

class Customer_Form_CustomerNotes extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('CustomerNotes');
        $customer = (isset($options['customer']) ? $options['customer'] : '');
        $customerNotes = (isset($options['customerNotes']) ? $options['customerNotes'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();
        $request = Zend_Controller_Front::getInstance()->getRequest();


        $notes = new Zend_Form_Element_Textarea('notes');
        $notes->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'textarea_field form-control'))
                ->setValue((!empty($customerNotes['notes']) ? $customerNotes['notes'] : ''));
        
        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));


        $this->addElements(array($notes, $button));

        $this->setMethod('post');
        $this->setAction($router->assemble(array('id' => $customer['customer_id']), 'customerNotes'));
    }

}

