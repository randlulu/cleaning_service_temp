<?php

class Customer_Form_CustomerContactLabel extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('CustomerContactLabel');


        $router = Zend_Controller_Front::getInstance()->getRouter();
        $request = Zend_Controller_Front::getInstance()->getRequest();


        $contactLabel = new Zend_Form_Element_Select('contact_label');
        $contactLabel->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'form-control'))
                ->setRequired()
                ->setErrorMessages(array('Required' => 'Please select the Contact Label'));
        $modelCustomerContactLabel= new Model_CustomerContactLabel();
        $contactLabel->addMultiOption('', 'Select One');
        $contactLabel->addMultiOptions($modelCustomerContactLabel->getContactLabelAsArray());

        $info = new Zend_Form_Element_Text('info');
        $info->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'text_field form-control'))
                ->setRequired()
                ->setErrorMessages(array('Required' => 'Please enter the Name'));

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));


        $this->addElements(array($contactLabel, $info, $button));

//        $this->setMethod('post');
//        //$this->setAction($router->assemble(array('id' => $customer['customer_id']), 'customerNotes'));
    }

}