<?php

class Customer_Form_CustomerContactLabel extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('CustomerContactLabel');


        $router = Zend_Controller_Front::getInstance()->getRouter();
        $request = Zend_Controller_Front::getInstance()->getRequest();


        $contactLabel = new Zend_Form_Element_Select('contact_label');
        $contactLabel->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('style' => ' width: 167px; background-color: #FFFFFF; color: #000000;  font-weight: normal; height: 22px; border: 1px solid #ACD2E7;  padding: 1px; font-size: 12px; margin-right :30px;'))
                ->setRequired()
                ->setErrorMessages(array('Required' => 'Please select the Contact Label'));
        $modelCustomerContactLabel= new Model_CustomerContactLabel();
        $contactLabel->addMultiOption('', 'Select One');
        $contactLabel->addMultiOptions($modelCustomerContactLabel->getContactLabelAsArray());

        $info = new Zend_Form_Element_Text('info');
        $info->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'text_field'))
                ->setRequired()
                ->setErrorMessages(array('Required' => 'Please enter the Name'));

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'button'));


        $this->addElements(array($contactLabel, $info, $button));

//        $this->setMethod('post');
//        //$this->setAction($router->assemble(array('id' => $customer['customer_id']), 'customerNotes'));
    }

}