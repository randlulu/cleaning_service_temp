<?php

class Inquiry_Form_Inquiry extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('Inquiry');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $inquiry = (isset($options['inquiry']) ? $options['inquiry'] : '');
        $countryId = (isset($options['country_id']) ? $options['country_id'] : 0);
        $optionState = (isset($options['state']) ? $options['state'] : '');
        $cityId = (isset($options['city_id']) ? $options['city_id'] : 0);

        $router = Zend_Controller_Front::getInstance()->getRouter();
        $request = Zend_Controller_Front::getInstance()->getRequest();

        //
        //get country & city for ajax
        //
        $city_obj = new Model_Cities();
        $city = $city_obj->getById((!empty($inquiry['city_id']) ? $inquiry['city_id'] : $cityId));

        $country_id = new Zend_Form_Element_Select('country_id');
        $country_id->removeDecorator('HtmlTag')
                ->removeDecorator('Label')
                ->setAttribs(array('class' => 'form-control', 'onchange' => 'getStateByCountryId();' , 'id'=>'country'))
                ->setRequired()
                ->setValue((!empty($city['country_id']) ? $city['country_id'] : ''))
                ->addDecorator('Errors', array('class' => 'errors'));

        $table = new Model_Countries();
        $country_id->addMultiOption('', 'Select One');
        foreach ($table->getCountriesAsArray() as $c) {
            $country_id->addMultiOption($c['id'], $c['name']);
        }

        $state = new Zend_Form_Element_Select('state');
        $state->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control', 'onchange' => 'getCitiesByCountryId();'))
                ->setAttribs(array('id' => 'state'))
                ->setValue((!empty($city['state']) ? $city['state'] : ''));
        $state->addMultiOption('', 'Select One');
        $state->addMultiOptions($city_obj->getStateByCountryId((!empty($countryId) ? $countryId : $city['country_id'])));

        $city_id = new Zend_Form_Element_Select('city_id');
        $city_id->removeDecorator('HtmlTag')
                ->removeDecorator('Label')
                ->setRequired()
                ->setAttribs(array('class' => 'select_field form-control', 'onchange' => 'getServices();'))
                ->setAttribs(array('id' => 'city_cont'))
                ->setValue((!empty($city['city_id']) ? $city['city_id'] : ''))
                ->addDecorator('Errors', array('class' => 'errors'));

        $city_id->addMultiOption('', 'Select One');
        $city_id->addMultiOptions($city_obj->getCitiesByCountryIdAndState((!empty($countryId) ? $countryId : $city['country_id']), (!empty($optionState) ? $optionState : $city['state']), true));



        //
        // Inquiry type 
        //
        $inquiryTypeId = new Zend_Form_Element_Select('inquiry_type_id');
        $inquiryTypeId->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'select_field form-control', 'onchange' => 'getInquiryTypeAttributes();'))
                ->setValue((!empty($inquiry['inquiry_type_id']) ? $inquiry['inquiry_type_id'] : ''))
                ->setErrorMessages(array('Required' => 'Please select the inquiry type.'));

        $InquiryTypeObj = new Model_InquiryType();
        $InquiryTypes = $InquiryTypeObj->getAll();

        $inquiryTypeId->addMultiOption('', 'Select One');
        foreach ($InquiryTypes as $t) {
            $inquiryTypeId->addMultiOption($t['inquiry_type_id'], $t['inquiry_name']);
        }

        //
        // Customer 
        //
        $customerId = new Zend_Form_Element_Hidden('customer_id');
        $customerId->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setValue((!empty($inquiry['customer_id']) ? $inquiry['customer_id'] : ''))
                ->setErrorMessages(array('Required' => 'Please select the customer from the list.'));

        if (isset($inquiry['customer_id']) && !empty($inquiry['customer_id'])) {
            $modelCustomer = new Model_Customer();
            $customer = $modelCustomer->getById($inquiry['customer_id']);
            $customerName = get_customer_name($customer);
        }

        $autoCustomerId = new Zend_Form_Element_Text('auto_customer_id');
        $autoCustomerId->setDecorators(array('ViewHelper'))
                ->setAttribs(array('class' => 'autocomplete_search form-control' , 'id'=>'auto_customer_id' , 'onkeyup'=> 'customerSearchInput()','autocomplete'=>'off'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setValue(isset($customerName) ? $customerName : '');


        //
        // comment
        //
        $comment = new Zend_Form_Element_Textarea('comment');
        $comment->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'textarea_field form-control','rows' => '15'))
                ->setValue((!empty($inquiry['comment']) ? $inquiry['comment'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the comment'));


        //
        // deferred_date
        //
        $deferred_date_val = !empty($inquiry['deferred_date']) ? date('D-M-YYYY LT', $inquiry['deferred_date']) : '';

        $deferred_date = new Zend_Form_Element_Text('deferred_date');
        $deferred_date->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'text_field date form-control'))
                ->setAttribs(array('id' => 'deferred_date'))
                ->setValue($deferred_date_val);
        $isToFollow = $request->getParam('is_to_follow');
        if ($isToFollow) {
            $deferred_date->setRequired();
            $deferred_date->setErrorMessages(array('Required' => 'Please Select Follow Date'));
        }

        //
        // title
        //
//        $title = new Zend_Form_Element_Text('title');
//        $title->setDecorators(array('ViewHelper'))
//                ->addDecorator('Errors', array('class' => 'errors'))
//                ->setAttribs(array('class' => 'text_field'))
//                ->setValue((!empty($inquiry['title']) ? $inquiry['title'] : ''));
//        $title->setRequired();
//        $title->setErrorMessages(array('Required' => 'Please Enter Title'));
        //
        // inquiry_num
        //
        $inquiry_num = new Zend_Form_Element_Text('inquiry_num');
        $inquiry_num->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'text_field form-control'))
                ->setValue((!empty($inquiry['inquiry_num']) ? $inquiry['inquiry_num'] : ''));

        $modelPropertyType = new Model_PropertyType();
        $modelInquiryRequiredType = new Model_InquiryRequiredType();

        $property_type = new Zend_Form_Element_Select('property_type');
        $property_type->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'select_field form-control'))
                ->setValue((!empty($inquiry['property_type_id']) ? $inquiry['property_type_id'] : ''));

        $property_type->addMultiOption('', 'Select One');
        $property_type->addMultiOptions($modelPropertyType->getAllAsArray());

        $required_type = new Zend_Form_Element_Select('required_type');
        $required_type->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors form-control'))
                ->setAttribs(array('class' => 'select_field form-control'))
                ->setValue((!empty($inquiry['required_type_id']) ? $inquiry['required_type_id'] : ''));

        $required_type->addMultiOption('', 'Select One');
        $required_type->addMultiOptions($modelInquiryRequiredType->getAllAsArray());


        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'button'));


        $this->addElements(array($country_id, $customerId, $autoCustomerId, $state, $city_id, $inquiryTypeId, $deferred_date, $comment, $inquiry_num, $button, $property_type, $required_type));

        $this->setMethod('post');
        if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $inquiry['inquiry_id']), 'inquiryEdit'));
        } else {
            $this->setAction($router->assemble(array(), 'inquiryAdd'));
        }
    }

}

