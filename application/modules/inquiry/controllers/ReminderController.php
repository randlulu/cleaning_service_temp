<?php

class Inquiry_ReminderController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
    }

    public function reminderAddAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('inquiryReminderAdd'));

        //load model
        $modelUser = new Model_User();

        //
        // get request parameters
        //
        $inquiryId = $this->request->getParam('inquiry_id');
        /*   //Rand
		$modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($inquiryId, 'inquiry', 'view_add_reminders');*/

        //
        // init action form
        //
        $form = new Inquiry_Form_Reminder(array('inquiry_id' => $inquiryId));

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $comment = $this->request->getParam('comment');
                $loggedUser = CheckAuth::getLoggedUser();
                $modelInquiryReminder = new Model_InquiryReminder();


                $data = array(
                    'inquiry_id' => $inquiryId,
                    'user_id' => $loggedUser['user_id'],
                    'created' => time(),
                    'comment' => $comment
                );
                $modelInquiryReminder->insert($data);

                $reminders = array();
                $inquiryReminder = $modelInquiryReminder->getContactHistory($inquiryId);
                foreach ($inquiryReminder as $reminder) {
                    $user = $modelUser->getById($reminder['user_id']);
					$dir = get_config('user_picture_path').$user['avatar'];
				     $dir_default_pic = get_config('user_default_picture_path');
					if (isset($user['avatar']) && !empty($user['avatar'])) {
							$avatar_url= $dir;
							} 
						else {
							$avatar_url=  $dir_default_pic;
							}
                    $reminders[] = array(
					    'avatar' => $avatar_url,
                        'reminder' => $user['username'] . ' - ' . getDateFormating( $reminder['created']),
						'comment' => $reminder['comment'],
						'created' => getDateFormating( $reminder['created']),
                        'url' => $this->router->assemble(array('id' => $reminder['id']), 'inquiryReminderView'),
                        'remove_url' => $this->router->assemble(array('id' => $inquiryId, 'reminder_id' => $reminder['id']), 'inquiryReminderDelete')
                    );
                }
                $add_url = $this->router->assemble(array('inquiry_id' => $inquiryId), 'inquiryReminderAdd');

                $json_array = array('id' => $inquiryId, 'reminders' => $reminders, 'add_url' => $add_url);
                echo json_encode($json_array);
                exit;
            }
        }

        $this->view->form = $form;
        $this->view->inquiry_id = $inquiryId;
        //
        // render views
        //
        echo $this->view->render('reminder/add.phtml');
        exit;
    }

    public function reminderDeleteAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('inquiryReminderDelete'));

        //load model
        $modelUser = new Model_User();

        //
        // get request parameters
        //
        $inquiryId = $this->request->getParam('id');
        $reminderId = $this->request->getParam('reminder_id');

        $modelInquiryReminder = new Model_InquiryReminder();
        $modelInquiryReminder->deleteById($reminderId);

        $reminders = array();
        $inquiryReminder = $modelInquiryReminder->getContactHistory($inquiryId);
        foreach ($inquiryReminder as $reminder) {
           $user = $modelUser->getById($reminder['user_id']);
					$dir = get_config('user_picture_path').$user['avatar'];
				     $dir_default_pic = get_config('user_default_picture_path');
					if (isset($user['avatar']) && !empty($user['avatar'])) {
							$avatar_url= $dir;
							} 
						else {
							$avatar_url=  $dir_default_pic;
							}
                    $reminders[] = array(
					    'avatar' => $avatar_url,
                        'reminder' => $user['username'] . ' - ' . getDateFormating( $reminder['created']),
						'comment' => $reminder['comment'],
						'created' => getDateFormating( $reminder['created']),
                        'url' => $this->router->assemble(array('id' => $reminder['id']), 'inquiryReminderView'),
                        'remove_url' => $this->router->assemble(array('id' => $inquiryId, 'reminder_id' => $reminder['id']), 'inquiryReminderDelete')
                    );
        }
        $add_url = $this->router->assemble(array('inquiry_id' => $inquiryId), 'inquiryReminderAdd');

        $json_array = array('id' => $inquiryId, 'reminders' => $reminders, 'add_url' => $add_url);
        echo json_encode($json_array);
        exit;
    }
	//By RAND
	public function reminderDeleteHistoryAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('inquiryReminderDelete'));

        //load model
        $modelUser = new Model_User();

        //
        // get request parameters
        //
		$bookingId = $this->request->getParam('booking_id');
        $inquiryId = $this->request->getParam('id');
        $reminderId = $this->request->getParam('reminder_id');
       
        $modelInquiryReminder = new Model_InquiryReminder();
        $modelInquiryReminder->deleteById($reminderId);

		$modelBookingContactHistory = new Model_BookingContactHistory();
        $modelInquiryReminder = new Model_InquiryReminder();

        $modelBooking = new Model_Booking();
        $booking = $modelBooking->getById($bookingId);
		
        $reminders = array();
        $inquiryReminder = $modelInquiryReminder->getContactHistory($inquiryId);
        foreach ($inquiryReminder as $reminder) {
			$user = $modelUser->getById($reminder['user_id']);
				$dir = get_config('user_picture_path').$user['avatar'];
				     $dir_default_pic = get_config('user_default_picture_path');
					if (isset($user['avatar']) && !empty($user['avatar'])) {
							$avatar_url= $dir;
							} 
						else {
							$avatar_url=  $dir_default_pic;
							}
                $reminders[] = array(
                            'avatar' => $avatar_url,
                            'reminder' => $user['username'] . ' - ' . getDateFormating($reminder['created']),
							'comment' => $reminder['comment'],
                            'url' => $this->router->assemble(array('id' => $reminder['id']), 'inquiryReminderView'),
                            'remove_url' => $this->router->assemble(array('booking_id' => $bookingId, 'id' => $booking['original_inquiry_id'], 'reminder_id' => $reminder['id']), 'inquiryReminderDeleteHistory')
                        );
        }
        $contactHistory = array();
		if($bookingId!= null){
        $bookingContactHistories = $modelBookingContactHistory->getContactHistory($bookingId);
        foreach ($bookingContactHistories as $bookingContactHistory) {
            $user = $modelUser->getById($bookingContactHistory['user_id']);
			$dir = get_config('user_picture_path').$user['avatar'];
		    $dir_default_pic = get_config('user_default_picture_path');
					if (isset($user['avatar']) && !empty($user['avatar'])) {
							$avatar_url= $dir;
							} 
						else {
							$avatar_url=  $dir_default_pic;
							}
            $contactHistory[] = array(
					    'avatar' => $avatar_url,
                        'reminder' => $user['username'] . ' - ' . getDateFormating($bookingContactHistory['created']),
                        'url' => $this->router->assemble(array('id' => $bookingContactHistory['id']), 'bookingContactHistoryView'),
						'comment' => $bookingContactHistory['comment'],
                        'remove_url' => $this->router->assemble(array('id' => $bookingId, 'contact_history_id' => $bookingContactHistory['id']), 'bookingContactHistoryDeleteReminder')
                    );
        }
		$add_url = $this->router->assemble(array('booking_id' => $bookingId), 'bookingContactHistoryAddReminder');
		}
		else {
        $add_url = $this->router->assemble(array('inquiry_id' => $inquiryId), 'inquiryReminderAdd');
		}
		 $contactHistory = array_merge($reminders, $contactHistory); 
		
        $json_array = array('id' => $inquiryId , 'booking_id' => $bookingId, 'reminders' => $contactHistory, 'add_url' => $add_url);
        echo json_encode($json_array);
        exit;
    }
/// END BY RAND
    public function reminderViewAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('inquiryReminder'));
        
        //
        // get request parameters
        //
        $id = $this->request->getParam('id');
        $modelInquiryReminder = new Model_InquiryReminder();
        $reminder = $modelInquiryReminder->getById($id);
		
        /* //Rand
		$modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, 'inquiry', 'view_reminder');  */
        //
        // init action form
        //
        $form = new Inquiry_Form_Reminder(array('reminder' => $reminder, 'mode' => 'update'));
        $this->view->form = $form;
        
        //
        // render views
        //
        echo $this->view->render('reminder/view.phtml');
        exit;
    }

}

