<?php

class Inquiry_LabelController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
    }

    public function selectLabelAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('inquiryLabel'));

        $inquiryId = $this->request->getParam('inquiry_id');
        $label_ids = $this->request->getParam('label_ids', array());

        $modelInquiry = new Model_Inquiry();
        if (!$modelInquiry->checkIfCanSeeInquiry($inquiryId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect($this->router->assemble(array(), 'inquiry'));
        }
        //
        //load model
        //
        $modelLabel = new Model_Label();
        $modelInquiryLabel = new Model_InquiryLabel();

        $inquiryLabels = $modelInquiryLabel->getByInquiryId($inquiryId);

        $labelIds = array();
        if ($inquiryLabels) {
            foreach ($inquiryLabels as $inquiryLabel) {
                $labelIds[] = $inquiryLabel['label_id'];
            }
        }

        //
        //get data
        //
        $labels = $modelLabel->getAll();
        $this->view->labels = $labels;

        if ($this->request->isPost()) {

            $modelInquiryLabel->setLabelsToInquiry($inquiryId, $label_ids);

            if (!empty($label_ids)) {
                foreach ($label_ids as $label_id) {
                    $label = $modelLabel->getById($label_id);
                    $json_labels[$label_id] = $label['label_name'];
                }
                echo json_encode(array('result' => 1, 'labels' => $json_labels, 'entityId' => $inquiryId));
            } else {
                echo json_encode(array('result' => 0, 'entityId' => $inquiryId));
            }
            exit;
        }

        $this->view->inquiryId = $inquiryId;
        $this->view->label_ids = $labelIds;

        echo $this->view->render('label/select-label.phtml');
        exit;
    }

    public function searchLabelAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('inquiryLabel'));

        $modelLabel = new Model_Label();

        $filters['keywords'] = $this->request->getParam('label-like');
        $labels = $modelLabel->getAll($filters);
        $this->view->labels = $labels;

        echo $this->view->render('label/search-label.phtml');
        exit;
    }

    public function filterLabelAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('inquiryFilterLabel'));

        $label_ids = $this->request->getParam('label_ids', array());
        $my_inquiries = $this->request->getParam('my_inquiries', false);
        $this->view->my_inquiries = $my_inquiries;

        //
        //load model
        //
        $modelLabel = new Model_Label();

        $labels = $modelLabel->getAll();
        $this->view->labels = $labels;

        if ($this->request->isPost()) {

            $fltrLabels = array();
            if ($label_ids) {
                foreach ($label_ids as $label_id) {
                    $fltrLabels[] = 'fltr[label_ids][]=' . $label_id;
                }
            }

            $fltr = implode('&', $fltrLabels);
            if ($my_inquiries) {
                $fltr .= ($fltr ? '&' : '') . 'my_inquiries=true';
            }

            echo $this->router->assemble(array(), 'inquiry') . ($fltr ? '?' . $fltr : '');
            exit;
        }

        echo $this->view->render('label/filter-label.phtml');
        exit;
    }

    public function addLabelAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('inquiryAddLabel'));

        //
        // get request parameters
        //
        $labelName = $this->request->getParam('label_name');
        $inquiryId = $this->request->getParam('inquiry_id');

        //
        //load model
        //
        $modelLabel = new Model_Label();
        $modelInquiryLabel = new Model_InquiryLabel();

        //
        // init action form
        //
        $form = new Inquiry_Form_Label();

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'label_name' => $labelName,
                    'company_id' => CheckAuth::getCompanySession()
                );

                $labelId = $modelLabel->insert($data);

                $params = array(
                    'inquiry_id' => $inquiryId,
                    'label_id' => $labelId
                );

                $modelInquiryLabel->assignLabelToInquiry($params);

                $inquiryLabels = $modelInquiryLabel->getByInquiryId($inquiryId);
                foreach ($inquiryLabels AS $inquiryLabel) {
                    $label = $modelLabel->getById($inquiryLabel['label_id']);
                    $json_labels[$inquiryLabel['label_id']] = $label['label_name'];
                }
                echo json_encode(array('result' => 1, 'labels' => $json_labels, 'entityId' => $inquiryId));
                exit;
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('label/add_edit.phtml');
        exit;
    }

}

