<?php

class Inquiry_IndexController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'Inquiries';
        $this->view->sub_menu = 'inquiry';
        
        $pageName = $this->request->getParam('page_name', '');
        $this->view->page_title = (!empty($pageName))?$pageName." - Inquiries":"Inquiries";
    }

    /**
     * List All complaint  action
     *
     */
    public function indexAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('inquiry'));

        //
        // get request parameters
        //
		
		
		
        $orderBy = $this->request->getParam('sort', 'inquiry_id');
        $sortingMethod = $this->request->getParam('method', 'desc');
        //$currentPage = $this->request->getParam('page', 1);
        //$perPage = $this->request->getParam('perPage', 15);
        $is_first_time = $this->request->getParam('is_first_time');
        $page_number = $this->request->getParam('page_number');
        $isDeleted = $this->request->getParam('is_deleted');
        $filters = $this->request->getParam('fltr', array('status' => 'inquiry', 'unlabeled' => '1'));

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        $loggedUser = CheckAuth::getLoggedUser();
        $my_inquiries = $this->request->getParam('my_inquiries', false);
        if ($my_inquiries) {
            $this->view->sub_menu = 'my_inquiries';
            $filters['my_inquiries'] = $loggedUser['user_id'];
        }
        ////////////////test spam
        $is_spam = $this->request->getParam('is_spam', 0);
        if ($is_spam) {
            $this->view->sub_menu = 'spam_inquiry';
            $filters['is_spam'] = $is_spam;
        }
        /* if ($perPage) {
          $filters['perPage'] = $perPage;
          } */
////////////end test 
        // to get all booking that belong to this user selected from user search
        if (isset($filters['user_id']) && !empty($filters['user_id'])) {
            $filters['my_inquiries'] = $filters['user_id'];
        }

        //
        // init pager model object
        //
        /* $pager = new Model_Pager();
          $pager->perPage = !empty($filters['perPage']) ? $filters['perPage'] : get_config('perPage');
          $pager->currentPage = $currentPage;
          $pager->url = $_SERVER['REQUEST_URI']; */

        //
        // get data list
        //
        $modelInquiry = new Model_Inquiry();

        if ($isDeleted) {
            $filters['is_deleted'] = 1;
        }

        if ($this->request->isPost()) {
            if (isset($page_number)) {
                $perPage = 15;
                $currentPage = $page_number + 1;
            }

            $data = $modelInquiry->getAll($filters, "{$orderBy} {$sortingMethod}", $pager, 0, $perPage, $currentPage);
			
			
            $modelInquiry->fills($data, array('allService', 'full_customer_info', 'city', 'labels', 'reminder', 'service_attribute', 'have_attachment'));

            if (isset($filters['is_deleted'])) {
                $isDeleted = 1;
            }

			$result = array();
            $this->view->isDeleted = $isDeleted;
            $this->view->data = $data;
            $this->view->is_first_time = $is_first_time;
			$this->view->filters = $filters;
            $result['data'] =  $this->view->render('index/draw-node.phtml');
			if($data){
			   $result['is_last_request'] = 0;
			  }else{
			   $result['is_last_request'] = 1;
			  }
			  
		    echo json_encode($result);
            exit;
        }






        //$this->view->data = $data;
        //
        // set view params
        //
        //$this->view->currentPage = $currentPage;
        //$this->view->perPage = $pager->perPage;
        //$this->view->pageLinks = $pager->getPager();
        $this->view->isDeleted = $isDeleted;
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
    }

    public function addAction() {
        
        $this->view->page_title = 'New Inquiry - '.$this->view->page_title;


        //
        // check Auth for logged user
        //
        //check if the user has logged in and premission to this credintial
        //
        CheckAuth::checkPermission(array('inquiryAdd'));
		
		/*//Rand
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent(0,'inquiry','view_add');*/
         
        //
        // get request parameters
        //
        $services = $this->request->getParam('services', array());
        $inquiryTypeId = $this->request->getParam('inquiry_type_id');
        $comment = $this->request->getParam('comment');
        $cityId = $this->request->getParam('city_id');
        $countryId = $this->request->getParam('country_id');
        $state = $this->request->getParam('state');
        $customerId = $this->request->getParam('customer_id');
        $deferred_date = $this->request->getParam('deferred_date');
        //$title = $this->request->getParam('title');
		$db_format = 0;
			$dateTimeObj= get_settings_date_format();
		 if($dateTimeObj)
		 {
			$db_format = 1;
		 }
        $requiredTypeId = $this->request->getParam('required_type', 0);
        $propertyTypeId = $this->request->getParam('property_type', 0);
        $isToFollow = $this->request->getParam('is_to_follow', 0);

        $companyId = CheckAuth::getCompanySession();
        $tradingNameObj = new Model_TradingName();
        $trading_names = $tradingNameObj->getByCompanyId($companyId);

        $tradingName = $this->request->getParam('trading_name', 0);

        if (!$tradingName) {
            $defaultTradingName = $tradingNameObj->getDefualtTradingName($companyId);
            $tradingName = $defaultTradingName['trading_name_id'];
        }

        $this->view->tradingNames = $trading_names;
        //
        //to get the inquiry address from the customer info
        //
		
        if ($customerId) {
            $modelCustomer = new Model_Customer();
            $customer = $modelCustomer->getById($customerId);

            $inquiryAddress = array(
                'street_address' => $customer['street_address'],
                'street_number' => $customer['street_number'],
                'suburb' => $customer['suburb'],
                'unit_lot_number' => $customer['unit_lot_number'],
                'postcode' => $customer['postcode'],
                'po_box' => $customer['po_box']
            );
            $this->view->inquiryAddress = $inquiryAddress;
        }

        //
        //fill default country
        //
        if (empty($countryId)) {
            $countryId = CheckAuth::getCountryId();
        }

        //
        //fill default city
        //
        if (empty($cityId)) {
            $cityId = CheckAuth::getCityId();
        }

        //
        // init action form
        //
        $options = array('country_id' => $countryId, 'state' => $state);
        if ($cityId) {
            $options['city_id'] = $cityId;
        }
        //////by islam
        if ($customerId) {
            $inquiry = array();
            $inquiry['customer_id'] = $customerId;
            $options['inquiry'] = $inquiry;
        }
        /////end
        $form = new Inquiry_Form_Inquiry($options);


        //
        // load model
        //
        $contractorServiceAvailability_obj = new Model_ContractorServiceAvailability();

        //
        //get inquiryService
        //
        if ($cityId) {
            $this->view->selectedServices = $services;

            //
            //get all Service Availabile in city
            //
            $all_servicesAvailabile = $contractorServiceAvailability_obj->getServiceByCityId(array('city_id'=>$cityId),true);

            $this->view->all_servicesAvailabile = $all_servicesAvailabile;
        }


        $valid = TRUE;
        //
        //service valideter
        //
        if ($this->request->isPost()) {
            if ($cityId) {

                if (empty($services)) {

                    $this->view->errServices = 'Please select at least one service';
                    $valid = FALSE;
                } else {
                    foreach ($services as $serviceId) {

                        $contractorServiceAvailability = $contractorServiceAvailability_obj->getByCityIdAndServiceId($cityId, $serviceId);

                        if (empty($contractorServiceAvailability)) {
                            $this->view->errServices = 'At least one service selected is not available in this area';
                            $valid = FALSE;
                        }
                    }
                }
            }
        }
        //	var_dump($valid);
        //	exit;
        //
        // handling the insertion process via post
        //
        if ($this->request->isPost()) {

            if ($form->isValid($this->request->getPost()) && $valid) {

                $modelInquiry = new Model_Inquiry();
                $loggedUser = CheckAuth::getLoggedUser();
                $company_id = CheckAuth::getCompanySession();
				if($deferred_date && $db_format) 
					$deferred_date = timeFormat_zend_to_purePhp('d-m-Y H:i', $deferred_date);
                $data = array(
                    //'title' => $title,
                    'inquiry_type_id' => $inquiryTypeId,
                    'required_type_id' => $requiredTypeId,
                    'property_type_id' => $propertyTypeId,
                    'comment' => $comment,
                    'city_id' => $cityId,
                    'customer_id' => $customerId,
                    'created' => time(),
                    'user_id' => $loggedUser['user_id'],
                    'deferred_date' => strtotime($deferred_date),
                    //'full_text_search' => $title . ' ' . $comment,
                    'company_id' => $company_id,
                    'is_to_follow' => $isToFollow ? 1 : 0,
                    'trading_name_id' => $tradingName
                );


                $inquiryId = $modelInquiry->insert($data);

                //
                // insert Inquiry services and his Attribute
                //
                $modelInquiryService = new Model_InquiryService();
                $modelInquiryService->setServicesToInquiry($inquiryId, $services);

                //
                //insert Inquiry Type Attribute Values
                //
                $modelInquiryTypeAttributeValue = new Model_InquiryTypeAttributeValue();
                $modelInquiryTypeAttributeValue->setInquiryTypeAttributeValueByInquiryIdAndInquiryTypeId($inquiryId, $inquiryTypeId);

                //
                // get the params of the booking and save it in the database
                //
                $streetAddress = $this->request->getParam('street_address');
                $streetNumber = $this->request->getParam('street_number');
                $suburb = $this->request->getParam('suburb');
                $state = $this->request->getParam('state');
                $unitLotNumber = $this->request->getParam('unit_lot_number');
                $postcode = $this->request->getParam('postcode');
                $poBox = $this->request->getParam('po_box');

                $data = array(
                    'street_address' => $streetAddress,
                    'street_number' => $streetNumber,
                    'suburb' => $suburb,
                    'state' => $state,
                    'unit_lot_number' => $unitLotNumber,
                    'postcode' => $postcode,
                    'po_box' => $poBox,
                    'inquiry_id' => $inquiryId
                );

                $modelInquiryAddress = new Model_InquiryAddress();
                $modelInquiryAddress->insert($data);


                $this->_redirect($this->router->assemble(array(), 'inquiry'));
            }
        } else {
            $isToFollow = 1;
        }

        $this->view->form = $form;
        $this->view->city_id = $cityId;
        $this->view->is_to_follow = $isToFollow;

        //
        // render views
        //
        //echo $this->view->render('index/add_edit.phtml');
        //exit;
    }

    public function markAsSpamAction() {
        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
        if ($id) {
            $ids[] = $id;
        }
        $success_array = array();
        $are_marked = 0;
        if (CheckAuth::checkCredential(array('inquiryMarkAsSpam'))) {
            $modelInquiry = new Model_Inquiry();
            foreach ($ids as $id) {
                $success_array[$id] = 0;

                $modelInquiry->updateById($id, array('is_spam' => 1));
                $success_array[$id] = 1;
            }
            if (!in_array(0, $success_array)) {
                $are_marked = 1;
            } else {
                $are_marked = 0;
            }
        }

        echo json_encode(array('are_marked' => $are_marked, 'ids' => $ids));
        exit;
    }

    public function addBannedIpAction() {
        $modelInquiry = new Model_Inquiry();
        $modelBannedIpAddress = new Model_BannedIpAddress();
        //
        // get request parameters
        //
        $ip = $this->request->getParam('ip', 0);
        $done = 0;
        if (CheckAuth::checkCredential(array('addBannedIp')) && $ip != 0) {

            $data = array('ip_address' => $ip);
            $done = $modelBannedIpAddress->insert($data);
            ////mark all previous inquiries from this ip as spam
            $modelInquiry->updateByIp($ip, array('is_spam' => 1));
        }

        echo json_encode(array('done' => $done, 'ip' => $ip));
        exit;
    }

    public function removeBannedIpAction() {
        $modelInquiry = new Model_Inquiry();
        //
        // get request parameters
        //
        $ip = $this->request->getParam('ip', 0);
        $done = 0;
        if (CheckAuth::checkCredential(array('removeBannedIp')) && $ip != 0) {
            $modelBannedIpAddress = new Model_BannedIpAddress();
            $done = $modelBannedIpAddress->deleteBannedIp($ip);
            $modelInquiry->updateByIp($ip, array('is_spam' => 0));
        }

        echo json_encode(array('done' => $done, 'ip' => $ip));
        exit;
    }

    /////////////////////

    public function deleteAction() {

        //
        // check Auth for logged user
        //
         //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
		
        if ($id) {
            $ids[] = $id;
        }
        $are_deleted = 0;
        if (CheckAuth::checkCredential(array('inquiryDelete'))) {

            $modelInquiry = new Model_Inquiry();

            $success_array = array();
            foreach ($ids as $id) {
                $success_array[$id] = 0;
                if (CheckAuth::checkIfCanHandelAllCompany('inquiry', $id)) {
                    if ($modelInquiry->checkIfCanSeeInquiry($id)) {
                        $modelInquiry->updateById($id, array('is_deleted' => 1));
                        $success_array[$id] = 1;
                    }
                }
            }

            if (!in_array(0, $success_array)) {
                $are_deleted = 1;
            } else {
                $are_deleted = 0;
            }

            if (!in_array(0, $success_array)) {
                if (count($success_array) > 1) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "All selected inquiries were deleted"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Inquiry deleted"));
                }
            } else {
                if (count($success_array) > 1) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Failed to delete selected inquiries"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Failed to delete selected inquiry"));
                }
            }
        } else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to delete this inquiry"));
        }
		
            $this->_redirect($this->router->assemble(array(), 'inquiry'));
      

        
        /* echo json_encode(array('are_deleted' => $are_deleted, 'ids' =>$ids));
          exit; */
    }

    public function deleteSpamAction() {

        //
        // check Auth for logged user
        //
		//
		// Load models
        //
		$modelInquiry = new Model_Inquiry();
        $modelCustomer = new Model_Customer();
        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
        if ($id) {
            $ids[] = $id;
        }
        $are_deleted = 0;
        if (CheckAuth::checkCredential(array('inquiryDelete'))) {

            $success_array = array();
            foreach ($ids as $id) {
                $success_array[$id] = 0;
                /////get inquiry data to get customer id to delete the customer also because it is spam
                $inquiry = $modelInquiry->getById($id);
                ////now delete the customer of this inquiry
                $modelCustomer->deleteById($inquiry['customer_id']);
                ////finally delete the inquiry itself
                $is_deleted = $modelInquiry->deleteForeverById($id);
                $success_array[$id] = $is_deleted;
            }

            if (!in_array(0, $success_array)) {
                $are_deleted = 1;
            } else {
                $are_deleted = 0;
            }
        }

        echo json_encode(array('are_deleted' => $are_deleted, 'ids' => $ids));
        exit;
    }

    public function undeleteAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('inquiryUndelete'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        if (CheckAuth::checkIfCanHandelAllCompany('inquiry', $id)) {
            $modelInquiry = new Model_Inquiry();
            $success = $modelInquiry->updateById($id, array('is_deleted' => 0));
        }
        if ($success) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Selected inquiries were successfully restored"));
        } else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Failed to restore selected inquiries"));
        }
        $this->_redirect($this->router->assemble(array(), 'inquiry'));
    }

    public function editAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('inquiryEdit'));
         
		
         
		 
        $modelInquiry = new Model_Inquiry();
        $inquiryId = $this->request->getParam('id');
        
		
        if (!$modelInquiry->checkIfCanSeeInquiry($inquiryId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this inquiry"));
            $this->_redirect($this->router->assemble(array(), 'inquiry'));
        }
        if (!CheckAuth::checkIfCanHandelAllCompany('inquiry', $inquiryId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this inquiry"));
            $this->_redirect($this->router->assemble(array(), 'inquiry'));
        }
         
		/* //Rand
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($inquiryId,'inquiry','view_edit');*/
		
		
        // get request parameters
        $services = $this->request->getParam('services', array());
        $inquiryTypeId = $this->request->getParam('inquiry_type_id');
        $comment = $this->request->getParam('comment');
        $cityId = $this->request->getParam('city_id');
        $countryId = $this->request->getParam('country_id');
        $state = $this->request->getParam('state');
        $customerId = $this->request->getParam('customer_id');
        $deferred_date = $this->request->getParam('deferred_date');
		//echo "deferred_date as recieved in action: " . $deferred_date;
		
		$db_format = 0;
		$dateTimeObj= get_settings_date_format();
		 if($dateTimeObj)
		 {
			$db_format = 1;
		 }
        $title = $this->request->getParam('title');

        $streetAddress = $this->request->getParam('street_address');
        $streetNumber = $this->request->getParam('street_number');
        $suburb = $this->request->getParam('suburb');
        $state = $this->request->getParam('state');
        $unitLotNumber = $this->request->getParam('unit_lot_number');
        $postcode = $this->request->getParam('postcode');
        $poBox = $this->request->getParam('po_box');
        $requiredTypeId = $this->request->getParam('required_type', 0);
        $propertyTypeId = $this->request->getParam('property_type', 0);
        $tradingName = $this->request->getParam('trading_name', 0);




        $this->view->inquiryId = $inquiryId;

        // Validation
        $modelInquiry = new Model_Inquiry();
        $inquiry = $modelInquiry->getById($inquiryId);
        if (!$inquiry) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Inquiry not found"));
            $this->_redirect($this->router->assemble(array(), 'inquiry'));
            return;
        }

        if ($inquiry['status'] != 'inquiry') {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Failed, status is not 'Inquiry'"));
            $this->_redirect($this->router->assemble(array(), 'inquiry'));
            return;
        }

        $this->view->inquiry = $inquiry;

        $modelOriginalInquiry = new Model_OriginalInquiry();
        $originalInquiry = $modelOriginalInquiry->getById($inquiry['original_inquiry_id']);
        $this->view->originalInquiry = $originalInquiry;


        $companyId = CheckAuth::getCompanySession();
        $tradingNameObj = new Model_TradingName();
        $trading_names = $tradingNameObj->getByCompanyId($companyId);

        $this->view->tradingNames = $trading_names;

        $tradingName = $this->request->getParam('trading_name', 0);

        if (!$tradingName) {
            $defaultTradingName = $tradingNameObj->getDefualtTradingName($companyId);
            $tradingName = $defaultTradingName['trading_name_id'];
        }

        $isToFollow = !empty($inquiry['is_to_follow']) ? 1 : 0;
        if ($this->request->isPost()) {
            $isToFollow = $this->request->getParam('is_to_follow');
        }

        // init action form
        $form = new Inquiry_Form_Inquiry(array('mode' => 'update', 'inquiry' => $inquiry, 'country_id' => $countryId, 'state' => $state));

        //
        //get inquiryService
        //
        $modelInquiryService = new Model_InquiryService();
        $allInquiryService = $modelInquiryService->getByInquiryId($inquiryId);

        $selectedServices = array();
        foreach ($allInquiryService as $inquiryService) {
            $selectedServices[] = $inquiryService['service_id'] . '_' . $inquiryService['clone'];
        }
        $this->view->selectedServices = $selectedServices;

        //
        //get all Service Availabile in city
        //
        $contractorServiceAvailability_obj = new Model_ContractorServiceAvailability();
        if ($cityId) {
            $all_servicesAvailabile = $contractorServiceAvailability_obj->getServiceByCityId(array('city_id'=>$cityId),true);
        } else {
            $all_servicesAvailabile = $contractorServiceAvailability_obj->getServiceByCityId(array('city_id'=>$inquiry['city_id']),true);
        }

        $this->view->all_servicesAvailabile = $all_servicesAvailabile;

        //to return the address values
        $modelInquiryAddress = new Model_InquiryAddress();
        $inquiryAddress = $modelInquiryAddress->getByInquiryId($inquiryId);

        if (!$inquiryAddress) {
            $modelCustomer = new Model_Customer();
            $customer = $modelCustomer->getById($inquiry['customer_id']);

            $inquiryAddress = array();
            $inquiryAddress['street_address'] = $customer['street_address'];
            $inquiryAddress['street_number'] = $customer['street_number'];
            $inquiryAddress['suburb'] = $customer['suburb'];
            $inquiryAddress['unit_lot_number'] = $customer['unit_lot_number'];
            $inquiryAddress['postcode'] = $customer['postcode'];
            $inquiryAddress['po_box'] = $customer['po_box'];
        }


        $temp_address = array();
        $temp_address['street_address'] = $this->request->getParam('street_address', $inquiryAddress['street_address']);
        $temp_address['street_number'] = $this->request->getParam('street_number', $inquiryAddress['street_number']);
        $temp_address['suburb'] = $this->request->getParam('suburb', $inquiryAddress['suburb']);
        $temp_address['unit_lot_number'] = $this->request->getParam('unit_lot_number', $inquiryAddress['unit_lot_number']);
        $temp_address['postcode'] = $this->request->getParam('postcode', $inquiryAddress['postcode']);
        $temp_address['po_box'] = $this->request->getParam('po_box', $inquiryAddress['po_box']);

        $inquiryAddress = $temp_address;
        $this->view->inquiryAddress = $inquiryAddress;

        $valid = TRUE;
        //
        //service valideter
        //
        if ($this->request->isPost()) {
            $this->view->selectedServices = $services;

            if ($cityId) {
                if (empty($services)) {
                    $this->view->errServices = 'Please select at least one service';
                    $valid = FALSE;
                }
            }
        }


        //
        // handling the insertion process via post
        //
        if ($this->request->isPost()) {
            if ($form->isValid($this->request->getPost()) && $valid) {
                $loggedUser = CheckAuth::getLoggedUser();
				//$dateTimeObj= get_settings_date_format();
				$timeZoneString  =  get_timeZone($dateTimeObj['time_format']);
				if($deferred_date && $db_format) 
					$deferred_date = timeFormat_zend_to_purePhp('d-m-Y H:i', $deferred_date);

                $data = array(
                    'title' => $title,
                    'inquiry_type_id' => $inquiryTypeId,
                    'required_type_id' => $requiredTypeId,
                    'property_type_id' => $propertyTypeId,
                    'comment' => $comment,
                    'city_id' => $cityId,
                    'customer_id' => $customerId,
                    'deferred_date' => strtotime($deferred_date),
                    'full_text_search' => $title . ' ' . $comment,
                    'is_to_follow' => $isToFollow ? 1 : 0,
                    'trading_name_id' => $tradingName
                );

                $modelInquiry->updateById($inquiryId, $data);

                //
                // insert Inquiry services and his Attribute
                //
                $modelInquiryService = new Model_InquiryService();
                $modelInquiryService->setServicesToInquiry($inquiryId, $services);

                //
                //insert Inquiry Type Attribute Values
                //
                $modelInquiryTypeAttributeValue = new Model_InquiryTypeAttributeValue();
                $modelInquiryTypeAttributeValue->setInquiryTypeAttributeValueByInquiryIdAndInquiryTypeId($inquiryId, $inquiryTypeId);

                //
                // get the params of the booking and updaate it in the database
                //
                $data = array(
                    'street_address' => $streetAddress,
                    'street_number' => $streetNumber,
                    'suburb' => $suburb,
                    'state' => $state,
                    'unit_lot_number' => $unitLotNumber,
                    'postcode' => $postcode,
                    'po_box' => $poBox,
                );
                if (CheckAuth::checkCredential(array('editInquiryAddress'))) {

                    $modelInquiryAddress = new Model_InquiryAddress();
                    $modelInquiryAddress->updateByInquiryId($inquiryId, $data);
                } else {
                    if (CheckAuth::checkCredential(array('editHisInquiryAddress'))) {

                        if ($loggedUser['user_id'] == $inquiry['user_id']) {
                            $modelInquiryAddress = new Model_InquiryAddress();
                            $modelInquiryAddress->updateByInquiryId($inquiryId, $data);
                        }
                    }
                }
                $this->_redirect($this->router->assemble(array(), 'inquiry'));
            }
        }
		/*$values = $form->getValues();
		var_dump($values);*/
        $this->view->form = $form;
        $this->view->is_to_follow = $isToFollow;
    }

    /*
      public function convertToBookingAction() {

      //
      // check Auth for logged user
      //
      CheckAuth::checkPermission(array('inquiryConvertToBooking'));

      // get request parameters
      $id = $this->request->getParam('id');

      if (!CheckAuth::checkIfCanHandelAllCompany('inquiry', $id)) {
      $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Inquiry not belongs to your Company"));
      $this->_redirect($this->router->assemble(array(), 'inquiry'));
      }
      $modelInquiry = new Model_Inquiry();

      if (!$modelInquiry->checkIfCanSeeInquiry($id)) {
      $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this Inquiry"));
      $this->_redirect($this->router->assemble(array(), 'inquiry'));
      }

      //D.A 14/09/2015 Remove inquiry Cache
      require_once 'Zend/Cache.php';
      $inquiryDetailsCacheID= $id.'_inquiryDetails';
      $company_id = CheckAuth::getCompanySession();
      $inquiryViewDir=get_config('cache').'/'.'inquiriesView'.'/'.$company_id;
      if (!is_dir($inquiryViewDir)) {
      mkdir($inquiryViewDir, 0777, true);
      }
      $frontEndOption= array('lifetime'=> 24 * 3600,
      'automatic_serialization'=> true);
      $backendOptions = array('cache_dir'=>$inquiryViewDir );
      $cache = Zend_Cache::factory('Core','File',$frontEndOption,$backendOptions);
      $cache->remove($inquiryDetailsCacheID);

      $this->_redirect('booking-add?inquiry_id=' . $id);
      exit;
      }
     */

    public function convertToBookingAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('inquiryConvertToBooking'));

        // get request parameters
        $id = $this->request->getParam('id');

        if (!CheckAuth::checkIfCanHandelAllCompany('inquiry', $id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this inquiry"));
            $this->_redirect($this->router->assemble(array(), 'inquiry'));
        }
        $modelInquiry = new Model_Inquiry();

        if (!$modelInquiry->checkIfCanSeeInquiry($id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this Inquiry"));
            $this->_redirect($this->router->assemble(array(), 'inquiry'));
        }

        //By Mona
        $inquiry = $modelInquiry->getById($id);
        if ($inquiry['status'] != 'inquiry') {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Inquiry is already converted to" . $inquiry['status'] . " "));
            $this->_redirect($this->router->assemble(array(), 'inquiry'));
        }

        //D.A 14/09/2015 Remove inquiry Cache
        require_once 'Zend/Cache.php';
        $inquiryDetailsCacheID = $id . '_inquiryDetails';
        $company_id = CheckAuth::getCompanySession();
        $inquiryViewDir = get_config('cache') . '/' . 'inquiriesView' . '/' . $company_id;
        if (!is_dir($inquiryViewDir)) {
            mkdir($inquiryViewDir, 0777, true);
        }
        $frontEndOption = array('lifetime' => NULL,
            'automatic_serialization' => true);
        $backendOptions = array('cache_dir' => $inquiryViewDir);
        $cache = Zend_Cache::factory('Core', 'File', $frontEndOption, $backendOptions);
        $cache->remove($inquiryDetailsCacheID);

        $this->_redirect('booking-add?inquiry_id=' . $id);
        exit;
    }

    /*
      public function convertToEstimateAction() {
      //
      // check Auth for logged user
      //
      CheckAuth::checkPermission(array('inquiryConvertToEstimate'));

      // get request parameters
      $id = $this->request->getParam('id');

      if (!CheckAuth::checkIfCanHandelAllCompany('inquiry', $id)) {
      $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Inquiry not belongs to your Company"));
      $this->_redirect($this->router->assemble(array(), 'inquiry'));
      }

      $modelInquiry = new Model_Inquiry();

      if (!$modelInquiry->checkIfCanSeeInquiry($id)) {
      $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this Inquiry"));
      $this->_redirect($this->router->assemble(array(), 'inquiry'));
      }

      //D.A 14/09/2015 Remove inquiry Cache
      require_once 'Zend/Cache.php';
      $inquiryDetailsCacheID= $id.'_inquiryDetails';
      $company_id = CheckAuth::getCompanySession();
      $inquiryViewDir=get_config('cache').'/'.'inquiriesView'.'/'.$company_id;
      if (!is_dir($inquiryViewDir)) {
      mkdir($inquiryViewDir, 0777, true);
      }
      $frontEndOption= array('lifetime'=> 24 * 3600,
      'automatic_serialization'=> true);
      $backendOptions = array('cache_dir'=>$inquiryViewDir );
      $cache = Zend_Cache::factory('Core','File',$frontEndOption,$backendOptions);
      $cache->remove($inquiryDetailsCacheID);

      $this->_redirect('booking-add?inquiry_id=' . $id . '&toEstimate=1');
      exit;
      }
     */

    public function convertToEstimateAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('inquiryConvertToEstimate'));

        // get request parameters
        $id = $this->request->getParam('id');

        if (!CheckAuth::checkIfCanHandelAllCompany('inquiry', $id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this inquiry"));
            $this->_redirect($this->router->assemble(array(), 'inquiry'));
        }

        $modelInquiry = new Model_Inquiry();

        if (!$modelInquiry->checkIfCanSeeInquiry($id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this Inquiry"));
            $this->_redirect($this->router->assemble(array(), 'inquiry'));
        }

        //By Mona
        $inquiry = $modelInquiry->getById($id);
        if ($inquiry['status'] != 'inquiry') {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Inquiry is already converted to" . $inquiry['status'] . " "));
            $this->_redirect($this->router->assemble(array(), 'inquiry'));
        }


        //D.A 14/09/2015 Remove inquiry Cache
        require_once 'Zend/Cache.php';
        $inquiryDetailsCacheID = $id . '_inquiryDetails';
        $company_id = CheckAuth::getCompanySession();
        $inquiryViewDir = get_config('cache') . '/' . 'inquiriesView' . '/' . $company_id;
        if (!is_dir($inquiryViewDir)) {
            mkdir($inquiryViewDir, 0777, true);
        }
        $frontEndOption = array('lifetime' => NULL,
            'automatic_serialization' => true);
        $backendOptions = array('cache_dir' => $inquiryViewDir);
        $cache = Zend_Cache::factory('Core', 'File', $frontEndOption, $backendOptions);
        $cache->remove($inquiryDetailsCacheID);

        $this->_redirect('booking-add?inquiry_id=' . $id . '&toEstimate=1');
        exit;
    }

    public function editDescriptionAction() {

        $inquiryId = $this->request->getParam('id', 0);
        $comment = $this->request->getParam('comment', 0);
        $data = array('comment' => $comment);
        $modelInquiry = new Model_Inquiry();
        $success = $modelInquiry->updateById($inquiryId, $data);

        if ($success) {
            echo 1;
            exit;
        }

        exit;
    }

    /*
      public function viewAction() {

      //
      // check Auth for logged user
      //
      CheckAuth::checkPermission(array('inquiryView'));
      $modelInquiry = new Model_Inquiry();

      // get request parameters
      $inquiryId = $this->request->getParam('id');

      if (!CheckAuth::checkIfCanHandelAllCompany('inquiry', $inquiryId)) {
      $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Inquiry not belongs to your Company"));
      $this->_redirect($this->router->assemble(array(), 'inquiry'));
      }
      if (!$modelInquiry->checkIfCanSeeInquiry($inquiryId)) {
      $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this Inquiry"));
      $this->_redirect($this->router->assemble(array(), 'inquiry'));
      }


      // Validation
      $inquiry = $modelInquiry->getById($inquiryId);
      if (!$inquiry) {
      $this->_redirect($this->router->assemble(array(), 'inquiry'));
      return;
      }

      $this->view->inquiry = $inquiry;

      $modelInquiryLabel = new Model_InquiryLabel();
      $modelLabel = new Model_Label();
      $inquiryLabels = $modelInquiryLabel->getByInquiryId($inquiryId);
      if ($inquiryLabels) {
      foreach ($inquiryLabels as &$inquiryLabel) {
      $inquiryLabel['label'] = $modelLabel->getById($inquiryLabel['label_id']);
      }
      }
      $this->view->inquiryLabels = $inquiryLabels;


      $modelCustomer = new Model_Customer();
      $customer = $modelCustomer->getById($inquiry['customer_id']);


      // customer type work order is_required Message

      $modelCustomerType = new Model_CustomerType();
      $isWorkOrder = false;
      $workOrder = $modelCustomerType->getCustomerTypeIsWorkOrder();

      if (in_array($customer['customer_type_id'], $workOrder)) {
      $modelBookingAttachment = new Model_BookingAttachment();
      $bookingAttachments = $modelBookingAttachment->getByInquiryId($inquiryId);
      $isWorkOrder = true;
      if (!empty($bookingAttachments)) {
      foreach ($bookingAttachments as $attachment) {
      if ($attachment['work_order'] == 1) {
      $isWorkOrder = false;
      }
      }
      }
      }
      $this->view->isWorkOrder = $isWorkOrder;


      $modelServices = new Model_Services();
      $modelInquiryService = new Model_InquiryService();
      $modelServiceAttribute = new Model_ServiceAttribute();
      $modelAttributeListValue = new Model_AttributeListValue();
      $modelInquiryServiceAttributeValue = new Model_InquiryServiceAttributeValue();
      $modelAttributeType = new Model_AttributeType();

      $modelContractorService = new Model_ContractorService();
      $modelInquiryAddress = new Model_InquiryAddress();
      $modelBookingAddress = new Model_BookingAddress();
      $modelUser = new Model_User();


      $removeRedundancy = array();
      $allInquiryService = $modelInquiryService->getByInquiryId($inquiryId);
      foreach ($allInquiryService as &$inquiryService) {
      ///set service ids in array to get distance between them and contractors
      $removeRedundancy[$inquiryService['service_id']] = $inquiryService['service_id'];
      //get servise
      $service = $modelServices->getById($inquiryService['service_id']);

      $inquiryService['service_name'] = $service['service_name'];

      //get service attribute
      $allServiceAttribute = $modelServiceAttribute->getAttributeByServiceId($inquiryService['service_id'], 0);

      $attributes = array();
      foreach ($allServiceAttribute as &$serviceAttribute) {
      $inquiryServiceAttributeValue = $modelInquiryServiceAttributeValue->getByInquiryIdAndServiceAttributeIdAndClone($inquiryId, $serviceAttribute['service_attribute_id'], $inquiryService['clone']);
      $attributeType = $modelAttributeType->getById($serviceAttribute['attribute_type_id']);

      $value = '';
      if ($attributeType['is_list']) {
      if ($inquiryServiceAttributeValue['is_serialized_array']) {
      $unserializeValues = unserialize($inquiryServiceAttributeValue['value']);
      if (is_array($unserializeValues)) {
      $values = array();
      foreach ($unserializeValues as $unserializeValue) {
      $attributeListValue = $modelAttributeListValue->getById($unserializeValue);
      if ($attributeListValue['unit_price']) {
      $values[] = number_format($attributeListValue['unit_price'], 2);
      } else {
      $values[] = $attributeListValue['attribute_value'];
      }
      }
      $value = $values;
      }
      } else {
      $attributeListValue = $modelAttributeListValue->getById($inquiryServiceAttributeValue['value']);
      if ($attributeListValue['unit_price']) {
      $value = number_format($attributeListValue['unit_price'], 2);
      } else {
      $value = $attributeListValue['attribute_value'];
      }
      }
      } else {
      $value = $inquiryServiceAttributeValue['value'];
      }

      $serviceAttribute['value'] = $value;
      $inquiryService['attributes'] = $allServiceAttribute;
      }
      }
      $this->view->services = $allInquiryService;

      $modelOriginalInquiry = new Model_OriginalInquiry();
      $originalInquiry = $modelOriginalInquiry->getById($inquiry['original_inquiry_id']);
      $this->view->originalInquiry = $originalInquiry;

      // Booking or Etimate Number
      $modelBooking = new Model_Booking();
      $booking = $modelBooking->getByInquiryId($inquiryId);

      if (!empty($booking)) {
      if ($booking['convert_status'] == 'estimate') {
      $modelBookingEstimate = new Model_BookingEstimate();
      $estimate = $modelBookingEstimate->getByBookingId($booking['booking_id']);
      $this->view->estimate = $estimate;
      } else {
      $this->view->booking = $booking;
      if ($booking['convert_status'] == 'invoice') {
      $modelBookingInvoice = new Model_BookingInvoice();
      $invoice = $modelBookingInvoice->getByBookingId($booking['booking_id']);
      $this->view->invoice = $invoice;
      }
      }
      }


      /////get Contractors Distances
      // get inquiry details by id to get city_id
      $cityId = $inquiry['city_id'];

      // get inquiry address by id, we need it to calculate the distance between it and contractor address
      $inquiryAddress =$modelInquiryAddress->getByInquiryId($inquiryId);

      $service_ids = $removeRedundancy;

      /// get get contractor ids by city_id and service_id
      if (!empty($service_ids)) {
      foreach ($service_ids as $service_id) {
      if ($cityId) {
      $ContractorServices = $modelContractorService->getContractorByServiceIdAndCityId($service_id, $cityId);
      } else {
      $ContractorServices = $modelContractorService->getContractorByServiceId($service_id);
      }
      foreach ($ContractorServices as $ContractorService) {
      $contractor_ids[$ContractorService['contractor_id']] = $ContractorService['contractor_id'];
      }
      }
      }

      /// here calculate the distance between each contractor and inquiry address
      $contractorDistances = array();
      if(!empty($contractor_ids)){
      foreach ($contractor_ids as $contractorId) {
      //// delete inquiries from contractor list
      if($contractorId != 1){
      $contractor = $modelUser->getById($contractorId);

      $contractorDistances[$contractor['user_id']] = array(
      'contractor_id' => $contractor['user_id'],
      'name' => $contractor['username'],
      'email1' => $contractor['email1'],
      'distance' => $modelBookingAddress->getDistanceByTwoAddress($contractor, $inquiryAddress)
      );
      }
      }
      }

      $modelImageAttachment = new Model_Image();
      $pager = null ;
      $this->view->photo = $modelImageAttachment->getAll($inquiryId , 'inquiry', 'iu.created desc' , $pager, $filter = array() , $limit = 10);
      $this->view->type = 'inquiry';
      $this->view->photoCount = count($modelImageAttachment->getAll($inquiryId , 'inquiry', 'iu.created desc'));


      $this->view->contractorDistances = $contractorDistances;

      }
     */

    public function viewAction() {

        $model_cronJob = new Model_CronJob();
       
         
//       if($_SERVER['REMOTE_ADDR'] == '176.106.46.142'){
//            $modelInquiry = new Model_Inquiry();
////            $modelInquiry->cronJobFaqCustomerEveryDay();
//       }
//        $data = array(
//            'cronjob_name' => 'customer_rate',
//            'every' => '0 14 * * *',
//            'php_code' => '//send email to customer to rate(review) contractors of his booking 
//             $modelBooking = new Model_Booking();
//           $modelBooking->cronJobCustomerRate();',
//            'email_template_id' => 45,
//            'running' => 1,
//            'description' => '//send email to customer to rate(review) contractors of his booking '
//        );
//        $model_cronJob->insert($data);
        //
        // check Auth for logged user
        //
        
		
		
        $modelInquiry = new Model_Inquiry();

        // get request parameters
        $inquiryId = $this->request->getParam('id');
		 
			   if(my_ip('176.58.72.165')){
		         var_dump(CheckAuth::getLoggedUser());
		       }
		
        if (CheckAuth::getRoleName() != 'customer') {
		    CheckAuth::checkPermission(array('inquiryView'));
        if (!CheckAuth::checkIfCanHandelAllCompany('inquiry', $inquiryId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this inquiry"));
            $this->_redirect($this->router->assemble(array(), 'inquiry'));
        }
        
            if (!$modelInquiry->checkIfCanSeeInquiry($inquiryId)) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this inquiry"));
                $this->_redirect($this->router->assemble(array(), 'inquiry'));
            }
        }
		
		 //Rand
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($inquiryId,'inquiry','viewed');
		
		
		

        require_once 'Zend/Cache.php';
        $company_id = CheckAuth::getCompanySession();
        $inquiryViewDir = get_config('cache') . '/' . 'inquiriesView' . '/' . $company_id;
        if (!is_dir($inquiryViewDir)) {
            mkdir($inquiryViewDir, 0777, true);
        }
        $frontEndOption = array('lifetime' => NULL,
            'automatic_serialization' => true);
        $backendOptions = array('cache_dir' => $inquiryViewDir);
        $cache = Zend_Cache::factory('Core', 'File', $frontEndOption, $backendOptions);

        // Booking or Etimate Number
        $modelBooking = new Model_Booking();
        $booking = $modelBooking->getByInquiryId($inquiryId);
        $estimate = '';
        $invoice = '';
        //D.A 10/09/2015 inquiry Details Cache
        $cacheID = $inquiryId . '_inquiryDetails';
        if (($result = $cache->load($cacheID)) === false) {
            $inquiry = $modelInquiry->getById($inquiryId);
            if (!empty($booking)) {
                if ($booking['convert_status'] == 'estimate') {
                    $modelBookingEstimate = new Model_BookingEstimate();
                    $estimate = $modelBookingEstimate->getByBookingId($booking['booking_id']);
                } else {
                    $this->view->booking = $booking;
                    if ($booking['convert_status'] == 'invoice') {
                        $modelBookingInvoice = new Model_BookingInvoice();
                        $invoice = $modelBookingInvoice->getByBookingId($booking['booking_id']);
                    }
                }
            }
            $result = array(
                'inquiry' => $inquiry,
                'estimate' => $estimate,
                'booking' => $booking,
                'invoice' => $invoice
            );
            if ($result) {
                $cache->save($result, $cacheID);
            }
        } else {
            $inquiry = $result['inquiry'];
            $estimate = $result['estimate'];
            $booking = $result['booking'];
            $invoice = $result['invoice'];
        }
		
	   

        if (!$inquiry) {
		  
            $this->_redirect($this->router->assemble(array(), 'inquiry'));
            return;
        }
        
        $this->view->page_title = $inquiry['inquiry_num'].' - '.$this->view->page_title;

        $this->view->inquiry = $inquiry;
        if ($booking['convert_status'] == 'estimate') {
            $this->view->estimate = $estimate;
        } else {
            $this->view->booking = $booking;
            if ($booking['convert_status'] == 'invoice') {
                $this->view->invoice = $invoice;
            }
        }

        //D.A 10/09/2015 inquiry Labels Cache
        $cacheID = $inquiryId . '_inquiryLabels';
        if (($result = $cache->load($cacheID)) === false) {
            $modelInquiryLabel = new Model_InquiryLabel();
            $modelLabel = new Model_Label();
            $inquiryLabels = $modelInquiryLabel->getByInquiryId($inquiryId);
            if ($inquiryLabels) {
                foreach ($inquiryLabels as &$inquiryLabel) {
                    $inquiryLabel['label'] = $modelLabel->getById($inquiryLabel['label_id']);
                }
            }
            $result = $inquiryLabels;
            if ($result) {
                $cache->save($result, $cacheID);
            }
        } else {
            $inquiryLabels = $result;
        }

        $this->view->inquiryLabels = $inquiryLabels;


        $modelCustomer = new Model_Customer();
        $customer = $modelCustomer->getById($inquiry['customer_id']);

        // customer type work order is_required Message
        $modelCustomerType = new Model_CustomerType();
        $isWorkOrder = false;
        $workOrder = $modelCustomerType->getCustomerTypeIsWorkOrder();

        if (in_array($customer['customer_type_id'], $workOrder)) {
            $modelBookingAttachment = new Model_BookingAttachment();
            $bookingAttachments = $modelBookingAttachment->getByInquiryId($inquiryId);
            $isWorkOrder = true;
            if (!empty($bookingAttachments)) {
                foreach ($bookingAttachments as $attachment) {
                    if ($attachment['work_order'] == 1) {
                        $isWorkOrder = false;
                    }
                }
            }
        }
        $this->view->isWorkOrder = $isWorkOrder;

        $modelInquiryAddress = new Model_InquiryAddress();
        $removeRedundancy = array();
        //D.A 10/09/2015 inquiry Services Cache
        $cacheID = $inquiryId . '_inquiryServices';
        if (($result = $cache->load($cacheID)) === false) {
            $modelServices = new Model_Services();
            $modelInquiryService = new Model_InquiryService();
            $modelServiceAttribute = new Model_ServiceAttribute();
            $modelAttributeListValue = new Model_AttributeListValue();
            $modelInquiryServiceAttributeValue = new Model_InquiryServiceAttributeValue();
            $modelAttributeType = new Model_AttributeType();
            $modelContractorService = new Model_ContractorService();
            $modelBookingAddress = new Model_BookingAddress();
            $modelUser = new Model_User();

            $allInquiryService = $modelInquiryService->getByInquiryId($inquiryId);
            foreach ($allInquiryService as &$inquiryService) {
                ///set service ids in array to get distance between them and contractors
                $removeRedundancy[$inquiryService['service_id']] = $inquiryService['service_id'];
                //get servise
                $service = $modelServices->getById($inquiryService['service_id']);

                $inquiryService['service_name'] = $service['service_name'];

                //get service attribute
                $allServiceAttribute = $modelServiceAttribute->getAttributeByServiceId($inquiryService['service_id'], 0);

                $attributes = array();
                foreach ($allServiceAttribute as &$serviceAttribute) {
                    $inquiryServiceAttributeValue = $modelInquiryServiceAttributeValue->getByInquiryIdAndServiceAttributeIdAndClone($inquiryId, $serviceAttribute['service_attribute_id'], $inquiryService['clone']);
                    $attributeType = $modelAttributeType->getById($serviceAttribute['attribute_type_id']);

                    $value = '';
                    if ($attributeType['is_list']) {
                        if ($inquiryServiceAttributeValue['is_serialized_array']) {
                            $unserializeValues = unserialize($inquiryServiceAttributeValue['value']);
                            if (is_array($unserializeValues)) {
                                $values = array();
                                foreach ($unserializeValues as $unserializeValue) {
                                    $attributeListValue = $modelAttributeListValue->getById($unserializeValue);
                                    if ($attributeListValue['unit_price']) {
                                        $values[] = number_format($attributeListValue['unit_price'], 2);
                                    } else {
                                        $values[] = $attributeListValue['attribute_value'];
                                    }
                                }
                                $value = $values;
                            }
                        } else {
                            $attributeListValue = $modelAttributeListValue->getById($inquiryServiceAttributeValue['value']);
                            if ($attributeListValue['unit_price']) {
                                $value = number_format($attributeListValue['unit_price'], 2);
                            } else {
                                $value = $attributeListValue['attribute_value'];
                            }
                        }
                    } else {
                        $value = $inquiryServiceAttributeValue['value'];
                    }

                    $serviceAttribute['value'] = $value;
                    $inquiryService['attributes'] = $allServiceAttribute;
                }
            }
            $result = $allInquiryService;
            if ($result) {
                $cache->save($result, $cacheID);
            }
        } else {
            $allInquiryService = $result;
        }
        $this->view->services = $allInquiryService;

        //D.A 10/09/2015 original Inquiry Cache
        $cacheID = $inquiryId . '_originalInquiry';
        if (($result = $cache->load($cacheID)) === false) {
            $modelOriginalInquiry = new Model_OriginalInquiry();
            $originalInquiry = $modelOriginalInquiry->getById($inquiry['original_inquiry_id']);
            $result = $originalInquiry;
            if ($result) {
                $cache->save($result, $cacheID);
            }
        } else {
            $originalInquiry = $result;
        }
        $this->view->originalInquiry = $originalInquiry;

        //D.A 10/09/2015 inquiry Available Technicians Cache
        $cacheID = $inquiryId . '_inquiryAvailableTechnicians';
        if (($result = $cache->load($cacheID)) === false) {
            /////get Contractors Distances
            // get inquiry details by id to get city_id 
            $cityId = $inquiry['city_id'];

            // get inquiry address by id, we need it to calculate the distance between it and contractor address
            $inquiryAddress = $modelInquiryAddress->getByInquiryId($inquiryId);
            $service_ids = $removeRedundancy;

            /// get get contractor ids by city_id and service_id
            if (!empty($service_ids)) {
                foreach ($service_ids as $service_id) {
                    if ($cityId) {
                        $ContractorServices = $modelContractorService->getContractorByServiceIdAndCityId($service_id, $cityId);
                    } else {
                        $ContractorServices = $modelContractorService->getContractorByServiceId($service_id);
                    }
                    foreach ($ContractorServices as $ContractorService) {
                        $contractor_ids[$ContractorService['contractor_id']] = $ContractorService['contractor_id'];
                    }
                }
            }

            /// here calculate the distance between each contractor and inquiry address
            $contractorDistances = array();
            if (!empty($contractor_ids)) {
                foreach ($contractor_ids as $contractorId) {
                    //// delete inquiries from contractor list
                    if ($contractorId != 1) {
                        $contractor = $modelUser->getById($contractorId);

                        $contractorDistances[$contractor['user_id']] = array(
                            'contractor_id' => $contractor['user_id'],
                            'name' => $contractor['username'],
                            'email1' => $contractor['email1'],
                            'distance' => $modelBookingAddress->getDistanceByTwoAddress($contractor, $inquiryAddress)
                        );
                    }
                }
            }
            $result = $contractorDistances;
            if ($result) {
                $cache->save($result, $cacheID);
            }
        } else {
            $contractorDistances = $result;
        }
        $this->view->contractorDistances = $contractorDistances;

        //D.A 10/09/2015 inquiry Available Technicians Cache
        $cacheID = $inquiryId . '_inquiryPhotos';
        if (($result = $cache->load($cacheID)) === false) {
            $modelImage = new Model_Image();
            $pager = null;
            $photo = $modelImage->getAll($inquiryId, 'inquiry', 'i.created desc', $pager, $filter = array(), $limit = 10);
            $photoCount = count($modelImage->getAll($inquiryId, 'inquiry', 'i.created desc'));
            $type = 'inquiry';
            $result = array(
                'photo' => $photo,
                'photoCount' => $photoCount,
                'type' => $type,
            );
            if ($result) {
                $cache->save($result, $cacheID);
            }
        } else {
            $photo = $result['photo'];
            $photoCount = $result['photoCount'];
            $type = $result['type'];
        }
        $this->view->photo = $photo;
        $this->view->photoCount = $photoCount;
        $this->view->type = $type;
    }

    //D.A 10/09/2015 clear inquiry cache
    public function inquiryCacheClearAction() {

        //check Auth for logged user
        CheckAuth::checkPermission(array('inquiryEdit'));

        //get request parameters
        $inquiryId = $this->request->getParam('id');

        require_once 'Zend/Cache.php';
        $company_id = CheckAuth::getCompanySession();
        $inquiryViewDir = get_config('cache') . '/' . 'inquiriesView' . '/' . $company_id;
        $frontEndOption = array('lifetime' => NULL,
            'automatic_serialization' => true);
        $backendOptions = array('cache_dir' => $inquiryViewDir);
        $Cache = Zend_Cache::factory('Core', 'File', $frontEndOption, $backendOptions);
        $Cache->remove($inquiryId . '_inquiryDetails');
        $Cache->remove($inquiryId . '_inquiryLabels');
        $Cache->remove($inquiryId . '_inquiryPhotos');
        $Cache->remove($inquiryId . '_inquiryAvailableTechnicians');
        $Cache->remove($inquiryId . '_inquiryServices');
        $Cache->remove($inquiryId . '_originalInquiry');

        $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Cache cleared"));
        $this->_redirect($this->router->assemble(array('id' => $inquiryId), 'inquiryView'));
    }

    public function outComingAction() {



        $model_bannedIpAddress = new Model_BannedIpAddress();
        $apiKey = $this->request->getParam('api_key');


        if (md5('ya7abeebi') != $apiKey) {
            exit;
        }
		
        // get request parameters


        $serviceArray = json_decode($this->request->getParam('service', ''));
        $floorArray = json_decode($this->request->getParam('floor', ''));
		
		
		
        $sizeArray = json_decode($this->request->getParam('size', ''));
        $ip_address = $this->request->getParam('ip_address', '');
        $page_url = $this->request->getParam('page_url', '');
        if (!is_array($serviceArray)) {
            $serviceArray = array($this->request->getParam('service'));
            $floorArray = array($this->request->getParam('floor'));
            $sizeArray = array($this->request->getParam('size'));
        }
		
		
		
		if(!(isset($floorArray[0]))){
		 $floorArray[0] = 0;
		}
	
        $cityName = $this->request->getParam('city', '');
        $property_type = $this->request->getParam('property_type', '');
        $name = $this->request->getParam('name', '');
        $email = trim($this->request->getParam('email', ''));
        $mobile = trim($this->request->getParam('mobile', ''));
      
        
        $phone = trim($this->request->getParam('phone', ''));
        $address1 = $this->request->getParam('address', '');
        $postcode = $this->request->getParam('postcode', '');
        
        $addressData = $this->getState($postcode);
        $address = $address1.' '.$addressData;
        
        $required = $this->request->getParam('required', '');

        $comment = $this->request->getParam('comment', '');
        $keyword_referer = $this->request->getParam('keyword_referer', '');
        $website = $this->request->getParam('website', '');

        $search_engine_referer = $this->request->getParam('search_engine_referer', '');
        $full_url = $this->request->getParam('full_url', '');

        $ref_no = $this->request->getParam('ref_no', '');
        $date_posted = $this->request->getParam('date_posted', date('Y-m-d H:i:s', time()));
        $companyName = $this->request->getParam('company_name', '');
        ////new By islam
        $keywords = $this->request->getParam('keywords', '');
        $search_engine = $this->request->getParam('search_engine', '');
        $browser = $this->request->getParam('browser', '');
        $browser_version = $this->request->getParam('browser_version', '');
        $browser_platform = $this->request->getParam('browser_platform', '');
        $device_type = $this->request->getParam('device_type', '');
        $state = $this->request->getParam('state', '');
        ////end by islam

        if (get_config('remove_white_spacing')) {
            $mobile = preparer_number($mobile);
            $phone = preparer_number($phone);
        }
        
               


        ////////////// Add By Islam
        /* $keywords = $this->get_keyword($full_url);
          $search_engine_referer = $keywords[0];
          $keyword_referer = $keywords[1];

          // perform some basic spam checks for common spam attacks the website already suffered from in the past
          $spam = ($phone == '123456') ? '1' : $spam;
          $spam = (strpos($comment, 'ttp://')) ? '1' : $spam;
          $spam = (strpos($comment 'ttps://')) ? '1' : $spam;
          $spam = (strpos($comment 'www')) ? '1' : $spam;
          $spam = (strpos($comment, '.ru')) ? '1' : $spam;
          $spam = (strpos($address, 'ttp:')) ? '1' : $spam;
          $spam = (strpos($address, 'ttps:')) ? '1' : $spam;
          $spam = (strpos($address, 'www.')) ? '1' : $spam;

          if($ip_address == '194.8.75.239') { $spam = '1'; }
         */
        ////check if the ip is banned
        $isSpam = 0;
        $isBanned = $model_bannedIpAddress->isBannedIP($ip_address);
        if ($isBanned) {
            $isSpam = 1;
        }

        ///////////// End

        $originalInquiry = array(
            'ip_address' => $ip_address,
            'page_url' => $page_url,
            'floor' => $floorArray[0],
            'service' => $serviceArray[0],
            'city' => $cityName,
            'property_type' => $property_type,
            'name' => $name,
            'email' => $email,
            'mobile' => $mobile,
            'phone' => $phone,
            'address' => $address,
            'postcode' => $postcode,
            'required' => $required,
            'size' => $sizeArray[0],
            'comment' => $comment,
            'keyword_referer' => $keyword_referer,
            'search_engine_referer' => $search_engine_referer,
            'website' => $website,
            'ref_no' => $ref_no,
            'date_posted' => $date_posted,
            'keywords' => $keywords,
            'search_engine' => $search_engine,
            'browser' => $browser,
            'browser_version' => $browser_version,
            'browser_platform' => $browser_platform,
            'device_type' => $device_type
        );
        //print_r($originalInquiry);
        $modelOriginalInquiry = new Model_OriginalInquiry();
        $originalInquiryId = $modelOriginalInquiry->insert($originalInquiry);

        //get company
        $modelCompanies = new Model_Companies();
        $company = $modelCompanies->getByName($companyName);

        if (empty($company)) {
            exit;
        }

        //insert city
        $modelCities = new Model_Cities();
        $city = $modelCities->getByName($cityName);

        if (empty($city)) {
            $newCity = array(
                'city_name' => $cityName,
                'country_id' => 9,
                'state' => $state,
                'postcode_key' => $postcode,
            );
            $city_model = new Model_Cities();
            $id = $city_model->insert($newCity);
            //exit;
        }
        $city = $modelCities->getByName($cityName);
        /*
          $modelServices = new Model_Services();
          $service = $modelServices->getByServiceNameAndCompanyId($service_name,$company['company_id']);

          if (empty($service)) {
          $data = array(
          'service_name'=>$service_name,
          'company_id' => $company['company_id'],
          'created'=> time(),
          'min_price'=> 350,
          'price_equasion'=> '[price]*[quantity]-[discount]'
          );
          $service_id = $modelServices->insert($data);
          $service = $modelServices->getByServiceName($service_name);

          //add two attributes for it
          $modelAttribute = new Model_Attributes();
          $modelServiceAttribute = new Model_ServiceAttribute();
          $floorAttribute = $modelAttribute->getByVariableName('floor');
          $quantityAttribute = $modelAttribute->getByVariableName('quantity');
          if(isset($floorAttribute)&& !empty($floorAttribute)){
          $serviceFloorAttribute = array(
          'service_id' => $service_id,
          'attribute_id' => $floorAttribute['attribute_id'],
          'default_value' => '',
          'is_readonly' => 0
          );
          $modelServiceAttribute->insert($serviceFloorAttribute);
          }
          if(isset($quantityAttribute)&& !empty($quantityAttribute)){
          $serviceQuantityAttribute = array(
          'service_id' => $service_id,
          'attribute_id' => $quantityAttribute['attribute_id'],
          'default_value' => '',
          'is_readonly' => 0
          );
          $modelServiceAttribute->insert($serviceQuantityAttribute);
          }

          //exit;
          }
         */
        $modelCustomerType = new Model_CustomerType();
        $propertyType = $modelCustomerType->getByTypeName($property_type);

        $commercial = $modelCustomerType->getByTypeName('Commercial');
        $residential = $modelCustomerType->getByTypeName('Residential ');
        if ($propertyType['customer_type_id'] == $commercial['customer_type_id']) {
            $customerTypeId = $commercial['customer_type_id'];
        } else {
            $customerTypeId = $residential['customer_type_id'];
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $tmp = uniqid('inq');
            $email = "tmp_{$tmp}@update_this_email.tmp";
        }

        $data = array(
            'title' => 'mr',
            'first_name' => $name,
            'city_id' => $city['city_id'],
            'street_address' => $address,
            'postcode' => $postcode,
            'email1' => $email,
            'phone1' => $phone,
            'mobile1' => $mobile,
            'full_name_search' => 'mr.' . $name,
            'created' => time(),
            'customer_type_id' => $customerTypeId,
            'company_id' => $company['company_id']
        );
        $modelCustomer = new Model_Customer();
        $newCustomerId = $modelCustomer->insert($data);

        $modelInquiryRequiredType = new Model_InquiryRequiredType();
        $inquiryRequiredType = $modelInquiryRequiredType->getByType($required);

        $modelPropertyType = new Model_PropertyType();
        $propertyType = $modelPropertyType->getByType($property_type);

        $modelInquiryType = new Model_InquiryType();
        $inquiryType = $modelInquiryType->getByType('Website');

        $modelUser = new Model_User();
        $user = $modelUser->getByUserCode(sha1("General {$companyName}"));

        $modelInquiry = new Model_Inquiry();
        $data = array(
            'title' => "from {$companyName}",
            'inquiry_type_id' => $inquiryType['inquiry_type_id'],
            'comment' => $comment,
            'city_id' => $city['city_id'],
            'customer_id' => $newCustomerId,
            'created' => time(),
            'user_id' => $user['user_id'],
            'full_text_search' => "from {$companyName} " . $comment,
            'original_inquiry_id' => $originalInquiryId,
            'company_id' => $company['company_id'],
            'property_type_id' => (isset($propertyType['id']) ? $propertyType['id'] : 0),
            'required_type_id' => (isset($inquiryRequiredType['id']) ? $inquiryRequiredType['id'] : 0),
            'is_spam' => $isSpam
        );
        $inquiryId = $modelInquiry->insert($data, $company['company_id']);

        if ($inquiryId) {

            /*
             * add Inquiry Address
             */
            $db_params = array();
            $db_params['inquiry_id'] = $inquiryId;
            $db_params['street_address'] = $address;
            $db_params['suburb'] = '';
            $db_params['state'] = '';
            $db_params['unit_lot_number'] = '';
            $db_params['street_number'] = '';
            $db_params['postcode'] = $postcode;
            $db_params['po_box'] = '';
            $modelInquiryAddress = new Model_InquiryAddress();
            $modelInquiryAddress->insert($db_params);


            foreach ($serviceArray as $key => $serv) {
                $modelServices = new Model_Services();
                $service = $modelServices->getByServiceName($serv);
                if (empty($service)) {
                    exit;
                }
                // add service to inquiry


                $params = array(
                    'service_id' => $service['service_id'],
                    'inquiry_id' => $inquiryId,
                    'clone' => 0
                );
                $modelInquiryService = new Model_InquiryService();
                $modelInquiryService->insert($params);

                // add floor to inquiry
				if(my_ip()){
				echo 'add floor';
				exit;
				}

                $modelAttributeListValue = new Model_AttributeListValue();
                $attributeValue = $modelAttributeListValue->getByAttributeValue($floorArray[$key]);
                $modelInquiryServiceAttributeValue = new Model_InquiryServiceAttributeValue();
                if ($attributeValue) {
                    $modelServiceAttribute = new Model_ServiceAttribute();
                    $serviceAttribute = $modelServiceAttribute->getByAttributeIdAndServiceId($attributeValue['attribute_id'], $service['service_id']);

                    if ($serviceAttribute) {
                        $data = array(
                            'service_attribute_id' => $serviceAttribute['service_attribute_id'],
                            'inquiry_id' => $inquiryId,
                            'value' => $attributeValue['attribute_value_id'],
                            'clone' => 0
                        );

                        $modelInquiryServiceAttributeValue->insert($data);
                    }
                }

                //send email to cusomer regarding of websites  

                $to = array($email);

                $trading_namesObj = new Model_TradingName();
                $trading_names = $trading_namesObj->getTradingNameByWebsite($website);
//            if($_SERVER['REMOTE_ADDR'] == '176.106.46.142'){
//                echo "salim";
//                var_dump($trading_names);
//                exit;
//            }

                $template_params = array(
                    //customer
                    '{customer_name}' => $name,
                    '{year}' => date("Y", time()),
                    '{phone}' => $trading_names['phone'],
                    '{trading_name}' => $trading_names['trading_name'],
                    '{website}' => $trading_names['website_url'],
                    '{trading_name_id}' => $trading_names['trading_name_id'],
                    '{color}' => $trading_names['color']
                );



                $modelEmailTemplate = new Model_EmailTemplate();
                $emailTemplate = $modelEmailTemplate->getEmailTemplate('confirmation_email_for_websites', $template_params, $trading_names['company_id']);
                $params = array(
                    'to' => $to,
                    'reply' => array('name' => $trading_names['trading_name'] . " - Reply", 'email' => $trading_names['email']),
                    'body' => $emailTemplate['body'],
                    'subject' => $emailTemplate['subject'],
//                'companyId' => $inquiry['company_id'],
                    'trading_name' => $trading_names['trading_name'],
                    'from' => $trading_names['email'],
                );



                $email_log = array('reference_id' => $inquiryId, 'type' => 'inquiry');
                EmailNotification::sendEmail($params, 'confirmation_email_for_websites', $template_params, $email_log, $trading_names['company_id']);

                /*
                 * add size == quantity to inquiry
                 */
                $modelAttribute = new Model_Attributes();
                $attribute = $modelAttribute->getByVariableName('quantity');
                if (!empty($sizeArray)) {
                    if ($attribute) {
                        $modelServiceAttribute = new Model_ServiceAttribute();
                        $serviceAttribute = $modelServiceAttribute->getByAttributeIdAndServiceId($attribute['attribute_id'], $service['service_id']);

                        if ($serviceAttribute) {
                            $data = array(
                                'service_attribute_id' => $serviceAttribute['service_attribute_id'],
                                'inquiry_id' => $inquiryId,
                                'value' => $sizeArray[$key],
                                'clone' => 0
                            );

                            $modelInquiryServiceAttributeValue->insert($data);
                        }
                    }
                }
            }
        }

        //By Islam 
        $inquiry = $modelInquiry->getById($inquiryId);
        $fullTextSearch = array();


        // inquiry       
        $fullTextSearch[] = trim($inquiry['inquiry_num']);
        $fullTextSearch[] = trim($inquiry['comment']);

        //customer
        //$modelCustomer = new Model_Customer();
        $fullTextCustomerInquiry = $modelCustomer->getFullTextCustomerContacts($inquiry['customer_id']);
        $fullTextSearch[] = trim($fullTextCustomerInquiry);
        $data = array();
        $data['full_text_search'] = implode(' ', $fullTextSearch);

        $modelInquiry->updateById($inquiryId, $data);
        ///

        echo $inquiryId;

        exit;
    }
    
    public function getState($postCode){
          $postcodeINT = (int) $postCode;
                if ($postcodeINT >= 2600 && $postcodeINT <= 2609) {

                    $city = 'Canberra';
                    $state = 'ACT';
                } else if ($postcodeINT >= 2610 && $postcodeINT <= 2620) {

                    $city = 'Regional';
                    $state = 'ACT';
                } else if (($postcodeINT >= 2000 && $postcodeINT <= 2234 ) || ($postcodeINT >= 1100 && $postcodeINT <= 1299)) {

                    $city = 'Sydney';
                    $state = 'NSW';
                } else if ($postcodeINT >= 2640 && $postcodeINT <= 2660) {

                    $city = 'Riverina Area';
                    $state = 'NSW';
                } else if ($postcodeINT >= 2500 && $postcodeINT <= 2534) {

                    $city = 'Wollongong';
                    $state = 'NSW';
                } else if ($postcodeINT >= 2265 && $postcodeINT <= 2333) {

                    $city = 'Newcastle';
                    $state = 'NSW';
                } else if (preg_match('/^2450/', $postcodeINT) || ( $postcodeINT >= 2413 && $postcodeINT <= 2484) || ($postcodeINT >= 2460 && $postcodeINT <= 2465)) {

                    $city = 'Northern Rivers';
                    $state = 'NT';
                } else if ($postcodeINT >= 2235 && $postcodeINT <= 2999) {

                    $city = 'Regional';
                    $state = 'NSW';
                } else if (( $postcodeINT >= 3000 && $postcodeINT <= 3207) || ($postcodeINT >= 8000 && $postcodeINT <= 8399)) {

                    $city = 'Melbourne';
                    $state = 'VIC';
                } else if ($postcodeINT >= 3208 && $postcodeINT <= 3999) {
                    $city = 'Regional';
                    $state = 'VIC';
                } else if (( $postcodeINT >= 4000 && $postcodeINT <= 4207) || ($postcodeINT >= 4300 && $postcodeINT <= 4305) || ($postcodeINT >= 4500 && $postcodeINT <= 4519) || ($postcodeINT >= 9000 && $postcodeINT <= 9015)) {
                    $city = 'Brisbane';
                    $state = 'QLD';
                } else if ($postcodeINT >= 4208 && $postcodeINT <= 4287) {
                    $city = 'Gold Coast';
                    $state = 'QLD';
                } else if ($postcodeINT >= 4550 && $postcodeINT <= 4575) {
                    $city = 'Sunshine Coast';
                    $state = 'QLD';
                } else if (($postcodeINT >= 4208 && $postcodeINT <= 4299) || ($postcodeINT >= 4306 && $postcodeINT <= 4499) || ($postcodeINT >= 4520 && $postcodeINT <= 4999)) {
                    $city = 'Regional';
                    $state = 'QLD';
                } else if ($postcodeINT >= 5000 && $postcodeINT <= 5199) {
                    $city = 'Adelaide';
                    $state = 'SA';
                } else if ($postcodeINT >= 5200 && $postcodeINT <= 5999) {
                    $city = 'Regional';
                    $state = 'SA';
                } else if (preg_match('/^6827/', $postcodeINT) || ( $postcodeINT >= 6000 && $postcodeINT <= 6199) || ( $postcodeINT >= 6830 && $postcodeINT <= 6832) || ( $postcodeINT >= 6837 && $postcodeINT <= 6849)) {
                    $city = 'Perth';
                    $state = 'WA';
                } else if ($postcodeINT >= 6200 && $postcodeINT <= 6799) {
                    $city = 'Regional';
                    $state = 'WA';
                } else if ($postcodeINT >= 7000 && $postcodeINT <= 7099) {
                    $city = 'Hobart';
                    $state = 'TAS';
                } else if ($postcodeINT >= 7100 && $postcodeINT <= 7999) {
                    $city = 'Regional';
                    $state = 'TAS';
                } else if ($postcodeINT >= 800 && $postcodeINT <= 832) {
                    $city = 'Darwin';
                    $state = 'NT';
                } else if ($postcodeINT >= 833 && $postcodeINT <= 899) {
                    $city = 'Regional';
                    $state = 'NT';
                } else {
                    $city = 'unknown';
                    $state = 'NSW';
                }
//                $data = array(
//                    'city' => $city,
//                    'state' =>$state
//                );
                return $state;
    }

    public function get_keyword($referer) {
        $search_phrase = '';
        $engines = array('dmoz' => 'q=',
            'aol' => 'q=',
            'ask' => 'q=',
            'google' => 'q=',
            'bing' => 'q=',
            'hotbot' => 'q=',
            'teoma' => 'q=',
            'yahoo' => 'p=',
            'altavista' => 'p=',
            'lycos' => 'query=',
            'kanoodle' => 'query='
        );

        foreach ($engines as $engine => $query_param) {
            // Check if the referer is a search engine from our list.
            // Also check if the query parameter is valid.
            if (strpos($referer, $engine . ".") !== false &&
                    strpos($referer, $query_param) !== false) {

                // Grab the keyword from the referer url
                $referer .= "&";
                $pattern = "/[?&]{$query_param}(.*?)&/si";
                preg_match($pattern, $referer, $matches);
                $search_phrase = urldecode($matches[1]);
                return array($engine, $search_phrase);
            }
        }
        return;
    }

    //////////

    public function deleteForeverAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('inquiryDeleteForever'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
        $ids = $this->request->getParam('ids', array());
		$is_spam = $this->request->getParam('is_spam', 0); 
        if ($id) {
            $ids[] = $id;
        }

        $modelInquiry = new Model_Inquiry();

        $success_array = array();
        foreach ($ids as $id) {
            $success_array[$id] = 0;
            if (CheckAuth::checkIfCanHandelAllCompany('inquiry', $id)) {
                if ($modelInquiry->checkIfCanSeeInquiry($id)) {
                    $modelInquiry->deleteById($id);
                    $success_array[$id] = 1;
                }
            }
        }

        if (!in_array(0, $success_array)) {
            if (count($success_array) > 1) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Selected inquiries have been deleted"));
            } else {
                $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Inquiry deleted"));
            }
        } else {
            if (count($success_array) > 1) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Failed to delete selected inquiries"));
            } else {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Failed to delete inquiry"));
            }
        }

        //$this->_redirect($this->router->assemble(array(), 'showDeletedInquiry'));
		if($is_spam){
			$this->_redirect($this->router->assemble(array(), 'inquiry').'?is_spam=1'); 
		}else{
			$this->_redirect($this->router->assemble(array('is_deleted'=>'deleted'), 'showDeletedInquiry'));
		}
		
        
    }

    public function showDeletedAction() {
        //
        //authinticat
        //
        if (CheckAuth::checkCredential(array('canSeeDeletedInquiry'))) {

            //
            // get request parameters
            //
            $orderBy = $this->request->getParam('sort', 'inquiry_id');
            $sortingMethod = $this->request->getParam('method', 'desc');
            $currentPage = $this->request->getParam('page', 1);
            $filters = $this->request->getParam('fltr', array());

            if ($filters) {
                foreach ($filters as &$filter) {
                    if (!is_array($filter)) {
                        $filter = trim($filter);
                    }
                }
            }
            $this->view->sub_menu = 'deleted_inquiry';
            //
            //get all deleted customers
            //
            $filters['is_deleted'] = 1;



            //
            // init pager and articles model object
            //
            $pager = new Model_Pager();
            $pager->perPage = get_config('perPage');
            $pager->currentPage = $currentPage;
            $pager->url = $_SERVER['REQUEST_URI'];

            //
            // get data list
            //
            $modelService = new Model_Services();
            $modelInquiryService = new Model_InquiryService();
            $modelInquiry = new Model_Inquiry();
            $data = $modelInquiry->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);

            foreach ($data as &$row) {
                $inquiryServices = $modelInquiryService->getByInquiryId($row['inquiry_id']);

                $allService = array();
                foreach ($inquiryServices as $inquiryService) {
                    $service = $modelService->getById($inquiryService['service_id']);
                    $allService[$service['service_id']] = $service['service_name'];
                }
                $row['allService'] = $allService;
            }

            $this->view->data = $data;
            $this->view->perPage = $pager->perPage;
            $this->view->pageLinks = $pager->getPager();
        } else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view deleted inquiries"));
            $this->_redirect($this->router->assemble(array(), 'inquiry'));
        }
    }

    public function sendInquiryAdvertisingEmailAction() {

        //
        // get params 
        //
        $inquiryId = $this->request->getParam('id', 0);

        if (!CheckAuth::checkIfCanHandelAllCompany('inquiry', $inquiryId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this inquiry"));
            $this->_redirect($this->router->assemble(array(), 'inquiry'));
        }
        
		/*//Rand
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($inquiryId,'inquiry','send_inquiry_advertising_email');*/
		
        //
        // load models
        //
        $modelInquiry = new Model_Inquiry();
        $modelCustomer = new Model_Customer();
        $modelInquiryAddress = new Model_InquiryAddress();
        $modelInquiryService = new Model_InquiryService();


        //
        // geting data
        //
        $inquiry = $modelInquiry->getById($inquiryId);

        //
        // validation
        //
        if (!$inquiry) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Inquiry not found"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        if (!$modelInquiry->checkIfCanSeeInquiry($inquiryId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this inquiry"));
            $this->_redirect($this->router->assemble(array(), 'inquiry'));
        }

        //
        // filling extra data
        //
        $customer = $modelCustomer->getById($inquiry['customer_id']);

        // get all images
        $allIimageAttachments = $this->getImageAttachmentsbyInquiryId($inquiryId);

        $image_attachments = '';
        foreach ($allIimageAttachments as $key => $imageAttachments) {
            $image_attachments .= "<div><span style='font-weight: bold;'>Floor :{$key}</span>";
            foreach ($imageAttachments as $imageAttachment) {

                $large_path = Zend_Controller_Front::getInstance()->getBaseUrl() . '/uploads/image_attachment/' . $imageAttachment['large_path'];
                $description = $imageAttachment['description'];

                $image_attachments .= "<div style='margin-top: 15px;'>";
                if ($description) {
                    $image_attachments .= "<div>";
                    $image_attachments .= "<span style='font-weight: bold;'>{$description}</span>";
                    $image_attachments .= "</div>";
                }
                $image_attachments .= "<div>";
                $image_attachments .= "<img width='510' data-mce-src='{$large_path}' alt='{$description}' src='{$large_path}'/>";
                $image_attachments .= "</div>";
                $image_attachments .= "</div>";
            }
            $image_attachments .= "</div>";
        }

        $template_params = array(
            //imageAttachments
            '{image_attachments}' => $image_attachments,
            //inquiry
            '{inquiry_num}' => $inquiry['inquiry_num'],
            '{comment}' => $inquiry['comment'] ? $inquiry['comment'] : '',
            '{inquiry_created}' => date('d/m/Y', $inquiry['created']),
            '{inquiry_address}' => get_line_address($modelInquiryAddress->getByInquiryId($inquiryId)),
            '{service}' => nl2br($modelInquiryService->getByInquiryIdAsText($inquiryId)),
            //customer
            '{customer_name}' => get_customer_name($customer),
            '{customer_first_name}' => isset($customer['first_name']) && $customer['first_name'] ? ucwords($customer['first_name']) : '',
            '{customer_last_name}' => isset($customer['last_name']) && $customer['last_name'] ? ' ' . ucwords($customer['last_name']) : '',
            '{customer_contacts}' => nl2br($modelCustomer->getCustomerContacts($inquiry['customer_id']))
        );

        $modelEmailTemplate = new Model_EmailTemplate();
        $emailTemplate = $modelEmailTemplate->getEmailTemplate('send_inquiry_advertising_email', $template_params);

        $body = $emailTemplate['body'];
        $subject = $emailTemplate['subject'];
        $to = array();
        if ($customer['email1']) {
            $to[] = $customer['email1'];
        }
        if ($customer['email2']) {
            $to[] = $customer['email2'];
        }
        if ($customer['email2']) {
            $to[] = $customer['email3'];
        }
        $to = implode(',', $to);

        if ($this->request->isPost()) {
            $to = $this->request->getParam('to');
            $cc = $this->request->getParam('cc');
            $subject = $this->request->getParam('subject');
            $body = $this->request->getParam('body');


            $params = array(
                'to' => $to,
                'cc' => $cc,
                'body' => $body,
                'subject' => $subject,
                'layout' => 'designed_email_template'
            );

            $error_mesages = array();
            if (EmailNotification::validation($params, $error_mesages)) {

                // Send Email
                $success = EmailNotification::sendEmail($params, '', array(), array('reference_id' => $inquiryId, 'type' => 'inquiry'));

                if ($success) {

                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Email sent"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Failed to send email"));
                }

                echo 1;
                exit;
            }
            $this->view->error_mesages = $error_mesages;
        }

        $this->view->inquiry = $inquiry;
        $this->view->to = $to;
        $this->view->subject = $subject;
        $this->view->body = $body;
        $this->view->cc = isset($cc) ? $cc : '';

        echo $this->view->render('index/send-inquiry-advertising-email.phtml');
        exit;
    }

    public function getImageAttachmentsbyInquiryId($inquiryId) {


        $modelInquiryService = new Model_InquiryService();
        $modelServiceAttribute = new Model_ServiceAttribute();
        $modelAttributeListValue = new Model_AttributeListValue();
        $modelInquiryServiceAttributeValue = new Model_InquiryServiceAttributeValue();
        $modelAttributeType = new Model_AttributeType();
        $modelAttributes = new Model_Attributes();
        $modelImageAttachment = new Model_ImageAttachment();

        $allInquiryService = $modelInquiryService->getByInquiryId($inquiryId);

        $allImagesInquires = array();
        foreach ($allInquiryService as $inquiryService) {

            $attribute = $modelAttributes->getByVariableName('Floor');

            //get service attribute
            $serviceAttribute = $modelServiceAttribute->getByAttributeIdAndServiceId($attribute['attribute_id'], $inquiryService['service_id']);

            $inquiryServiceAttributeValue = $modelInquiryServiceAttributeValue->getByInquiryIdAndServiceAttributeIdAndClone($inquiryId, $serviceAttribute['service_attribute_id'], $inquiryService['clone']);

            $attributeListValue = $modelAttributeListValue->getById($inquiryServiceAttributeValue['value']);

            // get images
            $limit = get_config('image_attachments_limit');
            $allImagesInquires[$attributeListValue['attribute_value']] = $modelImageAttachment->getByAttributListValueId($attributeListValue['attribute_value_id'], $limit);
        }

        return $allImagesInquires;
    }

    public function removeFollowUpInquiryAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('inquiryEdit'));

        //
        // get params
        //
        $inquiryId = $this->request->getParam('id');

        //
        // load model
        //
        $modelInquiry = new Model_Inquiry();
        $inquiry = $modelInquiry->getById($inquiryId);

        if (!$inquiry) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Inquiry not found"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        if (!$modelInquiry->checkIfCanSeeInquiry($inquiryId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this Inquiry"));
            $this->_redirect($this->router->assemble(array(), 'inquiry'));
        }

        $modelInquiry->updateById($inquiryId, array('is_to_follow' => 0));

        $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
    }

    public function followUpInquiryAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('inquiryEdit'));

        //
        // get params
        //
        $inquiryId = $this->request->getParam('id');
        $followDate = $this->request->getParam('followDate');
		
		$db_format = 0;
			$dateTimeObj= get_settings_date_format();
		 if($dateTimeObj)
		 {
			$db_format = 1;
		 }
          
		  

        //
        // load model
        //
        $modelInquiry = new Model_Inquiry();
        $inquiry = $modelInquiry->getById($inquiryId);

        if (!$inquiry) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Inquiry not found"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        if (!$modelInquiry->checkIfCanSeeInquiry($inquiryId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this Inquiry"));
            $this->_redirect($this->router->assemble(array(), 'inquiry'));
        }
         
		/*  //Rand
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($inquiryId,'inquiry','follow_up_inquiry');*/
		
		
        //
        // init action form
        //
        $form = new Inquiry_Form_FollowUPInquiry(array('inquiry' => $inquiry));

        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
				if($followDate && $db_format) {
					$followDate = timeFormat_zend_to_purePhp('d-m-Y H:i', $followDate);
				}
					
                $data = array(
                    'is_to_follow' => 1,
                    'deferred_date' => mySql2PhpTime($followDate),
                );

                $success = $modelInquiry->updateById($inquiryId, $data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                }

                echo 1;
                exit;
            }
        }

        $this->view->form = $form;
		
        //
        // render views
        //
        echo $this->view->render('index/follow-up-inquiry.phtml');
        exit;
    }

    public function modifyInquiryFollowDateAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('inquiryEdit'));


        $inquiryId = $this->request->getParam('inquiry_id', 0);
        $followDate = $this->request->getParam('follow_date', 0);
		$db_format = 0;
			$dateTimeObj= get_settings_date_format();
		 if($dateTimeObj)
		 {
			$db_format = 1;
		 }
		 if($followDate && $db_format) 
					$followDate = timeFormat_zend_to_purePhp('d-m-Y H:i', $followDate);
        $followDate = strtotime($followDate);

        $modelInquiry = new Model_Inquiry();
        $inquiry = $modelInquiry->getById($inquiryId);

        if (!$inquiry) {
            echo 1;
            exit;
        }

        $modelInquiry->updateById($inquiryId, array('deferred_date' => $followDate));
		//echo "stored val: " . $followDate;
        $followDates = getDateFormating($followDate);
		//echo "sent val: " . $followDates;

        echo json_encode(array('st' => 1, 'msg' => $followDates));
        ;
        exit;
    }

    public function getAllContractorDistancesAction() {
        
		/*
          // Load models
         */
        $modelContractorService = new Model_ContractorService();
        $modelInquiry = new Model_Inquiry();
        $modelInquiryAddress = new Model_InquiryAddress();
        $modelBookingAddress = new Model_BookingAddress();
        $modelUser = new Model_User();
        //
        // get request parameters
        //
        $inquiry_id = $this->request->getParam('inquiry_id', 0);
        $service_ids = $this->request->getParam('service_ids', array());
        
		/*//Rand
		$modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($inquiry_id, 'inquiry', 'get_all_contractor_distances');*/
		
		
        // get inquiry details by id to get city_id 
        $inquiry = $modelInquiry->getById($inquiry_id);
        $cityId = $inquiry['city_id'];

        // get inquiry address by id, we need it to calculate the distance between it and contractor address
        $inquiryAddress = $modelInquiryAddress->getByInquiryId($inquiry_id);

        // check service if is serliaze when return from tb_show
        $service_ids = $this->is_serial($service_ids) ? unserialize($service_ids) : $service_ids;

        // Remove Redundancy
        $removeRedundancy = array();
        foreach ($service_ids as $service_id) {
            $removeRedundancy[$service_id] = $service_id;
        }
        $service_ids = $removeRedundancy;

        /// get get contractor ids by city_id and service_id
        if (!empty($service_ids)) {
            foreach ($service_ids as $service_id) {
                if ($cityId) {
                    $ContractorServices = $modelContractorService->getContractorByServiceIdAndCityId($service_id, $cityId);
                } else {
                    $ContractorServices = $modelContractorService->getContractorByServiceId($service_id);
                }
                foreach ($ContractorServices as $ContractorService) {
                    $contractor_ids[$ContractorService['contractor_id']] = $ContractorService['contractor_id'];
                }
            }
        }

        /// here calculate the distance between each contractor and inquiry address
        $contractorDistances = array();
        foreach ($contractor_ids as $contractorId) {
            //// delete inquiries from contractor list
            if ($contractorId != 1) {
                $contractor = $modelUser->getById($contractorId);

                $contractorDistances[$contractor['user_id']] = array(
                    'contractor_id' => $contractor['user_id'],
                    'name' => $contractor['username'],
                    'distance' => $modelBookingAddress->getDistanceByTwoAddress($contractor, $inquiryAddress)
                );
            }
        }


        $this->view->contractorDistances = $contractorDistances;

        echo $this->view->render('index/get-all-contractor-distances.phtml');
        exit;
    }

    public function is_serial($data) {
        $data = @unserialize($data);
        if ($data === false) {
            return false;
        } else {
            return true;
        }
    }

    public function advanceSearchAction() {

        echo $this->view->render('index/filters.phtml');
        exit;
    }
	
	  public function sendInquiryAsSmsAction() {
	   CheckAuth::checkPermission(array('sendSmsTwilio'));
       $inquiryId = $this->request->getParam('id', 0);
       $modelSmsHistory = new Model_SmsHistorty();
	   $loggedUser = CheckAuth::getLoggedUser();
		$user_log=$loggedUser['user_id'];

		/*//Rand
		$modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($inquiryId, 'inquiry', 'view_send_sms');*/
		
		
       /* if (!CheckAuth::checkIfCanHandelAllCompany('inquiry', $inquiryId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Inquiry not belongs to your Company"));
            $this->_redirect($this->router->assemble(array(), 'inquiry'));
        }

        //
/*        if (!$inquiry) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Inquiry not exist"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        if (!$modelInquiry->checkIfCanSeeInquiry($inquiryId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this Inquiry"));
            $this->_redirect($this->router->assemble(array(), 'inquiry'));
        }
*/
        $modelBooking = new Model_Booking();
        $modelCustomer = new Model_Customer();
		$modelSmsTemplate = new Model_SmsTemplate();
		$modelInquiry = new Model_Inquiry();
		$modelTradingName=new Model_TradingName();
    	$inquiryInfo=$modelInquiry->getById($inquiryId);
		$trading_name_id=$inquiryInfo['trading_name_id'];
     	$customerId=$inquiryInfo['customer_id'];
		$fortwilioMobileFormat=1;
		$customerInfo = $modelCustomer->getById($customerId,$fortwilioMobileFormat);
		$customerFirstName=$customerInfo['first_name'];
        $trading_info=$modelTradingName->getById($trading_name_id);
		$trading_name=$trading_info['trading_name'];
		$phone=$trading_info['phone'];
		$to_customer=array();
		array_push($to_customer, $customerInfo['mobile1'], $customerInfo['mobile2'],$customerInfo['mobile3']);
		     $customer_first_name = explode(" ", $customerFirstName);
			 $template_params = array(
            '{customerFirstName}'          => $customer_first_name[0],
            '{trading_name}'               => $trading_name,
			'{phone}'                      => $phone
             );
		
		$templateInfo=$modelSmsTemplate->getByName('send message to customer for inquiry');
		$template_id=$templateInfo['id'];
        $smsTemplate = $modelSmsTemplate->getsmsTemplate($template_id, $template_params);
        $message = $smsTemplate['message'];
        
		if ($this->request->isPost()) {
		$mobile_no = $this->request->getParam('mobile_no');
		$messages = $this->request->getParam('message');
	    $mobile=explode(",",$mobile_no);
		for($i=0;$i<count($mobile);$i++)
		{
			$fromNumber = "+61447075733";
			if($mobile[$i]!='')
			$toNumber = "$mobile[$i]";
			else
			continue;
			$sms_id=$modelSmsHistory->sendSmsTwilio($fromNumber,$toNumber,$messages);
			if($sms_id){
			$params = array(
			'reference_id'      => $template_id,
			'from'              => '+61447075733',
			 'to'               => $toNumber,
			 'message_sid'      => $sms_id,
			 'message'          => $messages,
			 'receiver_id'      => $customerId,
			 'status'           => '',
			 'sms_type'         => 'sent',
			 'sms_reason'       => 'inquiry',
			 'reason_id'        =>$inquiryId,
             'template_type'    =>'standard',
			 'created_by'        =>$user_log,

			);
			$modelSmsHistory->insert($params);
		   }
		 
			   }
			   $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
				}
				
			 else
			 {

			 $this->view->to_customer = $to_customer;
			 $this->view->message = $message;
			 $this->view->id=$inquiryId;
			 $form = new Invoices_Form_sendSms();
			 $this->view->form = $form;
			 echo $this->view->render('index/send-inquiry-as-sms.phtml');
			 exit;
			 }
    }
	/*******************pauseResumeInquirySms*******/
	 public function pauseResumeInquirySmsAction() {
       
        $inquiryId = $this->request->getParam('id');
        $process = $this->request->getParam('process', 'pause');

        $modelInquiry = new Model_Inquiry();
        
        if ($process == 'pause') {
            $modelInquiry->updateById($inquiryId, array('pause_inquiry_sms' => 1));
        } else if ($process == 'resume') {
            $modelInquiry->updateById($inquiryId, array('pause_inquiry_sms' => 0));
        }


        $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
    }

   
  
	

}
