<?php

class Inquiry_IndexController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'inquiries';
        $this->view->sub_menu = 'inquiry';
    }

    /**
     * List All complaint  action
     *
     */
    public function indexAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('inquiry'));

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'inquiry_id');
        $sortingMethod = $this->request->getParam('method', 'desc');
        $currentPage = $this->request->getParam('page', 1);
        $perPage = $this->request->getParam('perPage', 15);
		$filters = $this->request->getParam('fltr', array('status' => 'inquiry', 'unlabeled' => '1'));

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        $loggedUser = CheckAuth::getLoggedUser();
        $my_inquiries = $this->request->getParam('my_inquiries', false);
        if ($my_inquiries) {
            $this->view->sub_menu = 'my_inquiries';
            $filters['my_inquiries'] = $loggedUser['user_id'];
        }
		////////////////test spam
		$is_spam = $this->request->getParam('is_spam', 0);
        if ($is_spam) {
            $this->view->sub_menu = 'spam_inquiry';
            $filters['is_spam'] = $is_spam;
        }
		if ($perPage) {
            $filters['perPage'] = $perPage;
        }
////////////end test 
        // to get all booking that belong to this user selected from user search
        if (isset($filters['user_id']) && !empty($filters['user_id'])) {
            $filters['my_inquiries'] = $filters['user_id'];
        }

        //
        // init pager model object
        //
        $pager = new Model_Pager();
        $pager->perPage = !empty($filters['perPage'])? $filters['perPage'] : get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        //
        // get data list
        //
        $modelInquiry = new Model_Inquiry();
        $data = $modelInquiry->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);
        $modelInquiry->fills($data, array('allService', 'full_customer_info', 'city', 'labels', 'reminder', 'service_attribute', 'have_attachment'));

        $this->view->data = $data;
        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
    }

    public function addAction() {

        //
        // check Auth for logged user
        //
        //check if the user has logged in and premission to this credintial
        //
        CheckAuth::checkPermission(array('inquiryAdd'));


        //
        // get request parameters
        //
        $services = $this->request->getParam('services', array());
        $inquiryTypeId = $this->request->getParam('inquiry_type_id');
        $comment = $this->request->getParam('comment');
        $cityId = $this->request->getParam('city_id');
        $countryId = $this->request->getParam('country_id');
        $state = $this->request->getParam('state');
        $customerId = $this->request->getParam('customer_id');
        $deferred_date = $this->request->getParam('deferred_date');
        //$title = $this->request->getParam('title');
        $requiredTypeId = $this->request->getParam('required_type', 0);
        $propertyTypeId = $this->request->getParam('property_type', 0);
        $isToFollow = $this->request->getParam('is_to_follow', 0);
	    

        //
        //to get the inquiry address from the customer info
        //
		
        if ($customerId) {
            $modelCustomer = new Model_Customer();
            $customer = $modelCustomer->getById($customerId);

            $inquiryAddress = array(
                'street_address' => $customer['street_address'],
                'street_number' => $customer['street_number'],
                'suburb' => $customer['suburb'],
                'unit_lot_number' => $customer['unit_lot_number'],
                'postcode' => $customer['postcode'],
                'po_box' => $customer['po_box']
            );
            $this->view->inquiryAddress = $inquiryAddress;
        }

        //
        //fill default country
        //
        if (empty($countryId)) {
            $countryId = CheckAuth::getCountryId();
        }

        //
        //fill default city
        //
        if (empty($cityId)) {
            $cityId = CheckAuth::getCityId();
        }

        //
        // init action form
        //
        $options = array('country_id' => $countryId, 'state' => $state);
        if ($cityId) {
            $options['city_id'] = $cityId;
        }
		//////by islam
		 if ($customerId) {
			$inquiry=array();
			$inquiry['customer_id']=$customerId;
			 $options['inquiry'] = $inquiry;
        }
		/////end
        $form = new Inquiry_Form_Inquiry($options);


        //
        // load model
        //
        $contractorServiceAvailability_obj = new Model_ContractorServiceAvailability();

        //
        //get inquiryService
        //
        if ($cityId) {
            $this->view->selectedServices = $services;

            //
            //get all Service Availabile in city
            //
            $all_servicesAvailabile = $contractorServiceAvailability_obj->getServiceByCityId($cityId);

            $this->view->all_servicesAvailabile = $all_servicesAvailabile;
        }


        $valid = TRUE;
        //
        //service valideter
        //
        if ($this->request->isPost()) {
            if ($cityId) {
				
                if (empty($services)) {
					
                    $this->view->errServices = 'Please select at least one service';
                    $valid = FALSE;
                } else {
                    foreach ($services as $serviceId) {
				
                        $contractorServiceAvailability = $contractorServiceAvailability_obj->getByCityIdAndServiceId($cityId, $serviceId);
						
                        if (empty($contractorServiceAvailability)) {
                            $this->view->errServices = 'Service or more not Available in this City';
                            $valid = FALSE;
                        }
                    }
                }
            }
        }
	//	var_dump($valid);
	//	exit;


        //
        // handling the insertion process via post
        //
        if ($this->request->isPost()) {

            if ($form->isValid($this->request->getPost()) && $valid) {
				
                $modelInquiry = new Model_Inquiry();
                $loggedUser = CheckAuth::getLoggedUser();
                $company_id = CheckAuth::getCompanySession();
                $data = array(
                    //'title' => $title,
                    'inquiry_type_id' => $inquiryTypeId,
                    'required_type_id' => $requiredTypeId,
                    'property_type_id' => $propertyTypeId,
                    'comment' => $comment,
                    'city_id' => $cityId,
                    'customer_id' => $customerId,
                    'created' => time(),
                    'user_id' => $loggedUser['user_id'],
                    'deferred_date' => strtotime($deferred_date),
                    //'full_text_search' => $title . ' ' . $comment,
                    'company_id' => $company_id,
                    'is_to_follow' => $isToFollow ? 1 : 0
                );


                $inquiryId = $modelInquiry->insert($data);

                //
                // insert Inquiry services and his Attribute
                //
                $modelInquiryService = new Model_InquiryService();
                $modelInquiryService->setServicesToInquiry($inquiryId, $services);

                //
                //insert Inquiry Type Attribute Values
                //
                $modelInquiryTypeAttributeValue = new Model_InquiryTypeAttributeValue();
                $modelInquiryTypeAttributeValue->setInquiryTypeAttributeValueByInquiryIdAndInquiryTypeId($inquiryId, $inquiryTypeId);

                //
                // get the params of the booking and save it in the database
                //
                $streetAddress = $this->request->getParam('street_address');
                $streetNumber = $this->request->getParam('street_number');
                $suburb = $this->request->getParam('suburb');
                $state = $this->request->getParam('state');
                $unitLotNumber = $this->request->getParam('unit_lot_number');
                $postcode = $this->request->getParam('postcode');
                $poBox = $this->request->getParam('po_box');

                $data = array(
                    'street_address' => $streetAddress,
                    'street_number' => $streetNumber,
                    'suburb' => $suburb,
                    'state' => $state,
                    'unit_lot_number' => $unitLotNumber,
                    'postcode' => $postcode,
                    'po_box' => $poBox,
                    'inquiry_id' => $inquiryId
                );

                $modelInquiryAddress = new Model_InquiryAddress();
                $modelInquiryAddress->insert($data);


                $this->_redirect($this->router->assemble(array(), 'inquiry'));
            }
        } else {
            $isToFollow = 1;
        }

        $this->view->form = $form;
        $this->view->city_id = $cityId;
        $this->view->is_to_follow = $isToFollow;

        //
        // render views
        //
        //echo $this->view->render('index/add_edit.phtml');
        //exit;
    }
	
	public function markAsSpamAction() {
		 //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
        if ($id) {
            $ids[] = $id;
        }
		$success_array = array();
		$are_marked = 0;
        if (CheckAuth::checkCredential(array('inquiryMarkAsSpam'))) {
            $modelInquiry = new Model_Inquiry();
            foreach ($ids as $id) {
                $success_array[$id] = 0;
                   
                $modelInquiry->updateById($id, array('is_spam' => 1));
                $success_array[$id] = 1;
                   
            }
			if (!in_array(0, $success_array)) {
				$are_marked = 1;
			}
			else{
				$are_marked = 0;
			}
		}
		
	    echo json_encode(array('are_marked' => $are_marked, 'ids' =>$ids));
        exit;
	}
	
	
	public function addBannedIpAction() {
		$modelInquiry = new Model_Inquiry();
		$modelBannedIpAddress = new Model_BannedIpAddress();
		 //
        // get request parameters
        //
        $ip = $this->request->getParam('ip', 0);
        $done = 0;
        if (CheckAuth::checkCredential(array('addBannedIp')) && $ip != 0) {
            
            $data = array('ip_address'=> $ip);
			$done = $modelBannedIpAddress->insert($data);
			////mark all previous inquiries from this ip as spam
			$modelInquiry->updateByIp($ip,array('is_spam'=> 1));
		}
		
	    echo json_encode(array('done' => $done, 'ip' =>$ip));
        exit;
		
	}
	
	public function removeBannedIpAction() {
		$modelInquiry = new Model_Inquiry();
		 //
        // get request parameters
        //
        $ip = $this->request->getParam('ip', 0);
        $done = 0;
        if (CheckAuth::checkCredential(array('removeBannedIp')) && $ip != 0) {
            $modelBannedIpAddress = new Model_BannedIpAddress();
            $done = $modelBannedIpAddress->deleteBannedIp($ip);
			$modelInquiry->updateByIp($ip,array('is_spam'=> 0));
		}
		
	    echo json_encode(array('done' => $done, 'ip' =>$ip));
        exit;
		
	}
	
	
	/////////////////////

    public function deleteAction() {

        //
        // check Auth for logged user
        //
         //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
        if ($id) {
            $ids[] = $id;
        }
		$are_deleted = 0;
        if (CheckAuth::checkCredential(array('inquiryDelete'))) {

            $modelInquiry = new Model_Inquiry();

            $success_array = array();
            foreach ($ids as $id) {
                $success_array[$id] = 0;
                if (CheckAuth::checkIfCanHandelAllCompany('inquiry', $id)) {
                    if ($modelInquiry->checkIfCanSeeInquiry($id)) {
                        $modelInquiry->updateById($id, array('is_deleted' => 1));
                        $success_array[$id] = 1;
                    }
                }
            }

			if (!in_array(0, $success_array)) {
				$are_deleted = 1;
			}
			else{
				$are_deleted = 0;
			}
			
           /* if (!in_array(0, $success_array)) {
                if (count($success_array) > 1) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "The selected Inquiries have been deleted"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "This Inquiry has been deleted"));
                }
            } else {
                if (count($success_array) > 1) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "The selected  Inquiries could not be deleted"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Inquiry could not be deleted"));
                }
            }*/
        } 
		/*else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to delete Inquiry"));
        }*/
        //$this->_redirect($this->router->assemble(array(), 'inquiry'));
		echo json_encode(array('are_deleted' => $are_deleted, 'ids' =>$ids));
        exit;
    }
	
	public function deleteSpamAction() {

        //
        // check Auth for logged user
        //
		//
		// Load models
		//
		$modelInquiry = new Model_Inquiry();
		$modelCustomer = new Model_Customer();
         //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
        if ($id) {
            $ids[] = $id;
        }
		$are_deleted = 0;
        if (CheckAuth::checkCredential(array('inquiryDelete'))) {
            
            $success_array = array();
            foreach ($ids as $id) {
                $success_array[$id] = 0;
				/////get inquiry data to get customer id to delete the customer also because it is spam
					$inquiry = $modelInquiry->getById($id);
				////now delete the customer of this inquiry
					$modelCustomer->deleteById($inquiry['customer_id']);
				////finally delete the inquiry itself
                    $is_deleted = $modelInquiry->deleteForeverById($id);
                    $success_array[$id] = $is_deleted;
            }

			if (!in_array(0, $success_array)) {
				$are_deleted = 1;
			}
			else{
				$are_deleted = 0;
			}
			
          
        } 
		
		echo json_encode(array('are_deleted' => $are_deleted, 'ids' =>$ids));
        exit;
    }

    public function undeleteAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('inquiryUndelete'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        if (CheckAuth::checkIfCanHandelAllCompany('inquiry', $id)) {
            $modelInquiry = new Model_Inquiry();
            $success = $modelInquiry->updateById($id, array('is_deleted' => 0));
        }
        if ($success) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "The Selected Inquiries have been restored."));
        } else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "The Selected Inquiries could not be restored"));
        }
        $this->_redirect($this->router->assemble(array(), 'inquiry'));
    }

    public function editAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('inquiryEdit'));

        $modelInquiry = new Model_Inquiry();
        $inquiryId = $this->request->getParam('id');

        if (!$modelInquiry->checkIfCanSeeInquiry($inquiryId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this Inquiry"));
            $this->_redirect($this->router->assemble(array(), 'inquiry'));
        }
        if (!CheckAuth::checkIfCanHandelAllCompany('inquiry', $inquiryId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Inquiry not belongs to your Company"));
            $this->_redirect($this->router->assemble(array(), 'inquiry'));
        }

        // get request parameters
        $services = $this->request->getParam('services', array());
        $inquiryTypeId = $this->request->getParam('inquiry_type_id');
        $comment = $this->request->getParam('comment');
        $cityId = $this->request->getParam('city_id');
        $countryId = $this->request->getParam('country_id');
        $state = $this->request->getParam('state');
        $customerId = $this->request->getParam('customer_id');
        $deferred_date = $this->request->getParam('deferred_date');
        $title = $this->request->getParam('title');

        $streetAddress = $this->request->getParam('street_address');
        $streetNumber = $this->request->getParam('street_number');
        $suburb = $this->request->getParam('suburb');
        $state = $this->request->getParam('state');
        $unitLotNumber = $this->request->getParam('unit_lot_number');
        $postcode = $this->request->getParam('postcode');
        $poBox = $this->request->getParam('po_box');
        $requiredTypeId = $this->request->getParam('required_type', 0);
        $propertyTypeId = $this->request->getParam('property_type', 0);


        $this->view->inquiryId = $inquiryId;

        // Validation
        $modelInquiry = new Model_Inquiry();
        $inquiry = $modelInquiry->getById($inquiryId);
        if (!$inquiry) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Inquiry not exist"));
            $this->_redirect($this->router->assemble(array(), 'inquiry'));
            return;
        }

        if ($inquiry['status'] != 'inquiry') {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Status is not Inquiry"));
            $this->_redirect($this->router->assemble(array(), 'inquiry'));
            return;
        }

        $this->view->inquiry = $inquiry;

        $modelOriginalInquiry = new Model_OriginalInquiry();
        $originalInquiry = $modelOriginalInquiry->getById($inquiry['original_inquiry_id']);
        $this->view->originalInquiry = $originalInquiry;



        $isToFollow = !empty($inquiry['is_to_follow']) ? 1 : 0;
        if ($this->request->isPost()) {
            $isToFollow = $this->request->getParam('is_to_follow');
        }

        // init action form
        $form = new Inquiry_Form_Inquiry(array('mode' => 'update', 'inquiry' => $inquiry, 'country_id' => $countryId, 'state' => $state));

        //
        //get inquiryService
        //
        $modelInquiryService = new Model_InquiryService();
        $allInquiryService = $modelInquiryService->getByInquiryId($inquiryId);

        $selectedServices = array();
        foreach ($allInquiryService as $inquiryService) {
            $selectedServices[] = $inquiryService['service_id'] . '_' . $inquiryService['clone'];
        }
        $this->view->selectedServices = $selectedServices;

        //
        //get all Service Availabile in city
        //
        $contractorServiceAvailability_obj = new Model_ContractorServiceAvailability();
        if ($cityId) {
            $all_servicesAvailabile = $contractorServiceAvailability_obj->getServiceByCityId($cityId);
        } else {
            $all_servicesAvailabile = $contractorServiceAvailability_obj->getServiceByCityId($inquiry['city_id']);
        }

        $this->view->all_servicesAvailabile = $all_servicesAvailabile;

        //to return the address values
        $modelInquiryAddress = new Model_InquiryAddress();
        $inquiryAddress = $modelInquiryAddress->getByInquiryId($inquiryId);

        if (!$inquiryAddress) {
            $modelCustomer = new Model_Customer();
            $customer = $modelCustomer->getById($inquiry['customer_id']);

            $inquiryAddress = array();
            $inquiryAddress['street_address'] = $customer['street_address'];
            $inquiryAddress['street_number'] = $customer['street_number'];
            $inquiryAddress['suburb'] = $customer['suburb'];
            $inquiryAddress['unit_lot_number'] = $customer['unit_lot_number'];
            $inquiryAddress['postcode'] = $customer['postcode'];
            $inquiryAddress['po_box'] = $customer['po_box'];
        }


        $temp_address = array();
        $temp_address['street_address'] = $this->request->getParam('street_address', $inquiryAddress['street_address']);
        $temp_address['street_number'] = $this->request->getParam('street_number', $inquiryAddress['street_number']);
        $temp_address['suburb'] = $this->request->getParam('suburb', $inquiryAddress['suburb']);
        $temp_address['unit_lot_number'] = $this->request->getParam('unit_lot_number', $inquiryAddress['unit_lot_number']);
        $temp_address['postcode'] = $this->request->getParam('postcode', $inquiryAddress['postcode']);
        $temp_address['po_box'] = $this->request->getParam('po_box', $inquiryAddress['po_box']);

        $inquiryAddress = $temp_address;
        $this->view->inquiryAddress = $inquiryAddress;

        $valid = TRUE;
        //
        //service valideter
        //
        if ($this->request->isPost()) {
            $this->view->selectedServices = $services;

            if ($cityId) {
                if (empty($services)) {
                    $this->view->errServices = 'Please select at least one service';
                    $valid = FALSE;
                }
            }
        }


        //
        // handling the insertion process via post
        //
        if ($this->request->isPost()) {
            if ($form->isValid($this->request->getPost()) && $valid) {
                $loggedUser = CheckAuth::getLoggedUser();
                $data = array(
                    'title' => $title,
                    'inquiry_type_id' => $inquiryTypeId,
                    'required_type_id' => $requiredTypeId,
                    'property_type_id' => $propertyTypeId,
                    'comment' => $comment,
                    'city_id' => $cityId,
                    'customer_id' => $customerId,
                    'deferred_date' => strtotime($deferred_date),
                    'full_text_search' => $title . ' ' . $comment,
                    'is_to_follow' => $isToFollow ? 1 : 0
                );

                $modelInquiry->updateById($inquiryId, $data);

                //
                // insert Inquiry services and his Attribute
                //
                $modelInquiryService = new Model_InquiryService();
                $modelInquiryService->setServicesToInquiry($inquiryId, $services);

                //
                //insert Inquiry Type Attribute Values
                //
                $modelInquiryTypeAttributeValue = new Model_InquiryTypeAttributeValue();
                $modelInquiryTypeAttributeValue->setInquiryTypeAttributeValueByInquiryIdAndInquiryTypeId($inquiryId, $inquiryTypeId);

                //
                // get the params of the booking and updaate it in the database
                //
                $data = array(
                    'street_address' => $streetAddress,
                    'street_number' => $streetNumber,
                    'suburb' => $suburb,
                    'state' => $state,
                    'unit_lot_number' => $unitLotNumber,
                    'postcode' => $postcode,
                    'po_box' => $poBox,
                );
                if (CheckAuth::checkCredential(array('editInquiryAddress'))) {

                    $modelInquiryAddress = new Model_InquiryAddress();
                    $modelInquiryAddress->updateByInquiryId($inquiryId, $data);
                } else {
                    if (CheckAuth::checkCredential(array('editHisInquiryAddress'))) {

                        if ($loggedUser['user_id'] == $inquiry['user_id']) {
                            $modelInquiryAddress = new Model_InquiryAddress();
                            $modelInquiryAddress->updateByInquiryId($inquiryId, $data);
                        }
                    }
                }
                $this->_redirect($this->router->assemble(array(), 'inquiry'));
            }
        }

        $this->view->form = $form;
        $this->view->is_to_follow = $isToFollow;
    }

    public function convertToBookingAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('inquiryConvertToBooking'));

        // get request parameters
        $id = $this->request->getParam('id');

        if (!CheckAuth::checkIfCanHandelAllCompany('inquiry', $id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Inquiry not belongs to your Company"));
            $this->_redirect($this->router->assemble(array(), 'inquiry'));
        }
        $modelInquiry = new Model_Inquiry();

        if (!$modelInquiry->checkIfCanSeeInquiry($id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this Inquiry"));
            $this->_redirect($this->router->assemble(array(), 'inquiry'));
        }


        $this->_redirect('booking-add?inquiry_id=' . $id);
        exit;
    }

    public function convertToEstimateAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('inquiryConvertToEstimate'));

        // get request parameters
        $id = $this->request->getParam('id');

        if (!CheckAuth::checkIfCanHandelAllCompany('inquiry', $id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Inquiry not belongs to your Company"));
            $this->_redirect($this->router->assemble(array(), 'inquiry'));
        }

        $modelInquiry = new Model_Inquiry();

        if (!$modelInquiry->checkIfCanSeeInquiry($id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this Inquiry"));
            $this->_redirect($this->router->assemble(array(), 'inquiry'));
        }

        $this->_redirect('booking-add?inquiry_id=' . $id . '&toEstimate=1');
        exit;
    }
	
	
	public function editDescriptionAction(){
	  
	  $inquiryId = $this->request->getParam('id', 0);
	  $comment = $this->request->getParam('comment', 0);
	  $data = array('comment'=>$comment);	  
	  $modelInquiry = new Model_Inquiry();
	  $success =  $modelInquiry->updateById($inquiryId , $data );
	  
	  if($success){
	    echo 1;
		exit;
	  }
	  
	 exit; 
	
	} 
	

    public function viewAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('inquiryView'));
        $modelInquiry = new Model_Inquiry();

        // get request parameters
        $inquiryId = $this->request->getParam('id');

        if (!CheckAuth::checkIfCanHandelAllCompany('inquiry', $inquiryId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Inquiry not belongs to your Company"));
            $this->_redirect($this->router->assemble(array(), 'inquiry'));
        }
        if (!$modelInquiry->checkIfCanSeeInquiry($inquiryId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this Inquiry"));
            $this->_redirect($this->router->assemble(array(), 'inquiry'));
        }


        // Validation
        $inquiry = $modelInquiry->getById($inquiryId);
        if (!$inquiry) {
            $this->_redirect($this->router->assemble(array(), 'inquiry'));
            return;
        }

        $this->view->inquiry = $inquiry;

        $modelInquiryLabel = new Model_InquiryLabel();
        $modelLabel = new Model_Label();
        $inquiryLabels = $modelInquiryLabel->getByInquiryId($inquiryId);
        if ($inquiryLabels) {
            foreach ($inquiryLabels as &$inquiryLabel) {
                $inquiryLabel['label'] = $modelLabel->getById($inquiryLabel['label_id']);
            }
        }
        $this->view->inquiryLabels = $inquiryLabels;


        $modelCustomer = new Model_Customer();
        $customer = $modelCustomer->getById($inquiry['customer_id']);


        // customer type work order is_required Message

        $modelCustomerType = new Model_CustomerType();
        $isWorkOrder = false;
        $workOrder = $modelCustomerType->getCustomerTypeIsWorkOrder();

        if (in_array($customer['customer_type_id'], $workOrder)) {
            $modelBookingAttachment = new Model_BookingAttachment();
            $bookingAttachments = $modelBookingAttachment->getByInquiryId($inquiryId);
            $isWorkOrder = true;
            if (!empty($bookingAttachments)) {
                foreach ($bookingAttachments as $attachment) {
                    if ($attachment['work_order'] == 1) {
                        $isWorkOrder = false;
                    }
                }
            }
        }
        $this->view->isWorkOrder = $isWorkOrder;


        $modelServices = new Model_Services();
        $modelInquiryService = new Model_InquiryService();
        $modelServiceAttribute = new Model_ServiceAttribute();
        $modelAttributeListValue = new Model_AttributeListValue();
        $modelInquiryServiceAttributeValue = new Model_InquiryServiceAttributeValue();
        $modelAttributeType = new Model_AttributeType();
		
		$modelContractorService = new Model_ContractorService();
		$modelInquiryAddress = new Model_InquiryAddress();
		$modelBookingAddress = new Model_BookingAddress();
        $modelUser = new Model_User();
		 
		 
		$removeRedundancy = array();
        $allInquiryService = $modelInquiryService->getByInquiryId($inquiryId);
        foreach ($allInquiryService as &$inquiryService) {
			///set service ids in array to get distance between them and contractors
			 $removeRedundancy[$inquiryService['service_id']] = $inquiryService['service_id'];
            //get servise
            $service = $modelServices->getById($inquiryService['service_id']);

            $inquiryService['service_name'] = $service['service_name'];

            //get service attribute
            $allServiceAttribute = $modelServiceAttribute->getAttributeByServiceId($inquiryService['service_id'], 0);

            $attributes = array();
            foreach ($allServiceAttribute as &$serviceAttribute) {
                $inquiryServiceAttributeValue = $modelInquiryServiceAttributeValue->getByInquiryIdAndServiceAttributeIdAndClone($inquiryId, $serviceAttribute['service_attribute_id'], $inquiryService['clone']);
                $attributeType = $modelAttributeType->getById($serviceAttribute['attribute_type_id']);

                $value = '';
                if ($attributeType['is_list']) {
                    if ($inquiryServiceAttributeValue['is_serialized_array']) {
                        $unserializeValues = unserialize($inquiryServiceAttributeValue['value']);
                        if (is_array($unserializeValues)) {
                            $values = array();
                            foreach ($unserializeValues as $unserializeValue) {
                                $attributeListValue = $modelAttributeListValue->getById($unserializeValue);
                                if ($attributeListValue['unit_price']) {
                                    $values[] = number_format($attributeListValue['unit_price'], 2);
                                } else {
                                    $values[] = $attributeListValue['attribute_value'];
                                }
                            }
                            $value = $values;
                        }
                    } else {
                        $attributeListValue = $modelAttributeListValue->getById($inquiryServiceAttributeValue['value']);
                        if ($attributeListValue['unit_price']) {
                            $value = number_format($attributeListValue['unit_price'], 2);
                        } else {
                            $value = $attributeListValue['attribute_value'];
                        }
                    }
                } else {
                    $value = $inquiryServiceAttributeValue['value'];
                }

                $serviceAttribute['value'] = $value;
                $inquiryService['attributes'] = $allServiceAttribute;
            }
        }
        $this->view->services = $allInquiryService;

        $modelOriginalInquiry = new Model_OriginalInquiry();
        $originalInquiry = $modelOriginalInquiry->getById($inquiry['original_inquiry_id']);
        $this->view->originalInquiry = $originalInquiry;

        // Booking or Etimate Number
        $modelBooking = new Model_Booking();
        $booking = $modelBooking->getByInquiryId($inquiryId);

        if (!empty($booking)) {
            if ($booking['convert_status'] == 'estimate') {
                $modelBookingEstimate = new Model_BookingEstimate();
                $estimate = $modelBookingEstimate->getByBookingId($booking['booking_id']);
                $this->view->estimate = $estimate;
            } else {
                $this->view->booking = $booking;
                if ($booking['convert_status'] == 'invoice') {
                    $modelBookingInvoice = new Model_BookingInvoice();
                    $invoice = $modelBookingInvoice->getByBookingId($booking['booking_id']);
                    $this->view->invoice = $invoice;
                }
            }
        }
		
		
		/////get Contractors Distances
		// get inquiry details by id to get city_id 
		$cityId = $inquiry['city_id'];
		
		// get inquiry address by id, we need it to calculate the distance between it and contractor address
		$inquiryAddress =$modelInquiryAddress->getByInquiryId($inquiryId);
		
        $service_ids = $removeRedundancy;
		
		/// get get contractor ids by city_id and service_id
		if (!empty($service_ids)) {
            foreach ($service_ids as $service_id) {
					if ($cityId) {
						$ContractorServices = $modelContractorService->getContractorByServiceIdAndCityId($service_id, $cityId);
					} else {
						$ContractorServices = $modelContractorService->getContractorByServiceId($service_id);
					}
				foreach ($ContractorServices as $ContractorService) {
					 $contractor_ids[$ContractorService['contractor_id']] = $ContractorService['contractor_id'];
				}
            }
        }
		
		/// here calculate the distance between each contractor and inquiry address
		$contractorDistances = array();
		if(!empty($contractor_ids)){
        foreach ($contractor_ids as $contractorId) {
			//// delete inquiries from contractor list
			if($contractorId != 1){
				$contractor = $modelUser->getById($contractorId);

				$contractorDistances[$contractor['user_id']] = array(
					'contractor_id' => $contractor['user_id'],
					'name' => $contractor['username'],
					'email1' => $contractor['email1'],
					'distance' => $modelBookingAddress->getDistanceByTwoAddress($contractor, $inquiryAddress)
				);
			}
        }
	}	

       $modelImageAttachment = new Model_Image();
	   $pager = null ;
       $this->view->photo = $modelImageAttachment->getAll($inquiryId , 'inquiry', 'iu.created desc' , $pager, $filter = array() , $limit = 10);
       $this->view->type = 'inquiry';
       $this->view->photoCount = count($modelImageAttachment->getAll($inquiryId , 'inquiry', 'iu.created desc'));
        
		
		$this->view->contractorDistances = $contractorDistances;
		
    }

    public function outComingAction() {
		
		$model_bannedIpAddress = new Model_BannedIpAddress();
		
        $apiKey = $this->request->getParam('api_key');

        if (md5('ya7abeebi') != $apiKey) {
            exit;
        }
	
        // get request parameters
        $ip_address = $this->request->getParam('ip_address', '');
        $page_url = $this->request->getParam('page_url', '');
        $floor = $this->request->getParam('floor', '');
        $service = $this->request->getParam('service', '');
        $cityName = $this->request->getParam('city', '');
        $property_type = $this->request->getParam('property_type', '');
        $name = $this->request->getParam('name', '');
        $email = trim($this->request->getParam('email', ''));
        $mobile = trim($this->request->getParam('mobile', ''));
        $phone = trim($this->request->getParam('phone', ''));
        $address = $this->request->getParam('address', '');
        $postcode = $this->request->getParam('postcode', '');
        $required = $this->request->getParam('required', '');
		$size = $this->request->getParam('size', '');
        $comment = $this->request->getParam('comment', '');
        $keyword_referer = $this->request->getParam('keyword_referer', '');
        $website = $this->request->getParam('website', '');
        
		$search_engine_referer = $this->request->getParam('search_engine_referer', '');
        $full_url = $this->request->getParam('full_url', '');
		
		$ref_no = $this->request->getParam('ref_no', '');
        $date_posted = $this->request->getParam('date_posted', date('Y-m-d H:i:s', time()));
        $companyName = $this->request->getParam('company_name', '');
		////new By islam
		$keywords = $this->request->getParam('keywords', '');
		$search_engine = $this->request->getParam('search_engine', '');
		$browser = $this->request->getParam('browser', '');
		$browser_version = $this->request->getParam('browser_version', '');
		$browser_platform = $this->request->getParam('browser_platform', '');
		$device_type = $this->request->getParam('device_type', '');		
		$state = $this->request->getParam('state', '');		
		////end by islam
		
        if (get_config('remove_white_spacing')) {
            $mobile = preparer_number($mobile);
            $phone = preparer_number($phone);
        }
		
		////////////// Add By Islam
		/*$keywords = $this->get_keyword($full_url);
		$search_engine_referer = $keywords[0];
		$keyword_referer = $keywords[1];
		
		// perform some basic spam checks for common spam attacks the website already suffered from in the past
		$spam = ($phone == '123456') ? '1' : $spam;
		$spam = (strpos($comment, 'ttp://')) ? '1' : $spam;
		$spam = (strpos($comment 'ttps://')) ? '1' : $spam;
		$spam = (strpos($comment 'www')) ? '1' : $spam;
		$spam = (strpos($comment, '.ru')) ? '1' : $spam;
		$spam = (strpos($address, 'ttp:')) ? '1' : $spam;
		$spam = (strpos($address, 'ttps:')) ? '1' : $spam;
		$spam = (strpos($address, 'www.')) ? '1' : $spam;
		
		if($ip_address == '194.8.75.239') { $spam = '1'; }
		*/
		////check if the ip is banned
		$isSpam =0;
		$isBanned = $model_bannedIpAddress->isBannedIP($ip_address);
		if($isBanned){
			$isSpam = 1;
		}
		
		///////////// End

        $originalInquiry = array(
            'ip_address' => $ip_address,
            'page_url' => $page_url,
            'floor' => $floor,
            'service' => $service,
            'city' => $cityName,
            'property_type' => $property_type,
            'name' => $name,
            'email' => $email,
            'mobile' => $mobile,
            'phone' => $phone,
            'address' => $address,
            'postcode' => $postcode,
            'required' => $required,
			'size' => $size,
            'comment' => $comment,
            'keyword_referer' => $keyword_referer,
			'search_engine_referer' => $search_engine_referer,
			'website' => $website,
            'ref_no' => $ref_no,
            'date_posted' => $date_posted,
			
            'keywords' => $keywords,
            'search_engine' => $search_engine,
            'browser' => $browser,
            'browser_version' => $browser_version,
            'browser_platform' => $browser_platform,
            'device_type' => $device_type
			
        );

        $modelOriginalInquiry = new Model_OriginalInquiry();
        $originalInquiryId = $modelOriginalInquiry->insert($originalInquiry);

        //get company
        $modelCompanies = new Model_Companies();
        $company = $modelCompanies->getByName($companyName);

        if (empty($company)) {
            exit;
        }

        //insert city
        $modelCities = new Model_Cities();
        $city = $modelCities->getByName($cityName);

        if (empty($city)) {
			$newCity = array(
			'city_name'=> $cityName,
			'country_id'=> 9,
			'state'=> $state,
			'postcode_key'=> $postcode,
			
			);
			$city_model = new Model_Cities();
			$id = $city_model->insert($newCity);
            //exit;
        }
		$city = $modelCities->getByName($cityName);
		
        $modelServices = new Model_Services();
        $service = $modelServices->getByServiceName($service);
        if (empty($service)) {
            exit;
        }

        $modelCustomerType = new Model_CustomerType();
        $propertyType = $modelCustomerType->getByTypeName($property_type);

        $commercial = $modelCustomerType->getByTypeName('Commercial');
        $residential = $modelCustomerType->getByTypeName('Residential ');
        if ($propertyType['customer_type_id'] == $commercial['customer_type_id']) {
            $customerTypeId = $commercial['customer_type_id'];
        } else {
            $customerTypeId = $residential['customer_type_id'];
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $tmp = uniqid('inq');
            $email = "tmp_{$tmp}@update_this_email.tmp";
        }

        $data = array(
            'title' => 'mr',
            'first_name' => $name,
            'city_id' => $city['city_id'],
            'street_address' => $address,
            'postcode' => $postcode,
            'email1' => $email,
            'phone1' => $phone,
            'mobile1' => $mobile,
            'full_name_search' => 'mr.' . $name,
            'created' => time(),
            'customer_type_id' => $customerTypeId,
            'company_id' => $company['company_id']
        );
        $modelCustomer = new Model_Customer();
        $newCustomerId = $modelCustomer->insert($data);

        $modelInquiryRequiredType = new Model_InquiryRequiredType();
        $inquiryRequiredType = $modelInquiryRequiredType->getByType($required);

        $modelPropertyType = new Model_PropertyType();
        $propertyType = $modelPropertyType->getByType($property_type);

        $modelInquiryType = new Model_InquiryType();
        $inquiryType = $modelInquiryType->getByType('Website');

        $modelUser = new Model_User();
        $user = $modelUser->getByUserCode(sha1("General {$companyName}"));

        $modelInquiry = new Model_Inquiry();
        $data = array(
            'title' => "from {$companyName}",
            'inquiry_type_id' => $inquiryType['inquiry_type_id'],
            'comment' => $comment,
            'city_id' => $city['city_id'],
            'customer_id' => $newCustomerId,
            'created' => time(),
            'user_id' => $user['user_id'],
            'full_text_search' => "from {$companyName} " . $comment,
            'original_inquiry_id' => $originalInquiryId,
            'company_id' => $company['company_id'],
            'property_type_id' => (isset($propertyType['id']) ? $propertyType['id'] : 0),
            'required_type_id' => (isset($inquiryRequiredType['id']) ? $inquiryRequiredType['id'] : 0),
			'is_spam' => $isSpam
        );
        $inquiryId = $modelInquiry->insert($data, $company['company_id']);

        if ($inquiryId) {

            /*
             * add Inquiry Address
             */
            $db_params = array();
            $db_params['inquiry_id'] = $inquiryId;
            $db_params['street_address'] = $address;
            $db_params['suburb'] = '';
            $db_params['state'] = '';
            $db_params['unit_lot_number'] = '';
            $db_params['street_number'] = '';
            $db_params['postcode'] = $postcode;
            $db_params['po_box'] = '';
            $modelInquiryAddress = new Model_InquiryAddress();
            $modelInquiryAddress->insert($db_params);


            /*
             * add service to inquiry
             */
            $params = array(
                'service_id' => $service['service_id'],
                'inquiry_id' => $inquiryId,
                'clone' => 0
            );
            $modelInquiryService = new Model_InquiryService();
            $modelInquiryService->insert($params);


            /*
             * add floor to inquiry
             */
            $modelAttributeListValue = new Model_AttributeListValue();
            $attributeValue = $modelAttributeListValue->getByAttributeValue($floor);
			 $modelInquiryServiceAttributeValue = new Model_InquiryServiceAttributeValue();
            if ($attributeValue) {
                $modelServiceAttribute = new Model_ServiceAttribute();
                $serviceAttribute = $modelServiceAttribute->getByAttributeIdAndServiceId($attributeValue['attribute_id'], $service['service_id']);

                if ($serviceAttribute) {
                    $data = array(
                        'service_attribute_id' => $serviceAttribute['service_attribute_id'],
                        'inquiry_id' => $inquiryId,
                        'value' => $attributeValue['attribute_value_id'],
                        'clone' => 0
                    );
                   
                    $modelInquiryServiceAttributeValue->insert($data);
                }
            }
			
			
			/*
             * add size == quantity to inquiry
             */
            $modelAttribute = new Model_Attributes();
            $attribute = $modelAttribute->getByVariableName('quantity');

            if ($attribute) {
                $modelServiceAttribute = new Model_ServiceAttribute();
                $serviceAttribute = $modelServiceAttribute->getByAttributeIdAndServiceId($attribute['attribute_id'], $service['service_id']);

                if ($serviceAttribute) {
                    $data = array(
                        'service_attribute_id' => $serviceAttribute['service_attribute_id'],
                        'inquiry_id' => $inquiryId,
                        'value' => $size,
                        'clone' => 0
                    );
                    
                    $modelInquiryServiceAttributeValue->insert($data);
                }
            }
        }
        exit;
    }
	
	
	public function get_keyword($referer){
    $search_phrase = '';
    $engines = array('dmoz'     => 'q=',
                     'aol'      => 'q=',
                     'ask'      => 'q=',
                     'google'   => 'q=',
                     'bing'     => 'q=',
                     'hotbot'   => 'q=',
                     'teoma'    => 'q=',
                     'yahoo'    => 'p=',
                     'altavista'=> 'p=',
                     'lycos'    => 'query=',
                     'kanoodle' => 'query='
                     );
 
    foreach($engines as $engine => $query_param) {
        // Check if the referer is a search engine from our list.
        // Also check if the query parameter is valid.
        if (strpos($referer, $engine.".") !==  false && 
            strpos($referer, $query_param) !==  false) {
 
            // Grab the keyword from the referer url
            $referer .= "&";
            $pattern = "/[?&]{$query_param}(.*?)&/si";
            preg_match($pattern, $referer, $matches);
            $search_phrase = urldecode($matches[1]);
            return array($engine, $search_phrase);
        }   
    }
    return;
	}
	//////////

    public function deleteForeverAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('inquiryDeleteForever'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
        if ($id) {
            $ids[] = $id;
        }

        $modelInquiry = new Model_Inquiry();

        $success_array = array();
        foreach ($ids as $id) {
            $success_array[$id] = 0;
            if (CheckAuth::checkIfCanHandelAllCompany('inquiry', $id)) {
                if ($modelInquiry->checkIfCanSeeInquiry($id)) {
                    $modelInquiry->deleteById($id);
                    $success_array[$id] = 1;
                }
            }
        }

        if (!in_array(0, $success_array)) {
            if (count($success_array) > 1) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "The selected Inquiries have been deleted"));
            } else {
                $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "This Inquiry has been deleted"));
            }
        } else {
            if (count($success_array) > 1) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "The selected  Inquiries could not be deleted"));
            } else {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Inquiry could not be deleted"));
            }
        }

        $this->_redirect($this->router->assemble(array(), 'showDeletedInquiry'));
    }

    public function showDeletedAction() {
        //
        //authinticat
        //
        if (CheckAuth::checkCredential(array('canSeeDeletedInquiry'))) {

            //
            // get request parameters
            //
            $orderBy = $this->request->getParam('sort', 'inquiry_id');
            $sortingMethod = $this->request->getParam('method', 'desc');
            $currentPage = $this->request->getParam('page', 1);
            $filters = $this->request->getParam('fltr', array());

            if ($filters) {
                foreach ($filters as &$filter) {
                    if (!is_array($filter)) {
                        $filter = trim($filter);
                    }
                }
            }
            $this->view->sub_menu = 'deleted_inquiry';
            //
            //get all deleted customers
            //
            $filters['is_deleted'] = 1;



            //
            // init pager and articles model object
            //
            $pager = new Model_Pager();
            $pager->perPage = get_config('perPage');
            $pager->currentPage = $currentPage;
            $pager->url = $_SERVER['REQUEST_URI'];

            //
            // get data list
            //
            $modelService = new Model_Services();
            $modelInquiryService = new Model_InquiryService();
            $modelInquiry = new Model_Inquiry();
            $data = $modelInquiry->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);

            foreach ($data as &$row) {
                $inquiryServices = $modelInquiryService->getByInquiryId($row['inquiry_id']);

                $allService = array();
                foreach ($inquiryServices as $inquiryService) {
                    $service = $modelService->getById($inquiryService['service_id']);
                    $allService[$service['service_id']] = $service['service_name'];
                }
                $row['allService'] = $allService;
            }

            $this->view->data = $data;
            $this->view->perPage = $pager->perPage;
            $this->view->pageLinks = $pager->getPager();
        } else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view deleted Inquiries"));
            $this->_redirect($this->router->assemble(array(), 'inquiry'));
        }
    }

    public function sendInquiryAdvertisingEmailAction() {

        //
        // get params 
        //
        $inquiryId = $this->request->getParam('id', 0);

        if (!CheckAuth::checkIfCanHandelAllCompany('inquiry', $inquiryId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Inquiry not belongs to your Company"));
            $this->_redirect($this->router->assemble(array(), 'inquiry'));
        }

        //
        // load models
        //
        $modelInquiry = new Model_Inquiry();
        $modelCustomer = new Model_Customer();
        $modelInquiryAddress = new Model_InquiryAddress();
        $modelInquiryService = new Model_InquiryService();


        //
        // geting data
        //
        $inquiry = $modelInquiry->getById($inquiryId);

        //
        // validation
        //
        if (!$inquiry) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Inquiry not exist"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        if (!$modelInquiry->checkIfCanSeeInquiry($inquiryId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this Inquiry"));
            $this->_redirect($this->router->assemble(array(), 'inquiry'));
        }

        //
        // filling extra data
        //
        $customer = $modelCustomer->getById($inquiry['customer_id']);

        // get all images
        $allIimageAttachments = $this->getImageAttachmentsbyInquiryId($inquiryId);

        $image_attachments = '';
        foreach ($allIimageAttachments as $key => $imageAttachments) {
            $image_attachments .= "<div><span style='font-weight: bold;'>Floor :{$key}</span>";
            foreach ($imageAttachments as $imageAttachment) {

                $large_path = Zend_Controller_Front::getInstance()->getBaseUrl() . '/uploads/image_attachment/' . $imageAttachment['large_path'];
                $description = $imageAttachment['description'];

                $image_attachments .= "<div style='margin-top: 15px;'>";
                if ($description) {
                    $image_attachments .= "<div>";
                    $image_attachments .= "<span style='font-weight: bold;'>{$description}</span>";
                    $image_attachments .= "</div>";
                }
                $image_attachments .= "<div>";
                $image_attachments .= "<img width='510' data-mce-src='{$large_path}' alt='{$description}' src='{$large_path}'/>";
                $image_attachments .= "</div>";
                $image_attachments .= "</div>";
            }
            $image_attachments .= "</div>";
        }

        $template_params = array(
            //imageAttachments
            '{image_attachments}' => $image_attachments,
            //inquiry
            '{inquiry_num}' => $inquiry['inquiry_num'],
            '{comment}' => $inquiry['comment'] ? $inquiry['comment'] : '',
            '{inquiry_created}' => date('d/m/Y', $inquiry['created']),
            '{inquiry_address}' => get_line_address($modelInquiryAddress->getByInquiryId($inquiryId)),
            '{service}' => nl2br($modelInquiryService->getByInquiryIdAsText($inquiryId)),
            //customer
            '{customer_name}' => get_customer_name($customer),
            '{customer_first_name}' => isset($customer['first_name']) && $customer['first_name'] ? ucwords($customer['first_name']) : '',
            '{customer_last_name}' => isset($customer['last_name']) && $customer['last_name'] ? ' ' . ucwords($customer['last_name']) : '',
            '{customer_contacts}' => nl2br($modelCustomer->getCustomerContacts($inquiry['customer_id']))
        );

        $modelEmailTemplate = new Model_EmailTemplate();
        $emailTemplate = $modelEmailTemplate->getEmailTemplate('send_inquiry_advertising_email', $template_params);

        $body = $emailTemplate['body'];
        $subject = $emailTemplate['subject'];
        $to = array();
        if ($customer['email1']) {
            $to[] = $customer['email1'];
        }
        if ($customer['email2']) {
            $to[] = $customer['email2'];
        }
        if ($customer['email2']) {
            $to[] = $customer['email3'];
        }
        $to = implode(',', $to);

        if ($this->request->isPost()) {
            $to = $this->request->getParam('to');
            $cc = $this->request->getParam('cc');
            $subject = $this->request->getParam('subject');
            $body = $this->request->getParam('body');


            $params = array(
                'to' => $to,
                'cc' => $cc,
                'body' => $body,
                'subject' => $subject,
                'layout' => 'designed_email_template'
            );

            $error_mesages = array();
            if (EmailNotification::validation($params, $error_mesages)) {

                // Send Email
                $success = EmailNotification::sendEmail($params, '', array(), array('reference_id' => $inquiryId, 'type' => 'inquiry'));

                if ($success) {

                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Email sent successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Email not sent"));
                }

                echo 1;
                exit;
            }
            $this->view->error_mesages = $error_mesages;
        }

        $this->view->inquiry = $inquiry;
        $this->view->to = $to;
        $this->view->subject = $subject;
        $this->view->body = $body;
        $this->view->cc = isset($cc) ? $cc : '';

        echo $this->view->render('index/send-inquiry-advertising-email.phtml');
        exit;
    }

    public function getImageAttachmentsbyInquiryId($inquiryId) {


        $modelInquiryService = new Model_InquiryService();
        $modelServiceAttribute = new Model_ServiceAttribute();
        $modelAttributeListValue = new Model_AttributeListValue();
        $modelInquiryServiceAttributeValue = new Model_InquiryServiceAttributeValue();
        $modelAttributeType = new Model_AttributeType();
        $modelAttributes = new Model_Attributes();
        $modelImageAttachment = new Model_ImageAttachment();

        $allInquiryService = $modelInquiryService->getByInquiryId($inquiryId);

        $allImagesInquires = array();
        foreach ($allInquiryService as $inquiryService) {

            $attribute = $modelAttributes->getByVariableName('Floor');

            //get service attribute
            $serviceAttribute = $modelServiceAttribute->getByAttributeIdAndServiceId($attribute['attribute_id'], $inquiryService['service_id']);

            $inquiryServiceAttributeValue = $modelInquiryServiceAttributeValue->getByInquiryIdAndServiceAttributeIdAndClone($inquiryId, $serviceAttribute['service_attribute_id'], $inquiryService['clone']);

            $attributeListValue = $modelAttributeListValue->getById($inquiryServiceAttributeValue['value']);

            // get images
            $limit = get_config('image_attachments_limit');
            $allImagesInquires[$attributeListValue['attribute_value']] = $modelImageAttachment->getByAttributListValueId($attributeListValue['attribute_value_id'], $limit);
        }

        return $allImagesInquires;
    }

    public function removeFollowUpInquiryAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('inquiryEdit'));

        //
        // get params
        //
        $inquiryId = $this->request->getParam('id');

        //
        // load model
        //
        $modelInquiry = new Model_Inquiry();
        $inquiry = $modelInquiry->getById($inquiryId);

        if (!$inquiry) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Inquiry not exist"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        if (!$modelInquiry->checkIfCanSeeInquiry($inquiryId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this Inquiry"));
            $this->_redirect($this->router->assemble(array(), 'inquiry'));
        }

        $modelInquiry->updateById($inquiryId, array('is_to_follow' => 0));

        $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
    }

    public function followUpInquiryAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('inquiryEdit'));

        //
        // get params
        //
        $inquiryId = $this->request->getParam('id');
        $followDate = $this->request->getParam('followDate');


        //
        // load model
        //
        $modelInquiry = new Model_Inquiry();
        $inquiry = $modelInquiry->getById($inquiryId);

        if (!$inquiry) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Inquiry not exist"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        if (!$modelInquiry->checkIfCanSeeInquiry($inquiryId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this Inquiry"));
            $this->_redirect($this->router->assemble(array(), 'inquiry'));
        }

        //
        // init action form
        //
        $form = new Inquiry_Form_FollowUPInquiry(array('inquiry' => $inquiry));

        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'is_to_follow' => 1,
                    'deferred_date' => mySql2PhpTime($followDate),
                );

                $success = $modelInquiry->updateById($inquiryId, $data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Change in Follow up"));
                }

                echo 1;
                exit;
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('index/follow-up-inquiry.phtml');
        exit;
    }

    public function modifyInquiryFollowDateAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('inquiryEdit'));


        $inquiryId = $this->request->getParam('inquiry_id', 0);
        $followDate = $this->request->getParam('follow_date', 0);
        $followDate = strtotime($followDate);

        $modelInquiry = new Model_Inquiry();
        $inquiry = $modelInquiry->getById($inquiryId);

        if (!$inquiry) {
            echo 1;
            exit;
        }

        $modelInquiry->updateById($inquiryId, array('deferred_date' => $followDate));
		
		$followDates = getDateFormating($followDate);
		
		
        echo json_encode(array('st' => 1, 'msg' => $followDates));;
        exit;
    }
	public function getAllContractorDistancesAction() {
		/*
		// Load models
		*/
		$modelContractorService = new Model_ContractorService();
		$modelInquiry = new Model_Inquiry();
		$modelInquiryAddress = new Model_InquiryAddress();
		$modelBookingAddress = new Model_BookingAddress();
        $modelUser = new Model_User();
		 //
        // get request parameters
        //
        $inquiry_id = $this->request->getParam('inquiry_id', 0);
		$service_ids = $this->request->getParam('service_ids', array());
		
		// get inquiry details by id to get city_id 
		$inquiry = $modelInquiry->getById($inquiry_id);
		$cityId = $inquiry['city_id'];
		
		// get inquiry address by id, we need it to calculate the distance between it and contractor address
		$inquiryAddress =$modelInquiryAddress->getByInquiryId($inquiry_id);
		
		// check service if is serliaze when return from tb_show
        $service_ids = $this->is_serial($service_ids) ? unserialize($service_ids) : $service_ids;
		
		// Remove Redundancy
        $removeRedundancy = array();
        foreach ($service_ids as $service_id) {
            $removeRedundancy[$service_id] = $service_id;
        }
        $service_ids = $removeRedundancy;
		
		/// get get contractor ids by city_id and service_id
		if (!empty($service_ids)) {
            foreach ($service_ids as $service_id) {
					if ($cityId) {
						$ContractorServices = $modelContractorService->getContractorByServiceIdAndCityId($service_id, $cityId);
					} else {
						$ContractorServices = $modelContractorService->getContractorByServiceId($service_id);
					}
				foreach ($ContractorServices as $ContractorService) {
					 $contractor_ids[$ContractorService['contractor_id']] = $ContractorService['contractor_id'];
				}
            }
        }
		
		/// here calculate the distance between each contractor and inquiry address
		$contractorDistances = array();
        foreach ($contractor_ids as $contractorId) {
			//// delete inquiries from contractor list
			if($contractorId != 1){
				$contractor = $modelUser->getById($contractorId);

				$contractorDistances[$contractor['user_id']] = array(
					'contractor_id' => $contractor['user_id'],
					'name' => $contractor['username'],
					'distance' => $modelBookingAddress->getDistanceByTwoAddress($contractor, $inquiryAddress)
				);
			}
        }

        
		$this->view->contractorDistances = $contractorDistances;
		
		echo $this->view->render('index/get-all-contractor-distances.phtml');
        exit;
		
	}
	
	public function is_serial($data) {
        $data = @unserialize($data);
        if ($data === false) {
            return false;
        } else {
            return true;
        }
    }
	
	
	public function advanceSearchAction() {
			
				echo $this->view->render('index/filters.phtml');
			exit;
			}


}