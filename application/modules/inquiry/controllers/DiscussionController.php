<?php

class Inquiry_DiscussionController extends Zend_Controller_Action {

    private $request;
    private $router;
    private $loggedUser;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        //
        //checkLoggedIn
        //
        CheckAuth::checkLoggedIn();

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('inquiryDiscussion'));

        //
        //get Logged User
        //
        $this->loggedUser = CheckAuth::getLoggedUser();
    }

    public function indexAction() {

        //
        //get Params
        //
        $inquiryId = $this->request->getParam('id', 0);

        //
        // load model
        //
        $modelInquiry = new Model_Inquiry();
        if (!$modelInquiry->checkIfCanSeeInquiry($inquiryId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect($this->router->assemble(array(), 'inquiry'));
        }
        $modelInquiryDiscussion = new Model_InquiryDiscussion();
        $modelUser = new Model_User();
		$modelImageAttachment = new Model_Image();

        $inquiry = $modelInquiry->getById($inquiryId);
        $this->view->inquiry = $inquiry;

        if (!$inquiry) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Item not found"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

    $inquiryDiscussions = $modelInquiryDiscussion->getByInquiryId($inquiryId, 'DESC');
	$GroupsinquiryImageDiscussions = $modelInquiryDiscussion->getByInquiryId($inquiryId, 'DESC' , array('groups'=>'groups'));
	$inquiryDiscussions = array_merge($inquiryDiscussions,$GroupsinquiryImageDiscussions);
	array_multisort($inquiryDiscussions ,SORT_ASC );
        foreach ($inquiryDiscussions as &$inquiryDiscussion) {
            $inquiryDiscussion['user'] = $modelUser->getById($inquiryDiscussion['user_id']);
			
        }

        $this->view->inquiryDiscussions = $inquiryDiscussions;

        echo $this->view->render('discussion/index.phtml');
        exit;
    }

    public function submitAction() {

        //
        //get Params
        //
        $inquiryId = $this->request->getParam('id', 0);
        $discussion = $this->request->getParam('discussion', '');
		$imageId = $this->request->getParam('imageId', 0);

        //
        // load model
        //
        $modelInquiryDiscussion = new Model_InquiryDiscussion();

        $success = 0;
        if ($discussion) {
            $data = array(
                'inquiry_id' => $inquiryId,
                'user_id' => $this->loggedUser['user_id'],
                'user_message' => $discussion,
                'created' => time()
            );
            $success = $modelInquiryDiscussion->insert($data);
			
			$modelItemImageDiscussion =new Model_ItemImageDiscussion();
				 
			$dataDiscssion = array(
				 'image_id'=>$imageId,
				 'group_id' =>0,
				 'item_id'=>$success,
				 'type'=>'inquiry'
				 );
			$modelItemImageDiscussion->insert($dataDiscssion);
			
        }

        if ($success) {
            echo json_encode(array('success' => 1));
        } else {
            echo json_encode(array('success' => 0, 'errMsg' => 'Invalid discussion entry'));
        }
        exit;
    }

    public function getAllDiscussionAction() {

        //
        //get Params
        //
        $inquiryId = $this->request->getParam('id', 0);

        //
        // load model
        //
        $modelInquiryDiscussion = new Model_InquiryDiscussion();
        $modelUser = new Model_User();
		$modelImageAttachment = new Model_Image();


    $inquiryDiscussions = $modelInquiryDiscussion->getByInquiryId($inquiryId, 'DESC');
	$GroupsinquiryImageDiscussions = $modelInquiryDiscussion->getByInquiryId($inquiryId, 'DESC' , array('groups'=>'groups'));
	$inquiryDiscussions = array_merge($inquiryDiscussions,$GroupsinquiryImageDiscussions);
	array_multisort($inquiryDiscussions ,SORT_ASC );
        foreach ($inquiryDiscussions as &$inquiryDiscussion) {
            $inquiryDiscussion['user'] = $modelUser->getById($inquiryDiscussion['user_id']);
            $inquiryDiscussion['discussion_date'] = getDateFormating( $inquiryDiscussion['created']);
            $inquiryDiscussion['user_message'] = nl2br(htmlentities($inquiryDiscussion['user_message'], ENT_NOQUOTES, 'UTF-8'));
			if($inquiryDiscussion['group_id']){
			 $inquiryDiscussion['grouped_images'] = $modelImageAttachment->getByGroupId($inquiryDiscussion['group_id']);
			}
			
        }

        $json_array = array(
            'discussions' => $inquiryDiscussions,
            'count' => count($inquiryDiscussions)
        );

        echo json_encode($json_array);
        exit;
    }
	
	
	public function getAllImageDiscussionAction() {
      
        //get Params
        //
       $imageId = $this->request->getParam('id', 0);

        //
        // load model
        //
        $modelInquiryDiscussion = new Model_InquiryDiscussion();
        $modelContractorInfo = new Model_ContractorInfo();
        $modelUser = new Model_User();

        $imageDiscussions = $modelInquiryDiscussion->getByImageId($imageId,'Desc');
       
        foreach ($imageDiscussions as &$imageDiscussion) {
            $imageDiscussion['user'] = $modelUser->getById($imageDiscussion['user_id']);
            $imageDiscussion['discussion_date'] = getDateFormating( $imageDiscussion['created']);
            $imageDiscussion['user_message'] = nl2br(htmlentities($imageDiscussion['user_message'], ENT_NOQUOTES, 'UTF-8'));
            if ($imageDiscussion['user']['role_name'] == 'contractor') {
                $contractorInfo = $modelContractorInfo->getByContractorId($imageDiscussion['user']['user_id']);
                $imageDiscussion['user']['username'] = ucwords($imageDiscussion['user']['username']) . ' - ' . ucwords($contractorInfo['business_name']);
            } else {
                $imageDiscussion['user']['username'] = ucwords($imageDiscussion['user']['username']);
            }
        }

        $json_array = array(
            'discussions' => $imageDiscussions,
            'count' => count($imageDiscussions)
        );
		
	

        echo json_encode($json_array);
        exit;
    }
	
	
	public function updateDeletedCommentAction(){
		$discussion_id = $this->request->getParam('discussion_id');
		$inquiry_id = $this->request->getParam('inquiry_id');
		$have_images = $this->request->getParam('have_images', 0);
		$image_id = $this->request->getParam('image_id',0);
		$group_id = $this->request->getParam('group_id',0);
		$success = 0;
		
		$modelInquiryDiscussion = new Model_InquiryDiscussion();
		
		$data = array(
            'is_deleted' => 1             
        );
		
		$success = $modelInquiryDiscussion->updateById($discussion_id, $data);
		if($have_images){
			
			$successDelete = 0;
			$successDeletes = array();
			$modelImage = new Model_Image();
			
			if($image_id){
				
				$successDelete = $modelImage->deleteById($image_id);
			}else if($group_id){
							
				$inquiry_grouped_images = $modelImage->getByGroupIdAndType($group_id, $inquiry_id, 'inquiry');
 
				foreach($inquiry_grouped_images as $k => $img){
					$successDeletes[$k] = $modelImage->deleteById($img['image_id']);
				}
			}
			
			if($success && ($successDelete || !in_array(0,$successDeletes))){
				echo 1;				
			}
			exit;
		}
		
		if($success){
			echo 1;
		}
		exit;
		
	}
	

}

