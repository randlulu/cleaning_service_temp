<?php

class Inquiry_DiscussionController extends Zend_Controller_Action {

    private $request;
    private $router;
    private $loggedUser;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        //
        //checkLoggedIn
        //
        CheckAuth::checkLoggedIn();

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('inquiryDiscussion'));

        //
        //get Logged User
        //
        $this->loggedUser = CheckAuth::getLoggedUser();
    }

    public function indexAction() {

        //
        //get Params
        //
        $inquiryId = $this->request->getParam('id', 0);

        //
        // load model
        //
        $modelInquiry = new Model_Inquiry();
        if (!$modelInquiry->checkIfCanSeeInquiry($inquiryId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect($this->router->assemble(array(), 'inquiry'));
        }
        $modelInquiryDiscussion = new Model_InquiryDiscussion();
        $modelUser = new Model_User();

        $inquiry = $modelInquiry->getById($inquiryId);
        $this->view->inquiry = $inquiry;

        if (!$inquiry) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "There Is No Item Exists"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $inquiryDiscussions = $modelInquiryDiscussion->getByInquiryId($inquiryId);
        foreach ($inquiryDiscussions as &$inquiryDiscussion) {
            $inquiryDiscussion['user'] = $modelUser->getById($inquiryDiscussion['user_id']);
        }

        $this->view->inquiryDiscussions = $inquiryDiscussions;

        echo $this->view->render('discussion/index.phtml');
        exit;
    }

    public function submitAction() {

        //
        //get Params
        //
        $inquiryId = $this->request->getParam('id', 0);
        $discussion = $this->request->getParam('discussion', '');

        //
        // load model
        //
        $modelInquiryDiscussion = new Model_InquiryDiscussion();

        $success = 0;
        if ($discussion) {
            $data = array(
                'inquiry_id' => $inquiryId,
                'user_id' => $this->loggedUser['user_id'],
                'user_message' => $discussion,
                'created' => time()
            );
            $success = $modelInquiryDiscussion->insert($data);
        }

        if ($success) {
            echo json_encode(array('success' => 1));
        } else {
            echo json_encode(array('success' => 0, 'errMsg' => 'Invalid Discussion'));
        }
        exit;
    }

    public function getAllDiscussionAction() {

        //
        //get Params
        //
        $inquiryId = $this->request->getParam('id', 0);

        //
        // load model
        //
        $modelInquiryDiscussion = new Model_InquiryDiscussion();
        $modelUser = new Model_User();


        $inquiryDiscussions = $modelInquiryDiscussion->getByInquiryId($inquiryId);
        foreach ($inquiryDiscussions as &$inquiryDiscussion) {
            $inquiryDiscussion['user'] = $modelUser->getById($inquiryDiscussion['user_id']);
            $inquiryDiscussion['discussion_date'] = getDateFormating( $inquiryDiscussion['created']);
            $inquiryDiscussion['user_message'] = nl2br(htmlentities($inquiryDiscussion['user_message'], ENT_NOQUOTES, 'UTF-8'));
        }

        $json_array = array(
            'discussions' => $inquiryDiscussions,
            'count' => count($inquiryDiscussions)
        );

        echo json_encode($json_array);
        exit;
    }

}

