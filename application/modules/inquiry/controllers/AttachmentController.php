<?php

class Inquiry_AttachmentController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
        
        $pageName = $this->request->getParam('page_name', '');
        $this->view->page_title = (!empty($pageName))?$pageName." - Attachments - Inquiries":"Attachments - Inquiries";
    }

    public function indexAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('attachment'));

        // get request parameters
        $inquiryId = $this->request->getParam('id');
        $orderBy = $this->request->getParam('sort', 'attachment_id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        // Load Model
        $modelBookingAttachment = new Model_BookingAttachment();
        $modelInquiry = new Model_Inquiry();

        if (!$inquiryId) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        if (!$modelInquiry->checkIfCanSeeInquiry($inquiryId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect($this->router->assemble(array(), 'inquiry'));
        }

        $filters['inquiry_id'] = $inquiryId;
            
		/*//Rand
		$modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($inquiryId, 'inquiry', 'view_attachment');*/
			
        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        //
        // get data list
        //
        $inquiryAttachment = $modelBookingAttachment->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);


        //
        // set view params
        //
        $this->view->inquiryAttachment = $inquiryAttachment;
        $this->view->inquiry_id = $inquiryId;
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
    }

    public function fileUploadAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('attachmentFileUpload'));

        //
        // get request parameters
        //
        $inquiryId = $this->request->getParam('inquiry_id');

        // load model
        $modelBookingAttachment = new Model_BookingAttachment();
        $modelInquiry = new Model_Inquiry();


        if (!CheckAuth::checkIfCanHandelAllCompany('inquiry', $inquiryId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permession."));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        if (!$modelInquiry->checkIfCanSeeInquiry($inquiryId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permession."));
            $this->_redirect($this->router->assemble(array(), 'inquiry'));
        }

        $loggedUser = CheckAuth::getLoggedUser();
        $userId = $loggedUser['user_id'];
           
		 /*  //Rand
		$modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($inquiryId, 'inquiry','view_file_upload');*/
			
			
          
        //init action form
        $form = new Inquiry_Form_Attachment(array('inquiry_id' => $inquiryId));
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {

            if (!$form->isValid($this->getRequest()->getParams())) {
                echo $this->view->render('attachment/file-upload.phtml');
                exit;
            }

            if (!$form->file->receive()) {
                $this->view->message = '<div class="errors">Errors Receiving File.</div>';
                echo $this->view->render('attachment/file-upload.phtml');
                exit;
            }

            if ($form->file->isUploaded()) {
                $description = $form->description->getvalue();
                $source = $form->file->getFileName();
                $size = $form->file->getfilesize();
                $fileInfo = pathinfo($source);
                $ext = $fileInfo['extension'];
                $workOrder = $form->work_order->getvalue();

                //to re-name the file, all you need to do is save it with a new name, instead of the name they uploaded it with. Normally, I use the primary key of the database row where I'm storing the name of the image. For example, if it's an image of Person 1, I call it 1.jpg. The important thing is that you make sure the image name will be unique in whatever directory you save it to.
                //get sub dir
                $dir = get_config('attachment') . '/';
                $subdir = date('Y/m/d/');

                //check if file exists or not
                $fullDir = $dir . $subdir;

                if (!is_dir($fullDir)) {
                    mkdir($fullDir, 0777, true);
                }

                $data = array(
                    'inquiry_id' => $inquiryId,
                    'created' => time(),
                    'size' => $size,
                    'description' => $description,
                    'created_by' => $userId,
                    'work_order' => $workOrder
                );

                $attachmentId = $modelBookingAttachment->insert($data);
                $fileName = $attachmentId . '.' . $ext;

                $data = array(
                    'path' => $subdir . $fileName,
                    'file_name' => $fileInfo['basename']
                );

                $modelBookingAttachment->updateById($attachmentId, $data);

                //save image to database and filesystem here
                $file_saved = copy($source, $fullDir . $fileName);

                if ($file_saved) {

                    if (file_exists($source)) {
                        unlink($source);
                    }
                    // $this->_redirect($this->router->assemble(array('id' => $inquiryId), 'inquiryAttachment'));
                    echo 1;
                    exit;
                }
            }
        }

        //
        // render views
        //
        echo $this->view->render('attachment/file-upload.phtml');
        exit;
    }

    public function deleteAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('attachmentFileDelete'));


        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());


        // load model
        $modelBookingAttachment = new Model_BookingAttachment();
        $modelInquiry = new Model_Inquiry();

        $dir = get_config('attachment') . '/';
        if ($id) {
            $ids[] = $id;
        }
        foreach ($ids as $id) {
            $isValid = true;
            $bookingAttachment = $modelBookingAttachment->getById($id);

            if (!$bookingAttachment) {
                $isValid = false;
            }

            $inquiryId = !empty($bookingAttachment['inquiry_id']) ? $bookingAttachment['inquiry_id'] : 0;

            if ($inquiryId) {
                if (!CheckAuth::checkIfCanHandelAllCompany('inquiry', $inquiryId)) {
                    $isValid = false;
                }
                if (!$modelInquiry->checkIfCanSeeInquiry($inquiryId)) {
                    $isValid = false;
                }
            } else {
                $isValid = false;
            }

            if ($isValid) {
                $bookingAttachment = $modelBookingAttachment->getById($id);
                // get source file
                $source = $dir . $bookingAttachment['path'];

                // delete source file
                unlink($source);

                $modelBookingAttachment->deleteById($id);
            }
        }
        $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
    }

    public function downloadAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('attachmentFileDownload'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);

        // load model
        $modelBookingAttachment = new Model_BookingAttachment();
        $modelInquiry = new Model_Inquiry();


        $bookingAttachment = $modelBookingAttachment->getById($id);

        if (!$bookingAttachment) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "File not found"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $inquiryId = !empty($bookingAttachment['inquiry_id']) ? $bookingAttachment['inquiry_id'] : 0;

        if ($inquiryId) {
            if (!CheckAuth::checkIfCanHandelAllCompany('inquiry', $inquiryId)) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permession"));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            }
            if (!$modelInquiry->checkIfCanSeeInquiry($inquiryId)) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permession"));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            }
        } else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permession"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }


        $path = $bookingAttachment['path'];
        $dir = get_config('attachment') . '/';
        $filename = "{$dir}/{$path}";
        $file_extension = strtolower(substr(strrchr($filename, "."), 1));

        switch ($file_extension) {
            case "pdf": $ctype = "application/pdf";
                break;
            case "exe": $ctype = "application/octet-stream";
                break;
            case "zip": $ctype = "application/zip";
                break;
            case "doc": $ctype = "application/msword";
                break;
            case "xls": $ctype = "application/vnd.ms-excel";
                break;
            case "ppt": $ctype = "application/vnd.ms-powerpoint";
                break;
            case "gif": $ctype = "image/gif";
                break;
            case "png": $ctype = "image/png";
                break;
            case "jpe": case "jpeg":
            case "jpg": $ctype = "image/jpg";
                break;
            default: $ctype = "application/force-download";
        }

        if (!file_exists($filename)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => 'file not found'));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private", false);
        header("Content-Type: $ctype");
        header("Content-Disposition: attachment; filename=\"" . basename($filename) . "\";");
        header("Content-Transfer-Encoding: binary");
        header("Content-Length: " . @filesize($filename));
        set_time_limit(0);
        readfile("$filename") or die("File not found.");
        exit;
    }

    public function editDescriptionAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('attachmentFileEditDescription'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $description = $this->request->getParam('description');

        // load model
        $modelBookingAttachment = new Model_BookingAttachment();
        $modelInquiry = new Model_Inquiry();


        $bookingAttachment = $modelBookingAttachment->getById($id);

        if (!$bookingAttachment) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "file not found"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $inquiryId = !empty($bookingAttachment['inquiry_id']) ? $bookingAttachment['inquiry_id'] : 0;

        if ($inquiryId) {
            if (!CheckAuth::checkIfCanHandelAllCompany('inquiry', $inquiryId)) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permession"));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            }
            if (!$modelInquiry->checkIfCanSeeInquiry($inquiryId)) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permession"));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            }
        } else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permession"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        /* //Rand
		$modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, 'inquiry', 'view_edit_attachment_description');*/
 
        //
        // init action form
        //      = 
        $form = new Inquiry_Form_DescriptionFile(array('description' => $bookingAttachment['description'], 'id' => $id));

        //
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'description' => $description
                );

                $success = $modelBookingAttachment->updateById($id, $data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes to apply to description"));
                }

                echo 1;
                exit;
            }
        }

        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('attachment/edit-description.phtml');
        exit;
    }

    public function editWorkOrderAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('attachmentEditWorkOrder'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $workOrder = $this->request->getParam('work_order');

        // load model
        $modelBookingAttachment = new Model_BookingAttachment();
        $modelInquiry = new Model_Inquiry();


        $bookingAttachment = $modelBookingAttachment->getById($id);

        if (!$bookingAttachment) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "file not found"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $inquiryId = !empty($bookingAttachment['inquiry_id']) ? $bookingAttachment['inquiry_id'] : 0;

        if ($inquiryId) {
            if (!CheckAuth::checkIfCanHandelAllCompany('inquiry', $inquiryId)) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permession"));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            }
            if (!$modelInquiry->checkIfCanSeeInquiry($inquiryId)) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permession"));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            }
        } else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permession"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $workOrder = !empty($workOrder) ? 0 : 1;

        $data = array(
            'work_order' => $workOrder
        );

        $success = $modelBookingAttachment->updateById($id, $data);

        if ($success) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved"));
        } else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes to apply to work order"));
        }
        $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
    }

    public function viewDescriptionAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('attachmentFileViewDescription'));

        //
        // get request parameters
        //
       
        $id = $this->request->getParam('id');


        // load model
        $modelBookingAttachment = new Model_BookingAttachment();
        $modelInquiry = new Model_Inquiry();

        $bookingAttachment = $modelBookingAttachment->getById($id);

        $inquiryId = !empty($bookingAttachment['inquiry_id']) ? $bookingAttachment['inquiry_id'] : 0;

        if ($inquiryId) {
            if (!CheckAuth::checkIfCanHandelAllCompany('inquiry', $inquiryId)) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permession"));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            }
            if (!$modelInquiry->checkIfCanSeeInquiry($inquiryId)) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permession"));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            }
        } else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permession"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
         
		/* //Rand
		$modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, 'inquiry', 'view_attachment_description');*/
 
 
        $this->view->bookingAttachment = $bookingAttachment;
        //
        // render views
        //
        echo $this->view->render('attachment/view-description.phtml');
        exit;
    }

}

