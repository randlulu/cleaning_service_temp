<?php

class Inquiry_Form_Attachment extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $inquiryId = (isset($options['inquiry_id']) ? $options['inquiry_id'] : '');


        $router = Zend_Controller_Front::getInstance()->getRouter();
        $request = Zend_Controller_Front::getInstance()->getRequest();


        $file = new Zend_Form_Element_File('file');
        $file->setDestination(get_config('temp_uploads'))
                ->setDecorators(array('File', 'Errors', array('Description', array('tag' => 'p', 'class' => 'hint'))))
                ->setRequired(true)
                ->setMaxFileSize(get_config('upload_max_filesize')) // limits the filesize on the client side
                ->setDescription('Click Browse and click on the  file you would like to upload');
        $file->addValidator('Count', false, 1);  // ensure only 1 file
        $file->addValidator('Size', false, get_config('upload_max_filesize')); //10240000 limit to 10 meg


        $description = new Zend_Form_Element_Textarea('description');
        $description->setDecorators(array('ViewHelper', 'Errors'))
                ->setAttribs(array('class' => 'textarea_field', 'cols' => '40', 'rows' => '10'));
        $workOrder = new Zend_Form_Element_Checkbox('work_order');
        $workOrder->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'checkbox_field'));

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'button'));

        $this->addElements(array($description, $file, $workOrder,$button));

        $this->setName('attachment');
        $this->setMethod(Zend_Form::METHOD_POST);
        $this->setEnctype(Zend_Form::ENCTYPE_MULTIPART);
        $this->setAction($router->assemble(array('inquiry_id' => $inquiryId), 'inquiryFileUpload'));
    }

}
