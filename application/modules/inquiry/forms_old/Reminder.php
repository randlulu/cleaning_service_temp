<?php

class Inquiry_Form_Reminder extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('Reminder');

        $reminder = (isset($options['reminder']) ? $options['reminder'] : '');
        $inquiryId = (isset($options['inquiry_id']) ? $options['inquiry_id'] : '');
        $mode = (isset($options['mode']) ? $options['mode'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();


        //
        // comment
        //
        $comment = new Zend_Form_Element_Textarea('comment');
        $comment->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'textarea_field', 'style' => 'width: 330px;'))
                ->setValue((!empty($reminder['comment']) ? $reminder['comment'] : ''));

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'button'));

        $this->addElements(array($comment, $button));

        if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $reminder['id']), 'inquiryReminderView'));
        } else {
            $this->setAction($router->assemble(array('inquiry_id' => $inquiryId), 'inquiryReminderAdd'));
        }
    }

}

