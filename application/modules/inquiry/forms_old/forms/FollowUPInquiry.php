<?php

class Inquiry_Form_FollowUPInquiry extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('FollowUPInquiry');

        $inquiry = (isset($options['inquiry']) ? $options['inquiry'] : '');

        $followDateVal = (!empty($inquiry['deferred_date']) ? php2JsTime($inquiry['deferred_date']) : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();
        $request = Zend_Controller_Front::getInstance()->getRequest();


        $followDate = new Zend_Form_Element_Text('followDate');
        $followDate->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'date text_input form-control'))
                ->setValue($followDateVal);


        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));
        $button->setAttribs(array('style' => 'margin-top: 10px;'));

        $this->addElements(array($followDate, $button));
        $this->setMethod('post');
        $this->setAction($router->assemble(array('id' => $inquiry['inquiry_id']), 'followUpInquiry'));
    }

}

