<?php

class Reports_Form_ChartFilters extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('filters');
        $actionUrl = (isset($options['action_url']) ? $options['action_url'] : '');

        $request = Zend_Controller_Front::getInstance()->getRequest();
        $filters = $request->getParam('fltr', array());

        $startTimeBetween = new Zend_Form_Element_Text('start');
        $startTimeBetween->setBelongsTo('fltr');
        $startTimeBetween->setAttribs(array('id' => "start_time", 'readonly' => "readonly", 'class'=> 'form-control'));
        $startTimeBetween->setDecorators(array('ViewHelper'));
        $startTimeBetween->setValue(isset($filters['start']) ? $filters['start'] : '');

        $endTimeBetween = new Zend_Form_Element_Text('end');
        $endTimeBetween->setBelongsTo('fltr');
        $endTimeBetween->setAttribs(array('id' => "end_time", 'readonly' => "readonly", 'class' => "form-control"));
        $endTimeBetween->setDecorators(array('ViewHelper'));
        $endTimeBetween->setValue(isset($filters['end']) ? $filters['end'] : '');


        $mode = new Zend_Form_Element_Select('mode');
        $mode->setBelongsTo('fltr');
        $mode->setAttribs(array('class' => "form-control"));
        $mode->setDecorators(array('ViewHelper'));
        $mode->setValue(isset($filters['mode']) ? $filters['mode'] : 'monthly');
        $mode->addMultiOptions(array('yearly' => 'Yearly', 'monthly' => 'Monthly', 'weekly' => 'Weekly', 'daily' => 'Daily'));

        $by = new Zend_Form_Element_Select('by');
        $by->setBelongsTo('fltr');
        $by->setAttribs(array('class' => "form-control"));
        $by->setDecorators(array('ViewHelper'));
        $by->setValue(isset($filters['by']) ? $filters['by'] : 'count');
        $by->addMultiOptions(array('count' => 'Count', 'total' => 'Total'));

        $type = new Zend_Form_Element_Select('type');
        $type->setBelongsTo('fltr');
        $type->setAttribs(array('class' => "form-control"));
        $type->setDecorators(array('ViewHelper'));
        $type->setValue(isset($filters['type']) ? $filters['type'] : '');
        $type->addMultiOption('', 'Select One');
        $modelDailyReport = new Model_DailyReport();
        $type->addMultiOptions($modelDailyReport->getTypes());


        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Search');
        $button->setAttribs(array('class' => 'btn btn-primary'));

        $this->addElements(array($startTimeBetween, $endTimeBetween, $type, $by, $mode, $button));
        $this->setMethod('POST');

        $this->setAction($actionUrl);
    }

}

