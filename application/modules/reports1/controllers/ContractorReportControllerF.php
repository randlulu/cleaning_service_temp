<?php

class Reports_ContractorReportController extends Zend_Controller_Action {

    private $request;
    private $router;
    private $LoggedUser;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'reports';
        $this->view->sub_menu = 'reports';

        $this->LoggedUser = CheckAuth::getLoggedUser();
    }

    public function contractorsDetailedSummaryAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('reportContractorsDetailedSummary'));

        $modelBooking = new Model_Booking();
        $modelUser = new Model_User();
        $modelBookingStatus = new Model_BookingStatus();
        $modelAuthRole = new Model_AuthRole();
        $modelContractorInfo = new Model_ContractorInfo();
        $modelPayment = new Model_Payment;
        $modelPaymentType = new Model_PaymentType();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
		////By Islam we need it to test if it paid to contractor or not 
		$modelBookingContractorPayment = new Model_BookingContractorPayment();
		////////
        $form = new Reports_Form_ContractorFilters(array('action_url' => $this->router->assemble(array(), 'reportContractorsDetailedSummary'), 'type' => 'contractors_detailed_summary'));
        $this->view->form = $form;

        $bookingStatuses = $modelBookingStatus->getAll();
        $inProcess = $modelBookingStatus->getByStatusName('IN PROGRESS');
        $inProcess_invoiced = array('booking_status_id' => $inProcess['booking_status_id'], 'name' => 'IN PROGRESS (invoice)');
        array_push($bookingStatuses, $inProcess_invoiced);

        // Set Completed ,Faild and IN PROGRESS(invoiced) in the last array to display in the last columns in the reports
        foreach ($bookingStatuses as $key => $bookingStatus) {
            if ($bookingStatus['name'] == 'FAILED') {
                unset($bookingStatuses[$key]);
                $bookingStatus['name'] .= ' (invoice)';
                array_push($bookingStatuses, $bookingStatus);
            }
        }
        foreach ($bookingStatuses as $key => $bookingStatus) {
            if ($bookingStatus['name'] == 'COMPLETED') {
                unset($bookingStatuses[$key]);
                $bookingStatus['name'] .= ' (invoice)';
                array_push($bookingStatuses, $bookingStatus);
            }
        }
        $this->view->bookingStatuses = $bookingStatuses;

        //Payment Type
        $paymentTypes = $modelPaymentType->getAll(array('without_cash' => true));
        $this->view->paymentTypes = $paymentTypes;

        $data = array();
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        if ($filters) {
            $contractorId = isset($filters['contractor_id']) && $filters['contractor_id'] ? $filters['contractor_id'] : 0;
            $user = $modelUser->getById($contractorId);
            $this->view->user = $user;

            if (CheckAuth::checkCredential(array('canSeeAllContractorsDetailedSummaryReport'))) {
                if (isset($filters['contractor_id']) && $filters['contractor_id']) {
                    $filters['user_id'] = $filters['contractor_id'];
                }
            } else {
                $filters['user_id'] = $this->LoggedUser['user_id'];
            }

            $filters['role_id'] = $modelAuthRole->getRoleIdByName('contractor');

            $data = $modelUser->getAll($filters);
            foreach ($data as &$row) {

                // Number of booking(jobs)
                $total_booking_filters = array();
                $total_booking_filters['contractor_id'] = $row['user_id'];
                $total_booking_filters['booking_start_between'] = $filters['booking_start_between'];
                $total_booking_filters['booking_end_between'] = $filters['booking_end_between'] ? $filters['booking_end_between'] . ' 23:59:59' : '';
                $row['total_booking'] = $modelBooking->getBookingCount($total_booking_filters);

                // Contractor Commission and  Regisered Gst to display its in rebort
                $contractorInfo = $modelContractorInfo->getByContractorId($row['user_id']);
                $row['commission'] = $contractorInfo['commission'];
                $row['registerd_gst'] = $contractorInfo['gst'];

                // Number  and Total Amount for each Booking Status
                $bookingStatusesArray = array();
                $bookingStatusesArray = $bookingStatuses;
                foreach ($bookingStatusesArray as &$bookingStatus) {
                    $total_booking_statuses_filters = array();
                    $total_booking_statuses_filters['contractor_id'] = $row['user_id'];
                    $total_booking_statuses_filters['status'] = $bookingStatus['booking_status_id'];
                    $total_booking_statuses_filters['booking_start_between'] = $filters['booking_start_between'];
                    $total_booking_statuses_filters['booking_end_between'] = $filters['booking_end_between'] ? $filters['booking_end_between'] . ' 23:59:59' : '';
                    if ($bookingStatus['name'] == 'IN PROGRESS (invoice)') {
                        $total_booking_statuses_filters['convert_status'] = 'invoice';
                    } elseif ($bookingStatus['name'] == 'IN PROGRESS') {
                        $total_booking_statuses_filters['convert_status'] = 'booking';
                    }
                    $bookingStatus['total_number'] = $modelBooking->getBookingCount($total_booking_statuses_filters);
                    $bookingStatus['total_amount'] = $modelContractorServiceBooking->getTotalAmountServicesByContractorIdAndStatusId($row['user_id'], $bookingStatus['booking_status_id'], $total_booking_statuses_filters);
                }
                $row['booking_statuses'] = $bookingStatusesArray;


                // Total office Payment and The total for each Payment Type
                $totalOfficePayment = 0;
                $paymentTypesWithoutCash = array();
                $paymentTypesWithoutCash = $paymentTypes;
                foreach ($paymentTypesWithoutCash as &$paymentTypeWithoutCash) {
                    $total_payment_types_filters = array();
                    $total_payment_types_filters['contractor_id'] = $row['user_id'];
                    $total_payment_types_filters['payment_type'] = $paymentTypeWithoutCash['id'];
                    $total_payment_types_filters['booking_start_between'] = $filters['booking_start_between'];
                    $total_payment_types_filters['booking_end_between'] = $filters['booking_end_between'] ? $filters['booking_end_between'] . ' 23:59:59' : '';
                    $total_payment_types_filters['payment_contractor_id'] = $row['user_id'];
                    $amount = $modelPayment->getTotalPayment($total_payment_types_filters);
                    $amount = $amount ? $amount : 0;

                    $paymentTypeWithoutCash['amount'] = $amount;
                    $totalOfficePayment = $totalOfficePayment + $amount;
                }

                $row['payment_types'] = $paymentTypesWithoutCash;

                // total payment cash
                $total_payment_cash_filters = array();
                $total_payment_cash_filters['contractor_id'] = $row['user_id'];
                $total_payment_cash_filters['payment_type'] = $modelPaymentType->getPaymentIdBySlug('cash');
                $total_payment_cash_filters['booking_start_between'] = $filters['booking_start_between'];
                $total_payment_cash_filters['booking_end_between'] = $filters['booking_end_between'] ? $filters['booking_end_between'] . ' 23:59:59' : '';
                $total_payment_cash_filters['payment_contractor_id'] = $row['user_id'];
                $contractorCashPayment = $modelPayment->getTotalPayment($total_payment_cash_filters);
                $row['cashPayment'] = $contractorCashPayment;

                /* total amount Service for each Invoice
                 * include amount service after discount refunds and call out fee
                 */
                $total_amount_services_filters = array();
                $total_amount_services_filters['booking_start_between'] = $filters['booking_start_between'];
                $total_amount_services_filters['booking_end_between'] = $filters['booking_end_between'] ? $filters['booking_end_between'] . ' 23:59:59' : '';
                $totalAmountServices = $modelContractorServiceBooking->getTotalAmountServicesInvoicedByContractorId($row['user_id'], $total_amount_services_filters);
                $row['total_amount_services'] = $totalAmountServices;


                $booking_date_filters = array();
                $booking_date_filters['booking_start_between'] = $filters['booking_start_between'];
                $booking_date_filters['booking_end_between'] = $filters['booking_end_between'] ? $filters['booking_end_between'] . ' 23:59:59' : '';

                //  total Refund  
                $row['total_refund'] = $modelContractorServiceBooking->getTotalServiceRefund($row['user_id'], $booking_date_filters);


                //  total unpaid 
                $row['unpaid'] = $totalAmountServices - ($totalOfficePayment + $contractorCashPayment - $row['total_refund']);

                // total office 
                $row['totalOffice'] = $totalAmountServices - $contractorCashPayment;


                /* operator Share
                 * calculate the total for each opreator share from its amount service after discount and refunds
                 * depends on the Comission and Registed Gst
                 */
                $operator_share = $modelContractorServiceBooking->getTotalContractorShare($row['user_id'], $booking_date_filters);
                $row['operator_share'] = $operator_share;

                //company share
                $row['company_share'] = $totalAmountServices - $operator_share;

                //payment to sub contractor
                $row['payment_to_sub_contractor'] = $operator_share - $contractorCashPayment;
            }
        }

        $this->view->data = $data;
        $this->view->filters = $filters;
    }

    public function contractorsBookingsSummaryAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('reportContractorsBookingsSummary'));

        $modelBooking = new Model_Booking();
        $modelUser = new Model_User();
        $modelAuthRole = new Model_AuthRole();
        $modelContractorInfo = new Model_ContractorInfo();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelBookingContractorPayment = new Model_BookingContractorPayment();
		$modelPaymentToContractors = new Model_PaymentToContractors();
		 
        $form = new Reports_Form_ContractorFilters(array('action_url' => $this->router->assemble(array(), 'reportContractorsBookingsSummary'), 'type' => 'contractors_bookings_summary'));
        $this->view->form = $form;


        /*
         * Contractors Details (First Table)
         */

       

        $contractors = array();
        $contractorServiceBookings = array();
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        if ($filters) {
		/////////By Islam
		
		if ($this->LoggedUser['role_id'] == $modelAuthRole->getRoleIdByName('contractor')) {
			//here for contractor who given a special permission to see summary report for him only and he can not select other contractors from drop list
			$contractorId=$this->LoggedUser['user_id'];
		}
		else{
			// this for super admin who can select contrator name from the dropdown list
			$contractorId = isset($filters['contractor_id']) && $filters['contractor_id'] ? $filters['contractor_id'] : 0;
		}
		//////////////            
		
		
            // user
            $user = $modelUser->getById($contractorId);
            $this->view->user = $user;



            if (CheckAuth::checkCredential(array('canSeeAllContractorsDetailedSummaryReport'))) {
                if (isset($filters['contractor_id']) && $filters['contractor_id']) {
					$filters['user_id'] = $filters['contractor_id'];
                }
            } else {
				    $filters['user_id'] = $this->LoggedUser['user_id'];
					
            }

            $filters['role_id'] = $modelAuthRole->getRoleIdByName('contractor');

            $contractors = $modelUser->getAll($filters);
            foreach ($contractors as &$contractor) {

                // Number of booking(jobs)
                $total_booking_filters = array();
                $total_booking_filters['contractor_id'] = $contractor['user_id']? $filters['user_id'] : '';
                $total_booking_filters['booking_start_between'] = (isset($filters['booking_start_between']) && $filters['booking_start_between'])? $filters['booking_start_between'] : '';
                $total_booking_filters['booking_end_between'] = (isset($filters['booking_end_between']) && $filters['booking_end_between']) ? $filters['booking_end_between']. ' 23:59:59' : '';
                $contractor['total_booking'] = $modelBooking->getBookingCount($total_booking_filters);

                // Contractor Commission and  Regisered Gst to display its in rebort
                $contractorInfo = $modelContractorInfo->getByContractorId($contractor['user_id']);
                $contractor['commission'] = $contractorInfo['commission'];
                $contractor['registerd_gst'] = $contractorInfo['gst'];

            }

            $this->view->contractors = $contractors;


            /*
             * bookings Details summary ( Second Table)
             */

            //
            //get all the booking
            //
            $all_booking_filters = array();
            $all_booking_filters['contractor_id'] = $contractorId;
			////////by Islam
			
			$all_booking_filters['paid_status']= (isset($filters['paid_status']) && $filters['paid_status'])? $filters['paid_status'] : '';
			$all_booking_filters['Inv']= (isset($filters['Inv']) && $filters['Inv'])? $filters['Inv'] : '';
			$all_booking_filters['customer_inv']= (isset($filters['customer_inv']) && $filters['customer_inv'])? $filters['customer_inv'] : '';
			$fillterd_booking_with_paid_status=array();
			$this->view->paid_status = $all_booking_filters['paid_status'];
			$this->view->Inv = $all_booking_filters['Inv'];
			
			$all_booking_filters['status_id']= (isset($filters['booking_status']) && $filters['booking_status'])? $filters['booking_status'] : '';
			$all_booking_filters['customer_payment_status']= (isset($filters['customer_payment_status']) && $filters['customer_payment_status'])? $filters['customer_payment_status'] : '';
			
			/////////////end/
            $all_booking_filters['booking_start_between'] = (isset($filters['booking_start_between']) && $filters['booking_start_between'])? $filters['booking_start_between'] : '';
			$this->view->startt = $all_booking_filters['booking_start_between'];
            $all_booking_filters['booking_end_between'] = (isset($filters['booking_end_between']) && $filters['booking_end_between'])? $filters['booking_end_between']. ' 23:59:59' : '';
            $all_booking_filters['order_by_booking_start'] = true;
            /////test if the filters contains only contractor_id and contractor_invoice_num(==INV) 
			////we will get booking ids of that invoice number from contractor_booking_payment table then set these booking ids at the end of
			//// or long query which is in test function ////all this work just to speed up the search and minimize seach time
			if(isset($filters['contractor_id']) && isset($filters['Inv']) && !isset($filters['booking_start_between']) && !isset($filters['booking_end_between'])){
				//$payment_to_contractor = $modelPaymentToContractors->getIdByInvoiceNumAndContractorId($filters['Inv'],$filters['contractor_id']);
				//$booking_ids_of_invoice_number = $modelBookingContractorPayment->getBookingIdsByPaymentToContractorId($payment_to_contractor['payment_to_contractor_id']);
				
				//$booking_ids_of_invoice_number = $modelBookingContractorPayment->getBookingIdsByInvNumber($filters['Inv']);
				////add these booking_ids in all_booking_filters[] to set them in where clouse at the end of our query
				//$all_booking_filters['booking_ids_of_invoice_number'] = $booking_ids_of_invoice_number;
			}
			$contractorServiceBookings = $modelContractorServiceBooking->test($all_booking_filters);
            
        }

				
        $this->view->bookings = $contractorServiceBookings;
        $this->view->filters = $filters;
		
		
    }

	
	
    public function contractorsSummaryAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('reportContractorsSummary'));

        // load model
        $modelUser = new Model_User();
        $modelAuthRole = new Model_AuthRole();
        $modelContractorInfo = new Model_ContractorInfo();
        $modelPayment = new Model_Payment;
        $modelPaymentType = new Model_PaymentType();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();

        $form = new Reports_Form_ContractorFilters(array('action_url' => $this->router->assemble(array(), 'reportContractorsSummary'), 'type' => 'contractors_summary'));
        $this->view->form = $form;

        $paymentTypes = $modelPaymentType->getAll(array('without_cash' => true));
        $this->view->paymentTypes = $paymentTypes;

        $data = array();
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        if ($filters) {
            $contractorId = isset($filters['contractor_id']) && $filters['contractor_id'] ? $filters['contractor_id'] : 0;
            $user = $modelUser->getById($contractorId);
            $this->view->user = $user;

            if (CheckAuth::checkCredential(array('canSeeAllContractorsSummaryReport'))) {
                if (isset($filters['contractor_id']) && $filters['contractor_id']) {
                    $filters['user_id'] = $filters['contractor_id'];
                }
            } else {
                $filters['user_id'] = $this->LoggedUser['user_id'];
            }

            $filters['role_id'] = $modelAuthRole->getRoleIdByName('contractor');

            $data = $modelUser->getAll($filters);
            foreach ($data as &$row) {

                $contractorInfo = $modelContractorInfo->getByContractorId($row['user_id']);

                $row['commission'] = $contractorInfo['commission'];
                $row['registerd_gst'] = $contractorInfo['gst'];


                // Total office and The total for each Payment Type
                $totalOfficePayment = 0;
                $paymentTypesWithoutCash = array();
                $paymentTypesWithoutCash = $paymentTypes;
                foreach ($paymentTypesWithoutCash as &$paymentTypeWithoutCash) {
                    $total_payment_types_filters = array();
                    $total_payment_types_filters['contractor_id'] = $row['user_id'];
                    $total_payment_types_filters['payment_type'] = $paymentTypeWithoutCash['id'];
                    $total_payment_types_filters['booking_start_between'] = $filters['booking_start_between'];
                    $total_payment_types_filters['booking_end_between'] = $filters['booking_end_between'] ? $filters['booking_end_between'] . ' 23:59:59' : '';
                    $total_payment_types_filters['payment_contractor_id'] = $row['user_id'];
                    $amount = $modelPayment->getTotalPayment($total_payment_types_filters);
                    $amount = $amount ? $amount : 0;

                    $paymentTypeWithoutCash['amount'] = $amount;
                    $totalOfficePayment = $totalOfficePayment + $amount;
                }

                $row['payment_types'] = $paymentTypesWithoutCash;


                // total payment cash
                $total_payment_cash_filters = array();
                $total_payment_cash_filters['contractor_id'] = $row['user_id'];
                $total_payment_cash_filters['payment_type'] = $modelPaymentType->getPaymentIdBySlug('cash');
                $total_payment_cash_filters['booking_start_between'] = $filters['booking_start_between'];
                $total_payment_cash_filters['booking_end_between'] = $filters['booking_end_between'] ? $filters['booking_end_between'] . ' 23:59:59' : '';
                $total_payment_cash_filters['payment_contractor_id'] = $row['user_id'];
                $contractorCashPayment = $modelPayment->getTotalPayment($total_payment_cash_filters);
                $row['cashPayment'] = $contractorCashPayment;


                /* total amount Service for each Invoice
                 * include amount service after discount refunds and call out fee
                 */
                $total_amount_services_filters = array();
                $total_amount_services_filters['booking_start_between'] = $filters['booking_start_between'];
                $total_amount_services_filters['booking_end_between'] = $filters['booking_end_between'] ? $filters['booking_end_between'] . ' 23:59:59' : '';
                $totalAmountServices = $modelContractorServiceBooking->getTotalAmountServicesInvoicedByContractorId($row['user_id'], $total_amount_services_filters);
                $row['total_amount_services'] = $totalAmountServices;



                $booking_date_filters = array();
                $booking_date_filters['booking_start_between'] = $filters['booking_start_between'];
                $booking_date_filters['booking_end_between'] = $filters['booking_end_between'] ? $filters['booking_end_between'] . ' 23:59:59' : '';

                // total refund 
                $row['total_refund'] = $modelContractorServiceBooking->getTotalServiceRefund($row['user_id'], $booking_date_filters);

                //  total unpaid 
                $row['unpaid'] = $totalAmountServices - ($totalOfficePayment + $contractorCashPayment - $row['total_refund']);

                // total office 
                $row['totalOffice'] = $totalAmountServices - $contractorCashPayment;

                /* operator Share
                 * calculate the total for each opreator share from its amount service after discount and refunds
                 * depends on the Comission and Registed Gst
                 */
                $booking_date_filters = array();
                $booking_date_filters['booking_start_between'] = $filters['booking_start_between'];
                $booking_date_filters['booking_end_between'] = $filters['booking_end_between'] ? $filters['booking_end_between'] . ' 23:59:59' : '';
                $operator_share = $modelContractorServiceBooking->getTotalContractorShare($row['user_id'], $booking_date_filters);
                $row['operator_share'] = $operator_share;

                // company share
                $row['company_share'] = $totalAmountServices - $operator_share;

                $row['payment_to_sub_contractor'] = $operator_share - $contractorCashPayment;
            }
        }

        $this->view->data = $data;
        $this->view->filters = $filters;
    }

    public function contractorsBookingPaymentAction() {


        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('contractorsBookingPayment'));

        //
        //declaring the moduels
        //
        $modelBooking = new Model_Booking();
        $modelUser = new Model_User();
        $modelPaymentType = new Model_PaymentType();
        $modelPayment = new Model_Payment();
        $modelBookingStatus = new Model_BookingStatus();
        //
        //get the requested param
        //
        $filters = $this->request->getParam('fltr', array());


        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        $form = new Reports_Form_ContractorFilters(array('action_url' => $this->router->assemble(array(), 'contractorsBookingPayment'), 'type' => 'contractors_booking_payment'));
        $this->view->form = $form;

        $paymentTypes = $modelPaymentType->getAll(array('without_cash' => true));
        $this->view->paymentTypes = $paymentTypes;

        $data = array();
        if ($filters) {

            //
            //the requested contractor info
            //
            $user = $modelUser->getById($filters['contractor_id']);
            $this->view->user = $user;

            //
            //get all the booking
            //
            $completedStatus = $modelBookingStatus->getByStatusName('COMPLETED');
            $faildStatus = $modelBookingStatus->getByStatusName('FAILED');

            $all_booking_filters = array();
            $all_booking_filters['contractor_id'] = $filters['contractor_id'];
            $all_booking_filters['booking_start_between'] = $filters['booking_start_between'];
            $all_booking_filters['booking_end_between'] = $filters['booking_end_between'] ? $filters['booking_end_between'] . ' 23:59:59' : '';
            // $all_booking_filters['more_one_status'] = array('completedStatus' => $completedStatus['booking_status_id'], 'faildStatus' => $faildStatus['booking_status_id']);
            $all_booking_filters['convert_status'] = 'invoice';
            $data = $modelBooking->getAll($all_booking_filters, 'booking_start ASC');

            $modelBooking->fills($data, array('contractors', 'address', 'created_by', 'status', 'services', 'invoice', 'status_discussion', 'approved_payment', 'unapproved_payment', 'approved_refund', 'unapproved_refund', 'total_without_cash', 'cash_payment'));
        }

        $this->view->data = $data;
        $this->view->filters = $filters;
    }

    public function contractorsPaymentDetailedSummaryAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('reportContractorsPaymentDetailedSummary'));

        $modelBooking = new Model_Booking();
        $modelUser = new Model_User();
        $modelBookingStatus = new Model_BookingStatus();
        $modelAuthRole = new Model_AuthRole();
        $modelContractorInfo = new Model_ContractorInfo();
        $modelPayment = new Model_Payment;
        $modelPaymentType = new Model_PaymentType();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();

        $form = new Reports_Form_ContractorFilters(array('action_url' => $this->router->assemble(array(), 'reportContractorsPaymentDetailedSummary'), 'type' => 'contractors_payment_detailed_summary'));
        $this->view->form = $form;

        $inProcess = $modelBookingStatus->getByStatusName('IN PROGRESS');
        $inProcess['name'] .= ' (invoice)';

        $completedStatus = $modelBookingStatus->getByStatusName('COMPLETED');
        $completedStatus['name'] .= ' (invoice)';

        $faildStatus = $modelBookingStatus->getByStatusName('FAILED');
        $faildStatus['name'] .= ' (invoice)';

        // only complete IN PROGRESS and faild status
        $bookingStatuses = array('inProcess' => $inProcess, 'faildStatus' => $faildStatus, 'completedStatus' => $completedStatus);
        $this->view->bookingStatuses = $bookingStatuses;


        $paymentTypes = $modelPaymentType->getAll(array('without_cash' => true));
        $this->view->paymentTypes = $paymentTypes;


        $data = array();
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        if ($filters) {
            $contractorId = isset($filters['contractor_id']) && $filters['contractor_id'] ? $filters['contractor_id'] : 0;
            $user = $modelUser->getById($contractorId);
            $this->view->user = $user;

            if (CheckAuth::checkCredential(array('canSeeAllContractorsDetailedSummaryReport'))) {
                if (isset($filters['contractor_id']) && $filters['contractor_id']) {
                    $filters['user_id'] = $filters['contractor_id'];
                }
            } else {
                $filters['user_id'] = $this->LoggedUser['user_id'];
            }

            $filters['role_id'] = $modelAuthRole->getRoleIdByName('contractor');

            $data = $modelUser->getAll($filters);
            foreach ($data as &$row) {

                $total_booking_filters = array();
                $total_booking_filters['contractor_id'] = $row['user_id'];
                $total_booking_filters['booking_start_between'] = $filters['booking_start_between'];
                $total_booking_filters['booking_end_between'] = $filters['booking_end_between'] ? $filters['booking_end_between'] . ' 23:59:59' : '';
                //$total_booking_filters['more_one_status'] = array('inProcess' => $inProcess['booking_status_id'],'faildStatus' => $faildStatus['booking_status_id'], 'completedStatus' => $completedStatus['booking_status_id']);
                $total_booking_filters['convert_status'] = 'invoice';
                $row['total_booking'] = $modelBooking->getBookingCount($total_booking_filters);

                $contractorInfo = $modelContractorInfo->getByContractorId($row['user_id']);
                $row['commission'] = $contractorInfo['commission'];
                $row['registerd_gst'] = $contractorInfo['gst'];

                $bookingStatusesArray = array();
                $bookingStatusesArray = $bookingStatuses;
                foreach ($bookingStatusesArray as &$bookingStatus) {
                    $total_booking_statuses_filters = array();
                    $total_booking_statuses_filters['contractor_id'] = $row['user_id'];
                    $total_booking_statuses_filters['status'] = $bookingStatus['booking_status_id'];
                    $total_booking_statuses_filters['booking_start_between'] = $filters['booking_start_between'];
                    $total_booking_statuses_filters['booking_end_between'] = $filters['booking_end_between'] ? $filters['booking_end_between'] . ' 23:59:59' : '';
                    $total_booking_statuses_filters['convert_status'] = 'invoice';
                    $bookingStatus['total_number'] = $modelBooking->getBookingCount($total_booking_statuses_filters);
                    $bookingStatus['total_amount'] = $modelContractorServiceBooking->getTotalAmountServicesByContractorIdAndStatusId($row['user_id'], $bookingStatus['booking_status_id'], $total_booking_statuses_filters);
                }
                $row['booking_statuses'] = $bookingStatusesArray;


                // Total office and The total for each Payment Type
                $totalOfficePayment = 0;
                $paymentTypesWithoutCash = array();
                $paymentTypesWithoutCash = $paymentTypes;
                foreach ($paymentTypesWithoutCash as &$paymentTypeWithoutCash) {
                    $total_payment_types_filters = array();
                    $total_payment_types_filters['contractor_id'] = $row['user_id'];
                    $total_payment_types_filters['payment_type'] = $paymentTypeWithoutCash['id'];
                    $total_payment_types_filters['booking_start_between'] = $filters['booking_start_between'];
                    $total_payment_types_filters['booking_end_between'] = $filters['booking_end_between'] ? $filters['booking_end_between'] . ' 23:59:59' : '';
                    $total_payment_types_filters['payment_contractor_id'] = $row['user_id'];
                    $amount = $modelPayment->getTotalPayment($total_payment_types_filters);
                    $amount = $amount ? $amount : 0;

                    $paymentTypeWithoutCash['amount'] = $amount;
                    $totalOfficePayment = $totalOfficePayment + $amount;
                }

                $row['payment_types'] = $paymentTypesWithoutCash;


                // total payment cash
                $total_payment_cash_filters = array();
                $total_payment_cash_filters['contractor_id'] = $row['user_id'];
                $total_payment_cash_filters['payment_type'] = $modelPaymentType->getPaymentIdBySlug('cash');
                $total_payment_cash_filters['booking_start_between'] = $filters['booking_start_between'];
                $total_payment_cash_filters['booking_end_between'] = $filters['booking_end_between'] ? $filters['booking_end_between'] . ' 23:59:59' : '';
                $total_payment_cash_filters['payment_contractor_id'] = $row['user_id'];
                $contractorCashPayment = $modelPayment->getTotalPayment($total_payment_cash_filters);
                $row['cashPayment'] = $contractorCashPayment;

                // total amount Service (invoice)
                $total_amount_services_filters = array();
                $total_amount_services_filters['booking_start_between'] = $filters['booking_start_between'];
                $total_amount_services_filters['booking_end_between'] = $filters['booking_end_between'] ? $filters['booking_end_between'] . ' 23:59:59' : '';
                $totalAmountServices = $modelContractorServiceBooking->getTotalAmountServicesInvoicedByContractorId($row['user_id'], $total_amount_services_filters);
                $row['total_amount_services'] = $totalAmountServices;

                $booking_date_filters = array();
                $booking_date_filters['booking_start_between'] = $filters['booking_start_between'];
                $booking_date_filters['booking_end_between'] = $filters['booking_end_between'] ? $filters['booking_end_between'] . ' 23:59:59' : '';

                // refund 
                $row['total_refund'] = $modelContractorServiceBooking->getTotalServiceRefund($row['user_id'], $booking_date_filters);


                // unpaid 
                $row['unpaid'] = $row['total_amount_services'] - ($totalOfficePayment + $contractorCashPayment - $row['total_refund']);

                // total office 
                $row['totalOffice'] = $totalAmountServices - $contractorCashPayment;

                // operator Share
                $booking_date_filters = array();
                $booking_date_filters['booking_start_between'] = $filters['booking_start_between'];
                $booking_date_filters['booking_end_between'] = $filters['booking_end_between'] ? $filters['booking_end_between'] . ' 23:59:59' : '';
                $operator_share = $modelContractorServiceBooking->getTotalContractorShare($row['user_id'], $booking_date_filters);
                $row['operator_share'] = $operator_share;

                // company share
                $row['company_share'] = $totalAmountServices - $operator_share;

                $row['payment_to_sub_contractor'] = $operator_share - $contractorCashPayment;
            }
        }

        $this->view->data = $data;
        $this->view->filters = $filters;
    }

    public function addPaymentToContractorAction() {

        //
        //get request param
        //
        
        $bookingsContractorPayment = $this->request->getParam('bookings_contractor_payment', array());
        $startTimeFltr = $this->request->getParam('start_time_value', '');
        $endTimeFltr = $this->request->getParam('end_time_value', '');

        // load model
        $modelBookingContractorPayment = new Model_BookingContractorPayment();


        $contractors = array();
        $bookingPayment = array();
        if (!empty($bookingsContractorPayment)) {

            foreach ($bookingsContractorPayment as $bookingContractorPayment) {
                $contractorPayment = explode('_', $bookingContractorPayment);
                $bookingId = (int) (isset($contractorPayment[0]) ? $contractorPayment[0] : 0);
                $contractorId = (int) (isset($contractorPayment[1]) ? $contractorPayment[1] : 0);

                // paid to contractor by booking (table 2)
                $paymentToContractor = floatval($this->request->getParam('payment_' . $bookingId . '_' . $contractorId));

                $data = array(
                    'booking_id' => $bookingId,
                    'contractor_id' => $contractorId,
                    'payment_to_contractor' => $paymentToContractor,
					'date_created'=> date('Y-m-d H:i:s'),
					'created_by' => $userId
                );

                $contractorPayment = $modelBookingContractorPayment->getBybookingIdAndContractorId($bookingId, $contractorId);
                if (empty($contractorPayment)) {
                    $contractorPaymentId = $modelBookingContractorPayment->insert($data);
                } else {
                    $contractorPaymentId = $contractorPayment['id'];
                }

                $contractors[$contractorId] = array('contractor_id' => $contractorId);

                $bookingPayment[] = array(
                    'payment_id' => $contractorPaymentId,
                    'contractor_id' => $contractorId,
                    'booking_id' => $bookingId
                );
            }

            // calculate the total payment after add payments
            $booking_date_filters = array();
            $booking_date_filters['booking_start_between'] = $startTimeFltr;
            $booking_date_filters['booking_end_between'] = $endTimeFltr ? $endTimeFltr . ' 23:59:59' : '';

            foreach ($contractors as &$contractor) {
                $contractor['total_payment'] = $modelBookingContractorPayment->getTotalPaymentByContractorId($contractor['contractor_id'], $booking_date_filters);
            }
        }

        echo json_encode(array('contractors' => $contractors, 'booking_payment' => $bookingPayment));
        exit;
    }

	
	public function editPaidAmountAction(){
		$booking_id = $this->request->getParam('id');
	 $contractor_id = $this->request->getParam('contractor_id');
	 $old_paid_amount = $this->request->getParam('old_paid_amount');
	 $new_paid_amount = $this->request->getParam('amount_paid_input_txt_'.$booking_id.'_'.$contractor_id);
	 $modelBookingContractorPayment = new Model_BookingContractorPayment();
	 $modelPaymentToContractors = new Model_PaymentToContractors();
	 
	 $options = array();
	 $options['booking_id']= $booking_id;
	 $options['contractor_id']= $contractor_id;
	 $options['old_paid_amount']= $old_paid_amount;
	 $options['new_paid_amount']= $new_paid_amount;
	
	 

	 $form = new Reports_Form_EditAmountPaid($options);
	 $this->view->form= $form;
	 if ($this->request->isPost()) { // check if POST request method
	 $new_paid_amount = $this->request->getParam('paid_amount');
	
		$contractorPayment = $modelBookingContractorPayment->getBybookingIdAndContractorId($booking_id, $contractor_id);
                        
						if (!empty($contractorPayment)) {
                            $is_updated = $modelBookingContractorPayment->updateById($contractorPayment['id'], array('payment_to_contractor' => $new_paid_amount));
							//echo $isUpdated;
							$payment_to_contractor_id = $contractorPayment['payment_to_contractor_id'];
							/*if(!empty($payment_to_contractor_id)){
							/////get old amount_paid for this payment
							$old_payment_to_contractor=$modelPaymentToContractors->getById($payment_to_contractor_id);
							$total_old_amount_paid=$old_payment_to_contractor['amount_paid'];
							$total_new_amount_paid= ($total_old_amount_paid - $old_paid_amount)+ $new_paid_amount;
							$modelPaymentToContractors->updateById($payment_to_contractor_id, array('amount_paid' => $total_new_amount_paid));
                        
							}*/
                        }
	
		echo json_encode(array('is_updated'=>$is_updated,'booking_id'=>$booking_id, 'contractor_id'=>$contractor_id,'old_paid_amount'=>$old_paid_amount, 'new_paid_amount'=>$new_paid_amount));
	
        exit;
		}
		$this->view->booking_id = $booking_id;
        $this->view->contractor_id = $contractor_id;
        $this->view->old_paid_amount = $old_paid_amount;
        $this->view->new_paid_amount = $new_paid_amount;
        
	 
	 echo $this->view->render('contractor-report/edit-amount-paid.phtml');
        exit;
	}
	
	public function setContractorShareFromReportAction(){
	
	 $booking_id = $this->request->getParam('id');
	 $contractor_id = $this->request->getParam('contractor_id');
	 $old_contractor_share = $this->request->getParam('old_contractor_share');
	 $new_contractor_share = $this->request->getParam('contractor_share_input_txt_'.$booking_id.'_'.$contractor_id);
     
	  $modelContractorShareBooking = new Model_ContractorShareBooking();
	 $data = array(
                    'booking_id' => $booking_id,
                    'contractor_id' => $contractor_id,
                    'contractor_share' => $new_contractor_share
                );

                $contractorShareBooking = $modelContractorShareBooking->getByBookingIdAndContractorId($booking_id, $contractor_id);

                if ($contractorShareBooking) {
					
                    $modelContractorShareBooking->updateById($contractorShareBooking['id'], $data);
                } else {
                    $modelContractorShareBooking->insert($data);
                }
	 
	echo json_encode(array('booking_id'=>$booking_id, 'contractor_id'=>$contractor_id,'old_contractor_share'=>$old_contractor_share, 'new_contractor_share'=>$new_contractor_share));
	exit;
	}
	
	
    /////////by islam edit/delete payment to contractor 
	public function editPaymentToContractorAction(){
	      // this function get the old data of the selected record to view it in the update form (in popup window)
        //get request param
        //
        
        $paymentToContractors = $this->request->getParam('payment_to_contractors', array());
		$filters = $this->request->getParam('fltr', array());
		
        // load model
        $modelPaymentToContractors = new Model_PaymentToContractors();
		//$modelPaymentAttachment = new Model_PaymentAttachment();

		$data= array();
        $contractors = array();
        $bookingPayment = array();
        if (!empty($paymentToContractors)) {

            foreach ($paymentToContractors as $paymentToContractor) {
                $paymentToContractor = explode('_', $paymentToContractor);
                $paymentToContractorId = (int) (isset($paymentToContractor[0]) ? $paymentToContractor[0] : 0);
                //$attachmentId = (int) (isset($paymentToContractor[1]) ? $paymentToContractor[1] : 0);

                $payment=$modelPaymentToContractors->getById($paymentToContractorId);
				//$attachment=$modelPaymentAttachment->getById($attachmentId);
                $data = array(
                    'contractor_invoice_num' => $payment['contractor_invoice_num'],
                    'date_of_payment' => $payment['date_of_payment'],
                    'amount_paid' => $payment['amount_paid'],
					'cash_paid'=> $payment['cash_paid'],
					'invoice_total_amount'=> $payment['invoice_total_amount'],
					'payment_reference' => $payment['payment_reference'],
                    'description' => $payment['description'],
					'payment_to_contractor_id' => $paymentToContractorId
                );

               
            }

           $this->view->fltr = $filters;
		    
			 echo json_encode(array('payment' => $data));
			 exit;
        }
		$this->view->fltr = $filters;
        //echo json_encode(array('payment' => $data));
		echo $this->view->render('contractor-report/edit-contractor-invoice-number.phtml');
	
        exit;
	}
	
	
	
	public function editContractorInvoiceNumberAction(){
		$filters = $this->request->getParam('fltr', array());
		$this->view->fltr = $filters;
		
		
			if ($this->getRequest()->isPost()) {
					$paymentToContractorId=$this->request->getParam('payment_to_contractor_id');
					//$attachmentId=$this->request->getParam('attachment_id');
					$invoiceNumber=$this->request->getParam('invoice_number');
					
					$datePaid =$this->request->getParam('date_paid', '');
					$datePaid = date('Y-m-d H:i:s',strtotime($datePaid));
					
					$amountPaid=str_replace(',', '', $this->request->getParam('amount_paid'));
					$cashPaid = str_replace(',', '', $this->request->getParam('cash_paid'));
					$invoiceTotalAmount = str_replace(',', '', $this->request->getParam('invoice_total_amount'));
					
					$reference=$this->request->getParam('reference');
					$description=$this->request->getParam('description');
					//$filePath=$this->request->getParam('file_path');
					
					////////load models
					$modelPaymentAttachment=new Model_PaymentAttachment();
					$modelPaymentToContractors = new Model_PaymentToContractors();
					
					///////update payment to contractor payment record
					$data=array(
									'contractor_invoice_num' => $invoiceNumber,
									'date_of_payment' => $datePaid,
									'amount_paid' => $amountPaid,
									'cash_paid'=> $cashPaid,
									'invoice_total_amount'=> $invoiceTotalAmount,
									'description' => $description,
									'payment_reference' => $reference,
									'date_created'=> date('Y-m-d H:i:s')
									
							);
					$isUpdatedPayment=$modelPaymentToContractors->updateById($paymentToContractorId, $data);
					
					//////update attachment
					$loggedUser = CheckAuth::getLoggedUser();
					$userId = $loggedUser['user_id'];
					
					$apt    = new Zend_File_Transfer_Adapter_Http();
$files  = $apt->getFileInfo();

foreach($files as $file => $fileInfo) {
    if ($apt->isUploaded($file)) {
        if ($apt->isValid($file)) {
            if ($apt->receive($file)) {
                $info = $apt->getFileInfo($file);
				$name= $info[$file]['name'];
				$size = $info[$file]['size'];
                $tmp  = $info[$file]['tmp_name'];
                
                $nameArr = explode(".", $name);
				$ext=end($nameArr);
				
                $dir = get_config('payment_attachment') . '/';
                $subdir = date('Y/m/d/');

                //check if file exists or not
                $fullDir = $dir . $subdir;

                if (!is_dir($fullDir)) {
                    mkdir($fullDir, 0777, true);
                }
			
				//$fileName = $attachmentId.'.'. $ext;
////////////save file attachment Details in the dataBase
                $data = array(
                    'date_created' => date('Y-m-d H:i:s'),
                    'size' => $size,
                    'created_by' => $userId,
                    
                );

                $newAttachmentId = $modelPaymentAttachment->insert($data);
				$fileName = $newAttachmentId.'.'. $ext;

                $data = array(
					'payment_to_contractor_id' => $paymentToContractorId,
                    'path' => $subdir . $fileName,
                    'file_name' => $fileName
                );

                $modelPaymentAttachment->updateById($newAttachmentId, $data);
				//////////update old attachment to be isdeleted=1
				//$modelPaymentAttachment->updateById($attachmentId, array('is_deleted'=>1));
				
		 //save image to database and filesystem here
                //$file_saved = copy($source, $fullDir . $fileName);
				$file_saved = copy($tmp, $fullDir . $fileName);

                if ($file_saved) {

                    /*if (file_exists($temp)) {
                        unlink($temp);
                    }*/
                    // $this->_redirect($this->router->assemble(array('id' => $inquiryId), 'inquiryAttachment'));
                    //echo 1;
                    //exit;
                }
				 
				 //////
            }
			
        }
    }
}
		if ($isUpdatedPayment) {
			$this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => " payment to contractor is updated successfully."));
          $this->_redirect($this->router->assemble(array(), 'reportPaymentToContractors'));
		  
	
        } else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Not updated, please try again...."));
           
        }
					
					
					exit;
			}
			
	
	}
	
	
	
	public function deletePaymentToContractorsAction(){
		$payment_to_contractors_id=$this->request->getParam('payment_to_contractors_id');
		
		////////load models
		$modelPaymentAttachment=new Model_PaymentAttachment();
		$modelPaymentToContractors = new Model_PaymentToContractors();
		
		/*if(!empty($attachment_id) && $attachment_id !="0" ){
				$isDeleted= $modelPaymentAttachment->updateById($attachment_id, array('is_deleted'=>1));
				echo '$isDeleted   '.$isDeleted;
		}*/
		$isDeletedPayment=$modelPaymentToContractors->deleteById($payment_to_contractors_id);
		$areAttachmentsDeleted = $modelPaymentAttachment->deleteByPaymentToContractorId($payment_to_contractors_id);
		
		echo json_encode(array('isDeletedPayment' => $isDeletedPayment,'payment_to_contractors_id' => $payment_to_contractors_id,'areAttachmentsDeleted' => $areAttachmentsDeleted ));
		exit;
	}
	
	
	
	///////end 
	
	public function unPaidContractorAction() {

        //
        //get request param
        //
        $id = $this->request->getParam('id', 0);
        $startTimeFltr = $this->request->getParam('start_time_value', '');
        $endTimeFltr = $this->request->getParam('end_time_value', '');

        // load model
        $modelBookingContractorPayment = new Model_BookingContractorPayment();

        $contractorPayment = $modelBookingContractorPayment->getById($id);
	
        // delete row
        $modelBookingContractorPayment->deleteById($id);

        // calculate the total payment after add payments
        $booking_date_filters = array();
        $booking_date_filters['booking_start_between'] = $startTimeFltr;
        $booking_date_filters['booking_end_between'] = $endTimeFltr ? $endTimeFltr . ' 23:59:59' : '';
        $contractorTotalPayment = $modelBookingContractorPayment->getTotalPaymentByContractorId($contractorPayment['contractor_id'], $booking_date_filters);

        echo json_encode(array('total_payment' => $contractorTotalPayment,'payment_to_contractor' => $contractorPayment['payment_to_contractor'], 'contractor_id' => $contractorPayment['contractor_id'], 'booking_id' => $contractorPayment['booking_id']));
        exit;
    }

    public function addContractorInvoiceNumberAction() {

		// load model
        $modelPaymentAttachment = new Model_PaymentAttachment();
		$modelPaymentToContractors = new Model_PaymentToContractors();
		$modelInquiry = new Model_Inquiry();


        $loggedUser = CheckAuth::getLoggedUser();
        $userId = $loggedUser['user_id'];
        
        //get request param
        //
        $bookingsContractorPayment = $this->request->getParam('bookings_contractor_payment', array());
        $contractorInvoiceNumber = $this->request->getParam('invoice_number', '');
        $contractorInvoiceCreated = $this->request->getParam('invoice_created', '');
		$flag = $this->request->getParam('flag',0);
		$options=array();
		$options['flag']= $flag;

        $json = $this->request->getParam('json', 0);

        $form = new Reports_Form_ContractorInvoiceNumber($options);

        if ($this->request->isPost()) {
            if ($form->isValid($this->request->getPost())) {
			
                //get request param
				//
				$startTimeFltr = $this->request->getParam('start_time_value', '');
				$endTimeFltr = $this->request->getParam('end_time_value', '');

				// load model
				$modelBookingContractorPayment = new Model_BookingContractorPayment();
				$contractors = array();
				$bookingPayment = array();
			   
				$description=$this->request->getParam('description', '');
				$reference =$this->request->getParam('reference', '');
				$contractorInvoiceNumber =$this->request->getParam('invoice_number', '');
				$datePaid =$this->request->getParam('date_paid', '');
				$datePaid = date('Y-m-d H:i:s',strtotime($datePaid));
				
				$amountPaid = str_replace(',', '', $this->request->getParam('amount_paid'));
				$cashPaid = str_replace(',', '', $this->request->getParam('cash_paid')); 
				$invoiceTotalAmount = str_replace(',', '', $this->request->getParam('invoice_total_amount')); 
				
				
				////////////add record in payment_to_contractors table
				///if flag = 2 that means this payment assigned before and this payment saved in payment_to_contractors table
				if($flag != 2){
					$paymentToContractorData = array(
							'date_of_payment' => $datePaid,
							'contractor_invoice_num' => $contractorInvoiceNumber,
							'amount_paid' => $amountPaid,
							'cash_paid'=> $cashPaid,
							'invoice_total_amount'=> $invoiceTotalAmount,
							'description' => $description,
							'payment_reference' => $reference,
							'date_created'=> date('Y-m-d H:i:s'),
							'created_by' => $userId
					);
					$paymentToContractorId= $modelPaymentToContractors->insert($paymentToContractorData);
				}	
				////////////update booking_contractor_payment record by getting booking_id, contractor_id, payment_id from the script testVal
				$bookingsContractorPayment = $this->request->getParam('booking_payments');
			    
                $bookingPayment = array();
				$returedData= array();
                if (!empty($bookingsContractorPayment)) {
                   
                        $bookingsContractorPayment = explode('_', $bookingsContractorPayment);
						
						$length = count($bookingsContractorPayment);
						if($flag != 2){
						$updatedPaymentToContractorId= $modelPaymentToContractors->updateById($paymentToContractorId,array('contractor_id' => $bookingsContractorPayment[1]));
						}
						$i = 0;
						$calculated_amount=0;
						while ($i < $length-1) {
							
							$bookingId = (int) (isset($bookingsContractorPayment[$i]) ? $bookingsContractorPayment[$i] : 0);
							$i++;
							$contractorId = (int) (isset($bookingsContractorPayment[$i]) ? $bookingsContractorPayment[$i] : 0);
							$i++;
							/////flag == 0 means this process not payment it just assign, so payment_to_contractor=0
							if($flag == 0){
							$paymentToContractor =0;
							}
							else{
							$paymentToContractor = (float) (isset($bookingsContractorPayment[$i]) ? $bookingsContractorPayment[$i] : 0);
							
							}
							
								/////get calculated amount
								//$calculated_amount=$calculated_amount+$paymentToContractor;
								$data = array(
									'booking_id' => $bookingId,
									'contractor_id' => $contractorId,
									'payment_to_contractor' => $paymentToContractor,
									'contractor_invoice_num' => $contractorInvoiceNumber,
									'contractor_invoice_created' => time(),
									'date_created'=> date('Y-m-d H:i:s'),
									'created_by' => $userId
									
								);
								/////if flag = 2 this payment for assigned invoice so we just update payment value in contractor_booking_payments
								if($flag != 2){
									$data['payment_to_contractor_id']= $paymentToContractorId;
									$contractorPaymentId = $modelBookingContractorPayment->insert($data);
									
								}
								else{
									$contractorPayment = $modelBookingContractorPayment->getBybookingIdAndContractorId($bookingId, $contractorId); 
									$contractorPaymentId = $contractorPayment['id'];
									$paymentToContractorId = $contractorPayment['payment_to_contractor_id'];
									$data['payment_to_contractor_id']= $paymentToContractorId;
									$modelBookingContractorPayment->updateById($contractorPaymentId ,array('payment_to_contractor' => $paymentToContractor));
								}
								
								$data['payment_id']= $contractorPaymentId;
								$returedData[]= $data;
							$i++;
						}
						//$updatedPaymentToContractorId= $modelPaymentToContractors->updateById($paymentToContractorId,array('amount_calculated' => $calculated_amount));
                 }
                
								/////////upload multiFiles
				$apt    = new Zend_File_Transfer_Adapter_Http();
				$files  = $apt->getFileInfo();

				foreach($files as $file => $fileInfo) {
					if ($apt->isUploaded($file)) {
						if ($apt->isValid($file)) {
							if ($apt->receive($file)) {
								$info = $apt->getFileInfo($file);
								$name= $info[$file]['name'];
								$size = $info[$file]['size'];
								$tmp  = $info[$file]['tmp_name'];
								
								$nameArr = explode(".", $name);
								$ext=end($nameArr);
								
								$dir = get_config('payment_attachment') . '/';
								$subdir = date('Y/m/d/');

								//check if file exists or not
								$fullDir = $dir . $subdir;

								if (!is_dir($fullDir)) {
									mkdir($fullDir, 0777, true);
								}
								////////////save file attachment Details in the dataBase
								$data = array(
									'date_created' => date('Y-m-d H:i:s'),
									'size' => $size,
									'created_by' => $userId,
									
								);

								$attachmentId = $modelPaymentAttachment->insert($data);
								$fileName = $attachmentId.'.'. $ext;

								$data = array(
									'payment_to_contractor_id' => $paymentToContractorId,
									'path' => $subdir . $fileName,
									'file_name' => $fileName
								);

								$modelPaymentAttachment->updateById($attachmentId, $data);
								//save image to database and filesystem here
								//$file_saved = copy($source, $fullDir . $fileName);
								$file_saved = copy($tmp, $fullDir . $fileName);

								if ($file_saved) {
									//echo 1;
									//exit;
								}
								 
								
							}
						}
					}
				}
		
		}
       
	    echo json_encode(array('returedData' => $returedData,'flag'=> $flag));
        exit;
		}

        $this->view->form = $form;

        //
        // render views
        //
        
        if ($json) {
            echo json_encode(array('success' => 0, 'html' => $this->view->render('contractor-report/add-contractor-invoice-number.phtml')));
        } else {
            echo $this->view->render('contractor-report/add-contractor-invoice-number.phtml');
        }
        exit;
    }

	
	public function editContractorInvoiceNumberOnlyAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('editInvoiceNumber'));

        //
        // get params
        //
        $payment_to_contractor_id = $this->request->getParam('payment_to_contractor_id');
        $invoice_number = $this->request->getParam('invoice_number');
		
		 //
        // load model
        //
		$modelPaymentToContractors = new Model_PaymentToContractors();
		
		
		$payment = $modelPaymentToContractors->getById($payment_to_contractor_id);
		
       
        if (!$this->request->isPost()) {
            $invoice_number = $payment['contractor_invoice_num'];
        }
       
        $form = new Reports_Form_EditInvoiceNumber(array('payment_to_contractor_id' => $payment_to_contractor_id, 'invoice_number' => $invoice_number));

        if ($this->request->isPost()) {
            if ($form->isValid($this->request->getPost())) { // validate form data
                
                $is_updated = $modelPaymentToContractors->updateById($payment_to_contractor_id, array('contractor_invoice_num' => $invoice_number)); 
				echo json_encode(array('is_updated' => $is_updated,'payment_to_contractor_id'=> $payment_to_contractor_id,'new_invoice_number' => $invoice_number));
                exit;
            }
			else{
				echo json_encode(array('is_updated' => 0));
			}
        }
        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('contractor-report/edit-invoice-number.phtml');
        exit;
    }
	
	
	
	}