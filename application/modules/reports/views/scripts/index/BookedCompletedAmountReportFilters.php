<?php

class Reports_Form_BookedCompletedAmountReportFilters extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('filters');
        $actionUrl = (isset($options['action_url']) ? $options['action_url'] : '');
        $typeValue = (isset($options['type']) ? $options['type'] : '');


        $request = Zend_Controller_Front::getInstance()->getRequest();
        $filters = $request->getParam('fltr', array());
//        var_dump($filters);
//        exit;
        $contractor = new Zend_Form_Element_Select('contractor_id');
        $contractor->setBelongsTo('fltr');
        $contractor->setDecorators(array('ViewHelper'));
        $contractor->setAttrib('style', 'width: 210px;');
        $contractor->setAttrib('class', 'form-control');
        $contractor->addMultiOption('', 'Select One');
        $contractor->setValue(isset($filters['contractor_id']) ? $filters['contractor_id'] : '');
        $modelUser = new Model_User();
        $options = $modelUser->getAllContractor(true,'TRUE'); 
		//$options = array(1,2,3);
        $contractor->addMultiOptions($options);

        $startTimeBetween = new Zend_Form_Element_Text('booking_start_between');
        $startTimeBetween->setBelongsTo('fltr');
        $startTimeBetween->setAttribs(array('id' => "start_time", 'readonly' => "readonly", 'style' => "width: 100%;", 'class' => "form-control"));
        $startTimeBetween->setDecorators(array('ViewHelper'));
        $startTimeBetween->setValue(isset($filters['booking_start_between']) ? $filters['booking_start_between'] : '');


        $endTimeBetween = new Zend_Form_Element_Text('booking_end_between');
        $endTimeBetween->setBelongsTo('fltr');
        $endTimeBetween->setAttribs(array('id' => "end_time", 'readonly' => "readonly", 'style' => "width: 100%;", 'class' => "form-control", 'onchange' => 'checkdate()'));
        $endTimeBetween->setDecorators(array('ViewHelper'));
        $endTimeBetween->setValue(isset($filters['booking_end_between']) ? $filters['booking_end_between'] : '');

		$modelBookingStatus = new Model_BookingStatus();
		$completed = $modelBookingStatus->getByStatusName('COMPLETED');
		$canceled = $modelBookingStatus->getByStatusName('CANCELLED');
		$failed = $modelBookingStatus->getByStatusName('FAILED');
        $endStatusOptions = array($completed['booking_status_id'] => 'Completed', $canceled['booking_status_id'] => 'Canceled', $failed['booking_status_id'] => 'Failed');
        $status = new Zend_Form_Element_Select('completed_status');
        $status->setBelongsTo('fltr');
        $status->setDecorators(array('ViewHelper'));
        $status->setValue(isset($filters['completed_status']) ? $filters['completed_status'] : '');
        $status->setAttrib('style', 'width: 210px;');
        $status->setAttrib('class', 'form-control');
        $status->addMultiOption('', 'Select One');
        $status->addMultiOptions($endStatusOptions);

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Search');
        $button->setAttribs(array('class' => 'btn btn-primary'));

        /*$type = new Zend_Form_Element_Hidden('type');
        $type->setDecorators(array('ViewHelper'));
        $type->setAttribs(array('readonly' => "readonly"));
        $type->setValue($typeValue);*/

        $as_xls = new Zend_Form_Element_Button('as_xls');
        $as_xls->setDecorators(array('ViewHelper'));
        $as_xls->setLabel('Save As (xls)');
        $as_xls->setAttribs(array('class' => 'btn btn-primary', 'onclick' => "save_as_xls();"));

        $this->addElements(array($contractor, $startTimeBetween, $endTimeBetween, $status, $button, $as_xls));
        $this->setMethod('POST');

        $this->setAction($actionUrl);
    }

}
