<?php

class Reports_Form_CustomerFilters extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('filters');
        $actionUrl = (isset($options['action_url']) ? $options['action_url'] : '');
        $typeValue = (isset($options['type']) ? $options['type'] : '');


        $request = Zend_Controller_Front::getInstance()->getRequest();
        $filters = $request->getParam('fltr', array());

        //
        //
        // Customer 
        //
        $customerId = new Zend_Form_Element_Hidden('customer_id');
        $customerId->setBelongsTo('fltr');
        $customerId->setAttribs(array('id' => 'customer_id'));
        $customerId->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setValue((!empty($filters['customer_id']) ? $filters['customer_id'] : ''));
        if (isset($filters['customer_id']) && !empty($filters['customer_id'])) {
            $modelCustomer = new Model_Customer();
            $customer = $modelCustomer->getById($filters['customer_id']);
            $customerName = get_customer_name($customer);
        }

        $autoCustomerId = new Zend_Form_Element_Text('auto_customer_id');
        $autoCustomerId->setDecorators(array('ViewHelper'))
                ->setAttribs(array('style' => 'width: 100%; padding:1px 1px 1px 0;'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setValue(isset($customerName) ? $customerName : '');


        $startTimeBetween = new Zend_Form_Element_Text('booking_start_between');
        $startTimeBetween->setBelongsTo('fltr');
        $startTimeBetween->setAttribs(array('id' => "start_time", 'readonly' => "readonly", 'style' => "width: 100%;"));
        $startTimeBetween->setDecorators(array('ViewHelper'));
        $startTimeBetween->setValue(isset($filters['booking_start_between']) ? $filters['booking_start_between'] : '');


        $endTimeBetween = new Zend_Form_Element_Text('booking_end_between');
        $endTimeBetween->setBelongsTo('fltr');
        $endTimeBetween->setAttribs(array('id' => "end_time", 'readonly' => "readonly", 'style' => "width: 100%;"));
        $endTimeBetween->setDecorators(array('ViewHelper'));
        $endTimeBetween->setValue(isset($filters['booking_end_between']) ? $filters['booking_end_between'] : '');


        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Search');
        $button->setAttribs(array('class' => 'button'));

        $type = new Zend_Form_Element_Hidden('type');
        $type->setDecorators(array('ViewHelper'));
        $type->setAttribs(array('readonly' => "readonly"));
        $type->setValue($typeValue);

        $as_xls = new Zend_Form_Element_Button('as_xls');
        $as_xls->setDecorators(array('ViewHelper'));
        $as_xls->setLabel('Save As (xls)');
        $as_xls->setAttribs(array('class' => 'button', 'onclick' => "save_as_xls();"));

        $this->addElements(array($customerId, $autoCustomerId, $startTimeBetween, $endTimeBetween, $button, $as_xls, $type));
        $this->setMethod('POST');

        $this->setAction($actionUrl);
    }

}

