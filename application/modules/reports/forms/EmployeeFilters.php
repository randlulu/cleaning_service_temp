<?php

class Reports_Form_EmployeeFilters extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('filters');
        $actionUrl = (isset($options['action_url']) ? $options['action_url'] : '');
        $typeValue = (isset($options['type']) ? $options['type'] : '');


        $request = Zend_Controller_Front::getInstance()->getRequest();
        $filters = $request->getParam('fltr', array());
		
		$select_active = new Zend_Form_Element_Select('active');
        $select_active->setDecorators(array('ViewHelper'));
        $select_active->setValue('ALL');
        $select_active->setAttrib('onchange', "getContracotrsByStatus()");
        $select_active->setAttrib('class', "form-control");
		$select_active->setAttrib('style', 'width: 210px;');
		        
        $select_active->addMultiOption('TRUE', 'ACTIVE');
        $select_active->addMultiOption('FALSE', 'INACTIVE');
		$select_active->addMultiOption('ALL', 'ALL');

        $employee = new Zend_Form_Element_Select('created_by');
        $employee->setBelongsTo('fltr');
        $employee->setDecorators(array('ViewHelper'));
        $employee->setValue(isset($filters['created_by']) ? $filters['created_by'] : '');
        $employee->setAttrib('style', 'width: 210px;');
        $employee->setAttrib('class', 'form-control');
        $employee->addMultiOption('', 'Select One');
        $modelUser = new Model_User();
        $options = $modelUser->getAllEmployee(true);
        $employee->addMultiOptions($options);


        $startTimeBetween = new Zend_Form_Element_Text('booking_start_between');
        $startTimeBetween->setBelongsTo('fltr');
        $startTimeBetween->setAttribs(array('id' => "start_time", 'readonly' => "readonly", 'style' => "width: 100%;",'class' => "form-control"));
        $startTimeBetween->setDecorators(array('ViewHelper'));
        $startTimeBetween->setValue(isset($filters['booking_start_between']) ? $filters['booking_start_between'] : '');


        $endTimeBetween = new Zend_Form_Element_Text('booking_end_between');
        $endTimeBetween->setBelongsTo('fltr');
        $endTimeBetween->setAttribs(array('id' => "end_time", 'readonly' => "readonly", 'style' => "width: 100%;", 'class' => "form-control", 'onchange' => 'checkdate()'));
        $endTimeBetween->setDecorators(array('ViewHelper'));
        $endTimeBetween->setValue(isset($filters['booking_end_between']) ? $filters['booking_end_between'] : '');


        // by salim 16/2/2016
        $statusOptions = array('1' => 'Date Started', '2' => 'Date Completed');
        $status = new Zend_Form_Element_Select('report_status');
        $status->setBelongsTo('fltr');
        $status->setDecorators(array('ViewHelper'));
        $status->setValue(isset($filters['report_status']) ? $filters['report_status'] : '');
        $status->setAttrib('style', 'width: 210px;');
        $status->setAttrib('class', 'form-control');
        $status->addMultiOption('', 'Select One');
        $status->addMultiOptions($statusOptions);

        // by salim 16/2/2016
        $calimOwnerOptions = array('1' => 'Booking Creator', '2' => 'Booking Owner');
        $claimOwner = new Zend_Form_Element_Select('claim_owner');
        $claimOwner->setBelongsTo('fltr');
        $claimOwner->setDecorators(array('ViewHelper'));
        $claimOwner->setValue(isset($filters['claim_owner']) ? $filters['claim_owner'] : '');
        $claimOwner->setAttrib('style', 'width: 210px;');
        $claimOwner->setAttrib('class', 'form-control');
        $claimOwner->addMultiOption('', 'Select One');
        $claimOwner->addMultiOptions($calimOwnerOptions);
        
        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Search');
        $button->setAttribs(array('class' => 'btn btn-primary'));

        $type = new Zend_Form_Element_Hidden('type');
        $type->setDecorators(array('ViewHelper'));
        $type->setAttribs(array('readonly' => "readonly"));
        $type->setValue($typeValue);

        $as_xls = new Zend_Form_Element_Button('as_xls');
        $as_xls->setDecorators(array('ViewHelper'));
        $as_xls->setLabel('Save As (xls)');
        $as_xls->setAttribs(array('class' => 'btn btn-primary', 'onclick' => "save_as_xls();"));

        $this->addElements(array($employee, $startTimeBetween, $endTimeBetween, $status, $claimOwner, $button, $as_xls, $type,$select_active));
        $this->setMethod('POST');

        $this->setAction($actionUrl);
    }

}

