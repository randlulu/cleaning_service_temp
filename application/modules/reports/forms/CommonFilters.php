<?php

class Reports_Form_CommonFilters extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('filters');
        $actionUrl = (isset($options['action_url']) ? $options['action_url'] : '');
        $typeValue = (isset($options['type']) ? $options['type'] : '');

        $request = Zend_Controller_Front::getInstance()->getRequest();
        $filters = $request->getParam('fltr', array());

        $startTimeBetween = new Zend_Form_Element_Text('booking_start_between');
        $startTimeBetween->setBelongsTo('fltr');
        $startTimeBetween->setAttribs(array('id' => "start_time", 'readonly' => "readonly", 'style' => "width: 100%;" ,'class'=>"form-control"));
        $startTimeBetween->setAttribs(array('class'=>'form-control'));
        $startTimeBetween->setDecorators(array('ViewHelper'));
        $startTimeBetween->setValue(isset($filters['booking_start_between']) ? $filters['booking_start_between'] : '');
        

        $endTimeBetween = new Zend_Form_Element_Text('booking_end_between');
        $endTimeBetween->setBelongsTo('fltr');
        $endTimeBetween->setAttribs(array('id' => "end_time", 'readonly' => "readonly", 'style' => "width: 100%;" , 'class'=>'form-control'));
        $endTimeBetween->setDecorators(array('ViewHelper'));
        $endTimeBetween->setValue(isset($filters['booking_end_between']) ? $filters['booking_end_between'] : '');


        $modelServices = new Model_Services();
        $allService = $modelServices->getAllService();
     
        $service = new Zend_Form_Element_Select('service_id');
        $service->setBelongsTo('fltr');
        $service->setDecorators(array('ViewHelper'));
        $service->setAttrib("style", "width: 100%;" );
        $service->setAttribs(array('class'=>'form-control'));
        $service->setValue((!empty($filters['service_id']) ? $filters['service_id'] : ''));
        $service->addMultiOption('', 'Select One');
        $service->addMultiOptions($allService);

        $modelAttributes = new Model_Attributes();
        $attribute = $modelAttributes->getByVariableName('Floor');

        $modelAttributeListValue = new Model_AttributeListValue();
        $attributeListValue = $modelAttributeListValue->getByAttributeIdAsArray($attribute['attribute_id']);
    
        
        $floor = new Zend_Form_Element_Select('attribute_value_id');
        $floor->setBelongsTo('fltr');
        $floor->setDecorators(array('ViewHelper'));
        $floor->setAttrib("style", "width: 100%;");
        $floor->setAttribs(array('class'=>'form-control'));
        $floor->setValue((!empty($filters['attribute_value_id']) ? $filters['attribute_value_id'] : ''));
        $floor->addMultiOption('', 'Select One');
        $floor->addMultiOptions($attributeListValue);


        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Search');
        $button->setAttribs(array('class' => 'btn btn-primary'));

        $type = new Zend_Form_Element_Hidden('type');
        $type->setDecorators(array('ViewHelper'));
        $type->setAttribs(array('readonly' => "readonly"));
        $type->setValue($typeValue);

        $as_xls = new Zend_Form_Element_Button('as_xls');
        $as_xls->setDecorators(array('ViewHelper'));
        $as_xls->setLabel('Save As (xls)');
        $as_xls->setAttribs(array('class' => 'btn btn-info', 'onclick' => "save_as_xls();"));

        $this->addElements(array($startTimeBetween, $endTimeBetween, $service, $floor, $button, $as_xls, $type));
        $this->setMethod('POST');

        $this->setAction($actionUrl);
    }

}

