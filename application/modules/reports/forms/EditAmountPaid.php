<?php

class Reports_Form_EditAmountPaid extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $old_paid_amount = isset($options['old_paid_amount']) ? $options['old_paid_amount'] : '';

        $this->setName('EditAmountPaid');

        $router = Zend_Controller_Front::getInstance()->getRouter();

        $paid_amount = new Zend_Form_Element_Text('paid_amount');
        $paid_amount->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue($old_paid_amount)
                ->setErrorMessages(array('Required' => 'Please Enter Invoice Number.'));

        
        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));


        $this->addElements(array($paid_amount, $button));

        $this->setMethod('post');

        $this->setAction($router->assemble(array(), 'editPaidAmount'));
    }

}