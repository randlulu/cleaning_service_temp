<?php

class Reports_Form_ContractorFilters extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('filters');
        $actionUrl = (isset($options['action_url']) ? $options['action_url'] : '');
        $typeValue = (isset($options['type']) ? $options['type'] : '');

        $request = Zend_Controller_Front::getInstance()->getRequest();
        $filters = $request->getParam('fltr', array());

        $contractor = new Zend_Form_Element_Select('contractor_id');
        $contractor->setBelongsTo('fltr');
        $contractor->setDecorators(array('ViewHelper'));
        $contractor->setValue(isset($filters['contractor_id']) ? $filters['contractor_id'] : '');
        $contractor->setAttrib('style', 'width: 210px;');
        $contractor->addMultiOption('', 'Select One');
        $modelUser = new Model_User();
        $options = $modelUser->getAllContractor(true);
        $contractor->addMultiOptions($options);
		
		////////By Islam for new filters paid status and invoice number
		$paid_status = new Zend_Form_Element_Select('paid_status');
        $paid_status->setBelongsTo('fltr');
        $paid_status->setDecorators(array('ViewHelper'));
        $paid_status->setValue(isset($filters['paid_status']) ? $filters['paid_status'] : '');
        $paid_status->setAttrib('style', 'width: 210px;');
        $paid_status->addMultiOption('', 'Select One');
        $options = array('All','Paid to contractor','Unpaid to contractor');
        $paid_status->addMultiOptions($options);
		
		$customer_payment_status = new Zend_Form_Element_Select('customer_payment_status');
        $customer_payment_status->setBelongsTo('fltr');
        $customer_payment_status->setDecorators(array('ViewHelper'));
        $customer_payment_status->setValue(isset($filters['customer_payment_status']) ? $filters['customer_payment_status'] : '');
        $customer_payment_status->setAttrib('style', 'width: 210px;');
        $customer_payment_status->addMultiOption('', 'Select One');
        $options = array('All','Paid by customer','Unpaid by customer');
        $customer_payment_status->addMultiOptions($options);
		
		$Inv = new Zend_Form_Element_Text('Inv');
        $Inv->setBelongsTo('fltr');
        $Inv->setAttribs(array('id' => "Inv", 'style' => "width: 100%;"));
        $Inv->setDecorators(array('ViewHelper'));
        $Inv->setValue(isset($filters['Inv']) ? $filters['Inv'] : '');
		
		$booking_status = new Zend_Form_Element_Select('booking_status');
        $booking_status->setBelongsTo('fltr');
        $booking_status->setDecorators(array('ViewHelper'));
        $booking_status->setValue(isset($filters['booking_status']) ? $filters['booking_status'] : '');
        $booking_status->setAttrib('style', 'width: 210px;');
        $booking_status->addMultiOption('', 'Select One');
		$modelBookingStatus = new Model_BookingStatus();
        $options = $modelBookingStatus->getAllStatusAsArray();
        $booking_status->addMultiOptions($options);

		
		/////////////////////

        $startTimeBetween = new Zend_Form_Element_Text('booking_start_between');
        $startTimeBetween->setBelongsTo('fltr');
        $startTimeBetween->setAttribs(array('id' => "start_time", 'readonly' => "readonly", 'style' => "width: 100%;"));
        $startTimeBetween->setDecorators(array('ViewHelper'));
        $startTimeBetween->setValue(isset($filters['booking_start_between']) ? $filters['booking_start_between'] : '');


        $endTimeBetween = new Zend_Form_Element_Text('booking_end_between');
        $endTimeBetween->setBelongsTo('fltr');
        $endTimeBetween->setAttribs(array('id' => "end_time", 'readonly' => "readonly", 'style' => "width: 100%;"));
        $endTimeBetween->setDecorators(array('ViewHelper'));
        $endTimeBetween->setValue(isset($filters['booking_end_between']) ? $filters['booking_end_between'] : '');



        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Search');
        $button->setAttribs(array('class' => 'button'));

        $type = new Zend_Form_Element_Hidden('type');
        $type->setDecorators(array('ViewHelper'));
        $type->setAttribs(array('readonly' => "readonly"));
        $type->setValue($typeValue);

        $as_xls = new Zend_Form_Element_Button('as_xls');
        $as_xls->setDecorators(array('ViewHelper'));
        $as_xls->setLabel('Save As (xls)');
        $as_xls->setAttribs(array('class' => 'button', 'onclick' => "save_as_xls();"));

        $this->addElements(array($contractor, $paid_status, $customer_payment_status, $Inv, $booking_status, $startTimeBetween, $endTimeBetween, $button, $as_xls, $type));
        $this->setMethod('POST');

        $this->setAction($actionUrl);
    }

}

