<?php

class Reports_Form_PaymentFilters extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('filters');
        $actionUrl = (isset($options['action_url']) ? $options['action_url'] : '');
        $typeValue = (isset($options['type']) ? $options['type'] : '');
        $createdDate = (isset($options['created_date']) ? $options['created_date'] : '');
        $endDate = (isset($options['end_date']) ? $options['end_date'] : '');


        $request = Zend_Controller_Front::getInstance()->getRequest();
        $filters = $request->getParam('fltr', array());

        $startDateBetween = new Zend_Form_Element_Text('payment_created_between');
        $startDateBetween->setBelongsTo('fltr');
        $startDateBetween->setAttribs(array('id' => "start_time",'class' => "form-control", 'readonly' => "readonly", 'style' => "width: 100%;", 'onclick' => 'selectFilterOption(10)'));
        $startDateBetween->setDecorators(array('ViewHelper'));
        $startDateBetween->setValue(isset($createdDate) ? $createdDate : '');

        $endDateBetween = new Zend_Form_Element_Text('payment_end_between');
        $endDateBetween->setBelongsTo('fltr');
        $endDateBetween->setAttribs(array('id' => "end_time", 'readonly' => "readonly",'class' => "form-control", 'style' => "width: 100%;", 'onclick' => 'selectFilterOption(10)'));
        $endDateBetween->setDecorators(array('ViewHelper'));
        $endDateBetween->setValue(isset($endDate) ? $endDate : '');


        $dateRange = new Zend_Form_Element_Select('date_range');
        $dateRange->setBelongsTo('fltr');
        $dateRange->setAttribs(array('id' => "date_range", 'class' => "form-control", 'onchange' => 'selectFromToDate();'));
        $dateRange->setDecorators(array('ViewHelper'));
        $dateRange->setValue(isset($filters['date_range']) ? $filters['date_range'] : '4');

        $option = array(
            'current' => array(
                '0' => 'today',
                '2' => 'this week',
                '4' => 'this month',
                '6' => 'this quarter',
                '8' => 'this year'
            ),
            'previous' => array(
                '1' => 'yesterday',
                '3' => 'previous week',
                '5' => 'previous month',
                '7' => 'previous quarter',
                '9' => 'previous year'
            ),
            'custome' => array('10' => 'custome')
        );
        $dateRange->setMultiOptions($option);

        $type = new Zend_Form_Element_Hidden('type');
        $type->setDecorators(array('ViewHelper'));
        $type->setAttribs(array('readonly' => "readonly"));
        $type->setValue($typeValue);

        $as_xls = new Zend_Form_Element_Button('as_xls');
        $as_xls->setDecorators(array('ViewHelper'));
        $as_xls->setLabel('Save As (xls)');
        $as_xls->setAttribs(array('class' => 'btn btn-primary', 'onclick' => "save_as_xls();"));

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Search');
        $button->setAttribs(array('class' => 'btn btn-primary'));

        $this->addElements(array($startDateBetween, $endDateBetween, $button, $dateRange, $as_xls, $type));
        $this->setMethod('POST');

        $this->setAction($actionUrl);
    }

}

