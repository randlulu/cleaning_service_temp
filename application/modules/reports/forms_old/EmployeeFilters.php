<?php

class Reports_Form_EmployeeFilters extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('filters');
        $actionUrl = (isset($options['action_url']) ? $options['action_url'] : '');
        $typeValue = (isset($options['type']) ? $options['type'] : '');


        $request = Zend_Controller_Front::getInstance()->getRequest();
        $filters = $request->getParam('fltr', array());

        $employee = new Zend_Form_Element_Select('created_by');
        $employee->setBelongsTo('fltr');
        $employee->setDecorators(array('ViewHelper'));
        $employee->setValue(isset($filters['created_by']) ? $filters['created_by'] : '');
        $employee->setAttrib('style', 'width: 210px;');
        $employee->addMultiOption('', 'Select One');
        $modelUser = new Model_User();
        $options = $modelUser->getAllEmployee(true);
        $employee->addMultiOptions($options);


        $startTimeBetween = new Zend_Form_Element_Text('booking_start_between');
        $startTimeBetween->setBelongsTo('fltr');
        $startTimeBetween->setAttribs(array('id' => "start_time", 'readonly' => "readonly", 'style' => "width: 100%;"));
        $startTimeBetween->setDecorators(array('ViewHelper'));
        $startTimeBetween->setValue(isset($filters['booking_start_between']) ? $filters['booking_start_between'] : '');


        $endTimeBetween = new Zend_Form_Element_Text('booking_end_between');
        $endTimeBetween->setBelongsTo('fltr');
        $endTimeBetween->setAttribs(array('id' => "end_time", 'readonly' => "readonly", 'style' => "width: 100%;"));
        $endTimeBetween->setDecorators(array('ViewHelper'));
        $endTimeBetween->setValue(isset($filters['booking_end_between']) ? $filters['booking_end_between'] : '');


        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Search');
        $button->setAttribs(array('class' => 'button'));

        $type = new Zend_Form_Element_Hidden('type');
        $type->setDecorators(array('ViewHelper'));
        $type->setAttribs(array('readonly' => "readonly"));
        $type->setValue($typeValue);

        $as_xls = new Zend_Form_Element_Button('as_xls');
        $as_xls->setDecorators(array('ViewHelper'));
        $as_xls->setLabel('Save As (xls)');
        $as_xls->setAttribs(array('class' => 'button', 'onclick' => "save_as_xls();"));

        $this->addElements(array($employee, $startTimeBetween, $endTimeBetween, $button, $as_xls, $type));
        $this->setMethod('POST');

        $this->setAction($actionUrl);
    }

}

