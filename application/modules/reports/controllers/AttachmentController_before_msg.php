<?php

class Reports_AttachmentController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
    }

    public function indexAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('attachment'));

        // get request parameters
        $inquiryId = $this->request->getParam('id');
        $orderBy = $this->request->getParam('sort', 'attachment_id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        // Load Model
        $modelBookingAttachment = new Model_BookingAttachment();
        $modelInquiry = new Model_Inquiry();

        if (!$inquiryId) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permession."));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        if (!$modelInquiry->checkIfCanSeeInquiry($inquiryId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permession."));
            $this->_redirect($this->router->assemble(array(), 'inquiry'));
        }

        $filters['inquiry_id'] = $inquiryId;

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        //
        // get data list
        //
        $inquiryAttachment = $modelBookingAttachment->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);


        //
        // set view params
        //
        $this->view->inquiryAttachment = $inquiryAttachment;
        $this->view->inquiry_id = $inquiryId;
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
    }

    public function fileUploadAction() {

        //
        // check Auth for logged user
        //
       // CheckAuth::checkPermission(array('attachmentFileUpload'));

        //
        // get request parameters
        //
        // load model
        $modelPaymentAttachment = new Model_PaymentAttachment();
		$modelPaymentToContractors = new Model_PaymentToContractors();
		$modelInquiry = new Model_Inquiry();


        $loggedUser = CheckAuth::getLoggedUser();
        $userId = $loggedUser['user_id'];


         //init action form
        $form = new Reports_Form_ContractorInvoiceNumber();
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
		if ($form->isValid($this->request->getPost())) {
                //get request param
				//
				$startTimeFltr = $this->request->getParam('start_time_value', '');
				$endTimeFltr = $this->request->getParam('end_time_value', '');

				// load model
				$modelBookingContractorPayment = new Model_BookingContractorPayment();
				$contractors = array();
				$bookingPayment = array();
			   
				$description=$this->request->getParam('description', '');
				$reference =$this->request->getParam('reference', '');
				$contractorInvoiceNumber =$this->request->getParam('invoice_number', '');
				$datePaid =$this->request->getParam('date_paid', '');
				$datePaid = date('Y-m-d H:i:s',strtotime($datePaid));
				
				$amountPaid = str_replace(',', '', $this->request->getParam('amount_paid'));
				$cashPaid = str_replace(',', '', $this->request->getParam('cash_paid')); 
				$invoiceTotalAmount = str_replace(',', '', $this->request->getParam('invoice_total_amount')); 
				
				////////////add record in payment_to_contractors table
				$paymentToContractorData = array(
						'date_of_payment' => $datePaid,
						'contractor_invoice_num' => $contractorInvoiceNumber,
						'amount_paid' => $amountPaid,
						'cash_paid'=> $cashPaid,
						'invoice_total_amount'=> $invoiceTotalAmount,
						'description' => $description,
						'payment_reference' => $reference,
						'date_created'=> date('Y-m-d H:i:s'),
						'created_by' => $userId
				);
				$paymentToContractorId= $modelPaymentToContractors->insert($paymentToContractorData);
					
				////////////update booking_contractor_payment record by getting booking_id, contractor_id, payment_id from the script testVal
				$bookingsContractorPayment = $this->request->getParam('booking_payments');
			    //echo "bookingsContractorPayment =".json_encode($bookingsContractorPayment)."";
				
                $bookingPayment = array();
				$returedData= array();
                if (!empty($bookingsContractorPayment)) {
                   
                        $bookingsContractorPayment = explode('_', $bookingsContractorPayment);
						//print_r($bookingsContractorPayment);
						//echo "bookingsContractorPayment =".json_encode($bookingsContractorPayment)."";
						$length = count($bookingsContractorPayment);
						$updatedPaymentToContractorId= $modelPaymentToContractors->updateById($paymentToContractorId,array('contractor_id' => $bookingsContractorPayment[1]));
                    
						$i = 0;
						$calculated_amount=0;
						while ($i < $length-1) {
							
							$bookingId = (int) (isset($bookingsContractorPayment[$i]) ? $bookingsContractorPayment[$i] : 0);
							$i++;
							$contractorId = (int) (isset($bookingsContractorPayment[$i]) ? $bookingsContractorPayment[$i] : 0);
							$i++;
							$paymentToContractor = (float) (isset($bookingsContractorPayment[$i]) ? $bookingsContractorPayment[$i] : 0);
							
								/////get calculated amount
								$calculated_amount=$calculated_amount+$paymentToContractor;
								
								$data = array(
									'booking_id' => $bookingId,
									'contractor_id' => $contractorId,
									'payment_to_contractor' => $paymentToContractor,
									'contractor_invoice_num' => $contractorInvoiceNumber,
									'contractor_invoice_created' => time(),
									'payment_to_contractor_id'=>$paymentToContractorId,
									'date_created'=> date('Y-m-d H:i:s'),
									'created_by' => $userId
								);
								
								
								
								//date('Y-m-d H:i:s')
								$contractorPaymentId = $modelBookingContractorPayment->insert($data);
								$data['payment_id']= $contractorPaymentId;
								$returedData[]= $data;
							$i++;
						}
						$updatedPaymentToContractorId= $modelPaymentToContractors->updateById($paymentToContractorId,array('amount_calculated' => $calculated_amount));
                 }
                
				
				
								/////////upload multiFiles
				$apt    = new Zend_File_Transfer_Adapter_Http();
				$files  = $apt->getFileInfo();

				foreach($files as $file => $fileInfo) {
					if ($apt->isUploaded($file)) {
						if ($apt->isValid($file)) {
							if ($apt->receive($file)) {
								$info = $apt->getFileInfo($file);
								$name= $info[$file]['name'];
								$size = $info[$file]['size'];
								$tmp  = $info[$file]['tmp_name'];
								
								$nameArr = explode(".", $name);
								$ext=end($nameArr);
								
								$dir = get_config('payment_attachment') . '/';
								$subdir = date('Y/m/d/');

								//check if file exists or not
								$fullDir = $dir . $subdir;

								if (!is_dir($fullDir)) {
									mkdir($fullDir, 0777, true);
								}
								////////////save file attachment Details in the dataBase
								$data = array(
									'date_created' => date('Y-m-d H:i:s'),
									'size' => $size,
									'created_by' => $userId,
									
								);

								$attachmentId = $modelPaymentAttachment->insert($data);
								$fileName = $attachmentId.'.'. $ext;

								$data = array(
									'payment_to_contractor_id' => $paymentToContractorId,
									'path' => $subdir . $fileName,
									'file_name' => $fileName
								);

								$modelPaymentAttachment->updateById($attachmentId, $data);
								//save image to database and filesystem here
								//$file_saved = copy($source, $fullDir . $fileName);
								$file_saved = copy($tmp, $fullDir . $fileName);

								if ($file_saved) {
									//echo 1;
									//exit;
								}
								 
								
							}
						}
					}
				}
		
		}
       
	    echo json_encode(array('returedData' => $returedData));
        exit;
		}
		else{
			echo json_encode(array('returedData' => 'error'));
			exit;
		}

    }

    public function deleteAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('attachmentFileDelete'));


        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());


        // load model
        $modelBookingAttachment = new Model_BookingAttachment();
        $modelInquiry = new Model_Inquiry();

        $dir = get_config('attachment') . '/';
        if ($id) {
            $ids[] = $id;
        }
        foreach ($ids as $id) {
            $isValid = true;
            $bookingAttachment = $modelBookingAttachment->getById($id);

            if (!$bookingAttachment) {
                $isValid = false;
            }

            $inquiryId = !empty($bookingAttachment['inquiry_id']) ? $bookingAttachment['inquiry_id'] : 0;

            if ($inquiryId) {
                if (!CheckAuth::checkIfCanHandelAllCompany('inquiry', $inquiryId)) {
                    $isValid = false;
                }
                if (!$modelInquiry->checkIfCanSeeInquiry($inquiryId)) {
                    $isValid = false;
                }
            } else {
                $isValid = false;
            }

            if ($isValid) {
                $bookingAttachment = $modelBookingAttachment->getById($id);
                // get source file
                $source = $dir . $bookingAttachment['path'];

                // delete source file
                unlink($source);

                $modelBookingAttachment->deleteById($id);
            }
        }
        $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
    }

    public function downloadAction() {
 //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('attachmentFileDownload'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
		$file_name = $this->request->getParam('file_name');
        // load model 
        $modelPaymentAttachment = new Model_PaymentAttachment();
        


        $paymentAttachment = $modelPaymentAttachment->getById($id);

        if (!$paymentAttachment) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "file not exit"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $inquiryId = !empty($bookingAttachment['inquiry_id']) ? $bookingAttachment['inquiry_id'] : 0;

        

        $path = $paymentAttachment['path'];
        $dir = get_config('payment_attachment') . '/';
        $filename = "{$dir}/{$path}";
        $file_extension = strtolower(substr(strrchr($filename, "."), 1));

        switch ($file_extension) {
            case "pdf": $ctype = "application/pdf";
                break;
            case "exe": $ctype = "application/octet-stream";
                break;
            case "zip": $ctype = "application/zip";
                break;
            case "doc": $ctype = "application/msword";
                break;
            case "xls": $ctype = "application/vnd.ms-excel";
                break;
            case "ppt": $ctype = "application/vnd.ms-powerpoint";
                break;
            case "gif": $ctype = "image/gif";
                break;
            case "png": $ctype = "image/png";
                break;
            case "jpe": case "jpeg":
            case "jpg": $ctype = "image/jpg";
                break;
            default: $ctype = "application/force-download";
        }

        if (!file_exists($filename)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => 'file not exit'));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private", false);
        header("Content-Type: $ctype");
        header("Content-Disposition: inline; filename=\"" . $file_name.".".$file_extension . "\";");
        header("Content-Transfer-Encoding: binary");
        header("Content-Length: " . @filesize($filename));
        set_time_limit(0);
        readfile("$filename") or die("File not found.");
        exit;
    }
    public function editDescriptionAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('attachmentFileEditDescription'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $description = $this->request->getParam('description');

        // load model
        $modelBookingAttachment = new Model_BookingAttachment();
        $modelInquiry = new Model_Inquiry();


        $bookingAttachment = $modelBookingAttachment->getById($id);

        if (!$bookingAttachment) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "file not exit"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $inquiryId = !empty($bookingAttachment['inquiry_id']) ? $bookingAttachment['inquiry_id'] : 0;

        if ($inquiryId) {
            if (!CheckAuth::checkIfCanHandelAllCompany('inquiry', $inquiryId)) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permession."));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            }
            if (!$modelInquiry->checkIfCanSeeInquiry($inquiryId)) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permession."));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            }
        } else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permession."));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }


        //
        // init action form
        //      = 
        $form = new Inquiry_Form_DescriptionFile(array('description' => $bookingAttachment['description'], 'id' => $id));

        //
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'description' => $description
                );

                $success = $modelBookingAttachment->updateById($id, $data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in description file"));
                }

                echo 1;
                exit;
            }
        }

        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('attachment/edit-description.phtml');
        exit;
    }

    public function editWorkOrderAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('attachmentEditWorkOrder'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $workOrder = $this->request->getParam('work_order');

        // load model
        $modelBookingAttachment = new Model_BookingAttachment();
        $modelInquiry = new Model_Inquiry();


        $bookingAttachment = $modelBookingAttachment->getById($id);

        if (!$bookingAttachment) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "file not exit"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $inquiryId = !empty($bookingAttachment['inquiry_id']) ? $bookingAttachment['inquiry_id'] : 0;

        if ($inquiryId) {
            if (!CheckAuth::checkIfCanHandelAllCompany('inquiry', $inquiryId)) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permession."));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            }
            if (!$modelInquiry->checkIfCanSeeInquiry($inquiryId)) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permession."));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            }
        } else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permession."));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $workOrder = !empty($workOrder) ? 0 : 1;

        $data = array(
            'work_order' => $workOrder
        );

        $success = $modelBookingAttachment->updateById($id, $data);

        if ($success) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Changed successfully"));
        } else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Work Order"));
        }
        $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
    }

    public function viewDescriptionAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('attachmentFileViewDescription'));

        //
        // get request parameters
        //
       
        $id = $this->request->getParam('id');


        // load model
        $modelBookingAttachment = new Model_BookingAttachment();
        $modelInquiry = new Model_Inquiry();

        $bookingAttachment = $modelBookingAttachment->getById($id);

        $inquiryId = !empty($bookingAttachment['inquiry_id']) ? $bookingAttachment['inquiry_id'] : 0;

        if ($inquiryId) {
            if (!CheckAuth::checkIfCanHandelAllCompany('inquiry', $inquiryId)) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permession."));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            }
            if (!$modelInquiry->checkIfCanSeeInquiry($inquiryId)) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permession."));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            }
        } else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permession."));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $this->view->bookingAttachment = $bookingAttachment;
        //
        // render views
        //
        echo $this->view->render('attachment/view-description.phtml');
        exit;
    }

}

