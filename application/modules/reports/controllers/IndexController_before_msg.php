<?php

class Reports_IndexController extends Zend_Controller_Action {

    private $request;
    private $router;
    private $LoggedUser;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'reports';
        $this->view->sub_menu = 'reports';

        $this->LoggedUser = CheckAuth::getLoggedUser();
    }

    public function indexAction() {
        
    }

    public function savedReportAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('savedReport'));

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'report_id');
        $sortingMethod = $this->request->getParam('method', 'DESC');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        //
        // get data list
        //
        $modelReport = new Model_Report();
        $data = $modelReport->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);

        $modelUser = new Model_User();
        $modelUser->fills($data, array('user'));

        //
        // set view params
        //
        $this->view->data = $data;
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
        $this->view->sub_menu = 'savedReport';
    }

    public function deleteAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('savedReportDelete'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
        if ($id) {
            $ids[] = $id;
        }

        $modelReport = new Model_Report();
        foreach ($ids as $id) {
            if (CheckAuth::checkIfCanHandelAllCompany('report', $id)) {
                $report = $modelReport->getById($id);
                if ($report) {
                    $dir = get_config('report_file') . "/{$report['report_type']}";
                    $filename = "{$dir}/{$id}.xls";

                    unlink($filename);

                    $modelReport->deleteById($id);
                }
            }
        }

        $this->_redirect($this->router->assemble(array(), 'savedReport'));
    }

    public function downloadAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('savedReportDownload'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);

        if (!CheckAuth::checkIfCanHandelAllCompany('report', $id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $modelReport = new Model_Report();
        $report = $modelReport->getById($id);
        if ($report) {
            $dir = get_config('report_file') . "/{$report['report_type']}";
            $filename = "{$dir}/{$id}.xls";


            header("Content-type: application/x-msdownload");
            header("Content-Disposition: attachment; filename={$report['report_type']}_report.xls");
            header("Pragma: no-cache");
            header("Expires: 0");
            
            if (file_exists($filename)) {
                readfile($filename);
            } else {
                print $report['xls_body'];
            }
            
            exit;
        }

        $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => 'Report Error'));
        $this->_redirect($this->router->assemble(array(), 'savedReport'));
    }

}