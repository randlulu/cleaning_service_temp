<?php

class Reports_PaymentReportController extends Zend_Controller_Action {

    private $request;
    private $router;
    private $LoggedUser;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'reports';
        $this->view->sub_menu = 'reports';

        $this->LoggedUser = CheckAuth::getLoggedUser();
    }

    public function paymentReceivedAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('reportPaymentReceived'));
        $modelPayment = new Model_Payment();
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        if (!isset($filters['payment_created_between']) && !isset($filters['payment_end_between'])) {
            $filters['payment_created_between'] = date('Y-m-1', time());
            $filters['payment_end_between'] = date('Y-m-d', strtotime('-1 second', strtotime('+1 month', strtotime(date('m') . '/01/' . date('Y') . ' 00:00:00'))));
        }

        $options = array(
            'action_url' => $this->router->assemble(array(), 'reportPaymentReceived'),
            'created_date' => $filters['payment_created_between'],
            'end_date' => $filters['payment_end_between'],
            'type' => 'payment_received'
        );
        $form = new Reports_Form_PaymentFilters($options);

        $all_payment_filters = array();
        $all_payment_filters['payment_created_between'] = $filters['payment_created_between'] ? $filters['payment_created_between'] . " 00:00:00" : "";
        $all_payment_filters['payment_end_between'] = $filters['payment_end_between'] ? $filters['payment_end_between'] . " 23:59:59" : "";

        $payments = $modelPayment->getAll($all_payment_filters, 'received_date ASC');
        $modelPayment->fills($payments, array('customer', 'payment_type', 'total_amount', 'invoice'));

        $this->view->payments = $payments;
        $this->view->fltr = $filters;
        $this->view->form = $form;
    }
	
	
	////////By Islam action of payment to contractor report
	public function paymentToContractorsAction() {

        //
        // check Auth for logged user
        //
        //CheckAuth::checkPermission(array('reportPaymentReceived'));
        $modelPayment = new Model_PaymentToContractors();
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }
		if (!isset($filters['contractor_id']) && !isset($filters['contractor_id'])) {
			$LoggedUser=CheckAuth::getLoggedUser();
			$modelAuthRole = new Model_AuthRole();
			if ($LoggedUser['role_id'] == $modelAuthRole->getRoleIdByName('contractor') && !CheckAuth::checkCredential(array('canSeePaymentsToAllContractors'))){
				$filters['contractor_id'] = $LoggedUser['user_id'];
				}
			else{
				$filters['contractor_id'] = "";
			}
                        
            
        }

        if (!isset($filters['payment_created_between']) && !isset($filters['payment_end_between'])) {
            $filters['payment_created_between'] = date('Y-m-1', time());
            $filters['payment_end_between'] = date('Y-m-d', strtotime('-1 second', strtotime('+1 month', strtotime(date('m') . '/01/' . date('Y') . ' 00:00:00'))));
        }
		
		if (!isset($filters['payment_created_between']) && !isset($filters['payment_end_between'])) {
            $filters['payment_created_between'] = date('Y-m-1', time());
            $filters['payment_end_between'] = date('Y-m-d', strtotime('-1 second', strtotime('+1 month', strtotime(date('m') . '/01/' . date('Y') . ' 00:00:00'))));
        }

        $options = array(
            'action_url' => $this->router->assemble(array(), 'reportPaymentToContractors'),
            'created_date' => $filters['payment_created_between'],
            'end_date' => $filters['payment_end_between'],
            'type' => 'payment_to_contractors'
        );
        $form = new Reports_Form_PaymentToContractorsFilters($options);

        $all_payment_filters = array();
        $all_payment_filters['payment_created_between'] = $filters['payment_created_between'] ? $filters['payment_created_between'] . " 00:00:00" : "";
        $all_payment_filters['payment_end_between'] = $filters['payment_end_between'] ? $filters['payment_end_between'] . " 23:59:59" : "";
		$all_payment_filters['contractor_id']= $filters['contractor_id'] ? $filters['contractor_id']: "";
        $payments = $modelPayment->getAll($all_payment_filters, 'date_of_payment ASC');
        
        $this->view->payments = $payments;
        $this->view->fltr = $filters;
        $this->view->form = $form;
    }

    public function refundHistoryAction() {


        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('reportRefundHistory'));

        // Load Model
        $modelRefund = new Model_Refund();


        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        if (!isset($filters['payment_created_between']) && !isset($filters['payment_end_between'])) {
            $filters['payment_created_between'] = date('Y-m-1', time());
            $filters['payment_end_between'] = date('Y-m-d', strtotime('-1 second', strtotime('+1 month', strtotime(date('m') . '/01/' . date('Y') . ' 00:00:00'))));
        }


        $options = array(
            'action_url' => $this->router->assemble(array(), 'reportRefundHistory'),
            'created_date' => $filters['payment_created_between'],
            'end_date' => $filters['payment_end_between'],
            'type' => 'refund_history'
        );

        $form = new Reports_Form_PaymentFilters($options);

        $all_refund_filters = array();
        $all_refund_filters['payment_created_between'] = $filters['payment_created_between'] ? $filters['payment_created_between'] . " 00:00:00" : "";
        $all_refund_filters['payment_end_between'] = $filters['payment_end_between'] ? $filters['payment_end_between'] . " 23:59:59" : "";

        $refunds = $modelRefund->getAll($all_refund_filters, 'received_date ASC');
        $modelRefund->fills($refunds, array('customer', 'payment_type', 'invoice'));

        $this->view->refunds = $refunds;
        $this->view->fltr = $filters;
        $this->view->form = $form;
    }

    public function overdueInvoicesAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('reportOverdueInvoices'));

        //
        //declaring the moduels
        //
        $modelBooking = new Model_Booking();
        $modelUser = new Model_User();
        $modelPaymentType = new Model_PaymentType();
        $modelPayment = new Model_Payment();

        //
        //get the requested param
        //
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        $form = new Reports_Form_ContractorFilters(array('action_url' => $this->router->assemble(array(), 'reportOverdueInvoices'), 'type' => 'overdue_invoices'));
        $this->view->form = $form;

        $paymentTypes = $modelPaymentType->getAll(array('without_cash' => true));
        $this->view->paymentTypes = $paymentTypes;

        $data = array();
        if ($filters) {
            //
            //the requested contractor info
            //
            $user = $modelUser->getById($filters['contractor_id']);
            $this->view->user = $user;

            //
            //get all the booking
            //
            $all_booking_filters = array();
            $all_booking_filters['contractor_id'] = $filters['contractor_id'];
            $all_booking_filters['booking_start_between'] = $filters['booking_start_between'];
            $all_booking_filters['booking_end_between'] = !empty($filters['booking_end_between']) ? $filters['booking_end_between'] . ' 23:59:59' : '';
            $all_booking_filters['convert_status'] = 'invoice';
            $all_booking_filters['invoice_type'] = 'unpaid';

            $data = $modelBooking->getAll($all_booking_filters, 'booking_start ASC');
            $modelBooking->fills($data, array('contractors', 'address', 'created_by', 'status', 'services', 'invoice', 'full_customer_info', 'number_of_due_days','status_discussion','approved_ammount','unapproved_ammount','total_without_cash','cash_payment'));

        }

        $this->view->data = $data;
        $this->view->filters = $filters;
    }

	
	public function uploadDocumentToGoogleDriveAction(){
		$modleGoogleDrive = new Model_GoogleDrive();
		$modlePaymentAttachment = new Model_PaymentAttachment();
		$contractorId = $this->request->getParam('contractor_id');
		$attachmentId = $this->request->getParam('attachment_id');
		$fileName = $this->request->getParam('file_name');
		$pathArr = $modlePaymentAttachment->getPathById($attachmentId);
		$path = $pathArr['path'];
		$originalFileName = get_config('payment_attachment').'/'.$path;
		
		$retVal = $modleGoogleDrive->uploadDocumentToGoogleDrive($contractorId, false, $originalFileName,$fileName);
		
		echo json_encode(array('retVal' => $retVal, 'attachmentId' => $attachmentId));
		  
		exit;		  
	
	}
	
	
	public function deletePaymentToContractorAttachmentAction(){
			$modlePaymentAttachment = new Model_PaymentAttachment();
			
			$attachmentId = $this->request->getParam('attachment_id');
			
			$isDeleted = $modlePaymentAttachment->deleteById($attachmentId);
			echo json_encode(array('isDeleted' => $isDeleted,'attachmentId' => $attachmentId ));
			exit;
	
	}
	
	public function deletePaymentToContractorsAction(){ 
	
		$payment_to_contractors_id=$this->request->getParam('id');
		////////load models
		$modelPaymentAttachment=new Model_PaymentAttachment();
		$modelPaymentToContractors = new Model_PaymentToContractors();
		
		
		$isDeletedPayment=$modelPaymentToContractors->deleteById($payment_to_contractors_id);
		$areAttachmentsDeleted= 0;
		if($isDeletedPayment){
			$areAttachmentsDeleted = $modelPaymentAttachment->deleteByPaymentToContractorId($payment_to_contractors_id);
		}
		
		echo json_encode(array('isDeletedPayment' => $isDeletedPayment,'payment_to_contractors_id' => $payment_to_contractors_id,'areAttachmentsDeleted' => $areAttachmentsDeleted ));
		exit;
	}
	
}