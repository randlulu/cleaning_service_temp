<?php

class Reports_CommonReportController extends Zend_Controller_Action {

    private $request;
    private $router;
    private $LoggedUser;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'reports';
        $this->view->sub_menu = 'reports';

        $this->LoggedUser = CheckAuth::getLoggedUser();
        
        $pageName = $this->request->getParam('page_name', '');
        $this->view->page_title = (!empty($pageName))?$pageName." - Common Reports - Reports":"Common Reports - Reports";
    }

    public function bookingSummaryAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('bookingSummary'));

        //
        //declaring the moduels
        //
        $modelBooking = new Model_Booking();
        $modelUser = new Model_User();
        $modelPaymentType = new Model_PaymentType();
        $modelPayment = new Model_Payment();
        $modelServices = new Model_Services();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelRefund = new Model_Refund();


        //
        //get the requested param
        //
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        $form = new Reports_Form_ContractorFilters(array('action_url' => $this->router->assemble(array(), 'bookingSummary'), 'type' => 'booking_summary'));
        $this->view->form = $form;

        $paymentTypes = $modelPaymentType->getAll(array('without_cash' => true));
        $this->view->paymentTypes = $paymentTypes;

        $contractorServiceBookings = array();
        if ($filters) {
            //
            //the requested contractor info
            //


            if (CheckAuth::checkCredential(array('canSeeAllContractorReport'))) {

                $contractor_id = isset($filters['contractor_id']) ? $filters['contractor_id'] : '';
            } else {
                $loggedUser = CheckAuth::getLoggedUser();
                $contractor_id = $loggedUser['user_id'];
            }

            $user = $modelUser->getById($contractor_id);
            $this->view->user = $user;
            //
            //get all the booking
            //
            $all_booking_filters = array();
            $all_booking_filters['contractor_id'] = $contractor_id;
            $all_booking_filters['booking_start_between'] = $filters['booking_start_between'];
            $all_booking_filters['booking_end_between'] = $filters['booking_end_between'] ? $filters['booking_end_between'] . ' 23:59:59' : '';

            // get booking for each contractor, so the booking will duplicate if they have more one contractor
            $contractorServiceBookings = $modelContractorServiceBooking->getBookingForEachContractor($all_booking_filters, 'booking_start ASC');

            foreach ($contractorServiceBookings as &$contractorServiceBooking) {

                $bookingId = $contractorServiceBooking['booking_id'];
                $contractorId = $contractorServiceBooking['contractor_id'];

                // contractor
                $modelUser->fill($contractorServiceBooking, array('contractor'));

                // services
                $services = $modelContractorServiceBooking->getByBookingIdAndContractorId($bookingId, $contractorId);
                foreach ($services as &$service) {
                    $serviceinfo = $modelServices->getById($service['service_id']);
                    $service['service_name'] = $serviceinfo['service_name'];
                }
                $contractorServiceBooking['services'] = $services;

                //booking with all data
                $booking = $modelBooking->getById($bookingId);
                $modelBooking->fill($booking, array('address', 'created_by', 'status', 'invoice', 'status_discussion'));
                $contractorServiceBooking['booking'] = $booking;

                // total amount
                $contractorServiceBooking['total_amount'] = $modelContractorServiceBooking->getTotalAmountServicesByContractorIdAndStatusId($contractorId, 0, array('booking_id' => $bookingId));

                //approved and unapproved payment
                $contractorServiceBooking['approved_payment'] = $modelPayment->getTotalPayment(array('booking_id' => $bookingId, 'payment_contractor_id' => $contractorId, 'is_approved' => 'yes'));
                $contractorServiceBooking['unapproved_payment'] = $modelPayment->getTotalPayment(array('booking_id' => $bookingId, 'payment_contractor_id' => $contractorId, 'is_approved' => 'no'));

                //approved and unapproved refund
                $contractorServiceBooking['approved_refund'] = $modelContractorServiceBooking->getTotalContractorRefund($contractorId, array('booking_id' => $bookingId));
                $contractorServiceBooking['unapproved_refund'] = $modelContractorServiceBooking->getTotalContractorRefund($contractorId, array('booking_id' => $bookingId), 'no');

                //  total unpaid 
                $contractorServiceBooking['unpaid'] = $contractorServiceBooking['total_amount'] - ($contractorServiceBooking['approved_payment'] - $contractorServiceBooking['approved_refund']);


                // office payment 
                $totalOfficePayment = 0;
                foreach ($paymentTypes as $paymentType) {
                    $amount = $modelPayment->getTotalPayment(array('booking_id' => $bookingId, 'contractor_id' => $contractorId, 'payment_type' => $paymentType['id'], 'payment_contractor_id' => $contractorId));
                    $amount = $amount ? $amount : 0;
                    $totalOfficePayment = $totalOfficePayment + $amount;
                }
                $contractorServiceBooking['office_payment'] = $totalOfficePayment;

                // total payment cash
                $paymentCashId = $modelPaymentType->getPaymentIdBySlug('cash');
                $contractorCashPayment = $modelPayment->getTotalPayment(array('booking_id' => $bookingId, 'contractor_id' => $contractorId, 'payment_type' => $paymentCashId, 'payment_contractor_id' => $contractorId));
                $contractorServiceBooking['cash_payment'] = $contractorCashPayment;

                /* operator Share
                 * calculate the opreator share from its amount service after discount and refunds
                 * depends on the Comission and Registed Gst
                 */
                $operatorShare = $modelContractorServiceBooking->getTotalContractorShare($contractorId, array('booking_id' => $bookingId));
                $contractorServiceBooking['operator_share'] = $operatorShare;

                //company share
                if ($booking['convert_status'] != 'invoice') {
                    $contractorServiceBooking['company_share'] = 0;
                } else {
                    $contractorServiceBooking['company_share'] = $contractorServiceBooking['total_amount'] - $operatorShare;
                }

                //payment to sub contractor
                $contractorServiceBooking['payment_to_sub_contractor'] = $operatorShare - $contractorCashPayment;
            }
        }


        $this->view->data = $contractorServiceBookings;
        $this->view->filters = $filters;
    }

    public function testAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('bookingServiceTileSummary'));

        //
        //declaring the moduels
        //
        $modelBooking = new Model_Booking();
        $modelPaymentType = new Model_PaymentType();
        $modelPayment = new Model_Payment();
        $modelServices = new Model_Services();
        $modelAttributeListValue = new Model_AttributeListValue();

        //
        //get the requested param
        //
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        $form = new Reports_Form_CommonFilters(array('action_url' => $this->router->assemble(array(), 'bookingServiceTileSummary'), 'type' => 'booking_service_tile_summary'));
        $this->view->form = $form;

        $paymentTypes = $modelPaymentType->getAll(array('without_cash' => true));
        $this->view->paymentTypes = $paymentTypes;

        $data = array();
        if ($filters) {

            //
            //get all the booking
            //
            $all_booking_filters = array();
            $all_booking_filters['service_id'] = $filters['service_id'];
            $all_booking_filters['attribute_value_id'] = $filters['attribute_value_id'];
            $all_booking_filters['booking_start_between'] = $filters['booking_start_between'];
            $all_booking_filters['booking_end_between'] = $filters['booking_end_between'] ? $filters['booking_end_between'] . ' 23:59:59' : '';
            $data = $modelBooking->getAll($all_booking_filters, 'booking_start ASC');
            $modelBooking->fills($data, array('contractors', 'address', 'created_by', 'status', 'services_with_floor_type', 'invoice', 'status_discussion', 'approved_ammount', 'unapproved_ammount', 'total_without_cash', 'cash_payment'));

            $this->view->service = $modelServices->getById($filters['service_id']);
            $this->view->floor = $modelAttributeListValue->getById($filters['attribute_value_id']);
        }

        $this->view->data = $data;
        $this->view->filters = $filters;
    }

    public function bookingServiceTileSummaryAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('bookingServiceTileSummary'));

        //
        //declaring the moduels
        //
        $modelBooking = new Model_Booking();
        $modelUser = new Model_User();
        $modelPaymentType = new Model_PaymentType();
        $modelPayment = new Model_Payment();
        $modelServices = new Model_Services();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelRefund = new Model_Refund();
        $modelAttributeListValue = new Model_AttributeListValue();



        //
        //get the requested param
        //
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }


        $form = new Reports_Form_CommonFilters(array('action_url' => $this->router->assemble(array(), 'bookingServiceTileSummary'), 'type' => 'booking_service_tile_summary'));
        $this->view->form = $form;

        $paymentTypes = $modelPaymentType->getAll(array('without_cash' => true));
        $this->view->paymentTypes = $paymentTypes;

        $contractorServiceBookings = array();
        if ($filters) {


            //
            //get all the booking
            //
            
            $all_booking_filters = array();
            $all_booking_filters['service_id'] = $filters['service_id'];
            $all_booking_filters['attribute_value_id'] = $filters['attribute_value_id'];
            $all_booking_filters['booking_start_between'] = $filters['booking_start_between'];
            $all_booking_filters['booking_end_between'] = $filters['booking_end_between'] ? $filters['booking_end_between'] . ' 23:59:59' : '';

            // get booking for each service and tile type

            $contractorServiceBookings = $modelContractorServiceBooking->getBookingsbyServiceIdandAttributeValueId($all_booking_filters, 'booking_start ASC');

            foreach ($contractorServiceBookings as &$contractorServiceBooking) {

                $bookingId = $contractorServiceBooking['booking_id'];
                $contractorId = $contractorServiceBooking['contractor_id'];

                // contractor
                $modelUser->fill($contractorServiceBooking, array('contractor'));

                // services
                $services = $modelContractorServiceBooking->getByBookingIdAndContractorId($bookingId, $contractorId);
                foreach ($services as &$service) {
                    $serviceinfo = $modelServices->getById($service['service_id']);
                    $service['service_name'] = $serviceinfo['service_name'];
                }
                $contractorServiceBooking['services'] = $services;

                //booking with all data
                $booking = $modelBooking->getById($bookingId);
                $modelBooking->fill($booking, array('address', 'created_by', 'status', 'invoice', 'status_discussion'));
                $contractorServiceBooking['booking'] = $booking;

                // total amount
                $contractorServiceBooking['total_amount'] = $modelContractorServiceBooking->getTotalAmountServicesByContractorIdAndStatusId($contractorId, 0, array('booking_id' => $bookingId));

                //approved and unapproved payment
                $contractorServiceBooking['approved_payment'] = $modelPayment->getTotalPayment(array('booking_id' => $bookingId, 'payment_contractor_id' => $contractorId, 'is_approved' => 'yes'));
                $contractorServiceBooking['unapproved_payment'] = $modelPayment->getTotalPayment(array('booking_id' => $bookingId, 'payment_contractor_id' => $contractorId, 'is_approved' => 'no'));

                //approved and unapproved refund
                $contractorServiceBooking['approved_refund'] = $modelContractorServiceBooking->getTotalContractorRefund($contractorId, array('booking_id' => $bookingId));
                $contractorServiceBooking['unapproved_refund'] = $modelContractorServiceBooking->getTotalContractorRefund($contractorId, array('booking_id' => $bookingId), 'no');

                //  total unpaid 
                $contractorServiceBooking['unpaid'] = $contractorServiceBooking['total_amount'] - ($contractorServiceBooking['approved_payment'] - $contractorServiceBooking['approved_refund']);


                // office payment 
                $totalOfficePayment = 0;
                foreach ($paymentTypes as $paymentType) {
                    $amount = $modelPayment->getTotalPayment(array('booking_id' => $bookingId, 'contractor_id' => $contractorId, 'payment_type' => $paymentType['id'], 'payment_contractor_id' => $contractorId));
                    $amount = $amount ? $amount : 0;
                    $totalOfficePayment = $totalOfficePayment + $amount;
                }
                $contractorServiceBooking['office_payment'] = $totalOfficePayment;

                // total payment cash
                $paymentCashId = $modelPaymentType->getPaymentIdBySlug('cash');
                $contractorCashPayment = $modelPayment->getTotalPayment(array('booking_id' => $bookingId, 'contractor_id' => $contractorId, 'payment_type' => $paymentCashId, 'payment_contractor_id' => $contractorId));
                $contractorServiceBooking['cash_payment'] = $contractorCashPayment;

                /* operator Share
                 * calculate the opreator share from its amount service after discount and refunds
                 * depends on the Comission and Registed Gst
                 */
                $operatorShare = $modelContractorServiceBooking->getTotalContractorShare($contractorId, array('booking_id' => $bookingId));
                $contractorServiceBooking['operator_share'] = $operatorShare;

                //company share
                if ($booking['convert_status'] != 'invoice') {
                    $contractorServiceBooking['company_share'] = 0;
                } else {
                    $contractorServiceBooking['company_share'] = $contractorServiceBooking['total_amount'] - $operatorShare;
                }

                //payment to sub contractor
                $contractorServiceBooking['payment_to_sub_contractor'] = $operatorShare - $contractorCashPayment;
            }

            $this->view->service = $modelServices->getById($filters['service_id']);
            $this->view->floor = $modelAttributeListValue->getById($filters['attribute_value_id']);
        }


        $this->view->data = $contractorServiceBookings;
        $this->view->filters = $filters;
    }

    public function bookingSummaryByChartAction() {

        //
        //get the requested param
        //
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }


        $form = new Reports_Form_ChartFilters(array('action_url' => $this->router->assemble(array(), 'bookingSummaryByChart')));
        $this->view->form = $form;

        if ($filters) {

            if (empty($filters['mode'])) {
                $filters['mode'] = 'monthly';
            }
            if (!in_array($filters['mode'], array('yearly', 'monthly', 'weekly', 'daily'))) {
                $filters['mode'] = 'monthly';
            }

            $startDate = !empty($filters['start']) ? $filters['start'] : date('Y-m-d', time());
            $endDate = !empty($filters['end']) ? $filters['end'] : date('Y-m-d', time());

            //points : filter handler
            $points = array();

            switch ($filters['mode']) {
                case 'yearly':

                    $startDate = getTimePeriodByName('this_year', $startDate);
                    $endDate = getTimePeriodByName('this_year', $endDate);

                    $start = strtotime($startDate['start']);
                    $end = strtotime($endDate['end']);
                    while ($start < $end) {
                        $points[] = getTimePeriodByName('this_year', date('Y-m-d', $start));
                        $start = strtotime("+1 year", $start);
                    }

                    break;

                case 'monthly':

                    $startDate = getTimePeriodByName('this_month', $startDate);
                    $endDate = getTimePeriodByName('this_month', $endDate);

                    $start = strtotime($startDate['start']);
                    $end = strtotime($endDate['end']);
                    while ($start < $end) {
                        $points[] = getTimePeriodByName('this_month', date('Y-m-d', $start));
                        $start = strtotime("+1 month", $start);
                    }

                    break;

                case 'weekly':

                    $startDate = getTimePeriodByName('this_week', $startDate);
                    $endDate = getTimePeriodByName('this_week', $endDate);

                    $start = strtotime($startDate['start']);
                    $end = strtotime($endDate['end']);
                    while ($start < $end) {
                        $points[] = getTimePeriodByName('this_week', date('Y-m-d', $start));
                        $start = strtotime("+1 week", $start);
                    }

                    break;

                case 'daily':

                    $startDate = getTimePeriodByName('this_day', $startDate);
                    $endDate = getTimePeriodByName('this_day', $endDate);

                    $start = strtotime($startDate['start']);
                    $end = strtotime($endDate['end']);
                    while ($start < $end) {
                        $points[] = getTimePeriodByName('this_day', date('Y-m-d', $start));
                        $start = strtotime("+1 day", $start);
                    }

                    break;
            }

            $results = array();
            $modelDailyReport = new Model_DailyReport();
            if ($points) {
                foreach ($points as $point) {
                    $filters['start_between'] = $point['start'];
                    $filters['end_between'] = $point['end'];

                    $resultForChart = array();
                    $resultForChart['expected'] = $modelDailyReport->getCountAndTotal($filters);
                    $resultForChart['real'] = $modelDailyReport->getRealData($filters);
                    $resultForChart['date'] = $modelDailyReport->getDateForChart($filters['mode'], $point['start']);
                    $results[] = $resultForChart;
                }
            }

            $this->view->filters = $filters;
            $this->view->chartArray = $modelDailyReport->chartArray($results, $filters['by']);
        }
    }
	
	
	public function convertToBookingDurationAction(){
        
        $this->view->page_title = 'Convert to Booking Duration - '.$this->view->page_title;


        
        //check Auth for logged user
        CheckAuth::checkPermission(array('booking'));

        //get request parameters
        $orderBy = $this->request->getParam('sort', 'created');
        $sortingMethod = $this->request->getParam('method', 'desc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array('convert_status' => 'booking'));
        $isDeleted = $this->request->getParam('is_deleted', 0);
        $this->view->isDeleted = $isDeleted;

        $filters['withoutEstimateStatus'] = true;
		
        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        $loggedUser = CheckAuth::getLoggedUser();
        $my_bookings = $this->request->getParam('my_bookings', false);


        if ($my_bookings) {
            $this->view->sub_menu = 'my_bookings';
            $filters['my_bookings'] = $loggedUser['user_id'];
        }
        // to get all booking that belong to this user selected from user search
        if (isset($filters['user_id']) && !empty($filters['user_id'])) {
            $filters['my_bookings'] = $filters['user_id'];
        }

        if (isset($filters['convert_status']) && $filters['convert_status'] == 'all') {
            unset($filters['convert_status']);
        }

        if ($isDeleted) {
            if (CheckAuth::checkCredential(array('canSeeDeletedBooking'))) {
                $this->view->sub_menu = 'deleted_booking';

                //get all deleted booking
                $filters['is_deleted'] = $isDeleted;
                unset($filters['convert_status']);
            } else {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
                $this->_redirect($this->router->assemble(array(), 'booking'));
            }
        }


        //Load Model
        $modelBookingStatus = new Model_BookingStatus();
        $modelBooking = new Model_Booking();


        //get all booking Status for drop dawn menu
        $allStatus = $modelBookingStatus->getAllStatusAsArray(array('withoutQouted' => true));


        $select = new Zend_Form_Element_Select('fltr');
        $select->setBelongsTo('status');
        $select->setDecorators(array('ViewHelper'));
        $select->setValue(isset($filters['status']) ? $filters['status'] : '');
        $select->setAttrib('onchange', "fltr('" . $this->router->assemble(array(), 'booking') . "?fltr[status]=','status-fltr')");
        $select->setAttrib('style', "width: 150px;");
        $select->addMultiOption('', 'Booking Status');
        $select->addMultiOption('current', 'CURRENT');
        $select->addMultiOptions($allStatus);
        $this->view->allStatus = $select;



        //init pager and articles model object
        $pager = new Model_Pager();
        $pager->perPage = 30;
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];


        //get data list
        $data = $modelBooking->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);
        $modelBooking->fills($data, array('contractors', 'customer', 'city', 'labels', 'address', 'status', 'services', 'services_temp', 'is_accepted', 'can_delete', 'not_accepted_or_rejected', 'not_accepted', 'reminder', 'booking_users', 'have_attachment'));


        //set view params
        $this->view->data = $data;
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;

        
    }


}