<?php

class Reports_EmployeeReportController extends Zend_Controller_Action {

    private $request;
    private $router;
    private $LoggedUser;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'reports';
        $this->view->sub_menu = 'reports';

        $this->LoggedUser = CheckAuth::getLoggedUser();
    }

    public function salesByEmployeeAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('reportSalesByEmployee'));

        //
        //load model
        //
        $modelBooking = new Model_Booking();
        $modelComplaint = new Model_Complaint();
        $modelUser = new Model_User();
        $modelBookingStatus = new Model_BookingStatus();
        $modelAuthRole = new Model_AuthRole();

        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        $form = new Reports_Form_EmployeeFilters(array('action_url' => $this->router->assemble(array(), 'reportSalesByEmployee'), 'type' => 'sales_by_employee'));
        $this->view->form = $form;
        $data = array();

        if ($filters) {
            $employee = $modelUser->getById($filters['created_by']);
            $this->view->employee = $employee;

            if (CheckAuth::checkCredential(array('canSeeAllSalesByEmployeeReport'))) {
                if (isset($filters['created_by']) && $filters['created_by']) {
                    $filters['user_id'] = $filters['created_by'];
                }
            } else {
                $filters['user_id'] = $this->LoggedUser['user_id'];
            }

            $filters['not_role_id'] = $modelAuthRole->getRoleIdByName('contractor');
            $data = $modelUser->getAll($filters);

            //
            // get all booking Statuses and send to view
            //
            $bookingStatuses = $modelBookingStatus->getAll();
            $this->view->bookingStatuses = $bookingStatuses;

            foreach ($data as &$row) {

                //
                // get all booking counts
                //
                $created_by_filters = array();
                $created_by_filters['created_by'] = $row['user_id'];
                $created_by_filters['booking_start_between'] = $filters['booking_start_between'];
                $created_by_filters['booking_end_between'] = $filters['booking_end_between'] ? $filters['booking_end_between'] . ' 23:59:59' : '';
				///By Islam, get bookings which have approved payments
				$created_by_filters['has_approved_payment'] = 1;
				
                $row['total_booking'] = $modelBooking->getBookingCount($created_by_filters);
                $row['total_complaint'] = $modelComplaint->getComplaintCount($created_by_filters);

                //
                // get total estimate
                //
                $total_estimates_filters = array();
                $total_estimates_filters['convert_status'] = 'estimate';
                $total_estimates_filters['created_by'] = $row['user_id'];
                $total_estimates_filters['booking_start_between'] = $filters['booking_start_between'];
                $total_estimates_filters['booking_end_between'] = $filters['booking_end_between'] ? $filters['booking_end_between'] . ' 23:59:59' : '';
				///By Islam, get bookings which have approved payments
				$total_estimates_filters['has_approved_payment'] = 1;
				
                $row['total_estimates'] = $modelBooking->total($total_estimates_filters);

                //
                // get total invoice
                //
                $total_invoices_filters = array();
                $total_invoices_filters['convert_status'] = 'invoice';
                $total_invoices_filters['created_by'] = $row['user_id'];
                $total_invoices_filters['booking_start_between'] = $filters['booking_start_between'];
                $total_invoices_filters['booking_end_between'] = $filters['booking_end_between'] ? $filters['booking_end_between'] . ' 23:59:59' : '';
				///By Islam, get bookings which have approved payments
				$total_invoices_filters['has_approved_payment'] = 1;
                $row['total_invoices'] = $modelBooking->total($total_invoices_filters);

                //
                // get total invoice
                //
                $bookingStatusesArray = array();
                $bookingStatusesArray = $bookingStatuses;
                foreach ($bookingStatusesArray as &$bookingStatus) {
                    $total_booking_statuses_filters = array();
                    $total_booking_statuses_filters['created_by'] = $row['user_id'];
                    $total_booking_statuses_filters['status'] = $bookingStatus['booking_status_id'];
                    $total_booking_statuses_filters['booking_start_between'] = $filters['booking_start_between'];
                    $total_booking_statuses_filters['booking_end_between'] = $filters['booking_end_between'] ? $filters['booking_end_between'] . ' 23:59:59' : '';

					///By Islam, get bookings which have approved payments
					$total_booking_statuses_filters['has_approved_payment'] = 1;
					
                    $bookingStatus['total'] = $modelBooking->getBookingCount($total_booking_statuses_filters);
                }
                $row['booking_statuses'] = $bookingStatusesArray;
            }
        }

        $this->view->data = $data;
        $this->view->filters = $filters;
    }

}