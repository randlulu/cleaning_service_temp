<?php

class Subscription_Form_CompaniesContact extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('Companies');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $company = (isset($options['company']) ? $options['company'] : '');
        $countryId = (isset($options['country_id']) ? $options['country_id'] : 0);

        $router = Zend_Controller_Front::getInstance()->getRouter();

   
        $companyWebsite = new Zend_Form_Element_Text('company_website');
        $companyWebsite->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($company['company_website']) ? $company['company_website'] : ''));

        $companyEnquiriesEmail = new Zend_Form_Element_Text('company_enquiries_email');
        $companyEnquiriesEmail->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->addValidator(new Zend_Validate_EmailAddress())
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($company['company_enquiries_email']) ? $company['company_enquiries_email'] : ''));
        //->setErrorMessages(array('Required' => 'Please enter the enquiries email', 'EmailAddress' => 'Invalid email Address'));

        $companyInvoicesEmail = new Zend_Form_Element_Text('company_invoices_email');
        $companyInvoicesEmail->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->addValidator(new Zend_Validate_EmailAddress())
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($company['company_invoices_email']) ? $company['company_invoices_email'] : ''));

        $companyAccountsEmail = new Zend_Form_Element_Text('company_accounts_email');
        $companyAccountsEmail->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->addValidator(new Zend_Validate_EmailAddress())
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($company['company_accounts_email']) ? $company['company_accounts_email'] : ''));


        $companyComplaintsEmail = new Zend_Form_Element_Text('company_complaints_email');
        $companyComplaintsEmail->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->addValidator(new Zend_Validate_EmailAddress())
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($company['company_complaints_email']) ? $company['company_complaints_email'] : ''));


        $companyMobile1 = new Zend_Form_Element_Text('company_mobile1');
        $companyMobile1->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($company['company_mobile1']) ? $company['company_mobile1'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the company mobile'));


        $companyMobile2 = new Zend_Form_Element_Text('company_mobile2');
        $companyMobile2->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($company['company_mobile2']) ? $company['company_mobile2'] : ''));

        $companyMobile3 = new Zend_Form_Element_Text('company_mobile3');
        $companyMobile3->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($company['company_mobile3']) ? $company['company_mobile3'] : ''));
        //->setErrorMessages(array('Required' => 'Please enter the company mobile'));


        $companyPhone1 = new Zend_Form_Element_Text('company_phone1');
        $companyPhone1->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($company['company_phone1']) ? $company['company_phone1'] : ''));
        //->setErrorMessages(array('Required' => 'Please enter the company phone number'));


        $companyPhone2 = new Zend_Form_Element_Text('company_phone2');
        $companyPhone2->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($company['company_phone2']) ? $company['company_phone2'] : ''));
        //->setErrorMessages(array('Required' => 'Please enter the company phone number'));

        $companyPhone3 = new Zend_Form_Element_Text('company_phone3');
        $companyPhone3->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($company['company_phone3']) ? $company['company_phone3'] : ''));
        //->setErrorMessages(array('Required' => 'Please enter the company phone number'));


        $companyFax = new Zend_Form_Element_Text('company_fax');
        $companyFax->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($company['company_fax']) ? $company['company_fax'] : ''));
        //->setErrorMessages(array('Required' => 'Please enter the company Fax'));



        $companyEmergencyPhone = new Zend_Form_Element_Text('company_emergency_phone');
        $companyEmergencyPhone->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($company['company_emergency_phone']) ? $company['company_emergency_phone'] : ''));
        //->setErrorMessages(array('Required' => 'Please enter the company emergency phone'));

      
    


        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));

        $this->addElements(array( $companyWebsite,  $companyEnquiriesEmail, $companyInvoicesEmail, $companyAccountsEmail, $companyComplaintsEmail, $companyMobile1, $companyMobile2, $companyMobile3, $companyPhone1, $companyPhone2, $companyPhone3, $companyFax, $companyEmergencyPhone , $button));
        $this->setMethod('post');
        if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $company['company_id']), 'completecompanypanel'));
        } else {
            $this->setAction($router->assemble(array(), 'completecompanypanel'));
        }
    }

}

