<?php

class Subscription_Form_CompaniesInformation extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('Companies');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $company = (isset($options['company']) ? $options['company'] : '');
        $countryId = (isset($options['country_id']) ? $options['country_id'] : 0);

        $router = Zend_Controller_Front::getInstance()->getRouter();

        $companyName = new Zend_Form_Element_Text('company_name');
        $companyName->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($company['company_name']) ? $company['company_name'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the company name'));


        $companyBusinessName = new Zend_Form_Element_Text('company_business_name');
        $companyBusinessName->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($company['company_business_name']) ? $company['company_business_name'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the company business name'));


        $companyAbn = new Zend_Form_Element_Text('company_abn');
        $companyAbn->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($company['company_abn']) ? $company['company_abn'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the company ABN'));



        $companyAcn = new Zend_Form_Element_Text('company_acn');
        $companyAcn->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($company['company_acn']) ? $company['company_acn'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the company ACN'));

        $companyGst = new Zend_Form_Element_Checkbox('company_gst');
        $companyGst->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'checkbox_field'))
                ->setValue((!empty($company['company_gst']) ? $company['company_gst'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the company GST'));


        $companyGstDateRegistered = new Zend_Form_Element_Text('company_gst_date_registered');
        $companyGstDateRegistered->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control' , 'id'=>'company_gst_date_registered'))
                ->setAttrib('readonly', TRUE)
                ->setValue((!empty($company['company_gst_date_registered']) ? date('d-m-Y', $company['company_gst_date_registered']) : ''))
                ->setErrorMessages(array('Required' => 'Please enter the company GST Date Registered'));


     


        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));

        $this->addElements(array($companyName, $companyBusinessName, $companyAbn, $companyAcn, $companyGst, $companyGstDateRegistered,  $button));
        $this->setMethod('post');
        if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $company['company_id']), 'completecompanypanel'));
        } else {
            $this->setAction($router->assemble(array(), 'completecompanypanel'));
        }
    }

}

