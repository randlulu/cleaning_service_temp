<?php

class Subscription_Form_PlanType extends Zend_Form
{

    public function __construct($options = null) {
        parent::__construct($options);

    	 $this->setName('Plan Types');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $planType = (isset($options['planType']) ? $options['planType'] : '');
        $router = Zend_Controller_Front::getInstance()->getRouter();


		$view_plan_type_name = new Zend_Form_Element_Text('view_plan_type_name');
        $view_plan_type_name->setDecorators(array('ViewHelper'))
        		->setRequired()
                ->addDecorator('Errors', array('class' => 'errors'))
				->setValue((!empty($planType['view_plan_type_name']) ? $planType['view_plan_type_name'] : ''))
                ->setAttribs(array('class' => 'form-control'));

        // $status = new Zend_Form_Element_Select('status');
        // $status->setDecorators(array('ViewHelper'))
        //         ->addDecorator('Errors', array('class' => 'errors'))
        //         ->setRequired()
        //         ->setAttribs(array('class' => 'select_field form-control'))
        //         ->setValue((!empty($planType['status']) ? $planType['status'] : ''))
        //         ->setErrorMessages(array('Required' => 'Please select plan status.'));\
        // $status->addMultiOption('active', 'Active');
        // $status->addMultiOption('notactive', 'Not Active');

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'button btn btn-primary'));

        $this->addElements(array($view_plan_type_name,$button));


        $this->setMethod('post');
        if ('update' == $mode) {
            $this->setAction($router->assemble(array('plan_type_id' => $planType['plan_type_id']), 'editPlanType'));
        } else {
            $this->setAction($router->assemble(array(), 'addPlanType'));
        }

    }


}