<?php

class Model_Employee extends Zend_Db_Table_Abstract {

    protected $_name = 'employee';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null, $limit = 0, $perPage = 0, $currentPage = 0) {
        $select = $this->getAdapter()->select();
        $select->from(array('emp' => $this->_name));
        $select->order($order);
        $select->distinct();

        // $filters['company_id'] = CheckAuth::getCompanySession();

        if ($filters) {
            if (!empty($filters['keywords'])) {
                $keywords = $this->getAdapter()->quote('%' . $filters['keywords'] . '%');
                $select->where("username LIKE {$keywords}");
            }
            if (!empty($filters['employee_id'])) {
                $select->where("emp.employee_id = {$filters['employee_id']}");
            }
            if (!empty($filters['active'])) {
                $select->where("emp.active = '{$filters['active']}'");
            }
            if (!empty($filters['not_username'])) {
                $select->where("emp.username != '{$filters['not_username']}'");
            }
    //         if (isset($filters['roles']) && !empty($filters['roles'])) {
                
				// //$rolesIds = array(2, 7, 8);
				// $select->where('u.role_id IN (?)', $filters['roles']);
                
    //             if (!empty($filters['logged_user_id'])) {
    //                 $select->where("u.user_id != '{$filters['logged_user_id']}'");
    //             } 
    //         }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        } elseif ($limit) {
            $select->limit($limit);
        } elseif ($perPage && $currentPage) {
            $select->limit($perPage, ($currentPage - 1) * $perPage);
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table row according to the assigned $id and submited $data
     * 
     * @param $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        $ret_val = parent::update($data, "employee_id = '{$id}'");
        // if ($ret_val) {
        //     $this->calculateProfileCompleteness($id);
        // }
        return $ret_val;
    }

    /**
     * delete table row according to the assigned $id
     * 
     * @param  int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("employee_id = '{$id}'");
    }

    /**
     * get table row according to the assigned $id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from(array('emp' => $this->_name));
        // $select->joinInner(array('ar' => 'auth_role'), 'ar.role_id = u.role_id', array('ar.role_name'));
        $select->where("employee_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }
	
    /**
     * get table rows according to the assigned $username
     * 
     * @param string $username
     * @return array 
     */
    public function getByUsername($username) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("username = '{$username}'");

        return $this->getAdapter()->fetchRow($select);
    }



    /**
     * get table rows according to the assigned $email
     * 
     * @param string $email
     * @return array 
     */
    public function getByEmail($email) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("email = '{$email}'");

        return $this->getAdapter()->fetchRow($select);
    }


    /**
     * get table rows according to the employee role id 
     * as array to choose @return as array 'employee_id' => business_name or the query result
     * 
     * @param tinyint $asArray
     * @return array 
     */
    public function getAllEmployeeAsArray() {
        $select = $this->getAdapter()->select();
        $select->from(array('emp' => $this->_name));

        $results = $this->getAdapter()->fetchAll($select);

        $data = array();
        if ($results) {
            foreach ($results as $result) {
                $data[$result['employee_id']] = $result['display_name'] ? ucwords($result['display_name']) : ucwords($result['username']);
            }
        }
        return $data;
    }




}
