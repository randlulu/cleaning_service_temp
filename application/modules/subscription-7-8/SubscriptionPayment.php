<?php

class Model_SubscriptionPayment extends Zend_Db_Table_Abstract {

    protected $_name = 'subscription_payment';

    public function getAll($filters = array(), $order = null, &$pager = null, $limit = 0, $perPage = 0, $currentPage = 0) {
        $select = $this->getAdapter()->select();
        $select->from(array('py' => $this->_name));
        $select->order($order);
        $select->distinct();

        if ($order) {
            $select->order($order);
        }

        $wheresAndJoins = $this->getWheresAndJoinsByFilters($filters);
        $joinInner = $wheresAndJoins['joinInner'];
        $wheres = $wheresAndJoins['wheres'];
        $groupBy = $wheresAndJoins['groupBy'];

        if ($wheres) {
            foreach ($wheres as $where) {
                $select->where($where);
            }
        }

        if ($joinInner) {
            foreach ($joinInner as $inner) {
                $select->joinInner($inner['name'], $inner['cond'], $inner['cols']);
            }
        }

        if ($groupBy) {
            foreach ($groupBy as $group) {
                $select->group($group);
            }
        }


        if ($pager) {
            if (!empty($filters['perPage'])) {
                $select->limit($filters['perPage'], ($pager->currentPage - 1) * $filters['perPage']);
            } else {
                $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            }

            $pager->dbSelect = $select;
        } elseif ($perPage && $currentPage) {
            $select->limit($perPage, ($currentPage - 1) * $perPage);
        } else {
            if (!empty($filters['perPage'])) {
                $select->limit($filters['perPage']);
            }
        }

        return $this->getAdapter()->fetchAll($select);
    }

    public function getWheresAndJoinsByFilters($filters) {

        $select = $this->getAdapter()->select();
        $select->from(array('py' => $this->_name));

        $wheres = array();
        $joinInner = array();
        $groupBy = array();

        if ($filters) {
            
        }

        return array('wheres' => $wheres, 'joinInner' => $joinInner, 'groupBy' => $groupBy);
    }

    // get table row according to the assigned id  
    public function getPaymentByAccountID($account_id) {
        $id = (int) $account_id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order('subscription_payment_id DESC');
        $select->where("account_id = '{$id}'");
        return $this->getAdapter()->fetchRow($select);
    }

    //get all payment accourding to period for specific account_id..
    public function getPaymentByAccountIDAndPeriod($account_id, $from, $to) {
        $id = (int) $account_id;
        $select = $this->getAdapter()->select();
        $select->from(array('py' => $this->_name));
        $select->order('id DESC');
        $select->joinInner(array('acc' => 'account'), 'py.account_id = acc.id', '');
        $select->where("py.account_id = '{$id}'");
        $select->where("py.created between '" . strtotime($from) . "' and '" . strtotime($to) . "'");
        return $this->getAdapter()->fetchRow($select);
    }

    //get payment by ID
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("subscription_payment_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

//    //this function calculate the total amount that should be paid by user when he want to change his plan.......
//    public function calculateTotalAmountChangePlan($accountId, $newPlanId = 0, $oldPlanId = 0) {
//        //select all records related to account.......
//        $account = new Model_Account();
//        $accountInfo = $account->getById($accountId);
//        $company = $account->getCompanyById($accountInfo['company_id']);
//        $newplan = $account->getPlanById($newPlanId);
//        $oldPlan = $account->getPlanById($oldPlanId);
//        $users = $account->getUsersByCompanyId($company['company_id']);
//
//        $total_amount = '';
//        //if subscriber in trial or overdue mode and want to pay only for the registered user......
//        if ((strcmp($accountInfo['account_status'], 'Subscriber-trial') == 0)) {
//            if ($newPlanId != 0) {
//                $total_amount = (count($users) ) * $plan['charge_amount'];
//                $data = array(
//                    'max_users' => $plan['max_users'],
//                    'name' => $plan['name'],
//                    'charge_amount' => $plan['charge_amount'],
//                    'totalAmount' => $total_amount,
//                    'plan_id' => $plan['plan_id']
//                );
//            }
//
//            return $data;
//
//            //if subscriber in trial or paid mode and want to pay for registered user and the added user......
//        } elseif (strcmp($accountInfo['account_status'], 'Subscriber-paid') == 0) {
//            $difference = time() - strtotime($accountInfo['from']);
//            if ($difference < 0) {
//                $difference = 0;
//            }
//            $nbrOfDay = floor($difference / 60 / 60 / 24);
//
//            //$positive_total_amount = (count($users) * ($nbrOfDay - 1) * $newplan['charge_amount']) / 30;
//            $total_amount = count($users) * $newplan['charge_amount'];
//            //$total_amount = $total - $positive_total_amount;
//
//            $data = array(
//                'max_users' => $newplan['max_users'],
//                'name' => $newplan['name'],
//                'charge_amount' => $newplan['charge_amount'],
//                'totalAmount' => $total_amount,
//                'plan_id' => $newplan['plan_id']
//            );
//            return $data;
//        }
//    }
    //calculate total amount that should payed by e-way
//    public function calculateTotalAmount($accountId, $nbrUser = 0) {
//
//        //select all records related to account.......
//        $account = new Model_Account();
//        $accountInfo = $account->getById($accountId);
//        $company = $account->getCompanyById($accountInfo['company_id']);
//        $plan = $account->getPlanById($accountInfo['plan_id']);
//        $users = $account->getUsersByCompanyId($company['company_id']);
//
//        $total_amount = '';
//        //if subscriber in trial or overdue mode and want to pay only for the registered user......
//        if ((strcmp($accountInfo['account_status'], 'Subscriber-trial') == 0 || strcmp($accountInfo['account_status'], 'Overdue') == 0) && $nbrUser == 0) {
//            $total_amount = count($users) * $plan['charge_amount'];
//            //if subscriber in trial or overdue mode and want to pay for registered user and the added user......
//        } elseif ((strcmp($accountInfo['account_status'], 'Subscriber-trial') == 0 || strcmp($accountInfo['account_status'], 'Overdue') == 0) && $nbrUser != 0) {
//            $nbrOfusersForPaying = count($users) + $nbrUser;
//            $total_amount = $nbrOfusersForPaying * $plan['charge_amount'];
//            //if subscriber in paied mode and want to pay......    
//        } elseif (strcmp($accountInfo['account_status'], 'Subscriber-paid') == 0) {
//            $difference = strtotime($accountInfo['to']) - time();
//            if ($difference < 0) {
//                $difference = 0;
//            }
//            $nbrOfDay = floor($difference / 60 / 60 / 24);
//
//            $total_amount = ($nbrUser * ($nbrOfDay) * $plan['charge_amount']) / 30;
//        }
//        return $total_amount;
//    }


    public function addUserTotalAmount($accountId, $nbrUser = 0) {

        //select all records related to account.......
        $account = new Model_Account();
        $accountInfo = $account->getById($accountId);
        $company = $account->getCompanyById($accountInfo['company_id']);
        $plan = $account->getPlanById($accountInfo['plan_id']);
        $users = $account->getUsersByCompanyId($company['company_id']);

        $total_amount = '';
        //if subscriber in trial or overdue mode and want to pay only for the registered user......
        if ((strcmp($accountInfo['account_status'], 'Subscriber-trial') == 0 || strcmp($accountInfo['account_status'], 'Overdue') == 0) && $nbrUser == 0) {
            $return['total_amount'] = count($users) * $plan['charge_amount'];
            //if subscriber in trial or overdue mode and want to pay for registered user and the added user......
        } elseif ((strcmp($accountInfo['account_status'], 'Subscriber-trial') == 0 || strcmp($accountInfo['account_status'], 'Overdue') == 0) && $nbrUser != 0) {

            $return['total_amount'] = $nbrUser * $plan['charge_amount'];
            //if subscriber in paied mode and want to pay......    
        } elseif (strcmp($accountInfo['account_status'], 'Subscriber-paid') == 0) {
            $difference = strtotime($accountInfo['to']) - time();
            if ($difference < 0) {
                $difference = 0;
            }
            $nbrOfDay = floor($difference / 60 / 60 / 24);

            $total_amount = ($nbrUser * ($nbrOfDay) * $plan['charge_amount']) / 30;
            // $return['nbrOfDay'] = $nbrOfDay;
        }
        return $total_amount;
    }

    public function validateTotalAmount($accountId, $nbrUser = 0) {

        //select all records related to account.......
        $account = new Model_Account();
        $accountInfo = $account->getById($accountId);
        $company = $account->getCompanyById($accountInfo['company_id']);
        $plan = $account->getPlanById($accountInfo['plan_id']);
        $users = $account->getUsersByCompanyId($company['company_id']);

        if($nbrUser > $plan['max_users']){
            return 'faild' ;
        }

        $total_amount = '';
        //if subscriber in trial or overdue mode and want to pay only for the registered user......
        if ((strcmp($accountInfo['account_status'], 'Subscriber-trial') == 0 || strcmp($accountInfo['account_status'], 'Overdue') == 0) && $nbrUser == 0) {
            $return['total_amount'] = count($users) * $plan['charge_amount'];
            //if subscriber in trial or overdue mode and want to pay for registered user and the added user......
        } elseif ((strcmp($accountInfo['account_status'], 'Subscriber-trial') == 0 || strcmp($accountInfo['account_status'], 'Overdue') == 0) && $nbrUser != 0) {

            $return['total_amount'] = $nbrUser * $plan['charge_amount'];
            //if subscriber in paied mode and want to pay......    
        } elseif (strcmp($accountInfo['account_status'], 'Subscriber-paid') == 0) {
            $difference = strtotime($accountInfo['to']) - time();
            if ($difference < 0) {
                $difference = 0;
            }
            $nbrOfDay = floor($difference / 60 / 60 / 24);

            $return['total_amount'] = ($nbrUser * ($nbrOfDay) * $plan['charge_amount']) / 30;
            $return['nbrOfDay'] = $nbrOfDay;
        }
        return $return;
    }

    public function calculateTotalAmountAddUserChangePlan($accountId, $newPlanId = 0, $oldPlanId = 0, $nbrUserToAdd = 0) {

        //select all records related to account.......
        $account = new Model_Account();
        $accountInfo = $account->getById($accountId);
        $company = $account->getCompanyById($accountInfo['company_id']);
        $newplan = $account->getPlanById($newPlanId);
        $oldPlan = $account->getPlanById($oldPlanId);
        $users   = $account->getUsersByCompanyId($company['company_id']);
        $total_amount = '';
        $data = array();

        if (strcmp($accountInfo['account_status'], 'Subscriber-trial') == 0) {


            $stop_date = new DateTime($accountInfo['trial_end_date']);
            $stop_date->format('Y-m-d');
            $stop_date->modify('+1 day');
            $end_date = new DateTime($accountInfo['trial_end_date']);
            $end_date->format('Y-m-d');
            $end_date->modify('+1 day +1 month');
            $from = $stop_date->format('Y-m-d');
            $to = $end_date->format('Y-m-d');



            //if subscriber want to add users to the exist plan........
            if ($nbrUserToAdd != 0 && $newPlanId == 0) {
                $total_amount = ($nbrUserToAdd * $oldPlan['charge_amount']) + (count($users) * $oldPlan['charge_amount']);
                $data = array(
                    'max_users' => $oldPlan['max_users'],
                    'name' => $oldPlan['name'],
                    'charge_amount' => $oldPlan['charge_amount'],
                    'totalAmount' => $total_amount,
                    'plan_id' => $oldPlan['plan_id'],
                    'from' => $from,
                    'to' => $to,
                    'period' => 'month' ,
                    'reason' => 'premium'
                );
            }
            //if subscriber want to change plan without adding users... 
            elseif ($nbrUserToAdd == 0 && $newPlanId != 0) {
                $total_amount = count($users) * $newplan['charge_amount'];
                $data = array(
                    'max_users' => $newplan['max_users'],
                    'name' => $newplan['name'],
                    'charge_amount' => $newplan['charge_amount'],
                    'totalAmount' => $total_amount,
                    'plan_id' => $newplan['plan_id'],
                    'from' => $from,
                    'to' => $to,
                    'period' => 'month',
                    'reason' => 'premium'
                );

                //if subscriber want to add users to new plan    
            } elseif ($nbrUserToAdd != 0 && $newPlanId != 0) {
                $total_amount = count($users) * $newplan['charge_amount'] + $nbrUserToAdd * $newplan['charge_amount'];
                $data = array(
                    'max_users' => $newplan['max_users'],
                    'name' => $newplan['name'],
                    'charge_amount' => $newplan['charge_amount'],
                    'totalAmount' => $total_amount,
                    'plan_id' => $newplan['plan_id'],
                    'from' => $from,
                    'to' => $to,
                    'period' => 'month' ,
                    'reason' => 'premium'
                );

                //if subscriber want to pay only to the existance users and plan(Onload event) .......    
            } elseif ($nbrUserToAdd == 0 && $newPlanId == 0) {
                $total_amount = count($users) * $oldPlan['charge_amount'];
                $data = array(
                    'max_users' => $oldPlan['max_users'],
                    'name' => $oldPlan['name'],
                    'charge_amount' => $oldPlan['charge_amount'],
                    'totalAmount' => $total_amount,
                    'plan_id' => $oldPlan['plan_id'],
                    'from' => $from,
                    'to' => $to,
                    'period' => 'month' ,
                    'reason' => 'premium'
                );
            }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////            
        } else {
            //if subscriber want to add users to the exist plan........
            if ($nbrUserToAdd != 0 && $newPlanId == 0) {

                $toEndPeriodObject = new DateTime($accountInfo['to']);
                $toEndPeriodString = $toEndPeriodObject->format('Y-m-d');
                $toEndPeriodTime = strtotime($toEndPeriodString);

                $TodayString = date("Y-m-d");
                $TodayTime = strtotime($TodayString);


                $difference = $toEndPeriodTime - $TodayTime;
                if ($difference < 0) {
                    $difference = 0;
                }

                $nbrOfDay = floor($difference / 60 / 60 / 24);
                $total_amount = ($nbrUserToAdd * ($nbrOfDay) * $oldPlan['charge_amount']) / 30;
                $data = array(
                    'max_users' => $oldPlan['max_users'],
                    'name' => $oldPlan['name'],
                    'charge_amount' => $oldPlan['charge_amount'],
                    'totalAmount' => $total_amount,
                    'plan_id' => $oldPlan['plan_id'],
                    'from' => $TodayString,
                    'to' => $toEndPeriodString,
                    'period' => $nbrOfDay,
                    'reason' => '0'
                );
            }
            //if subscriber want to change plan without adding users... 
            elseif ($nbrUserToAdd == 0 && $newPlanId != 0) {

                $fromStartPeriodObject = new DateTime($accountInfo['from']);
                $fromStartPeriodString = $fromStartPeriodObject->format('Y-m-d');
                $fromStartPeriodTime = strtotime($fromStartPeriodString);

                $TodayString = date("Y-m-d");
                $TodayTime = strtotime($TodayString);

                $to = date("Y-m-d", mktime(0, 0, 0, date("n", time()) + 1, date("j", time()), date("Y", time())));

                //$TodayAfteroneMonth = $TodayString->modify('+1 day +1 month');

                $difference = $TodayTime - $fromStartPeriodTime;
                if ($difference < 0) {
                    $difference = 0;
                }

                $nbrOfDay = floor($difference / 60 / 60 / 24);
                $positive_total_amount = (count($users) * (30 - $nbrOfDay) * $oldPlan['charge_amount']) / 30;
                $total = count($users) * $newplan['charge_amount'];
                $total_amount = $total - $positive_total_amount;

                $data = array(
                    'max_users' => $newplan['max_users'],
                    'name' => $newplan['name'],
                    'charge_amount' => $newplan['charge_amount'],
                    'totalAmount' => $total_amount,
                    'plan_id' => $newplan['plan_id'],
                    'from' => $TodayString,
                    'to' => $to,
                    'period' => 'month' ,
                    'reason' => 'change-plan'
                );

                //if subscriber want to add users to new plan    
            } elseif ($nbrUserToAdd != 0 && $newPlanId != 0) {

                $fromStartPeriodObject = new DateTime($accountInfo['from']);
                $fromStartPeriodString = $fromStartPeriodObject->format('Y-m-d');
                $fromStartPeriodTime = strtotime($fromStartPeriodString);

                $TodayString = date("Y-m-d");
                $TodayTime = strtotime($TodayString);

                $to = date("Y-m-d", mktime(0, 0, 0, date("n", time()) + 1, date("j", time()), date("Y", time())));


                $difference = $TodayTime - $fromStartPeriodTime;

                //$difference = time() - strtotime($accountInfo['from']);
                if ($difference < 0) {
                    $difference = 0;
                }

                $nbrOfDay = floor($difference / 60 / 60 / 24);
                $positive_total_amount = (count($users) * (30 - $nbrOfDay) * $oldPlan['charge_amount']) / 30;
                $totalForExistingUserNewPlan = count($users) * $newplan['charge_amount'];
                $totalAmountBeforeAddingUser = $totalForExistingUserNewPlan - $positive_total_amount;
                $total_amount = ($nbrUserToAdd * $newplan['charge_amount']) + $totalAmountBeforeAddingUser;

                $data = array(
                    'max_users' => $newplan['max_users'],
                    'name' => $newplan['name'],
                    'charge_amount' => $newplan['charge_amount'],
                    'totalAmount' => $total_amount,
                    'plan_id' => $newplan['plan_id'],
                    'from' => $TodayString,
                    'to' => $to,
                    'period' => 'month' ,
                    'reason' => 'change-plan'
                );
            }
            //if subscriber want to pay only to the existance users and plan(Onload event) .......   
            elseif ($nbrUserToAdd == 0 && $newPlanId == 0) {
                $data = array(
                    'max_users' => $oldPlan['max_users'],
                    'name' => $oldPlan['name'],
                    'charge_amount' => $oldPlan['charge_amount'],
                    'totalAmount' => 0,
                    'plan_id' => $oldPlan['plan_id'] ,
                    'reason' => '0'                 
                );
            }
        }
        return $data;
    }

    public function expireDatetoPremium($id) {


        $planModel = new Model_Plan();
        $accountModel = new Model_Account();
        $account = $accountModel->getById($id);
        $dataPlan = $planModel->getById($account['plan_id']);


        if ($dataPlan['charge_period'] == 'monthly') {

            $stop_date = new DateTime($account['trial_end_date']);
            $stop_date->format('Y-m-d');
            $stop_date->modify('+1 month');
            $to = $stop_date->format('Y-m-d');
            $return['to'] = $to;
            $too = strtotime($to);
            $from = strtotime($account['trial_end_date']);
            $datediff = $too - $from;
            $return['period'] = floor($datediff / (60 * 60 * 24));
            return $return;
        } else if ($dataPlan['charge_period'] == 'annually') {

            $stop_date = new DateTime($account['trial_end_date']);
            $stop_date->format('Y-m-d');
            $stop_date->modify('+1 year');
            $to = $stop_date->format('Y-m-d');
            return $to;
        }


    }

}
