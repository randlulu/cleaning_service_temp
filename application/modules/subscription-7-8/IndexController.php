<?php

class Subscription_IndexController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {

      //  parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
        $this->_helper->layout->setLayout('layout_subscription');
    }

    /**
     * Items list action
     */
    public function indexAction() {

         $updateCheck =  $this->_getParam('update');
        // var_dump($updateCheck); exit;
        if($updateCheck == 'update'){
                $accountID = (int) $this->_getParam('id');
                $accountModel = new Model_Account();
                $account = $accountModel->getById($accountID);
                if (!$account or $account['account_status'] != 'Non-subscriber') die(' no account'); 
               // var_dump($account); exit;
                $modelUser  = new Model_User();
                $user = $modelUser->getById1($account['created_by']);
                 // var_dump($user) ; exit;
                if(!$user) die('nooo user fff');;
                // var_dump($account) ; exit;
                $emailAccount = $user['email1'];

                //var_dump($emailAccount) ; exit;
               // $emailAccount = $this->request->getParam('emailAccount');
                $companiesObj = new Model_Companies();
                $comp = $companiesObj ->getById($account['company_id']);

                $company_name = $comp['company_name'];
                $form = new Subscription_Form_newaccount( array('company_name' =>$company_name ,'username_email' =>$emailAccount ,'mode' =>'update' 
                                                              ,'idAccount' =>$account['id']));
               $request = $this->getRequest();

                 if ($this->getRequest()->isPost()) {

                         if ($form->isValid($request->getPost())) {
                            $roleuser = new Model_AuthRole();
                            $roleadmin = $roleuser->getRoleIdByName('account_admin');

                            $company_name = $this->request->getParam('company_name');
                            $username_email = $this->request->getParam('username_email');
                            $password = $this->request->getParam('password');
                            $password_confirm = $this->request->getParam('password_confirm');
                            $mode = $this->request->getParam('mode');
                            $modelUser = new Model_User();


    
                             $accountID  = $this->request->getParam('idAccount');
                             $accountModel = new Model_Account();
                             $account = $accountModel->getById($accountID);
                             $modelUser = new Model_User();
                            // $user = $modelUser->getById($account['created_by']);



                     if ($password == $password_confirm) {

                            $data = array(
                                'email1' => $username_email,
                                'password' => sha1($password),
                                'user_code' => sha1($username_email),
                                'created' => time(),
                                'role_id' => (isset($roleadmin) ? $roleadmin : 10 ), // set admin to this account  // get by name is better 
                                'active' => 'FALSE'
                            );

                            $hash_code = sha1($username_email);
                            $userID = $modelUser->updateById($account['created_by'] , $data );   /// inser user admin for new account 

                            $companiesObj = new Model_Companies();

                            $dataCompany = array(
                                'company_name' => $company_name
                            );


                            $companyID = $companiesObj->updateById($account['company_id'] ,$dataCompany );


                          // $this->sendEmailNewAccount($username_email , $hash_code);

                   
                            //////////////////////////////////////////////////////////////////////////////////////////////////       
                            ///////////////////////////////////  redirect with success ///////////////////////////////////////
                            $this->view->assign('data' , 'true');
                            $this->view->assign('email' , $username_email);
                            $this->view->assign('idAccount' , $accountID);
                            return;
                        } else {
                            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "password dont match"));
                            $this->_redirect($this->router->assemble(array('update' => 'update' ,'id' =>$account['id']), 'signupupdate'));
                        }
                     }
                 }
               
                  
        }
       else{

        $plan_id = (int) $this->_getParam('id');
        if (!is_int($plan_id) | $plan_id <= 0) {     // check is int id of plan
            exit;
        }

        $PlanModel = new Model_Plan();
        $get_dataPlan = $PlanModel->getById($plan_id);
        if (!$get_dataPlan) {            ///  check if plan id is exist in table plan
            exit;
        }

        $free_time = $get_dataPlan["trial_period"];
        $request = $this->getRequest();
        $form = new Subscription_Form_newaccount();

     }
  
        if ($this->getRequest()->isPost()) {

            
            $isFroresend = $this->request->getParam('isFroresend');
            if($isFroresend == 'goResendform'){

                $emailAccount = $this->request->getParam('emailAccount');
                $modelUser  = new Model_User();
                $user = $modelUser->getByEmail($emailAccount);
                 // var_dump($user) ; exit;
                if (!$user) exit;
                $accountModel = new Model_Account();
                $account = $accountModel->getByCreatedBy($user['user_id']);
                // var_dump($account) ; exit;
                if (!$account or $account['account_status'] != 'Non-subscriber') die(' no account'); 
                $companiesObj = new Model_Companies();
                $comp = $companiesObj ->getById($account['company_id']);

                $company_name = $comp['company_name'];
                $form = new Subscription_Form_newaccount( array('company_name' =>$company_name ,'username_email' =>$emailAccount ,'mode' =>'update' 
                                                              ,'idAccount' =>$account['id']));
               
                  
            }
            else{



            if ($form->isValid($request->getPost())) {

            	$roleuser = new Model_AuthRole();
            	$roleadmin = $roleuser->getRoleIdByName('account_admin');

                $end_trial = date('Y-m-d H:i:s', strtotime("+" . $free_time . " days"));   // get end trial date 
                $company_name = $this->request->getParam('company_name');
                $username_email = $this->request->getParam('username_email');
                $password = $this->request->getParam('password');
                $password_confirm = $this->request->getParam('password_confirm');
                $mode = $this->request->getParam('mode');
                $modelUser = new Model_User();


                if ($mode == 'update'){

                  // check security again 


                     $accountID  = $this->request->getParam('idAccount');
                     $accountModel = new Model_Account();
                     $account = $accountModel->getById($accountID);
                     $modelUser = new Model_User();
                    // $user = $modelUser->getById($account['created_by']);



                     if ($password == $password_confirm) {

                            $data = array(
                                'email1' => $username_email,
                                'password' => sha1($password),
                                'user_code' => sha1($username_email),
                                'created' => time(),
                                'role_id' => (isset($roleadmin) ? $roleadmin : 10 ), // set admin to this account  // get by name is better 
                                'active' => 'FALSE'
                            );

                            $hash_code = sha1($username_email);
                            $userID = $modelUser->updateById($account['created_by'] , $data );   /// inser user admin for new account 

                            $companiesObj = new Model_Companies();

                            $dataCompany = array(
                                'company_name' => $company_name
                            );


                            $companyID = $companiesObj->updateById($account['company_id'] ,$dataCompany );


                          // $this->sendEmailNewAccount($username_email , $hash_code);

                   
                            //////////////////////////////////////////////////////////////////////////////////////////////////       
                            ///////////////////////////////////  redirect with success ///////////////////////////////////////
                            $this->view->assign('data' , 'true');
                            $this->view->assign('email' , $username_email);
                            $this->view->assign('idAccount' , $accountID);
                            return;
                        } else {
                            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "password dont match"));
                            $this->_redirect($this->router->assemble(array('update' => 'update' ,'id' =>$account['id']), 'signupupdate'));
                        }
                      


                } 
                else{

                        $user = $modelUser->getByEmail($username_email);

                        if ($user) {

                            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "email is used "));
                            $this->_redirect($this->router->assemble(array(), 'signup'));
                            exit;

                        }

                        if ($password == $password_confirm) {

                            $data = array(
                                'email1' => $username_email,
                                'password' => sha1($password),
                                'user_code' => sha1($username_email),
                                'created' => time(),
                                'role_id' => (isset($roleadmin) ? $roleadmin : 10 ), // set admin to this account  // get by name is better 
                                'active' => 'FALSE'
                            );

                            $hash_code = sha1($username_email);
                            $userID = $modelUser->insert($data);   /// inser user admin for new account 

                            $companiesObj = new Model_Companies();

                            $dataCompany = array(
                                'company_name' => $company_name
                            );


                            $companyID = $companiesObj->insert($dataCompany);

                            $data = array(
                                'company_id' => $companyID,
                                'created_by' => $userID,
                                'account_status' => 'Non-subscriber', //  not active
                                'payment_status' => 0, //  trial time
                                'plan_id' => $plan_id, //  plan id
                                'trial_end_date' => $end_trial,
                                'renew_automatic' => 0, //  not renew automatically
                                'renew_automatic_mode' => 'monthly'
                            );

                            $accountModel = new Model_Account();

                            $accountID = $accountModel->insert($data);

                            $dataCompany = array(
                                'user_id' => $userID,
                                'company_id' => $companyID,
                                'created' => time()
                            );


                            $userCompaniesModel = new Model_UserCompanies();
                            $userCompaniesModel->insert($dataCompany);

                            //////////////////////  end  success add ////////////////////////////////////
                            //////////////////////////////  send email to new user account ////////////////

                           
                           $this->sendEmailNewAccount($username_email , $hash_code);

                   
                            //////////////////////////////////////////////////////////////////////////////////////////////////		 
                            ///////////////////////////////////  redirect with success ///////////////////////////////////////
                            $this->view->assign('data' , 'true');
                            $this->view->assign('email' , $username_email);
                            $this->view->assign('idAccount' , $accountID);
                            return;
                        } else {
                            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "password dont match"));
                            $this->_redirect($this->router->assemble(array(), 'signup'));
                        }
                     }
                }
            }
        }

        $id = $this->_getParam('id');
        $this->view->form = $form;
    }

    public function sentemailAction() {
        
    }

    public function checkvalidateaccountAction() {


        if (Zend_Auth::getInstance()->hasIdentity()) {
            CheckAuth::logout();
        }

        $email = $this->_getParam('email');
        $hashcode = $this->_getParam('hashcode');


        $userModel = new Model_User();
        $user = $userModel->getByEmail($email);




        /*  by mohammed Mukhaimar
          check if user  is active befor
          if not  check if hash code is equal in table
          then active user
          then check if this user is create account
          if yes i will active the account in account table
         */



        if ($user) {
            //	var_dump($user); exit;

            if ($user['active'] == 'TRUE') {
                $this->view->assign('data', 'active_prev');
                return;
            } else {

                if ($user['user_code'] == $hashcode) {

                    $data = array(
                        'active' => 'TRUE',
                        'user_code' => ''
                    );

                    $userModel->updateById($user['user_id'], $data);
                    $this->_redirect($this->router->assemble(array(), 'Login'));

                    return;
                }
                exit;
            }
        }

        exit;
    }

    public function getAuthrezed() {
        $authrezed = new Zend_Auth_Adapter_DbTable(Zend_Db_Table::getDefaultAdapter());
        $authrezed->setTableName('user')
                ->setIdentityColumn('email1')
                ->setCredentialColumn('password')
        ;

        return $authrezed;
    }

    public function panelFirstLoginAction() {

        CheckAuth::checkLoggedIn();
        $userr = CheckAuth::getLoggedUser();

        // var_dump(CheckAuth::checkAccountSteps($userr['user_id']));exit;

        if (CheckAuth::checkAccountSteps($userr['user_id']) == 1) {
            $step = 1;
            $this->view->assign('step', $step);
        } else if (CheckAuth::checkAccountSteps($userr['user_id']) == 2) {
            $step = 2;
            $this->view->assign('step', $step);
        } else if (CheckAuth::checkAccountSteps($userr['user_id']) == 3) {
            $step = 3;

            // var_dump($step); exit; 
            $this->view->assign('step', $step);

            $userr = CheckAuth::getLoggedUser();
            $accountModel = new Model_Account();
            $account = $accountModel->getByCreatedBy($userr['user_id']);
            $userCompaniesModel = new Model_UserCompanies();


            /////////////////  chech if it has permission to add new user to his account ////////////////
            ///////////////////////// get max users from paln who choose it before 
            $planModel = new Model_Plan();
            $Plan = $planModel->getByID($account['plan_id']);

            ////// get all users with this account /////////////////////////////////
            $users = $userCompaniesModel->getUsersByCompanyId($account['company_id']);

            if (count($users) == $Plan['max_users']) {
                // move it to 
                $step = 4;
                $this->view->assign('step', $step);
            }
        } else {
            $this->_redirect($this->router->assemble(array(), 'settings'));
        }
        $this->_helper->layout->setLayout('layout');
    }

    /*

      public function panelSecondComAction(){

      CheckAuth::checkLoggedIn();
      $userr =CheckAuth::getLoggedUser();
      if(CheckAuth::checkAccountSteps($userr['user_id']) != 3){
      exit;
      }

      $userr = CheckAuth::getLoggedUser();
      $accountModel = new Model_Account() ;
      $account =  $accountModel->getByCreatedBy($userr['user_id']);
      $userCompaniesModel = new Model_UserCompanies();


      /////////////////  chech if it has permission to add new user to his account ////////////////
      ///////////////////////// get max users from paln who choose it before
      $planModel = new Model_Plan() ;

      $Plan   =  $planModel ->getByID($account['plan_id']);


      ////// get all users with this account /////////////////////////////////
      $users = $userCompaniesModel->getUsersByCompanyId($account['company_id']);
      if(count($users) == $Plan['max_users'] ){

      $this->_redirect($this->router->assemble(array(), 'panellastone'));

      }

      $this->_helper->layout->setLayout('layout');

      }

      public function completeUserPanelAction(){
      CheckAuth::checkLoggedIn();
      $userr =CheckAuth::getLoggedUser();
      if(CheckAuth::checkAccountSteps($userr['user_id']) != 2){
      exit;
      }
      $this->_helper->layout->setLayout('layout');

      }


      public function panelSettingsAction(){
      CheckAuth::checkLoggedIn();
      $this->_helper->layout->setLayout('layout');
      }

     */

    public function completeCompanyAction() {


        CheckAuth::checkLoggedIn();

        $userr = CheckAuth::getLoggedUser();
        $accountModel = new Model_Account();
        $account = $accountModel->getByCreatedBy($userr['user_id']);   // check if user has create account
        $companiesObj = new Model_Companies();
        $company = $companiesObj->getById($account['company_id']);
        $id = $account['company_id'];
        $companyName = $this->request->getParam('company_name');
        $companyBusinessName = $this->request->getParam('company_business_name');
        $companyAbn = $this->request->getParam('company_abn');
        $companyAcn = $this->request->getParam('company_acn');
        $companyGst = $this->request->getParam('company_gst');
        $companyGstDateRegistered = $this->request->getParam('company_gst_date_registered');
        $companyWebsite = $this->request->getParam('company_website');
        $companyEnquiriesEmail = $this->request->getParam('company_enquiries_email');
        $companyInvoicesEmail = $this->request->getParam('company_invoices_email');
        $companyAccountsEmail = $this->request->getParam('company_accounts_email');
        $companyComplaintsEmail = $this->request->getParam('company_complaints_email');
        $companyMobile1 = $this->request->getParam('company_mobile1');
        $companyMobile2 = $this->request->getParam('company_mobile2');
        $companyMobile3 = $this->request->getParam('company_mobile3');
        $companyPhone1 = $this->request->getParam('company_phone1');
        $companyPhone2 = $this->request->getParam('company_phone2');
        $companyPhone3 = $this->request->getParam('company_phone3');
        $companyFax = $this->request->getParam('company_fax');
        $companyEmergencyPhone = $this->request->getParam('company_emergency_phone');
        $companyStreetAddress = $this->request->getParam('company_street_address');
        $companyUnitLotNo = $this->request->getParam('company_unit_lot_no');
        $companyStreetNo = $this->request->getParam('company_street_no');
        $companySuburb = $this->request->getParam('company_suburb');
        $cityId = $this->request->getParam('city_id');
        $countryId = $this->request->getParam('country_id');
        $companyState = $this->request->getParam('company_state');
        $companyPostcode = $this->request->getParam('company_postcode');
        $companyPoBox = $this->request->getParam('company_po_box');

        if (get_config('remove_white_spacing')) {
            $companyMobile1 = preparer_number($companyMobile1);
            $companyMobile2 = preparer_number($companyMobile2);
            $companyMobile3 = preparer_number($companyMobile3);
            $companyPhone1 = preparer_number($companyPhone1);
            $companyPhone2 = preparer_number($companyPhone2);
            $companyPhone3 = preparer_number($companyPhone3);
        }


        //
        // validation
        //

        //
        // init action form
        //
      $form = new Subscription_Form_Companies(array('mode' => 'update', 'company' => $company));


        //
        // handling the updating process


        if ($this->request->isPost()) { // check if POST request method
            //	var_dump('post and will check validate'); exit;
            if ($form->isValid($this->request->getPost())) { // validate form data
                $companiesObj = new Model_Companies();
                $company = $companiesObj->getById($id);

                $data = array(
                    'company_name' => trim($companyName),
                    'company_abn' => $companyAbn,
                    'company_acn' => $companyAcn,
                    'city_id' => $cityId,
                    'company_enquiries_email' => $companyEnquiriesEmail,
                    'company_postcode' => $companyPostcode,
                    'company_street_address' => $companyStreetAddress,
                    'company_unit_lot_no' => $companyUnitLotNo,
                    'company_street_no' => $companyStreetNo,
                    'company_suburb' => $companySuburb,
                    'company_state' => $companyState,
                    'company_po_box' => $companyPoBox,
                    'company_po_box' => $companyPoBox,
                    'company_website' => $companyWebsite,
                    'company_accounts_email' => $companyAccountsEmail,
                    'company_complaints_email' => $companyComplaintsEmail,
                    'company_invoices_email' => $companyInvoicesEmail,
                    'company_phone1' => $companyPhone1,
                    'company_phone2' => $companyPhone2,
                    'company_phone3' => $companyPhone3,
                    'company_mobile1' => $companyMobile1,
                    'company_mobile2' => $companyMobile2,
                    'company_mobile3' => $companyMobile3,
                    'company_gst' => $companyGst,
                    'company_gst_date_registered' => strtotime($companyGstDateRegistered),
                    'company_fax' => $companyFax,
                    'company_emergency_phone' => $companyEmergencyPhone,
                    'company_business_name' => $companyBusinessName
                );
                $success = $companiesObj->updateById($id, $data);

                if ($success) {

                    $return['success'] = true;
                    $return['url'] = 'panel-first-login';
                    echo json_encode($return);
                    exit;
                } else {

                    $return['success'] = true;
                    $return['url'] = 'panel-first-login';
                    echo json_encode($return);
                    exit;
                }
            }
        }


        $this->view->form = $form;
        echo $this->view->render('index/complete-company.phtml');
        exit;
    }

    public function completeAdminAccountAction() {

        CheckAuth::checkLoggedIn();
        $this->_helper->layout->setLayout('layout');

        $userr = CheckAuth::getLoggedUser();
        $id = $userr['user_id'];

        $display_name = $this->request->getParam('display_name');
        $username = $this->request->getParam('username');
        //$password = $this->request->getParam('password');
        //  $roleId = $this->request->getParam('role_id');
        $first_name = $this->request->getParam('first_name');
        $last_name = $this->request->getParam('last_name');
        $email1 = $this->request->getParam('email1');
        $email2 = $this->request->getParam('email2');
        $email3 = $this->request->getParam('email3');
        $systemEmail = $this->request->getParam('systemEmail');

        $internation_key = $this->request->getParam('international_key');
        $mobile_key = $this->request->getParam('mobile_key');
        $phone_key = $this->request->getParam('phone_key');

        $mobile1 = $this->request->getParam('mobile1');
        $mobile2 = $this->request->getParam('mobile2');
        $mobile3 = $this->request->getParam('mobile3');
        $phone1 = $this->request->getParam('phone1');
        $phone2 = $this->request->getParam('phone2');
        $phone3 = $this->request->getParam('phone3');
        $fax = $this->request->getParam('fax');
        $emergencyPhone = $this->request->getParam('emergency_phone');
        $unitLotNumber = $this->request->getParam('unit_lot_number');
        $streetNumber = $this->request->getParam('street_number');
        $streetAddress = $this->request->getParam('street_address');
        $suburb = $this->request->getParam('suburb');
        $state = $this->request->getParam('state');
        $postcode = $this->request->getParam('postcode');
        $po_box = $this->request->getParam('po_box');
        $cityId = $this->request->getParam('city_id');
        $countryId = $this->request->getParam('country_id');
        $companyId = $this->request->getParam('company_id');


        /*         * ******Check if user put phone or mobile Number? and put full format***********IBM */
        if (isset($mobile1) && $mobile1 != "") {
            $mobile1_key = substr($mobile_key, 1, 1);
            $mobile1 = $internation_key . $mobile1_key . $mobile1;
        }
        if (isset($mobile2) && $mobile2 != "") {
            $mobile2_key = substr($mobile_key, 1, 1);
            $mobile2 = $internation_key . $mobile2_key . $mobile2;
        }
        if (isset($mobile3) && $mobile3 != "") {
            $mobile3_key = substr($mobile_key, 1, 1);
            $mobile3 = $internation_key . $mobile3_key . $mobile3;
        }
        if (isset($phone1) && $phone1 != "") {
            $phone1_key = substr($phone_key, 1, 1);
            $phone1 = $internation_key . $phone1_key . $phone1;
        }
        if (isset($phone2) && $phone2 != "") {
            $phone2_key = substr($phone_key, 1, 1);
            $phone2 = $internation_key . $phone2_key . $phone2;
        }
        if (isset($phone3) && $phone3 != "") {
            $phone3_key = substr($phone_key, 1, 1);
            $phone3 = $internation_key . $phone3_key . $phone3;
        }

        if (get_config('remove_white_spacing')) {
            $mobile1 = preparer_number($mobile1);
            $mobile2 = preparer_number($mobile2);
            $mobile3 = preparer_number($mobile3);
            $phone1 = preparer_number($phone1);
            $phone2 = preparer_number($phone2);
            $phone3 = preparer_number($phone3);
        }

        $modelUser = new Model_User();
        $user = $modelUser->getPureDatdById($id);
        // var_dump($user ); exit;

        if (!$user) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect($this->router->assemble(array(), 'completeadminaccount'));
            return;
        }


        $userCompaniesModel = new Model_UserCompanies();
        $userCompanies = $userCompaniesModel->getByUserId($id);

        $form = new Subscription_Form_Useradmin(array('user' => $user, 'country_id' => $countryId, 'state' => $state, 'company_id' => $userCompanies['company_id']));
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'display_name' => $display_name,
                    'username' => $username,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'email1' => $email1,
                    'email2' => $email2,
                    'email3' => $email3,
                    'system_email' => $systemEmail,
                    'mobile1' => $mobile1,
                    'mobile2' => $mobile2,
                    'city_id' => $cityId,
                    'mobile3' => $mobile3,
                    'phone1' => $phone1,
                    'phone2' => $phone2,
                    'phone3' => $phone3,
                    'fax' => $fax,
                    'emergency_phone' => $emergencyPhone,
                    'unit_lot_number' => $unitLotNumber,
                    'street_number' => $streetNumber,
                    'street_address' => $streetAddress,
                    'suburb' => $suburb,
                    'state' => $state,
                    'postcode' => $postcode,
                    'po_box' => $po_box
                );

                $success = $modelUser->updateById($id, $data);

                $userCompaniesModel = new Model_UserCompanies();
                $userCompanies = $userCompaniesModel->getByUserId($id);

                $accountModel = new Model_Account();
                $account = $accountModel->getByCreatedBy($id);   // check if user has create account	
                $dataCompany = array(
                    'user_id' => $id,
                    'company_id' => $account['company_id'],
                    'created' => time()
                );

                if ($userCompanies) {
                    $userCompaniesModel->updateById($userCompanies['id'], $dataCompany);
                } else {
                    $userCompaniesModel->insert($dataCompany);
                }

                if ($success) {
                    //// if sucess  update status of account to complete admin info  and then
                    //// then redirect to panel to add users 




                    $return['success'] = true;
                    $return['url'] = 'panel-first-login';
                    echo json_encode($return);
                    exit;



                    //////////////////////////////////////////////////////////////////////////
                }
            }
        }

        $this->view->form = $form;
        echo $this->view->render('index/complete-admin-account.phtml');
        exit;
    }

    public function addUsersPageAction() {
        CheckAuth::checkLoggedIn();
        //$this->_helper->layout->setLayout('layout');

        $first_name = $this->request->getParam('first_name');
        $last_name = $this->request->getParam('last_name');
        $email = $this->request->getParam('email');



        $form = new Subscription_Form_AddUsersCompany();
        $userr = CheckAuth::getLoggedUser();
        $accountModel = new Model_Account();

        $userModel = new Model_User();
        $userAdmin = $userModel->getById($userr['user_id']);



        // var_dump($userr);exit;
        $account = $accountModel->getByCreatedBy($userr['user_id']);
        $userCompaniesModel = new Model_UserCompanies();



        $accountModel = new Model_Account();
        $account = $accountModel->getByCreatedBy($userr['user_id']);

        if ($account) {
            $data = array(
                'account_status' => 'Subscriber-trial'
            );
           $accountModel->updateById($account['id'], $data);  // active the account 	
        }



        /////////////////  chech if it has permission to add new user to his account ////////////////
        ///////////////////////// get max users from paln who choose it before 
        $planModel = new Model_Plan();

        $Plan = $planModel->getByID($account['plan_id']);

        ////// get all users with this account /////////////////////////////////
        $users = $userCompaniesModel->getUsersByCompanyId($account['company_id']);
        if (count($users) == $Plan['max_users']) {

            $return['success'] = true;
            $return['url'] = 'panel-first-login';
            echo json_encode($return);
            exit;
        }

        //////////////////////////////////////////////////////////////////////////////////////////////

        if ($this->getRequest()->isPost()) {

            if ($form->isValid($this->request->getPost())) {

                $modelUser = new Model_User();
                $city      = new  Model_Cities();
                $citySydny = $city ->getIDByName('Sydney') ;
                
                $user      = $modelUser->getByEmail($email);

                if ($user) {

                    // $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "email is used "));
                    $return['success'] = false;
                    $return['msg'] = 'email is used ';
                    echo json_encode($return);
                    exit;
                }

                $data = array(
                    'email1' => $email,
                    'user_code' => sha1($email),
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'created' => time(),
                    'role_id' => 1, // set admin to this account
                    'active' => 'FALSE' ,
                    'city_id' => $citySydny
                );

                $hash_code = sha1($email);
                $userID = $modelUser->insert($data);   /// inser user admin for new account 

             // var_dump($userID) ;exit; 

                $dataCompany = array(
                    'user_id' => $userID,
                    'company_id' => $account['company_id'],
                    'created' => time()
                );


                $insertff = $userCompaniesModel->insert($dataCompany);


                /////////////////////////////send email invitation to new user //////////////////////////////////////////
                /* 	
                  $template_params = array(
                  '{admin_name}'       => $user['first_name'] ,
                  '{user_activecode}'  => 'http://temp.tilecleaners.com.au/getinvitation/'.$email.'/'.$hash_code

                  );
                 */
                //we got customer info to use company_id in preparing email template

                $to[] = $email;

                $to = implode(',', $to);

                $username = $userAdmin ['first_name'];
                $image1 = $_SERVER['SERVER_NAME'] . '/emaillogo.png';
                $image2 = $_SERVER['SERVER_NAME'] . "/bkemail.png";

                $username = $userAdmin['username'];

                $href = 'http://temp.tilecleaners.com.au/getinvitation/' . $email . '/' . $hash_code;

                $bodyhtml = '
			<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" media="screen">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<script src="http://code.jquery.com/jquery.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Open+Sans|Montserrat" rel="stylesheet" type="text/css">
</head>
<body>
 <table border="0" cellpadding="0" align="center"  width="600" style="border-collapse: collapse;">
  <tr>
    <td align="center" bgcolor="#fff" >
      <img src="' . $image1 . '" alt="Creating Email Magic" width="300" height="56" style="display: block;" />
    </td>
 </tr>
 <tr style="background:url(' . $image2 . '); height:600px;">

  <td style="padding:260px 50px 0px 50px; text-align:center; color:#fff; font-size:18px;"> 
      ' . $username . '
	  <br></br>
 Invited you to join their team

      </br>
	  <a href="' . $href . '" > 
	  <div style="padding:6px 30px 6px 30px; text-align:center; color:#fff; font-size:18px; margin:40px auto ;
	        width:260px; background:#0091d7; border-radius:10px; ">
             Accept Invite
	  </div>
	  </a>
  </td>
 </tr>
 </tr>
 </table>
	
	<style>
	  a, a:hover{
	   color:#FFF;
	   text-decoration:none;
	  }
	</style>
</body>
</html>';


                $params = array(
                    'to' => $to,
                    'body' => $bodyhtml,
                    'subject' => '(octopuspro)�?',
                        // 'from' => 'admin@octupospro.com',
                        // 'name' => 'octupospro'				
                );

                try {
                    EmailNotification::sendEmail($params, '', array(), array(), 1);
                    $sent = 'done';
                } catch (Zend_Mail_Transport_Exception $e) {
                    $sent = 'error';
                    var_dump('error');
                    exit;
                }


                $users = $userCompaniesModel->getUsersByCompanyId($account['company_id']);
                if (count($users) == $Plan['max_users']) {

                    $return['success'] = true;
                    $return['url'] = 'panel-first-login';
                    echo json_encode($return);
                    exit;
                }


                $return['success'] = 5;
                $return['msg'] = 'add user successfully ';
                echo json_encode($return);
                exit;
            }
        }

        $userCompaniesModel = new Model_UserCompanies();
        $users = $userCompaniesModel->getUsersByCompanyId($account['company_id']);
        //var_dump($users); exit;
        $this->view->form = $form;
        $this->view->assign('users', $users);

        echo $this->view->render('index/add-users-page.phtml');
        exit;
    }

    public function getInvitationAction() {


        if (Zend_Auth::getInstance()->hasIdentity()) {
            CheckAuth::logout();
        }


        $form = new Subscription_Form_Invitation();

        $email = $this->_getParam('email');
        $hashcode = $this->_getParam('hashcode');

        $userModel = new Model_User();
        $user = $userModel->getByEmail($email);

        $hashcode = $this->request->getParam('hashcode');

        if ($user) {



            if ($user['active'] == 'TRUE') {


                $this->view->assign('data', 'active_prev');
                return;
            } else {


                if ($user['user_code'] == $hashcode) {



                    $request = $this->getRequest();
                    $form = new Subscription_Form_invitation(array('user' => $user, 'hashcode' => $hashcode));


                    if ($this->getRequest()->isPost()) {
                        if ($form->isValid($request->getPost())) {

                            /* $data= array(
                              'active'    =>  'TRUE' ,
                              'user_code' =>  ''
                              ); */

                            $username_email = $this->request->getParam('email1');
                            $first_name = $this->request->getParam('first_name');
                            $last_name = $this->request->getParam('last_name');
                            $password = $this->request->getParam('password');
                            $password_confirm = $this->request->getParam('password_confirm');

                            $modelUser = new Model_User();
                            $user = $modelUser->getByEmail($username_email);

                            if ($user and $user['email1'] != $email) {

                                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "email is used "));
                                $this->_redirect($this->router->assemble(array(), 'invitationform'));

                                exit;
                            }


                            if ($password == $password_confirm) {

                                $data = array(
                                    'email1' => $username_email,
                                    'password' => sha1($password),
                                    'user_code' => '',
                                    'active' => 'TRUE',
                                    'created' => time(),
                                    'first_name' => $first_name, // set admin to this account
                                    'last_name' => $last_name
                                );

                                $userID = $modelUser->updateById($user['user_id'], $data);   /// inser user admin for new account 
                                $this->_redirect($this->router->assemble(array(), 'Login'));
                            } else {
                                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "password dont match"));
                                $this->_redirect($this->router->assemble(array(), 'invitationform'));
                            }
                        }
                    }
                } else {
                    exit;
                }
            }
        } else {
            exit;
        }

        $this->view->form = $form;
    }

    /////////////////////////////////////////////////////////////



    public function checkCompleteAction() {


        $userr = CheckAuth::getLoggedUser();
       // echo json_encode($userr );

        if (CheckAuth::checkAccountSteps($userr['user_id']) < 4) {

            $return['success'] = true;
            echo json_encode($return);
        } else {
            $return['success'] = false;
            echo json_encode($return);
        }
        exit;
    }

    public function checkCompleteNextAction() {

        $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Must Complete This Steps"));
        $this->_redirect($this->router->assemble(array(), 'panelFirstLogin'));
        exit;
    }

    public function testemailAction() {


           echo sha1(123456); exit; 

        //$totalAmount = $this->request->getParam('totalAmount');
       // $this->view->totalAmount = $totalAmount;


        //   CheckAuth::checkPermission(array('reportPaymentReceived'));

        /*
          $to = 'abusalem.islam1988@gmail.com' ;
          $modelEmailTemplate = new Model_EmailTemplate();
          $emailTemplate = $modelEmailTemplate->getEmailTemplate('Limited_time_offer');

          //var_dump($emailTemplate); exit;
          $body = $emailTemplate['body'];
          $subject = $emailTemplate['subject'];




          $params = array(
          'to' => $to,
          'body' => $body,
          'subject' => $subject,
          //'email' => 'mohammed91my@gmail.com',
          //'name' => 'octupospro'
          );

          try {
          EmailNotification::sendEmail($params, '', array(), array() , 1);
          $sent = 'done';
          } catch (Zend_Mail_Transport_Exception $e) {
          $sent = 'error';
          var_dump('error'); exit;
          }
          exit;

         */
    }

    public function subscriptionPageAction() {

        $this->_helper->layout->setLayout('layout');
        CheckAuth::checkLoggedIn();
        $userr = CheckAuth::getLoggedUser();
        $accountModel = new Model_Account();
        $planModel = new Model_Plan();
        $userCompaniesModel = new Model_UserCompanies();
        $SubscriptionPaymentModel = new Model_SubscriptionPayment();
        $account = $accountModel->getByCreatedBy($userr['user_id']);

        if ($account) {


            $companiesObj = new Model_Companies();
            $company = $companiesObj->getById($account['company_id']);
            $dataPlan = $planModel->getById($account['plan_id']);
            $users = $userCompaniesModel->getUsersByCompanyId($account['company_id']);


            switch ($account['account_status']) {
                case 'Subscriber-trial':
                    $statusAccount = 'trial';
                    break;
                case 'No-subscriber':
                    $statusAccount = 'trial';
                    break;
                case 'Subscriber-paid':
                    $statusAccount = 'premium account';
                    break;
                case 'Overdue':
                    $statusAccount = 'Overdue';
                    break;
                default:
                    $statusAccount = 'undefined';
            }


            $stop_date = new DateTime($account['trial_end_date']);
            $stop_date->format('Y-m-d');
            $trialEndDate = $stop_date->format('Y-m-d');

            $stop_date = new DateTime($account['from']);
            $subcription_from = $stop_date->format('Y-m-d');

            $stop_date = new DateTime($account['to']);
            $subcription_to = $stop_date->format('Y-m-d');

            if ($statusAccount == 'trial') {
                     $remind_days = $accountModel->remindDays($trialEndDate);
                     ( ( $remind_days < 0 ) ? $remind_days = 0 : $remind_days )  ;

                        
                
            } else {

                $remind_days = $accountModel->remindDays($subcription_to);
                 ( ( $remind_days < 0 ) ? $remind_days = 0 : $remind_days )  ;
                $price = $SubscriptionPaymentModel->getPaymentByAccountID($account['id']);
                $price = $price['payment_amount']/100 ;
                $price = money_format('%i', $price);
                $this->view->assign('last_payment', $price);
            }

            $this->view->assign('company_name', $company['company_name']);
            $this->view->assign('plan_name', $dataPlan['name']);
            $this->view->assign('max_plan', $dataPlan['max_users']);
            $this->view->assign('account_status', $statusAccount);
            $this->view->assign('trial_end_date', $trialEndDate);
            $this->view->assign('from', $subcription_from);
            $this->view->assign('to', $subcription_to);
            $this->view->assign('company_users_active', count($users));
            $this->view->assign('remind_days', $remind_days);
            
            $this->view->assign('paidforusers', $account['paid_user_count'] );
            

        } else {
            exit;
        }
    }

    public function addUserChangeAction() {

        $isSubscribe = $this->request->getParam('isSubscribe', 0);

        if ($isSubscribe) {
            $this->view->isSubscribe = 1;
        }
        
        

        $this->_helper->layout->setLayout('layout');
        CheckAuth::checkLoggedIn();
        $userr = CheckAuth::getLoggedUser();
        $accountModel = new Model_Account();
        $account = $accountModel->getByCreatedBy($userr['user_id']);
        $accountId = $account['id'];
        $nbrUser = $this->request->getParam('nbrUser', 0);      
        
                 switch ($account['account_status']) {
                case 'Subscriber-trial':
                    $statusAccount = 'Trial Account';
                    break;
                case 'No-subscriber':
                    $statusAccount = 'Trial Account';
                    break;
                case 'Subscriber-paid':
                    $statusAccount = 'Premium Account';
                    break;
                case 'Overdue':
                    $statusAccount = 'Overdue Account';
                    break;
                default:
                    $statusAccount = 'undefined';
            }
        $this->view->accountId = $accountId;
        $this->view->statusAccount = $statusAccount;

        if ($this->request->isPost()) {

            $SubscriptionPaymentObj = new Model_SubscriptionPayment();
            $totalAmount = $SubscriptionPaymentObj->addUserTotalAmount($accountId, $nbrUser);

            echo $totalAmount;
            exit;
        }
    }

    public function changePlanMessegeAction() {

         $isGreaterThan = $this->request->getParam('isGreaterThan', 0);
         $this->view->isGreaterThan = $isGreaterThan; 
         echo $this->view->render('index/informMaxUserMessege.phtml');
         exit;
    }


    
    public function addUserChangePlanAction(){
        
        $isSubscribe = $this->request->getParam('isSubscribe', 0);
        if ($isSubscribe) {
            $this->view->isSubscribe = 1;
        }
                
        $this->_helper->layout->setLayout('layout');
        CheckAuth::checkLoggedIn();
        $userr = CheckAuth::getLoggedUser();
        $accountModel = new Model_Account();
        $account = $accountModel->getByCreatedBy($userr['user_id']);
        $accountId = $account['id'];
        
        //$result = $accountModel->getExpiredAccount($accountId);
        
       // print_r($result); 
        
        $nbrOfUserToAdd = $this->request->getParam('nbrOfUserToAdd', 0);
        $new_plan_id    = $this->request->getParam('newPlanId', 0);
        $old_plan_id    = $this->request->getParam('oldPlanId', 0);
        $form = new Subscription_Form_ChangePlan(array('plan_id' => $account['plan_id']));
        
            switch ($account['account_status']) {
                case 'Subscriber-trial':
                    $statusAccount = 'Trial Account';
                    break;
                case 'No-subscriber':
                    $statusAccount = 'Trial Account';
                    break;
                case 'Subscriber-paid':
                    $statusAccount = 'Premium Account';
                    break;
                case 'Overdue':
                    $statusAccount = 'Overdue Account';
                    break;
                default:
                    $statusAccount = 'undefined';
            }
                        
        $this->view->form          = $form;
        $this->view->accountId     = $accountId;  
        $this->view->statusAccount = $statusAccount;
//        $this->view->newPlanId     = $new_plan_id;
//        $this->view->oldPlanId     = $old_plan_id;

        if ($this->request->isPost()) {
            
            $SubscriptionPaymentObj = new Model_SubscriptionPayment();
            $result = $SubscriptionPaymentObj->calculateTotalAmountAddUserChangePlan($accountId, $new_plan_id, $old_plan_id, $nbrOfUserToAdd);
           // $this->view->totalAmount     = $result['totalAmount'];
            foreach ($result as $a) {
                echo $a . ",";
            }
            exit;
           
        }
    }





    public function sendEmailNewAccount($username_email , $hash_code){
        $template_params = array(
                        '{user_activecode}' => $_SERVER['SERVER_NAME'].'/checkvalidateaccount/' . $username_email . '/' . $hash_code
                    );

                    $href = $_SERVER['SERVER_NAME'] .'/checkvalidateaccount/' . $username_email . '/' . $hash_code;

                    //we got customer info to use company_id in preparing email template

                    $to[] = $username_email;

                    $to = implode(',', $to);

                    $modelEmailTemplate = new Model_EmailTemplate();
                    $emailTemplate = $modelEmailTemplate->getEmailTemplate('activation_new_account ', $template_params);

                    $image1 = $_SERVER['SERVER_NAME'] . '/emaillogo.png';
                    $image2 = $_SERVER['SERVER_NAME'] . "/bkemail.png";

                    //var_dump($image1); exit ;

                    $bodyhtml = '
                 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" media="screen">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<script src="http://code.jquery.com/jquery.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<link href="https://fonts.googleapis.com/css?family=Open+Sans|Montserrat" rel="stylesheet" type="text/css">

</head>

<body>
 <table border="0" cellpadding="0" align="center"  width="600" style="border-collapse: collapse;">
  <tr>
    <td align="center" bgcolor="#fff" >
      <img src="' . $image1 . '" alt="Creating Email Magic" width="300" height="56" style="display: block;" />
    </td>
 </tr>
 <tr style="background:url(' . $image2 . '); height:600px;">
  <td style="padding:260px 50px 0px 50px; text-align:center; color:#fff; font-size:18px;">
      
      Thank You for your registration 
      </br>
 to activate your account please click the link below
      </br>
      <a href="' . $href . '" > 
      <div style="padding:6px 30px 6px 30px; text-align:center; color:#fff; font-size:18px; margin:40px auto ;
            width:260px; background:#0091d7; border-radius:10px; ">
            click here 
      </div>
      </a>
  </td>
 </tr>
 </tr>
 </table>
    <style>
      a, a:hover{
       color:#FFF;
       text-decoration:none;
      }
    </style>
</body>
</html>';
                    $params = array(
                        'to' => $to,
                        'body' => $bodyhtml,
                        'subject' => 'Activate Your Account',
                            // 'email' => 'admin@octupospro.com', 
                            // 'name' => 'octupospro', 
                    );

                    try {
                        EmailNotification::sendEmail($params, '', array(), array());
                        $sent = 'done';
                    } catch (Zend_Mail_Transport_Exception $e) {
                        $sent = 'error';
                    }


    }








}
