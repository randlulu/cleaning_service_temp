<?php

class Subscription_AccountController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        // parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        CheckAuth::checkEmployeePermission();
        
        $this->_helper->layout->setLayout('layout_sub');
    }

    /**
     * Items list action
     */
    public function indexAction() {

        $modelAccount = new Model_Account();
        $orderBy = $this->request->getParam('sort', 'id');
        $sortingMethod = $this->request->getParam('method', 'desc');
        $filters = $this->request->getParam('fltr', array());
        //$is_suspended = $this->request->getParam('is_suspended', '');
        $id = $this->request->getParam('id', 0);

        //should added to index controller to read eway controller
        $nbrUser = $this->request->getParam('nbrOfUser', 0);
        $totalAmount = $this->request->getParam('totalAmount', 0);
        $from = $this->request->getParam('from', 0);
        $to = $this->request->getParam('to', 0);
        $this->view->nbrUser = $nbrUser;
        $this->view->totalAmount = $totalAmount;
        $this->view->to = $to;
        $this->view->from = $from;
        //end that................................

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        $accounts = $modelAccount->getAll($filters, "{$orderBy} {$sortingMethod}");

        $this->view->data = $accounts;

        $account = $modelAccount->getById($id);
        $user = $modelAccount->getUserById($account['created_by']);

//        $data = $modelAccount->getAll();
//        $this->view->data = $data;
        if ($this->request->isPost()) {
            $is_suspended = $this->request->getParam('is_suspended', '');
            if (strcmp($is_suspended, 'suspend') == 0) {

                $data = array(
                    'account_status' => 'suspended'
                );
                $response = $modelAccount->updateById($id, $data);
                echo 1;
                exit;

//            if ($response) {
//                $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "This Subscription has been suspended successfuly"));
//            }
                //$this->_redirect('http://temp.tilecleaners.com.au/subscription/account');
            } elseif (strcmp($is_suspended, 'unsuspend') == 0) {
                $paymentStatus = $modelAccount->checkSubscriptionPaymentStatus($id);
                if ($paymentStatus == 'Trial') {
                    $data = array(
                        'account_status' => 'Subscriber-trial'
                    );
                    $response1 = $modelAccount->updateById($id, $data);
                    echo 2;
                    exit;
                } elseif ($paymentStatus == 'Payed') {
                    $data = array(
                        'account_status' => 'Subscriber-paid'
                    );
                    $response1 = $modelAccount->updateById($id, $data);
                    echo 3;
                    exit;
                } else {
                    $data = array(
                        'account_status' => 'Overdue'
                    );
                    $response1 = $modelAccount->updateById($id, $data);
                    echo 4;
                    exit;
                }
                //echo 2; exit;
//            if ($response1) {
//                $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "This Subscription has been unsuspended successfuly"));
//            }
                //$this->_redirect('http://temp.tilecleaners.com.au/subscription/account');
            }
        }
    }

    public function viewAction() {


        $id = $this->request->getParam('id', 0);
        $this->view->id = $id;

        $account = new Model_Account();
        $accountInfo = $account->getById($id);
        $user = $account->getUserById($accountInfo['created_by']);
        $company = $account->getCompanyById($accountInfo['company_id']);
        $plan = $account->getPlanById($accountInfo['plan_id']);
        $users = $account->getUsersByCompanyId($company['company_id']);

        $this->view->accountInfo = $accountInfo;
        $this->view->user = $user;
        $this->view->company = $company;
        $this->view->plan = $plan;
        $this->view->users = $users;
    }

    public function addUserAction() {


        $username = $this->request->getParam('username', '');
        $display_name = $this->request->getParam('display_name', '');
        $password = $this->request->getParam('password', '');
        $roleId = 1;
        $first_name = $this->request->getParam('first_name', '');
        $last_name = $this->request->getParam('last_name', '');
        $email1 = $this->request->getParam('email1', '');
        $email2 = $this->request->getParam('email2', '');
        $email3 = $this->request->getParam('email3', '');
        $systemEmail = $this->request->getParam('systemEmail', '');

        $internation_key = $this->request->getParam('international_key', '');
        $mobile_key = $this->request->getParam('mobile_key', '');
        $phone_key = $this->request->getParam('phone_key', '');

        $mobile1 = $this->request->getParam('mobile1', '');
        $mobile2 = $this->request->getParam('mobile2', '');
        $mobile3 = $this->request->getParam('mobile3', '');
        $phone1 = $this->request->getParam('phone1', '');
        $phone2 = $this->request->getParam('phone2', '');
        $phone3 = $this->request->getParam('phone3', '');
        $fax = $this->request->getParam('fax', '');
        $emergencyPhone = $this->request->getParam('emergency_phone', '');
        $unitLotNumber = $this->request->getParam('unit_lot_number', '');
        $streetNumber = $this->request->getParam('street_number', '');
        $streetAddress = $this->request->getParam('street_address', '');
        $suburb = $this->request->getParam('suburb', '');
        $state = $this->request->getParam('state', '');
        $postcode = $this->request->getParam('postcode', '');
        $po_box = $this->request->getParam('po_box', '');
        $cityId = $this->request->getParam('city_id', '');
        $countryId = $this->request->getParam('country_id', '');
        $companyId = $this->request->getParam('company_id', '');


        /*         * ******Check if user put phone or mobile Number? and put full format***********IBM */
        if (isset($mobile1) && $mobile1 != "") {
            $mobile1_key = substr($mobile_key, 1, 1);
            $mobile1 = $internation_key . $mobile1_key . $mobile1;
        }
        if (isset($mobile2) && $mobile2 != "") {
            $mobile2_key = substr($mobile_key, 1, 1);
            $mobile2 = $internation_key . $mobile2_key . $mobile2;
        }
        if (isset($mobile3) && $mobile3 != "") {
            $mobile3_key = substr($mobile_key, 1, 1);
            $mobile3 = $internation_key . $mobile3_key . $mobile3;
        }
        if (isset($phone1) && $phone1 != "") {
            $phone1_key = substr($phone_key, 1, 1);
            $phone1 = $internation_key . $phone1_key . $phone1;
        }
        if (isset($phone2) && $phone2 != "") {
            $phone2_key = substr($phone_key, 1, 1);
            $phone2 = $internation_key . $phone2_key . $phone2;
        }
        if (isset($phone3) && $phone3 != "") {
            $phone3_key = substr($phone_key, 1, 1);
            $phone3 = $internation_key . $phone3_key . $phone3;
        }

        /*         * *****End********** */

        if (get_config('remove_white_spacing')) {
            $mobile1 = preparer_number($mobile1);
            $mobile2 = preparer_number($mobile2);
            $mobile3 = preparer_number($mobile3);
            $phone1 = preparer_number($phone1);
            $phone2 = preparer_number($phone2);
            $phone3 = preparer_number($phone3);
        }

        $router = Zend_Controller_Front::getInstance()->getRouter();

//        if (isset($roleId)) {
//            $modelAuthRole = new Model_AuthRole();
//            $role = $modelAuthRole->getById($roleId);
//            if ($role['role_name'] == 'super_admin' && !CheckAuth::checkCredential(array('canHandleAllCompanies'))) {
//                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You have entered a bad option !!"));
//                echo 1;
//                exit;
//            }
//        }
        //
        // init action form
        //
        $form = new Subscription_Form_SubscriptionUser(array('country_id' => $countryId, 'state' => $state));
        //
        // handling the insertion process
        //
        
        if ($this->request->isPost()) {

            // // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $modelUser = new Model_User();
                $data = array(
                    'display_name' => $display_name,
                    'username' => $username,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'password' => sha1($password),
                    'user_code' => sha1($username),
                    'last_login' => time(),
                    'created' => time(),
                    'city_id' => $cityId,
                    'role_id' => $roleId,
                    'email1' => $email1,
                    'email2' => $email2,
                    'email3' => $email3,
                    'system_email' => $systemEmail,
                    'mobile1' => $mobile1,
                    'mobile2' => $mobile2,
                    'mobile3' => $mobile3,
                    'phone1' => $phone1,
                    'phone2' => $phone2,
                    'phone3' => $phone3,
                    'fax' => $fax,
                    'emergency_phone' => $emergencyPhone,
                    'unit_lot_number' => $unitLotNumber,
                    'street_number' => $streetNumber,
                    'street_address' => $streetAddress,
                    'suburb' => $suburb,
                    'state' => $state,
                    'postcode' => $postcode,
                    'po_box' => $po_box,
                    'active' => 'FALSE'
                );
                //   echo '<script type="text/javascript">alert("Data has been submitted to ' . $phone1_key . '");</script>';
                // echo '<script type="text/javascript">alert("Data has been submitted to ' . $data . '");</script>';
                // var_dump($phone_key) ; exit;
                //
                //get role id for contractor
                //
                $userId = $modelUser->insert($data);


                $dataCompany = array(
                    'user_id' => $userId,
                    'company_id' => $companyId,
                    'created' => time()
                );
                $userCompaniesModel = new Model_UserCompanies();
                $userCompaniesModel->insert($dataCompany);

                if ($userId) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in User"));
                }

                echo 1;
                exit;
            }
        }

        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('account/add_edit.phtml');
        exit;
    }

    public function deleteUserAction() {
        $user_id = $this->request->getParam('user_id', 0);

        //$router = Zend_Controller_Front::getInstance()->getRouter();
        $form = new Subscription_Form_DeleteUser(array('user_id' => $user_id));

        if ($this->request->isPost()) {
            $modelUser = new Model_User();
            $userId = $modelUser->deleteById($user_id);
            if ($userId) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Deleted successfully"));
            }
            echo 1;
            exit;
        }
        $this->view->form = $form;


        echo $this->view->render('account/DeleteUser.phtml');
        exit;
    }

    public function editUserAction() {
        $user_id = $this->request->getParam('user_id');
        $username = $this->request->getParam('username', '');
        $display_name = $this->request->getParam('display_name', '');
        $password = $this->request->getParam('password', '');
        //$roleId = 1;
        $first_name = $this->request->getParam('first_name', '');
        $last_name = $this->request->getParam('last_name', '');
        $email1 = $this->request->getParam('email1', '');
        $email2 = $this->request->getParam('email2', '');
        $email3 = $this->request->getParam('email3', '');
        $systemEmail = $this->request->getParam('systemEmail', '');

        $internation_key = $this->request->getParam('international_key', '');
        $mobile_key = $this->request->getParam('mobile_key', '');
        $phone_key = $this->request->getParam('phone_key', '');

        $mobile1 = $this->request->getParam('mobile1', '');
        $mobile2 = $this->request->getParam('mobile2', '');
        $mobile3 = $this->request->getParam('mobile3', '');
        $phone1 = $this->request->getParam('phone1', '');
        $phone2 = $this->request->getParam('phone2', '');
        $phone3 = $this->request->getParam('phone3', '');
        $fax = $this->request->getParam('fax', '');
        $emergencyPhone = $this->request->getParam('emergency_phone', '');
        $unitLotNumber = $this->request->getParam('unit_lot_number', '');
        $streetNumber = $this->request->getParam('street_number', '');
        $streetAddress = $this->request->getParam('street_address', '');
        $suburb = $this->request->getParam('suburb', '');
        $state = $this->request->getParam('state', '');
        $postcode = $this->request->getParam('postcode', '');
        $po_box = $this->request->getParam('po_box', '');
        $cityId = $this->request->getParam('city_id', '');
        $countryId = $this->request->getParam('country_id', '');
        $companyId = $this->request->getParam('company_id', '');


        /*         * ******Check if user put phone or mobile Number? and put full format***********IBM */
        if (isset($mobile1) && $mobile1 != "") {
            $mobile1_key = substr($mobile_key, 1, 1);
            $mobile1 = $internation_key . $mobile1_key . $mobile1;
        }
        if (isset($mobile2) && $mobile2 != "") {
            $mobile2_key = substr($mobile_key, 1, 1);
            $mobile2 = $internation_key . $mobile2_key . $mobile2;
        }
        if (isset($mobile3) && $mobile3 != "") {
            $mobile3_key = substr($mobile_key, 1, 1);
            $mobile3 = $internation_key . $mobile3_key . $mobile3;
        }
        if (isset($phone1) && $phone1 != "") {
            $phone1_key = substr($phone_key, 1, 1);
            $phone1 = $internation_key . $phone1_key . $phone1;
        }
        if (isset($phone2) && $phone2 != "") {
            $phone2_key = substr($phone_key, 1, 1);
            $phone2 = $internation_key . $phone2_key . $phone2;
        }
        if (isset($phone3) && $phone3 != "") {
            $phone3_key = substr($phone_key, 1, 1);
            $phone3 = $internation_key . $phone3_key . $phone3;
        }

        /*         * *****End********** */

        if (get_config('remove_white_spacing')) {
            $mobile1 = preparer_number($mobile1);
            $mobile2 = preparer_number($mobile2);
            $mobile3 = preparer_number($mobile3);
            $phone1 = preparer_number($phone1);
            $phone2 = preparer_number($phone2);
            $phone3 = preparer_number($phone3);
        }

        $router = Zend_Controller_Front::getInstance()->getRouter();

        $modelUser = new Model_User();
        $user = $modelUser->getById1($user_id);

        if (!$user) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect($this->router->assemble(array(), 'account'));
            return;
        }

        $form = new Subscription_Form_SubscriptionUser(array('mode' => 'update', 'user' => $user, 'country_id' => $countryId, 'state' => $state));


        // handling the updating process
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'display_name' => $display_name,
                    'username' => $username,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'city_id' => $cityId,
                    //'role_id' => $roleId,
                    'email1' => $email1,
                    'email2' => $email2,
                    'email3' => $email3,
                    'system_email' => $systemEmail,
                    'mobile1' => $mobile1,
                    'mobile2' => $mobile2,
                    'mobile3' => $mobile3,
                    'phone1' => $phone1,
                    'phone2' => $phone2,
                    'phone3' => $phone3,
                    'fax' => $fax,
                    'emergency_phone' => $emergencyPhone,
                    'unit_lot_number' => $unitLotNumber,
                    'street_number' => $streetNumber,
                    'street_address' => $streetAddress,
                    'suburb' => $suburb,
                    'state' => $state,
                    'postcode' => $postcode,
                    'po_box' => $po_box
                );

                $success = $modelUser->updateById($user_id, $data);


                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in User"));
                }

                echo 1;
                exit;
            }
        }


        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('account/add_edit.phtml');
        exit;
    }

    public function viewUserAction() {
        $id = $this->request->getParam('user_id', 0);
        $this->view->id = $id;
        $account = new Model_Account();
        $user = $account->getUserById($id);
        $this->view->user = $user;
        //$router = Zend_Controller_Front::getInstance()->getRouter();

        echo $this->view->render('account/ViewUser.phtml');
        exit;
    }

    public function suspendAction() {
        $account_id = $this->request->getParam('id', 0);
        $operation = $this->request->getParam('is_suspended', '');
        $this->view->account_id = $account_id;
        $this->view->operation = $operation;



        echo $this->view->render('account/suspendAction.phtml');
        exit;
    }

//    public function changePlanMessegeAction() {
//
//        echo $this->view->render('account/informMaxUserMessege.phtml');
//        exit;
//    }
//
//    public function changePlanAction() {
//        $plan_id = $this->request->getParam('plan_id', 0);
//        $plan = new Model_Plan();
//        $plans = $plan->getAll();
//        //$this->view->plans = $plans;
//
//        $form = new Subscription_Form_ChangePlan(array('plan_id' => $plan_id));
//
//        $this->view->form = $form;
//        echo $this->view->render('account/changePlan.phtml');
//        exit;
//    }
//
//    public function addUserChangeAction() {
//
//        //$accountId = 80;
//        $nbrUser = $this->request->getParam('nbrUser', 0);
//        //$this->view->accountId = $accountId;
//
//        if ($this->request->isPost()) {
//
//            $SubscriptionPaymentObj = new Model_SubscriptionPayment();
//            $totalAmount = $SubscriptionPaymentObj->calculateTotalAmount($accountId, $nbrUser);
//
//            echo $totalAmount;
//            exit;
//        }
//    }
//
//    public function changePlanViewAction() {
//
//        //$accountId   = 80;
//        $plan_id     = $this->request->getParam('planId', 0);
//        $old_plan_id = $this->request->getParam('oldPlanId',0); 
//        //$this->view->accountId = $accountId;
//        $form = new Subscription_Form_ChangePlan(array('plan_id' => 2));
//        $this->view->form = $form;
//
//        if ($this->request->isPost()) {
//
//            $SubscriptionPaymentObj = new Model_SubscriptionPayment();
//            $array1 = $SubscriptionPaymentObj->calculateTotalAmountChangePlan($accountId, $plan_id, $old_plan_id);
//            foreach ($array1 as $a) {
//                echo $a . ",";
//            }
//            exit;
//        }
//    }

}
