<?php

class Model_Plan extends Zend_Db_Table_Abstract {

    protected $_name = 'plan';

    
    
    /** get table row according to the assigned id  */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("plan_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }
    
    //** get all account by plan Id */
    public function getAccountById($id){
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from(array('acc' => 'account'));
        $select->joinInner(array('pln' => $this->_name), 'acc.plan_id = pln.id', '');
        $select->where("pln.id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    
    /** delete plan by Id */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("plan_id = '{$id}'");
    }
    
    /** update the plan according the id **/ 
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "plan_id = '{$id}'");
    }

    public function getAll($filters = array(), $order = null, &$pager = null, $limit = 0, $perPage = 0, $currentPage = 0) {
        $select = $this->getAdapter()->select();
        $select->from(array('pln' => $this->_name));
        $select->order($order);
        $select->distinct();

        if ($order) {
            $select->order($order);
        }

        $wheresAndJoins = $this->getWheresAndJoinsByFilters($filters);
        $joinInner = $wheresAndJoins['joinInner'];
        $wheres = $wheresAndJoins['wheres'];
        $groupBy = $wheresAndJoins['groupBy'];

        if ($wheres) {
            foreach ($wheres as $where) {
                $select->where($where);
            }
        }

        if ($joinInner) {
            foreach ($joinInner as $inner) {
                $select->joinInner($inner['name'], $inner['cond'], $inner['cols']);
            }
        }
        /*         * *Select Group BY **IBM */
        if ($groupBy) {
            foreach ($groupBy as $group) {
                $select->group($group);
            }
        }
        /*         * End Select Group By */

        if ($pager) {
            if (!empty($filters['perPage'])) {
                $select->limit($filters['perPage'], ($pager->currentPage - 1) * $filters['perPage']);
            } else {
                $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            }

            $pager->dbSelect = $select;
        } elseif ($perPage && $currentPage) {
            $select->limit($perPage, ($currentPage - 1) * $perPage);
        } else {
            if (!empty($filters['perPage'])) {
                $select->limit($filters['perPage']);
            }
        }

        return $this->getAdapter()->fetchAll($select);
    }

    public function getWheresAndJoinsByFilters($filters, $isCronJob = false) {

        $select = $this->getAdapter()->select();
        $select->from(array('pln' => $this->_name));
        
        $wheres = array();
        $joinInner = array();
        $groupBy = array();
        // $loggedEmployee = CheckAuth::getLoggedEmployee();        

        if ($filters) {
            if (!empty($filters['keywords'])) {
                $keywords = $this->getAdapter()->quote('%' . trim($filters['keywords']) . '%');
                // $joinInner['account'] = array('name' => array('acount' => 'account'), 'cond' => 'c.booking_id = bok.booking_id', 'cols' => '');
                $wheres['keywords'] = ("pln.name LIKE {$keywords} OR pln.plan_code LIKE {$keywords}");
            }

            if (!empty($filters['status'])) {
                $wheres['status'] = ("pln.status = {$filters['status']}");
            }

            if (!empty($filters['groupBy'])) {
                $groupBy['groupBy'] = ("c.groupBy = '{$filters['groupBy']}'");
            }

            if (!empty($filters['plan_id'])) {
                $plan_id = (int) $filters['plan_id'];
                $wheres['plan_id'] = ("pln.plan_id = {$plan_id}");
            }

        }

        // GroupBY
        return array('wheres' => $wheres, 'joinInner' => $joinInner, 'groupBy' => $groupBy);
    }

    public function getCount($filters = array()) {
        $count = $this->getAll($filters);
        return count($count);
    }

    public function getAccountsByPlanId($id){
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from(array('acc' => 'account'));
        $select->joinInner(array('pln' => $this->_name), 'acc.plan_id = pln.plan_id', '');
        $select->where("pln.plan_id = '{$id}'");

        return $this->getAdapter()->fetchAll($select);
    }

}
