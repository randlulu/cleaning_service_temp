<?php
class Model_SmsHistorty extends Zend_Db_Table_Abstract {

    protected $_name = 'sms_history';
	public function getAll($filters = array(), $order = null, &$pager = null, $limit = 0) {
        $select = $this->getAdapter()->select();
        $select->from(array('el' => $this->_name));
        if ($filters) {
            if (!empty($filters['reference_id'])) {
                $reference_id = $this->getAdapter()->quote($filters['reference_id']);
                $select->where("el.reference_id = {$reference_id}");
            }
        }

        return $this->getAdapter()->fetchAll($select);
    }
    /**
     * update table rows according to the assigned id and data
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "id = '{$id}'");
    }

    public function insert($data) {
        return parent::insert($data);
    }
    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("id = '{$id}'");
    }
    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id = '{$id}'");
        return $this->getAdapter()->fetchRow($select);
    }

    public function getByReferenceIdAndCronjobHistoryId($ref_id, $history_id) {

        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("reference_id = {$ref_id}");
        $select->where("cronjob_history_id = {$history_id}");
        
        return $this->getAdapter()->fetchAll($select);
    }

}