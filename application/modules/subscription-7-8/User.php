<?php

class Model_User extends Zend_Db_Table_Abstract {

    protected $_name = 'user';
    protected $_userColumns = array();
    protected $_userColumnsWithoutAlias = array();
    private $modelContractorInfo;

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
	public function __construct(){
        
        parent::__construct(); 
		$columns = $this->getAdapter()->fetchAll("SHOW COLUMNS FROM `user` where field not in ('phone1','phone2','phone3','mobile1','mobile2','mobile3')");
		
		foreach ($columns as $column) {
            $userColumns[] = "u.{$column['Field']}";
            $userColumnsWithoutAlias[] = "{$column['Field']}";
        }
		
		
		$userColumns['phone1'] = new Zend_Db_Expr("IF((length(phone1)>9 and phone1 like '0061%'),CONCAT('0',right(phone1,9)),phone1)");
		$userColumns['phone2'] =  new Zend_Db_Expr("IF((length(phone2)>9 and phone2 like '0061%'),CONCAT('0',right(phone2,9)),phone2)");
		$userColumns['phone3'] = new Zend_Db_Expr("IF((length(phone3)>9 and phone3 like '0061%'),CONCAT('0',right(phone3,9)),phone3)");
		$userColumns['mobile1'] = new Zend_Db_Expr("IF((length(mobile1)>9 and mobile1 like '0061%'),CONCAT('0',right(mobile1,9)),mobile1)");
		$userColumns['mobile2'] = new Zend_Db_Expr("IF((length(mobile2)>9 and mobile2 like '0061%'),CONCAT('0',right(mobile2,9)),mobile2)");
		$userColumns['mobile3'] = new Zend_Db_Expr("IF((length(mobile3)>9 and mobile3 like '0061%'),CONCAT('0',right(mobile3,9)),mobile3)");
		
		$userColumnsWithoutAlias['phone1'] = new Zend_Db_Expr("IF((length(phone1)>9 and phone1 like '0061%'),CONCAT('0',right(phone1,9)),phone1)");
		$userColumnsWithoutAlias['phone2'] =  new Zend_Db_Expr("IF((length(phone2)>9 and phone2 like '0061%'),CONCAT('0',right(phone2,9)),phone2)");
		$userColumnsWithoutAlias['phone3'] = new Zend_Db_Expr("IF((length(phone3)>9 and phone3 like '0061%'),CONCAT('0',right(phone3,9)),phone3)");
		$userColumnsWithoutAlias['mobile1'] = new Zend_Db_Expr("IF((length(mobile1)>9 and mobile1 like '0061%'),CONCAT('0',right(mobile1,9)),mobile1)");
		$userColumnsWithoutAlias['mobile2'] = new Zend_Db_Expr("IF((length(mobile2)>9 and mobile2 like '0061%'),CONCAT('0',right(mobile2,9)),mobile2)");
		$userColumnsWithoutAlias['mobile3'] = new Zend_Db_Expr("IF((length(mobile3)>9 and mobile3 like '0061%'),CONCAT('0',right(mobile3,9)),mobile3)");
		
		
		$this->_userColumns = $userColumns;
		$this->_userColumnsWithoutAlias = $userColumnsWithoutAlias;
		
		
    }

	  
    public function getAll($filters = array(), $order = null, &$pager = null , $limit = 0, $perPage = 0, $currentPage = 0) {
        $select = $this->getAdapter()->select();
        $select->from(array('u' => $this->_name),$this->_userColumns);
        $select->joinInner(array('ci' => 'city'), 'ci.city_id = u.city_id', array('ci.city_name'));
        $select->joinInner(array('co' => 'country'), 'ci.country_id = co.country_id', array('co.country_name'));
        $select->joinInner(array('ar' => 'auth_role'), 'ar.role_id = u.role_id', array('ar.role_name', 'ar.view_role_name'));
        $select->order($order);
        $select->distinct();

        $filters['company_id'] = CheckAuth::getCompanySession();

        if ($filters) {
            if (!empty($filters['keywords'])) {
                $keywords = $this->getAdapter()->quote('%' . $filters['keywords'] . '%');
                $select->where("username LIKE {$keywords}");
            }
            if (!empty($filters['user_id'])) {
                $select->where("u.user_id = {$filters['user_id']}");
            }
            if (!empty($filters['role_id'])) {
                $select->where("u.role_id = {$filters['role_id']}");
            }
			if (!empty($filters['active'])) {
                $select->where("u.active = '{$filters['active']}'");
            }
			if (!empty($filters['not_username'])) {
                $select->where("u.username != '{$filters['not_username']}'");
            }
            if (!empty($filters['not_role_id'])) {
                $select->where("u.role_id != {$filters['not_role_id']}");
            }
            if (!empty($filters['company_id'])) {
                $select->joinInner(array('uc' => 'user_company'), 'uc.user_id = u.user_id', '');

                $company_id = (int) $filters['company_id'];
                $select->where("uc.company_id = {$company_id}");
            }
			
			if (isset($filters['roles']) && !empty($filters['roles'])) {
                
				//$rolesIds = array(2, 7, 8);
				$select->where('u.role_id IN (?)', $filters['roles']);
                
                if (!empty($filters['logged_user_id'])) {
                    $select->where("u.user_id != '{$filters['logged_user_id']}'");
                } 
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }elseif ($limit) {
            $select->limit($limit);
        } elseif ($perPage && $currentPage) {
            $select->limit($perPage, ($currentPage - 1) * $perPage);
        }
		
		

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table row according to the assigned $id and submited $data
     * 
     * @param $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        $ret_val = parent::update($data, "user_id = '{$id}'");
        if ($ret_val) {
            $this->calculateProfileCompleteness($id);
        }
        return $ret_val;
    }

    /**
     * delete table row according to the assigned $id
     * 
     * @param  int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("user_id = '{$id}'");
    }

    /**
     * get table row according to the assigned $id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
		
     	
		
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from(array('u' => $this->_name),$this->_userColumns);
        $select->joinInner(array('ci' => 'city'), 'ci.city_id = u.city_id', array('ci.city_name'));
        $select->joinInner(array('co' => 'country'), 'ci.country_id = co.country_id', array('co.country_name'));
        $select->joinInner(array('ar' => 'auth_role'), 'ar.role_id = u.role_id', array('ar.role_name'));
        $select->where("user_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table rows according to the assigned $username
     * 
     * @param string $username
     * @return array 
     */
    public function getByUsername($username) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name,$this->_userColumnsWithoutAlias);
        $select->where("username = '{$username}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table rows according to the assigned $username
     * 
     * @param string $username
     * @return array 
     */
    public function getByUserCode($userCode) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name,$this->_userColumnsWithoutAlias);
        $select->where("user_code = '{$userCode}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table rows according to the assigned $username
     * 
     * @param string $username
     * @return array 
     */
    public function getByUsernameAndCompanyId($username, $companyId) {
        $select = $this->getAdapter()->select();
        $select->from(array('usr' => $this->_name));
        $select->where("usr.username = '{$username}'");

        $select->joinInner(array('uc' => 'user_company'), 'uc.user_id = usr.user_id', '');
        $select->where("uc.company_id = {$companyId}");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table rows according to the assigned $email
     * 
     * @param string $email
     * @return array 
     */
    public function getByEmail($email) {
        $select = $this->getAdapter()->select();
       // $select->from($this->_name,$this->_userColumns);
	   $select->from($this->_name,$this->_userColumnsWithoutAlias);
        $select->where("email1 = '{$email}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table rows according to the contractor role id 
     * as array to choose @return as array 'user_id' => business_name or the query result
     * 
     * @param tinyint $asArray
     * @return array 
     */
    public function getAllContractor($asArray = 0,$status = 'ALL') {
		

        $select = $this->getAdapter()->select();
        $select->from(array('usr' => $this->_name));

        $modelAuthRole = new Model_AuthRole();
        $role = $modelAuthRole->getRoleIdByName('contractor');
        $select->where("usr.role_id = '{$role}'");
		if($status != 'ALL'){
		$select->where("usr.active = '{$status}'");

		}

        $select->joinInner(array('ci' => 'contractor_info'), 'ci.contractor_id = usr.user_id');

        $companyId = CheckAuth::getCompanySession();
        $select->joinInner(array('uc' => 'user_company'), 'uc.user_id = usr.user_id', '');
        $select->where("uc.company_id = {$companyId}");

        $results = $this->getAdapter()->fetchAll($select);


        if ($asArray) {
            $data = array();
            if ($results) {
                foreach ($results as $result) {
                    $data[$result['user_id']] = ucwords($result['username']) . ($result['business_name'] ? " - " . ucwords($result['business_name']) : '');
                }
            }
            return $data;
        } else {
            return $results;
        }
    }

    /**
     * get table rows according to the contractor role id 
     * as array to choose @return as array 'user_id' => business_name or the query result
     * 
     * @param tinyint $asArray
     * @return array 
     */
    public function getAllContractorDetails($asArray = 0) {

        $modelAuthRole = new Model_AuthRole();
        $modelUserCompanies = new Model_UserCompanies();

        $select = $this->getAdapter()->select();
        $select->from(array('usr' => $this->_name));
        $role = $modelAuthRole->getRoleIdByName('contractor');
        $select->where("usr.role_id = '{$role}'");
        $select->joinInner(array('ci' => 'contractor_info'), 'ci.contractor_id = usr.user_id');

        $results = $this->getAdapter()->fetchAll($select);

        if ($asArray) {
            $data = array();
            if ($results) {
                foreach ($results as $result) {
                    $userCompanies = $modelUserCompanies->getById($result['user_id']);
                    $data[$result['user_id']] =
                            array(
                                'name' => ucwords($result['username']) . ($result['business_name'] ? " - " . ucwords($result['business_name']) : ''),
                                'email' => $result['email1'],
                                'company_id' => $userCompanies['company_id']
                    );
                }
            }
            return $data;
        } else {
            return $results;
        }
    }

    /**
     * get table row according to the assigned $id
     * 
     * @param int $id
     * @return array 
     */
    public function getContractorById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from(array('usr' => $this->_name));

        $modelAuthRole = new Model_AuthRole();
        $role = $modelAuthRole->getRoleIdByName('contractor');
        $select->where("usr.role_id = '{$role}'");

        $select->joinInner(array('ci' => 'contractor_info'), 'ci.contractor_id = usr.user_id');

        $select->where("usr.user_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table rows according to the assigned $business_name
     * 
     * @param string $business_name
     * @return array 
     */
    public function getContractorIdsByNameSearch($business_name) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name, 'user_id');
        $business_name = $this->getAdapter()->quote('%' . $business_name . '%');
        $select->where("username LIKE {$business_name}");

        $results = $this->getAdapter()->fetchAll($select);

        $data = array();
        if ($results) {
            foreach ($results as $result) {
                $data[] = $result['user_id'];
            }
        }
        return $data;
    }

    public function updateGeneralContractor() {

        //Load model
        $modelContractorInfo = new Model_ContractorInfo();
        $modelAuthRole = new Model_AuthRole();
        $modelServices = new Model_Services();
        $modelContractorService = new Model_ContractorService();
        $modelContractorServiceAvailability = new Model_ContractorServiceAvailability();
        $modelCities = new Model_Cities();
        $modelCompanies = new Model_Companies();


        $companyId = CheckAuth::getCompanySession();
        $company = $modelCompanies->getById($companyId);

        $email = $company['company_enquiries_email'];
        $username = "General {$company['company_name']}";
        $business_name = $company['company_name'];
        $user_code = "General {$company['company_name']}";
        $password = uniqid();

        $cityId = CheckAuth::getCityId();
        $roleId = $modelAuthRole->getRoleIdByName('contractor');


        $generalContractor = $this->getByUserCode(sha1($user_code));

        if (!$generalContractor) {

            $userData = array(
                'username' => $username,
                'password' => sha1($password),
                'user_code' => sha1($user_code),
                'last_login' => time(),
                'created' => time(),
                'city_id' => $cityId,
                'role_id' => $roleId,
                'email1' => $email,
                'email2' => '',
                'email3' => '',
                'mobile1' => '',
                'mobile2' => '',
                'mobile3' => '',
                'phone1' => '',
                'phone2' => '',
                'phone3' => '',
                'fax' => '',
                'emergency_phone' => '',
                'unit_lot_number' => '',
                'street_number' => '',
                'street_address' => '',
                'suburb' => '',
                'state' => '',
                'postcode' => '',
                'po_box' => ''
            );


            $userId = $this->insert($userData);

            $contractorData = array(
                'business_name' => $business_name,
                'contractor_id' => $userId,
                'acn' => '',
                'abn' => '',
                'tfn' => '',
                'gst' => '',
                'gst_date_registered' => 0,
                'insurance_policy_number' => '',
                'insurance_policy_start' => '',
                'insurance_policy_expiry' => '',
                'insurance_listed_services_covered' => '',
                'drivers_licence_number' => '',
                'drivers_licence_expiry' => '',
                'bond_to_be_withheld' => '',
                'commission' => ''
            );

            $modelContractorInfo->insert($contractorData);

            $contractor_id = $userId;
        } else {

            $contractor = $modelContractorInfo->getByContractorId($generalContractor['user_id']);
            $contractor_id = $generalContractor['user_id'];
        }

        if ($contractor_id) {

            $userCompaniesModel = new Model_UserCompanies();
            $userCompanies = $userCompaniesModel->getByUserId($contractor_id);

            $dataCompany = array(
                'user_id' => $contractor_id,
                'company_id' => $companyId
            );
            if ($userCompanies) {
                $userCompaniesModel->updateById($userCompanies['id'], $dataCompany);
            } else {
                $dataCompany['created'] = time();
                $userCompaniesModel->insert($dataCompany);
            }

            $filters = array();
            $filters['company_id'] = $companyId;
            $services = $modelServices->getAll($filters);

            $cities = $modelCities->getAll();

            foreach ($services as $service) {
                $contractorService = $modelContractorService->getByContractorIdAndServiceId($contractor_id, $service['service_id']);

                $data = array(
                    'contractor_id' => $contractor_id,
                    'service_id' => $service['service_id']
                );
                if ($contractorService) {
                    $modelContractorService->updateById($contractorService['contractor_service_id'], $data);
                    $contractorServiceId = $contractorService['contractor_service_id'];
                } else {
                    $contractorServiceId = $modelContractorService->insert($data);
                }

                foreach ($cities as $city) {

                    $contractorServiceAvailability = $modelContractorServiceAvailability->getByCityIdAndContractorServiceId($city['city_id'], $contractorServiceId);
                    $data = array(
                        'contractor_service_id' => $contractorServiceId,
                        'city_id' => $city['city_id']
                    );
                    if ($contractorServiceAvailability) {
                        $modelContractorServiceAvailability->updateById($contractorServiceAvailability['id'], $data);
                    } else {
                        $modelContractorServiceAvailability->insert($data);
                    }
                }
            }
        }
    }

    /**
     * get table rows according to the employee role id 
     * as array to choose @return as array 'user_id' => business_name or the query result
     * 
     * @param tinyint $asArray
     * @return array 
     */
    public function getAllEmployee($asArray = 0 , $status = true) {
        $select = $this->getAdapter()->select();
        $select->from(array('usr' => $this->_name));
		
		$companyId = CheckAuth::getCompanySession();
        $select->joinInner(array('uc' => 'user_company'), 'uc.user_id = usr.user_id', '');
        $select->where("uc.company_id = {$companyId}");

        $modelAuthRole = new Model_AuthRole();
        $role = $modelAuthRole->getRoleIdByName('contractor');
        $select->where("usr.role_id != '{$role}'");
		if($status != 'ALL'){
         $select->where("usr.active = '{$status}'");
		}
		
        $results = $this->getAdapter()->fetchAll($select);

        if ($asArray) {
            $data = array();
            if ($results) {
                foreach ($results as $result) {
                    $data[$result['user_id']] = ucwords($result['username']);
                }
            }
            return $data;
        } else {
            return $results;
        }
    }

    /**
     * get table rows according to the employee role id 
     * as array to choose @return as array 'user_id' => business_name or the query result
     * 
     * @param tinyint $asArray
     * @return array 
     */
    public function getAllUserAsArray() {
        $select = $this->getAdapter()->select();
        $select->from(array('usr' => $this->_name));

        $results = $this->getAdapter()->fetchAll($select);

        $data = array();
        if ($results) {
            foreach ($results as $result) {
                $data[$result['user_id']] = $result['display_name'] ? ucwords($result['display_name']) : ucwords($result['username']);
            }
        }
        return $data;
    }

    /**
     * get extra info for all the rows
     * @param array $row
     * @param array $types
     * @return array
     */
    public function fills(&$rows, $types = array()) {
        foreach ($rows as &$row) {
            $this->fill($row, $types);
        }
    }

    /**
     * get extra info for each row (Ex: getting the category row by c_id, author row by author id ... etc)
     * 
     * @param array $row
     * @param array $types
     * @return array
     */
    public function fill(&$row, $types = array()) {

        if (in_array('user', $types)) {
            $row['user'] = $this->getById($row['user_id']);
        }
        if (in_array('contractor', $types)) {
            $row['contractor'] = $this->getById($row['contractor_id']);
        }
        
        if (in_array('contractor_info', $types)) {
            if (!$this->modelContractorInfo) {
                $this->modelContractorInfo = new Model_ContractorInfo();
            }
            $contractorId = (isset($row['user_id']) ? $row['user_id'] : (isset($row['contractor_id']) ? $row['contractor_id'] : 0));
            $contractorInfo = $this->modelContractorInfo->getByContractorId($contractorId);
            
            $row['contractor_info'] = $contractorInfo;
            $row['contractor_name'] = ucwords($row['username']) . " - " . ucwords($contractorInfo['business_name']);
        }

        return $row;
    }

    public function deleteRelatedUser($id) {

        //delete data from user_info
        $this->getAdapter()->delete('user_info', "user_id = '{$id}'");

        //delete data from ios_user
        $this->getAdapter()->delete('ios_user', "user_id = '{$id}'");

        //delete data from log_user
        $this->getAdapter()->delete('log_user', "user_id = '{$id}'");

        //delete data from user_company
        $this->getAdapter()->delete('user_company', "user_id = '{$id}'");

        //delete data from company_invoice
        $this->getAdapter()->delete('company_invoice', "user_id = '{$id}'");

        //delete data from email_log
        $this->getAdapter()->delete('email_log', "user_id = '{$id}'");

        //delete data from report
        $this->getAdapter()->delete('report', "user_id = '{$id}'");

        //delete data from code
        $this->getAdapter()->delete('code', "user_id = '{$id}'");

        //delete data from contractor_service
        $this->getAdapter()->delete('contractor_service', "contractor_id = '{$id}'");

        //delete data from contractor_gmail_accounts
        $this->getAdapter()->delete('contractor_gmail_accounts', "contractor_id = '{$id}'");

        //delete data from postal_address
        $this->getAdapter()->delete('postal_address', "contractor_id = '{$id}'");
    }

    public function checkBeforeDeleteUser($id, &$tables = array()) {

        $sucsess = true;

        $select_inquiry = $this->getAdapter()->select();
        $select_inquiry->from('inquiry', 'COUNT(*)');
        $select_inquiry->where("user_id = {$id}");
        $count_inquiry = $this->getAdapter()->fetchOne($select_inquiry);

        if ($count_inquiry) {
            $tables[] = 'inquiry';
            $sucsess = false;
        }

        $select_inquiry_discussion = $this->getAdapter()->select();
        $select_inquiry_discussion->from('inquiry_discussion', 'COUNT(*)');
        $select_inquiry_discussion->where("user_id = {$id}");
        $count_inquiry_discussion = $this->getAdapter()->fetchOne($select_inquiry_discussion);

        if ($count_inquiry_discussion) {
            $tables[] = 'inquiry_discussion';
            $sucsess = false;
        }

        $select_customer_reminder = $this->getAdapter()->select();
        $select_customer_reminder->from('customer_reminder', 'COUNT(*)');
        $select_customer_reminder->where("user_id = {$id}");
        $count_customer_reminder = $this->getAdapter()->fetchOne($select_customer_reminder);

        if ($count_customer_reminder) {
            $tables[] = 'customer_reminder';
            $sucsess = false;
        }

        $select_booking = $this->getAdapter()->select();
        $select_booking->from('booking', 'COUNT(*)');
        $select_booking->where("created_by = {$id}");
        $count_booking = $this->getAdapter()->fetchOne($select_booking);

        if ($count_booking) {
            $tables[] = 'booking';
            $sucsess = false;
        }

        $select_booking_attachment = $this->getAdapter()->select();
        $select_booking_attachment->from('booking_attachment', 'COUNT(*)');
        $select_booking_attachment->where("created_by = {$id}");
        $count_booking_attachment = $this->getAdapter()->fetchOne($select_booking_attachment);

        if ($count_booking_attachment) {
            $tables[] = 'booking_attachment';
            $sucsess = false;
        }

        $select_booking_reminder = $this->getAdapter()->select();
        $select_booking_reminder->from('booking_reminder', 'COUNT(*)');
        $select_booking_reminder->where("user_id = {$id}");
        $count_booking_reminder = $this->getAdapter()->fetchOne($select_booking_reminder);

        if ($count_booking_reminder) {
            $tables[] = 'booking_reminder';
            $sucsess = false;
        }

        $select_booking_discussion = $this->getAdapter()->select();
        $select_booking_discussion->from('booking_discussion', 'COUNT(*)');
        $select_booking_discussion->where("user_id = {$id}");
        $count_booking_discussion = $this->getAdapter()->fetchOne($select_booking_discussion);

        if ($count_booking_discussion) {
            $tables[] = 'booking_discussion';
            $sucsess = false;
        }

        $select_booking_contact_history = $this->getAdapter()->select();
        $select_booking_contact_history->from('booking_contact_history', 'COUNT(*)');
        $select_booking_contact_history->where("user_id = {$id}");
        $count_booking_contact_history = $this->getAdapter()->fetchOne($select_booking_contact_history);

        if ($count_booking_contact_history) {
            $tables[] = 'booking_contact_history';
            $sucsess = false;
        }

        $select_booking_status_history = $this->getAdapter()->select();
        $select_booking_status_history->from('booking_status_history', 'COUNT(*)');
        $select_booking_status_history->where("user_id = {$id}");
        $count_booking_status_history = $this->getAdapter()->fetchOne($select_booking_status_history);

        if ($count_booking_status_history) {
            $tables[] = 'booking_status_history';
            $sucsess = false;
        }

        $select_contractor_share_booking = $this->getAdapter()->select();
        $select_contractor_share_booking->from('contractor_share_booking', 'COUNT(*)');
        $select_contractor_share_booking->where("contractor_id = {$id}");
        $count_contractor_share_booking = $this->getAdapter()->fetchOne($select_contractor_share_booking);

        if ($count_contractor_share_booking) {
            $tables[] = 'contractor_share_booking';
            $sucsess = false;
        }

        $select_contractor_service_booking_temp = $this->getAdapter()->select();
        $select_contractor_service_booking_temp->from('contractor_service_booking_temp', 'COUNT(*)');
        $select_contractor_service_booking_temp->where("contractor_id = {$id}");
        $count_contractor_service_booking_temp = $this->getAdapter()->fetchOne($select_contractor_service_booking_temp);

        if ($count_contractor_service_booking_temp) {
            $tables[] = 'contractor_service_booking_temp';
            $sucsess = false;
        }

        $select_contractor_service_booking = $this->getAdapter()->select();
        $select_contractor_service_booking->from('contractor_service_booking', 'COUNT(*)');
        $select_contractor_service_booking->where("contractor_id = {$id}");
        $count_contractor_service_booking = $this->getAdapter()->fetchOne($select_contractor_service_booking);

        if ($count_contractor_service_booking) {
            $tables[] = 'contractor_service_booking';
            $sucsess = false;
        }

        /*$select_ios_sync = $this->getAdapter()->select();
        $select_ios_sync->from('ios_sync', 'COUNT(*)');
        $select_ios_sync->where("ios_user_id = {$id}");
        $count_ios_sync = $this->getAdapter()->fetchOne($select_ios_sync);

        if ($count_ios_sync) {
            $tables[] = 'ios_sync';
            $sucsess = false;
        }*/

        $select_complaint = $this->getAdapter()->select();
        $select_complaint->from('complaint', 'COUNT(*)');
        $select_complaint->where("user_id = {$id}");
        $count_complaint = $this->getAdapter()->fetchOne($select_complaint);

        if ($count_complaint) {
            $tables[] = 'complaint';
            $sucsess = false;
        }

        $select_complaint_discussion = $this->getAdapter()->select();
        $select_complaint_discussion->from('complaint_discussion', 'COUNT(*)');
        $select_complaint_discussion->where("user_id = {$id}");
        $count_complaint_discussion = $this->getAdapter()->fetchOne($select_complaint_discussion);

        if ($count_complaint_discussion) {
            $tables[] = 'complaint_discussion';
            $sucsess = false;
        }

        $select_payment = $this->getAdapter()->select();
        $select_payment->from('payment', 'COUNT(*)');
        $select_payment->where("user_id = {$id}");
        $count_payment = $this->getAdapter()->fetchOne($select_payment);

        if ($count_payment) {
            $tables[] = 'payment';
            $sucsess = false;
        }

        $select_refund = $this->getAdapter()->select();
        $select_refund->from('refund', 'COUNT(*)');
        $select_refund->where("user_id = {$id}");
        $count_refund = $this->getAdapter()->fetchOne($select_refund);

        if ($count_refund) {
            $tables[] = 'refund';
            $sucsess = false;
        }

        $select_booking_contractor_payment = $this->getAdapter()->select();
        $select_booking_contractor_payment->from('booking_contractor_payment', 'COUNT(*)');
        $select_booking_contractor_payment->where("contractor_id = {$id}");
        $count_booking_contractor_payment = $this->getAdapter()->fetchOne($select_booking_contractor_payment);

        if ($count_booking_contractor_payment) {
            $tables[] = 'booking_contractor_payment';
            $sucsess = false;
        }

        $select_estimate_discussion = $this->getAdapter()->select();
        $select_estimate_discussion->from('estimate_discussion', 'COUNT(*)');
        $select_estimate_discussion->where("user_id = {$id}");
        $count_estimate_discussion = $this->getAdapter()->fetchOne($select_estimate_discussion);

        if ($count_estimate_discussion) {
            $tables[] = 'estimate_discussion';
            $sucsess = false;
        }

        $select_invoice_discussion = $this->getAdapter()->select();
        $select_invoice_discussion->from('invoice_discussion', 'COUNT(*)');
        $select_invoice_discussion->where("user_id = {$id}");
        $count_invoice_discussion = $this->getAdapter()->fetchOne($select_invoice_discussion);

        if ($count_invoice_discussion) {
            $tables[] = 'invoice_discussion';
            $sucsess = false;
        }

        $select_contractor_info = $this->getAdapter()->select();
        $select_contractor_info->from('contractor_info', 'COUNT(*)');
        $select_contractor_info->where("contractor_id = {$id}");
        $count_contractor_info = $this->getAdapter()->fetchOne($select_contractor_info);

        if ($count_contractor_info) {
            $tables[] = 'contractor_info';
            $sucsess = false;
        }

        $select_customer = $this->getAdapter()->select();
        $select_customer->from('customer', 'COUNT(*)');
        $select_customer->where("created_by = {$id}");
        $count_customer = $this->getAdapter()->fetchOne($select_customer);

        if ($count_customer) {
            $tables[] = 'customer';
            $sucsess = false;
        }

        $select_customer_notes = $this->getAdapter()->select();
        $select_customer_notes->from('customer_notes', 'COUNT(*)');
        $select_customer_notes->where("created_by = {$id}");
        $count_customer_notes = $this->getAdapter()->fetchOne($select_customer_notes);

        if ($count_customer_notes) {
            $tables[] = 'customer_notes';
            $sucsess = false;
        }
		
		

        return $sucsess;
    }
	
	// by mona
	
	public function getContractorsByStatus($status = 'ALL') {
        
        $select = $this->getAdapter()->select();
        $select->from(array('usr' => $this->_name),array('user_id','username'));

        $modelAuthRole = new Model_AuthRole();
        $role = $modelAuthRole->getRoleIdByName('contractor');
        $select->where("usr.role_id = '{$role}'");
		if($status != 'ALL'){
		$select->where("usr.active = '{$status}'");

		}
		$companyId = CheckAuth::getCompanySession();
        $select->joinInner(array('uc' => 'user_company'), 'uc.user_id = usr.user_id', '');
        $select->where("uc.company_id = {$companyId}");

		
        return $this->getAdapter()->fetchAll($select);
    }
     //By Haneen

    public function getDefaultCustomerId() {
        
        $select = $this->getAdapter()->select();
        $select->from(array('usr' => $this->_name));
        $select->joinInner(array('ar' => 'auth_role'), 'ar.role_id = usr.role_id', array('usr.user_id'));
        $select->where("ar.role_name = 'Customer'");
        return $this->getAdapter()->fetchOne($select);
    }
	
	/*******Get Contrator By %name%*********IBM*/
    public function getContractorNameByNameSearch($name,$role_id) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $name = $this->getAdapter()->quote('%' . $name . '%');
        $select->where("username LIKE {$name}");
        $select->where("role_id =  {$role_id}");
        $results = $this->getAdapter()->fetchAll($select);
        return $results;
    }
	
    //calculate the profile completeness for contractor.........
    public function calculateProfileCompleteness($user_id) {

        //load models that used for caculate profile completeness     
        $user = $this->getById($user_id);

        $contractorInfoObj = new Model_ContractorInfo();
        $contractorInfo = $contractorInfoObj->getByContractorId($user['user_id']);

        //if ($contractorInfo) {
        //get Insurance
        $contractorInsuranceObj = new Model_ContractorInsurance();
        $contractorInsurances = $contractorInsuranceObj->getAll(array('contractor_info_id' => $contractorInfo['contractor_info_id']));

        //get contractorOwner
        $contractorOwnerObj = new Model_ContractorOwner();
        $contractorOwner = $contractorOwnerObj->getAll(array('contractor_info_id' => $contractorInfo['contractor_info_id']));

        //get Vehicle
        $contractorVehicleObj = new Model_ContractorVehicle();
        $contractorVehicle = $contractorVehicleObj->getByContractorInfoId($contractorInfo['contractor_info_id']);

        //get DeclarationOfChemicals
        $declarationOfChemicalsObj = new Model_DeclarationOfChemicals();
        $declarationOfChemicals = $declarationOfChemicalsObj->getByContractorInfoId($contractorInfo['contractor_info_id']);

        //get DeclarationOfEquipment
        $declarationOfEquipmentObj = new Model_DeclarationOfEquipment();
        $declarationOfEquipment = $declarationOfEquipmentObj->getByContractorInfoId($contractorInfo['contractor_info_id']);


        $completeness = 0;
        if ($user['username'] && $user['mobile1'] && $user['street_number'] && $user['street_address'] && $user['postcode'] && $user['country_name'] && $user['state']) {
            $completeness +=20;
        }

        if ($contractorInfo['business_name'] && ($contractorInfo['acn'] || $contractorInfo['abn'] || $contractorInfo['tfn'])) {
            $completeness +=10;
        }

        if ($contractorInfo['account_name'] && $contractorInfo['account_number'] && $contractorInfo['bsb']) {
            $completeness +=10;
        }

        if ($contractorInfo['drivers_licence_number']) {
            $completeness +=10;
        }

        if (!empty($contractorInsurances)) {
            $completeness +=10;
        }

        if (!empty($contractorOwner)) {
            $completeness +=10;
        }

        if (!empty($contractorVehicle)) {
            $completeness +=10;
        }

        if (!empty($declarationOfChemicals)) {
            $completeness +=10;
        }

        if (!empty($declarationOfEquipment)) {
            $completeness +=10;
        }

        $this->updateById($user_id, array('profile_completeness' => $completeness));
    }


    public function cronJobProfileCompletenessReminder() {

        //get all contractor that the score <100 .........
        $select_users = $this->getAdapter()->select();
        $select_users->from(array('u' => $this->_name));
        $select_users->where("u.profile_completeness < 80");
        $users = $this->getAdapter()->fetchAll($select_users);

        foreach ($users as $user) {
                $uncomplete = $this->findMissingValueInProfileContractor($user['user_id']);

                if (!empty($uncomplete)) {
                    $uncomplete = implode(' ', $uncomplete);
                    $template_params = array(
                        '{display_name}' => $user['display_name'],
                        '{profile_completness}' => $user['profile_completeness'],
                        '{contractor_page}' => 'http://temp.tilecleaners.com.au/settings/contractor-info/' . $user['user_id'],
                        '{missing_part}' => $uncomplete
                    );

                    $modelEmailTemplate = new Model_EmailTemplate();
                    $emailTemplate = $modelEmailTemplate->getEmailTemplate('profile_completeness_reminder', $template_params, 0);

                    $to = array();
                    if ($user['email1'] && filter_var($user['email1'], FILTER_VALIDATE_EMAIL)) {
                        //$to[] = $user['email1'];
                    }
                    if ($user['email2'] && filter_var($user['email2'], FILTER_VALIDATE_EMAIL)) {
                        $to[] = $user['email2'];
                    }
                    if ($user['email3'] && filter_var($user['email3'], FILTER_VALIDATE_EMAIL)) {
                        $to[] = $user['email3'];
                    }

                    $body = $emailTemplate['body'];
                    //$subject = 'Test Email profile';
                    $to = implode(',', $to);

                    $params = array(
                        'to' => $to,
                        'body' => $body,
                        'subject' => $subject
                            //'companyId' => $booking['company_id']
                    );


                    $email_log = array('reference_id' => $user['user_id'], 'type' => 'user');
                    if ($to) {
                        try {
                            EmailNotification::sendEmail($params, 'profile_completeness_reminder', $template_params, $email_log);
                        } catch (Zend_Mail_Transport_Exception $e) {
                            
                        }
                    }
                }
            
        }
    }

    //find the missing value in each profile completeness and return array contain the uncompleted value...
    public function findMissingValueInProfileContractor($user_id) {

        //load models that used for caculate profile completeness     
        $user = $this->getById($user_id);

        $contractorInfoObj = new Model_ContractorInfo();
        $contractorInfo = $contractorInfoObj->getByContractorId($user['user_id']);

        //if ($contractorInfo) {
        //get Insurance
        $contractorInsuranceObj = new Model_ContractorInsurance();
        $contractorInsurances = $contractorInsuranceObj->getAll(array('contractor_info_id' => $contractorInfo['contractor_info_id']));

        //get contractorOwner
        $contractorOwnerObj = new Model_ContractorOwner();
        $contractorOwner = $contractorOwnerObj->getAll(array('contractor_info_id' => $contractorInfo['contractor_info_id']));

        //get Vehicle
        $contractorVehicleObj = new Model_ContractorVehicle();
        $contractorVehicle = $contractorVehicleObj->getByContractorInfoId($contractorInfo['contractor_info_id']);

        //get DeclarationOfChemicals
        $declarationOfChemicalsObj = new Model_DeclarationOfChemicals();
        $declarationOfChemicals = $declarationOfChemicalsObj->getByContractorInfoId($contractorInfo['contractor_info_id']);

        //get DeclarationOfEquipment
        $declarationOfEquipmentObj = new Model_DeclarationOfEquipment();
        $declarationOfEquipment = $declarationOfEquipmentObj->getByContractorInfoId($contractorInfo['contractor_info_id']);

        //calulate profile completeness........
        $uncomplete = array();


        if (!$contractorInfo['business_name']) {
            array_push($uncomplete, '<p style="text-align: center; ">- business name field</p>');
        }
        if (!$contractorInfo['business_name'] && (!$contractorInfo['acn'] || !$contractorInfo['abn'] || !$contractorInfo['tfn'])) {
            array_push($uncomplete, '<p style="text-align: center; ">- at least one of acn or abn or tfn should not be empty</p>');
        }

        if (!$contractorInfo['account_name']) {
            array_push($uncomplete, '<p style="text-align: center; ">- Account name field</p>');
        } elseif (!$contractorInfo['account_number']) {
            array_push($uncomplete, '<p style="text-align: center; ">- Account Number field</p>');
        } elseif (!$contractorInfo['bsb']) {
            array_push($uncomplete, '<p style="text-align: center; ">- bsb field</p>');
        }

        if (!$contractorInfo['drivers_licence_number']) {
            array_push($uncomplete, '<p style="text-align: center; ">- Driver licence information</p>');
        }

        if (empty($contractorInsurances)) {
            array_push($uncomplete, '<p style="text-align: center; ">- Insurance information</p>');
        }

        if (empty($contractorOwner)) {
            array_push($uncomplete, '<p style="text-align: center; ">- Owner information</p>');
        }

        if (empty($contractorVehicle)) {
            array_push($uncomplete, '<p style="text-align: center; ">- Vehicle information</p>');
        }

        if (empty($declarationOfChemicals)) {
            array_push($uncomplete, '<p style="text-align: center; ">- Product information</p>');
        }

        if (empty($declarationOfEquipment)) {
            array_push($uncomplete, '<p style="text-align: center;">- Equipment information</p>');
        }

        return $uncomplete;
    }	
	

}