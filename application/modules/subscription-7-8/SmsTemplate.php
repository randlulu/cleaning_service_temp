<?php
class Model_SmsTemplate extends Zend_Db_Table_Abstract {

    protected $_name = 'sms_template';
	public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order($order);
        if ($filters) {
            if (!empty($filters['keywords'])) {
                $keywords = $this->getAdapter()->quote('%' . $filters['keywords'] . '%');
                $select->where("name LIKE {$keywords}");
            }
        }
        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }
        return $this->getAdapter()->fetchAll($select);
    }
    /**
     * update table rows according to the assigned id and data
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "id = '{$id}'");
    }
	public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("id = '{$id}'");
    }
    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
      public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id = '{$id}'");
        return $this->getAdapter()->fetchRow($select);
    }
	/////////////////////get by type
		public function getAllByType($type) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        //$select->order($order);
		$select->where("type = '{$type}'");
		return $this->getAdapter()->fetchAll($select);
    }
	
	////////////////////////////////////////////get template/////////////////
	    public function getsmsTemplate($template_id =0, $template_params = array(), $companyId = 0) {
       
        $placeholder = array();
        $template_images = array();
        foreach ($template_params as $key => $value) {
            $placeholder[] = $key;
        }
        $message = '';
        if ($template_id) {
            $template = $this->getById($template_id);
			
            if($template) {
                $message = $template['message'];
            foreach($template_params as $key => $value) {
            $message = str_replace($key, $value, $message);
					
                }
            }
        }

        return array('message' => $message);
    }
}