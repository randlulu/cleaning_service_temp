<?php

class Model_PlanTypeCredential extends Zend_Db_Table_Abstract {

    protected $_name = 'plan_type_credential';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order($order);

        if ($filters) {
            if (!empty($filters['keywords'])) {
                $keywords = $this->getAdapter()->quote('%' . $filters['keywords'] . '%');
                $select->where("role_name LIKE {$keywords}");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "id = '{$id}'");
    }

    /**
     * delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("id = '{$id}'");
    }

        /**
     *get table row according to the assigned id
*/
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getByPlanTypeId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("plan_type_id = {$id}");
        return $this->getAdapter()->fetchAll($select);
       // return $this->getAdapter()->fetchRow($select);
    }

    public function getByPlanTypeId2($plan_type_id) {
        $plan_type_id = (int) $plan_type_id;
        $select = $this->getAdapter()->select();
        $select->from(array('ptc' => $this->_name));
        $select->joinInner(array('ac' => 'auth_credential'), 'ptc.auth_credential_id = ac.credential_id'); //not argent needed
        $select->where("ptc.plan_type_id = '{$plan_type_id}'");

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     *delete table row according to the assigned Role Id
     */
    public function deleteByPlanTypeId($plan_type_id) {
        $plan_type_id = (int) $plan_type_id;
        return parent::delete("plan_type_id = '{$plan_type_id}'");
    }

       /**
     *get table row according to the assigned Role Id and Credential Id
     */
    public function getByPlanTypeIdAndCredentialId($plan_type_id, $credential_id) {
        $plan_type_id = (int) $plan_type_id;
        $credential_id = (int) $credential_id;

        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("auth_credential_id = '{$credential_id}'");
        $select->where("plan_type_id = '{$plan_type_id}'");

        return $this->getAdapter()->fetchRow($select);
    }


   public function getCredentialsByParentName($parent_name , $plan_type_id){
        $select1 = $this->getAdapter()->select();
        $select1->from(array('ac'=>'auth_credential') ,  'credential_id');
        $select1->where("ac.credential_name = '{$parent_name}'");

        $parent_data = $this->getAdapter()->fetchRow($select1); 

        $select = $this->getAdapter()->select();
        $select->from(array('ptc'=>$this->_name) , 'ac.credential_name');
        $select->joinRight(array('ac'=>'auth_credential') , 'ac.credential_id = ptc.auth_credential_id' , 'ac.credential_name');
        $select->where("ac.parent_id = '{$parent_data['credential_id']}'");
        $select->where("ptc.plan_type_id = '{$plan_type_id}'");

        $result = $this->getAdapter()->fetchAll($select); 
       return $result;
    }

 
	
	

}