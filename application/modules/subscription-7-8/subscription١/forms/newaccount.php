<?php

class Subscription_Form_newaccount extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('account');
        $router = Zend_Controller_Front::getInstance()->getRouter();

        $company_name = new Zend_Form_Element_Text('company_name');
        $company_name->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'inputstyle','placeholder' => 'Company Name'))
                ->setRequired();
				
        $username_email = new Zend_Form_Element_Text('username_email');
        $username_email->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'inputstyle','placeholder' => 'Email Address'))
                ->setRequired();
				
		$username_email->addValidator(new Zend_Validate_EmailAddress());
				
				
		$password = new Zend_Form_Element_Password('password');
        $password ->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'inputstyle','placeholder'=>'Password'))
                ->setRequired();
				
				
        $password->addValidator(new Zend_Validate_StringLength(array('min' => 6, 'max' => 20)));
        $password_confirm = new Zend_Form_Element_Password('password_confirm');
        $password_confirm->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'inputstyle','placeholder' => 'Confirm Password'))
				
                ->setRequired();		
				$password_confirm->addValidator(new Zend_Validate_StringLength(array('min' => 6, 'max' => 20)));

/*
        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('SUBMIT');
        $button->setAttribs(array('class' => 'btn'));*/


        $this->addElements(array($company_name,$username_email,$password,$password_confirm /*,$button*/));
       
		
        $this->setMethod(Zend_Form::METHOD_POST);
        
		
    }

}

