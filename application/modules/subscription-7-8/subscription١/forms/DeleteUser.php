<?php

class Subscription_Form_DeleteUser extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);
		
       
        $userId = (isset($options['user_id']) ? $options['user_id'] : 0);
       

        $router = Zend_Controller_Front::getInstance()->getRouter();
        $request = Zend_Controller_Front::getInstance()->getRequest();



        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Delete it');
        $button->setAttribs(array('class' => 'btn btn-danger'));


        $this->setMethod('post');
        
        $this->addElements(array($button));
			
        $this->setAction($router->assemble(array(), 'deleteUserSubscription'));	
            
       
    }

}

