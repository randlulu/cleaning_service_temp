<?php

class Subscription_EmployeeController extends Zend_Controller_Action
{

    public function init()
    {
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
        // $this->_helper->layout->setLayout('layout_sub');
        /* Initialize action controller here */
    }

    public function indexAction()
    {

        if (Zend_Auth::getInstance()->hasIdentity()) {
            $loggedEmployee = Zend_Auth::getInstance()->getIdentity();            
            $defaultPage = "/subscription/account";

            $messages = $this->_helper->flashMessenger->getMessages();
            if (count($messages)) {
                foreach ($messages as $message) {
                    $this->_helper->flashMessenger->addMessage(array('type' => $message['type'], 'message' => $message['message']));
                }
            }

            $this->_redirect($defaultPage);
        }
        // get request parameters 
        $keepSign = $this->request->getParam('keep_sign', 0);
        $username = $this->request->getParam('username');
        $password = $this->request->getParam('password');
//
// init action form
//
        $form = new Subscription_Form_Login();

        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $authrezed = $this->getAuthrezed();

                $authrezed->setIdentity($username);
                $authrezed->setCredential(sha1($password));
                // $authrezed->setCredential($password);

                $auth = Zend_Auth::getInstance();
                $result = $auth->authenticate($authrezed);


                if ($result->isValid()) {                 

                    $identity = $authrezed->getResultRowObject();
                    $extr = array('is_employee' => 'octopus');
                    $identity_merged = (object) array_merge((array) $identity, $extr);

                    $authStorge = $auth->getStorage();
                    $authStorge->write($identity_merged);


                    if ($keepSign) {
                        $session = new Zend_Session_Namespace();
                        $session->setExpirationSeconds(86400);
                        // $session->setExpirationSeconds( strtotime('30 day', 0) );
                        Zend_Session::rememberMe();

                    } else {
                        Zend_Session::forgetMe();
                    }
                    var_dump(CheckAuth::afterEmployeeLogin()); exit;  
                    CheckAuth::afterEmployeeLogin();
                                 
                } else {
                    // $form->getElement('username')->setErrors(array('invalid email or password'));
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => 'invalid email or password'));
                    $this->_redirect($this->router->assemble(array(), 'employeeLogin'));
                }
            }
        }
        $this->view->form = $form;   
        // action body
    }

    public function loginAction() {
     // action body
     
    }

    public function logoutAction() {
        CheckAuth::employeeLogout();
        $this->_redirect($this->router->assemble(array(), 'employeeLogin'));

    }

    public function getAuthrezed() {
        $authrezed = new Zend_Auth_Adapter_DbTable(Zend_Db_Table::getDefaultAdapter());
        $authrezed->setTableName('employee')
                ->setIdentityColumn('email')
                ->setCredentialColumn('password')
                ->setCredentialTreatment('? AND active = "TRUE"');

        return $authrezed;

    }


}

