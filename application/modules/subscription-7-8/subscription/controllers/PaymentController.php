<?php

class Subscription_PaymentController extends Zend_Controller_Action {

    private $request;
    private $router;
    private $loggedUser;

    public function init() {
       // parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'invoices';
        $this->view->sub_menu = 'payment';

        //
        // check Auth for logged user
        //
       // $this->loggedUser = CheckAuth::getLoggedUser();
    }

    public function addUsersPaymentAction() {

        CheckAuth::checkLoggedIn();
        $userr         = CheckAuth::getLoggedUser();
        $accountModel  = new Model_Account();
        $planModel     = new Model_Plan();
        $account       = $accountModel->getByCreatedBy($userr['user_id']);
        $dataPlan      = $planModel->getById($account['plan_id']);
     
        if (!$account) exit;

        $amount        = $this->request->getParam('totalAmount');
        $nbrOfUser     = $this->request->getParam('nbrOfUser');
        $operation     = $this->request->getParam('operation');
        $appropaction  = $this->request->getParam('appropaction');
        $nmbExistUsers = $this->request->getParam('ExistOfUser');
        

     //  echo 'amount '.$amount .' n of users' .$nbrOfUser .' operation ' .$operation .' appo action '. $appropaction ; exit;
      if($operation != 'pre-transaction'){
           exit;
       }

      if($appropaction == 'paySunbscription'){
           
            //
           $planId =  $this->request->getParam('finalPlanId');
           $changePlan =  $this->request->getParam('planIdInLoad');
           $subFrom =  $this->request->getParam('subFrom');
           $subTo =  $this->request->getParam('subTo');
           $period =  $this->request->getParam('period');
           $reason =  $this->request->getParam('reason');
           //var_dump($reason); exit;
           $dataPlan      = $planModel->getById($planId );
         //  var_dump($nmbExistUsers , $planId  , $subFrom ,$subTo, $period, $amount); exit; 
           $this->view->appropaction = 'changetopremium';
           $this->view->numusers    = $nmbExistUsers+$nbrOfUser;
           $this->view->fromDate    = $subFrom;
           $this->view->expireDate  = $subTo;

         }else{
            $this->view->appropaction = 'addUsers';
            $this->view->numusers     = $nbrOfUser;
            $this->view->expireDate   = $account['to'] ;
         }
         if($account['account_status'] == 'Subscriber-paid' and $changePlan == 0 ){
         	$this->view->numusers     = $nbrOfUser;
         }
        $this->view->amount     = $amount;
        $this->view->account_id = $account['id'];
        $this->view->planName   = $dataPlan['name'];
        $this->view->user_id    = $userr['user_id'];
        $this->view->reason     = $reason;
        
    }


     public function addUsersPaymentToTransactionAction() {

        CheckAuth::checkLoggedIn();
        $userr = CheckAuth::getLoggedUser();
        $accountModel = new Model_Account();
        $planModel = new Model_Plan();
        $account = $accountModel->getByCreatedBy($userr['user_id']);
        $dataPlan     = $planModel->getById($account['plan_id']);
     
        if (!$account) exit;

        if ($this->request->isPost()) {

            $appropaction = $this->request->getParam('appropaction');
            if($appropaction == 'changetopremium'){ 

                $this->sendTransactionSubscription();
            }    
            else{
                 $this->sendTransaction();   
            }
        }

        exit ; 
    }
    
    
    public function sendTransaction() {
        
        CheckAuth::checkLoggedIn();
        $userr = CheckAuth::getLoggedUser();
        
        $period = 15 ;
        $user_id = $this->request->getParam('user_id');
        $account_id = $this->request->getParam('account_id');
        $amount = $this->request->getParam('amount');
        $num_users = $this->request->getParam('num_users');     

        $SubscriptionPaymentObj = new Model_SubscriptionPayment();      

        $checkValueAmount = $SubscriptionPaymentObj->validateTotalAmount($account_id , $num_users);


       // var_dump($checkValueAmount); exit;

         if( $checkValueAmount == 'failed'){
             exit;
         }

        
        $amount = $checkValueAmount['total_amount'] * 100 ;    
        $CardHolderName = $this->request->getParam('card_name', '');
        $CreditCardNumber = $this->request->getParam('card_number', 0);
        $CreditCardNumber1 = $this->request->getParam('EWAY_CARDNUMBER', 0);
        $CVN = $this->request->getParam('card_cvn', 0);
        $CVN1 = $this->request->getParam('EWAY_CARDCVN', 0);
        $InvoiceDescription =$this->request->getParam('invoice_description');
        $expiry_date = $this->request->getParam('expiry_date', '12/202');
        $expiry_array = explode('/', $expiry_date);
        $ExpiryMonth = $expiry_array[0];
        $ExpiryYear = $expiry_array[1];
        $CurrencyCode = 'AUD';
        

        //$received_date = $this->request->getParam('received_date', '');
        $received_date = date('d M Y');

       // var_dump($CardHolderName); exit;

        $CardDetails = array(
            'Name' => $CardHolderName,
            'Number' => $CreditCardNumber1,
            'ExpiryMonth' => $ExpiryMonth,
            'ExpiryYear' => $ExpiryYear,
            'CVN' => $CVN1,
        );

        $Payment = array(
            'TotalAmount' => $amount ,
            'InvoiceDescription' => $InvoiceDescription,
            'CurrencyCode' => $CurrencyCode
        );

         $Customer = array(
            'FirstName' => $userr['first_name'],
            'LastName' => $userr['last_name'],
            'Street1' => $userr['street_address'] . ' ' . $userr['street_number'],
            'PostalCode' => $userr['postcode'],
            'Email' => $userr['email1'],
            'CardDetails' => $CardDetails
        );


        $transaction = array(
            'Customer' => $Customer,
            'Payment' => $Payment
        );




        $modelPayment = new Model_Payment();


        Zend_Loader::loadFile('include_eway.php', APPLICATION_PATH . '/../library/eway-rapid-php-master');

        if (0) {
            $apiKey = 'C3AB9CgGmWUyn8oaghTUTDS2DyZmB/j5aPOWLaqtO2QT5/aaFD1UJnX9mDM697Vg7tAHWT';
            $apiPassword = 'bdtLryqv';
            $apiEndpoint = \Eway\Rapid\Client::MODE_SANDBOX;
        }
        $apiKey = 'A1001CbqrpVrgR/OJWzGztNull6mdN+cGg/+Pe6aJQtj4dpaByyXOtfhAEuoyj8G2TUDSM';
        $apiPassword = 'nABgHxXz';
        $apiEndpoint = \Eway\Rapid\Client::MODE_SANDBOX;
        /* $apiKey = '44DD7AhRBX08hfSoic2wv9mAF/Bs5St52K15ISLq6EaA29SCLz7GuY+G6eyiJkDLUvRKRU';      
          $apiPassword = '72tQqX16';
          $apiEndpoint = 'production'; */
        $client = \Eway\Rapid::createClient($apiKey, $apiPassword, $apiEndpoint);


        $transaction['TransactionType'] = \Eway\Rapid\Enum\TransactionType::PURCHASE;
        $response = $client->createTransaction(\Eway\Rapid\Enum\ApiMethod::DIRECT, $transaction);


        $modelPayment = new Model_SubscriptionPayment();
         //   var_dump($response); exit;
     
        if ($response->TransactionStatus) {
            
            // var_dump($response) ;  exit;

            $data = array(

                'account_id' => $account_id ,
                'payment_amount' => round($amount, 2),
                'description' => $InvoiceDescription,
                'created_by' => $user_id ,
                'reference' => $response->TransactionID,
                'period' => $period
            );

            $success = $modelPayment->insert($data);
            if ($success) {

                 
                 /// here to update the account how paied for new users                  
                   $accountModel = new Model_Account();
                   $account = $accountModel->getByCreatedBy($userr['user_id']);  
  
                  $data = array(
                   'paid_user_count' => $account['paid_user_count'] + $num_users,
                  );

                $success = $accountModel->updateById($account['id'], $data);        

                if($success)    {
                     $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Add successfully"));
                    $this->_redirect($this->router->assemble(array(), 'pagesubscription'));
                  
                }           
                 
            }



        } else {
            if ($response->getErrors()) {
                foreach ($response->getErrors() as $error) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Error: " . \Eway\Rapid::getMessage($error)));
                    $this->_redirect($this->router->assemble(array(), 'pagesubscription'));
                }
            } else {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => 'Sorry, your payment was declined'));
                $this->_redirect($this->router->assemble(array(), 'pagesubscription'));
            }
        }

        exit;
    }



    public function  sendTransactionSubscription(){
        CheckAuth::checkLoggedIn();
        $userr = CheckAuth::getLoggedUser();

        $accountModel = new Model_Account();
        $planModel = new Model_Plan();
        $account = $accountModel->getByCreatedBy($userr['user_id']); 
        $dataPlan     = $planModel->getById($account['plan_id']);
        $exx=$this->getExpireDatetoPremium(); 
           
        $period = $exx['period'];
        $user_id = $this->request->getParam('user_id');
        $account_id = $this->request->getParam('account_id');
        $amount = $this->request->getParam('amount');
        $num_users = $this->request->getParam('num_users');     

        $SubscriptionPaymentObj = new Model_SubscriptionPayment();      


        $amount = $amount  *100 ;
       //  var_dump($amount) ; exit; 
        $CardHolderName = $this->request->getParam('card_name', '');
        $CreditCardNumber = $this->request->getParam('card_number', 0);
        $CreditCardNumber1 = $this->request->getParam('EWAY_CARDNUMBER', 0);
        $CVN = $this->request->getParam('card_cvn', 0);
        $CVN1 = $this->request->getParam('EWAY_CARDCVN', 0);
        $InvoiceDescription =$this->request->getParam('invoice_description');
        $expiry_date = $this->request->getParam('expiry_date', '12/202');
        $expiry_array = explode('/', $expiry_date);
        $ExpiryMonth = $expiry_array[0];
        $ExpiryYear = $expiry_array[1];
        $CurrencyCode = 'AUD';
        

        //$received_date = $this->request->getParam('received_date', '');
        $received_date = date('d M Y');

       // var_dump($CardHolderName); exit;

        $CardDetails = array(
            'Name' => $CardHolderName,
            'Number' => $CreditCardNumber1,
            'ExpiryMonth' => $ExpiryMonth,
            'ExpiryYear' => $ExpiryYear,
            'CVN' => $CVN1,
        );

        $Payment = array(
            'TotalAmount' => $amount ,
            'InvoiceDescription' => $InvoiceDescription,
            'CurrencyCode' => $CurrencyCode
        );

         $Customer = array(
            'FirstName' => $userr['first_name'],
            'LastName' => $userr['last_name'],
            'Street1' => $userr['street_address'] . ' ' . $userr['street_number'],
            'PostalCode' => $userr['postcode'],
            'Email' => $userr['email1'],
            'CardDetails' => $CardDetails
        );


        $transaction = array(
            'Customer' => $Customer,
            'Payment' => $Payment
        );


       // var_dump($Payment) ; exit ;




        $modelPayment = new Model_Payment();


        Zend_Loader::loadFile('include_eway.php', APPLICATION_PATH . '/../library/eway-rapid-php-master');

        if (0) {
            $apiKey = 'C3AB9CgGmWUyn8oaghTUTDS2DyZmB/j5aPOWLaqtO2QT5/aaFD1UJnX9mDM697Vg7tAHWT';
            $apiPassword = 'bdtLryqv';
            $apiEndpoint = \Eway\Rapid\Client::MODE_SANDBOX;
        }

        $apiKey = 'A1001CbqrpVrgR/OJWzGztNull6mdN+cGg/+Pe6aJQtj4dpaByyXOtfhAEuoyj8G2TUDSM';
        $apiPassword = 'nABgHxXz';
        $apiEndpoint = \Eway\Rapid\Client::MODE_SANDBOX;
        /* $apiKey = '44DD7AhRBX08hfSoic2wv9mAF/Bs5St52K15ISLq6EaA29SCLz7GuY+G6eyiJkDLUvRKRU';      
          $apiPassword = '72tQqX16';
          $apiEndpoint = 'production'; */
        $client = \Eway\Rapid::createClient($apiKey, $apiPassword, $apiEndpoint);
        $transaction['TransactionType'] = \Eway\Rapid\Enum\TransactionType::PURCHASE;
        $response = $client->createTransaction(\Eway\Rapid\Enum\ApiMethod::DIRECT, $transaction);


      //  var_dump($response) ;  exit;

        if ($response->TransactionStatus) {
            
            // 
            $modelPayment = new Model_SubscriptionPayment();

            $data = array(

                'account_id' => $account_id ,
                'payment_amount' => $amount,
                'description' => $InvoiceDescription,
                'created_by' => $user_id ,
                'reference' => $response->TransactionID,
                'period' => $period
            );

            $success = $modelPayment->insert($data);

            if ($success) {

                /// here to update the account how paied for new users                  
                $accountModel = new Model_Account();
                $account = $accountModel->getByCreatedBy($userr['user_id']); 
                $accountModel->insertInHistoryAccountById($account['id'] , $userr['user_id'] , 'premium');
                 
                $stop_date = new DateTime($account['trial_end_date']);
                $stop_date->format('Y-m-d'); 
                $stop_date->modify('+1 day');
                $from = $stop_date->format('Y-m-d');


//////////////////////// to check the end date to premuim 
               if($dataPlan['charge_period'] == 'monthly'){

                 $stop_date = new DateTime($account['trial_end_date']);
                 $stop_date->format('Y-m-d'); 
                 $stop_date->modify('+1 month');
                 $to = $stop_date->format('Y-m-d');
               }
               else if($dataPlan['charge_period'] == 'annually'){
                
                  $stop_date = new DateTime($account['trial_end_date']);
                  $stop_date->format('Y-m-d'); 
                  $stop_date->modify('+1 year');
                  $to = $stop_date->format('Y-m-d');

               }
              
  
                $data = array(
                    'paid_user_count' => $num_users ,
                    'from'            => $from      ,
                    'to'             =>  $to        ,
                    'account_status' => 'Subscriber-paid'     
                );

                $success = $accountModel->updateById($account['id'], $data);        

                if($success)    {
                     $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "the payment was successfully done "));
                    $this->_redirect($this->router->assemble(array(), 'pagesubscription'));
                  
                }           
                 
            }



        } else {
            if ($response->getErrors()) {
                foreach ($response->getErrors() as $error) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Error: " . \Eway\Rapid::getMessage($error)));
                    $this->_redirect($this->router->assemble(array(), 'pagesubscription'));
                }
            } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => 'Sorry, your payment was declined'));
                    $this->_redirect($this->router->assemble(array(), 'pagesubscription'));
            }
        }

       
         exit;

    }




   

    public function getExpireDatetoPremium(){

        CheckAuth::checkLoggedIn();
        $userr = CheckAuth::getLoggedUser();
        $accountModel = new Model_Account();
        $planModel = new Model_Plan();
        $account = $accountModel->getByCreatedBy($userr['user_id']); 
        $dataPlan     = $planModel->getById($account['plan_id']);

         
               if($dataPlan['charge_period'] == 'monthly'){

                 $stop_date = new DateTime($account['trial_end_date']);
                 $stop_date->format('Y-m-d'); 
                 $stop_date->modify('+1 month');
                 $to = $stop_date->format('Y-m-d');
                 $return['to'] = $to ;
                 $too = strtotime($to);
                 $from = strtotime($account['trial_end_date']);
                 $datediff = $too - $from ;
                 $return['period']=floor($datediff/(60*60*24));
                 return $return ;
                 
               }
               else if($dataPlan['charge_period'] == 'annually'){
                
                  $stop_date = new DateTime($account['trial_end_date']);
                  $stop_date->format('Y-m-d'); 
                  $stop_date->modify('+1 year');
                  $to = $stop_date->format('Y-m-d');
                  return $to ;
               }

    }



}
