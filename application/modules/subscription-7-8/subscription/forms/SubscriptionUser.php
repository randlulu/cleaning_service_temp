<?php

class Subscription_Form_SubscriptionUser extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

// print_r($options);
//exit;
        $this->setName('User');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
		 $typeUpdate = (isset($options['typeUpdate']) ? $options['typeUpdate'] : '');
		
        $user = (isset($options['user']) ? $options['user'] : '');
        $countryId = (isset($options['country_id']) ? $options['country_id'] : 0);
        $optionState = (isset($options['state']) ? $options['state'] : '');
        // $phoneKey = (isset($options['phone_key']) ? $options['phone_key'] : '');
        $companyId = (isset($options['company_id']) ? $options['company_id'] : 0);

        $router = Zend_Controller_Front::getInstance()->getRouter();
        $request = Zend_Controller_Front::getInstance()->getRequest();

        $user_name = (!empty($user['username']) ? $user['username'] : '');
   if ('update' == $mode) {
        $username = new Zend_Form_Element_Text('username');
        $username->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue($user_name);
//        if ($user_name != $request->getParam('username')) {
//            $username->addValidator(new Zend_Validate_Db_NoRecordExists('user', 'username'));
//        }

            $password = new Zend_Form_Element_Password('password');
            $password->setDecorators(array('ViewHelper'))
                    ->addDecorator('Errors', array('class' => 'errors'))
                    ->setRequired()
                    ->setAttribs(array('class' => 'form-control'))
                    ->setValue((!empty($user['password']) ? $user['password'] : ''))
                    ->addValidator(new Zend_Validate_StringLength(array('min' => 6, 'max' => 20)));

        $display_name = new Zend_Form_Element_Text('display_name');
        $display_name->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($user['display_name']) ? $user['display_name'] : ''));
   }
//        $roleId = new Zend_Form_Element_Select('role_id');
//        $roleId->setDecorators(array('ViewHelper'))
//                ->addDecorator('Errors', array('class' => 'errors'))
//                ->setRequired()
//                ->setAttribs(array('class' => 'form-control', 'onchange' => 'getFeildByRoleId()'))
//                ->setValue((!empty($user['role_id']) ? $user['role_id'] : ''));
//        $table = new Model_AuthRole();
//        $roleId->addMultiOption('', 'Select One');
//        foreach ($table->getRoleAsArray() as $r) {
//            $roleId->addMultiOption($r['id'], $r['name']);
//        }
//   if ('update' == $mode) {
//        $company_id = new Zend_Form_Element_Select('company_id');
//        $company_id->setDecorators(array('ViewHelper'))
//                ->addDecorator('Errors', array('class' => 'errors'))
//                ->setAttribs(array('class' => 'form-control'))
//                ->setValue((!empty($companyId) ? $companyId : ''));
//        $table = new Model_Companies();
//        $company_id->setRequired();
//        $company_id->addMultiOption('', 'Select One');
//        foreach ($table->getCompaniesAsArray() as $r) {
//            $company_id->addMultiOption($r['id'], $r['name']);
//        }
//   }
        $userEmail1 = (!empty($user['email1']) ? $user['email1'] : '');
        $email1 = new Zend_Form_Element_Text('email1');
        $email1->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue($userEmail1);
        $email1->addValidator(new Zend_Validate_EmailAddress());
        if ($request->getParam('email1') && $email1->isValid($request->getParam('email1')) && $userEmail1 != $request->getParam('email1')) {
            $email1->addValidator(new Zend_Validate_Db_NoRecordExists('user', 'email1'));
        }


   if ('update' == $mode) {
        $email2 = new Zend_Form_Element_Text('email2');
        $email2->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($user['email2']) ? $user['email2'] : ''));
        $email2->addValidator(new Zend_Validate_EmailAddress());
   
     
        $email3 = new Zend_Form_Element_Text('email3');
        $email3->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($user['email3']) ? $user['email3'] : ''));
        $email3->addValidator(new Zend_Validate_EmailAddress());
      
        
		$systemEmail = new Zend_Form_Element_Text('systemEmail');
        $systemEmail->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($user['system_email']) ? $user['system_email'] : ''));
        $systemEmail->addValidator(new Zend_Validate_EmailAddress());
   }
		
		
        $first_name = new Zend_Form_Element_Text('first_name');
        $first_name->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors')) 			
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($user['first_name']) ? $user['first_name'] : ''))
				->setRequired();
		
		
		$last_name = new Zend_Form_Element_Text('last_name');
        $last_name->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'form-control'))
				->setRequired()
                ->setValue((!empty($user['last_name']) ? $user['last_name'] : ''));

   if ('update' == $mode) {
		$mobile1 = new Zend_Form_Element_Text('mobile1');
        $mobile1->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($user['mobile1']) ? substr($user['mobile1'] , -8,8) : ''));
   
     

        $mobile2 = new Zend_Form_Element_Text('mobile2');
        $mobile2->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($user['mobile2']) ? substr($user['mobile2'] , -8,8) : ''));
      
    
        $mobile3 = new Zend_Form_Element_Text('mobile3');
        $mobile3->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($user['mobile3']) ? substr($user['mobile3'] , -8,8) : ''));

        $phone1 = new Zend_Form_Element_Text('phone1');
        $phone1->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($user['phone1']) ? substr($user['phone1'] , -8,8) : ''));

        $phone2 = new Zend_Form_Element_Text('phone2');
        $phone2->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($user['phone2']) ? substr($user['phone2'] , -8,8) : ''));

        $phone3 = new Zend_Form_Element_Text('phone3');
        $phone3->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($user['phone3']) ? substr($user['phone3'] , -8,8) : ''));

        $fax = new Zend_Form_Element_Text('fax');
        $fax->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($user['fax']) ? $user['fax'] : ''));

        $emergencyPhone = new Zend_Form_Element_Text('emergency_phone');
        $emergencyPhone->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($user['emergency_phone']) ? $user['emergency_phone'] : ''));

        $unitLotNumber = new Zend_Form_Element_Text('unit_lot_number');
        $unitLotNumber->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($user['unit_lot_number']) ? $user['unit_lot_number'] : ''));

        $streetNumber = new Zend_Form_Element_Text('street_number');
        $streetNumber->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($user['street_number']) ? $user['street_number'] : ''));

        $streetAddress = new Zend_Form_Element_Text('street_address');
        $streetAddress->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($user['street_address']) ? $user['street_address'] : ''));

        $suburb = new Zend_Form_Element_Text('suburb');
        $suburb->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($user['suburb']) ? $user['suburb'] : ''));

   
        //
        //get country & city for ajax
        //
        $city_obj = new Model_Cities();
        $city = $city_obj->getById((!empty($user['city_id']) ? $user['city_id'] : ''));
        // $state_key = $city_obj->getById((!empty($user['state']) ? $user['state'] : CheckAuth::getCityId()));
        // var_dump($state_key); exit;

        $country_id = new Zend_Form_Element_Select('country_id');
        $country_id->removeDecorator('HtmlTag')
                ->removeDecorator('Label')
                ->setAttribs(array('class' => 'form-control', 'onchange' => 'getState();' , 'id'=>'country_id'))
                ->setRequired()
                ->setValue((!empty($city['country_id']) ? $city['country_id'] : ''))
                ->addDecorator('Errors', array('class' => 'errors'));

        $table = new Model_Countries();
        $country_id->addMultiOption('', 'Select One');
        foreach ($table->getCountriesAsArray() as $c) {
            $country_id->addMultiOption($c['id'], $c['name']);
        }

        $state = new Zend_Form_Element_Select('state');
        $state->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control', 'onchange' => 'getCities();' , 'id'=>'state'))
                ->setValue((!empty($city['state']) ? $city['state'] : ''));
                
                // ->setValue((!empty($optionState) ? $optionState : ''));

        $state->addMultiOption('', 'Select One');
        $state->addMultiOptions($city_obj->getStateByCountryId((!empty($countryId) ? $countryId : $city['country_id'])));

        $city_id = new Zend_Form_Element_Select('city_id');
        $city_id->removeDecorator('HtmlTag')
                ->removeDecorator('Label')
                ->setAttribs(array('class' => 'form-control'))
                ->setRequired()
                ->setValue((!empty($city['city_id']) ? $city['city_id'] : ''))
                ->addDecorator('Errors', array('class' => 'errors'));

        $city_id->addMultiOption('', 'Select One');
        $city_id->addMultiOptions($city_obj->getCitiesByCountryIdAndState((!empty($countryId) ? $countryId : $city['country_id']), (!empty($optionState) ? $optionState : $city['state']), true));

        $international_key = new Zend_Form_Element_Text('international_key');
        $international_key->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'form-control'))
                ->setAttribs(array('readonly' => 'true'))
                ->setValue('0061');
                // ->addMultiOption('', '0061');

        $mobile_key = new Zend_Form_Element_Text('mobile_key');
        $mobile_key->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'form-control', 'id' => 'mobile_code'))
                ->setAttribs(array('readonly' => 'true'))
                ->setValue(('04'));

        $phone_key = new Zend_Form_Element_Text('phone_key');
        $phone_key->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'form-control'))
                ->setAttribs(array('readonly' => 'true'))
                ->setValue((!empty($city['phone_key']) ? $city['phone_key'] : ''));
                // ->setValue((!empty($phoneKey) ? $phoneKey : $city['phone_key']));



        $postcode = new Zend_Form_Element_Text('postcode');
        $postcode->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($user['postcode']) ? $user['postcode'] : ''));

        $poBox = new Zend_Form_Element_Text('po_box');
        $poBox->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($user['po_box']) ? $user['po_box'] : ''));
   }
        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));


        $this->setMethod('post');
        if ('update' == $mode) {
            $this->addElements(array($display_name, $username,$last_name,$first_name, $email1, $email2, $email3,$systemEmail, $mobile1,$international_key,$phone_key,$mobile_key, $mobile2, $mobile3, $phone1, $phone2, $phone3, $fax, $emergencyPhone, $unitLotNumber, $streetNumber, $streetAddress, $state, $suburb, $country_id, $city_id, $postcode, $poBox, $button));
		
				$this->setAction($router->assemble(array('id' => $user['user_id']), 'editUserSubscription'));
			
            
        } else {
            $this->addElements(array($last_name,$first_name, $email1, $button));
            $this->setAction($router->assemble(array(), 'addUserSubscription'));
        }
    }

}

