<?php

class WebService_IndexController extends Zend_Controller_Action {

    private $request;
    private $iosLoggedUser;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
    }

    public function indexAction() {

        $email = $this->request->getParam('email');
        $password = $this->request->getParam('password');
        $mode = $this->request->getParam('mode', 'login');
        $uuid = $this->request->getParam('uuid');

        $data = array('authrezed' => 0);

        if (!empty($email) && !empty($password) && !empty($uuid)) { // validate form data
            $authrezed = $this->getAuthrezed();

            $authrezed->setIdentity($email);
            $authrezed->setCredential(sha1($password));

            $auth = Zend_Auth::getInstance();
            $authrezedResult = $auth->authenticate($authrezed);

            if ($authrezedResult->isValid()) {

                $identity = $authrezed->getResultRowObject();
				
				//$identity['ios_user_id'] = '55';
				
                $authStorge = $auth->getStorage();
                $authStorge->write($identity);

                CheckAuth::afterlogin(false);

               $modelIosUser = new Model_IosUser();
               ////check if this is the firs time the user login from this device
				/*$loggedUser = Zend_Auth::getInstance()->getIdentity();
				$loggedBefore = $modelIosUser->getByUserIdAndUUID($loggedUser->user_id, $uuid);
				$ignoreSyncedDataParam = 1;
				if($loggedBefore){
					$ignoreSyncedDataParam = 0;
				}*/
				
				$this->iosLoggedUser = $modelIosUser->afterIosLogin($uuid);
				
                $result = array();

                switch ($mode) {
                    case 'booking':
                        $result = $this->getBooking();
                        break;
					case 'booking_original':
                        $result = $this->getBookingOriginal();
                        break;
                    case 'update_sync':
                        $this->updateSync();
                        break;
                    case 'accept_or_reject_booking':
                        $result = $this->acceptOrRejectBooking();
                        break;
                    case 'add_payment':
                        $result = $this->addPayment();
                        break;
                    case 'payment_types':
                        $result = $this->getPaymentTypes();
                        break;
                    case 'booking_status':
                        $result = $this->getBookingStatus();
                        break;
                    case 'prepare_to_edit':
                        $result = $this->prepareToEdit();
                        break;
                    case 'save_booking':
                        $result = $this->saveBooking();
                        break;
                }

                $data['result'] = $result;
                $data['authrezed'] = 1;
            }
        }

        echo json_encode($data);
        exit;
    }
	
	///new function
	public function loginAction() {

        $email = $this->request->getParam('email');
        $password = $this->request->getParam('password');
        $mode = $this->request->getParam('mode', 'login');
        $uuid = $this->request->getParam('uuid');

        $data = array('authrezed' => 0);

        if (!empty($email) && !empty($password) && !empty($uuid)) { // validate form data
            $authrezed = $this->getAuthrezed();

            $authrezed->setIdentity($email);
            $authrezed->setCredential(sha1($password));

            $auth = Zend_Auth::getInstance();
            $authrezedResult = $auth->authenticate($authrezed);

            if ($authrezedResult->isValid()) {

                $identity = $authrezed->getResultRowObject();

                $authStorge = $auth->getStorage();
                $authStorge->write($identity);

                CheckAuth::afterlogin(false);
				
               $modelIosUser = new Model_IosUser();
               
				$accessToken = $modelIosUser->afterIosLogin($uuid);
				$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
				
				$this->iosLoggedUser = $user['id'];
				
                $result = array('access_token'=> $accessToken);

                $data['result'] = $result;
                $data['authrezed'] = 1;
            }
        }

		// Turn on output buffering with the gzhandler
		//ob_start('ob_gzhandler');
        echo json_encode($data);
        exit;
    }
	
	
	///get all bookings
	public function getAllBookingsAction() {
		$accessToken = $this->request->getParam('access_token',0);
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];
		
		
		$loggedUser = CheckAuth::getLoggedUser();
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if($authrezed){
				$result = $this->getBooking();
				$data['result'] = $result;
				$data['authrezed'] = 1;
			}
			
		}
		else{
			
            $result = $this->getBooking();

            $data['result'] = $result;
            $data['authrezed'] = 1;
		}

        echo json_encode($data);
        exit;
    }
	
	
	public function getAllProductsAction() {
		$accessToken = $this->request->getParam('access_token',0);
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];
		$modelProduct = new Model_Product();

		
		$loggedUser = CheckAuth::getLoggedUser();
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($data);
				exit;
				
			}
			
		}
		$result = $modelProduct->getAll();
		$data['result'] = $result;
		$data['authrezed'] = 1;
		
        echo json_encode($data);
        exit;
    }
	
	public function getAllPropertyTypeAction() {
		$accessToken = $this->request->getParam('access_token',0);
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];
		$modelPropertyType = new Model_PropertyType();

		
		$loggedUser = CheckAuth::getLoggedUser();
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($data);
				exit;
				
			}
			
		}
		$result = $modelPropertyType->getAll();
		$data['result'] = $result;
		$data['authrezed'] = 1;
		
        echo json_encode($data);
        exit;
    }
	
	public function getCallOutFeeAction() {
		$accessToken = $this->request->getParam('access_token',0);
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];
		$modelCompanies = new Model_Companies();

		
		$loggedUser = CheckAuth::getLoggedUser();
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($data);
				exit;
				
			}
			
		}
		$company_id = CheckAuth::getCompanySession();
		$result = $modelCompanies->getById($company_id);
		$data['result'] = $result['call_out_fee'];
		$data['authrezed'] = 1;
		
        echo json_encode($data);
        exit;
    }
	
	public function getAllFloorAction() {
		$accessToken = $this->request->getParam('access_token',0);
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];
		$modelCompanies = new Model_Companies();

		
		$loggedUser = CheckAuth::getLoggedUser();
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($data);
				exit;
				
			}
			
		}
	
		$modelAttributeListValue = new Model_AttributeListValue();
		$result = $modelAttributeListValue->getAllFloor();
		$data['result'] = $result;
		$data['authrezed'] = 1;
		
        echo json_encode($data);
        exit;
    }
	////get unsynced bookings only not all
	public function getUpdatedBookingsAction() {
		$accessToken = $this->request->getParam('access_token',0);
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];
		
		
		$loggedUser = CheckAuth::getLoggedUser();		
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if($authrezed){
				$result = $this->getBooking(true, array());
				$data['result'] = $result;
				$data['authrezed'] = 1;
			}
			
		}
		else{
			
            $result = $this->getBooking(true, array());

            $data['result'] = $result;
            $data['authrezed'] = 1;
		}

        echo json_encode($data);
        exit;
    }
	/////after receiving booking the app should confirm receiving them by sending updateSync request
	/////which asks the server to mark the received booking ids as synced in order to ignore these bookings in the second login
	public function updateSyncAction() {
		
		$rowData = (array)json_decode(file_get_contents("php://input"));
		//print_r($rowData);
		$accessToken = $rowData['access_token'];
		$booking_ids = $rowData['booking_ids'];
		//$accessToken = $this->request->getParam('access_token',0);
		//$accessToken = $this->request->getParam('access_token',0);
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];
		
		$loggedUser = CheckAuth::getLoggedUser();
		$data = array('authrezed' => 0);
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
		
				
        if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if($authrezed){
				$result = $this->updateSync($booking_ids);
				//$data['result'] = $result;
				$data['result'] = 'updated!!!!!';
				$data['authrezed'] = 1;
				
			}
			
		}
		else{
			$result = $this->updateSync($booking_ids);
            //$data['result'] = $result;
            $data['result'] = 'updated!!!!!';
            $data['authrezed'] = 1;
		}

        echo json_encode($data);
        exit;
		
    }
	
	/// accept or reject booking
	public function acceptOrRejectBookingAction() {
		$accessToken = $this->request->getParam('access_token',0);
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];
		
		$loggedUser = CheckAuth::getLoggedUser();		
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
        if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if($authrezed){
				$result = $this->acceptOrRejectBooking();

				$data['result'] = $result;
				$data['authrezed'] = 1;
				
			}
			
		}
		else{
			$result = $this->acceptOrRejectBooking();

            $data['result'] = $result;
            $data['authrezed'] = 1;
		}

        echo json_encode($data);
        exit;
    }
	
	/// add payment
	public function addPaymentAction() {
		$accessToken = $this->request->getParam('access_token',0);
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];
		
		
		$loggedUser = CheckAuth::getLoggedUser();
		//$this->iosLoggedUser = $iosUserId;
		$data = array('authrezed' => 0);
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if($authrezed){
				$result = $this->addPayment();
				$data['result'] = $result;
				$data['authrezed'] = 1;
				
			}
			
		}
		else{
			$result = $this->addPayment();

            $data['result'] = $result;
            $data['authrezed'] = 1;
		}

        echo json_encode($data);
        exit;
    }
	
	/// payment_types
	public function getPaymentTypesAction() {
			
		$accessToken = $this->request->getParam('access_token',0);
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];
		
		
		$loggedUser = CheckAuth::getLoggedUser();
		
		$data = array('authrezed' => 0);
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
        if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			$data['test'] = $authrezed;
			//
			if($authrezed){
				$result = $this->getPaymentTypes();
				$data['result'] = $result;
				$data['authrezed'] = 1;
				
			}
			
		}
		else{
			$result = $this->getPaymentTypes();
            $data['result'] = $result;
            $data['authrezed'] = 1;
		}

        echo json_encode($data);
        exit;
    }
	/// booking_status
	public function getBookingStatusAction() {
				
		$accessToken = $this->request->getParam('access_token',0);
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];
		
		$loggedUser = CheckAuth::getLoggedUser();
		$data = array('authrezed' => 0);
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
        if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if($authrezed){
				$result = $this->getBookingStatus();
				$data['result'] = $result;
				$data['authrezed'] = 1;
				
			}
			
		}
		else{
			$result = $this->getBookingStatus();
            $data['result'] = $result;
            $data['authrezed'] = 1;
		}

        echo json_encode($data);
        exit;
    }
	
	/// prepare_to_edit
	public function prepareToEditAction() {
		
		$accessToken = $this->request->getParam('access_token',0);
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];
		
		$loggedUser = CheckAuth::getLoggedUser();
		
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
        if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if($authrezed){
				$result = $this->prepareToEdit();
				$data['result'] = $result;
				$data['authrezed'] = 1;
				
			}
			
		}
		else{
			$result = $this->prepareToEdit();
            $data['result'] = $result;
            $data['authrezed'] = 1;
		}

        echo json_encode($data);
        exit;
    }
	
	/// save_booking
	public function saveBookingAction() {
			
		$accessToken = $this->request->getParam('access_token',0);
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];
		
		$loggedUser = CheckAuth::getLoggedUser();
		
		$data = array('authrezed' => 0);
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){			
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
        if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if($authrezed){
				$result = $this->saveBooking();
				$data['result'] = $result;
				$data['authrezed'] = 1;
				
			}
			
		}
		else{
			$result = $this->saveBooking();
            $data['result'] = $result;
            $data['authrezed'] = 1;
		}

        echo json_encode($data);
        exit;
    }
	
	public function testAction() {
		$test = $this->request->getParam('test','');
		echo $test;
		echo "done By islam";
		exit;
	}
		/// save_booking offline
	public function saveBookingsOfflineAction(){
			
			$accessToken = $this->request->getParam('access_token',0);
			$os = $this->request->getParam('os','ios');
			$modelIosUser = new Model_IosUser();
			$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
			$this->iosLoggedUser = $user['id'];
			 
			$loggedUser = CheckAuth::getLoggedUser();
			
			$data = array('authrezed' => 0);
			if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){			
				$data['msg'] = 'wrong access token';
				echo json_encode($data);
				exit;
			}
			if (empty($loggedUser)) {			
				//open new session
				$authrezed = $this->openNewSession($accessToken);
				if(!$authrezed){
					echo json_encode($data);
					exit;			
				}			
			}
			if($os == 'android'){
				$rawData = $_POST;
				if(empty($rawData)){
					$rawData = $_GET;
				}
			}
			else{
				$rawData = (array)json_decode(file_get_contents("php://input"));			
			}
			
			//foreach($bookings as $booking){
					//echo 'in the foreach .....';
					//$booking = (array) $booking;
			
					$result = $this->saveBookingOffline('update',$rawData);
			//}
		
			
			
			
			
		   $data['result'] = $result;
           $data['authrezed'] = 1;
		

        echo json_encode($data);
        exit;
    }
	
/////////////////End

    public function getAuthrezed() {
        $modelAuthRole = new Model_AuthRole();
		$contractorRoleId = $modelAuthRole->getRoleIdByName('contractor');
		
        $authrezed = new Zend_Auth_Adapter_DbTable(Zend_Db_Table::getDefaultAdapter());
        $authrezed->setTableName('user')
                ->setIdentityColumn('email1')
                ->setCredentialColumn('password')
                ->setCredentialTreatment("? AND active = 'TRUE' And role_id={$contractorRoleId}");

        return $authrezed;
    }
	
	public function getIosAuthrezed() {
        
        $authrezed = new Zend_Auth_Adapter_DbTable(Zend_Db_Table::getDefaultAdapter());
        $authrezed->setTableName('ios_user')
                ->setIdentityColumn('user_id');

        return $ios_authrezed;
    }

    public function getBooking($ignoreSynced = false, $filters = array()) {
	

        $currentPage = $this->request->getParam('current_page', 0);
        //$ignoreSyncedDataParam = $this->request->getParam('ignore_synced_data', 1);
        
        $modelBooking = new Model_Booking();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelUser = new Model_User();
        $modelContractorinfo = new Model_ContractorInfo();
        $modelServiceAttribute = new Model_ServiceAttribute();
        $modelServiceAttributeValue = new Model_ServiceAttributeValue();
        $modelAttributes = new Model_Attributes();
        $modelAttributeType = new Model_AttributeType();
        $modelAttributeListValue = new Model_AttributeListValue();
        $modelIosSync = new Model_IosSync();
        $modelBookingEstimate = new Model_BookingEstimate();
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelPropertyType = new Model_PropertyType();
        $modelCustomer = new Model_Customer();
        $modelBookingAddress = new Model_BookingAddress();
        $modelServices = new Model_Services();
        $modelBookingStatus = new Model_BookingStatus();

        if ($ignoreSynced) {
            $syncedBookingIds = $modelIosSync->getSyncedBookingIds($this->iosLoggedUser);
            $filters['not_booking_ids'] = $syncedBookingIds;
        }
		else{
			$filters['not_booking_ids'] = $syncedBookingIds;
		}
		
        $limit = 100;
        $perPage = 10;
        if (!$currentPage) {
            $limit = 100;
        }
        $pager = null;
        $bookings = $modelBooking->getAll($filters, 'created DESC', $pager, $limit, $perPage, $currentPage);

        $result = array();
		$modelBookingMultipleDays = new Model_BookingMultipleDays();
		
        foreach ($bookings as $key => $booking) {

            $db_params = array();
            $db_params['is_sync'] = 0;
            $db_params['ios_user_id'] = $this->iosLoggedUser;
            $db_params['booking_id'] = $booking['booking_id'];
			
            $syncedBooking = $modelIosSync->getByBookingIdAndIosUser($booking['booking_id'], $this->iosLoggedUser);
            
			if ($syncedBooking) {
				 
                $modelIosSync->updateById($syncedBooking['id'], $db_params);
            } else {
				
                $modelIosSync->insert($db_params);
            }
			
            //
            //booking
            //
			//////get all dates of this booking 
			$multipleDays = $modelBookingMultipleDays->getByBookingId($booking['booking_id']);
			$result[$key]['multiple_days'] = $multipleDays;
			
            $propertyType = $modelPropertyType->getById($booking['property_type_id']);
            $address = $modelBookingAddress->getByBookingId($booking['booking_id']);
            $is_accepted = $modelBooking->checkBookingIfAccepted($booking['booking_id']);
            $is_rejected = $modelBooking->checkBookingIfRejected($booking['booking_id']);
            $status = $modelBookingStatus->getById($booking['status_id']);

            $result[$key]['booking_num'] = $booking['booking_num'];
            $result[$key]['is_change'] = $booking['is_change'];
            $result[$key]['title'] = $booking['title'];
            $result[$key]['booking_start'] = $booking['booking_start'];
            $result[$key]['booking_end'] = $booking['booking_end'];
            $result[$key]['property_type'] = $propertyType['property_type'];
            $result[$key]['qoute'] = $booking['qoute'];
            $result[$key]['sub_total'] = $booking['sub_total'];
            $result[$key]['total_discount'] = $booking['total_discount'];
            $result[$key]['gst'] = $booking['gst'];
            $result[$key]['paid_amount'] = $booking['paid_amount'];
            $result[$key]['description'] = $booking['description'];
            $result[$key]['status'] = $status['name'];
            $result[$key]['convert_status'] = $booking['convert_status'];
            $result[$key]['created'] = $booking['created'];
            $result[$key]['why'] = $booking['why'];
			///by islam
			if($is_accepted){
				$result[$key]['accept_status'] = 'accepted';
			}
			elseif($is_rejected){
				$result[$key]['accept_status'] = 'rejected';
			}
			else{
				$result[$key]['accept_status'] = 'unknown';
			}
			
            //////
            $result[$key]['original_booking_id'] = $booking['booking_id'];

            //booking address
            $result[$key]['booking_address'] = get_line_address($address);
            $result[$key]['po_box'] = $address['po_box'];
            $result[$key]['postcode'] = $address['postcode'];
            $result[$key]['state'] = $address['state'];
            $result[$key]['street_address'] = $address['street_address'];
            $result[$key]['street_number'] = $address['street_number'];
            $result[$key]['suburb'] = $address['suburb'];
            $result[$key]['unit_lot_number'] = $address['unit_lot_number'];
			///// get booking distance
			$loggedUser = CheckAuth::getLoggedUser();
			$distance = $modelBookingAddress->getDistanceByTwoAddress($loggedUser, $address);
			//$result[$key]['booking_distance'] = number_format($distance, 2);
			$result[$key]['booking_distance'] = $distance;
			////get all products of this booking
			$modelBookingProduct = new Model_BookingProduct();
			$products = $modelBookingProduct->getProductNamesByBookingId($booking['booking_id']);
			$result[$key]['products'] = $products;
			
            //
            //estimate
            //
            $estimate = $modelBookingEstimate->getNotDeletedByBookingId($booking['booking_id']);

            if ($estimate) {
                $result[$key]['estimate']['created'] = date('Y-m-d H:i:s', $estimate['created']);
                $result[$key]['estimate']['estimate_number'] = $estimate['estimate_num'];
                $result[$key]['estimate']['estimate_type'] = $estimate['estimate_type'];
                $result[$key]['estimate']['original_estimate_id'] = $estimate['id'];
            } else {
                $result[$key]['estimate'] = array();
            }


            //
            //invoice
            //
            $invoice = $modelBookingInvoice->getByBookingId($booking['booking_id']);

            if ($invoice) {
                $result[$key]['invoice']['created'] = date('Y-m-d H:i:s', $invoice['created']);
                $result[$key]['invoice']['invoice_number'] = $invoice['invoice_num'];
                $result[$key]['invoice']['invoice_type'] = $invoice['invoice_type'];
                $result[$key]['invoice']['condition_report'] = $invoice['condition_report'];
                $result[$key]['invoice']['original_invoice_id'] = $invoice['id'];
            } else {
                $result[$key]['invoice'] = array();
            }


            //
            //customer
            //
            $customer = $modelCustomer->getById($booking['customer_id']);
            $customer_name = get_customer_name($customer);

            $result[$key]['customer']['name'] = $customer_name;
            $result[$key]['customer']['address'] = get_line_address($customer);
           // $result[$key]['customer']['email1'] = $customer['email1'];
           // $result[$key]['customer']['email2'] = $customer['email2'];
            //$result[$key]['customer']['email3'] = $customer['email3'];
            $result[$key]['customer']['phone1'] = $customer['phone1'];
            $result[$key]['customer']['phone2'] = $customer['phone2'];
            $result[$key]['customer']['phone3'] = $customer['phone3'];
            $result[$key]['customer']['mobile1'] = $customer['mobile1'];
            $result[$key]['customer']['mobile2'] = $customer['mobile2'];
            $result[$key]['customer']['mobile3'] = $customer['mobile3'];
            $result[$key]['customer']['fax'] = $customer['fax'];
            $result[$key]['customer']['city'] = $customer['city_name'];
            $result[$key]['customer']['country'] = $customer['country_name'];
            $result[$key]['customer']['original_customer_id'] = $customer['customer_id'];


            //
            //services & service_attribute & service_attribute_value
            //
            
            $filters = array();
            $filters['booking_id'] = $booking['booking_id'];
            $services = $modelContractorServiceBooking->getAll($filters);

            foreach ($services as $service_key => $service) {


                $serviceId = isset($service['service_id']) ? $service['service_id'] : 0;
                $clone = isset($service['clone']) ? $service['clone'] : 0;
                $bookingId = isset($service['booking_id']) ? $service['booking_id'] : 0;
                $service_info = $modelServices->getById($service['service_id']);

                $contractorServiceBooking = $modelContractorServiceBooking->getByBookingAndServiceAndClone($bookingId, $serviceId, $clone);
                $contractorInfo = $modelContractorinfo->getByContractorId($contractorServiceBooking['contractor_id']);
                $userInfo = $modelUser->getById($contractorServiceBooking['contractor_id']);

                $contractorName = ucwords($userInfo['username']) . ucwords($contractorInfo['business_name'] ? " - " . $contractorInfo['business_name'] : '');

                $unit_price = 0;
                $qty = 0;

                //get the service attribute id
                $service_attributes = $modelServiceAttribute->getByServiceId($serviceId);
                foreach ($service_attributes as $service_attribute) {

                    $attribute = $modelAttributes->getById($service_attribute['attribute_id']);

                    //get the service attribute value
                    $attributeValue = $modelServiceAttributeValue->getByBookingIdAndServiceAttributeIdAndClone($bookingId, $service_attribute['service_attribute_id'], $clone);
					$unit_price = $attributeValue['value'];
                    switch ($attribute['attribute_name']) {
                        case 'Quantity':
                            $qty = $attributeValue['value'];
                            break;
                        case 'Price':
                            $unit_price = $attributeValue['value'];
                            break;
                        default:
                            $atributetype = $modelAttributeType->getById($attribute['attribute_type_id']);

                            $values = array(); 

                            // check if the attribute type is list
                            if ($atributetype['is_list']) {
                                // check if the attribute value is serialized
                                if ($attributeValue['is_serialized_array']) {
                                    $unserializeValues = unserialize($attributeValue['value']);
                                    foreach ($unserializeValues as $unserializeKey => $unserializeValue) {
                                        $attributeListValue = $modelAttributeListValue->getById($unserializeValue);

                                        if ($attributeValue['service_attribute_value_id']) {
                                            $attribute_value = array();
                                            $attribute_value['value'] = $attributeListValue['attribute_value'];
                                            $attribute_value['original_service_attribute_value_id'] = $attributeValue['service_attribute_value_id'] . '_' . $unserializeKey;
                                            $values[] = $attribute_value;
                                        }
                                    }
                                } else {
                                    $attributeListValue = $modelAttributeListValue->getById($attributeValue['value']);

                                    if ($attributeValue['service_attribute_value_id']) {
                                        $attribute_value = array();
                                        $attribute_value['value'] = $attributeListValue['attribute_value'];
                                        $attribute_value['original_service_attribute_value_id'] = $attributeValue['service_attribute_value_id'];
                                        $values[] = $attribute_value;
                                    }
                                }
                            } else {
                                if ($attributeValue['service_attribute_value_id']) {
                                    $attribute_value = array();
                                    $attribute_value['value'] = $attributeValue['value'];
                                    $attribute_value['original_service_attribute_value_id'] = $attributeValue['service_attribute_value_id'];
                                    $values[] = $attribute_value;
                                }
                            }

                            if ($attribute['attribute_name']) {

                                $attribute_name = array();
                                $attribute_name['name'] = $attribute['attribute_name'];
                                $attribute_name['original_service_attribute_id'] = $service['id'] . '_' . $service_attribute['service_attribute_id'];
                                $attribute_name['values'] = $values;

                                $result[$key]['services'][$service_key]['service_attribute'][] = $attribute_name;
                            }
                            break;
                    }
					
					
                    
                }

                $result[$key]['services'][$service_key]['technician'] = $contractorName;
                $result[$key]['services'][$service_key]['service_name'] = $service_info['service_name'];
                $result[$key]['services'][$service_key]['min_price'] = $service_info['min_price'];
                $result[$key]['services'][$service_key]['is_accepted'] = $service['is_accepted'];
                $result[$key]['services'][$service_key]['is_rejected'] = $service['is_rejected'];
                $result[$key]['services'][$service_key]['quantity'] = $qty;
                $result[$key]['services'][$service_key]['unit_price'] = $unit_price;
                $result[$key]['services'][$service_key]['total'] = $modelContractorServiceBooking->getTotalServiceBookingQoute($bookingId, $serviceId, $clone);
                $result[$key]['services'][$service_key]['original_contractor_service_booking_id'] = $service['id'];
                $result[$key]['services'][$service_key]['price_equasion'] = $service_info['price_equasion'];
                $result[$key]['services'][$service_key]['clone'] = $clone;
            }
        }

        $status_colors = array(
			'0' => '#888888',
			'1' => '#cc3333',
			'2' => '#dd4477',
			'3' => '#994499',
			'4' => '#6633cc',
			'5' => '#336699',
			'6' => '#3366cc',
			'7' => '#22aa99',
			'8' => '#329262',
			'9' => '#109618',
			'10' => '#66aa00',
			'11' => '#aaaa11',
			'12' => '#d6ae00',
			'13' => '#ee8800',
			'14' => '#dd5511',
			'15' => '#a87070',
			'16' => '#8c6d8c',
			'17' => '#627487',
			'18' => '#7083a8',
			'19' => '#5c8d87',
			'20' => '#898951',
			'21' => '#b08b59',
			'-1' => '#7a367a'
		);

		$bookingStatus = $modelBookingStatus->getAll();
		$allowedBookingStatus = array();
		foreach($bookingStatus as $key=>$status){
			$allowedBookingStatus[$key]['name']= $status['name'];
			$allowedBookingStatus[$key]['color']= $status_colors[$status['color']];
		}
		$BookingsWithAllowedStatus = array('result'=>$result,'allowedBookingStatus'=>$allowedBookingStatus);
					
        return $BookingsWithAllowedStatus;
    }

    
	
	public function getBookingOriginal($ignoreSynced = true, $filters = array()) {

        $currentPage = $this->request->getParam('current_page', 0);
        $ignoreSyncedDataParam = $this->request->getParam('ignore_synced_data', 0);
        
        $modelBooking = new Model_Booking();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelUser = new Model_User();
        $modelContractorinfo = new Model_ContractorInfo();
        $modelServiceAttribute = new Model_ServiceAttribute();
        $modelServiceAttributeValue = new Model_ServiceAttributeValue();
        $modelAttributes = new Model_Attributes();
        $modelAttributeType = new Model_AttributeType();
        $modelAttributeListValue = new Model_AttributeListValue();
        $modelIosSync = new Model_IosSync();
        $modelBookingEstimate = new Model_BookingEstimate();
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelPropertyType = new Model_PropertyType();
        $modelCustomer = new Model_Customer();
        $modelBookingAddress = new Model_BookingAddress();
        $modelServices = new Model_Services();
        $modelBookingStatus = new Model_BookingStatus();

        if ($ignoreSynced && !$ignoreSyncedDataParam) {
            $syncedBookingIds = $modelIosSync->getSyncedBookingIds($this->iosLoggedUser);
            $filters['not_booking_ids'] = $syncedBookingIds;
        }

        $limit = 0;
        $perPage = 10;
        if (!$currentPage) {
            $limit = 100;
        }
        $pager = null;
        $bookings = $modelBooking->getAll($filters, 'created DESC', $pager, $limit, $perPage, $currentPage);

        $result = array();
        foreach ($bookings as $key => $booking) {

            $db_params = array();
            $db_params['is_sync'] = 0;
            $db_params['ios_user_id'] = $this->iosLoggedUser;
            $db_params['booking_id'] = $booking['booking_id'];

            $syncedBooking = $modelIosSync->getByBookingIdAndIosUser($booking['booking_id'], $this->iosLoggedUser);
            if ($syncedBooking) {
                $modelIosSync->updateById($syncedBooking['id'], $db_params);
            } else {
                $modelIosSync->insert($db_params);
            }

            //
            //booking
            //
            $propertyType = $modelPropertyType->getById($booking['property_type_id']);
            $address = $modelBookingAddress->getByBookingId($booking['booking_id']);
            $is_accepted = $modelBooking->checkBookingIfAccepted($booking['booking_id']);
            $is_rejected = $modelBooking->checkBookingIfRejected($booking['booking_id']);
            $status = $modelBookingStatus->getById($booking['status_id']);

            $result[$key]['booking_num'] = $booking['booking_num'];
            $result[$key]['title'] = $booking['title'];
            $result[$key]['booking_start'] = $booking['booking_start'];
            $result[$key]['booking_end'] = $booking['booking_end'];
            $result[$key]['property_type'] = $propertyType['property_type'];
            $result[$key]['qoute'] = $booking['qoute'];
            $result[$key]['sub_total'] = $booking['sub_total'];
            $result[$key]['total_discount'] = $booking['total_discount'];
            $result[$key]['gst'] = $booking['gst'];
            $result[$key]['paid_amount'] = $booking['paid_amount'];
            $result[$key]['description'] = $booking['description'];
            $result[$key]['status'] = $status['name'];
            $result[$key]['convert_status'] = $booking['convert_status'];
            $result[$key]['created'] = $booking['created'];
			///by islam
			if($is_accepted){
				$result[$key]['accept_status'] = 'accepted';
			}
			elseif($is_rejected){
				$result[$key]['accept_status'] = 'rejected';
			}
			else{
				$result[$key]['accept_status'] = 'unknown';
			}
            //////
			
            
            $result[$key]['original_booking_id'] = $booking['booking_id'];

            //booking address
            $result[$key]['booking_address'] = get_line_address($address);
            $result[$key]['po_box'] = $address['po_box'];
            $result[$key]['postcode'] = $address['postcode'];
            $result[$key]['state'] = $address['state'];
            $result[$key]['street_address'] = $address['street_address'];
            $result[$key]['street_number'] = $address['street_number'];
            $result[$key]['suburb'] = $address['suburb'];
            $result[$key]['unit_lot_number'] = $address['unit_lot_number'];
			///// get booking distance
			$loggedUser = CheckAuth::getLoggedUser();
			$distance = $modelBookingAddress->getDistanceByTwoAddress($loggedUser, $address);
			$result[$key]['booking_distance'] = number_format($distance, 2);
			
            //
            //estimate
            //
            $estimate = $modelBookingEstimate->getNotDeletedByBookingId($booking['booking_id']);

            if ($estimate) {
                $result[$key]['estimate']['created'] = date('Y-m-d H:i:s', $estimate['created']);
                $result[$key]['estimate']['estimate_number'] = $estimate['estimate_num'];
                $result[$key]['estimate']['estimate_type'] = $estimate['estimate_type'];
                $result[$key]['estimate']['original_estimate_id'] = $estimate['id'];
            } else {
                $result[$key]['estimate'] = array();
            }


            //
            //invoice
            //
            $invoice = $modelBookingInvoice->getByBookingId($booking['booking_id']);

            if ($invoice) {
                $result[$key]['invoice']['created'] = date('Y-m-d H:i:s', $invoice['created']);
                $result[$key]['invoice']['invoice_number'] = $invoice['invoice_num'];
                $result[$key]['invoice']['invoice_type'] = $invoice['invoice_type'];
                $result[$key]['invoice']['condition_report'] = $invoice['condition_report'];
                $result[$key]['invoice']['original_invoice_id'] = $invoice['id'];
            } else {
                $result[$key]['invoice'] = array();
            }


            //
            //customer
            //
            $customer = $modelCustomer->getById($booking['customer_id']);
            $customer_name = get_customer_name($customer);

            $result[$key]['customer']['name'] = $customer_name;
            $result[$key]['customer']['address'] = get_line_address($customer);
            $result[$key]['customer']['email1'] = $customer['email1'];
            $result[$key]['customer']['email2'] = $customer['email2'];
            $result[$key]['customer']['email3'] = $customer['email3'];
            $result[$key]['customer']['phone1'] = $customer['phone1'];
            $result[$key]['customer']['phone2'] = $customer['phone2'];
            $result[$key]['customer']['phone3'] = $customer['phone3'];
            $result[$key]['customer']['mobile1'] = $customer['mobile1'];
            $result[$key]['customer']['mobile2'] = $customer['mobile2'];
            $result[$key]['customer']['mobile3'] = $customer['mobile3'];
            $result[$key]['customer']['fax'] = $customer['fax'];
            $result[$key]['customer']['city'] = $customer['city_name'];
            $result[$key]['customer']['country'] = $customer['country_name'];
            $result[$key]['customer']['original_customer_id'] = $customer['customer_id'];


            //
            //services & service_attribute & service_attribute_value
            //
            
            $filters = array();
            $filters['booking_id'] = $booking['booking_id'];
            $services = $modelContractorServiceBooking->getAll($filters);

            foreach ($services as $service_key => $service) {


                $serviceId = isset($service['service_id']) ? $service['service_id'] : 0;
                $clone = isset($service['clone']) ? $service['clone'] : 0;
                $bookingId = isset($service['booking_id']) ? $service['booking_id'] : 0;
                $service_info = $modelServices->getById($service['service_id']);

                $contractorServiceBooking = $modelContractorServiceBooking->getByBookingAndServiceAndClone($bookingId, $serviceId, $clone);
                $contractorInfo = $modelContractorinfo->getByContractorId($contractorServiceBooking['contractor_id']);
                $userInfo = $modelUser->getById($contractorServiceBooking['contractor_id']);

                $contractorName = ucwords($userInfo['username']) . ucwords($contractorInfo['business_name'] ? " - " . $contractorInfo['business_name'] : '');

                $unit_price = 0;
                $qty = 0;

                //get the service attribute id
                $service_attributes = $modelServiceAttribute->getByServiceId($serviceId);
                foreach ($service_attributes as $service_attribute) {

                    $attribute = $modelAttributes->getById($service_attribute['attribute_id']);

                    //get the service attribute value
                    $attributeValue = $modelServiceAttributeValue->getByBookingIdAndServiceAttributeIdAndClone($bookingId, $service_attribute['service_attribute_id'], $clone);

                    switch ($attribute['attribute_name']) {
                        case 'Quantity':
                            $qty = $attributeValue['value'];
                            break;
                        case 'Price':
                            $unit_price = $attributeValue['value'];
                            break;
                        default:
                            $atributetype = $modelAttributeType->getById($attribute['attribute_type_id']);

                            $values = array();

                            // check if the attribute type is list
                            if ($atributetype['is_list']) {
                                // check if the attribute value is serialized
                                if ($attributeValue['is_serialized_array']) {
                                    $unserializeValues = unserialize($attributeValue['value']);
                                    foreach ($unserializeValues as $unserializeKey => $unserializeValue) {
                                        $attributeListValue = $modelAttributeListValue->getById($unserializeValue);

                                        if ($attributeValue['service_attribute_value_id']) {
                                            $attribute_value = array();
                                            $attribute_value['value'] = $attributeListValue['attribute_value'];
                                            $attribute_value['original_service_attribute_value_id'] = $attributeValue['service_attribute_value_id'] . '_' . $unserializeKey;
                                            $values[] = $attribute_value;
                                        }
                                    }
                                } else {
                                    $attributeListValue = $modelAttributeListValue->getById($attributeValue['value']);

                                    if ($attributeValue['service_attribute_value_id']) {
                                        $attribute_value = array();
                                        $attribute_value['value'] = $attributeListValue['attribute_value'];
                                        $attribute_value['original_service_attribute_value_id'] = $attributeValue['service_attribute_value_id'];
                                        $values[] = $attribute_value;
                                    }
                                }
                            } else {
                                if ($attributeValue['service_attribute_value_id']) {
                                    $attribute_value = array();
                                    $attribute_value['value'] = $attributeValue['value'];
                                    $attribute_value['original_service_attribute_value_id'] = $attributeValue['service_attribute_value_id'];
                                    $values[] = $attribute_value;
                                }
                            }

                            if ($attribute['attribute_name']) {

                                $attribute_name = array();
                                $attribute_name['name'] = $attribute['attribute_name'];
                                $attribute_name['original_service_attribute_id'] = $service['id'] . '_' . $service_attribute['service_attribute_id'];
                                $attribute_name['values'] = $values;

                                $result[$key]['services'][$service_key]['service_attribute'][] = $attribute_name;
                            }
                            break;
                    }
                }

                $result[$key]['services'][$service_key]['technician'] = $contractorName;
                $result[$key]['services'][$service_key]['service_name'] = $service_info['service_name'];
                $result[$key]['services'][$service_key]['min_price'] = $service_info['min_price'];
                $result[$key]['services'][$service_key]['is_accepted'] = $service['is_accepted'];
                $result[$key]['services'][$service_key]['is_rejected'] = $service['is_rejected'];
                $result[$key]['services'][$service_key]['quantity'] = $qty;
                $result[$key]['services'][$service_key]['unit_price'] = $unit_price;
                $result[$key]['services'][$service_key]['total'] = $modelContractorServiceBooking->getTotalServiceBookingQoute($bookingId, $serviceId, $clone);
                $result[$key]['services'][$service_key]['original_contractor_service_booking_id'] = $service['id'];
            }
        }

        return $result;
    }
	
	
	
	public function updateSync($booking_ids = array()) {
        //$booking_ids = $this->request->getParam('booking_ids',array());
		//$booking_ids = (array)json_decode(file_get_contents("php://input"));
        $modelIosSync = new Model_IosSync();
		/*echo "test";
		print_r($booking_ids);
		echo "test";*/
        foreach ($booking_ids as $booking_id) {
            $db_params = array();
            $db_params['is_sync'] = 1;
            $db_params['ios_user_id'] = $this->iosLoggedUser;
            $db_params['booking_id'] = $booking_id;

            $syncedBooking = $modelIosSync->getByBookingIdAndIosUser($booking_id, $this->iosLoggedUser);
            if ($syncedBooking) {
                $modelIosSync->updateById($syncedBooking['id'], $db_params);
            } else {
                $modelIosSync->insert($db_params);
            }
        }
		return $booking_ids;
    }

    public function acceptOrRejectBooking() {
        $booking_id = $this->request->getParam('booking_id');
        $accept_status = $this->request->getParam('accept_status');
        $loggedUser = CheckAuth::getLoggedUser();

        $modelContractorServiceBooking = new Model_ContractorServiceBooking();

        $filters = array();
        $filters['booking_id'] = $booking_id;
        if (!CheckAuth::checkCredential(array('canSeeAllBooking'))) {
            $filters['contractor_id'] = $loggedUser['user_id'];
        }
        $bookingServices = $modelContractorServiceBooking->getAll($filters);

        foreach ($bookingServices as $bookingService) {
            if ($accept_status == 'accepted') {
                $modelContractorServiceBooking->updateById($bookingService['id'], array('is_accepted' => 1, 'is_rejected' => 0));
            } else if ($accept_status == 'rejected') {
                $modelContractorServiceBooking->updateById($bookingService['id'], array('is_accepted' => 0, 'is_rejected' => 1));
            }
        }

        return $this->getBooking(false, array('booking_id' => $booking_id));
    }

    /**
     * Add Payment
     */
    public function addPayment() {

        //
        // check Auth for logged user
        //
        if (!CheckAuth::checkCredential(array('paymentAdd'))) {
            return array('type' => 'error', 'message' => "You Don't Have Permission");
        }

        //
        // get request parameters
        //
        $received_date = $this->request->getParam('received_date', date('Y-m-d H:i:s', time()));
        $bankCharges = $this->request->getParam('bank_charges', 0);
        $amount = (float) $this->request->getParam('amount', 0);
        $description = $this->request->getParam('description', '');
        $reference = $this->request->getParam('reference', '');
        $amountWithheld = $this->request->getParam('amount_withheld', 0);
        $withholdingTax = $this->request->getParam('withholding_tax', 0);
        $isAcknowledgment = $this->request->getParam('is_acknowledgment', 0);
        $paymentTypeId = $this->request->getParam('payment_type_id', 0);
        $bookingId = $this->request->getParam('booking_id', 0);
        $loggedUser = CheckAuth::getLoggedUser();

        //
        // Load Model
        //
        $bookingModel = new Model_Booking();
        $modelPayment = new Model_Payment();

        $booking = $bookingModel->getById($bookingId);
        if (!$booking) {
            return array('type' => 'error', 'message' => "Invalid Booking");
        }

        if (!$bookingModel->checkIfCanEditBooking($booking['booking_id'])) {
            return array('type' => 'error', 'message' => "You Don't Have Permission");
        }

        $approvedPayment = $modelPayment->getTotalAmount(array('booking_id' => $booking['booking_id'], 'is_approved' => 'yes'));
        if ($approvedPayment >= $booking['qoute']) {
            return array('type' => 'error', 'message' => "fully Paid");
        }

        $allPayment = $modelPayment->getTotalAmount(array('booking_id' => $booking['booking_id'], 'is_approved' => 'all'));
        if ($allPayment >= $booking['qoute']) {
            return array('type' => 'error', 'message' => "fully Paid,Check Unapproved Payment");
        }

        if ($amount > ($booking['qoute'] - $allPayment)) {
            return array('type' => 'error', 'message' => "Amount Greter than Requaerd");
        }

        if ($amount <= 0) {
            return array('type' => 'error', 'message' => "Invalid Amount");
        }

        //
        // handling the insertion process
        //
        $data = array(
            'received_date' => strtotime($received_date),
            'bank_charges' => round($bankCharges, 2),
            'amount' => round($amount, 2),
            'description' => $description,
            'payment_type_id' => $paymentTypeId,
            'booking_id' => $booking['booking_id'],
            'customer_id' => $booking['customer_id'],
            'user_id' => $loggedUser['user_id'],
            'created' => time(),
            'reference' => $reference,
            'amount_withheld' => round($amountWithheld, 2),
            'withholding_tax' => $withholdingTax,
            'is_acknowledgment' => $isAcknowledgment
        );

        $success = $modelPayment->insert($data);

        if ($success) {
            $modelBookingInvoice = new Model_BookingInvoice();
            $modelBookingInvoice->updateByBookingId($booking['booking_id'], array('invoice_type' => 'open'));
            return array('type' => 'success', 'message' => "Saved successfully, Please Approved this Payment", 'result' => $this->getBooking(false, array('booking_id' => $bookingId)));
        } else {
            return array('type' => 'error', 'message' => "Error in Payment");
        }
    }

    public function getPaymentTypes() {
        $modelPaymentType = new Model_PaymentType();
        return $modelPaymentType->getPaymentTypeAsArray();
    }

    public function getBookingStatus() {
        $modelBookingStatus = new Model_BookingStatus();
        return $modelBookingStatus->getAllStatusAsArrayIOS();
    }

    public function prepareToEdit() {

        $bookingId = $this->request->getParam('booking_id', 0);

        $modelBooking = new Model_Booking();
        $booking = $modelBooking->getById($bookingId);

        $result = array();
        if ($booking) {
            $result['booking_status'] = $this->getBookingStatus();

            $customerSatisfactionLevels = array(
                array('name' => 'Very Dissatisfied', 'value' => 1),
                array('name' => 'Dissatisfied', 'value' => 2),
                array('name' => 'Neutral', 'value' => 3),
                array('name' => 'Satisfied', 'value' => 4),
                array('name' => 'Very Satisfied', 'value' => 5)
            );
            $result['customerSatisfactionLevels'] = $customerSatisfactionLevels;

            $modelProduct = new Model_Product();
            $productNames = $modelProduct->getAllAsIOSArray();
            $result['productNames'] = $productNames; //needs webservice

            $modelCompanies = new Model_Companies();
            $company = $modelCompanies->getById($booking['company_id']);
            $result['callOutFee'] = round($company['call_out_fee'], 2);

            //
            //section_1
            //
            $rows = array();

            $modelCustomer = new Model_Customer();
            $customer = $modelCustomer->getById($booking['customer_id']);
            $customer_name = get_customer_name($customer);

            //row_1
            $row = array();
            $row['type'] = 'textField';
            $row['field']['value'] = !empty($customer_name) ? $customer_name : '';
            $row['field']['enabled'] = "NO";
            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
            $row['name'] = 'customer_id';
            $row['label'] = 'Customer Name';
            $row['hiddenField'] = $customer['customer_id'];
            $row['validation'] = "required";
            $rows[] = $row;

            //fill section
            $result['sections'][] = array('title' => 'Select Customer', 'rows' => $rows);

            //
            //section_2
            //
            $rows = array();

            $modelBookingAddress = new Model_BookingAddress();
            $address = $modelBookingAddress->getByBookingId($booking['booking_id']); //needs webservice
			///very Important
			$modelBookingStatus = new Model_BookingStatus();
            $status = $modelBookingStatus->getById($booking['status_id']);

			$loggedUser = CheckAuth::getLoggedUser();
              
			$allowedForContractor = $modelBooking->checkContractorTimePeriod($booking['booking_id'],$loggedUser['user_id']);
///
		if($allowedForContractor || $status['name'] != 'AWAITING UPDATE' && $status['name'] != 'IN PROGRESS'){
            //row_1
            $row = array();
            $row['type'] = 'textField';
            $row['field']['value'] = !empty($address['unit_lot_number']) ? $address['unit_lot_number'] : '';
            $row['field']['enabled'] = "YES";
            $row['field']['keyboardType'] = 'UIKeyboardTypeNumbersAndPunctuation';
            $row['name'] = 'unit_lot_number';
            $row['label'] = 'Unit/Lot No.';
            $row['hiddenField'] = "NO";
            $row['validation'] = "";
            $rows[] = $row;

            //row_2
            $row = array();
            $row['type'] = 'textField';
            $row['field']['value'] = !empty($address['street_number']) ? $address['street_number'] : '';
            $row['field']['enabled'] = "YES";
            $row['field']['keyboardType'] = 'UIKeyboardTypeNumbersAndPunctuation';
            $row['name'] = 'street_number';
            $row['label'] = 'Street Number';
            $row['hiddenField'] = "NO";
            $row['validation'] = "required";
            $rows[] = $row;

            //row_3
            $row = array();
            $row['type'] = 'textField';
            $row['field']['value'] = !empty($address['street_address']) ? $address['street_address'] : '';
            $row['field']['enabled'] = "YES";
            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
            $row['name'] = 'street_address';
            $row['label'] = 'Street Name';
            $row['hiddenField'] = "NO";
            $row['validation'] = "required";
            $rows[] = $row;
		}
            //row_4
            $row = array();
            $row['type'] = 'textField';
            $row['field']['value'] = !empty($address['suburb']) ? $address['suburb'] : '';
            $row['field']['enabled'] = "YES";
            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
            $row['name'] = 'suburb';
            $row['label'] = 'Suburb';
            $row['hiddenField'] = "NO";
            $row['validation'] = "required";
            $rows[] = $row;
			
			//row_4
            $row = array();
            $row['type'] = 'textField';
            $row['field']['value'] = !empty($address['state']) ? $address['suburb'] : '';
            $row['field']['enabled'] = "YES";
            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
            $row['name'] = 'state';
            $row['label'] = 'state';
            $row['hiddenField'] = "NO";
            $row['validation'] = "required";
            $rows[] = $row;

            //row_5
            $row = array();
            $row['type'] = 'textField';
            $row['field']['value'] = !empty($address['postcode']) ? $address['postcode'] : '';
            $row['field']['enabled'] = "YES";
            $row['field']['keyboardType'] = 'UIKeyboardTypeNumbersAndPunctuation';
            $row['name'] = 'postcode';
            $row['label'] = 'Post Code';
            $row['hiddenField'] = "NO";
            $row['validation'] = "required";
            $rows[] = $row;

		if($allowedForContractor || $status['name'] != 'AWAITING UPDATE' && $status['name'] != 'IN PROGRESS'){
            //row_6
            $row = array();
            $row['type'] = 'textField';
            $row['field']['value'] = !empty($address['po_box']) ? $address['po_box'] : '';
            $row['field']['enabled'] = "YES";
            $row['field']['keyboardType'] = 'UIKeyboardTypeNumbersAndPunctuation';
            $row['name'] = 'po_box';
            $row['label'] = 'P.O Box';
            $row['hiddenField'] = "NO";
            $row['validation'] = "";
            $rows[] = $row;
		}

            //fill section
            $result['sections'][] = array('title' => 'Booking Address', 'rows' => $rows);

            //
            //section_3
            //
            $rows = array();
            
            //row_1
            $row = array();
            $row['type'] = 'bookingStatus';
            $row['field']['value'] = !empty($status['name']) ? $status['name'] : '';
            $row['field']['enabled'] = "YES";
            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
            $row['name'] = 'bookingStatus';
            $row['label'] = 'Booking Status';
            $row['hiddenField'] = !empty($status['booking_status_id']) ? $status['booking_status_id'] : '';
            $row['validation'] = "";
            $rows[] = $row;

            //row_2
            $row = array();
            $row['type'] = 'textField';
            $row['field']['value'] = !empty($booking['title']) ? $booking['title'] : '';
            $row['field']['enabled'] = "NO";
            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
            $row['name'] = 'title';
            $row['label'] = 'Title';
            $row['hiddenField'] = "NO";
            $row['validation'] = "";
            $rows[] = $row;

            //row_3
            $row = array();
            $row['type'] = 'textField';
            $row['field']['value'] = !empty($booking['booking_start']) ? $booking['booking_start'] : '';
            $row['field']['enabled'] = "NO";
            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
            $row['name'] = 'booking_start';
            $row['label'] = 'Start Time';
            $row['hiddenField'] = "NO";
            $row['validation'] = "";
            $rows[] = $row;

            //row_4
            $row = array();
            $row['type'] = 'textField';
            $row['field']['value'] = !empty($booking['booking_end']) ? $booking['booking_end'] : '';
            $row['field']['enabled'] = "NO";
            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
            $row['name'] = 'booking_end';
            $row['label'] = 'End Time';
            $row['hiddenField'] = "NO";
            $row['validation'] = "";
            $rows[] = $row;

            //row_5
            $row = array();
            $modelCities = new Model_Cities();
            $city = $modelCities->getById($booking['city_id']);

            $modelCountries = new Model_Countries();
            $country = $modelCountries->getById($city['country_id']);

            $row['type'] = 'textField';
            $row['field']['value'] = !empty($country['country_name']) ? $country['country_name'] : '';
            $row['field']['enabled'] = "NO";
            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
            $row['name'] = 'country';
            $row['label'] = 'Country';
            $row['hiddenField'] = !empty($country['country_id']) ? $country['country_id'] : '';
            $row['validation'] = "";
            $rows[] = $row;

            //row_6
            $row = array();
            $row['type'] = 'textField';
            $row['field']['value'] = !empty($city['state']) ? $city['state'] : '';
            $row['field']['enabled'] = "NO";
            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
            $row['name'] = 'state';
            $row['label'] = 'State';
            $row['hiddenField'] = "NO";
            $row['validation'] = "";
            $rows[] = $row;

            //row_7
            $row = array();
            $row['type'] = 'textField';
            $row['field']['value'] = !empty($city['city_name']) ? $city['city_name'] : '';
            $row['field']['enabled'] = "NO";
            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
            $row['name'] = 'city_id';
            $row['label'] = 'City';
            $row['hiddenField'] = !empty($city['city_id']) ? $city['city_id'] : '';
            $row['validation'] = "";
            $rows[] = $row;

            //row_8
            $row = array();
            $modelPropertyType = new Model_PropertyType();
            $propertyType = $modelPropertyType->getById($booking['property_type_id']);

            $row['type'] = 'textField';
            $row['field']['value'] = !empty($propertyType['property_type']) ? $propertyType['property_type'] : '';
            $row['field']['enabled'] = "NO";
            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
            $row['name'] = 'property_type';
            $row['label'] = 'Property Type';
            $row['hiddenField'] = !empty($propertyType['id']) ? $propertyType['id'] : '';
            $row['validation'] = "";
            $rows[] = $row;

            //fill section
            $result['sections'][] = array('title' => 'Booking Status', 'rows' => $rows);

            //
            //section_4
            //
            $rows = array();

            //row_1
            $row = array();
            $row['type'] = 'textView';
            $row['field']['value'] = !empty($booking['description']) ? str_replace("\'", "'", $booking['description']) : '';
            $row['field']['enabled'] = "NO";
            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
            $row['name'] = 'description';
            $row['label'] = 'Description';
            $row['hiddenField'] = "NO";
            $row['validation'] = "";
            $rows[] = $row;

            //fill section
            $result['sections'][] = array('title' => 'Booking Description', 'rows' => $rows);

            //
            //section_5
            //
            if ($status) {
                if ($status['name'] != 'TO DO' && $status['name'] != 'IN PROGRESS') {
                    /**
                     * get why Discussion
                     */
                    $modelBookingStatusDiscussion = new Model_BookingStatusDiscussion();
                    $whyDiscussion = $modelBookingStatusDiscussion->getLastDiscussionByBookingIdAndStatusId($bookingId, $booking['status_id']);

                    $rows = array();

                    switch ($status['name']) {
                        case 'AWAITING UPDATE':
                            //row_1
                            $row = array();
                            $row['type'] = 'textView';
                            $row['field']['value'] = !empty($whyDiscussion['user_message']) ? str_replace("\'", "'", $whyDiscussion['user_message']) : '';
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'why';
                            $row['label'] = 'Why Awaiting Update ?';
                            $row['hiddenField'] = "NO";
                            $row['validation'] = "required";
                            $rows[] = $row;
                            break;
                        case 'CANCELLED':
                            //row_1
                            $row = array();
                            $row['type'] = 'textView';
                            $row['field']['value'] = !empty($whyDiscussion['user_message']) ? str_replace("\'", "'", $whyDiscussion['user_message']) : '';
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'why';
                            $row['label'] = 'Why Cancelled ?';
                            $row['hiddenField'] = "NO";
                            $row['validation'] = "required";
                            $rows[] = $row;
                            break;
                        case 'FAILED':
                            //row_1
                            $row = array();
                            $row['type'] = 'textField';
                            $row['field']['value'] = !empty($booking['onsite_client_name']) ? $booking['onsite_client_name'] : '';
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'onsite_client_name';
                            $row['label'] = 'Onsite Client Name';
                            $row['hiddenField'] = "NO";
                            $row['validation'] = "required";
                            $rows[] = $row;

                            //row_2
                            $row = array();
                            $row['type'] = 'dateTime';
                            $row['field']['value'] = !empty($booking['job_start_time']) ? date('Y-m-d H:i:s', $booking['job_start_time']) : '';
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'job_start_time';
                            $row['label'] = 'Job Start Time';
                            $row['hiddenField'] = "NO";
                            $row['validation'] = "required";
                            $rows[] = $row;

                            //row_3
                            $row = array();
                            $row['type'] = 'dateTime';
                            $row['field']['value'] = !empty($booking['job_finish_time']) ? date('Y-m-d H:i:s', $booking['job_finish_time']) : '';
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'job_finish_time';
                            $row['label'] = 'Job Finish Time';
                            $row['hiddenField'] = "NO";
                            $row['validation'] = "required";
                            $rows[] = $row;

                            //row_4
                            $satisfaction = 'Very Dissatisfied';
                            if (!empty($booking['satisfaction'])) {
                                if ($booking['satisfaction'] == 1) {
                                    $satisfaction = 'Very Dissatisfied';
                                }
                                if ($booking['satisfaction'] == 2) {
                                    $satisfaction = 'Dissatisfied';
                                }
                                if ($booking['satisfaction'] == 3) {
                                    $satisfaction = 'Neutral';
                                }
                                if ($booking['satisfaction'] == 4) {
                                    $satisfaction = 'Satisfied';
                                }
                                if ($booking['satisfaction'] == 5) {
                                    $satisfaction = 'Very Satisfied';
                                }
                            }
                            $row = array();
                            $row['type'] = 'pickerView';
                            $row['field']['value'] = $satisfaction;
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'satisfaction';
                            $row['label'] = 'Customer Satisfaction';
                            $row['hiddenField'] = !empty($booking['satisfaction']) ? $booking['satisfaction'] : '1';
                            $row['data'] = $customerSatisfactionLevels;
                            $row['validation'] = "required";
                            $rows[] = $row;

                            //row_5
                            $modelBookingProduct = new Model_BookingProduct();
                            $allBookingProduct = $modelBookingProduct->getByBookingId($bookingId);
                            if ($allBookingProduct) {
                                foreach ($allBookingProduct as $key => $bookingProduct) {
                                    $product = $modelProduct->getById($bookingProduct['product_id']);
                                    if ($product) {
                                        $row = array();
                                        $row['type'] = 'pickerView';
                                        $row['field']['value'] = $product['product'];
                                        $row['field']['enabled'] = "YES";
                                        $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                                        $row['name'] = "product[{$key}]";
                                        $row['label'] = 'Product Name';
                                        $row['hiddenField'] = $bookingProduct['product_id'];
                                        $row['data'] = $productNames;
                                        $row['validation'] = "required";
                                        if ($key != 0) {
                                            $row['removeButton'] = "YES";
                                        }
                                        $rows[] = $row;

                                        $row = array();
                                        $row['type'] = 'textField';
                                        $row['field']['value'] = $bookingProduct['ltr'];
                                        $row['field']['enabled'] = "YES";
                                        $row['field']['keyboardType'] = 'UIKeyboardTypeDecimalPad';
                                        $row['name'] = "ltr[{$key}]";
                                        $row['label'] = 'Product Ltr';
                                        $row['hiddenField'] = 'NO';
                                        $row['validation'] = "required";
                                        $rows[] = $row;
                                    }
                                }
                            } else {
                                $row = array();
                                $row['type'] = 'pickerView';
                                $row['field']['value'] = '';
                                $row['field']['enabled'] = "YES";
                                $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                                $row['name'] = "product[0]";
                                $row['label'] = 'Product Name';
                                $row['hiddenField'] = '';
                                $row['data'] = $productNames;
                                $row['validation'] = "required";
                                $rows[] = $row;

                                $row = array();
                                $row['type'] = 'textField';
                                $row['field']['value'] = '';
                                $row['field']['enabled'] = "YES";
                                $row['field']['keyboardType'] = 'UIKeyboardTypeDecimalPad';
                                $row['name'] = "ltr[0]";
                                $row['label'] = 'Product Ltr';
                                $row['hiddenField'] = 'NO';
                                $row['validation'] = "required";
                                $rows[] = $row;
                            }

                            $row = array();
                            $row['type'] = 'button';
                            $row['field']['value'] = 'Add More Product';
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = "add_more_product";
                            $row['label'] = 'Press here to Add More Product';
                            $row['hiddenField'] = 'NO';
                            $row['validation'] = "";
                            $rows[] = $row;

                            //row_6
                            $row = array();
                            $row['type'] = 'textView';
                            $row['field']['value'] = !empty($whyDiscussion['user_message']) ? str_replace("\'", "'", $whyDiscussion['user_message']) : '';
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'why';
                            $row['label'] = 'Why Failed ?';
                            $row['hiddenField'] = "NO";
                            $row['validation'] = "required";
                            $rows[] = $row;
                            break;
                        case 'COMPLETED':
                            //row_1
                            $row = array();
                            $row['type'] = 'textField';
                            $row['field']['value'] = !empty($booking['onsite_client_name']) ? $booking['onsite_client_name'] : '';
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'onsite_client_name';
                            $row['label'] = 'Onsite Client Name';
                            $row['hiddenField'] = "NO";
                            $row['validation'] = "required";
                            $rows[] = $row;

                            //row_2
                            $row = array();
                            $row['type'] = 'dateTime';
                            $row['field']['value'] = !empty($booking['job_start_time']) ? date('Y-m-d H:i:s', $booking['job_start_time']) : '';
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'job_start_time';
                            $row['label'] = 'Job Start Time';
                            $row['hiddenField'] = "NO";
                            $row['validation'] = "required";
                            $rows[] = $row;

                            //row_3
                            $row = array();
                            $row['type'] = 'dateTime';
                            $row['field']['value'] = !empty($booking['job_finish_time']) ? date('Y-m-d H:i:s', $booking['job_finish_time']) : '';
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'job_finish_time';
                            $row['label'] = 'Job Finish Time';
                            $row['hiddenField'] = "NO";
                            $row['validation'] = "required";
                            $rows[] = $row;

                            $row = array();
                            $row['type'] = 'textView';
                            $row['field']['value'] = !empty($whyDiscussion['user_message']) ? str_replace("\'", "'", $whyDiscussion['user_message']) : '';
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'extra_comments';
                            $row['label'] = 'Extra Comments';
                            $row['hiddenField'] = "NO";
                            $row['validation'] = "";
                            $rows[] = $row;

                            //row_4
                            $satisfaction = 'Very Dissatisfied';
                            if (!empty($booking['satisfaction'])) {
                                if ($booking['satisfaction'] == 1) {
                                    $satisfaction = 'Very Dissatisfied';
                                }
                                if ($booking['satisfaction'] == 2) {
                                    $satisfaction = 'Dissatisfied';
                                }
                                if ($booking['satisfaction'] == 3) {
                                    $satisfaction = 'Neutral';
                                }
                                if ($booking['satisfaction'] == 4) {
                                    $satisfaction = 'Satisfied';
                                }
                                if ($booking['satisfaction'] == 5) {
                                    $satisfaction = 'Very Satisfied';
                                }
                            }
                            $row = array();
                            $row['type'] = 'pickerView';
                            $row['field']['value'] = $satisfaction;
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'satisfaction';
                            $row['label'] = 'Customer Satisfaction';
                            $row['hiddenField'] = !empty($booking['satisfaction']) ? $booking['satisfaction'] : '1';
                            $row['data'] = $customerSatisfactionLevels;
                            $row['validation'] = "required";
                            $rows[] = $row;

                            //row_5
                            $modelBookingProduct = new Model_BookingProduct();
                            $allBookingProduct = $modelBookingProduct->getByBookingId($bookingId);
                            if ($allBookingProduct) {
                                foreach ($allBookingProduct as $key => $bookingProduct) {
                                    $product = $modelProduct->getById($bookingProduct['product_id']);
                                    if ($product) {
                                        $row = array();
                                        $row['type'] = 'pickerView';
                                        $row['field']['value'] = $product['product'];
                                        $row['field']['enabled'] = "YES";
                                        $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                                        $row['name'] = "product[{$key}]";
                                        $row['label'] = 'Product Name';
                                        $row['hiddenField'] = $bookingProduct['product_id'];
                                        $row['data'] = $productNames;
                                        $row['validation'] = "required";
                                        if ($key != 0) {
                                            $row['removeButton'] = "YES";
                                        }
                                        $rows[] = $row;

                                        $row = array();
                                        $row['type'] = 'textField';
                                        $row['field']['value'] = $bookingProduct['ltr'];
                                        $row['field']['enabled'] = "YES";
                                        $row['field']['keyboardType'] = 'UIKeyboardTypeDecimalPad';
                                        $row['name'] = "ltr[{$key}]";
                                        $row['label'] = 'Product Ltr';
                                        $row['hiddenField'] = 'NO';
                                        $row['validation'] = "required";
                                        $rows[] = $row;
                                    }
                                }
                            } else {
                                $row = array();
                                $row['type'] = 'pickerView';
                                $row['field']['value'] = '';
                                $row['field']['enabled'] = "YES";
                                $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                                $row['name'] = "product[0]";
                                $row['label'] = 'Product Name';
                                $row['hiddenField'] = '';
                                $row['data'] = $productNames;
                                $row['validation'] = "required";
                                $rows[] = $row;

                                $row = array();
                                $row['type'] = 'textField';
                                $row['field']['value'] = '';
                                $row['field']['enabled'] = "YES";
                                $row['field']['keyboardType'] = 'UIKeyboardTypeDecimalPad';
                                $row['name'] = "ltr[0]";
                                $row['label'] = 'Product Ltr';
                                $row['hiddenField'] = 'NO';
                                $row['validation'] = "required";
                                $rows[] = $row;
                            }

                            $row = array();
                            $row['type'] = 'button';
                            $row['field']['value'] = 'Add More Product';
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = "add_more_product";
                            $row['label'] = 'Press here to Add More Product';
                            $row['hiddenField'] = 'NO';
                            $row['validation'] = "";
                            $rows[] = $row;

                            break;
                        case 'ON HOLD':
                            //row_1
                            $row = array();
                            $row['type'] = 'textView';
                            $row['field']['value'] = !empty($whyDiscussion['user_message']) ? str_replace("\'", "'", $whyDiscussion['user_message']) : '';
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'why';
                            $row['label'] = 'Why On Hold ?';
                            $row['hiddenField'] = "NO";
                            $row['validation'] = "required";
                            $rows[] = $row;

                            //row_2
                            $row = array();
                            $row['type'] = 'dateTime';
                            $row['field']['value'] = !empty($booking['to_follow']) ? date('Y-m-d H:i:s', $booking['to_follow']) : '';
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'to_follow';
                            $row['label'] = 'To Follow';
                            $row['hiddenField'] = "NO";
                            $row['validation'] = "required";
                            $rows[] = $row;
                            break;
                        case 'TENTATIVE':
                            //row_1
                            $row = array();
                            $row['type'] = 'textView';
                            $row['field']['value'] = !empty($whyDiscussion['user_message']) ? str_replace("\'", "'", $whyDiscussion['user_message']) : '';
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'why';
                            $row['label'] = 'Why Tentative ?';
                            $row['hiddenField'] = "NO";
                            $row['validation'] = "required";
                            $rows[] = $row;
                            break;
                        case 'TO VISIT':
                            //row_1
                            $row = array();
                            $row['type'] = 'textView';
                            $row['field']['value'] = !empty($whyDiscussion['user_message']) ? str_replace("\'", "'", $whyDiscussion['user_message']) : '';
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'why';
                            $row['label'] = 'Why To Visit ?';
                            $row['hiddenField'] = "NO";
                            $row['validation'] = "required";
                            $rows[] = $row;
                            break;
                        case 'QUOTED':
                            //row_1
                            $row = array();
                            $row['type'] = 'switch';
                            $row['field']['value'] = isset($booking['is_to_follow']) ? $booking['is_to_follow'] : 0;
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'is_to_follow';
                            $row['label'] = 'Confirm To Follow';
                            $row['hiddenField'] = "NO";
                            $row['validation'] = "";
                            $rows[] = $row;

                            //row_2
                            $row = array();
                            $row['type'] = 'dateTime';
                            $row['field']['value'] = !empty($booking['to_follow']) ? date('Y-m-d H:i:s', $booking['to_follow']) : '';
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'to_follow';
                            $row['label'] = 'To Follow';
                            $row['hiddenField'] = "NO";
                            $row['validation'] = "required";
                            $rows[] = $row;
                            break;
                    }

                    //fill section
                    $result['sections'][] = array('title' => 'Extra Info', 'rows' => $rows);
                }
            }

            //
            //services & service_attribute & service_attribute_value
            //
            $modelContractorServiceBooking = new Model_ContractorServiceBooking();
            $modelUser = new Model_User();
            $modelContractorinfo = new Model_ContractorInfo();
            $modelServiceAttribute = new Model_ServiceAttribute();
            $modelServiceAttributeValue = new Model_ServiceAttributeValue();
            $modelAttributes = new Model_Attributes();
            $modelAttributeType = new Model_AttributeType();
            $modelAttributeListValue = new Model_AttributeListValue();
            $modelServices = new Model_Services();
            $services = $modelContractorServiceBooking->getAll(array('booking_id' => $booking['booking_id']));

            if ($services) {
                $serviceNum = 0;
                foreach ($services as $service) {

                    $rows = array();

                    $serviceId = isset($service['service_id']) ? $service['service_id'] : 0;
                    $clone = isset($service['clone']) ? $service['clone'] : 0;
                    $bookingId = isset($service['booking_id']) ? $service['booking_id'] : 0;
                    $service_info = $modelServices->getById($service['service_id']);

                    $contractorServiceBooking = $modelContractorServiceBooking->getByBookingAndServiceAndClone($bookingId, $serviceId, $clone);
                    $contractorInfo = $modelContractorinfo->getByContractorId($contractorServiceBooking['contractor_id']);
                    $userInfo = $modelUser->getById($contractorServiceBooking['contractor_id']);

                    $contractorName = ucwords($userInfo['username']) . ucwords($contractorInfo['business_name'] ? " - " . $contractorInfo['business_name'] : '');

                    $row = array();
                    $row['type'] = 'textField';
                    $row['field']['value'] = !empty($contractorName) ? $contractorName : '';
                    $row['field']['enabled'] = "NO";
                    $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                    $row['name'] = 'contractor_' . $serviceId . ($clone ? '_' . $clone : '');
                    $row['label'] = 'Technician';
                    $row['hiddenField'] = $contractorServiceBooking['contractor_id'];
                    $row['validation'] = "";
                    $rows[] = $row;

                    $row = array();
                    $row['type'] = 'textField';
                    $row['field']['value'] = !empty($service_info['service_name']) ? $service_info['service_name'] : '';
                    $row['field']['enabled'] = "NO";
                    $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                    $row['name'] = 'services[' . $serviceNum . ']';
                    $row['label'] = 'Service Name';
                    $row['hiddenField'] = $serviceId . ($clone ? '_' . $clone : '');
                    $row['validation'] = "";
                    $rows[] = $row;


                    //get the service attribute id
                    $service_attributes = $modelServiceAttribute->getByServiceId($serviceId);
                    foreach ($service_attributes as $service_attribute) {

                        $attribute = $modelAttributes->getById($service_attribute['attribute_id']);

                        if ($attribute) {
                            //get the service attribute value
                            $attributeValue = $modelServiceAttributeValue->getByBookingIdAndServiceAttributeIdAndClone($bookingId, $service_attribute['service_attribute_id'], $clone);
                            $atributetype = $modelAttributeType->getById($attribute['attribute_type_id']);

                            $name = 'attribute_' . $service_attribute['service_id'] . $service_attribute['attribute_id'] . ($clone ? '_' . $clone : '');

                            switch ($atributetype['attribute_type']) {
                                case 'checkbox':
                                    $row = array();
                                    $row['type'] = 'switch';
                                    $row['field']['value'] = !empty($attributeValue['value']) ? $attributeValue['value'] : '';
                                    $row['field']['enabled'] = "YES";
                                    $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                                    $row['name'] = $name;
                                    $row['label'] = $attribute['attribute_name'];
                                    $row['hiddenField'] = "NO";
                                    if ($attribute['is_price_attribute']) {
                                        $row['priceAttribute']['name'] = $attribute['attribute_variable_name'];
                                        $row['priceAttribute']['serviceId'] = $service_attribute['service_id'];
                                        $row['priceAttribute']['clone'] = $clone;
                                        $row['priceAttribute']['equasion'] = $service_info['price_equasion'];
                                        $row['priceAttribute']['min_price'] = !empty($service_info['min_price']) ? $service_info['min_price'] : '';
                                    } else {
                                        $row['priceAttribute'] = "NO";
                                    }
                                    $row['validation'] = "";
                                    $rows[] = $row;
                                    break;

                                case 'date':
                                    $row = array();
                                    $row['type'] = 'date';
                                    $row['field']['value'] = !empty($attributeValue['value']) ? $attributeValue['value'] : '';
                                    $row['field']['enabled'] = "YES";
                                    $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                                    $row['name'] = $name;
                                    $row['label'] = $attribute['attribute_name'];
                                    $row['hiddenField'] = "NO";
                                    if ($attribute['is_price_attribute']) {
                                        $row['priceAttribute']['name'] = $attribute['attribute_variable_name'];
                                        $row['priceAttribute']['serviceId'] = $service_attribute['service_id'];
                                        $row['priceAttribute']['clone'] = $clone;
                                        $row['priceAttribute']['equasion'] = $service_info['price_equasion'];
                                        $row['priceAttribute']['min_price'] = !empty($service_info['min_price']) ? $service_info['min_price'] : '';
                                    } else {
                                        $row['priceAttribute'] = "NO";
                                    }
                                    $row['validation'] = "";
                                    $rows[] = $row;
                                    break;

                                case 'time':
                                    $row = array();
                                    $row['type'] = 'time';
                                    $row['field']['value'] = !empty($attributeValue['value']) ? $attributeValue['value'] : '';
                                    $row['field']['enabled'] = "YES";
                                    $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                                    $row['name'] = $name;
                                    $row['label'] = $attribute['attribute_name'];
                                    $row['hiddenField'] = "NO";
                                    if ($attribute['is_price_attribute']) {
                                        $row['priceAttribute']['name'] = $attribute['attribute_variable_name'];
                                        $row['priceAttribute']['serviceId'] = $service_attribute['service_id'];
                                        $row['priceAttribute']['clone'] = $clone;
                                        $row['priceAttribute']['equasion'] = $service_info['price_equasion'];
                                        $row['priceAttribute']['min_price'] = !empty($service_info['min_price']) ? $service_info['min_price'] : '';
                                    } else {
                                        $row['priceAttribute'] = "NO";
                                    }
                                    $row['validation'] = "";
                                    $rows[] = $row;
                                    break;

                                case 'long_text':
                                    $row = array();
                                    $row['type'] = 'textView';
                                    $row['field']['value'] = !empty($attributeValue['value']) ? $attributeValue['value'] : '';
                                    $row['field']['enabled'] = "YES";
                                    $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                                    $row['name'] = $name;
                                    $row['label'] = $attribute['attribute_name'];
                                    $row['hiddenField'] = "NO";
                                    if ($attribute['is_price_attribute']) {
                                        $row['priceAttribute']['name'] = $attribute['attribute_variable_name'];
                                        $row['priceAttribute']['serviceId'] = $service_attribute['service_id'];
                                        $row['priceAttribute']['clone'] = $clone;
                                        $row['priceAttribute']['equasion'] = $service_info['price_equasion'];
                                        $row['priceAttribute']['min_price'] = !empty($service_info['min_price']) ? $service_info['min_price'] : '';
                                    } else {
                                        $row['priceAttribute'] = "NO";
                                    }
                                    $row['validation'] = "";
                                    $rows[] = $row;
                                    break;

                                case 'text_input':
                                    $row = array();
                                    $row['type'] = 'textField';
                                    $row['field']['value'] = !empty($attributeValue['value']) ? $attributeValue['value'] : '';
                                    $row['field']['enabled'] = "YES";
                                    if ($attribute['attribute_variable_name'] == 'price') {
                                        $row['field']['keyboardType'] = 'UIKeyboardTypeNumbersAndPunctuation';
                                    } else if ($attribute['attribute_variable_name'] == 'quantity') {
                                        $row['field']['keyboardType'] = 'UIKeyboardTypeNumbersAndPunctuation';
                                    } else if ($attribute['attribute_variable_name'] == 'discount') {
                                        $row['field']['keyboardType'] = 'UIKeyboardTypeNumbersAndPunctuation';
                                    } else {
                                        $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                                    }
                                    $row['name'] = $name;
                                    $row['label'] = $attribute['attribute_name'];
                                    $row['hiddenField'] = "NO";
                                    if ($attribute['is_price_attribute']) {
                                        $row['priceAttribute']['name'] = $attribute['attribute_variable_name'];
                                        $row['priceAttribute']['serviceId'] = $service_attribute['service_id'];
                                        $row['priceAttribute']['clone'] = $clone;
                                        $row['priceAttribute']['equasion'] = $service_info['price_equasion'];
                                        $row['priceAttribute']['min_price'] = !empty($service_info['min_price']) ? $service_info['min_price'] : '';
                                    } else {
                                        $row['priceAttribute'] = "NO";
                                    }
                                    $row['validation'] = "";
                                    $rows[] = $row;
                                    break;

                                case 'dropdown':
                                case 'enum':
                                    $row = array();
                                    $attributeListValue = $modelAttributeListValue->getById($attributeValue['value']);
                                    $attributeListValues = $modelAttributeListValue->getByAttributeIdAsIosArray($service_attribute['attribute_id']);

                                    $row['type'] = 'pickerView';
                                    $row['field']['value'] = !empty($attributeListValue['attribute_value']) ? $attributeListValue['attribute_value'] : '';
                                    $row['field']['enabled'] = "YES";
                                    $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                                    $row['name'] = $name;
                                    $row['label'] = $attribute['attribute_name'];
                                    $row['hiddenField'] = $attributeValue['value'];
                                    $row['data'] = $attributeListValues;
                                    if ($attribute['is_price_attribute']) {
                                        $row['priceAttribute']['name'] = $attribute['attribute_variable_name'];
                                        $row['priceAttribute']['serviceId'] = $service_attribute['service_id'];
                                        $row['priceAttribute']['clone'] = $clone;
                                        $row['priceAttribute']['equasion'] = $service_info['price_equasion'];
                                        $row['priceAttribute']['min_price'] = !empty($service_info['min_price']) ? $service_info['min_price'] : '';
                                    } else {
                                        $row['priceAttribute'] = "NO";
                                    }
                                    $row['validation'] = "";
                                    $rows[] = $row;
                                    break;


                                case 'list':
                                case 'set':
                                    $row = array();
                                    $attributeListValues = $modelAttributeListValue->getByAttributeIdAsIosArray($service_attribute['attribute_id']);

                                    $row['type'] = 'alPickerView';
                                    if ($attributeValue['is_serialized_array']) {
                                        $values = array();
                                        $unserializeValues = unserialize($attributeValue['value']);
                                        foreach ($unserializeValues as $key => $unserializeValue) {
                                            $attributeListValue = $modelAttributeListValue->getById($unserializeValue);
                                            $values[] = array('name' => $attributeListValue['attribute_value'], 'value' => $attributeListValue['attribute_value_id']);
                                        }
                                        $row['field']['value'] = $values;
                                    } else {
                                        $row['field']['value'] = !empty($attributeListValue['attribute_value']) ? $attributeListValue['attribute_value'] : '';
                                    }
                                    $row['field']['enabled'] = "YES";
                                    $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                                    $row['name'] = $name;
                                    $row['label'] = $attribute['attribute_name'];
                                    $row['hiddenField'] = "NO";
                                    $row['data'] = $attributeListValues;
                                    if ($attribute['is_price_attribute']) {
                                        $row['priceAttribute']['name'] = $attribute['attribute_variable_name'];
                                        $row['priceAttribute']['serviceId'] = $service_attribute['service_id'];
                                        $row['priceAttribute']['clone'] = $clone;
                                        $row['priceAttribute']['equasion'] = $service_info['price_equasion'];
                                        $row['priceAttribute']['min_price'] = !empty($service_info['min_price']) ? $service_info['min_price'] : '';
                                    } else {
                                        $row['priceAttribute'] = "NO";
                                    }
                                    $row['validation'] = "";
                                    $rows[] = $row;
                                    break;
                            }
                        }
                    }


                    $row = array();
                    $row['type'] = 'switch';
                    $row['field']['value'] = isset($service['consider_min_price']) ? $service['consider_min_price'] : 0;
                    $row['field']['enabled'] = "YES";
                    $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                    $row['name'] = 'consider_min_price_' . $serviceId . ($clone ? '_' . $clone : '');
                    $row['label'] = 'Consider Minimum Price ($ ' . (!empty($service_info['min_price']) ? $service_info['min_price'] : '') . ')';
                    $row['priceAttribute']['name'] = 'consider_minimum_price';
                    $row['priceAttribute']['serviceId'] = $serviceId;
                    $row['priceAttribute']['clone'] = $clone;
                    $row['priceAttribute']['equasion'] = $service_info['price_equasion'];
                    $row['priceAttribute']['min_price'] = !empty($service_info['min_price']) ? $service_info['min_price'] : '';
                    $row['hiddenField'] = "NO";
                    $row['validation'] = "";
                    $rows[] = $row;

                    $row = array();
                    $row['type'] = 'textField';
                    $row['field']['value'] = round($modelContractorServiceBooking->getServiceBookingQoute($bookingId, $serviceId, $clone), 2);
                    $row['field']['enabled'] = "NO";
                    $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                    $row['name'] = 'total_' . $serviceId . ($clone ? '_' . $clone : '');
                    $row['label'] = 'Total';
                    $row['hiddenField'] = "NO";
                    $row['validation'] = "";
                    $rows[] = $row;

                    $row = array();
                    $row['type'] = 'textField';
                    $row['field']['value'] = round($modelContractorServiceBooking->getTotalServiceBookingQoute($bookingId, $serviceId, $clone), 2);
                    $row['field']['enabled'] = "NO";
                    $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                    $row['name'] = 'amount_' . $serviceId . ($clone ? '_' . $clone : '');
                    $row['label'] = 'Amount';
                    $row['hiddenField'] = "NO";
                    $row['validation'] = "";
                    $rows[] = $row;

                    $serviceNum++;

                    //fill section
                    $result['sections'][] = array('title' => (!empty($service_info['service_name']) ? $service_info['service_name'] : ''), 'rows' => $rows);
                }
            }

            //
            //section_6
            //
            $rows = array();

            //row_1
            $row = array();
            $row['type'] = 'textField';
            $row['field']['value'] = round(!empty($booking['sub_total']) ? $booking['sub_total'] : 0, 2);
            $row['field']['enabled'] = "NO";
            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
            $row['name'] = 'sub_total';
            $row['label'] = 'Sub Total:';
            $row['hiddenField'] = "NO";
            $row['validation'] = "";
            $rows[] = $row;

            if (!empty($status['name']) && $status['name'] == 'FAILED') {
                $row = array();
                $row['type'] = 'textField';
                $row['field']['value'] = round(!empty($booking['call_out_fee']) ? $booking['call_out_fee'] : 0, 2);
                $row['field']['enabled'] = "YES";
                $row['field']['keyboardType'] = 'UIKeyboardTypeDecimalPad';
                $row['name'] = 'call_out_fee';
                $row['label'] = 'Call Out Fee:';
                $row['hiddenField'] = "NO";
                $row['validation'] = "";
                $rows[] = $row;
            }

            //row_2
            $row = array();
            $row['type'] = 'textField';
            $row['field']['value'] = round(!empty($booking['total_discount']) ? $booking['total_discount'] : 0, 2);
            $row['field']['enabled'] = "YES";
            $row['field']['keyboardType'] = 'UIKeyboardTypeDecimalPad';
            $row['name'] = 'total_discount';
            $row['label'] = 'Discount:';
            $row['hiddenField'] = "NO";
            $row['validation'] = "";
            $rows[] = $row;

            //row_3
            $row = array();
            $row['type'] = 'textField';
            $row['field']['value'] = round(!empty($booking['gst']) ? $booking['gst'] : 0, 2);
            $row['field']['enabled'] = "NO";
            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
            $row['name'] = 'gst';
            $row['label'] = 'GST (10%):';
            $row['hiddenField'] = "NO";
            $row['validation'] = "";
            $rows[] = $row;

            //row_4
            $row = array();
            $row['type'] = 'textField';
            $row['field']['value'] = round(!empty($booking['qoute']) ? $booking['qoute'] : 0, 2);
            $row['field']['enabled'] = "NO";
            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
            $row['name'] = 'qoute';
            $row['label'] = 'Total';
            $row['hiddenField'] = "NO";
            $row['validation'] = "";
            $rows[] = $row;

            //fill section
            $result['sections'][] = array('title' => 'Total Booking Cost', 'rows' => $rows);
        }

        return $result;
    }
	
	public function saveBookingOffline($mode='update',$booking) {

        /**
         * get request Params
         */
		
        $title = '';
        $bookingId =  isset($booking['booking_id']) ? $booking['booking_id'] : 0; 
        $inquiryId =  isset($booking['inquiry_id']) ? $booking['inquiry_id'] : 0; 
        $toEstimate =  isset($booking['toEstimate']) ? $booking['toEstimate'] : 0; 
        $st =  isset($booking['booking_start']) ? $booking['booking_start'] : "0000-00-00 00:00:00"; 
        $et =  isset($booking['booking_end']) ? $booking['booking_end'] : "0000-00-00 00:00:00"; 
        $isAllDayEvent =  isset($booking['isAllDayEvent']) ? $booking['isAllDayEvent'] : 0; 
        $description_booking =  isset($booking['description']) ? $booking['description'] : ''; 
        $cityId =  isset($booking['city_id']) ? $booking['city_id'] : 0; 
        $statusId =  isset($booking['status']) ? $booking['status'] : 0; 
        $customer_id =  isset($booking['customer_id']) ? $booking['customer_id'] : 0; 
        $totalDiscount =  isset($booking['total_discount']) ? $booking['total_discount'] : 0; 
        $onsiteClientName =  isset($booking['onsite_client_name']) ? $booking['onsite_client_name'] : ''; 
        $satisfaction =  isset($booking['satisfaction']) ? $booking['satisfaction'] : 1; 
        $jobStartTime =  isset($booking['job_start_time']) ? $booking['job_start_time'] : $st; 
        $jobFinishTime =  isset($booking['job_finish_time']) ? $booking['job_finish_time'] : $et; 
        $services =  isset($booking['services']) ? (array)$booking['services'] : array();
        $services = (array) $services;
        $product_ids =  isset($booking['product']) ? $booking['product'] : array();
        $ltrs =  isset($booking['ltr']) ? $booking['ltr'] : array();
        $propertyTypeId =  isset($booking['property_type']) ? $booking['property_type'] : 0;
        $callOutFee =  isset($booking['call_out_fee']) ? $booking['call_out_fee'] : 0;
        $to_follow =  isset($booking['to_follow']) ? $booking['to_follow'] : '';
        $is_to_follow =  isset($booking['is_to_follow']) ? $booking['is_to_follow'] : 0;
        $multi_stpartdate =  isset($booking['multi_stpartdate']) ? $booking['multi_stpartdate'] : 0;
        $confirmResetAttribut =  isset($booking['confirmResetAttribut']) ? $booking['confirmResetAttribut'] : 0;
        /*
		$bookingId = $this->request->getParam('booking_id', 0);
        $inquiryId = $this->request->getParam('inquiry_id', 0);
        $toEstimate = $this->request->getParam('toEstimate', 0);
        $st = $this->request->getParam('booking_start', "0000-00-00 00:00:00");
        $et = $this->request->getParam('booking_end', "0000-00-00 00:00:00");
        $isAllDayEvent = $this->request->getParam('isAllDayEvent', 0);
        $description_booking = $this->request->getParam('description', '');
        $cityId = $this->request->getParam('city_id', 0);
        $statusId = $this->request->getParam('bookingStatus', 0);
        $customer_id = $this->request->getParam('customer_id', 0);
        $totalDiscount = $this->request->getParam('total_discount', 0);
        $onsiteClientName = $this->request->getParam('onsite_client_name', '');
        $satisfaction = $this->request->getParam('satisfaction', 1);
        $jobStartTime = $this->request->getParam('job_start_time', $st);
        $jobFinishTime = $this->request->getParam('job_finish_time', $et);
        
		$services = $this->request->getParam('services', array());
		$services = json_decode($services,true);
        $product_ids = $this->request->getParam('product', array());
        $ltrs = $this->request->getParam('ltr', array());
        $propertyTypeId = $this->request->getParam('property_type', 0);
        $callOutFee = $this->request->getParam('call_out_fee', 0);
        $to_follow = $this->request->getParam('to_follow', '');
        $is_to_follow = $this->request->getParam('is_to_follow', 0);
        $multi_stpartdate = $this->request->getParam('multi_stpartdate');
        $confirmResetAttribut = $this->request->getParam('confirmResetAttribut', 0);
		*/
		
		$postArr = array( 
			'booking_id'=> $bookingId,
			'inquiry_id'=> $inquiryId, 
			'toEstimate'=>$toEstimate,
			'booking_start'=> $st ,
			'booking_end'=>$et,
			'isAllDayEvent'=>$isAllDayEvent,
			'description'=>$description_booking,
			'city_id'=>$cityId,
			'bookingStatus'=>$statusId,
			'customer_id'=>$customer_id,
			'total_discount'=>$totalDiscount,
			'onsite_client_name'=>$onsiteClientName,
			'satisfaction'=>$satisfaction,
			'job_start_time'=>$jobStartTime,
			'job_finish_time'=>$jobFinishTime,
			'services'=>$services,
			'product'=>$product_ids,
			'ltr'=>$ltrs,
			'property_type'=>$propertyTypeId,
			'call_out_fee'=>$callOutFee,
			'to_follow'=>$to_follow,
			'is_to_follow'=>$is_to_follow,
			'multi_stpartdate'=>$multi_stpartdate,
			'confirmResetAttribut'=>$confirmResetAttribut       
		);
		//print_r(postArr);
		//echo 'rrrrrrrrrrrrrr';
        /**
         * get Logged User && Company
         */
        $loggedUser = CheckAuth::getLoggedUser();
        $companyId = CheckAuth::getCompanySession();

        /**
         * load models
         */
        $modelBooking = new Model_Booking();
        $modelBookingStatus = new Model_BookingStatus();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelRefund = new Model_Refund();
        $modelBookingEstimate = new Model_BookingEstimate();
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelCompanies = new Model_Companies();
        $modelBookingStatusHistory = new Model_BookingStatusHistory();

        /**
         * Get Booking Status
         */
        $completed = $modelBookingStatus->getByStatusName('COMPLETED');
        $faild = $modelBookingStatus->getByStatusName('FAILED');
        $to_do = $modelBookingStatus->getByStatusName('TO DO');
        $quoted = $modelBookingStatus->getByStatusName('QUOTED');
        $toVisit = $modelBookingStatus->getByStatusName('TO VISIT');

        /**
         * get old Booking
         */
        $oldBooking = $modelBooking->getById($bookingId);

        /**
         * previous
         */
        $oldStatusId = isset($oldBooking['status_id']) ? $oldBooking['status_id'] : 0;
        $oldCallOutFee = isset($oldBooking['call_out_fee']) ? $oldBooking['call_out_fee'] : 0;
        $oldTotalDiscount = isset($oldBooking['total_discount']) ? $oldBooking['total_discount'] : 0;

        /**
         * validation
         */
        $errorMessages = array();
		$returnData = array();
        if ($this->validaterOffline($errorMessages,$postArr)){
			echo "validate returns true ";
            /**
             * prepare data to be saved on database
             */
            $db_params = array();
            $db_params['status_id'] = $statusId;
            $db_params['onsite_client_name'] = $onsiteClientName;
            $db_params['job_start_time'] = strtotime($jobStartTime);
            $db_params['job_finish_time'] = strtotime($jobFinishTime);
            $db_params['satisfaction'] = $satisfaction;

            /**
             * contractor can't change booking Details
             * in update mode
             */
			/* if($modelBooking->checkCanEditBookingDetails($bookingId)){
				 echo 'can edit details returns true  ';
			 }
			 else{
				 echo 'can edit details returns NO  ';
			 }*/
            if (($mode == 'create') || (($mode == 'update') && $modelBooking->checkCanEditBookingDetails($bookingId))) {
				echo 'prepare parms....';
                $db_params['customer_id'] = $customer_id;
                $db_params['booking_start'] = php2MySqlTime(strtotime($st));
                $db_params['booking_end'] = php2MySqlTime(strtotime($et));
                $db_params['title'] = $title;
                $db_params['is_all_day_event'] = $isAllDayEvent ? 1 : 0;
                $db_params['description'] = $description_booking;
                $db_params['city_id'] = $cityId;
                $db_params['property_type_id'] = $propertyTypeId;
                $db_params['to_follow'] = $to_follow ? strtotime($to_follow) : 0;
                $db_params['is_to_follow'] = $is_to_follow ? 1 : 0;
            }

            /**
             * delete google Calendar event if status is check as delete google calender like on hold or canceled 
             */
            /*$deleteGoogleCalender = $modelBookingStatus->getDeleteGoogleCalender();
			//print_r($deleteGoogleCalender);
            if (in_array($statusId, $deleteGoogleCalender)) {
                $modelGoogleCalendarEvent = new Model_GoogleCalendarEvent();
                $modelGoogleCalendarEvent->deleteGoogleCalendarEventByBookingId($bookingId);
            }*/
			//echo 'test ';
            /**
             * if booking changed from TO Visit to TO DO update created
             */
            if (($mode == 'update') && $statusId == $to_do['booking_status_id'] && $oldStatusId) {
                if ($oldStatusId == $toVisit['booking_status_id']) {
                    $db_params['created'] = time();
                }
            }

            /**
             * is_multiple_days
             */
            if ($multi_stpartdate) {
                $db_params['is_multiple_days'] = 1;
            } else {
                $db_params['is_multiple_days'] = 0;
            }

            /**
             * save item in the database
             */
			 echo 'update item....';
            if ($mode == 'create') {
				//echo 'save item....';
                $db_params['created_by'] = $loggedUser['user_id'];
                $db_params['created'] = time();
                $db_params['company_id'] = $companyId;
                $db_params['original_inquiry_id'] = $inquiryId;

                $returnData = $modelBooking->addDetailedCalendar($db_params);
            } else {
				echo 'update item....';
                $returnData = $modelBooking->updateDetailedCalendar($bookingId, $db_params);
				echo 'update item....eeee';
            }
			//echo 'test222 ';
			//print_r($returnData);
            if (isset($returnData['Data']) && $returnData['Data']) {
				print_r($returnData['Data']);
                $bookingId = (int) $returnData['Data'];

                /**
                 * save  booking status history
                 */
                if ($mode == 'create') {
                    $modelBookingStatusHistory = new Model_BookingStatusHistory();
                    $modelBookingStatusHistory->addStatusHistory($bookingId, $statusId);
                }
				//echo 'booking history done';

                /**
                 * insert booking services and his quote and quantity
                 */
                if (($mode == 'create') || (($mode == 'update') && $modelBooking->checkCanEditBookingDetails($bookingId))) {
                    $modelContractorServiceBooking->setServicesToBookingOffline($bookingId, $services);
                }
				//echo 'booking services done';
                /**
                 * save multiple days
                 */
                $modelBookingMultipleDays = new Model_BookingMultipleDays();
                $modelBookingMultipleDays->saveMultipleDays($bookingId);

                /**
                 * save address
                 */
                $this->saveAddress($bookingId, $mode);
                   //echo 'booking address done';
                /**
                 * set product to booking
                 */
                if ($product_ids) {
                    $modelBookingProduct = new Model_BookingProduct();
                    $modelBookingProduct->setProductsToBooking($bookingId, $product_ids, $ltrs);
                }
					//echo 'booking products done';
                /**
                 * save why booking status
                 */
                $this->saveWhyStatus($bookingId, $statusId, false);

                /**
                 * convert To Estimate if status is qouted
                 */
				// echo 'booking estimates done';
                if ($statusId == $quoted['booking_status_id']) {
                    $modelBookingEstimate->convertToEstimate($bookingId, false, false);
                }
				//echo 'booking lll estimated done';
                /**
                 * delete Estimate if status is not quoted
                 */
                if ($mode == 'update' && $statusId != $quoted['booking_status_id']) {
                    $modelBookingEstimate->changedEstimateToBooking($bookingId, $statusId, false);
                }
				//echo 'booking Estimate if status is not quoted done';
                /**
                 * convert To Invoice if booking status is complete or faild
                 */
                if ($statusId == $completed['booking_status_id'] || $statusId == $faild['booking_status_id']) {
                    $modelBookingInvoice->convertToInvoice($bookingId, false);
                }
				//echo 'booking convert To Invoice if booking status is complete or faild done';
                /**
                 * set Quantity and Discount attribute value to Zero when booking status is faild
                 */
                if ($statusId == $faild['booking_status_id']) {
                    if ($confirmResetAttribut) {
                        $modelContractorServiceBooking->changeAttributIfFaildOffline($bookingId, $services);
                    }
                } else {
                    $callOutFee = 0;
                }


                /**
                 *  insert services in  the temp until approved when the booking is update by contractor
                 */
                $totalDiscountTemp = 0;
                $callOutFeeTemp = 0;
                $db_params = array();
                if ((($mode == 'update') && !$modelBooking->checkCanEditBookingDetails($bookingId))) {

                    $modelContractorServiceBookingTemp = new Model_ContractorServiceBookingTemp();
                    $modelContractorServiceBookingTemp->setServicesToBookingOffline($bookingId, $services);

                    /**
                     * if the contractor change call out fee value ,it will saved in the temp ,
                     * the original call out fee get from it's company  and saved in the booking
                     */
                    $callOutFeeTemp = $callOutFee;
                    $companies = $modelCompanies->getById(CheckAuth::getCompanySession());
                    if ($callOutFee) {
                        if ($oldCallOutFee) {
                            $callOutFee = $oldCallOutFee;
                        } else {
                            $callOutFee = $companies['call_out_fee'];
                        }
                        if ($callOutFeeTemp != $callOutFee) {
                            $db_params['is_change'] = 1;
                        }
                    }

                    // get the total discount from original booking because the gst and the total calculate from it.
                    $totalDiscountTemp = $totalDiscount;

                    if (isset($totalDiscount) && $oldTotalDiscount != $totalDiscount) {
                        $totalDiscount = $oldTotalDiscount;
                        $db_params['is_change'] = 1;
                    }

                    if ($statusId != $oldStatusId) {
                        $db_params['is_change'] = 1;
                    }
                }


                /**
                 * save calculation qoute
                 */
                $totalRefund = $modelRefund->getTotalRefund(array('booking_id' => $bookingId, 'is_approved' => 'yes'));
                $subTotal = $modelContractorServiceBooking->getTotalBookingQoute($bookingId);
                $total = ($subTotal + $callOutFee) - $totalDiscount;
                $gstTax = $total * get_config('gst_tax');
                $totalQoute = $total + $gstTax;
                $totalQoute = $totalQoute - $totalRefund;



                $db_params['sub_total'] = round($subTotal, 2);
                $db_params['gst'] = round($gstTax, 2);
                $db_params['qoute'] = round($totalQoute, 2);
                $db_params['call_out_fee'] = round($callOutFee, 2);
                $db_params['refund'] = round($totalRefund, 2);
                $db_params['total_discount'] = round($totalDiscount, 2);
                $db_params['total_discount_temp'] = round($totalDiscountTemp, 2);
                $db_params['call_out_fee_temp'] = round($callOutFeeTemp, 2);


                /**
                 * generate Title
                 */
                $title = $this->generateTitle($services, $cityId, number_format($totalQoute, 2));
                $db_params['title'] = $title;


                /**
                 * update saved Booking
                 */
                $modelBooking->updateById($bookingId, $db_params, false);

                /**
                 * send Booking To Gmail Acc if status is completed or faild or to_do or in_process
                 */
                /*$pushGoogleCalender = $modelBookingStatus->getPushGoogleCalender();
                if (in_array($statusId, $pushGoogleCalender)) {
                    $modelGoogleCalendarEvent = new Model_GoogleCalendarEvent();
                    $modelGoogleCalendarEvent->sendCreatedBooking($bookingId);
                }*/
            }

            if ($mode == 'create' && $inquiryId) {
                $modelInquiry = new Model_Inquiry();

                $data = array(
                    'status' => 'booking',
                    'deferred_date' => 0
                );
                $modelInquiry->updateById($inquiryId, $data);

                if ($toEstimate) {
                    $data = array(
                        'status' => 'estimate',
                        'deferred_date' => 0
                    );
                    $modelInquiry->updateById($inquiryId, $data);
                }
            }
        } else {
			//echo "validate returns true ";
            
            $returnData['IsSuccess'] = false;
            $returnData['Msg'] = $errorMessages;
        }
	//echo "validate returns true22 ";
	//print_r($returnData);
        return $returnData;
    }
	///////////////////////////
    public function saveBooking() {

        $bookingId = $this->request->getParam('booking_id', 0);

        $returnData = $this->save('update');

        $success = TRUE;
        if (empty($returnData['IsSuccess'])) {
            $success = FALSE;
        }

        if ($success) {
            return array('type' => 'success', 'message' => "Saved successfully", 'result' => $this->getBooking(false, array('booking_id' => $bookingId)));
        } else {
            $message = "Error in Saving Booking";
            if (isset($returnData['Msg'])) {
                if (is_array($returnData['Msg'])) {
                    foreach ($returnData['Msg'] as $msg) {
                        $message .= " ({$msg})";
                    }
                }
            }
            return array('type' => 'error', 'message' => $message);
        }
    }

    public function save($mode) {

        /**
         * get request Params
         */
		 // $postData = $this->request->getPost();
		//print_r($postData);
        $title = '';
        $bookingId = $this->request->getParam('booking_id', 0);
        $inquiryId = $this->request->getParam('inquiry_id', 0);
        $toEstimate = $this->request->getParam('toEstimate', 0);
        $st = $this->request->getParam('booking_start', "0000-00-00 00:00:00");
        $et = $this->request->getParam('booking_end', "0000-00-00 00:00:00");
        $isAllDayEvent = $this->request->getParam('isAllDayEvent', 0);
        $description_booking = $this->request->getParam('description', '');
        $cityId = $this->request->getParam('city_id', 0);
        $statusId = $this->request->getParam('bookingStatus', 0);
        $customer_id = $this->request->getParam('customer_id', 0);
        $totalDiscount = $this->request->getParam('total_discount', 0);
        $onsiteClientName = $this->request->getParam('onsite_client_name', '');
        $satisfaction = $this->request->getParam('satisfaction', 1);
        $jobStartTime = $this->request->getParam('job_start_time', $st);
        $jobFinishTime = $this->request->getParam('job_finish_time', $et);
        $services = $this->request->getParam('services', array());
        $product_ids = $this->request->getParam('product', array());
        $ltrs = $this->request->getParam('ltr', array());
        $propertyTypeId = $this->request->getParam('property_type', 0);
        $callOutFee = $this->request->getParam('call_out_fee', 0);
        $to_follow = $this->request->getParam('to_follow', '');
        $is_to_follow = $this->request->getParam('is_to_follow', 0);
        $multi_stpartdate = $this->request->getParam('multi_stpartdate');
        $confirmResetAttribut = $this->request->getParam('confirmResetAttribut', 0);

		
        /**
         * get Logged User && Company
         */
        $loggedUser = CheckAuth::getLoggedUser();
        $companyId = CheckAuth::getCompanySession();

        /**
         * load models
         */
        $modelBooking = new Model_Booking();
        $modelBookingStatus = new Model_BookingStatus();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelRefund = new Model_Refund();
        $modelBookingEstimate = new Model_BookingEstimate();
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelCompanies = new Model_Companies();
        $modelBookingStatusHistory = new Model_BookingStatusHistory();

        /**
         * Get Booking Status
         */
        $completed = $modelBookingStatus->getByStatusName('COMPLETED');
        $faild = $modelBookingStatus->getByStatusName('FAILED');
        $to_do = $modelBookingStatus->getByStatusName('TO DO');
        $quoted = $modelBookingStatus->getByStatusName('QUOTED');
        $toVisit = $modelBookingStatus->getByStatusName('TO VISIT');

        /**
         * get old Booking
         */
        $oldBooking = $modelBooking->getById($bookingId);

        /**
         * previous
         */
        $oldStatusId = isset($oldBooking['status_id']) ? $oldBooking['status_id'] : 0;
        $oldCallOutFee = isset($oldBooking['call_out_fee']) ? $oldBooking['call_out_fee'] : 0;
        $oldTotalDiscount = isset($oldBooking['total_discount']) ? $oldBooking['total_discount'] : 0;

        /**
         * validation
         */
        $errorMessages = array();
		$returnData = array();
        if ($this->validater($errorMessages)) {
			//echo "validate returns true ";
            /**
             * prepare data to be saved on database
             */
            $db_params = array();
            $db_params['status_id'] = $statusId;
            $db_params['onsite_client_name'] = $onsiteClientName;
            $db_params['job_start_time'] = strtotime($jobStartTime);
            $db_params['job_finish_time'] = strtotime($jobFinishTime);
            $db_params['satisfaction'] = $satisfaction;

            /**
             * contractor can't change booking Details
             * in update mode
             */
			/* if($modelBooking->checkCanEditBookingDetails($bookingId)){
				 echo 'can edit details returns true  ';
			 }
			 else{
				 echo 'can edit details returns NO  ';
			 }*/
            if (($mode == 'create') || (($mode == 'update') && $modelBooking->checkCanEditBookingDetails($bookingId))) {
				
                $db_params['customer_id'] = $customer_id;
                $db_params['booking_start'] = php2MySqlTime(strtotime($st));
                $db_params['booking_end'] = php2MySqlTime(strtotime($et));
                $db_params['title'] = $title;
                $db_params['is_all_day_event'] = $isAllDayEvent ? 1 : 0;
                $db_params['description'] = $description_booking;
                $db_params['city_id'] = $cityId;
                $db_params['property_type_id'] = $propertyTypeId;
                $db_params['to_follow'] = $to_follow ? strtotime($to_follow) : 0;
                $db_params['is_to_follow'] = $is_to_follow ? 1 : 0;
            }

            /**
             * delete google Calendar event if status is check as delete google calender like on hold or canceled 
             */
            $deleteGoogleCalender = $modelBookingStatus->getDeleteGoogleCalender();
			//print_r($deleteGoogleCalender);
            if (in_array($statusId, $deleteGoogleCalender)) {
                $modelGoogleCalendarEvent = new Model_GoogleCalendarEvent();
                $modelGoogleCalendarEvent->deleteGoogleCalendarEventByBookingId($bookingId);
            }
			//echo 'test ';
            /**
             * if booking changed from TO Visit to TO DO update created
             */
            if (($mode == 'update') && $statusId == $to_do['booking_status_id'] && $oldStatusId) {
                if ($oldStatusId == $toVisit['booking_status_id']) {
                    $db_params['created'] = time();
                }
            }

            /**
             * is_multiple_days
             */
            if ($multi_stpartdate) {
                $db_params['is_multiple_days'] = 1;
            } else {
                $db_params['is_multiple_days'] = 0;
            }

            /**
             * save item in the database
             */
            if ($mode == 'create') {
                $db_params['created_by'] = $loggedUser['user_id'];
                $db_params['created'] = time();
                $db_params['company_id'] = $companyId;
                $db_params['original_inquiry_id'] = $inquiryId;

                $returnData = $modelBooking->addDetailedCalendar($db_params);
            } else {
                $returnData = $modelBooking->updateDetailedCalendar($bookingId, $db_params);
            }
			//echo 'test222 ';
			//print_r($returnData);
            if (isset($returnData['Data']) && $returnData['Data']) {

                $bookingId = (int) $returnData['Data'];

                /**
                 * save  booking status history
                 */
                if ($mode == 'create') {
                    $modelBookingStatusHistory = new Model_BookingStatusHistory();
                    $modelBookingStatusHistory->addStatusHistory($bookingId, $statusId);
                }
				//echo 'booking history done';

                /**
                 * insert booking services and his quote and quantity
                 */
                if (($mode == 'create') || (($mode == 'update') && $modelBooking->checkCanEditBookingDetails($bookingId))) {
                    $modelContractorServiceBooking->setServicesToBooking($bookingId, $services);
                }
				//echo 'booking services done';
                /**
                 * save multiple days
                 */
                $modelBookingMultipleDays = new Model_BookingMultipleDays();
                $modelBookingMultipleDays->saveMultipleDays($bookingId);

                /**
                 * save address
                 */
                $this->saveAddress($bookingId, $mode);
                   //echo 'booking address done';
                /**
                 * set product to booking
                 */
                if ($product_ids) {
                    $modelBookingProduct = new Model_BookingProduct();
                    $modelBookingProduct->setProductsToBooking($bookingId, $product_ids, $ltrs);
                }
					//echo 'booking products done';
                /**
                 * save why booking status
                 */
                $this->saveWhyStatus($bookingId, $statusId, false);

                /**
                 * convert To Estimate if status is qouted
                 */
				// echo 'booking estimates done';
                if ($statusId == $quoted['booking_status_id']) {
                    $modelBookingEstimate->convertToEstimate($bookingId, false, false);
                }
				//echo 'booking lll estimated done';
                /**
                 * delete Estimate if status is not quoted
                 */
                if ($mode == 'update' && $statusId != $quoted['booking_status_id']) {
                    $modelBookingEstimate->changedEstimateToBooking($bookingId, $statusId, false);
                }
				//echo 'booking Estimate if status is not quoted done';
                /**
                 * convert To Invoice if booking status is complete or faild
                 */
                if ($statusId == $completed['booking_status_id'] || $statusId == $faild['booking_status_id']) {
                    $modelBookingInvoice->convertToInvoice($bookingId, false);
                }
				//echo 'booking convert To Invoice if booking status is complete or faild done';
                /**
                 * set Quantity and Discount attribute value to Zero when booking status is faild
                 */
                if ($statusId == $faild['booking_status_id']) {
                    if ($confirmResetAttribut) {
                        $modelContractorServiceBooking->changeAttributIfFaild($bookingId, $services);
                    }
                } else {
                    $callOutFee = 0;
                }


                /**
                 *  insert services in  the temp until approved when the booking is update by contractor
                 */
                $totalDiscountTemp = 0;
                $callOutFeeTemp = 0;
                $db_params = array();
                if ((($mode == 'update') && !$modelBooking->checkCanEditBookingDetails($bookingId))) {

                    $modelContractorServiceBookingTemp = new Model_ContractorServiceBookingTemp();
                    $modelContractorServiceBookingTemp->setServicesToBooking($bookingId, $services);

                    /**
                     * if the contractor change call out fee value ,it will saved in the temp ,
                     * the original call out fee get from it's company  and saved in the booking
                     */
                    $callOutFeeTemp = $callOutFee;
                    $companies = $modelCompanies->getById(CheckAuth::getCompanySession());
                    if ($callOutFee) {
                        if ($oldCallOutFee) {
                            $callOutFee = $oldCallOutFee;
                        } else {
                            $callOutFee = $companies['call_out_fee'];
                        }
                        if ($callOutFeeTemp != $callOutFee) {
                            $db_params['is_change'] = 1;
                        }
                    }

                    // get the total discount from original booking because the gst and the total calculate from it.
                    $totalDiscountTemp = $totalDiscount;

                    if (isset($totalDiscount) && $oldTotalDiscount != $totalDiscount) {
                        $totalDiscount = $oldTotalDiscount;
                        $db_params['is_change'] = 1;
                    }

                    if ($statusId != $oldStatusId) {
                        $db_params['is_change'] = 1;
                    }
                }


                /**
                 * save calculation qoute
                 */
                $totalRefund = $modelRefund->getTotalRefund(array('booking_id' => $bookingId, 'is_approved' => 'yes'));
                $subTotal = $modelContractorServiceBooking->getTotalBookingQoute($bookingId);
                $total = ($subTotal + $callOutFee) - $totalDiscount;
                $gstTax = $total * get_config('gst_tax');
                $totalQoute = $total + $gstTax;
                $totalQoute = $totalQoute - $totalRefund;



                $db_params['sub_total'] = round($subTotal, 2);
                $db_params['gst'] = round($gstTax, 2);
                $db_params['qoute'] = round($totalQoute, 2);
                $db_params['call_out_fee'] = round($callOutFee, 2);
                $db_params['refund'] = round($totalRefund, 2);
                $db_params['total_discount'] = round($totalDiscount, 2);
                $db_params['total_discount_temp'] = round($totalDiscountTemp, 2);
                $db_params['call_out_fee_temp'] = round($callOutFeeTemp, 2);


                /**
                 * generate Title
                 */
                $title = $this->generateTitle($services, $cityId, number_format($totalQoute, 2));
                $db_params['title'] = $title;


                /**
                 * update saved Booking
                 */
                $modelBooking->updateById($bookingId, $db_params, false);

                /**
                 * send Booking To Gmail Acc if status is completed or faild or to_do or in_process
                 */
                /*$pushGoogleCalender = $modelBookingStatus->getPushGoogleCalender();
                if (in_array($statusId, $pushGoogleCalender)) {
                    $modelGoogleCalendarEvent = new Model_GoogleCalendarEvent();
                    $modelGoogleCalendarEvent->sendCreatedBooking($bookingId);
                }*/
            }

            if ($mode == 'create' && $inquiryId) {
                $modelInquiry = new Model_Inquiry();

                $data = array(
                    'status' => 'booking',
                    'deferred_date' => 0
                );
                $modelInquiry->updateById($inquiryId, $data);

                if ($toEstimate) {
                    $data = array(
                        'status' => 'estimate',
                        'deferred_date' => 0
                    );
                    $modelInquiry->updateById($inquiryId, $data);
                }
            }
        } else {
			//echo "validate returns true ";
            
            $returnData['IsSuccess'] = false;
            $returnData['Msg'] = $errorMessages;
        }
	//echo "validate returns true22 ";
	//print_r($returnData);
        return $returnData;
    }

    /**
     * validater
     * 
     * @param type $errorMessages
     * @return type boolean
     */
    /*public function validater(&$errorMessages) {

        $postData = $this->request->getPost();

        $bookingId = $this->request->getParam('booking_id');
        $statusId = $this->request->getParam('bookingStatus', 0);

        $modelBooking = new Model_Booking();
        $message = '';
        $isCanChangeBookingStatus = $modelBooking->checkIfCanChangeBookingStatus($bookingId, $statusId, $message);

        if (!$isCanChangeBookingStatus) {
            $errorMessages['bookingStatus'] = $message;
        }

        foreach ($postData as $key => $value) {

            /**
             * is empty validater
             /
			 /// we changed the name of one parameter from total_qoute to qoute
			 
			  $required_filed = array('country', 'city_id', 'bookingStatus', 'sub_total', 'qoute', 'stpartdate', 'etpartdate', 'customer_id', 'to_follow', 'why', 'street_number', 'street_address', 'suburb', 'postcode', 'state');
            if (in_array($key, $required_filed)) {
                if (empty($value)) {
                    $errorMessages[$key] = 'Field required';
                }
            }

            /**
             * time format
             /
            $is_all_day_event = isset($postData["is_all_day_event"]) ? 1 : 0;
            if (!$is_all_day_event) {
                $time_format = array('stparttime', 'etparttime');
                if (in_array($key, $time_format)) {
                    if (empty($value)) {
                        $errorMessages[$key] = 'stparttime and etparttime are required Fields ';
                    }
                }
            }
        }



        $services = isset($postData['services']) ? $postData['services'] : array();
		$services = $this->request->getParam('services',array());
        if (count($services) === 0) {
            $errorMessages['services'] = 'Services cannot be empty';
        } else {
            foreach ($services as $service) {
                //$contractor = isset($postData['contractor_' . $service]) ? $postData['contractor_' . $service] : '';
                $contractor = $this->request->getParam('contractor_' . $service,'');
                //$contractor = isset($this->request->getParam('contractor_' . $service)) ? $this->request->getParam('contractor_' . $service) : '';
                if (empty($contractor)) {
                    $errorMessages['contractor_' . $service] = 'cccc Field required';
                } else {
                    $modelContractorService = new Model_ContractorService();
                    $contractorService = $modelContractorService->getByContractorIdAndServiceId($contractor, $service);
                    if (empty($contractorService)) {
                        $errorMessages['contractor_' . $service] = "This Technician dosn't Provide this Service";
                    } else {
                        //$city_id = isset($postData['city_id']) ? $postData['city_id'] : 0;
                        $city_id = $this->request->getParam('city_id',0);
                

                        $modelContractorServiceAvailability = new Model_ContractorServiceAvailability();
                        $contractorServiceAvailability = $modelContractorServiceAvailability->getByCityIdAndContractorServiceId($city_id, $contractorService['contractor_service_id']);
                        if (empty($contractorServiceAvailability)) {
                            $errorMessages['contractor_' . $service] = "This Technician dosn't Provide this Service in this City";
                        }
                    }
                }
            }
        }

        //$customerId = isset($postData['customer_id']) ? $postData['customer_id'] : 0;
        $customerId = $this->request->getParam('customer_id',0);

        /*if ('contractor' == CheckAuth::getRoleName() && $customerId) {
            $loggedUser = CheckAuth::getLoggedUser();
            $modelCustomer = new Model_Customer();
            if (!$modelCustomer->getByCustomerIdAndCreatedBy($customerId, $loggedUser['user_id'])) {
                $errorMessages['customer_id'] = 'You dont have permission to select this customer';
            }
        }

        if (count($errorMessages) !== 0) {
            return false;
        }
        return true;
    }*/

	//////////////validater from live server
	public function validater(&$errorMessages) {

        $postData = $this->request->getPost();

        $bookingId = $this->request->getParam('booking_id');
        $statusId = $this->request->getParam('bookingStatus', 0);

        $modelBooking = new Model_Booking();
        $message = '';
        $isCanChangeBookingStatus = $modelBooking->checkIfCanChangeBookingStatus($bookingId, $statusId, $message);

        if (!$isCanChangeBookingStatus) {
            $errorMessages['bookingStatus'] = $message;
        }

        foreach ($postData as $key => $value) {

            /**
             * is empty validater
             */
            $required_filed = array('country', 'city_id', 'bookingStatus', 'sub_total', 'total_qoute', 'stpartdate', 'etpartdate', 'customer_id', 'to_follow', 'why', 'street_number', 'street_address', 'suburb', 'postcode', 'state');
            if (in_array($key, $required_filed)) {
                if (empty($value)) {
                    $errorMessages[$key] = 'Field required';
                }
            }

            /**
             * time format
             */
            $is_all_day_event = isset($postData["is_all_day_event"]) ? 1 : 0;
            if (!$is_all_day_event) {
                $time_format = array('stparttime', 'etparttime');
                if (in_array($key, $time_format)) {
                    if (empty($value)) {
                        $errorMessages[$key] = 'Field required';
                    }
                }
            }
        }



        $services = isset($postData['services']) ? $postData['services'] : array();

        if (count($services) === 0) {
            $errorMessages['services'] = 'Services cannot be empty';
        } else {
            foreach ($services as $service) {
                $contractor = isset($postData['contractor_' . $service]) ? $postData['contractor_' . $service] : '';
                if (empty($contractor)) {
                    $errorMessages['contractor_' . $service] = 'Field required';
                } else {
                    $modelContractorService = new Model_ContractorService();
                    $contractorService = $modelContractorService->getByContractorIdAndServiceId($contractor, $service);
                    if (empty($contractorService)) {
                        $errorMessages['contractor_' . $service] = "This Technician dosn't Provide this Service";
                    } else {
                        $city_id = isset($postData['city_id']) ? $postData['city_id'] : 0;

                        $modelContractorServiceAvailability = new Model_ContractorServiceAvailability();
                        $contractorServiceAvailability = $modelContractorServiceAvailability->getByCityIdAndContractorServiceId($city_id, $contractorService['contractor_service_id']);
                        if (empty($contractorServiceAvailability)) {
                            $errorMessages['contractor_' . $service] = "This Technician dosn't Provide this Service in this City";
                        }
                    }
                }
            }
        }

        $customerId = isset($postData['customer_id']) ? $postData['customer_id'] : 0;

        /*if ('contractor' == CheckAuth::getRoleName() && $customerId) {
            $loggedUser = CheckAuth::getLoggedUser();
            $modelCustomer = new Model_Customer();
            if (!$modelCustomer->getByCustomerIdAndCreatedBy($customerId, $loggedUser['user_id'])) {
                $errorMessages['customer_id'] = 'You dont have permission to select this customer';
            }
        }*/
		print_r($errorMessages);
        if (count($errorMessages) !== 0) {
            return false;
        }
        return true;
    }
	
	public function validaterOffline(&$errorMessages,$postData) {

       /* print_r(postData);
        $bookingId = $postData['booking_id'];
        $statusId = $postData['bookingStatus'];*/
        

        $modelBooking = new Model_Booking();
        $message = '';
        $isCanChangeBookingStatus = $modelBooking->checkIfCanChangeBookingStatus($bookingId, $statusId, $message);

        if (!$isCanChangeBookingStatus) {
            $errorMessages['bookingStatus'] = $message;
        }

        foreach ($postData as $key => $value) {

            /**
             * is empty validater
             */
            $required_filed = array('country', 'city_id', 'bookingStatus', 'sub_total', 'total_qoute', 'stpartdate', 'etpartdate', 'customer_id', 'to_follow', 'why', 'street_number', 'street_address', 'suburb', 'postcode', 'state');
            if (in_array($key, $required_filed)) {
                if (empty($value)) {
                    $errorMessages[$key] = 'Field required';
                }
            }

            /**
             * time format
             */
            $is_all_day_event = isset($postData["is_all_day_event"]) ? 1 : 0;
            if (!$is_all_day_event) {
                $time_format = array('stparttime', 'etparttime');
                if (in_array($key, $time_format)) {
                    if (empty($value)) {
                        $errorMessages[$key] = 'Field required';
                    }
                }
            }
        }



        $services = isset($postData['services']) ? (array)$postData['services'] : array();
		
		echo 'services.....';
		print_r($services);
        if (count($services) === 0) {
            $errorMessages['services'] = 'Services cannot be empty';
        } else {
            foreach ($services as &$service) {
				$service = (array)$service;
				$loggedUser = CheckAuth::getLoggedUser();
                //$contractor = isset($postData['contractor_' . $service]) ? $postData['contractor_' . $service] : '';
				$contractor = $loggedUser['user_id'];
				 echo 'contractor   '.$contractor;
				 echo 'service id    '.$service['id'];
                if (empty($contractor)) {
                    $errorMessages['contractor_' . $service] = 'Field required';
                } else {
                    $modelContractorService = new Model_ContractorService();
                    $contractorService = $modelContractorService->getByContractorIdAndServiceId($contractor, $service['id']);
                    if (empty($contractorService)) {
                        $errorMessages['contractor_' . $service] = "This Technician dosn't Provide this Service";
                    } else {
                        $city_id = isset($postData['city_id']) ? $postData['city_id'] : 0;

                        $modelContractorServiceAvailability = new Model_ContractorServiceAvailability();
                        $contractorServiceAvailability = $modelContractorServiceAvailability->getByCityIdAndContractorServiceId($city_id, $contractorService['contractor_service_id']);
                        if (empty($contractorServiceAvailability)) {
                            $errorMessages['contractor_' . $service] = "This Technician dosn't Provide this Service in this City";
                        }
                    }
                }
            }
        }

        $customerId = isset($postData['customer_id']) ? $postData['customer_id'] : 0;

        /*if ('contractor' == CheckAuth::getRoleName() && $customerId) {
            $loggedUser = CheckAuth::getLoggedUser();
            $modelCustomer = new Model_Customer();
            if (!$modelCustomer->getByCustomerIdAndCreatedBy($customerId, $loggedUser['user_id'])) {
                $errorMessages['customer_id'] = 'You dont have permission to select this customer';
            }
        }*/
		print_r($errorMessages);
        if (count($errorMessages) !== 0) {
            return false;
        }
        return true;
    }
	

    /**
     * saveAddress
     *
     * @param type $bookingId
     * @param type $mode 
     */
    public function saveAddress($bookingId, $mode = 'create') {

        $modelBookingAddress = new Model_BookingAddress();
        $modelBookingAddressTemp = new Model_BookingAddressTemp();
        $modelBooking = new Model_Booking();


        $data = $this->fillAddressParam($bookingId);

        $bookingAddress = $modelBookingAddress->getByBookingId($bookingId);

        $orginalAddress = $modelBookingAddress->getByBookingIdWithOutLatAndLon($bookingId);
        $newAddress = $this->fillAddressParam($bookingId, false);

        if (($mode == 'create') || (($mode == 'update') && $modelBooking->checkCanEditBookingDetails($bookingId))) {
            if ($bookingAddress) {
                $modelBookingAddress->updateById($bookingAddress['booking_address_id'], $data);
            } else {
                $modelBookingAddress->insert($data);
            }
        } else {
            if ($orginalAddress != $newAddress) {
                $db_params = array();
                $db_params['is_change'] = 1;
                $modelBooking->updateById($bookingId, $db_params);

                $bookingAddressTemp = $modelBookingAddressTemp->getByBookingId($bookingId);
                if ($bookingAddressTemp) {
                    $modelBookingAddressTemp->updateById($bookingAddressTemp['booking_address_id'], $data);
                } else {
                    $modelBookingAddressTemp->insert($data);
                }
            }
        }
    }

    /**
     * fill Address Param
     * 
     * @param type $bookingid
     * @return type array
     */
    public function fillAddressParam($bookingid, $with_lat_lon = true) {
        $streetAddress = $this->request->getParam('street_address', '');
        $streetNumber = $this->request->getParam('street_number', '');
        $suburb = $this->request->getParam('suburb', '');
        $state = $this->request->getParam('state', '');
        $unitLotNumber = $this->request->getParam('unit_lot_number', '');
        $postcode = $this->request->getParam('postcode', '');
        $poBox = $this->request->getParam('po_box', '');

        $address = array();
        $address['unit_lot_number'] = trim($unitLotNumber);
        $address['street_number'] = trim($streetNumber);
        $address['street_address'] = trim($streetAddress);
        $address['suburb'] = trim($suburb);
        $address['state'] = trim($state);
        $address['postcode'] = trim($postcode);
        $address['po_box'] = trim($poBox);
        $address['booking_id'] = (int) $bookingid;

        if ($with_lat_lon) {
            $modelBookingAddress = new Model_BookingAddress();
            $geocode = $modelBookingAddress->getLatAndLon($address);
            $address['lat'] = $geocode['lat'] ? $geocode['lat'] : 0;
            $address['lon'] = $geocode['lon'] ? $geocode['lon'] : 0;
        }

        return $address;
    }

    /**
     * saveWhyStatus
     * 
     * @param type $bookingId
     * @param type $statusId 
     */
    public function saveWhyStatus($bookingId, $statusId, $addLog = true) {

        $loggedUser = CheckAuth::getLoggedUser();

        $whyBookingStatus = $this->request->getParam('why', $this->request->getParam('extra_comments', ''));
		
        if ($whyBookingStatus) {
			
            $modelBookingStatusDiscussion = new Model_BookingStatusDiscussion();
            $modelBookingDiscussion = new Model_BookingDiscussion();
            $modelBooking = new Model_Booking();

            $db_params = array();
            $db_params['booking_id'] = $bookingId;
            $db_params['user_id'] = $loggedUser['user_id'];
            $db_params['user_message'] = $whyBookingStatus;
            $db_params['created'] = time();

            $lastDiscussion = $modelBookingStatusDiscussion->getLastDiscussionByBookingIdAndStatusId($bookingId, $statusId);
            if (!$lastDiscussion) {
				
                $discussionId = $modelBookingDiscussion->insert($db_params);
            } else {
				
				
                $discussionId = $lastDiscussion['discussion_id'];
				
                $modelBookingDiscussion->updateById($discussionId, $db_params);
            }

            if ($discussionId) {
				
                $db_params = array();
                $db_params['discussion_id'] = $discussionId;
                $db_params['booking_id'] = $bookingId;
                $db_params['status_id'] = $statusId;
                $db_params['created'] = time();

                $bookingStatusDiscussion = $modelBookingStatusDiscussion->getByBookingIdAndStatusIdAndDiscussionId($bookingId, $statusId, $discussionId);
                if ($bookingStatusDiscussion) {
                    $modelBookingStatusDiscussion->updateById($bookingStatusDiscussion['id'], $db_params);
                } else {
                    $modelBookingStatusDiscussion->insert($db_params);
                }
            }
				
            if ($bookingId && $whyBookingStatus) {
                $modelBooking->updateById($bookingId, array('why' => $whyBookingStatus), $addLog);
            }
			
        }
    }

    /**
     * generateTitle
     *
     * @param type $services
     * @param type $cityId
     * @param type $totalQoute
     * @return string 
     */
    public function generateTitle($services, $cityId, $totalQoute) {

        $modelServices = new Model_Services();
        $modelUser = new Model_User();
        $modelCities = new Model_Cities();
        $modelContractorInfo = new Model_ContractorInfo();

        /**
         * get The First service;
         */
        $serviceAndClone = explode('_', $services[0]);
        $serviceId = (int) (isset($serviceAndClone[0]) ? $serviceAndClone[0] : 0);
        $clone = (int) (isset($serviceAndClone[1]) && $serviceAndClone[1] ? $serviceAndClone[1] : 0);
        $contractorId = (int) $this->request->getParam('contractor_' . $serviceId . ($clone ? '_' . $clone : ''));

        $generat_title_service = $modelServices->getById($serviceId);
        $servicesName = $generat_title_service['service_name'];

        $generat_title_city = $modelCities->getById($cityId);
        $cityName = strtoupper($generat_title_city['city_name']);

        $contractor = $modelUser->getById($contractorId);
        $modelContractorInfo->fill($contractor, array('contractor_info_by_user_id'));
        $contractorName = ucwords($contractor['username']);

        $title = "{$cityName}" . ' ' . "$servicesName" . ' - ' . "\${$totalQoute}" . ' - ' . "$contractorName";

        return $title;
    }

	
	public function registerMobileIosAction() {
		// load models
		$model_ContractorMobileInfo = new Model_ContractorMobileInfo();
		//$loggedUser = CheckAuth::getLoggedUser();
		$loggedUser = Zend_Auth::getInstance()->getIdentity();
		
		
		//$modelUser = new Model_User();
		//$user = $modelUser->getById($user_id);
		$ret_data = array('status' => 0);
		

        if (!empty($loggedUser)) {
			$user_id = $loggedUser->user_id;	
						
			$appname = $this->request->getParam('appname');
			$appversion = $this->request->getParam('appversion');
			$deviceuid = $this->request->getParam('deviceuid');
			$devicetoken = $this->request->getParam('devicetoken');
			$devicename = $this->request->getParam('devicename');
			$devicemodel = $this->request->getParam('devicemodel');
			$deviceversion = $this->request->getParam('deviceversion');
			$pushbadge = $this->request->getParam('pushbadge');
			$pushalert = $this->request->getParam('pushalert');
			$pushsound = $this->request->getParam('pushsound');
			$status = $this->request->getParam('status');
			//$contractor_id = $this->request->getParam('contractor_id');
			
			
			$data = array(
			'contractor_id'=> $user_id,
			'user_id' => $user_id,
			'appname' => $appname,
			'appversion' => $appversion,
			'deviceuid' => $deviceuid,
			'devicetoken' => $devicetoken,
			'devicename' => $devicename,
			'devicemodel' => $devicemodel,
			'deviceversion' => $deviceversion,
			'pushbadge' => $pushbadge,
			'pushalert' => $pushalert,
			'pushsound' => $pushsound
			);
		
			switch ($status) {
						case 'update':
							$results = $model_ContractorMobileInfo->updateByContractorIdAndDeviceToken($user_id,$devicetoken,$data);
							break;
						case 'delete':		
							$results = $model_ContractorMobileInfo->deleteByContractorIdAndDeviceToken($user_id,$devicetoken);
						break;
						default:
							$results = $model_ContractorMobileInfo->insert($data);
						break;
						
					}
			if($results){
				$ret_data['status'] = 1;
				echo json_encode($ret_data);
			}
			else{
				$ret_data['status'] = 0;
				$ret_data['msg'] = 'database error';
				echo json_encode($ret_data);
			}
			
		}
		else{
			$ret_data['status'] = 0;
			$ret_data['msg'] = 'unauthorized, please login at first';
			echo json_encode($ret_data);
		}
		
		exit;
		
	}
	
	
	public function getIosNotificationSettingsAction() {
		// load models
		$model_IosNotificationSetting = new Model_IosNotificationSetting();
		
		$loggedUser = Zend_Auth::getInstance()->getIdentity();
		
		$ret_data = array('status' => 0);
		

        if (!empty($loggedUser)) {	  
			
			$results = $model_IosNotificationSetting->getAllIosNotificationSetting();
			$ret_data['status'] = 1;
			$ret_data['results'] = $results;
			echo json_encode($ret_data);
		}
		else{
			$ret_data['status'] = 0;
			$ret_data['msg'] = 'unauthorized, please login at first';
			echo json_encode($ret_data);
		}
		
		exit;
		
	}
	
	
	public function iosUserNotificationSettingsAction() {
		// load models
		/*
		$model_IosUserNotificationSetting = new Model_IosUserNotificationSetting();
		
		$accessToken = $this->request->getParam('access_token',0);
		
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];
		
		$loggedUser = CheckAuth::getLoggedUser();
		
		$ret_data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$ret_data['msg'] = 'wrong access token';
			echo json_encode($ret_data);
			exit;
		}
		
        if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($ret_data);
				exit;
				
			}
			
		}
		
		
		
		$user_id = $loggedUser->user_id;
		echo '$user_id  '.$user_id;
			$mode = $this->request->getParam('mode');
			           
			switch ($mode) {
                    case 'get_user_settings':
                        $results = $model_IosUserNotificationSetting->getAllIosSettingNamesByUserId($user_id);
						break;
                    case 'update_user_setting':
						$setting_id = $this->request->getParam('settingId');
						$is_active = $this->request->getParam('isActive');
						$data = array('is_active'=> $is_active);			
                        $results = $model_IosUserNotificationSetting->updateBySettingIdAndUserId($user_id,$setting_id,$data);
                        break;
                }
			$ret_data['status'] = 1;
			$ret_data['results'] = $results;
			//echo json_encode($ret_data);
			
		
        echo json_encode($ret_data);
        exit;*/
		
		// load models
		$model_IosUserNotificationSetting = new Model_IosUserNotificationSetting();
		
		$loggedUser = Zend_Auth::getInstance()->getIdentity();
		
		$ret_data = array('status' => 0);
		

        if (!empty($loggedUser)) {	  
			$user_id = $loggedUser->user_id;
			$mode = $this->request->getParam('mode');
			           
			switch ($mode) {
                    case 'get_user_settings':
                        $results = $model_IosUserNotificationSetting->getAllIosSettingNamesByUserId($user_id);
						break;
                    case 'update_user_setting':
						$setting_id = $this->request->getParam('settingId');
						$is_active = $this->request->getParam('isActive');
						$data = array('is_active'=> $is_active);			
                        $results = $model_IosUserNotificationSetting->updateBySettingIdAndUserId($user_id,$setting_id,$data);
                        break;
                }
			$ret_data['status'] = 1;
			$ret_data['results'] = $results;
			echo json_encode($ret_data);
		}
		else{
			$ret_data['status'] = 0;
			$ret_data['msg'] = 'unauthorized, please login at first';
			echo json_encode($ret_data);
		}
		
		exit;
		
	}

	
	public function iosUserNotificationAction() {
		$accessToken = $this->request->getParam('access_token',0);
				
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];
		
		
		$loggedUser = CheckAuth::getLoggedUser();
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if($authrezed){
				if(!empty($notification_ids)){
					$data['msg'] = 'conflict between logged user and access token';
					echo json_encode($data);
					exit;
					
				}	
			}
			
		}
		//else{
			
            $mongo = new Model_Mongo();
			$notification = $mongo->getNotificationsByContractorId();
			//print_r($results);
			$results= array();
			$i=0;
			
			foreach ($notification as $row){
				$results[$i]['notification_id']= $row['_id']->{'$id'};
				$results[$i]['notification_text']= $row['notification_text'];
				$results[$i]['send_date']= $row['send_date'];
				$results[$i]['booking_id']= $row['booking_id'];
				$results[$i]['seen']= $row['seen'];
				$i++;
			}
			$data['authrezed'] = 1;
			$data['results'] = $results;
			echo json_encode($data);
		
		
		exit;
		
	}
	
	public function iosUpdateNotificationSeenAction() {
		$accessToken = $this->request->getParam('access_token',0);
		$notification_ids = $this->request->getParam('notification_ids',array());
				
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];
		
		
		$loggedUser = CheckAuth::getLoggedUser();
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if($authrezed){
				if(!empty($notification_ids)){
					$data['msg'] = 'conflict between logged user and access token';
					echo json_encode($data);
					exit;
					
				}	
			}
			
		}
		//else{
			
            if(!empty($notification_ids)){
					$mongo = new Model_Mongo();
					$updated = 0;
					foreach($notification_ids as $notification_id){
						$updated = $mongo->updateNotification($notification_id);
					}
					
			}	

            $data['result'] = $updated;
            $data['authrezed'] = 1;
		//}

        echo json_encode($data);
        
		exit;
		
	}
	
	
	public function iosBookingDistanceAction() {
		//get parameters
		$first_booking_id = $this->request->getParam('first_booking_id');
		$second_booking_id = $this->request->getParam('second_booking_id');
		$contractor_id = $this->request->getParam('contractor_id');
			
		// load models
		$model_IosUserNotificationSetting = new Model_IosUserNotificationSetting();
		$modelBookingAddress = new Model_BookingAddress();
		
		$loggedUser = Zend_Auth::getInstance()->getIdentity();
		
		$ret_data = array('status' => 0);
		

        if (!empty($loggedUser)) {	
			$firstBookingAddress = $modelBookingAddress->getByBookingId($first_booking_id);
				if($second_booking_id){
					$secondBookingAddress = $modelBookingAddress->getByBookingId($second_booking_id);
				}
				else{
					$modelUser = new Model_User();
					$secondBookingAddress = $modelUser->getById($contractor_id);
				}
			$distance = $modelBookingAddress->getDistanceByTwoAddress($firstBookingAddress, $secondBookingAddress);
					
			$ret_data['status'] = 1;
			$ret_data['results'] = $distance;
			echo json_encode($ret_data);
		}
		else{
			$ret_data['status'] = 0;
			$ret_data['msg'] = 'unauthorized, please login first';
			echo json_encode($ret_data);
		}
		
		exit;
		
	}
	
	
	public function getAllBookingStatusAction() {
		
		// load models
		$modelBookingStatus = new Model_BookingStatus();
		
		$loggedUser = Zend_Auth::getInstance()->getIdentity();
		
		$ret_data = array('status' => 0);
		

        if (!empty($loggedUser)) {	
			$allBookingStatus = $modelBookingStatus->getAll();
			
			$ret_data['status'] = 1;
			$ret_data['results'] = $allBookingStatus;
			echo json_encode($ret_data);
		}
		else{
			$ret_data['status'] = 0;
			$ret_data['msg'] = 'unauthorized, please login first';
			echo json_encode($ret_data);
		}
		
		exit;
		
	}
	
	
	public function getAllStatesAction() {
		
		// load models
		$modelCities = new Model_Cities();
		
		$loggedUser = Zend_Auth::getInstance()->getIdentity();
		//$companyId = CheckAuth::getCompanySession();
		
		$ret_data = array('status' => 0);
		

        if (!empty($loggedUser)) {	
			$allCities = $modelCities->getAll();
			$allStates = array();
			foreach($allCities as $city){
				$allStates[] = $city['state'];
			}
			
			$ret_data['status'] = 1;
			$ret_data['results'] = $allStates;
			echo json_encode($ret_data);
		}
		else{
			$ret_data['status'] = 0;
			$ret_data['msg'] = 'unauthorized, please login first';
			echo json_encode($ret_data);
		}
		
		exit;
		
	}
	
	
	
	public function receiveEmailsParametersAction() {
		//receive data and then save it in the temp db
		$ser = $_POST['str'];
		//echo $ser;
		$ret= unserialize($ser);
		print_r($ret);
		
		$request_id = uniqid();
		$modelCronjobDataTemp = new Model_CronjobDataTemp();
		foreach($ret as $ele){
			//echo 'serialize($ret  '.serialize($ret['data']);
			//print_r($ret['data']);
			$date = array(
			'email_info' => serialize($ele['data']),
			'email_content_parms' => serialize($ele['template_params']),
			'email_log' => serialize($ele['email_log']),
			'company_id' => 1,
			'request_id' => $request_id,
			'email_type' => $ele['cronjob_name'],
			'status' => 'sending'
			);
			$modelCronjobDataTemp->insert($date);
		}
		
			
		//return $request_id;	
		exit;
		
	}
	
	
	public function updateSentEmailsAction() {
		////// get update request
		$ser = $_POST['str'];
		//echo $ser;
		$ret= unserialize($ser);
		print_r($ret);
		
		$table = $ret['table'];
		echo 'table'.$table;
		
		$update_records = $ret['update_records'];
		$modelCronjobDataTemp = new Model_CronjobDataTemp();
		foreach($update_records as $ele){
			$ref_id = $ele[0];
			echo '$ref_id   '. $ref_id;
			$updaete_value = $ele[1];
			print_r($updaete_value);
			
		}
		/////now get sutible model according to table name
		//switch case
		
		
        exit;
		
	}
	
	public function openNewSession($accessToken){
			$modelIosUser = new Model_IosUser();
			//$user = $modelIosUser->getByUserInfoById($iosUserId);
			//echo 'accessToken   '.$accessToken;
			$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
			//echo 'tessssssssssssssssst';
			//print_r($user);
			//echo 'is empty '.empty($user);
			if(!empty($user)){
				$email = $user['email1'];
				$password = $user['password'];
				$authrezed = $this->getAuthrezed();
				$authrezed->setIdentity($email);
				$authrezed->setCredential($password);

				$auth = Zend_Auth::getInstance();
				$authrezedResult = $auth->authenticate($authrezed);
				if ($authrezedResult->isValid()) {

					$identity = $authrezed->getResultRowObject();

					$authStorge = $auth->getStorage();
					$authStorge->write($identity);

					CheckAuth::afterlogin(false);
					
					$this->iosLoggedUser = $user['id'];
					
					return 1;
				}
				else{
					return 0;
				}
				
			}
			else{
				return 0;
				
			}
	
	}
	
	
	public function checkAccessTokenOfLoggedUser($accessToken){
			$modelIosUser = new Model_IosUser();
			//$user = $modelIosUser->getByUserInfoById($iosUserId);
			$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
			$loggedUser = CheckAuth::getLoggedUser();
			if($user['user_id'] == $loggedUser['user_id']){
				return 1;
			}
			return 0;
			
	}
	
	
	public function getPermissionsAction() {
		$accessToken = $this->request->getParam('access_token',0);
		//$registration_id = $this->request->getParam('registration_id');
			
		$modelIosUser = new Model_IosUser();
		$model_ContractorAndroidDeviceInfo = new Model_ContractorAndroidDeviceInfo();
		
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];
		
		
		$loggedUser = CheckAuth::getLoggedUser();
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)){
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			$loggedUser = CheckAuth::getLoggedUser();
			if(!$authrezed){
				
				echo json_encode($data);
				exit;
			}
			
		}
		
		$modelAuthRoleCredential = new Model_AuthRoleCredential();
		$permissions = $modelAuthRoleCredential->getAllCredentialByRoleId($loggedUser['role_id']);
		$data['result'] = $permissions;
		$data['authrezed'] = 1;	

        echo json_encode($data);
        exit;		
		
	}
	///***************ANDROID
	public function registerMobileAndroidAction() {
		$accessToken = $this->request->getParam('access_token',0);
		$registration_id = $this->request->getParam('registration_id');
			
		$modelIosUser = new Model_IosUser();
		$model_ContractorAndroidDeviceInfo = new Model_ContractorAndroidDeviceInfo();
		
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];
		
		
		$loggedUser = CheckAuth::getLoggedUser();
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			$loggedUser = CheckAuth::getLoggedUser();
			if($authrezed){
				$info = array(
				'contractor_id'=> $loggedUser['user_id'],
				'registration_id' => $registration_id
				
				);
				$result = $model_ContractorAndroidDeviceInfo->insert($info);
			
			
				$data['result'] = $result;
				$data['authrezed'] = 1;
			}
			
		}
		else{
			
            $info = array(
			'contractor_id'=> $loggedUser['user_id'],
			'registration_id' => $registration_id
			
			);
			$result = $model_ContractorAndroidDeviceInfo->insert($info);
			

            $data['result'] = $result;
            $data['authrezed'] = 1;
		}

        echo json_encode($data);
        exit;		
		
	}
	
	
}