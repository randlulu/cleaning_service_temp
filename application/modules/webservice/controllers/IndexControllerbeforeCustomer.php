<?php

class WebService_IndexController extends Zend_Controller_Action {

    private $request;
    private $iosLoggedUser;
	private $logId;

	
    public function init() {
        parent::init();
        $this->request = $this->getRequest();
		$this->logId = 0;
    }	
	
 
	public function receiveDataFromAndroidAction() {
	   
		header('Content-Type: application/json');
		$bookingId = $this->request->getParam('booking_id',0);
		$accessToken = $this->request->getParam('access_token',0);
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];
		
		
		$loggedUser = CheckAuth::getLoggedUser();
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($data);
				exit;
				
			}
		$loggedUser = 	CheckAuth::getLoggedUser();
		
		}
		//echo 'hhhhh';
		//echo 'by mona';
		//print_r($loggedUser);
		
		
		
		//echo 'eeee';
		$data = $this->request->getPost();
		
		//$data = $_POST;
		if(empty($data)){
			$data = $_GET;
		}
		
		
		
		if(!empty($data)){
		//var_dump($data['bookings']);
		
		//exit;
		//echo 'booking_address11111   '.$data['booking_id'];
		
		
		
		
		$json = json_decode($data['bookings']);
       
		
		$result = array();
		
		
		if(!empty($json)){
        
		foreach ($json as $key => $value){
						//echo 'key '.$key;
						//echo 'value '.$value;
						if(!is_array($value)){
			               $value = urldecode($value);
			             }
						$result[$key] = $value;
			}
		}

    
		$result1 = array();
		
		if($result['products']){
		$products = $result['products'];
		
		
        if(!empty($products)){			
		foreach ($products as $key => &$value) {			
			$result2 = array();
			foreach ($value as $k => $v) {
			if(!is_array($v)){
			               $v = urldecode($v);
			             }
				$result2[$k] = $v;
			}	
			$result1[$key] = $result2;
		 }
		}
	  }	
		//print_r($result1);
		$result['products'] = $result1;
		///***************
		
		if($result['services']){
		$services = $result['services'];
		if(!empty($services)){	
		$result1 = array();
				
		foreach ($services as $key => &$value) {			
			$result2 = array();
			foreach ($value as $k => $v) {
			if(!is_array($v)){
			               $v = urldecode($v);
			             }
				$result2[$k] = $v;
			}
            $result2['contractor_id'] = $loggedUser['user_id'];			
			$result1[$key] = $result2;
		 }
		}
	   }	
		//print_r($result1);
		//****attributes 
		$services = $result1;
		$result1 = array();
		
		if(!empty($services)){	
		foreach ($services as $key => &$value) {			
			$attributes =$value['attributes'];
			//print_r($attributes);
			$attributes_arr = array();
			foreach ($attributes as $k => $v) {
				$attribute = array();
				foreach ($v as $k1 => $v1) {
					if(!is_array($v1)){
			               $v1 = urldecode($v1);
			             }
					$attribute[$k1] =  $v1;
				}
					$attributes_arr[$k] = $attribute;
			}	
			//print_r($attributes_arr);
			$value['attributes'] = $attributes_arr;
			//$result1[$key] = $result2;
		 } 
		}
		
		$result['services'] = $services;
		
		//print_r($services);
		if($result['BookingDates']){
		$BookingDates = $result['BookingDates'];
		if(!empty($BookingDates)){	
		$result1 = array();
				
		foreach ($BookingDates as $key => &$value) {			
			$result2 = array();
			foreach ($value as $k => $v) {
			if(!is_array($v)){
			               $v = urldecode($v);
			             }
				$result2[$k] = $v;
			}			
			$result1[$key] = $result2;			
		 }	
         $BookingDates =  $result1 ; 
         if(!empty($BookingDates)){
		    foreach ($BookingDates as $key => &$value) {
            		
			if(array_key_exists('products',$value)){
			 if($value['products']){
			$products =$value['products'];
			$products_arr = array();
			foreach ($products as $k => $v) {
				$products = array();
				foreach ($v as $k1 => $v1) {
					if(!is_array($v1)){
			               $v1 = urldecode($v1);
			             }
						 
					$products[$k1] =  $v1;
				}
					$products_arr[$k] = $products;				
			}	
			$value['products'] = $products_arr;
			 }
			}
		   } 
		 
		 }		 
		}
		$result['BookingDates'] = $BookingDates;
	   }
	   
	   
	   
	   
		
		
		$modelContractorServiceBooking = new Model_ContractorServiceBooking();
		//$modelContractorServiceBooking->setServicesToBookingOffline($bookingId, $services);
		
		
		//print_r($result);
	    //exit;
	   $booking = $result;
	
		
		$result = $this->save($mode='update',$booking,'android');
		

		
		
		if($result['IsSuccess']){
		$booking = $this->getBooking(false, array('booking_id' => $bookingId));	
		$returnData['IsSuccess'] = true;
		$returnData['Msg'] = $booking;
		//$returnData['is_change'] = $booking['is_change'];
		 }else{
		   
		$returnData['IsSuccess'] = $result['IsSuccess'];
		$returnData['Msg'] = $result['Msg'];
		 
		 }
	    }
		else{
			$returnData['IsSuccess'] = false;
			$returnData['Msg'] = "NO DATA";
		}
		echo json_encode($returnData);
		exit;
		//echo '*******************************************************';
		//echo 'booking_address   '.$json['booking_address'];
		//echo 'islam testtt ';
		//echo json_encode(array('test'=>'done'));
        //exit;
		
	}
    public function indexAction() {
		header('Content-Type: application/json');
        $email = $this->request->getParam('email');
        $password = $this->request->getParam('password');
        $mode = $this->request->getParam('mode', 'login');
        $uuid = $this->request->getParam('uuid');

        $data = array('authrezed' => 0);

        if (!empty($email) && !empty($password) && !empty($uuid)) { // validate form data
            $authrezed = $this->getAuthrezed();

            $authrezed->setIdentity($email);
            $authrezed->setCredential(sha1($password));

            $auth = Zend_Auth::getInstance();
            $authrezedResult = $auth->authenticate($authrezed);

            if ($authrezedResult->isValid()) {

                $identity = $authrezed->getResultRowObject();
				
				//$identity['ios_user_id'] = '55';
				
                $authStorge = $auth->getStorage();
                $authStorge->write($identity);

                CheckAuth::afterlogin(false,'app');
                //CheckAuth::afterlogin(false);

               $modelIosUser = new Model_IosUser();
               ////check if this is the firs time the user login from this device
				/*$loggedUser = Zend_Auth::getInstance()->getIdentity();
				$loggedBefore = $modelIosUser->getByUserIdAndUUID($loggedUser->user_id, $uuid);
				$ignoreSyncedDataParam = 1;
				if($loggedBefore){
					$ignoreSyncedDataParam = 0;
				}*/
				
				$this->iosLoggedUser = $modelIosUser->afterIosLogin($uuid);
				
                $result = array();

                switch ($mode) {
                    case 'booking':
                        $result = $this->getBooking();
                        break;
					case 'booking_original':
                        $result = $this->getBookingOriginal();
                        break;
                    case 'update_sync':
                        $this->updateSync();
                        break;
                    case 'accept_or_reject_booking':
                        $result = $this->acceptOrRejectBooking();
                        break;
                    case 'add_payment':
                        $result = $this->addPayment();
                        break;
                    case 'payment_types':
                        $result = $this->getPaymentTypes();
                        break;
                    case 'booking_status':
                        $result = $this->getBookingStatus();
                        break;
                    case 'prepare_to_edit':
                        $result = $this->prepareToEdit();
                        break;
                    case 'save_booking':
                        $result = $this->saveBooking();
                        break;
                }

                $data['result'] = $result;
                $data['authrezed'] = 1;
            }
        }

        echo json_encode($data);
        exit;
    }
	
	///new function
	public function loginAction() {

        $email = $this->request->getParam('email');
        $password = $this->request->getParam('password');
        $mode = $this->request->getParam('mode', 'login');
        $uuid = $this->request->getParam('uuid');
		
        
		


        if (!empty($email) && !empty($password) && !empty($uuid)) { // validate form data
            $authrezed = $this->getAuthrezed();

            $authrezed->setIdentity($email);
            $authrezed->setCredential(sha1($password));

            $auth = Zend_Auth::getInstance();
            $authrezedResult = $auth->authenticate($authrezed);

            if ($authrezedResult->isValid()) {

                $identity = $authrezed->getResultRowObject();

                $authStorge = $auth->getStorage();
                $authStorge->write($identity);

                CheckAuth::afterlogin(false,'app');
                //CheckAuth::afterlogin(false);
				
               $modelIosUser = new Model_IosUser();
               
				$accessToken = $modelIosUser->afterIosLogin($uuid);
				$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
				
				$this->iosLoggedUser = $user['id'];
				$loggedUser = CheckAuth::getLoggedUser();
			    $userId = $loggedUser['user_id'];
                $result = array('access_token'=> $accessToken,'user_id'=>$userId);

                $data['result'] = $result;
                $data['authrezed'] = 1;
            }else{
			    $data['result'] = array();
                $data['authrezed'] = 0;
			}
        }
		
		
		// Turn on output buffering with the gzhandler
		//ob_start('ob_gzhandler');
		header('Content-Type: application/json');
        echo json_encode($data);
        exit;
    }
	
	
	///get all bookings
	public function getAllBookingsAction() {
		header('Content-Type: application/json');
	   // echo 'inside service';
		
		$accessToken = $this->request->getParam('access_token',0);
		$count = $this->request->getParam('month_count',0);
		$modelIosUser = new Model_IosUser();
		$modelBooking = new Model_Booking();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		
		$this->iosLoggedUser = $user['id'];
		
		$loggedUser = CheckAuth::getLoggedUser();
		$filters = array();
		$is_last_request = 0;
		
		
		 
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
		
		
		if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);			
			if($authrezed){
			
			    			
		if($count){
		 //date_default_timezone_set('Australia/Sydney');         
		 //$filters['created_to'] =  date('d-m-Y H:i ', strtotime(date('d-m-Y H:i ')." -".($month_count-1)." month"));
		 //$filters['created_from'] = date('d-m-Y H:i ', strtotime(date('d-m-Y H:i ')." -".$month_count." month"));
         $loggedUser = CheckAuth::getLoggedUser();
		 /*if(!empty($loggedUser)){
		  $contractor_id = $loggedUser['user_id'];
		  $firstRecord = $modelBooking->getFirstRecordForContractor($contractor_id);
		 if($firstRecord['created'] < strtotime(trim($filters['created_from']))){
		  $is_last_request = 0;
		 }else{
		  $is_last_request = 1;
		 }
		}*/		 
	   }
				$result = $this->getBooking(false,$filters,$count);
				if(empty($result['result'])){
				 $is_last_request = 1;
				}else{
				 $is_last_request = 0;
				}
				$data['result'] = $result;
				$data['authrezed'] = 1;
				$data['is_last_request'] = $is_last_request;
			}
			
		}
		else{
		
		if($count){
		 //date_default_timezone_set('Australia/Sydney');         
		 //$filters['created_to'] =  date('d-m-Y H:i ', strtotime(date('d-m-Y H:i ')." -".($month_count-1)." month"));
		 //$filters['created_from'] = date('d-m-Y H:i ', strtotime(date('d-m-Y H:i ')." -".$month_count." month"));
         $loggedUser = CheckAuth::getLoggedUser();
		 /*if(!empty($loggedUser)){
		  $contractor_id = $loggedUser['user_id'];
		  $firstRecord = $modelBooking->getFirstRecordForContractor($contractor_id);
		 if($firstRecord['created'] < strtotime(trim($filters['created_from']))){
		  $is_last_request = 0;
		 }else{
		  $is_last_request = 1;
		 }
		}*/		 
	   }
				$result = $this->getBooking(false,$filters,$count);
				if(empty($result['result'])){
				 $is_last_request = 1;
				}else{
				 $is_last_request = 0;
				}
				$data['result'] = $result;
				$data['authrezed'] = 1;
				$data['is_last_request'] = $is_last_request;
		}

		
        echo json_encode($data);
        exit;
    }
	
	
	public function getAllProductsAction() {
		header('Content-Type: application/json');
		$accessToken = $this->request->getParam('access_token',0);
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];
		$modelProduct = new Model_Product();

		
		$loggedUser = CheckAuth::getLoggedUser();
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($data);
				exit;
				
			}
			
		}
		$result = $modelProduct->getAll();
		$data['result'] = $result;
		$data['authrezed'] = 1;
		
        echo json_encode($data);
        exit;
    }
	
	public function getAllPropertyTypeAction() {
		header('Content-Type: application/json');
		$accessToken = $this->request->getParam('access_token',0);
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];
		$modelPropertyType = new Model_PropertyType();

		
		$loggedUser = CheckAuth::getLoggedUser();
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($data);
				exit;
				
			}
			
		}
		$result = $modelPropertyType->getAll();
		$data['result'] = $result;
		$data['authrezed'] = 1;
		
        echo json_encode($data);
        exit;
    }
	
	public function getCallOutFeeAction() {
		header('Content-Type: application/json');
		$accessToken = $this->request->getParam('access_token',0);
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];
		$modelCompanies = new Model_Companies();

		
		$loggedUser = CheckAuth::getLoggedUser();
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($data);
				exit;
				
			}
			
		}
		$company_id = CheckAuth::getCompanySession();
		$result = $modelCompanies->getById($company_id);
		$data['result'] = $result['call_out_fee'];
		$data['authrezed'] = 1;
		
        echo json_encode($data);
        exit;
    }
	
	public function getAllFloorAction() {
		header('Content-Type: application/json');
		$accessToken = $this->request->getParam('access_token',0);
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];
		$modelCompanies = new Model_Companies();

		
		$loggedUser = CheckAuth::getLoggedUser();
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($data);
				exit;
				
			}
			
		}
	
		$modelAttributeListValue = new Model_AttributeListValue();
		$result = $modelAttributeListValue->getAllFloor();
		$data['result'] = $result;
		$data['authrezed'] = 1;
		
        echo json_encode($data);
        exit;
    }
	
	
	///get all sealer
	public function getAllSealerAction() {
		header('Content-Type: application/json');
		$accessToken = $this->request->getParam('access_token',0);
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];
		$modelCompanies = new Model_Companies();

		
		$loggedUser = CheckAuth::getLoggedUser();
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($data);
				exit;
				
			}
			
		}
	
		$modelAttributeListValue = new Model_AttributeListValue();
		$result = $modelAttributeListValue->getAllSealers();
		$data['result'] = $result;
		$data['authrezed'] = 1;
		
        echo json_encode($data);
        exit;
    }
	
	////get unsynced bookings only not all
	public function getUpdatedBookingsAction() {
		header('Content-Type: application/json');
		$accessToken = $this->request->getParam('access_token',0);
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];
		
		
		$loggedUser = CheckAuth::getLoggedUser();		
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if($authrezed){
				$result = $this->getBooking(true, array());
				$data['result'] = $result;
				$data['authrezed'] = 1;
			}			
		}
		else{
			
            $result = $this->getBooking(true, array());

            $data['result'] = $result;
            $data['authrezed'] = 1;
		}

        echo json_encode($data);
        exit;
    }
	/////after receiving booking the app should confirm receiving them by sending updateSync request
	/////which asks the server to mark the received booking ids as synced in order to ignore these bookings in the second login
	public function updateSyncAction() {
		header('Content-Type: application/json');
		$rowData = (array)json_decode(file_get_contents("php://input"));
		//print_r($rowData);
		if($rowData){
		  $accessToken = $rowData['access_token'];
		  $booking_ids = $rowData['booking_ids'];
		}else{
		 $accessToken = $this->request->getParam('access_token',0);
		 $booking_ids = $this->request->getParam('booking_ids',0);
		}
		
		
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];
		
		$loggedUser = CheckAuth::getLoggedUser();
		$data = array('authrezed' => 0);
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
		
				
        if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if($authrezed){
				$result = $this->updateSync($booking_ids);
				//$data['result'] = $result;
				$data['result'] = 'updated!!!!!';
				$data['authrezed'] = 1;
				
			}
			
		}
		else{
			$result = $this->updateSync($booking_ids);
            //$data['result'] = $result;
            $data['result'] = 'updated!!!!!';
            $data['authrezed'] = 1;
		}

        echo json_encode($data);
        exit;
		
    }
	
	/// accept or reject booking
	public function acceptOrRejectBookingAction() {
		header('Content-Type: application/json');
		$os = $this->request->getParam('os','ios');
		if($os == 'ios'){
		  $rawData = (array)json_decode(file_get_contents("php://input"));	
          $accessToken = $rawData['access_token'];		  
		}
		else {
		   $accessToken = $this->request->getParam('access_token',0);
		}
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];
		
		$loggedUser = CheckAuth::getLoggedUser();		
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
        if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if($authrezed){
				$result = $this->acceptOrRejectBooking();

				$data['result'] = $result;
				$data['authrezed'] = 1;
				
			}
			
		}
		else{
			$result = $this->acceptOrRejectBooking();

            $data['result'] = $result;
            $data['authrezed'] = 1;
		}

        echo json_encode($data);
        exit;
    }
	
	/// add payment
	public function addPaymentAction() {
		header('Content-Type: application/json');
		$accessToken = $this->request->getParam('access_token',0);
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];
		
		
		$loggedUser = CheckAuth::getLoggedUser();
		//$this->iosLoggedUser = $iosUserId;
		$data = array('authrezed' => 0);
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if($authrezed){
				$result = $this->addPayment();
				$data['result'] = $result;
				$data['authrezed'] = 1;
				
			}
			
		}
		else{
			$result = $this->addPayment();

            $data['result'] = $result;
            $data['authrezed'] = 1;
		}

        echo json_encode($data);
        exit;
    }
	
	
		 
	 public function editPaymentAction() {
	

			$accessToken = $this->request->getParam('access_token',0);
			$modelIosUser = new Model_IosUser();
			$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
			$this->iosLoggedUser = $user['id'];
			$loggedUser = CheckAuth::getLoggedUser();
			
			$invoiceId = $this->request->getParam('invoice_id',0);
			$invoice_number = $this->request->getParam('invoice_num',0); 
			
			$mobileRetArr = array('authrezed' => 0);
			if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
				$mobileRetArr['msg'] = 'wrong access token';
				$mobileRetArr['type'] = 'error';
				$mobileRetArr['result'] = array();
				echo json_encode($mobileRetArr);
				exit;
			}
			
			if (empty($loggedUser)) {
				
				//open new session
				$authrezed = $this->openNewSession($accessToken);			
				if(!$authrezed){
					echo json_encode($mobileRetArr);
					exit;	
				}		
			}
			$mobileRetArr['authrezed'] = 1;
        
		
		////////////////////////////////////

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $receivedDate = $this->request->getParam('received_date');
        $bankCharges = $this->request->getParam('bank_charges');
        $amount = $this->request->getParam('amount');
        $description = $this->request->getParam('description');
		$description = urldecode($description);
        $reference = $this->request->getParam('reference');
		$reference = urldecode($reference);
        $amountWithheld = $this->request->getParam('amount_withheld');
        $withholdingTax = $this->request->getParam('withholding_tax');
        $isAcknowledgment = $this->request->getParam('is_acknowledgment');
        $paymentTypeId = $this->request->getParam('payment_type_id');
        $contractorId = $this->request->getParam('contractor_id', 0);
        
		
        //
        // Load Model
        //
        $modelPayment = new Model_Payment();
        $modelBooking = new Model_Booking();
        $modelBookingInvoice = new Model_BookingInvoice();

        //
        //Validation
        //
        $payment = $modelPayment->getById($id);
 

        if($payment['is_approved']){
			$mobileRetArr['msg'] = 'Could not edit Approved Payment, Please change to Unapproved to edit';
			$mobileRetArr['type'] = 'error';
			$mobileRetArr['result'] = array();
			echo json_encode($mobileRetArr);
			exit;
		}

		//echo "payment['booking_id']  ".$payment['booking_id'];
        $booking = $modelBooking->getById($payment['booking_id']);
		// get company name for customer 
		

		if(!$booking){
			$mobileRetArr['msg'] = 'Invalid Booking';
			$mobileRetArr['type'] = 'error';
			$mobileRetArr['result'] = array();
			echo json_encode($mobileRetArr);
			exit;
		}
       
	   if(!$modelBooking->checkIfCanEditBooking($booking['booking_id'],$loggedUser['user_id'])){
			$mobileRetArr['msg'] = "You Don't Have Permission";
			$mobileRetArr['type'] = 'error';
			$mobileRetArr['result'] = $this->getBooking(false, array('booking_id' => $booking['booking_id'] ));
			echo json_encode($mobileRetArr);
			exit;
		}

        $approvedPayment = $modelPayment->getTotalAmount(array('booking_id' => $booking['booking_id'], 'is_approved' => 'yes'));
 
		if($approvedPayment >= $booking['qoute'] ){
			$mobileRetArr['msg'] = "fully Paid";
			$mobileRetArr['type'] = 'error';
			$mobileRetArr['result'] = $this->getBooking(false, array('booking_id' => $booking['booking_id'] ));
			echo json_encode($mobileRetArr);
			exit;
		}

			    $loggedUser = CheckAuth::getLoggedUser();
                $data = array(
                    'received_date' => strtotime($receivedDate),
                    'bank_charges' => round($bankCharges, 2),
                    'amount' => round($amount, 2),
                    'description' => urldecode($description),
                    'payment_type_id' => $paymentTypeId,
                    'booking_id' => $booking['booking_id'],
                    'customer_id' => $booking['customer_id'],
                    'user_id' => $loggedUser['user_id'],
                    'created' => time(),
                    'reference' => urldecode($reference),
                    'amount_withheld' => round($amountWithheld, 2),
                    'withholding_tax' => $withholdingTax,
                    'is_acknowledgment' => $isAcknowledgment,
                    'contractor_id' => $contractorId
                );

                $success = $modelPayment->updateById($id, $data);
				
				if ($success) {
                    $mobileRetArr['msg'] = "Saved successfully, Please Approved this Payment";
                    $mobileRetArr['result'] = $this->getBooking(false, array('booking_id' => $booking['booking_id'] ));
					echo json_encode($mobileRetArr);
					exit;
                } 
                    $mobileRetArr['msg'] = "No Changes in Payment";
					$mobileRetArr['result'] = $this->getBooking(false, array('booking_id' => $booking['booking_id'] ));
					$mobileRetArr['type'] = 'error';
					echo json_encode($mobileRetArr);
					exit;

    }
	
	/// payment_types
	public function getPaymentTypesAction() {
		header('Content-Type: application/json');
			
		$accessToken = $this->request->getParam('access_token',0);
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];
		
		
		$loggedUser = CheckAuth::getLoggedUser();
		
		$data = array('authrezed' => 0);
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
        if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			$data['test'] = $authrezed;
			//
			if($authrezed){
				$result = $this->getPaymentTypes();
				$data['result'] = $result;
				$data['authrezed'] = 1;
				
			}
			
		}
		else{
			$result = $this->getPaymentTypes();
            $data['result'] = $result;
            $data['authrezed'] = 1;
		}

        echo json_encode($data);
        exit;
    }
	/// booking_status
	public function getBookingStatusAction() {
		header('Content-Type: application/json');
	
				
		$accessToken = $this->request->getParam('access_token',0);
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];
		
		$loggedUser = CheckAuth::getLoggedUser();
		$data = array('authrezed' => 0);
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
        if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if($authrezed){
				$result = $this->getBookingStatus();
				$data['result'] = $result;
				$data['authrezed'] = 1;
				
			}
			
		}
		else{
			$result = $this->getBookingStatus();
            $data['result'] = $result;
            $data['authrezed'] = 1;
		}

        echo json_encode($data);
        exit;
    }
	
	/// prepare_to_edit
	public function prepareToEditAction() {
		header('Content-Type: application/json');
		$accessToken = $this->request->getParam('access_token',0);
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];
		
		$loggedUser = CheckAuth::getLoggedUser();
		
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
        if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if($authrezed){
				$result = $this->prepareToEdit();
				$data['result'] = $result;
				$data['authrezed'] = 1;
				
			}
			
		}
		else{
			$result = $this->prepareToEdit();
            $data['result'] = $result;
            $data['authrezed'] = 1;
		}

        echo json_encode($data);
        exit;
    }
	
	/// save_booking
	public function saveBookingAction() {
		header('Content-Type: application/json');	
		$accessToken = $this->request->getParam('access_token',0);
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];
		
		$loggedUser = CheckAuth::getLoggedUser();
		
		$data = array('authrezed' => 0);
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){			
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
        if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if($authrezed){
				$result = $this->saveBooking();
				$data['result'] = $result;
				$data['authrezed'] = 1;
				
			}
			
		}
		else{
			$result = $this->saveBooking();
            $data['result'] = $result;
            $data['authrezed'] = 1;
		}

        echo json_encode($data);
        exit;
    }
	
	public function testAction() {
		$test = $this->request->getParam('test','');
		echo $test;
		echo "done By islam";
		exit;
	}
		/// save_booking offline
	public function saveBookingsOfflineAction(){
			header('Content-Type: application/json');
			//echo 'teeeeeeeee';
			$rawData = (array)json_decode(file_get_contents("php://input"));
			//print_r($rawData);
			//$accessToken = $this->request->getParam('access_token',0);
			//$os = $this->request->getParam('os','ios');
			$accessToken = $rawData['access_token'];
			$os = $rawData['os'];
			$modelIosUser = new Model_IosUser();
			$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
			$this->iosLoggedUser = $user['id'];
			 
			$loggedUser = CheckAuth::getLoggedUser();
			
			$data = array('authrezed' => 0);
			if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){			
				$data['msg'] = 'wrong access token';
				echo json_encode($data);
				exit;
			}
			if (empty($loggedUser)) {			
				//open new session
				$authrezed = $this->openNewSession($accessToken);
				if(!$authrezed){
					echo json_encode($data);
					exit;			
				}			
			}
			
			$loggedUser = CheckAuth::getLoggedUser();
			if($os == 'android'){
				$rawData = $_POST;
				if(empty($rawData)){
					$rawData = $_GET;
				}
				//print_r($rawData);
				//echo 'jsondata  '.$rawData['jsondata'];
				$jsondata = $rawData['jsondata'];
				//print_r($_GET);
				$jsondata = (array)json_decode($jsondata);
				//print_r($jsondata);
				//echo 'teeeeest  ';
			}
			else{
				$rawData = (array)json_decode(file_get_contents("php://input"));
				//print_r($rawData);
			}
			
			//foreach($bookings as $booking){
					//echo 'in the foreach .....';
					//$booking = (array) $booking;
					//print_r($rawData['bookings'][0]);
					$booking = $rawData['bookings'][0];
					$result = array();
					foreach ($booking as $key => $value) {
						//echo 'key '.$key;
						//echo 'value '.$value;
						$result[$key] = $value;
					}
					
					
					/*echo 'by islam';
					print_r($result);
					echo 'by islam';
					exit;*/
					//print_r($result);
					//print_r($booking);
					//echo 'gst......'.$booking['gst'];
					//echo 'doneee';
					
					$services = $result['services'];
					
		$result1 = array();
		foreach ($services as $key => &$value) {	
			
			$result2 = array();
			foreach ($value as $k => $v) {
				/*foreach ($v as $k1 => $v1) {
				$result2[$k1] = $v1;
				}*/
				$result2[$k] = $v;
			}
            $result2['contractor_id'] = $loggedUser['user_id'];			
			$result1[$key] = $result2;
		}
		
			
		//print_r($result1);
		//****attributes 
		$services = $result1;
		
				
		$result1 = array();
		foreach ($services as $key => &$value) {			
			$attributes =$value['attributes'];
			
			
			//print_r($attributes);
			//By MONA
			/*$attributes_arr = array();
			foreach ($attributes as $k => $v) {
			 $attribute = array();
				foreach ($v as $k1 => $v1) {					
					$attribute[$k1] =  $v1;
					$attribute_test = array();
				foreach($v1 as $k2=>$v2){
					   $attribute_test[$k2] =  $v2;
				}
					
				$attribute[$k1] = $attribute_test;	
				}
				
					$attributes_arr[$k] = $attribute;
			}	
			$value['attributes'] = $attributes_arr;
			*/
			///By Islam
			$attributes_arr = array();
			foreach ($attributes as $k => $v) {
				$attribute = array();
				foreach ($v as $k1 => $v1) {
					
					$attribute[$k1] =  $v1;
				}
					$attributes_arr[$k] = $attribute;
			}	
			//print_r($attributes_arr);
			$value['attributes'] = $attributes_arr;
			//$result1[$key] = $result2;
			
		}
		 
		$result['services'] = $services;
		
		
		$bookingsDates = $result['BookingDates'];
		$result1 =array();
		if(!empty($bookingsDates)){	
		foreach ($bookingsDates as $key => &$value) {	
			
			$result2 = array();
			foreach ($value as $k => $v) {
				/*foreach ($v as $k1 => $v1) {
				$result2[$k1] = $v1;
				}*/
				$result2[$k] = $v;
			}
			$result1[$key] = $result2;
			
		 }
		}
		
		$bookingDates = $result1;
		
		$result1 = array();
		foreach ($bookingDates as $key => &$value) {			
			$products =$value['products'];
			$products_arr = array();
			foreach ($products as $k => $v) {
				$product = array();
				foreach ($v as $k1 => $v1) {
					
					$product[$k1] =  $v1;
				}
					$products_arr[$k] = $product;
			}	
			$value['products'] = $products_arr;
			
		}
		
		$result['BookingDates'] = $bookingDates;
		
		$products = $result['products'];
		
		$result1 = array();
		foreach ($products as $key => &$value) {			
			
			$products_arr = array();
			foreach ($value as $k => $v) {
				
					$products_arr[$k] = $v;
			}	
			$result1['key'] = $products_arr;
			
		}
		$result['products'] = $result1;
		
					$result = $this->saveBookingOffline('update',$result);
			//}
		
			
			
			
			
		   $data['result'] = $result;
           $data['authrezed'] = 1;
		

        echo json_encode($data);
        exit;
    }
	
/////////////////End

    public function getAuthrezed() {
        $modelAuthRole = new Model_AuthRole();
		$contractorRoleId = $modelAuthRole->getRoleIdByName('contractor');
		
        $authrezed = new Zend_Auth_Adapter_DbTable(Zend_Db_Table::getDefaultAdapter());
        $authrezed->setTableName('user')
                ->setIdentityColumn('email1')
                ->setCredentialColumn('password')
                ->setCredentialTreatment("? AND active = 'TRUE' And role_id={$contractorRoleId}");

        return $authrezed;
    }
	
	public function getIosAuthrezed() {
        
        $authrezed = new Zend_Auth_Adapter_DbTable(Zend_Db_Table::getDefaultAdapter());
        $authrezed->setTableName('ios_user')
                ->setIdentityColumn('user_id');

        return $ios_authrezed;
    }

    public function getBooking($ignoreSynced = false, $filters = array(),$count = 0 ) {

		//By Islam
		$os = $this->request->getParam('os','ios');
		$is_last_request = $this->request->getParam('is_last_request',1);
	    
        $currentPage = $this->request->getParam('current_page', 0);
        //$ignoreSyncedDataParam = $this->request->getParam('ignore_synced_data', 1);
        $is_accepted = 0 ; $is_rejected = 0;
        $modelBooking = new Model_Booking();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelUser = new Model_User();
        $modelContractorinfo = new Model_ContractorInfo();
        $modelServiceAttribute = new Model_ServiceAttribute();
        $modelServiceAttributeValue = new Model_ServiceAttributeValue();
        $modelAttributes = new Model_Attributes();
        $modelAttributeType = new Model_AttributeType();
        $modelAttributeListValue = new Model_AttributeListValue();
        $modelIosSync = new Model_IosSync();
        $modelBookingEstimate = new Model_BookingEstimate();
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelPropertyType = new Model_PropertyType();
        $modelCustomer = new Model_Customer();
        $modelBookingAddress = new Model_BookingAddress();
        $modelServices = new Model_Services();
        $modelBookingStatus = new Model_BookingStatus();

 
		
        if ($ignoreSynced) {
            if(!($is_last_request)){
			  $mobileSyncedBookingIds = $modelIosSync->getMobileSyncedBookingIds($this->iosLoggedUser);
			  $filters['not_booking_ids'] = $mobileSyncedBookingIds;
			}else{
			  $syncedBookingIds = $modelIosSync->getSyncedBookingIds($this->iosLoggedUser);
			  $filters['not_booking_ids'] = $syncedBookingIds;
			}
            
			//var_dump($filters);
        }
		/*else{
			$filters['not_booking_ids'] = $syncedBookingIds;
		}*/
		
		
		
		if($count != 0){		
		$currentPage = $count;
		$limit = 0;
        $perPage = 30;
		}else{
		 $limit = 100;
        $perPage = 10;
        if (!$currentPage) {
            $limit = 100;
        }
		}
        
        $pager = null;
        $bookings = $modelBooking->getAll($filters, 'created DESC', $pager,$limit, $perPage, $currentPage);

        $result = array();
		$modelBookingMultipleDays = new Model_BookingMultipleDays();
		$modelVisitedExtraInfo = new Model_VisitedExtraInfo();
		$modelBookingProduct = new Model_BookingProduct();
		$modelBookingDiscussion = new Model_BookingDiscussion();
		$modelRejectBookingQuestionAnswers = new Model_RejectBookingQuestionAnswers();
		
        foreach ($bookings as $key => $booking) {

            $db_params = array();
            $db_params['is_sync'] = 0;
            $db_params['ios_user_id'] = $this->iosLoggedUser;
            $db_params['booking_id'] = $booking['booking_id'];
            
			
            $syncedBooking = $modelIosSync->getByBookingIdAndIosUser($booking['booking_id'], $this->iosLoggedUser);
            
			if ($syncedBooking) {
				 
                $modelIosSync->updateById($syncedBooking['id'], $db_params);
            } else {
				
                $modelIosSync->insert($db_params);
            }
			
            //
            //booking
            //
			//////get all dates of this booking 
			$multipleDays = $modelBookingMultipleDays->getByBookingId($booking['booking_id']);
			//echo $os;
			//print_r($multipleDays);
          if($os == 'android' || $os = 'ios'){
            $rejectBookingQuestionAnswers = $modelRejectBookingQuestionAnswers->getAnswersByBookingId($booking['booking_id']);
            $result[$key]['rejectedQuestions'] = $rejectBookingQuestionAnswers;
		   }
			
			//if($os == 'android'){
			  $multipleDayResult = array();
			foreach($multipleDays as $k=>$multipleDay){
			  $multipleDayResult[$k]['multipleDay_id'] = $multipleDay['id'];
			  $multipleDayResult[$k]['booking_id'] = $multipleDay['booking_id'];
			  $multipleDayResult[$k]['booking_start'] = $multipleDay['booking_start'];
			  $multipleDayResult[$k]['booking_end'] = $multipleDay['booking_end'];
			  $multipleDayResult[$k]['is_all_day_event'] = $multipleDay['is_all_day_event'];
			  $ExtraInfo = $modelVisitedExtraInfo->getById($multipleDay['visited_extra_info_id']);
			  if(isset($ExtraInfo['is_visited']) && $ExtraInfo['is_visited']){
			  //$ExtraInfo = $modelVisitedExtraInfo->getById($multipleDay['visited_extra_info_id']);
			  //$multipleDayResult[$k]['job_start'] = strtotime($ExtraInfo['job_start']);
			  $multipleDayResult[$k]['job_start'] = $ExtraInfo['job_start'];
			  //$multipleDayResult[$k]['job_end'] = strtotime($ExtraInfo['job_end']);
			  $multipleDayResult[$k]['job_end'] = $ExtraInfo['job_end'];
			  $multipleDayResult[$k]['onsite_client_name'] = $ExtraInfo['onsite_client_name'];
			 // $products = $modelBookingProduct->getProductsByExtraInfoId($multipleDay['visited_extra_info_id'] , $multipleDay['booking_id']);
			  $products = $modelBookingProduct->getByVisitedExtraInfoId($multipleDay['visited_extra_info_id']);
			  $multipleDayResult[$k]['products'] = $products;
			  $multipleDayResult[$k]['is_visited']  = $ExtraInfo['is_visited'];
			  }else{
			   $multipleDayResult[$k]['is_visited']  = 0;
			   $multipleDayResult[$k]['job_start'] = null;
			   $multipleDayResult[$k]['job_end'] = null;
			   $multipleDayResult[$k]['onsite_client_name'] = null;
			   $multipleDayResult[$k]['products'] = array();
			  }
			  
			   $discussion = $modelBookingDiscussion->getByExtraInfoId($multipleDay['visited_extra_info_id']);
			   $multipleDayResult[$k]['extra_comments'] = $discussion['user_message'];
			  
			  
			}
			//print_r($multipleDays);
			//print_r($multipleDayResult);
			 $result[$key]['multiple_days'] = $multipleDayResult;
			/*}else{		  
			 $result[$key]['multiple_days'] = $multipleDays;
			}*/
			
			
			
			
            $propertyType = $modelPropertyType->getById($booking['property_type_id']);
            $address = $modelBookingAddress->getByBookingId($booking['booking_id']);
            //$is_accepted = $modelBooking->checkBookingIfAccepted($booking['booking_id']);
            //$is_rejected = $modelBooking->checkBookingIfRejected($booking['booking_id']);
            $status = $modelBookingStatus->getById($booking['status_id']);

            $result[$key]['booking_num'] = $booking['booking_num'];
			$result[$key]['city_id'] = $booking['city_id'];
            $result[$key]['is_change'] = $booking['is_change'];
            $result[$key]['title'] = $booking['title'];
            $result[$key]['booking_start'] = $booking['booking_start'];
            $result[$key]['booking_end'] = $booking['booking_end'];
            $result[$key]['property_type'] = $propertyType['property_type'];
            $result[$key]['qoute'] = $booking['qoute'];
            $result[$key]['sub_total'] = $booking['sub_total'];
            $result[$key]['total_discount'] = $booking['total_discount'];
            $result[$key]['gst'] = $booking['gst'];
            $result[$key]['paid_amount'] = $booking['paid_amount'];
            $result[$key]['refund'] = !empty($booking['refund']) ? $booking['refund'] : 0;
            $result[$key]['description'] = $booking['description'];
            $result[$key]['status'] = $status['name'];
            $result[$key]['convert_status'] = $booking['convert_status'];
            $result[$key]['created'] = $booking['created'];
            $result[$key]['why'] = $booking['why'];
            
			//commented By Islam
			//if($os == 'android'){
				$result[$key]['call_out_fee'] = $booking['call_out_fee'];
				$first_extra_info = $modelVisitedExtraInfo->getById($booking['visited_extra_info_id']);
				if(isset($first_extra_info['is_visited'])){
				  $result[$key]['is_visited'] = $first_extra_info['is_visited'];
				 }else{
				  $result[$key]['is_visited']  = 0;
				 }
				
				
			//}
			///by islam
			/*if($is_accepted){
				$result[$key]['accept_status'] = 'accepted';
			}
			elseif($is_rejected){
				$result[$key]['accept_status'] = 'rejected';
			}
			else{
				$result[$key]['accept_status'] = 'unknown';
			}*/
			
            //////
            $result[$key]['original_booking_id'] = $booking['booking_id'];

            //booking address
            $result[$key]['booking_address'] = get_line_address($address);
            $result[$key]['po_box'] = $address['po_box'];
            $result[$key]['postcode'] = $address['postcode'];
            $result[$key]['state'] = $address['state'];
            $result[$key]['street_address'] = $address['street_address'];
            $result[$key]['street_number'] = $address['street_number'];
            $result[$key]['suburb'] = $address['suburb'];
            $result[$key]['unit_lot_number'] = $address['unit_lot_number'];
            $result[$key]['lat'] = $address['lat'];
            $result[$key]['lon'] = $address['lon'];
			///// get booking distance
			$loggedUser = CheckAuth::getLoggedUser();
			$distance = $modelBookingAddress->getDistanceByTwoAddress($loggedUser, $address);
			//$result[$key]['booking_distance'] = number_format($distance, 2);
			$result[$key]['booking_distance'] = $distance;
			////get all products of this booking
			$modelBookingProduct = new Model_BookingProduct();
			//if($os == 'android'){
			 //$products = $modelBookingProduct->getProductsByExtraInfoId($booking['visited_extra_info_id'],$booking['booking_id']);
			 $products = $modelBookingProduct->getByVisitedExtraInfoId($booking['visited_extra_info_id']);
			/*}else{
			 $products = $modelBookingProduct->getProductNamesByBookingId($booking['booking_id']);
			}*/
			$result[$key]['products'] = $products;
			///By Islam 
			if($os == 'android' || $os = 'ios'){
			 ///get All Complaints of this booking
			 $modelComplaint = new Model_Complaint();
			 $modelComplaintTemp = new Model_ComplaintTemp();
		         $complaintsTemp = $modelComplaintTemp->getByBookingId($booking['booking_id']); 
		         $complaints = $modelComplaint->getByBookingIdAndIsApproved($booking['booking_id']);
		         $complaints = array_merge ($complaints , $complaintsTemp);
			 $result[$key]['complaints'] = $complaints;
			}
			
			
			/////End
			//if($os == 'android'){
			   $extra_info = $modelVisitedExtraInfo->getById($booking['visited_extra_info_id']);
			   $result[$key]['onsite_client_name'] = $extra_info['onsite_client_name'];
               //$result[$key]['job_start_time'] = isset($extra_info['job_start']) ? strtotime($extra_info['job_start']):0;
               $result[$key]['job_start_time'] = $extra_info['job_start'];
               //$result[$key]['job_finish_time'] = isset($extra_info['job_end']) ? strtotime($extra_info['job_end']) : 0;
			   $result[$key]['job_finish_time'] = $extra_info['job_end'];
			   $whyBookingdiscussion = $modelBookingDiscussion->getByExtraInfoId($booking['visited_extra_info_id']);
			   $result[$key]['user_message'] = isset($whyBookingdiscussion['user_message']) ? $whyBookingdiscussion['user_message'] : '';
			/*}else{
				$result[$key]['onsite_client_name'] = $booking['onsite_client_name'];
				$result[$key]['job_start_time'] = $booking['job_start_time'];
				$result[$key]['job_finish_time'] = $booking['job_finish_time'];
				$result[$key]['satisfaction'] = $booking['satisfaction'];
				
				//get extra comment
			$modelBookingStatusDiscussion = new Model_BookingStatusDiscussion();
			$whyDiscussion = $modelBookingStatusDiscussion->getLastDiscussionByBookingIdAndStatusId($booking['booking_id'], $booking['status_id']);
			$result[$key]['user_message'] = isset($whyDiscussion['user_message']) ? $whyDiscussion['user_message'] : '';
			
			}*/
			
			
            $result[$key]['is_to_follow'] = $booking['is_to_follow'];
            $result[$key]['to_follow'] = $booking['to_follow'];
            $result[$key]['is_all_day_event'] = $booking['is_all_day_event'];
            $result[$key]['is_multiple_days'] = $booking['is_multiple_days'];
            
			
			
			//get all questions
			$modelUpdateBookingQuestionAnswer = new Model_UpdateBookingQuestionAnswer();
			$questions = $modelUpdateBookingQuestionAnswer->getByBookingIdAndStatusId($booking['status_id'],$booking['booking_id']);
			$result[$key]['questions'] = $questions;
			
            //
            //estimate
            //
            $estimate = $modelBookingEstimate->getNotDeletedByBookingId($booking['booking_id']);

            if ($estimate) {
                $result[$key]['estimate']['created'] = date('Y-m-d H:i:s', $estimate['created']);
                $result[$key]['estimate']['estimate_number'] = $estimate['estimate_num'];
                $result[$key]['estimate']['estimate_type'] = $estimate['estimate_type'];
                $result[$key]['estimate']['original_estimate_id'] = $estimate['id'];
            } else {
                $result[$key]['estimate'] = array();
            }

            //
            //invoice
            //
            $invoice = $modelBookingInvoice->getByBookingId($booking['booking_id']);

            if ($invoice) {
                $result[$key]['invoice']['created'] = date('Y-m-d H:i:s', $invoice['created']);
                $result[$key]['invoice']['invoice_number'] = $invoice['invoice_num'];
                $result[$key]['invoice']['invoice_type'] = $invoice['invoice_type'];
                $result[$key]['invoice']['condition_report'] = $invoice['condition_report'];
                $result[$key]['invoice']['original_invoice_id'] = $invoice['id'];
				
				if($os == 'android' || $os = 'ios'){
					$modelPayment = new Model_Payment();
					$payments = $modelPayment->getByBookingId($booking['booking_id']);
					if ($payments) {
						//foreach($payments as $payment){
							$result[$key]['invoice']['payments']= $payments;
						//}
					}
					
				    $modelRefund = new Model_Refund();
	                //$Refunds = $modelRefund->getAll(array('booking_id'=>$booking['booking_id']));
	                $Refunds = $modelRefund->getByBookingId($booking['booking_id']);
					if($Refunds){
					   $result[$key]['invoice']['refunds']= $Refunds;
					}
					
				}
				
            } else {
                $result[$key]['invoice'] = array();
            }


            //
            //customer
            //
            $customer = $modelCustomer->getById($booking['customer_id']);
            $customer_name = get_customer_name($customer);

            $result[$key]['customer']['name'] = $customer_name;
            $result[$key]['customer']['address'] = get_line_address($customer);
           // $result[$key]['customer']['email1'] = $customer['email1'];
           // $result[$key]['customer']['email2'] = $customer['email2'];
            //$result[$key]['customer']['email3'] = $customer['email3'];
            $result[$key]['customer']['phone1'] = $customer['phone1'];
            $result[$key]['customer']['phone2'] = $customer['phone2'];
            $result[$key]['customer']['phone3'] = $customer['phone3'];
            $result[$key]['customer']['mobile1'] = $customer['mobile1'];
            $result[$key]['customer']['mobile2'] = $customer['mobile2'];
            $result[$key]['customer']['mobile3'] = $customer['mobile3'];
            $result[$key]['customer']['fax'] = $customer['fax'];
            $result[$key]['customer']['city'] = $customer['city_name'];
            $result[$key]['customer']['country'] = $customer['country_name'];
            $result[$key]['customer']['original_customer_id'] = $customer['customer_id'];


            //
            //services & service_attribute & service_attribute_value
            //
            
            $filters = array();
            $filters['booking_id'] = $booking['booking_id'];
            $services = $modelContractorServiceBooking->getAll($filters);

            foreach ($services as $service_key => $service) {


                $serviceId = isset($service['service_id']) ? $service['service_id'] : 0;
                $clone = isset($service['clone']) ? $service['clone'] : 0;
                $bookingId = isset($service['booking_id']) ? $service['booking_id'] : 0;
                $service_info = $modelServices->getById($service['service_id']);

                $contractorServiceBooking = $modelContractorServiceBooking->getByBookingAndServiceAndClone($bookingId, $serviceId, $clone);
                $contractorInfo = $modelContractorinfo->getByContractorId($contractorServiceBooking['contractor_id']);
                $userInfo = $modelUser->getById($contractorServiceBooking['contractor_id']);

                $contractorName = ucwords($userInfo['username']) . ucwords($contractorInfo['business_name'] ? " - " . $contractorInfo['business_name'] : '');

                $unit_price = "0";
                $qty = "0";

                //get the service attribute id
                $service_attributes = $modelServiceAttribute->getByServiceId($serviceId);
                foreach ($service_attributes as $service_attribute) {

                    $attribute = $modelAttributes->getById($service_attribute['attribute_id']);

                    //get the service attribute value
                    $attributeValue = $modelServiceAttributeValue->getByBookingIdAndServiceAttributeIdAndClone($bookingId, $service_attribute['service_attribute_id'], $clone);
					
					//$unit_price = $attributeValue['value'];
                    
					switch ($attribute['attribute_name']) {
                        case 'Quantity':
                            $qty = $attributeValue['value'];
                            break;
                        case 'Price':
                            $unit_price = $attributeValue['value'];
                            break;
                        default:
                            $atributetype = $modelAttributeType->getById($attribute['attribute_type_id']);

                            $values = array(); 

                            // check if the attribute type is list
                            if ($atributetype['is_list']) {
                                // check if the attribute value is serialized
                                if ($attributeValue['is_serialized_array']) {
                                    $unserializeValues = unserialize($attributeValue['value']);
                                    foreach ($unserializeValues as $unserializeKey => $unserializeValue) {
                                        $attributeListValue = $modelAttributeListValue->getById($unserializeValue);

                                        if ($attributeValue['service_attribute_value_id']) {
                                            $attribute_value = array();
                                            $attribute_value['value'] = $attributeListValue['attribute_value'];
                                            $attribute_value['original_service_attribute_value_id'] = $attributeValue['service_attribute_value_id'] . '_' . $unserializeKey;
                                            $values[] = $attribute_value;
                                        }
                                    }
                                } else {
                                    $attributeListValue = $modelAttributeListValue->getById($attributeValue['value']);

                                    if ($attributeValue['service_attribute_value_id']) {
                                        $attribute_value = array();
                                        $attribute_value['value'] = $attributeListValue['attribute_value'];
                                       ////By islam we should send id of the selected value, for example instead of sending cramice we should send its id
									   $attribute_value['attribute_value_id'] = $attributeValue['value'];
										////end
                                        $attribute_value['original_service_attribute_value_id'] = $attributeValue['service_attribute_value_id'];
                                        $values[] = $attribute_value;
                                    }
                                }
                            } else {
                                if ($attributeValue['service_attribute_value_id']) {
                                    $attribute_value = array();
                                    $attribute_value['value'] = $attributeValue['value'];
                                    $attribute_value['original_service_attribute_value_id'] = $attributeValue['service_attribute_value_id'];
                                    $values[] = $attribute_value;
                                }
                            }

                            if ($attribute['attribute_name']) {

                                $attribute_name = array();
                                $attribute_name['name'] = $attribute['attribute_name'];
                                $attribute_name['attribute_id'] = $attribute['attribute_id'];
                                $attribute_name['original_service_attribute_id'] = $service['id'] . '_' . $service_attribute['service_attribute_id'];
                                $attribute_name['values'] = $values;
                                $attribute_name['type'] = $atributetype['attribute_type'];

                                $result[$key]['services'][$service_key]['service_attribute'][] = $attribute_name;
                            }
                            break;
                    }
					
					
                    
                }

                $result[$key]['services'][$service_key]['technician'] = $contractorName;
                $result[$key]['services'][$service_key]['service_name'] = $service_info['service_name'];
                $result[$key]['services'][$service_key]['min_price'] = $service_info['min_price'];
                $result[$key]['services'][$service_key]['is_accepted'] = $service['is_accepted'];
                $result[$key]['services'][$service_key]['is_rejected'] = $service['is_rejected'];
                $result[$key]['services'][$service_key]['quantity'] = $qty;
                $result[$key]['services'][$service_key]['unit_price'] = $unit_price;
                $result[$key]['services'][$service_key]['total'] = $modelContractorServiceBooking->getTotalServiceBookingQoute($bookingId, $serviceId, $clone);
                $result[$key]['services'][$service_key]['original_contractor_service_booking_id'] = $service['id'];
                $result[$key]['services'][$service_key]['price_equasion'] = $service_info['price_equasion'];
                $result[$key]['services'][$service_key]['consider_min_price'] = $contractorServiceBooking['consider_min_price'];
                $result[$key]['services'][$service_key]['clone'] = $clone;
                $result[$key]['services'][$service_key]['service_id'] = $serviceId;
				
				
				if(!($is_accepted == '1')){
				  $is_accepted = $service['is_accepted'];
				}
				
				if(!($is_rejected == '1')){
				  $is_rejected = $service['is_rejected'];
				}
				
            }
			
			
			if($is_accepted){
				$result[$key]['accept_status'] = 'accepted';
			}
			elseif($is_rejected){
				$result[$key]['accept_status'] = 'rejected';
			}
			else{
				$result[$key]['accept_status'] = 'unknown';
			}
        }

        $status_colors = array(
			'0' => '#888888',
			'1' => '#cc3333',
			'2' => '#dd4477',
			'3' => '#994499',
			'4' => '#6633cc',
			'5' => '#336699',
			'6' => '#3366cc',
			'7' => '#22aa99',
			'8' => '#329262',
			'9' => '#109618',
			'10' => '#66aa00',
			'11' => '#aaaa11',
			'12' => '#d6ae00',
			'13' => '#ee8800',
			'14' => '#dd5511',
			'15' => '#a87070',
			'16' => '#8c6d8c',
			'17' => '#627487',
			'18' => '#7083a8',
			'19' => '#5c8d87',
			'20' => '#898951',
			'21' => '#b08b59',
			'-1' => '#7a367a'
		);

		$bookingStatus = $modelBookingStatus->getAll();
		$allowedBookingStatus = array();
		foreach($bookingStatus as $key=>$status){
			$allowedBookingStatus[$key]['name']= $status['name'];
			$allowedBookingStatus[$key]['color']= $status_colors[$status['color']];
		}
		$BookingsWithAllowedStatus = array('result'=>$result,'allowedBookingStatus'=>$allowedBookingStatus);
					
        return $BookingsWithAllowedStatus;
    }

    
	
	public function getBookingOriginal($ignoreSynced = true, $filters = array()) {

        $currentPage = $this->request->getParam('current_page', 0);
        $ignoreSyncedDataParam = $this->request->getParam('ignore_synced_data', 0);
        
        $modelBooking = new Model_Booking();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelUser = new Model_User();
        $modelContractorinfo = new Model_ContractorInfo();
        $modelServiceAttribute = new Model_ServiceAttribute();
        $modelServiceAttributeValue = new Model_ServiceAttributeValue();
        $modelAttributes = new Model_Attributes();
        $modelAttributeType = new Model_AttributeType();
        $modelAttributeListValue = new Model_AttributeListValue();
        $modelIosSync = new Model_IosSync();
        $modelBookingEstimate = new Model_BookingEstimate();
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelPropertyType = new Model_PropertyType();
        $modelCustomer = new Model_Customer();
        $modelBookingAddress = new Model_BookingAddress();
        $modelServices = new Model_Services();
        $modelBookingStatus = new Model_BookingStatus();

        if ($ignoreSynced && !$ignoreSyncedDataParam) {
            $syncedBookingIds = $modelIosSync->getSyncedBookingIds($this->iosLoggedUser);
            $filters['not_booking_ids'] = $syncedBookingIds;
        }

        $limit = 100;
        $perPage = 10;
        if (!$currentPage) {
            $limit = 100;
        }
        $pager = null;
        $bookings = $modelBooking->getAll($filters, 'created DESC', $pager, $limit, $perPage, $currentPage);

        $result = array();
        foreach ($bookings as $key => $booking) {

            $db_params = array();
            $db_params['is_sync'] = 0;
            $db_params['ios_user_id'] = $this->iosLoggedUser;
            $db_params['booking_id'] = $booking['booking_id'];

            $syncedBooking = $modelIosSync->getByBookingIdAndIosUser($booking['booking_id'], $this->iosLoggedUser);
            if ($syncedBooking) {
                $modelIosSync->updateById($syncedBooking['id'], $db_params);
            } else {
                $modelIosSync->insert($db_params);
            }

            //
            //booking
            //
            $propertyType = $modelPropertyType->getById($booking['property_type_id']);
            $address = $modelBookingAddress->getByBookingId($booking['booking_id']);
            $is_accepted = $modelBooking->checkBookingIfAccepted($booking['booking_id']);
            $is_rejected = $modelBooking->checkBookingIfRejected($booking['booking_id']);
            $status = $modelBookingStatus->getById($booking['status_id']);

            $result[$key]['booking_num'] = $booking['booking_num'];
            $result[$key]['title'] = $booking['title'];
            $result[$key]['booking_start'] = $booking['booking_start'];
            $result[$key]['booking_end'] = $booking['booking_end'];
            $result[$key]['property_type'] = $propertyType['property_type'];
            $result[$key]['qoute'] = $booking['qoute'];
            $result[$key]['sub_total'] = $booking['sub_total'];
            $result[$key]['total_discount'] = $booking['total_discount'];
            $result[$key]['gst'] = $booking['gst'];
            $result[$key]['paid_amount'] = $booking['paid_amount'];
            $result[$key]['description'] = $booking['description'];
            $result[$key]['status'] = $status['name'];
            $result[$key]['convert_status'] = $booking['convert_status'];
            $result[$key]['created'] = $booking['created'];
			///by islam
			if($is_accepted){
				$result[$key]['accept_status'] = 'accepted';
			}
			elseif($is_rejected){
				$result[$key]['accept_status'] = 'rejected';
			}
			else{
				$result[$key]['accept_status'] = 'unknown';
			}
            //////
			
            
            $result[$key]['original_booking_id'] = $booking['booking_id'];

            //booking address
            $result[$key]['booking_address'] = get_line_address($address);
            $result[$key]['po_box'] = $address['po_box'];
            $result[$key]['postcode'] = $address['postcode'];
            $result[$key]['state'] = $address['state'];
            $result[$key]['street_address'] = $address['street_address'];
            $result[$key]['street_number'] = $address['street_number'];
            $result[$key]['suburb'] = $address['suburb'];
            $result[$key]['unit_lot_number'] = $address['unit_lot_number'];
			///// get booking distance
			$loggedUser = CheckAuth::getLoggedUser();
			$distance = $modelBookingAddress->getDistanceByTwoAddress($loggedUser, $address);
			$result[$key]['booking_distance'] = number_format($distance, 2);
			
            //
            //estimate
            //
            $estimate = $modelBookingEstimate->getNotDeletedByBookingId($booking['booking_id']);

            if ($estimate) {
                $result[$key]['estimate']['created'] = date('Y-m-d H:i:s', $estimate['created']);
                $result[$key]['estimate']['estimate_number'] = $estimate['estimate_num'];
                $result[$key]['estimate']['estimate_type'] = $estimate['estimate_type'];
                $result[$key]['estimate']['original_estimate_id'] = $estimate['id'];
            } else {
                $result[$key]['estimate'] = array();
            }


            //
            //invoice
            //
            $invoice = $modelBookingInvoice->getByBookingId($booking['booking_id']);

            if ($invoice) {
                $result[$key]['invoice']['created'] = date('Y-m-d H:i:s', $invoice['created']);
                $result[$key]['invoice']['invoice_number'] = $invoice['invoice_num'];
                $result[$key]['invoice']['invoice_type'] = $invoice['invoice_type'];
                $result[$key]['invoice']['condition_report'] = $invoice['condition_report'];
                $result[$key]['invoice']['original_invoice_id'] = $invoice['id'];
            } else {
                $result[$key]['invoice'] = array();
            }


            //
            //customer
            //
            $customer = $modelCustomer->getById($booking['customer_id']);
            $customer_name = get_customer_name($customer);

            $result[$key]['customer']['name'] = $customer_name;
            $result[$key]['customer']['address'] = get_line_address($customer);
            $result[$key]['customer']['email1'] = $customer['email1'];
            $result[$key]['customer']['email2'] = $customer['email2'];
            $result[$key]['customer']['email3'] = $customer['email3'];
            $result[$key]['customer']['phone1'] = $customer['phone1'];
            $result[$key]['customer']['phone2'] = $customer['phone2'];
            $result[$key]['customer']['phone3'] = $customer['phone3'];
            $result[$key]['customer']['mobile1'] = $customer['mobile1'];
            $result[$key]['customer']['mobile2'] = $customer['mobile2'];
            $result[$key]['customer']['mobile3'] = $customer['mobile3'];
            $result[$key]['customer']['fax'] = $customer['fax'];
            $result[$key]['customer']['city'] = $customer['city_name'];
            $result[$key]['customer']['country'] = $customer['country_name'];
            $result[$key]['customer']['original_customer_id'] = $customer['customer_id'];


            //
            //services & service_attribute & service_attribute_value
            //
            
            $filters = array();
            $filters['booking_id'] = $booking['booking_id'];
            $services = $modelContractorServiceBooking->getAll($filters);

            foreach ($services as $service_key => $service) {


                $serviceId = isset($service['service_id']) ? $service['service_id'] : 0;
                $clone = isset($service['clone']) ? $service['clone'] : 0;
                $bookingId = isset($service['booking_id']) ? $service['booking_id'] : 0;
                $service_info = $modelServices->getById($service['service_id']);

                $contractorServiceBooking = $modelContractorServiceBooking->getByBookingAndServiceAndClone($bookingId, $serviceId, $clone);
                $contractorInfo = $modelContractorinfo->getByContractorId($contractorServiceBooking['contractor_id']);
                $userInfo = $modelUser->getById($contractorServiceBooking['contractor_id']);

                $contractorName = ucwords($userInfo['username']) . ucwords($contractorInfo['business_name'] ? " - " . $contractorInfo['business_name'] : '');

                $unit_price = 0;
                $qty = 0;

                //get the service attribute id
                $service_attributes = $modelServiceAttribute->getByServiceId($serviceId);
                foreach ($service_attributes as $service_attribute) {

                    $attribute = $modelAttributes->getById($service_attribute['attribute_id']);

                    //get the service attribute value
                    $attributeValue = $modelServiceAttributeValue->getByBookingIdAndServiceAttributeIdAndClone($bookingId, $service_attribute['service_attribute_id'], $clone);

                    switch ($attribute['attribute_name']) {
                        case 'Quantity':
                            $qty = $attributeValue['value'];
                            break;
                        case 'Price':
                            $unit_price = $attributeValue['value'];
                            break;
                        default:
                            $atributetype = $modelAttributeType->getById($attribute['attribute_type_id']);

                            $values = array();

                            // check if the attribute type is list
                            if ($atributetype['is_list']) {
                                // check if the attribute value is serialized
                                if ($attributeValue['is_serialized_array']) {
                                    $unserializeValues = unserialize($attributeValue['value']);
                                    foreach ($unserializeValues as $unserializeKey => $unserializeValue) {
                                        $attributeListValue = $modelAttributeListValue->getById($unserializeValue);

                                        if ($attributeValue['service_attribute_value_id']) {
                                            $attribute_value = array();
                                            $attribute_value['value'] = $attributeListValue['attribute_value'];
                                            $attribute_value['original_service_attribute_value_id'] = $attributeValue['service_attribute_value_id'] . '_' . $unserializeKey;
                                            $values[] = $attribute_value;
                                        }
                                    }
                                } else {
                                    $attributeListValue = $modelAttributeListValue->getById($attributeValue['value']);

                                    if ($attributeValue['service_attribute_value_id']) {
                                        $attribute_value = array();
                                        $attribute_value['value'] = $attributeListValue['attribute_value'];
                                        $attribute_value['original_service_attribute_value_id'] = $attributeValue['service_attribute_value_id'];
                                        $values[] = $attribute_value;
                                    }
                                }
                            } else {
                                if ($attributeValue['service_attribute_value_id']) {
                                    $attribute_value = array();
                                    $attribute_value['value'] = $attributeValue['value'];
                                    $attribute_value['original_service_attribute_value_id'] = $attributeValue['service_attribute_value_id'];
                                    $values[] = $attribute_value;
                                }
                            }

                            if ($attribute['attribute_name']) {

                                $attribute_name = array();
                                $attribute_name['name'] = $attribute['attribute_name'];
                                $attribute_name['original_service_attribute_id'] = $service['id'] . '_' . $service_attribute['service_attribute_id'];
                                $attribute_name['values'] = $values;

                                $result[$key]['services'][$service_key]['service_attribute'][] = $attribute_name;
                            }
                            break;
                    }
                }

                $result[$key]['services'][$service_key]['technician'] = $contractorName;
                $result[$key]['services'][$service_key]['service_name'] = $service_info['service_name'];
                $result[$key]['services'][$service_key]['min_price'] = $service_info['min_price'];
                $result[$key]['services'][$service_key]['is_accepted'] = $service['is_accepted'];
                $result[$key]['services'][$service_key]['is_rejected'] = $service['is_rejected'];
                $result[$key]['services'][$service_key]['quantity'] = $qty;
                $result[$key]['services'][$service_key]['unit_price'] = $unit_price;
                $result[$key]['services'][$service_key]['total'] = $modelContractorServiceBooking->getTotalServiceBookingQoute($bookingId, $serviceId, $clone);
                $result[$key]['services'][$service_key]['original_contractor_service_booking_id'] = $service['id'];
            }
        }

        return $result;
    }
	
	
	
	

	
	public function updateSync($booking_ids = array()) {
        //$booking_ids = $this->request->getParam('booking_ids',array());
		//$booking_ids = (array)json_decode(file_get_contents("php://input"));
        $modelIosSync = new Model_IosSync();
		/*echo "test";
		print_r($booking_ids);
		echo "test";*/
        foreach ($booking_ids as $booking_id) {
            $db_params = array();
            $db_params['is_sync'] = 1;
            $db_params['ios_user_id'] = $this->iosLoggedUser;
            $db_params['booking_id'] = $booking_id;

            $syncedBooking = $modelIosSync->getByBookingIdAndIosUser($booking_id, $this->iosLoggedUser);
			//print_r($syncedBooking);
			//print_r($db_params);
			
            if ($syncedBooking) {
                $modelIosSync->updateById($syncedBooking['id'], $db_params);
            } else {
                $modelIosSync->insert($db_params);
            }
        }
		return $booking_ids;
    }

    public function acceptOrRejectBooking() {
     
		$os = $this->request->getParam('os','ios');
		if($os == 'ios'){
		  $rawData = (array)json_decode(file_get_contents("php://input"));
		  $booking_id = $rawData['booking_id'];
          $services = $rawData['services'];  
		}else{
		  $services = $this->request->getParam('services');
		  $booking_id = $this->request->getParam('booking_id');
		  $services = json_decode($services);
		}
		
		$services_arr = array();
		
		foreach ($services as $key => &$value) {			
			$result2 = array();
			foreach ($value as $k => $v) {
				$result2[$k] = $v;
			}	
			$services_arr[$key] = $result2;
		}
		
		//print_r($services_arr);
		//exit;
		
        $loggedUser = CheckAuth::getLoggedUser();

        $modelContractorServiceBooking = new Model_ContractorServiceBooking();

        $filters = array();
        $filters['booking_id'] = $booking_id;
        if (!CheckAuth::checkCredential(array('canSeeAllBooking'))) {
            $filters['contractor_id'] = $loggedUser['user_id'];
        }
        $bookingServices = $modelContractorServiceBooking->getAll($filters);
         
		 foreach($services_arr as $service){
		 
		 	$modelContractorServiceBooking->updateById($service['original_contractor_service_booking_id'], array('is_accepted' => $service['is_accepted'], 'is_rejected' => $service['is_rejected']));			   
		    /*foreach ($bookingServices as $bookingService) {
			
			 if($bookingService['service_id'] == $service['service_id']){
			
		       $modelContractorServiceBooking->updateById($bookingService['id'], array('is_accepted' => $service['is_accepted'], 'is_rejected' => $service['is_rejected']));			  
			   break;
			 }
			}*/
		 }        


        return $this->getBooking(false, array('booking_id' => $booking_id));
		
    }

    /**
     * Add Payment
     */
    public function addPayment() {

        //
        // check Auth for logged user
        //
        if (!CheckAuth::checkCredential(array('paymentAdd'))) {
            return array('type' => 'error', 'message' => "You Don't Have Permission");
        }

        //
        // get request parameters
        //
        $received_date = $this->request->getParam('received_date', date('Y-m-d H:i:s', time()));
        $bankCharges = $this->request->getParam('bank_charges', 0);
        $amount = (float) $this->request->getParam('amount', 0);
        $description = $this->request->getParam('description', '');
        $reference = $this->request->getParam('reference', '');
        $amountWithheld = $this->request->getParam('amount_withheld', 0);
        $withholdingTax = $this->request->getParam('withholding_tax', 0);
        $isAcknowledgment = $this->request->getParam('is_acknowledgment', 0);
        $paymentTypeId = $this->request->getParam('payment_type_id', 0);
        $bookingId = $this->request->getParam('booking_id', 0);
        $loggedUser = CheckAuth::getLoggedUser();

        //
        // Load Model
        //
        $bookingModel = new Model_Booking();
        $modelPayment = new Model_Payment();

        $booking = $bookingModel->getById($bookingId);
        if (!$booking) {
            return array('type' => 'error', 'message' => "Invalid Booking");
        }

        if (!$bookingModel->checkIfCanEditBooking($booking['booking_id'])) {
            return array('type' => 'error', 'message' => "You Don't Have Permission , You have to Accept This Booking Services First");
        }

        $approvedPayment = $modelPayment->getTotalAmount(array('booking_id' => $booking['booking_id'], 'is_approved' => 'yes'));
        if ($approvedPayment >= $booking['qoute']) {
            return array('type' => 'error', 'message' => "fully Paid");
        }

        $allPayment = $modelPayment->getTotalAmount(array('booking_id' => $booking['booking_id'], 'is_approved' => 'all'));
        if ($allPayment >= $booking['qoute']) {
            return array('type' => 'error', 'message' => "fully Paid,Check Unapproved Payment");
        }

        if ($amount > ($booking['qoute'] - $allPayment)) {
            return array('type' => 'error', 'message' => "Amount entered is larger than amount required, please adjust the amount and try again.");
        }

        if ($amount <= 0) {
            return array('type' => 'error', 'message' => "Invalid Amount");
        }

        //
        // handling the insertion process
        //
        $data = array(
            'received_date' => strtotime($received_date),
            'bank_charges' => round($bankCharges, 2),
            'amount' => round($amount, 2),
            'description' => urldecode($description),
            'payment_type_id' => $paymentTypeId,
            'booking_id' => $booking['booking_id'],
            'customer_id' => $booking['customer_id'],
            'user_id' => $loggedUser['user_id'],
            'created' => time(),
            'reference' => urldecode($reference),
            'amount_withheld' => round($amountWithheld, 2),
            'withholding_tax' => $withholdingTax,
            'is_acknowledgment' => $isAcknowledgment
        );

        $success = $modelPayment->insert($data);

        if ($success) {
            $modelBookingInvoice = new Model_BookingInvoice();
            $modelBookingInvoice->updateByBookingId($booking['booking_id'], array('invoice_type' => 'open'));
            return array('type' => 'success', 'message' => "Saved successfully, Please Approved this Payment", 'result' => $this->getBooking(false, array('booking_id' => $bookingId)) );
        } else {
            return array('type' => 'error', 'message' => "Error in Payment");
        }
    }

    public function getPaymentTypes() {
        $modelPaymentType = new Model_PaymentType();
        return $modelPaymentType->getPaymentTypeAsArray();
    }

    public function getBookingStatus() {
        $modelBookingStatus = new Model_BookingStatus();
        return $modelBookingStatus->getAllStatusAsArrayIOS();
    }

    public function prepareToEdit() {

        $bookingId = $this->request->getParam('booking_id', 0);

        $modelBooking = new Model_Booking();
        $booking = $modelBooking->getById($bookingId);

        $result = array();
        if ($booking) {
            $result['booking_status'] = $this->getBookingStatus();

            $customerSatisfactionLevels = array(
                array('name' => 'Very Dissatisfied', 'value' => 1),
                array('name' => 'Dissatisfied', 'value' => 2),
                array('name' => 'Neutral', 'value' => 3),
                array('name' => 'Satisfied', 'value' => 4),
                array('name' => 'Very Satisfied', 'value' => 5)
            );
            $result['customerSatisfactionLevels'] = $customerSatisfactionLevels;

            $modelProduct = new Model_Product();
            $productNames = $modelProduct->getAllAsIOSArray();
            $result['productNames'] = $productNames; //needs webservice

            $modelCompanies = new Model_Companies();
            $company = $modelCompanies->getById($booking['company_id']);
            $result['callOutFee'] = round($company['call_out_fee'], 2);

            //
            //section_1
            //
            $rows = array();

            $modelCustomer = new Model_Customer();
            $customer = $modelCustomer->getById($booking['customer_id']);
            $customer_name = get_customer_name($customer);

            //row_1
            $row = array();
            $row['type'] = 'textField';
            $row['field']['value'] = !empty($customer_name) ? $customer_name : '';
            $row['field']['enabled'] = "NO";
            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
            $row['name'] = 'customer_id';
            $row['label'] = 'Customer Name';
            $row['hiddenField'] = $customer['customer_id'];
            $row['validation'] = "required";
            $rows[] = $row;

            //fill section
            $result['sections'][] = array('title' => 'Select Customer', 'rows' => $rows);

            //
            //section_2
            //
            $rows = array();

            $modelBookingAddress = new Model_BookingAddress();
            $address = $modelBookingAddress->getByBookingId($booking['booking_id']); //needs webservice
			///very Important
			$modelBookingStatus = new Model_BookingStatus();
            $status = $modelBookingStatus->getById($booking['status_id']);
			$loggedUser = CheckAuth::getLoggedUser();
              
			$allowedForContractor = $modelBooking->checkContractorTimePeriod($booking['booking_id'],$loggedUser['user_id']);
///
		if($allowedForContractor || $status['name'] != 'AWAITING UPDATE' && $status['name'] != 'IN PROGRESS'){
            //row_1
            $row = array();
            $row['type'] = 'textField';
            $row['field']['value'] = !empty($address['unit_lot_number']) ? $address['unit_lot_number'] : '';
            $row['field']['enabled'] = "YES";
            $row['field']['keyboardType'] = 'UIKeyboardTypeNumbersAndPunctuation';
            $row['name'] = 'unit_lot_number';
            $row['label'] = 'Unit/Lot No.';
            $row['hiddenField'] = "NO";
            $row['validation'] = "";
            $rows[] = $row;

            //row_2
            $row = array();
            $row['type'] = 'textField';
            $row['field']['value'] = !empty($address['street_number']) ? $address['street_number'] : '';
            $row['field']['enabled'] = "YES";
            $row['field']['keyboardType'] = 'UIKeyboardTypeNumbersAndPunctuation';
            $row['name'] = 'street_number';
            $row['label'] = 'Street Number';
            $row['hiddenField'] = "NO";
            $row['validation'] = "required";
            $rows[] = $row;

            //row_3
            $row = array();
            $row['type'] = 'textField';
            $row['field']['value'] = !empty($address['street_address']) ? $address['street_address'] : '';
            $row['field']['enabled'] = "YES";
            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
            $row['name'] = 'street_address';
            $row['label'] = 'Street Name';
            $row['hiddenField'] = "NO";
            $row['validation'] = "required";
            $rows[] = $row;
		}
            //row_4
            $row = array();
            $row['type'] = 'textField';
            $row['field']['value'] = !empty($address['suburb']) ? $address['suburb'] : '';
            $row['field']['enabled'] = "YES";
            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
            $row['name'] = 'suburb';
            $row['label'] = 'Suburb';
            $row['hiddenField'] = "NO";
            $row['validation'] = "required";
            $rows[] = $row;
			
			//row_4
            $row = array();
            $row['type'] = 'textField';
            $row['field']['value'] = !empty($address['state']) ? $address['suburb'] : '';
            $row['field']['enabled'] = "YES";
            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
            $row['name'] = 'state';
            $row['label'] = 'state';
            $row['hiddenField'] = "NO";
            $row['validation'] = "required";
            $rows[] = $row;

            //row_5
            $row = array();
            $row['type'] = 'textField';
            $row['field']['value'] = !empty($address['postcode']) ? $address['postcode'] : '';
            $row['field']['enabled'] = "YES";
            $row['field']['keyboardType'] = 'UIKeyboardTypeNumbersAndPunctuation';
            $row['name'] = 'postcode';
            $row['label'] = 'Post Code';
            $row['hiddenField'] = "NO";
            $row['validation'] = "required";
            $rows[] = $row;

		if($allowedForContractor || $status['name'] != 'AWAITING UPDATE' && $status['name'] != 'IN PROGRESS'){
            //row_6
            $row = array();
            $row['type'] = 'textField';
            $row['field']['value'] = !empty($address['po_box']) ? $address['po_box'] : '';
            $row['field']['enabled'] = "YES";
            $row['field']['keyboardType'] = 'UIKeyboardTypeNumbersAndPunctuation';
            $row['name'] = 'po_box';
            $row['label'] = 'P.O Box';
            $row['hiddenField'] = "NO";
            $row['validation'] = "";
            $rows[] = $row;
		}

            //fill section
            $result['sections'][] = array('title' => 'Booking Address', 'rows' => $rows);

            //
            //section_3
            //
            $rows = array();
            
            //row_1
            $row = array();
            $row['type'] = 'bookingStatus';
            $row['field']['value'] = !empty($status['name']) ? $status['name'] : '';
            $row['field']['enabled'] = "YES";
            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
            $row['name'] = 'bookingStatus';
            $row['label'] = 'Booking Status';
            $row['hiddenField'] = !empty($status['booking_status_id']) ? $status['booking_status_id'] : '';
            $row['validation'] = "";
            $rows[] = $row;

            //row_2
            $row = array();
            $row['type'] = 'textField';
            $row['field']['value'] = !empty($booking['title']) ? $booking['title'] : '';
            $row['field']['enabled'] = "NO";
            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
            $row['name'] = 'title';
            $row['label'] = 'Title';
            $row['hiddenField'] = "NO";
            $row['validation'] = "";
            $rows[] = $row;

            //row_3
            $row = array();
            $row['type'] = 'textField';
            $row['field']['value'] = !empty($booking['booking_start']) ? $booking['booking_start'] : '';
            $row['field']['enabled'] = "NO";
            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
            $row['name'] = 'booking_start';
            $row['label'] = 'Start Time';
            $row['hiddenField'] = "NO";
            $row['validation'] = "";
            $rows[] = $row;

            //row_4
            $row = array();
            $row['type'] = 'textField';
            $row['field']['value'] = !empty($booking['booking_end']) ? $booking['booking_end'] : '';
            $row['field']['enabled'] = "NO";
            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
            $row['name'] = 'booking_end';
            $row['label'] = 'End Time';
            $row['hiddenField'] = "NO";
            $row['validation'] = "";
            $rows[] = $row;

            //row_5
            $row = array();
            $modelCities = new Model_Cities();
            $city = $modelCities->getById($booking['city_id']);

            $modelCountries = new Model_Countries();
            $country = $modelCountries->getById($city['country_id']);

            $row['type'] = 'textField';
            $row['field']['value'] = !empty($country['country_name']) ? $country['country_name'] : '';
            $row['field']['enabled'] = "NO";
            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
            $row['name'] = 'country';
            $row['label'] = 'Country';
            $row['hiddenField'] = !empty($country['country_id']) ? $country['country_id'] : '';
            $row['validation'] = "";
            $rows[] = $row;

            //row_6
            $row = array();
            $row['type'] = 'textField';
            $row['field']['value'] = !empty($city['state']) ? $city['state'] : '';
            $row['field']['enabled'] = "NO";
            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
            $row['name'] = 'state';
            $row['label'] = 'State';
            $row['hiddenField'] = "NO";
            $row['validation'] = "";
            $rows[] = $row;

            //row_7
            $row = array();
            $row['type'] = 'textField';
            $row['field']['value'] = !empty($city['city_name']) ? $city['city_name'] : '';
            $row['field']['enabled'] = "NO";
            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
            $row['name'] = 'city_id';
            $row['label'] = 'City';
            $row['hiddenField'] = !empty($city['city_id']) ? $city['city_id'] : '';
            $row['validation'] = "";
            $rows[] = $row;

            //row_8
            $row = array();
            $modelPropertyType = new Model_PropertyType();
            $propertyType = $modelPropertyType->getById($booking['property_type_id']);

            $row['type'] = 'textField';
            $row['field']['value'] = !empty($propertyType['property_type']) ? $propertyType['property_type'] : '';
            $row['field']['enabled'] = "NO";
            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
            $row['name'] = 'property_type';
            $row['label'] = 'Property Type';
            $row['hiddenField'] = !empty($propertyType['id']) ? $propertyType['id'] : '';
            $row['validation'] = "";
            $rows[] = $row;

            //fill section
            $result['sections'][] = array('title' => 'Booking Status', 'rows' => $rows);

            //
            //section_4
            //
            $rows = array();

            //row_1
            $row = array();
            $row['type'] = 'textView';
            $row['field']['value'] = !empty($booking['description']) ? str_replace("\'", "'", $booking['description']) : '';
            $row['field']['enabled'] = "NO";
            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
            $row['name'] = 'description';
            $row['label'] = 'Description';
            $row['hiddenField'] = "NO";
            $row['validation'] = "";
            $rows[] = $row;

            //fill section
            $result['sections'][] = array('title' => 'Booking Description', 'rows' => $rows);

            //
            //section_5
            //
            if ($status) {
                if ($status['name'] != 'TO DO' && $status['name'] != 'IN PROGRESS') {
                    /**
                     * get why Discussion
                     */
                    $modelBookingStatusDiscussion = new Model_BookingStatusDiscussion();
                    $whyDiscussion = $modelBookingStatusDiscussion->getLastDiscussionByBookingIdAndStatusId($bookingId, $booking['status_id']);

                    $rows = array();

                    switch ($status['name']) {
                        case 'AWAITING UPDATE':
                            //row_1
                            $row = array();
                            $row['type'] = 'textView';
                            $row['field']['value'] = !empty($whyDiscussion['user_message']) ? str_replace("\'", "'", $whyDiscussion['user_message']) : '';
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'why';
                            $row['label'] = 'Why Awaiting Update ?';
                            $row['hiddenField'] = "NO";
                            $row['validation'] = "required";
                            $rows[] = $row;
                            break;
                        case 'CANCELLED':
                            //row_1
                            $row = array();
                            $row['type'] = 'textView';
                            $row['field']['value'] = !empty($whyDiscussion['user_message']) ? str_replace("\'", "'", $whyDiscussion['user_message']) : '';
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'why';
                            $row['label'] = 'Why Cancelled ?';
                            $row['hiddenField'] = "NO";
                            $row['validation'] = "required";
                            $rows[] = $row;
                            break;
                        case 'FAILED':
                            //row_1
                            $row = array();
                            $row['type'] = 'textField';
                            $row['field']['value'] = !empty($booking['onsite_client_name']) ? $booking['onsite_client_name'] : '';
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'onsite_client_name';
                            $row['label'] = 'Onsite Client Name';
                            $row['hiddenField'] = "NO";
                            $row['validation'] = "required";
                            $rows[] = $row;

                            //row_2
                            $row = array();
                            $row['type'] = 'dateTime';
                            $row['field']['value'] = !empty($booking['job_start_time']) ? date('Y-m-d H:i:s', $booking['job_start_time']) : '';
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'job_start_time';
                            $row['label'] = 'Job Start Time';
                            $row['hiddenField'] = "NO";
                            $row['validation'] = "required";
                            $rows[] = $row;

                            //row_3
                            $row = array();
                            $row['type'] = 'dateTime';
                            $row['field']['value'] = !empty($booking['job_finish_time']) ? date('Y-m-d H:i:s', $booking['job_finish_time']) : '';
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'job_finish_time';
                            $row['label'] = 'Job Finish Time';
                            $row['hiddenField'] = "NO";
                            $row['validation'] = "required";
                            $rows[] = $row;

                            //row_4
                            $satisfaction = 'Very Dissatisfied';
                            if (!empty($booking['satisfaction'])) {
                                if ($booking['satisfaction'] == 1) {
                                    $satisfaction = 'Very Dissatisfied';
                                }
                                if ($booking['satisfaction'] == 2) {
                                    $satisfaction = 'Dissatisfied';
                                }
                                if ($booking['satisfaction'] == 3) {
                                    $satisfaction = 'Neutral';
                                }
                                if ($booking['satisfaction'] == 4) {
                                    $satisfaction = 'Satisfied';
                                }
                                if ($booking['satisfaction'] == 5) {
                                    $satisfaction = 'Very Satisfied';
                                }
                            }
                            $row = array();
                            $row['type'] = 'pickerView';
                            $row['field']['value'] = $satisfaction;
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'satisfaction';
                            $row['label'] = 'Customer Satisfaction';
                            $row['hiddenField'] = !empty($booking['satisfaction']) ? $booking['satisfaction'] : '1';
                            $row['data'] = $customerSatisfactionLevels;
                            $row['validation'] = "required";
                            $rows[] = $row;

                            //row_5
                            $modelBookingProduct = new Model_BookingProduct();
                            $allBookingProduct = $modelBookingProduct->getByBookingId($bookingId);
                            if ($allBookingProduct) {
                                foreach ($allBookingProduct as $key => $bookingProduct) {
                                    $product = $modelProduct->getById($bookingProduct['product_id']);
                                    if ($product) {
                                        $row = array();
                                        $row['type'] = 'pickerView';
                                        $row['field']['value'] = $product['product'];
                                        $row['field']['enabled'] = "YES";
                                        $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                                        $row['name'] = "product[{$key}]";
                                        $row['label'] = 'Product Name';
                                        $row['hiddenField'] = $bookingProduct['product_id'];
                                        $row['data'] = $productNames;
                                        $row['validation'] = "required";
                                        if ($key != 0) {
                                            $row['removeButton'] = "YES";
                                        }
                                        $rows[] = $row;

                                        $row = array();
                                        $row['type'] = 'textField';
                                        $row['field']['value'] = $bookingProduct['ltr'];
                                        $row['field']['enabled'] = "YES";
                                        $row['field']['keyboardType'] = 'UIKeyboardTypeDecimalPad';
                                        $row['name'] = "ltr[{$key}]";
                                        $row['label'] = 'Product Ltr';
                                        $row['hiddenField'] = 'NO';
                                        $row['validation'] = "required";
                                        $rows[] = $row;
                                    }
                                }
                            } else {
                                $row = array();
                                $row['type'] = 'pickerView';
                                $row['field']['value'] = '';
                                $row['field']['enabled'] = "YES";
                                $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                                $row['name'] = "product[0]";
                                $row['label'] = 'Product Name';
                                $row['hiddenField'] = '';
                                $row['data'] = $productNames;
                                $row['validation'] = "required";
                                $rows[] = $row;

                                $row = array();
                                $row['type'] = 'textField';
                                $row['field']['value'] = '';
                                $row['field']['enabled'] = "YES";
                                $row['field']['keyboardType'] = 'UIKeyboardTypeDecimalPad';
                                $row['name'] = "ltr[0]";
                                $row['label'] = 'Product Ltr';
                                $row['hiddenField'] = 'NO';
                                $row['validation'] = "required";
                                $rows[] = $row;
                            }

                            $row = array();
                            $row['type'] = 'button';
                            $row['field']['value'] = 'Add More Product';
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = "add_more_product";
                            $row['label'] = 'Press here to Add More Product';
                            $row['hiddenField'] = 'NO';
                            $row['validation'] = "";
                            $rows[] = $row;

                            //row_6
                            $row = array();
                            $row['type'] = 'textView';
                            $row['field']['value'] = !empty($whyDiscussion['user_message']) ? str_replace("\'", "'", $whyDiscussion['user_message']) : '';
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'why';
                            $row['label'] = 'Why Failed ?';
                            $row['hiddenField'] = "NO";
                            $row['validation'] = "required";
                            $rows[] = $row;
                            break;
                        case 'COMPLETED':
                            //row_1
                            $row = array();
                            $row['type'] = 'textField';
                            $row['field']['value'] = !empty($booking['onsite_client_name']) ? $booking['onsite_client_name'] : '';
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'onsite_client_name';
                            $row['label'] = 'Onsite Client Name';
                            $row['hiddenField'] = "NO";
                            $row['validation'] = "required";
                            $rows[] = $row;

                            //row_2
                            $row = array();
                            $row['type'] = 'dateTime';
                            $row['field']['value'] = !empty($booking['job_start_time']) ? date('Y-m-d H:i:s', $booking['job_start_time']) : '';
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'job_start_time';
                            $row['label'] = 'Job Start Time';
                            $row['hiddenField'] = "NO";
                            $row['validation'] = "required";
                            $rows[] = $row;

                            //row_3
                            $row = array();
                            $row['type'] = 'dateTime';
                            $row['field']['value'] = !empty($booking['job_finish_time']) ? date('Y-m-d H:i:s', $booking['job_finish_time']) : '';
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'job_finish_time';
                            $row['label'] = 'Job Finish Time';
                            $row['hiddenField'] = "NO";
                            $row['validation'] = "required";
                            $rows[] = $row;

                            $row = array();
                            $row['type'] = 'textView';
                            $row['field']['value'] = !empty($whyDiscussion['user_message']) ? str_replace("\'", "'", $whyDiscussion['user_message']) : '';
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'extra_comments';
                            $row['label'] = 'Extra Comments';
                            $row['hiddenField'] = "NO";
                            $row['validation'] = "";
                            $rows[] = $row;

                            //row_4
                            $satisfaction = 'Very Dissatisfied';
                            if (!empty($booking['satisfaction'])) {
                                if ($booking['satisfaction'] == 1) {
                                    $satisfaction = 'Very Dissatisfied';
                                }
                                if ($booking['satisfaction'] == 2) {
                                    $satisfaction = 'Dissatisfied';
                                }
                                if ($booking['satisfaction'] == 3) {
                                    $satisfaction = 'Neutral';
                                }
                                if ($booking['satisfaction'] == 4) {
                                    $satisfaction = 'Satisfied';
                                }
                                if ($booking['satisfaction'] == 5) {
                                    $satisfaction = 'Very Satisfied';
                                }
                            }
                            $row = array();
                            $row['type'] = 'pickerView';
                            $row['field']['value'] = $satisfaction;
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'satisfaction';
                            $row['label'] = 'Customer Satisfaction';
                            $row['hiddenField'] = !empty($booking['satisfaction']) ? $booking['satisfaction'] : '1';
                            $row['data'] = $customerSatisfactionLevels;
                            $row['validation'] = "required";
                            $rows[] = $row;

                            //row_5
                            $modelBookingProduct = new Model_BookingProduct();
                            $allBookingProduct = $modelBookingProduct->getByBookingId($bookingId);
                            if ($allBookingProduct) {
                                foreach ($allBookingProduct as $key => $bookingProduct) {
                                    $product = $modelProduct->getById($bookingProduct['product_id']);
                                    if ($product) {
                                        $row = array();
                                        $row['type'] = 'pickerView';
                                        $row['field']['value'] = $product['product'];
                                        $row['field']['enabled'] = "YES";
                                        $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                                        $row['name'] = "product[{$key}]";
                                        $row['label'] = 'Product Name';
                                        $row['hiddenField'] = $bookingProduct['product_id'];
                                        $row['data'] = $productNames;
                                        $row['validation'] = "required";
                                        if ($key != 0) {
                                            $row['removeButton'] = "YES";
                                        }
                                        $rows[] = $row;

                                        $row = array();
                                        $row['type'] = 'textField';
                                        $row['field']['value'] = $bookingProduct['ltr'];
                                        $row['field']['enabled'] = "YES";
                                        $row['field']['keyboardType'] = 'UIKeyboardTypeDecimalPad';
                                        $row['name'] = "ltr[{$key}]";
                                        $row['label'] = 'Product Ltr';
                                        $row['hiddenField'] = 'NO';
                                        $row['validation'] = "required";
                                        $rows[] = $row;
                                    }
                                }
                            } else {
                                $row = array();
                                $row['type'] = 'pickerView';
                                $row['field']['value'] = '';
                                $row['field']['enabled'] = "YES";
                                $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                                $row['name'] = "product[0]";
                                $row['label'] = 'Product Name';
                                $row['hiddenField'] = '';
                                $row['data'] = $productNames;
                                $row['validation'] = "required";
                                $rows[] = $row;

                                $row = array();
                                $row['type'] = 'textField';
                                $row['field']['value'] = '';
                                $row['field']['enabled'] = "YES";
                                $row['field']['keyboardType'] = 'UIKeyboardTypeDecimalPad';
                                $row['name'] = "ltr[0]";
                                $row['label'] = 'Product Ltr';
                                $row['hiddenField'] = 'NO';
                                $row['validation'] = "required";
                                $rows[] = $row;
                            }

                            $row = array();
                            $row['type'] = 'button';
                            $row['field']['value'] = 'Add More Product';
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = "add_more_product";
                            $row['label'] = 'Press here to Add More Product';
                            $row['hiddenField'] = 'NO';
                            $row['validation'] = "";
                            $rows[] = $row;

                            break;
                        case 'ON HOLD':
                            //row_1
                            $row = array();
                            $row['type'] = 'textView';
                            $row['field']['value'] = !empty($whyDiscussion['user_message']) ? str_replace("\'", "'", $whyDiscussion['user_message']) : '';
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'why';
                            $row['label'] = 'Why On Hold ?';
                            $row['hiddenField'] = "NO";
                            $row['validation'] = "required";
                            $rows[] = $row;

                            //row_2
                            $row = array();
                            $row['type'] = 'dateTime';
                            $row['field']['value'] = !empty($booking['to_follow']) ? date('Y-m-d H:i:s', $booking['to_follow']) : '';
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'to_follow';
                            $row['label'] = 'To Follow';
                            $row['hiddenField'] = "NO";
                            $row['validation'] = "required";
                            $rows[] = $row;
                            break;
                        case 'TENTATIVE':
                            //row_1
                            $row = array();
                            $row['type'] = 'textView';
                            $row['field']['value'] = !empty($whyDiscussion['user_message']) ? str_replace("\'", "'", $whyDiscussion['user_message']) : '';
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'why';
                            $row['label'] = 'Why Tentative ?';
                            $row['hiddenField'] = "NO";
                            $row['validation'] = "required";
                            $rows[] = $row;
                            break;
                        case 'TO VISIT':
                            //row_1
                            $row = array();
                            $row['type'] = 'textView';
                            $row['field']['value'] = !empty($whyDiscussion['user_message']) ? str_replace("\'", "'", $whyDiscussion['user_message']) : '';
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'why';
                            $row['label'] = 'Why To Visit ?';
                            $row['hiddenField'] = "NO";
                            $row['validation'] = "required";
                            $rows[] = $row;
                            break;
                        case 'QUOTED':
                            //row_1
                            $row = array();
                            $row['type'] = 'switch';
                            $row['field']['value'] = isset($booking['is_to_follow']) ? $booking['is_to_follow'] : 0;
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'is_to_follow';
                            $row['label'] = 'Confirm To Follow';
                            $row['hiddenField'] = "NO";
                            $row['validation'] = "";
                            $rows[] = $row;

                            //row_2
                            $row = array();
                            $row['type'] = 'dateTime';
                            $row['field']['value'] = !empty($booking['to_follow']) ? date('Y-m-d H:i:s', $booking['to_follow']) : '';
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'to_follow';
                            $row['label'] = 'To Follow';
                            $row['hiddenField'] = "NO";
                            $row['validation'] = "required";
                            $rows[] = $row;
                            break;
                    }

                    //fill section
                    $result['sections'][] = array('title' => 'Extra Info', 'rows' => $rows);
                }
            }

            //
            //services & service_attribute & service_attribute_value
            //
            $modelContractorServiceBooking = new Model_ContractorServiceBooking();
            $modelUser = new Model_User();
            $modelContractorinfo = new Model_ContractorInfo();
            $modelServiceAttribute = new Model_ServiceAttribute();
            $modelServiceAttributeValue = new Model_ServiceAttributeValue();
            $modelAttributes = new Model_Attributes();
            $modelAttributeType = new Model_AttributeType();
            $modelAttributeListValue = new Model_AttributeListValue();
            $modelServices = new Model_Services();
            $services = $modelContractorServiceBooking->getAll(array('booking_id' => $booking['booking_id']));

            if ($services) {
                $serviceNum = 0;
                foreach ($services as $service) {

                    $rows = array();

                    $serviceId = isset($service['service_id']) ? $service['service_id'] : 0;
                    $clone = isset($service['clone']) ? $service['clone'] : 0;
                    $bookingId = isset($service['booking_id']) ? $service['booking_id'] : 0;
                    $service_info = $modelServices->getById($service['service_id']);

                    $contractorServiceBooking = $modelContractorServiceBooking->getByBookingAndServiceAndClone($bookingId, $serviceId, $clone);
                    $contractorInfo = $modelContractorinfo->getByContractorId($contractorServiceBooking['contractor_id']);
                    $userInfo = $modelUser->getById($contractorServiceBooking['contractor_id']);

                    $contractorName = ucwords($userInfo['username']) . ucwords($contractorInfo['business_name'] ? " - " . $contractorInfo['business_name'] : '');

                    $row = array();
                    $row['type'] = 'textField';
                    $row['field']['value'] = !empty($contractorName) ? $contractorName : '';
                    $row['field']['enabled'] = "NO";
                    $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                    $row['name'] = 'contractor_' . $serviceId . ($clone ? '_' . $clone : '');
                    $row['label'] = 'Technician';
                    $row['hiddenField'] = $contractorServiceBooking['contractor_id'];
                    $row['validation'] = "";
                    $rows[] = $row;

                    $row = array();
                    $row['type'] = 'textField';
                    $row['field']['value'] = !empty($service_info['service_name']) ? $service_info['service_name'] : '';
                    $row['field']['enabled'] = "NO";
                    $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                    $row['name'] = 'services[' . $serviceNum . ']';
                    $row['label'] = 'Service Name';
                    $row['hiddenField'] = $serviceId . ($clone ? '_' . $clone : '');
                    $row['validation'] = "";
                    $rows[] = $row;


                    //get the service attribute id
                    $service_attributes = $modelServiceAttribute->getByServiceId($serviceId);
                    foreach ($service_attributes as $service_attribute) {

                        $attribute = $modelAttributes->getById($service_attribute['attribute_id']);

                        if ($attribute) {
                            //get the service attribute value
                            $attributeValue = $modelServiceAttributeValue->getByBookingIdAndServiceAttributeIdAndClone($bookingId, $service_attribute['service_attribute_id'], $clone);
                            $atributetype = $modelAttributeType->getById($attribute['attribute_type_id']);

                            $name = 'attribute_' . $service_attribute['service_id'] . $service_attribute['attribute_id'] . ($clone ? '_' . $clone : '');

                            switch ($atributetype['attribute_type']) {
                                case 'checkbox':
                                    $row = array();
                                    $row['type'] = 'switch';
                                    $row['field']['value'] = !empty($attributeValue['value']) ? $attributeValue['value'] : '';
                                    $row['field']['enabled'] = "YES";
                                    $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                                    $row['name'] = $name;
                                    $row['label'] = $attribute['attribute_name'];
                                    $row['hiddenField'] = "NO";
                                    if ($attribute['is_price_attribute']) {
                                        $row['priceAttribute']['name'] = $attribute['attribute_variable_name'];
                                        $row['priceAttribute']['serviceId'] = $service_attribute['service_id'];
                                        $row['priceAttribute']['clone'] = $clone;
                                        $row['priceAttribute']['equasion'] = $service_info['price_equasion'];
                                        $row['priceAttribute']['min_price'] = !empty($service_info['min_price']) ? $service_info['min_price'] : '';
                                    } else {
                                        $row['priceAttribute'] = "NO";
                                    }
                                    $row['validation'] = "";
                                    $rows[] = $row;
                                    break;

                                case 'date':
                                    $row = array();
                                    $row['type'] = 'date';
                                    $row['field']['value'] = !empty($attributeValue['value']) ? $attributeValue['value'] : '';
                                    $row['field']['enabled'] = "YES";
                                    $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                                    $row['name'] = $name;
                                    $row['label'] = $attribute['attribute_name'];
                                    $row['hiddenField'] = "NO";
                                    if ($attribute['is_price_attribute']) {
                                        $row['priceAttribute']['name'] = $attribute['attribute_variable_name'];
                                        $row['priceAttribute']['serviceId'] = $service_attribute['service_id'];
                                        $row['priceAttribute']['clone'] = $clone;
                                        $row['priceAttribute']['equasion'] = $service_info['price_equasion'];
                                        $row['priceAttribute']['min_price'] = !empty($service_info['min_price']) ? $service_info['min_price'] : '';
                                    } else {
                                        $row['priceAttribute'] = "NO";
                                    }
                                    $row['validation'] = "";
                                    $rows[] = $row;
                                    break;

                                case 'time':
                                    $row = array();
                                    $row['type'] = 'time';
                                    $row['field']['value'] = !empty($attributeValue['value']) ? $attributeValue['value'] : '';
                                    $row['field']['enabled'] = "YES";
                                    $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                                    $row['name'] = $name;
                                    $row['label'] = $attribute['attribute_name'];
                                    $row['hiddenField'] = "NO";
                                    if ($attribute['is_price_attribute']) {
                                        $row['priceAttribute']['name'] = $attribute['attribute_variable_name'];
                                        $row['priceAttribute']['serviceId'] = $service_attribute['service_id'];
                                        $row['priceAttribute']['clone'] = $clone;
                                        $row['priceAttribute']['equasion'] = $service_info['price_equasion'];
                                        $row['priceAttribute']['min_price'] = !empty($service_info['min_price']) ? $service_info['min_price'] : '';
                                    } else {
                                        $row['priceAttribute'] = "NO";
                                    }
                                    $row['validation'] = "";
                                    $rows[] = $row;
                                    break;

                                case 'long_text':
                                    $row = array();
                                    $row['type'] = 'textView';
                                    $row['field']['value'] = !empty($attributeValue['value']) ? $attributeValue['value'] : '';
                                    $row['field']['enabled'] = "YES";
                                    $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                                    $row['name'] = $name;
                                    $row['label'] = $attribute['attribute_name'];
                                    $row['hiddenField'] = "NO";
                                    if ($attribute['is_price_attribute']) {
                                        $row['priceAttribute']['name'] = $attribute['attribute_variable_name'];
                                        $row['priceAttribute']['serviceId'] = $service_attribute['service_id'];
                                        $row['priceAttribute']['clone'] = $clone;
                                        $row['priceAttribute']['equasion'] = $service_info['price_equasion'];
                                        $row['priceAttribute']['min_price'] = !empty($service_info['min_price']) ? $service_info['min_price'] : '';
                                    } else {
                                        $row['priceAttribute'] = "NO";
                                    }
                                    $row['validation'] = "";
                                    $rows[] = $row;
                                    break;

                                case 'text_input':
                                    $row = array();
                                    $row['type'] = 'textField';
                                    $row['field']['value'] = !empty($attributeValue['value']) ? $attributeValue['value'] : '';
                                    $row['field']['enabled'] = "YES";
                                    if ($attribute['attribute_variable_name'] == 'price') {
                                        $row['field']['keyboardType'] = 'UIKeyboardTypeNumbersAndPunctuation';
                                    } else if ($attribute['attribute_variable_name'] == 'quantity') {
                                        $row['field']['keyboardType'] = 'UIKeyboardTypeNumbersAndPunctuation';
                                    } else if ($attribute['attribute_variable_name'] == 'discount') {
                                        $row['field']['keyboardType'] = 'UIKeyboardTypeNumbersAndPunctuation';
                                    } else {
                                        $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                                    }
                                    $row['name'] = $name;
                                    $row['label'] = $attribute['attribute_name'];
                                    $row['hiddenField'] = "NO";
                                    if ($attribute['is_price_attribute']) {
                                        $row['priceAttribute']['name'] = $attribute['attribute_variable_name'];
                                        $row['priceAttribute']['serviceId'] = $service_attribute['service_id'];
                                        $row['priceAttribute']['clone'] = $clone;
                                        $row['priceAttribute']['equasion'] = $service_info['price_equasion'];
                                        $row['priceAttribute']['min_price'] = !empty($service_info['min_price']) ? $service_info['min_price'] : '';
                                    } else {
                                        $row['priceAttribute'] = "NO";
                                    }
                                    $row['validation'] = "";
                                    $rows[] = $row;
                                    break;

                                case 'dropdown':
                                case 'enum':
                                    $row = array();
                                    $attributeListValue = $modelAttributeListValue->getById($attributeValue['value']);
                                    $attributeListValues = $modelAttributeListValue->getByAttributeIdAsIosArray($service_attribute['attribute_id']);

                                    $row['type'] = 'pickerView';
                                    $row['field']['value'] = !empty($attributeListValue['attribute_value']) ? $attributeListValue['attribute_value'] : '';
                                    $row['field']['enabled'] = "YES";
                                    $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                                    $row['name'] = $name;
                                    $row['label'] = $attribute['attribute_name'];
                                    $row['hiddenField'] = $attributeValue['value'];
                                    $row['data'] = $attributeListValues;
                                    if ($attribute['is_price_attribute']) {
                                        $row['priceAttribute']['name'] = $attribute['attribute_variable_name'];
                                        $row['priceAttribute']['serviceId'] = $service_attribute['service_id'];
                                        $row['priceAttribute']['clone'] = $clone;
                                        $row['priceAttribute']['equasion'] = $service_info['price_equasion'];
                                        $row['priceAttribute']['min_price'] = !empty($service_info['min_price']) ? $service_info['min_price'] : '';
                                    } else {
                                        $row['priceAttribute'] = "NO";
                                    }
                                    $row['validation'] = "";
                                    $rows[] = $row;
                                    break;


                                case 'list':
                                case 'set':
                                    $row = array();
                                    $attributeListValues = $modelAttributeListValue->getByAttributeIdAsIosArray($service_attribute['attribute_id']);

                                    $row['type'] = 'alPickerView';
                                    if ($attributeValue['is_serialized_array']) {
                                        $values = array();
                                        $unserializeValues = unserialize($attributeValue['value']);
                                        foreach ($unserializeValues as $key => $unserializeValue) {
                                            $attributeListValue = $modelAttributeListValue->getById($unserializeValue);
                                            $values[] = array('name' => $attributeListValue['attribute_value'], 'value' => $attributeListValue['attribute_value_id']);
                                        }
                                        $row['field']['value'] = $values;
                                    } else {
                                        $row['field']['value'] = !empty($attributeListValue['attribute_value']) ? $attributeListValue['attribute_value'] : '';
                                    }
                                    $row['field']['enabled'] = "YES";
                                    $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                                    $row['name'] = $name;
                                    $row['label'] = $attribute['attribute_name'];
                                    $row['hiddenField'] = "NO";
                                    $row['data'] = $attributeListValues;
                                    if ($attribute['is_price_attribute']) {
                                        $row['priceAttribute']['name'] = $attribute['attribute_variable_name'];
                                        $row['priceAttribute']['serviceId'] = $service_attribute['service_id'];
                                        $row['priceAttribute']['clone'] = $clone;
                                        $row['priceAttribute']['equasion'] = $service_info['price_equasion'];
                                        $row['priceAttribute']['min_price'] = !empty($service_info['min_price']) ? $service_info['min_price'] : '';
                                    } else {
                                        $row['priceAttribute'] = "NO";
                                    }
                                    $row['validation'] = "";
                                    $rows[] = $row;
                                    break;
                            }
                        }
                    }


                    $row = array();
                    $row['type'] = 'switch';
                    $row['field']['value'] = isset($service['consider_min_price']) ? $service['consider_min_price'] : 0;
                    $row['field']['enabled'] = "YES";
                    $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                    $row['name'] = 'consider_min_price_' . $serviceId . ($clone ? '_' . $clone : '');
                    $row['label'] = 'Consider Minimum Price ($ ' . (!empty($service_info['min_price']) ? $service_info['min_price'] : '') . ')';
                    $row['priceAttribute']['name'] = 'consider_minimum_price';
                    $row['priceAttribute']['serviceId'] = $serviceId;
                    $row['priceAttribute']['clone'] = $clone;
                    $row['priceAttribute']['equasion'] = $service_info['price_equasion'];
                    $row['priceAttribute']['min_price'] = !empty($service_info['min_price']) ? $service_info['min_price'] : '';
                    $row['hiddenField'] = "NO";
                    $row['validation'] = "";
                    $rows[] = $row;

                    $row = array();
                    $row['type'] = 'textField';
                    $row['field']['value'] = round($modelContractorServiceBooking->getServiceBookingQoute($bookingId, $serviceId, $clone), 2);
                    $row['field']['enabled'] = "NO";
                    $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                    $row['name'] = 'total_' . $serviceId . ($clone ? '_' . $clone : '');
                    $row['label'] = 'Total';
                    $row['hiddenField'] = "NO";
                    $row['validation'] = "";
                    $rows[] = $row;

                    $row = array();
                    $row['type'] = 'textField';
                    $row['field']['value'] = round($modelContractorServiceBooking->getTotalServiceBookingQoute($bookingId, $serviceId, $clone), 2);
                    $row['field']['enabled'] = "NO";
                    $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                    $row['name'] = 'amount_' . $serviceId . ($clone ? '_' . $clone : '');
                    $row['label'] = 'Amount';
                    $row['hiddenField'] = "NO";
                    $row['validation'] = "";
                    $rows[] = $row;

                    $serviceNum++;

                    //fill section
                    $result['sections'][] = array('title' => (!empty($service_info['service_name']) ? $service_info['service_name'] : ''), 'rows' => $rows);
                }
            }

            //
            //section_6
            //
            $rows = array();

            //row_1
            $row = array();
            $row['type'] = 'textField';
            $row['field']['value'] = round(!empty($booking['sub_total']) ? $booking['sub_total'] : 0, 2);
            $row['field']['enabled'] = "NO";
            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
            $row['name'] = 'sub_total';
            $row['label'] = 'Sub Total:';
            $row['hiddenField'] = "NO";
            $row['validation'] = "";
            $rows[] = $row;

            if (!empty($status['name']) && $status['name'] == 'FAILED') {
                $row = array();
                $row['type'] = 'textField';
                $row['field']['value'] = round(!empty($booking['call_out_fee']) ? $booking['call_out_fee'] : 0, 2);
                $row['field']['enabled'] = "YES";
                $row['field']['keyboardType'] = 'UIKeyboardTypeDecimalPad';
                $row['name'] = 'call_out_fee';
                $row['label'] = 'Call Out Fee:';
                $row['hiddenField'] = "NO";
                $row['validation'] = "";
                $rows[] = $row;
            }

            //row_2
            $row = array();
            $row['type'] = 'textField';
            $row['field']['value'] = round(!empty($booking['total_discount']) ? $booking['total_discount'] : 0, 2);
            $row['field']['enabled'] = "YES";
            $row['field']['keyboardType'] = 'UIKeyboardTypeDecimalPad';
            $row['name'] = 'total_discount';
            $row['label'] = 'Discount:';
            $row['hiddenField'] = "NO";
            $row['validation'] = "";
            $rows[] = $row;

            //row_3
            $row = array();
            $row['type'] = 'textField';
            $row['field']['value'] = round(!empty($booking['gst']) ? $booking['gst'] : 0, 2);
            $row['field']['enabled'] = "NO";
            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
            $row['name'] = 'gst';
            $row['label'] = 'GST (10%):';
            $row['hiddenField'] = "NO";
            $row['validation'] = "";
            $rows[] = $row;

            //row_4
            $row = array();
            $row['type'] = 'textField';
            $row['field']['value'] = round(!empty($booking['qoute']) ? $booking['qoute'] : 0, 2);
            $row['field']['enabled'] = "NO";
            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
            $row['name'] = 'qoute';
            $row['label'] = 'Total';
            $row['hiddenField'] = "NO";
            $row['validation'] = "";
            $rows[] = $row;

            //fill section
            $result['sections'][] = array('title' => 'Total Booking Cost', 'rows' => $rows);
        }

        return $result;
    }
	
	public function saveBookingOffline($mode='update',$booking) {

        /**
         * get request Params
         */
		//echo 'we are here.......';
		//print_r($booking);
        $title = '';
        $bookingId =  isset($booking['booking_id']) ? $booking['booking_id'] : 0; 
        $inquiryId =  isset($booking['inquiry_id']) ? $booking['inquiry_id'] : 0; 
        $toEstimate =  isset($booking['toEstimate']) ? $booking['toEstimate'] : 0; 
        $st =  isset($booking['booking_start']) ? $booking['booking_start'] : "0000-00-00 00:00:00"; 
        $et =  isset($booking['booking_end']) ? $booking['booking_end'] : "0000-00-00 00:00:00"; 
        $isAllDayEvent =  isset($booking['isAllDayEvent']) ? $booking['isAllDayEvent'] : 0; 
        $description_booking =  isset($booking['description']) ? $booking['description'] : ''; 
        $cityId =  isset($booking['city_id']) ? $booking['city_id'] : 0; 
        $statusId =  isset($booking['status']) ? $booking['status'] : 0; 
        $customer_id =  isset($booking['customer_id']) ? $booking['customer_id'] : 0; 
        $totalDiscount =  isset($booking['total_discount']) ? $booking['total_discount'] : 0; 
        $onsiteClientName =  isset($booking['onsite_client_name']) ? $booking['onsite_client_name'] : ''; 
        $satisfaction =  isset($booking['satisfaction']) ? $booking['satisfaction'] : 1; 
        $jobStartTime =  isset($booking['job_start_time']) ? $booking['job_start_time'] : $st; 
        $jobFinishTime =  isset($booking['job_finish_time']) ? $booking['job_finish_time'] : $et; 
        $services =  isset($booking['services']) ? (array)$booking['services'] : array();
        $services = (array) $services;
        $product_ids =  isset($booking['product']) ? $booking['product'] : array();
        $ltrs =  isset($booking['ltr']) ? $booking['ltr'] : array();
        $propertyTypeId =  isset($booking['property_type']) ? $booking['property_type'] : 0;
        $callOutFee =  isset($booking['call_out_fee']) ? $booking['call_out_fee'] : 0;
        $to_follow =  isset($booking['to_follow']) ? $booking['to_follow'] : '';
        $is_to_follow =  isset($booking['is_to_follow']) ? $booking['is_to_follow'] : 0;
        $multi_stpartdate =  isset($booking['multi_stpartdate']) ? $booking['multi_stpartdate'] : 0;
        $confirmResetAttribut =  isset($booking['confirmResetAttribut']) ? $booking['confirmResetAttribut'] : 0;
	$BookingDates =  isset($booking['BookingDates']) ? $booking['BookingDates'] : 0;
        $date_visited =  isset($booking['is_visited']) ? $booking['is_visited'] : 0;
        $extra_comment =  isset($booking['extra_comment']) ? $booking['extra_comment'] : 0;
        $products =  isset($booking['products']) ? $booking['products'] : 0;
        /*
		$bookingId = $this->request->getParam('booking_id', 0);
        $inquiryId = $this->request->getParam('inquiry_id', 0);
        $toEstimate = $this->request->getParam('toEstimate', 0);
        $st = $this->request->getParam('booking_start', "0000-00-00 00:00:00");
        $et = $this->request->getParam('booking_end', "0000-00-00 00:00:00");
        $isAllDayEvent = $this->request->getParam('isAllDayEvent', 0);
        $description_booking = $this->request->getParam('description', '');
        $cityId = $this->request->getParam('city_id', 0);
        $statusId = $this->request->getParam('bookingStatus', 0);
        $customer_id = $this->request->getParam('customer_id', 0);
        $totalDiscount = $this->request->getParam('total_discount', 0);
        $onsiteClientName = $this->request->getParam('onsite_client_name', '');
        $satisfaction = $this->request->getParam('satisfaction', 1);
        $jobStartTime = $this->request->getParam('job_start_time', $st);
        $jobFinishTime = $this->request->getParam('job_finish_time', $et);
        
		$services = $this->request->getParam('services', array());
		$services = json_decode($services,true);
        $product_ids = $this->request->getParam('product', array());
        $ltrs = $this->request->getParam('ltr', array());
        $propertyTypeId = $this->request->getParam('property_type', 0);
        $callOutFee = $this->request->getParam('call_out_fee', 0);
        $to_follow = $this->request->getParam('to_follow', '');
        $is_to_follow = $this->request->getParam('is_to_follow', 0);
        $multi_stpartdate = $this->request->getParam('multi_stpartdate');
        $confirmResetAttribut = $this->request->getParam('confirmResetAttribut', 0);
		*/
		
		$postArr = array( 
			'booking_id'=> $bookingId,
			'inquiry_id'=> $inquiryId, 
			'toEstimate'=>$toEstimate,
			'booking_start'=> $st ,
			'booking_end'=>$et,
			'isAllDayEvent'=>$isAllDayEvent,
			'description'=>$description_booking,
			'city_id'=>$cityId,
			'bookingStatus'=>$statusId,
			'customer_id'=>$customer_id,
			'total_discount'=>$totalDiscount,
			'onsite_client_name'=>$onsiteClientName,
			'satisfaction'=>$satisfaction,
			'job_start_time'=>$jobStartTime,
			'job_finish_time'=>$jobFinishTime,
			'services'=>$services,
			'product'=>$product_ids,
			'ltr'=>$ltrs,
			'property_type'=>$propertyTypeId,
			'call_out_fee'=>$callOutFee,
			'to_follow'=>$to_follow,
			'is_to_follow'=>$is_to_follow,
			'multi_stpartdate'=>$multi_stpartdate,
			'confirmResetAttribut'=>$confirmResetAttribut       
		);
		//print_r($postArr);
		//echo 'rrrrrrrrrrrrrr';
        /**
         * get Logged User && Company
         */
        $loggedUser = CheckAuth::getLoggedUser();
        $companyId = CheckAuth::getCompanySession();

        /**
         * load models
         */
        $modelBooking = new Model_Booking();
        $modelBookingStatus = new Model_BookingStatus();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelRefund = new Model_Refund();
        $modelBookingEstimate = new Model_BookingEstimate();
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelCompanies = new Model_Companies();
        $modelBookingStatusHistory = new Model_BookingStatusHistory();
		$modelBookingDiscussion = new Model_BookingDiscussion();
		$modelBookingMultipleDays = new Model_BookingMultipleDays();
		$modelVisitedExtraInfo = new Model_VisitedExtraInfo();
		$modelBookingProduct = new Model_BookingProduct();

        /**
         * Get Booking Status
         */
        $completed = $modelBookingStatus->getByStatusName('COMPLETED');
        $faild = $modelBookingStatus->getByStatusName('FAILED');
        $to_do = $modelBookingStatus->getByStatusName('TO DO');
        $quoted = $modelBookingStatus->getByStatusName('QUOTED');
        $toVisit = $modelBookingStatus->getByStatusName('TO VISIT');

        /**
         * get old Booking
         */
        $oldBooking = $modelBooking->getById($bookingId);

        /**
         * previous
         */
        $oldStatusId = isset($oldBooking['status_id']) ? $oldBooking['status_id'] : 0;
        $oldCallOutFee = isset($oldBooking['call_out_fee']) ? $oldBooking['call_out_fee'] : 0;
        $oldTotalDiscount = isset($oldBooking['total_discount']) ? $oldBooking['total_discount'] : 0;

        /**
         * validation
         */
        $errorMessages = array();
		$returnData = array();
        if ($this->validaterOffline($errorMessages,$postArr)){
			//echo "validate returns true ";
            /**
             * prepare data to be saved on database
             */
            $db_params = array();
            $db_params['status_id'] = $statusId;
            //$db_params['onsite_client_name'] = $onsiteClientName;
            //$db_params['job_start_time'] = strtotime($jobStartTime);
            //$db_params['job_finish_time'] = strtotime($jobFinishTime);
            //$db_params['satisfaction'] = $satisfaction;

            /**
             * contractor can't change booking Details
             * in update mode
             */
			/* if($modelBooking->checkCanEditBookingDetails($bookingId)){
				 echo 'can edit details returns true  ';
			 }
			 else{
				 echo 'can edit details returns NO  ';
			 }*/
			 
		if($mode == 'update'){
		     $visited_info_id = $oldBooking['visited_extra_info_id'];
		    
			
			$date_fields = array(
				'job_start'=> $jobStartTime ,
				'job_end'=> $jobFinishTime,
				'booking_id'=> $bookingId,
				'onsite_client_name'=> $onsiteClientName,
				'is_visited'=> $date_visited
			);
			
			
			
			//echo $visited_info_id;
			
			if($visited_info_id){
			   $modelVisitedExtraInfo->updateById($visited_info_id , $date_fields);
			}else{
			  $visited_info_id = $modelVisitedExtraInfo->insert($date_fields);
			}
		
			
			if(!empty($extra_comment)){
			   
			   if($visited_info_id){
				 $bookingDiscussion = $modelBookingDiscussion->getByExtraInfoId($visited_info_id);
				 
				 if($bookingDiscussion){
				   $modelBookingDiscussion->updateById($bookingDiscussion['discussion_id'] ,array('user_id'=>$loggedUser['user_id'] , 'user_message' => $extra_comment) );
				 }else{
				   $modelBookingDiscussion->insert(array('booking_id'=>$bookingId , 'user_id'=>$loggedUser['user_id'] , 'user_message' => $extra_comment , 'visited_extra_info_id'=>$visited_info_id ));
				 }
			   
				}else{
				  $modelBookingDiscussion->insert(array('booking_id'=>$bookingId , 'user_id'=>$loggedUser['user_id'] , 'user_message' => $extra_comment , 'visited_extra_info_id'=>$visited_info_id ));
				}
			
			}
					
		if($date_visited){
			$date_products  = array();
			$date_ltr  = array();
			 foreach($products as $key=>$product){
			    $date_products[$key] = $product['product_id'];
			    $date_ltr[$key] = $product['ltr'];
			  }
		  	   
			if ($date_products) {
				 if($visited_info_id){
					 $delete_old = 0;
                    $modelBookingProduct = new Model_BookingProduct();
                    $modelBookingProduct->setProductsToBooking($bookingId, $date_products, $date_ltr , $visited_info_id,$delete_old,$this->logId);
				 }
			}
		}
			
			$modelBooking->updateById($bookingId , array('visited_extra_info_id' => $visited_info_id),false);
			
			//var_dump($BookingDates);
		   foreach($BookingDates as $key=>$extra_info){
			  $bookingId = $extra_info['booking_id'];
			  $is_visited = isset($extra_info['is_visited']) ? $extra_info['is_visited'] : 0;
			  $is_new =  isset($extra_info['is_new']) ? $extra_info['is_new'] : 0;
			  $is_deleted =  isset($extra_info['is_deleted']) ? $extra_info['is_deleted'] : 0;
			  $multipleDay = $modelBookingMultipleDays->getById($extra_info['id']);
			  $visited_info_id = $multipleDay['visited_extra_info_id'];
			  
			  
			  if($is_visited && $is_deleted){		  
			   $modelVisitedExtraInfo->deleteById($multipleDay['visited_extra_info_id']);
			    if(!empty($extra_info['extra_comments'])){	           				  
					$modelBookingDiscussion->deleteByExtraInfoId($multipleDay['visited_extra_info_id']);
				}
				$products = $extra_info['products'];
				if($products){
				  $modelBookingProduct->deleteByExtraInfoId($multipleDay['visited_extra_info_id']);
				}
				
				 $modelBookingMultipleDays->deleteById($extra_info['id']);
			   	
			  }else if($is_deleted && !($is_visited)){						  
			    $modelBookingMultipleDays->deleteById($extra_info['id']);
			  }else if(!($is_deleted)){
			    $date_extra_comments = $extra_info['extra_comments'];
				
				$visisted_extra_info = array(
				  'job_start'=>$extra_info['job_start_time'],
				  'job_end'=>$extra_info['job_finish_time'],
				  'onsite_client_name'=>$extra_info['onsite_client_name'],
				  'booking_id'=>$extra_info['booking_id'],	  
				  'is_visited'=> $is_visited
				  );
			  
			  
			  if($visited_info_id){
					$modelVisitedExtraInfo->updateById($visited_info_id, $visisted_extra_info);
				}else{
					$visited_info_id = $modelVisitedExtraInfo->insert($visisted_extra_info);
				}
				
			
			  
			  
				
		    if($is_visited){ 
			  $products = $extra_info['products'];			
			  $date_products  = array();
			  $date_ltr  = array();
			  foreach($products as $k=>$product){
			    $date_products[$k] = $product['product_id'];
			    $date_ltr[$k] = $product['ltr'];
			  }
			   if ($date_products) {
					 if($visited_info_id){
						 $delete_old = 0;
						
						$modelBookingProduct = new Model_BookingProduct();
						$modelBookingProduct->setProductsToBooking($bookingId, $date_products, $date_ltr , $visited_info_id,$delete_old,$this->logId);
					 }
				}				
			 }
			 
				  $multiple_db_params = array();
				  //echo strtotime($extra_info['booking_start']);
				  $booking_start = date("Y-m-d H:i:s", strtotime($extra_info['booking_start']));
				  $booking_end = date("Y-m-d H:i:s", strtotime($extra_info['booking_end']));
				  $multiple_db_params['booking_start'] = $booking_start;
				  $multiple_db_params['booking_end'] = $booking_end;
				  $multiple_db_params['booking_id'] = $bookingId;
				  $multiple_db_params['visited_extra_info_id'] = $visited_info_id;
			     if($is_new){
				  $result = $modelBookingMultipleDays->insert($multiple_db_params);
                 }else{
				  $result = $modelBookingMultipleDays->updateById($extra_info['id'] , $multiple_db_params);
				 }
				 
				 
				 if(!empty($date_extra_comments)){
				 
				 
				 
				 if($visited_info_id){
						 $bookingDiscussion = $modelBookingDiscussion->getByExtraInfoId($visited_info_id);
						 if($bookingDiscussion){
						   $modelBookingDiscussion->updateById($bookingDiscussion['discussion_id'] ,array('user_id'=>$loggedUser['user_id'] , 'user_message' => $date_extra_comments) );
						 }else{
						   $modelBookingDiscussion->insert(array('booking_id'=>$bookingId , 'user_id'=>$loggedUser['user_id'] , 'user_message' => $date_extra_comments , 'visited_extra_info_id'=>$visited_info_id ));
						 }
					   
						}else{						 
						  $modelBookingDiscussion->insert(array('booking_id'=>$bookingId , 'user_id'=>$loggedUser['user_id'] , 'user_message' => $date_extra_comments , 'visited_extra_info_id'=>$visited_info_id ));
						}
				}


				 
			 
			 
			}

			}
			
			
			
		  
		}	
			 
			 
            if (($mode == 'create') || (($mode == 'update') && $modelBooking->checkCanEditBookingDetails($bookingId))) {
				//echo 'prepare parms....';
                $db_params['customer_id'] = $customer_id;
                $db_params['booking_start'] = php2MySqlTime(strtotime($st));
                $db_params['booking_end'] = php2MySqlTime(strtotime($et));
                $db_params['title'] = $title;
                $db_params['is_all_day_event'] = $isAllDayEvent ? 1 : 0;
                $db_params['description'] = $description_booking;
                $db_params['city_id'] = $cityId;
                $db_params['property_type_id'] = $propertyTypeId;
                $db_params['to_follow'] = $to_follow ? strtotime($to_follow) : 0;
                $db_params['is_to_follow'] = $is_to_follow ? 1 : 0;
            }

            /**
             * delete google Calendar event if status is check as delete google calender like on hold or canceled 
             */
            /*$deleteGoogleCalender = $modelBookingStatus->getDeleteGoogleCalender();
			//print_r($deleteGoogleCalender);
            if (in_array($statusId, $deleteGoogleCalender)) {
                $modelGoogleCalendarEvent = new Model_GoogleCalendarEvent();
                $modelGoogleCalendarEvent->deleteGoogleCalendarEventByBookingId($bookingId);
            }*/
			//echo 'test ';
            /**
             * if booking changed from TO Visit to TO DO update created
             */
            if (($mode == 'update') && $statusId == $to_do['booking_status_id'] && $oldStatusId) {
                if ($oldStatusId == $toVisit['booking_status_id']) {
                    $db_params['created'] = time();
                }
            }

            /**
             * is_multiple_days
             */
            if ($multi_stpartdate) {
                $db_params['is_multiple_days'] = 1;
            } else {
                $db_params['is_multiple_days'] = 0;
            }

            /**
             * save item in the database
             */
			 //echo 'update item....';
            if ($mode == 'create') {
				//echo 'save item....';
                $db_params['created_by'] = $loggedUser['user_id'];
                $db_params['created'] = time();
                $db_params['company_id'] = $companyId;
                $db_params['original_inquiry_id'] = $inquiryId;

                $returnData = $modelBooking->addDetailedCalendar($db_params);
				$this->logId = $returnData['log'];
            } else {
				//echo 'update item....';
                $returnData = $modelBooking->updateDetailedCalendar($bookingId, $db_params);
				$this->logId = $returnData['log'];
				//echo 'update item....eeee';
            }
			//echo 'test222 ';
			//print_r($returnData);
            if (isset($returnData['Data']) && $returnData['Data']) {
				//print_r($returnData['Data']);
                $bookingId = (int) $returnData['Data'];

                /**
                 * save  booking status history
                 */
                if ($mode == 'create') {
                    $modelBookingStatusHistory = new Model_BookingStatusHistory();
                    $modelBookingStatusHistory->addStatusHistory($bookingId, $statusId);
                }
				//echo 'booking history done';

                /**
                 * insert booking services and his quote and quantity
                 */
                if (($mode == 'create') || (($mode == 'update') && $modelBooking->checkCanEditBookingDetails($bookingId))) {
                    $modelContractorServiceBooking->setServicesToBookingOffline($bookingId, $services , $this->logId );
                }
				//echo 'booking services done';
                /**
                 * save multiple days
                 */
                $modelBookingMultipleDays = new Model_BookingMultipleDays();
                $modelBookingMultipleDays->saveMultipleDays($bookingId);

                /**
                 * save address
                 */
                $this->saveAddress($booking,$bookingId, $mode);
                   //echo 'booking address done';
                /**
                 * set product to booking
                 */
                if ($product_ids) {
                    $modelBookingProduct = new Model_BookingProduct();
                    $modelBookingProduct->setProductsToBooking($bookingId, $product_ids, $ltrs , 0 , $this->logId );
                }
					//echo 'booking products done';
                /**
                 * save why booking status
                 */
                $this->saveWhyStatus($booking,$bookingId, $statusId, false);

                /**
                 * convert To Estimate if status is qouted
                 */
				// echo 'booking estimates done';
                if ($statusId == $quoted['booking_status_id']) {
                    $modelBookingEstimate->convertToEstimate($bookingId, false, false);
                }
				//echo 'booking lll estimated done';
                /**
                 * delete Estimate if status is not quoted
                 */
                if ($mode == 'update' && $statusId != $quoted['booking_status_id']) {
                    $modelBookingEstimate->changedEstimateToBooking($bookingId, $statusId, false);
                }
				//echo 'booking Estimate if status is not quoted done';
                /**
                 * convert To Invoice if booking status is complete or faild
                 */
                if ($statusId == $completed['booking_status_id'] || $statusId == $faild['booking_status_id']) {
                    $modelBookingInvoice->convertToInvoice($bookingId, false);
                }
				//echo 'booking convert To Invoice if booking status is complete or faild done';
                /**
                 * set Quantity and Discount attribute value to Zero when booking status is faild
                 */
                if ($statusId == $faild['booking_status_id']) {
                    if ($confirmResetAttribut) {
                        $modelContractorServiceBooking->changeAttributIfFaildOffline($bookingId, $services , $this->logId );
                    }
                } else {
                    $callOutFee = 0;
                }


                /**
                 *  insert services in  the temp until approved when the booking is update by contractor
                 */
                $totalDiscountTemp = 0;
                $callOutFeeTemp = 0;
                $db_params = array();
                if ((($mode == 'update') && !$modelBooking->checkCanEditBookingDetails($bookingId))) {

                    $modelContractorServiceBookingTemp = new Model_ContractorServiceBookingTemp();
                    $modelContractorServiceBookingTemp->setServicesToBookingOffline($bookingId, $services , $this->logId );

                    /**
                     * if the contractor change call out fee value ,it will saved in the temp ,
                     * the original call out fee get from it's company  and saved in the booking
                     */
                    $callOutFeeTemp = $callOutFee;
                    $companies = $modelCompanies->getById(CheckAuth::getCompanySession());
                    if ($callOutFee) {
                        if ($oldCallOutFee) {
                            $callOutFee = $oldCallOutFee;
                        } else {
                            $callOutFee = $companies['call_out_fee'];
                        }
                        if ($callOutFeeTemp != $callOutFee) {
                            $db_params['is_change'] = 1;
                        }
                    }

                    // get the total discount from original booking because the gst and the total calculate from it.
                    $totalDiscountTemp = $totalDiscount;

                    if (isset($totalDiscount) && $oldTotalDiscount != $totalDiscount) {
                        $totalDiscount = $oldTotalDiscount;
                        $db_params['is_change'] = 1;
                    }

                    if ($statusId != $oldStatusId) {
                        $db_params['is_change'] = 1;
                    }
                }


                /**
                 * save calculation qoute
                 */
                $totalRefund = $modelRefund->getTotalRefund(array('booking_id' => $bookingId, 'is_approved' => 'yes'));
                $subTotal = $modelContractorServiceBooking->getTotalBookingQoute($bookingId);
                /*$total = ($subTotal + $callOutFee) - $totalDiscount;
                $gstTax = $total * get_config('gst_tax');
                $totalQoute = $total + $gstTax;
                $totalQoute = $totalQoute - $totalRefund;*/
				
				
				$total = ($subTotal + $callOutFee);
                $gstTax = $total * get_config('gst_tax');
				$totalQoute = $total + $gstTax - $totalDiscount;      
                $totalQoute = $totalQoute - $totalRefund;



                $db_params['sub_total'] = round($subTotal, 2);
                $db_params['gst'] = round($gstTax, 2);
                $db_params['qoute'] = round($totalQoute, 2);
                $db_params['call_out_fee'] = round($callOutFee, 2);
                $db_params['refund'] = round($totalRefund, 2);
                $db_params['total_discount'] = round($totalDiscount, 2);
                $db_params['total_discount_temp'] = round($totalDiscountTemp, 2);
                $db_params['call_out_fee_temp'] = round($callOutFeeTemp, 2);


                /**
                 * generate Title
                 */
                //$title = $this->generateTitle($services, $cityId, number_format($totalQoute, 2));
                $db_params['title'] = 'title disabled....'; 


                /**
                 * update saved Booking
                 */
                $isSuccess = $modelBooking->updateById($bookingId, $db_params, false);

				
                /**
                 * send Booking To Gmail Acc if status is completed or faild or to_do or in_process
                 */
                /*$pushGoogleCalender = $modelBookingStatus->getPushGoogleCalender();
                if (in_array($statusId, $pushGoogleCalender)) {
                    $modelGoogleCalendarEvent = new Model_GoogleCalendarEvent();
                    $modelGoogleCalendarEvent->sendCreatedBooking($bookingId);
                }*/
            }

            if ($mode == 'create' && $inquiryId) {
                $modelInquiry = new Model_Inquiry();

                $data = array(
                    'status' => 'booking',
                    'deferred_date' => 0
                );
                $modelInquiry->updateById($inquiryId, $data);

                if ($toEstimate) {
                    $data = array(
                        'status' => 'estimate',
                        'deferred_date' => 0
                    );
                    $modelInquiry->updateById($inquiryId, $data);
                }
            }
			//if($isSuccess){
				////return the booking in the response
				$booking = $this->getBooking(false, array('booking_id' => $bookingId));	
				$returnData['IsSuccess'] = true;
				$returnData['Msg'] = $booking;
			//}
			
			
        } else {
			//echo "validate returns true ";
            
            $returnData['IsSuccess'] = false;
            $returnData['Msg'] = $errorMessages;
        }
	//echo "validate returns true22 ";
	//print_r($returnData);
        return $returnData;
    }
	///////////////////////////
    public function saveBooking() {

        $bookingId = $this->request->getParam('booking_id', 0);

        $returnData = $this->save('update');
		
        $success = TRUE;
        if (empty($returnData['IsSuccess'])) {
            $success = FALSE;
        }

        if ($success) {
            return array('type' => 'success', 'message' => "Saved successfully", 'result' => $this->getBooking(false, array('booking_id' => $bookingId)));
        } else {
            $message = "Error in Saving Booking";
            if (isset($returnData['Msg'])) {
                if (is_array($returnData['Msg'])) {
                    foreach ($returnData['Msg'] as $msg) {
                        $message .= " ({$msg})";
                    }
                }
            }
            return array('type' => 'error', 'message' => $message);
        }
    }

    public function save($mode, $booking= array(), $os = 'ios') {

	    //print_r($booking);
        /**
         * get request Params
         */
		 // $postData = $this->request->getPost();
		//print_r($postData);
		if(empty($booking)){
			$title = '';
			$bookingId = $this->request->getParam('booking_id', 0);
			$inquiryId = $this->request->getParam('inquiry_id', 0);
			$toEstimate = $this->request->getParam('toEstimate', 0);
			$st = $this->request->getParam('booking_start', "0000-00-00 00:00:00");
			$et = $this->request->getParam('booking_end', "0000-00-00 00:00:00");
			$isAllDayEvent = $this->request->getParam('isAllDayEvent', 0);
			$description_booking = $this->request->getParam('description', '');
			$cityId = $this->request->getParam('city_id', 0);
			$statusId = $this->request->getParam('bookingStatus', 0);
			$customer_id = $this->request->getParam('customer_id', 0);
			$totalDiscount = $this->request->getParam('total_discount', 0);
			$onsiteClientName = $this->request->getParam('onsite_client_name', '');
			$satisfaction = $this->request->getParam('satisfaction', 1);
			$jobStartTime = $this->request->getParam('job_start_time', $st);
			$jobFinishTime = $this->request->getParam('job_finish_time', $et);
			$services = $this->request->getParam('services', array());
			$product_ids = $this->request->getParam('product', array());
			$ltrs = $this->request->getParam('ltr', array());
			$propertyTypeId = $this->request->getParam('property_type', 0);
			$callOutFee = $this->request->getParam('call_out_fee', 0);
			$to_follow = $this->request->getParam('to_follow', '');
			$is_to_follow = $this->request->getParam('is_to_follow', 0);
			$multi_stpartdate = $this->request->getParam('multi_stpartdate');
			$confirmResetAttribut = $this->request->getParam('confirmResetAttribut', 0);
		}
		else{
			$title = '';
        $bookingId = $this->request->getParam('booking_id', 0);
        $inquiryId =  isset($booking['inquiry_id']) ? $booking['inquiry_id'] : 0; 
        $toEstimate =  isset($booking['toEstimate']) ? $booking['toEstimate'] : 0; 
        $st =  isset($booking['booking_start']) ? $booking['booking_start'] : "0000-00-00 00:00:00"; 
        $et =  isset($booking['booking_end']) ? $booking['booking_end'] : "0000-00-00 00:00:00"; 
        $isAllDayEvent =  isset($booking['isAllDayEvent']) ? $booking['isAllDayEvent'] : 0; 
        $description_booking =  isset($booking['description']) ? $booking['description'] : ''; 
        $cityId =  isset($booking['city_id']) ? $booking['city_id'] : 0; 
        $statusId =  isset($booking['status']) ? $booking['status'] : 0; 
        $customer_id =  isset($booking['customer_id']) ? $booking['customer_id'] : 0; 
        $totalDiscount =  isset($booking['total_discount']) ? $booking['total_discount'] : 0; 
        $onsiteClientName =  isset($booking['onsite_client_name']) ? $booking['onsite_client_name'] : ''; 
        $satisfaction =  isset($booking['satisfaction']) ? $booking['satisfaction'] : 1; 
        $jobStartTime =  isset($booking['job_start_time']) ? $booking['job_start_time'] : $st; 
        $jobFinishTime =  isset($booking['job_finish_time']) ? $booking['job_finish_time'] : $et; 
        $services =  isset($booking['services']) ? (array)$booking['services'] : array();
        $services = (array) $services;
        $product_ids =  isset($booking['product']) ? $booking['product'] : array();
        $ltrs =  isset($booking['ltr']) ? $booking['ltr'] : array();
        $propertyTypeId =  isset($booking['property_type']) ? $booking['property_type'] : 0;
        $callOutFee =  isset($booking['call_out_fee']) ? $booking['call_out_fee'] : 0;
        $to_follow =  isset($booking['to_follow']) ? $booking['to_follow'] : '';
        $is_to_follow =  isset($booking['is_to_follow']) ? $booking['is_to_follow'] : 0;
        $multi_stpartdate =  isset($booking['multi_stpartdate']) ? $booking['multi_stpartdate'] : 0;
        $confirmResetAttribut =  isset($booking['confirmResetAttribut']) ? $booking['confirmResetAttribut'] : 0;
        $BookingDates =  isset($booking['BookingDates']) ? $booking['BookingDates'] : 0;
        $date_visited =  isset($booking['is_visited']) ? $booking['is_visited'] : 0;
        $extra_comment =  isset($booking['extra_comment']) ? $booking['extra_comment'] : 0;
        $products =  isset($booking['products']) ? $booking['products'] : 0;
		
		
	
			//echo '$statusId   '.$statusId;
			//exit;
		}
		
		//echo 'islam test ......';
		//print_r($booking);
        /**
         * get Logged User && Company
         */
        $loggedUser = CheckAuth::getLoggedUser();
        $companyId = CheckAuth::getCompanySession();

        /**
         * load models
         */
        $modelBooking = new Model_Booking();
        $modelBookingStatus = new Model_BookingStatus();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelRefund = new Model_Refund();
        $modelBookingEstimate = new Model_BookingEstimate();
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelCompanies = new Model_Companies();
        $modelBookingStatusHistory = new Model_BookingStatusHistory();
		$modelBookingDiscussion = new Model_BookingDiscussion();
		$modelBookingMultipleDays = new Model_BookingMultipleDays();
		$modelVisitedExtraInfo = new Model_VisitedExtraInfo();
		$modelBookingProduct = new Model_BookingProduct();

        /**
         * Get Booking Status
         */
        $completed = $modelBookingStatus->getByStatusName('COMPLETED');
        $faild = $modelBookingStatus->getByStatusName('FAILED');
        $to_do = $modelBookingStatus->getByStatusName('TO DO');
        $quoted = $modelBookingStatus->getByStatusName('QUOTED');
        $toVisit = $modelBookingStatus->getByStatusName('TO VISIT');

		
		
        /**
         * get old Booking
         */
        $oldBooking = $modelBooking->getById($bookingId);

		
		
        /**
         * previous
         */
        $oldStatusId = isset($oldBooking['status_id']) ? $oldBooking['status_id'] : 0;
        $oldCallOutFee = isset($oldBooking['call_out_fee']) ? $oldBooking['call_out_fee'] : 0;
        $oldTotalDiscount = isset($oldBooking['total_discount']) ? $oldBooking['total_discount'] : 0;

        /**
         * validation
         */
        $errorMessages = array();
		$returnData = array();
		    //echo 'before vaildator';
			
			
			if($mode == 'update' && $os == 'android'){
		
		      $visited_info_id = $oldBooking['visited_extra_info_id'];
		    
			   
			$date_fields = array(
				'job_start'=> $jobStartTime ,
				'job_end'=> $jobFinishTime,
				'booking_id'=> $bookingId,
				'onsite_client_name'=> $onsiteClientName,
				'is_visited'=> $date_visited
			);
			
			
			if($visited_info_id){
			   $modelVisitedExtraInfo->updateById($visited_info_id , $date_fields);
			}else{
			  $visited_info_id = $modelVisitedExtraInfo->insert($date_fields);
			}
		
			
			if(!empty($extra_comment)){
			   
			   if($visited_info_id){
				 $bookingDiscussion = $modelBookingDiscussion->getByExtraInfoId($visited_info_id);
				 
				 if($bookingDiscussion){
				   $modelBookingDiscussion->updateById($bookingDiscussion['discussion_id'] ,array('user_id'=>$loggedUser['user_id'] , 'user_message' => $extra_comment) );
				 }else{
				   $modelBookingDiscussion->insert(array('booking_id'=>$bookingId , 'user_id'=>$loggedUser['user_id'] , 'user_message' => $extra_comment , 'visited_extra_info_id'=>$visited_info_id ));
				 }
			   
				}else{
				  $modelBookingDiscussion->insert(array('booking_id'=>$bookingId , 'user_id'=>$loggedUser['user_id'] , 'user_message' => $extra_comment , 'visited_extra_info_id'=>$visited_info_id ));
				}
			
			}
					
		if($date_visited){
			$date_products  = array();
			$date_ltr  = array();
			 foreach($products as $key=>$product){
			    $date_products[$key] = $product['product_id'];
			    $date_ltr[$key] = $product['ltr'];
			  }
		  	   
			if ($date_products) {
				 if($visited_info_id){
					 $delete_old = 0;
                    $modelBookingProduct = new Model_BookingProduct();
                    $modelBookingProduct->setProductsToBooking($bookingId, $date_products, $date_ltr , $visited_info_id,$delete_old,$this->logId);
				 }
			}
		}
			
			$modelBooking->updateById($bookingId , array('visited_extra_info_id' => $visited_info_id),false);
			

		   foreach($BookingDates as $key=>$extra_info){
			  $bookingId = $extra_info['booking_id'];
			  $is_visited = isset($extra_info['is_visited']) ? $extra_info['is_visited'] : 0;
			  $is_new =  isset($extra_info['is_new']) ? $extra_info['is_new'] : 0;
			  $is_deleted =  isset($extra_info['is_deleted']) ? $extra_info['is_deleted'] : 0;
			  $multipleDay = $modelBookingMultipleDays->getById($extra_info['id']);
			  $visited_info_id = $multipleDay['visited_extra_info_id'];
			  if($is_visited && $is_deleted){		  
			   $modelVisitedExtraInfo->deleteById($multipleDay['visited_extra_info_id']);
			    if(!empty($extra_info['extra_comments'])){	           				  
					$modelBookingDiscussion->deleteByExtraInfoId($multipleDay['visited_extra_info_id']);
				}
				$products = $extra_info['products'];
				if($products){
				  $modelBookingProduct->deleteByExtraInfoId($multipleDay['visited_extra_info_id']);
				}
				
				 $modelBookingMultipleDays->deleteById($extra_info['id']);
			   	
			  }else if($is_deleted && !($is_visited)){						  
			    $modelBookingMultipleDays->deleteById($extra_info['id']);
			  }else if(!($is_deleted)){
			    $date_extra_comments = $extra_info['extra_comments'];
				
				$visisted_extra_info = array(
				  'job_start'=>$extra_info['job_start_time'],
				  'job_end'=>$extra_info['job_finish_time'],
				  'onsite_client_name'=>$extra_info['onsite_client_name'],
				  'booking_id'=>$extra_info['booking_id'],	  
				  'is_visited'=> $is_visited
				  );
			  
			  
			  if($visited_info_id){
					$modelVisitedExtraInfo->updateById($visited_info_id, $visisted_extra_info);
				}else{
					$visited_info_id = $modelVisitedExtraInfo->insert($visisted_extra_info);
				}
				
			
			  
			  
				
		    if($is_visited){ 
			  $products = $extra_info['products'];			
			  $date_products  = array();
			  $date_ltr  = array();
			  foreach($products as $k=>$product){
			    $date_products[$k] = $product['product_id'];
			    $date_ltr[$k] = $product['ltr'];
			  }
			   if ($date_products) {
					 if($visited_info_id){
						 $delete_old = 0;
						
						$modelBookingProduct = new Model_BookingProduct();
						$modelBookingProduct->setProductsToBooking($bookingId, $date_products, $date_ltr , $visited_info_id,$delete_old,$this->logId);
					 }
				}				
			 }
			 
				  $db_param = array();
				  //echo strtotime($extra_info['booking_start']);
				  $booking_start = date("Y-m-d H:i:s", strtotime($extra_info['booking_start']));
				  $booking_end = date("Y-m-d H:i:s", strtotime($extra_info['booking_end']));
				  $db_param['booking_start'] = $booking_start;
				  $db_param['booking_end'] = $booking_end;
				  $db_param['booking_id'] = $bookingId;
				  $db_param['visited_extra_info_id'] = $visited_info_id;
			     if($is_new){
				  $result = $modelBookingMultipleDays->insert($db_param);
                 }else{
				  $result = $modelBookingMultipleDays->updateById($extra_info['id'] , $db_param);
				 }
				 
				 
				 if(!empty($date_extra_comments)){
                    		  
				 if($visited_info_id){
				         
						 $bookingDiscussion = $modelBookingDiscussion->getByExtraInfoId($visited_info_id);
						 if($bookingDiscussion){
						    
						   $modelBookingDiscussion->updateById($bookingDiscussion['discussion_id'] ,array('user_id'=>$loggedUser['user_id'] , 'user_message' => $date_extra_comments) );
						 }else{
						  
						   $modelBookingDiscussion->insert(array('booking_id'=>$bookingId , 'user_id'=>$loggedUser['user_id'] , 'user_message' => $date_extra_comments , 'visited_extra_info_id'=>$visited_info_id ));
						 }
					   
						}else{				
                        					
						  $modelBookingDiscussion->insert(array('booking_id'=>$bookingId , 'user_id'=>$loggedUser['user_id'] , 'user_message' => $date_extra_comments , 'visited_extra_info_id'=>$visited_info_id ));
						}
				}


				 
			 
			 
			}

			}
			
			
		  
		    }	
			
        
		if ($this->validater($errorMessages,$booking)) {
			
			//echo "validate returns true ";
            /**
             * prepare data to be saved on database
             */
            $db_params = array();
            $db_params['status_id'] = $statusId;
            $db_params['onsite_client_name'] = $onsiteClientName;
            $db_params['job_start_time'] = strtotime($jobStartTime);
            $db_params['job_finish_time'] = strtotime($jobFinishTime);
            $db_params['satisfaction'] = $satisfaction;

            /**
             * contractor can't change booking Details
             * in update mode
             */
			/* if($modelBooking->checkCanEditBookingDetails($bookingId)){
				 echo 'can edit details returns true  ';
			 }
			 else{
				 echo 'can edit details returns NO  ';
			 }*/
			 
			 
			 //echo $modelBooking->checkCanEditBookingDetails($bookingId);
			 
            if (($mode == 'create') || (($mode == 'update') && $modelBooking->checkCanEditBookingDetails($bookingId))) {
				//echo 'inside mode';
                
				$db_params['customer_id'] = $customer_id;
                $db_params['booking_start'] = php2MySqlTime(strtotime($st));
                $db_params['booking_end'] = php2MySqlTime(strtotime($et));
                $db_params['title'] = $title;
                $db_params['is_all_day_event'] = $isAllDayEvent ? 1 : 0;
                $db_params['description'] = $description_booking;
                $db_params['city_id'] = $cityId;
                $db_params['property_type_id'] = $propertyTypeId;
                $db_params['to_follow'] = $to_follow ? strtotime($to_follow) : 0;
                $db_params['is_to_follow'] = $is_to_follow ? 1 : 0;
            }

            /**
             * delete google Calendar event if status is check as delete google calender like on hold or canceled 
             */
            $deleteGoogleCalender = $modelBookingStatus->getDeleteGoogleCalender();
			//print_r($deleteGoogleCalender);
            if (in_array($statusId, $deleteGoogleCalender)) {
                $modelGoogleCalendarEvent = new Model_GoogleCalendarEvent();
                $modelGoogleCalendarEvent->deleteGoogleCalendarEventByBookingId($bookingId);
            }
			//echo 'test ';
            /**
             * if booking changed from TO Visit to TO DO update created
             */
            if (($mode == 'update') && $statusId == $to_do['booking_status_id'] && $oldStatusId) {
                if ($oldStatusId == $toVisit['booking_status_id']) {
                    $db_params['created'] = time();
                }
            }

            /**
             * is_multiple_days
             */
            if ($multi_stpartdate) {
                $db_params['is_multiple_days'] = 1;
            } else {
                $db_params['is_multiple_days'] = 0;
            }

            /**
             * save item in the database
             */
            if ($mode == 'create') {
                $db_params['created_by'] = $loggedUser['user_id'];
                $db_params['created'] = time();
                $db_params['company_id'] = $companyId;
                $db_params['original_inquiry_id'] = $inquiryId;

                $returnData = $modelBooking->addDetailedCalendar($db_params);
				$this->logId = $returnData['log'];
            } else {
			    //echo 'update booking';
                $returnData = $modelBooking->updateDetailedCalendar($bookingId, $db_params);
				$this->logId = $returnData['log'];
            }
			//echo 'test222 ';
			//print_r($returnData);
            if (isset($returnData['Data']) && $returnData['Data']) {
			
			//echo 'if return data';

                $bookingId = (int) $returnData['Data'];

                /**
                 * save  booking status history
                 */
                if ($mode == 'create') {
                    $modelBookingStatusHistory = new Model_BookingStatusHistory();
                    $modelBookingStatusHistory->addStatusHistory($bookingId, $statusId);
                }
				//echo 'booking history done';

                /**
                 * insert booking services and his quote and quantity
                 */
				 //print_r( $modelBooking->checkCanEditBookingDetails($bookingId));
				 
                if (($mode == 'create') || (($mode == 'update') && $modelBooking->checkCanEditBookingDetails($bookingId))) {
				    //echo 'set services';
					///////By Islam for android
					//if($os =='android'){
						$modelContractorServiceBooking->setServicesToBookingOffline($bookingId, $services,$this->logId);
					/*}
                    else{
						$modelContractorServiceBooking->setServicesToBooking($bookingId, $services);
					}
                    */
                }
				//echo 'booking services done';
                /**
                 * save multiple days
                 */
				 
                $modelBookingMultipleDays = new Model_BookingMultipleDays();
                $modelBookingMultipleDays->saveMultipleDays($bookingId,'android');

				
                /**
                 * save address
                 */
                $this->saveAddress($booking,$bookingId, $mode);
                   //echo 'booking address done';
                /**
                 * set product to booking
                 */
                if ($product_ids & $os =='ios') {
                    $modelBookingProduct = new Model_BookingProduct();
                    $modelBookingProduct->setProductsToBooking($bookingId, $product_ids, $ltrs,0,$this->logId);
                }
					//echo 'booking products done';
                /**
                 * save why booking status
                 */
                $this->saveWhyStatus($booking,$bookingId, $statusId, false);

                /**
                 * convert To Estimate if status is qouted
                 */
				// echo 'booking estimates done';
                if ($statusId == $quoted['booking_status_id']) {
                    $modelBookingEstimate->convertToEstimate($bookingId, false, false);
                }
				//echo 'booking lll estimated done';
                /**
                 * delete Estimate if status is not quoted
                 */
                if ($mode == 'update' && $statusId != $quoted['booking_status_id']) {
                    $modelBookingEstimate->changedEstimateToBooking($bookingId, $statusId, false);
                }
				//echo 'booking Estimate if status is not quoted done';
                /**
                 * convert To Invoice if booking status is complete or faild
                 */
                if ($statusId == $completed['booking_status_id'] || $statusId == $faild['booking_status_id']) {
                    $modelBookingInvoice->convertToInvoice($bookingId, false);
                }
				//echo 'booking convert To Invoice if booking status is complete or faild done';
                /**
                 * set Quantity and Discount attribute value to Zero when booking status is faild
                 */
                if ($statusId == $faild['booking_status_id']) {
                    if ($confirmResetAttribut) {
						///////By Islam for android
						if($os = 'android'){
							$modelContractorServiceBooking->changeAttributIfFaildOffline($bookingId, $services,$this->logId);
						}else{
							$modelContractorServiceBooking->changeAttributIfFaild($bookingId, $services,$this->logId);
						}
                        
                    }
                } else {
                    $callOutFee = 0;
                }


                /**
                 *  insert services in  the temp until approved when the booking is update by contractor
                 */
                $totalDiscountTemp = 0;
                $callOutFeeTemp = 0;
                $db_params = array();
                if ((($mode == 'update') && !$modelBooking->checkCanEditBookingDetails($bookingId))) {
				    $loggedUser = CheckAuth::getLoggedUser();
					$modelAuthRole = new Model_AuthRole();
                    $contractorRoleId = $modelAuthRole->getRoleIdByName('contractor');
					
				    if($loggedUser['role_id'] == $contractorRoleId){
					  $db_params['property_type_id'] = $propertyTypeId;
					}

                    $modelContractorServiceBookingTemp = new Model_ContractorServiceBookingTemp();
					///////By Islam for android
					//echo 'edit per';
					//if($os = 'android'){
						$modelContractorServiceBookingTemp->setServicesToBookingOffline($bookingId, $services,$this->logId);
					/*}
					else{
                    $modelContractorServiceBookingTemp->setServicesToBooking($bookingId, $services);
					}*/
                    /**
                     * if the contractor change call out fee value ,it will saved in the temp ,
                     * the original call out fee get from it's company  and saved in the booking
                     */
                    $callOutFeeTemp = $callOutFee;
                    $companies = $modelCompanies->getById(CheckAuth::getCompanySession());
                    if ($callOutFee) {
                        if ($oldCallOutFee) {
                            $callOutFee = $oldCallOutFee;
                        } else {
                            $callOutFee = $companies['call_out_fee'];
                        }
                        if ($callOutFeeTemp != $callOutFee) {
                            $db_params['is_change'] = 1;
                        }
                    }

                    // get the total discount from original booking because the gst and the total calculate from it.
                    $totalDiscountTemp = $totalDiscount;

                    if (isset($totalDiscount) && $oldTotalDiscount != $totalDiscount) {
                        $totalDiscount = $oldTotalDiscount;
                        $db_params['is_change'] = 1;
                    }

                    if ($statusId != $oldStatusId) {
                        $db_params['is_change'] = 1;
                    }
                }


                /**
                 * save calculation qoute
                 */
                $totalRefund = $modelRefund->getTotalRefund(array('booking_id' => $bookingId, 'is_approved' => 'yes'));
                $subTotal = $modelContractorServiceBooking->getTotalBookingQoute($bookingId);
                
				/*$total = ($subTotal + $callOutFee) - $totalDiscount;
                $gstTax = $total * get_config('gst_tax');
                $totalQoute = $total + $gstTax;
                $totalQoute = $totalQoute - $totalRefund;*/
				
				
				$total = ($subTotal + $callOutFee);
                $gstTax = $total * get_config('gst_tax');
				$totalQoute = $total + $gstTax - $totalDiscount;      
                $totalQoute = $totalQoute - $totalRefund;



                $db_params['sub_total'] = round($subTotal, 2);
                $db_params['gst'] = round($gstTax, 2);
                $db_params['qoute'] = round($totalQoute, 2);
                $db_params['call_out_fee'] = round($callOutFee, 2);
                $db_params['refund'] = round($totalRefund, 2);
                $db_params['total_discount'] = round($totalDiscount, 2);
                $db_params['total_discount_temp'] = round($totalDiscountTemp, 2);
                $db_params['call_out_fee_temp'] = round($callOutFeeTemp, 2);


                /**
                 * generate Title
                 */
                //$title = $this->generateTitle($services, $cityId, number_format($totalQoute, 2));
                $db_params['title'] = 'tilte...';


                /**
                 * update saved Booking
                 */
                $modelBooking->updateById($bookingId, $db_params, false);

                /**
                 * send Booking To Gmail Acc if status is completed or faild or to_do or in_process
                 */
                /*$pushGoogleCalender = $modelBookingStatus->getPushGoogleCalender();
                if (in_array($statusId, $pushGoogleCalender)) {
                    $modelGoogleCalendarEvent = new Model_GoogleCalendarEvent();
                    $modelGoogleCalendarEvent->sendCreatedBooking($bookingId);
                }*/
            }

            if ($mode == 'create' && $inquiryId) {
                $modelInquiry = new Model_Inquiry();

                $data = array(
                    'status' => 'booking',
                    'deferred_date' => 0
                );
                $modelInquiry->updateById($inquiryId, $data);

                if ($toEstimate) {
                    $data = array(
                        'status' => 'estimate',
                        'deferred_date' => 0
                    );
                    $modelInquiry->updateById($inquiryId, $data);
                }
            }
        } else {
			//echo "validate returns true ";
            
            $returnData['IsSuccess'] = false;
            $returnData['Msg'] = $errorMessages;
        }
	  //echo "validate returns true22 ";
	    //print_r($returnData);
		//exit;
        return $returnData;
    }

    /**
     * validater
     * 
     * @param type $errorMessages
     * @return type boolean
     */
    /*public function validater(&$errorMessages) {

        $postData = $this->request->getPost();

        $bookingId = $this->request->getParam('booking_id');
        $statusId = $this->request->getParam('bookingStatus', 0);

        $modelBooking = new Model_Booking();
        $message = '';
        $isCanChangeBookingStatus = $modelBooking->checkIfCanChangeBookingStatus($bookingId, $statusId, $message);

        if (!$isCanChangeBookingStatus) {
            $errorMessages['bookingStatus'] = $message;
        }

        foreach ($postData as $key => $value) {

            /**
             * is empty validater
             /
			 /// we changed the name of one parameter from total_qoute to qoute
			 
			  $required_filed = array('country', 'city_id', 'bookingStatus', 'sub_total', 'qoute', 'stpartdate', 'etpartdate', 'customer_id', 'to_follow', 'why', 'street_number', 'street_address', 'suburb', 'postcode', 'state');
            if (in_array($key, $required_filed)) {
                if (empty($value)) {
                    $errorMessages[$key] = 'Field required';
                }
            }

            /**
             * time format
             /
            $is_all_day_event = isset($postData["is_all_day_event"]) ? 1 : 0;
            if (!$is_all_day_event) {
                $time_format = array('stparttime', 'etparttime');
                if (in_array($key, $time_format)) {
                    if (empty($value)) {
                        $errorMessages[$key] = 'stparttime and etparttime are required Fields ';
                    }
                }
            }
        }



        $services = isset($postData['services']) ? $postData['services'] : array();
		$services = $this->request->getParam('services',array());
        if (count($services) === 0) {
            $errorMessages['services'] = 'Services cannot be empty';
        } else {
            foreach ($services as $service) {
                //$contractor = isset($postData['contractor_' . $service]) ? $postData['contractor_' . $service] : '';
                $contractor = $this->request->getParam('contractor_' . $service,'');
                //$contractor = isset($this->request->getParam('contractor_' . $service)) ? $this->request->getParam('contractor_' . $service) : '';
                if (empty($contractor)) {
                    $errorMessages['contractor_' . $service] = 'cccc Field required';
                } else {
                    $modelContractorService = new Model_ContractorService();
                    $contractorService = $modelContractorService->getByContractorIdAndServiceId($contractor, $service);
                    if (empty($contractorService)) {
                        $errorMessages['contractor_' . $service] = "This Technician dosn't Provide this Service";
                    } else {
                        //$city_id = isset($postData['city_id']) ? $postData['city_id'] : 0;
                        $city_id = $this->request->getParam('city_id',0);
                

                        $modelContractorServiceAvailability = new Model_ContractorServiceAvailability();
                        $contractorServiceAvailability = $modelContractorServiceAvailability->getByCityIdAndContractorServiceId($city_id, $contractorService['contractor_service_id']);
                        if (empty($contractorServiceAvailability)) {
                            $errorMessages['contractor_' . $service] = "This Technician dosn't Provide this Service in this City";
                        }
                    }
                }
            }
        }

        //$customerId = isset($postData['customer_id']) ? $postData['customer_id'] : 0;
        $customerId = $this->request->getParam('customer_id',0);

        /*if ('contractor' == CheckAuth::getRoleName() && $customerId) {
            $loggedUser = CheckAuth::getLoggedUser();
            $modelCustomer = new Model_Customer();
            if (!$modelCustomer->getByCustomerIdAndCreatedBy($customerId, $loggedUser['user_id'])) {
                $errorMessages['customer_id'] = 'You dont have permission to select this customer';
            }
        }

        if (count($errorMessages) !== 0) {
            return false;
        }
        return true;
    }*/

	//////////////validater from live server
	public function validater(&$errorMessages,&$booking) {
		//echo 'teeeesttt ';
		/*$os = $this->request->getParam('os','ios');
		
		
        $postData = $_GET; 
		//print_r($postData);
		if($os == 'android'){
			$w = $postData['bookings'];
			$postData =  json_decode($w);
			//print_r($postData);
		}*/
		if(empty($booking)){
			$postData = $this->request->getPost();
			$bookingId = $this->request->getParam('booking_id');
            $statusId = $this->request->getParam('bookingStatus', 0);
		}
		else{
			$postData = $booking;
			$bookingId = isset($booking['booking_id']) ? $booking['booking_id'] : 0;
			$statusId = isset($booking['status']) ? $booking['status'] : 0;
		}
		

        

        $modelBooking = new Model_Booking();
        $message = '';
        $isCanChangeBookingStatus = $modelBooking->checkIfCanChangeBookingStatus($bookingId, $statusId, $message);

        if (!$isCanChangeBookingStatus) {
            $errorMessages['bookingStatus'] = $message;
        }
		
		//print_r($postData);

        foreach ($postData as $key => $value) {

            /**
             * is empty validater
             */ 
			
			 
            $required_filed = array('country', 'city_id', 'bookingStatus', 'sub_total', 'total_qoute', 'customer_id', 'to_follow', 'street_number', 'street_address', 'suburb', 'postcode', 'state');
            if (in_array($key, $required_filed)) {
                if (empty($value) && $value != 0) {
                    $errorMessages[$key] = 'Field required';
                }
            }

            /**
             * time format
             */
            $is_all_day_event = isset($postData["is_all_day_event"]) ? 1 : 0;
            if (!$is_all_day_event) {
                $time_format = array('stparttime', 'etparttime');
                if (in_array($key, $time_format)) {
                    if (empty($value)) {
                        $errorMessages[$key] = 'Field required';
                    }
                }
            }
        }



        $services = isset($postData['services']) ? $postData['services'] : array();
		//print_r($services);
        if (count($services) === 0) {
            $errorMessages['services'] = 'Services cannot be empty';
        } else {
            foreach ($services as $service) {
				///By Islam for Android
				if(empty($booking)){
					$contractor = isset($postData['contractor_' . $service]) ? $postData['contractor_' . $service] : '';
					if (empty($contractor)) {
						$errorMessages['contractor_' . $service] = 'Field required';
					} else {
						$modelContractorService = new Model_ContractorService();
						$contractorService = $modelContractorService->getByContractorIdAndServiceId($contractor, $service);
						if (empty($contractorService)) {
							$errorMessages['contractor_' . $service] = "This Technician dosn't Provide this Service";
						} else {
							$city_id = isset($postData['city_id']) ? $postData['city_id'] : 0;

							$modelContractorServiceAvailability = new Model_ContractorServiceAvailability();
							$contractorServiceAvailability = $modelContractorServiceAvailability->getByCityIdAndContractorServiceId($city_id, $contractorService['contractor_service_id']);
							if (empty($contractorServiceAvailability)) {
								$errorMessages['contractor_' . $service] = "This Technician dosn't Provide this Service in this City";
							}
						}
					}
				}
				else{
					$loggedUser = CheckAuth::getLoggedUser();
					
					$contractor = $loggedUser['user_id'];
					if (empty($contractor)) {
						$errorMessages['contractor_' . $service['service_id']] = 'Field required';
					} else {
						$modelContractorService = new Model_ContractorService();
						$contractorService = $modelContractorService->getByContractorIdAndServiceId($contractor, $service['service_id']);
						if (empty($contractorService)) {
							$errorMessages['contractor_' . $service['service_id']] = "This Technician dosn't Provide this Service";
						} else {
							$city_id = isset($postData['city_id']) ? $postData['city_id'] : 0;

							$modelContractorServiceAvailability = new Model_ContractorServiceAvailability();
							$contractorServiceAvailability = $modelContractorServiceAvailability->getByCityIdAndContractorServiceId($city_id, $contractorService['contractor_service_id']);
							if (empty($contractorServiceAvailability)) {
								$errorMessages['contractor_' . $service['service_id']] = "This Technician dosn't Provide this Service in this City";
							}
						}
					}
					
					
				}
            }
        }

        $customerId = isset($postData['customer_id']) ? $postData['customer_id'] : 0;

        /*if ('contractor' == CheckAuth::getRoleName() && $customerId) {
            $loggedUser = CheckAuth::getLoggedUser();
            $modelCustomer = new Model_Customer();
            if (!$modelCustomer->getByCustomerIdAndCreatedBy($customerId, $loggedUser['user_id'])) {
                $errorMessages['customer_id'] = 'You dont have permission to select this customer';
            }
        }*/
		//print_r($errorMessages);
        if (count($errorMessages) !== 0) {
            return false;
        }
        return true;
    }
	
	public function validaterOffline(&$errorMessages,$postData) {

       /* print_r($postData);
		echo 'test by islam'; */
       /* $bookingId = $postData['booking_id'];
        $statusId = $postData['bookingStatus'];*/
        

        $modelBooking = new Model_Booking();
        $message = '';
        $isCanChangeBookingStatus = $modelBooking->checkIfCanChangeBookingStatus($postData['booking_id'], $postData['bookingStatus'], $message);

        if (!$isCanChangeBookingStatus) {
            $errorMessages['bookingStatus'] = $message;
        }

        foreach ($postData as $key => $value) {

            /**
             * is empty validater
             */
			 //'to_follow',
            $required_filed = array('country', 'city_id', 'bookingStatus', 'sub_total', 'total_qoute', 'stpartdate', 'etpartdate', 'customer_id',  'why', 'street_number', 'street_address', 'suburb', 'postcode', 'state');
            if (in_array($key, $required_filed)) {
                if (empty($value)) {
                    $errorMessages[$key] = 'Field required';
                }
            }

            /**
             * time format
             */
            $is_all_day_event = isset($postData["is_all_day_event"]) ? 1 : 0;
            if (!$is_all_day_event) {
                $time_format = array('stparttime', 'etparttime');
                if (in_array($key, $time_format)) {
                    if (empty($value)) {
                        $errorMessages[$key] = 'Field required';
                    }
                }
            }
        }



        $services = isset($postData['services']) ? (array)$postData['services'] : array();
		
		//echo '**************************************';
		//print_r($services);
				/*$pureService= array();
				foreach ($services as $key => $value) {
					foreach ($value as $k => $v) {
						//echo 'key '.$key;
						//echo 'value '.$value;
						foreach ($v as $k1 => $v1) {
							//echo 'key '.$key;
							//echo 'value '.$value;
							$pureService[$k1] = $v1;
						}
						
					}
				}
				print_r($pureService);*/
				
        if (count($services) === 0) {
            $errorMessages['services'] = 'Services cannot be empty';
        } else {
            foreach ($services as &$service) {
				/*$pureService= array();
				foreach ($service as $key => $value) {
						foreach ($value as $k1 => $v1) {
							//echo 'key '.$key;
							//echo 'value '.$value;
							$pureService[$k1] = $v1;
						}
					}
				$service = $pureService;*/
				//echo '/////////////////////// ';
				//print_r($service);
				//echo '///////////////////////';
				$loggedUser = CheckAuth::getLoggedUser();
                //$contractor = isset($postData['contractor_' . $service]) ? $postData['contractor_' . $service] : '';
				$contractor = $loggedUser['user_id'];
				// echo 'contractor   '.$contractor;
				// echo 'service id    '.$service['service_id'];
                if (empty($contractor)) {
                    $errorMessages['contractor_' . $service['service_id']] = 'Field required';
                } else {
                    $modelContractorService = new Model_ContractorService();
                    $contractorService = $modelContractorService->getByContractorIdAndServiceId($contractor, $service['service_id']);
                    if (empty($contractorService)) {
                        $errorMessages['contractor_' . $service['service_id']] = "This Technician dosn't Provide this Service";
                    } else {
                        $city_id = isset($postData['city_id']) ? $postData['city_id'] : 0;

                        $modelContractorServiceAvailability = new Model_ContractorServiceAvailability();
                        $contractorServiceAvailability = $modelContractorServiceAvailability->getByCityIdAndContractorServiceId($city_id, $contractorService['contractor_service_id']);
                        if (empty($contractorServiceAvailability)) {
                            $errorMessages['contractor_' . $service] = "This Technician dosn't Provide this Service in this City";
                        }
                    }
                }
            }
        }

        $customerId = isset($postData['customer_id']) ? $postData['customer_id'] : 0;

        /*if ('contractor' == CheckAuth::getRoleName() && $customerId) {
            $loggedUser = CheckAuth::getLoggedUser();
            $modelCustomer = new Model_Customer();
            if (!$modelCustomer->getByCustomerIdAndCreatedBy($customerId, $loggedUser['user_id'])) {
                $errorMessages['customer_id'] = 'You dont have permission to select this customer';
            }
        }*/
		//print_r($errorMessages);
        if (count($errorMessages) !== 0) {
            return false;
        }
        return true;
    }
	

    /**
     * saveAddress
     *
     * @param type $bookingId
     * @param type $mode 
     */
    public function saveAddress($booking , $bookingId, $mode = 'create') {

        $modelBookingAddress = new Model_BookingAddress();
        $modelBookingAddressTemp = new Model_BookingAddressTemp();
        $modelBooking = new Model_Booking();

 
        $data = $this->fillAddressParam($booking);

        $bookingAddress = $modelBookingAddress->getByBookingId($bookingId);

        $orginalAddress = $modelBookingAddress->getByBookingIdWithOutLatAndLon($bookingId);
        $newAddress = $this->fillAddressParam($booking, false);
		
        if (($mode == 'create') || (($mode == 'update') && $modelBooking->checkCanEditBookingDetails($bookingId))) {
            if ($bookingAddress) {
                $modelBookingAddress->updateById($bookingAddress['booking_address_id'], $data , $this->logId);
            } else {
                $modelBookingAddress->insert($data);
            }
        } else {
            if ($orginalAddress != $newAddress) {
			
                $db_params = array();
                $db_params['is_change'] = 1;
                $update = $modelBooking->updateById($bookingId, $db_params);
				$this->logId = $update['log_id'];

                $bookingAddressTemp = $modelBookingAddressTemp->getByBookingId($bookingId);
				
                if ($bookingAddressTemp) {
				  
                    $modelBookingAddressTemp->updateById($bookingAddressTemp['booking_address_id'], $data);
                } else {
				
				    
                    $modelBookingAddressTemp->insert($data);
                }
            }
        }
    }

    /**
     * fill Address Param
     * 
     * @param type $bookingid
     * @return type array
     */
    public function fillAddressParam($booking, $with_lat_lon = true) {
        /*$streetAddress = $this->request->getParam('street_address', '');
        $streetNumber = $this->request->getParam('street_number', '');
        $suburb = $this->request->getParam('suburb', '');
        $state = $this->request->getParam('state', '');
        $unitLotNumber = $this->request->getParam('unit_lot_number', '');
        $postcode = $this->request->getParam('postcode', '');
        $poBox = $this->request->getParam('po_box', '');
		
		*/
		$streetAddress = isset($booking['street_address']) ? $booking['street_address'] : ''; 
        $streetNumber = isset($booking['street_number']) ? $booking['street_number'] : ''; 
        $suburb = isset($booking['suburb']) ? $booking['suburb'] : '';
        $state = isset($booking['state']) ? $booking['state'] : '';
        $unitLotNumber = isset($booking['unit_lot_number']) ? $booking['unit_lot_number'] : '';
        $postcode = isset($booking['postcode']) ? $booking['postcode'] : '';
        $poBox = isset($booking['po_box']) ? $booking['po_box'] : '';
		$bookingId = isset($booking['original_booking_id']) ? $booking['original_booking_id'] : '';
		

        $address = array();
        $address['unit_lot_number'] = trim($unitLotNumber);
        $address['street_number'] = trim($streetNumber);
        $address['street_address'] = trim($streetAddress);
        $address['suburb'] = trim($suburb);
        $address['state'] = trim($state);
        $address['postcode'] = trim($postcode);
        $address['po_box'] = trim($poBox);
        $address['booking_id'] = (int) $bookingId;

        if ($with_lat_lon) {
            $modelBookingAddress = new Model_BookingAddress();
            $geocode = $modelBookingAddress->getLatAndLon($address);
            $address['lat'] = $geocode['lat'] ? $geocode['lat'] : 0;
            $address['lon'] = $geocode['lon'] ? $geocode['lon'] : 0;
        }

        return $address;
    }

    /**
     * saveWhyStatus
     * 
     * @param type $bookingId
     * @param type $statusId 
     */
    public function saveWhyStatus($booking = array(),$bookingId, $statusId, $addLog = true) {

        $loggedUser = CheckAuth::getLoggedUser();

        //$whyBookingStatus = $this->request->getParam('why', $this->request->getParam('extra_comments', ''));
		 $whyBookingStatus =  isset($booking['why']) ? $booking['why'] : '';

		
        if ($whyBookingStatus) {
			
            $modelBookingStatusDiscussion = new Model_BookingStatusDiscussion();
            $modelBookingDiscussion = new Model_BookingDiscussion();
            $modelBooking = new Model_Booking();

            $db_params = array();
            $db_params['booking_id'] = $bookingId;
            $db_params['user_id'] = $loggedUser['user_id'];
            $db_params['user_message'] = $whyBookingStatus;
            $db_params['created'] = time();

            $lastDiscussion = $modelBookingStatusDiscussion->getLastDiscussionByBookingIdAndStatusId($bookingId, $statusId);
            if (!$lastDiscussion) {
				
                $discussionId = $modelBookingDiscussion->insert($db_params);
            } else {
				
			
                $discussionId = $lastDiscussion['discussion_id'];
				
                $modelBookingDiscussion->updateById($discussionId, $db_params);
            }

            if ($discussionId) {
				
                $db_params = array();
                $db_params['discussion_id'] = $discussionId;
                $db_params['booking_id'] = $bookingId;
                $db_params['status_id'] = $statusId;
                $db_params['created'] = time();

                $bookingStatusDiscussion = $modelBookingStatusDiscussion->getByBookingIdAndStatusIdAndDiscussionId($bookingId, $statusId, $discussionId);
                if ($bookingStatusDiscussion) {
                    $modelBookingStatusDiscussion->updateById($bookingStatusDiscussion['id'], $db_params);
                } else {
                    $modelBookingStatusDiscussion->insert($db_params);
                }
            }
				
            if ($bookingId && $whyBookingStatus) {
                $modelBooking->updateById($bookingId, array('why' => $whyBookingStatus), $addLog,$this->logId);
            }
			
        }
    }

    /**
     * generateTitle
     *
     * @param type $services
     * @param type $cityId
     * @param type $totalQoute
     * @return string 
     */
    public function generateTitle($services, $cityId, $totalQoute) {

        $modelServices = new Model_Services();
        $modelUser = new Model_User();
        $modelCities = new Model_Cities();
        $modelContractorInfo = new Model_ContractorInfo();

        /**
         * get The First service;
         */
        $serviceAndClone = explode('_', $services[0]);
        $serviceId = (int) (isset($serviceAndClone[0]) ? $serviceAndClone[0] : 0);
        $clone = (int) (isset($serviceAndClone[1]) && $serviceAndClone[1] ? $serviceAndClone[1] : 0);
        $contractorId = (int) $this->request->getParam('contractor_' . $serviceId . ($clone ? '_' . $clone : ''));

        $generat_title_service = $modelServices->getById($serviceId);
        $servicesName = $generat_title_service['service_name'];

        $generat_title_city = $modelCities->getById($cityId);
        $cityName = strtoupper($generat_title_city['city_name']);

        $contractor = CheckAuth::getLoggedUser();;
        $modelContractorInfo->fill($contractor, array('contractor_info_by_user_id'));
        $contractorName = ucwords($contractor['username']);

        $title = "{$cityName}" . ' ' . "$servicesName" . ' - ' . "\${$totalQoute}" . ' - ' . "$contractorName";

        return $title;
    }

	
	public function registerMobileIosAction() {
		header('Content-Type: application/json');
		$accessToken = $this->request->getParam('access_token',0);
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];
		
		$loggedUser = CheckAuth::getLoggedUser();
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($data);
				exit;
				
			}
			
		}
		$loggedUser = CheckAuth::getLoggedUser();
		$ret_data = array('status' => 0);
		
		$model_ContractorMobileInfo = new Model_ContractorMobileInfo();
		
			$os = $this->request->getParam('os','ios');
			$user_id = $loggedUser['user_id'];	 
			
			$appname = $this->request->getParam('appname');
			$appversion = $this->request->getParam('appversion');
			$deviceuid = $this->request->getParam('deviceuid');
			$devicetoken = $this->request->getParam('devicetoken');
			$devicename = $this->request->getParam('devicename');
			$devicemodel = $this->request->getParam('devicemodel');
			$deviceversion = $this->request->getParam('deviceversion');
			$pushbadge = $this->request->getParam('pushbadge');
			$pushalert = $this->request->getParam('pushalert');
			$pushsound = $this->request->getParam('pushsound');
			$status = $this->request->getParam('status','insert');
			
			///delete all devices with the same device token
			$allDevices = $model_ContractorMobileInfo->getAllByDeviceUid($deviceuid, $os);
			if(!empty($allDevices)){
				$model_ContractorMobileInfo->deleteByDeviceUid($deviceuid);
				
			}
			///End
			
			
			$contractorMobileInfo = $model_ContractorMobileInfo->getByContractorIdAndDeviceTokenAndOs($user_id, $devicetoken, $os);
			
			
			$results = array();
			if(empty($contractorMobileInfo) && $status == 'insert' ){
				echo 'in insert....';
				$data = array(
					'contractor_id'=> $user_id,
					'user_id' => $user_id,
					'appname' => $appname,
					'appversion' => $appversion,
					'deviceuid' => $deviceuid,
					'devicetoken' => $devicetoken,
					'devicename' => $devicename,
					'devicemodel' => $devicemodel,
					'deviceversion' => $deviceversion,
					'pushbadge' => $pushbadge,
					'pushalert' => $pushalert,
					'pushsound' => $pushsound,
					'os' => $os
				);
				$results = $model_ContractorMobileInfo->insert($data);				
			}
			else if(!empty($contractorMobileInfo) && $status == 'delete'){
				
				$results = $model_ContractorMobileInfo->deleteByContractorIdAndDeviceToken($user_id,$devicetoken);
			}
			
			$ret_data['status'] = 1;
			if($results){
				$ret_data['msg'] = 'successfully';
				echo json_encode($ret_data);
			}
			else{
				
				$ret_data['msg'] = 'no change';
				echo json_encode($ret_data);
			} 
		exit;
		
	}
	
	public function getIosNotificationSettingsAction() {
		header('Content-Type: application/json');
		// load models
		$model_IosNotificationSetting = new Model_IosNotificationSetting();
		
		$loggedUser = Zend_Auth::getInstance()->getIdentity();
		
		$ret_data = array('status' => 0);
		

        if (!empty($loggedUser)) {	  
			
			$results = $model_IosNotificationSetting->getAllIosNotificationSetting();
			$ret_data['status'] = 1;
			$ret_data['results'] = $results;
			echo json_encode($ret_data);
		}
		else{
			$ret_data['status'] = 0;
			$ret_data['msg'] = 'unauthorized, please login at first';
			echo json_encode($ret_data);
		}
		
		exit;
		
	}
	
	
	public function iosUserNotificationSettingsAction() {
		header('Content-Type: application/json');
		$accessToken = $this->request->getParam('access_token',0);
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];
		
		
		$loggedUser = CheckAuth::getLoggedUser();
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($data);
				exit;
				
			}
			
		}
		
		
		// load models
		$modelAuthCredential = new Model_AuthRoleCredential();
		$mongo = new Model_Mongo();

				
		
		$ret_data = array('authrezed' => 0);
		
		

        //if (!empty($loggedUser)) {	  
			$user_id = $user['user_id'];
			$mode = $this->request->getParam('mode');
			           
			switch ($mode) {
                    case 'get_user_settings':
					    $loggedUser = CheckAuth::getLoggedUser();
					    $loggedUserRole = $loggedUser['role_id'];
                        $userRoles = $modelAuthCredential->getCredentialsByParentName('notifications', $loggedUserRole);
						$results = array();
						foreach ($userRoles as $key => $roles){
						  $results[$key]['credential_name'] = ucfirst(spaceBeforeCapital($roles['credential_name']));						  
                          $notification_settings = $mongo->getByTitleAndUserId(strtolower(preg_replace('/(?<!\ )[A-Z]/', ' $0', $roles['credential_name'])));
						  if($notification_settings){
						   $status = true;
						  }else{
						   $status = false;
						  }
						   $results[$key]['status'] = $status;
				        }
						
						break;
                    case 'update_user_setting':
						$credential_name = $this->request->getParam('credential_name');
                        $status = $this->request->getParam('status');
					
                        $credential_name = strtolower(preg_replace('/(?<!\ )[A-Z]/', ' $0', $credential_name));
						$credential_name = trim($credential_name);
						 if ($status == 'true') {						    
							$mongo->updateShowByTitle($credential_name);
						} else {
							$loggedUser = CheckAuth::getLoggedUser();
					        $user_id = $loggedUser['user_id'];
							$mongo->deleteByUserIdByCredentialNameAndUserID($credential_name, $user_id);
						}
						$results = array('is_success'=>true);
                        break;
                }
			$ret_data['authrezed'] = 1;
			$ret_data['results'] = $results;
			echo json_encode($ret_data);
		/*}
		else{
			$ret_data['status'] = 0;
			$ret_data['msg'] = 'unauthorized, please login at first';
			echo json_encode($ret_data);
		}*/
		
		exit;
		
	}

	
	public function iosUserNotificationAction() {
		header('Content-Type: application/json');
		$accessToken = $this->request->getParam('access_token',0);
				
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];
		
		
		$loggedUser = CheckAuth::getLoggedUser();
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
		
		
		
		
         if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($data);
				exit;
				
			}
		$loggedUser = 	CheckAuth::getLoggedUser();
		
		}
		//else{
			
         
			$mongo = new Model_Mongo();
			$notification = $mongo->getNotificationsByContractorId(true);
			//$notification = $mongo->testNotification(true);
			//print_r($results);
			

			
			$results= array();
			$i=0;
			
		if(!empty($notification)){
			foreach ($notification as $row){
				$results[$i]['notification_id']= $row['_id']->{'$id'};
				$results[$i]['notification_text']= $row['notification_text'];
				$results[$i]['date_sent']= $row['date_sent'];
				$results[$i]['booking_id']= $row['booking_id'];
				$results[$i]['read']= $row['read'];
				$results[$i]['title']= $row['title'];
				$results[$i]['seen']= $row['seen'];
				$results[$i]['show']= $row['show'];
				if(isset($row['thumbnails'])){
				  $results[$i]['thumbnails']= $row['thumbnails'];
				}else{
				 $results[$i]['thumbnails']= array();
				}
				
				$i++;
			}
		  }	
			$data['authrezed'] = 1;
			$data['results'] = $results;
			echo json_encode($data);
		
		
		exit;
		
	}
	
	public function iosUpdateNotificationReadAction() {
		header('Content-Type: application/json');
		$accessToken = $this->request->getParam('access_token',0);
		$notification_id = $this->request->getParam('notification_id','');
				
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];
		
		
		$loggedUser = CheckAuth::getLoggedUser();
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
		
	
         if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($data);
				exit;
				
			}
		$loggedUser = 	CheckAuth::getLoggedUser();
		
		}
		//else{
			
            if(!empty($notification_id)){
					$mongo = new Model_Mongo();
					$updated = 0;
				    $updated = $mongo->updateNotification($notification_id);
					
					
			}	

            $data['result'] = $updated;
            $data['authrezed'] = 1;
		//}

        echo json_encode($data);
        
		exit;
		
	}
	public function iosUpdateNotificationSeenAction() {
		header('Content-Type: application/json');
		$accessToken = $this->request->getParam('access_token',0);
	
				
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];
		
		
		$loggedUser = CheckAuth::getLoggedUser();
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($data);
				exit;
				
			}
		$loggedUser = 	CheckAuth::getLoggedUser();
		
		}
	

			$mongo = new Model_Mongo();
			$status = $mongo->clearCountstatusNotification();			
            $data['result'] = $status;
            $data['authrezed'] = 1;
	

        echo json_encode($data);
        
		exit;
		
	}
	
	
	public function iosBookingDistanceAction() {
		header('Content-Type: application/json');
		//get parameters
		$first_booking_id = $this->request->getParam('first_booking_id');
		$second_booking_id = $this->request->getParam('second_booking_id');
		$contractor_id = $this->request->getParam('contractor_id');
			
		// load models
		$model_IosUserNotificationSetting = new Model_IosUserNotificationSetting();
		$modelBookingAddress = new Model_BookingAddress();
		
		$loggedUser = Zend_Auth::getInstance()->getIdentity();
		
		$ret_data = array('status' => 0);
		

        if (!empty($loggedUser)) {	
			$firstBookingAddress = $modelBookingAddress->getByBookingId($first_booking_id);
				if($second_booking_id){
					$secondBookingAddress = $modelBookingAddress->getByBookingId($second_booking_id);
				}
				else{
					$modelUser = new Model_User();
					$secondBookingAddress = $modelUser->getById($contractor_id);
				}
			$distance = $modelBookingAddress->getDistanceByTwoAddress($firstBookingAddress, $secondBookingAddress);
					
			$ret_data['status'] = 1;
			$ret_data['results'] = $distance;
			echo json_encode($ret_data);
		}
		else{
			$ret_data['status'] = 0;
			$ret_data['msg'] = 'unauthorized, please login first';
			echo json_encode($ret_data);
		}
		
		exit;
		
	}
	
	
	public function getAllBookingStatusAction() {
		header('Content-Type: application/json');
		
		// load models
		$modelBookingStatus = new Model_BookingStatus();
		
		$loggedUser = Zend_Auth::getInstance()->getIdentity();
		
		$ret_data = array('status' => 0);
		

        if (!empty($loggedUser)) {	
			$allBookingStatus = $modelBookingStatus->getAll();
			
			$ret_data['status'] = 1;
			$ret_data['results'] = $allBookingStatus;
			echo json_encode($ret_data);
		}
		else{
			$ret_data['status'] = 0;
			$ret_data['msg'] = 'unauthorized, please login first';
			echo json_encode($ret_data);
		}
		
		exit;
		
	}
	
	
	public function getAllStatesAction() {
		header('Content-Type: application/json');
		
		// load models
		$modelCities = new Model_Cities();
		
		$loggedUser = Zend_Auth::getInstance()->getIdentity();
		//$companyId = CheckAuth::getCompanySession();
		
		$ret_data = array('status' => 0);
		

        if (!empty($loggedUser)) {	
			$allCities = $modelCities->getAll();
			$allStates = array();
			foreach($allCities as $city){
				$allStates[] = $city['state'];
			}
			
			$ret_data['status'] = 1;
			$ret_data['results'] = $allStates;
			echo json_encode($ret_data);
		}
		else{
			$ret_data['status'] = 0;
			$ret_data['msg'] = 'unauthorized, please login first';
			echo json_encode($ret_data);
		}
		
		exit;
		
	}
	
	
	
	public function receiveEmailsParametersAction() {
		header('Content-Type: application/json');
		//receive data and then save it in the temp db
		$ser = $_POST['str'];
		//echo $ser;
		$ret= unserialize($ser);
		print_r($ret);
		
		$request_id = uniqid();
		$modelCronjobDataTemp = new Model_CronjobDataTemp();
		foreach($ret as $ele){
			//echo 'serialize($ret  '.serialize($ret['data']);
			//print_r($ret['data']);
			$date = array(
			'email_info' => serialize($ele['data']),
			'email_content_parms' => serialize($ele['template_params']),
			'email_log' => serialize($ele['email_log']),
			'company_id' => 1,
			'request_id' => $request_id,
			'email_type' => $ele['cronjob_name'],
			'status' => 'sending'
			);
			$modelCronjobDataTemp->insert($date);
		}
		
			
		//return $request_id;	
		exit;
		
	}
	
	
	public function updateSentEmailsAction() {
		header('Content-Type: application/json');
		////// get update request
		$ser = $_POST['str'];
		//echo $ser;
		$ret= unserialize($ser);
		print_r($ret);
		
		$table = $ret['table'];
		echo 'table'.$table;
		
		$update_records = $ret['update_records'];
		$modelCronjobDataTemp = new Model_CronjobDataTemp();
		foreach($update_records as $ele){
			$ref_id = $ele[0];
			echo '$ref_id   '. $ref_id;
			$updaete_value = $ele[1];
			print_r($updaete_value);
			
		}
		/////now get sutible model according to table name
		//switch case
		
		
        exit;
		
	}
	
	public function openNewSession($accessToken){
			$modelIosUser = new Model_IosUser();
			//$user = $modelIosUser->getByUserInfoById($iosUserId);
			//echo 'accessToken   '.$accessToken;
			$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
			//echo 'tessssssssssssssssst';
			//print_r($user);
			//echo 'is empty '.empty($user);
			if(!empty($user)){
				$email = $user['email1'];
				$password = $user['password'];
				$authrezed = $this->getAuthrezed();
				$authrezed->setIdentity($email);
				$authrezed->setCredential($password);

				$auth = Zend_Auth::getInstance();
				$authrezedResult = $auth->authenticate($authrezed);
				if ($authrezedResult->isValid()) {

					$identity = $authrezed->getResultRowObject();

					$authStorge = $auth->getStorage();
					$authStorge->write($identity);

					CheckAuth::afterlogin(false,'app');
					//CheckAuth::afterlogin(false);
					
					$this->iosLoggedUser = $user['id'];
					
					return 1;
				}
				else{
					return 0;
				}
				
			}
			else{
				return 0;
				
			}
	
	}
	
	
	
	public function checkAccessTokenOfLoggedUser($accessToken){
			$modelIosUser = new Model_IosUser();
			//$user = $modelIosUser->getByUserInfoById($iosUserId);
			$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
			$loggedUser = CheckAuth::getLoggedUser();
			if($user['user_id'] == $loggedUser['user_id']){
				return 1;
			}
			return 0;
			
	}
	
	
	public function getPermissionsAction() {
		header('Content-Type: application/json');
		$accessToken = $this->request->getParam('access_token',0);
		//$registration_id = $this->request->getParam('registration_id');
			
		$modelIosUser = new Model_IosUser();
		$model_ContractorAndroidDeviceInfo = new Model_ContractorAndroidDeviceInfo();
		
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];
		
		
		$loggedUser = CheckAuth::getLoggedUser();
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)){
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			$loggedUser = CheckAuth::getLoggedUser();
			if(!$authrezed){
				
				echo json_encode($data);
				exit;
			}
			
		}
		
		$modelAuthRoleCredential = new Model_AuthRoleCredential();
		$permissions = $modelAuthRoleCredential->getAllCredentialByRoleId($loggedUser['role_id']);
		$data['result'] = $permissions;
		$data['authrezed'] = 1;	

        echo json_encode($data);
        exit;		
		
	}
	///***************ANDROID
	public function registerMobileAndroidAction() {
		header('Content-Type: application/json');
		$accessToken = $this->request->getParam('access_token',0);
		$registration_id = $this->request->getParam('registration_id');
			
		$modelIosUser = new Model_IosUser();
		$model_ContractorAndroidDeviceInfo = new Model_ContractorAndroidDeviceInfo();
		
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];
		
		
		$loggedUser = CheckAuth::getLoggedUser();
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			$loggedUser = CheckAuth::getLoggedUser();
			if($authrezed){
				$info = array(
				'contractor_id'=> $loggedUser['user_id'],
				'registration_id' => $registration_id
				
				);
				$result = $model_ContractorAndroidDeviceInfo->insert($info);
			
			
				$data['result'] = $result;
				$data['authrezed'] = 1;
			}
			
		}
		else{
			
            $info = array(
			'contractor_id'=> $loggedUser['user_id'],
			'registration_id' => $registration_id
			
			);
			$result = $model_ContractorAndroidDeviceInfo->insert($info);
			

            $data['result'] = $result;
            $data['authrezed'] = 1;
		}

        echo json_encode($data);
        exit;		
		
	}
	
	public function contractorServicesAction(){
		header('Content-Type: application/json');
	
	    $accessToken = $this->request->getParam('access_token',0);
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];
		$modelCompanies = new Model_Companies();

		
		$loggedUser = CheckAuth::getLoggedUser();
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($data);
				exit;	
			}
			
		}
	
		$modelContractorService = new Model_ContractorService();
		$loggedUser = 	CheckAuth::getLoggedUser();
		$contractor = $loggedUser['user_id'];
		$result = $modelContractorService->getContractorServices($contractor);
		
		$newResult = array();
		foreach($result as $key=>$value){
		  $newResult[$key] = $value;
		  $attributes = $modelContractorService->getContractorServicesAttributes($value['service_id']);
		  
		  $attributesArray = array();
		  foreach($attributes as $k=>$v){
			  $v['contractor_service_id']=$value['contractor_service_id'];
			  
		   $attributesArray[$k] = $v;
		   if($v['is_list'] == '1'){
		    $listValues = $modelContractorService->getContractorServicesAttributesListValues($v['attribute_id']);
		    $attributesArray[$k]['attribute_values'] = $listValues;
		   }
		   
		  }
		  
		  $newResult[$key]['attributes'] = $attributesArray;
		}
		
		$data['result'] = $newResult;
		$data['authrezed'] = 1;
		
        echo json_encode($data);
        exit;
	}
	
	
	public function getCountsAction(){
		header('Content-Type: application/json');
	    $accessToken = $this->request->getParam('access_token',0);
		$modelIosUser = new Model_IosUser();
		$modelBooking = new Model_Booking();
		$modelBookingStatus = new Model_BookingStatus();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];
		$modelCompanies = new Model_Companies();

		
		$loggedUser = CheckAuth::getLoggedUser();
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($data);
				exit;	
			}
			
		}
		
	  $loggedUser = CheckAuth::getLoggedUser();	 
	  $contractor_id = $loggedUser['user_id'];
	  
	  
	  
	  $BookingStatus = $modelBookingStatus->getAllWithoutPermission();
      
	  $counts = array();
	  foreach($BookingStatus as $status){
	   $results = array();
	   $total = $modelBooking->getcountBookingsByStatus($status['name'] , $contractor_id);
	   $results['name'] = $status['name'];
	   $results['total'] = $total;
	   array_push($counts,$results);
	  }

	  $results = array();
	  
	  $UnapprovedBooking = $modelBooking->getCountUnapprovedBooking();
	  $results['name'] = 'AWaiting Approval';
	  $results['total'] = $UnapprovedBooking;
	  array_push($counts,$results);
	  
	  $countRejectedBookings = $modelBooking->getCountRejectedBookings($contractor_id);
	  $results['name'] = 'Rejected';
	  $results['total'] = $countRejectedBookings;
	  array_push($counts,$results);
	  
	  $countNewReuests = $modelBooking->getCountNewRequest($contractor_id);
	  
	  $results['name'] = 'New Requests';
	  $results['total'] = $countNewReuests;
	  array_push($counts,$results);
	  
	  $modelComplaint = new Model_Complaint();
	  $countOpenComplaint = $modelComplaint->getComplaintCount(array('complaintStatus'=>'open','is_approved' => '1'));
	  $results['name'] = 'unresolved complaints';
	  $results['total'] = $countOpenComplaint;
	  array_push($counts,$results);
	  
	  
	  $data['result'] = $counts;
	  $data['authrezed'] = 1;
		
      echo json_encode($data);
      exit;
	}
	
	
	
	public function disableContractorServiceAction(){
	 header('Content-Type: application/json');
	 $accessToken = $this->request->getParam('access_token',0);
	 $modelIosUser = new Model_IosUser();
         $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);		
	 $service_id = $this->request->getParam('service_id',0);
	 $contractorServiceObj = new Model_ContractorService();
	 
	    $this->iosLoggedUser = $user['id'];
		$modelCompanies = new Model_Companies();

		
		$loggedUser = CheckAuth::getLoggedUser();
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			$data['type'] = 'error';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($data);
				exit;	
			}
			
		}
		
	  $loggedUser = CheckAuth::getLoggedUser();	 
	  $contractor_id = $loggedUser['user_id'];	
	  $isSuccess = false;
	  
	  $result = $contractorServiceObj->getByContractorIdAndServiceId($contractor_id, $service_id);
	  if($result){	     
		 //$update_data = array('service_status'=>$status);
		 $isSuccess = $contractorServiceObj->deleteById($result['contractor_service_id']);   
	  }
	  
	  if($isSuccess){
	    $data['msg'] = 'Service deleted successfully';
		$data['type'] = 'success';
	  }else{
	    $data['msg'] = 'No Changes in Contractor Services';
		$data['type'] = 'error';
	  }
	  
	  $data['IsSuccess'] = $isSuccess;
	  $data['authrezed'] = 1;
		
      echo json_encode($data);
      exit;
	
	}
	/*
	public function getCountriesAction(){
	
	header('Content-Type: application/json');
	 $accessToken = $this->request->getParam('access_token',0);
	 $modelIosUser = new Model_IosUser();
     $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
	 
	 $this->iosLoggedUser = $user['id'];
     $modelCompanies = new Model_Companies();	
	 $loggedUser = CheckAuth::getLoggedUser();
	 $data = array('authrezed' => 0);	
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($data);
				exit;	
			}
			
		}
	
	 $modelcountries = new Model_Countries();
	 $countries = $modelcountries->getCountriesAsArray();

	 
	 $data['result'] = $countries;
	 $data['authrezed'] = 1;
     echo json_encode($data);
     exit;	 
	 	
	}
	
	public function getCitiesAction(){
	
	header('Content-Type: application/json');
	 $accessToken = $this->request->getParam('access_token',0);
	 $country_id = $this->request->getParam('country_id', 0);
	 $state = $this->request->getParam('state', 0);
	 $modelIosUser = new Model_IosUser();
     $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
	 
	 $this->iosLoggedUser = $user['id'];
     $modelCompanies = new Model_Companies();	
	 $loggedUser = CheckAuth::getLoggedUser();
	 $data = array('authrezed' => 0);	
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($data);
				exit;	
			}
			
		}
	
	 $cities_obj = new Model_Cities();
     $cities = $cities_obj->getCitiesByCountryIdAndState($country_id, $state);

	 $tempCities = array();
        foreach ($cities as $city) {
            $tempCities[] = array(
                'id' => $city['city_id'],
                'name' => $city['city_name']
            );
        }

	 
	 $data['result'] = $tempCities;
	 $data['authrezed'] = 1;
     echo json_encode($data);
     exit;	 
	 	
	}*/
	
	public function contractorServiceCitiesAvailablitiyAction(){
	header('Content-Type: application/json');
	
	 $accessToken = $this->request->getParam('access_token',0);
	 $service_id = $this->request->getParam('service_id', 0);
	 //$cityId = $this->request->getParam('city_id');
	 //$cityIds = $this->request->getParam('city_id', array());
	 //$services = json_decode($service_ids);
	 
	 
	 $modelIosUser = new Model_IosUser();
     $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
	 
	 $this->iosLoggedUser = $user['id'];
     $modelCompanies = new Model_Companies();	
	 $loggedUser = CheckAuth::getLoggedUser();
	 $data = array('authrezed' => 0);	
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			$data['type'] = 'error';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($data);
				exit;	
			}
			
		}
		
	$loggedUser = CheckAuth::getLoggedUser();	 
	$contractor_id = $loggedUser['user_id'];
    $cityId = 	$loggedUser['city_id'];
	/*$serviceId_array = array();
	if(!empty($services)){
    foreach ($services as $key => &$value) {			
			$result = array();
			foreach ($value as $k => $v) {
				$result[$k] = $v;
			}	
			$serviceId_array[$key] = $result;
	 }
	} */
	 
	 

	$contractorServiceAvailabilityObj = new Model_ContractorServiceAvailability();
	$contractorServiceObj = new Model_ContractorService();

	
	 
	 
	  $success = 0;
	  $newService = array(
                        'service_id' => $service_id,
                        'contractor_id' => $contractor_id
                    );
		$contractorService = $contractorServiceObj->getByContractorIdAndServiceId($contractor_id, $service_id);	
		if (!$contractorService) {
                        $contractor_service_id = $contractorServiceObj->insert($newService);
						$newServiceAvailability = array(
						'city_id' => $cityId,
						'contractor_service_id' => $contractor_service_id
					);
					
					$success = $contractorServiceAvailabilityObj->insert($newServiceAvailability);
        }else if($contractorService){		        
					if(!$contractorServiceAvailabilityObj->getByCityIdAndContractorServiceId($cityId , $contractorService['contractor_service_id'])){
					  $newServiceAvailability = array(
						'city_id' => $cityId,
						'contractor_service_id' => $contractorService['contractor_service_id']
					  );
					  $success = $contractorServiceAvailabilityObj->insert($newServiceAvailability);
					}
        }
					
					
					
					
	 /* foreach ($serviceId_array as $serviceId) {
	                $success = 0;
                    $newService = array(
                        'service_id' => $serviceId['service_id'],
                        'contractor_id' => $contractor_id
                    );

                    if (!$contractorServiceObj->getByContractorIdAndServiceId($contractor_id, $serviceId)) {
                        $contractor_service_id = $contractorServiceObj->insert($newService);
                    }
					
					$newServiceAvailability = array(
						'city_id' => $cityId,
						'contractor_service_id' => $contractor_service_id
					);
					
					$success = $contractorServiceAvailabilityObj->insert($newServiceAvailability);
                }*/

	 	        
	   $IsSuccess = 0;
	   if($success){
	    $IsSuccess = 1;
		$data['msg'] = 'Service added successfully';
		$data['type'] = 'success';
	   }else{
	    $data['msg'] = 'No Changes in Contractor Services';
		$data['type'] = 'error';
	   }
	   
	   
	   
	  $result = array('IsSuccess' => $IsSuccess);
	  
	  $data['result'] = $result;
	  $data['authrezed'] = 1;
		
      echo json_encode($data);
      exit; 
 	 	
	}
	
	public function checkInOutAction(){
		
		
		header('Content-Type: application/json');
	
	 $accessToken = $this->request->getParam('access_token',0);
	 $case = $this->request->getParam('case','check_in');
	 $bookingAttendanceId = $this->request->getParam('booking_attendance_id',0);
	 $bookingId = $this->request->getParam('booking_id', 0);
	 $checkInTime = $this->request->getParam('check_in_time', null);
	 $checkInDistance= $this->request->getParam('check_in_distance', 0);
	 $checkInLat = $this->request->getParam('check_in_lat', 0);
	 $checkInLon = $this->request->getParam('check_in_lon', 0);
	 $checkOutTime = $this->request->getParam('check_out_time', null);
	 $checkOutDistance = $this->request->getParam('check_out_distance', 0);
	 $checkOutLat = $this->request->getParam('check_out_lat', 0);
	 $checkOutLon = $this->request->getParam('check_out_lon', 0);
		
	 /////
	/* echo 'case     '.$case;
	 echo '   bookingAttendanceId     '.$bookingAttendanceId;
	 echo '   bookingId     '.$bookingId;
	 echo '   checkInTime     '.$checkInTime;
	 echo '   checkInDistance     '.$checkInDistance;
	 echo '   checkInLat     '.$checkInLat;
	 echo '   checkInLon     '.$checkInLon;*/
	 /*echo '   checkOutTime     '.$checkOutTime;
	 echo '   checkOutDistance     '.$checkOutDistance;
	 echo '   checkOutLat     '.$checkOutLat;
	 echo '   checkOutLon     '.$checkOutLon;
	 exit;*/
	 
	 ///
	 $modelIosUser = new Model_IosUser();
     $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
	 
	 $this->iosLoggedUser = $user['id'];
      
	 $loggedUser = CheckAuth::getLoggedUser();
	 $data = array('authrezed' => 0);	
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($data);
				exit;	
			}
			
		}
		
	  $modelBookingAttendance = new Model_BookingAttendance();	
	  if($case == 'check_in'){
	      $loggedUser = CheckAuth::getLoggedUser();
		  
		  $attendaceData = array(
			 'booking_id' => $bookingId,
			 'contractor_id' => $loggedUser['user_id'],
			 'check_in_time' => $checkInTime,
			 'check_in_distance' => $checkInDistance,
			 'check_in_lat' => $checkInLat,
			 'check_in_lon' => $checkInLon,
			 'created'=> time()
			);
	 
		$bookingAttendanceId = $modelBookingAttendance->insert($attendaceData);
		if($bookingAttendanceId != 0){
			$success = 1;
		}
		else{
			$success = 0;
		}
		$result = array('IsSuccess' => $success,'booking_attendance_id'=>$bookingAttendanceId);
	   
	  }
	  else{
		  $attendaceData = array(
			 'check_out_time' => $checkOutTime,
			 'check_out_distance' => $checkOutDistance,
			 'check_out_lat' => $checkOutLat,
			 'check_out_lon' => $checkOutLon
			);
		  
		$loggedUser = CheckAuth::getLoggedUser();	
		$lastCheckInId = $modelBookingAttendance->getLastCheckInByBookingIdAndContractorId($bookingId,$loggedUser['user_id']);
		
		 
		 
		$bookingAttendanceId = $modelBookingAttendance->updateById($lastCheckInId,$attendaceData);
		$result = array('IsSuccess' => $bookingAttendanceId);
	   
	  }
	  
	  $data['result'] = $result;
	  $data['authrezed'] = 1;
		
      echo json_encode($data);
      exit; 
 	 	
	}
	
	
	public function unavailableTimeAction(){
	  
	    
	    $accessToken = $this->request->getParam('access_token',0);
        $modelIosUser = new Model_IosUser();
        $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
        $this->iosLoggedUser = $user['id'];
        $loggedUser = CheckAuth::getLoggedUser();
		$start = $this->request->getParam('event_start',0);
		$unavailable_event_id = $this->request->getParam('unavailable_event_id',0);
		$new_start = $start / 1000 ; 
		//$timestamp_start = date('Y-m-d H:i:s ', strtotime($start));
		$event_start = date(DATE_ATOM, $new_start);
		//echo $event_start;
		//exit;
		$end = $this->request->getParam('event_end',0);
		$new_end = $end / 1000 ; 
		//$timestamp_end = date('Y-m-d H:i:s ', strtotime($end));
		$event_end = date(DATE_ATOM, $new_end);	
	    $title = $this->request->getParam('title',0);  
	    $postcode = $this->request->getParam('postcode',0); 

			   
		
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
		
	    if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);			
			if(!$authrezed){
				echo json_encode($data);
				exit;	
			}		
        }
		
		
		
		
		$loggedUser = CheckAuth::getLoggedUser();
		$contractorId = $loggedUser['user_id'];
		$calendarServiceAccount = new Model_CalendarServiceAccount($contractorId);
		$unavailableEvent = new Model_UnavailableEvent();
		$event = new Google_Service_Calendar_Event();		
		$event->setDescription($title);
		$event->setSummary($title);
        if($postcode){
		 $event->setLocation($postcode);
		}
		
		
		
		$startObj = new Google_Service_Calendar_EventDateTime();
		$startObj->setTimeZone('Australia/Sydney');
		$startObj->setDateTime($event_start);
		$event->setStart($startObj);
		
		$endObj = new Google_Service_Calendar_EventDateTime();
		$endObj->setTimeZone('Australia/Sydney');
		$endObj->setDateTime($event_end);
		$event->setEnd($endObj);
		$event_id = 0;
		if($unavailable_event_id){
		  $old_event = $unavailableEvent->getById($unavailable_event_id);
		  $event_id = $old_event['event_id'];
		}
		
		
		if($event_id){
		  $updateEvent = $calendarServiceAccount->updateEvent($event_id, $event);
		  $EventId = $updateEvent['id'];
		}else{
		  $EventId = $calendarServiceAccount->insertEvent($event);
		}
				
		
		if($EventId){
		   $loggedUser = CheckAuth::getLoggedUser();
		   $contractorId = $loggedUser['user_id'];
		   $params = array('title'=>$title , 'event_start'=> $start ,'event_end'=> $end ,'postcode'=> $postcode , 'contractor_id'=>$contractorId,'event_id'=>$EventId);
		   if($event_id){
	
		    $unavailableEvent->update($params, "event_id= '{$event_id}'");	
		   }else{
		     $unavailableEvent->insert($params);
		   }
		   
		   
		   $data['isSuccess'] = 1;
           $data['authrezed'] = 1;
           $data['result'] = $unavailableEvent->getByEventId($EventId);
		}
	  
	  
	    
		
        echo json_encode($data);
        exit;
	
	}
	
	
	public function deleteUnavailableEventAction(){
	  
	  $data = array('authrezed' => 0);
	  $accessToken = $this->request->getParam('access_token',0);
	  $unavailable_event_id = $this->request->getParam('unavailable_event_id',0);
      $modelIosUser = new Model_IosUser();
      $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
      $this->iosLoggedUser = $user['id'];
      $loggedUser = CheckAuth::getLoggedUser();	
	  $unavailableEvent = new Model_UnavailableEvent();
	  
	  if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
		
	    if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);			
			if(!$authrezed){
				echo json_encode($data);
				exit;	
			}		
        }
		
		$loggedUser = CheckAuth::getLoggedUser();
        $contractorId = $loggedUser['user_id'];
		$calendarServiceAccount = new Model_CalendarServiceAccount($contractorId);
		
		$event_id = 0;
		if($unavailable_event_id){
		  $old_event = $unavailableEvent->getById($unavailable_event_id);
		  $event_id = $old_event['event_id'];
		  
		}
		
		$deletedEvent = $calendarServiceAccount->deleteEvent($event_id);
		$deleted = $unavailableEvent->deleteById($unavailable_event_id);
		if($deleted){
		  
		  $data['isSuccess'] = 1;
          $data['authrezed'] = 1;
		}else{
		  $data['isSuccess'] = 0;
          $data['authrezed'] = 1;
		}
		
		
		echo json_encode($data);
        exit;
	
	
	}
	
	
	public function getAllUnavailableEventAction(){
	  $data = array('authrezed' => 0);
	  $accessToken = $this->request->getParam('access_token',0);
      $modelIosUser = new Model_IosUser();
      $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
      $this->iosLoggedUser = $user['id'];
      $loggedUser = CheckAuth::getLoggedUser();	
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
		
	    if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);			
			if(!$authrezed){
				echo json_encode($data);
				exit;	
			}		
        }
	  
	  $loggedUser = CheckAuth::getLoggedUser();
      $contractorId = $loggedUser['user_id'];
	  $unavailableEvent = new Model_UnavailableEvent();
	  $results = $unavailableEvent->getAll(array('contractor_id'=>$contractorId));

	  $data['result'] = $results;
      $data['authrezed'] = 1;
	  
	  echo json_encode($data);
	  exit;
	}
	
	
	public function editInvoiceNumAction(){
	  
	  
	    $accessToken = $this->request->getParam('access_token',0);
        $modelIosUser = new Model_IosUser();
        $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
        $this->iosLoggedUser = $user['id'];
        $loggedUser = CheckAuth::getLoggedUser();
	    
		$invoiceId = $this->request->getParam('invoice_id',0);
	    $invoice_number = $this->request->getParam('invoice_num',0); 

				
		
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
		
	    if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);			
			if(!$authrezed){
				echo json_encode($data);
				exit;	
			}		
        }
		
		$loggedUser = CheckAuth::getLoggedUser();
		//
        // load model
        //
        
        $modelBookingLog = new Model_BookingLog();
        $modelBookingInvoice = new Model_BookingInvoice();
        $invoice = $modelBookingInvoice->getById($invoiceId);
        $modelBooking = new Model_Booking();
        if (!$invoice) {
            
			$data['msg'] = 'Invoice not exist';
			echo json_encode($data);
        }
        if (!CheckAuth::checkIfCanHandelAllCompany('invoice', $invoiceId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $data['msg'] = "You Don't have permission";
			echo json_encode($data);
        }
		
				if ((!$modelBooking->checkCanEditBookingDetails($invoice['booking_id'])) && ($invoice['invoice_num'] != $invoice_number )) {
                    $modelBooking->fillBookingAndServicesFieldTemp($invoice['booking_id']);
                    $modelBooking->updateById($invoice['booking_id'], array('is_change' => 1));
                    $dbParams = array(
                        'invoice_num' => $invoice_number,
                        'invoice_num_temp' => $invoice['invoice_num']
                    );
                } else {
                    $dbParams = array(
                        'invoice_num' => $invoice_number
                    );
                }

                // add  data log
                $modelBookingLog->addBookingLog($invoice['booking_id']);

                $updated = $modelBookingInvoice->updateById($invoiceId, $dbParams);

				if($updated){

					$data['type'] = 'success';
					$data['message'] = 'updated successfully ';
					$data['result'] = $this->getBooking(false, array('booking_id' => $invoice['booking_id']),0,true);
				}
				else{
					$data['type'] = 'error';
					$data['message'] = 'No data has changed';
				}
				
                $data['authrezed'] = 1;
				echo json_encode($data);
				exit;

		
		
	}
	
	public function visitedExtraInfoAction(){
			echo 'eeee';
			$data = $this->request->getPost();
			
			//$data = $_POST;
			if(empty($data)){
				$data = $_GET;
			}
			if(isset($data['BookingDates'])){
				$data = json_decode($data['BookingDates']);
			}
			
			$result = array();
			if(!empty($data)){
				foreach ($data as $key => $value){
					$date = array();
						foreach ($value as $k => $v){
							if(!is_array($v)){
							   $v = urldecode($v);
							 }
							$date[$k] = $v;
							
						}
					$result[$key] = $date;
                    $result1 = array();					
					$products = $result[$key]['products'];
					foreach ($products as $k1 => &$value) {			
						$result2 = array();
						foreach ($value as $k => $v) {
							$result2[$k] = $v;
						}	
						$result1[$k1] = $result2;
					}
					$result[$key]['products'] = $result1;
				}
	  }
	  
	   
			
			$modelVisitedExtraInfo = new Model_VisitedExtraInfo();		
			$modelBookingDiscussion = new Model_BookingDiscussion();
            $modelBooking = new Model_Booking();			
			foreach($result as $key=>$extra_info){
			  $date_extra_comments = $extra_info['extra_comments'];
			  $bookingId = $extra_info['booking_id'];
			  $products = $extra_info['products'];
			  $visisted_extra_info = array(
			  'job_start'=>$extra_info['job_start_time'],
			  'job_end'=>$extra_info['job_finish_time'],
			  'onsite_client_name'=>$extra_info['onsite_client_name'],
			  'booking_id'=>$extra_info['booking_id'],	  
			  );
			  $date_products  = array();
			  $date_ltr  = array();
			  foreach($products as $key=>$product){
			    $date_products[$key] = $product['product_id'];
			    $date_ltr[$key] = $product['ltr'];
			  }
			  
			  
             
			  $visited_info_id = $modelVisitedExtraInfo->insert($visisted_extra_info);	
				  if(!empty($date_extra_comments)){	
                   				  
					$modelBookingDiscussion->insert(array('booking_id'=>$bookingId , 'user_id'=> '8' , 'user_message' => $date_extra_comments , 'visited_extra_info_id'=>$visited_info_id ));
				}
			 	
				 if ($date_products) {
					 if($visited_info_id){
						 $delete_old = 0;
						
						$modelBookingProduct = new Model_BookingProduct();
						$modelBookingProduct->setProductsToBooking($bookingId, $date_products, $date_ltr , $visited_info_id,$delete_old,$this->logId);
					 }
				}			 
			}
			
			//print_r($result);
			
			$modelBooking->updateById($bookingId , array('visited_extra_info_id' => $visited_info_id));
			
			exit;
			
	
			
			
			
			//print_r($date_products);
			//echo 'after booking products';
			
			
			
		
	
	
	}
	
	public function getComplaintTypesAction(){
	  
	    $accessToken = $this->request->getParam('access_token',0);
        $modelIosUser = new Model_IosUser();
        $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
        $this->iosLoggedUser = $user['id'];
        $loggedUser = CheckAuth::getLoggedUser();
	    
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
		
	    if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);			
			if(!$authrezed){
				echo json_encode($data);
				exit;	
			}		
        }
		$data['authrezed'] = 1;
		$loggedUser = CheckAuth::getLoggedUser();

        //
        // get data list
        //
        $modelComplaintType = new Model_ComplaintType();
        $complaintTypes = $modelComplaintType->getAll();

		if(!empty($complaintTypes)){
			$data['msg'] = $complaintTypes;
		}
		
		echo json_encode($data);
		exit;
		
		
	}
	
	
	public function changeComplaintStatusAction(){
	  
	    $accessToken = $this->request->getParam('access_token',0);
        $modelIosUser = new Model_IosUser();
        $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
        $this->iosLoggedUser = $user['id'];
        $loggedUser = CheckAuth::getLoggedUser();
	    
		$result = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$result['msg'] = 'wrong access token';
			echo json_encode($result);
			exit;
		}
		
	    if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);			
			if(!$authrezed){
				echo json_encode($result);
				exit;	
			}		
        } 
		$result['authrezed'] = 1;
		$loggedUser = CheckAuth::getLoggedUser();

        //
        // get data list
        //
		$complaintId = $this->request->getParam('complaint_id',0);
		$complaint_status =$this->request->getParam('complaint_status','open');
		$discussion = $this->request->getParam('discussion','');
		$complaintData = array('complaint_status'=> $complaint_status );
        $modelComplaint = new Model_Complaint();
		$modelComplaintTemp = new Model_ComplaintTemp();
		$modelComplaintDiscussion = new Model_ComplaintDiscussion();		
		$loggedUser = CheckAuth::getLoggedUser();

		
		$complaint = $modelComplaint->getById($complaintId);
		if(! $complaint){		  
		  $result['msg'] = "error, the complaint doesn't exist";
		  echo json_encode($result);
		  exit;
		}
		
		
		$success = 0;
        if ($discussion) {
            $Discussiondata = array(
                'complaint_id' => $complaintId,
                'user_id' => $loggedUser['user_id'],
                'user_message' => $discussion,
                'created' => time()
            );
            $success = $modelComplaintDiscussion->insert($Discussiondata);
			//$modelComplaint->sendComplaintAsEmailToContractor($complaintId);
		
		$updated = 0;
		
		if($complaint_status == 'closed'){
		  
		    $loggedUser = CheckAuth::getLoggedUser();
			$user_id = !empty($loggedUser) ? $loggedUser['user_id'] : 0;
			
			$modelAuthRole = new Model_AuthRole();
			$contractor_role_id = $modelAuthRole->getRoleIdByName('contractor');
			if($contractor_role_id == $loggedUser['role_id'] ){
					$db_params = array();
					$db_params['is_approved'] = 0;
					$modelComplaint->updateById($complaintId, $db_params);
					$complaintTemp = $modelComplaintTemp->getByComplaintId($complaintId);
					$loggedUser = CheckAuth::getLoggedUser();$loggedUser = CheckAuth::getLoggedUser();
					$data['user_id'] = $loggedUser['user_id'];
					$data['complaint_status'] = 'closed';
					if ($complaintTemp) {					    
						$updated = $modelComplaintTemp->updateById($complaintTemp['id'], $data);
					} else {
						$added = $modelComplaintTemp->addComplaintTemp($complaintId,$data);
						if($added){
						  $updated = 1;
						}
					}
				
			}else{
			  $updated = $modelComplaint->updateById($complaintId, $data);
			}
		  
		
		}else if($complaint_status == 'open' ){
		
		$complaintTemp = $modelComplaintTemp->getByComplaintId($complaintId);
		if ($complaintTemp) {	
		   $modelComplaintTemp->deleteById($complaintTemp['id']); 
		 }
		   $updated = $modelComplaint->updateById($complaintId, array('complaint_status' => 'open','is_approved'=>1));
		}
	}	

		if($updated && $success){
			$result['msg'] = 'Update saved';
			if($complaint_status == 'open' ){
			 $result['result'] = $modelComplaint->getById($complaintId);		 
			}else{
			 $result['result'] = $modelComplaintTemp->getByComplaintId($complaintId);
			}
		}
		else{
			$result['msg'] = 'error, please try later';
		}
		
		
		
		echo json_encode($result);
		exit;
		
		
	}
	
	
	public function getAllRejectQuestionsAction(){
	 
	    $accessToken = $this->request->getParam('access_token',0);
        $modelIosUser = new Model_IosUser();
        $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
        $this->iosLoggedUser = $user['id'];
        $loggedUser = CheckAuth::getLoggedUser();
	    
		$result = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$result['msg'] = 'wrong access token';
			echo json_encode($result);
			exit;
		}
		
	    if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);			
			if(!$authrezed){
				echo json_encode($result);
				exit;	
			}		
        } 
		$result['authrezed'] = 1;
		$loggedUser = CheckAuth::getLoggedUser();

        //
        // get data list
        //
		$modelRejectBookingQuestion = new Model_RejectBookingQuestion();
		$modelRejectBookingQuestionOptions = new Model_RejectBookingQuestionOptions();
		$questions = $modelRejectBookingQuestion->getAll();
		
		foreach($questions as &$question){
			$options = array();
			if( $question['question_type'] == 'multiple'){
				$options = $modelRejectBookingQuestionOptions->getByQuestionId($question['id']);
			}
			$question['options'] = $options;
		} 
		$result['result'] = $questions;
		
		echo json_encode($result);
		exit;
		
		
	}
	
	public function answerRejectQuestionsAction(){
	 
	   $os = $this->request->getParam('os','ios');
		if($os == 'ios'){
		  $rawData = (array)json_decode(file_get_contents("php://input"));
		  $accessToken = $rawData['access_token'];
          $booking_id = $rawData['booking_id']; 
		}else{
		  $accessToken = $this->request->getParam('access_token',0);
	      $booking_id = $this->request->getParam('booking_id',0);
		}
	 
	    
        $modelIosUser = new Model_IosUser();
        $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
        $this->iosLoggedUser = $user['id'];
        $loggedUser = CheckAuth::getLoggedUser();
	    
		$result = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$result['msg'] = 'wrong access token';
			echo json_encode($result);
			exit;
		}
		
	    if (empty($loggedUser)) {
			//open new session
			$authrezed = $this->openNewSession($accessToken);			
			if(!$authrezed){
				echo json_encode($result);
				exit;	
			}		
        } 
		$result['authrezed'] = 1;
		$loggedUser = CheckAuth::getLoggedUser();

		
		
		if($os == 'ios'){
		 $answers = $rawData['answers'];
		}else{
		 $data = $this->request->getPost();
		}
		
		if(empty($data)){
			$data = $_GET;
		}
		if(!empty($data)){
		
		
		$answers = json_decode($data['answers']);
		
		}
       
	   $result1 = array();
		if(!empty($answers)){			
		foreach ($answers as $key => &$value) {			
			$result2 = array();
			foreach ($value as $k => $v) {
			if(!is_array($v)){
			               $v = urldecode($v);
			             }
				$result2[$k] = $v;
			}	
			$result1[$key] = $result2;
		 }
		}
		//print_r($result1);
		
		$modelRejectBookingQuestionAnswers = new Model_RejectBookingQuestionAnswers();
		$modelRejectBookingQuestionAnswers->deleteByBookingId($booking_id);
		foreach($result1 as $answer){
		    
			$ret = $modelRejectBookingQuestionAnswers->insert($answer);
		}
		//echo $ret;
		$result['result'] = $this->getBooking(false, array('booking_id' => $booking_id));
		/*if($ret != 0 ){
			$result['result'] = "saved successfully";
		}
		else{
			$result['result'] = "failed, please try again";
			
		}*/
		
		echo json_encode($result);
		exit;
		
		
	}


	public function getImageTagsAction(){
	
	    $itemId =  $this->request->getParam('itemid',0);
	    $type =  $this->request->getParam('type',0);
	    $accessToken = $this->request->getParam('access_token',0);
        $modelIosUser = new Model_IosUser();
        $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
        $this->iosLoggedUser = $user['id'];
        $loggedUser = CheckAuth::getLoggedUser();
	    
		$result = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$result['msg'] = 'wrong access token';
			echo json_encode($result);
			exit;
		}
		
	    if (empty($loggedUser)) {
			//open new session
			$authrezed = $this->openNewSession($accessToken);			
			if(!$authrezed){
				echo json_encode($result);
				exit;	
			}		
        } 
		$result['authrezed'] = 1;
		$loggedUser = CheckAuth::getLoggedUser();
	  		  
		$modelImageTypes = new Model_ImageTypes();	   
		$imagetypes = $modelImageTypes->getAll();
		
		$tempImagetypes = array();
        foreach ($imagetypes as $imagetype) {
            $tempImagetypes[] = array(
                'id' => $imagetype['image_types_id'],
                'name' => $imagetype['name']
            );
        }
		
		$result['ImageTypes'] = $tempImagetypes;
		  
		echo json_encode($result);
		exit;
	  
	
	}
	
	
	public function imageUploadAction(){
	  
	  
	  $count = $this->request->getParam('count','');  
	  $iosPhotos = $this->request->getParam('photos',array());  
	  $accessToken = $this->request->getParam('access_token',0);
	  $user_role = $this->request->getParam('user_role' , 'contractor');
	  $customer_id = $this->request->getParam('customer_id' , 0);
	  $success = 0; 
	  $modelItemImageDiscussion =new Model_ItemImageDiscussion();
	  $modelImage =new Model_Image();
	  
	  
	 //var_dump($iosPhotos);
	 //exit;
	 
	    $modelIosUser = new Model_IosUser();
        $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
        $this->iosLoggedUser = $user['id'];
       
	   
	  $loggedUser = CheckAuth::getLoggedUser();  
	  $result = array('authrezed' => 0);
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$result['msg'] = 'wrong access token';
			echo json_encode($result);
			exit;
		}
	 
		
	    if (empty($loggedUser)) {
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			
			if(!$authrezed){
				echo json_encode($result);
				exit;	
			}		
        }
       		
		$result['authrezed'] = 1;
		$loggedUser = CheckAuth::getLoggedUser();
		
		
		
				
		$dir = get_config('image_attachment') . '/';
                 $subdir = date('Y/m/d/');
                 $fullDir = $dir . $subdir;
                  if (!is_dir($fullDir)) {
                        mkdir($fullDir, 0777, true);
       }
	   
	   
	   // android save image 
	    
	    if((isset($count) && $count != '')){ 
		$image_group = 0;
		if( $count > 1){
			 $maxGroup =  max($modelImage->getMaxGroup());
	         $image_group = $maxGroup['group_id'] + 1;			 
			}
			
		for($i=0; $i<$count; $i++) {
		
		$source  = $_FILES['imageFile'.$i]['tmp_name'];
		$extension = $this->request->getParam('ext'.$i,'png');
        $imageInfo = pathinfo($source);
        $ext = $extension;
		$successUpload = 0;
		$fileName = "image_{$i}.{$ext}";
		$file = $fullDir . $fileName;
		$image_saved = copy($source, $fullDir . $fileName);						
		if ($image_saved) {
			$successUpload = 1;
			$image_id = $this->SaveImagesData($image_group,$count);
			$original_path = "original_{$image_id}.{$ext}";
			$large_path = "large_{$image_id}.{$ext}";
			$small_path = "small_{$image_id}.{$ext}";
			$thumbnail_path = "thumbnail_{$image_id}.{$ext}";
			$compressed_path = "compressed_{$image_id}.{$ext}";

			 $data = array(
							'original_path' => $subdir . $original_path,
							'large_path' => $subdir . $large_path,
							'small_path' => $subdir . $small_path,
							'thumbnail_path' => $subdir . $thumbnail_path ,
							'compressed_path' => $subdir . $compressed_path
			 );

			$modelImage->updateById($image_id, $data);		
			$compressed_saved = copy($source, $fullDir . $compressed_path);
			$image_saved = copy($source, $fullDir . $original_path);		
			ImageMagick::scale_image($source, $fullDir . $large_path, 510);
			ImageMagick::scale_image($source, $fullDir . $small_path, 250);
			ImageMagick::create_thumbnail($source, $fullDir . $thumbnail_path, 78, 64);
			ImageMagick::convert($source, $fullDir . $compressed_path);
			ImageMagick::compress_image($source, $fullDir . $compressed_path);

			
		}
						
						
		if ($image_saved) {
                        if (file_exists($source)) {
                            unlink($source);
                        }
						if (file_exists($file)) {
                            unlink($file);
                        }               
                    }
						
			}

	    $result['result'] = $successUpload;
        echo json_encode($result);
		exit;
 				
		}
		
		
		// ios save image
		
	 if((isset($iosPhotos) && !empty($iosPhotos))){ 
		$image_group = 0;
		$count = count($iosPhotos);
		if( $count > 1){
			 $maxGroup =  max($modelImage->getMaxGroup());
	         $image_group = $maxGroup['group_id'] + 1;			 
			}
			
		foreach($iosPhotos as $key=>$imageData){
		
		$successUpload = 0;
		
		if(isset($imageData) && !empty($imageData)){
		  $image = str_replace('data:image/png;base64,', '', $imageData);
          $image = str_replace(' ', '+', $image);
	      $image = base64_decode($image);
	      $fileName = "image_{$key}.png";	  
	      $file = $fullDir. $fileName;
          $success = file_put_contents($file, $image);
		  if($success){
		  
			$successUpload = 1;
			$image_id = $this->SaveImagesData($image_group,$count);
			$original_path = "original_{$image_id}.png";
			$large_path = "large_{$image_id}.png";
			$small_path = "small_{$image_id}.png";
			$thumbnail_path = "thumbnail_{$image_id}.png";
			$compressed_path = "compressed_{$image_id}.jpg";

			 $data = array(
							'original_path' => $subdir . $original_path,
							'large_path' => $subdir . $large_path,
							'small_path' => $subdir . $small_path,
							'thumbnail_path' => $subdir . $thumbnail_path ,
							'compressed_path' => $subdir . $compressed_path
			 );

			$modelImage->updateById($image_id, $data);		
			$compressed_saved = copy($file, $fullDir . $compressed_path);
			$image_saved = copy($file, $fullDir . $original_path);		
			ImageMagick::scale_image($file, $fullDir . $large_path, 510);
			ImageMagick::scale_image($file, $fullDir . $small_path, 250);
			ImageMagick::create_thumbnail($file, $fullDir . $thumbnail_path, 78, 64);
			ImageMagick::convert($file, $fullDir . $compressed_path);
			ImageMagick::compress_image($file, $fullDir . $compressed_path);
		
			if (file_exists($file)) {
                            unlink($file);
                        } 
			
		  }		  
		}			
						
	    }

	    $result['result'] = $successUpload;
        echo json_encode($result);
		exit;
 				
		}
		
		
		
		$result['msg'] = 'there is no files';
        echo json_encode($result);
		exit;
		
	}
	
	public function SaveImagesData($imageGroup , $count ){
	  
	  $itemId =  $this->request->getParam('bookingId',0);
	  $type =  $this->request->getParam('type','booking');	
	  $image_label = $this->request->getParam('imageType','');
	  $discussion = $this->request->getParam('comment','');
          $service_id = $this->request->getParam('service', 0);
	  $user_role = $this->request->getParam('user_role' , 'contractor');
	  
	  
	  
	  $modelItemImageDiscussion =new Model_ItemImageDiscussion();
	  $modelBookingDiscussion = new Model_BookingDiscussion();
	  $modelImage = new Model_Image();
	  $loggedUser = CheckAuth::getLoggedUser();
	  
	  
	   
	  $logged_user_id = ($user_role == 'customer') ? $loggedUser['customer_id'] :  $loggedUser['user_id'] ; 
	  
	  $data = array(
                      'user_ip'=> ''
				     ,'created_by'=>$logged_user_id
					 ,'image_types_id'=>$image_label
					 ,'group_id'=>$imageGroup
					
        ); 
			
	    $id = $modelImage->insert($data);	
		$image_id = $id;
		if($count > 1){
			$image_id = 0;
		}
	  
	  $success = 0;
        if ($discussion) {				 
			if($type == 'booking'){
			$data = array(
                    'booking_id' => $itemId,
                    'user_id' => $logged_user_id,
                    'user_message' => $discussion,
                    'created' => time()
                 );				 
			$success = $modelBookingDiscussion->insert($data);
			$dataDiscssion = array(
				 'image_id'=>$image_id,
				 'group_id' =>$imageGroup,
				 'item_id'=>$success,
				 'type'=>$type
				 );
		    $modelItemImageDiscussion->insert($dataDiscssion);
			 } 
		    }
		if(!($service_id == 0)){
		    $type_new = $type;
				if($success){
					$type_new = $type_new.'_discussion' ;
				  }
				   $type_new = $type_new.'_service';				    
				   if($type == 'service'){
					   $itemId = $service_id;
					}					 
			}else{
				  $type_new = $type;
				  if($success){
					$type_new = $type_new.'_discussion' ;
				  }
				}
		$Itemdata = array(
				'item_id'=>$itemId,
				'service_id'=>$service_id,
				'discussion_id'=>$success,
				'image_id' =>$id,
				'type'=>$type_new
				);
				 
				 
		$modelItemImage = new Model_ItemImage();
        $Item_image_id = $modelItemImage->insert($Itemdata);

        return $id;
	  
	
	
	}
	
	
	
	
	/*public function imageUploadAction(){
	  
	  $itemId =  $this->request->getParam('bookingId',0);
	  $type =  $this->request->getParam('type','booking');	
	  $image_label = $this->request->getParam('imageType','');
	  $discussion = $this->request->getParam('comment','');
      $service_id = $this->request->getParam('service', 0);
	  $count = $this->request->getParam('count','');  
	  $accessToken = $this->request->getParam('access_token',0);
	  $success = 0;
      $modelIosUser = new Model_IosUser();
      $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
	  $modelItemImageDiscussion =new Model_ItemImageDiscussion();
	  $modelBookingDiscussion = new Model_BookingDiscussion();
	  $modelImageAttachment = new Model_Image();
      $this->iosLoggedUser = $user['id'];
      $loggedUser = CheckAuth::getLoggedUser();  
	  
	  $result = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$result['msg'] = 'wrong access token';
			echo json_encode($result);
			exit;
		}
		
	    if (empty($loggedUser)) {
			//open new session
			$authrezed = $this->openNewSession($accessToken);			
			if(!$authrezed){
				echo json_encode($result);
				exit;	
			}		
        } 
		$result['authrezed'] = 1;
		$loggedUser = CheckAuth::getLoggedUser();
		
				
		$dir = get_config('image_attachment') . '/';
                 $subdir = date('Y/m/d/');
                 $fullDir = $dir . $subdir;
                  if (!is_dir($fullDir)) {
                        mkdir($fullDir, 0777, true);
       }
	   
	    
	    if(isset($count) && $count != ''){
		   
		$image_group = 0;
		if( $count > 1){
			 $maxGroup =  max($modelItemImageDiscussion->getMaxGroup());
	         $image_group = $maxGroup['group_id'] + 1;			 
			}
			
		for($i=0; $i<$count; $i++) {
		
		$source  = $_FILES['imageFile'.$i]['tmp_name'];
		$extension = $this->request->getParam('ext'.$i,'png');
        $imageInfo = pathinfo($source);
		
        //$ext = $imageInfo['extension'];
        $ext = $extension;


		  $data = array(
                      'user_ip'=> ''
				     ,'created_by'=>$loggedUser['user_id'] 
					 ,'image_types_id'=>$image_label
					 ,'group_id'=>$image_group
                 ); 
			
	    $id = $modelImageAttachment->insert($data);	
		$image_id = $id;
		if($count > 1){
			$image_id = 0;
		}
		
		
		
		$success = 0;
               if ($discussion) {				 
				 if($type == 'booking'){
				   $data = array(
                    'booking_id' => $itemId,
                    'user_id' => $loggedUser['user_id'],
                    'user_message' => $discussion,
                    'created' => time(),
                 );				 
				   $success = $modelBookingDiscussion->insert($data);
				 
				 
				 
				 $dataDiscssion = array(
				 'image_id'=>$image_id,
				 'group_id' =>$image_group,
				 'item_id'=>$success,
				 'type'=>$type
				 );
				 $modelItemImageDiscussion->insert($dataDiscssion);
				 
				 
				 
                } 
		    }
		
		if(!($service_id == 0)){
				 $type_new = $type;
				  if($success){
					$type_new = $type_new.'_discussion' ;
				  }
				  $type_new = $type_new.'_service';				    
					 if($type == 'service'){
					   $itemId = $service_id;
					}					 
				}else{
				  $type_new = $type;
				  if($success){
					$type_new = $type_new.'_discussion' ;
				  }
				}
		$Itemdata = array(
				'item_id'=>$itemId,
				'service_id'=>$service_id,
				'discussion_id'=>$success,
				'image_id' =>$id,
				'type'=>$type_new
				);
				 
				 
				$modelItemImage = new Model_ItemImage();
                $Item_image_id = $modelItemImage->insert($Itemdata);

                $original_path = "original_{$id}.{$ext}";
                $large_path = "large_{$id}.{$ext}";
                $small_path = "small_{$id}.{$ext}";
                $thumbnail_path = "thumbnail_{$id}.{$ext}";
                $compressed_path = "compressed_{$id}.jpg";

                $data = array(
                        'original_path' => $subdir . $original_path,
                        'large_path' => $subdir . $large_path,
                        'small_path' => $subdir . $small_path,
                        'thumbnail_path' => $subdir . $thumbnail_path ,
                        'compressed_path' => $subdir . $compressed_path
                    );

                $modelImageAttachment->updateById($id, $data);
				 
				//$original_path = basename($_FILES['imageFile'.$i]['name']);
				$successUpload = 0;
				$image_saved = copy($_FILES['imageFile'.$i]['tmp_name'], $fullDir . $original_path);
			    $compressed_saved = copy($source, $fullDir . $compressed_path);	
						
						if ($image_saved) {
						    $successUpload = 1;
							ImageMagick::scale_image($source, $fullDir . $large_path, 510);
				            ImageMagick::scale_image($source, $fullDir . $small_path, 250);
				            ImageMagick::create_thumbnail($source, $fullDir . $thumbnail_path, 78, 64);
				            ImageMagick::convert($source, $fullDir . $compressed_path);
				            ImageMagick::compress_image($source, $fullDir . $compressed_path);
						}
						
				if ($image_saved) {
                        if (file_exists($source)) {
                            unlink($source);
                        }               
                    }
						
			}

	    $result['result'] = $successUpload;
        echo json_encode($result);
		exit;
 				
		}
		
		$result['msg'] = 'there is no files';
        echo json_encode($result);
		exit;
		
	}*/
	public function getEmailContentAction(){
	
	 header('Content-Type: application/json');
	 $accessToken = $this->request->getParam('access_token',0);
	 $modelIosUser = new Model_IosUser();
     $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
	 
	 $this->iosLoggedUser = $user['id'];
     $modelCompanies = new Model_Companies();	
	 $loggedUser = CheckAuth::getLoggedUser();
	 $data = array('authrezed' => 0);	
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($data);
				exit;	
			}
			
		}
	 $type = $this->request->getParam('type','invoice');
	 $id = $this->request->getParam('id',0);
	 
        //
        // load models
        //
        $modelUser = new Model_User();
        $modelBooking = new Model_Booking();
        $modelCustomer = new Model_Customer();
        $modelBookingAddress = new Model_BookingAddress();
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();

        //
        // geting data
        //
        $invoice = $modelBookingInvoice->getById($id);
        $modelBookingInvoice->fill($invoice, array('booking'));

        //
        // validation
        //
        if (!$invoice) {
			$data['msg'] = "Invoice not exist";
			 $data['authrezed'] = 1;
			 echo json_encode($data);
			 exit;	 
           
        }
        //check in can see his or assigned invoices
        if (!$modelBookingInvoice->checkIfCanSeeHisOrAssignedInvoice($id)) {
			$data['msg'] = "You Don't have permission";
			 $data['authrezed'] = 1;
			 echo json_encode($data);
			 exit;
           
        }

        //
        // filling extra data
        //
        $customer = $modelCustomer->getById($invoice['booking']['customer_id']);
        $modelCustomer->fill($customer, array('customer_commercial_info', 'customer_contacts'));
        $this->view->customerContacts = $customer['customer_contacts'];

        $user = $modelUser->getById($invoice['booking']['created_by']);

        $viewParam = $this->getInvoiceViewParam($id, true);
        $view = new Zend_View();
        $view->setScriptPath(APPLICATION_PATH . '/modules/invoices/views/scripts/index');
        $view->bookingServices = $viewParam['bookingServices'];
        $view->thisBookingServices = $viewParam['thisBookingServices'];
        $view->priceArray = $viewParam['priceArray'];
        $view->invoice = $viewParam['invoice'];
        $view->customer = $customer;
        $view->isAccepted = $modelBooking->checkBookingIfAccepted($invoice['booking_id']);
        $view->booking = $invoice['booking'];
        $bodyInvoice = $view->render('invoice.phtml');
	
		 
           // Create pdf
            $pdfPath = createPdfPath();
           $destination = $pdfPath['fullDir'] . $viewParam['invoice']['invoice_num'] . '.pdf';
            wkhtmltopdf($bodyInvoice, $destination);
            //echo 'destination   '.$destination;
			$parts = explode("public", $destination);
			//print_r($parts);
			$attachment = $parts[1];


        $template_params = array(
            //invoice
            '{invoice_num}' => $invoice['invoice_num'],
            '{invoice_created}' => date('d/m/Y', $invoice['created']),
            //booking
            '{booking_num}' => $invoice['booking']['booking_num'],
            '{total_without_tax}' => number_format($invoice['booking']['sub_total'], 2),
            '{gst_tax}' => number_format($invoice['booking']['gst'], 2),
            '{total_with_tax}' => number_format($invoice['booking']['qoute'], 2),
            '{description}' => $invoice['booking']['description'] ? $invoice['booking']['description'] : '',
            '{booking_created}' => date('d/m/Y', $invoice['booking']['created']),
            '{booking_created_by}' => ucwords($user['username']),
            '{booking_start}' => date("F j, Y, g:i a", strtotime($invoice['booking']['booking_start'])),
            '{booking_address}' => get_line_address($modelBookingAddress->getByBookingId($invoice['booking']['booking_id'])),
            '{service}' => nl2br($modelContractorServiceBooking->getBookingAsText($invoice['booking']['booking_id'], true)),
            '{invoice_view}' => $bodyInvoice,
            //customer
            '{customer_name}' => get_customer_name($customer),
            '{customer_first_name}' => isset($customer['first_name']) && $customer['first_name'] ? ucwords($customer['first_name']) : '',
            '{customer_last_name}' => isset($customer['last_name']) && $customer['last_name'] ? ' ' . ucwords($customer['last_name']) : '',
            '{customer_contacts}' => nl2br($modelCustomer->getCustomerContacts($invoice['booking']['customer_id'])),
        );

        $modelEmailTemplate = new Model_EmailTemplate();
        $emailTemplate = $modelEmailTemplate->getEmailTemplate('send_invoice_as_email', $template_params);

		//print_r($emailTemplate); 
        $body = $emailTemplate['body'];
        $subject = $emailTemplate['subject'];
        $to = array();
        if ($customer['email1']) {
            $to[] = $customer['email1'];
        }
        if ($customer['email2']) {
            $to[] = $customer['email2'];
        }
        if ($customer['email2']) {
            $to[] = $customer['email3'];
        }
        $to = implode(',', $to);
		$email = array('to'=>$to,'subject'=>$subject,'body'=>$body,'attachment'=>$attachment,'attachment_full_path'=>$destination);
		

	 
	 $data['result'] = $email;
	 $data['authrezed'] = 1;
     echo json_encode($data);
     exit;	 
	 	
	}
	public function getInvoiceViewParam($invoiceId, $toBuffer = false) {

        //
        // load model
        //
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelContractorServiceBookingTemp = new Model_ContractorServiceBookingTemp();
        $modelBooking = new Model_Booking();

        //
        // geting data
        //
        $invoice = $modelBookingInvoice->getById($invoiceId);


        //
        // validation
        //
        if (!$invoice) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Invoice not exist 7"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // filling extra data
        //
        $modelBookingInvoice->fill($invoice, array('booking', 'labels', 'due_date', 'multiple_days'));

        // $thisBookingServices : to put all service for this booking 
        $thisBookingServices = array();

        // $priceArray : to put all price and service for this booking 
        $priceArray = array();


        /*
         *  $bookingServices : to put all booking service from  original booking service (or from temp if the contractor change values untill approved)
         */

        //$bookingServices = $modelContractorServiceBooking->getByBookingId($invoice['booking_id']);

        $contractorServiceBookings = $modelContractorServiceBooking->getByBookingId($invoice['booking_id']);
        $contractorServiceBookingsTemp = $modelContractorServiceBookingTemp->getByBookingId($invoice['booking_id']);
        $isTemp = false;
        $bookingServices = array();
        if ($contractorServiceBookingsTemp && !$modelBooking->checkCanEditBookingDetails($invoice['booking_id']) && !$toBuffer) {
            $bookingServices = $contractorServiceBookingsTemp;
            $isTemp = true;
            foreach ($bookingServices as $bookingService) {

                $serviceId = $bookingService['service_id'];
                $clone = $bookingService['clone'];
                $bookingId = $bookingService['booking_id'];

                $service_and_clone = $serviceId . ($clone ? '_' . $clone : '');

                $thisBookingServices[] = $service_and_clone;

                $priceArray[$service_and_clone] = $modelContractorServiceBookingTemp->getTotalServiceBookingQoute($bookingId, $serviceId, $clone);
            }
        } elseif ($contractorServiceBookings) {
            $bookingServices = $contractorServiceBookings;
            foreach ($bookingServices as $bookingService) {

                $serviceId = $bookingService['service_id'];
                $clone = $bookingService['clone'];
                $bookingId = $bookingService['booking_id'];

                $service_and_clone = $serviceId . ($clone ? '_' . $clone : '');

                $thisBookingServices[] = $service_and_clone;

                $priceArray[$service_and_clone] = $modelContractorServiceBooking->getTotalServiceBookingQoute($bookingId, $serviceId, $clone);
            }
        }

        if (!$toBuffer) {

            $this->view->invoice = $invoice;
            $this->view->bookingServices = $bookingServices;
            $this->view->thisBookingServices = $thisBookingServices;
            $this->view->priceArray = $priceArray;
            $this->view->isTemp = $isTemp;
        } else {
            $viewParam = array();

            $viewParam['invoice'] = $invoice;

            $viewParam['bookingServices'] = $bookingServices;
            $viewParam['thisBookingServices'] = $thisBookingServices;
            $viewParam['priceArray'] = $priceArray;
            //$viewParam['isTemp'] = $isTemp;

            return $viewParam;
        }

        return false;
    }
	
	
	
	public function sendEmailFromAppAction(){ 
	
	 header('Content-Type: application/json');
	 $accessToken = $this->request->getParam('access_token',0);
	 $modelIosUser = new Model_IosUser();
     $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
	 
	 $this->iosLoggedUser = $user['id'];
     $modelCompanies = new Model_Companies();	
	 $loggedUser = CheckAuth::getLoggedUser();

	 $data = array('authrezed' => 0);	
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($data);
				exit;	
			}
			
		}
			$type = $this->request->getParam('type','invoice');
			$id = $this->request->getParam('id',0);	   
			$to = $this->request->getParam('to');
            $cc = $this->request->getParam('cc');
            $subject = $this->request->getParam('subject');
            $body = $this->request->getParam('body');
            $pdf_attachment = $this->request->getParam('pdf_attachment', 0);
            $attachment_full_path = $this->request->getParam('attachment_full_path', 0);

            $params = array(
                'to' => $to,
                'cc' => $cc,
                'body' => urldecode($body),
                'subject' => urldecode($subject)
            );

            
                if (!empty($pdf_attachment)) {
                    // Create pdf
                    /*$pdfPath = createPdfPath();
                    $destination = $pdfPath['fullDir'] . $viewParam['invoice']['invoice_num'] . '.pdf';
                    wkhtmltopdf($bodyInvoice, $destination);*/
                    $params['attachment'] = $attachment_full_path;
                }

                // Send Email
                $success = EmailNotification::sendEmail($params, '', array(), array('reference_id' => $id, 'type' => 'invoice'));

                if ($success) {
                    
					$data['msg'] = 'Sent Successfully';
	 	 
	 	
                } else {
                    
					$data['msg'] = 'Could not send this email, please try again';
                }

                $data['authrezed'] = 1;
				 echo json_encode($data);
				 exit;
	
	}
	
	
	
	public function getServicesAction(){
	
	header('Content-Type: application/json');
	 $accessToken = $this->request->getParam('access_token',0);
	 $modelIosUser = new Model_IosUser();
     $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
	 
	 $this->iosLoggedUser = $user['id'];
     $modelCompanies = new Model_Companies();	
	 $loggedUser = CheckAuth::getLoggedUser();
	 $data = array('authrezed' => 0);	
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($data);
				exit;	
			}
			
		}
	
	 $servicesObj = new Model_Services();
	 $services = $servicesObj->getServiceAsArray();

	 
	 $data['result'] = $services;
	 $data['authrezed'] = 1;
     echo json_encode($data);
     exit;	 
	 	
	}
	
	public function getPhotosAction(){
	
	 header('Content-Type: application/json');
	 $accessToken = $this->request->getParam('access_token',0);
	 $booking_id = $this->request->getParam('booking_id', 0);
	 $modelIosUser = new Model_IosUser();
     $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
	 
	 $this->iosLoggedUser = $user['id'];
     $modelCompanies = new Model_Companies();	
	 $loggedUser = CheckAuth::getLoggedUser();
	 $data = array('authrezed' => 0);	
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($data);
				exit;	
			}
			
		}
	 $filters = array();
	 $modelImage = new Model_Image();
	 $photos = $modelImage->getAll($booking_id , 'booking');
	 $modelBookingDiscussion = new Model_BookingDiscussion();
	 $result = array();
	 foreach($photos as $key=>$photo){
	   $result[$key]['original_path'] = $photo['compressed_path'];
	   $result[$key]['thumbnail_path'] = $photo['thumbnail_path'];
	   $result[$key]['image_types_id'] = isset($photo['image_types_id']) ? $photo['image_types_id'] : 0;
	   $result[$key]['service_id'] = $photo['service_id']; 
	   $imageDiscussions = $modelBookingDiscussion->getByImageId($photo['image_id'], 'DESC' ,$photo['group_id'], $filters);
	   $Discussions = array();
	   foreach($imageDiscussions as $k=>$imageDiscussion){
	     $Discussions[$k]['discussion_id'] = $imageDiscussion['discussion_id'];
	     $Discussions[$k]['booking_id'] = $imageDiscussion['booking_id'];
	     $Discussions[$k]['user_id'] = $imageDiscussion['user_id'];
	     $Discussions[$k]['user_message'] = $imageDiscussion['user_message'];
	   }
	   $result[$key]['discussions'] = $Discussions;
	 }
	 
	 $data['result'] = $result;
	 $data['booking_id'] = $booking_id;
	 $data['authrezed'] = 1; 
     echo json_encode($data);
     exit;	 
	 	
	}
	
	
	/*public function getRefundsAction(){
	
	  
	 header('Content-Type: application/json');
	 $accessToken = $this->request->getParam('access_token',0);
	 $modelIosUser = new Model_IosUser();
     $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
	 
	 $this->iosLoggedUser = $user['id'];
     $modelCompanies = new Model_Companies();	
	 $loggedUser = CheckAuth::getLoggedUser();
	 $data = array('authrezed' => 0);	
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($data);
				exit;	
			}
			
		}
	
	 $modelRefund = new Model_Refund();
	 $Refunds = $modelRefund->getAll();
	 
	 $data['result'] = $Refunds;
	 $data['authrezed'] = 1;
     echo json_encode($data);
     exit;	 
	
	}*/
	
	
	
		public function addEditRefundAction(){
		header('Content-Type: application/json');
		$accessToken = $this->request->getParam('access_token',0);
		$amount = $this->request->getParam('amount',0);
		$id = $this->request->getParam('id');
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];
		$loggedUser = CheckAuth::getLoggedUser();
		//$this->iosLoggedUser = $iosUserId;
		$data = array('authrezed' => 0);
		
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
        if (empty($loggedUser)) {
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if($authrezed){
				if(isset($id)){
					$result = $this->editRefund();
				   }else{
					$result = $this->addRefund();
				   }
				foreach($result as $key=>$row){
				$data[$key] = $row;
			   }
		   
				//$data['result'] = $result;
				$data['authrezed'] = 1;				
			}
		}
		else{
		   if(isset($id)){
		    $result = $this->editRefund();
		   }else{
		    $result = $this->addRefund();
		   }

		   foreach($result as $key=>$row){
		    $data[$key] = $row;
		   }
		  
            //$data['result'] = $result;
            $data['authrezed'] = 1;
		}
        echo json_encode($data);
        exit;
    }
	
	
	public function editRefund(){
	  
	    CheckAuth::checkPermission(array('refundEdit'));
		

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $receivedDate = $this->request->getParam('received_date');
        $bankCharges = $this->request->getParam('bank_charges');
        $amount = $this->request->getParam('amount');
        $description = $this->request->getParam('description');
        $reference = $this->request->getParam('reference');
        $amountWithheld = $this->request->getParam('amount_withheld');
        $withholdingTax = $this->request->getParam('withholding_tax');
        $isAcknowledgment = $this->request->getParam('is_acknowledgment');
        $paymentTypeId = $this->request->getParam('payment_type_id');
		$loggedUser = CheckAuth::getLoggedUser();
		
		$contractorId = $loggedUser['user_id'];


        //
        // Load Model
        //
        $modelRefund = new Model_Refund();
        $modelBooking = new Model_Booking();
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelPayment = new Model_Payment();

        //
        //Validation
        //
        $refund = $modelRefund->getById($id);
		
		
		
        if ($refund['is_approved']) {
            return array('type' => 'error', 'msg' => "Could not edit Approved Refund, Please change to Unapproved to edit");
        }

        $booking = $modelBooking->getById($refund['booking_id']);
        if (!$booking) {
            return array('type' => 'error', 'msg' => "Invalid Booking");
        }

        if (!$modelBooking->checkIfCanEditBooking($booking['booking_id'])) {
            return array('type' => 'error', 'msg' => "You Don't Have Permission");
        }

        //
        //calculate amount
        //
        
                $data = array(
                    'received_date' => strtotime($receivedDate),
                    'bank_charges' => round($bankCharges, 2),
                    'amount' => round($amount, 2),
                    'description' => $description,
                    'payment_type_id' => $paymentTypeId,
                    'booking_id' => $booking['booking_id'],
                    'customer_id' => $booking['customer_id'],
                    'user_id' => $loggedUser['user_id'],
                    'created' => time(),
                    'reference' => $reference,
                    'amount_withheld' => round($amountWithheld, 2),
                    'withholding_tax' => $withholdingTax,
                    'is_acknowledgment' => $isAcknowledgment,
					'contractor_id' => $contractorId
                );

                $success = $modelRefund->updateById($id, $data);
                if ($success) {
                    $modelBookingInvoice->updateByBookingId($booking['booking_id'], array('invoice_type' => 'open'));
                    return array('type' => 'success', 'msg' => "Refund saved successfully, Please Approved this Refund", 'result' => $this->getBooking(false, array('booking_id' => $booking['booking_id'])));
                } else {
                    return array('type' => 'error', 'msg' => "No Changes in Refund");
                }	
	}
	
	
	public function deleteRefundAction(){
	
	  
	  header('Content-Type: application/json');
		$accessToken = $this->request->getParam('access_token',0);
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];
		$loggedUser = CheckAuth::getLoggedUser();
		//$this->iosLoggedUser = $iosUserId;
		$data = array('authrezed' => 0);
		
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
        if (empty($loggedUser)) {
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if($authrezed){
			    $result = $this->deleteRefund();
				$data['result'] = $result;
				$data['authrezed'] = 1;				
			}
		}
		else{
		    $result = $this->deleteRefund();
            $data['result'] = $result;
            $data['authrezed'] = 1;
		}
        echo json_encode($data);
        exit;
	
	}
	
	
	public function deleteRefund(){
	  
	    CheckAuth::checkPermission(array('refundDelete'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $bookingId = $this->request->getParam('booking_id', 0);

        //
        // Load MODEL
        //
        $modelBooking = new Model_Booking();
        $modelRefund = new Model_Refund();

        //
        //Validation
        //
        if (!$modelBooking->checkIfCanEditBooking($bookingId)) {
            return array('type' => 'error', 'message' => "You Don't Have Permission");
        }

        $refund = $modelRefund->getByIdAndBookingId($id, $bookingId);
        if ($refund['is_approved']) {
            return array('type' => 'error', 'message' => "Could not delete Approved Refund, Please change to Unapproved to delete");
        }
		

        $deleted = 0;
        if ($refund) {
            //delete refund
           $deleted =  $modelRefund->deleteById($id);
        }else{
		 return array('type' => 'error', 'message' => "Refund not exists");
		}

        if($deleted){
		    return array('type' => 'success', 'message' => "the refund deleted successfully");
		}
	
	
	}
	
	public function addRefund(){
	   
	   
	   CheckAuth::checkPermission(array('refundAdd'));


	   
        //
        // get request parameters
        //
        $receivedDate = $this->request->getParam('received_date');
        $bankCharges = $this->request->getParam('bank_charges');
        $amount = $this->request->getParam('amount');
        $description = $this->request->getParam('description');
        $reference = $this->request->getParam('reference');
        $amountWithheld = $this->request->getParam('amount_withheld');
        $withholdingTax = $this->request->getParam('withholding_tax');
        $isAcknowledgment = $this->request->getParam('is_acknowledgment');
        $paymentTypeId = $this->request->getParam('payment_type_id');
        $bookingId = $this->request->getParam('booking_id', 0);
		$loggedUser = CheckAuth::getLoggedUser();
		
		
		$contractorId = $loggedUser['user_id'];

        //
        // Load MODEL
        //
        $modelBooking = new Model_Booking();
        $modelPayment = new Model_Payment();
        $modelRefund = new Model_Refund();
        $modelBookingInvoice = new Model_BookingInvoice();

        //
        //get booking 
        //
        $booking = $modelBooking->getById($bookingId);
        if (!$booking) {
            return array('type' => 'error', 'msg' => "Invalid Booking");
        }

		
		
        if (!$modelBooking->checkIfCanEditBooking($booking['booking_id'])) {
           return array('type' => 'error', 'msg' => "You Don't Have Permission");
            
        }

        $approvedPayment = $modelPayment->getTotalPayment(array('booking_id' => $booking['booking_id'], 'is_approved' => 'yes'));
        if ($approvedPayment <= 0) {
            return array('type' => 'error', 'msg' => "You Con't Add Refund , No Approved Payment");
        }

        $approvedRefund = $modelRefund->getTotalRefund(array('booking_id' => $booking['booking_id'], 'is_approved' => 'yes'));
        if ($approvedRefund >= $approvedPayment) {
            return array('type' => 'error', 'msg' => "fully Refund");
        }

        $allRefund = $modelRefund->getTotalRefund(array('booking_id' => $booking['booking_id'], 'is_approved' => 'all'));
        if ($allRefund >= $approvedPayment) {
           return array('type' => 'error', 'msg' => "fully Refund,Check Unapproved Refund");
        }

        $data = array(
                    'received_date' => strtotime($receivedDate),
                    'bank_charges' => round($bankCharges, 2),
                    'amount' => round($amount, 2),
                    'description' => $description,
                    'payment_type_id' => $paymentTypeId,
                    'booking_id' => $booking['booking_id'],
                    'customer_id' => $booking['customer_id'],
                    'user_id' => $loggedUser['user_id'],
                    'created' => time(),
                    'reference' => $reference,
                    'amount_withheld' => round($amountWithheld, 2),
                    'withholding_tax' => $withholdingTax,
                    'is_acknowledgment' => $isAcknowledgment,
					'contractor_id' => $contractorId
                );

                $success = $modelRefund->insert($data);

                if ($success) {
                    $modelBookingInvoice->updateByBookingId($booking['booking_id'], array('invoice_type' => 'open'));
                    return array('type' => 'success', 'msg' => "Refund saved successfully, Please Approved this Refund" , 'result' => $this->getBooking(false, array('booking_id' => $bookingId)));
                } else {
                    return array('type' => 'error', 'msg' => "No Changes in Refund");
                }
		
	}
	
	
   public function getAllDiscussionAction(){
	  
	    header('Content-Type: application/json');
		$accessToken = $this->request->getParam('access_token',0);
		$type = $this->request->getParam('type','');
		$itemId = $this->request->getParam('id',0);
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];
		$modelCompanies = new Model_Companies();

		
		$loggedUser = CheckAuth::getLoggedUser();
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($data);
				exit;	
			}
		}
	    $Discussions = array();
	    if($type == 'booking'){
		$modelBookingDiscussion = new Model_BookingDiscussion();
		$bookingDiscussions = $modelBookingDiscussion->getByBookingId($itemId, 'DESC');
		$GroupsImageDiscussions = $modelBookingDiscussion->getByBookingId($itemId, 'DESC' , array('groups'=>'groups'));
	    $Discussions = array_merge($bookingDiscussions,$GroupsImageDiscussions);
	    array_multisort($Discussions ,SORT_ASC );
		}else if($type == 'complaint'){
		 $modelComplaintDiscussion = new Model_ComplaintDiscussion(); 
		 $complaintDiscussions = $modelComplaintDiscussion->getByComplaintId($itemId);
         $GroupscomplaintImageDiscussions = $modelComplaintDiscussion->getByComplaintId($itemId, 'created DESC', array('groups' => 'groups'));
         $Discussions = array_merge($complaintDiscussions, $GroupscomplaintImageDiscussions);
         array_multisort($Discussions, SORT_ASC);		
		}
		
		
		$modelImage = new Model_Image();
		$modelContractorInfo = new Model_ContractorInfo();
		$modelUser = new Model_User();
		$results = array();
		foreach($Discussions as $key=>$discussion){	
          $contractorInfo = $modelContractorInfo->getByContractorId($discussion['user_id']);
          $userData = $modelUser->getById($discussion['user_id']);		  
		  if ($discussion['group_id']){
		    
		    $grouped_images = $modelImage->getByGroupIdAndType($discussion['group_id'], $itemId, $type);
		    $results[$key]['images'] =  $grouped_images;
			$results[$key]['thumbnail_path'] = '';
			}else{
			if(isset($discussion['thumbnail_path'])){
			 $results[$key]['thumbnail_path'] = $discussion['thumbnail_path'];
			 }else{
			 $results[$key]['thumbnail_path'] = '';
			 }
			 $results[$key]['images'] = array();
			}
			$results[$key]['user_message'] = $discussion['user_message'];
			$results[$key]['created'] = $discussion['created'];
			$results[$key]['username'] = $userData['username'];
			if($contractorInfo['business_name']){
			  $results[$key]['business_name'] = $contractorInfo['business_name'];
			}else{
			  $results[$key]['business_name'] = '';
			}
		  }

		$loggedUser = CheckAuth::getLoggedUser();  
		$contractorInfo = $modelContractorInfo->getByContractorId($loggedUser['user_id']); 
		
		$data['result'] = $results;
		$data['authrezed'] = 1;
		$data['username'] = $loggedUser['username'];
        $data['business_name'] = $contractorInfo['business_name'];
        echo json_encode($data);
        exit;
	
	}
	
	
	public function addDiscussionAction(){
	  
	    header('Content-Type: application/json');
		$accessToken = $this->request->getParam('access_token',0);
		$itemId = $this->request->getParam('id',0);
		$type = $this->request->getParam('type','');
		$discussion = $this->request->getParam('discussion',0);
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];


		
		$loggedUser = CheckAuth::getLoggedUser();
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($data);
				exit;	
			}
		}
		
		
		$modelBookingDiscussion = new Model_BookingDiscussion();
		$modelComplaintDiscussion = new Model_ComplaintDiscussion();
        $loggedUser = CheckAuth::getLoggedUser();
        $success = 0;
        if ($discussion) {
			if($type == 'booking'){
			  $params = array(
                'booking_id' => $itemId,
                'user_id' => $loggedUser['user_id'],
                'user_message' => $discussion,
                'created' => time()
            );
			
			  $success = $modelBookingDiscussion->insert($params);
			}else if($type == 'complaint'){
			  $params = array(
                'complaint_id' => $itemId,
                'user_id' => $loggedUser['user_id'],
                'user_message' => $discussion,
                'created' => time()
            );
			  $success = $modelComplaintDiscussion->insert($params);
			}
            
			$modelItemImageDiscussion = new Model_ItemImageDiscussion();
				 
			$dataDiscssion = array(
				 'image_id'=>0,
				 'item_id'=>$success,
				 'type'=>$type,
				 'group_id' =>0
				 );
			$modelItemImageDiscussion->insert($dataDiscssion);
			
		}
		
		
		
		if ($success) {
            $data['type'] = 'success';
            $data['msg'] = 'Discussion Added Successfully';
           
        } else {
		    $data['type'] = 'error';
            $data['msg'] = 'Invalid Discussion';
        }

		$data['authrezed'] = 1;
		
        echo json_encode($data);
        exit;	  
	
	
	}
	public function profileAction() {
        //
        //check login
        //
		header('Content-Type: application/json');
		$accessToken = $this->request->getParam('access_token',0);
		$itemId = $this->request->getParam('id',0);
		$type = $this->request->getParam('type','');
		$discussion = $this->request->getParam('discussion',0);
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];


		
		$loggedUser = CheckAuth::getLoggedUser();
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($data);
				exit;	
			}
		}
		
		
       

        $loggedUser = CheckAuth::getLoggedUser();

        $modelUser = new Model_User();
        $user = $modelUser->getById($loggedUser['user_id']);
		
			$user['avatar'] = $_SERVER['HTTP_HOST'].'/uploads/user_pic/thumb_medium/'.$user['avatar']; 
			$data['user'] = $user;

            $modelAuthRole = new Model_AuthRole();
            $role = $modelAuthRole->getById($user['role_id']);
            $data['role'] = $role;


            if ('contractor' == $role['role_name']){

                // booking 
                $modelBookingStatus = new Model_BookingStatus();
                $modelBooking = new Model_Booking();
                /*$BookingStatus = $modelBookingStatus->getAllWithoutPermission();
                $counts = array();
                foreach ($BookingStatus as $status) {
                    $results = array();
                    $total = $modelBooking->getcountBookingsByStatus($status['name'], $loggedUser['user_id']);
                    $results['name'] = $status['name'];
                    $results['booking_status_id'] = $status['booking_status_id'];
                    $results['color'] = $status['color'];
                    $results['total'] = $total;
                    array_push($counts, $results);
                } 
                $contractorInfoObj = new Model_ContractorInfo();
                $contractorInfo = $contractorInfoObj->getByContractorId($loggedUser['user_id']);
                $data['contractorInfo'] = $contractorInfo;
                $data['bookingStatusCounts'] = $counts;*/

				$contractorInfoObj = new Model_ContractorInfo();
                $contractorInfo = $contractorInfoObj->getByContractorId($loggedUser['user_id']);
                $data['contractorInfo'] = $contractorInfo;
				
				if($contractorInfo){
				  
				  $modelAttachment = new Model_Attachment(); 
				  $filter = array('type'=>'insurance','itemid'=>$contractorInfo['contractor_info_id'] , 'item_type'=>'image');
				  $pager = null;
				  //$insuranceAttachments =  $modelAttachment->getAll('a.created desc',$pager,$filter);
				  //$data['contractorInfo']['insurance_attachment'] = $insuranceAttachments;
				  $filter = array('type'=>'licence','itemid'=>$contractorInfo['contractor_info_id'] ,'item_type'=>'image');
			      $licenceAttachments =  $modelAttachment->getAll('a.created desc',$pager,$filter);
				  $data['contractorInfo']['drivers_licence_attachment'] = $licenceAttachments;
				  
				}
				



                if ($contractorInfo) {
                    //get contractorOwner
                    $contractorOwnerObj = new Model_ContractorOwner();
                    $contractorOwner = $contractorOwnerObj->getAll(array('contractor_info_id' => $contractorInfo['contractor_info_id']));

                    $data['contractorOwner'] = $contractorOwner;

                    //get contractorEmployee
                    $contractorEmployeeObj = new Model_ContractorEmployee();
                    $contractorEmployee = $contractorEmployeeObj->getAll(array('contractor_info_id' => $contractorInfo['contractor_info_id']));

                    $data['contractorEmployee'] = $contractorEmployee;

                    //get DeclarationOfChemicals
                    $declarationOfChemicalsObj = new Model_DeclarationOfChemicals();
                    $declarationOfChemicals = $declarationOfChemicalsObj->getByContractorInfoId($contractorInfo['contractor_info_id']);

                    $data['declarationOfChemicals'] = $declarationOfChemicals;

                    //get DeclarationOfEquipment
                    $declarationOfEquipmentObj = new Model_DeclarationOfEquipment();
                    $declarationOfEquipments = $declarationOfEquipmentObj->getByContractorInfoId($contractorInfo['contractor_info_id']);
					$allDeclarationOfEquipments =array();
					$modelAttachment = new Model_Attachment();
					$pager = null;
					foreach($declarationOfEquipments as $key=>$declarationOfEquipment){
					  $filter = array('type'=>'equipment','itemid'=>$declarationOfEquipment['id'],'item_type'=>'image');
					  $attachments =  $modelAttachment->getAll('a.created desc',$pager,$filter);
					  $allDeclarationOfEquipments[$key]['id']  = $declarationOfEquipment['id'];
					  $allDeclarationOfEquipments[$key]['equipment']  = $declarationOfEquipment['equipment'];
					  $allDeclarationOfEquipments[$key]['contractor_info_id']  = $declarationOfEquipment['contractor_info_id'];
					  $allDeclarationOfEquipments[$key]['attachments']  = $attachments;					  
					}
					

                    $data['declarationOfEquipment'] = $allDeclarationOfEquipments;

                    //get DeclarationOfOtherApparatus
                    $declarationOfOtherApparatusObj = new Model_DeclarationOfOtherApparatus();
                    $declarationOfOtherApparatus = $declarationOfOtherApparatusObj->getByContractorInfoId($contractorInfo['contractor_info_id']);

                    $data['declarationOfOtherApparatus'] = $declarationOfOtherApparatus;

                    //get Vehicle
                    $contractorVehicleObj = new Model_ContractorVehicle();
                    $contractorVehicles = $contractorVehicleObj->getByContractorInfoId($contractorInfo['contractor_info_id']);
					$allcontractorVehicle =array();
					$modelAttachment = new Model_Attachment();
					$pager = null;
					foreach($contractorVehicles as $key=>$contractorVehicle){
					  $filter = array('type'=>'vehicle','itemid'=>$contractorVehicle['vehicle_id'],'item_type'=>'image');
					  $attachments =  $modelAttachment->getAll('a.created desc',$pager,$filter);
					  $allcontractorVehicle[$key]['vehicle_id']  = $contractorVehicle['vehicle_id'];
					  $allcontractorVehicle[$key]['registration_number']  = $contractorVehicle['registration_number'];
					  $allcontractorVehicle[$key]['make']  = $contractorVehicle['make'];
					  $allcontractorVehicle[$key]['model']  = $contractorVehicle['model'];
					  $allcontractorVehicle[$key]['colour']  = $contractorVehicle['colour'];
					  $allcontractorVehicle[$key]['contractor_info_id']  = $contractorVehicle['contractor_info_id'];
					  $allcontractorVehicle[$key]['attachments']  = $attachments;					  
					}

                    $data['contractorVehicle'] = $allcontractorVehicle;
					//get Contractor Insurance
                    $contractorInsuranceObj = new Model_ContractorInsurance();
                    $contractorInsurances = $contractorInsuranceObj->getAll(array('contractor_info_id' => $contractorInfo['contractor_info_id']));
					$allcontractorInsurance =array();
					$modelAttachment = new Model_Attachment();
					$pager = null;
					foreach($contractorInsurances as $key=>$contractorInsurance){
					  $filter = array('type'=>'insurance','itemid'=>$contractorInsurance['contractor_insurance_id'],'item_type'=>'image');
					  $attachments =  $modelAttachment->getAll('a.created desc',$pager,$filter);
					  $allcontractorInsurance[$key]['contractor_insurance_id']  = $contractorInsurance['contractor_insurance_id'];
					  $allcontractorInsurance[$key]['insurance_policy_number']  = $contractorInsurance['insurance_policy_number'];
					  $allcontractorInsurance[$key]['insurance_policy_expiry']  = $contractorInsurance['insurance_policy_expiry'];
					  $allcontractorInsurance[$key]['insurance_policy_start']  = $contractorInsurance['insurance_policy_start'];
					  $allcontractorInsurance[$key]['insurance_listed_services_covered']  = $contractorInsurance['insurance_listed_services_covered'];
					  $allcontractorInsurance[$key]['insurance_type']  = $contractorInsurance['insurance_type'];
					  $allcontractorInsurance[$key]['contractor_info_id']  = $contractorInsurance['contractor_info_id'];
					  $allcontractorInsurance[$key]['attachments']  = $attachments;					  
					}

                    $data['contractorInsurance'] = $allcontractorInsurance;
                }
            } else {
                $userInfoObj = new Model_UserInfo();
                $userInfo = $userInfoObj->getByUserId($loggedUser['user_id']);

                $data['userInfo'] = $userInfo;
            }
		$data['authrezed'] = 1;
		
        echo json_encode($data);
        exit;
        
    }
	
	// Contractor Vehicles 
	public function addContractorVehicleAction(){
	
	
	    header('Content-Type: application/json');
		$accessToken = $this->request->getParam('access_token',0);
		$registrationNumber = $this->request->getParam('registration_number');
        $make = $this->request->getParam('make');
        $model = $this->request->getParam('model');
        $colour = $this->request->getParam('colour');
        $attachCount = $this->request->getParam('attachCount',0);
		
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];


		
		$loggedUser = CheckAuth::getLoggedUser();
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($data);
				exit;	
			}
		}
		
		
        $loggedUser = CheckAuth::getLoggedUser();
		$contractorInfoObj = new Model_ContractorInfo();
        $contractorInfo = $contractorInfoObj->getByContractorId($loggedUser['user_id']);
		$contractorVehicleObj = new Model_ContractorVehicle();
		$attachmentObj = new Model_Attachment();
        $vehicleAttachmentObj = new Model_VehicleAttachment();
                $params = array(
                    'registration_number' => $registrationNumber,
                    'make' => $make,
                    'model' => $model,
                    'colour' => $colour,
                    'contractor_info_id' => $contractorInfo['contractor_info_id']
                );

        $success = $contractorVehicleObj->insert($params);
		$id= 0;
		if($attachCount){
		  for($i=0; $i<$attachCount; $i++) {
		    $source  = $_FILES['vehicleFile'.$i]['tmp_name'];
		    $extension = $this->request->getParam('ext'.$i);
            $fileInfo = pathinfo($source);
			$ext = $extension;
			
			$dir = get_config('attachment') . '/';
            $subdir = date('Y/m/d/');
            $fullDir = $dir . $subdir;
            if (!is_dir($fullDir)) {
                mkdir($fullDir, 0777, true);
            }
			
			$fileName = $success.'_'.$i.'.'. $ext;
			$image_saved = copy($source, $fullDir . $fileName);
			$size = filesize($fullDir . $fileName);
			$type = mime_content_type($fullDir . $fileName);
			$typeParts = explode("/",$type);
            if($typeParts[0] == 'image'){
				$thumbName = $success.'_thumbnail_'.$i.'.'. $ext;
				ImageMagick::create_thumbnail($source, $fullDir . $thumbName, 78, 64);
			}else{
				$thumbName = $success.'_'.$i.'.jpg';
				ImageMagick::convertFile($source.'[0]', $fullDir . $thumbName);
			}
			$attach = array(
				'created_by'=>$loggedUser['user_id'] 
				,'created'=>time()
				,'size' => $size
				,'type'=>$type
				,'path' => $fullDir . $fileName,
                'file_name' => $fileName,
				'thumbnail_file' => $fullDir . $thumbName
			);
			
			$id = $attachmentObj->insert($attach);
			$vehicleAttachmentObj->insert(array('vehicle_id'=>$success,'attachment_id'=>$id));
			if ($image_saved) {
				if (file_exists($source)) {
					unlink($source);
				}               
			}
			
		  }
		}
		
		$vehicle = $contractorVehicleObj->getById($success);
		$modelAttachment = new Model_Attachment();
		$filter = array('type'=>'vehicle','itemid'=>$success,'item_type'=>'image');
	    $attachments =  $modelAttachment->getAll('a.created desc',$pager,$filter);
		$vehicle['attachments'] = $attachments;
		
	    if($success || $id){
		  $data['msg'] = 'Saved successfully';	 
		  $data['type'] = 'success';	 
		  $data['result'] = $vehicle;	 
		}else{
		  $data['msg'] = 'No Changes in Contractor Vehicle';
		  $data['type'] = 'error';	
		}
		
		 $data['authrezed'] = 1;
		 echo json_encode($data);
		 exit;
	
	}
	
	
		public function deleteContractorVehicleAction(){
	
	
	    header('Content-Type: application/json');
		$accessToken = $this->request->getParam('access_token',0);
		$id = $this->request->getParam('id', 0);
		
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];


		
		$loggedUser = CheckAuth::getLoggedUser();
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			$data['type'] = 'error';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($data);
				exit;	
			}
		}
		
		
        $loggedUser = CheckAuth::getLoggedUser();
		$contractorVehicleObj = new Model_ContractorVehicle();
		$modelAttachment = new Model_Attachment();
		$contractorVehicle = $contractorVehicleObj->getById($id);
		if($contractorVehicle) {
		    $filter = array('type'=>'vehicle','itemid'=>$id);        
					$pager = null;
					$attachments =  $modelAttachment->getAll('a.created desc',$pager, $filter);
					if($attachments){
					  foreach($attachments as $attachment){
						$modelAttachment->updateById($attachment['attachment_id'] , array('is_deleted' => '1'));
					  }
					}
					
            $contractorVehicleObj->deleteById($id);
			$data['msg'] = 'Vehicle deleted successfully';
			$data['type'] = 'success';
        }else{
		   $data['msg'] = 'Vehicle not found';
		   $data['type'] = 'error';
		}
		
		 $data['authrezed'] = 1;
		 echo json_encode($data);
		 exit;
	
	}
	
	
	public function addEditContractorOwnerEmployeeAction(){
	  
	    header('Content-Type: application/json');
		$accessToken = $this->request->getParam('access_token',0);
		$id = $this->request->getParam('id');
		$type = $this->request->getParam('type');
		$name = $this->request->getParam('name');
        $email1 = $this->request->getParam('email1');
        $email2 = $this->request->getParam('email2');
        $email3 = $this->request->getParam('email3');
        $mobile1 = $this->request->getParam('mobile1');
        $mobile2 = $this->request->getParam('mobile2');
        $mobile3 = $this->request->getParam('mobile3');
        $phone1 = $this->request->getParam('phone1');
        $phone2 = $this->request->getParam('phone2');
        $phone3 = $this->request->getParam('phone3');
        $fax = $this->request->getParam('fax');
        $emergencyPhone = $this->request->getParam('emergency_phone');
        $unitLotNumber = $this->request->getParam('unit_lot_number');
        $streetNumber = $this->request->getParam('street_number');
        $streetAddress = $this->request->getParam('street_address');
        $suburb = $this->request->getParam('suburb');
        $state = $this->request->getParam('state');
        $postcode = $this->request->getParam('postcode');
        $po_box = $this->request->getParam('po_box');
        $cityId = $this->request->getParam('city_id');
        $countryId = $this->request->getParam('country_id');
		
		if (get_config('remove_white_spacing')) {
            $mobile1 = preparer_number($mobile1);
            $mobile2 = preparer_number($mobile2);
            $mobile3 = preparer_number($mobile3);
            $phone1 = preparer_number($phone1);
            $phone2 = preparer_number($phone2);
            $phone3 = preparer_number($phone3);
        }
		
		
		
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];


		
		$loggedUser = CheckAuth::getLoggedUser();
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			$data['type'] = 'error';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($data);
				exit;	
			}
		}
		
		
        $loggedUser = CheckAuth::getLoggedUser();
		$contractorInfoObj = new Model_ContractorInfo();
		$contractorEmployeeObj = new Model_ContractorEmployee();
        $modelContractorOwner = new Model_ContractorOwner();
		
		$contractorInfo = $contractorInfoObj->getByContractorId($loggedUser['user_id']);
		
		
              
                $params = array(
                    'name' => $name,
                    'contractor_info_id' => $contractorInfo['contractor_info_id'],
                    'created' => time(),
                    'city_id' => $cityId,
                    'email1' => $email1,
                    'email2' => $email2,
                    'email3' => $email3,
                    'mobile1' => $mobile1,
                    'mobile2' => $mobile2,
                    'mobile3' => $mobile3,
                    'phone1' => $phone1,
                    'phone2' => $phone2,
                    'phone3' => $phone3,
                    'fax' => $fax,
                    'emergency_phone' => $emergencyPhone,
                    'unit_lot_number' => $unitLotNumber,
                    'street_number' => $streetNumber,
                    'street_address' => $streetAddress,
                    'suburb' => $suburb,
                    'state' => $state,
                    'postcode' => $postcode,
                    'po_box' => $po_box
                );

                if($id && $type == 'owner'){
				   $success = $modelContractorOwner->updateById($id, $params);
				   $data['result'] = $modelContractorOwner->getById($id);
		
				 }else if($id && $type == 'employee'){
				   $success = $contractorEmployeeObj->updateById($id, $params);
				   $data['result'] = $contractorEmployeeObj->getById($id);

				 } else if($type == 'owner' && !($id)){
				   $success = $modelContractorOwner->insert($params);

				   
				   $data['result'] = $modelContractorOwner->getById($success);
		
				 } else if($type == 'employee' && !($id)){

				   $success = $contractorEmployeeObj->insert($params);
				   $data['result'] = $contractorEmployeeObj->getById($success);
				   
				 }
                
				
				if($success){
				  $data['msg'] = 'Saved successfully';
				  $data['type'] = 'success';
				}else{
				  $data['msg'] = 'No Changes in Contractor Owner';
				  $data['type'] = 'error';
				}

		
		 $data['authrezed'] = 1;
		 echo json_encode($data);
		 exit;
	
	
	}
	
	
	public function deleteContractorOwnerAction(){
	  
	    header('Content-Type: application/json');
	    $accessToken = $this->request->getParam('access_token',0);
		$id = $this->request->getParam('id', 0);
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];


		
		$loggedUser = CheckAuth::getLoggedUser();
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			$data['type'] = 'error';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($data);
				exit;	
			}
		}
		
		
        $loggedUser = CheckAuth::getLoggedUser();
		$contractorOwnerObj = new Model_ContractorOwner();
		$contractorOwner = $contractorOwnerObj->getById($id);
		
		if($contractorOwner){
		  $contractorOwnerObj->updateById($id, array('is_deleted' => 1));
		  $data['msg'] = 'Owner deleted successfully';
		  $data['type'] = 'success';
		}else{
		  $data['msg'] = 'Owner not found';
		  $data['type'] = 'error';
		}



		
		 $data['authrezed'] = 1;
		 echo json_encode($data);
		 exit;
	
	
	}
	
	
	public function contractorOwnerEmployeePhotoUploadAction(){
	
	    header('Content-Type: application/json');
	    $accessToken = $this->request->getParam('access_token',0);
		$id = $this->request->getParam('id');
		$type = $this->request->getParam('type');
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];


		
		$loggedUser = CheckAuth::getLoggedUser();
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			$data['type'] = 'error';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($data);
				exit;	
			}
		}
		
		
        $loggedUser = CheckAuth::getLoggedUser();
		
		$source  = $_FILES['avatar']['tmp_name'];
		$extension = $this->request->getParam('extension','png');
        $imageInfo = pathinfo($source);
        $ext = $extension;
		
		$dir = get_config('avatar') . '/';
        $subdir = date('Y/m/d/');
        $fullDir = $dir . $subdir;

         if (!is_dir($fullDir)) {
            mkdir($fullDir, 0777, true);
        }

        $orginal_avatar_owner = "orginal_avatar_owner_{$id}.{$ext}";
        $small_avatar_owner = "small_avatar_owner_{$id}.{$ext}";
        $large_avatar_owner = "large_avatar_owner_{$id}.{$ext}";

        $params = array(
                    'small_avatar_path' => $subdir . $small_avatar_owner,
                    'large_avatar_path' => $subdir . $large_avatar_owner
                );

		if($type == 'owner'){
		  $modelContractorOwner = new Model_ContractorOwner();
		  $modelContractorOwner->updateById($id, $params);
		}else if($type == 'employee'){
		  $modelContractorEmployee = new Model_ContractorEmployee();
		  $modelContractorEmployee->updateById($id, $params);
		}		
        

                
                //save image to database and filesystem here
        $file_saved = copy($source, $fullDir . $orginal_avatar_owner);
        ImageMagick::create_thumbnail($source, $fullDir . $small_avatar_owner, 73, 90);
        ImageMagick::create_thumbnail($source, $fullDir . $large_avatar_owner, 150, 185);
            if ($file_saved) {
                if (file_exists($source)) {
                        unlink($source);
                    }
                    $data['msg'] = 'Saved successfully'; 
					$data['type'] = 'success';
                }else{
				  $data['msg'] = 'There is no file'; 
				  $data['type'] = 'error';
				}
		



		
		 $data['authrezed'] = 1;
		 echo json_encode($data);
		 exit;
	
	}
	
	
	public function deleteContractorEmployeeAction(){
	  
	    header('Content-Type: application/json');
	    $accessToken = $this->request->getParam('access_token',0);
		$id = $this->request->getParam('id', 0);
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];


		
		$loggedUser = CheckAuth::getLoggedUser();
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			$data['type'] = 'error';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($data);
				exit;	
			}
		}
		
		
        $loggedUser = CheckAuth::getLoggedUser();
		$contractorEmployeeObj = new Model_ContractorEmployee();
		$contractorEmployee = $contractorEmployeeObj->getById($id);
		
		if($contractorEmployee){
		  $contractorEmployeeObj->updateById($id, array('is_deleted' => 1));
		  $data['msg'] = 'Employee deleted successfully';
		  $data['type'] = 'success';
		}else{
		  $data['msg'] = 'Employee not found';
		  $data['type'] = 'error';
		}



		
		 $data['authrezed'] = 1;
		 echo json_encode($data);
		 exit;
	
	
	}
	
	
	public function addContractorEquipmentAction(){
	  	  
	    header('Content-Type: application/json');
	    $accessToken = $this->request->getParam('access_token',0);
		$equipment = $this->request->getParam('equipment');
		$attachCount = $this->request->getParam('attachCount');
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];


		
		$loggedUser = CheckAuth::getLoggedUser();
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			$data['type'] = 'error';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($data);
				exit;	
			}
		}
		
		
        $loggedUser = CheckAuth::getLoggedUser();
		$declarationOfEquipmentObj = new Model_DeclarationOfEquipment();
		$contractorInfoObj = new Model_ContractorInfo();
		$attachmentObj = new Model_Attachment();
		$declarationOfEquipmentAttachmentObj = new Model_DeclarationOfEquipmentAttachment();
		$contractorInfo = $contractorInfoObj->getByContractorId($loggedUser['user_id']);
                $params = array(
                    'equipment' => $equipment,
                    'contractor_info_id' => $contractorInfo['contractor_info_id']
                );
				
        $success = $declarationOfEquipmentObj->insert($params);
		$id = 0;
		if($attachCount){
		  for($i=0; $i<$attachCount; $i++) {
		    $source  = $_FILES['equipmentFile'.$i]['tmp_name'];
		    $extension = $this->request->getParam('ext'.$i);
            $fileInfo = pathinfo($source);
			$ext = $extension;
			$dir = get_config('attachment') . '/';
            $subdir = date('Y/m/d/');
            $fullDir = $dir . $subdir;
            if (!is_dir($fullDir)) {
                mkdir($fullDir, 0777, true);
            }
			
			$fileName = $success.'_'.$i .'.'. $ext;
			$image_saved = copy($source, $fullDir . $fileName);
			$size = filesize($fullDir . $fileName);
			
			
			$type = mime_content_type($fullDir . $fileName);
			
	
			
			$typeParts = explode("/",$type);
            if($typeParts[0] == 'image'){
				$thumbName = $success.'_thumbnail_'.$i.'.'. $ext;
				ImageMagick::create_thumbnail($source, $fullDir . $thumbName, 78, 64);
			}else{
				$thumbName = $success.'_'.$i.'.jpg';
				ImageMagick::convertFile($source.'[0]', $fullDir . $thumbName);
			}
			$Attchparams = array(
				'created_by'=>$loggedUser['user_id'] 
				,'created'=>time()
				,'size' => $size
				,'type'=>$type
				,'path' => $fullDir . $fileName,
                'file_name' => $fileName,
				'thumbnail_file' => $fullDir . $thumbName
			);
			
			$id = $attachmentObj->insert($Attchparams);
			$declarationOfEquipmentAttachmentObj->insert(array('equipment_id'=>$success,'attachment_id'=>$id));
			if ($image_saved) {
				if (file_exists($source)) {
					unlink($source);
				}               
			}			
		  }
		}
		
		$equipment = $declarationOfEquipmentObj->getById($success);
		$modelAttachment = new Model_Attachment();
		$filter = array('type'=>'equipment','itemid'=>$success,'item_type'=>'image');
		$pager = null;
	    $attachments =  $modelAttachment->getAll('a.created desc',$pager,$filter);
		$equipment['attachments'] = $attachments;
        if($success || $id){
		  $data['msg'] = 'Saved successfully';
		  $data['type'] = 'success';
		  $data['result'] = $equipment;
		}else{
		  $data['msg'] = 'No Changes in Declaration Of Equipment';
		  $data['type'] = 'error';
		}

		 $data['authrezed'] = 1;
		 echo json_encode($data);
		 exit;
	  
	
	}
	
	
	public function deleteContractorEquipmentAction(){
	  
	    header('Content-Type: application/json');
	    $accessToken = $this->request->getParam('access_token',0);
		$id = $this->request->getParam('id', 0);
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];


		
		$loggedUser = CheckAuth::getLoggedUser();
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			$data['type'] = 'error';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($data);
				exit;	
			}
		}
		
		
        $loggedUser = CheckAuth::getLoggedUser();
		$declarationOfEquipmentObj = new Model_DeclarationOfEquipment();
		$modelAttachment = new Model_Attachment();
		$declarationOfEquipment = $declarationOfEquipmentObj->getById($id);
		
		if($declarationOfEquipment){
		  $filter = array('type'=>'equipment','itemid'=>$id);    
            $pager = null;			
		    $attachments =  $modelAttachment->getAll('a.created desc',$pager, $filter);
			if($attachments ){
			  foreach($attachments as $attachment){
			    $modelAttachment->updateById($attachment['attachment_id'] , array('is_deleted' => '1'));
			  }
			}
			
		  $declarationOfEquipmentObj->deleteById($id);
		  $data['msg'] = 'Equipment deleted successfully';
		  $data['type'] = 'success';
		}else{
		  $data['msg'] = 'Equipment not found';
		  $data['type'] = 'error';
		}
		
		 $data['authrezed'] = 1;
		 echo json_encode($data);
		 exit;
	
	
	}
	
	
	public function addContractorChemicalsAction(){
	  	  
	    header('Content-Type: application/json');
	    $accessToken = $this->request->getParam('access_token',0);
		$chemicals = $this->request->getParam('chemicals');
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];


		
		$loggedUser = CheckAuth::getLoggedUser();
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			$data['type'] = 'error';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($data);
				exit;	
			}
		}
		
		
		
        $loggedUser = CheckAuth::getLoggedUser();
		$contractorInfoObj = new Model_ContractorInfo();
		$contractorInfo = $contractorInfoObj->getByContractorId($loggedUser['user_id']);
        $declarationOfChemicalsObj = new Model_DeclarationOfChemicals();
                $params = array(
                    'chemicals' => $chemicals,
                    'contractor_info_id' => $contractorInfo['contractor_info_id']
                );

        $success = $declarationOfChemicalsObj->insert($params);
		
		$Chemical = $declarationOfChemicalsObj->getById($success);
        
		if($success){
		  $data['msg'] = 'Saved successfully';
		  $data['type'] = 'success';
		  $data['result'] = $Chemical;
		}else{
		  $data['msg'] = 'No Changes in Declaration Of Chemicals';
		  $data['type'] = 'error';
		}

		 $data['authrezed'] = 1;
		 echo json_encode($data);
		 exit;
	  
	
	}
	
	
	public function deleteContractorChemicalsAction(){
	  
	    header('Content-Type: application/json');
	    $accessToken = $this->request->getParam('access_token',0);
		$id = $this->request->getParam('id', 0);
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];


		
		$loggedUser = CheckAuth::getLoggedUser();
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			$data['type'] = 'error';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($data);
				exit;	
			}
		}
		
		
        $loggedUser = CheckAuth::getLoggedUser();
		$declarationOfChemicalsObj = new Model_DeclarationOfChemicals();
		$declarationOfChemicals = $declarationOfChemicalsObj->getById($id);
		
		if($declarationOfChemicals){
		  $declarationOfChemicalsObj->deleteById($id);
		  $data['msg'] = 'Chemicals deleted successfully';
		  $data['type'] = 'success';
		}else{
		  $data['msg'] = 'Chemicals not found';
		  $data['type'] = 'error';
		}
		
		 $data['authrezed'] = 1;
		 echo json_encode($data);
		 exit;
	
	
	}	public function addContractorApparatusAction(){
	  	  
	    header('Content-Type: application/json');
	    $accessToken = $this->request->getParam('access_token',0);
		$other_apparatus = $this->request->getParam('other_apparatus');
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];


		
		$loggedUser = CheckAuth::getLoggedUser();
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			$data['type'] = 'error';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($data);
				exit;	
			}
		}
		
		
        $loggedUser = CheckAuth::getLoggedUser();
		$contractorInfoObj = new Model_ContractorInfo();
		$contractorInfo = $contractorInfoObj->getByContractorId($loggedUser['user_id']);
        $declarationOfOtherApparatusObj = new Model_DeclarationOfOtherApparatus();
                $params = array(
                    'other_apparatus' => $other_apparatus,
                    'contractor_info_id' => $contractorInfo['contractor_info_id']
                );

        $success = $declarationOfOtherApparatusObj->insert($params);
		$Apparatus = $declarationOfOtherApparatusObj->getById($success);
        
		if($success){
		  $data['msg'] = 'Saved successfully';
		  $data['type']  = 'success';
		  $data['result'] = $Apparatus;
		}else{
		  $data['msg'] = 'No Changes in Declaration Of Other Apparatus';
		  $data['type']  = 'error';
		}

		 $data['authrezed'] = 1;
		 echo json_encode($data);
		 exit;
	  
	
	}
	
	
	public function deleteContractorApparatusAction(){
	  
	    header('Content-Type: application/json');
	    $accessToken = $this->request->getParam('access_token',0);
		$id = $this->request->getParam('id', 0);
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];


		
		$loggedUser = CheckAuth::getLoggedUser();
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			$data['type'] = 'error';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($data);
				exit;	
			}
		}
		
		
        $loggedUser = CheckAuth::getLoggedUser();
		$declarationOfOtherApparatusObj = new Model_DeclarationOfOtherApparatus();
		$declarationOfOtherApparatus = $declarationOfOtherApparatusObj->getById($id);
		
		if($declarationOfOtherApparatus){
		  $declarationOfOtherApparatusObj->deleteById($id);
		  $data['msg'] = 'Apparatus deleted successfully';
		  $data['type'] = 'success';
		}else{
		  $data['msg'] = 'Apparatus not found';
		  $data['type'] = 'error';
		}
		
		 $data['authrezed'] = 1;
		 echo json_encode($data);
		 exit;
	
	
	}
	
	
	public function changeContractorPasswordAction(){
	  
	    header('Content-Type: application/json');
	    $accessToken = $this->request->getParam('access_token',0);
        $new_password = $this->request->getParam('new_password');
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];


		
		$loggedUser = CheckAuth::getLoggedUser();
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			$data['type'] = 'error';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($data);
				exit;	
			}
		}
		
		
        $loggedUser = CheckAuth::getLoggedUser();
		 $params = array(
                    'password' => sha1($new_password)
                );
		$modelUser = new Model_User();		
        $success = $modelUser->updateById($loggedUser['user_id'], $params);
		if($success){
		 $data['msg'] = 'Your password changed successfully';
		 $data['type'] = 'success';
		}else{
		 $data['msg'] = 'Could not change your password';
		 $data['type'] = 'error';
		}
		
		
		 $data['authrezed'] = 1;
		 echo json_encode($data);
		 exit;
	
	}
	
	public function changeContractorEmailAction(){
	  
	    header('Content-Type: application/json');
	    $accessToken = $this->request->getParam('access_token',0);
        $old_email = $this->request->getParam('old_email');
        $new_email = $this->request->getParam('new_email');
        $confirm_new_email = $this->request->getParam('confirm_new_email');
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];


		
		$loggedUser = CheckAuth::getLoggedUser();
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			$data['type'] = 'error';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($data);
				exit;	
			}
		}
		
		
        $loggedUser = CheckAuth::getLoggedUser();
		$modelUser = new Model_User();
		$old_user_email = $modelUser->getByEmail($old_email);
		if ($loggedUser['user_id'] == $old_user_email['user_id']) {
            if ($new_email == $confirm_new_email) {
			  $modelUser = new Model_User();

                        $dataEmail = array(
                            'temp_email' => $new_email
                        );
                        $modelUser->updateById($user['user_id'], $dataEmail);
                        $activation_code = sha1(uniqid());
                        $params = array(
                            'code' => $activation_code,
                            'created' => time(),
                            'type' => 'change_email',
                            'ip_address' => $_SERVER['REMOTE_ADDR'],
                            'user_id' => $user['user_id']
                        );
                        $modelCode = new Model_Code();
                        $modelCode->insert($params);

                        $activation_link = $this->router->assemble(array('code' => $activation_code, 'id' => $user['user_id']), 'changeEmailStep2');

                        $template_params = array(
                            '{username}' => ucwords($user['username']),
                            '{new_email}' => $new_email,
                            '{activation_link}' => '<a href="' . $activation_link . '">' . $activation_link . '</a>'
                        );

                        $sucess = EmailNotification::sendEmail(array('to' => $user['email1']), 'change_email', $template_params);
						
						if($success){
						  $data['msg'] = 'Please Check Your Email address and Complete the Instruction';
						  $data['type'] = 'success';
						}else{
						  $data['msg'] = 'Could Not Send change Email';
						  $data['type'] = 'error';
						}
			}
		}			
		
		
		
		
		 $data['authrezed'] = 1;
		 echo json_encode($data);
		 exit;
	
	}
	
	
	public function uploadContractorPhotoAction(){
	  
	  
	    header('Content-Type: application/json');
	    $accessToken = $this->request->getParam('access_token',0);
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];


		
		$loggedUser = CheckAuth::getLoggedUser();
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			$data['type'] = 'error';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($data);
				exit;	
			}
		}
		
		
        $loggedUser = CheckAuth::getLoggedUser();
		$id = $loggedUser['user_id'];
		$source  = $_FILES['avatar']['tmp_name'];
	
		
		$extension = $this->request->getParam('extension','png');
        $imageInfo = pathinfo($source);
        $ext = $extension;
		
		$dir = get_config('user_picture');
        $dir2 = get_config('user_picture_medium');
        $dir3 = get_config('user_picture_small');

        if (!is_dir($dir)) {
             mkdir($dir, 0777, true);
        }
        if (!is_dir($dir2)) {
            mkdir($dir2, 0777, true);
         }
         if (!is_dir($dir3)) {
            mkdir($dir3, 0777, true);
         }
		 
		 $original_path = time() . "_" . $id . '.' . $ext;
		 
		 //echo 'source: '.$source;
		 //echo 'path: '.$original_path;
		 //exit;
		 
         $image_saved = copy($source, $dir . $original_path);
		 ImageMagick::create_thumbnail($source, $dir2 . $original_path, 210, 210); 
		 ImageMagick::create_thumbnail($source, $dir3 . $original_path, 50, 50);

         $modeluser = new Model_User();
         if ($image_saved) {
             $param = array('avatar' => $original_path);
			 $success = $modeluser->updateById($id, $param);
                if ($success) {
				     $data['msg'] = 'Saved Successfully';
					 $data['type'] = 'success';
                }else{
				     $data['msg'] = 'Could not change photo';
					 $data['type'] = 'error';
				}
            }
		
		
		 $data['authrezed'] = 1;
		 echo json_encode($data);
		 exit;
	  
	
	
	}
	
	
	public function changeContractorInfoAction(){
	  
	    header('Content-Type: application/json');
	    $accessToken = $this->request->getParam('access_token',0);
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];
		$username = $this->request->getParam('username');
		$business_name = $this->request->getParam('business_name');
        $acn = $this->request->getParam('acn');
        $abn = $this->request->getParam('abn');
        $tfn = $this->request->getParam('tfn');
        $gst = $this->request->getParam('gst');
        $gst_date_registered = $this->request->getParam('gst_date_registered');
        $insurance_policy_number = $this->request->getParam('insurance_policy_number');
        $insurance_policy_start = $this->request->getParam('insurance_policy_start');
        $insurance_policy_expiry = $this->request->getParam('insurance_policy_expiry');
        $insurance_type = $this->request->getParam('insurance_type');
        $insurance_listed_services_covered = $this->request->getParam('insurance_listed_services_covered');
        $drivers_licence_number = $this->request->getParam('drivers_licence_number');
        $drivers_licence_expiry = $this->request->getParam('drivers_licence_expiry');
        $driver_licence_type = $this->request->getParam('driver_licence_type');
        $driver_licence_type = $this->request->getParam('driver_licence_type');
        $bank_name = $this->request->getParam('bank_name');
        $account_name = $this->request->getParam('account_name');
        $bsb = $this->request->getParam('bsb');
        $account_number = $this->request->getParam('account_number');
		$insurance_file_count = $this->request->getParam('insurance_file_count',0);
		$drivers_file_count = $this->request->getParam('drivers_file_count',0);

		$loggedUser = CheckAuth::getLoggedUser();
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			$data['type'] = 'error';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($data);
				exit;	
			}
		}
		
		
        $loggedUser = CheckAuth::getLoggedUser();
		$contractorInfoObj = new Model_ContractorInfo();
		$modelContractorInfoAttachment = new Model_ContractorInfoAttachment();
		$attachmentObj = new Model_Attachment();
        $contractorInfo = $contractorInfoObj->getByContractorId($loggedUser['user_id']);
		
		
		
		$params = array(
                    'business_name' => $business_name,
                    'acn' => $acn,
                    'abn' => $abn,
                    'tfn' => $tfn,
                    'gst' => $gst,
                    'gst_date_registered' => strtotime($gst_date_registered),
                    'insurance_policy_number' => $insurance_policy_number,
                    'insurance_type' => $insurance_type,
                    'insurance_policy_start' => strtotime($insurance_policy_start),
                    'insurance_policy_expiry' => strtotime($insurance_policy_expiry),
                    'insurance_listed_services_covered' => $insurance_listed_services_covered,
                    'drivers_licence_number' => $drivers_licence_number,
                    'driver_licence_type' => $driver_licence_type,
                    'drivers_licence_expiry' => strtotime($drivers_licence_expiry),
					'bank_name' => $bank_name,
					'account_name' => $account_name,
					'bsb' => $bsb,
					'account_number' => $account_number,
                );

                if (!$contractorInfo) {
                    $data['contractor_id'] = $loggedUser['user_id'];
                    $success = $contractorInfoObj->insert($params);
					$contractorInfo = $contractorInfoObj->getByContractorId($loggedUser['user_id']);
                }else{
                    $success = $contractorInfoObj->updateById($contractorInfo['contractor_info_id'], $params);
					$modelUser = new Model_User();
					$userParams = array('username' => $username);
		            $updateUser = $modelUser->updateById($loggedUser['user_id'], $userParams);
                }
				
			   
				 $insurance_files = 0;
				if($insurance_file_count){
				  $Attachments = $contractorInfoObj->getAllAttachmentByTypeAndId($contractorInfo['contractor_info_id'] , 'insurance');
				  $counter = count($Attachments);
				  $dir = get_config('attachment') . '/';
                           $subdir = date('Y/m/d/');
                           $fullDir = $dir . $subdir;
                           if (!is_dir($fullDir)) {
                              mkdir($fullDir, 0777, true);
                          }
				  for($i = 0 ; $i<$insurance_file_count ; $i++){
				    $counter = $counter + 1;
					$source  = $_FILES['insuranceFile'.$i]['tmp_name'];
		            $ext = $this->request->getParam('insuranceExt'.$i,'png');
					$fileName = $contractorInfo['contractor_info_id'].'_'.$counter.'_'.'insurance'.'.'. $ext;
				    $image_saved = copy($source, $fullDir . $fileName);
					$size = filesize($fullDir . $fileName);
			        $type = mime_content_type($fullDir . $fileName);
			        $typeParts = explode("/",$type);
					if($typeParts[0] == 'image'){
						    $thumbName = $contractorInfo['contractor_info_id'].'_'.$counter.'_'.'insurance_thumbnail'.'.'. $ext;
						    ImageMagick::create_thumbnail($source, $fullDir . $thumbName, 78, 64);
					}else{
						   $thumbName = $contractorInfo['contractor_info_id'].'_'.$counter.'_'.'insurance'.'.jpg';
						   ImageMagick::convertFile($source.'[0]', $fullDir . $thumbName);
					}
					
					$data = array(
							 'created_by'=>$loggedUser['user_id'] 
							 ,'created'=>time()
							 ,'size' => $size
							 ,'type' =>$type
							 ,'path' => $fullDir . $fileName,
                             'file_name' => $fileName,
						     'thumbnail_file' => $fullDir . $thumbName
						 );
					$Attachid = $attachmentObj->insert($data);
					$insurance_files = $Attachid;
					$contractorInfoAttachmentId = $modelContractorInfoAttachment->insert(array('contractor_info_id'=>$contractorInfo['contractor_info_id'],'attachment_id'=>$Attachid , 'type' =>'insurance'));
                    					
				  }
				
				}
				
				$driver_files = 0;
				
				if($drivers_file_count){
				  $Attachments = $contractorInfoObj->getAllAttachmentByTypeAndId($contractorInfo['contractor_info_id'] , 'licence');
				  $counter = count($Attachments);
				  $dir = get_config('attachment') . '/';
                           $subdir = date('Y/m/d/');
                           $fullDir = $dir . $subdir;
                           if (!is_dir($fullDir)) {
                              mkdir($fullDir, 0777, true);
                          }
				  for($i = 0 ; $i< $drivers_file_count ; $i++){
				    $counter = $counter + 1;
					$source  = $_FILES['driverFile'.$i]['tmp_name'];					
		            $ext = $this->request->getParam('driverExt'.$i,'png');
					$fileName = $contractorInfo['contractor_info_id'].'_'.$counter.'_'.'licence'.'.'. $ext;
				    $image_saved = copy($source, $fullDir . $fileName);
					$size = filesize($fullDir . $fileName);
			        $type = mime_content_type($fullDir . $fileName);
			        $typeParts = explode("/",$type);
					if($typeParts[0] == 'image'){
						    $thumbName = $contractorInfo['contractor_info_id'].'_'.$counter.'_'.'licence_thumbnail'.'.'. $ext;
						    ImageMagick::create_thumbnail($source, $fullDir . $thumbName, 78, 64);
					}else{
						   $thumbName = $contractorInfo['contractor_info_id'].'_'.$counter.'_'.'licence'.'.jpg';
						   ImageMagick::convertFile($source.'[0]', $fullDir . $thumbName);
					}
					
					$data = array(
							 'created_by'=>$loggedUser['user_id'] 
							 ,'created'=>time()
							 ,'size' => $size
							 ,'type' =>$type
							 ,'path' => $fullDir . $fileName,
                             'file_name' => $fileName,
						     'thumbnail_file' => $fullDir . $thumbName
						 );
					$Attachid = $attachmentObj->insert($data);
					$driver_files = $Attachid;
					$contractorInfoAttachmentId = $modelContractorInfoAttachment->insert(array('contractor_info_id'=>$contractorInfo['contractor_info_id'],'attachment_id'=>$Attachid , 'type' =>'licence'));
                    					
				  }
				
				}
				
                $contractorInfo = $contractorInfoObj->getByContractorId($loggedUser['user_id']);
                $data['result'] = $contractorInfo;
				
				if($contractorInfo){			  
				  $modelAttachment = new Model_Attachment(); 
				  $filter = array('type'=>'insurance','itemid'=>$contractorInfo['contractor_info_id'] , 'item_type'=>'image');
				  $pager = null;
				  $insuranceAttachments =  $modelAttachment->getAll('a.created desc',$pager,$filter);
				  $data['result']['insurance_attachment'] = $insuranceAttachments;
				  $filter = array('type'=>'licence','itemid'=>$contractorInfo['contractor_info_id'] ,'item_type'=>'image');
			      $licenceAttachments =  $modelAttachment->getAll('a.created desc',$pager,$filter);
				  $data['result']['drivers_licence_attachment'] = $licenceAttachments;				  
				}
				
				

                if ($success || $updateUser || $insurance_files || $driver_files) {
				    $data['msg'] = "Saved successfully";
					$data['type'] = 'success';
                }else{
				     $data['msg'] = "No Changes in Contractor Info";
					 $data['type'] = 'error';
                }
		

		
		
		 $data['authrezed'] = 1;
		 echo json_encode($data);
		 exit;
	
	
	}
	
	
	public function changeContractorAccountAction(){
	
	header('Content-Type: application/json');
	    $accessToken = $this->request->getParam('access_token',0);
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];
		
		
        $email2 = $this->request->getParam('email2');
        $email3 = $this->request->getParam('email3');
        $mobile1 = $this->request->getParam('mobile1');
        $mobile2 = $this->request->getParam('mobile2');
        $mobile3 = $this->request->getParam('mobile3');
        $phone1 = $this->request->getParam('phone1');
        $phone2 = $this->request->getParam('phone2');
        $phone3 = $this->request->getParam('phone3');
        $fax = $this->request->getParam('fax');
        $emergencyPhone = $this->request->getParam('emergency_phone');
        $unitLotNumber = $this->request->getParam('unit_lot_number');
        $streetNumber = $this->request->getParam('street_number');
        $streetAddress = $this->request->getParam('street_address');
        $suburb = $this->request->getParam('suburb');
        $state = $this->request->getParam('state');
        $postcode = $this->request->getParam('postcode');
        $po_box = $this->request->getParam('po_box');
        $cityId = $this->request->getParam('city_id');
        $countryId = $this->request->getParam('country_id');
       
		
		 if (get_config('remove_white_spacing')) {
            $mobile1 = preparer_number($mobile1);
            $mobile2 = preparer_number($mobile2);
            $mobile3 = preparer_number($mobile3);
            $phone1 = preparer_number($phone1);
            $phone2 = preparer_number($phone2);
            $phone3 = preparer_number($phone3);
        }

		$loggedUser = CheckAuth::getLoggedUser();
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			$data['type'] = 'error';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($data);
				exit;	
			}
		}
		
		
        $loggedUser = CheckAuth::getLoggedUser();
		$params = array(
                    'city_id' => $cityId,
                    'email2' => $email2,
                    'email3' => $email3,
                    'mobile1' => $mobile1,
                    'mobile2' => $mobile2,
                    'mobile3' => $mobile3,
                    'phone1' => $phone1,
                    'phone2' => $phone2,
                    'phone3' => $phone3,
                    'fax' => $fax,
                    'emergency_phone' => $emergencyPhone,
                    'unit_lot_number' => $unitLotNumber,
                    'street_number' => $streetNumber,
                    'street_address' => $streetAddress,
                    'suburb' => $suburb,
                    'state' => $state,
                    'postcode' => $postcode,
                    'po_box' => $po_box
                );

            $modelUser = new Model_User();
            $success = $modelUser->updateById($loggedUser['user_id'], $params);
			
			
			if($success){
			 $data['msg'] = 'Saved successfully';
			 $data['type'] = 'success';
			}else{
			 $data['msg'] = 'No Changes in User';
			 $data['type'] = 'error';
			}
		
		 $data['authrezed'] = 1;
		 echo json_encode($data);
		 exit;
	
	}
	
	
	public function getCountriesAction(){
	  
	    header('Content-Type: application/json');
	    $accessToken = $this->request->getParam('access_token',0);
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];

		$loggedUser = CheckAuth::getLoggedUser();
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($data);
				exit;	
			}
		}
		
		
        $loggedUser = CheckAuth::getLoggedUser();
		$modelCountries = new Model_Countries();
		$countries = $modelCountries->getCountriesAsArray();
        $data['result'] = $countries;
		
		$data['authrezed'] = 1;
		echo json_encode($data);
		exit;
	  
	}
	
	
	public function getCitiesAction(){
	
	 header('Content-Type: application/json');
	 $accessToken = $this->request->getParam('access_token',0);
	 $country_id = $this->request->getParam('country_id', 0);
	 $state = $this->request->getParam('state', 0);
	 $modelIosUser = new Model_IosUser();
     $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
	 
	 $this->iosLoggedUser = $user['id'];
     $modelCompanies = new Model_Companies();	
	 $loggedUser = CheckAuth::getLoggedUser();
	 $data = array('authrezed' => 0);	
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($data);
				exit;	
			}
			
		}
	
	 $cities_obj = new Model_Cities();
     $cities = $cities_obj->getCitiesByCountryIdAndState($country_id, $state);

	    $tempCities = array();
        foreach ($cities as $city) {
            $tempCities[] = array(
                'id' => $city['city_id'],
                'name' => $city['city_name']
            );
        }

	 
	 $data['result'] = $tempCities;
	 $data['authrezed'] = 1;
     echo json_encode($data);
     exit;	 
	 	
	}

	
	public function getStateAction(){
	 
	 header('Content-Type: application/json');
	 $accessToken = $this->request->getParam('access_token',0);
	 $countryId = $this->request->getParam('country_id', 0);
	 $modelIosUser = new Model_IosUser();
     $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
	 
	 $this->iosLoggedUser = $user['id'];
     $modelCompanies = new Model_Companies();	
	 $loggedUser = CheckAuth::getLoggedUser();
	 $data = array('authrezed' => 0);	
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($data);
				exit;	
			}
			
		}
		
		
		$cities_obj = new Model_Cities();
        $states = $cities_obj->getStateByCountryIdAsArray($countryId);
	
	 

	 
	 $data['result'] = $states;
	 $data['authrezed'] = 1;
     echo json_encode($data);
     exit;	
	
	}
	
	
		public function changeContractorStatusAction(){
	
	    header('Content-Type: application/json');
	    $accessToken = $this->request->getParam('access_token',0);
		$status_text = $this->request->getParam('status_text','');
		$status = $this->request->getParam('status','');
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];


		
		$loggedUser = CheckAuth::getLoggedUser();
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			$data['type'] = 'error';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($data);
				exit;	
			}
		}
		
		 $loggedUser = CheckAuth::getLoggedUser();
		 $contractorInfoObj = new Model_ContractorInfo();
		 $contractorInfo = $contractorInfoObj->getByContractorId($loggedUser['user_id']);
		
		
		
		$params = array(
                    'status_text' => $status_text,
                    'status' => $status,
                );

                if (!$contractorInfo) {
                    $data['contractor_id'] = $loggedUser['user_id'];
                    $success = $contractorInfoObj->insert($params);
                }else{
                    $success = $contractorInfoObj->updateById($contractorInfo['contractor_info_id'], $params);
                }
				
	 $data['authrezed'] = 1;
     if($success){
	    $data['msg'] = 'Changed Successfully';
		$data['type'] = 'success';
	 }else{
	   $data['msg'] = 'No changes on contractor status';
	   $data['type'] = 'error';
	 }	 
	
     echo json_encode($data);
     exit;
		
	
	}
	
	
	public function addEditContractorInsuranceAction(){
	  	  
	    header('Content-Type: application/json');
	    $accessToken = $this->request->getParam('access_token',0);
		$insurance_policy_number = $this->request->getParam('insurance_policy_number');
        $insurance_policy_start = $this->request->getParam('insurance_policy_start');
        $insurance_policy_expiry = $this->request->getParam('insurance_policy_expiry');
        $insurance_listed_services_covered = $this->request->getParam('insurance_listed_services_covered');
        $insurance_type = $this->request->getParam('insurance_type');
		$attachCount = $this->request->getParam('attachCount');
		$id = $this->request->getParam('id');
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];


		
		$loggedUser = CheckAuth::getLoggedUser();
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			$data['type'] = 'error';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($data);
				exit;	
			}
		}
		
		
        $loggedUser = CheckAuth::getLoggedUser();
		$contractorInfoObj = new Model_ContractorInfo();
		$attachmentObj = new Model_Attachment();
        $ContractorInsuranceAttachmentObj = new Model_ContractorInsuranceAttachment();
		$contractorInsuranceObj = new Model_ContractorInsurance();
        $contractorInfo = $contractorInfoObj->getByContractorId($loggedUser['user_id']);
		
		 $data = array(
                    'insurance_policy_number' => $insurance_policy_number,
                    'insurance_policy_start' => strtotime($insurance_policy_start),
                    'insurance_policy_expiry' => strtotime($insurance_policy_expiry),
                    'insurance_listed_services_covered' => $insurance_listed_services_covered,
                    'insurance_type' => $insurance_type,
                    'contractor_info_id' => $contractorInfo['contractor_info_id'],
                    'created' => time(),
                    'created_by' => $loggedUser['user_id'],
                );

			    if($id){
				 $success = $contractorInsuranceObj->updateById($id,$data);
				 $success = $id;
				}else{
				 $success = $contractorInsuranceObj->insert($data);
				}	
                
		
			
		$attchid = 0;
		if($attachCount){
		  if($id){
		   $Attachments = $contractorInsuranceObj->getAllAttachmentById($id);
		   $counter = count($Attachments);
		  }else{
		    $counter = 0;
		  }
		  for($i=0; $i<$attachCount; $i++) {
		    $source  = $_FILES['insuranceFile'.$i]['tmp_name'];
		    $extension = $this->request->getParam('ext'.$i);
            $fileInfo = pathinfo($source);
			$ext = $extension;
			$dir = get_config('attachment') . '/';
            $subdir = date('Y/m/d/');
            $fullDir = $dir . $subdir;
            if (!is_dir($fullDir)) {
                mkdir($fullDir, 0777, true);
            }
			
			$counter = $counter + 1;
			
			$fileName = $success.'_'.$counter .'.'. $ext;
			$image_saved = copy($source, $fullDir . $fileName);
			$size = filesize($fullDir . $fileName);
			
			$type = mime_content_type($fullDir . $fileName);
			$typeParts = explode("/",$type);
            if($typeParts[0] == 'image'){
				$thumbName = $success.'_thumbnail_'.$counter.'.'. $ext;
				ImageMagick::create_thumbnail($source, $fullDir . $thumbName, 78, 64);
			}else{
				$thumbName = $success.'_'.$counter.'.jpg';
				ImageMagick::convertFile($source.'[0]', $fullDir . $thumbName);
			}
			$Attchparams = array(
				'created_by'=>$loggedUser['user_id'] 
				,'created'=>time()
				,'size' => $size
				,'type'=>$type
				,'path' => $fullDir . $fileName,
                'file_name' => $fileName,
				'thumbnail_file' => $fullDir . $thumbName
			);
			
			$attchid = $attachmentObj->insert($Attchparams);
			$ContractorInsuranceAttachmentObj->insert(array('contractor_insurance_id'=>$success,'attachment_id'=>$attchid));
			if ($image_saved) {
				if (file_exists($source)) {
					unlink($source);
				}               
			}			
		  }
		}
		
		$insurance = $contractorInsuranceObj->getById($success);
		$modelAttachment = new Model_Attachment();
		$filter = array('type'=>'insurance','itemid'=>$success,'item_type'=>'image');
		$pager = null;
	    $attachments =  $modelAttachment->getAll('a.created desc',$pager,$filter);
		$insurance['attachments'] = $attachments;
        if($success || $attchid){
		  $data['msg'] = 'Saved successfully';
		  $data['type'] = 'success';
		  $data['result'] = $insurance;
		}else{
		  $data['msg'] = 'No Changes in Contractor Insurance';
		  $data['type'] = 'error';
		}

		 $data['authrezed'] = 1;
		 echo json_encode($data);
		 exit;
	  
	
	}
	
	
	public function deleteContractorInsuranceAction(){
	
	
	    header('Content-Type: application/json');
		$accessToken = $this->request->getParam('access_token',0);
		$id = $this->request->getParam('id', 0);
		
		$modelIosUser = new Model_IosUser();
		$user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
		$this->iosLoggedUser = $user['id'];


		
		$loggedUser = CheckAuth::getLoggedUser();
		$data = array('authrezed' => 0);
		
		if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
			$data['msg'] = 'wrong access token';
			$data['type'] = 'error';
			echo json_encode($data);
			exit;
		}
		
        if (empty($loggedUser)) {
			//open new session
			$authrezed = $this->openNewSession($accessToken);
			if(!$authrezed){
				echo json_encode($data);
				exit;	
			}
		}
		
		
        $loggedUser = CheckAuth::getLoggedUser();
		$contractorInsuranceObj = new Model_ContractorInsurance();
		$modelAttachment = new Model_Attachment();
		$contractorInsurance = $contractorInsuranceObj->getById($id);
		if($contractorInsurance) {
		    $filter = array('type'=>'insurance','itemid'=>$id);        
					$pager = null;
					$attachments =  $modelAttachment->getAll('a.created desc',$pager, $filter);
					if($attachments){
					  foreach($attachments as $attachment){
						$modelAttachment->updateById($attachment['attachment_id'] , array('is_deleted' => '1'));
					  }
					}
					
            $contractorInsuranceObj->deleteById($id);
			$data['msg'] = 'Insurance deleted successfully';
			$data['type'] = 'success';
        }else{
		   $data['msg'] = 'Insurance not found';
		   $data['type'] = 'error';
		}
		
		 $data['authrezed'] = 1;
		 echo json_encode($data);
		 exit;
	
	}
}
