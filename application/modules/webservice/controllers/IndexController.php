

<?php

class WebService_IndexController extends Zend_Controller_Action {

    private $request;
    private $iosLoggedUser;
    private $logId;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->logId = 0;
    }

    public function receiveDataFromAndroidAction() {

        header('Content-Type: application/json');
        $bookingId = $this->request->getParam('booking_id', 0);
        $accessToken = $this->request->getParam('access_token', 0);
        $this->checkAccessToken($accessToken);
        $loggedUser = CheckAuth::getLoggedUser();

        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];


          $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {

          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          $loggedUser = CheckAuth::getLoggedUser();
          } */
        //echo 'hhhhh';
        //echo 'by mona';
        //print_r($loggedUser);
        //echo 'eeee';
        $data = $this->request->getPost();

        //$data = $_POST;
        if (empty($data)) {
            $data = $_GET;
        }



        if (!empty($data)) {
            //var_dump($data['bookings']);
            //exit;
            //echo 'booking_address11111   '.$data['booking_id'];




            $json = json_decode($data['bookings']);


            $result = array();


            if (!empty($json)) {

                foreach ($json as $key => $value) {
                    //echo 'key '.$key;
                    //echo 'value '.$value;
                    if (!is_array($value)) {
                        $value = urldecode($value);
                    }
                    $result[$key] = $value;
                }
            }


            $result1 = array();

            if ($result['products']) {
                $products = $result['products'];


                if (!empty($products)) {
                    foreach ($products as $key => &$value) {
                        $result2 = array();
                        foreach ($value as $k => $v) {
                            if (!is_array($v)) {
                                $v = urldecode($v);
                            }
                            $result2[$k] = $v;
                        }
                        $result1[$key] = $result2;
                    }
                }
            }
            //print_r($result1);
            $result['products'] = $result1;
            ///***************

            if ($result['services']) {
                $services = $result['services'];
                if (!empty($services)) {
                    $result1 = array();

                    foreach ($services as $key => &$value) {
                        $result2 = array();
                        foreach ($value as $k => $v) {
                            if (!is_array($v)) {
                                $v = urldecode($v);
                            }
                            $result2[$k] = $v;
                        }
                        $result2['contractor_id'] = $loggedUser['user_id'];
                        $result1[$key] = $result2;
                    }
                }
            }
            //print_r($result1);
            //****attributes 
            $services = $result1;
            $result1 = array();

            if (!empty($services)) {
                foreach ($services as $key => &$value) {
                    $attributes = $value['attributes'];
                    //print_r($attributes);
                    $attributes_arr = array();
                    foreach ($attributes as $k => $v) {
                        $attribute = array();
                        foreach ($v as $k1 => $v1) {
                            if (!is_array($v1)) {
                                $v1 = urldecode($v1);
                            }
                            $attribute[$k1] = $v1;
                        }
                        $attributes_arr[$k] = $attribute;
                    }
                    //print_r($attributes_arr);
                    $value['attributes'] = $attributes_arr;
                    //$result1[$key] = $result2;
                }
            }

            $result['services'] = $services;

            //print_r($services);
            if ($result['BookingDates']) {
                $BookingDates = $result['BookingDates'];
                if (!empty($BookingDates)) {
                    $result1 = array();

                    foreach ($BookingDates as $key => &$value) {
                        $result2 = array();
                        foreach ($value as $k => $v) {
                            if (!is_array($v)) {
                                $v = urldecode($v);
                            }
                            $result2[$k] = $v;
                        }
                        $result1[$key] = $result2;
                    }
                    $BookingDates = $result1;
                    if (!empty($BookingDates)) {
                        foreach ($BookingDates as $key => &$value) {

                            if (array_key_exists('products', $value)) {
                                if ($value['products']) {
                                    $products = $value['products'];
                                    $products_arr = array();
                                    foreach ($products as $k => $v) {
                                        $products = array();
                                        foreach ($v as $k1 => $v1) {
                                            if (!is_array($v1)) {
                                                $v1 = urldecode($v1);
                                            }

                                            $products[$k1] = $v1;
                                        }
                                        $products_arr[$k] = $products;
                                    }
                                    $value['products'] = $products_arr;
                                }
                            }
                        }
                    }
                }
                $result['BookingDates'] = $BookingDates;
            }






            $modelContractorServiceBooking = new Model_ContractorServiceBooking();
            //$modelContractorServiceBooking->setServicesToBookingOffline($bookingId, $services);
            //print_r($result);
            //exit;
            $booking = $result;


            $result = $this->save($mode = 'update', $booking, 'android');




            if ($result['IsSuccess']) {
                $booking = $this->getBooking(false, array('booking_id' => $bookingId));
                $returnData['IsSuccess'] = true;
                $returnData['Msg'] = $booking;
                //$returnData['is_change'] = $booking['is_change'];
            } else {

                $returnData['IsSuccess'] = $result['IsSuccess'];
                $returnData['Msg'] = $result['Msg'];
            }
        } else {
            $returnData['IsSuccess'] = false;
            $returnData['Msg'] = "NO DATA";
        }
        echo json_encode($returnData);
        exit;
        //echo '*******************************************************';
        //echo 'booking_address   '.$json['booking_address'];
        //echo 'islam testtt ';
        //echo json_encode(array('test'=>'done'));
        //exit;
    }

    public function indexAction() {
        header('Content-Type: application/json');
        CheckAuth::logout();

        $email = $this->request->getParam('email');
        $password = $this->request->getParam('password');
        $mode = $this->request->getParam('mode', 'login');
        $uuid = $this->request->getParam('uuid');

        $data = array('authrezed' => 0);

        if (!empty($email) && !empty($password) && !empty($uuid)) { // validate form data
            $authrezed = $this->getAuthrezed();

            $authrezed->setIdentity($email);
            $authrezed->setCredential(sha1($password));

            $auth = Zend_Auth::getInstance();
            $authrezedResult = $auth->authenticate($authrezed);

            if ($authrezedResult->isValid()) {

                $identity = $authrezed->getResultRowObject();

                //$identity['ios_user_id'] = '55';

                $authStorge = $auth->getStorage();
                $authStorge->write($identity);

                CheckAuth::afterlogin(false, 'app');
                //CheckAuth::afterlogin(false);

                $modelIosUser = new Model_IosUser();
                ////check if this is the firs time the user login from this device
                /* $loggedUser = Zend_Auth::getInstance()->getIdentity();
                  $loggedBefore = $modelIosUser->getByUserIdAndUUID($loggedUser->user_id, $uuid);
                  $ignoreSyncedDataParam = 1;
                  if($loggedBefore){
                  $ignoreSyncedDataParam = 0;
                  } */

                $this->iosLoggedUser = $modelIosUser->afterIosLogin($uuid);

                $result = array();

                switch ($mode) {
                    case 'booking':
                        $result = $this->getBooking();
                        break;
                    case 'booking_original':
                        $result = $this->getBookingOriginal();
                        break;
                    case 'update_sync':
                        $this->updateSync();
                        break;
                    case 'accept_or_reject_booking':
                        $result = $this->acceptOrRejectBooking();
                        break;
                    case 'add_payment':
                        $result = $this->addPayment();
                        break;
                    case 'payment_types':
                        $result = $this->getPaymentTypes();
                        break;
                    case 'booking_status':
                        $result = $this->getBookingStatus();
                        break;
                    case 'prepare_to_edit':
                        $result = $this->prepareToEdit();
                        break;
                    case 'save_booking':
                        $result = $this->saveBooking();
                        break;
                }

                $data['result'] = $result;
                $data['authrezed'] = 1;
            }
        }

        echo json_encode($data);
        exit;
    }

    public function getSystemSettingsAction() {

        $result = array();
        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        //$this->checkAccessToken($accessToken);
        //$loggedUser = CheckAuth::getLoggedUser();
        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];

          $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {

          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */



        // get all products 
        $modelProduct = new Model_Product();
        $products = $modelProduct->getAll();
        $result['products'] = $products;
        // get all property type 
        $modelPropertyType = new Model_PropertyType();
        $propertyTypes = $modelPropertyType->getAll();
        $result['propertyTypes'] = $propertyTypes;
        //get all payment Types
        $result['paymentTypes'] = $this->getPaymentTypes();
        //get all booking status
        $result['bookingStatus'] = $this->getBookingStatus();
        $result['allowedBookingStatus'] = $this->getBookingStatus();
        //get all notification settings
        $model_IosNotificationSetting = new Model_IosNotificationSetting();
        $notificationSettings = $model_IosNotificationSetting->getAllIosNotificationSetting();
        $result['notificationSettings'] = $notificationSettings;

        // get all complaint types 
        $modelComplaintType = new Model_ComplaintType();
        $complaintTypes = $modelComplaintType->getAll();
        $result['complaintTypes'] = $complaintTypes;
        // get all rejected questions 
        $modelRejectBookingQuestion = new Model_RejectBookingQuestion();
        $modelRejectBookingQuestionOptions = new Model_RejectBookingQuestionOptions();
        $questions = $modelRejectBookingQuestion->getAll();
        foreach ($questions as &$question) {
            $options = array();
            if ($question['question_type'] == 'multiple') {
                $options = $modelRejectBookingQuestionOptions->getByQuestionId($question['id']);
            }
            $question['options'] = $options;
        }
        $result['questions'] = $questions;

        // get all image types 
        $modelImageTypes = new Model_ImageTypes();
        $imagetypes = $modelImageTypes->getAll();
        $tempImagetypes = array();
        foreach ($imagetypes as $imagetype) {
            $tempImagetypes[] = array(
                'id' => $imagetype['image_types_id'],
                'name' => $imagetype['name']
            );
        }
        $result['ImageTypes'] = $tempImagetypes;
        // get all services
        $servicesObj = new Model_Services();
        $services = $servicesObj->getServiceAsArray();
        $result['services'] = $services;
        // get all countries
        $modelCountries = new Model_Countries();
        $countries = $modelCountries->getCountriesAsArray();
        $result['countries'] = $countries;
        // get all companies
        $modelCompany = new Model_Companies();
        $companies = $modelCompany->getAllForWebService();
        $result['companies'] = $companies;

        // get allowed booking status by islam
        //$allowedBookingStatus = $this->getAllowedBookingStatus();
        //$result['allowedBookingStatus'] = $allowedBookingStatus;
        ///get call out fee of this company
        /* $modelCompanies = new Model_Companies();
          $company_id = CheckAuth::getCompanySession();
          $company = $modelCompanies->getById($company_id); */
        $modelCompanies = new Model_Companies();
        $company = $modelCompanies->getById(1);
        $data['call_out_fee'] = $company['call_out_fee'];

        $modelCustomerType = new Model_CustomerType();
        $customerTypes = $modelCustomerType->getCustomerTypeAsArray(false);
        $data['customer_types'] = $customerTypes;

        $modelWeather = new Model_WeatherStatus();
        $weather = $modelWeather->getAllFromDB();

        $result['weatherForecast'] = $weather;

        $data['result'] = $result;
        echo json_encode($data);
        exit;
    }

    public function getAllowedBookingStatus() {
        $status_colors = array(
            '0' => '#888888',
            '1' => '#cc3333',
            '2' => '#dd4477',
            '3' => '#994499',
            '4' => '#6633cc',
            '5' => '#336699',
            '6' => '#3366cc',
            '7' => '#22aa99',
            '8' => '#329262',
            '9' => '#109618',
            '10' => '#66aa00',
            '11' => '#aaaa11',
            '12' => '#d6ae00',
            '13' => '#ee8800',
            '14' => '#dd5511',
            '15' => '#a87070',
            '16' => '#8c6d8c',
            '17' => '#627487',
            '18' => '#7083a8',
            '19' => '#5c8d87',
            '20' => '#898951',
            '21' => '#b08b59',
            '-1' => '#7a367a'
        );
        $modelBookingStatus = new Model_BookingStatus();
        $bookingStatus = $modelBookingStatus->getAll();
        $allowedBookingStatus = array();
        foreach ($bookingStatus as $key => $status) {
            $allowedBookingStatus[$key]['name'] = $status['name'];
            $allowedBookingStatus[$key]['color'] = $status_colors[$status['color']];
        }

        return $allowedBookingStatus;
    }

    ///new function
    /* public function loginAction() {


      CheckAuth::logout();
      $email = $this->request->getParam('email');
      $password = $this->request->getParam('password');
      $mode = $this->request->getParam('mode', 'login');
      $uuid = $this->request->getParam('uuid');





      if (!empty($email) && !empty($password) && !empty($uuid)) { // validate form data
      $authrezed = $this->getAuthrezed();

      $authrezed->setIdentity($email);
      $authrezed->setCredential(sha1($password));

      $auth = Zend_Auth::getInstance();
      $authrezedResult = $auth->authenticate($authrezed);

      if ($authrezedResult->isValid()) {

      $identity = $authrezed->getResultRowObject();

      $authStorge = $auth->getStorage();
      $authStorge->write($identity);

      CheckAuth::afterlogin(false, 'app');
      //CheckAuth::afterlogin(false);

      $modelIosUser = new Model_IosUser();

      $accessToken = $modelIosUser->afterIosLogin($uuid);
      $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);

      $this->iosLoggedUser = $user['id'];
      $loggedUser = CheckAuth::getLoggedUser();
      $userId = $loggedUser['user_id'];
      $active_status = ($loggedUser['active'] == true) ? 1 : 0;
      $result = array('access_token' => $accessToken, 'user_id' => $userId, 'active_status' => $active_status);

      $data['result'] = $result;
      $data['authrezed'] = 1;
      } else {
      $data['result'] = array();
      $data['authrezed'] = 0;
      }
      }


      // Turn on output buffering with the gzhandler
      //ob_start('ob_gzhandler');
      header('Content-Type: application/json');
      echo json_encode($data);
      exit;
      } */

    public function loginAction() {


        CheckAuth::logout();

        $_REQUEST = array_merge($_REQUEST, (array) json_decode(file_get_contents("php://input"), true));
        $email = $_REQUEST['email'];
        $password = $_REQUEST['password'];
        $mode = isset($_REQUEST['mode']) ? $_REQUEST['mode'] : 'login';
        $uuid = $_REQUEST['uuid'];
        $os = isset($_REQUEST['os']) ? $_REQUEST['os'] : 'android';
        //By islam for iOS team
        $extra_data = isset($_REQUEST['extra_data']) ? $_REQUEST['extra_data'] : 0;






        if (!empty($email) && !empty($password) && !empty($uuid)) { // validate form data
            $authrezed = $this->getAuthrezed();

            $authrezed->setIdentity($email);
            $authrezed->setCredential(sha1($password));

            $auth = Zend_Auth::getInstance();
            $authrezedResult = $auth->authenticate($authrezed);

            if ($authrezedResult->isValid()) {

                $identity = $authrezed->getResultRowObject();

                $authStorge = $auth->getStorage();
                $authStorge->write($identity);

                CheckAuth::afterlogin(false, 'app');

                //CheckAuth::afterlogin(false);

                $modelIosUser = new Model_IosUser();

                $accessToken = $modelIosUser->afterIosLogin($uuid);
                $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);

                $this->iosLoggedUser = $user['id'];
                $loggedUser = CheckAuth::getLoggedUser();
                $userId = $loggedUser['user_id'];

                $active_status = ($loggedUser['active'] == 'TRUE') ? 1 : 0;
                $result = array('access_token' => $accessToken, 'user_id' => $userId, 'active_status' => $active_status);
                $result['blocked'] = $loggedUser['blocked'];

                if ($loggedUser && $os == 'ios') {
                    $is_registered = $this->registerMobileDevice();
                }

                $line_address = get_line_address($loggedUser);
                $MAP_OBJECT_Booking = new GoogleMapAPI();
                $address = $MAP_OBJECT_Booking->getGeocode($line_address);
                $result['avatar'] = $_SERVER['HTTP_HOST'] . '/uploads/user_pic/thumb_medium/' . $loggedUser['avatar'];
                $result['lat'] = $address['lat'];
                $result['lon'] = $address['lon'];
                $result['display_name'] = $loggedUser['display_name'];

                $modelContractorRate = new Model_ContractorRate();
                $contractorRate = $modelContractorRate->getRateByContractor($loggedUser['user_id']);
                $result['contractor_rate'] = $contractorRate;

                //By islam for iOS team
                if ($extra_data) {
                    //get counts
                    $contractor_id = $loggedUser['user_id'];
                    $counts = $this->getCounts(CheckAuth::getRoleName(), $contractor_id);
                    $result['menu_count'] = $counts;
                    $result['contractor_services'] = $counts;

                    //contractor services
                    $contractorServices = $this->getContractorServices($contractor_id);
                    $result['contractor_services'] = $contractorServices;

                    //get first 30 bookings
                    $today = getTimePeriodByName('today');

                    $todayFilters = array(
                        'booking_start_between' => $today['start'],
                        'booking_end_between' => $today['end'],
                        'multiple_booking_start_between' => $today['start'],
                        'multiple_booking_end_between' => $today['end']
                    );
                    $initialBookings = $this->getBooking(false, $todayFilters, 1);
                    if (empty($initialBookings['result'])) {
                        $is_last_request = 1;
                    } else {
                        $is_last_request = 0;
                    }
                    $initialBookings['is_last_request'] = $is_last_request;
                    $result['initial_bookings'] = $initialBookings;
                }
                //Rand 
                $modelCompanies = new Model_Companies();
                $company_id = CheckAuth::getCompanySession();
                $company = $modelCompanies->getById($company_id);
                $company_logo = $_SERVER['HTTP_HOST'] . '/uploads/company_logo/' . $company['company_logo'];
                $company['company_logo'] = $company_logo;
                $result['company'] = $company;
                //End Rand


                $data['result'] = $result;
                $data['authrezed'] = 1;
            } else {
                $data['result'] = array();
                $data['authrezed'] = 0;
            }
        }


        // Turn on output buffering with the gzhandler
        //ob_start('ob_gzhandler');
        header('Content-Type: application/json');
        echo json_encode($data);
        exit;
    }

    public function getContractorServices($contractor) {

        $modelCompanies = new Model_Companies();
        $modelCustomer = new Model_Customer();
        $modelCity = new Model_Cities();
        $modelAttachment = new Model_Attachment();
        $modelContractorServiceAvailability = new Model_ContractorServiceAvailability();
        $modelServiceAttribute = new Model_ServiceAttribute();
        $attributeListValueAttachmentObj = new Model_AttributeListValueAttachment();
        $modelContractorService = new Model_ContractorService();

        $loggedUser = CheckAuth::getLoggedUser();


        //$contractor = $loggedUser['user_id'];
        $result = $modelContractorService->getContractorServices($contractor);



        $newResult = array();
        $newResultTemp = array();
        foreach ($result as $key => $value) {
            $contractorServiceAvailability = $modelContractorServiceAvailability->getCitiesByContractorServiceId($value['contractor_service_id']);
            if (!empty($contractorServiceAvailability)) {
                $newResult[$key] = $value;
                $attributes = $modelContractorService->getContractorServicesAttributes($value['service_id']);
                // $defaultImage = $modelAttachment->getById($value['attachment_id']);
                $ServiceAttachmentObj = new Model_ServiceAttachment();
                $serviceAttachments = $ServiceAttachmentObj->getByServiceId($value['service_id']);


                $images = array();
                $largeImages = array();
                $compressedImages = array();
                if ($serviceAttachments) {
                    foreach ($serviceAttachments as $k_image => $serviceAttachment) {
                        //$parts = explode("public",$serviceAttachment['thumbnail_file']);
                        /* $parts = $serviceAttachment['large_file']?explode("public",$serviceAttachment['large_file']):explode("public",$serviceAttachment['thumbnail_file']);

                          $images[$k_image] = $parts['1']; */
                        $thumbnailParts = explode("public", $serviceAttachment['thumbnail_file']);
                        $largeParts = $serviceAttachment['large_file'] ? explode("public", $serviceAttachment['large_file']) : '';
                        $compressedParts = $serviceAttachment['compressed_file'] ? explode("public", $serviceAttachment['compressed_file']) : '';
                        $images[$k_image] = $thumbnailParts['1'];
                        $largeImages[$k_image] = $largeParts ? $largeParts['1'] : '';
                        $compressedImages[$k_image] = $compressedParts ? $compressedParts['1'] : '';
                    }
                }

                $newResult[$key]['images'] = $largeImages;
                $newResult[$key]['large_images'] = $largeImages;
                $newResult[$key]['compressed_images'] = $compressedImages;

                unset($newResult[$key]['attachment_id']);
                $units = array();
                $units[0] = 'm2';
                $newResult[$key]['units'] = $units;
                $newResult[$key]['estimate_hours'] = (isset($value['estimate_hours']) && $value['estimate_hours'] != 0) ? $value['estimate_hours'] : 3;

                if (isset($postCode)) {
                    $Description = $modelServiceAttribute->getByAttributeName('Description', $value['service_id']);
                    $newResult[$key]['description'] = $Description['default_value'];
                }
                $attributesArray = array();
                foreach ($attributes as $k => $v) {
                    //$v['contractor_service_id'] = $value['contractor_service_id'];

                    $attributesArray[$k] = $v;
                    if ($v['is_list'] == '1') {
                        $listValues = $modelContractorService->getContractorServicesAttributesListValues($v['attribute_id']);
                        foreach ($listValues as $list_value_k => $listValue) {
                            $attribute_value_images = $attributeListValueAttachmentObj->getByAttributeValueId($listValue['attribute_value_id']);
                            $attributeValueImages = array();
                            if ($attribute_value_images) {
                                foreach ($attribute_value_images as $attr_v_key => $attribute_value_image) {
                                    $typeParts = explode("/", $attribute_value_image['type']);
                                    if ($typeParts[0] == 'image') {
                                        $parts = explode("public", $attribute_value_image['path']);
                                        $attributeValueImages[] = $parts['1'];
                                    }
                                }
                            }
                            $listValues[$list_value_k]['images'] = $attributeValueImages;
                        }
                        $attributesArray[$k]['attribute_values'] = $listValues;
                    }
                }

                $newResult[$key]['attributes'] = $attributesArray;
                //by islam to fix services issue which appeared in adamf account due to indexes
                $newResultTemp[] = $newResult[$key];
            }
        }

        return $newResultTemp;
    }

    public function registerMobileDevice() {

        $os = $this->request->getParam('os', 'ios');
        $_REQUEST = array_merge($_REQUEST, (array) json_decode(file_get_contents("php://input"), true));
        //if($os == 'ios'){
        $appname = $_REQUEST['appname'];
        $appversion = $_REQUEST['appversion'];
        $deviceuid = $_REQUEST['deviceuid'];
        $devicetoken = $_REQUEST['devicetoken'];
        $devicename = $_REQUEST['devicename'];
        $devicemodel = $_REQUEST['devicemodel'];
        $deviceversion = $_REQUEST['deviceversion'];
        $pushbadge = $_REQUEST['pushbadge'];
        $pushalert = $_REQUEST['pushalert'];
        $pushsound = $_REQUEST['pushsound'];
        $status = isset($_REQUEST['status']) ? $_REQUEST['status'] : 'insert';
        $os = isset($_REQUEST['os']) ? $_REQUEST['os'] : 'android';
        /* }else{
          $appname = $this->request->getParam('appname');
          $appversion = $this->request->getParam('appversion');
          $deviceuid = $this->request->getParam('deviceuid');
          $devicetoken = $this->request->getParam('devicetoken');
          $devicename = $this->request->getParam('devicename');
          $devicemodel = $this->request->getParam('devicemodel');
          $deviceversion = $this->request->getParam('deviceversion');
          $pushbadge = $this->request->getParam('pushbadge');
          $pushalert = $this->request->getParam('pushalert');
          $pushsound = $this->request->getParam('pushsound');
          $status = $this->request->getParam('status', 'insert');
          } */
        $loggedUser = CheckAuth::getLoggedUser();
        $userId = $loggedUser['user_id'];

        $model_ContractorMobileInfo = new Model_ContractorMobileInfo();
        $allDevices = $model_ContractorMobileInfo->getAllByDeviceUid($deviceuid, $os);
        if (!empty($allDevices)) {
            $success = $model_ContractorMobileInfo->deleteByDeviceUid($deviceuid);
        }
        $contractorMobileInfo = $model_ContractorMobileInfo->getByContractorIdAndDeviceTokenAndOs($userId, $devicetoken, $os);
        if (empty($contractorMobileInfo) && $status == 'insert') {
            $data = array(
                'contractor_id' => $userId,
                'user_id' => $userId,
                'appname' => $appname,
                'appversion' => $appversion,
                'deviceuid' => $deviceuid,
                'devicetoken' => $devicetoken,
                'devicename' => $devicename,
                'devicemodel' => $devicemodel,
                'deviceversion' => $deviceversion,
                'pushbadge' => $pushbadge,
                'pushalert' => $pushalert,
                'pushsound' => $pushsound,
                'os' => $os
            );

            $result = $model_ContractorMobileInfo->insert($data);
        } else if (!empty($contractorMobileInfo) && $status == 'delete') {
            $result = $model_ContractorMobileInfo->deleteByContractorIdAndDeviceToken($userId, $devicetoken);
        }


        return $result;
    }

    ///get all bookings
    public function getAllBookingsAction() {
        header('Content-Type: application/json');
        // echo 'inside service';
        $os = $this->request->getParams('os', 'ios');
        if ($os == 'ios') {
            $_REQUEST = array_merge($_REQUEST, (array) json_decode(file_get_contents("php://input"), true));
            $accessToken = $_REQUEST['access_token'];
            $count = $_REQUEST['month_count'];
        }
        $accessToken = $this->request->getParam('access_token', 0);
        $count = $this->request->getParam('month_count', 0);
        $filters = array();
        $is_last_request = 0;


        $modelBooking = new Model_Booking();
        $this->checkAccessToken($accessToken);
        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);

          $this->iosLoggedUser = $user['id'];

          $loggedUser = CheckAuth::getLoggedUser();




          $data = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          echo json_encode($data);
          exit;
          }


          if (empty($loggedUser)) {

          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */

        if ($count) {
            //date_default_timezone_set('Australia/Sydney');         
            //$filters['created_to'] =  date('d-m-Y H:i ', strtotime(date('d-m-Y H:i ')." -".($month_count-1)." month"));
            //$filters['created_from'] = date('d-m-Y H:i ', strtotime(date('d-m-Y H:i ')." -".$month_count." month"));
            $loggedUser = CheckAuth::getLoggedUser();
            /* if(!empty($loggedUser)){
              $contractor_id = $loggedUser['user_id'];
              $firstRecord = $modelBooking->getFirstRecordForContractor($contractor_id);
              if($firstRecord['created'] < strtotime(trim($filters['created_from']))){
              $is_last_request = 0;
              }else{
              $is_last_request = 1;
              }
              } */
        }
        $result = $this->getBooking(false, $filters, $count);
        if (empty($result['result'])) {
            $is_last_request = 1;
        } else {
            $is_last_request = 0;
        }
        $data['result'] = $result;
        $data['authrezed'] = 1;
        $data['is_last_request'] = $is_last_request;



        echo json_encode($data);
        exit;
    }

    public function getAllProductsAction() {
        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $modelProduct = new Model_Product();
        $this->checkAccessToken($accessToken);
        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];



          $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {

          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */
        $result = $modelProduct->getAll();
        $data['result'] = $result;
        $data['authrezed'] = 1;

        echo json_encode($data);
        exit;
    }

    public function getAllPropertyTypeAction() {
        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $modelPropertyType = new Model_PropertyType();
        if (isset($accessToken) && $accessToken) {
            $modelCompanies = new Model_Companies();
            $this->checkAccessToken($accessToken);
            /* $modelIosUser = new Model_IosUser();
              $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
              $this->iosLoggedUser = $user['id'];
              $loggedUser = CheckAuth::getLoggedUser();
              $data = array('authrezed' => 0);

              if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
              $data['msg'] = 'wrong access token';
              echo json_encode($data);
              exit;
              }

              if (empty($loggedUser)) {

              //open new session
              $authrezed = $this->openNewSession($accessToken);
              if (!$authrezed) {
              echo json_encode($data);
              exit;
              }
              } */
            $loggedUser = CheckAuth::getLoggedUser();
            $result = $modelPropertyType->getAll();
        } else {
            $modelCompanies = new Model_Companies();
            $companyData = $modelCompanies->getByName('Tile Cleaners Pty Ltd');
            $companyId = $companyData['company_id'];
            $result = $modelPropertyType->getAll(array('company_id' => $companyId));
        }

        $data['authrezed'] = 1;
        $data['result'] = $result;


        echo json_encode($data);
        exit;
    }

    public function getCallOutFeeAction() {
        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $modelCompanies = new Model_Companies();
        $this->checkAccessToken($accessToken);
        $loggedUser = CheckAuth::getLoggedUser();
        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];



          $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {

          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */
        $company_id = CheckAuth::getCompanySession();
        $result = $modelCompanies->getById($company_id);
        $data['result'] = $result['call_out_fee'];
        $data['authrezed'] = 1;

        echo json_encode($data);
        exit;
    }

    public function getWorkingHoursAction() {
        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $this->checkAccessToken($accessToken);
        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];

          $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['type'] = 'error';
          $data['msg'] = 'wrong access token';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {
          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */
        $loggedUser = CheckAuth::getLoggedUser();

        $model_workingHours = new Model_WorkingHours();
        $filters = array(
            'contractor_id' => $loggedUser['user_id']
        );
        $working_hours = $model_workingHours->getAll($filters);
        $data['result'] = $working_hours;
        $data['authrezed'] = 1;
        echo json_encode($data);
        exit;
    }

    public function addWorkingHoursAction() {

        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $day = $this->request->getParam('day');
        $start_time = $this->request->getParam('start_time');
        $end_time = $this->request->getParam('end_time');
        $working_hours_id = $this->request->getParam('id');

        $model_workingHours = new Model_WorkingHours();

        $this->checkAccessToken($accessToken);
        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];
          $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          $data['type'] = 'error';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {
          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */

        if (!empty($day) || !empty($start_time) || !empty($end_time)) {
            $loggedUser = CheckAuth::getLoggedUser();

//            $model_workingHours->deleteByContractorId($user['user_id']);
            $params = array(
                'contractor_id' => $loggedUser['user_id'],
                'day' => $day,
                'start_time' => $start_time,
                'end_time' => $end_time
            );

            if (!empty($working_hours_id)) {
                $success = $model_workingHours->updateById($working_hours_id, $params);
            } else {
                $success = $model_workingHours->insert($params);
            }

            if ($success) {
                $data['msg'] = 'Saved successfully';
                $data['type'] = 'success';

                $filters = array(
                    'contractor_id' => $loggedUser['user_id'],
                );
                $working_hours = $model_workingHours->getAll($filters);
                $data['result'] = $working_hours;
            } else {
                $data['msg'] = 'No Changes in Working Hours';
                $data['type'] = 'error';
            }
        } else {
            $data['msg'] = 'You should fill all fields';
            $data['type'] = 'error';
        }
        $data['authrezed'] = 1;
        echo json_encode($data);
        exit;
    }

    public function deleteWorkingHoursAction() {

        $data = array('authrezed' => 0);
        $accessToken = $this->request->getParam('access_token', 0);
        $working_hours_id = $this->request->getParam('working_hours_id', 0);
        $this->checkAccessToken($accessToken);
        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];
          $loggedUser = CheckAuth::getLoggedUser();

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {

          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */


        $model_workingHours = new Model_WorkingHours();
        $deleted = $model_workingHours->deleteById($working_hours_id);

        if ($deleted) {
            $data['isSuccess'] = 1;
            $data['authrezed'] = 1;
        } else {
            $data['isSuccess'] = 0;
            $data['authrezed'] = 1;
        }


        echo json_encode($data);
        exit;
    }

    public function getAllFloorAction() {
        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $modelCompanies = new Model_Companies();
        $this->checkAccessToken($accessToken);
        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];



          $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {

          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */

        $modelAttributeListValue = new Model_AttributeListValue();
        $result = $modelAttributeListValue->getAllFloor();
        $data['result'] = $result;
        $data['authrezed'] = 1;

        echo json_encode($data);
        exit;
    }

    //By Walaa
    //get All property type for a specific comapny
    public function getAllPropertyTypeByCompanyIdAction() {
        header('Content-Type: application/json');

        $comapany_id = $this->request->getParam('comapany_id');

        $filters = array(
            'company_id' => $comapany_id
        );
        $modelPropertyType = new Model_PropertyType();
        $result = $modelPropertyType->getAll($filters);

        $data['result'] = $result;

        echo json_encode($data);
        exit;
    }

    //By Walaa
    //get All services with default value of comapny_id = 1 and contractor_id = 1
    public function getServicesWithDefaultValueAction() {
        header('Content-Type: application/json');

        $filters = array(
            'default_company_id' => 1,
            'contractor_id' => 1
        );
        $modelContractorService = new Model_ContractorService();
        $result = $modelContractorService->getAll($filters);

        $data['result'] = $result;

        echo json_encode($data);
        exit;
    }

    ///get all sealer
    public function getAllSealerAction() {
        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $this->checkAccessToken($accessToken);
        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];
          $modelCompanies = new Model_Companies();


          $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {

          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */

        $modelAttributeListValue = new Model_AttributeListValue();
        $result = $modelAttributeListValue->getAllSealers();
        $data['result'] = $result;
        $data['authrezed'] = 1;

        echo json_encode($data);
        exit;
    }

    ////get unsynced bookings only not all
    public function getUpdatedBookingsAction() {
        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $this->checkAccessToken($accessToken);
        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];


          $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {

          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if ($authrezed) {
          $result = $this->getBooking(true, array());
          $data['result'] = $result;
          $data['authrezed'] = 1;
          }
          } */

        $result = $this->getBooking(true, array());

        $data['result'] = $result;
        $data['authrezed'] = 1;


        echo json_encode($data);
        exit;
    }

    /////after receiving booking the app should confirm receiving them by sending updateSync request
    /////which asks the server to mark the received booking ids as synced in order to ignore these bookings in the second login
    public function updateSyncAction() {
        header('Content-Type: application/json');
        $rowData = (array) json_decode(file_get_contents("php://input"));
        //print_r($rowData);
        if ($rowData) {
            $accessToken = $rowData['access_token'];
            $booking_ids = $rowData['booking_ids'];
        } else {
            $accessToken = $this->request->getParam('access_token', 0);
            $booking_ids = $this->request->getParam('booking_ids', 0);
        }

        $this->checkAccessToken($accessToken);
        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];

          $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);
          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          echo json_encode($data);
          exit;
          }


          if (empty($loggedUser)) {

          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if ($authrezed) {
          $result = $this->updateSync($booking_ids);
          //$data['result'] = $result;
          $data['result'] = 'updated!!!!!';
          $data['authrezed'] = 1;
          }
          } */
        $result = $this->updateSync($booking_ids);
        //$data['result'] = $result;
        $data['result'] = 'updated!!!!!';
        $data['authrezed'] = 1;


        echo json_encode($data);
        exit;
    }

    /// accept or reject booking
    public function acceptOrRejectBookingAction() {
        header('Content-Type: application/json');
        $os = $this->request->getParam('os', 'ios');
        if ($os == 'ios') {
            $rawData = (array) json_decode(file_get_contents("php://input"));
            $accessToken = $rawData['access_token'];
            //print_r($rawData);
        } else {
            $accessToken = $this->request->getParam('access_token', 0);
        }
        // echo 'accessToken   '.$accessToken;
        //exit;

        $this->checkAccessToken($accessToken);
        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];

          $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          echo json_encode($data);
          exit;
          }
          if (empty($loggedUser)) {

          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if ($authrezed) {
          $result = $this->acceptOrRejectBooking();

          $data['result'] = $result;
          $data['authrezed'] = 1;
          }
          } */

        $result = $this->acceptOrRejectBooking();

        $data['result'] = $result;
        $data['authrezed'] = 1;


        echo json_encode($data);
        exit;
    }

    /// add payment
    public function addPaymentAction() {
        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $user_role = $this->request->getParam('user_role', 'contractor');
        $customer_id = $this->request->getParam('customer_id', 0);
        $this->checkAccessToken($accessToken, $user_role, $customer_id);
        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];


          $loggedUser = CheckAuth::getLoggedUser();
          //$this->iosLoggedUser = $iosUserId;
          $data = array('authrezed' => 0);
          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          $data['type'] = 'error';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {

          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if ($authrezed) {
          $result = $this->addPayment();
          $data['result'] = $result;
          $data['authrezed'] = 1;
          }
          } */
        $result = $this->addPayment();

        $data['result'] = $result;
        $data['authrezed'] = 1;


        echo json_encode($data);
        exit;
    }

    public function editPaymentAction() {

        $accessToken = $this->request->getParam('access_token', 0);
        $this->checkAccessToken($accessToken);

        $invoiceId = $this->request->getParam('invoice_id', 0);
        $invoice_number = $this->request->getParam('invoice_num', 0);

        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];
          $loggedUser = CheckAuth::getLoggedUser();
          $mobileRetArr = array('authrezed' => 0);
          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $mobileRetArr['msg'] = 'wrong access token';
          $mobileRetArr['type'] = 'error';
          $mobileRetArr['result'] = array();
          echo json_encode($mobileRetArr);
          exit;
          }

          if (empty($loggedUser)) {

          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($mobileRetArr);
          exit;
          }
          } */

        $mobileRetArr['authrezed'] = 1;


        ////////////////////////////////////
        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $receivedDate = $this->request->getParam('received_date');
        $bankCharges = $this->request->getParam('bank_charges');
        $amount = $this->request->getParam('amount');
        $description = $this->request->getParam('description');
        $description = urldecode($description);
        $reference = $this->request->getParam('reference');
        $reference = urldecode($reference);
        $amountWithheld = $this->request->getParam('amount_withheld');
        $withholdingTax = $this->request->getParam('withholding_tax');
        $isAcknowledgment = $this->request->getParam('is_acknowledgment');
        $paymentTypeId = $this->request->getParam('payment_type_id');
        $contractorId = $this->request->getParam('contractor_id', 0);


        //
        // Load Model
        //
        $modelPayment = new Model_Payment();
        $modelBooking = new Model_Booking();
        $modelBookingInvoice = new Model_BookingInvoice();

        //
        //Validation
        //
        $payment = $modelPayment->getById($id);


        if ($payment['is_approved']) {
            $mobileRetArr['msg'] = 'Could not edit Approved Payment, Please change to Unapproved to edit';
            $mobileRetArr['type'] = 'error';
            $mobileRetArr['result'] = array();
            echo json_encode($mobileRetArr);
            exit;
        }

        //echo "payment['booking_id']  ".$payment['booking_id'];
        $booking = $modelBooking->getById($payment['booking_id']);
        // get company name for customer 


        if (!$booking) {
            $mobileRetArr['msg'] = 'Invalid Booking';
            $mobileRetArr['type'] = 'error';
            $mobileRetArr['result'] = array();
            echo json_encode($mobileRetArr);
            exit;
        }

        /* if(!$modelBooking->checkIfCanEditBooking($booking['booking_id'],$loggedUser['user_id'])){
          $mobileRetArr['msg'] = "You Don't Have Permission";
          $mobileRetArr['type'] = 'error';
          $mobileRetArr['result'] = $this->getBooking(false, array('booking_id' => $booking['booking_id'] ));
          echo json_encode($mobileRetArr);
          exit;
          } */

        $approvedPayment = $modelPayment->getTotalAmount(array('booking_id' => $booking['booking_id'], 'is_approved' => 'yes'));

        /* if($approvedPayment >= $booking['qoute'] ){
          $mobileRetArr['msg'] = "fully Paid";
          $mobileRetArr['type'] = 'error';
          $mobileRetArr['result'] = $this->getBooking(false, array('booking_id' => $booking['booking_id'] ));
          echo json_encode($mobileRetArr);
          exit;
          } */

        $loggedUser = CheckAuth::getLoggedUser();
        $data = array(
            'received_date' => strtotime($receivedDate),
            'bank_charges' => round($bankCharges, 2),
            'amount' => round($amount, 2),
            'description' => urldecode($description),
            'payment_type_id' => $paymentTypeId,
            'booking_id' => $booking['booking_id'],
            'customer_id' => $booking['customer_id'],
            'user_id' => $loggedUser['user_id'],
            'created' => time(),
            'reference' => urldecode($reference),
            'amount_withheld' => round($amountWithheld, 2),
            'withholding_tax' => $withholdingTax,
            'is_acknowledgment' => $isAcknowledgment,
            'contractor_id' => $loggedUser['user_id']
        );

        $success = $modelPayment->updateById($id, $data);

        if ($success) {
            $mobileRetArr['msg'] = "Saved successfully, Please Approved this Payment";
            $mobileRetArr['result'] = $this->getBooking(false, array('booking_id' => $booking['booking_id']));
            $mobileRetArr['type'] = 'success';
            echo json_encode($mobileRetArr);
            exit;
        }
        $mobileRetArr['msg'] = "No Changes in Payment";
        $mobileRetArr['result'] = $this->getBooking(false, array('booking_id' => $booking['booking_id']));
        $mobileRetArr['type'] = 'error';
        echo json_encode($mobileRetArr);
        exit;
    }

    /// payment_types
    public function getPaymentTypesAction() {
        header('Content-Type: application/json');

        $accessToken = $this->request->getParam('access_token', 0);
        $this->checkAccessToken($accessToken);
        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];


          $loggedUser = CheckAuth::getLoggedUser();

          $data = array('authrezed' => 0);
          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          echo json_encode($data);
          exit;
          }
          if (empty($loggedUser)) {

          //open new session
          $authrezed = $this->openNewSession($accessToken);
          $data['test'] = $authrezed;
          //
          if ($authrezed) {
          $result = $this->getPaymentTypes();
          $data['result'] = $result;
          $data['authrezed'] = 1;
          }
          } */

        $result = $this->getPaymentTypes();
        $data['result'] = $result;
        $data['authrezed'] = 1;


        echo json_encode($data);
        exit;
    }

    /// booking_status
    public function getBookingStatusAction() {
        header('Content-Type: application/json');


        $accessToken = $this->request->getParam('access_token', 0);
        $this->checkAccessToken($accessToken);
        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];

          $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);
          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          echo json_encode($data);
          exit;
          }
          if (empty($loggedUser)) {

          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if ($authrezed) {
          $result = $this->getBookingStatus();
          $data['result'] = $result;
          $data['authrezed'] = 1;
          }
          } */


        $result = $this->getBookingStatus();
        $data['result'] = $result;
        $data['authrezed'] = 1;


        echo json_encode($data);
        exit;
    }

    /// prepare_to_edit
    public function prepareToEditAction() {
        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $this->checkAccessToken($accessToken);
        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];

          $loggedUser = CheckAuth::getLoggedUser();

          $data = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          echo json_encode($data);
          exit;
          }
          if (empty($loggedUser)) {

          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if ($authrezed) {
          $result = $this->prepareToEdit();
          $data['result'] = $result;
          $data['authrezed'] = 1;
          }
          } */
        $result = $this->prepareToEdit();
        $data['result'] = $result;
        $data['authrezed'] = 1;


        echo json_encode($data);
        exit;
    }

    /// save_booking
    public function saveBookingAction() {
        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $this->checkAccessToken($accessToken);
        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];

          $loggedUser = CheckAuth::getLoggedUser();

          $data = array('authrezed' => 0);
          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          echo json_encode($data);
          exit;
          }
          if (empty($loggedUser)) {

          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if ($authrezed) {
          $result = $this->saveBooking();
          $data['result'] = $result;
          $data['authrezed'] = 1;
          }
          } */

        $result = $this->saveBooking();
        $data['result'] = $result;
        $data['authrezed'] = 1;


        echo json_encode($data);
        exit;
    }

    public function testAction() {
        $test = $this->request->getParam('test', '');
        echo $test;
        echo "done By islam";
        exit;
    }

    /// save_booking offline
    public function saveBookingsOfflineAction() {
        header('Content-Type: application/json');
        //echo 'teeeeeeeee';
        $rawData = (array) json_decode(file_get_contents("php://input"));


        //$accessToken = $this->request->getParam('access_token',0);
        //$os = $this->request->getParam('os','ios');
        $accessToken = $rawData['access_token'];
        $os = $rawData['os'];
        $modelIosUser = new Model_IosUser();
        $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
        $this->iosLoggedUser = $user['id'];

        $loggedUser = CheckAuth::getLoggedUser();

        $data = array('authrezed' => 0);
        if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
            $data['msg'] = 'wrong access token';
            echo json_encode($data);
            exit;
        }
        if (empty($loggedUser)) {
            //open new session
            $authrezed = $this->openNewSession($accessToken);
            if (!$authrezed) {
                echo json_encode($data);
                exit;
            }
        }

        $loggedUser = CheckAuth::getLoggedUser();
        if ($os == 'android') {
            $rawData = $_POST;
            if (empty($rawData)) {
                $rawData = $_GET;
            }
            //print_r($rawData);
            //echo 'jsondata  '.$rawData['jsondata'];
            $jsondata = $rawData['jsondata'];
            //print_r($_GET);
            $jsondata = (array) json_decode($jsondata);
            //print_r($jsondata);
            //echo 'teeeeest  ';
        } else {
            $rawData = (array) json_decode(file_get_contents("php://input"));
            //print_r($rawData);
        }


        //foreach($bookings as $booking){
        //echo 'in the foreach .....';
        //$booking = (array) $booking;
        //$booking = $rawData['bookings'][0];
        $bookings = $rawData['bookings'];
        $result = array();
        $results = array();
        foreach ($bookings as $booking_key => $booking) {

            foreach ($booking as $key => $value) {
                //echo 'key '.$key;
                //echo 'value '.$value;
                $result[$key] = $value;
            }


            /* echo 'by islam';
              print_r($result);
              echo 'by islam';
              exit; */
            //print_r($result);
            //print_r($booking);
            //echo 'gst......'.$booking['gst'];
            //echo 'doneee';

            $services = $result['services'];

            $result1 = array();
            foreach ($services as $key => &$value) {

                $result2 = array();
                foreach ($value as $k => $v) {
                    /* foreach ($v as $k1 => $v1) {
                      $result2[$k1] = $v1;
                      } */
                    $result2[$k] = $v;
                }
                $result2['contractor_id'] = $loggedUser['user_id'];
                $result1[$key] = $result2;
            }


            //print_r($result1);
            //****attributes 
            $services = $result1;


            $result1 = array();
            foreach ($services as $key => &$value) {
                $attributes = $value['attributes'];


                //print_r($attributes);
                //By MONA
                /* $attributes_arr = array();
                  foreach ($attributes as $k => $v) {
                  $attribute = array();
                  foreach ($v as $k1 => $v1) {
                  $attribute[$k1] =  $v1;
                  $attribute_test = array();
                  foreach($v1 as $k2=>$v2){
                  $attribute_test[$k2] =  $v2;
                  }

                  $attribute[$k1] = $attribute_test;
                  }

                  $attributes_arr[$k] = $attribute;
                  }
                  $value['attributes'] = $attributes_arr;
                 */
                ///By Islam
                $attributes_arr = array();
                foreach ($attributes as $k => $v) {
                    $attribute = array();
                    foreach ($v as $k1 => $v1) {

                        $attribute[$k1] = $v1;
                    }
                    $attributes_arr[$k] = $attribute;
                }
                //print_r($attributes_arr);
                $value['attributes'] = $attributes_arr;
                //$result1[$key] = $result2;
            }

            $result['services'] = $services;


            $bookingsDates = $result['BookingDates'];
            $result1 = array();
            if (!empty($bookingsDates)) {
                foreach ($bookingsDates as $key => &$value) {

                    $result2 = array();
                    foreach ($value as $k => $v) {
                        /* foreach ($v as $k1 => $v1) {
                          $result2[$k1] = $v1;
                          } */
                        $result2[$k] = $v;
                    }
                    $result1[$key] = $result2;
                }
            }

            $bookingDates = $result1;

            $result1 = array();
            foreach ($bookingDates as $key => &$value) {
                $products = $value['products'];
                $products_arr = array();
                foreach ($products as $k => $v) {
                    $product = array();
                    foreach ($v as $k1 => $v1) {

                        $product[$k1] = $v1;
                    }
                    $products_arr[$k] = $product;
                }
                $value['products'] = $products_arr;
            }

            $result['BookingDates'] = $bookingDates;

            $products = $result['products'];

            $result1 = array();
            foreach ($products as $key => &$value) {

                $products_arr = array();
                foreach ($value as $k => $v) {

                    $products_arr[$k] = $v;
                }
                $result1['key'] = $products_arr;
            }
            $result['products'] = $result1;


            $result = $this->saveBookingOffline('update', $result);
            //}
            $results[$booking_key] = $result;
        }

        $data['result'] = $results;
        $data['authrezed'] = 1;


        echo json_encode($data);
        exit;
    }

/////////////////End

    public function getAuthrezed() {
        $modelAuthRole = new Model_AuthRole();
        $contractorRoleId = $modelAuthRole->getRoleIdByName('contractor');

        $authrezed = new Zend_Auth_Adapter_DbTable(Zend_Db_Table::getDefaultAdapter());
        $authrezed->setTableName('user')
                ->setIdentityColumn('email1')
                ->setCredentialColumn('password')
                ->setCredentialTreatment("? AND blocked = 0")
                ->setCredentialTreatment("? AND role_id={$contractorRoleId} ");

        return $authrezed;
    }

    public function getIosAuthrezed() {

        $authrezed = new Zend_Auth_Adapter_DbTable(Zend_Db_Table::getDefaultAdapter());
        $authrezed->setTableName('ios_user')
                ->setIdentityColumn('user_id');

        return $ios_authrezed;
    }

    public function getBooking($ignoreSynced = false, $filters = array(), $count = 0, $user_type = 'contractor') {

        //By Islam
        $os = $this->request->getParam('os', 'ios');
        $is_last_request = $this->request->getParam('is_last_request', 1);

        $currentPage = $this->request->getParam('current_page', 0);
        //$ignoreSyncedDataParam = $this->request->getParam('ignore_synced_data', 1);
        $is_accepted = 0;
        $is_rejected = 0;
        $modelBooking = new Model_Booking();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelContractorServiceBookingTemp = new Model_ContractorServiceBookingTemp();
        $modelUser = new Model_User();
        $modelContractorinfo = new Model_ContractorInfo();
        $modelServiceAttribute = new Model_ServiceAttribute();
        $modelServiceAttributeValue = new Model_ServiceAttributeValue();
        $modelServiceAttributeValueTemp = new Model_ServiceAttributeValueTemp();
        $modelAttributes = new Model_Attributes();
        $modelAttributeType = new Model_AttributeType();
        $modelAttributeListValue = new Model_AttributeListValue();
        $modelIosSync = new Model_IosSync();
        $modelBookingEstimate = new Model_BookingEstimate();
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelPropertyType = new Model_PropertyType();
        $modelCustomer = new Model_Customer();
        $modelBookingAddress = new Model_BookingAddress();
        $modelServices = new Model_Services();
        $modelBookingStatus = new Model_BookingStatus();



        if ($ignoreSynced) {
            if (!($is_last_request)) {
                $mobileSyncedBookingIds = $modelIosSync->getMobileSyncedBookingIds($this->iosLoggedUser);
                $filters['not_booking_ids'] = $mobileSyncedBookingIds;
            } else {
                $syncedBookingIds = $modelIosSync->getSyncedBookingIds($this->iosLoggedUser);
                $filters['not_booking_ids'] = $syncedBookingIds;
            }

            //var_dump($filters);
        }
        /* else{
          $filters['not_booking_ids'] = $syncedBookingIds;
          } */



        if ($count != 0) {
            $currentPage = $count;
            $limit = 0;
            $perPage = 30;
        } else {
            $limit = 100;
            $perPage = 10;
            if (!$currentPage) {
                $limit = 100;
            }
        }

        $pager = null;
        $bookings = $modelBooking->getAll($filters, 'created DESC', $pager, $limit, $perPage, $currentPage);

        $result = array();
        $modelBookingMultipleDays = new Model_BookingMultipleDays();
        $modelVisitedExtraInfo = new Model_VisitedExtraInfo();
        $modelBookingProduct = new Model_BookingProduct();
        $modelBookingDiscussion = new Model_BookingDiscussion();
        $modelRejectBookingQuestionAnswers = new Model_RejectBookingQuestionAnswers();

        foreach ($bookings as $key => $booking) {

            $db_params = array();
            $db_params['is_sync'] = 0;
            $db_params['ios_user_id'] = $this->iosLoggedUser;
            $db_params['booking_id'] = $booking['booking_id'];



            //By islam
            $result[$key]['is_editable'] = $booking['is_editable'];


            if ($user_type == 'contractor') {

                $syncedBooking = $modelIosSync->getByBookingIdAndIosUser($booking['booking_id'], $this->iosLoggedUser);

                if ($syncedBooking) {

                    $modelIosSync->updateById($syncedBooking['id'], $db_params);
                } else {

                    $modelIosSync->insert($db_params);
                }
            }

            //
            //booking
            //
			//////get all dates of this booking 
            $multipleDays = $modelBookingMultipleDays->getByBookingId($booking['booking_id']);
            //echo $os;
            //print_r($multipleDays);
            if ($user_type == 'contractor') {
                $rejectBookingQuestionAnswers = $modelRejectBookingQuestionAnswers->getAnswersByBookingId($booking['booking_id']);
                $result[$key]['rejectedQuestions'] = $rejectBookingQuestionAnswers;
            }

            //if($os == 'android'){
            $multipleDayResult = array();
            foreach ($multipleDays as $k => $multipleDay) {
                $multipleDayResult[$k]['multipleDay_id'] = $multipleDay['id'];
                $multipleDayResult[$k]['booking_id'] = $multipleDay['booking_id'];
                $multipleDayResult[$k]['booking_start'] = $multipleDay['booking_start'];
                $multipleDayResult[$k]['booking_end'] = $multipleDay['booking_end'];
                $multipleDayResult[$k]['is_all_day_event'] = $multipleDay['is_all_day_event'];
                $ExtraInfo = $modelVisitedExtraInfo->getById($multipleDay['visited_extra_info_id']);
                if (isset($ExtraInfo['is_visited']) && $ExtraInfo['is_visited'] && $ExtraInfo['is_visited'] != 2) {
                    //$ExtraInfo = $modelVisitedExtraInfo->getById($multipleDay['visited_extra_info_id']);
                    //$multipleDayResult[$k]['job_start'] = strtotime($ExtraInfo['job_start']);
                    $multipleDayResult[$k]['job_start'] = $ExtraInfo['job_start'];
                    //$multipleDayResult[$k]['job_end'] = strtotime($ExtraInfo['job_end']);
                    $multipleDayResult[$k]['job_end'] = $ExtraInfo['job_end'];
                    $multipleDayResult[$k]['onsite_client_name'] = $ExtraInfo['onsite_client_name'];
                    // $products = $modelBookingProduct->getProductsByExtraInfoId($multipleDay['visited_extra_info_id'] , $multipleDay['booking_id']);
                    $products = $modelBookingProduct->getByVisitedExtraInfoId($multipleDay['visited_extra_info_id']);
                    $multipleDayResult[$k]['products'] = $products;
                    $multipleDayResult[$k]['is_visited'] = $ExtraInfo['is_visited'];
                } else {
                    $multipleDayResult[$k]['is_visited'] = 0;
                    $multipleDayResult[$k]['job_start'] = null;
                    $multipleDayResult[$k]['job_end'] = null;
                    $multipleDayResult[$k]['onsite_client_name'] = null;
                    $multipleDayResult[$k]['products'] = array();
                }

                $discussion = $modelBookingDiscussion->getByExtraInfoId($multipleDay['visited_extra_info_id']);
                $multipleDayResult[$k]['extra_comments'] = $discussion['user_message'] ? $discussion['user_message'] : '';
            }
            //print_r($multipleDays);
            //print_r($multipleDayResult);
            $result[$key]['multiple_days'] = $multipleDayResult;
            /* }else{		  
              $result[$key]['multiple_days'] = $multipleDays;
              } */




            $propertyType = $modelPropertyType->getById($booking['property_type_id']);
            $address = $modelBookingAddress->getByBookingId($booking['booking_id']);
            //$is_accepted = $modelBooking->checkBookingIfAccepted($booking['booking_id']);
            //$is_rejected = $modelBooking->checkBookingIfRejected($booking['booking_id']);
            $status = $modelBookingStatus->getById($booking['status_id']);
            $modelContractorRate = new Model_ContractorRate();
            $booking_rate = $modelContractorRate->getRateByBooking($booking['booking_id']);



            $totalAmountDetails = $modelBooking->getTotalAmountBookingDetails($booking ['booking_id']);
            $result[$key]['sub_total'] = $totalAmountDetails['sub_total'];
            $result[$key]['total_discount'] = $totalAmountDetails['total_discount'];
            $result[$key]['gst'] = $totalAmountDetails['gst'];
            $result[$key]['paid_amount'] = $totalAmountDetails['paid_amount'];
            $result[$key]['refund'] = $totalAmountDetails['refund'];
            $result[$key]['call_out_fee'] = $totalAmountDetails['call_out_fee'];
            $result[$key]['qoute'] = $totalAmountDetails['total'];

            //By islam, return booking_id not only original_booking_id
            $result[$key]['booking_id'] = $booking['booking_id'];
            $result[$key]['booking_num'] = $booking['booking_num'];
            $result[$key]['last_approval_time'] = $booking['last_approval_time'];
            $result[$key]['city_id'] = $booking['city_id'];
            $result[$key]['is_change'] = $booking['is_change'];
            $result[$key]['title'] = $booking['title'];
            $result[$key]['booking_start'] = $booking['booking_start'];
            $result[$key]['booking_end'] = $booking['booking_end'];
            $result[$key]['property_type'] = $propertyType['property_type'];
            /* $result[$key]['qoute'] = $booking['qoute'];
              $result[$key]['sub_total'] = $booking['sub_total'];
              $result[$key]['total_discount'] = $booking['total_discount'];
              $result[$key]['gst'] = $booking['gst'];
              $result[$key]['paid_amount'] = $booking['paid_amount'];
              $result[$key]['refund'] = !empty($booking['refund']) ? $booking['refund'] : 0; */

            $result[$key]['description'] = $booking['description'];
            $result[$key]['status'] = $status['name'];
            $result[$key]['convert_status'] = $booking['convert_status'];
            $result[$key]['created'] = $booking['created'];
            $result[$key]['why'] = $booking['why'];
            $result[$key]['booking_rate'] = $booking_rate ? (float) $booking_rate : 0;

            //commented By Islam
            //if($os == 'android'){
            //$result[$key]['call_out_fee'] = $booking['call_out_fee'];
            $first_extra_info = $modelVisitedExtraInfo->getById($booking['visited_extra_info_id']);
            if (isset($first_extra_info['is_visited'])) {
                $result[$key]['is_visited'] = $first_extra_info['is_visited'];
            } else {
                $result[$key]['is_visited'] = 0;
            }


            //}
            ///by islam
            /* if($is_accepted){
              $result[$key]['accept_status'] = 'accepted';
              }
              elseif($is_rejected){
              $result[$key]['accept_status'] = 'rejected';
              }
              else{
              $result[$key]['accept_status'] = 'unknown';
              } */

            //////
            $result[$key]['original_booking_id'] = $booking['booking_id'];

            //booking address
            $result[$key]['booking_address'] = get_line_address($address);
            $result[$key]['po_box'] = $address['po_box'];
            $result[$key]['postcode'] = $address['postcode'];
            $result[$key]['state'] = $address['state'];
            $result[$key]['street_address'] = $address['street_address'];
            $result[$key]['street_number'] = $address['street_number'];
            $result[$key]['suburb'] = $address['suburb'];
            $result[$key]['unit_lot_number'] = $address['unit_lot_number'];
            $result[$key]['lat'] = $address['lat'];
            $result[$key]['lon'] = $address['lon'];
            ///// get booking distance
            $loggedUser = CheckAuth::getLoggedUser();
            $distance = $modelBookingAddress->getDistanceByTwoAddress($loggedUser, $address);
            //$result[$key]['booking_distance'] = number_format($distance, 2);
            $result[$key]['booking_distance'] = $distance;
            ////get all products of this booking
            $modelBookingProduct = new Model_BookingProduct();
            //if($os == 'android'){
            //$products = $modelBookingProduct->getProductsByExtraInfoId($booking['visited_extra_info_id'],$booking['booking_id']);
            $products = $modelBookingProduct->getByVisitedExtraInfoId($booking['visited_extra_info_id']);
            /* }else{
              $products = $modelBookingProduct->getProductNamesByBookingId($booking['booking_id']);
              } */
            $result[$key]['products'] = $products;
            ///By Islam 
            if ($os == 'android' || $os = 'ios') {
                ///get All Complaints of this booking
                $modelComplaint = new Model_Complaint();
                $modelComplaintTemp = new Model_ComplaintTemp();
                $complaintsTemp = $modelComplaintTemp->getByBookingId($booking['booking_id']);
                $complaints = $modelComplaint->getByBookingIdAndIsApproved($booking['booking_id']);
                $complaints = array_merge($complaints, $complaintsTemp);
                $result[$key]['complaints'] = $complaints;
            }


            /////End
            //if($os == 'android'){
            $extra_info = $modelVisitedExtraInfo->getById($booking['visited_extra_info_id']);
            $result[$key]['onsite_client_name'] = $extra_info['onsite_client_name'];
            //$result[$key]['job_start_time'] = isset($extra_info['job_start']) ? strtotime($extra_info['job_start']):0;
            $result[$key]['job_start_time'] = $extra_info['job_start'];
            //$result[$key]['job_finish_time'] = isset($extra_info['job_end']) ? strtotime($extra_info['job_end']) : 0;
            $result[$key]['job_finish_time'] = $extra_info['job_end'];
            $whyBookingdiscussion = $modelBookingDiscussion->getByExtraInfoId($booking['visited_extra_info_id']);
            $result[$key]['user_message'] = isset($whyBookingdiscussion['user_message']) ? $whyBookingdiscussion['user_message'] : '';
            /* }else{
              $result[$key]['onsite_client_name'] = $booking['onsite_client_name'];
              $result[$key]['job_start_time'] = $booking['job_start_time'];
              $result[$key]['job_finish_time'] = $booking['job_finish_time'];
              $result[$key]['satisfaction'] = $booking['satisfaction'];

              //get extra comment
              $modelBookingStatusDiscussion = new Model_BookingStatusDiscussion();
              $whyDiscussion = $modelBookingStatusDiscussion->getLastDiscussionByBookingIdAndStatusId($booking['booking_id'], $booking['status_id']);
              $result[$key]['user_message'] = isset($whyDiscussion['user_message']) ? $whyDiscussion['user_message'] : '';

              } */


            $result[$key]['is_to_follow'] = $booking['is_to_follow'];
            $result[$key]['to_follow'] = $booking['to_follow'];
            $result[$key]['is_all_day_event'] = $booking['is_all_day_event'];
            $result[$key]['is_multiple_days'] = $booking['is_multiple_days'];



            //get all questions
            $modelUpdateBookingQuestionAnswer = new Model_UpdateBookingQuestionAnswer();
            $questions = $modelUpdateBookingQuestionAnswer->getByBookingIdAndStatusId($booking['status_id'], $booking['booking_id']);
            $result[$key]['questions'] = $questions;

            //
            //estimate
            //
            $estimate = $modelBookingEstimate->getNotDeletedByBookingId($booking['booking_id']);

            if ($estimate) {
                $result[$key]['estimate']['created'] = date('Y-m-d H:i:s', $estimate['created']);
                $result[$key]['estimate']['estimate_number'] = $estimate['estimate_num'];
                $result[$key]['estimate']['estimate_type'] = $estimate['estimate_type'];
                $result[$key]['estimate']['original_estimate_id'] = $estimate['id'];
            } else {
                $result[$key]['estimate'] = array();
            }

            //
            //invoice
            //
            $invoice = $modelBookingInvoice->getByBookingId($booking['booking_id']);

            if ($invoice) {
                $result[$key]['invoice']['created'] = date('Y-m-d H:i:s', $invoice['created']);
                $result[$key]['invoice']['invoice_number'] = $invoice['invoice_num'];
                if ($os == 'android' && $invoice['invoice_type'] == 'void') {
                    $result[$key]['invoice']['invoice_type'] = '_void';
                } else {
                    $result[$key]['invoice']['invoice_type'] = $invoice['invoice_type'];
                }
                $result[$key]['invoice']['condition_report'] = $invoice['condition_report'];
                $result[$key]['invoice']['original_invoice_id'] = $invoice['id'];

                if ($os == 'android' || $os = 'ios') {
                    $modelPayment = new Model_Payment();
                    $modelRefund = new Model_Refund();
                    $payments = $modelPayment->getByBookingId($booking['booking_id']);
                    if ($payments) {
                        //foreach($payments as $payment){
                        $result[$key]['invoice']['payments'] = $payments;
                        //}

                        foreach ($payments as $num => $payment) {
                            $totalAllRefund = $modelRefund->getTotalRefund(array('booking_id' => $booking['booking_id'], 'payment_transaction_id' => $payment['reference'], 'is_approved' => 'all'));
                            $is_fully_refunded = $payment['amount'] - $totalAllRefund;
                            $result[$key]['invoice']['payments'][$num]['refunded'] = ($is_fully_refunded <= 0) ? 1 : 0;
                        }
                    }


                    //$Refunds = $modelRefund->getAll(array('booking_id'=>$booking['booking_id']));
                    $Refunds = $modelRefund->getByBookingId($booking['booking_id']);


                    if ($Refunds) {
                        $result[$key]['invoice']['refunds'] = $Refunds;
                    }
                }
            } else {
                $result[$key]['invoice'] = array();
            }


            //
            //customer
            //
            $customer = $modelCustomer->getById($booking['customer_id']);
            $customer_name = get_customer_name($customer);

            $result[$key]['customer']['name'] = $customer_name;


            if ($user_type == 'customer') {
                $modelBookingReminder = new Model_BookingReminder();
                $authRole_model = new Model_AuthRole();
                $customerRole = $authRole_model->getRoleIdByName('Customer');
                $lastCofirm = $modelBookingReminder->getLastConfirmationByBookingIdForAllUsers($booking['booking_id']);
                if ($lastCofirm['user_role'] == $customerRole) {
                    $user = $modelCustomer->getById($lastCofirm['user_id']);
                    $lastCofirm['username'] = $user['first_name'] . ' ' . $user['last_name'];
                } else {
                    $user = $modelUser->getById($lastCofirm['user_id']);
                    $lastCofirm['username'] = $user['username'];
                }

                $result[$key]['last_confirmation'] = $lastCofirm;
                $result[$key]['customer']['unit_lot_number'] = $customer['unit_lot_number'];
                $result[$key]['customer']['street_address'] = $customer['street_address'];
                $result[$key]['customer']['suburb'] = $customer['suburb'];
                $result[$key]['customer']['state'] = $customer['state'];
                $result[$key]['customer']['postcode'] = $customer['postcode'];
                $result[$key]['customer']['po_box'] = $customer['po_box'];
                $result[$key]['customer']['country_id'] = $customer['country_id'];
                $result[$key]['customer']['city_id'] = $customer['city_id'];
                $result[$key]['customer']['email1'] = $customer['email1'];
                $result[$key]['customer']['email2'] = $customer['email2'];
                $result[$key]['customer']['email3'] = $customer['email3'];
                $result[$key]['customer']['title1'] = $customer['title'];
                $result[$key]['customer']['first_name1'] = $customer['first_name'];
                $result[$key]['customer']['last_name1'] = $customer['last_name'];
                $result[$key]['customer']['title2'] = $customer['title2'];
                $result[$key]['customer']['first_name2'] = $customer['first_name2'];
                $result[$key]['customer']['last_name2'] = $customer['last_name2'];
                $result[$key]['customer']['title3'] = $customer['title3'];
                $result[$key]['customer']['first_name3'] = $customer['first_name3'];
                $result[$key]['customer']['last_name3'] = $customer['last_name3'];
                $result[$key]['customer']['customer_type'] = $customer['customer_type'];
                $result[$key]['customer']['customer_type_id'] = $customer['customer_type_id'];
                $result[$key]['customer']['company_name'] = $customer['company_name'];
                $business_name = '';
                $modelCustomerCommercialInfo = new Model_CustomerCommercialInfo();
                $businessData = $modelCustomerCommercialInfo->getByCustomerId($customer['customer_id']);
                if (isset($businessData) && !empty($businessData)) {
                    $business_name = $businessData['business_name'];
                }
                $result[$key]['customer']['business_name'] = $business_name;
            } else {
                $result[$key]['customer']['address'] = get_line_address($customer);
            }

            $result[$key]['customer']['phone1'] = $customer['phone1'];
            $result[$key]['customer']['phone2'] = $customer['phone2'];
            $result[$key]['customer']['phone3'] = $customer['phone3'];
            $result[$key]['customer']['mobile1'] = $customer['mobile1'];
            $result[$key]['customer']['mobile2'] = $customer['mobile2'];
            $result[$key]['customer']['mobile3'] = $customer['mobile3'];
            $result[$key]['customer']['fax'] = $customer['fax'];
            $result[$key]['customer']['city'] = $customer['city_name'];
            $result[$key]['customer']['country'] = $customer['country_name'];
            $result[$key]['customer']['original_customer_id'] = $customer['customer_id'];
            $result[$key]['customer']['first_name1'] = $customer['first_name'];
            $result[$key]['customer']['last_name1'] = $customer['last_name'];
            $result[$key]['customer']['first_name2'] = $customer['first_name2'];
            $result[$key]['customer']['last_name2'] = $customer['last_name2'];
            $result[$key]['customer']['first_name3'] = $customer['first_name3'];
            $result[$key]['customer']['last_name3'] = $customer['last_name3'];
            $result[$key]['customer']['state'] = $customer['state'];
            $result[$key]['customer']['postcode'] = $customer['postcode'];
            $result[$key]['customer']['street_address'] = $customer['street_address'];
            $result[$key]['customer']['email1'] = $customer['email1'];
            $result[$key]['customer']['email2'] = $customer['email2'];
            $result[$key]['customer']['email3'] = $customer['email3'];
            $result[$key]['customer']['customer_id'] = $customer['customer_id'];
            $modelCustomerContact = new Model_CustomerContact();

            $contacts = $modelCustomerContact->getByCustomerId($booking['customer_id']);
            $contacts_result = array();
            foreach ($contacts as $k => $contact) {
                $contacts_result[$k]['id'] = $contact['id'];
                $contacts_result[$k]['customer_contanct_label_id'] = isset($contact['customer_contanct_label_id']) ? $contact['customer_contanct_label_id'] : 0;
                $contacts_result[$k]['contact'] = $contact['contact'];
                $contacts_result[$k]['title'] = $contact['title'];
                $contacts_result[$k]['first_name'] = $contact['first_name'];
                $contacts_result[$k]['last_name'] = $contact['last_name'];
                $contacts_result[$k]['email1'] = $contact['email1'];
                $contacts_result[$k]['email2'] = $contact['email2'];
                $contacts_result[$k]['email3'] = $contact['email3'];
                $contacts_result[$k]['phone1'] = $contact['phone1'];
                $contacts_result[$k]['phone2'] = $contact['phone2'];
                $contacts_result[$k]['phone3'] = $contact['phone3'];
                $contacts_result[$k]['mobile1'] = $contact['mobile1'];
                $contacts_result[$k]['mobile2'] = $contact['mobile2'];
                $contacts_result[$k]['mobile3'] = $contact['mobile3'];
            }
            $result[$key]['customer']['contacts'] = $contacts_result;

            //$modelCustomerContact = new Model_CustomerContact();
            //$result[$key]['customer']['contacts'] = $modelCustomerContact->getByCustomerId($booking['customer_id']);
            $business_name = '';
            $modelCustomerCommercialInfo = new Model_CustomerCommercialInfo();
            $businessData = $modelCustomerCommercialInfo->getByCustomerId($customer['customer_id']);
            if (isset($businessData) && !empty($businessData)) {
                $business_name = $businessData['business_name'];
            }
            $result[$key]['customer']['business_name'] = $business_name;



            //
            //services & service_attribute & service_attribute_value
            //
            
            $filters = array();
            $filters['booking_id'] = $booking['booking_id'];
            $bookingService = array();
            $services = $modelContractorServiceBooking->getAll($filters);
            $servicesTemp = $modelContractorServiceBookingTemp->getAll($filters);

            if ($servicesTemp && !$modelBooking->checkCanEditBookingDetails($booking['booking_id']) && $booking['is_change']) {

                foreach ($servicesTemp as $service_key => $service) {


                    $serviceId = isset($service['service_id']) ? $service['service_id'] : 0;
                    $clone = isset($service['clone']) ? $service['clone'] : 0;
                    $bookingId = isset($service['booking_id']) ? $service['booking_id'] : 0;
                    $service_info = $modelServices->getById($service['service_id']);

                    //$contractorServiceBooking = $modelContractorServiceBooking->getByBookingAndServiceAndClone($bookingId, $serviceId, $clone);
                    $contractorServiceBooking = $modelContractorServiceBookingTemp->getByBookingAndServiceAndClone($bookingId, $serviceId, $clone);
                    $contractorInfo = $modelContractorinfo->getByContractorId($contractorServiceBooking['contractor_id']);
                    $userInfo = $modelUser->getById($contractorServiceBooking['contractor_id']);

                    $contractorName = ucwords($userInfo['username']) . ucwords($contractorInfo['business_name'] ? " - " . $contractorInfo['business_name'] : '');
                    $contractorDisplayName = $userInfo['display_name'];
                    $contractorId = $contractorServiceBooking['contractor_id'];
                    $contractorAvatar = $_SERVER['HTTP_HOST'] . '/uploads/user_pic/thumb_medium/' . $userInfo['avatar'];
                    $unit_price = "0";
                    $qty = "0";

                    //get the service attribute id
                    $service_attributes = $modelServiceAttribute->getByServiceId($serviceId);

                    foreach ($service_attributes as $service_attribute) {

                        $attribute = $modelAttributes->getById($service_attribute['attribute_id']);

                        //get the service attribute value
                        $attributeValue = $modelServiceAttributeValueTemp->getByBookingIdAndServiceAttributeIdAndClone($bookingId, $service_attribute['service_attribute_id'], $clone);

                        //$unit_price = $attributeValue['value'];

                        switch ($attribute['attribute_name']) {
                            case 'Quantity':
                                $qty = $attributeValue['value'];
                                $qty_var = $attribute['attribute_variable_name'];
                                break;
                            case 'Price':
                                $unit_price = $attributeValue['value'];
                                $unit_price_var = $attribute['attribute_variable_name'];
                                break;
                            case 'Discount':
                                $discount = $attributeValue['value'];
                            //break;
                            default:
                                $atributetype = $modelAttributeType->getById($attribute['attribute_type_id']);

                                $values = array();

                                // check if the attribute type is list
                                if ($atributetype['is_list']) {
                                    // check if the attribute value is serialized
                                    if ($attributeValue['is_serialized_array']) {
                                        $unserializeValues = unserialize($attributeValue['value']);
                                        foreach ($unserializeValues as $unserializeKey => $unserializeValue) {
                                            $attributeListValue = $modelAttributeListValue->getById($unserializeValue);

                                            if ($attributeValue['service_attribute_value_id']) {
                                                $attribute_value = array();
                                                $attribute_value['value'] = $attributeListValue['attribute_value'];
                                                $attribute_value['original_service_attribute_value_id'] = $attributeValue['service_attribute_value_id'] . '_' . $unserializeKey;
                                                $values[] = $attribute_value;
                                            }
                                        }
                                    } else {
                                        $attributeListValue = $modelAttributeListValue->getById($attributeValue['value']);

                                        if ($attributeValue['service_attribute_value_id']) {
                                            $attribute_value = array();
                                            $attribute_value['value'] = $attributeListValue['attribute_value'];
                                            ////By islam we should send id of the selected value, for example instead of sending cramice we should send its id
                                            $attribute_value['attribute_value_id'] = $attributeValue['value'];
                                            ////end
                                            $attribute_value['original_service_attribute_value_id'] = $attributeValue['service_attribute_value_id'];
                                            $values[] = $attribute_value;
                                        }
                                    }
                                } else {
                                    if ($attributeValue['service_attribute_value_id']) {
                                        $attribute_value = array();
                                        if ($os == 'ios' && $attribute['attribute_name'] == "Description") {
                                            $attribute_value['value'] = strip_tags($attributeValue['value']);
                                        } else {
                                            $attribute_value['value'] = $attributeValue['value'];
                                        }
                                        $attribute_value['original_service_attribute_value_id'] = $attributeValue['service_attribute_value_id'];
                                        $values[] = $attribute_value;
                                    }
                                }

                                if ($attribute['attribute_name']) {

                                    $attribute_name = array();
                                    $attribute_name['name'] = $attribute['attribute_name'];
                                    $attribute_name['attribute_id'] = $attribute['attribute_id'];
                                    $attribute_name['original_service_attribute_id'] = $service['id'] . '_' . $service_attribute['service_attribute_id'];
                                    $attribute_name['values'] = $values;
                                    $attribute_name['type'] = $atributetype['attribute_type'];
                                    //by islam to solve problem for Hussam
                                    $attribute_name['attribute_variable_name'] = $attribute['attribute_variable_name'];
                                    $attribute_name['attribute_type'] = $atributetype['attribute_type'];

                                    $result[$key]['services'][$service_key]['service_attribute'][] = $attribute_name;
                                }
                                break;
                        }
                    }

                    $result[$key]['services'][$service_key]['technician'] = $contractorName;
                    $result[$key]['services'][$service_key]['display_name'] = $contractorDisplayName ? $contractorDisplayName : '';
                    $result[$key]['services'][$service_key]['contractor_id'] = $contractorId;
                    $result[$key]['services'][$service_key]['contractor_avatar'] = $contractorAvatar;
                    $result[$key]['services'][$service_key]['mobile'] = $userInfo['mobile1'];
                    $result[$key]['services'][$service_key]['service_name'] = $service_info['service_name'];
                    $result[$key]['services'][$service_key]['min_price'] = $service_info['min_price'];
                    $result[$key]['services'][$service_key]['is_accepted'] = $service['is_accepted'];
                    $result[$key]['services'][$service_key]['is_rejected'] = $service['is_rejected'];
                    $result[$key]['services'][$service_key]['quantity'] = $qty;
                    $result[$key]['services'][$service_key]['quantity_variable_name'] = $qty_var;
                    $result[$key]['services'][$service_key]['unit_price'] = $unit_price;
                    $result[$key]['services'][$service_key]['unit_price_variable_name'] = $unit_price_var;
                    $result[$key]['services'][$service_key]['estimate_hours'] = $service['estimate_hours'] ? $service['estimate_hours'] : 3;

                    //$result[$key]['services'][$service_key]['total'] = $modelContractorServiceBooking->getTotalServiceBookingQoute($bookingId, $serviceId, $clone);
                    $result[$key]['services'][$service_key]['total'] = $modelContractorServiceBookingTemp->getTotalServiceBookingQoute($bookingId, $serviceId, $clone);
                    $result[$key]['services'][$service_key]['original_contractor_service_booking_id'] = $service['id'];
                    $result[$key]['services'][$service_key]['price_equasion'] = $service_info['price_equasion'];
                    $result[$key]['services'][$service_key]['consider_min_price'] = $contractorServiceBooking['consider_min_price'];
                    $result[$key]['services'][$service_key]['clone'] = $clone;
                    $result[$key]['services'][$service_key]['service_id'] = $serviceId;
                    $result[$key]['services'][$service_key]['discount'] = $discount;


                    if (!($is_accepted == '1')) {
                        $is_accepted = $service['is_accepted'];
                    }

                    if (!($is_rejected == '1')) {
                        $is_rejected = $service['is_rejected'];
                    }
                }
            } else if ($services) {


                foreach ($services as $service_key => $service) {


                    $serviceId = isset($service['service_id']) ? $service['service_id'] : 0;
                    $clone = isset($service['clone']) ? $service['clone'] : 0;
                    $bookingId = isset($service['booking_id']) ? $service['booking_id'] : 0;
                    $service_info = $modelServices->getById($service['service_id']);

                    $contractorServiceBooking = $modelContractorServiceBooking->getByBookingAndServiceAndClone($bookingId, $serviceId, $clone);
                    $contractorInfo = $modelContractorinfo->getByContractorId($contractorServiceBooking['contractor_id']);
                    $userInfo = $modelUser->getById($contractorServiceBooking['contractor_id']);

                    $contractorName = ucwords($userInfo['username']) . ucwords($contractorInfo['business_name'] ? " - " . $contractorInfo['business_name'] : '');
                    $contractorDisplayName = $userInfo['display_name'];
                    $contractorId = $contractorServiceBooking['contractor_id'];
                    $contractorAvatar = $_SERVER['HTTP_HOST'] . '/uploads/user_pic/thumb_medium/' . $userInfo['avatar'];
                    $unit_price = "0";
                    $qty = "0";
                    $qty_var = 'quantity';
                    $unit_price_var = 'price';

                    //get the service attribute id
                    $service_attributes = $modelServiceAttribute->getByServiceId($serviceId);
                    foreach ($service_attributes as $service_attribute) {

                        $attribute = $modelAttributes->getById($service_attribute['attribute_id']);

                        //get the service attribute value
                        $attributeValue = $modelServiceAttributeValue->getByBookingIdAndServiceAttributeIdAndClone($bookingId, $service_attribute['service_attribute_id'], $clone);

                        //$unit_price = $attributeValue['value'];

                        switch ($attribute['attribute_name']) {
                            case 'Quantity':
                                $qty = $attributeValue['value'];
                                $qty_var = $attribute['attribute_variable_name'];
                                break;
                            case 'Price':
                                $unit_price = $attributeValue['value'];
                                $unit_price_var = $attribute['attribute_variable_name'];

                                break;
                            case 'Discount':
                                $discount = $attributeValue['value'];
                            //break;
                            default:
                                $atributetype = $modelAttributeType->getById($attribute['attribute_type_id']);

                                $values = array();

                                // check if the attribute type is list
                                if ($atributetype['is_list']) {
                                    // check if the attribute value is serialized
                                    if ($attributeValue['is_serialized_array']) {
                                        $unserializeValues = unserialize($attributeValue['value']);
                                        foreach ($unserializeValues as $unserializeKey => $unserializeValue) {
                                            $attributeListValue = $modelAttributeListValue->getById($unserializeValue);

                                            if ($attributeValue['service_attribute_value_id']) {
                                                $attribute_value = array();
                                                $attribute_value['value'] = $attributeListValue['attribute_value'];
                                                $attribute_value['original_service_attribute_value_id'] = $attributeValue['service_attribute_value_id'] . '_' . $unserializeKey;
                                                $values[] = $attribute_value;
                                            }
                                        }
                                    } else {
                                        $attributeListValue = $modelAttributeListValue->getById($attributeValue['value']);

                                        if ($attributeValue['service_attribute_value_id']) {
                                            $attribute_value = array();
                                            $attribute_value['value'] = $attributeListValue['attribute_value'];
                                            ////By islam we should send id of the selected value, for example instead of sending cramice we should send its id
                                            $attribute_value['attribute_value_id'] = $attributeValue['value'];
                                            ////end
                                            $attribute_value['original_service_attribute_value_id'] = $attributeValue['service_attribute_value_id'];
                                            $values[] = $attribute_value;
                                        }
                                    }
                                } else {
                                    if ($attributeValue['service_attribute_value_id']) {
                                        $attribute_value = array();
                                        if ($os == 'ios' && $attribute['attribute_name'] == "Description") {
                                            $attribute_value['value'] = strip_tags($attributeValue['value']);
                                        } else {
                                            $attribute_value['value'] = $attributeValue['value'];
                                        }
                                        $attribute_value['original_service_attribute_value_id'] = $attributeValue['service_attribute_value_id'];
                                        $values[] = $attribute_value;
                                    }
                                }

                                if ($attribute['attribute_name']) {

                                    $attribute_name = array();
                                    $attribute_name['name'] = $attribute['attribute_name'];
                                    $attribute_name['attribute_id'] = $attribute['attribute_id'];
                                    $attribute_name['original_service_attribute_id'] = $service['id'] . '_' . $service_attribute['service_attribute_id'];
                                    $attribute_name['values'] = $values;
                                    $attribute_name['type'] = $atributetype['attribute_type'];
                                    //by islam to solve a problem for Hussam
                                    $attribute_name['attribute_type'] = $atributetype['attribute_type'];
                                    $attribute_name['attribute_variable_name'] = $attribute['attribute_variable_name'];
                                    $result[$key]['services'][$service_key]['service_attribute'][] = $attribute_name;
                                }
                                break;
                        }
                    }

                    $result[$key]['services'][$service_key]['technician'] = $contractorName;
                    $result[$key]['services'][$service_key]['display_name'] = $contractorDisplayName ? $contractorDisplayName : '';
                    $result[$key]['services'][$service_key]['contractor_id'] = $contractorId;
                    $result[$key]['services'][$service_key]['contractor_avatar'] = $contractorAvatar;
                    $result[$key]['services'][$service_key]['mobile'] = $userInfo['mobile1'];
                    $result[$key]['services'][$service_key]['service_name'] = $service_info['service_name'];
                    $result[$key]['services'][$service_key]['min_price'] = $service_info['min_price'];
                    $result[$key]['services'][$service_key]['is_accepted'] = $service['is_accepted'];
                    $result[$key]['services'][$service_key]['is_rejected'] = $service['is_rejected'];
                    $result[$key]['services'][$service_key]['quantity'] = $qty;
                    $result[$key]['services'][$service_key]['quantity_variable_name'] = $qty_var;
                    $result[$key]['services'][$service_key]['unit_price'] = $unit_price;
                    $result[$key]['services'][$service_key]['unit_price_variable_name'] = $unit_price_var;
                    $result[$key]['services'][$service_key]['estimate_hours'] = $service['estimate_hours'] ? $service['estimate_hours'] : 3;
                    $result[$key]['services'][$service_key]['total'] = $modelContractorServiceBooking->getTotalServiceBookingQoute($bookingId, $serviceId, $clone);
                    $result[$key]['services'][$service_key]['original_contractor_service_booking_id'] = $service['id'];
                    $result[$key]['services'][$service_key]['price_equasion'] = $service_info['price_equasion'];
                    $result[$key]['services'][$service_key]['consider_min_price'] = $contractorServiceBooking['consider_min_price'];
                    $result[$key]['services'][$service_key]['clone'] = $clone;
                    $result[$key]['services'][$service_key]['service_id'] = $serviceId;
                    //$result[$key]['services'][$service_key]['discount'] = $discount;


                    if (!($is_accepted == '1')) {
                        $is_accepted = $service['is_accepted'];
                    }

                    if (!($is_rejected == '1')) {
                        $is_rejected = $service['is_rejected'];
                    }
                }
            }


            if ($is_accepted) {
                $result[$key]['accept_status'] = 'accepted';
            } elseif ($is_rejected) {
                $result[$key]['accept_status'] = 'rejected';
            } else {
                $result[$key]['accept_status'] = 'unknown';
            }
        }

        $status_colors = array(
            '0' => '#888888',
            '1' => '#cc3333',
            '2' => '#dd4477',
            '3' => '#994499',
            '4' => '#6633cc',
            '5' => '#336699',
            '6' => '#3366cc',
            '7' => '#22aa99',
            '8' => '#329262',
            '9' => '#109618',
            '10' => '#66aa00',
            '11' => '#aaaa11',
            '12' => '#d6ae00',
            '13' => '#ee8800',
            '14' => '#dd5511',
            '15' => '#a87070',
            '16' => '#8c6d8c',
            '17' => '#627487',
            '18' => '#7083a8',
            '19' => '#5c8d87',
            '20' => '#898951',
            '21' => '#b08b59',
            '-1' => '#7a367a'
        );

        $bookingStatus = $modelBookingStatus->getAll();
        $allowedBookingStatus = array();
        foreach ($bookingStatus as $key => $status) {
            $allowedBookingStatus[$key]['name'] = $status['name'];
            $allowedBookingStatus[$key]['color'] = $status_colors[$status['color']];
        }
        $BookingsWithAllowedStatus = array('result' => $result, 'allowedBookingStatus' => $allowedBookingStatus);

        return $BookingsWithAllowedStatus;
    }

    public function getBookingOriginal($ignoreSynced = true, $filters = array()) {

        $currentPage = $this->request->getParam('current_page', 0);
        $ignoreSyncedDataParam = $this->request->getParam('ignore_synced_data', 0);

        $modelBooking = new Model_Booking();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelUser = new Model_User();
        $modelContractorinfo = new Model_ContractorInfo();
        $modelServiceAttribute = new Model_ServiceAttribute();
        $modelServiceAttributeValue = new Model_ServiceAttributeValue();
        $modelAttributes = new Model_Attributes();
        $modelAttributeType = new Model_AttributeType();
        $modelAttributeListValue = new Model_AttributeListValue();
        $modelIosSync = new Model_IosSync();
        $modelBookingEstimate = new Model_BookingEstimate();
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelPropertyType = new Model_PropertyType();
        $modelCustomer = new Model_Customer();
        $modelBookingAddress = new Model_BookingAddress();
        $modelServices = new Model_Services();
        $modelBookingStatus = new Model_BookingStatus();

        if ($ignoreSynced && !$ignoreSyncedDataParam) {
            $syncedBookingIds = $modelIosSync->getSyncedBookingIds($this->iosLoggedUser);
            $filters['not_booking_ids'] = $syncedBookingIds;
        }

        $limit = 100;
        $perPage = 10;
        if (!$currentPage) {
            $limit = 100;
        }
        $pager = null;
        $bookings = $modelBooking->getAll($filters, 'created DESC', $pager, $limit, $perPage, $currentPage);

        $result = array();
        foreach ($bookings as $key => $booking) {

            $db_params = array();
            $db_params['is_sync'] = 0;
            $db_params['ios_user_id'] = $this->iosLoggedUser;
            $db_params['booking_id'] = $booking['booking_id'];

            $syncedBooking = $modelIosSync->getByBookingIdAndIosUser($booking['booking_id'], $this->iosLoggedUser);
            if ($syncedBooking) {
                $modelIosSync->updateById($syncedBooking['id'], $db_params);
            } else {
                $modelIosSync->insert($db_params);
            }

            //
            //booking
            //
            $propertyType = $modelPropertyType->getById($booking['property_type_id']);
            $address = $modelBookingAddress->getByBookingId($booking['booking_id']);
            $is_accepted = $modelBooking->checkBookingIfAccepted($booking['booking_id']);
            $is_rejected = $modelBooking->checkBookingIfRejected($booking['booking_id']);
            $status = $modelBookingStatus->getById($booking['status_id']);

            $result[$key]['booking_num'] = $booking['booking_num'];
            $result[$key]['title'] = $booking['title'];
            $result[$key]['booking_start'] = $booking['booking_start'];
            $result[$key]['booking_end'] = $booking['booking_end'];
            $result[$key]['property_type'] = $propertyType['property_type'];
            $result[$key]['qoute'] = $booking['qoute'];
            $result[$key]['sub_total'] = $booking['sub_total'];
            $result[$key]['total_discount'] = $booking['total_discount'];
            $result[$key]['gst'] = $booking['gst'];
            $result[$key]['paid_amount'] = $booking['paid_amount'];
            $result[$key]['description'] = $booking['description'];
            $result[$key]['status'] = $status['name'];
            $result[$key]['convert_status'] = $booking['convert_status'];
            $result[$key]['created'] = $booking['created'];
            ///by islam
            if ($is_accepted) {
                $result[$key]['accept_status'] = 'accepted';
            } elseif ($is_rejected) {
                $result[$key]['accept_status'] = 'rejected';
            } else {
                $result[$key]['accept_status'] = 'unknown';
            }
            //////


            $result[$key]['original_booking_id'] = $booking['booking_id'];

            //booking address
            $result[$key]['booking_address'] = get_line_address($address);
            $result[$key]['po_box'] = $address['po_box'];
            $result[$key]['postcode'] = $address['postcode'];
            $result[$key]['state'] = $address['state'];
            $result[$key]['street_address'] = $address['street_address'];
            $result[$key]['street_number'] = $address['street_number'];
            $result[$key]['suburb'] = $address['suburb'];
            $result[$key]['unit_lot_number'] = $address['unit_lot_number'];
            ///// get booking distance
            $loggedUser = CheckAuth::getLoggedUser();
            $distance = $modelBookingAddress->getDistanceByTwoAddress($loggedUser, $address);
            $result[$key]['booking_distance'] = number_format($distance, 2);

            //
            //estimate
            //
            $estimate = $modelBookingEstimate->getNotDeletedByBookingId($booking['booking_id']);

            if ($estimate) {
                $result[$key]['estimate']['created'] = date('Y-m-d H:i:s', $estimate['created']);
                $result[$key]['estimate']['estimate_number'] = $estimate['estimate_num'];
                $result[$key]['estimate']['estimate_type'] = $estimate['estimate_type'];
                $result[$key]['estimate']['original_estimate_id'] = $estimate['id'];
            } else {
                $result[$key]['estimate'] = array();
            }


            //
            //invoice
            //
            $invoice = $modelBookingInvoice->getByBookingId($booking['booking_id']);

            if ($invoice) {
                $result[$key]['invoice']['created'] = date('Y-m-d H:i:s', $invoice['created']);
                $result[$key]['invoice']['invoice_number'] = $invoice['invoice_num'];
                $result[$key]['invoice']['invoice_type'] = $invoice['invoice_type'];
                $result[$key]['invoice']['condition_report'] = $invoice['condition_report'];
                $result[$key]['invoice']['original_invoice_id'] = $invoice['id'];
            } else {
                $result[$key]['invoice'] = array();
            }


            //
            //customer
            //
            $customer = $modelCustomer->getById($booking['customer_id']);
            $customer_name = get_customer_name($customer);

            $result[$key]['customer']['name'] = $customer_name;
            $result[$key]['customer']['address'] = get_line_address($customer);
            $result[$key]['customer']['email1'] = $customer['email1'];
            $result[$key]['customer']['email2'] = $customer['email2'];
            $result[$key]['customer']['email3'] = $customer['email3'];
            $result[$key]['customer']['phone1'] = $customer['phone1'];
            $result[$key]['customer']['phone2'] = $customer['phone2'];
            $result[$key]['customer']['phone3'] = $customer['phone3'];
            $result[$key]['customer']['mobile1'] = $customer['mobile1'];
            $result[$key]['customer']['mobile2'] = $customer['mobile2'];
            $result[$key]['customer']['mobile3'] = $customer['mobile3'];
            $result[$key]['customer']['fax'] = $customer['fax'];
            $result[$key]['customer']['city'] = $customer['city_name'];
            $result[$key]['customer']['country'] = $customer['country_name'];
            $result[$key]['customer']['original_customer_id'] = $customer['customer_id'];


            //
            //services & service_attribute & service_attribute_value
            //
            
            $filters = array();
            $filters['booking_id'] = $booking['booking_id'];
            $services = $modelContractorServiceBooking->getAll($filters);

            foreach ($services as $service_key => $service) {


                $serviceId = isset($service['service_id']) ? $service['service_id'] : 0;
                $clone = isset($service['clone']) ? $service['clone'] : 0;
                $bookingId = isset($service['booking_id']) ? $service['booking_id'] : 0;
                $service_info = $modelServices->getById($service['service_id']);

                $contractorServiceBooking = $modelContractorServiceBooking->getByBookingAndServiceAndClone($bookingId, $serviceId, $clone);
                $contractorInfo = $modelContractorinfo->getByContractorId($contractorServiceBooking['contractor_id']);
                $userInfo = $modelUser->getById($contractorServiceBooking['contractor_id']);

                $contractorName = ucwords($userInfo['username']) . ucwords($contractorInfo['business_name'] ? " - " . $contractorInfo['business_name'] : '');
                $contractorDisplayName = $userInfo['display_name'];

                $unit_price = 0;
                $qty = 0;

                //get the service attribute id
                $service_attributes = $modelServiceAttribute->getByServiceId($serviceId);
                foreach ($service_attributes as $service_attribute) {

                    $attribute = $modelAttributes->getById($service_attribute['attribute_id']);

                    //get the service attribute value
                    $attributeValue = $modelServiceAttributeValue->getByBookingIdAndServiceAttributeIdAndClone($bookingId, $service_attribute['service_attribute_id'], $clone);

                    switch ($attribute['attribute_name']) {
                        case 'Quantity':
                            $qty = $attributeValue['value'];
                            break;
                        case 'Price':
                            $unit_price = $attributeValue['value'];
                            break;
                        default:
                            $atributetype = $modelAttributeType->getById($attribute['attribute_type_id']);

                            $values = array();

                            // check if the attribute type is list
                            if ($atributetype['is_list']) {
                                // check if the attribute value is serialized
                                if ($attributeValue['is_serialized_array']) {
                                    $unserializeValues = unserialize($attributeValue['value']);
                                    foreach ($unserializeValues as $unserializeKey => $unserializeValue) {
                                        $attributeListValue = $modelAttributeListValue->getById($unserializeValue);

                                        if ($attributeValue['service_attribute_value_id']) {
                                            $attribute_value = array();
                                            $attribute_value['value'] = $attributeListValue['attribute_value'];
                                            $attribute_value['original_service_attribute_value_id'] = $attributeValue['service_attribute_value_id'] . '_' . $unserializeKey;
                                            $values[] = $attribute_value;
                                        }
                                    }
                                } else {
                                    $attributeListValue = $modelAttributeListValue->getById($attributeValue['value']);

                                    if ($attributeValue['service_attribute_value_id']) {
                                        $attribute_value = array();
                                        $attribute_value['value'] = $attributeListValue['attribute_value'];
                                        $attribute_value['original_service_attribute_value_id'] = $attributeValue['service_attribute_value_id'];
                                        $values[] = $attribute_value;
                                    }
                                }
                            } else {
                                if ($attributeValue['service_attribute_value_id']) {
                                    $attribute_value = array();
                                    $attribute_value['value'] = $attributeValue['value'];
                                    $attribute_value['original_service_attribute_value_id'] = $attributeValue['service_attribute_value_id'];
                                    $values[] = $attribute_value;
                                }
                            }

                            if ($attribute['attribute_name']) {

                                $attribute_name = array();
                                $attribute_name['name'] = $attribute['attribute_name'];
                                $attribute_name['original_service_attribute_id'] = $service['id'] . '_' . $service_attribute['service_attribute_id'];
                                $attribute_name['values'] = $values;

                                $result[$key]['services'][$service_key]['service_attribute'][] = $attribute_name;
                            }
                            break;
                    }
                }

                $result[$key]['services'][$service_key]['technician'] = $contractorName;
                $result[$key]['services'][$service_key]['display_name'] = $contractorDisplayName ? $contractorDisplayName : '';
                $result[$key]['services'][$service_key]['service_name'] = $service_info['service_name'];
                $result[$key]['services'][$service_key]['min_price'] = $service_info['min_price'];
                $result[$key]['services'][$service_key]['is_accepted'] = $service['is_accepted'];
                $result[$key]['services'][$service_key]['is_rejected'] = $service['is_rejected'];
                $result[$key]['services'][$service_key]['quantity'] = $qty;
                $result[$key]['services'][$service_key]['unit_price'] = $unit_price;
                $result[$key]['services'][$service_key]['total'] = $modelContractorServiceBooking->getTotalServiceBookingQoute($bookingId, $serviceId, $clone);
                $result[$key]['services'][$service_key]['original_contractor_service_booking_id'] = $service['id'];
            }
        }

        return $result;
    }

    public function updateSync($booking_ids = array()) {
        //$booking_ids = $this->request->getParam('booking_ids',array());
        //$booking_ids = (array)json_decode(file_get_contents("php://input"));
        $modelIosSync = new Model_IosSync();
        /* echo "test";
          print_r($booking_ids);
          echo "test"; */
        if (isset($booking_ids) && !empty($booking_ids)) {
            foreach ($booking_ids as $booking_id) {
                $db_params = array();
                $db_params['is_sync'] = 1;
                $db_params['ios_user_id'] = $this->iosLoggedUser;
                $db_params['booking_id'] = $booking_id;

                $syncedBooking = $modelIosSync->getByBookingIdAndIosUser($booking_id, $this->iosLoggedUser);
                //print_r($syncedBooking);
                //print_r($db_params);

                if ($syncedBooking) {
                    $modelIosSync->updateById($syncedBooking['id'], $db_params);
                } else {
                    $modelIosSync->insert($db_params);
                }
            }
        }
        return $booking_ids;
    }

    public function acceptOrRejectBooking() {

        $os = $this->request->getParam('os', 'ios');
        if ($os == 'ios') {
            $rawData = (array) json_decode(file_get_contents("php://input"));
            $booking_id = $rawData['booking_id'];
            $services = $rawData['services'];
        } else {
            $services = $this->request->getParam('services');
            $booking_id = $this->request->getParam('booking_id');
            $services = json_decode($services);
        }

        $services_arr = array();

        foreach ($services as $key => &$value) {
            $result2 = array();
            foreach ($value as $k => $v) {
                $result2[$k] = $v;
            }
            $services_arr[$key] = $result2;
        }

        //print_r($services_arr);
        //exit;

        $loggedUser = CheckAuth::getLoggedUser();

        $modelContractorServiceBooking = new Model_ContractorServiceBooking();

        $filters = array();
        $filters['booking_id'] = $booking_id;
        if (!CheckAuth::checkCredential(array('canSeeAllBooking'))) {
            $filters['contractor_id'] = $loggedUser['user_id'];
        }
        $bookingServices = $modelContractorServiceBooking->getAll($filters);

        foreach ($services_arr as $service) {

            $modelContractorServiceBooking->updateById($service['original_contractor_service_booking_id'], array('is_accepted' => $service['is_accepted'], 'is_rejected' => $service['is_rejected']));
            /* foreach ($bookingServices as $bookingService) {

              if($bookingService['service_id'] == $service['service_id']){

              $modelContractorServiceBooking->updateById($bookingService['id'], array('is_accepted' => $service['is_accepted'], 'is_rejected' => $service['is_rejected']));
              break;
              }
              } */
        }


        return $this->getBooking(false, array('booking_id' => $booking_id));
    }

    /**
     * Add Payment
     */
    public function addPayment() {
        //
        // get request parameters
        //
        $received_date = $this->request->getParam('received_date', date('Y-m-d H:i:s', time()));
        $bankCharges = $this->request->getParam('bank_charges', 0);
        $amount = (float) $this->request->getParam('amount', 0);
        $description = $this->request->getParam('description', '');
        $reference = $this->request->getParam('reference', '');
        $amountWithheld = $this->request->getParam('amount_withheld', 0);
        $withholdingTax = $this->request->getParam('withholding_tax', 0);
        $isAcknowledgment = $this->request->getParam('is_acknowledgment', 0);
//        $paymentTypeId = $this->request->getParam('payment_type_id', 0);
        $bookingId = $this->request->getParam('booking_id', 0);
        $accessCode = $this->request->getParam('access_code', '');
        $modelAuthRole = new Model_AuthRole();
        $contractorRoleId = $modelAuthRole->getRoleIdByName('contractor');
        $customerRoleId = $modelAuthRole->getRoleIdByName('customer');


        $loggedUser = CheckAuth::getLoggedUser();

        //
        // check Auth for logged user
        //
		if ($loggedUser['role_id'] != $customerRoleId) {
            if (!CheckAuth::checkCredential(array('paymentAdd'))) {
                return array('type' => 'error', 'message' => "You Don't Have Permission");
            }
        }
        //
        // Load Model
        //
        $bookingModel = new Model_Booking();
        $modelPayment = new Model_Payment();

        $modelBookingInvoice = new Model_BookingInvoice();
        $modelUser = new Model_User();
        $modelPaymentType = new Model_PaymentType();
        $paymentType = $modelPaymentType->getByPaymentTypeAndCompany('Credit Card');


        $booking = $bookingModel->getById($bookingId);
        if (!$booking) {
            return array('type' => 'error', 'message' => "Invalid Booking");
        }

        /* if (!$bookingModel->checkIfCanEditBooking($booking['booking_id'])) {
          return array('type' => 'error', 'message' => "You Don't Have Permission , You have to Accept This Booking Services First");
          } */

        $approvedPayment = $modelPayment->getTotalAmount(array('booking_id' => $booking['booking_id'], 'is_approved' => 'yes'));
        /* if ($approvedPayment >= $booking['qoute']) {
          return array('type' => 'error', 'message' => "fully Paid");
          } */

        $allPayment = $modelPayment->getTotalAmount(array('booking_id' => $booking['booking_id'], 'is_approved' => 'all'));
        /* if ($allPayment >= $booking['qoute']) {
          return array('type' => 'error', 'message' => "fully Paid,Check Unapproved Payment");
          }

          if ($amount > ($booking['qoute'] - $allPayment)) {
          return array('type' => 'error', 'message' => "Amount entered is larger than amount required, please adjust the amount and try again.");
          } */

        if ($amount <= 0) {
            return array('type' => 'error', 'message' => "Invalid Amount");
        }

        //
        // handling the insertion process of eway payment
        //
		//echo 'accessCode  '.$accessCode;
        if ($accessCode) {

            Zend_Loader::loadFile('include_eway.php', APPLICATION_PATH . '/../library/eway-rapid-php-master');


            /* $apiKey = '44DD7CYeE+kcvjudSPOlfxY02iJdMGZNkchVAmtmetc7hQuW2Z/t3Fdwh3rlTTlleFZNlv';
              $apiPassword = 'xMhNzNbS';
              $apiEndpoint = \Eway\Rapid\Client::MODE_SANDBOX; */

            $apiKey = '44DD7AhRBX08hfSoic2wv9mAF/Bs5St52K15ISLq6EaA29SCLz7GuY+G6eyiJkDLUvRKRU';
            $apiPassword = '72tQqX16';
            $apiEndpoint = 'production';

            $client = \Eway\Rapid::createClient($apiKey, $apiPassword, $apiEndpoint);
            $response = $client->queryTransaction($accessCode);

            $response = $response->Transactions[0];
            // print_r($response);
            /* echo 'test by islam transaction status';
              echo $response->TransactionStatus;
              echo 'test by islam transaction status';
              echo $response->TransactionID;
              echo 'test by islam transaction status';
              echo urldecode($response->TransactionID);
              echo 'done'; */
            if ($response->TransactionStatus) {
                if ($loggedUser['role_id'] == $contractorRoleId) {
                    $contractor_id = $loggedUser['user_id'];
                } else if ($loggedUser['role_id'] == $customerRoleId) {
                    $contractor = $modelUser->getByUsername('enquiries');
                    $contractor_id = $contractor['user_id'];
                }

                //$reference = $response->TransactionID;
                $data = array(
                    'received_date' => strtotime($received_date),
                    'bank_charges' => round($bankCharges, 2),
                    'amount' => round($amount, 2),
                    'description' => urldecode($description),
                    'payment_type_id' => $paymentType['id'],
                    'booking_id' => $booking['booking_id'],
                    'customer_id' => $booking['customer_id'],
                    'user_id' => $contractor_id,
                    'created' => time(),
                    'reference' => urldecode($response->TransactionID),
                    'amount_withheld' => round($amountWithheld, 2),
                    'withholding_tax' => $withholdingTax,
                    'is_acknowledgment' => $isAcknowledgment,
                    'contractor_id' => $loggedUser['user_id'],
                    'is_approved' => 1,
                    'approved_by' => 'system'
                );
                $success = $modelPayment->insert($data);

                if ($success) {

                    $paidAmount = $modelPayment->getTotalAmount(array('booking_id' => $booking['booking_id'], 'is_approved' => 'yes'));

                    if ($paidAmount < 0) {
                        $paidAmount = 0;
                    }

                    $bookingModel->updateById($booking['booking_id'], array('paid_amount' => round($paidAmount, 2)));

                    if ($paidAmount >= $booking['qoute']) {
                        $modelBookingInvoice->updateByBookingId($booking['booking_id'], array('invoice_type' => 'closed'));
                    } else {
                        $modelBookingInvoice->updateByBookingId($booking['booking_id'], array('invoice_type' => 'open'));
                    }
                    // return array('type' => 'success', 'message' => 'Saved Successfully');

                    if ($loggedUser['role_id'] == $contractorRoleId) {
                        $booking = $this->getBooking(false, array('booking_id' => $booking['booking_id']));
                    } else if ($loggedUser['role_id'] == $customerRoleId) {
                        $booking = $this->getBooking(false, array('booking_id' => $booking['booking_id']), 0, 'customer');
                    }
                    return array('type' => 'success', 'message' => "Saved successfully", 'result' => $booking);
                } else {
                    return array('type' => 'error', 'message' => "Error in Payment");
                }
            } else {

                if ($response->getErrors()) {
                    $errorMsg = '';
                    foreach ($response->getErrors() as $error) {
                        $errorMsg .= \Eway\Rapid::getMessage($error) . ', ';
                    }
                    return array('type' => 'error', 'message' => $errorMsg);
                } else {
                    return array('type' => 'error', 'message' => 'Sorry, payment declined');
                }




                /* $errors = split(', ', $response->ResponseMessage);
                  $errorMsg = 'Payment failed: ';
                  foreach ($errors as $error) {
                  $errorMsg .= \Eway\Rapid::getMessage($error) . ', ';
                  }
                  return array('type' => 'error', 'message' => $errorMsg); */
            }
        }

        //
        // handling the insertion process
        //
		
	
		
		if ($loggedUser['role_id'] == $contractorRoleId) {

            $data = array(
                'received_date' => strtotime($received_date),
                'bank_charges' => round($bankCharges, 2),
                'amount' => round($amount, 2),
                'description' => urldecode($description),
                'payment_type_id' => $paymentType['id'],
                'booking_id' => $booking['booking_id'],
                'customer_id' => $booking['customer_id'],
                'user_id' => $loggedUser['user_id'],
                'created' => time(),
                'reference' => urldecode($reference),
                'amount_withheld' => round($amountWithheld, 2),
                'withholding_tax' => $withholdingTax,
                'is_acknowledgment' => $isAcknowledgment,
                'contractor_id' => $loggedUser['user_id']
            );



            $success = $modelPayment->insert($data);

            if ($success) {

                $modelBookingInvoice->updateByBookingId($booking['booking_id'], array('invoice_type' => 'open'));
                return array('type' => 'success', 'message' => "Saved successfully, Please Approved this Payment", 'result' => $this->getBooking(false, array('booking_id' => $bookingId)));
            } else {
                return array('type' => 'error', 'message' => "Error in Payment");
            }
        }
    }

    public function getPaymentTypes() {
        $modelPaymentType = new Model_PaymentType();
        return $modelPaymentType->getPaymentTypeAsArray();
    }

    public function getBookingStatus() {
        $modelBookingStatus = new Model_BookingStatus();
        return $modelBookingStatus->getAllStatusAsArrayIOS();
    }

    public function prepareToEdit() {

        $bookingId = $this->request->getParam('booking_id', 0);

        $modelBooking = new Model_Booking();
        $booking = $modelBooking->getById($bookingId);

        $result = array();
        if ($booking) {
            $result['booking_status'] = $this->getBookingStatus();

            $customerSatisfactionLevels = array(
                array('name' => 'Very Dissatisfied', 'value' => 1),
                array('name' => 'Dissatisfied', 'value' => 2),
                array('name' => 'Neutral', 'value' => 3),
                array('name' => 'Satisfied', 'value' => 4),
                array('name' => 'Very Satisfied', 'value' => 5)
            );
            $result['customerSatisfactionLevels'] = $customerSatisfactionLevels;

            $modelProduct = new Model_Product();
            $productNames = $modelProduct->getAllAsIOSArray();
            $result['productNames'] = $productNames; //needs webservice

            $modelCompanies = new Model_Companies();
            $company = $modelCompanies->getById($booking['company_id']);
            $result['callOutFee'] = round($company['call_out_fee'], 2);

            //
            //section_1
            //
            $rows = array();

            $modelCustomer = new Model_Customer();
            $customer = $modelCustomer->getById($booking['customer_id']);
            $customer_name = get_customer_name($customer);

            //row_1
            $row = array();
            $row['type'] = 'textField';
            $row['field']['value'] = !empty($customer_name) ? $customer_name : '';
            $row['field']['enabled'] = "NO";
            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
            $row['name'] = 'customer_id';
            $row['label'] = 'Customer Name';
            $row['hiddenField'] = $customer['customer_id'];
            $row['validation'] = "required";
            $rows[] = $row;

            //fill section
            $result['sections'][] = array('title' => 'Select Customer', 'rows' => $rows);

            //
            //section_2
            //
            $rows = array();

            $modelBookingAddress = new Model_BookingAddress();
            $address = $modelBookingAddress->getByBookingId($booking['booking_id']); //needs webservice
            ///very Important
            $modelBookingStatus = new Model_BookingStatus();
            $status = $modelBookingStatus->getById($booking['status_id']);
            $loggedUser = CheckAuth::getLoggedUser();

            $allowedForContractor = $modelBooking->checkContractorTimePeriod($booking['booking_id'], $loggedUser['user_id']);
///
            if ($allowedForContractor || $status['name'] != 'AWAITING UPDATE' && $status['name'] != 'IN PROGRESS') {
                //row_1
                $row = array();
                $row['type'] = 'textField';
                $row['field']['value'] = !empty($address['unit_lot_number']) ? $address['unit_lot_number'] : '';
                $row['field']['enabled'] = "YES";
                $row['field']['keyboardType'] = 'UIKeyboardTypeNumbersAndPunctuation';
                $row['name'] = 'unit_lot_number';
                $row['label'] = 'Unit/Lot No.';
                $row['hiddenField'] = "NO";
                $row['validation'] = "";
                $rows[] = $row;

                //row_2
                $row = array();
                $row['type'] = 'textField';
                $row['field']['value'] = !empty($address['street_number']) ? $address['street_number'] : '';
                $row['field']['enabled'] = "YES";
                $row['field']['keyboardType'] = 'UIKeyboardTypeNumbersAndPunctuation';
                $row['name'] = 'street_number';
                $row['label'] = 'Street Number';
                $row['hiddenField'] = "NO";
                $row['validation'] = "required";
                $rows[] = $row;

                //row_3
                $row = array();
                $row['type'] = 'textField';
                $row['field']['value'] = !empty($address['street_address']) ? $address['street_address'] : '';
                $row['field']['enabled'] = "YES";
                $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                $row['name'] = 'street_address';
                $row['label'] = 'Street Name';
                $row['hiddenField'] = "NO";
                $row['validation'] = "required";
                $rows[] = $row;
            }
            //row_4
            $row = array();
            $row['type'] = 'textField';
            $row['field']['value'] = !empty($address['suburb']) ? $address['suburb'] : '';
            $row['field']['enabled'] = "YES";
            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
            $row['name'] = 'suburb';
            $row['label'] = 'Suburb';
            $row['hiddenField'] = "NO";
            $row['validation'] = "required";
            $rows[] = $row;

            //row_4
            $row = array();
            $row['type'] = 'textField';
            $row['field']['value'] = !empty($address['state']) ? $address['suburb'] : '';
            $row['field']['enabled'] = "YES";
            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
            $row['name'] = 'state';
            $row['label'] = 'state';
            $row['hiddenField'] = "NO";
            $row['validation'] = "required";
            $rows[] = $row;

            //row_5
            $row = array();
            $row['type'] = 'textField';
            $row['field']['value'] = !empty($address['postcode']) ? $address['postcode'] : '';
            $row['field']['enabled'] = "YES";
            $row['field']['keyboardType'] = 'UIKeyboardTypeNumbersAndPunctuation';
            $row['name'] = 'postcode';
            $row['label'] = 'Post Code';
            $row['hiddenField'] = "NO";
            $row['validation'] = "required";
            $rows[] = $row;

            if ($allowedForContractor || $status['name'] != 'AWAITING UPDATE' && $status['name'] != 'IN PROGRESS') {
                //row_6
                $row = array();
                $row['type'] = 'textField';
                $row['field']['value'] = !empty($address['po_box']) ? $address['po_box'] : '';
                $row['field']['enabled'] = "YES";
                $row['field']['keyboardType'] = 'UIKeyboardTypeNumbersAndPunctuation';
                $row['name'] = 'po_box';
                $row['label'] = 'P.O Box';
                $row['hiddenField'] = "NO";
                $row['validation'] = "";
                $rows[] = $row;
            }

            //fill section
            $result['sections'][] = array('title' => 'Booking Address', 'rows' => $rows);

            //
            //section_3
            //
            $rows = array();

            //row_1
            $row = array();
            $row['type'] = 'bookingStatus';
            $row['field']['value'] = !empty($status['name']) ? $status['name'] : '';
            $row['field']['enabled'] = "YES";
            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
            $row['name'] = 'bookingStatus';
            $row['label'] = 'Booking Status';
            $row['hiddenField'] = !empty($status['booking_status_id']) ? $status['booking_status_id'] : '';
            $row['validation'] = "";
            $rows[] = $row;

            //row_2
            $row = array();
            $row['type'] = 'textField';
            $row['field']['value'] = !empty($booking['title']) ? $booking['title'] : '';
            $row['field']['enabled'] = "NO";
            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
            $row['name'] = 'title';
            $row['label'] = 'Title';
            $row['hiddenField'] = "NO";
            $row['validation'] = "";
            $rows[] = $row;

            //row_3
            $row = array();
            $row['type'] = 'textField';
            $row['field']['value'] = !empty($booking['booking_start']) ? $booking['booking_start'] : '';
            $row['field']['enabled'] = "NO";
            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
            $row['name'] = 'booking_start';
            $row['label'] = 'Start Time';
            $row['hiddenField'] = "NO";
            $row['validation'] = "";
            $rows[] = $row;

            //row_4
            $row = array();
            $row['type'] = 'textField';
            $row['field']['value'] = !empty($booking['booking_end']) ? $booking['booking_end'] : '';
            $row['field']['enabled'] = "NO";
            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
            $row['name'] = 'booking_end';
            $row['label'] = 'End Time';
            $row['hiddenField'] = "NO";
            $row['validation'] = "";
            $rows[] = $row;

            //row_5
            $row = array();
            $modelCities = new Model_Cities();
            $city = $modelCities->getById($booking['city_id']);

            $modelCountries = new Model_Countries();
            $country = $modelCountries->getById($city['country_id']);

            $row['type'] = 'textField';
            $row['field']['value'] = !empty($country['country_name']) ? $country['country_name'] : '';
            $row['field']['enabled'] = "NO";
            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
            $row['name'] = 'country';
            $row['label'] = 'Country';
            $row['hiddenField'] = !empty($country['country_id']) ? $country['country_id'] : '';
            $row['validation'] = "";
            $rows[] = $row;

            //row_6
            $row = array();
            $row['type'] = 'textField';
            $row['field']['value'] = !empty($city['state']) ? $city['state'] : '';
            $row['field']['enabled'] = "NO";
            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
            $row['name'] = 'state';
            $row['label'] = 'State';
            $row['hiddenField'] = "NO";
            $row['validation'] = "";
            $rows[] = $row;

            //row_7
            $row = array();
            $row['type'] = 'textField';
            $row['field']['value'] = !empty($city['city_name']) ? $city['city_name'] : '';
            $row['field']['enabled'] = "NO";
            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
            $row['name'] = 'city_id';
            $row['label'] = 'City';
            $row['hiddenField'] = !empty($city['city_id']) ? $city['city_id'] : '';
            $row['validation'] = "";
            $rows[] = $row;

            //row_8
            $row = array();
            $modelPropertyType = new Model_PropertyType();
            $propertyType = $modelPropertyType->getById($booking['property_type_id']);

            $row['type'] = 'textField';
            $row['field']['value'] = !empty($propertyType['property_type']) ? $propertyType['property_type'] : '';
            $row['field']['enabled'] = "NO";
            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
            $row['name'] = 'property_type';
            $row['label'] = 'Property Type';
            $row['hiddenField'] = !empty($propertyType['id']) ? $propertyType['id'] : '';
            $row['validation'] = "";
            $rows[] = $row;

            //fill section
            $result['sections'][] = array('title' => 'Booking Status', 'rows' => $rows);

            //
            //section_4
            //
            $rows = array();

            //row_1
            $row = array();
            $row['type'] = 'textView';
            $row['field']['value'] = !empty($booking['description']) ? str_replace("\'", "'", $booking['description']) : '';
            $row['field']['enabled'] = "NO";
            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
            $row['name'] = 'description';
            $row['label'] = 'Description';
            $row['hiddenField'] = "NO";
            $row['validation'] = "";
            $rows[] = $row;

            //fill section
            $result['sections'][] = array('title' => 'Booking Description', 'rows' => $rows);

            //
            //section_5
            //
            if ($status) {
                if ($status['name'] != 'TO DO' && $status['name'] != 'IN PROGRESS') {
                    /**
                     * get why Discussion
                     */
                    $modelBookingStatusDiscussion = new Model_BookingStatusDiscussion();
                    $whyDiscussion = $modelBookingStatusDiscussion->getLastDiscussionByBookingIdAndStatusId($bookingId, $booking['status_id']);

                    $rows = array();

                    switch ($status['name']) {
                        case 'AWAITING UPDATE':
                            //row_1
                            $row = array();
                            $row['type'] = 'textView';
                            $row['field']['value'] = !empty($whyDiscussion['user_message']) ? str_replace("\'", "'", $whyDiscussion['user_message']) : '';
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'why';
                            $row['label'] = 'Why Awaiting Update ?';
                            $row['hiddenField'] = "NO";
                            $row['validation'] = "required";
                            $rows[] = $row;
                            break;
                        case 'CANCELLED':
                            //row_1
                            $row = array();
                            $row['type'] = 'textView';
                            $row['field']['value'] = !empty($whyDiscussion['user_message']) ? str_replace("\'", "'", $whyDiscussion['user_message']) : '';
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'why';
                            $row['label'] = 'Why Cancelled ?';
                            $row['hiddenField'] = "NO";
                            $row['validation'] = "required";
                            $rows[] = $row;
                            break;
                        case 'FAILED':
                            //row_1
                            $row = array();
                            $row['type'] = 'textField';
                            $row['field']['value'] = !empty($booking['onsite_client_name']) ? $booking['onsite_client_name'] : '';
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'onsite_client_name';
                            $row['label'] = 'Onsite Client Name';
                            $row['hiddenField'] = "NO";
                            $row['validation'] = "required";
                            $rows[] = $row;

                            //row_2
                            $row = array();
                            $row['type'] = 'dateTime';
                            $row['field']['value'] = !empty($booking['job_start_time']) ? date('Y-m-d H:i:s', $booking['job_start_time']) : '';
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'job_start_time';
                            $row['label'] = 'Job Start Time';
                            $row['hiddenField'] = "NO";
                            $row['validation'] = "required";
                            $rows[] = $row;

                            //row_3
                            $row = array();
                            $row['type'] = 'dateTime';
                            $row['field']['value'] = !empty($booking['job_finish_time']) ? date('Y-m-d H:i:s', $booking['job_finish_time']) : '';
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'job_finish_time';
                            $row['label'] = 'Job Finish Time';
                            $row['hiddenField'] = "NO";
                            $row['validation'] = "required";
                            $rows[] = $row;

                            //row_4
                            $satisfaction = 'Very Dissatisfied';
                            if (!empty($booking['satisfaction'])) {
                                if ($booking['satisfaction'] == 1) {
                                    $satisfaction = 'Very Dissatisfied';
                                }
                                if ($booking['satisfaction'] == 2) {
                                    $satisfaction = 'Dissatisfied';
                                }
                                if ($booking['satisfaction'] == 3) {
                                    $satisfaction = 'Neutral';
                                }
                                if ($booking['satisfaction'] == 4) {
                                    $satisfaction = 'Satisfied';
                                }
                                if ($booking['satisfaction'] == 5) {
                                    $satisfaction = 'Very Satisfied';
                                }
                            }
                            $row = array();
                            $row['type'] = 'pickerView';
                            $row['field']['value'] = $satisfaction;
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'satisfaction';
                            $row['label'] = 'Customer Satisfaction';
                            $row['hiddenField'] = !empty($booking['satisfaction']) ? $booking['satisfaction'] : '1';
                            $row['data'] = $customerSatisfactionLevels;
                            $row['validation'] = "required";
                            $rows[] = $row;

                            //row_5
                            $modelBookingProduct = new Model_BookingProduct();
                            $allBookingProduct = $modelBookingProduct->getByBookingId($bookingId);
                            if ($allBookingProduct) {
                                foreach ($allBookingProduct as $key => $bookingProduct) {
                                    $product = $modelProduct->getById($bookingProduct['product_id']);
                                    if ($product) {
                                        $row = array();
                                        $row['type'] = 'pickerView';
                                        $row['field']['value'] = $product['product'];
                                        $row['field']['enabled'] = "YES";
                                        $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                                        $row['name'] = "product[{$key}]";
                                        $row['label'] = 'Product Name';
                                        $row['hiddenField'] = $bookingProduct['product_id'];
                                        $row['data'] = $productNames;
                                        $row['validation'] = "required";
                                        if ($key != 0) {
                                            $row['removeButton'] = "YES";
                                        }
                                        $rows[] = $row;

                                        $row = array();
                                        $row['type'] = 'textField';
                                        $row['field']['value'] = $bookingProduct['ltr'];
                                        $row['field']['enabled'] = "YES";
                                        $row['field']['keyboardType'] = 'UIKeyboardTypeDecimalPad';
                                        $row['name'] = "ltr[{$key}]";
                                        $row['label'] = 'Product Ltr';
                                        $row['hiddenField'] = 'NO';
                                        $row['validation'] = "required";
                                        $rows[] = $row;
                                    }
                                }
                            } else {
                                $row = array();
                                $row['type'] = 'pickerView';
                                $row['field']['value'] = '';
                                $row['field']['enabled'] = "YES";
                                $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                                $row['name'] = "product[0]";
                                $row['label'] = 'Product Name';
                                $row['hiddenField'] = '';
                                $row['data'] = $productNames;
                                $row['validation'] = "required";
                                $rows[] = $row;

                                $row = array();
                                $row['type'] = 'textField';
                                $row['field']['value'] = '';
                                $row['field']['enabled'] = "YES";
                                $row['field']['keyboardType'] = 'UIKeyboardTypeDecimalPad';
                                $row['name'] = "ltr[0]";
                                $row['label'] = 'Product Ltr';
                                $row['hiddenField'] = 'NO';
                                $row['validation'] = "required";
                                $rows[] = $row;
                            }

                            $row = array();
                            $row['type'] = 'button';
                            $row['field']['value'] = 'Add More Product';
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = "add_more_product";
                            $row['label'] = 'Press here to Add More Product';
                            $row['hiddenField'] = 'NO';
                            $row['validation'] = "";
                            $rows[] = $row;

                            //row_6
                            $row = array();
                            $row['type'] = 'textView';
                            $row['field']['value'] = !empty($whyDiscussion['user_message']) ? str_replace("\'", "'", $whyDiscussion['user_message']) : '';
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'why';
                            $row['label'] = 'Why Failed ?';
                            $row['hiddenField'] = "NO";
                            $row['validation'] = "required";
                            $rows[] = $row;
                            break;
                        case 'COMPLETED':
                            //row_1
                            $row = array();
                            $row['type'] = 'textField';
                            $row['field']['value'] = !empty($booking['onsite_client_name']) ? $booking['onsite_client_name'] : '';
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'onsite_client_name';
                            $row['label'] = 'Onsite Client Name';
                            $row['hiddenField'] = "NO";
                            $row['validation'] = "required";
                            $rows[] = $row;

                            //row_2
                            $row = array();
                            $row['type'] = 'dateTime';
                            $row['field']['value'] = !empty($booking['job_start_time']) ? date('Y-m-d H:i:s', $booking['job_start_time']) : '';
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'job_start_time';
                            $row['label'] = 'Job Start Time';
                            $row['hiddenField'] = "NO";
                            $row['validation'] = "required";
                            $rows[] = $row;

                            //row_3
                            $row = array();
                            $row['type'] = 'dateTime';
                            $row['field']['value'] = !empty($booking['job_finish_time']) ? date('Y-m-d H:i:s', $booking['job_finish_time']) : '';
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'job_finish_time';
                            $row['label'] = 'Job Finish Time';
                            $row['hiddenField'] = "NO";
                            $row['validation'] = "required";
                            $rows[] = $row;

                            $row = array();
                            $row['type'] = 'textView';
                            $row['field']['value'] = !empty($whyDiscussion['user_message']) ? str_replace("\'", "'", $whyDiscussion['user_message']) : '';
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'extra_comments';
                            $row['label'] = 'Extra Comments';
                            $row['hiddenField'] = "NO";
                            $row['validation'] = "";
                            $rows[] = $row;

                            //row_4
                            $satisfaction = 'Very Dissatisfied';
                            if (!empty($booking['satisfaction'])) {
                                if ($booking['satisfaction'] == 1) {
                                    $satisfaction = 'Very Dissatisfied';
                                }
                                if ($booking['satisfaction'] == 2) {
                                    $satisfaction = 'Dissatisfied';
                                }
                                if ($booking['satisfaction'] == 3) {
                                    $satisfaction = 'Neutral';
                                }
                                if ($booking['satisfaction'] == 4) {
                                    $satisfaction = 'Satisfied';
                                }
                                if ($booking['satisfaction'] == 5) {
                                    $satisfaction = 'Very Satisfied';
                                }
                            }
                            $row = array();
                            $row['type'] = 'pickerView';
                            $row['field']['value'] = $satisfaction;
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'satisfaction';
                            $row['label'] = 'Customer Satisfaction';
                            $row['hiddenField'] = !empty($booking['satisfaction']) ? $booking['satisfaction'] : '1';
                            $row['data'] = $customerSatisfactionLevels;
                            $row['validation'] = "required";
                            $rows[] = $row;

                            //row_5
                            $modelBookingProduct = new Model_BookingProduct();
                            $allBookingProduct = $modelBookingProduct->getByBookingId($bookingId);
                            if ($allBookingProduct) {
                                foreach ($allBookingProduct as $key => $bookingProduct) {
                                    $product = $modelProduct->getById($bookingProduct['product_id']);
                                    if ($product) {
                                        $row = array();
                                        $row['type'] = 'pickerView';
                                        $row['field']['value'] = $product['product'];
                                        $row['field']['enabled'] = "YES";
                                        $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                                        $row['name'] = "product[{$key}]";
                                        $row['label'] = 'Product Name';
                                        $row['hiddenField'] = $bookingProduct['product_id'];
                                        $row['data'] = $productNames;
                                        $row['validation'] = "required";
                                        if ($key != 0) {
                                            $row['removeButton'] = "YES";
                                        }
                                        $rows[] = $row;

                                        $row = array();
                                        $row['type'] = 'textField';
                                        $row['field']['value'] = $bookingProduct['ltr'];
                                        $row['field']['enabled'] = "YES";
                                        $row['field']['keyboardType'] = 'UIKeyboardTypeDecimalPad';
                                        $row['name'] = "ltr[{$key}]";
                                        $row['label'] = 'Product Ltr';
                                        $row['hiddenField'] = 'NO';
                                        $row['validation'] = "required";
                                        $rows[] = $row;
                                    }
                                }
                            } else {
                                $row = array();
                                $row['type'] = 'pickerView';
                                $row['field']['value'] = '';
                                $row['field']['enabled'] = "YES";
                                $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                                $row['name'] = "product[0]";
                                $row['label'] = 'Product Name';
                                $row['hiddenField'] = '';
                                $row['data'] = $productNames;
                                $row['validation'] = "required";
                                $rows[] = $row;

                                $row = array();
                                $row['type'] = 'textField';
                                $row['field']['value'] = '';
                                $row['field']['enabled'] = "YES";
                                $row['field']['keyboardType'] = 'UIKeyboardTypeDecimalPad';
                                $row['name'] = "ltr[0]";
                                $row['label'] = 'Product Ltr';
                                $row['hiddenField'] = 'NO';
                                $row['validation'] = "required";
                                $rows[] = $row;
                            }

                            $row = array();
                            $row['type'] = 'button';
                            $row['field']['value'] = 'Add More Product';
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = "add_more_product";
                            $row['label'] = 'Press here to Add More Product';
                            $row['hiddenField'] = 'NO';
                            $row['validation'] = "";
                            $rows[] = $row;

                            break;
                        case 'ON HOLD':
                            //row_1
                            $row = array();
                            $row['type'] = 'textView';
                            $row['field']['value'] = !empty($whyDiscussion['user_message']) ? str_replace("\'", "'", $whyDiscussion['user_message']) : '';
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'why';
                            $row['label'] = 'Why On Hold ?';
                            $row['hiddenField'] = "NO";
                            $row['validation'] = "required";
                            $rows[] = $row;

                            //row_2
                            $row = array();
                            $row['type'] = 'dateTime';
                            $row['field']['value'] = !empty($booking['to_follow']) ? date('Y-m-d H:i:s', $booking['to_follow']) : '';
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'to_follow';
                            $row['label'] = 'To Follow';
                            $row['hiddenField'] = "NO";
                            $row['validation'] = "required";
                            $rows[] = $row;
                            break;
                        case 'TENTATIVE':
                            //row_1
                            $row = array();
                            $row['type'] = 'textView';
                            $row['field']['value'] = !empty($whyDiscussion['user_message']) ? str_replace("\'", "'", $whyDiscussion['user_message']) : '';
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'why';
                            $row['label'] = 'Why Tentative ?';
                            $row['hiddenField'] = "NO";
                            $row['validation'] = "required";
                            $rows[] = $row;
                            break;
                        case 'TO VISIT':
                            //row_1
                            $row = array();
                            $row['type'] = 'textView';
                            $row['field']['value'] = !empty($whyDiscussion['user_message']) ? str_replace("\'", "'", $whyDiscussion['user_message']) : '';
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'why';
                            $row['label'] = 'Why To Visit ?';
                            $row['hiddenField'] = "NO";
                            $row['validation'] = "required";
                            $rows[] = $row;
                            break;
                        case 'QUOTED':
                            //row_1
                            $row = array();
                            $row['type'] = 'switch';
                            $row['field']['value'] = isset($booking['is_to_follow']) ? $booking['is_to_follow'] : 0;
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'is_to_follow';
                            $row['label'] = 'Confirm To Follow';
                            $row['hiddenField'] = "NO";
                            $row['validation'] = "";
                            $rows[] = $row;

                            //row_2
                            $row = array();
                            $row['type'] = 'dateTime';
                            $row['field']['value'] = !empty($booking['to_follow']) ? date('Y-m-d H:i:s', $booking['to_follow']) : '';
                            $row['field']['enabled'] = "YES";
                            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                            $row['name'] = 'to_follow';
                            $row['label'] = 'To Follow';
                            $row['hiddenField'] = "NO";
                            $row['validation'] = "required";
                            $rows[] = $row;
                            break;
                    }

                    //fill section
                    $result['sections'][] = array('title' => 'Extra Info', 'rows' => $rows);
                }
            }

            //
            //services & service_attribute & service_attribute_value
            //
            $modelContractorServiceBooking = new Model_ContractorServiceBooking();
            $modelUser = new Model_User();
            $modelContractorinfo = new Model_ContractorInfo();
            $modelServiceAttribute = new Model_ServiceAttribute();
            $modelServiceAttributeValue = new Model_ServiceAttributeValue();
            $modelAttributes = new Model_Attributes();
            $modelAttributeType = new Model_AttributeType();
            $modelAttributeListValue = new Model_AttributeListValue();
            $modelServices = new Model_Services();
            $services = $modelContractorServiceBooking->getAll(array('booking_id' => $booking['booking_id']));

            if ($services) {
                $serviceNum = 0;
                foreach ($services as $service) {

                    $rows = array();

                    $serviceId = isset($service['service_id']) ? $service['service_id'] : 0;
                    $clone = isset($service['clone']) ? $service['clone'] : 0;
                    $bookingId = isset($service['booking_id']) ? $service['booking_id'] : 0;
                    $service_info = $modelServices->getById($service['service_id']);

                    $contractorServiceBooking = $modelContractorServiceBooking->getByBookingAndServiceAndClone($bookingId, $serviceId, $clone);
                    $contractorInfo = $modelContractorinfo->getByContractorId($contractorServiceBooking['contractor_id']);
                    $userInfo = $modelUser->getById($contractorServiceBooking['contractor_id']);

                    $contractorName = ucwords($userInfo['username']) . ucwords($contractorInfo['business_name'] ? " - " . $contractorInfo['business_name'] : '');

                    $row = array();
                    $row['type'] = 'textField';
                    $row['field']['value'] = !empty($contractorName) ? $contractorName : '';
                    $row['field']['enabled'] = "NO";
                    $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                    $row['name'] = 'contractor_' . $serviceId . ($clone ? '_' . $clone : '');
                    $row['label'] = 'Technician';
                    $row['hiddenField'] = $contractorServiceBooking['contractor_id'];
                    $row['validation'] = "";
                    $rows[] = $row;

                    $row = array();
                    $row['type'] = 'textField';
                    $row['field']['value'] = !empty($service_info['service_name']) ? $service_info['service_name'] : '';
                    $row['field']['enabled'] = "NO";
                    $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                    $row['name'] = 'services[' . $serviceNum . ']';
                    $row['label'] = 'Service Name';
                    $row['hiddenField'] = $serviceId . ($clone ? '_' . $clone : '');
                    $row['validation'] = "";
                    $rows[] = $row;


                    //get the service attribute id
                    $service_attributes = $modelServiceAttribute->getByServiceId($serviceId);
                    foreach ($service_attributes as $service_attribute) {

                        $attribute = $modelAttributes->getById($service_attribute['attribute_id']);

                        if ($attribute) {
                            //get the service attribute value
                            $attributeValue = $modelServiceAttributeValue->getByBookingIdAndServiceAttributeIdAndClone($bookingId, $service_attribute['service_attribute_id'], $clone);
                            $atributetype = $modelAttributeType->getById($attribute['attribute_type_id']);

                            $name = 'attribute_' . $service_attribute['service_id'] . $service_attribute['attribute_id'] . ($clone ? '_' . $clone : '');

                            switch ($atributetype['attribute_type']) {
                                case 'checkbox':
                                    $row = array();
                                    $row['type'] = 'switch';
                                    $row['field']['value'] = !empty($attributeValue['value']) ? $attributeValue['value'] : '';
                                    $row['field']['enabled'] = "YES";
                                    $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                                    $row['name'] = $name;
                                    $row['label'] = $attribute['attribute_name'];
                                    $row['hiddenField'] = "NO";
                                    if ($attribute['is_price_attribute']) {
                                        $row['priceAttribute']['name'] = $attribute['attribute_variable_name'];
                                        $row['priceAttribute']['serviceId'] = $service_attribute['service_id'];
                                        $row['priceAttribute']['clone'] = $clone;
                                        $row['priceAttribute']['equasion'] = $service_info['price_equasion'];
                                        $row['priceAttribute']['min_price'] = !empty($service_info['min_price']) ? $service_info['min_price'] : '';
                                    } else {
                                        $row['priceAttribute'] = "NO";
                                    }
                                    $row['validation'] = "";
                                    $rows[] = $row;
                                    break;

                                case 'date':
                                    $row = array();
                                    $row['type'] = 'date';
                                    $row['field']['value'] = !empty($attributeValue['value']) ? $attributeValue['value'] : '';
                                    $row['field']['enabled'] = "YES";
                                    $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                                    $row['name'] = $name;
                                    $row['label'] = $attribute['attribute_name'];
                                    $row['hiddenField'] = "NO";
                                    if ($attribute['is_price_attribute']) {
                                        $row['priceAttribute']['name'] = $attribute['attribute_variable_name'];
                                        $row['priceAttribute']['serviceId'] = $service_attribute['service_id'];
                                        $row['priceAttribute']['clone'] = $clone;
                                        $row['priceAttribute']['equasion'] = $service_info['price_equasion'];
                                        $row['priceAttribute']['min_price'] = !empty($service_info['min_price']) ? $service_info['min_price'] : '';
                                    } else {
                                        $row['priceAttribute'] = "NO";
                                    }
                                    $row['validation'] = "";
                                    $rows[] = $row;
                                    break;

                                case 'time':
                                    $row = array();
                                    $row['type'] = 'time';
                                    $row['field']['value'] = !empty($attributeValue['value']) ? $attributeValue['value'] : '';
                                    $row['field']['enabled'] = "YES";
                                    $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                                    $row['name'] = $name;
                                    $row['label'] = $attribute['attribute_name'];
                                    $row['hiddenField'] = "NO";
                                    if ($attribute['is_price_attribute']) {
                                        $row['priceAttribute']['name'] = $attribute['attribute_variable_name'];
                                        $row['priceAttribute']['serviceId'] = $service_attribute['service_id'];
                                        $row['priceAttribute']['clone'] = $clone;
                                        $row['priceAttribute']['equasion'] = $service_info['price_equasion'];
                                        $row['priceAttribute']['min_price'] = !empty($service_info['min_price']) ? $service_info['min_price'] : '';
                                    } else {
                                        $row['priceAttribute'] = "NO";
                                    }
                                    $row['validation'] = "";
                                    $rows[] = $row;
                                    break;

                                case 'long_text':
                                    $row = array();
                                    $row['type'] = 'textView';
                                    $row['field']['value'] = !empty($attributeValue['value']) ? $attributeValue['value'] : '';
                                    $row['field']['enabled'] = "YES";
                                    $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                                    $row['name'] = $name;
                                    $row['label'] = $attribute['attribute_name'];
                                    $row['hiddenField'] = "NO";
                                    if ($attribute['is_price_attribute']) {
                                        $row['priceAttribute']['name'] = $attribute['attribute_variable_name'];
                                        $row['priceAttribute']['serviceId'] = $service_attribute['service_id'];
                                        $row['priceAttribute']['clone'] = $clone;
                                        $row['priceAttribute']['equasion'] = $service_info['price_equasion'];
                                        $row['priceAttribute']['min_price'] = !empty($service_info['min_price']) ? $service_info['min_price'] : '';
                                    } else {
                                        $row['priceAttribute'] = "NO";
                                    }
                                    $row['validation'] = "";
                                    $rows[] = $row;
                                    break;

                                case 'text_input':
                                    $row = array();
                                    $row['type'] = 'textField';
                                    $row['field']['value'] = !empty($attributeValue['value']) ? $attributeValue['value'] : '';
                                    $row['field']['enabled'] = "YES";
                                    if ($attribute['attribute_variable_name'] == 'price') {
                                        $row['field']['keyboardType'] = 'UIKeyboardTypeNumbersAndPunctuation';
                                    } else if ($attribute['attribute_variable_name'] == 'quantity') {
                                        $row['field']['keyboardType'] = 'UIKeyboardTypeNumbersAndPunctuation';
                                    } else if ($attribute['attribute_variable_name'] == 'discount') {
                                        $row['field']['keyboardType'] = 'UIKeyboardTypeNumbersAndPunctuation';
                                    } else {
                                        $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                                    }
                                    $row['name'] = $name;
                                    $row['label'] = $attribute['attribute_name'];
                                    $row['hiddenField'] = "NO";
                                    if ($attribute['is_price_attribute']) {
                                        $row['priceAttribute']['name'] = $attribute['attribute_variable_name'];
                                        $row['priceAttribute']['serviceId'] = $service_attribute['service_id'];
                                        $row['priceAttribute']['clone'] = $clone;
                                        $row['priceAttribute']['equasion'] = $service_info['price_equasion'];
                                        $row['priceAttribute']['min_price'] = !empty($service_info['min_price']) ? $service_info['min_price'] : '';
                                    } else {
                                        $row['priceAttribute'] = "NO";
                                    }
                                    $row['validation'] = "";
                                    $rows[] = $row;
                                    break;

                                case 'dropdown':
                                case 'enum':
                                    $row = array();
                                    $attributeListValue = $modelAttributeListValue->getById($attributeValue['value']);
                                    $attributeListValues = $modelAttributeListValue->getByAttributeIdAsIosArray($service_attribute['attribute_id']);

                                    $row['type'] = 'pickerView';
                                    $row['field']['value'] = !empty($attributeListValue['attribute_value']) ? $attributeListValue['attribute_value'] : '';
                                    $row['field']['enabled'] = "YES";
                                    $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                                    $row['name'] = $name;
                                    $row['label'] = $attribute['attribute_name'];
                                    $row['hiddenField'] = $attributeValue['value'];
                                    $row['data'] = $attributeListValues;
                                    if ($attribute['is_price_attribute']) {
                                        $row['priceAttribute']['name'] = $attribute['attribute_variable_name'];
                                        $row['priceAttribute']['serviceId'] = $service_attribute['service_id'];
                                        $row['priceAttribute']['clone'] = $clone;
                                        $row['priceAttribute']['equasion'] = $service_info['price_equasion'];
                                        $row['priceAttribute']['min_price'] = !empty($service_info['min_price']) ? $service_info['min_price'] : '';
                                    } else {
                                        $row['priceAttribute'] = "NO";
                                    }
                                    $row['validation'] = "";
                                    $rows[] = $row;
                                    break;


                                case 'list':
                                case 'set':
                                    $row = array();
                                    $attributeListValues = $modelAttributeListValue->getByAttributeIdAsIosArray($service_attribute['attribute_id']);

                                    $row['type'] = 'alPickerView';
                                    if ($attributeValue['is_serialized_array']) {
                                        $values = array();
                                        $unserializeValues = unserialize($attributeValue['value']);
                                        foreach ($unserializeValues as $key => $unserializeValue) {
                                            $attributeListValue = $modelAttributeListValue->getById($unserializeValue);
                                            $values[] = array('name' => $attributeListValue['attribute_value'], 'value' => $attributeListValue['attribute_value_id']);
                                        }
                                        $row['field']['value'] = $values;
                                    } else {
                                        $row['field']['value'] = !empty($attributeListValue['attribute_value']) ? $attributeListValue['attribute_value'] : '';
                                    }
                                    $row['field']['enabled'] = "YES";
                                    $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                                    $row['name'] = $name;
                                    $row['label'] = $attribute['attribute_name'];
                                    $row['hiddenField'] = "NO";
                                    $row['data'] = $attributeListValues;
                                    if ($attribute['is_price_attribute']) {
                                        $row['priceAttribute']['name'] = $attribute['attribute_variable_name'];
                                        $row['priceAttribute']['serviceId'] = $service_attribute['service_id'];
                                        $row['priceAttribute']['clone'] = $clone;
                                        $row['priceAttribute']['equasion'] = $service_info['price_equasion'];
                                        $row['priceAttribute']['min_price'] = !empty($service_info['min_price']) ? $service_info['min_price'] : '';
                                    } else {
                                        $row['priceAttribute'] = "NO";
                                    }
                                    $row['validation'] = "";
                                    $rows[] = $row;
                                    break;
                            }
                        }
                    }


                    $row = array();
                    $row['type'] = 'switch';
                    $row['field']['value'] = isset($service['consider_min_price']) ? $service['consider_min_price'] : 0;
                    $row['field']['enabled'] = "YES";
                    $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                    $row['name'] = 'consider_min_price_' . $serviceId . ($clone ? '_' . $clone : '');
                    $row['label'] = 'Consider Minimum Price ($ ' . (!empty($service_info['min_price']) ? $service_info['min_price'] : '') . ')';
                    $row['priceAttribute']['name'] = 'consider_minimum_price';
                    $row['priceAttribute']['serviceId'] = $serviceId;
                    $row['priceAttribute']['clone'] = $clone;
                    $row['priceAttribute']['equasion'] = $service_info['price_equasion'];
                    $row['priceAttribute']['min_price'] = !empty($service_info['min_price']) ? $service_info['min_price'] : '';
                    $row['hiddenField'] = "NO";
                    $row['validation'] = "";
                    $rows[] = $row;

                    $row = array();
                    $row['type'] = 'textField';
                    $row['field']['value'] = round($modelContractorServiceBooking->getServiceBookingQoute($bookingId, $serviceId, $clone), 2);
                    $row['field']['enabled'] = "NO";
                    $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                    $row['name'] = 'total_' . $serviceId . ($clone ? '_' . $clone : '');
                    $row['label'] = 'Total';
                    $row['hiddenField'] = "NO";
                    $row['validation'] = "";
                    $rows[] = $row;

                    $row = array();
                    $row['type'] = 'textField';
                    $row['field']['value'] = round($modelContractorServiceBooking->getTotalServiceBookingQoute($bookingId, $serviceId, $clone), 2);
                    $row['field']['enabled'] = "NO";
                    $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
                    $row['name'] = 'amount_' . $serviceId . ($clone ? '_' . $clone : '');
                    $row['label'] = 'Amount';
                    $row['hiddenField'] = "NO";
                    $row['validation'] = "";
                    $rows[] = $row;

                    $serviceNum++;

                    //fill section
                    $result['sections'][] = array('title' => (!empty($service_info['service_name']) ? $service_info['service_name'] : ''), 'rows' => $rows);
                }
            }

            //
            //section_6
            //
            $rows = array();

            //row_1
            $row = array();
            $row['type'] = 'textField';
            $row['field']['value'] = round(!empty($booking['sub_total']) ? $booking['sub_total'] : 0, 2);
            $row['field']['enabled'] = "NO";
            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
            $row['name'] = 'sub_total';
            $row['label'] = 'Sub Total:';
            $row['hiddenField'] = "NO";
            $row['validation'] = "";
            $rows[] = $row;

            if (!empty($status['name']) && $status['name'] == 'FAILED') {
                $row = array();
                $row['type'] = 'textField';
                $row['field']['value'] = round(!empty($booking['call_out_fee']) ? $booking['call_out_fee'] : 0, 2);
                $row['field']['enabled'] = "YES";
                $row['field']['keyboardType'] = 'UIKeyboardTypeDecimalPad';
                $row['name'] = 'call_out_fee';
                $row['label'] = 'Call Out Fee:';
                $row['hiddenField'] = "NO";
                $row['validation'] = "";
                $rows[] = $row;
            }

            //row_2
            $row = array();
            $row['type'] = 'textField';
            $row['field']['value'] = round(!empty($booking['total_discount']) ? $booking['total_discount'] : 0, 2);
            $row['field']['enabled'] = "YES";
            $row['field']['keyboardType'] = 'UIKeyboardTypeDecimalPad';
            $row['name'] = 'total_discount';
            $row['label'] = 'Discount:';
            $row['hiddenField'] = "NO";
            $row['validation'] = "";
            $rows[] = $row;

            //row_3
            $row = array();
            $row['type'] = 'textField';
            $row['field']['value'] = round(!empty($booking['gst']) ? $booking['gst'] : 0, 2);
            $row['field']['enabled'] = "NO";
            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
            $row['name'] = 'gst';
            $row['label'] = 'GST (10%):';
            $row['hiddenField'] = "NO";
            $row['validation'] = "";
            $rows[] = $row;

            //row_4
            $row = array();
            $row['type'] = 'textField';
            $row['field']['value'] = round(!empty($booking['qoute']) ? $booking['qoute'] : 0, 2);
            $row['field']['enabled'] = "NO";
            $row['field']['keyboardType'] = 'UIKeyboardTypeDefault';
            $row['name'] = 'qoute';
            $row['label'] = 'Total';
            $row['hiddenField'] = "NO";
            $row['validation'] = "";
            $rows[] = $row;

            //fill section
            $result['sections'][] = array('title' => 'Total Booking Cost', 'rows' => $rows);
        }

        return $result;
    }

    public function saveBookingOffline($mode = 'update', $booking) {

        /**
         * get request Params
         */
        //echo 'we are here.......';
        //print_r($booking);
        $title = '';
        $bookingId = isset($booking['booking_id']) ? $booking['booking_id'] : 0;
        $inquiryId = isset($booking['inquiry_id']) ? $booking['inquiry_id'] : 0;
        $toEstimate = isset($booking['toEstimate']) ? $booking['toEstimate'] : 0;
        $st = isset($booking['booking_start']) ? $booking['booking_start'] : "0000-00-00 00:00:00";
        $et = isset($booking['booking_end']) ? $booking['booking_end'] : "0000-00-00 00:00:00";
        $isAllDayEvent = isset($booking['isAllDayEvent']) ? $booking['isAllDayEvent'] : 0;
        $description_booking = isset($booking['description']) ? $booking['description'] : '';
        $cityId = isset($booking['city_id']) ? $booking['city_id'] : 0;
        $statusName = isset($booking['status']) ? $booking['status'] : 0;
        //By Islam
        $modelBookingStatus = new Model_BookingStatus();
        $status = $modelBookingStatus->getByStatusName($statusName);
        $statusId = $status['booking_status_id'];

        $customer_id = isset($booking['customer_id']) ? $booking['customer_id'] : 0;
        $totalDiscount = isset($booking['total_discount']) ? $booking['total_discount'] : 0;
        $onsiteClientName = isset($booking['onsite_client_name']) ? $booking['onsite_client_name'] : '';
        $satisfaction = isset($booking['satisfaction']) ? $booking['satisfaction'] : 1;
        $jobStartTime = isset($booking['job_start_time']) ? $booking['job_start_time'] : $st;
        $jobFinishTime = isset($booking['job_finish_time']) ? $booking['job_finish_time'] : $et;
        $services = isset($booking['services']) ? (array) $booking['services'] : array();
        $services = (array) $services;
        $product_ids = isset($booking['product']) ? $booking['product'] : array();
        $ltrs = isset($booking['ltr']) ? $booking['ltr'] : array();
        $propertyTypeId = isset($booking['property_type']) ? $booking['property_type'] : 0;
        $callOutFee = isset($booking['call_out_fee']) ? $booking['call_out_fee'] : 0;
        $to_follow = isset($booking['to_follow']) ? $booking['to_follow'] : '';
        $is_to_follow = isset($booking['is_to_follow']) ? $booking['is_to_follow'] : 0;
        $multi_stpartdate = isset($booking['multi_stpartdate']) ? $booking['multi_stpartdate'] : 0;
        $confirmResetAttribut = isset($booking['confirmResetAttribut']) ? $booking['confirmResetAttribut'] : 0;
        $BookingDates = isset($booking['BookingDates']) ? $booking['BookingDates'] : 0;
        $date_visited = isset($booking['is_visited']) ? $booking['is_visited'] : 0;
        $extra_comment = isset($booking['extra_comment']) ? $booking['extra_comment'] : 0;
        $products = isset($booking['products']) ? $booking['products'] : 0;

        /*
          $bookingId = $this->request->getParam('booking_id', 0);
          $inquiryId = $this->request->getParam('inquiry_id', 0);
          $toEstimate = $this->request->getParam('toEstimate', 0);
          $st = $this->request->getParam('booking_start', "0000-00-00 00:00:00");
          $et = $this->request->getParam('booking_end', "0000-00-00 00:00:00");
          $isAllDayEvent = $this->request->getParam('isAllDayEvent', 0);
          $description_booking = $this->request->getParam('description', '');
          $cityId = $this->request->getParam('city_id', 0);
          $statusId = $this->request->getParam('bookingStatus', 0);
          $customer_id = $this->request->getParam('customer_id', 0);
          $totalDiscount = $this->request->getParam('total_discount', 0);
          $onsiteClientName = $this->request->getParam('onsite_client_name', '');
          $satisfaction = $this->request->getParam('satisfaction', 1);
          $jobStartTime = $this->request->getParam('job_start_time', $st);
          $jobFinishTime = $this->request->getParam('job_finish_time', $et);

          $services = $this->request->getParam('services', array());
          $services = json_decode($services,true);
          $product_ids = $this->request->getParam('product', array());
          $ltrs = $this->request->getParam('ltr', array());
          $propertyTypeId = $this->request->getParam('property_type', 0);
          $callOutFee = $this->request->getParam('call_out_fee', 0);
          $to_follow = $this->request->getParam('to_follow', '');
          $is_to_follow = $this->request->getParam('is_to_follow', 0);
          $multi_stpartdate = $this->request->getParam('multi_stpartdate');
          $confirmResetAttribut = $this->request->getParam('confirmResetAttribut', 0);
         */

        $postArr = array(
            'booking_id' => $bookingId,
            'inquiry_id' => $inquiryId,
            'toEstimate' => $toEstimate,
            'booking_start' => $st,
            'booking_end' => $et,
            'isAllDayEvent' => $isAllDayEvent,
            'description' => $description_booking,
            'city_id' => $cityId,
            'bookingStatus' => $statusId,
            'customer_id' => $customer_id,
            'total_discount' => $totalDiscount,
            'onsite_client_name' => $onsiteClientName,
            'satisfaction' => $satisfaction,
            'job_start_time' => $jobStartTime,
            'job_finish_time' => $jobFinishTime,
            'services' => $services,
            'product' => $product_ids,
            'ltr' => $ltrs,
            'property_type' => $propertyTypeId,
            'call_out_fee' => $callOutFee,
            'to_follow' => $to_follow,
            'is_to_follow' => $is_to_follow,
            'multi_stpartdate' => $multi_stpartdate,
            'confirmResetAttribut' => $confirmResetAttribut
        );
        //print_r($postArr);
        //echo 'rrrrrrrrrrrrrr';
        /**
         * get Logged User && Company
         */
        $loggedUser = CheckAuth::getLoggedUser();
        $companyId = CheckAuth::getCompanySession();

        /**
         * load models
         */
        $modelBooking = new Model_Booking();
        $modelBookingStatus = new Model_BookingStatus();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelRefund = new Model_Refund();
        $modelBookingEstimate = new Model_BookingEstimate();
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelCompanies = new Model_Companies();
        $modelBookingStatusHistory = new Model_BookingStatusHistory();
        $modelBookingDiscussion = new Model_BookingDiscussion();
        $modelBookingMultipleDays = new Model_BookingMultipleDays();
        $modelVisitedExtraInfo = new Model_VisitedExtraInfo();
        $modelBookingProduct = new Model_BookingProduct();

        /**
         * Get Booking Status
         */
        $completed = $modelBookingStatus->getByStatusName('COMPLETED');
        $faild = $modelBookingStatus->getByStatusName('FAILED');
        $to_do = $modelBookingStatus->getByStatusName('TO DO');
        $quoted = $modelBookingStatus->getByStatusName('QUOTED');
        $toVisit = $modelBookingStatus->getByStatusName('TO VISIT');

        /**
         * get old Booking
         */
        $oldBooking = $modelBooking->getById($bookingId);

        /**
         * previous
         */
        $oldStatusId = isset($oldBooking['status_id']) ? $oldBooking['status_id'] : 0;
        $oldCallOutFee = isset($oldBooking['call_out_fee']) ? $oldBooking['call_out_fee'] : 0;
        $oldTotalDiscount = isset($oldBooking['total_discount']) ? $oldBooking['total_discount'] : 0;

        /**
         * validation
         */
        $errorMessages = array();
        $returnData = array();
        if ($this->validaterOffline($errorMessages, $postArr)) {
            //echo "validate returns true ";
            /**
             * prepare data to be saved on database
             */
            $db_params = array();
            $db_params['status_id'] = $statusId;
            //$db_params['onsite_client_name'] = $onsiteClientName;
            //$db_params['job_start_time'] = strtotime($jobStartTime);
            //$db_params['job_finish_time'] = strtotime($jobFinishTime);
            //$db_params['satisfaction'] = $satisfaction;

            /**
             * contractor can't change booking Details
             * in update mode
             */
            /* if($modelBooking->checkCanEditBookingDetails($bookingId)){
              echo 'can edit details returns true  ';
              }
              else{
              echo 'can edit details returns NO  ';
              } */

            if ($mode == 'update') {
                $visited_info_id = $oldBooking['visited_extra_info_id'];


                $date_fields = array(
                    'job_start' => $jobStartTime,
                    'job_end' => $jobFinishTime,
                    'booking_id' => $bookingId,
                    'onsite_client_name' => $onsiteClientName,
                    'is_visited' => $date_visited
                );



                //echo $visited_info_id;

                if ($visited_info_id) {
                    $modelVisitedExtraInfo->updateById($visited_info_id, $date_fields);
                } else {
                    $visited_info_id = $modelVisitedExtraInfo->insert($date_fields);
                }


                if (!empty($extra_comment)) {

                    if ($visited_info_id) {
                        $bookingDiscussion = $modelBookingDiscussion->getByExtraInfoId($visited_info_id);

                        if ($bookingDiscussion) {
                            $modelBookingDiscussion->updateById($bookingDiscussion['discussion_id'], array('user_id' => $loggedUser['user_id'], 'user_message' => $extra_comment));
                        } else {
                            $modelBookingDiscussion->insert(array('booking_id' => $bookingId, 'user_id' => $loggedUser['user_id'], 'user_message' => $extra_comment, 'visited_extra_info_id' => $visited_info_id, 'created' => time()));
                        }
                    } else {
                        $modelBookingDiscussion->insert(array('booking_id' => $bookingId, 'user_id' => $loggedUser['user_id'], 'user_message' => $extra_comment, 'visited_extra_info_id' => $visited_info_id, 'created' => time()));
                    }
                }

                if ($date_visited) {
                    $date_products = array();
                    $date_ltr = array();
                    foreach ($products as $key => $product) {
                        $date_products[$key] = $product['product_id'];
                        $date_ltr[$key] = $product['ltr'];
                    }

                    if ($date_products) {
                        if ($visited_info_id) {
                            $delete_old = 0;
                            $modelBookingProduct = new Model_BookingProduct();
                            $modelBookingProduct->setProductsToBooking($bookingId, $date_products, $date_ltr, $visited_info_id, $delete_old, $this->logId);
                        }
                    }
                }

                echo"before visited_extra_info_id";
                $modelBooking->updateById($bookingId, array('visited_extra_info_id' => $visited_info_id), false, $log_id = 0, $os = 'ios');
                echo"after visited_extra_info_id";

                //var_dump($BookingDates);
                foreach ($BookingDates as $key => $extra_info) {
                    $bookingId = $extra_info['booking_id'];
                    $is_visited = isset($extra_info['is_visited']) ? $extra_info['is_visited'] : 0;
                    $is_new = isset($extra_info['is_new']) ? $extra_info['is_new'] : 0;
                    $is_deleted = isset($extra_info['is_deleted']) ? $extra_info['is_deleted'] : 0;
                    $multipleDay = $modelBookingMultipleDays->getById($extra_info['id']);
                    $visited_info_id = $multipleDay['visited_extra_info_id'];
                    if ($is_visited && $is_deleted) {
                        $modelVisitedExtraInfo->deleteById($multipleDay['visited_extra_info_id']);
                        if (!empty($extra_info['extra_comments'])) {
                            $modelBookingDiscussion->deleteByExtraInfoId($multipleDay['visited_extra_info_id']);
                        }
                        $products = $extra_info['products'];
                        if ($products) {
                            $modelBookingProduct->deleteByExtraInfoId($multipleDay['visited_extra_info_id']);
                        }

                        $modelBookingMultipleDays->deleteById($extra_info['id']);
                    } else if ($is_deleted && !($is_visited)) {
                        $modelBookingMultipleDays->deleteById($extra_info['id']);
                    } else if (!($is_deleted)) {
                        $date_extra_comments = $extra_info['extra_comments'];
                        $date_extra_comments = trim($date_extra_comments);
                        $visisted_extra_info = array(
                            'job_start' => $extra_info['job_start_time'],
                            'job_end' => $extra_info['job_finish_time'],
                            'onsite_client_name' => $extra_info['onsite_client_name'],
                            'booking_id' => $extra_info['booking_id'],
                            'is_visited' => $is_visited
                        );

                        if ($visited_info_id) {
                            $modelVisitedExtraInfo->updateById($visited_info_id, $visisted_extra_info);
                        } else {
                            $visited_info_id = $modelVisitedExtraInfo->insert($visisted_extra_info);
                        }

                        if ($is_visited) {
                            $products = $extra_info['products'];
                            $date_products = array();
                            $date_ltr = array();
                            foreach ($products as $k => $product) {
                                $date_products[$k] = $product['product_id'];
                                $date_ltr[$k] = $product['ltr'];
                            }
                            if ($date_products) {
                                if ($visited_info_id) {
                                    $delete_old = 0;

                                    $modelBookingProduct = new Model_BookingProduct();
                                    $modelBookingProduct->setProductsToBooking($bookingId, $date_products, $date_ltr, $visited_info_id, $delete_old, $this->logId);
                                }
                            }
                        }

                        $db_param = array();
                        //echo strtotime($extra_info['booking_start']);
                        $booking_start = date("Y-m-d H:i:s", strtotime($extra_info['booking_start']));
                        $booking_end = date("Y-m-d H:i:s", strtotime($extra_info['booking_end']));
                        $db_param['booking_start'] = $booking_start;
                        $db_param['booking_end'] = $booking_end;
                        $db_param['booking_id'] = $bookingId;
                        $db_param['visited_extra_info_id'] = $visited_info_id;
                        if ($is_new) {
                            $result = $modelBookingMultipleDays->insert($db_param);
                        } else {
                            $result = $modelBookingMultipleDays->updateById($extra_info['id'], $db_param);
                        }


                        if (!empty($date_extra_comments)) {

                            if ($visited_info_id) {

                                $bookingDiscussion = $modelBookingDiscussion->getByExtraInfoId($visited_info_id);
                                if ($bookingDiscussion) {

                                    $modelBookingDiscussion->updateById($bookingDiscussion['discussion_id'], array('user_id' => $loggedUser['user_id'], 'user_message' => $date_extra_comments));
                                } else {

                                    $modelBookingDiscussion->insert(array('booking_id' => $bookingId, 'user_id' => $loggedUser['user_id'], 'user_message' => $date_extra_comments, 'visited_extra_info_id' => $visited_info_id, 'created' => time()));
                                }
                            } else {

                                $modelBookingDiscussion->insert(array('booking_id' => $bookingId, 'user_id' => $loggedUser['user_id'], 'user_message' => $date_extra_comments, 'visited_extra_info_id' => $visited_info_id, 'created' => time()));
                            }
                        }
                    }
                }
            }


            if (($mode == 'create') || (($mode == 'update') && $modelBooking->checkCanEditBookingDetails($bookingId))) {
                //echo 'prepare parms....';
                $db_params['customer_id'] = $customer_id;
                $db_params['booking_start'] = php2MySqlTime(strtotime($st));
                $db_params['booking_end'] = php2MySqlTime(strtotime($et));
                $db_params['title'] = $title;
                $db_params['is_all_day_event'] = $isAllDayEvent ? 1 : 0;
                $db_params['description'] = $description_booking;
                $db_params['city_id'] = $cityId;
                $db_params['property_type_id'] = $propertyTypeId;
                $db_params['to_follow'] = $to_follow ? strtotime($to_follow) : 0;
                $db_params['is_to_follow'] = $is_to_follow ? 1 : 0;
            }

            /**
             * delete google Calendar event if status is check as delete google calender like on hold or canceled 
             */
            /* $deleteGoogleCalender = $modelBookingStatus->getDeleteGoogleCalender();
              //print_r($deleteGoogleCalender);
              if (in_array($statusId, $deleteGoogleCalender)) {
              $modelGoogleCalendarEvent = new Model_GoogleCalendarEvent();
              $modelGoogleCalendarEvent->deleteGoogleCalendarEventByBookingId($bookingId);
              } */
            //echo 'test ';
            /**
             * if booking changed from TO Visit to TO DO update created
             */
            if (($mode == 'update') && $statusId == $to_do['booking_status_id'] && $oldStatusId) {
                if ($oldStatusId == $toVisit['booking_status_id']) {
                    $db_params['created'] = time();
                }
            }

            /**
             * is_multiple_days
             */
            if ($multi_stpartdate) {
                $db_params['is_multiple_days'] = 1;
            } else {
                $db_params['is_multiple_days'] = 0;
            }

            /**
             * save item in the database
             */
            //echo 'update item....';
            if ($mode == 'create') {
                //echo 'save item....';
                $db_params['created_by'] = $loggedUser['user_id'];
                $db_params['created'] = time();
                $db_params['company_id'] = $companyId;
                $db_params['original_inquiry_id'] = $inquiryId;

                $returnData = $modelBooking->addDetailedCalendar($db_params);
                $this->logId = $returnData['log'];
            } else {
                //echo 'update item....';

                echo"before call updateDetailedCalendar";
                $returnData = $modelBooking->updateDetailedCalendar($bookingId, $db_params, $os = 'ios');
                echo"after call updateDetailedCalendar";

                $this->logId = $returnData['log'];
                //echo 'update item....eeee';
            }
            //echo 'test222 ';
            //print_r($returnData);
            if (isset($returnData['Data']) && $returnData['Data']) {
                //print_r($returnData['Data']);
                $bookingId = (int) $returnData['Data'];

                /**
                 * save  booking status history
                 */
                if ($mode == 'create') {
                    $modelBookingStatusHistory = new Model_BookingStatusHistory();
                    $modelBookingStatusHistory->addStatusHistory($bookingId, $statusId);
                }
                //echo 'booking history done';

                /**
                 * insert booking services and his quote and quantity
                 */
                if (($mode == 'create') || (($mode == 'update') && $modelBooking->checkCanEditBookingDetails($bookingId))) {
                    $modelContractorServiceBooking->setServicesToBookingOffline($bookingId, $services, $this->logId);
                }
                //echo 'booking services done';
                /**
                 * save multiple days
                 */
                $modelBookingMultipleDays = new Model_BookingMultipleDays();
                $modelBookingMultipleDays->saveMultipleDays($bookingId);

                /**
                 * save address
                 */
                echo"before call saveAddress ios";
                $this->saveAddress($booking, $bookingId, $mode, $this->logId, $os = 'ios');
                echo"after call saveAddress ios";
                //echo 'booking address done';
                /**
                 * set product to booking
                 */
                if ($product_ids) {
                    $modelBookingProduct = new Model_BookingProduct();
                    $modelBookingProduct->setProductsToBooking($bookingId, $product_ids, $ltrs, 0, $this->logId);
                }
                //echo 'booking products done';
                /**
                 * save why booking status
                 */
                //,$os='ios'   
//				 public function saveWhyStatus($booking = array(), $bookingId, $statusId, $addLog = true, $user_role = 'contractor') {
                echo"before call saveWhyStatus ios";

                $this->saveWhyStatus($booking, $bookingId, $statusId, false, $user_role = 'contractor', $this->logId, $os = 'ios');
                echo"after call saveWhyStatus ios";

                /**
                 * convert To Estimate if status is qouted
                 */
                // echo 'booking estimates done';
                if ($statusId == $quoted['booking_status_id']) {
                    //    public function convertToEstimate($bookingId, $toFollow = false, $addLog = true, $isToFollow = false,$os='') {
                    echo"before call convertToEstimate ios";

                    $modelBookingEstimate->convertToEstimate($bookingId, false, false, false, $this->logId, $os = 'ios');
                    echo"after call convertToEstimate ios";
                }
                //echo 'booking lll estimated done';
                /**
                 * delete Estimate if status is not quoted
                 */
                if ($mode == 'update' && $statusId != $quoted['booking_status_id']) {
                    echo"before call changedEstimateToBooking ios";

                    $modelBookingEstimate->changedEstimateToBooking($bookingId, $statusId, false, $this->logId, $os = 'ios');
                    echo"after call changedEstimateToBooking ios";
                }
                //echo 'booking Estimate if status is not quoted done';
                /**
                 * convert To Invoice if booking status is complete or faild
                 */
                if ($statusId == $completed['booking_status_id'] || $statusId == $faild['booking_status_id']) {
                    echo"before call convertToInvoice ios";

                    $modelBookingInvoice->convertToInvoice($bookingId, false, $this->logId, $os = 'ios');
                    echo"after call convertToInvoice ios";
                }
                //echo 'booking convert To Invoice if booking status is complete or faild done';
                /**
                 * set Quantity and Discount attribute value to Zero when booking status is faild
                 */
                if ($statusId == $faild['booking_status_id']) {
                    if ($confirmResetAttribut) {
                        $modelContractorServiceBooking->changeAttributIfFaildOffline($bookingId, $services, $this->logId);
                    }
                } else {
                    $callOutFee = 0;
                }


                /**
                 *  insert services in  the temp until approved when the booking is update by contractor
                 */
                $totalDiscountTemp = 0;
                $callOutFeeTemp = 0;
                $db_params = array();
                if ((($mode == 'update') && !$modelBooking->checkCanEditBookingDetails($bookingId))) {
                    /* echo 'by islam';
                      var_dump($services);
                      echo 'by islam22222'; */
                    $modelContractorServiceBookingTemp = new Model_ContractorServiceBookingTemp();

                    $modelContractorServiceBookingTemp->setServicesToBookingOffline($bookingId, $services, $this->logId, $os = 'ios');

                    /**
                     * if the contractor change call out fee value ,it will saved in the temp ,
                     * the original call out fee get from it's company  and saved in the booking
                     */
                    $callOutFeeTemp = $callOutFee;
                    $companies = $modelCompanies->getById(CheckAuth::getCompanySession());
                    if ($callOutFee) {
                        if ($oldCallOutFee) {
                            $callOutFee = $oldCallOutFee;
                        } else {
                            $callOutFee = $companies['call_out_fee'];
                        }
                        if ($callOutFeeTemp != $callOutFee) {
                            $db_params['is_change'] = 1;
                        }
                    }

                    // get the total discount from original booking because the gst and the total calculate from it.
                    $totalDiscountTemp = $totalDiscount;

                    if (isset($totalDiscount) && $oldTotalDiscount != $totalDiscount) {
                        $totalDiscount = $oldTotalDiscount;
                        $db_params['is_change'] = 1;
                    }

                    if ($statusId != $oldStatusId) {
                        $db_params['is_change'] = 1;
                    }
                }


                /**
                 * save calculation qoute
                 */
                $totalRefund = $modelRefund->getTotalRefund(array('booking_id' => $bookingId, 'is_approved' => 'yes'));
                $subTotal = $modelContractorServiceBooking->getTotalBookingQoute($bookingId);
                /* $total = ($subTotal + $callOutFee) - $totalDiscount;
                  $gstTax = $total * get_config('gst_tax');
                  $totalQoute = $total + $gstTax;
                  $totalQoute = $totalQoute - $totalRefund; */


                $total = ($subTotal + $callOutFee);
                $gstTax = $total * get_config('gst_tax');
                $totalQoute = $total + $gstTax - $totalDiscount;
                $totalQoute = $totalQoute - $totalRefund;



                $db_params['sub_total'] = round($subTotal, 2);
                $db_params['gst'] = round($gstTax, 2);
                $db_params['qoute'] = round($totalQoute, 2);
                $db_params['call_out_fee'] = round($callOutFee, 2);
                $db_params['refund'] = round($totalRefund, 2);
                $db_params['total_discount'] = round($totalDiscount, 2);
                $db_params['total_discount_temp'] = round($totalDiscountTemp, 2);
                $db_params['call_out_fee_temp'] = round($callOutFeeTemp, 2);


                /**
                 * generate Title
                 */
                //$title = $this->generateTitle($services, $cityId, number_format($totalQoute, 2));
                $db_params['title'] = 'title disabled....';



                /**
                 * update saved Booking
                 */
                echo"before isSuccess ";
                $isSuccess = $modelBooking->updateById($bookingId, $db_params, false, $this->logId, $os = 'ios');
                echo"after isSuccess ";


                /**
                 * send Booking To Gmail Acc if status is completed or faild or to_do or in_process
                 */
                /* $pushGoogleCalender = $modelBookingStatus->getPushGoogleCalender();
                  if (in_array($statusId, $pushGoogleCalender)) {
                  $modelGoogleCalendarEvent = new Model_GoogleCalendarEvent();
                  $modelGoogleCalendarEvent->sendCreatedBooking($bookingId);
                  } */
            }

            if ($mode == 'create' && $inquiryId) {
                $modelInquiry = new Model_Inquiry();

                $data = array(
                    'status' => 'booking',
                    'deferred_date' => 0
                );
                $modelInquiry->updateById($inquiryId, $data);

                if ($toEstimate) {
                    $data = array(
                        'status' => 'estimate',
                        'deferred_date' => 0
                    );
                    $modelInquiry->updateById($inquiryId, $data);
                }
            }
            //if($isSuccess){
            ////return the booking in the response
            $booking = $this->getBooking(false, array('booking_id' => $bookingId));
            $returnData['IsSuccess'] = true;
            $returnData['Msg'] = $booking;
            //}
        } else {
            //echo "validate returns true ";

            $returnData['IsSuccess'] = false;
            $returnData['Msg'] = $errorMessages;
        }
        //echo "validate returns true22 ";
        //print_r($returnData);
        return $returnData;
    }

    ///////////////////////////
    public function saveBooking() {

        $bookingId = $this->request->getParam('booking_id', 0);

        $returnData = $this->save('update');

        $success = TRUE;
        if (empty($returnData['IsSuccess'])) {
            $success = FALSE;
        }

        if ($success) {
            return array('type' => 'success', 'message' => "Saved successfully", 'result' => $this->getBooking(false, array('booking_id' => $bookingId)));
        } else {
            $message = "Error in Saving Booking";
            if (isset($returnData['Msg'])) {
                if (is_array($returnData['Msg'])) {
                    foreach ($returnData['Msg'] as $msg) {
                        $message .= " ({$msg})";
                    }
                }
            }
            return array('type' => 'error', 'message' => $message);
        }
    }

    // $result = $this->save($mode = 'update', $booking, 'android');

    public function save($mode, $booking = array(), $os = 'ios') {

        //print_r($booking);
        /**
         * get request Params
         */
        // $postData = $this->request->getPost();
        //print_r($postData);

        if (empty($booking)) {
            $title = '';
            $bookingId = $this->request->getParam('booking_id', 0);
            $inquiryId = $this->request->getParam('inquiry_id', 0);
            $toEstimate = $this->request->getParam('toEstimate', 0);
            $st = $this->request->getParam('booking_start', "0000-00-00 00:00:00");
            $et = $this->request->getParam('booking_end', "0000-00-00 00:00:00");
            $isAllDayEvent = $this->request->getParam('isAllDayEvent', 0);
            $description_booking = $this->request->getParam('description', '');
            $cityId = $this->request->getParam('city_id', 0);
            $statusId = $this->request->getParam('bookingStatus', 0);
            $customer_id = $this->request->getParam('customer_id', 0);
            $totalDiscount = $this->request->getParam('total_discount', 0);
            $onsiteClientName = $this->request->getParam('onsite_client_name', '');
            $satisfaction = $this->request->getParam('satisfaction', 1);
            $jobStartTime = $this->request->getParam('job_start_time', $st);
            $jobFinishTime = $this->request->getParam('job_finish_time', $et);
            $services = $this->request->getParam('services', array());
            $product_ids = $this->request->getParam('product', array());
            $ltrs = $this->request->getParam('ltr', array());
            $propertyTypeId = $this->request->getParam('property_type', 0);
            $callOutFee = $this->request->getParam('call_out_fee', 0);
            $to_follow = $this->request->getParam('to_follow', '');
            $is_to_follow = $this->request->getParam('is_to_follow', 0);
            $multi_stpartdate = $this->request->getParam('multi_stpartdate');
            $confirmResetAttribut = $this->request->getParam('confirmResetAttribut', 0);
        } else {
            $title = '';
            $bookingId = $this->request->getParam('booking_id', 0);
            $inquiryId = isset($booking['inquiry_id']) ? $booking['inquiry_id'] : 0;
            $toEstimate = isset($booking['toEstimate']) ? $booking['toEstimate'] : 0;
            $st = isset($booking['booking_start']) ? $booking['booking_start'] : "0000-00-00 00:00:00";
            $et = isset($booking['booking_end']) ? $booking['booking_end'] : "0000-00-00 00:00:00";
            $isAllDayEvent = isset($booking['isAllDayEvent']) ? $booking['isAllDayEvent'] : 0;
            $description_booking = isset($booking['description']) ? $booking['description'] : '';
            $cityId = isset($booking['city_id']) ? $booking['city_id'] : 0;
            $statusId = isset($booking['status']) ? $booking['status'] : 0;
            $customer_id = isset($booking['customer_id']) ? $booking['customer_id'] : 0;
            $totalDiscount = isset($booking['total_discount']) ? $booking['total_discount'] : 0;
            $onsiteClientName = isset($booking['onsite_client_name']) ? $booking['onsite_client_name'] : '';
            $satisfaction = isset($booking['satisfaction']) ? $booking['satisfaction'] : 1;
            $jobStartTime = isset($booking['job_start_time']) ? $booking['job_start_time'] : $st;
            $jobFinishTime = isset($booking['job_finish_time']) ? $booking['job_finish_time'] : $et;
            $services = isset($booking['services']) ? (array) $booking['services'] : array();
            $services = (array) $services;
            $product_ids = isset($booking['product']) ? $booking['product'] : array();
            $ltrs = isset($booking['ltr']) ? $booking['ltr'] : array();
            $propertyTypeId = isset($booking['property_type']) ? $booking['property_type'] : 0;
            $callOutFee = isset($booking['call_out_fee']) ? $booking['call_out_fee'] : 0;
            $to_follow = isset($booking['to_follow']) ? $booking['to_follow'] : '';
            $is_to_follow = isset($booking['is_to_follow']) ? $booking['is_to_follow'] : 0;
            $multi_stpartdate = isset($booking['multi_stpartdate']) ? $booking['multi_stpartdate'] : 0;
            $confirmResetAttribut = isset($booking['confirmResetAttribut']) ? $booking['confirmResetAttribut'] : 0;
            $BookingDates = isset($booking['BookingDates']) ? $booking['BookingDates'] : 0;
            $date_visited = isset($booking['is_visited']) ? $booking['is_visited'] : 0;
            $extra_comment = isset($booking['extra_comment']) ? $booking['extra_comment'] : 0;
            $extra_comment = trim($extra_comment);
            $products = isset($booking['products']) ? $booking['products'] : 0;



            //echo '$statusId   '.$statusId;
            //exit;
        }

        //echo 'islam test ......';
        //print_r($booking);
        /**
         * get Logged User && Company
         */
        $loggedUser = CheckAuth::getLoggedUser();
        $companyId = CheckAuth::getCompanySession();

        /**
         * load models
         */
        $modelBooking = new Model_Booking();
        $modelBookingStatus = new Model_BookingStatus();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelRefund = new Model_Refund();
        $modelBookingEstimate = new Model_BookingEstimate();
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelCompanies = new Model_Companies();
        $modelBookingStatusHistory = new Model_BookingStatusHistory();
        $modelBookingDiscussion = new Model_BookingDiscussion();
        $modelBookingMultipleDays = new Model_BookingMultipleDays();
        $modelVisitedExtraInfo = new Model_VisitedExtraInfo();
        $modelBookingProduct = new Model_BookingProduct();

        /**
         * Get Booking Status
         */
        $completed = $modelBookingStatus->getByStatusName('COMPLETED');
        $faild = $modelBookingStatus->getByStatusName('FAILED');
        $to_do = $modelBookingStatus->getByStatusName('TO DO');
        $quoted = $modelBookingStatus->getByStatusName('QUOTED');
        $toVisit = $modelBookingStatus->getByStatusName('TO VISIT');



        /**
         * get old Booking
         */
        $oldBooking = $modelBooking->getById($bookingId);



        /**
         * previous
         */
        $oldStatusId = isset($oldBooking['status_id']) ? $oldBooking['status_id'] : 0;
        $oldCallOutFee = isset($oldBooking['call_out_fee']) ? $oldBooking['call_out_fee'] : 0;
        $oldTotalDiscount = isset($oldBooking['total_discount']) ? $oldBooking['total_discount'] : 0;

        /**
         * validation
         */
        $errorMessages = array();
        $returnData = array();
        //echo 'before vaildator';


        if ($mode == 'update' && $os == 'android') {

            $visited_info_id = $oldBooking['visited_extra_info_id'];


            $date_fields = array(
                'job_start' => $jobStartTime,
                'job_end' => $jobFinishTime,
                'booking_id' => $bookingId,
                'onsite_client_name' => $onsiteClientName,
                'is_visited' => $date_visited
            );


            if ($visited_info_id) {
                $modelVisitedExtraInfo->updateById($visited_info_id, $date_fields);
            } else {
                $visited_info_id = $modelVisitedExtraInfo->insert($date_fields);
            }


            if (!empty($extra_comment)) {

                if ($visited_info_id) {
                    $bookingDiscussion = $modelBookingDiscussion->getByExtraInfoId($visited_info_id);

                    if ($bookingDiscussion) {
                        $modelBookingDiscussion->updateById($bookingDiscussion['discussion_id'], array('user_id' => $loggedUser['user_id'], 'user_message' => $extra_comment));
                    } else {
                        $modelBookingDiscussion->insert(array('booking_id' => $bookingId, 'user_id' => $loggedUser['user_id'], 'user_message' => $extra_comment, 'visited_extra_info_id' => $visited_info_id, 'created' => time()));
                    }
                } else {
                    $modelBookingDiscussion->insert(array('booking_id' => $bookingId, 'user_id' => $loggedUser['user_id'], 'user_message' => $extra_comment, 'visited_extra_info_id' => $visited_info_id, 'created' => time()));
                }
            }

            if ($date_visited) {
                $date_products = array();
                $date_ltr = array();
                foreach ($products as $key => $product) {
                    $date_products[$key] = $product['product_id'];
                    $date_ltr[$key] = $product['ltr'];
                }

                if ($date_products) {
                    if ($visited_info_id) {
                        $delete_old = 0;
                        $modelBookingProduct = new Model_BookingProduct();
                        $modelBookingProduct->setProductsToBooking($bookingId, $date_products, $date_ltr, $visited_info_id, $delete_old, $this->logId);
                    }
                }
            }

            $modelBooking->updateById($bookingId, array('visited_extra_info_id' => $visited_info_id), false, $log_id = 0, $os);


            foreach ($BookingDates as $key => $extra_info) {
                $bookingId = $extra_info['booking_id'];
                $is_visited = isset($extra_info['is_visited']) ? $extra_info['is_visited'] : 0;
                $is_new = isset($extra_info['is_new']) ? $extra_info['is_new'] : 0;
                $is_deleted = isset($extra_info['is_deleted']) ? $extra_info['is_deleted'] : 0;
                $multipleDay = $modelBookingMultipleDays->getById($extra_info['id']);
                $visited_info_id = $multipleDay['visited_extra_info_id'];
                if ($is_visited && $is_deleted) {
                    $modelVisitedExtraInfo->deleteById($multipleDay['visited_extra_info_id']);
                    if (!empty($extra_info['extra_comments'])) {
                        $modelBookingDiscussion->deleteByExtraInfoId($multipleDay['visited_extra_info_id']);
                    }
                    $products = $extra_info['products'];
                    if ($products) {
                        $modelBookingProduct->deleteByExtraInfoId($multipleDay['visited_extra_info_id']);
                    }

                    $modelBookingMultipleDays->deleteById($extra_info['id']);
                } else if ($is_deleted && !($is_visited)) {
                    $modelBookingMultipleDays->deleteById($extra_info['id']);
                } else if (!($is_deleted)) {
                    $date_extra_comments = $extra_info['extra_comments'];
                    $date_extra_comments = trim($date_extra_comments);

                    $visisted_extra_info = array(
                        'job_start' => $extra_info['job_start_time'],
                        'job_end' => $extra_info['job_finish_time'],
                        'onsite_client_name' => $extra_info['onsite_client_name'],
                        'booking_id' => $extra_info['booking_id'],
                        'is_visited' => $is_visited
                    );


                    if ($visited_info_id) {
                        $modelVisitedExtraInfo->updateById($visited_info_id, $visisted_extra_info);
                    } else {
                        $visited_info_id = $modelVisitedExtraInfo->insert($visisted_extra_info);
                    }





                    if ($is_visited) {
                        $products = $extra_info['products'];
                        $date_products = array();
                        $date_ltr = array();
                        foreach ($products as $k => $product) {
                            $date_products[$k] = $product['product_id'];
                            $date_ltr[$k] = $product['ltr'];
                        }
                        if ($date_products) {
                            if ($visited_info_id) {
                                $delete_old = 0;

                                $modelBookingProduct = new Model_BookingProduct();
                                $modelBookingProduct->setProductsToBooking($bookingId, $date_products, $date_ltr, $visited_info_id, $delete_old, $this->logId);
                            }
                        }
                    }

                    $db_param = array();
                    //echo strtotime($extra_info['booking_start']);
                    $booking_start = date("Y-m-d H:i:s", strtotime($extra_info['booking_start']));
                    $booking_end = date("Y-m-d H:i:s", strtotime($extra_info['booking_end']));
                    $db_param['booking_start'] = $booking_start;
                    $db_param['booking_end'] = $booking_end;
                    $db_param['booking_id'] = $bookingId;
                    $db_param['visited_extra_info_id'] = $visited_info_id;
                    if ($is_new) {
                        $result = $modelBookingMultipleDays->insert($db_param);
                    } else {
                        $result = $modelBookingMultipleDays->updateById($extra_info['id'], $db_param);
                    }


                    if (!empty($date_extra_comments)) {

                        if ($visited_info_id) {

                            $bookingDiscussion = $modelBookingDiscussion->getByExtraInfoId($visited_info_id);
                            if ($bookingDiscussion) {

                                $modelBookingDiscussion->updateById($bookingDiscussion['discussion_id'], array('user_id' => $loggedUser['user_id'], 'user_message' => $date_extra_comments));
                            } else {

                                $modelBookingDiscussion->insert(array('booking_id' => $bookingId, 'user_id' => $loggedUser['user_id'], 'user_message' => $date_extra_comments, 'visited_extra_info_id' => $visited_info_id, 'created' => time()));
                            }
                        } else {

                            $modelBookingDiscussion->insert(array('booking_id' => $bookingId, 'user_id' => $loggedUser['user_id'], 'user_message' => $date_extra_comments, 'visited_extra_info_id' => $visited_info_id, 'created' => time()));
                        }
                    }
                }
            }
        }





        if ($this->validater($errorMessages, $booking)) {

            //echo "validate returns true ";
            /**
             * prepare data to be saved on database
             */
            $db_params = array();
            $db_params['status_id'] = $statusId;
            $db_params['onsite_client_name'] = $onsiteClientName;
            $db_params['job_start_time'] = strtotime($jobStartTime);
            $db_params['job_finish_time'] = strtotime($jobFinishTime);
            $db_params['satisfaction'] = $satisfaction;

            /**
             * contractor can't change booking Details
             * in update mode
             */
            /* if($modelBooking->checkCanEditBookingDetails($bookingId)){
              echo 'can edit details returns true  ';
              }
              else{
              echo 'can edit details returns NO  ';
              } */


            //echo $modelBooking->checkCanEditBookingDetails($bookingId);

            if (($mode == 'create') || (($mode == 'update') && $modelBooking->checkCanEditBookingDetails($bookingId))) {
                //echo 'inside mode';

                $db_params['customer_id'] = $customer_id;
                $db_params['booking_start'] = php2MySqlTime(strtotime($st));
                $db_params['booking_end'] = php2MySqlTime(strtotime($et));
                $db_params['title'] = $title;
                $db_params['is_all_day_event'] = $isAllDayEvent ? 1 : 0;
                $db_params['description'] = $description_booking;
                $db_params['city_id'] = $cityId;
                $db_params['property_type_id'] = $propertyTypeId;
                $db_params['to_follow'] = $to_follow ? strtotime($to_follow) : 0;
                $db_params['is_to_follow'] = $is_to_follow ? 1 : 0;
            }

            /**
             * delete google Calendar event if status is check as delete google calender like on hold or canceled 
             */
            $deleteGoogleCalender = $modelBookingStatus->getDeleteGoogleCalender();
            //print_r($deleteGoogleCalender);
            if (in_array($statusId, $deleteGoogleCalender)) {
                $modelGoogleCalendarEvent = new Model_GoogleCalendarEvent();
                $modelGoogleCalendarEvent->deleteGoogleCalendarEventByBookingId($bookingId);
            }
            //echo 'test ';
            /**
             * if booking changed from TO Visit to TO DO update created
             */
            if (($mode == 'update') && $statusId == $to_do['booking_status_id'] && $oldStatusId) {
                if ($oldStatusId == $toVisit['booking_status_id']) {
                    $db_params['created'] = time();
                }
            }

            /**
             * is_multiple_days
             */
            if ($multi_stpartdate) {
                $db_params['is_multiple_days'] = 1;
            } else {
                $db_params['is_multiple_days'] = 0;
            }

            /**
             * save item in the database
             */
            if ($mode == 'create') {
                $db_params['created_by'] = $loggedUser['user_id'];
                $db_params['created'] = time();
                $db_params['company_id'] = $companyId;
                $db_params['original_inquiry_id'] = $inquiryId;

                $returnData = $modelBooking->addDetailedCalendar($db_params);
                $this->logId = $returnData['log'];
            } else {
                //echo 'update booking';
                $returnData = $modelBooking->updateDetailedCalendar($bookingId, $db_params, $os);
                $this->logId = $returnData['log'];
            }
            //echo 'test222 ';
            //print_r($returnData);
            if (isset($returnData['Data']) && $returnData['Data']) {

                //echo 'if return data';

                $bookingId = (int) $returnData['Data'];

                /**
                 * save  booking status history
                 */
                if ($mode == 'create') {
                    $modelBookingStatusHistory = new Model_BookingStatusHistory();
                    $modelBookingStatusHistory->addStatusHistory($bookingId, $statusId);
                }
                //echo 'booking history done';

                /**
                 * insert booking services and his quote and quantity
                 */
                //print_r( $modelBooking->checkCanEditBookingDetails($bookingId));

                if (($mode == 'create') || (($mode == 'update') && $modelBooking->checkCanEditBookingDetails($bookingId))) {
                    //echo 'set services';
                    ///////By Islam for android
                    //if($os =='android'){

                    $modelContractorServiceBooking->setServicesToBookingOffline($bookingId, $services, $this->logId);
                    /* }
                      else{
                      $modelContractorServiceBooking->setServicesToBooking($bookingId, $services);
                      }
                     */
                }
                //echo 'booking services done';
                /**
                 * save multiple days
                 */
                $modelBookingMultipleDays = new Model_BookingMultipleDays();
                $modelBookingMultipleDays->saveMultipleDays($bookingId, 'android');


                /**
                 * save address
                 */
                $this->saveAddress($booking, $bookingId, $mode, $this->logId, $os);
                //echo 'booking address done';
                /**
                 * set product to booking
                 */
                if ($product_ids & $os == 'ios') {
                    $modelBookingProduct = new Model_BookingProduct();
                    $modelBookingProduct->setProductsToBooking($bookingId, $product_ids, $ltrs, 0, $this->logId);
                }
                //echo 'booking products done';
                /**
                 * save why booking status
                 */
                $this->saveWhyStatus($booking, $bookingId, $statusId, false, $user_role = 'contractor', $this->logId, $os);

                /**
                 * convert To Estimate if status is qouted
                 */
                // echo 'booking estimates done';
                if ($statusId == $quoted['booking_status_id']) {

                    $modelBookingEstimate->convertToEstimate($bookingId, false, false, false, $this->logId, $os);
                }
                //echo 'booking lll estimated done';
                /**
                 * delete Estimate if status is not quoted
                 */
                if ($mode == 'update' && $statusId != $quoted['booking_status_id']) {

                    $modelBookingEstimate->changedEstimateToBooking($bookingId, $statusId, false, $this->logId, $os);
                }
                //echo 'booking Estimate if status is not quoted done';
                /**
                 * convert To Invoice if booking status is complete or faild
                 */
                if ($statusId == $completed['booking_status_id'] || $statusId == $faild['booking_status_id']) {

                    $modelBookingInvoice->convertToInvoice($bookingId, false, $this->logId, $os);
                }
                //echo 'booking convert To Invoice if booking status is complete or faild done';
                /**
                 * set Quantity and Discount attribute value to Zero when booking status is faild
                 */
                if ($statusId == $faild['booking_status_id']) {
                    if ($confirmResetAttribut) {
                        ///////By Islam for android
                        if ($os = 'android') {
                            $modelContractorServiceBooking->changeAttributIfFaildOffline($bookingId, $services, $this->logId);
                        } else {
                            $modelContractorServiceBooking->changeAttributIfFaild($bookingId, $services, $this->logId);
                        }
                    }
                } else {
                    $callOutFee = 0;
                }


                /**
                 *  insert services in  the temp until approved when the booking is update by contractor
                 */
                $totalDiscountTemp = 0;
                $callOutFeeTemp = 0;
                $db_params = array();
                if ((($mode == 'update') && !$modelBooking->checkCanEditBookingDetails($bookingId))) {
                    $loggedUser = CheckAuth::getLoggedUser();
                    $modelAuthRole = new Model_AuthRole();
                    $contractorRoleId = $modelAuthRole->getRoleIdByName('contractor');

                    if ($loggedUser['role_id'] == $contractorRoleId) {
                        $db_params['property_type_id'] = $propertyTypeId;
                    }

                    $modelContractorServiceBookingTemp = new Model_ContractorServiceBookingTemp();
                    ///////By Islam for android
                    //echo 'edit per';
                    //if($os = 'android'){
                    $modelContractorServiceBookingTemp->setServicesToBookingOffline($bookingId, $services, $this->logId, $os);
                    /* }
                      else{
                      $modelContractorServiceBookingTemp->setServicesToBooking($bookingId, $services);
                      } */
                    /**
                     * if the contractor change call out fee value ,it will saved in the temp ,
                     * the original call out fee get from it's company  and saved in the booking
                     */
                    $callOutFeeTemp = $callOutFee;
                    $companies = $modelCompanies->getById(CheckAuth::getCompanySession());
                    if ($callOutFee) {
                        if ($oldCallOutFee) {
                            $callOutFee = $oldCallOutFee;
                        } else {
                            $callOutFee = $companies['call_out_fee'];
                        }
                        if ($callOutFeeTemp != $callOutFee) {
                            $db_params['is_change'] = 1;
                        }
                    }

                    // get the total discount from original booking because the gst and the total calculate from it.
                    $totalDiscountTemp = $totalDiscount;

                    if (isset($totalDiscount) && $oldTotalDiscount != $totalDiscount) {
                        $totalDiscount = $oldTotalDiscount;
                        $db_params['is_change'] = 1;
                    }

                    if ($statusId != $oldStatusId) {
                        $db_params['is_change'] = 1;
                    }
                }


                /**
                 * save calculation qoute
                 */
                $totalRefund = $modelRefund->getTotalRefund(array('booking_id' => $bookingId, 'is_approved' => 'yes'));
                $subTotal = $modelContractorServiceBooking->getTotalBookingQoute($bookingId);

                /* $total = ($subTotal + $callOutFee) - $totalDiscount;
                  $gstTax = $total * get_config('gst_tax');
                  $totalQoute = $total + $gstTax;
                  $totalQoute = $totalQoute - $totalRefund; */


                $total = ($subTotal + $callOutFee);
                $gstTax = $total * get_config('gst_tax');
                $totalQoute = $total + $gstTax - $totalDiscount;
                $totalQoute = $totalQoute - $totalRefund;



                $db_params['sub_total'] = round($subTotal, 2);
                $db_params['gst'] = round($gstTax, 2);
                $db_params['qoute'] = round($totalQoute, 2);
                $db_params['call_out_fee'] = round($callOutFee, 2);
                $db_params['refund'] = round($totalRefund, 2);
                $db_params['total_discount'] = round($totalDiscount, 2);
                $db_params['total_discount_temp'] = round($totalDiscountTemp, 2);
                $db_params['call_out_fee_temp'] = round($callOutFeeTemp, 2);


                /**
                 * generate Title
                 */
                //$title = $this->generateTitle($services, $cityId, number_format($totalQoute, 2));
                $db_params['title'] = 'tilte...';


                /**
                 * update saved Booking
                 */
                $modelBooking->updateById($bookingId, $db_params, false, $this->logId, $os);

                /**
                 * send Booking To Gmail Acc if status is completed or faild or to_do or in_process
                 */
                /* $pushGoogleCalender = $modelBookingStatus->getPushGoogleCalender();
                  if (in_array($statusId, $pushGoogleCalender)) {
                  $modelGoogleCalendarEvent = new Model_GoogleCalendarEvent();
                  $modelGoogleCalendarEvent->sendCreatedBooking($bookingId);
                  } */
            }

            if ($mode == 'create' && $inquiryId) {
                $modelInquiry = new Model_Inquiry();

                $data = array(
                    'status' => 'booking',
                    'deferred_date' => 0
                );
                $modelInquiry->updateById($inquiryId, $data);

                if ($toEstimate) {
                    $data = array(
                        'status' => 'estimate',
                        'deferred_date' => 0
                    );
                    $modelInquiry->updateById($inquiryId, $data);
                }
            }
        } else {
            //echo "validate returns true ";

            $returnData['IsSuccess'] = false;
            $returnData['Msg'] = $errorMessages;
        }
        //echo "validate returns true22 ";
        //print_r($returnData);
        //exit;
        return $returnData;
    }

    /**
     * validater
     * 
     * @param type $errorMessages
     * @return type boolean
     */
    /* public function validater(&$errorMessages) {

      $postData = $this->request->getPost();

      $bookingId = $this->request->getParam('booking_id');
      $statusId = $this->request->getParam('bookingStatus', 0);

      $modelBooking = new Model_Booking();
      $message = '';
      $isCanChangeBookingStatus = $modelBooking->checkIfCanChangeBookingStatus($bookingId, $statusId, $message);

      if (!$isCanChangeBookingStatus) {
      $errorMessages['bookingStatus'] = $message;
      }

      foreach ($postData as $key => $value) {

      /**
     * is empty validater
      /
      /// we changed the name of one parameter from total_qoute to qoute

      $required_filed = array('country', 'city_id', 'bookingStatus', 'sub_total', 'qoute', 'stpartdate', 'etpartdate', 'customer_id', 'to_follow', 'why', 'street_number', 'street_address', 'suburb', 'postcode', 'state');
      if (in_array($key, $required_filed)) {
      if (empty($value)) {
      $errorMessages[$key] = 'Field required';
      }
      }

      /**
     * time format
      /
      $is_all_day_event = isset($postData["is_all_day_event"]) ? 1 : 0;
      if (!$is_all_day_event) {
      $time_format = array('stparttime', 'etparttime');
      if (in_array($key, $time_format)) {
      if (empty($value)) {
      $errorMessages[$key] = 'stparttime and etparttime are required Fields ';
      }
      }
      }
      }



      $services = isset($postData['services']) ? $postData['services'] : array();
      $services = $this->request->getParam('services',array());
      if (count($services) === 0) {
      $errorMessages['services'] = 'Services cannot be empty';
      } else {
      foreach ($services as $service) {
      //$contractor = isset($postData['contractor_' . $service]) ? $postData['contractor_' . $service] : '';
      $contractor = $this->request->getParam('contractor_' . $service,'');
      //$contractor = isset($this->request->getParam('contractor_' . $service)) ? $this->request->getParam('contractor_' . $service) : '';
      if (empty($contractor)) {
      $errorMessages['contractor_' . $service] = 'cccc Field required';
      } else {
      $modelContractorService = new Model_ContractorService();
      $contractorService = $modelContractorService->getByContractorIdAndServiceId($contractor, $service);
      if (empty($contractorService)) {
      $errorMessages['contractor_' . $service] = "This Technician dosn't Provide this Service";
      } else {
      //$city_id = isset($postData['city_id']) ? $postData['city_id'] : 0;
      $city_id = $this->request->getParam('city_id',0);


      $modelContractorServiceAvailability = new Model_ContractorServiceAvailability();
      $contractorServiceAvailability = $modelContractorServiceAvailability->getByCityIdAndContractorServiceId($city_id, $contractorService['contractor_service_id']);
      if (empty($contractorServiceAvailability)) {
      $errorMessages['contractor_' . $service] = "This Technician dosn't Provide this Service in this City";
      }
      }
      }
      }
      }

      //$customerId = isset($postData['customer_id']) ? $postData['customer_id'] : 0;
      $customerId = $this->request->getParam('customer_id',0);

      /*if ('contractor' == CheckAuth::getRoleName() && $customerId) {
      $loggedUser = CheckAuth::getLoggedUser();
      $modelCustomer = new Model_Customer();
      if (!$modelCustomer->getByCustomerIdAndCreatedBy($customerId, $loggedUser['user_id'])) {
      $errorMessages['customer_id'] = 'You dont have permission to select this customer';
      }
      }

      if (count($errorMessages) !== 0) {
      return false;
      }
      return true;
      } */

    //////////////validater from live server
    public function validater(&$errorMessages, &$booking) {
        //echo 'teeeesttt ';
        /* $os = $this->request->getParam('os','ios');


          $postData = $_GET;
          //print_r($postData);
          if($os == 'android'){
          $w = $postData['bookings'];
          $postData =  json_decode($w);
          //print_r($postData);
          } */
        if (empty($booking)) {
            $postData = $this->request->getPost();
            $bookingId = $this->request->getParam('booking_id');
            $statusId = $this->request->getParam('bookingStatus', 0);
        } else {
            $postData = $booking;
            $bookingId = isset($booking['booking_id']) ? $booking['booking_id'] : 0;
            $statusId = isset($booking['status']) ? $booking['status'] : 0;
        }




        $modelBooking = new Model_Booking();
        $message = '';
        $isCanChangeBookingStatus = $modelBooking->checkIfCanChangeBookingStatus($bookingId, $statusId, $message);

        if (!$isCanChangeBookingStatus) {
            $errorMessages['bookingStatus'] = $message;
        }

        //print_r($postData);

        foreach ($postData as $key => $value) {

            /**
             * is empty validater
             */
            $required_filed = array('country', 'city_id', 'bookingStatus', 'sub_total', 'total_qoute', 'customer_id', 'to_follow', 'street_number', 'street_address', 'suburb', 'postcode', 'state');
            if (in_array($key, $required_filed)) {
                if (empty($value) && $value != 0) {
                    $errorMessages[$key] = 'Field required';
                }
            }

            /**
             * time format
             */
            $is_all_day_event = isset($postData["is_all_day_event"]) ? 1 : 0;
            if (!$is_all_day_event) {
                $time_format = array('stparttime', 'etparttime');
                if (in_array($key, $time_format)) {
                    if (empty($value)) {
                        $errorMessages[$key] = 'Field required';
                    }
                }
            }
        }



        $services = isset($postData['services']) ? $postData['services'] : array();
        //print_r($services);
        if (count($services) === 0) {
            $errorMessages['services'] = 'Services cannot be empty';
        } else {
            foreach ($services as $service) {
                ///By Islam for Android
                if (empty($booking)) {
                    $contractor = isset($postData['contractor_' . $service]) ? $postData['contractor_' . $service] : '';
                    if (empty($contractor)) {
                        $errorMessages['contractor_' . $service] = 'Field required';
                    } else {
                        $modelContractorService = new Model_ContractorService();
                        $contractorService = $modelContractorService->getByContractorIdAndServiceId($contractor, $service);
                        if (empty($contractorService)) {
                            $errorMessages['contractor_' . $service] = "This Technician dosn't Provide this Service";
                        } else {
                            $city_id = isset($postData['city_id']) ? $postData['city_id'] : 0;

                            $modelContractorServiceAvailability = new Model_ContractorServiceAvailability();
                            $contractorServiceAvailability = $modelContractorServiceAvailability->getByCityIdAndContractorServiceId($city_id, $contractorService['contractor_service_id']);
                            if (empty($contractorServiceAvailability)) {
                                $errorMessages['contractor_' . $service] = "This Technician dosn't Provide this Service in this City";
                            }
                        }
                    }
                } else {
                    $loggedUser = CheckAuth::getLoggedUser();

                    $contractor = $loggedUser['user_id'];
                    if (empty($contractor)) {
                        $errorMessages['contractor_' . $service['service_id']] = 'Field required';
                    } else {
                        $modelContractorService = new Model_ContractorService();
                        $contractorService = $modelContractorService->getByContractorIdAndServiceId($contractor, $service['service_id']);
                        if (empty($contractorService)) {
                            $errorMessages['contractor_' . $service['service_id']] = "This Technician dosn't Provide this Service";
                        } else {
                            $city_id = isset($postData['city_id']) ? $postData['city_id'] : 0;

                            $modelContractorServiceAvailability = new Model_ContractorServiceAvailability();
                            $contractorServiceAvailability = $modelContractorServiceAvailability->getByCityIdAndContractorServiceId($city_id, $contractorService['contractor_service_id']);
                            if (empty($contractorServiceAvailability)) {
                                $errorMessages['contractor_' . $service['service_id']] = "This Technician dosn't Provide this Service in this City";
                            }
                        }
                    }
                }
            }
        }

        $customerId = isset($postData['customer_id']) ? $postData['customer_id'] : 0;

        /* if ('contractor' == CheckAuth::getRoleName() && $customerId) {
          $loggedUser = CheckAuth::getLoggedUser();
          $modelCustomer = new Model_Customer();
          if (!$modelCustomer->getByCustomerIdAndCreatedBy($customerId, $loggedUser['user_id'])) {
          $errorMessages['customer_id'] = 'You dont have permission to select this customer';
          }
          } */
        //print_r($errorMessages);
        if (count($errorMessages) !== 0) {
            return false;
        }
        return true;
    }

    public function validaterOffline(&$errorMessages, $postData) {

        /* print_r($postData);
          echo 'test by islam'; */
        /* $bookingId = $postData['booking_id'];
          $statusId = $postData['bookingStatus']; */


        $modelBooking = new Model_Booking();
        $message = '';
        $isCanChangeBookingStatus = $modelBooking->checkIfCanChangeBookingStatus($postData['booking_id'], $postData['bookingStatus'], $message);

        if (!$isCanChangeBookingStatus) {
            $errorMessages['bookingStatus'] = $message;
        }

        foreach ($postData as $key => $value) {

            /**
             * is empty validater
             */
            //'to_follow',
            $required_filed = array('country', 'city_id', 'bookingStatus', 'sub_total', 'total_qoute', 'stpartdate', 'etpartdate', 'customer_id', 'why', 'street_number', 'street_address', 'suburb', 'postcode', 'state');
            if (in_array($key, $required_filed)) {
                if (empty($value)) {
                    $errorMessages[$key] = 'Field required';
                }
            }

            /**
             * time format
             */
            $is_all_day_event = isset($postData["is_all_day_event"]) ? 1 : 0;
            if (!$is_all_day_event) {
                $time_format = array('stparttime', 'etparttime');
                if (in_array($key, $time_format)) {
                    if (empty($value)) {
                        $errorMessages[$key] = 'Field required';
                    }
                }
            }
        }



        $services = isset($postData['services']) ? (array) $postData['services'] : array();

        //echo '**************************************';
        //print_r($services);
        /* $pureService= array();
          foreach ($services as $key => $value) {
          foreach ($value as $k => $v) {
          //echo 'key '.$key;
          //echo 'value '.$value;
          foreach ($v as $k1 => $v1) {
          //echo 'key '.$key;
          //echo 'value '.$value;
          $pureService[$k1] = $v1;
          }

          }
          }
          print_r($pureService); */

        if (count($services) === 0) {
            $errorMessages['services'] = 'Services cannot be empty';
        } else {
            foreach ($services as &$service) {
                /* $pureService= array();
                  foreach ($service as $key => $value) {
                  foreach ($value as $k1 => $v1) {
                  //echo 'key '.$key;
                  //echo 'value '.$value;
                  $pureService[$k1] = $v1;
                  }
                  }
                  $service = $pureService; */
                //echo '/////////////////////// ';
                //print_r($service);
                //echo '///////////////////////';
                $loggedUser = CheckAuth::getLoggedUser();
                //$contractor = isset($postData['contractor_' . $service]) ? $postData['contractor_' . $service] : '';
                $contractor = $loggedUser['user_id'];
                // echo 'contractor   '.$contractor;
                // echo 'service id    '.$service['service_id'];
                /* if (empty($contractor)) {
                  $errorMessages['contractor_' . $service['service_id']] = 'Field required';
                  } else {
                  $modelContractorService = new Model_ContractorService();
                  $contractorService = $modelContractorService->getByContractorIdAndServiceId($contractor, $service['service_id']);
                  if (empty($contractorService)) {
                  $errorMessages['contractor_' . $service['service_id']] = "This Technician dosn't Provide this Service";
                  } else {
                  $city_id = isset($postData['city_id']) ? $postData['city_id'] : 0;

                  $modelContractorServiceAvailability = new Model_ContractorServiceAvailability();
                  $contractorServiceAvailability = $modelContractorServiceAvailability->getByCityIdAndContractorServiceId($city_id, $contractorService['contractor_service_id']);
                  if (empty($contractorServiceAvailability)) {
                  $errorMessages['contractor_' . $service] = "This Technician dosn't Provide this Service in this City";
                  }
                  }
                  } */
            }
        }

        $customerId = isset($postData['customer_id']) ? $postData['customer_id'] : 0;

        /* if ('contractor' == CheckAuth::getRoleName() && $customerId) {
          $loggedUser = CheckAuth::getLoggedUser();
          $modelCustomer = new Model_Customer();
          if (!$modelCustomer->getByCustomerIdAndCreatedBy($customerId, $loggedUser['user_id'])) {
          $errorMessages['customer_id'] = 'You dont have permission to select this customer';
          }
          } */
        //print_r($errorMessages);
        if (count($errorMessages) !== 0) {
            return false;
        }
        return true;
    }

    /**
     * saveAddress
     *
     * @param type $bookingId
     * @param type $mode 
     */
    //$this->saveAddress($booking, $bookingId, $mode,$os='ios');

    public function saveAddress($booking, $bookingId, $mode = 'create', $log_id = 0, $os = '') {

        $modelBookingAddress = new Model_BookingAddress();
        $modelBookingAddressTemp = new Model_BookingAddressTemp();
        $modelBooking = new Model_Booking();
        $data = $this->fillAddressParam($booking);
        $bookingAddress = $modelBookingAddress->getByBookingId($bookingId);

        $orginalAddress = $modelBookingAddress->getByBookingIdWithOutLatAndLon($bookingId);
        $newAddress = $this->fillAddressParam($booking, false);

        if (($mode == 'create') || (($mode == 'update') && $modelBooking->checkCanEditBookingDetails($bookingId))) {
            if ($bookingAddress) {
                $modelBookingAddress->updateById($bookingAddress['booking_address_id'], $data, $this->logId);
            } else {
                $modelBookingAddress->insert($data);
            }
        } else {
            if ($orginalAddress != $newAddress) {

                $db_params = array();
                $db_params['is_change'] = 1;
                $update = $modelBooking->updateById($bookingId, $db_params, false, $log_id = 0, $os);
                $this->logId = $update['log_id'];
                $bookingAddressTemp = $modelBookingAddressTemp->getByBookingId($bookingId);

                if ($bookingAddressTemp) {

                    $modelBookingAddressTemp->updateById($bookingAddressTemp['booking_address_id'], $data);
                } else {


                    $modelBookingAddressTemp->insert($data);
                }
            }
        }
    }

    /**
     * fill Address Param
     * 
     * @param type $bookingid
     * @return type array
     */
    public function fillAddressParam($booking, $with_lat_lon = true) {
        /* $streetAddress = $this->request->getParam('street_address', '');
          $streetNumber = $this->request->getParam('street_number', '');
          $suburb = $this->request->getParam('suburb', '');
          $state = $this->request->getParam('state', '');
          $unitLotNumber = $this->request->getParam('unit_lot_number', '');
          $postcode = $this->request->getParam('postcode', '');
          $poBox = $this->request->getParam('po_box', '');

         */
        $streetAddress = isset($booking['street_address']) ? $booking['street_address'] : '';
        $streetNumber = isset($booking['street_number']) ? $booking['street_number'] : '';
        $suburb = isset($booking['suburb']) ? $booking['suburb'] : '';
        $state = isset($booking['state']) ? $booking['state'] : '';
        $unitLotNumber = isset($booking['unit_lot_number']) ? $booking['unit_lot_number'] : '';
        $postcode = isset($booking['postcode']) ? $booking['postcode'] : '';
        $poBox = isset($booking['po_box']) ? $booking['po_box'] : '';
        $bookingId = isset($booking['original_booking_id']) ? $booking['original_booking_id'] : '';


        $address = array();
        $address['unit_lot_number'] = trim($unitLotNumber);
        $address['street_number'] = trim($streetNumber);
        $address['street_address'] = trim($streetAddress);
        $address['suburb'] = trim($suburb);
        $address['state'] = trim($state);
        $address['postcode'] = trim($postcode);
        $address['po_box'] = trim($poBox);
        $address['booking_id'] = (int) $bookingId;

        if ($with_lat_lon) {
            $modelBookingAddress = new Model_BookingAddress();
            $geocode = $modelBookingAddress->getLatAndLon($address);
            $address['lat'] = $geocode['lat'] ? $geocode['lat'] : 0;
            $address['lon'] = $geocode['lon'] ? $geocode['lon'] : 0;
        }

        return $address;
    }

    /**
     * saveWhyStatus
     * 
     * @param type $bookingId
     * @param type $statusId 
     */
    public function saveWhyStatus($booking = array(), $bookingId, $statusId, $addLog = true, $user_role = 'contractor', $os = '') {

        $loggedUser = CheckAuth::getLoggedUser();

        //$whyBookingStatus = $this->request->getParam('why', $this->request->getParam('extra_comments', ''));
        if (!empty($booking) && isset($booking)) {
            $whyBookingStatus = isset($booking['why']) ? $booking['why'] : '';
        } else {
            $whyBookingStatus = $this->request->getParam('why', '');
        }

        if ($user_role == 'customer') {
            $user_id = $loggedUser['customer_id'];
        } else {
            $user_id = $loggedUser['user_id'];
        }


        if ($whyBookingStatus) {

            $modelBookingStatusDiscussion = new Model_BookingStatusDiscussion();
            $modelBookingDiscussion = new Model_BookingDiscussion();
            $modelBooking = new Model_Booking();

            $db_params = array();
            $db_params['booking_id'] = $bookingId;
            $db_params['user_id'] = $user_id;
            $db_params['user_message'] = $whyBookingStatus;
            $db_params['created'] = time();
            $db_params['user_role'] = $loggedUser['role_id'];


            $lastDiscussion = $modelBookingStatusDiscussion->getLastDiscussionByBookingIdAndStatusId($bookingId, $statusId);
            if (!$lastDiscussion) {

                $discussionId = $modelBookingDiscussion->insert($db_params);
            } else {


                $discussionId = $lastDiscussion['discussion_id'];

                $modelBookingDiscussion->updateById($discussionId, $db_params);
            }

            if ($discussionId) {

                $db_params = array();
                $db_params['discussion_id'] = $discussionId;
                $db_params['booking_id'] = $bookingId;
                $db_params['status_id'] = $statusId;
                $db_params['created'] = time();

                $bookingStatusDiscussion = $modelBookingStatusDiscussion->getByBookingIdAndStatusIdAndDiscussionId($bookingId, $statusId, $discussionId);
                if ($bookingStatusDiscussion) {
                    $modelBookingStatusDiscussion->updateById($bookingStatusDiscussion['id'], $db_params);
                } else {
                    $modelBookingStatusDiscussion->insert($db_params);
                }
            }

            if ($bookingId && $whyBookingStatus) {
                $modelBooking->updateById($bookingId, array('why' => $whyBookingStatus), $addLog, $this->logId, $os);
            }
        }
    }

    /**
     * generateTitle
     *
     * @param type $services
     * @param type $cityId
     * @param type $totalQoute
     * @return string 
     */
    public function generateTitle($services, $cityId, $totalQoute) {

        $modelServices = new Model_Services();
        $modelUser = new Model_User();
        $modelCities = new Model_Cities();
        $modelContractorInfo = new Model_ContractorInfo();

        /**
         * get The First service;
         */
        $serviceAndClone = explode('_', $services[0]);
        $serviceId = (int) (isset($serviceAndClone[0]) ? $serviceAndClone[0] : 0);
        $clone = (int) (isset($serviceAndClone[1]) && $serviceAndClone[1] ? $serviceAndClone[1] : 0);
        $contractorId = (int) $this->request->getParam('contractor_' . $serviceId . ($clone ? '_' . $clone : ''));

        $generat_title_service = $modelServices->getById($serviceId);
        $servicesName = $generat_title_service['service_name'];

        $generat_title_city = $modelCities->getById($cityId);
        $cityName = strtoupper($generat_title_city['city_name']);

        $contractor = CheckAuth::getLoggedUser();
        ;
        $modelContractorInfo->fill($contractor, array('contractor_info_by_user_id'));
        $contractorName = ucwords($contractor['username']);

        $title = "{$cityName}" . ' ' . "$servicesName" . ' - ' . "\${$totalQoute}" . ' - ' . "$contractorName";

        return $title;
    }

    public function registerMobileIosAction() {
        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $this->checkAccessToken($accessToken);
        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];

          $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {

          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */

        $loggedUser = CheckAuth::getLoggedUser();
        $ret_data = array('status' => 0);

        $model_ContractorMobileInfo = new Model_ContractorMobileInfo();

        $os = $this->request->getParam('os', 'ios');
        $user_id = $loggedUser['user_id'];

        $appname = $this->request->getParam('appname');
        $appversion = $this->request->getParam('appversion');
        $deviceuid = $this->request->getParam('deviceuid');
        $devicetoken = $this->request->getParam('devicetoken');
        $devicename = $this->request->getParam('devicename');
        $devicemodel = $this->request->getParam('devicemodel');
        $deviceversion = $this->request->getParam('deviceversion');
        $pushbadge = $this->request->getParam('pushbadge');
        $pushalert = $this->request->getParam('pushalert');
        $pushsound = $this->request->getParam('pushsound');
        $status = $this->request->getParam('status', 'insert');

        ///delete all devices with the same device token 
        $allDevices = $model_ContractorMobileInfo->getAllByDeviceUid($deviceuid, $os);
        if (!empty($allDevices)) {
            $success = $model_ContractorMobileInfo->deleteByDeviceUid($deviceuid);
        }
        if (my_ip()) {
            echo "salim -------------------------------- Salim";
            echo $success;
//            exit;
        }
        ///End


        $contractorMobileInfo = $model_ContractorMobileInfo->getByContractorIdAndDeviceTokenAndOs($user_id, $devicetoken, $os);


        $results = array();
        if (empty($contractorMobileInfo) && $status == 'insert') {
            $data = array(
                'contractor_id' => $user_id,
                'user_id' => $user_id,
                'appname' => $appname,
                'appversion' => $appversion,
                'deviceuid' => $deviceuid,
                'devicetoken' => $devicetoken,
                'devicename' => $devicename,
                'devicemodel' => $devicemodel,
                'deviceversion' => $deviceversion,
                'pushbadge' => $pushbadge,
                'pushalert' => $pushalert,
                'pushsound' => $pushsound,
                'os' => $os,
                'created' => time()
            );
            $results = $model_ContractorMobileInfo->insert($data);
        } else if (!empty($contractorMobileInfo) && $status == 'delete') {

            $results = $model_ContractorMobileInfo->deleteByContractorIdAndDeviceToken($user_id, $devicetoken);
        }

        $ret_data['status'] = 1;
        if ($results) {
            $ret_data['msg'] = 'successfully';
            echo json_encode($ret_data);
        } else {

            $ret_data['msg'] = 'no change';
            echo json_encode($ret_data);
        }
        exit;
    }

    public function getIosNotificationSettingsAction() {
        header('Content-Type: application/json');
        // load models
        $model_IosNotificationSetting = new Model_IosNotificationSetting();

        $loggedUser = Zend_Auth::getInstance()->getIdentity();

        $ret_data = array('status' => 0);


        if (!empty($loggedUser)) {

            $results = $model_IosNotificationSetting->getAllIosNotificationSetting();
            $ret_data['status'] = 1;
            $ret_data['results'] = $results;
            echo json_encode($ret_data);
        } else {
            $ret_data['status'] = 0;
            $ret_data['msg'] = 'unauthorized, please login at first';
            echo json_encode($ret_data);
        }

        exit;
    }

    public function iosUserNotificationSettingsAction() {
        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);

        $this->checkAccessToken($accessToken);
        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];


          $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {

          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */


        // load models
        $modelAuthCredential = new Model_AuthRoleCredential();
        $mongo = new Model_Mongo();



        $ret_data = array('authrezed' => 0);



        //if (!empty($loggedUser)) {	  
        $user_id = $user['user_id'];
        $mode = $this->request->getParam('mode');

        switch ($mode) {
            case 'get_user_settings':
                $loggedUser = CheckAuth::getLoggedUser();
                $loggedUserRole = $loggedUser['role_id'];
                $userRoles = $modelAuthCredential->getCredentialsByParentName('notifications', $loggedUserRole, $loggedUser['active']);
                $results = array();
                foreach ($userRoles as $key => $roles) {
                    $results[$key]['credential_name'] = ucfirst(spaceBeforeCapital($roles['credential_name']));
                    $notification_settings = $mongo->getByTitleAndUserId(strtolower(preg_replace('/(?<!\ )[A-Z]/', ' $0', $roles['credential_name'])));
                    if ($notification_settings) {
                        $status = true;
                    } else {
                        $status = false;
                    }
                    $results[$key]['status'] = $status;
                }

                break;
            case 'update_user_setting':
                $credential_name = $this->request->getParam('credential_name');
                $status = $this->request->getParam('status');

                $credential_name = strtolower(preg_replace('/(?<!\ )[A-Z]/', ' $0', $credential_name));
                $credential_name = trim($credential_name);
                if ($status == 'true') {
                    $mongo->updateShowByTitle($credential_name);
                } else {
                    $loggedUser = CheckAuth::getLoggedUser();
                    $user_id = $loggedUser['user_id'];
                    $mongo->deleteByUserIdByCredentialNameAndUserID($credential_name, $user_id);
                }
                $results = array('is_success' => true);
                break;
        }
        $ret_data['authrezed'] = 1;
        $ret_data['results'] = $results;
        echo json_encode($ret_data);
        /* }
          else{
          $ret_data['status'] = 0;
          $ret_data['msg'] = 'unauthorized, please login at first';
          echo json_encode($ret_data);
          } */

        exit;
    }

    /* public function iosUserNotificationAction() {
      header('Content-Type: application/json');
      $accessToken = $this->request->getParam('access_token', 0);
      $this->checkAccessToken($accessToken);

      /*$modelIosUser = new Model_IosUser();
      $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
      $this->iosLoggedUser = $user['id'];


      $loggedUser = CheckAuth::getLoggedUser();
      $data = array('authrezed' => 0);

      if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
      $data['msg'] = 'wrong access token';
      echo json_encode($data);
      exit;
      }




      if (empty($loggedUser)) {

      //open new session
      $authrezed = $this->openNewSession($accessToken);
      if (!$authrezed) {
      echo json_encode($data);
      exit;
      }
      $loggedUser = CheckAuth::getLoggedUser();
      }/
      //else{


      $mongo = new Model_Mongo();
      $notification = $mongo->getNotificationsByContractorId(true);
      //$notification = $mongo->testNotification(true);
      //print_r($results);



      $results = array();
      $i = 0;

      if (!empty($notification)) {
      foreach ($notification as $row) {
      $results[$i]['notification_id'] = $row['_id']->{'$id'};
      $results[$i]['notification_text'] = $row['notification_text'];
      $results[$i]['date_sent'] = $row['date_sent'];
      $results[$i]['booking_id'] = $row['booking_id'];
      $results[$i]['read'] = $row['read'];
      $results[$i]['title'] = $row['title'];
      $results[$i]['seen'] = $row['seen'];
      $results[$i]['show'] = $row['show'];
      if (isset($row['thumbnails'])) {
      $results[$i]['thumbnails'] = $row['thumbnails'];
      } else {
      $results[$i]['thumbnails'] = array();
      }

      $i++;
      }
      }
      $data['authrezed'] = 1;
      $data['results'] = $results;
      echo json_encode($data);


      exit;
      } */

    public function iosUserNotificationAction() {

        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $this->checkAccessToken($accessToken);
        $loggedUser = CheckAuth::getLoggedUser();

        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];


          $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          echo json_encode($data);
          exit;
          }




          if (empty($loggedUser)) {

          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          $loggedUser = CheckAuth::getLoggedUser();
          } */
        //else{


        $mongo = new Model_Mongo();
        $notification = $mongo->getNotificationsByContractorId(true);
        //$notification = $mongo->testNotification(true);
        //print_r($results);



        $results = array();
        $i = 0;
        $read_count = 0;

        if (!empty($notification)) {
            foreach ($notification as $row) {
                $results[$i]['notification_id'] = $row['_id']->{'$id'};
                $results[$i]['notification_text'] = $row['notification_text'];
                $results[$i]['date_sent'] = $row['date_sent'];
                $results[$i]['booking_id'] = $row['booking_id'];
                $results[$i]['read'] = 0;
                if (!empty($row['read'])) {
                    $read = explode(',', $row['read']);
                    if (in_array($loggedUser['user_id'], $read)) {
                        $results[$i]['read'] = 1;
                        $read_count++;
                    }
                }
                $results[$i]['seen'] = 0;
                if (!empty($row['seen'])) {
                    $seen = explode(',', $row['seen']);
                    if (in_array($loggedUser['user_id'], $seen)) {
                        $results[$i]['seen'] = 1;
                    }
                }

                $results[$i]['title'] = $row['title'];

                $results[$i]['show'] = $row['show'];
                if (isset($row['thumbnails'])) {
                    $results[$i]['thumbnails'] = $row['thumbnails'];
                } else {
                    $results[$i]['thumbnails'] = array();
                }

                $i++;
            }
        }


        ///////HERE new Function by islam
        $contractor_id = $loggedUser['user_id'];
        $counts = $this->getCounts(CheckAuth::getRoleName(), $contractor_id);
        $unread_count = 30 - $read_count;

        $data['authrezed'] = 1;
        $data['results'] = $results;
        $data['counts'] = $counts;
        $data['unread'] = "" . $unread_count;
        echo json_encode($data);


        exit;
    }

    public function iosUpdateNotificationReadAction() {
        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $notification_id = $this->request->getParam('notification_id', '');
        $markAllAsRead = $this->request->getParam('mark_all_as_read', 0);
        $this->checkAccessToken($accessToken);
        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];


          $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          echo json_encode($data);
          exit;
          }


          if (empty($loggedUser)) {

          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          $loggedUser = CheckAuth::getLoggedUser();
          } */
        //else{

        $mongo = new Model_Mongo();
        $updated = 0;
        if ($markAllAsRead) {
            $updated = $mongo->markAllAsRead();
        } else if (!empty($notification_id)) {
            $updated = $mongo->updateNotification($notification_id);
        }

        $data['result'] = $updated;
        $data['authrezed'] = 1;
        //}

        echo json_encode($data);

        exit;
    }

    public function iosUpdateNotificationSeenAction() {
        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $this->checkAccessToken($accessToken);


        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];


          $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {

          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          $loggedUser = CheckAuth::getLoggedUser();
          } */


        $loggedUser = CheckAuth::getLoggedUser();
        $mongo = new Model_Mongo();
        $status = $mongo->clearCountstatusNotification(array(), null, true);
        $data['result'] = $status;
        $data['authrezed'] = 1;
        echo json_encode($data);
        exit;
    }

    public function iosBookingDistanceAction() {
        header('Content-Type: application/json');
        //get parameters
        $first_booking_id = $this->request->getParam('first_booking_id');
        $second_booking_id = $this->request->getParam('second_booking_id');
        $contractor_id = $this->request->getParam('contractor_id');

        // load models
        $model_IosUserNotificationSetting = new Model_IosUserNotificationSetting();
        $modelBookingAddress = new Model_BookingAddress();

        $loggedUser = Zend_Auth::getInstance()->getIdentity();

        $ret_data = array('status' => 0);


        if (!empty($loggedUser)) {
            $firstBookingAddress = $modelBookingAddress->getByBookingId($first_booking_id);
            if ($second_booking_id) {
                $secondBookingAddress = $modelBookingAddress->getByBookingId($second_booking_id);
            } else {
                $modelUser = new Model_User();
                $secondBookingAddress = $modelUser->getById($contractor_id);
            }
            $distance = $modelBookingAddress->getDistanceByTwoAddress($firstBookingAddress, $secondBookingAddress);

            $ret_data['status'] = 1;
            $ret_data['results'] = $distance;
            echo json_encode($ret_data);
        } else {
            $ret_data['status'] = 0;
            $ret_data['msg'] = 'unauthorized, please login first';
            echo json_encode($ret_data);
        }

        exit;
    }

    public function getAllBookingStatusAction() {
        header('Content-Type: application/json');

        // load models
        $modelBookingStatus = new Model_BookingStatus();

        $loggedUser = Zend_Auth::getInstance()->getIdentity();

        $ret_data = array('status' => 0);


        if (!empty($loggedUser)) {
            $allBookingStatus = $modelBookingStatus->getAll();

            $ret_data['status'] = 1;
            $ret_data['results'] = $allBookingStatus;
            echo json_encode($ret_data);
        } else {
            $ret_data['status'] = 0;
            $ret_data['msg'] = 'unauthorized, please login first';
            echo json_encode($ret_data);
        }

        exit;
    }

    public function getAllStatesAction() {
        header('Content-Type: application/json');

        // load models
        $modelCities = new Model_Cities();

        $loggedUser = Zend_Auth::getInstance()->getIdentity();
        //$companyId = CheckAuth::getCompanySession();

        $ret_data = array('status' => 0);


        if (!empty($loggedUser)) {
            $allCities = $modelCities->getAll();
            $allStates = array();
            foreach ($allCities as $city) {
                $allStates[] = $city['state'];
            }

            $ret_data['status'] = 1;
            $ret_data['results'] = $allStates;
            echo json_encode($ret_data);
        } else {
            $ret_data['status'] = 0;
            $ret_data['msg'] = 'unauthorized, please login first';
            echo json_encode($ret_data);
        }

        exit;
    }

    public function receiveEmailsParametersAction() {
        header('Content-Type: application/json');
        //receive data and then save it in the temp db
        $ser = $_POST['str'];
        //echo $ser;
        $ret = unserialize($ser);
        print_r($ret);

        $request_id = uniqid();
        $modelCronjobDataTemp = new Model_CronjobDataTemp();
        foreach ($ret as $ele) {
            //echo 'serialize($ret  '.serialize($ret['data']);
            //print_r($ret['data']);
            $date = array(
                'email_info' => serialize($ele['data']),
                'email_content_parms' => serialize($ele['template_params']),
                'email_log' => serialize($ele['email_log']),
                'company_id' => 1,
                'request_id' => $request_id,
                'email_type' => $ele['cronjob_name'],
                'status' => 'sending'
            );
            $modelCronjobDataTemp->insert($date);
        }


        //return $request_id;	
        exit;
    }

    public function updateSentEmailsAction() {
        header('Content-Type: application/json');
        ////// get update request
        $ser = $_POST['str'];
        //echo $ser;
        $ret = unserialize($ser);
        print_r($ret);

        $table = $ret['table'];
        echo 'table' . $table;

        $update_records = $ret['update_records'];
        $modelCronjobDataTemp = new Model_CronjobDataTemp();
        foreach ($update_records as $ele) {
            $ref_id = $ele[0];
            echo '$ref_id   ' . $ref_id;
            $updaete_value = $ele[1];
            print_r($updaete_value);
        }
        /////now get sutible model according to table name
        //switch case


        exit;
    }

    public function openNewSession($accessToken, $user_role = 'contractor', $customer_id = 0) {

        if ($user_role == 'contractor') {
            $modelIosUser = new Model_IosUser();
            $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
            if (!empty($user)) {
                $email = $user['email1'];
                $password = $user['password'];
                $authrezed = $this->getAuthrezed();
                $authrezed->setIdentity($email);
                $authrezed->setCredential($password);

                $auth = Zend_Auth::getInstance();
                $authrezedResult = $auth->authenticate($authrezed);
                if ($authrezedResult->isValid()) {

                    $identity = $authrezed->getResultRowObject();

                    $authStorge = $auth->getStorage();
                    $authStorge->write($identity);

                    CheckAuth::afterlogin(false, 'app');
                    //CheckAuth::afterlogin(false);

                    $this->iosLoggedUser = $user['id'];

                    return 1;
                } else {
                    return 0;
                }
            } else {
                return 0;
            }
        } else if ($user_role == 'customer') {
            $modelCustomerAccessToken = new Model_CustomerAccessToken();
            $Customer = $modelCustomerAccessToken->getByUserInfoByAccessToken($accessToken);
            $accessTokenData = $modelCustomerAccessToken->getByCustomerId($customer_id);
            //var_dump($accessTokenData);

            if (!empty($Customer) && ($accessTokenData['access_token'] == $accessToken)) {
                $authRole_model = new Model_AuthRole();
                $auth = Zend_Auth::getInstance();
                $authStorge = $auth->getStorage();
                $role = $authRole_model->getRoleIdByName('customer');
                $Customer['role_id'] = $role;
                //$user_id = $Customer['customer_id'];
                $Customer['user_id'] = '';
                $Customer['username'] = $Customer['first_name'];
                $customer_obj = (object) $Customer;
                $authStorge->write($customer_obj);
                CheckAuth::redirectCustomer();
                return 1;
            } else {
                return 0;
            }
        }
    }

    public function checkAccessTokenOfLoggedUser($accessToken, $user_role = 'contractor') {
        $modelIosUser = new Model_IosUser();
        $loggedUser = CheckAuth::getLoggedUser();
        //$user = $modelIosUser->getByUserInfoById($iosUserId);
        if ($user_role == 'customer') {
            $modelCustomerAccessToken = new Model_CustomerAccessToken();
            $user = $modelCustomerAccessToken->getByUserInfoByAccessToken($accessToken);
            $user_id = $user['customer_id'];
            $logged_user_id = $loggedUser['customer_id'];
        } else {
            $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
            $user_id = $user['user_id'];
            $logged_user_id = $loggedUser['user_id'];
        }




        if ($user_id == $logged_user_id) {
            return 1;
        }
        return 0;
    }

    public function getPermissionsAction() {
        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $model_ContractorAndroidDeviceInfo = new Model_ContractorAndroidDeviceInfo();
        //$registration_id = $this->request->getParam('registration_id');
        $this->checkAccessToken($accessToken);
        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];


          $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {

          //open new session
          $authrezed = $this->openNewSession($accessToken);
          $loggedUser = CheckAuth::getLoggedUser();
          if (!$authrezed) {

          echo json_encode($data);
          exit;
          }
          } */

        $modelAuthRoleCredential = new Model_AuthRoleCredential();
        $permissions = $modelAuthRoleCredential->getAllCredentialByRoleId($loggedUser['role_id'], $loggedUser['active']);
        $data['result'] = $permissions;
        $data['authrezed'] = 1;

        echo json_encode($data);
        exit;
    }

    ///***************ANDROID
    public function registerMobileAndroidAction() {
        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $registration_id = $this->request->getParam('registration_id');
        $model_ContractorAndroidDeviceInfo = new Model_ContractorAndroidDeviceInfo();

        $this->checkAccessToken($accessToken);
        $loggedUser = CheckAuth::getLoggedUser();

        /*
          $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];


          $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {

          //open new session
          $authrezed = $this->openNewSession($accessToken);
          $loggedUser = CheckAuth::getLoggedUser();
          if ($authrezed) {
          $info = array(
          'contractor_id' => $loggedUser['user_id'],
          'registration_id' => $registration_id
          );
          $result = $model_ContractorAndroidDeviceInfo->insert($info);


          $data['result'] = $result;
          $data['authrezed'] = 1;
          }
          } */

        $info = array(
            'contractor_id' => $loggedUser['user_id'],
            'registration_id' => $registration_id
        );
        $result = $model_ContractorAndroidDeviceInfo->insert($info);


        $data['result'] = $result;
        $data['authrezed'] = 1;


        echo json_encode($data);
        exit;
    }

    public function contractorServicesAction() {
        header('Content-Type: application/json');

        $accessToken = $this->request->getParam('access_token', 0);
        $postCode = $this->request->getParam('postCode');
        $companyName = $this->request->getParam('company_name', 'Tile Cleaners Pty Ltd');
        $from_web = $this->request->getParam('from_web');
        $from_session = $this->request->getParam('from_session');


        $modelCompanies = new Model_Companies();
        $modelCustomer = new Model_Customer();
        $modelCity = new Model_Cities();
        $modelAttachment = new Model_Attachment();
        $modelContractorServiceAvailability = new Model_ContractorServiceAvailability();
        $modelServiceAttribute = new Model_ServiceAttribute();
        $attributeListValueAttachmentObj = new Model_AttributeListValueAttachment();


        if (!isset($postCode) && (isset($accessToken) && $accessToken)) {
            /* $loggedUser = CheckAuth::getLoggedUser();
              $modelIosUser = new Model_IosUser();
              $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
              $this->iosLoggedUser = $user['id'];
              $data = array('authrezed' => 0);

              if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
              $data['msg'] = 'wrong access token';
              echo json_encode($data);
              exit;
              }

              if (empty($loggedUser)) {

              //open new session
              $authrezed = $this->openNewSession($accessToken);
              if (!$authrezed) {
              echo json_encode($data);
              exit;
              }
              } */
            $this->checkAccessToken($accessToken);
        }

        $modelContractorService = new Model_ContractorService();
        $loggedUser = CheckAuth::getLoggedUser();
        if (isset($loggedUser) && (isset($accessToken) && $accessToken)) {
            $contractor = $loggedUser['user_id'];
            $result = $modelContractorService->getContractorServices($contractor);
            // var_dump($result);
            $data['authrezed'] = 1;
        }

        if (isset($postCode)) {
            $companyInfo = $modelCompanies->getByName($companyName);

            $cityState = $modelCustomer->getStateAndCityByPostCode($postCode, true);
            $city = $modelCity->getByName($cityState['city']);
            if (empty($city) || $cityState['city'] == 'unknown') {
                $data['msg'] = " No services available in this location ";
                $data['type'] = "error";
                echo json_encode($data);
                exit;
            }

            $result = $modelContractorServiceAvailability->getServiceByCityId(array('city_id' => $city['city_id'], 'company_id' => $companyInfo['company_id']), true);
        }
        if (isset($from_web) && $from_web) {
            $_SESSION['new_booking']['customer-postcode'] = $postCode;
            foreach ($result as $key => $value) {
                $Description = $modelServiceAttribute->getByAttributeName('Description', $value['service_id']);
                $ServiceAttachmentObj = new Model_ServiceAttachment();
                $serviceDefualtImage = $ServiceAttachmentObj->getByServiceId($value['service_id'], 1);
                if (!empty($serviceDefualtImage) && $serviceDefualtImage) {
                    //$parts = explode("public",$serviceDefualtImage['thumbnail_file']);
                    /* $parts = $serviceDefualtImage['large_file']?explode("public",$serviceDefualtImage['large_file']):explode("public",$serviceDefualtImage['thumbnail_file']);
                      $defualt_image = $parts['1']; */
                    $thumbnailParts = explode("public", $serviceDefualtImage['thumbnail_file']);
                    $largeParts = $serviceDefualtImage['large_file'] ? explode("public", $serviceDefualtImage['large_file']) : '';
                    $defualt_image_compressed = '';
                    if (isset($serviceDefualtImage['compressed_file'])) {
                        $compressedParts = $serviceDefualtImage['compressed_file'] ? explode("public", $serviceDefualtImage['compressed_file']) : '';
                        $defualt_image_compressed = $compressedParts ? $compressedParts['1'] : '';
                    }
                    $defualt_image = $thumbnailParts['1'];
                    $defualt_image_large = $largeParts ? $largeParts['1'] : '';
                } else {
                    $defualt_image = '/pic/attribute_pics/default_image.png';
                    $defualt_image_large = '/pic/attribute_pics/default_image.png';
                    $defualt_image_compressed = '/pic/attribute_pics/default_image.png';
                }

                $result[$key]['description'] = $Description['default_value'];
                $result[$key]['defualt_image'] = $defualt_image;
                $result[$key]['defualt_image_large'] = $defualt_image_large;
                $result[$key]['defualt_image_compressed'] = $defualt_image_compressed;
                $result[$key]['estimate_hours'] = isset($result[$key]['estimate_hours']) ? $result[$key]['estimate_hours'] : 3;
                $result[$key]['description'] = nl2br($result[$key]['description']);
            }
            $data['msg'] = "success";
            $data['result'] = $result;
            if (!$from_session) {
                $_SESSION['new_booking']['current_tab'] = 'all-services';
                $_SESSION['new_booking']['prev_tab'] = 'select-postCode';
            }
            echo json_encode($data);
            exit;
        }
        $newResult = array();
        $newResultTemp = array();
        foreach ($result as $key => $value) {
            $contractorServiceAvailability = $modelContractorServiceAvailability->getCitiesByContractorServiceId($value['contractor_service_id']);
            if (!empty($contractorServiceAvailability)) {
                $newResult[$key] = $value;
                $attributes = $modelContractorService->getContractorServicesAttributes($value['service_id']);
                // $defaultImage = $modelAttachment->getById($value['attachment_id']);
                $ServiceAttachmentObj = new Model_ServiceAttachment();
                $serviceAttachments = $ServiceAttachmentObj->getByServiceId($value['service_id']);


                $images = array();
                $largeImages = array();
                $compressedImages = array();
                if ($serviceAttachments) {
                    foreach ($serviceAttachments as $k_image => $serviceAttachment) {
                        //$parts = explode("public",$serviceAttachment['thumbnail_file']);
                        /* $parts = $serviceAttachment['large_file']?explode("public",$serviceAttachment['large_file']):explode("public",$serviceAttachment['thumbnail_file']);

                          $images[$k_image] = $parts['1']; */
                        $thumbnailParts = explode("public", $serviceAttachment['thumbnail_file']);
                        $largeParts = $serviceAttachment['large_file'] ? explode("public", $serviceAttachment['large_file']) : '';
                        $compressedParts = $serviceAttachment['compressed_file'] ? explode("public", $serviceAttachment['compressed_file']) : '';
                        $images[$k_image] = $thumbnailParts['1'];
                        $largeImages[$k_image] = $largeParts ? $largeParts['1'] : '';
                        $compressedImages[$k_image] = $compressedParts ? $compressedParts['1'] : '';
                    }
                }

                $newResult[$key]['images'] = $largeImages;
                $newResult[$key]['large_images'] = $largeImages;
                $newResult[$key]['compressed_images'] = $compressedImages;

                unset($newResult[$key]['attachment_id']);
                $units = array();
                $units[0] = 'm2';
                $newResult[$key]['units'] = $units;
                $newResult[$key]['estimate_hours'] = isset($value['estimate_hours']) ? $value['estimate_hours'] : 3;
                if (isset($postCode)) {
                    $Description = $modelServiceAttribute->getByAttributeName('Description', $value['service_id']);
                    $newResult[$key]['description'] = $Description['default_value'];
                }
                $attributesArray = array();
                foreach ($attributes as $k => $v) {
                    //$v['contractor_service_id'] = $value['contractor_service_id'];

                    $attributesArray[$k] = $v;
                    if ($v['is_list'] == '1') {
                        $listValues = $modelContractorService->getContractorServicesAttributesListValues($v['attribute_id']);
                        foreach ($listValues as $list_value_k => $listValue) {
                            $attribute_value_images = $attributeListValueAttachmentObj->getByAttributeValueId($listValue['attribute_value_id']);
                            $attributeValueImages = array();
                            if ($attribute_value_images) {
                                foreach ($attribute_value_images as $attr_v_key => $attribute_value_image) {
                                    $typeParts = explode("/", $attribute_value_image['type']);
                                    if ($typeParts[0] == 'image') {
                                        $parts = explode("public", $attribute_value_image['path']);
                                        $attributeValueImages[] = $parts['1'];
                                    }
                                }
                            }
                            $listValues[$list_value_k]['images'] = $attributeValueImages;
                        }
                        $attributesArray[$k]['attribute_values'] = $listValues;
                    }

                    $modelAttributeAttachment = new Model_AttributeAttachment();
                    $attributeDefualtImage = $modelAttributeAttachment->getByAttributeId($v['attribute_id'], 1);
                    if (!empty($attributeDefualtImage) && $attributeDefualtImage) {
                        $thumbnailParts = explode("public", $attributeDefualtImage['thumbnail_file']);
                        $largeParts = $attributeDefualtImage['large_file'] ? explode("public", $attributeDefualtImage['large_file']) : '';
                        $defualt_image_compressed = '';
                        if (isset($attributeDefualtImage['compressed_file'])) {
                            $compressedParts = $attributeDefualtImage['compressed_file'] ? explode("public", $attributeDefualtImage['compressed_file']) : '';
                            $defualt_image_compressed = $compressedParts ? $compressedParts['1'] : '';
                        }
                        $defualt_image = $thumbnailParts['1'];
                        $defualt_image_large = $largeParts ? $largeParts['1'] : '';
                    } else {
                        $defualt_image = '/pic/attribute_pics/default_image.png';
                        $defualt_image_large = '/pic/attribute_pics/default_image.png';
                        $defualt_image_compressed = '/pic/attribute_pics/default_image.png';
                    }

                    $attributesArray[$k]['defualt_image'] = $defualt_image;
                    $attributesArray[$k]['defualt_image_large'] = $defualt_image_large;
                    $attributesArray[$k]['defualt_image_compressed'] = $defualt_image_compressed;
                }

                $newResult[$key]['attributes'] = $attributesArray;
                //by islam to fix services issue which appeared in adamf account due to indexes
                $newResultTemp[] = $newResult[$key];
            }
        }

        $data['result'] = $newResultTemp;
        echo json_encode($data);
        exit;
    }

    public function getCountsAction() {
        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);

        $modelBooking = new Model_Booking();
        $modelBookingStatus = new Model_BookingStatus();
        $modelCompanies = new Model_Companies();
        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];



          $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {

          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */

        $this->checkAccessToken($accessToken);
        $loggedUser = CheckAuth::getLoggedUser();
        $contractor_id = $loggedUser['user_id'];



        $BookingStatus = $modelBookingStatus->getAllWithoutPermission();

        $counts = array();
        foreach ($BookingStatus as $status) {
            $results = array();
            $total = $modelBooking->getcountBookingsByStatus($status['name'], $contractor_id);
            $results['name'] = $status['name'];
            $results['total'] = $total;
            array_push($counts, $results);
        }

        $results = array();

        $UnapprovedBooking = $modelBooking->getCountUnapprovedBooking();
        $results['name'] = 'AWaiting Approval';
        $results['total'] = $UnapprovedBooking;
        array_push($counts, $results);

        $countRejectedBookings = $modelBooking->getCountRejectedBookings($contractor_id);
        $results['name'] = 'Rejected';
        $results['total'] = $countRejectedBookings;
        array_push($counts, $results);

        $countNewReuests = $modelBooking->getCountNewRequest($contractor_id);

        $results['name'] = 'New Requests';
        $results['total'] = $countNewReuests;
        array_push($counts, $results);

        $modelComplaint = new Model_Complaint();
        $countOpenComplaint = $modelComplaint->getComplaintCount(array('complaintStatus' => 'open', 'is_approved' => '1'));
        $results['name'] = 'unresolved complaints';
        $results['total'] = $countOpenComplaint;
        array_push($counts, $results);
        /////By Islam
        if ($loggedUser && CheckAuth::getRoleName() != 'customer') {

            //By Nour
            $model_BookingEstimate = new Model_BookingEstimate();
            $countmyBookingEstimate = $model_BookingEstimate->getCount(array('to_follow' => true, 'to_follow_date' => 'today'));
            $results['name'] = 'countmyBookingEstimate';
            $results['total'] = $countmyBookingEstimate[0]['count'];
            array_push($counts, $results);



            $countToFollowEstimate = $model_BookingEstimate->getCount(array('to_follow' => true, 'estimate_type' => 'draft'));
            $results['name'] = 'countToFollowEstimate';
            $results['total'] = $countToFollowEstimate[0]['count'];
            array_push($counts, $results);


            $time = time();
            /* $countToDraft =  $model_BookingEstimate ->getCount(array('estimate_type' => 'draft'));

              $view->countToDraft = $countToDraft[0]['count']; */
            //18-11
            $model_Inquiry = new Model_Inquiry();
            $countToFollowInquiry = $model_Inquiry->getCount(array('status' => 'inquiry', 'to_follow' => true, 'to_follow_date' => 'past'));
            $results['name'] = 'countToFollowInquiry';
            $results['total'] = $countToFollowInquiry[0]['count'];
            array_push($counts, $results);


            // count inquiry 
            $model_inquiry = new Model_Inquiry();
            $countmyInquiry = $model_inquiry->getCountInquiry(true);
            $results['name'] = 'countmyInquiry';
            $results['total'] = $countmyInquiry;
            array_push($counts, $results);


            $model_BookingInvoice = new Model_BookingInvoice();
            $countOfUnpaid = $model_BookingInvoice->getCount(array('invoice_type' => 'unpaid'));
            $countOfOpen = $model_BookingInvoice->getCount(array('invoice_type' => 'open'));
            $countOfOverdue = $model_BookingInvoice->getCount(array('invoice_type' => 'overdue'));

            $results['name'] = 'countOfUnpaid';
            $results['total'] = $countOfUnpaid;
            array_push($counts, $results);

            $results['name'] = 'countOfOpen';
            $results['total'] = $countOfOpen;
            array_push($counts, $results);

            $results['name'] = 'countOfOverdue';
            $results['total'] = $countOfOverdue;
            array_push($counts, $results);



            $modelBooking = new Model_Booking();
            $modelComplaint = new Model_Complaint();
            $mongo = new Model_Mongo();

            $countAwaitingupdateBooking = $modelBooking->getCountAwaitingupdateBooking();
            $results['name'] = 'countAwaitingupdateBooking';
            $results['total'] = $countAwaitingupdateBooking;
            array_push($counts, $results);


            $filters = array('convert_status' => 'booking', 'to_follow' => true);

            $countToFollowBooking = $modelBooking->getCountToFollowBooking($filters);
            $results['name'] = 'countToFollowBooking';
            $results['total'] = $countToFollowBooking['count'];
            array_push($counts, $results);



            $countAwaitingacceptBooking = $modelBooking->getCountAwaitingAcceptBooking();
            $results['name'] = 'countAwaitingacceptBooking';
            $results['total'] = $countAwaitingacceptBooking;
            array_push($counts, $results);


            $countUnapprovedBooking = $modelBooking->getCountUnapprovedBooking();
            $results['name'] = 'countUnapprovedBooking';
            $results['total'] = $countUnapprovedBooking;
            array_push($counts, $results);


            //by Salim 
            $cancelledStatus = $modelBookingStatus->getByStatusName('CANCELLED');
            $onHoldStatus = $modelBookingStatus->getByStatusName('ON HOLD');

            $today = getTimePeriodByName('today');
            $tomorrow = getTimePeriodByName('tomorrow');

            $primaryTodayFilters = array(
                'booking_start_between' => $today['start'],
                'booking_end_between' => $today['end'],
                'withoutEstimateStatus' => 1,
                'exclude_more_one_status' => $cancelledStatus['booking_status_id'] . ',' . $onHoldStatus['booking_status_id']
            );
            $multipleTodayFilter = array(
                'multiple_start_between' => $today['start'],
                'multiple_end_between' => $today['end'],
                'withoutEstimateStatus' => 1,
                'exclude_more_one_status' => $cancelledStatus['booking_status_id'] . ',' . $onHoldStatus['booking_status_id']
            );

            $sameMultipleTodayFilter = array(
                'multiple_start_between' => $today['start'],
                'multiple_end_between' => $today['end'],
                'same_dates' => 1,
                'withoutEstimateStatus' => 1,
                'exclude_more_one_status' => $cancelledStatus['booking_status_id'] . ',' . $onHoldStatus['booking_status_id']
            );

            $primaryTomorrowFilters = array(
                'booking_start_between' => $tomorrow['start'],
                'booking_end_between' => $tomorrow['end'],
                'withoutEstimateStatus' => 1,
                'exclude_more_one_status' => $cancelledStatus['booking_status_id'] . ',' . $onHoldStatus['booking_status_id']
            );
            $multipleTomorrowFilter = array(
                'multiple_start_between' => $tomorrow['start'],
                'multiple_end_between' => $tomorrow['end'],
                'withoutEstimateStatus' => 1,
                'exclude_more_one_status' => $cancelledStatus['booking_status_id'] . ',' . $onHoldStatus['booking_status_id']
            );

            $sameMultipleTomorrowFilter = array(
                'multiple_start_between' => $tomorrow['start'],
                'multiple_end_between' => $tomorrow['end'],
                'same_dates' => 1,
                'withoutEstimateStatus' => 1,
                'exclude_more_one_status' => $cancelledStatus['booking_status_id'] . ',' . $onHoldStatus['booking_status_id']
            );

            $cancelledStatus = $modelBookingStatus->getByStatusName('CANCELLED');
            $onHoldStatus = $modelBookingStatus->getByStatusName('ON HOLD');

            $futureFilters = array(
                'booking_not_started_yet' => true,
                'multiple_not_started_yet' => true,
                'withoutEstimateStatus' => 1,
                'exclude_more_one_status' => $cancelledStatus['booking_status_id'] . ',' . $onHoldStatus['booking_status_id']
            );





            $pager = null;
//            echo "test";
//            $countToday = $modelBooking->getAll($todayFilters, null, $pager, 0, 0, 0, 1);
//            $countTomorrow = $modelBooking->getAll($tomorrowFilters, null, $pager, 0, 0, 0, 1);
            $countFuture = $modelBooking->getAll($futureFilters, null, $pager, 0, 0, 0, 1);
            $modelBookingStatus = new Model_BookingStatus();
            $inProcessStatus = $modelBookingStatus->getByStatusName('IN PROGRESS');
            $toDo = $modelBookingStatus->getByStatusName('TO DO');
            $toVisit = $modelBookingStatus->getByStatusName('TO VISIT');
            $notAssignedFilters = array(
                'withoutEstimateStatus' => 1,
                'more_one_status' => $inProcessStatus['booking_status_id'] . ',' . $toDo['booking_status_id'] . ',' . $toVisit['booking_status_id'],
                'notAssigned' => 1
            );




            $notAssignedCount = $modelBooking->getAll($notAssignedFilters, null, $pager, 0, 0, 0, 1);
            $countRejectBooking = $modelBooking->getCountRejectBooking();

            $results['name'] = 'countNotAssigned';
            $results['total'] = $notAssignedCount[0]['count'] + $countRejectBooking;
            array_push($counts, $results);


            $results['name'] = 'countRejectBooking';
            $results['total'] = $countRejectBooking;
            array_push($counts, $results);

            $newRequestsFilters = array(
                'acceptance' => 'notAcceptNorReject',
                'withoutEstimateStatus' => 1,
                'booking_not_started_yet' => true
            );
            $toConfirmFilters = array(
                'to_confirm' => true,
                'next_working_day' => true,
                'withoutEstimateStatus' => 1
            );
            $countNewRequests = $modelBooking->getAll($newRequestsFilters, null, $pager, 0, 0, 0, 1);
            $countToConfirm = $modelBooking->getAll($toConfirmFilters, null, $pager, 0, 0, 0, 1);

            $AllNotMultipleBookingToday = $modelBooking->getAll($primaryTodayFilters, null, $pager, 0, 0, 0, 1);
            $AllMultipleBookingToday = $modelBooking->getAll($multipleTodayFilter, null, $pager, 0, 0, 0, 1);
            $sameMultipleBookingToday = $modelBooking->getAll($sameMultipleTodayFilter, null, $pager, 0, 0, 0, 1);

            $AllNotMultipleBookingTomorrow = $modelBooking->getAll($primaryTomorrowFilters, null, $pager, 0, 0, 0, 1);
            $AllMultipleBookingTomorrow = $modelBooking->getAll($multipleTomorrowFilter, null, $pager, 0, 0, 0, 1);
            $sameMultipleBookingTomorrow = $modelBooking->getAll($sameMultipleTomorrowFilter, null, $pager, 0, 0, 0, 1);
            if ((!isset($sameMultipleBookingTomorrow[0]['count']))) {
                $sameData = 0;
            } else {
                $sameData = $sameMultipleBookingTomorrow[0]['count'];
            }

            if ((!isset($sameMultipleBookingToday[0]['count']))) {
                $sameDataToday = 0;
            } else {
                $sameDataToday = $sameMultipleBookingToday[0]['count'];
            }

            $results['name'] = 'countToday';
            $results['total'] = abs(($AllNotMultipleBookingToday[0]['count'] + $AllMultipleBookingToday[0]['count']) - $sameDataToday);
            array_push($counts, $results);


            $results['name'] = 'countTomorrow';
            $results['total'] = abs(($AllNotMultipleBookingTomorrow[0]['count'] + $AllMultipleBookingTomorrow[0]['count']) - $sameData);
            array_push($counts, $results);


            $countDuplicates = $model_BookingInvoice->getDuplicatedInvoiceNumbers(1);


            $results['name'] = 'countFuture';
            $results['total'] = $countFuture[0]['count'];
            array_push($counts, $results);

            $results['name'] = 'countNewRequests';
            $results['total'] = $countNewRequests[0]['count'];
            array_push($counts, $results);

            $results['name'] = 'countToConfirm';
            $results['total'] = $countToConfirm[0]['count'];
            array_push($counts, $results);

            $results['name'] = 'countDuplicates';
            $results['total'] = $countDuplicates;
            array_push($counts, $results);


            $countUapprovedCompalints = $modelComplaint->countUapprovedCompalints();
            $results['name'] = 'countUapprovedCompalints';
            $results['total'] = $countUapprovedCompalints;
            array_push($counts, $results);

            /* $countInProcessBooking = $modelBooking->getCountInProcessBooking();
              $view->countInProcessBooking = $countInProcessBooking;
              $results['name'] = 'countUapprovedCompalints';
              $results['total'] = $countUapprovedCompalints;
              array_push($counts,$results); */





            $countOpenComplaint = $modelComplaint->getComplaintCount(array('complaintStatus' => 'open', 'is_approved' => '1'));
            $results['name'] = 'countOpenComplaint';
            $results['total'] = $countOpenComplaint;
            array_push($counts, $results);



            $countOpenComplaint = $modelComplaint->getCount(array('complaintStatus' => 'open', 'is_approved' => '1'));
            $results['name'] = 'countOpenComplaintNew';
            $results['total'] = $countOpenComplaint;
            array_push($counts, $results);


            $results['name'] = 'countNotifincations';
            $results['total'] = $mongo->countNotifications();
            array_push($counts, $results);

            // request owners

            $modelClaimOwner = new Model_ClaimOwner();
            $countClaimOwner = $modelClaimOwner->getCountClaimOwner();
            $results['name'] = 'countClaimOwner';
            $results['total'] = $countClaimOwner;
            array_push($counts, $results);


            //missed call
            $modelMissedCalls = new Model_MissedCalls();
            $countMissedCalls = $modelMissedCalls->getCountMissedCalls();
            $results['name'] = 'countMissedCalls';
            $results['total'] = $countMissedCalls;
            array_push($counts, $results);

            //new refunds
            $modelRefund = new Model_Refund();
            $newRefundsFilters = array('is_approved' => 'no');
            $newRefunds = $modelRefund->getAll($newRefundsFilters);
            $results['name'] = 'countNewRefunds';
            $results['total'] = count($newRefunds);
            array_push($counts, $results);


            // Unapproved Account
            $modelPayment = new Model_Payment();
            $countUnapprovedPayments = $modelPayment->getCountUnapprovedPayments();
            $results['name'] = 'countUnapprovedPayments';
            $results['total'] = $countUnapprovedPayments;
            array_push($counts, $results);
        }

        $data['result'] = $counts;
        $data['authrezed'] = 1;

        echo json_encode($data);
        exit;
    }

    public function disableContractorServiceAction() {
        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $service_id = $this->request->getParam('service_id', 0);
        $contractorServiceObj = new Model_ContractorService();
        $modelCompanies = new Model_Companies();
        $this->checkAccessToken($accessToken);


        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];
          $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          $data['type'] = 'error';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {

          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */

        $loggedUser = CheckAuth::getLoggedUser();
        $contractor_id = $loggedUser['user_id'];
        $isSuccess = false;

        $result = $contractorServiceObj->getByContractorIdAndServiceId($contractor_id, $service_id);
        if ($result) {
            //$update_data = array('service_status'=>$status);
            $isSuccess = $contractorServiceObj->deleteById($result['contractor_service_id']);
        }

        if ($isSuccess) {
            $data['msg'] = 'Service deleted successfully';
            $data['type'] = 'success';
        } else {
            $data['msg'] = 'No Changes in Contractor Services';
            $data['type'] = 'error';
        }

        $data['IsSuccess'] = $isSuccess;
        $data['authrezed'] = 1;

        echo json_encode($data);
        exit;
    }

    /*
      public function getCountriesAction(){

      header('Content-Type: application/json');
      $accessToken = $this->request->getParam('access_token',0);
      $modelIosUser = new Model_IosUser();
      $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);

      $this->iosLoggedUser = $user['id'];
      $modelCompanies = new Model_Companies();
      $loggedUser = CheckAuth::getLoggedUser();
      $data = array('authrezed' => 0);
      if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
      $data['msg'] = 'wrong access token';
      echo json_encode($data);
      exit;
      }

      if (empty($loggedUser)) {

      //open new session
      $authrezed = $this->openNewSession($accessToken);
      if(!$authrezed){
      echo json_encode($data);
      exit;
      }

      }

      $modelcountries = new Model_Countries();
      $countries = $modelcountries->getCountriesAsArray();


      $data['result'] = $countries;
      $data['authrezed'] = 1;
      echo json_encode($data);
      exit;

      }

      public function getCitiesAction(){

      header('Content-Type: application/json');
      $accessToken = $this->request->getParam('access_token',0);
      $country_id = $this->request->getParam('country_id', 0);
      $state = $this->request->getParam('state', 0);
      $modelIosUser = new Model_IosUser();
      $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);

      $this->iosLoggedUser = $user['id'];
      $modelCompanies = new Model_Companies();
      $loggedUser = CheckAuth::getLoggedUser();
      $data = array('authrezed' => 0);
      if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
      $data['msg'] = 'wrong access token';
      echo json_encode($data);
      exit;
      }

      if (empty($loggedUser)) {

      //open new session
      $authrezed = $this->openNewSession($accessToken);
      if(!$authrezed){
      echo json_encode($data);
      exit;
      }

      }

      $cities_obj = new Model_Cities();
      $cities = $cities_obj->getCitiesByCountryIdAndState($country_id, $state);

      $tempCities = array();
      foreach ($cities as $city) {
      $tempCities[] = array(
      'id' => $city['city_id'],
      'name' => $city['city_name']
      );
      }


      $data['result'] = $tempCities;
      $data['authrezed'] = 1;
      echo json_encode($data);
      exit;

      } */

    public function contractorServiceCitiesAvailablitiyAction() {
        header('Content-Type: application/json');

        $accessToken = $this->request->getParam('access_token', 0);
        $service_id = $this->request->getParam('service_id', 0);
        $this->checkAccessToken($accessToken);
        //$cityId = $this->request->getParam('city_id');
        //$cityIds = $this->request->getParam('city_id', array());
        //$services = json_decode($service_ids);


        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);

          $this->iosLoggedUser = $user['id'];
          $modelCompanies = new Model_Companies();
          $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);
          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          $data['type'] = 'error';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {

          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */

        $loggedUser = CheckAuth::getLoggedUser();
        $contractor_id = $loggedUser['user_id'];
        $cityId = $loggedUser['city_id'];
        /* $serviceId_array = array();
          if(!empty($services)){
          foreach ($services as $key => &$value) {
          $result = array();
          foreach ($value as $k => $v) {
          $result[$k] = $v;
          }
          $serviceId_array[$key] = $result;
          }
          } */



        $contractorServiceAvailabilityObj = new Model_ContractorServiceAvailability();
        $contractorServiceObj = new Model_ContractorService();




        $success = 0;
        $newService = array(
            'service_id' => $service_id,
            'contractor_id' => $contractor_id
        );
        $contractorService = $contractorServiceObj->getByContractorIdAndServiceId($contractor_id, $service_id);
        if (!$contractorService) {
            $contractor_service_id = $contractorServiceObj->insert($newService);
            $newServiceAvailability = array(
                'city_id' => $cityId,
                'contractor_service_id' => $contractor_service_id
            );

            $success = $contractorServiceAvailabilityObj->insert($newServiceAvailability);
        } else if ($contractorService) {
            if (!$contractorServiceAvailabilityObj->getByCityIdAndContractorServiceId($cityId, $contractorService['contractor_service_id'])) {
                $newServiceAvailability = array(
                    'city_id' => $cityId,
                    'contractor_service_id' => $contractorService['contractor_service_id']
                );
                $success = $contractorServiceAvailabilityObj->insert($newServiceAvailability);
            }
        }




        /* foreach ($serviceId_array as $serviceId) {
          $success = 0;
          $newService = array(
          'service_id' => $serviceId['service_id'],
          'contractor_id' => $contractor_id
          );

          if (!$contractorServiceObj->getByContractorIdAndServiceId($contractor_id, $serviceId)) {
          $contractor_service_id = $contractorServiceObj->insert($newService);
          }

          $newServiceAvailability = array(
          'city_id' => $cityId,
          'contractor_service_id' => $contractor_service_id
          );

          $success = $contractorServiceAvailabilityObj->insert($newServiceAvailability);
          } */


        $IsSuccess = 0;
        if ($success) {
            $IsSuccess = 1;
            $data['msg'] = 'Service added successfully';
            $data['type'] = 'success';
        } else {
            $data['msg'] = 'No Changes in Contractor Services';
            $data['type'] = 'error';
        }



        $result = array('IsSuccess' => $IsSuccess);

        $data['result'] = $result;
        $data['authrezed'] = 1;

        echo json_encode($data);
        exit;
    }

    public function checkInOutAction() {


        header('Content-Type: application/json');

        $accessToken = $this->request->getParam('access_token', 0);
        $case = $this->request->getParam('case', 'check_in');
        $bookingAttendanceId = $this->request->getParam('booking_attendance_id', 0);
        $bookingId = $this->request->getParam('booking_id', 0);
        $checkInTime = $this->request->getParam('check_in_time', null);
        $checkInDistance = $this->request->getParam('check_in_distance', 0);
        $checkInLat = $this->request->getParam('check_in_lat', 0);
        $checkInLon = $this->request->getParam('check_in_lon', 0);
        $checkOutTime = $this->request->getParam('check_out_time', null);
        $checkOutDistance = $this->request->getParam('check_out_distance', 0);
        $checkOutLat = $this->request->getParam('check_out_lat', 0);
        $checkOutLon = $this->request->getParam('check_out_lon', 0);

        /////
        /* echo 'case     '.$case;
          echo '   bookingAttendanceId     '.$bookingAttendanceId;
          echo '   bookingId     '.$bookingId;
          echo '   checkInTime     '.$checkInTime;
          echo '   checkInDistance     '.$checkInDistance;
          echo '   checkInLat     '.$checkInLat;
          echo '   checkInLon     '.$checkInLon; */
        /* echo '   checkOutTime     '.$checkOutTime;
          echo '   checkOutDistance     '.$checkOutDistance;
          echo '   checkOutLat     '.$checkOutLat;
          echo '   checkOutLon     '.$checkOutLon;
          exit; */

        ///

        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);

          $this->iosLoggedUser = $user['id'];

          $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);
          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {

          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */
        $this->checkAccessToken($accessToken);

        $modelBookingAttendance = new Model_BookingAttendance();
        if ($case == 'check_in') {
            $loggedUser = CheckAuth::getLoggedUser();

            $attendaceData = array(
                'booking_id' => $bookingId,
                'contractor_id' => $loggedUser['user_id'],
                'check_in_time' => $checkInTime,
                'check_in_distance' => $checkInDistance,
                'check_in_lat' => $checkInLat,
                'check_in_lon' => $checkInLon,
                'created' => time()
            );

            $bookingAttendanceId = $modelBookingAttendance->insert($attendaceData);
            if ($bookingAttendanceId != 0) {
                $success = 1;
            } else {
                $success = 0;
            }
            $result = array('IsSuccess' => $success, 'booking_attendance_id' => $bookingAttendanceId);
        } else {
            $attendaceData = array(
                'check_out_time' => $checkOutTime,
                'check_out_distance' => $checkOutDistance,
                'check_out_lat' => $checkOutLat,
                'check_out_lon' => $checkOutLon
            );

            $loggedUser = CheckAuth::getLoggedUser();
            $lastCheckInId = $modelBookingAttendance->getLastCheckInByBookingIdAndContractorId($bookingId, $loggedUser['user_id']);



            $bookingAttendanceId = $modelBookingAttendance->updateById($lastCheckInId, $attendaceData);
            $result = array('IsSuccess' => $bookingAttendanceId);
        }

        $data['result'] = $result;
        $data['authrezed'] = 1;

        echo json_encode($data);
        exit;
    }

    public function multipleInOutChecksAction() {


        header('Content-Type: application/json');
        $_POST = array_merge($_POST, (array) json_decode(file_get_contents("php://input"), true));
        $checks = $_POST['checks'];
        $accessToken = $_POST['access_token'];


        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);

          $this->iosLoggedUser = $user['id'];

          $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);
          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {

          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */

        $this->checkAccessToken($accessToken);
        $loggedUser = CheckAuth::getLoggedUser();

        $results = array();
        foreach ($checks as $key => $check) {


            $case = isset($check['case']) ? $check['case'] : 'check_in';
            $bookingAttendanceId = isset($check['booking_attendance_id']) ? $check['booking_attendance_id'] : 'check_in';
            $bookingId = isset($check['booking_id']) ? $check['booking_id'] : 0;
            $checkInTime = isset($check['check_in_time']) ? $check['check_in_time'] : null;
            $checkInDistance = isset($check['check_in_distance']) ? $check['check_in_distance'] : 0;
            $checkInLat = isset($check['check_in_lat']) ? $check['check_in_lat'] : 0;
            $checkInLon = isset($check['check_in_lon']) ? $check['check_in_lon'] : 0;
            $checkOutTime = isset($check['check_out_time']) ? $check['check_out_time'] : null;
            $checkOutDistance = isset($check['check_out_distance']) ? $check['check_out_distance'] : 0;
            $checkOutLat = isset($check['check_out_lat']) ? $check['check_out_lat'] : 0;
            $checkOutLon = isset($check['check_out_lon']) ? $check['check_out_lon'] : 0;

            $modelBookingAttendance = new Model_BookingAttendance();
            if ($case == 'check_in') {
                $loggedUser = CheckAuth::getLoggedUser();

                $attendaceData = array(
                    'booking_id' => $bookingId,
                    'contractor_id' => $loggedUser['user_id'],
                    'check_in_time' => $checkInTime,
                    'check_in_distance' => $checkInDistance,
                    'check_in_lat' => $checkInLat,
                    'check_in_lon' => $checkInLon,
                    'created' => time()
                );

                $bookingAttendanceId = $modelBookingAttendance->insert($attendaceData);
                if ($bookingAttendanceId != 0) {
                    $success = 1;
                } else {
                    $success = 0;
                }
                $result = array('IsSuccess' => $success, 'booking_attendance_id' => $bookingAttendanceId);
            } else {
                $attendaceData = array(
                    'check_out_time' => $checkOutTime,
                    'check_out_distance' => $checkOutDistance,
                    'check_out_lat' => $checkOutLat,
                    'check_out_lon' => $checkOutLon
                );

                $loggedUser = CheckAuth::getLoggedUser();
                $lastCheckInId = $modelBookingAttendance->getLastCheckInByBookingIdAndContractorId($bookingId, $loggedUser['user_id']);



                $bookingAttendanceId = $modelBookingAttendance->updateById($lastCheckInId, $attendaceData);
                $result = array('IsSuccess' => $bookingAttendanceId);
            }

            $results[$key] = $result;
        }

        $data['result'] = $results;
        $data['authrezed'] = 1;

        echo json_encode($data);
        exit;
    }

    public function unavailableTimeAction() {


        $accessToken = $this->request->getParam('access_token', 0);
        $modelIosUser = new Model_IosUser();
        $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
        $this->iosLoggedUser = $user['id'];
        $loggedUser = CheckAuth::getLoggedUser();
        $start = $this->request->getParam('event_start', 0);
        $unavailable_event_id = $this->request->getParam('unavailable_event_id', 0);
        $new_start = $start / 1000;
        //$timestamp_start = date('Y-m-d H:i:s ', strtotime($start));
        $event_start = date(DATE_ATOM, $new_start);
        //echo $event_start;
        //exit;
        $end = $this->request->getParam('event_end', 0);
        $new_end = $end / 1000;
        //$timestamp_end = date('Y-m-d H:i:s ', strtotime($end));
        $event_end = date(DATE_ATOM, $new_end);
        $title = $this->request->getParam('title', 0);
        $postcode = $this->request->getParam('postcode', 0);


        $this->checkAccessToken($accessToken);

        /* $data = array('authrezed' => 0);
          $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];
          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {

          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */




        $loggedUser = CheckAuth::getLoggedUser();
        $contractorId = $loggedUser['user_id'];
        $calendarServiceAccount = new Model_CalendarServiceAccount($contractorId);
        $unavailableEvent = new Model_UnavailableEvent();
        $event = new Google_Service_Calendar_Event();
        $event->setDescription($title);
        $event->setSummary($title);
        if ($postcode) {
            $event->setLocation($postcode);
        }



        $startObj = new Google_Service_Calendar_EventDateTime();
        $startObj->setTimeZone('Australia/Sydney');
        $startObj->setDateTime($event_start);
        $event->setStart($startObj);

        $endObj = new Google_Service_Calendar_EventDateTime();
        $endObj->setTimeZone('Australia/Sydney');
        $endObj->setDateTime($event_end);
        $event->setEnd($endObj);
        $event_id = 0;
        if ($unavailable_event_id) {
            $old_event = $unavailableEvent->getById($unavailable_event_id);
            $event_id = $old_event['event_id'];
        }


        if ($event_id) {
            $updateEvent = $calendarServiceAccount->updateEvent($event_id, $event);
            $EventId = $updateEvent['id'];
        } else {
            $EventId = $calendarServiceAccount->insertEvent($event);
        }


        if ($EventId) {
            $loggedUser = CheckAuth::getLoggedUser();
            $contractorId = $loggedUser['user_id'];
            $params = array('title' => $title, 'event_start' => $start, 'event_end' => $end, 'postcode' => $postcode, 'contractor_id' => $contractorId, 'event_id' => $EventId);
            if ($event_id) {

                $unavailableEvent->update($params, "event_id= '{$event_id}'");
            } else {
                $unavailableEvent->insert($params);
            }


            $data['isSuccess'] = 1;
            $data['authrezed'] = 1;
            $data['result'] = $unavailableEvent->getByEventId($EventId);
        } else {
            $data['isSuccess'] = 0;
            $data['authrezed'] = 1;
        }




        echo json_encode($data);
        exit;
    }

    public function deleteUnavailableEventAction() {


        $accessToken = $this->request->getParam('access_token', 0);
        $unavailable_event_id = $this->request->getParam('unavailable_event_id', 0);

        $loggedUser = CheckAuth::getLoggedUser();
        $unavailableEvent = new Model_UnavailableEvent();

        $this->checkAccessToken($accessToken);

        /* $data = array('authrezed' => 0);
          $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];
          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {

          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */

        $loggedUser = CheckAuth::getLoggedUser();
        $contractorId = $loggedUser['user_id'];
        $calendarServiceAccount = new Model_CalendarServiceAccount($contractorId);

        $event_id = 0;
        if ($unavailable_event_id) {
            $old_event = $unavailableEvent->getById($unavailable_event_id);
            $event_id = $old_event['event_id'];
        }

        $deletedEvent = $calendarServiceAccount->deleteEvent($event_id);
        $deleted = $unavailableEvent->deleteById($unavailable_event_id);
        if ($deleted) {

            $data['isSuccess'] = 1;
            $data['authrezed'] = 1;
        } else {
            $data['isSuccess'] = 0;
            $data['authrezed'] = 1;
        }


        echo json_encode($data);
        exit;
    }

    public function getAllUnavailableEventAction() {

        $accessToken = $this->request->getParam('access_token', 0);

        /*
          $data = array('authrezed' => 0);
          $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];
          $loggedUser = CheckAuth::getLoggedUser();

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {

          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */

        $this->checkAccessToken($accessToken);


        $loggedUser = CheckAuth::getLoggedUser();
        $contractorId = $loggedUser['user_id'];
        $unavailableEvent = new Model_UnavailableEvent();
        $results = $unavailableEvent->getAll(array('contractor_id' => $contractorId));

        $data['result'] = $results;
        $data['authrezed'] = 1;

        echo json_encode($data);
        exit;
    }

    public function editInvoiceNumAction() {


        $accessToken = $this->request->getParam('access_token', 0);
        $invoiceId = $this->request->getParam('invoice_id', 0);
        $invoice_number = $this->request->getParam('invoice_num', 0);
        $this->checkAccessToken($accessToken);

        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];
          $loggedUser = CheckAuth::getLoggedUser();


          $data = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {

          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */

        $loggedUser = CheckAuth::getLoggedUser();
        //
        // load model
        //
        
        $modelBookingLog = new Model_BookingLog();
        $modelBookingInvoice = new Model_BookingInvoice();
        $invoice = $modelBookingInvoice->getById($invoiceId);
        $modelBooking = new Model_Booking();
        if (!$invoice) {

            $data['msg'] = 'Invoice not exist';
            echo json_encode($data);
        }
        if (!CheckAuth::checkIfCanHandelAllCompany('invoice', $invoiceId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $data['msg'] = "You Don't have permission";
            echo json_encode($data);
        }

        if ((!$modelBooking->checkCanEditBookingDetails($invoice['booking_id'])) && ($invoice['invoice_num'] != $invoice_number )) {
            $modelBooking->fillBookingAndServicesFieldTemp($invoice['booking_id']);
            $modelBooking->updateById($invoice['booking_id'], array('is_change' => 1));
            $dbParams = array(
                'invoice_num' => $invoice_number,
                'invoice_num_temp' => $invoice['invoice_num']
            );
        } else {
            $dbParams = array(
                'invoice_num' => $invoice_number
            );
        }

        // add  data log
        $modelBookingLog->addBookingLog($invoice['booking_id']);

        $updated = $modelBookingInvoice->updateById($invoiceId, $dbParams);

        if ($updated) {

            $data['type'] = 'success';
            $data['message'] = 'updated successfully ';
            $data['result'] = $this->getBooking(false, array('booking_id' => $invoice['booking_id']), 0, true);
        } else {
            $data['type'] = 'error';
            $data['message'] = 'No data has changed';
        }

        $data['authrezed'] = 1;
        echo json_encode($data);
        exit;
    }

    public function visitedExtraInfoAction() {
        echo 'eeee';
        $data = $this->request->getPost();

        //$data = $_POST;
        if (empty($data)) {
            $data = $_GET;
        }
        if (isset($data['BookingDates'])) {
            $data = json_decode($data['BookingDates']);
        }

        $result = array();
        if (!empty($data)) {
            foreach ($data as $key => $value) {
                $date = array();
                foreach ($value as $k => $v) {
                    if (!is_array($v)) {
                        $v = urldecode($v);
                    }
                    $date[$k] = $v;
                }
                $result[$key] = $date;
                $result1 = array();
                $products = $result[$key]['products'];
                foreach ($products as $k1 => &$value) {
                    $result2 = array();
                    foreach ($value as $k => $v) {
                        $result2[$k] = $v;
                    }
                    $result1[$k1] = $result2;
                }
                $result[$key]['products'] = $result1;
            }
        }



        $modelVisitedExtraInfo = new Model_VisitedExtraInfo();
        $modelBookingDiscussion = new Model_BookingDiscussion();
        $modelBooking = new Model_Booking();
        foreach ($result as $key => $extra_info) {
            $date_extra_comments = $extra_info['extra_comments'];
            $bookingId = $extra_info['booking_id'];
            $products = $extra_info['products'];
            $visisted_extra_info = array(
                'job_start' => $extra_info['job_start_time'],
                'job_end' => $extra_info['job_finish_time'],
                'onsite_client_name' => $extra_info['onsite_client_name'],
                'booking_id' => $extra_info['booking_id'],
            );
            $date_products = array();
            $date_ltr = array();
            foreach ($products as $key => $product) {
                $date_products[$key] = $product['product_id'];
                $date_ltr[$key] = $product['ltr'];
            }



            $visited_info_id = $modelVisitedExtraInfo->insert($visisted_extra_info);
            if (!empty($date_extra_comments)) {

                $modelBookingDiscussion->insert(array('booking_id' => $bookingId, 'user_id' => '8', 'user_message' => $date_extra_comments, 'visited_extra_info_id' => $visited_info_id, 'created' => time()));
            }

            if ($date_products) {
                if ($visited_info_id) {
                    $delete_old = 0;

                    $modelBookingProduct = new Model_BookingProduct();
                    $modelBookingProduct->setProductsToBooking($bookingId, $date_products, $date_ltr, $visited_info_id, $delete_old, $this->logId);
                }
            }
        }

        //print_r($result);

        $modelBooking->updateById($bookingId, array('visited_extra_info_id' => $visited_info_id));

        exit;





        //print_r($date_products);
        //echo 'after booking products';
    }

    public function getComplaintTypesAction() {

        $accessToken = $this->request->getParam('access_token', 0);
        $user_role = $this->request->getParam('user_role', 'contractor');
        $customer_id = $this->request->getParam('customer_id', 0);

        $this->checkAccessToken($accessToken, $user_role, $customer_id);

        /* if($user_role == 'contractor'){
          $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];
          }

          $loggedUser = CheckAuth::getLoggedUser();

          $data = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken, $user_role))) {
          $data['msg'] = 'wrong access token';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {

          //open new session
          $authrezed = $this->openNewSession($accessToken, $user_role, $customer_id);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */
        $data['authrezed'] = 1;
        $loggedUser = CheckAuth::getLoggedUser();

        //
        // get data list
        //
        $modelComplaintType = new Model_ComplaintType();
        $complaintTypes = $modelComplaintType->getAll();

        if (!empty($complaintTypes)) {
            $data['msg'] = $complaintTypes;
        }

        echo json_encode($data);
        exit;
    }

    public function changeComplaintStatusAction() {

        $accessToken = $this->request->getParam('access_token', 0);
        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];
          $loggedUser = CheckAuth::getLoggedUser();

          $result = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $result['msg'] = 'wrong access token';
          echo json_encode($result);
          exit;
          }

          if (empty($loggedUser)) {

          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($result);
          exit;
          }
          } */
        $this->checkAccessToken($accessToken);

        $result['authrezed'] = 1;
        $loggedUser = CheckAuth::getLoggedUser();

        //
        // get data list
        //
		$complaintId = $this->request->getParam('complaint_id', 0);
        $complaint_status = $this->request->getParam('complaint_status', 'open');
        $discussion = $this->request->getParam('discussion', '');
        $complaintData = array('complaint_status' => $complaint_status);
        $modelComplaint = new Model_Complaint();
        $modelComplaintTemp = new Model_ComplaintTemp();
        $modelComplaintDiscussion = new Model_ComplaintDiscussion();
        $loggedUser = CheckAuth::getLoggedUser();


        $complaint = $modelComplaint->getById($complaintId);
        if (!$complaint) {
            $result['msg'] = "error, the complaint doesn't exist";
            echo json_encode($result);
            exit;
        }


        $success = 0;
        if ($discussion) {
            $Discussiondata = array(
                'complaint_id' => $complaintId,
                'user_id' => $loggedUser['user_id'],
                'user_message' => $discussion,
                'created' => time()
            );
            $success = $modelComplaintDiscussion->insert($Discussiondata);
            //$modelComplaint->sendComplaintAsEmailToContractor($complaintId);

            $updated = 0;

            if ($complaint_status == 'closed') {

                $loggedUser = CheckAuth::getLoggedUser();
                $user_id = !empty($loggedUser) ? $loggedUser['user_id'] : 0;

                $modelAuthRole = new Model_AuthRole();
                $contractor_role_id = $modelAuthRole->getRoleIdByName('contractor');
                if ($contractor_role_id == $loggedUser['role_id']) {
                    $db_params = array();
                    $db_params['is_approved'] = 0;
                    $modelComplaint->updateById($complaintId, $db_params);
                    $complaintTemp = $modelComplaintTemp->getByComplaintId($complaintId);
                    $loggedUser = CheckAuth::getLoggedUser();
                    $loggedUser = CheckAuth::getLoggedUser();
                    $data['user_id'] = $loggedUser['user_id'];
                    $data['complaint_status'] = 'closed';
                    if ($complaintTemp) {
                        $updated = $modelComplaintTemp->updateById($complaintTemp['id'], $data);
                    } else {
                        $added = $modelComplaintTemp->addComplaintTemp($complaintId, $data);
                        if ($added) {
                            $updated = 1;
                        }
                    }
                } else {
                    $updated = $modelComplaint->updateById($complaintId, $data);
                }
            } else if ($complaint_status == 'open') {

                $complaintTemp = $modelComplaintTemp->getByComplaintId($complaintId);
                if ($complaintTemp) {
                    $modelComplaintTemp->deleteById($complaintTemp['id']);
                }
                $updated = $modelComplaint->updateById($complaintId, array('complaint_status' => 'open', 'is_approved' => 1));
            }
        }

        if ($updated && $success) {
            $result['msg'] = 'Update saved';
            if ($complaint_status == 'open') {
                $result['result'] = $modelComplaint->getById($complaintId);
            } else {
                $result['result'] = $modelComplaintTemp->getByComplaintId($complaintId);
            }
        } else {
            $result['msg'] = 'error, please try later';
        }



        echo json_encode($result);
        exit;
    }

    public function getAllRejectQuestionsAction() {

        $accessToken = $this->request->getParam('access_token', 0);

        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];
          $loggedUser = CheckAuth::getLoggedUser();

          $result = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $result['msg'] = 'wrong access token';
          echo json_encode($result);
          exit;
          }

          if (empty($loggedUser)) {

          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($result);
          exit;
          }
          } */

        $this->checkAccessToken($accessToken);
        $result['authrezed'] = 1;
        $loggedUser = CheckAuth::getLoggedUser();

        //
        // get data list
        //
		$modelRejectBookingQuestion = new Model_RejectBookingQuestion();
        $modelRejectBookingQuestionOptions = new Model_RejectBookingQuestionOptions();
        $questions = $modelRejectBookingQuestion->getAll();

        foreach ($questions as &$question) {
            $options = array();
            if ($question['question_type'] == 'multiple') {
                $options = $modelRejectBookingQuestionOptions->getByQuestionId($question['id']);
            }
            $question['options'] = $options;
        }
        $result['result'] = $questions;

        echo json_encode($result);
        exit;
    }

    public function answerRejectQuestionsAction() {

        $os = $this->request->getParam('os', 'ios');
        if ($os == 'ios') {
            //$rawData = (array) json_decode(file_get_contents("php://input"));
            $rawData = array_merge($_POST, (array) json_decode(file_get_contents("php://input"), true));
            $accessToken = $rawData['access_token'];
            $booking_id = $rawData['booking_id'];
        } else {
            $accessToken = $this->request->getParam('access_token', 0);
            $booking_id = $this->request->getParam('booking_id', 0);
        }





        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];
          $loggedUser = CheckAuth::getLoggedUser();

          $result = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $result['msg'] = 'wrong access token';
          echo json_encode($result);
          exit;
          }

          if (empty($loggedUser)) {
          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($result);
          exit;
          }
          } */
        $this->checkAccessToken($accessToken);

        $result['authrezed'] = 1;
        $loggedUser = CheckAuth::getLoggedUser();



        if ($os == 'ios') {
            $answers = $rawData['answers'];
        } else {
            $data = $this->request->getPost();
        }

        if (empty($data)) {
            $data = $_GET;
        }
        if (!empty($data)) {


            $answers = json_decode($data['answers']);
        }



        $result1 = array();
        if (!empty($answers)) {
            foreach ($answers as $key => &$value) {
                $result2 = array();
                foreach ($value as $k => $v) {
                    if (!is_array($v)) {
                        $v = urldecode($v);
                    }
                    $result2[$k] = $v;
                }
                $result1[$key] = $result2;
            }
        }
        //print_r($result1);

        $modelRejectBookingQuestionAnswers = new Model_RejectBookingQuestionAnswers();
        $modelRejectBookingQuestionAnswers->deleteByBookingId($booking_id);
        foreach ($result1 as $answer) {

            $ret = $modelRejectBookingQuestionAnswers->insert($answer);
        }
        //echo $ret;
        $result['result'] = $this->getBooking(false, array('booking_id' => $booking_id));
        /* if($ret != 0 ){
          $result['result'] = "saved successfully";
          }
          else{
          $result['result'] = "failed, please try again";

          } */

        echo json_encode($result);
        exit;
    }

    public function getImageTagsAction() {

        //$itemId =  $this->request->getParam('itemid',0);
        //$type =  $this->request->getParam('type',0);
        $accessToken = $this->request->getParam('access_token', 0);
        $user_role = $this->request->getParam('user_role', 'contractor');
        $customer_id = $this->request->getParam('customer_id', 0);

        //$this->checkAccessToken($accessToken,$user_role,$customer_id);

        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];
          $loggedUser = CheckAuth::getLoggedUser();

          if ($user_role == 'contractor') {
          $result = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $result['msg'] = 'wrong access token';
          echo json_encode($result);
          exit;
          }

          if (empty($loggedUser)) {
          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($result);
          exit;
          }
          }
          } */
        //$result['authrezed'] = 1;
        //$loggedUser = CheckAuth::getLoggedUser();

        $modelImageTypes = new Model_ImageTypes();
        $imagetypes = $modelImageTypes->getAll();

        $tempImagetypes = array();
        foreach ($imagetypes as $imagetype) {
            $tempImagetypes[] = array(
                'id' => $imagetype['image_types_id'],
                'name' => $imagetype['name']
            );
        }


        $result['ImageTypes'] = $tempImagetypes;

        echo json_encode($result);
        exit;
    }

    public function imageUploadAction() {


        $count = $this->request->getParam('count', '');
        $iosPhotos = $this->request->getParam('photos', array());

        $accessToken = $this->request->getParam('access_token', 0);
        $user_role = $this->request->getParam('user_role', 'contractor');
        $customer_id = $this->request->getParam('customer_id', 0);
        $discussion = $this->request->getParam('comment', '');
        $itemId = $this->request->getParam('bookingId', 0);
        $type = $this->request->getParam('type', 'booking');
        $success = 0;
        $modelItemImageDiscussion = new Model_ItemImageDiscussion();
        $modelBookingDiscussion = new Model_BookingDiscussion();
        $modelImage = new Model_Image();


        $this->checkAccessToken($accessToken, $user_role, $customer_id);

        /* if ($user_role == 'contractor') {
          $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];
          }

          $loggedUser = CheckAuth::getLoggedUser();
          $result = array('authrezed' => 0);
          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken, $user_role))) {
          $result['msg'] = 'wrong access token';
          echo json_encode($result);
          exit;
          }


          if (empty($loggedUser)) {
          //open new session
          $authrezed = $this->openNewSession($accessToken, $user_role, $customer_id);

          if (!$authrezed) {
          echo json_encode($result);
          exit;
          }
          } */

        $result['authrezed'] = 1;
        $loggedUser = CheckAuth::getLoggedUser();
        $logged_user_id = ($user_role == 'customer') ? $loggedUser['customer_id'] : $loggedUser['user_id'];




        $dir = get_config('image_attachment') . '/';
        $subdir = date('Y/m/d/');
        $fullDir = $dir . $subdir;
        if (!is_dir($fullDir)) {
            mkdir($fullDir, 0777, true);
        }


        // android save image 

        if ((isset($count) && $count != '')) {
            $image_group = 0;
            $success_discussion = 0;
            if ($count > 1) {
                $maxGroup = max($modelImage->getMaxGroup());
                $image_group = $maxGroup['group_id'] + 1;
                if ($discussion) {
                    if ($type == 'booking') {
                        $data = array(
                            'booking_id' => $itemId,
                            'user_id' => $logged_user_id,
                            'user_message' => $discussion,
                            'created' => time(),
                            'user_role' => $loggedUser['role_id']
                        );
                        $success_discussion = $modelBookingDiscussion->insert($data);
                        $dataDiscssion = array(
                            'image_id' => 0,
                            'group_id' => $image_group,
                            'item_id' => $success_discussion,
                            'type' => $type
                        );
                        $modelItemImageDiscussion->insert($dataDiscssion);
                    }
                }
            }

            for ($i = 0; $i < $count; $i++) {

                $source = $_FILES['imageFile' . $i]['tmp_name'];
                $extension = $this->request->getParam('ext' . $i, 'png');
                $imageInfo = pathinfo($source);
                $ext = $extension;
                $successUpload = 0;
                $fileName = "image_{$i}.{$ext}";
                $file = $fullDir . $fileName;
                $image_saved = copy($source, $fullDir . $fileName);
                if ($image_saved) {
                    $successUpload = 1;
                    $image_id = $this->SaveImagesData($image_group, $count, $success_discussion);
                    $original_path = "original_{$image_id}.{$ext}";
                    $large_path = "large_{$image_id}.{$ext}";
                    $small_path = "small_{$image_id}.{$ext}";
                    $thumbnail_path = "thumbnail_{$image_id}.{$ext}";
                    $compressed_path = "compressed_{$image_id}.{$ext}";

                    $data = array(
                        'original_path' => $subdir . $original_path,
                        'large_path' => $subdir . $large_path,
                        'small_path' => $subdir . $small_path,
                        'thumbnail_path' => $subdir . $thumbnail_path,
                        'compressed_path' => $subdir . $compressed_path
                    );

                    $modelImage->updateById($image_id, $data);
                    $compressed_saved = copy($source, $fullDir . $compressed_path);
                    $image_saved = copy($source, $fullDir . $original_path);
                    ImageMagick::scale_image($source, $fullDir . $large_path, 510);
                    ImageMagick::scale_image($source, $fullDir . $small_path, 250);
                    ImageMagick::create_thumbnail($source, $fullDir . $thumbnail_path, 78, 64);
                    ImageMagick::convert($source, $fullDir . $compressed_path);
                    ImageMagick::compress_image($source, $fullDir . $compressed_path);
                }


                if ($image_saved) {
                    if (file_exists($source)) {
                        unlink($source);
                    }
                    if (file_exists($file)) {
                        unlink($file);
                    }
                }
            }


            $this->clearPhotoCache();
            $result['result'] = $successUpload;
            echo json_encode($result);
            exit;
        }


        // ios save image

        if ((isset($iosPhotos) && !empty($iosPhotos))) {
            $image_group = 0;
            $success_discussion = 0;
            $count = count($iosPhotos);
            if ($count > 1) {
                $maxGroup = max($modelImage->getMaxGroup());
                $image_group = $maxGroup['group_id'] + 1;
                if ($discussion) {
                    if ($type == 'booking') {
                        $data = array(
                            'booking_id' => $itemId,
                            'user_id' => $logged_user_id,
                            'user_message' => $discussion,
                            'created' => time(),
                            'user_role' => $loggedUser['role_id']
                        );
                        $success_discussion = $modelBookingDiscussion->insert($data);
                        $dataDiscssion = array(
                            'image_id' => 0,
                            'group_id' => $image_group,
                            'item_id' => $success_discussion,
                            'type' => $type
                        );
                        $modelItemImageDiscussion->insert($dataDiscssion);
                    }
                }
            }

            foreach ($iosPhotos as $key => $imageData) {

                $successUpload = 0;

                if (isset($imageData) && !empty($imageData)) {
                    $image = str_replace('data:image/png;base64,', '', $imageData);
                    $image = str_replace(' ', '+', $image);
                    $image = base64_decode($image);
                    $fileName = "image_{$key}.png";
                    $file = $fullDir . $fileName;
                    $success = file_put_contents($file, $image);
                    if ($success) {

                        $successUpload = 1;
                        $image_id = $this->SaveImagesData($image_group, $count, $success_discussion);
                        $original_path = "original_{$image_id}.png";
                        $large_path = "large_{$image_id}.png";
                        $small_path = "small_{$image_id}.png";
                        $thumbnail_path = "thumbnail_{$image_id}.png";
                        $compressed_path = "compressed_{$image_id}.jpg";

                        $data = array(
                            'original_path' => $subdir . $original_path,
                            'large_path' => $subdir . $large_path,
                            'small_path' => $subdir . $small_path,
                            'thumbnail_path' => $subdir . $thumbnail_path,
                            'compressed_path' => $subdir . $compressed_path
                        );

                        $modelImage->updateById($image_id, $data);
                        $compressed_saved = copy($file, $fullDir . $compressed_path);
                        $image_saved = copy($file, $fullDir . $original_path);
                        ImageMagick::scale_image($file, $fullDir . $large_path, 510);
                        ImageMagick::scale_image($file, $fullDir . $small_path, 250);
                        ImageMagick::create_thumbnail($file, $fullDir . $thumbnail_path, 78, 64);
                        ImageMagick::convert($file, $fullDir . $compressed_path);
                        ImageMagick::compress_image($file, $fullDir . $compressed_path);

                        if (file_exists($file)) {
                            unlink($file);
                        }
                    }
                }
            }

            $this->clearPhotoCache();
            $result['result'] = $successUpload;
            $uploaded_photos = $modelImage->getAll($itemId, $type);
            $result['photos'] = $uploaded_photos;
            echo json_encode($result);
            exit;
        }






        $result['msg'] = 'there is no files';
        echo json_encode($result);
        exit;
    }

    public function clearPhotoCache() {

        $itemId = $this->request->getParam('bookingId', 0);
        require_once 'Zend/Cache.php';
        //$company_id = CheckAuth::getCompanySession();
        $company_id = 1;


        //D.A 30/08/2015 Remove Booking Photos Cache				
        $bookingPhotosCacheID = $itemId . '_bookingPhotos';
        $bookingViewDir = get_config('cache') . '/' . 'bookingsView' . '/' . $company_id;
        if (!is_dir($bookingViewDir)) {
            mkdir($bookingViewDir, 0777, true);
        }
        $frontEndOption = array('lifetime' => NULL,
            'automatic_serialization' => true);
        $backendOptions = array('cache_dir' => $bookingViewDir);
        $Cache = Zend_Cache::factory('Core', 'File', $frontEndOption, $backendOptions);
        $Cache->remove($bookingPhotosCacheID);
        $modelBooking = new Model_Booking();
        $modelBookingEstimate = new Model_BookingEstimate();
        $booking = $modelBooking->getById($itemId);
        $estimate = $modelBookingEstimate->getByBookingId($itemId);
        if ($estimate) {
            $estimateLabelsCacheID = $estimate['id'] . '_estimatePhoto';
            $estimateViewDir = get_config('cache') . '/' . 'estimatesView' . '/' . $company_id;
            if (!is_dir($estimateViewDir)) {
                mkdir($estimateViewDir, 0777, true);
            }
            $estimatePhotoFrontEndOption = array('lifetime' => NULL,
                'automatic_serialization' => true);
            $estimatePhotoBackendOptions = array('cache_dir' => $estimateViewDir);
            $estimatePhotoCache = Zend_Cache::factory('Core', 'File', $estimatePhotoFrontEndOption, $estimatePhotoBackendOptions);
            $estimatePhotoCache->remove($estimateLabelsCacheID);
        }
        if ($booking['original_inquiry_id']) {
            $inquiryPhotosCacheID = $booking['original_inquiry_id'] . '_inquiryPhotos';
            $inquiryViewDir = get_config('cache') . '/' . 'inquiriesView' . '/' . $company_id;
            if (!is_dir($inquiryViewDir)) {
                mkdir($inquiryViewDir, 0777, true);
            }
            $inquiryPhotosFrontEndOption = array('lifetime' => NULL,
                'automatic_serialization' => true);
            $inquiryPhotosBackendOptions = array('cache_dir' => $inquiryViewDir);
            $inquiryPhotosCache = Zend_Cache::factory('Core', 'File', $inquiryPhotosFrontEndOption, $inquiryPhotosBackendOptions);
            $inquiryPhotosCache->remove($inquiryPhotosCacheID);
        }
    }

    public function SaveImagesData($imageGroup, $count, $success_discussion) {

        $itemId = $this->request->getParam('bookingId', 0);
        $type = $this->request->getParam('type', 'booking');
        $image_label = $this->request->getParam('imageType', '');
        $discussion = $this->request->getParam('comment', '');
        $service_id = $this->request->getParam('service', 0);
        $user_role = $this->request->getParam('user_role', 'contractor');



        $modelItemImageDiscussion = new Model_ItemImageDiscussion();
        $modelBookingDiscussion = new Model_BookingDiscussion();
        $modelImage = new Model_Image();
        $loggedUser = CheckAuth::getLoggedUser();



        $logged_user_id = ($user_role == 'customer') ? $loggedUser['customer_id'] : $loggedUser['user_id'];

        $data = array(
            'user_ip' => ''
            , 'created_by' => $logged_user_id
            , 'image_types_id' => $image_label
            , 'group_id' => $imageGroup
            , 'user_role' => $loggedUser['role_id']
        );

        $id = $modelImage->insert($data);
        $image_id = $id;
        if ($count > 1) {
            $image_id = 0;
        }

        $success = 0;
        if ($count == 1) {

            if ($discussion) {
                if ($type == 'booking') {
                    $data = array(
                        'booking_id' => $itemId,
                        'user_id' => $logged_user_id,
                        'user_message' => $discussion,
                        'created' => time(),
                        'user_role' => $loggedUser['role_id']
                    );
                    $success = $modelBookingDiscussion->insert($data);
                    $dataDiscssion = array(
                        'image_id' => $image_id,
                        'group_id' => $imageGroup,
                        'item_id' => $success,
                        'type' => $type
                    );
                    $modelItemImageDiscussion->insert($dataDiscssion);
                }
            }
        }
        if (!($service_id == 0)) {
            $type_new = $type;
            if ($success || $success_discussion) {
                $type_new = $type_new . '_discussion';
            }
            $type_new = $type_new . '_service';
            if ($type == 'service') {
                $itemId = $service_id;
            }
        } else {
            $type_new = $type;
            if ($success || $success_discussion) {
                $type_new = $type_new . '_discussion';
            }
        }
        $success = ($success_discussion) ? $success_discussion : $success;
        $Itemdata = array(
            'item_id' => $itemId,
            'service_id' => $service_id,
            'discussion_id' => $success,
            'image_id' => $id,
            'type' => $type_new
        );


        $modelItemImage = new Model_ItemImage();
        $Item_image_id = $modelItemImage->insert($Itemdata);

        return $id;
    }

    /* public function imageUploadAction(){

      $itemId =  $this->request->getParam('bookingId',0);
      $type =  $this->request->getParam('type','booking');
      $image_label = $this->request->getParam('imageType','');
      $discussion = $this->request->getParam('comment','');
      $service_id = $this->request->getParam('service', 0);
      $count = $this->request->getParam('count','');
      $accessToken = $this->request->getParam('access_token',0);
      $success = 0;
      $modelIosUser = new Model_IosUser();
      $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
      $modelItemImageDiscussion =new Model_ItemImageDiscussion();
      $modelBookingDiscussion = new Model_BookingDiscussion();
      $modelImageAttachment = new Model_Image();
      $this->iosLoggedUser = $user['id'];
      $loggedUser = CheckAuth::getLoggedUser();

      $result = array('authrezed' => 0);

      if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
      $result['msg'] = 'wrong access token';
      echo json_encode($result);
      exit;
      }

      if (empty($loggedUser)) {
      //open new session
      $authrezed = $this->openNewSession($accessToken);
      if(!$authrezed){
      echo json_encode($result);
      exit;
      }
      }
      $result['authrezed'] = 1;
      $loggedUser = CheckAuth::getLoggedUser();


      $dir = get_config('image_attachment') . '/';
      $subdir = date('Y/m/d/');
      $fullDir = $dir . $subdir;
      if (!is_dir($fullDir)) {
      mkdir($fullDir, 0777, true);
      }


      if(isset($count) && $count != ''){

      $image_group = 0;
      if( $count > 1){
      $maxGroup =  max($modelItemImageDiscussion->getMaxGroup());
      $image_group = $maxGroup['group_id'] + 1;
      }

      for($i=0; $i<$count; $i++) {

      $source  = $_FILES['imageFile'.$i]['tmp_name'];
      $extension = $this->request->getParam('ext'.$i,'png');
      $imageInfo = pathinfo($source);

      //$ext = $imageInfo['extension'];
      $ext = $extension;


      $data = array(
      'user_ip'=> ''
      ,'created_by'=>$loggedUser['user_id']
      ,'image_types_id'=>$image_label
      ,'group_id'=>$image_group
      );

      $id = $modelImageAttachment->insert($data);
      $image_id = $id;
      if($count > 1){
      $image_id = 0;
      }



      $success = 0;
      if ($discussion) {
      if($type == 'booking'){
      $data = array(
      'booking_id' => $itemId,
      'user_id' => $loggedUser['user_id'],
      'user_message' => $discussion,
      'created' => time(),
      );
      $success = $modelBookingDiscussion->insert($data);



      $dataDiscssion = array(
      'image_id'=>$image_id,
      'group_id' =>$image_group,
      'item_id'=>$success,
      'type'=>$type
      );
      $modelItemImageDiscussion->insert($dataDiscssion);



      }
      }

      if(!($service_id == 0)){
      $type_new = $type;
      if($success){
      $type_new = $type_new.'_discussion' ;
      }
      $type_new = $type_new.'_service';
      if($type == 'service'){
      $itemId = $service_id;
      }
      }else{
      $type_new = $type;
      if($success){
      $type_new = $type_new.'_discussion' ;
      }
      }
      $Itemdata = array(
      'item_id'=>$itemId,
      'service_id'=>$service_id,
      'discussion_id'=>$success,
      'image_id' =>$id,
      'type'=>$type_new
      );


      $modelItemImage = new Model_ItemImage();
      $Item_image_id = $modelItemImage->insert($Itemdata);

      $original_path = "original_{$id}.{$ext}";
      $large_path = "large_{$id}.{$ext}";
      $small_path = "small_{$id}.{$ext}";
      $thumbnail_path = "thumbnail_{$id}.{$ext}";
      $compressed_path = "compressed_{$id}.jpg";

      $data = array(
      'original_path' => $subdir . $original_path,
      'large_path' => $subdir . $large_path,
      'small_path' => $subdir . $small_path,
      'thumbnail_path' => $subdir . $thumbnail_path ,
      'compressed_path' => $subdir . $compressed_path
      );

      $modelImageAttachment->updateById($id, $data);

      //$original_path = basename($_FILES['imageFile'.$i]['name']);
      $successUpload = 0;
      $image_saved = copy($_FILES['imageFile'.$i]['tmp_name'], $fullDir . $original_path);
      $compressed_saved = copy($source, $fullDir . $compressed_path);

      if ($image_saved) {
      $successUpload = 1;
      ImageMagick::scale_image($source, $fullDir . $large_path, 510);
      ImageMagick::scale_image($source, $fullDir . $small_path, 250);
      ImageMagick::create_thumbnail($source, $fullDir . $thumbnail_path, 78, 64);
      ImageMagick::convert($source, $fullDir . $compressed_path);
      ImageMagick::compress_image($source, $fullDir . $compressed_path);
      }

      if ($image_saved) {
      if (file_exists($source)) {
      unlink($source);
      }
      }

      }

      $result['result'] = $successUpload;
      echo json_encode($result);
      exit;

      }

      $result['msg'] = 'there is no files';
      echo json_encode($result);
      exit;

      } */

    public function getEmailContentAction() {

        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $modelCompanies = new Model_Companies();
        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);

          $this->iosLoggedUser = $user['id'];

          $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);
          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {

          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */
        $this->checkAccessToken($accessToken);
        $loggedUser = CheckAuth::getLoggedUser();

        $type = $this->request->getParam('type', 'invoice');
        $id = $this->request->getParam('id', 0);
        $customer_contact_id = $this->request->getParam('customer_contact_id', 0);


        //
        // load models
        //
        $modelUser = new Model_User();
        $modelBooking = new Model_Booking();
        $modelCustomer = new Model_Customer();
        $modelCustomerContact = new Model_CustomerContact();
        $modelBookingAddress = new Model_BookingAddress();
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();

        //
        // geting data
        //
        $invoice = $modelBookingInvoice->getById($id);
        $modelBookingInvoice->fill($invoice, array('booking'));

        //
        // validation
        //
        if (!$invoice) {
            $data['msg'] = "Invoice not exist";
            $data['authrezed'] = 1;
            echo json_encode($data);
            exit;
        }
        //check in can see his or assigned invoices

        /* if (!$modelBookingInvoice->checkIfCanSeeHisOrAssignedInvoice($id)) {
          $data['msg'] = "You Don't have permission";
          $data['authrezed'] = 1;
          echo json_encode($data);
          exit;
          } */

        //
        // filling extra data
        //
        if ($customer_contact_id) {
            $customer = $modelCustomerContact->getById($customer_contact_id);
            $modelCustomerContact->fill($customer, array('customer_commercial_info', 'customer_contacts'));
        } else {
            $customer = $modelCustomer->getById($invoice['booking']['customer_id']);
            $modelCustomer->fill($customer, array('customer_commercial_info', 'customer_contacts'));
        }
        $this->view->customerContacts = $customer['customer_contacts'];

        $user = $modelUser->getById($invoice['booking']['created_by']);

        $viewParam = $this->getInvoiceViewParam($id, true);
        $view = new Zend_View();
        $view->setScriptPath(APPLICATION_PATH . '/modules/invoices/views/scripts/index');
        $view->bookingServices = $viewParam['bookingServices'];
        $view->thisBookingServices = $viewParam['thisBookingServices'];
        $view->priceArray = $viewParam['priceArray'];
        $view->invoice = $viewParam['invoice'];
        $view->customer = $customer;
        $view->isAccepted = $modelBooking->checkBookingIfAccepted($invoice['booking_id']);
        $view->booking = $invoice['booking'];
        $bodyInvoice = $view->render('invoice.phtml');


        // Create pdf
        $pdfPath = createPdfPath();
        $destination = $pdfPath['fullDir'] . $viewParam['invoice']['invoice_num'] . '.pdf';
        wkhtmltopdf($bodyInvoice, $destination);
        //echo 'destination   '.$destination;
        $parts = explode("public", $destination);
        //print_r($parts);
        $attachment = $parts[1];

        $totalAmountDetails = $modelBooking->getTotalAmountBookingDetails($invoice['booking']['booking_id'], true);
        $totalQoute = $totalAmountDetails['total'];


        $template_params = array(
            //invoice
            '{invoice_num}' => $invoice['invoice_num'],
            '{invoice_created}' => date('d/m/Y', $invoice['created']),
            //booking
            '{booking_num}' => $invoice['booking']['booking_num'],
            '{total_without_tax}' => number_format($invoice['booking']['sub_total'], 2),
            '{gst_tax}' => number_format($invoice['booking']['gst'], 2),
            '{total_with_tax}' => number_format($totalQoute, 2),
            '{description}' => $invoice['booking']['description'] ? $invoice['booking']['description'] : '',
            '{booking_created}' => date('d/m/Y', $invoice['booking']['created']),
            '{booking_created_by}' => ucwords($user['username']),
            '{booking_start}' => date("F j, Y, g:i a", strtotime($invoice['booking']['booking_start'])),
            '{booking_address}' => get_line_address($modelBookingAddress->getByBookingId($invoice['booking']['booking_id'])),
            '{service}' => nl2br($modelContractorServiceBooking->getBookingAsText($invoice['booking']['booking_id'], true)),
            '{invoice_view}' => $bodyInvoice,
            //customer
            '{customer_name}' => get_customer_name($customer),
            '{customer_first_name}' => isset($customer['first_name']) && $customer['first_name'] ? ucwords($customer['first_name']) : '',
            '{customer_last_name}' => isset($customer['last_name']) && $customer['last_name'] ? ' ' . ucwords($customer['last_name']) : '',
            '{customer_contacts}' => nl2br($modelCustomer->getCustomerContacts($invoice['booking']['customer_id'])),
        );

        $modelEmailTemplate = new Model_EmailTemplate();
        $emailTemplate = $modelEmailTemplate->getEmailTemplate('send_invoice_as_email', $template_params);

        //print_r($emailTemplate); 
        $body = $emailTemplate['body'];
        $subject = $emailTemplate['subject'];
        $to = array();
        if ($customer['email1']) {
            $to[] = $customer['email1'];
        }
        if ($customer['email2']) {
            $to[] = $customer['email2'];
        }
        if ($customer['email2']) {
            $to[] = $customer['email3'];
        }
        $to = implode(',', $to);
        $email = array('to' => $to, 'subject' => $subject, 'body' => $body, 'attachment' => $attachment, 'attachment_full_path' => $destination);



        $data['result'] = $email;
        $data['authrezed'] = 1;
        echo json_encode($data);
        exit;
    }

    public function getInvoiceViewParam($invoiceId, $toBuffer = false) {

        //
        // load model
        //
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelContractorServiceBookingTemp = new Model_ContractorServiceBookingTemp();
        $modelBooking = new Model_Booking();

        //
        // geting data
        //
        $invoice = $modelBookingInvoice->getById($invoiceId);


        //
        // validation
        //
        if (!$invoice) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Invoice not exist 7"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // filling extra data
        //
        $modelBookingInvoice->fill($invoice, array('booking', 'labels', 'due_date', 'multiple_days'));

        // $thisBookingServices : to put all service for this booking 
        $thisBookingServices = array();

        // $priceArray : to put all price and service for this booking 
        $priceArray = array();


        /*
         *  $bookingServices : to put all booking service from  original booking service (or from temp if the contractor change values untill approved)
         */

        //$bookingServices = $modelContractorServiceBooking->getByBookingId($invoice['booking_id']);

        $contractorServiceBookings = $modelContractorServiceBooking->getByBookingId($invoice['booking_id']);
        $contractorServiceBookingsTemp = $modelContractorServiceBookingTemp->getByBookingId($invoice['booking_id']);
        $isTemp = false;
        $bookingServices = array();
        if ($contractorServiceBookingsTemp && !$modelBooking->checkCanEditBookingDetails($invoice['booking_id']) && !$toBuffer) {
            $bookingServices = $contractorServiceBookingsTemp;
            $isTemp = true;
            foreach ($bookingServices as $bookingService) {

                $serviceId = $bookingService['service_id'];
                $clone = $bookingService['clone'];
                $bookingId = $bookingService['booking_id'];

                $service_and_clone = $serviceId . ($clone ? '_' . $clone : '');

                $thisBookingServices[] = $service_and_clone;

                $priceArray[$service_and_clone] = $modelContractorServiceBookingTemp->getTotalServiceBookingQoute($bookingId, $serviceId, $clone);
            }
        } elseif ($contractorServiceBookings) {
            $bookingServices = $contractorServiceBookings;
            foreach ($bookingServices as $bookingService) {

                $serviceId = $bookingService['service_id'];
                $clone = $bookingService['clone'];
                $bookingId = $bookingService['booking_id'];

                $service_and_clone = $serviceId . ($clone ? '_' . $clone : '');

                $thisBookingServices[] = $service_and_clone;

                $priceArray[$service_and_clone] = $modelContractorServiceBooking->getTotalServiceBookingQoute($bookingId, $serviceId, $clone);
            }
        }

        if (!$toBuffer) {

            $this->view->invoice = $invoice;
            $this->view->bookingServices = $bookingServices;
            $this->view->thisBookingServices = $thisBookingServices;
            $this->view->priceArray = $priceArray;
            $this->view->isTemp = $isTemp;
        } else {
            $viewParam = array();

            $viewParam['invoice'] = $invoice;

            $viewParam['bookingServices'] = $bookingServices;
            $viewParam['thisBookingServices'] = $thisBookingServices;
            $viewParam['priceArray'] = $priceArray;
            //$viewParam['isTemp'] = $isTemp;

            return $viewParam;
        }

        return false;
    }

    public function sendEmailFromAppAction() {

        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $modelCompanies = new Model_Companies();

        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);

          $this->iosLoggedUser = $user['id'];

          $loggedUser = CheckAuth::getLoggedUser();

          $data = array('authrezed' => 0);
          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {

          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */
        $this->checkAccessToken($accessToken);
        $loggedUser = CheckAuth::getLoggedUser();

        $type = $this->request->getParam('type', 'invoice');
        $id = $this->request->getParam('id', 0);
        $to = $this->request->getParam('to');
        $cc = $this->request->getParam('cc');
        $subject = $this->request->getParam('subject');
        $body = $this->request->getParam('body');
        $pdf_attachment = $this->request->getParam('pdf_attachment', 0);
        $attachment_full_path = $this->request->getParam('attachment_full_path', 0);

        $params = array(
            'to' => $to,
            'cc' => $cc,
            'body' => urldecode($body),
            'subject' => urldecode($subject)
        );


        if (!empty($pdf_attachment)) {
            // Create pdf
            /* $pdfPath = createPdfPath();
              $destination = $pdfPath['fullDir'] . $viewParam['invoice']['invoice_num'] . '.pdf';
              wkhtmltopdf($bodyInvoice, $destination); */
            $params['attachment'] = $attachment_full_path;
        }

        // Send Email
        $success = EmailNotification::sendEmail($params, '', array(), array('reference_id' => $id, 'type' => 'invoice'));

        if ($success) {

            $data['msg'] = 'Sent Successfully';
        } else {

            $data['msg'] = 'Could not send this email, please try again';
        }

        $data['authrezed'] = 1;
        echo json_encode($data);
        exit;
    }

    public function getServicesAction() {

        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $modelCompanies = new Model_Companies();

        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);

          $this->iosLoggedUser = $user['id'];

          $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);
          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {

          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */

        $this->checkAccessToken($accessToken);
        $loggedUser = CheckAuth::getLoggedUser();

        $servicesObj = new Model_Services();
        $services = $servicesObj->getServiceAsArray();


        $data['result'] = $services;
        $data['authrezed'] = 1;
        echo json_encode($data);
        exit;
    }

    /* public function getPhotosAction() {

      header('Content-Type: application/json');
      $accessToken = $this->request->getParam('access_token', 0);
      $booking_id = $this->request->getParam('booking_id', 0);
      $user_role = $this->request->getParam('user_role', 'contractor');
      $customer_id = $this->request->getParam('customer_id', 0);
      $modelCompanies = new Model_Companies();

      $this->checkAccessToken($accessToken,$user_role,$customer_id);

      /*if ($user_role == 'contractor') {
      $modelIosUser = new Model_IosUser();
      $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
      $this->iosLoggedUser = $user['id'];
      }

      $loggedUser = CheckAuth::getLoggedUser();
      $data = array('authrezed' => 0);
      if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken, $user_role))) {
      $data['msg'] = 'wrong access token';
      echo json_encode($data);
      exit;
      }

      if (empty($loggedUser)) {

      //open new session
      $authrezed = $this->openNewSession($accessToken, $user_role, $customer_id);
      if (!$authrezed) {
      echo json_encode($data);
      exit;
      }
      }/

      $loggedUser = CheckAuth::getLoggedUser();
      $logged_user_id = ($user_role == 'customer') ? $loggedUser['customer_id'] : $loggedUser['user_id'];
      $filters = array();
      if ($user_role == 'customer') {
      $filters = array('user_id' => $logged_user_id, 'user_role' => 'customer');

      }

      $modelImage = new Model_Image();
      $modelUser = new Model_User();
      $modelCustomer =  new Model_Customer();
      $photos = $modelImage->getAll($booking_id, 'booking');
      $modelBookingDiscussion = new Model_BookingDiscussion();
      $result = array();
      $modelAuthRole = new Model_AuthRole();
      $customerRoleId = $modelAuthRole->getRoleIdByName('customer');

      foreach ($photos as $key => $photo) {
      $result[$key]['image_id'] = $photo['image_id'];
      $result[$key]['original_path'] = $photo['compressed_path'];
      $result[$key]['thumbnail_path'] = $photo['thumbnail_path'];
      $result[$key]['image_types_id'] = isset($photo['image_types_id']) ? $photo['image_types_id'] : 0;
      $result[$key]['service_id'] = $photo['service_id'];
      if($photo['user_role'] == $customerRoleId){
      $modelCustomer = new Model_Customer();
      $customer = $modelCustomer->getById($photo['created_by']);
      $display_name = get_customer_name($customer);
      }else{
      $modelUser = new Model_User();
      $user = $modelUser->getById($photo['created_by']);
      $display_name = $user['display_name'];
      }
      $result[$key]['created_by'] = $display_name;
      $result[$key]['created'] = strtotime($photo['created']);


      $imageDiscussions = $modelBookingDiscussion->getByImageId($photo['image_id'], 'DESC', $photo['group_id'], $filters);

      $Discussions = array();
      if($imageDiscussions){
      foreach ($imageDiscussions as $k => $imageDiscussion) {
      $discussion = array();
      $discussion['discussion_id'] = $imageDiscussion['discussion_id'];
      $discussion['booking_id'] = $imageDiscussion['booking_id'];
      $discussion['user_id'] = $imageDiscussion['user_id'];

      if ($imageDiscussion['user_role'] == 9) {
      $modelCustomer = new Model_Customer();
      $user = $modelCustomer->getById($imageDiscussion['user_id']);
      $username = $user['first_name'] . ' ' . $user['last_name'];
      } else {
      $user = $modelUser->getById($imageDiscussion['user_id']);
      $username = $user['username'];
      }

      $discussion['user_message'] = $imageDiscussion['user_message'];
      $discussion['created'] = $imageDiscussion['created'];
      $discussion['username'] = $username;
      $Discussions[] = $discussion;
      }
      }
      $result[$key]['discussions'] = $Discussions;
      }

      $data['result'] = $result;
      $data['booking_id'] = $booking_id;
      $data['authrezed'] = 1;
      echo json_encode($data);
      exit;
      } */

    public function getPhotosAction() {

        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $booking_id = $this->request->getParam('booking_id', 0);
        $user_role = $this->request->getParam('user_role', 'contractor');
        $customer_id = $this->request->getParam('customer_id', 0);
        $modelCompanies = new Model_Companies();
        $this->checkAccessToken($accessToken, $user_role, $customer_id);

        /* if ($user_role == 'contractor') { 
          $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];
          }

          $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);
          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken, $user_role))) {
          $data['msg'] = 'wrong access token';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {

          //open new session
          $authrezed = $this->openNewSession($accessToken, $user_role, $customer_id);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */

        $loggedUser = CheckAuth::getLoggedUser();
        $logged_user_id = ($user_role == 'customer') ? $loggedUser['customer_id'] : $loggedUser['user_id'];
        $filters = array();
        if ($user_role == 'customer') {
            $filters = array('user_id' => $logged_user_id, 'user_role' => 'customer');
        }


        $modelImage = new Model_Image();
        $photos = $modelImage->getAll($booking_id, 'booking');
        $modelBookingDiscussion = new Model_BookingDiscussion();
        $result = array();
        $modelAuthRole = new Model_AuthRole();
        //$contractorRoleId = $modelAuthRole->getRoleIdByName('contractor');
        $customerRoleId = $modelAuthRole->getRoleIdByName('customer');
        foreach ($photos as $key => $photo) {
            $result[$key]['image_id'] = $photo['image_id'];
            $result[$key]['original_path'] = $photo['compressed_path'];
            $result[$key]['thumbnail_path'] = $photo['thumbnail_path'];
            $result[$key]['image_types_id'] = isset($photo['image_types_id']) ? $photo['image_types_id'] : 0;
            if ($photo['user_role'] == $customerRoleId) {
                $modelCustomer = new Model_Customer();
                $customer = $modelCustomer->getById($photo['created_by']);
                $display_name = get_customer_name($customer);
            } else {
                $modelUser = new Model_User();
                $user = $modelUser->getById($photo['created_by']);
                $display_name = $user['display_name'];
            }
            $result[$key]['created_by'] = $display_name;
            $result[$key]['created'] = strtotime($photo['created']);
            $result[$key]['service_id'] = $photo['service_id'];
            $imageDiscussions = $modelBookingDiscussion->getByImageId($photo['image_id'], 'DESC', $photo['group_id'], $filters);

            $Discussions = array();
            foreach ($imageDiscussions as $k => $imageDiscussion) {
                $discussion = array();
                $discussion['discussion_id'] = $imageDiscussion['discussion_id'];
                $discussion['booking_id'] = $imageDiscussion['booking_id'];
                $discussion['user_id'] = $imageDiscussion['user_id'];
                $discussion['user_message'] = $imageDiscussion['user_message'];
                $Discussions[] = $discussion;
            }
            $result[$key]['discussions'] = $Discussions;
            $result[$key]['count'] = count($Discussions);
        }

        $data['result'] = $result;
        $data['booking_id'] = $booking_id;
        $data['authrezed'] = 1;
        echo json_encode($data);
        exit;
    }

    /* public function getRefundsAction(){


      header('Content-Type: application/json');
      $accessToken = $this->request->getParam('access_token',0);
      $modelIosUser = new Model_IosUser();
      $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);

      $this->iosLoggedUser = $user['id'];
      $modelCompanies = new Model_Companies();
      $loggedUser = CheckAuth::getLoggedUser();
      $data = array('authrezed' => 0);
      if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
      $data['msg'] = 'wrong access token';
      echo json_encode($data);
      exit;
      }

      if (empty($loggedUser)) {

      //open new session
      $authrezed = $this->openNewSession($accessToken);
      if(!$authrezed){
      echo json_encode($data);
      exit;
      }

      }

      $modelRefund = new Model_Refund();
      $Refunds = $modelRefund->getAll();

      $data['result'] = $Refunds;
      $data['authrezed'] = 1;
      echo json_encode($data);
      exit;

      } */

    public function addEditRefundAction() {
        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $amount = $this->request->getParam('amount', 0);
        $id = $this->request->getParam('id');

        $this->checkAccessToken($accessToken);
        $loggedUser = CheckAuth::getLoggedUser();

        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];
          $loggedUser = CheckAuth::getLoggedUser();
          //$this->iosLoggedUser = $iosUserId;
          $data = array('authrezed' => 0);


          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          echo json_encode($data);
          exit;
          }
          if (empty($loggedUser)) {
          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */
        if (isset($id)) {
            $result = $this->editRefund();
        } else {
            $result = $this->addRefund();
        }

        foreach ($result as $key => $row) {
            $data[$key] = $row;
        }

        //$data['result'] = $result;
        $data['authrezed'] = 1;

        echo json_encode($data);
        exit;
    }

    public function editRefund() {

        CheckAuth::checkPermission(array('refundEdit'));


        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $receivedDate = $this->request->getParam('received_date');
        $bankCharges = $this->request->getParam('bank_charges');
        $amount = $this->request->getParam('amount');
        $description = $this->request->getParam('description');
        $reference = $this->request->getParam('reference');
        $amountWithheld = $this->request->getParam('amount_withheld');
        $withholdingTax = $this->request->getParam('withholding_tax');
        $isAcknowledgment = $this->request->getParam('is_acknowledgment');
        $paymentTypeId = $this->request->getParam('payment_type_id');
        $loggedUser = CheckAuth::getLoggedUser();

        $contractorId = $loggedUser['user_id'];


        //
        // Load Model
        //
        $modelRefund = new Model_Refund();
        $modelBooking = new Model_Booking();
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelPayment = new Model_Payment();

        //
        //Validation
        //
        $refund = $modelRefund->getById($id);



        if ($refund['is_approved']) {
            return array('type' => 'error', 'msg' => "Could not edit Approved Refund, Please change to Unapproved to edit");
        }

        $booking = $modelBooking->getById($refund['booking_id']);
        if (!$booking) {
            return array('type' => 'error', 'msg' => "Invalid Booking");
        }

        if (!$modelBooking->checkIfCanEditBooking($booking['booking_id'])) {
            return array('type' => 'error', 'msg' => "You Don't Have Permission");
        }

        //
        //calculate amount
        //
        
                $data = array(
            'received_date' => strtotime($receivedDate),
            'bank_charges' => round($bankCharges, 2),
            'amount' => round($amount, 2),
            'description' => $description,
            'payment_type_id' => $paymentTypeId,
            'booking_id' => $booking['booking_id'],
            'customer_id' => $booking['customer_id'],
            'user_id' => $loggedUser['user_id'],
            'created' => time(),
            'reference' => $reference,
            'amount_withheld' => round($amountWithheld, 2),
            'withholding_tax' => $withholdingTax,
            'is_acknowledgment' => $isAcknowledgment,
            'contractor_id' => $contractorId
        );

        $success = $modelRefund->updateById($id, $data);
        if ($success) {
            $modelBookingInvoice->updateByBookingId($booking['booking_id'], array('invoice_type' => 'open'));
            return array('type' => 'success', 'msg' => "Refund saved successfully, Please Approved this Refund", 'result' => $this->getBooking(false, array('booking_id' => $booking['booking_id'])));
        } else {
            return array('type' => 'error', 'msg' => "No Changes in Refund");
        }
    }

    public function deleteRefundAction() {


        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $this->checkAccessToken($accessToken);
        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];
          $loggedUser = CheckAuth::getLoggedUser();
          //$this->iosLoggedUser = $iosUserId;
          $data = array('authrezed' => 0);


          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          echo json_encode($data);
          exit;
          }
          if (empty($loggedUser)) {
          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */
        $result = $this->deleteRefund();
        $data['result'] = $result;
        $data['authrezed'] = 1;

        echo json_encode($data);
        exit;
    }

    public function deleteRefund() {

        CheckAuth::checkPermission(array('refundDelete'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $bookingId = $this->request->getParam('booking_id', 0);

        //
        // Load MODEL
        //
        $modelBooking = new Model_Booking();
        $modelRefund = new Model_Refund();

        //
        //Validation
        //
        if (!$modelBooking->checkIfCanEditBooking($bookingId)) {
            return array('type' => 'error', 'message' => "You Don't Have Permission");
        }

        $refund = $modelRefund->getByIdAndBookingId($id, $bookingId);
        if ($refund['is_approved']) {
            return array('type' => 'error', 'message' => "Could not delete Approved Refund, Please change to Unapproved to delete");
        }


        $deleted = 0;
        if ($refund) {
            //delete refund
            $deleted = $modelRefund->deleteById($id);
        } else {
            return array('type' => 'error', 'message' => "Refund not exists");
        }

        if ($deleted) {
            return array('type' => 'success', 'message' => "the refund deleted successfully");
        }
    }

    public function addRefund() {


        CheckAuth::checkPermission(array('refundAdd'));



        //
        // get request parameters
        //
        $receivedDate = $this->request->getParam('received_date');
        $bankCharges = $this->request->getParam('bank_charges');
        $amount = $this->request->getParam('amount');
        $description = $this->request->getParam('description');
        $reference = $this->request->getParam('reference');
        $amountWithheld = $this->request->getParam('amount_withheld');
        $withholdingTax = $this->request->getParam('withholding_tax');
        $isAcknowledgment = $this->request->getParam('is_acknowledgment');
        $paymentTypeId = $this->request->getParam('payment_type_id');
        $bookingId = $this->request->getParam('booking_id', 0);
        $loggedUser = CheckAuth::getLoggedUser();


        $contractorId = $loggedUser['user_id'];

        //
        // Load MODEL
        //
        $modelBooking = new Model_Booking();
        $modelPayment = new Model_Payment();
        $modelRefund = new Model_Refund();
        $modelBookingInvoice = new Model_BookingInvoice();

        //
        //get booking 
        //
        $booking = $modelBooking->getById($bookingId);
        if (!$booking) {
            return array('type' => 'error', 'msg' => "Invalid Booking");
        }



        if (!$modelBooking->checkIfCanEditBooking($booking['booking_id'])) {
            return array('type' => 'error', 'msg' => "You Don't Have Permission");
        }

        $approvedPayment = $modelPayment->getTotalPayment(array('booking_id' => $booking['booking_id'], 'is_approved' => 'yes'));
        if ($approvedPayment <= 0) {
            return array('type' => 'error', 'msg' => "You Con't Add Refund , No Approved Payment");
        }

        $approvedRefund = $modelRefund->getTotalRefund(array('booking_id' => $booking['booking_id'], 'is_approved' => 'yes'));
        if ($approvedRefund >= $approvedPayment) {
            return array('type' => 'error', 'msg' => "fully Refund");
        }

        $allRefund = $modelRefund->getTotalRefund(array('booking_id' => $booking['booking_id'], 'is_approved' => 'all'));
        if ($allRefund >= $approvedPayment) {
            return array('type' => 'error', 'msg' => "fully Refund,Check Unapproved Refund");
        }

        $data = array(
            'received_date' => strtotime($receivedDate),
            'bank_charges' => round($bankCharges, 2),
            'amount' => round($amount, 2),
            'description' => $description,
            'payment_type_id' => $paymentTypeId,
            'booking_id' => $booking['booking_id'],
            'customer_id' => $booking['customer_id'],
            'user_id' => $loggedUser['user_id'],
            'created' => time(),
            'reference' => $reference,
            'amount_withheld' => round($amountWithheld, 2),
            'withholding_tax' => $withholdingTax,
            'is_acknowledgment' => $isAcknowledgment,
            'contractor_id' => $contractorId
        );

        $success = $modelRefund->insert($data);

        if ($success) {
            $modelBookingInvoice->updateByBookingId($booking['booking_id'], array('invoice_type' => 'open'));
            return array('type' => 'success', 'msg' => "Refund saved successfully, Please Approved this Refund", 'result' => $this->getBooking(false, array('booking_id' => $bookingId)));
        } else {
            return array('type' => 'error', 'msg' => "No Changes in Refund");
        }
    }

	public function getAllDiscussionByContractorAction(){ 
        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $user_role = $this->request->getParam('user_role', 'contractor');
        $customer_id = $this->request->getParam('customer_id', 0);


        $this->checkAccessToken($accessToken, $user_role, $customer_id);


        $loggedUser = CheckAuth::getLoggedUser();
        
        $logged_user_id = (int)$loggedUser['user_id'];
		
        $modelBookingDiscussion = new Model_ItemImageDiscussion();
        $mergedArray = $modelBookingDiscussion->getAllDiscussionByContractor($logged_user_id);

        $data['result'] = $mergedArray;
        $data['authrezed'] = 1;

        echo json_encode($data);
        exit;
		
			
			
	}		
    
    public function getAllDiscussionByContractor11111Action(){
         header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $user_role = $this->request->getParam('user_role', 'contractor');
        $customer_id = $this->request->getParam('customer_id', 0);


        $this->checkAccessToken($accessToken, $user_role, $customer_id);


        $loggedUser = CheckAuth::getLoggedUser();
        $logged_user_id = ($user_role == 'customer') ? $loggedUser['customer_id'] : $loggedUser['user_id'];
        $filters = array();
        $lastContractorDiscussion = array();
        if ($user_role == 'customer') {
            $filters = array('user_id' => $logged_user_id, 'user_role' => 'customer');
        }else{
            $modelContractorDiscussion = new Model_ContractorDiscussionMongo();
            $filters = array('user_id' => $logged_user_id, 'user_role' => 'contractor');
            $lastContractorDiscussion =  $modelContractorDiscussion->getByContractorIdthelast($logged_user_id);
        }

		
			$modelContractorDiscussion = new Model_ContractorDiscussionMongo();
            $filters = array('user_id' => $logged_user_id, 'user_role' => 'contractor');
            $lastContractorDiscussion =  $modelContractorDiscussion->getByContractorIdthelast($logged_user_id);
			
			$filters = array('user_id' => '8', 'user_role' => 'contractor');
            $modelBookingDiscussion = new Model_BookingDiscussion();
            $modelComplaintDiscussion = new Model_ComplaintDiscussion();            
            
            $BookingDiscussions = $modelBookingDiscussion->getByContractorId('DESC', $filters);
            $ComplaintDiscussions = $modelComplaintDiscussion->getByContractorId('DESC', $filters);           
            $mergedArray = array_merge($lastContractorDiscussion,$BookingDiscussions, $ComplaintDiscussions);
           // print_r($mergedArray);exit;



        $modelImage = new Model_Image();
        $modelContractorInfo = new Model_ContractorInfo();
        $modelUser = new Model_User();
        $results = array();
        $userRole = '';
        $modelAuthRole = new Model_AuthRole();
        foreach ($mergedArray as $key => $discussion) {
            if(!isset($discussion['user_role'])){
                $discussion['user_role'] = 1;
            }

            if (($modelAuthRole->getRoleIdByName('customer') == $discussion['user_role']) && isset($discussion['user_role'])) {
                $modelCustomer = new Model_Customer();
                $modelCustomerCommercialInfo = new Model_CustomerCommercialInfo();
                $DiscussionUser = $modelCustomer->getById($discussion['user_id']);
                $DiscussionUserInfo = $modelCustomerCommercialInfo->getByCustomerId($discussion['user_id']);
                $username = $DiscussionUser['first_name'];
                $avatar = '';
            } else {
                $DiscussionUser = $modelUser->getById($discussion['user_id']);
                $DiscussionUserInfo = $modelContractorInfo->getByContractorId($discussion['user_id']);
                $username = $DiscussionUser['username'];
                $avatar = $_SERVER['HTTP_HOST'] . '/uploads/user_pic/thumb_medium/' . $DiscussionUser['avatar'];
            }


           

                if (isset($discussion['large_path'])) {
                    $results[$key]['large_path'] = $discussion['large_path'];
                } else {
                    $results[$key]['large_path'] = '';
                }
                if (isset($discussion['thumbnail_path'])) {
                    $results[$key]['thumbnail_path'] = $discussion['thumbnail_path'];
                } else {
                    $results[$key]['thumbnail_path'] = '';
                }
                $results[$key]['images'] = array();
            
            $results[$key]['user_message'] = $discussion['user_message'];
            $results[$key]['created'] = isset($discussion['created']) ? $discussion['created'] : '';
            $results[$key]['username'] = $username;
            $results[$key]['avatar'] = $avatar;            
            $results[$key]['user_id'] = $discussion['user_id'];
            if ($DiscussionUserInfo['business_name']) {
                $results[$key]['business_name'] = $DiscussionUserInfo['business_name'];
            } else {
                $results[$key]['business_name'] = '';
            }
        }

        //$loggedUser = CheckAuth::getLoggedUser();
        if ($user_role == 'customer') {
            $modelCustomerCommercialInfo = new Model_CustomerCommercialInfo();
            $customerInfo = $modelCustomerCommercialInfo->getByCustomerId($logged_user_id);
            $data['business_name'] = isset($customerInfo['business_name']) ? $customerInfo['business_name'] : '';
        } else {
            $contractorInfo = $modelContractorInfo->getByContractorId($logged_user_id);
            $data['business_name'] = isset($contractorInfo['business_name']) ? $contractorInfo['business_name'] : '';
        }


        $data['result'] = $results;
        $data['authrezed'] = 1;
        $data['username'] = $loggedUser['username'];
        echo json_encode($data);
        exit;
    }
    public function getAllDiscussionAction() {

        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $type = $this->request->getParam('type', '');
        $itemId = $this->request->getParam('id', 0);
        $user_role = $this->request->getParam('user_role', 'contractor');
        $customer_id = $this->request->getParam('customer_id', 0);

        $modelCompanies = new Model_Companies();
        $modelBookingReminder = new Model_BookingReminder();

        $this->checkAccessToken($accessToken, $user_role, $customer_id);


        /* $loggedUser = CheckAuth::getLoggedUser();
          if ($user_role == 'contractor') {
          $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];
          }
          $data = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken, $user_role))) {
          $data['msg'] = 'wrong access token';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {
          //open new session
          $authrezed = $this->openNewSession($accessToken, $user_role, $customer_id);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */


        $loggedUser = CheckAuth::getLoggedUser();
        $logged_user_id = ($user_role == 'customer') ? $loggedUser['customer_id'] : $loggedUser['user_id'];
        $filters = array();
        if ($user_role == 'customer') {
            $filters = array('user_id' => $logged_user_id, 'user_role' => 'customer');
        }
        $Discussions = array();
        if ($type == 'booking') {
            $modelBookingDiscussion = new Model_BookingDiscussion();
            $Discussions = $modelBookingDiscussion->getByBookingId($itemId, 'DESC', $filters);
            //$filters['groups'] = 'groups';
            //$GroupsImageDiscussions = $modelBookingDiscussion->getByBookingId($itemId, 'DESC', $filters);
            //$Discussions = array_merge($bookingDiscussions, $GroupsImageDiscussions);
            //array_multisort($Discussions, SORT_ASC);
        } else if ($type == 'complaint') {
            $Discussions = array();
            $modelComplaintDiscussion = new Model_ComplaintDiscussion();
            $Discussions = $modelComplaintDiscussion->getByComplaintId($itemId, 'DESC', $filters);
            //$GroupscomplaintImageDiscussions = $modelComplaintDiscussion->getByComplaintId($itemId, 'created DESC', array('groups' => 'groups'));			
            //$Discussions = array_merge($complaintDiscussions, $GroupscomplaintImageDiscussions);
            //array_multisort($Discussions, SORT_ASC);
        }



        $modelImage = new Model_Image();
        $modelContractorInfo = new Model_ContractorInfo();
        $modelUser = new Model_User();
        $results = array();
        $userRole = '';
        $modelAuthRole = new Model_AuthRole();
        foreach ($Discussions as $key => $discussion) {

            if (($modelAuthRole->getRoleIdByName('customer') == $discussion['user_role']) && isset($discussion['user_role'])) {
                $modelCustomer = new Model_Customer();
                $modelCustomerCommercialInfo = new Model_CustomerCommercialInfo();
                $DiscussionUser = $modelCustomer->getById($discussion['user_id']);
                $DiscussionUserInfo = $modelCustomerCommercialInfo->getByCustomerId($discussion['user_id']);
                $username = $DiscussionUser['first_name'];
                $avatar = '';
            } else {
                $DiscussionUser = $modelUser->getById($discussion['user_id']);
                $DiscussionUserInfo = $modelContractorInfo->getByContractorId($discussion['user_id']);
                $username = $DiscussionUser['username'];
                $avatar = $_SERVER['HTTP_HOST'] . '/uploads/user_pic/thumb_medium/' . $DiscussionUser['avatar'];
            }


            if ($discussion['group_id']) {

                $grouped_images = $modelImage->getByGroupIdAndType($discussion['group_id'], $itemId, $type);

                $results[$key]['images'] = $grouped_images;
                $results[$key]['thumbnail_path'] = '';
                $results[$key]['large_path'] = '';
            } else {

                if (isset($discussion['large_path'])) {
                    $results[$key]['large_path'] = $discussion['large_path'];
                } else {
                    $results[$key]['large_path'] = '';
                }
                if (isset($discussion['thumbnail_path'])) {
                    $results[$key]['thumbnail_path'] = $discussion['thumbnail_path'];
                } else {
                    $results[$key]['thumbnail_path'] = '';
                }
                $results[$key]['images'] = array();
            }
            $results[$key]['user_message'] = $discussion['user_message'];
            $results[$key]['created'] = isset($discussion['created']) ? $discussion['created'] : '';
            $results[$key]['username'] = $username;
            $results[$key]['avatar'] = $avatar;
            $results[$key]['discussion_id'] = $discussion['discussion_id'];
            $results[$key]['user_id'] = $discussion['user_id'];
            if ($DiscussionUserInfo['business_name']) {
                $results[$key]['business_name'] = $DiscussionUserInfo['business_name'];
            } else {
                $results[$key]['business_name'] = '';
            }
        }

        $loggedUser = CheckAuth::getLoggedUser();
        if ($user_role == 'customer') {
            $modelCustomerCommercialInfo = new Model_CustomerCommercialInfo();
            $customerInfo = $modelCustomerCommercialInfo->getByCustomerId($logged_user_id);
            $data['business_name'] = isset($customerInfo['business_name']) ? $customerInfo['business_name'] : '';
        } else {
            $contractorInfo = $modelContractorInfo->getByContractorId($logged_user_id);
            $data['business_name'] = isset($contractorInfo['business_name']) ? $contractorInfo['business_name'] : '';
        }


        $data['result'] = $results;
        $data['authrezed'] = 1;
        $data['username'] = $loggedUser['username'];
        echo json_encode($data);
        exit;
    }

    public function addDiscussionForIosAction() {

        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $itemId = $this->request->getParam('id', 0);
        $type = $this->request->getParam('type', '');
        $discussion = $this->request->getParam('discussion', 0);
        $user_role = $this->request->getParam('user_role', 'contractor');
        $customer_id = $this->request->getParam('customer_id', 0);
        $discussion_images = $this->request->getParam('discussion_images', array());


        $this->checkAccessToken($accessToken, $user_role, $customer_id);

        /* $loggedUser = CheckAuth::getLoggedUser();
          if ($user_role == 'contractor') {
          $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];
          }
          $data = array('authrezed' => 0);
          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken, $user_role))) {
          $data['msg'] = 'wrong access token';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {
          //open new session
          $authrezed = $this->openNewSession($accessToken, $user_role, $customer_id);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */


        $modelBookingDiscussion = new Model_BookingDiscussion();
        $modelComplaintDiscussion = new Model_ComplaintDiscussion();
        $modelImage = new Model_Image();
        $loggedUser = CheckAuth::getLoggedUser();




        $logged_user_id = ($user_role == 'customer') ? $loggedUser['customer_id'] : $loggedUser['user_id'];

        //var_dump($logged_user_id); 

        $dir = get_config('image_attachment') . '/';
        $subdir = date('Y/m/d/');
        $fullDir = $dir . $subdir;
        if (!is_dir($fullDir)) {
            mkdir($fullDir, 0777, true);
        }

        $success = 0;
        if ($discussion) {
            if ((isset($discussion_images) && !empty($discussion_images))) {
                $image_group = 0;
                $count = count($discussion_images);
                if ($count > 1) {
                    $maxGroup = max($modelImage->getMaxGroup());
                    $image_group = $maxGroup['group_id'] + 1;
                    if ($type == 'booking') {
                        $params = array(
                            'booking_id' => $itemId,
                            'user_id' => $logged_user_id,
                            'user_message' => $discussion,
                            'created' => time(),
                            'user_role' => $loggedUser['role_id']
                        );

                        $success = $modelBookingDiscussion->insert($params);
                    } else if ($type == 'complaint') {
                        $params = array(
                            'complaint_id' => $itemId,
                            'user_id' => $loggedUser['user_id'],
                            'user_message' => $discussion,
                            'created' => time()
                        );
                        $success = $modelComplaintDiscussion->insert($params);
                    }

                    $modelItemImageDiscussion = new Model_ItemImageDiscussion();
                    $dataDiscssion = array(
                        'image_id' => 0,
                        'group_id' => $image_group,
                        'item_id' => $success,
                        'type' => $type
                    );

                    $modelItemImageDiscussion->insert($dataDiscssion);
                }

                foreach ($discussion_images as $key => $imageData) {
                    if (isset($imageData) && !empty($imageData)) {
                        $image = str_replace('data:image/png;base64,', '', $imageData);
                        $image = str_replace(' ', '+', $image);
                        $image = base64_decode($image);
                        $fileName = "image_{$key}.png";
                        $file = $fullDir . $fileName;
                        $success_upload_photo = file_put_contents($file, $image);
                        if ($success_upload_photo) {
                            $image_saved = copy($file, $fullDir . $fileName);
                            if ($image_saved) {
                                $successUpload = 1;
                                $data = array(
                                    'user_ip' => ''
                                    , 'created_by' => $logged_user_id
                                    , 'image_types_id' => 0
                                    , 'group_id' => $image_group
                                    , 'user_role' => $loggedUser['role_id']
                                );

                                $id = $modelImage->insert($data);
                                $image_id = $id;
                                if ($count > 1) {
                                    $image_id = 0;
                                }
                                if ($count == 1) {
                                    if ($type == 'booking') {
                                        $params = array(
                                            'booking_id' => $itemId,
                                            'user_id' => $logged_user_id,
                                            'user_message' => $discussion,
                                            'created' => time(),
                                            'user_role' => $loggedUser['role_id']
                                        );

                                        $success = $modelBookingDiscussion->insert($params);
                                    } else if ($type == 'complaint') {
                                        $params = array(
                                            'complaint_id' => $itemId,
                                            'user_id' => $loggedUser['user_id'],
                                            'user_message' => $discussion,
                                            'created' => time()
                                        );
                                        $success = $modelComplaintDiscussion->insert($params);
                                    }

                                    $modelItemImageDiscussion = new Model_ItemImageDiscussion();
                                    $dataDiscssion = array(
                                        'image_id' => $image_id,
                                        'group_id' => $image_group,
                                        'item_id' => $success,
                                        'type' => $type
                                    );

                                    $modelItemImageDiscussion->insert($dataDiscssion);
                                }
                                $Itemdata = array(
                                    'item_id' => $itemId,
                                    'service_id' => 0,
                                    'discussion_id' => $success,
                                    'image_id' => $id,
                                    'type' => $type . '_discussion',
                                    'floor_id' => 0
                                );


                                $modelItemImage = new Model_ItemImage();
                                $Item_image_id = $modelItemImage->insert($Itemdata);


                                $original_path = "original_{$id}.{$ext}";
                                $large_path = "large_{$id}.{$ext}";
                                $small_path = "small_{$id}.{$ext}";
                                $thumbnail_path = "thumbnail_{$id}.{$ext}";
                                $compressed_path = "compressed_{$id}.{$ext}";

                                $data = array(
                                    'original_path' => $subdir . $original_path,
                                    'large_path' => $subdir . $large_path,
                                    'small_path' => $subdir . $small_path,
                                    'thumbnail_path' => $subdir . $thumbnail_path,
                                    'compressed_path' => $subdir . $compressed_path
                                );


                                $modelImage->updateById($id, $data);
                                $compressed_saved = copy($source, $fullDir . $compressed_path);
                                $image_saved = copy($source, $fullDir . $original_path);
                                ImageMagick::scale_image($source, $fullDir . $large_path, 510);
                                ImageMagick::scale_image($source, $fullDir . $small_path, 250);
                                ImageMagick::create_thumbnail($source, $fullDir . $thumbnail_path, 78, 64);
                                ImageMagick::convert($source, $fullDir . $compressed_path);
                                ImageMagick::compress_image($source, $fullDir . $compressed_path);
                            }
                            if ($image_saved) {
                                if (file_exists($source)) {
                                    unlink($source);
                                }
                                if (file_exists($file)) {
                                    unlink($file);
                                }
                            }
                        }
                    }
                }
            } else {

                if ($type == 'booking') {
                    $params = array(
                        'booking_id' => $itemId,
                        'user_id' => $logged_user_id,
                        'user_message' => $discussion,
                        'created' => time(),
                        'user_role' => $loggedUser['role_id']
                    );

                    $success = $modelBookingDiscussion->insert($params);
                } else if ($type == 'complaint') {
                    $params = array(
                        'complaint_id' => $itemId,
                        'user_id' => $loggedUser['user_id'],
                        'user_message' => $discussion,
                        'created' => time()
                    );
                    $success = $modelComplaintDiscussion->insert($params);
                }

                $modelItemImageDiscussion = new Model_ItemImageDiscussion();
                $dataDiscssion = array(
                    'image_id' => 0,
                    'group_id' => 0,
                    'item_id' => $success,
                    'type' => $type
                );

                $modelItemImageDiscussion->insert($dataDiscssion);
            }
        }



        if ($success) {
            $data['type'] = 'success';
            $data['msg'] = 'Discussion Added Successfully';
        } else {
            $data['type'] = 'error';
            $data['msg'] = 'Invalid Discussion';
        }

        $data['authrezed'] = 1;

        echo json_encode($data);
        exit;
    }

    public function addDiscussionAction() {

        $_REQUEST = array_merge($_REQUEST, (array) json_decode(file_get_contents("php://input"), true));
        header('Content-Type: application/json');
        /* $accessToken = $this->request->getParam('access_token', 0);
          $itemId = $this->request->getParam('id', 0);
          $type = $this->request->getParam('type', '');
          $discussion = $this->request->getParam('discussion', 0);
          $user_role = $this->request->getParam('user_role', 'contractor');
          $customer_id = $this->request->getParam('customer_id', 0);
          $count = $this->request->getParam('count', '');
          $discussion_images = $this->request->getParam('discussion_images', array());
          $image_id = $this->request->getParam('image_id', 0);
          $notify_customer = $this->request->getParam('notify_customer', 0); */

        $accessToken = isset($_REQUEST['access_token']) ? $_REQUEST['access_token'] : 0;
        ;
        $itemId = isset($_REQUEST['id']) ? $_REQUEST['id'] : 0;
        $type = isset($_REQUEST['type']) ? $_REQUEST['type'] : '';
        $discussion = isset($_REQUEST['discussion']) ? $_REQUEST['discussion'] : 0;
        $user_role = isset($_REQUEST['user_role']) ? $_REQUEST['user_role'] : 'contractor';
        $customer_id = isset($_REQUEST['customer_id']) ? $_REQUEST['customer_id'] : 0;
        $count = isset($_REQUEST['count']) ? $_REQUEST['count'] : '';
        $discussion_images = isset($_REQUEST['discussion_images']) ? $_REQUEST['discussion_images'] : array();
        $image_id = isset($_REQUEST['image_id']) ? $_REQUEST['image_id'] : 0;
        $notify_customer = isset($_REQUEST['notify_customer']) ? $_REQUEST['notify_customer'] : 0;


        //echo "notify customer: " . $notify_customer;
        $visibility = ($notify_customer && $user_role == 'contractor') ? 3 : ($user_role == 'customer') ? 3 : 2;

        $this->checkAccessToken($accessToken, $user_role, $customer_id);



        /* $loggedUser = CheckAuth::getLoggedUser();
          if ($user_role == 'contractor') {
          $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];
          }
          $data = array('authrezed' => 0);
          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken, $user_role))) {
          $data['msg'] = 'wrong access token';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {
          //open new session
          $authrezed = $this->openNewSession($accessToken, $user_role, $customer_id);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */


        $modelBookingDiscussion = new Model_BookingDiscussion();
        $modelComplaintDiscussion = new Model_ComplaintDiscussion();
        $modelImage = new Model_Image();
        $loggedUser = CheckAuth::getLoggedUser();




        $logged_user_id = ($user_role == 'customer') ? $loggedUser['customer_id'] : $loggedUser['user_id'];

        //var_dump($logged_user_id); 

        $dir = get_config('image_attachment') . '/';
        $subdir = date('Y/m/d/');
        $fullDir = $dir . $subdir;
        if (!is_dir($fullDir)) {
            mkdir($fullDir, 0777, true);
        }

        $success = 0;
        if ($discussion) {

            if ((isset($count) && $count != '') || (isset($discussion_images) && !empty($discussion_images))) {

                if ((isset($discussion_images) && !empty($discussion_images))) {
                    $image_group = 0;
                    $ImageCounts = count($discussion_images);

                    if ($ImageCounts > 1) {
                        $maxGroup = max($modelImage->getMaxGroup());
                        $image_group = $maxGroup['group_id'] + 1;
                        if ($type == 'booking') {
                            $params = array(
                                'booking_id' => $itemId,
                                'user_id' => $logged_user_id,
                                'user_message' => $discussion,
                                'created' => time(),
                                'user_role' => $loggedUser['role_id'],
                                'visibility' => (int) $visibility
                            );


                            $success = $modelBookingDiscussion->insert($params);
                        } else if ($type == 'complaint') {
                            $params = array(
                                'complaint_id' => $itemId,
                                'user_id' => $logged_user_id,
                                'user_message' => $discussion,
                                'created' => time(),
                                'user_role' => $loggedUser['role_id'],
                                'visibility' => (int) $visibility
                            );
                            $success = $modelComplaintDiscussion->insert($params);
                        }

                        $modelItemImageDiscussion = new Model_ItemImageDiscussion();
                        $dataDiscssion = array(
                            'image_id' => 0,
                            'group_id' => $image_group,
                            'item_id' => $success,
                            'type' => $type
                        );

                        $modelItemImageDiscussion->insert($dataDiscssion);
                    }

                    foreach ($discussion_images as $key => $imageData) {
                        if (isset($imageData) && !empty($imageData)) {
                            $image = str_replace('data:image/png;base64,', '', $imageData);
                            $image = str_replace(' ', '+', $image);
                            $image = base64_decode($image);
                            $fileName = "image_{$key}.png";
                            $ext = 'png';
                            $file = $fullDir . $fileName;
                            $success_upload_photo = file_put_contents($file, $image);
                            if ($success_upload_photo) {
                                $successUpload = 1;
                                $data = array(
                                    'user_ip' => ''
                                    , 'created_by' => $logged_user_id
                                    , 'image_types_id' => 0
                                    , 'group_id' => $image_group
                                    , 'user_role' => $loggedUser['role_id']
                                );

                                $id = $modelImage->insert($data);
                                $image_id = $id;
                                if ($ImageCounts > 1) {
                                    $image_id = 0;
                                }
                                if ($ImageCounts == 1) {
                                    if ($type == 'booking') {
                                        $params = array(
                                            'booking_id' => $itemId,
                                            'user_id' => $logged_user_id,
                                            'user_message' => $discussion,
                                            'created' => time(),
                                            'user_role' => $loggedUser['role_id'],
                                            'visibility' => (int) $visibility
                                        );

                                        $success = $modelBookingDiscussion->insert($params);
                                    } else if ($type == 'complaint') {
                                        $params = array(
                                            'complaint_id' => $itemId,
                                            'user_id' => $logged_user_id,
                                            'user_message' => $discussion,
                                            'created' => time(),
                                            'user_role' => $loggedUser['role_id'],
                                            'visibility' => (int) $visibility
                                        );
                                        $success = $modelComplaintDiscussion->insert($params);
                                    }

                                    $modelItemImageDiscussion = new Model_ItemImageDiscussion();
                                    $dataDiscssion = array(
                                        'image_id' => $image_id,
                                        'group_id' => $image_group,
                                        'item_id' => $success,
                                        'type' => $type
                                    );

                                    $modelItemImageDiscussion->insert($dataDiscssion);
                                }
                                $Itemdata = array(
                                    'item_id' => $itemId,
                                    'service_id' => 0,
                                    'discussion_id' => $success,
                                    'image_id' => $id,
                                    'type' => $type . '_discussion',
                                    'floor_id' => 0
                                );


                                $modelItemImage = new Model_ItemImage();
                                $Item_image_id = $modelItemImage->insert($Itemdata);


                                $original_path = "original_{$id}.{$ext}";
                                $large_path = "large_{$id}.{$ext}";
                                $small_path = "small_{$id}.{$ext}";
                                $thumbnail_path = "thumbnail_{$id}.{$ext}";
                                $compressed_path = "compressed_{$id}.{$ext}";

                                $data = array(
                                    'original_path' => $subdir . $original_path,
                                    'large_path' => $subdir . $large_path,
                                    'small_path' => $subdir . $small_path,
                                    'thumbnail_path' => $subdir . $thumbnail_path,
                                    'compressed_path' => $subdir . $compressed_path
                                );


                                $modelImage->updateById($id, $data);
                                $compressed_saved = copy($file, $fullDir . $compressed_path);
                                $image_saved = copy($file, $fullDir . $original_path);
                                ImageMagick::scale_image($file, $fullDir . $large_path, 510);
                                ImageMagick::scale_image($file, $fullDir . $small_path, 250);
                                ImageMagick::create_thumbnail($file, $fullDir . $thumbnail_path, 78, 64);
                                ImageMagick::convert($file, $fullDir . $compressed_path);
                                ImageMagick::compress_image($file, $fullDir . $compressed_path);


                                if (file_exists($file)) {
                                    unlink($file);
                                }
                            }
                        }
                    }
                }
                if (isset($count) && $count != '') {
                    $image_group = 0;
                    if ($count > 1) {
                        $maxGroup = max($modelImage->getMaxGroup());
                        $image_group = $maxGroup['group_id'] + 1;
                        if ($type == 'booking') {
                            $params = array(
                                'booking_id' => $itemId,
                                'user_id' => $logged_user_id,
                                'user_message' => $discussion,
                                'created' => time(),
                                'user_role' => $loggedUser['role_id'],
                                'visibility' => (int) $visibility
                            );

                            $success = $modelBookingDiscussion->insert($params);
                        } else if ($type == 'complaint') {
                            $params = array(
                                'complaint_id' => $itemId,
                                'user_id' => $logged_user_id,
                                'user_message' => $discussion,
                                'created' => time(),
                                'user_role' => $loggedUser['role_id'],
                                'visibility' => (int) $visibility
                            );
                            $success = $modelComplaintDiscussion->insert($params);
                        }

                        $modelItemImageDiscussion = new Model_ItemImageDiscussion();
                        $dataDiscssion = array(
                            'image_id' => 0,
                            'group_id' => $image_group,
                            'item_id' => $success,
                            'type' => $type
                        );

                        $modelItemImageDiscussion->insert($dataDiscssion);
                    }

                    for ($i = 0; $i < $count; $i++) {

                        $source = $_FILES['imageDiscussion' . $i]['tmp_name'];
                        $extension = $this->request->getParam('ext' . $i, 'png');
                        $imageInfo = pathinfo($source);
                        $ext = $extension;
                        $successUpload = 0;
                        $fileName = "image_{$i}.{$ext}";
                        $file = $fullDir . $fileName;
                        $image_saved = copy($source, $fullDir . $fileName);
                        if ($image_saved) {
                            $successUpload = 1;
                            $data = array(
                                'user_ip' => ''
                                , 'created_by' => $logged_user_id
                                , 'image_types_id' => 0
                                , 'group_id' => $image_group
                                , 'user_role' => $loggedUser['role_id']
                            );

                            $id = $modelImage->insert($data);
                            $image_id = $id;
                            if ($count > 1) {
                                $image_id = 0;
                            }
                            if ($count == 1) {
                                if ($type == 'booking') {
                                    $params = array(
                                        'booking_id' => $itemId,
                                        'user_id' => $logged_user_id,
                                        'user_message' => $discussion,
                                        'created' => time(),
                                        'user_role' => $loggedUser['role_id'],
                                        'visibility' => (int) $visibility
                                    );

                                    $success = $modelBookingDiscussion->insert($params);
                                } else if ($type == 'complaint') {
                                    $params = array(
                                        'complaint_id' => $itemId,
                                        'user_id' => $logged_user_id,
                                        'user_message' => $discussion,
                                        'created' => time(),
                                        'user_role' => $loggedUser['role_id'],
                                        'visibility' => (int) $visibility
                                    );
                                    $success = $modelComplaintDiscussion->insert($params);
                                }

                                $modelItemImageDiscussion = new Model_ItemImageDiscussion();
                                $dataDiscssion = array(
                                    'image_id' => $image_id,
                                    'group_id' => $image_group,
                                    'item_id' => $success,
                                    'type' => $type
                                );

                                $modelItemImageDiscussion->insert($dataDiscssion);
                            }
                            $Itemdata = array(
                                'item_id' => $itemId,
                                'service_id' => 0,
                                'discussion_id' => $success,
                                'image_id' => $id,
                                'type' => $type . '_discussion',
                                'floor_id' => 0
                            );


                            $modelItemImage = new Model_ItemImage();
                            $Item_image_id = $modelItemImage->insert($Itemdata);


                            $original_path = "original_{$id}.{$ext}";
                            $large_path = "large_{$id}.{$ext}";
                            $small_path = "small_{$id}.{$ext}";
                            $thumbnail_path = "thumbnail_{$id}.{$ext}";
                            $compressed_path = "compressed_{$id}.{$ext}";

                            $data = array(
                                'original_path' => $subdir . $original_path,
                                'large_path' => $subdir . $large_path,
                                'small_path' => $subdir . $small_path,
                                'thumbnail_path' => $subdir . $thumbnail_path,
                                'compressed_path' => $subdir . $compressed_path
                            );

                            $modelImage->updateById($id, $data);
                            $compressed_saved = copy($source, $fullDir . $compressed_path);
                            $image_saved = copy($source, $fullDir . $original_path);
                            ImageMagick::scale_image($source, $fullDir . $large_path, 510);
                            ImageMagick::scale_image($source, $fullDir . $small_path, 250);
                            ImageMagick::create_thumbnail($source, $fullDir . $thumbnail_path, 78, 64);
                            ImageMagick::convert($source, $fullDir . $compressed_path);
                            ImageMagick::compress_image($source, $fullDir . $compressed_path);
                        }


                        if ($image_saved) {
                            if (file_exists($source)) {
                                unlink($source);
                            }
                            if (file_exists($file)) {
                                unlink($file);
                            }
                        }
                    }
                }
            } else {

                if ($type == 'booking') {
                    $params = array(
                        'booking_id' => $itemId,
                        'user_id' => $logged_user_id,
                        'user_message' => $discussion,
                        'created' => time(),
                        'user_role' => $loggedUser['role_id'],
                        'visibility' => (int) $visibility
                    );

                    $success = $modelBookingDiscussion->insert($params);
                } else if ($type == 'complaint') {
                    $params = array(
                        'complaint_id' => $itemId,
                        'user_id' => $logged_user_id,
                        'user_message' => $discussion,
                        'created' => time(),
                        'user_role' => $loggedUser['role_id'],
                        'visibility' => (int) $visibility
                    );
                    $success = $modelComplaintDiscussion->insert($params);
                }

                $modelItemImageDiscussion = new Model_ItemImageDiscussion();
                $dataDiscssion = array(
                    'image_id' => $image_id,
                    'group_id' => 0,
                    'item_id' => $success,
                    'type' => $type
                );

                $modelItemImageDiscussion->insert($dataDiscssion);
            }

            if ($type == 'booking') {
                MobileNotification::notify($itemId, 'new discussion', array('discussion_id' => $success));
            } else if ($type == 'complaint') {
                MobileNotification::notify(0, 'complaint discussion', array('complaint_id' => $itemId, 'discussion_id' => $success));
            }
        }



        if ($success) {
            $data['type'] = 'success';
            $data['msg'] = 'Discussion Added Successfully';
        } else {
            $data['type'] = 'error';
            $data['msg'] = 'Invalid Discussion';
        }

        $data['authrezed'] = 1;

        echo json_encode($data);
        exit;
    }

    public function getAllImageDiscussionAction() {



        //get Params
        //
		$accessToken = $this->request->getParam('access_token', 0);
        $image_id = $this->request->getParam('id', 0);
        $type = $this->request->getParam('type', 'booking');
        $user_role = $this->request->getParam('user_role', 'contractor');
        $customer_id = $this->request->getParam('customer_id', 0);

        $this->checkAccessToken($accessToken, $user_role, $customer_id);

        $modelBookingDiscussion = new Model_BookingDiscussion();
        $modelContractorInfo = new Model_ContractorInfo();
        $modelUser = new Model_User();
        $modelCustomerCommercialInfo = new Model_CustomerCommercialInfo();
        $modelCustomer = new Model_Customer();


        $logged_user = CheckAuth::getLoggedUser();

        $logged_user_id = ($user_role == 'customer') ? $logged_user['customer_id'] : $logged_user['user_id'];

        $filters = array();
        if ($user_role == 'customer') {
            $filters = array('user_id' => $logged_user_id, 'user_role' => 'customer');
        }

        $group_id = 0;
        if ($type == 'booking') {
            $imageDiscussions = $modelBookingDiscussion->getByImageId($image_id, 'desc', $group_id, $filters);
        }
        $results = array();
        if ($imageDiscussions) {



            foreach ($imageDiscussions as $key => $discussion) {



                if ($discussion['user_role'] == 9) {
                    $user = $modelCustomer->getById($discussion['user_id']);
                    $username = $user['first_name'] . ' ' . $user['last_name'];
                    $avatar = '';

                    $DiscussionUserInfo = $modelCustomerCommercialInfo->getByCustomerId($discussion['user_id']);
                } else {
                    $user = $modelUser->getById($discussion['user_id']);
                    $username = $user['username'];
                    $avatar = $_SERVER['HTTP_HOST'] . '/uploads/user_pic/thumb_medium/' . $user['avatar'];
                    $DiscussionUserInfo = $modelContractorInfo->getByContractorId($discussion['user_id']);
                }

                //$imageDiscussion['user'] = $modelUser->getById($imageDiscussion['user_id']);
                $results[$key]['created'] = $discussion['created'];
                $results[$key]['user_message'] = nl2br(htmlentities($discussion['user_message'], ENT_NOQUOTES, 'UTF-8'));
                $results[$key]['username'] = $username;
                $results[$key]['avatar'] = $avatar;
                $results[$key]['discussion_id'] = $discussion['discussion_id'];
                $results[$key]['user_id'] = $discussion['user_id'];

                if ($DiscussionUserInfo['business_name']) {
                    $results[$key]['business_name'] = $DiscussionUserInfo['business_name'];
                } else {
                    $results[$key]['business_name'] = '';
                }
            }


            if ($user_role == 'customer') {
                $customerInfo = $modelCustomerCommercialInfo->getByCustomerId($logged_user_id);
                $data['business_name'] = isset($customerInfo['business_name']) ? $customerInfo['business_name'] : '';
            } else {
                $contractorInfo = $modelContractorInfo->getByContractorId($logged_user_id);
                $data['business_name'] = isset($contractorInfo['business_name']) ? $contractorInfo['business_name'] : '';
            }
        }



        $data['result'] = $results;
        $data['authrezed'] = 1;
        $data['username'] = $logged_user['username'];
        echo json_encode($data);

        exit;
    }

    public function profileAction() {
        //
        //check login
        //
		header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $itemId = $this->request->getParam('id', 0);
        $type = $this->request->getParam('type', '');
        $discussion = $this->request->getParam('discussion', 0);

        $this->checkAccessToken($accessToken);
        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];



          $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {
          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */




        $loggedUser = CheckAuth::getLoggedUser();

        $modelUser = new Model_User();
        $user = $modelUser->getById($loggedUser['user_id']);

        $line_address = get_line_address($user);
        $MAP_OBJECT_Booking = new GoogleMapAPI();
        $address = $MAP_OBJECT_Booking->getGeocode($line_address);

        $user['avatar'] = $_SERVER['HTTP_HOST'] . '/uploads/user_pic/thumb_medium/' . $user['avatar'];
        $data['user'] = $user;

        $data['user']['lat'] = $address['lat'];
        $data['user']['lon'] = $address['lon'];

        $modelAuthRole = new Model_AuthRole();
        $role = $modelAuthRole->getById($user['role_id']);
        $data['role'] = $role;


        if ('contractor' == $role['role_name']) {

            // booking 
            $modelBookingStatus = new Model_BookingStatus();
            $modelBooking = new Model_Booking();
            /* $BookingStatus = $modelBookingStatus->getAllWithoutPermission();
              $counts = array();
              foreach ($BookingStatus as $status) {
              $results = array();
              $total = $modelBooking->getcountBookingsByStatus($status['name'], $loggedUser['user_id']);
              $results['name'] = $status['name'];
              $results['booking_status_id'] = $status['booking_status_id'];
              $results['color'] = $status['color'];
              $results['total'] = $total;
              array_push($counts, $results);
              }
              $contractorInfoObj = new Model_ContractorInfo();
              $contractorInfo = $contractorInfoObj->getByContractorId($loggedUser['user_id']);
              $data['contractorInfo'] = $contractorInfo;
              $data['bookingStatusCounts'] = $counts; */

            $contractorInfoObj = new Model_ContractorInfo();
            $contractorInfo = $contractorInfoObj->getByContractorId($loggedUser['user_id']);
            if ($contractorInfo) {
                $data['contractorInfo'] = $contractorInfo;
            } else {
                $data['contractorInfo'] = array();
            }


            $modelContractorRate = new Model_ContractorRate();
            $contractorRate = $modelContractorRate->getRateByContractor($contractorInfo['contractor_id']);
            $data['user']['contractorRate'] = $contractorRate;
            if ($contractorInfo) {

                $modelAttachment = new Model_Attachment();
                $filter = array('type' => 'insurance', 'itemid' => $contractorInfo['contractor_info_id'], 'item_type' => 'image');
                $pager = null;
                //$insuranceAttachments =  $modelAttachment->getAll('a.created desc',$pager,$filter);
                //$data['contractorInfo']['insurance_attachment'] = $insuranceAttachments;
                $filter = array('type' => 'licence', 'itemid' => $contractorInfo['contractor_info_id'], 'item_type' => 'image');
                $licenceAttachments = $modelAttachment->getAll('a.created desc', $pager, $filter);
                $data['contractorInfo']['drivers_licence_attachment'] = $licenceAttachments;
            }




            if ($contractorInfo) {
                //get contractorOwner
                $contractorOwnerObj = new Model_ContractorOwner();
                $contractorOwner = $contractorOwnerObj->getAll(array('contractor_info_id' => $contractorInfo['contractor_info_id']));

                $data['contractorOwner'] = $contractorOwner;

                //get contractorEmployee
                $contractorEmployeeObj = new Model_ContractorEmployee();
                $contractorEmployee = $contractorEmployeeObj->getAll(array('contractor_info_id' => $contractorInfo['contractor_info_id']));

                $data['contractorEmployee'] = $contractorEmployee;

                //get DeclarationOfChemicals
                $declarationOfChemicalsObj = new Model_DeclarationOfChemicals();
                $declarationOfChemicals = $declarationOfChemicalsObj->getByContractorInfoId($contractorInfo['contractor_info_id']);

                $data['declarationOfChemicals'] = $declarationOfChemicals;

                //get DeclarationOfEquipment
                $declarationOfEquipmentObj = new Model_DeclarationOfEquipment();
                $declarationOfEquipments = $declarationOfEquipmentObj->getByContractorInfoId($contractorInfo['contractor_info_id']);
                $allDeclarationOfEquipments = array();
                $modelAttachment = new Model_Attachment();
                $pager = null;
                foreach ($declarationOfEquipments as $key => $declarationOfEquipment) {
                    $filter = array('type' => 'equipment', 'itemid' => $declarationOfEquipment['id'], 'item_type' => 'image');
                    $attachments = $modelAttachment->getAll('a.created desc', $pager, $filter);
                    $allDeclarationOfEquipments[$key]['id'] = $declarationOfEquipment['id'];
                    $allDeclarationOfEquipments[$key]['equipment'] = $declarationOfEquipment['equipment'];
                    $allDeclarationOfEquipments[$key]['contractor_info_id'] = $declarationOfEquipment['contractor_info_id'];
                    $allDeclarationOfEquipments[$key]['attachments'] = $attachments;
                }


                $data['declarationOfEquipment'] = $allDeclarationOfEquipments;

                //get DeclarationOfOtherApparatus
                $declarationOfOtherApparatusObj = new Model_DeclarationOfOtherApparatus();
                $declarationOfOtherApparatus = $declarationOfOtherApparatusObj->getByContractorInfoId($contractorInfo['contractor_info_id']);

                $data['declarationOfOtherApparatus'] = $declarationOfOtherApparatus;

                //get Vehicle
                $contractorVehicleObj = new Model_ContractorVehicle();
                $contractorVehicles = $contractorVehicleObj->getByContractorInfoId($contractorInfo['contractor_info_id']);
                $allcontractorVehicle = array();
                $modelAttachment = new Model_Attachment();
                $pager = null;
                foreach ($contractorVehicles as $key => $contractorVehicle) {
                    $filter = array('type' => 'vehicle', 'itemid' => $contractorVehicle['vehicle_id'], 'item_type' => 'image');
                    $attachments = $modelAttachment->getAll('a.created desc', $pager, $filter);
                    $allcontractorVehicle[$key]['vehicle_id'] = $contractorVehicle['vehicle_id'];
                    $allcontractorVehicle[$key]['registration_number'] = $contractorVehicle['registration_number'];
                    $allcontractorVehicle[$key]['make'] = $contractorVehicle['make'];
                    $allcontractorVehicle[$key]['model'] = $contractorVehicle['model'];
                    $allcontractorVehicle[$key]['colour'] = $contractorVehicle['colour'];
                    $allcontractorVehicle[$key]['contractor_info_id'] = $contractorVehicle['contractor_info_id'];
                    $allcontractorVehicle[$key]['attachments'] = $attachments;
                }

                $data['contractorVehicle'] = $allcontractorVehicle;
                //get Contractor Insurance
                $contractorInsuranceObj = new Model_ContractorInsurance();
                $contractorInsurances = $contractorInsuranceObj->getAll(array('contractor_info_id' => $contractorInfo['contractor_info_id']));
                $allcontractorInsurance = array();
                $modelAttachment = new Model_Attachment();
                $pager = null;
                foreach ($contractorInsurances as $key => $contractorInsurance) {
                    $filter = array('type' => 'insurance', 'itemid' => $contractorInsurance['contractor_insurance_id'], 'item_type' => 'image');
                    $attachments = $modelAttachment->getAll('a.created desc', $pager, $filter);
                    $allcontractorInsurance[$key]['contractor_insurance_id'] = $contractorInsurance['contractor_insurance_id'];
                    $allcontractorInsurance[$key]['insurance_policy_number'] = $contractorInsurance['insurance_policy_number'];
                    $allcontractorInsurance[$key]['insurance_policy_expiry'] = $contractorInsurance['insurance_policy_expiry'];
                    $allcontractorInsurance[$key]['insurance_policy_start'] = $contractorInsurance['insurance_policy_start'];
                    $allcontractorInsurance[$key]['insurance_listed_services_covered'] = $contractorInsurance['insurance_listed_services_covered'];
                    $allcontractorInsurance[$key]['insurance_type'] = $contractorInsurance['insurance_type'];
                    $allcontractorInsurance[$key]['contractor_info_id'] = $contractorInsurance['contractor_info_id'];
                    $allcontractorInsurance[$key]['attachments'] = $attachments;
                }

                $data['contractorInsurance'] = $allcontractorInsurance;
            }
        } else {
            $userInfoObj = new Model_UserInfo();
            $userInfo = $userInfoObj->getByUserId($loggedUser['user_id']);

            $data['userInfo'] = $userInfo;
        }
        $data['authrezed'] = 1;

        echo json_encode($data);
        exit;
    }

    // Contractor Vehicles 
    public function addContractorVehicleAction() {


        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $registrationNumber = $this->request->getParam('registration_number');
        $make = $this->request->getParam('make');
        $model = $this->request->getParam('model');
        $colour = $this->request->getParam('colour');
        $attachCount = $this->request->getParam('attachCount', 0);
        $vehicleFiles = $this->request->getParam('vehicle_files', 0);

        $this->checkAccessToken($accessToken);
        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];



          $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {
          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */


        $loggedUser = CheckAuth::getLoggedUser();
        $contractorInfoObj = new Model_ContractorInfo();
        $contractorInfo = $contractorInfoObj->getByContractorId($loggedUser['user_id']);
        $contractorVehicleObj = new Model_ContractorVehicle();
        $attachmentObj = new Model_Attachment();
        $vehicleAttachmentObj = new Model_VehicleAttachment();
        $params = array(
            'registration_number' => $registrationNumber,
            'make' => $make,
            'model' => $model,
            'colour' => $colour,
            'contractor_info_id' => $contractorInfo['contractor_info_id']
        );

        $success = $contractorVehicleObj->insert($params);
        $id = 0;
        if ($attachCount) {
            for ($i = 0; $i < $attachCount; $i++) {
                $source = $_FILES['vehicleFile' . $i]['tmp_name'];
                $extension = $this->request->getParam('ext' . $i);
                $fileInfo = pathinfo($source);
                $ext = $extension;

                $dir = get_config('attachment') . '/';
                $subdir = date('Y/m/d/');
                $fullDir = $dir . $subdir;
                if (!is_dir($fullDir)) {
                    mkdir($fullDir, 0777, true);
                }

                $fileName = $success . '_' . $i . '.' . $ext;
                $image_saved = copy($source, $fullDir . $fileName);
                $size = filesize($fullDir . $fileName);
                $type = mime_content_type($fullDir . $fileName);
                $typeParts = explode("/", $type);
                if ($typeParts[0] == 'image') {
                    $thumbName = $success . '_thumbnail_' . $i . '.' . $ext;
                    ImageMagick::create_thumbnail($source, $fullDir . $thumbName, 78, 64);
                } else {
                    $thumbName = $success . '_' . $i . '.jpg';
                    ImageMagick::convertFile($source . '[0]', $fullDir . $thumbName);
                }
                $attach = array(
                    'created_by' => $loggedUser['user_id']
                    , 'created' => time()
                    , 'size' => $size
                    , 'type' => $type
                    , 'path' => $fullDir . $fileName,
                    'file_name' => $fileName,
                    'thumbnail_file' => $fullDir . $thumbName
                );

                $id = $attachmentObj->insert($attach);
                $vehicleAttachmentObj->insert(array('vehicle_id' => $success, 'attachment_id' => $id));
                if ($image_saved) {
                    if (file_exists($source)) {
                        unlink($source);
                    }
                }
            }
        }

        $id = 0;
        if ((isset($vehicleFiles) && !empty($vehicleFiles))) {
            $counter = 0;
            $dir = get_config('attachment') . '/';
            $subdir = date('Y/m/d/');
            $fullDir = $dir . $subdir;
            if (!is_dir($fullDir)) {
                mkdir($fullDir, 0777, true);
            }

            foreach ($vehicleFiles as $key => $imageData) {
                if (isset($imageData) && !empty($imageData)) {
                    $ext = 'png';
                    $image = str_replace('data:image/png;base64,', '', $imageData);
                    $image = str_replace(' ', '+', $image);
                    $image = base64_decode($image);
                    $tempFile = "image_{$key}." . $ext;
                    $file = $fullDir . $tempFile;
                    $successUpload = file_put_contents($file, $image);
                    $counter = $counter + 1;
                    if ($successUpload) {
                        $fileName = $success . '_' . $counter . '_' . '.' . $ext;
                        $image_saved = copy($file, $fullDir . $fileName);
                        $size = filesize($fullDir . $fileName);
                        $type = mime_content_type($fullDir . $fileName);
                        $typeParts = explode("/", $type);
                        if ($typeParts[0] == 'image') {
                            $thumbName = $success . '_thumbnail_' . $counter . '.' . $ext;
                            ImageMagick::create_thumbnail($file, $fullDir . $thumbName, 78, 64);
                        } else {
                            $thumbName = $success . '_' . $counter . '.jpg';
                            ImageMagick::convertFile($file . '[0]', $fullDir . $thumbName);
                        }

                        $attach = array(
                            'created_by' => $loggedUser['user_id']
                            , 'created' => time()
                            , 'size' => $size
                            , 'type' => $type
                            , 'path' => $fullDir . $fileName,
                            'file_name' => $fileName,
                            'thumbnail_file' => $fullDir . $thumbName
                        );

                        $id = $attachmentObj->insert($attach);
                        $vehicleAttachmentObj->insert(array('vehicle_id' => $success, 'attachment_id' => $id));
                        if ($image_saved) {
                            if (file_exists($file)) {
                                unlink($file);
                            }
                        }
                    }
                }
            }
        }


        $vehicle = $contractorVehicleObj->getById($success);
        $modelAttachment = new Model_Attachment();
        $filter = array('type' => 'vehicle', 'itemid' => $success, 'item_type' => 'image');
        $attachments = $modelAttachment->getAll('a.created desc', $pager, $filter);
        $vehicle['attachments'] = $attachments;

        if ($success || $id) {
            $data['msg'] = 'Saved successfully';
            $data['type'] = 'success';
            $data['result'] = $vehicle;
        } else {
            $data['msg'] = 'No Changes in Contractor Vehicle';
            $data['type'] = 'error';
        }

        $data['authrezed'] = 1;
        echo json_encode($data);
        exit;
    }

    public function deleteContractorVehicleAction() {


        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $id = $this->request->getParam('id', 0);


        $this->checkAccessToken($accessToken);
        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];



          $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          $data['type'] = 'error';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {
          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */


        $loggedUser = CheckAuth::getLoggedUser();
        $contractorVehicleObj = new Model_ContractorVehicle();
        $modelAttachment = new Model_Attachment();
        $contractorVehicle = $contractorVehicleObj->getById($id);
        if ($contractorVehicle) {
            $filter = array('type' => 'vehicle', 'itemid' => $id);
            $pager = null;
            $attachments = $modelAttachment->getAll('a.created desc', $pager, $filter);
            if ($attachments) {
                foreach ($attachments as $attachment) {
                    $modelAttachment->updateById($attachment['attachment_id'], array('is_deleted' => '1'));
                }
            }

            $contractorVehicleObj->deleteById($id);
            $data['msg'] = 'Vehicle deleted successfully';
            $data['type'] = 'success';
        } else {
            $data['msg'] = 'Vehicle not found';
            $data['type'] = 'error';
        }

        $data['authrezed'] = 1;
        echo json_encode($data);
        exit;
    }

    public function addEditContractorOwnerEmployeeAction() {

        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $id = $this->request->getParam('id');
        $type = $this->request->getParam('type');
        $name = $this->request->getParam('name');
        $email1 = $this->request->getParam('email1');
        $email2 = $this->request->getParam('email2', '');
        $email3 = $this->request->getParam('email3', '');
        $mobile1 = $this->request->getParam('mobile1');
        $mobile2 = $this->request->getParam('mobile2', '');
        $mobile3 = $this->request->getParam('mobile3', '');
        $phone1 = $this->request->getParam('phone1');
        $phone2 = $this->request->getParam('phone2', '');
        $phone3 = $this->request->getParam('phone3', '');
        $fax = $this->request->getParam('fax', '');
        $emergencyPhone = $this->request->getParam('emergency_phone', '');
        $unitLotNumber = $this->request->getParam('unit_lot_number', '');
        $streetNumber = $this->request->getParam('street_number');
        $streetAddress = $this->request->getParam('street_address');
        $suburb = $this->request->getParam('suburb', '');
        $state = $this->request->getParam('state');
        $postcode = $this->request->getParam('postcode');
        $po_box = $this->request->getParam('po_box', '');
        $cityId = $this->request->getParam('city_id');
        $countryId = $this->request->getParam('country_id');

        if (get_config('remove_white_spacing')) {
            $mobile1 = preparer_number($mobile1);
            $mobile2 = preparer_number($mobile2);
            $mobile3 = preparer_number($mobile3);
            $phone1 = preparer_number($phone1);
            $phone2 = preparer_number($phone2);
            $phone3 = preparer_number($phone3);
        }




        $this->checkAccessToken($accessToken);
        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];



          $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          $data['type'] = 'error';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {
          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */


        $loggedUser = CheckAuth::getLoggedUser();
        $contractorInfoObj = new Model_ContractorInfo();
        $contractorEmployeeObj = new Model_ContractorEmployee();
        $modelContractorOwner = new Model_ContractorOwner();

        $contractorInfo = $contractorInfoObj->getByContractorId($loggedUser['user_id']);


        if (!empty($mobile1) && !empty($email1) && !empty($name) && !empty($streetNumber) && !empty($streetAddress) && !empty($state) && !empty($cityId) && !empty($countryId) && !empty($postcode)) {
            $params = array(
                'name' => $name,
                'contractor_info_id' => $contractorInfo['contractor_info_id'],
                'created' => time(),
                'city_id' => $cityId,
                'email1' => $email1,
                'email2' => $email2,
                'email3' => $email3,
                'mobile1' => $mobile1,
                'mobile2' => $mobile2,
                'mobile3' => $mobile3,
                'phone1' => $phone1,
                'phone2' => $phone2,
                'phone3' => $phone3,
                'fax' => $fax,
                'emergency_phone' => $emergencyPhone,
                'unit_lot_number' => $unitLotNumber,
                'street_number' => $streetNumber,
                'street_address' => $streetAddress,
                'suburb' => $suburb,
                'state' => $state,
                'postcode' => $postcode,
                'po_box' => $po_box
            );

            if ($id && $type == 'owner') {
                $success = $modelContractorOwner->updateById($id, $params);
                $data['result'] = $modelContractorOwner->getById($id);
            } else if ($id && $type == 'employee') {
                $success = $contractorEmployeeObj->updateById($id, $params);
                $data['result'] = $contractorEmployeeObj->getById($id);
            } else if ($type == 'owner' && !($id)) {
                $success = $modelContractorOwner->insert($params);


                $data['result'] = $modelContractorOwner->getById($success);
            } else if ($type == 'employee' && !($id)) {

                $success = $contractorEmployeeObj->insert($params);
                $data['result'] = $contractorEmployeeObj->getById($success);
            }
        }


        if ($success) {
            $data['msg'] = 'Saved successfully';
            $data['type'] = 'success';
        } else {
            $data['msg'] = 'No Changes in Contractor Owner';
            $data['type'] = 'error';
        }


        $data['authrezed'] = 1;
        echo json_encode($data);
        exit;
    }

    public function deleteContractorOwnerAction() {

        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $id = $this->request->getParam('id', 0);
        $this->checkAccessToken($accessToken);

        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];



          $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          $data['type'] = 'error';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {
          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */


        $loggedUser = CheckAuth::getLoggedUser();
        $contractorOwnerObj = new Model_ContractorOwner();
        $contractorOwner = $contractorOwnerObj->getById($id);

        if ($contractorOwner) {
            $contractorOwnerObj->updateById($id, array('is_deleted' => 1));
            $data['msg'] = 'Owner deleted successfully';
            $data['type'] = 'success';
        } else {
            $data['msg'] = 'Owner not found';
            $data['type'] = 'error';
        }




        $data['authrezed'] = 1;
        echo json_encode($data);
        exit;
    }

    public function contractorOwnerEmployeePhotoUploadAction() {

        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $id = $this->request->getParam('id');
        $type = $this->request->getParam('type');
        $owner_employee_avatar = $this->request->getParam('owner_employee_avatar');

        $this->checkAccessToken($accessToken);
        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];



          $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          $data['type'] = 'error';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {
          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */


        $loggedUser = CheckAuth::getLoggedUser();

        $dir = get_config('avatar') . '/';
        $subdir = date('Y/m/d/');
        $fullDir = $dir . $subdir;

        if (!is_dir($fullDir)) {
            mkdir($fullDir, 0777, true);
        }



        if (isset($owner_employee_avatar) && !empty($owner_employee_avatar)) {
            $image = str_replace('data:image/png;base64,', '', $owner_employee_avatar);
            $extension = $this->request->getParam('extension', 'png');
            $image = str_replace(' ', '+', $image);
            $image = base64_decode($image);
            $fileName = "image." . $extension;
            $file = $fullDir . $fileName;
            $orginal_avatar_owner = "orginal_avatar_owner_{$id}.{$ext}";
            $small_avatar_owner = "small_avatar_owner_{$id}.{$ext}";
            $large_avatar_owner = "large_avatar_owner_{$id}.{$ext}";
            $success = file_put_contents($file, $image);
            if ($success) {
                $file_saved = copy($file, $fullDir . $orginal_avatar_owner);
                ImageMagick::create_thumbnail($file, $fullDir . $small_avatar_owner, 73, 90);
                ImageMagick::create_thumbnail($file, $fullDir . $large_avatar_owner, 150, 185);
                if ($file_saved) {
                    if (file_exists($file)) {
                        unlink($file);
                    }
                }
            }
        }

        if (isset($_FILES['avatar']) && !empty($_FILES['avatar'])) {

            $source = $_FILES['avatar']['tmp_name'];
            $extension = $this->request->getParam('extension', 'png');
            $imageInfo = pathinfo($source);
            $ext = $extension;

            $orginal_avatar_owner = "orginal_avatar_owner_{$id}.{$ext}";
            $small_avatar_owner = "small_avatar_owner_{$id}.{$ext}";
            $large_avatar_owner = "large_avatar_owner_{$id}.{$ext}";
            //save image to database and filesystem here
            $file_saved = copy($source, $fullDir . $orginal_avatar_owner);
            ImageMagick::create_thumbnail($source, $fullDir . $small_avatar_owner, 73, 90);
            ImageMagick::create_thumbnail($source, $fullDir . $large_avatar_owner, 150, 185);
            if ($file_saved) {
                if (file_exists($source)) {
                    unlink($source);
                }
            }
        }



        if ($file_saved) {

            $params = array(
                'small_avatar_path' => $subdir . $small_avatar_owner,
                'large_avatar_path' => $subdir . $large_avatar_owner
            );

            if ($type == 'owner') {
                $modelContractorOwner = new Model_ContractorOwner();
                $updated = $modelContractorOwner->updateById($id, $params);
            } else if ($type == 'employee') {
                $modelContractorEmployee = new Model_ContractorEmployee();
                $updated = $modelContractorEmployee->updateById($id, $params);
            }
        }


        if ($updated || $file_saved) {
            $data['result'] = $subdir . $small_avatar_owner;
            $data['msg'] = 'Saved successfully';
            $data['type'] = 'success';
        } else {
            $data['msg'] = 'There is no file';
            $data['type'] = 'error';
        }





        $data['authrezed'] = 1;
        echo json_encode($data);
        exit;
    }

    public function deleteContractorEmployeeAction() {

        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $id = $this->request->getParam('id', 0);


        $this->checkAccessToken($accessToken);

        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];



          $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          $data['type'] = 'error';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {
          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */


        $loggedUser = CheckAuth::getLoggedUser();
        $contractorEmployeeObj = new Model_ContractorEmployee();
        $contractorEmployee = $contractorEmployeeObj->getById($id);

        if ($contractorEmployee) {
            $contractorEmployeeObj->updateById($id, array('is_deleted' => 1));
            $data['msg'] = 'Employee deleted successfully';
            $data['type'] = 'success';
        } else {
            $data['msg'] = 'Employee not found';
            $data['type'] = 'error';
        }




        $data['authrezed'] = 1;
        echo json_encode($data);
        exit;
    }

    public function addContractorEquipmentAction() {

        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $equipment = $this->request->getParam('equipment');
        $attachCount = $this->request->getParam('attachCount');
        $equipmentFiles = $this->request->getParam('equipment_files');

        $this->checkAccessToken($accessToken);

        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];



          $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          $data['type'] = 'error';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {
          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */


        $loggedUser = CheckAuth::getLoggedUser();
        $declarationOfEquipmentObj = new Model_DeclarationOfEquipment();
        $contractorInfoObj = new Model_ContractorInfo();
        $attachmentObj = new Model_Attachment();
        $declarationOfEquipmentAttachmentObj = new Model_DeclarationOfEquipmentAttachment();
        $contractorInfo = $contractorInfoObj->getByContractorId($loggedUser['user_id']);
        $params = array(
            'equipment' => $equipment,
            'contractor_info_id' => $contractorInfo['contractor_info_id']
        );

        $success = $declarationOfEquipmentObj->insert($params);
        $id = 0;
        if ($attachCount) {
            for ($i = 0; $i < $attachCount; $i++) {
                $source = $_FILES['equipmentFile' . $i]['tmp_name'];
                $extension = $this->request->getParam('ext' . $i);
                $fileInfo = pathinfo($source);
                $ext = $extension;
                $dir = get_config('attachment') . '/';
                $subdir = date('Y/m/d/');
                $fullDir = $dir . $subdir;
                if (!is_dir($fullDir)) {
                    mkdir($fullDir, 0777, true);
                }

                $fileName = $success . '_' . $i . '.' . $ext;
                $image_saved = copy($source, $fullDir . $fileName);
                $size = filesize($fullDir . $fileName);


                $type = mime_content_type($fullDir . $fileName);



                $typeParts = explode("/", $type);
                if ($typeParts[0] == 'image') {
                    $thumbName = $success . '_thumbnail_' . $i . '.' . $ext;
                    ImageMagick::create_thumbnail($source, $fullDir . $thumbName, 78, 64);
                } else {
                    $thumbName = $success . '_' . $i . '.jpg';
                    ImageMagick::convertFile($source . '[0]', $fullDir . $thumbName);
                }
                $Attchparams = array(
                    'created_by' => $loggedUser['user_id']
                    , 'created' => time()
                    , 'size' => $size
                    , 'type' => $type
                    , 'path' => $fullDir . $fileName,
                    'file_name' => $fileName,
                    'thumbnail_file' => $fullDir . $thumbName
                );

                $id = $attachmentObj->insert($Attchparams);
                $declarationOfEquipmentAttachmentObj->insert(array('equipment_id' => $success, 'attachment_id' => $id));
                if ($image_saved) {
                    if (file_exists($source)) {
                        unlink($source);
                    }
                }
            }
        }

        $id = 0;
        if ((isset($equipmentFiles) && !empty($equipmentFiles))) {
            $counter = 0;
            $dir = get_config('attachment') . '/';
            $subdir = date('Y/m/d/');
            $fullDir = $dir . $subdir;
            if (!is_dir($fullDir)) {
                mkdir($fullDir, 0777, true);
            }

            foreach ($equipmentFiles as $key => $imageData) {
                if (isset($imageData) && !empty($imageData)) {
                    $ext = 'png';
                    $image = str_replace('data:image/png;base64,', '', $imageData);
                    $image = str_replace(' ', '+', $image);
                    $image = base64_decode($image);
                    $tempFile = "image_{$key}." . $ext;
                    $file = $fullDir . $tempFile;
                    $successUpload = file_put_contents($file, $image);
                    $counter = $counter + 1;
                    if ($successUpload) {
                        $fileName = $success . '_' . $counter . '_' . '.' . $ext;
                        $image_saved = copy($file, $fullDir . $fileName);
                        $size = filesize($fullDir . $fileName);
                        $type = mime_content_type($fullDir . $fileName);
                        $typeParts = explode("/", $type);
                        if ($typeParts[0] == 'image') {
                            $thumbName = $success . '_thumbnail_' . $counter . '.' . $ext;
                            ImageMagick::create_thumbnail($file, $fullDir . $thumbName, 78, 64);
                        } else {
                            $thumbName = $success . '_' . $counter . '.jpg';
                            ImageMagick::convertFile($file . '[0]', $fullDir . $thumbName);
                        }

                        $attach = array(
                            'created_by' => $loggedUser['user_id']
                            , 'created' => time()
                            , 'size' => $size
                            , 'type' => $type
                            , 'path' => $fullDir . $fileName,
                            'file_name' => $fileName,
                            'thumbnail_file' => $fullDir . $thumbName
                        );

                        $Attchparams = array(
                            'created_by' => $loggedUser['user_id']
                            , 'created' => time()
                            , 'size' => $size
                            , 'type' => $type
                            , 'path' => $fullDir . $fileName,
                            'file_name' => $fileName,
                            'thumbnail_file' => $fullDir . $thumbName
                        );

                        $id = $attachmentObj->insert($Attchparams);
                        $declarationOfEquipmentAttachmentObj->insert(array('equipment_id' => $success, 'attachment_id' => $id));
                        if ($image_saved) {
                            if (file_exists($file)) {
                                unlink($file);
                            }
                        }
                    }
                }
            }
        }

        $equipment = $declarationOfEquipmentObj->getById($success);
        $modelAttachment = new Model_Attachment();
        $filter = array('type' => 'equipment', 'itemid' => $success, 'item_type' => 'image');
        $pager = null;
        $attachments = $modelAttachment->getAll('a.created desc', $pager, $filter);
        $equipment['attachments'] = $attachments;
        if ($success || $id) {
            $data['msg'] = 'Saved successfully';
            $data['type'] = 'success';
            $data['result'] = $equipment;
        } else {
            $data['msg'] = 'No Changes in Declaration Of Equipment';
            $data['type'] = 'error';
        }

        $data['authrezed'] = 1;
        echo json_encode($data);
        exit;
    }

    public function deleteContractorEquipmentAction() {

        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $id = $this->request->getParam('id', 0);

        $this->checkAccessToken($accessToken);
        /* modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];



          $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          $data['type'] = 'error';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {
          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */


        $loggedUser = CheckAuth::getLoggedUser();
        $declarationOfEquipmentObj = new Model_DeclarationOfEquipment();
        $modelAttachment = new Model_Attachment();
        $declarationOfEquipment = $declarationOfEquipmentObj->getById($id);

        if ($declarationOfEquipment) {
            $filter = array('type' => 'equipment', 'itemid' => $id);
            $pager = null;
            $attachments = $modelAttachment->getAll('a.created desc', $pager, $filter);
            if ($attachments) {
                foreach ($attachments as $attachment) {
                    $modelAttachment->updateById($attachment['attachment_id'], array('is_deleted' => '1'));
                }
            }

            $declarationOfEquipmentObj->deleteById($id);
            $data['msg'] = 'Equipment deleted successfully';
            $data['type'] = 'success';
        } else {
            $data['msg'] = 'Equipment not found';
            $data['type'] = 'error';
        }

        $data['authrezed'] = 1;
        echo json_encode($data);
        exit;
    }

    public function addContractorChemicalsAction() {

        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $chemicals = $this->request->getParam('chemicals');

        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];



          $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          $data['type'] = 'error';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {
          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */


        $this->checkAccessToken($accessToken);
        $loggedUser = CheckAuth::getLoggedUser();
        $contractorInfoObj = new Model_ContractorInfo();
        $contractorInfo = $contractorInfoObj->getByContractorId($loggedUser['user_id']);
        $declarationOfChemicalsObj = new Model_DeclarationOfChemicals();
        $params = array(
            'chemicals' => $chemicals,
            'contractor_info_id' => $contractorInfo['contractor_info_id']
        );

        $success = $declarationOfChemicalsObj->insert($params);

        $Chemical = $declarationOfChemicalsObj->getById($success);

        if ($success) {
            $data['msg'] = 'Saved successfully';
            $data['type'] = 'success';
            $data['result'] = $Chemical;
        } else {
            $data['msg'] = 'No Changes in Declaration Of Chemicals';
            $data['type'] = 'error';
        }

        $data['authrezed'] = 1;
        echo json_encode($data);
        exit;
    }

    public function deleteContractorChemicalsAction() {

        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $id = $this->request->getParam('id', 0);

        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];



          $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          $data['type'] = 'error';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {
          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */

        $this->checkAccessToken($accessToken);
        $loggedUser = CheckAuth::getLoggedUser();
        $declarationOfChemicalsObj = new Model_DeclarationOfChemicals();
        $declarationOfChemicals = $declarationOfChemicalsObj->getById($id);

        if ($declarationOfChemicals) {
            $declarationOfChemicalsObj->deleteById($id);
            $data['msg'] = 'Chemicals deleted successfully';
            $data['type'] = 'success';
        } else {
            $data['msg'] = 'Chemicals not found';
            $data['type'] = 'error';
        }

        $data['authrezed'] = 1;
        echo json_encode($data);
        exit;
    }

    public function addContractorApparatusAction() {

        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $other_apparatus = $this->request->getParam('other_apparatus');

        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];



          $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          $data['type'] = 'error';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {
          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */

        $this->checkAccessToken($accessToken);
        $loggedUser = CheckAuth::getLoggedUser();
        $contractorInfoObj = new Model_ContractorInfo();
        $contractorInfo = $contractorInfoObj->getByContractorId($loggedUser['user_id']);
        $declarationOfOtherApparatusObj = new Model_DeclarationOfOtherApparatus();
        $params = array(
            'other_apparatus' => $other_apparatus,
            'contractor_info_id' => $contractorInfo['contractor_info_id']
        );

        $success = $declarationOfOtherApparatusObj->insert($params);
        $Apparatus = $declarationOfOtherApparatusObj->getById($success);

        if ($success) {
            $data['msg'] = 'Saved successfully';
            $data['type'] = 'success';
            $data['result'] = $Apparatus;
        } else {
            $data['msg'] = 'No Changes in Declaration Of Other Apparatus';
            $data['type'] = 'error';
        }

        $data['authrezed'] = 1;
        echo json_encode($data);
        exit;
    }

    public function deleteContractorApparatusAction() {

        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $id = $this->request->getParam('id', 0);

        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];



          $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          $data['type'] = 'error';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {
          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */

        $this->checkAccessToken($accessToken);
        $loggedUser = CheckAuth::getLoggedUser();
        $declarationOfOtherApparatusObj = new Model_DeclarationOfOtherApparatus();
        $declarationOfOtherApparatus = $declarationOfOtherApparatusObj->getById($id);

        if ($declarationOfOtherApparatus) {
            $declarationOfOtherApparatusObj->deleteById($id);
            $data['msg'] = 'Apparatus deleted successfully';
            $data['type'] = 'success';
        } else {
            $data['msg'] = 'Apparatus not found';
            $data['type'] = 'error';
        }

        $data['authrezed'] = 1;
        echo json_encode($data);
        exit;
    }

    public function changeContractorPasswordAction() {

        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $new_password = $this->request->getParam('new_password');

        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];



          $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          $data['type'] = 'error';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {
          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */

        $this->checkAccessToken($accessToken);
        $loggedUser = CheckAuth::getLoggedUser();
        $params = array(
            'password' => sha1($new_password)
        );
        $modelUser = new Model_User();
        $success = $modelUser->updateById($loggedUser['user_id'], $params);
        if ($success) {
            $data['msg'] = 'Your password changed successfully';
            $data['type'] = 'success';
        } else {
            $data['msg'] = 'Could not change your password';
            $data['type'] = 'error';
        }


        $data['authrezed'] = 1;
        echo json_encode($data);
        exit;
    }

    public function changeContractorEmailAction() {

        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $old_email = $this->request->getParam('old_email');
        $new_email = $this->request->getParam('new_email');
        $confirm_new_email = $this->request->getParam('confirm_new_email');

        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];



          $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          $data['type'] = 'error';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {
          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */

        $this->checkAccessToken($accessToken);
        $loggedUser = CheckAuth::getLoggedUser();
        $modelUser = new Model_User();
        $old_user_email = $modelUser->getByEmail($old_email);
        if ($loggedUser['user_id'] == $old_user_email['user_id']) {
            if ($new_email == $confirm_new_email) {
                $modelUser = new Model_User();

                $dataEmail = array(
                    'temp_email' => $new_email
                );
                $modelUser->updateById($user['user_id'], $dataEmail);
                $activation_code = sha1(uniqid());
                $params = array(
                    'code' => $activation_code,
                    'created' => time(),
                    'type' => 'change_email',
                    'ip_address' => $_SERVER['REMOTE_ADDR'],
                    'user_id' => $user['user_id']
                );
                $modelCode = new Model_Code();
                $modelCode->insert($params);
                $router = Zend_Controller_Front::getInstance()->getRouter();
                $activation_link = $router->assemble(array('code' => $activation_code, 'id' => $user['user_id']), 'changeEmailStep2');

                $template_params = array(
                    '{username}' => ucwords($user['username']),
                    '{new_email}' => $new_email,
                    '{activation_link}' => '<a href="' . $activation_link . '">' . $activation_link . '</a>'
                );

                $sucess = EmailNotification::sendEmail(array('to' => $user['email1']), 'change_email', $template_params);

                if ($success) {
                    $data['msg'] = 'Please Check Your Email address and Complete the Instruction';
                    $data['type'] = 'success';
                } else {
                    $data['msg'] = 'Could Not Send change Email';
                    $data['type'] = 'error';
                }
            }
        }




        $data['authrezed'] = 1;
        echo json_encode($data);
        exit;
    }

    public function uploadContractorPhotoAction() {


        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $modelIosUser = new Model_IosUser();
        $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
        $this->iosLoggedUser = $user['id'];
        $contractor_avatar = $this->request->getParam('contractor_avatar', 0);


        $this->checkAccessToken($accessToken);
        /* $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          $data['type'] = 'error';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {
          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */


        $loggedUser = CheckAuth::getLoggedUser();
        $id = $loggedUser['user_id'];


        $dir = get_config('user_picture');
        $dir2 = get_config('user_picture_medium');
        $dir3 = get_config('user_picture_small');

        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }
        if (!is_dir($dir2)) {
            mkdir($dir2, 0777, true);
        }
        if (!is_dir($dir3)) {
            mkdir($dir3, 0777, true);
        }



        //echo 'source: '.$source;
        //echo 'path: '.$original_path;
        //exit;

        if (isset($contractor_avatar) && !empty($contractor_avatar)) {
            $image = str_replace('data:image/png;base64,', '', $contractor_avatar);
            $extension = $this->request->getParam('extension', 'png');
            $image = str_replace(' ', '+', $image);
            $image = base64_decode($image);
            $fileName = "image." . $extension;
            $file = $dir . $fileName;
            $original_path = time() . "_" . $id . '.' . $extension;
            $success = file_put_contents($file, $image);
            if ($success) {
                $image_saved = copy($file, $dir . $original_path);
                ImageMagick::create_thumbnail($file, $dir2 . $original_path, 210, 210);
                ImageMagick::create_thumbnail($file, $dir3 . $original_path, 50, 50);

                if (file_exists($file)) {
                    unlink($file);
                }
            }
        }

        if (isset($_FILES['avatar'])) {
            $source = $_FILES['avatar']['tmp_name'];
            $extension = $this->request->getParam('extension', 'png');
            $imageInfo = pathinfo($source);
            $ext = $extension;
            $original_path = time() . "_" . $id . '.' . $ext;
            $image_saved = copy($source, $dir . $original_path);
            ImageMagick::create_thumbnail($source, $dir2 . $original_path, 210, 210);
            ImageMagick::create_thumbnail($source, $dir3 . $original_path, 50, 50);
        }

        $modeluser = new Model_User();
        if ($image_saved) {
            $param = array('avatar' => $original_path);
            $success = $modeluser->updateById($id, $param);
            if ($success) {
                $data['msg'] = 'Saved Successfully';
                $data['type'] = 'success';
            } else {
                $data['msg'] = 'Could not change photo';
                $data['type'] = 'error';
            }
        }


        $data['authrezed'] = 1;
        echo json_encode($data);
        exit;
    }

    public function changeContractorInfoAction() {

        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $username = $this->request->getParam('username');
        $business_name = $this->request->getParam('business_name');
        $acn = $this->request->getParam('acn');
        $abn = $this->request->getParam('abn');
        $tfn = $this->request->getParam('tfn');
        $gst = $this->request->getParam('gst');
        $gst_date_registered = $this->request->getParam('gst_date_registered');
        $insurance_policy_number = $this->request->getParam('insurance_policy_number');
        $insurance_policy_start = $this->request->getParam('insurance_policy_start');
        $insurance_policy_expiry = $this->request->getParam('insurance_policy_expiry');
        $insurance_type = $this->request->getParam('insurance_type');
        $insurance_listed_services_covered = $this->request->getParam('insurance_listed_services_covered');
        $drivers_licence_number = $this->request->getParam('drivers_licence_number');
        $drivers_licence_expiry = $this->request->getParam('drivers_licence_expiry');
        $driver_licence_type = $this->request->getParam('driver_licence_type');
        $driver_licence_type = $this->request->getParam('driver_licence_type');
        $bank_name = $this->request->getParam('bank_name');
        $account_name = $this->request->getParam('account_name');
        $bsb = $this->request->getParam('bsb');
        $account_number = $this->request->getParam('account_number');
        $insurance_file_count = $this->request->getParam('insurance_file_count', 0);
        $driverFiles = $this->request->getParam('driver_files', 0);
        //$insuranceFiles = $this->request->getParam('insurance_files', 0);
        $drivers_file_count = $this->request->getParam('drivers_file_count', 0);

        $this->checkAccessToken($accessToken);
        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];

          $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          $data['type'] = 'error';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {
          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */


        $loggedUser = CheckAuth::getLoggedUser();
        $contractorInfoObj = new Model_ContractorInfo();
        $modelContractorInfoAttachment = new Model_ContractorInfoAttachment();
        $attachmentObj = new Model_Attachment();
        $contractorInfo = $contractorInfoObj->getByContractorId($loggedUser['user_id']);



        $params = array(
            'business_name' => $business_name,
            'acn' => $acn,
            'abn' => $abn,
            'tfn' => $tfn,
            'gst' => $gst,
            'gst_date_registered' => strtotime($gst_date_registered),
            'insurance_policy_number' => $insurance_policy_number,
            'insurance_type' => $insurance_type,
            'insurance_policy_start' => strtotime($insurance_policy_start),
            'insurance_policy_expiry' => strtotime($insurance_policy_expiry),
            'insurance_listed_services_covered' => $insurance_listed_services_covered,
            'drivers_licence_number' => $drivers_licence_number,
            'driver_licence_type' => $driver_licence_type,
            'drivers_licence_expiry' => strtotime($drivers_licence_expiry),
            'bank_name' => $bank_name,
            'account_name' => $account_name,
            'bsb' => $bsb,
            'account_number' => $account_number,
        );

        if (!$contractorInfo) {
            $data['contractor_id'] = $loggedUser['user_id'];
            $success = $contractorInfoObj->insert($params);
            $contractorInfo = $contractorInfoObj->getByContractorId($loggedUser['user_id']);
        } else {
            $success = $contractorInfoObj->updateById($contractorInfo['contractor_info_id'], $params);
            $modelUser = new Model_User();
            $userParams = array('username' => $username);
            $updateUser = $modelUser->updateById($loggedUser['user_id'], $userParams);
        }


        $insurance_files = 0;
        if ($insurance_file_count) {
            $Attachments = $contractorInfoObj->getAllAttachmentByTypeAndId($contractorInfo['contractor_info_id'], 'insurance');
            $counter = count($Attachments);
            $dir = get_config('attachment') . '/';
            $subdir = date('Y/m/d/');
            $fullDir = $dir . $subdir;
            if (!is_dir($fullDir)) {
                mkdir($fullDir, 0777, true);
            }
            for ($i = 0; $i < $insurance_file_count; $i++) {
                $counter = $counter + 1;
                $source = $_FILES['insuranceFile' . $i]['tmp_name'];
                $ext = $this->request->getParam('insuranceExt' . $i, 'png');
                $fileName = $contractorInfo['contractor_info_id'] . '_' . $counter . '_' . 'insurance' . '.' . $ext;
                $image_saved = copy($source, $fullDir . $fileName);
                $size = filesize($fullDir . $fileName);
                $type = mime_content_type($fullDir . $fileName);
                $typeParts = explode("/", $type);
                if ($typeParts[0] == 'image') {
                    $thumbName = $contractorInfo['contractor_info_id'] . '_' . $counter . '_' . 'insurance_thumbnail' . '.' . $ext;
                    ImageMagick::create_thumbnail($source, $fullDir . $thumbName, 78, 64);
                } else {
                    $thumbName = $contractorInfo['contractor_info_id'] . '_' . $counter . '_' . 'insurance' . '.jpg';
                    ImageMagick::convertFile($source . '[0]', $fullDir . $thumbName);
                }

                $data = array(
                    'created_by' => $loggedUser['user_id']
                    , 'created' => time()
                    , 'size' => $size
                    , 'type' => $type
                    , 'path' => $fullDir . $fileName,
                    'file_name' => $fileName,
                    'thumbnail_file' => $fullDir . $thumbName
                );
                $Attachid = $attachmentObj->insert($data);
                $insurance_files = $Attachid;
                $contractorInfoAttachmentId = $modelContractorInfoAttachment->insert(array('contractor_info_id' => $contractorInfo['contractor_info_id'], 'attachment_id' => $Attachid, 'type' => 'insurance'));
            }
        }



        $driver_files = 0;

        if (isset($driverFiles) && !empty($driverFiles)) {

            $Attachments = $contractorInfoObj->getAllAttachmentByTypeAndId($contractorInfo['contractor_info_id'], 'licence');
            $counter = count($Attachments);
            $dir = get_config('attachment') . '/';
            $subdir = date('Y/m/d/');
            $fullDir = $dir . $subdir;
            if (!is_dir($fullDir)) {
                mkdir($fullDir, 0777, true);
            }

            foreach ($driverFiles as $key => $imageData) {
                if (isset($imageData) && !empty($imageData)) {
                    $ext = 'png';
                    $image = str_replace('data:image/png;base64,', '', $imageData);
                    $image = str_replace(' ', '+', $image);
                    $image = base64_decode($image);
                    $tempFile = "image_{$key}." . $ext;
                    $file = $fullDir . $tempFile;
                    $successUpload = file_put_contents($file, $image);
                    $counter = $counter + 1;
                    if ($successUpload) {
                        $fileName = $contractorInfo['contractor_info_id'] . '_' . $counter . '_' . 'licence' . '.' . $ext;
                        $image_saved = copy($file, $fullDir . $fileName);
                        $size = filesize($fullDir . $fileName);
                        $type = mime_content_type($fullDir . $fileName);
                        $typeParts = explode("/", $type);
                        if ($typeParts[0] == 'image') {
                            $thumbName = $contractorInfo['contractor_info_id'] . '_' . $counter . '_' . 'licence_thumbnail' . '.' . $ext;
                            ImageMagick::create_thumbnail($file, $fullDir . $thumbName, 78, 64);
                        } else {
                            $thumbName = $contractorInfo['contractor_info_id'] . '_' . $counter . '_' . 'licence' . '.jpg';
                            ImageMagick::convertFile($file . '[0]', $fullDir . $thumbName);
                        }

                        $Attchparams = array(
                            'created_by' => $loggedUser['user_id']
                            , 'created' => time()
                            , 'size' => $size
                            , 'type' => $type
                            , 'path' => $fullDir . $fileName,
                            'file_name' => $fileName,
                            'thumbnail_file' => $fullDir . $thumbName
                        );

                        $attchid = $attachmentObj->insert($Attchparams);
                        $contractorInfoAttachmentId = $modelContractorInfoAttachment->insert(array('contractor_info_id' => $contractorInfo['contractor_info_id'], 'attachment_id' => $attchid, 'type' => 'licence'));

                        if (file_exists($file)) {
                            unlink($file);
                        }
                    }
                }
            }
        }

        if ($drivers_file_count) {
            $Attachments = $contractorInfoObj->getAllAttachmentByTypeAndId($contractorInfo['contractor_info_id'], 'licence');
            $counter = count($Attachments);
            $dir = get_config('attachment') . '/';
            $subdir = date('Y/m/d/');
            $fullDir = $dir . $subdir;
            if (!is_dir($fullDir)) {
                mkdir($fullDir, 0777, true);
            }
            for ($i = 0; $i < $drivers_file_count; $i++) {
                $counter = $counter + 1;
                $source = $_FILES['driverFile' . $i]['tmp_name'];
                $ext = $this->request->getParam('driverExt' . $i, 'png');
                $fileName = $contractorInfo['contractor_info_id'] . '_' . $counter . '_' . 'licence' . '.' . $ext;
                $image_saved = copy($source, $fullDir . $fileName);
                $size = filesize($fullDir . $fileName);
                $type = mime_content_type($fullDir . $fileName);
                $typeParts = explode("/", $type);
                if ($typeParts[0] == 'image') {
                    $thumbName = $contractorInfo['contractor_info_id'] . '_' . $counter . '_' . 'licence_thumbnail' . '.' . $ext;
                    ImageMagick::create_thumbnail($source, $fullDir . $thumbName, 78, 64);
                } else {
                    $thumbName = $contractorInfo['contractor_info_id'] . '_' . $counter . '_' . 'licence' . '.jpg';
                    ImageMagick::convertFile($source . '[0]', $fullDir . $thumbName);
                }

                $data = array(
                    'created_by' => $loggedUser['user_id']
                    , 'created' => time()
                    , 'size' => $size
                    , 'type' => $type
                    , 'path' => $fullDir . $fileName,
                    'file_name' => $fileName,
                    'thumbnail_file' => $fullDir . $thumbName
                );
                $Attachid = $attachmentObj->insert($data);
                $driver_files = $Attachid;
                $contractorInfoAttachmentId = $modelContractorInfoAttachment->insert(array('contractor_info_id' => $contractorInfo['contractor_info_id'], 'attachment_id' => $Attachid, 'type' => 'licence'));
            }
        }

        $contractorInfo = $contractorInfoObj->getByContractorId($loggedUser['user_id']);
        $data['result'] = $contractorInfo;

        if ($contractorInfo) {
            $modelAttachment = new Model_Attachment();
            $filter = array('type' => 'insurance', 'itemid' => $contractorInfo['contractor_info_id'], 'item_type' => 'image');
            $pager = null;
            $insuranceAttachments = $modelAttachment->getAll('a.created desc', $pager, $filter);
            $data['result']['insurance_attachment'] = $insuranceAttachments;
            $filter = array('type' => 'licence', 'itemid' => $contractorInfo['contractor_info_id'], 'item_type' => 'image');
            $licenceAttachments = $modelAttachment->getAll('a.created desc', $pager, $filter);
            $data['result']['drivers_licence_attachment'] = $licenceAttachments;
        }



        if ($success || $updateUser || $insurance_files || $driver_files || $driverFiles) {
            $data['msg'] = "Saved successfully";
            $data['type'] = 'success';
        } else {
            $data['msg'] = "No Changes in Contractor Info";
            $data['type'] = 'error';
        }




        $data['authrezed'] = 1;
        echo json_encode($data);
        exit;
    }

    public function changeContractorAccountAction() {

        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $email2 = $this->request->getParam('email2');
        $email3 = $this->request->getParam('email3');
        $mobile1 = $this->request->getParam('mobile1');
        $mobile2 = $this->request->getParam('mobile2');
        $mobile3 = $this->request->getParam('mobile3');
        $phone1 = $this->request->getParam('phone1');
        $phone2 = $this->request->getParam('phone2');
        $phone3 = $this->request->getParam('phone3');
        $fax = $this->request->getParam('fax');
        $emergencyPhone = $this->request->getParam('emergency_phone');
        $unitLotNumber = $this->request->getParam('unit_lot_number');
        $streetNumber = $this->request->getParam('street_number');
        $streetAddress = $this->request->getParam('street_address');
        $suburb = $this->request->getParam('suburb');
        $state = $this->request->getParam('state');
        $postcode = $this->request->getParam('postcode');
        $po_box = $this->request->getParam('po_box');
        $cityId = $this->request->getParam('city_id');
        $countryId = $this->request->getParam('country_id');


        if (get_config('remove_white_spacing')) {
            $mobile1 = preparer_number($mobile1);
            $mobile2 = preparer_number($mobile2);
            $mobile3 = preparer_number($mobile3);
            $phone1 = preparer_number($phone1);
            $phone2 = preparer_number($phone2);
            $phone3 = preparer_number($phone3);
        }

        $this->checkAccessToken($accessToken);
        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];
          $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          $data['type'] = 'error';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {
          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */

        $success = 0;
        $loggedUser = CheckAuth::getLoggedUser();
        if (!empty($streetNumber) && !empty($streetAddress) && !empty($state) && !empty($cityId) && !empty($postcode)) {
            $params = array(
                'city_id' => $cityId,
                'email2' => $email2,
                'email3' => $email3,
                'mobile1' => $mobile1,
                'mobile2' => $mobile2,
                'mobile3' => $mobile3,
                'phone1' => $phone1,
                'phone2' => $phone2,
                'phone3' => $phone3,
                'fax' => $fax,
                'emergency_phone' => $emergencyPhone,
                'unit_lot_number' => $unitLotNumber,
                'street_number' => $streetNumber,
                'street_address' => $streetAddress,
                'suburb' => $suburb,
                'state' => $state,
                'postcode' => $postcode,
                'po_box' => $po_box
            );

            $modelUser = new Model_User();
            $success = $modelUser->updateById($loggedUser['user_id'], $params);
        }

        if ($success) {
            $data['msg'] = 'Saved successfully';
            $data['type'] = 'success';
        } else {
            $data['msg'] = 'No Changes in User';
            $data['type'] = 'error';
        }

        $data['authrezed'] = 1;
        echo json_encode($data);
        exit;
    }

    public function getCountriesAction() {

        header('Content-Type: application/json');
        /* $accessToken = $this->request->getParam('access_token',0);
          $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];

          $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);

          if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
          $data['msg'] = 'wrong access token';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {
          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if(!$authrezed){
          echo json_encode($data);
          exit;
          }
          } */


        $loggedUser = CheckAuth::getLoggedUser();
        $modelCountries = new Model_Countries();
        $countries = $modelCountries->getCountriesAsArray();
        $data['result'] = $countries;

        $data['authrezed'] = 1;
        echo json_encode($data);
        exit;
    }

    public function getCitiesAction() {

        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $country_id = $this->request->getParam('country_id', 9);
        $state = $this->request->getParam('state', 'NSW');
        if ($country_id == null || $country_id == 'null' || $country_id == 0) {
            $country_id = 9;
        }
        if ($state == null || $state == 'null' || $state == 0) {
            $state = 'NSW';
        }
        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);

          $this->iosLoggedUser = $user['id'];
          $modelCompanies = new Model_Companies();
          $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);
          if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
          $data['msg'] = 'wrong access token';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {

          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if(!$authrezed){
          echo json_encode($data);
          exit;
          }

          } */

        $cities_obj = new Model_Cities();
        $cities = $cities_obj->getCitiesByCountryIdAndState($country_id, $state);

        $tempCities = array();
        foreach ($cities as $city) {
            $tempCities[] = array(
                'id' => $city['city_id'],
                'name' => $city['city_name']
            );
        }


        $data['result'] = $tempCities;
        $data['authrezed'] = 1;
        echo json_encode($data);
        exit;
    }

    public function getStateAction() {

        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $countryId = $this->request->getParam('country_id', 9);
        if ($countryId == null || $countryId == 'null' || $countryId == 0) {
            $countryId = 9;
        }

        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);

          $this->iosLoggedUser = $user['id'];
          $modelCompanies = new Model_Companies();
          $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);
          if(!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))){
          $data['msg'] = 'wrong access token';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {

          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if(!$authrezed){
          echo json_encode($data);
          exit;
          }

          } */


        $cities_obj = new Model_Cities();
        $states = $cities_obj->getStateByCountryIdAsArray($countryId);




        $data['result'] = $states;
        $data['authrezed'] = 1;
        echo json_encode($data);
        exit;
    }

    public function getCompaniesAction() {

        header('Content-Type: application/json');
        $data = array();
        $modelCompany = new Model_Companies();
        $companies = $modelCompany->getAllForWebService();

        $data['result'] = $companies;

        echo json_encode($data);
        exit;
    }

    public function signUpAction() {

        header('Content-Type: application/json');

        $router = Zend_Controller_Front::getInstance()->getRouter();
        $forgetPassword = $router->assemble(array(), 'forgetPassword');
        $internation_key = "0061";
        $mobile1_key = "04";
        ///get parameters
        $request_type = $this->request->getParam('request_type', '');
        $first_name = $this->request->getParam('first_name', '');
        $last_name = $this->request->getParam('last_name', '');
        $username = $this->request->getParam('username');
        //$display_name = $this->request->getParam('display_name');
        $password = $this->request->getParam('password');
        $roleName = $this->request->getParam('role_name');
        $email1 = $this->request->getParam('email1');
        $email2 = $this->request->getParam('email2', '');
        $email3 = $this->request->getParam('email3', '');
        $systemEmail = $this->request->getParam('systemEmail', '');
        $mobile1 = $this->request->getParam('mobile1');
        $mobile2 = $this->request->getParam('mobile2', '');
        $mobile3 = $this->request->getParam('mobile3', '');
        $phone1 = $this->request->getParam('phone1');
        $phone2 = $this->request->getParam('phone2', '');
        $phone3 = $this->request->getParam('phone3', '');
        $fax = $this->request->getParam('fax', '');
        $emergencyPhone = $this->request->getParam('emergency_phone', '');
        $unitLotNumber = $this->request->getParam('unit_lot_number', '');
        $streetNumber = $this->request->getParam('street_number');
        $streetAddress = $this->request->getParam('street_address');
        $suburb = $this->request->getParam('suburb');
        //$state = $this->request->getParam('state');
        $postcode = $this->request->getParam('postcode');
        $po_box = $this->request->getParam('po_box', '');
        //$cityId = $this->request->getParam('city_id');
        //$countryId = $this->request->getParam('country_id');
        //$companyId = $this->request->getParam('company_id');

        $modelCompanies = new Model_Companies();
        $companyData = $modelCompanies->getByName('Tile Cleaners Pty Ltd');
        $companyId = $companyData['company_id'];
        //$cityId = $companyData['city_id'];
        $countryId = $companyData['country_id'];
        // $state = $companyData['state'];
        //Added by Walaa 
        $modelCustomer = new Model_Customer();
        $modelCity = new Model_Cities();
        $returnCityStateFlag = true;
        $cityState = $modelCustomer->getStateAndCityByPostCode($postcode, $returnCityStateFlag);
        $city = $modelCity->getByName($cityState['city']);
        if ($cityState['city'] == 'unknown') {
            $cityId = $companyData['city_id'];
            $state = $companyData['state'];
        } else {
            $cityId = $city['city_id'];
            $state = $cityState['state'];
        }


        $display_name = $first_name;
        $username = $first_name . substr($last_name, 0, 1);
        if (isset($mobile1) && $mobile1 != "") {
            $mobile1_key = substr($mobile1_key, 1, 1);
            $mobile1 = $internation_key . $mobile1_key . $mobile1;
        }
        if (isset($mobile2) && $mobile2 != "") {
            $mobile2_key = substr($mobile1_key, 1, 1);
            $mobile2 = $internation_key . $mobile2_key . $mobile2;
        }
        if (isset($mobile3) && $mobile3 != "") {
            $mobile3_key = substr($mobile1_key, 1, 1);
            $mobile3 = $internation_key . $mobile3_key . $mobile3;
        }

        if (get_config('remove_white_spacing')) {
            $mobile1 = preparer_number($mobile1);
            $mobile2 = preparer_number($mobile2);
            $mobile3 = preparer_number($mobile3);
            $phone1 = preparer_number($phone1);
            $phone2 = preparer_number($phone2);
            $phone3 = preparer_number($phone3);
        }


        if (!empty($username) && !empty($first_name) && !empty($last_name) && !empty($password) && !empty($display_name) && !empty($companyId) && !empty($email1) && !empty($mobile1) && !empty($streetNumber) && !empty($streetAddress) && !empty($countryId) && !empty($state) && !empty($cityId) && !empty($postcode)) {


            $modelUser = new Model_User();
            $ExistUser = $modelUser->getByEmail($email1);
            if ($ExistUser && !empty($ExistUser)) {
                $data['msg'] = "Failed, email already exists, try reset your password instead. <a href='" . $forgetPassword . "'>(Forget Password?)</a>";
                $data['type'] = "error";
                echo json_encode($data);
                exit;
            }

            $data = array(
                'display_name' => $display_name,
                'username' => $username,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'password' => sha1($password),
                'user_code' => sha1($username),
                'last_login' => time(),
                'created' => time(),
                'city_id' => $cityId,
                'role_id' => 1,
                'email1' => $email1,
                'email2' => $email2,
                'email3' => $email3,
                'system_email' => $systemEmail,
                'mobile1' => $mobile1,
                'mobile2' => $mobile2,
                'mobile3' => $mobile3,
                'phone1' => $phone1,
                'phone2' => $phone2,
                'phone3' => $phone3,
                'fax' => $fax,
                'emergency_phone' => $emergencyPhone,
                'unit_lot_number' => $unitLotNumber,
                'street_number' => $streetNumber,
                'street_address' => $streetAddress,
                'suburb' => $suburb,
                'state' => $state,
                'postcode' => $postcode,
                'active' => 'FALSE',
                'po_box' => $po_box
            );

            //
            //get role id for contractor
            //
                $userId = $modelUser->insert($data);

            $dataCompany = array(
                'user_id' => $userId,
                'company_id' => $companyId,
                'created' => time()
            );
            $userCompaniesModel = new Model_UserCompanies();
            $userCompaniesModel->insert($dataCompany);

            $modelAuthRole = new Model_AuthRole();
            $role = $modelAuthRole->getById(1);

            /* if ($role['role_name'] == 'contractor') {

              $modelContractorInfo = new Model_ContractorInfo();
              $modelContractorInfo->insert(array('contractor_id' => $userId));
              } */

            $data = array();

            if ($userId) {
                if ($request_type == 'web') {
                    $modelUser = new Model_User();
                    $user = $modelUser->getById($userId);
                    $auth = Zend_Auth::getInstance();
                    $authStorge = $auth->getStorage();
                    $user_obj = (object) $user;
                    $authStorge->write($user_obj);
                    CheckAuth::afterlogin(false, 'for_signUp');
                    $router = Zend_Controller_Front::getInstance()->getRouter();
                    $url = $router->assemble(array(), 'myAccount');
                    $data['type'] = "success";
                    $data['goTo'] = $url;
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Thanks for signing up ,Here your profile page where you can see and manage all your bookings "));
                } else {
                    $data['msg'] = "created successfully, you can access your profile and edit it only till the admin approve your account ";
                    $data['type'] = "success";
                }
            } else {
                $data['msg'] = "Failed, try again later";
                $data['type'] = "error";
            }
        } else {
            $data['msg'] = "Failed, try again later";
            $data['type'] = "error";
        }


        echo json_encode($data);
        exit;
    }

    /* public function changeContractorStatusAction() {

      header('Content-Type: application/json');
      $accessToken = $this->request->getParam('access_token', 0);
      $status_text = $this->request->getParam('status_text', '');
      $count = $this->request->getParam('count', '');
      $discussion_images = $this->request->getParam('discussion_images', '');
      $status_text = trim($status_text);
      $status = $this->request->getParam('status', '');
      $modelIosUser = new Model_IosUser();
      $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
      $this->iosLoggedUser = $user['id'];



      $loggedUser = CheckAuth::getLoggedUser();
      $data = array('authrezed' => 0);

      if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
      $data['msg'] = 'wrong access token';
      $data['type'] = 'error';
      echo json_encode($data);
      exit;
      }

      if (empty($loggedUser)) {
      //open new session
      $authrezed = $this->openNewSession($accessToken);
      if (!$authrezed) {
      echo json_encode($data);
      exit;
      }
      }


      $loggedUser = CheckAuth::getLoggedUser();


      ///new
      $modelContractorInfo = new Model_ContractorInfo();
      $modelImage = new Model_Image();
      $modelContractorDiscussion = new Model_ContractorDiscussion();
      $contractor_id = $loggedUser['user_id'];


      if (isset($status)) {
      $data = array(
      'status' => trim($status),
      );
      $success = $modelContractorInfo->updateByContractorId($contractor_id, $data);
      }
      if (isset($status_text) && !empty($status_text)) {
      $params = array('contractor_id' => $contractor_id,
      'user_id' => $loggedUser['user_id'],
      'user_message' => trim($status_text),
      'created' => time());
      $success_status = $modelContractorDiscussion->insert($params);

      $dir = get_config('image_attachment') . '/';
      $subdir = date('Y/m/d/');
      $fullDir = $dir . $subdir;
      if (!is_dir($fullDir)) {
      mkdir($fullDir, 0777, true);
      }


      if ((isset($count) && $count != '')) {
      $image_group = 0;
      if ($count > 1) {
      $maxGroup = max($modelImage->getMaxGroup());
      $image_group = $maxGroup['group_id'] + 1;
      }

      for ($i = 0; $i < $count; $i++) {

      $source = $_FILES['imageDiscussion' . $i]['tmp_name'];
      $extension = $this->request->getParam('ext' . $i, 'png');
      $imageInfo = pathinfo($source);
      $ext = $extension;
      $successUpload = 0;
      $fileName = "image_{$i}.{$ext}";
      $file = $fullDir . $fileName;
      $image_saved = copy($source, $fullDir . $fileName);
      if ($image_saved) {
      $successUpload = 1;
      $data = array(
      'user_ip' =>''
      , 'created_by' => $loggedUser['user_id']
      , 'image_types_id' => 0
      , 'group_id' => $image_group
      , 'user_role' => $loggedUser['role_id']
      );

      $id = $modelImage->insert($data);
      $Itemdata = array(
      'item_id' => $loggedUser['user_id'],
      'service_id' => 0,
      'discussion_id' => $success_status,
      'image_id' => $id,
      'type' => 'contractor_discussion',
      'floor_id' => 0
      );

      $modelItemImage = new Model_ItemImage();
      $Item_image_id = $modelItemImage->insert($Itemdata);
      $original_path = "original_{$id}.{$ext}";
      $large_path = "large_{$id}.{$ext}";
      $small_path = "small_{$id}.{$ext}";
      $thumbnail_path = "thumbnail_{$id}.{$ext}";
      $compressed_path = "compressed_{$id}.{$ext}";

      $data = array(
      'original_path' => $subdir . $original_path,
      'large_path' => $subdir . $large_path,
      'small_path' => $subdir . $small_path,
      'thumbnail_path' => $subdir . $thumbnail_path,
      'compressed_path' => $subdir . $compressed_path
      );

      $modelImage->updateById($id, $data);
      $compressed_saved = copy($source, $fullDir . $compressed_path);
      $image_saved = copy($source, $fullDir . $original_path);
      ImageMagick::scale_image($source, $fullDir . $large_path, 510);
      ImageMagick::scale_image($source, $fullDir . $small_path, 250);
      ImageMagick::create_thumbnail($source, $fullDir . $thumbnail_path, 78, 64);
      ImageMagick::convert($source, $fullDir . $compressed_path);
      ImageMagick::compress_image($source, $fullDir . $compressed_path);
      }


      if ($image_saved) {
      if (file_exists($source)) {
      unlink($source);
      }
      if (file_exists($file)) {
      unlink($file);
      }
      }
      }
      }


      }
      //

      $data['authrezed'] = 1;
      if ($success || $status_text) {
      $data['msg'] = 'Changed Successfully';
      $data['type'] = 'success';
      } else {
      $data['msg'] = 'No changes';
      $data['type'] = 'error';
      }

      echo json_encode($data);
      exit;
      } */

    public function changeContractorStatusAction() {

        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $status_text = $this->request->getParam('status_text', '');
        $count = $this->request->getParam('count', '');
        $discussionImages = $this->request->getParam('discussion_images', '');
        $status_text = trim($status_text);
        $status = $this->request->getParam('status', '');

        $modelUser = new Model_User();
        $modelContractorDiscussionMongo = new Model_ContractorDiscussionMongo();

        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];

          $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);

          //////////by walaa
          $thumbnails_paths = array();
          $originals_paths = array();
          $larges_paths = array();
          $small_paths = array();
          $compressed_paths = array();
          //////////////

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          $data['type'] = 'error';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {
          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */

        $this->checkAccessToken($accessToken);

        $loggedUser = CheckAuth::getLoggedUser();


        ///new
        $modelContractorInfo = new Model_ContractorInfo();
        $modelImage = new Model_Image();
        $modelContractorDiscussion = new Model_ContractorDiscussion();
        $contractor_id = $loggedUser['user_id'];


        if (isset($status)) {
            $data = array(
                'status' => trim($status),
            );

            $success = $modelContractorInfo->updateByContractorId($contractor_id, $data);
        }

        if (isset($status_text) && !empty($status_text)) {


            ////by walaa
            $user = $modelUser->getById($loggedUser['user_id']);
            if ($user['role_name'] == 'contractor') {
                $contractorInfo = $modelContractorInfo->getByContractorId($user['user_id']);
                $user['username'] = ucwords($user['username']) . ' - ' . ucwords($contractorInfo['business_name']);
            } else {
                $user['username'] = ucwords($user['username']);
            }
            $contractor = $modelUser->getById($contractor_id);

            $doc = array(
                'contractor_id' => (int) $contractor_id,
                'contractor_name' => $contractor['username'],
                'user_id' => $loggedUser['user_id'],
                'user_name' => $user['username'],
                'avatar' => $user['avatar'],
                'user_message' => $status_text,
                'created' => time(),
                'user_role_name' => $user['role_name'],
                'thumbnail_images' => array(),
                'original_images' => array(),
                'large_images' => array(),
                'small_images' => array(),
                'compressed_images' => array(),
                'seen_by_ids' => "",
                'seen_by_names' => "",
                'visibility' => 2,
                'type' => "contractor_discussion"
                    //,'audio_path' => ""
            );

            $success = $modelContractorDiscussionMongo->insertDiscussion($doc);
            $newDocID = $doc['_id'];
            foreach ($newDocID as $key => $value)
                $disc_id = $value;
            //////end by walaa

            $params = array('contractor_id' => $contractor_id, 'user_id' => $loggedUser['user_id'], 'user_message' => trim($status_text), 'created' => time());
            $success_status = $modelContractorDiscussion->insert($params);

            $dir = get_config('image_attachment') . '/';
            $subdir = date('Y/m/d/');
            $fullDir = $dir . $subdir;
            if (!is_dir($fullDir)) {
                mkdir($fullDir, 0777, true);
            }


            if ((isset($count) && $count != '')) {
                $image_group = 0;
                if ($count > 1) {
                    $maxGroup = max($modelImage->getMaxGroup());
                    $image_group = $maxGroup['group_id'] + 1;
                }

                for ($i = 0; $i < $count; $i++) {
                    $x = $i + 1;
                    $source = $_FILES['imageDiscussion' . $i]['tmp_name'];
                    $extension = $this->request->getParam('ext' . $i, 'png');
                    $imageInfo = pathinfo($source);
                    $ext = $extension;
                    $successUpload = 0;
                    $fileName = "image_{$i}.{$ext}";
                    $file = $fullDir . $fileName;
                    $image_saved = copy($source, $fullDir . $fileName);
                    if ($image_saved) {
                        $successUpload = 1;
                        $data = array(
                            'user_ip' => ''
                            , 'created_by' => $loggedUser['user_id']
                            , 'image_types_id' => 0
                            , 'group_id' => $image_group
                            , 'user_role' => $loggedUser['role_id']
                        );

                        $id = $modelImage->insert($data);
                        $Itemdata = array(
                            'item_id' => $loggedUser['user_id'],
                            'service_id' => 0,
                            'discussion_id' => $success_status,
                            'image_id' => $id,
                            'type' => 'contractor_discussion',
                            'floor_id' => 0
                        );

                        $modelItemImage = new Model_ItemImage();
                        $Item_image_id = $modelItemImage->insert($Itemdata);
                        $original_path = "original_{$id}.{$ext}";
                        $large_path = "large_{$id}.{$ext}";
                        $small_path = "small_{$id}.{$ext}";
                        $thumbnail_path = "thumbnail_{$id}.{$ext}";
                        $compressed_path = "compressed_{$id}.{$ext}";

                        $data = array(
                            'original_path' => $subdir . $original_path,
                            'large_path' => $subdir . $large_path,
                            'small_path' => $subdir . $small_path,
                            'thumbnail_path' => $subdir . $thumbnail_path,
                            'compressed_path' => $subdir . $compressed_path
                        );
                        $modelImage->updateById($id, $data);
                        /////start added by walaa
                        $mongo_id = $disc_id . $x;
                        $original_path_mongo = "original_{$mongo_id}.{$ext}";
                        $large_path_mongo = "large_{$mongo_id}.{$ext}";
                        $small_path_mongo = "small_{$mongo_id}.{$ext}";
                        $thumbnail_path_mongo = "thumbnail_{$mongo_id}.{$ext}";
                        $compressed_path_mongo = "compressed_{$mongo_id}.{$ext}";

                        $thumbnails_paths[$mongo_id] = $subdir . $thumbnail_path_mongo;
                        $originals_paths[$mongo_id] = $subdir . $original_path_mongo;
                        $larges_paths[$mongo_id] = $subdir . $large_path_mongo;
                        $small_paths[$mongo_id] = $subdir . $small_path_mongo;
                        $compressed_paths[$mongo_id] = $subdir . $compressed_path_mongo;

                        $compressed_saved = copy($source, $fullDir . $compressed_path_mongo);
                        $image_saved = copy($source, $fullDir . $original_path_mongo);
                        ImageMagick::scale_image($source, $fullDir . $large_path_mongo, 510);
                        ImageMagick::scale_image($source, $fullDir . $small_path_mongo, 250);
                        ImageMagick::create_thumbnail($source, $fullDir . $thumbnail_path_mongo, 78, 64);
                        ImageMagick::convert($source, $fullDir . $compressed_path_mongo);
                        ImageMagick::compress_image($source, $fullDir . $compressed_path_mongo);
                        ////////////


                        $compressed_saved = copy($source, $fullDir . $compressed_path);
                        $image_saved2 = copy($source, $fullDir . $original_path);
                        ImageMagick::scale_image($source, $fullDir . $large_path, 510);
                        ImageMagick::scale_image($source, $fullDir . $small_path, 250);
                        ImageMagick::create_thumbnail($source, $fullDir . $thumbnail_path, 78, 64);
                        ImageMagick::convert($source, $fullDir . $compressed_path);
                        ImageMagick::compress_image($source, $fullDir . $compressed_path);
                    }


                    if ($image_saved) {
                        if (file_exists($source)) {
                            unlink($source);
                        }
                        if (file_exists($file)) {
                            unlink($file);
                        }
                    }
                }

                $modelContractorDiscussionMongo->updateDiscImages($disc_id, $thumbnails_paths, $originals_paths, $larges_paths, $small_paths, $compressed_paths);
            }


            if ((isset($discussionImages) && !empty($discussionImages))) {

                $image_group = 0;
                if ($count > 1) {
                    $maxGroup = max($modelImage->getMaxGroup());
                    $image_group = $maxGroup['group_id'] + 1;
                }

                foreach ($discussionImages as $key => $imageData) {
                    $y = $key + 1;

                    if (isset($imageData) && !empty($imageData)) {
                        $image = str_replace('data:image/png;base64,', '', $imageData);
                        $image = str_replace(' ', '+', $image);
                        $image = base64_decode($image);
                        $ext = 'png';
                        $fileName = "image_{$key}.png";
                        $file = $fullDir . $fileName;
                        $success_upload = file_put_contents($file, $image);



                        if ($success_upload) {
                            $successUpload = 1;
                            $data = array(
                                'user_ip' => ''
                                , 'created_by' => $loggedUser['user_id']
                                , 'image_types_id' => 0
                                , 'group_id' => $image_group
                                , 'user_role' => $loggedUser['role_id']
                            );



                            $id = $modelImage->insert($data);
                            $Itemdata = array(
                                'item_id' => $loggedUser['user_id'],
                                'service_id' => 0,
                                'discussion_id' => $success_status,
                                'image_id' => $id,
                                'type' => 'contractor_discussion',
                                'floor_id' => 0
                            );



                            $modelItemImage = new Model_ItemImage();
                            $Item_image_id = $modelItemImage->insert($Itemdata);
                            $original_path = "original_{$id}.png";
                            $large_path = "large_{$id}.png";
                            $small_path = "small_{$id}.png";
                            $thumbnail_path = "thumbnail_{$id}.png";
                            $compressed_path = "compressed_{$id}.png";

                            $data = array(
                                'original_path' => $subdir . $original_path,
                                'large_path' => $subdir . $large_path,
                                'small_path' => $subdir . $small_path,
                                'thumbnail_path' => $subdir . $thumbnail_path,
                                'compressed_path' => $subdir . $compressed_path
                            );

                            $modelImage->updateById($id, $data);
                            $compressed_saved = copy($file, $fullDir . $compressed_path);
                            $image_saved = copy($file, $fullDir . $original_path);
                            ImageMagick::scale_image($file, $fullDir . $large_path, 510);
                            ImageMagick::scale_image($file, $fullDir . $small_path, 250);
                            ImageMagick::create_thumbnail($file, $fullDir . $thumbnail_path, 78, 64);
                            ImageMagick::convert($file, $fullDir . $compressed_path);
                            ImageMagick::compress_image($file, $fullDir . $compressed_path);

                            /////start added by walaa
                            $mongo_id = $disc_id . $y;
                            $original_path_mongo = "original_{$mongo_id}.{$ext}";
                            $large_path_mongo = "large_{$mongo_id}.{$ext}";
                            $small_path_mongo = "small_{$mongo_id}.{$ext}";
                            $thumbnail_path_mongo = "thumbnail_{$mongo_id}.{$ext}";
                            $compressed_path_mongo = "compressed_{$mongo_id}.{$ext}";

                            $thumbnails_paths[$mongo_id] = $subdir . $thumbnail_path_mongo;
                            $originals_paths[$mongo_id] = $subdir . $original_path_mongo;
                            $larges_paths[$mongo_id] = $subdir . $large_path_mongo;
                            $small_paths[$mongo_id] = $subdir . $small_path_mongo;
                            $compressed_paths[$mongo_id] = $subdir . $compressed_path_mongo;

                            $compressed_saved = copy($file, $fullDir . $compressed_path_mongo);
                            $image_saved = copy($file, $fullDir . $original_path_mongo);
                            ImageMagick::scale_image($file, $fullDir . $large_path_mongo, 510);
                            ImageMagick::scale_image($file, $fullDir . $small_path_mongo, 250);
                            ImageMagick::create_thumbnail($file, $fullDir . $thumbnail_path_mongo, 78, 64);
                            ImageMagick::convert($file, $fullDir . $compressed_path_mongo);
                            ImageMagick::compress_image($file, $fullDir . $compressed_path_mongo);
                            ////////////


                            if (file_exists($file)) {
                                unlink($file);
                            }
                        }
                    }
                }
                $modelContractorDiscussionMongo->updateDiscImages($disc_id, $thumbnails_paths, $originals_paths, $larges_paths, $small_paths, $compressed_paths);
            }

            MobileNotification::notify(0, 'new contractor discussion', array('contractor_id' => $loggedUser['user_id'], 'discussion_id' => $disc_id));
        }

        //

        $data = array();
        $data['authrezed'] = 1;
        if ($success || $status_text) {
            $data['msg'] = 'Changed Successfully';
            $data['type'] = 'success';
        } else {
            $data['msg'] = 'No changes';
            $data['type'] = 'error';
        }

        echo json_encode($data);
        exit;
    }

    public function addEditContractorInsuranceAction() {

        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $insurance_policy_number = $this->request->getParam('insurance_policy_number');
        $insurance_policy_start = $this->request->getParam('insurance_policy_start');
        $insurance_policy_expiry = $this->request->getParam('insurance_policy_expiry');
        $insurance_listed_services_covered = $this->request->getParam('insurance_listed_services_covered');
        $insurance_type = $this->request->getParam('insurance_type');
        $attachCount = $this->request->getParam('attachCount');
        $insuranceFiles = $this->request->getParam('insurance_files');
        $id = $this->request->getParam('id');

        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];



          $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          $data['type'] = 'error';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {
          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */
        $this->checkAccessToken($accessToken);

        $loggedUser = CheckAuth::getLoggedUser();
        $contractorInfoObj = new Model_ContractorInfo();
        $attachmentObj = new Model_Attachment();
        $ContractorInsuranceAttachmentObj = new Model_ContractorInsuranceAttachment();
        $contractorInsuranceObj = new Model_ContractorInsurance();
        $contractorInfo = $contractorInfoObj->getByContractorId($loggedUser['user_id']);

        $data = array(
            'insurance_policy_number' => $insurance_policy_number,
            'insurance_policy_start' => strtotime($insurance_policy_start),
            'insurance_policy_expiry' => strtotime($insurance_policy_expiry),
            'insurance_listed_services_covered' => $insurance_listed_services_covered,
            'insurance_type' => $insurance_type,
            'contractor_info_id' => $contractorInfo['contractor_info_id'],
            'created' => time(),
            'created_by' => $loggedUser['user_id'],
        );

        if ($id) {
            $success = $contractorInsuranceObj->updateById($id, $data);
            $success = $id;
        } else {
            $success = $contractorInsuranceObj->insert($data);
        }



        $attchid = 0;
        if ((isset($insuranceFiles) && !empty($insuranceFiles))) {

            if ($id) {
                $Attachments = $contractorInsuranceObj->getAllAttachmentById($id);
                $counter = count($Attachments);
            } else {
                $counter = 0;
            }
            $dir = get_config('attachment') . '/';
            $subdir = date('Y/m/d/');
            $fullDir = $dir . $subdir;
            if (!is_dir($fullDir)) {
                mkdir($fullDir, 0777, true);
            }







            foreach ($insuranceFiles as $key => $imageData) {
                if (isset($imageData) && !empty($imageData)) {
                    $ext = 'png';
                    $image = str_replace('data:image/png;base64,', '', $imageData);
                    $image = str_replace(' ', '+', $image);
                    $image = base64_decode($image);
                    $tempFile = "image_{$key}." . $ext;
                    $file = $fullDir . $tempFile;
                    $successUpload = file_put_contents($file, $image);
                    $counter = $counter + 1;
                    if ($successUpload) {
                        $fileName = $success . '_' . $counter . '_' . 'insurance' . '.' . $ext;
                        $image_saved = copy($file, $fullDir . $fileName);
                        $size = filesize($fullDir . $fileName);
                        $type = mime_content_type($fullDir . $fileName);
                        $typeParts = explode("/", $type);
                        if ($typeParts[0] == 'image') {
                            $thumbName = $success . '_' . $counter . '_' . 'insurance_thumbnail' . '.' . $ext;
                            ImageMagick::create_thumbnail($file, $fullDir . $thumbName, 78, 64);
                        } else {
                            $thumbName = $success . '_' . $counter . '_' . 'insurance' . '.jpg';
                            ImageMagick::convertFile($file . '[0]', $fullDir . $thumbName);
                        }

                        $Attchparams = array(
                            'created_by' => $loggedUser['user_id']
                            , 'created' => time()
                            , 'size' => $size
                            , 'type' => $type
                            , 'path' => $fullDir . $fileName,
                            'file_name' => $fileName,
                            'thumbnail_file' => $fullDir . $thumbName
                        );

                        $attchid = $attachmentObj->insert($Attchparams);
                        $ContractorInsuranceAttachmentObj->insert(array('contractor_insurance_id' => $success, 'attachment_id' => $attchid));

                        if (file_exists($file)) {
                            unlink($file);
                        }
                    }
                }
            }
        }
        if ($attachCount) {
            if ($id) {
                $Attachments = $contractorInsuranceObj->getAllAttachmentById($id);
                $counter = count($Attachments);
            } else {
                $counter = 0;
            }
            for ($i = 0; $i < $attachCount; $i++) {
                $source = $_FILES['insuranceFile' . $i]['tmp_name'];
                $extension = $this->request->getParam('ext' . $i);
                $fileInfo = pathinfo($source);
                $ext = $extension;
                $dir = get_config('attachment') . '/';
                $subdir = date('Y/m/d/');
                $fullDir = $dir . $subdir;
                if (!is_dir($fullDir)) {
                    mkdir($fullDir, 0777, true);
                }

                $counter = $counter + 1;

                $fileName = $success . '_' . $counter . '.' . $ext;
                $image_saved = copy($source, $fullDir . $fileName);
                $size = filesize($fullDir . $fileName);

                $type = mime_content_type($fullDir . $fileName);
                $typeParts = explode("/", $type);
                if ($typeParts[0] == 'image') {
                    $thumbName = $success . '_thumbnail_' . $counter . '.' . $ext;
                    ImageMagick::create_thumbnail($source, $fullDir . $thumbName, 78, 64);
                } else {
                    $thumbName = $success . '_' . $counter . '.jpg';
                    ImageMagick::convertFile($source . '[0]', $fullDir . $thumbName);
                }
                $Attchparams = array(
                    'created_by' => $loggedUser['user_id']
                    , 'created' => time()
                    , 'size' => $size
                    , 'type' => $type
                    , 'path' => $fullDir . $fileName,
                    'file_name' => $fileName,
                    'thumbnail_file' => $fullDir . $thumbName
                );

                $attchid = $attachmentObj->insert($Attchparams);
                $ContractorInsuranceAttachmentObj->insert(array('contractor_insurance_id' => $success, 'attachment_id' => $attchid));
                if ($image_saved) {
                    if (file_exists($source)) {
                        unlink($source);
                    }
                }
            }
        }

        $insurance = $contractorInsuranceObj->getById($success);
        $modelAttachment = new Model_Attachment();
        $filter = array('type' => 'insurance', 'itemid' => $success, 'item_type' => 'image');
        $pager = null;
        $attachments = $modelAttachment->getAll('a.created desc', $pager, $filter);
        $insurance['attachments'] = $attachments;
        if ($success || $attchid) {
            $data['msg'] = 'Saved successfully';
            $data['type'] = 'success';
            $data['result'] = $insurance;
        } else {
            $data['msg'] = 'No Changes in Contractor Insurance';
            $data['type'] = 'error';
        }

        $data['authrezed'] = 1;
        echo json_encode($data);
        exit;
    }

    public function deleteContractorInsuranceAction() {


        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $id = $this->request->getParam('id', 0);

        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];



          $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          $data['type'] = 'error';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {
          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */

        $this->checkAccessToken($accessToken);
        $loggedUser = CheckAuth::getLoggedUser();
        $contractorInsuranceObj = new Model_ContractorInsurance();
        $modelAttachment = new Model_Attachment();
        $contractorInsurance = $contractorInsuranceObj->getById($id);
        if ($contractorInsurance) {
            $filter = array('type' => 'insurance', 'itemid' => $id);
            $pager = null;
            $attachments = $modelAttachment->getAll('a.created desc', $pager, $filter);
            if ($attachments) {
                foreach ($attachments as $attachment) {
                    $modelAttachment->updateById($attachment['attachment_id'], array('is_deleted' => '1'));
                }
            }

            $contractorInsuranceObj->deleteById($id);
            $data['msg'] = 'Insurance deleted successfully';
            $data['type'] = 'success';
        } else {
            $data['msg'] = 'Insurance not found';
            $data['type'] = 'error';
        }

        $data['authrezed'] = 1;
        echo json_encode($data);
        exit;
    }

    //////// Customer Webservices 

    public function customerLoginAction() {

        CheckAuth::logout();
        $modelBooking = new Model_Booking();
        $modelCustomer = new Model_Customer();
        $booking_num = $this->request->getParam('booking_num', 0);
        $customer_contact = $this->request->getParam('customer_contact', 0);
        $redirect_flag = $this->request->getParam('redirect_flag', 0);


        $booking = $modelBooking->getByNum($booking_num, false);
        $Customer = $modelCustomer->getByCustomerContactsAndId($customer_contact, $booking['customer_id']);

        if (isset($Customer) && !empty($Customer) && ($Customer['customer_id'] == $booking['customer_id'])) {

            $authRole_model = new Model_AuthRole();
            $auth = Zend_Auth::getInstance();
            $authStorge = $auth->getStorage();
            $role = $authRole_model->getRoleIdByName('Customer');
            $Customer['role_id'] = $role;
            $user_id = $Customer['customer_id'];
            $Customer['user_id'] = '';
            $Customer['username'] = $Customer['first_name'];
            $customer_obj = (object) $Customer;
            $authStorge->write($customer_obj);
            CheckAuth::redirectCustomer();
            $modelCustomerAccessToken = new Model_CustomerAccessToken();
            $accessToken = $modelCustomerAccessToken->getAccessToken($Customer['customer_id']);


            $loggedUser = CheckAuth::getLoggedUser();
            //$userId = $loggedUser['user_id'];
            //$result = array('user_id'=>$userId);


            $bookingData = $this->getBooking(false, array('booking_id' => $booking['booking_id']), 0, 'customer');
            $data['result'] = $bookingData;
            $data['authrezed'] = 1;
            $data['access_token'] = $accessToken;

            $router = Zend_Controller_Front::getInstance()->getRouter();
            $url = $router->assemble(array('id' => $booking['booking_id']), 'bookingView');
            $data['type'] = "success";
            $data['goTo'] = $url;
        } else {

            if ($redirect_flag) {
                $data['msg'] = "Invalid customer contact or wrong booking number, please try again";
                $data['type'] = "error";
            } else {
                $data['result'] = array();
                $data['authrezed'] = 0;
            }
        }


        header('Content-Type: application/json');
        echo json_encode($data);
        exit;
    }

    public function getCustomerTypesAction() {

        header('Content-Type: application/json');

        $modelCustomerType = new Model_CustomerType();
        $customerTypes = $modelCustomerType->getCustomerTypeAsArray(false);
        $data['result'] = $customerTypes;

        echo json_encode($data);
        exit;
    }

    public function getCustomerRateAction() {
        $model_ratingTag = new Model_RatingTag();
        $model_contractorRate = new Model_ContractorRate();
        $model_ratingInfo = new Model_RatingInfo();
        $ratingTag = array();

        $item_id = $this->request->getParam('item_id');
        $item_type = $this->request->getParam('type');
        $accessToken = $this->request->getParam('access_token', 0);
        $user_role = $this->request->getParam('user_role', 'customer');
        $customer_id = $this->request->getParam('customer_id', 0);


        $this->checkAccessToken($accessToken, $user_role, $customer_id);
        /* $loggedUser = CheckAuth::getLoggedUser();
          $result = array('authrezed' => 0);
          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken, $user_role))) {
          $result['msg'] = 'wrong access token';
          echo json_encode($result);
          exit;
          }

          if (empty($loggedUser)) {
          //open new session
          $authrezed = $this->openNewSession($accessToken, $user_role, $customer_id);

          if (!$authrezed) {
          echo json_encode($result);
          exit;
          }
          } */


        $loggedUser = CheckAuth::getLoggedUser();

//       if ($item_type == 'booking') {

        $model_contractorServiceBooking = new Model_ContractorServiceBooking();
        $contractors = $model_contractorServiceBooking->getContractorsDataByBookingId($item_id);
//        }
        $modelUser = new Model_User();

        $x = array();
        foreach ($contractors as $key => $contractor) {

            $filters['with_join'] = 1;
            $filters['contractor_id'] = $contractor['contractor_id'];
            $filters['item_id'] = $item_id;
            $filters['with_image_attachment_join'] = 1;

            $customer_rating = $model_ratingTag->getAll2($filters);
            foreach ($customer_rating as &$rate) {
                $ratingInfo = $model_ratingInfo->getByContractorIdAndBookingId($contractor['contractor_id'], $item_id);
                $rate['comment'] = $ratingInfo['desc'] ? $ratingInfo['desc'] : '';

                $rate['contractor_id'] = $contractor['contractor_id'];
                $rate['contractor_name'] = $contractor['username'];

                if ($rate['rate'] == null) {
                    $rate['rate'] = 0;
                }
                //$rate['tag_image'] = $_SERVER['HTTP_HOST'] . '/uploads/user_pic/thumb_medium/' . $rate['tag_image'];
                $user = $modelUser->getById($rate['created_by']);
                if (!empty($user['display_name'])) {
                    $rate['created_by'] = $user['display_name'];
                } else {
                    $rate['created_by'] = $user['username'];
                }
            }


            $x["tags"] = $customer_rating;
            $ratingTag[] = $x;
        }
        $data['customer_rating'] = $ratingTag;
        $data['authrezed'] = 1;

        echo json_encode($data);
        exit;
    }

    public function saveCustomerRateAction() {

        $model_contractorRate = new Model_ContractorRate();
        $modelAuthRole = new Model_AuthRole();

        $rate = $this->request->getParam('rate');
        $tag_id = $this->request->getParam('tag_id');
        $contractor_id = $this->request->getParam('contractor_id');
        $item_id = $this->request->getParam('item_id');
        $item_type = $this->request->getParam('type', 'booking');
        $rated_by = $this->request->getParam('customer_id');
        $accessToken = $this->request->getParam('access_token', 0);
        $user_role = $this->request->getParam('user_role', 'customer');
        $customer_id = $rated_by;
        $comment = $this->request->getParam('comment');
        $rating_tags = $this->request->getParam('rating_tags');

        $os = $this->request->getParam('os');


        $this->checkAccessToken($accessToken, $user_role, $customer_id);
        /* $loggedUser = CheckAuth::getLoggedUser();
          $result = array('authrezed' => 0);
          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken, $user_role))) {
          $result['msg'] = 'wrong access token';
          echo json_encode($result);
          exit;
          }


          if (empty($loggedUser)) {
          //open new session
          $authrezed = $this->openNewSession($accessToken, $user_role, $customer_id);

          if (!$authrezed) {
          echo json_encode($result);
          exit;
          }
          } */


        $loggedUser = CheckAuth::getLoggedUser();

        if ($item_type == 'booking') {
            $modelBooking = new Model_Booking();
            //get customer_id of the booking to make sure this customer for this booking
            $booking = $modelBooking->getById($item_id);
            if ($rated_by == 0) {
                $retData['result'] = array("success" => 0, "msg" => "invalid customer id");
                echo json_encode($retData);
                exit;
            }

            if ($booking['customer_id'] != $rated_by) {
                $retData['result'] = array("success" => 0, "msg" => "this customer can't rate this booking");
                echo json_encode($retData);
                exit;
            }
        }


        $role = $modelAuthRole->getRoleIdByName('customer');
        if (!empty($role)) {
            $role_id = $role['role_id'];
        } else {
            $role_id = 0;
        }

        if (isset($os) && $os == 'android') {
            $success = array();
            if (!empty($rating_tags)) {
                $rating_tags = json_decode($rating_tags);
                foreach ($rating_tags as $key => $rating_tag) {
                    $ratingTag = array();
                    foreach ($rating_tag as $k => $val) {
                        $ratingTag[$k] = $val;
                    }
                    $rate_data = array(
                        'contractor_id' => $contractor_id,
                        'rated_by' => $rated_by,
                        'rate' => $ratingTag['rate'],
                        'user_role' => $role_id,
                        'created' => time(),
                        'rating_tag_id' => $ratingTag['tag_id'],
                        'item_id' => $item_id,
                        'item_type' => $item_type
                    );

                    $previousRate = $model_contractorRate->getByContractorIdAndRatedByAndItemIdAndTagId($contractor_id, $rated_by, $item_id, $ratingTag['tag_id']);
                    if (!empty($previousRate)) {

                        $success[] = $model_contractorRate->updateById($previousRate[0]['contractor_rate_id'], $rate_data);
                    } else {

                        $success[] = $model_contractorRate->insert($rate_data);
                    }
                    /* if ($success) {
                      /*$result = $model_contractorRate->getByContractorIdAndRatedByAndItemIdAndTagId($contractor_id, $rated_by, $item_id);

                      foreach ($result as $key => $data) {
                      $rate_db[] = $data;
                      $sum_rates[] = $data['rate'];
                      }
                      $rate_times = count($rate_db);
                      $sum_rates = array_sum($sum_rates);
                      $total = $sum_rates / $rate_times;
                      //                $rate_bg = (($rate_value) / 5) * 100;
                      $retData[$key]['result'] = array("success" => 1, "rate" => $ratingTag['rate'], 'total' => floor($total));
                      $retData['result'] = array('type' => 'success', 'msg' => 'Saved successfully');
                      } else {
                      $retData['result'] = array('type' => 'error', 'msg' => 'No changes in contractor rate');
                      } */
                }
                if (!empty($comment)) {
                    $model_ratingInfo = new Model_RatingInfo();
                    $successComment = 0;
                    $rateInfo = array(
                        'contractor_id' => $contractor_id,
                        'item_id' => $item_id,
                        'desc' => $comment
                    );
                    $successComment = $model_ratingInfo->insert($rateInfo);
                    $retData['successComment'] = $successComment;
                }

                if (!in_array(0, $success)) {
                    $retData['result'] = array('type' => 'success', 'msg' => 'Saved successfully');
                } else {
                    $retData['result'] = array('type' => 'error', 'msg' => 'No changes in contractor rate');
                }
            }
            $retData['authrezed'] = 1;
            echo json_encode($retData);
            exit;
        }


        $rate_data = array(
            'contractor_id' => $contractor_id,
            'rated_by' => $rated_by,
            'rate' => $rate,
            'user_role' => $role_id,
            'created' => time(),
            'rating_tag_id' => $tag_id,
            'item_id' => $item_id,
            'item_type' => $item_type
        );

        $previousRate = $model_contractorRate->getByContractorIdAndRatedByAndItemIdAndTagId($contractor_id, $rated_by, $item_id, $tag_id);
        if (!empty($previousRate)) {

            $success = $model_contractorRate->updateById($previousRate[0]['contractor_rate_id'], $rate_data);
        } else {

            $success = $model_contractorRate->insert($rate_data);
        }
        if ($success) {
            $result = $model_contractorRate->getByContractorIdAndRatedByAndItemIdAndTagId($contractor_id, $rated_by, $item_id);

            foreach ($result as $key => $data) {
                $rate_db[] = $data;
                $sum_rates[] = $data['rate'];
            }
            $rate_times = count($rate_db);
            $sum_rates = array_sum($sum_rates);
            $total = $sum_rates / $rate_times;
//                $rate_bg = (($rate_value) / 5) * 100;
            $retData['result'] = array("success" => 1, "rate" => $rate, 'total' => floor($total));
        } else {
            $retData['result'] = array("success" => 0);
        }

        $retData['authrezed'] = 1;
        echo json_encode($retData);
        exit;
    }

    public function editCustomerInfoAction() {


        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $customer_id = $this->request->getParam('customer_id');
        $booking_id = $this->request->getParam('booking_id');
        //customer info 
        $customerTypeId = $this->request->getParam('customer_type_id');
        $businessName = $this->request->getParam('customer_business_name');
        $title = $this->request->getParam('title');
        $firstName = $this->request->getParam('first_name');
        $lastName = $this->request->getParam('last_name');
        $title2 = $this->request->getParam('title2');
        $firstName2 = $this->request->getParam('first_name2');
        $lastName2 = $this->request->getParam('last_name2');
        $title3 = $this->request->getParam('title3');
        $firstName3 = $this->request->getParam('first_name3');
        $lastName3 = $this->request->getParam('last_name3');
        $email1 = $this->request->getParam('email1');
        $email2 = $this->request->getParam('email2');
        $email3 = $this->request->getParam('email3');
        $mobile1 = $this->request->getParam('mobile1');
        $mobile2 = $this->request->getParam('mobile2');
        $mobile3 = $this->request->getParam('mobile3');
        $phone1 = $this->request->getParam('phone1');
        $phone2 = $this->request->getParam('phone2');
        $phone3 = $this->request->getParam('phone3');
        $fax = $this->request->getParam('fax');
        //billing Address 
        $customerUnitLotNumber = $this->request->getParam('customer_unit_lot_number');
        $customerStreetNumber = $this->request->getParam('customer_street_number');
        $customerStreetAddress = $this->request->getParam('customer_street_address');
        $customerSuburb = $this->request->getParam('customer_suburb');
        $customerState = $this->request->getParam('customer_state');
        $customerPostcode = $this->request->getParam('customer_postcode');
        $customerPo_box = $this->request->getParam('customer_po_box');
        $customerCityId = $this->request->getParam('customer_city_id');

        // booking Address

        $bookingStreetAddress = $this->request->getParam('booking_street_address', '');
        $bookingStreetNumber = $this->request->getParam('booking_street_number', '');
        $bookingSuburb = $this->request->getParam('booking_suburb', '');
        $bookingUnitLotNumber = $this->request->getParam('booking_unit_lot_number', '');
        $bookingPostcode = $this->request->getParam('booking_postcode', '');



        $this->checkAccessToken($accessToken, 'customer', $customer_id);
        /* $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);
          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken, 'customer'))) {
          $data['msg'] = 'wrong access token';
          $data['type'] = 'error';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {
          $authrezed = $this->openNewSession($accessToken, 'customer', $customer_id);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */


        $modelCustomer = new Model_Customer();
        $modelBookingAddress = new Model_BookingAddress();
        $modelBookingAddressTemp = new Model_BookingAddressTemp();
        $modelCustomerCommercialInfo = new Model_CustomerCommercialInfo();
        $modelBooking = new Model_Booking();
        $customer = $modelCustomer->getById($customer_id);
        if (!$customer) {
            $data['msg'] = 'Customer not found';
            $data['type'] = 'error';
            echo json_encode($data);
            exit;
        }

        if (get_config('remove_white_spacing')) {
            $mobile1 = preparer_number($mobile1);
            $mobile2 = preparer_number($mobile2);
            $mobile3 = preparer_number($mobile3);
            $phone1 = preparer_number($phone1);
            $phone2 = preparer_number($phone2);
            $phone3 = preparer_number($phone3);
        }

        $db_params = array(
            'title' => $title,
            'first_name' => $firstName,
            'last_name' => $lastName,
            'title2' => $title2,
            'first_name2' => $firstName2,
            'last_name2' => $lastName2,
            'title3' => $title3,
            'first_name3' => $firstName3,
            'last_name3' => $lastName3,
            'city_id' => $customerCityId,
            'email1' => $email1,
            'email2' => $email2,
            'email3' => $email3,
            'mobile1' => $mobile1,
            'mobile2' => $mobile2,
            'mobile3' => $mobile3,
            'phone1' => $phone1,
            'phone2' => $phone2,
            'phone3' => $phone3,
            'fax' => $fax,
            'customer_type_id' => $customerTypeId,
            'unit_lot_number' => $customerUnitLotNumber,
            'street_number' => $customerStreetNumber,
            'street_address' => $customerStreetAddress,
            'suburb' => $customerSuburb,
            'state' => $customerState,
            'postcode' => $customerPostcode,
            'po_box' => $customerPo_box,
            'full_name_search' => $title . '.' . $firstName . ' ' . $lastName
        );


        $success = $modelCustomer->updateById($customer_id, $db_params);
        $modelCustomerCommercialInfo->setCustomerCommercialInfo($customer_id, $customerTypeId, $businessName);


        // save Booking Address
        $orginalAddress = $modelBookingAddress->getByBookingIdWithOutLatAndLon($booking_id);
        $newAddress = array();
        $newAddress['unit_lot_number'] = trim($bookingUnitLotNumber);
        $newAddress['street_number'] = trim($bookingStreetNumber);
        $newAddress['street_address'] = trim($bookingStreetAddress);
        $newAddress['suburb'] = trim($bookingSuburb);
        $newAddress['state'] = trim($orginalAddress['state']);
        $newAddress['postcode'] = trim($bookingPostcode);
        $newAddress['po_box'] = trim($orginalAddress['po_box']);
        $newAddress['booking_id'] = (int) $booking_id;

        $loggedUser = CheckAuth::getLoggedUser();
        $saveAddress = 0;

        if ($orginalAddress != $newAddress) {
            $db_params = array();
            $db_params['is_change'] = 1;
            $update = $modelBooking->updateById($booking_id, $db_params);
            $bookingAddressTemp = $modelBookingAddressTemp->getByBookingId($booking_id);
            $modelBookingAddress = new Model_BookingAddress();
            $geocode = $modelBookingAddress->getLatAndLon($newAddress);
            $newAddress['lat'] = $geocode['lat'] ? $geocode['lat'] : 0;
            $newAddress['lon'] = $geocode['lon'] ? $geocode['lon'] : 0;
            $newAddress['user_id'] = $loggedUser['customer_id'];
            $newAddress['user_role'] = $loggedUser['role_id'];

            if ($bookingAddressTemp) {
                $saveAddress = $modelBookingAddressTemp->updateById($bookingAddressTemp['booking_address_id'], $newAddress);
            } else {
                $saveAddress = $modelBookingAddressTemp->insert($newAddress);
            }
        }

        if ($success || $saveAddress) {
            $data['result'] = $this->getBooking(false, array('booking_id' => $booking_id), 0, 'customer');
            $data['msg'] = 'Customer info saved successfully';
            $data['type'] = 'success';
        } else {
            $data['msg'] = 'No changes in customer info';
            $data['type'] = 'error';
        }

        $data['authrezed'] = 1;
        echo json_encode($data);
        exit;
    }

    public function saveContractorLocationAction() {

        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $lat = $this->request->getParam('lat', 0);
        $lon = $this->request->getParam('lon', 0);
        $booking_id = $this->request->getParam('booking_id', 0);

        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];


          $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          $data['type'] = 'error';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {
          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */

        $this->checkAccessToken($accessToken);

        $loggedUser = CheckAuth::getLoggedUser();

        $db_params = array(
            'lat' => $lat,
            'lon' => $lon,
            'created' => time(),
            'user_id' => $loggedUser['user_id'],
            'booking_id' => $booking_id
        );

        $modelContractorLocation = new Model_ContractorLocation();
        $success = $modelContractorLocation->insert($db_params);

        if ($success) {
            $data['msg'] = 'location saved successfully';
            $data['type'] = 'success';
        } else {
            $data['msg'] = 'error , please try again';
            $data['type'] = 'error';
        }

        $data['authrezed'] = 1;
        echo json_encode($data);
        exit;
    }

    public function getContractorLocationAction() {

        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $contractorIds = $this->request->getParam('contractor_id', 0);
        $contractorIds = json_decode($contractorIds);
        $booking_id = $this->request->getParam('booking_id', 0);
        $customer_id = $this->request->getParam('customer_id', 0);


        $this->checkAccessToken($accessToken, 'customer', $customer_id);
        /* $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);
          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken, 'customer'))) {
          $data['msg'] = 'wrong access token';
          $data['type'] = 'error';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {
          $authrezed = $this->openNewSession($accessToken, 'customer', $customer_id);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */

        $loggedUser = CheckAuth::getLoggedUser();

        $modelContractorLocation = new Model_ContractorLocation();

        $contractorLocation = $modelContractorLocation->getByContractorId($contractorIds, $booking_id);
        if ($contractorLocation) {
            $data['result'] = $contractorLocation;
            $data['msg'] = 'success';
            $data['type'] = 'success';
        } else {
            $data['msg'] = 'Unknown Technician Location';
            $data['type'] = 'error';
        }



        $data['authrezed'] = 1;
        echo json_encode($data);
        exit;
    }

    public function confirmBookingByCustomerAction() {

        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $comment = $this->request->getParam('comment');
        $type = $this->request->getParam('type', 'customer');
        $booking_id = $this->request->getParam('booking_id', 0);
        $customer_id = $this->request->getParam('customer_id', 0);


        $this->checkAccessToken($accessToken);
        /* $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);
          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken, 'customer'))) {
          $data['msg'] = 'wrong access token';
          $data['type'] = 'error';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {
          $authrezed = $this->openNewSession($accessToken, 'customer', $customer_id);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */



        $loggedUser = CheckAuth::getLoggedUser();
        $params = array(
            'booking_id' => $booking_id,
            'user_id' => $loggedUser['customer_id'],
            'created' => time(),
            'comment' => $comment,
            'type' => $type,
            'user_role' => $loggedUser['role_id']
        );

        $modelBookingReminder = new Model_BookingReminder();
        $success = $modelBookingReminder->insert($params);
        if ($success) {
            MobileNotification::notify($booking_id, 'booking confirmation');
            $data['msg'] = 'Confirmation Done';
            $data['result'] = $success;
            $data['type'] = 'success';
        } else {
            $data['msg'] = 'Error , please try again';
            $data['type'] = 'error';
        }



        $data['authrezed'] = 1;
        echo json_encode($data);
        exit;
    }

    public function cancelConfirmBookingByCustomerAction() {

        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $reminderId = $this->request->getParam('reminder_id');
        $customer_id = $this->request->getParam('customer_id', 0);


        $this->checkAccessToken($accessToken);
        /* $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);
          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken, 'customer'))) {
          $data['msg'] = 'wrong access token';
          $data['type'] = 'error';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {
          $authrezed = $this->openNewSession($accessToken, 'customer', $customer_id);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */



        $loggedUser = CheckAuth::getLoggedUser();
        $modelBookingReminder = new Model_BookingReminder();
        $success = $modelBookingReminder->deleteById($reminderId);

        if ($success) {
            $data['msg'] = 'Confirmation cancelled successfully';
            $data['type'] = 'success';
        } else {
            $data['msg'] = 'Error , please try again';
            $data['type'] = 'error';
        }



        $data['authrezed'] = 1;
        echo json_encode($data);
        exit;
    }

    public function cancelBookingByCustomerAction() {

        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $bookingId = $this->request->getParam('booking_id');
        $customer_id = $this->request->getParam('customer_id', 0);
        //$status_id = $this->request->getParam('status_id',0);
        $bookingId = $this->request->getParam('booking_id', 0);
        $why = $this->request->getParam('why', '');


        $this->checkAccessToken($accessToken, 'customer', $customer_id);
        /* $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);
          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken, 'customer'))) {
          $data['msg'] = 'wrong access token';
          $data['type'] = 'error';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {
          $authrezed = $this->openNewSession($accessToken, 'customer', $customer_id);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */



        $loggedUser = CheckAuth::getLoggedUser();
        $modelBooking = new Model_Booking();
        $modelBookingStatus = new Model_BookingStatus();
        $status = $modelBookingStatus->getByStatusName('CANCELLED');
        $statusId = $status['booking_status_id'];


        // delete From calendar 
        $deleteGoogleCalender = $modelBookingStatus->getDeleteGoogleCalender();
        if (in_array($statusId, $deleteGoogleCalender)) {
            $modelGoogleCalendarEvent = new Model_GoogleCalendarEvent();
            $modelGoogleCalendarEvent->deleteGoogleCalendarEventByBookingId($bookingId);
        }

        $Updated = $modelBooking->updateById($bookingId, array('status_id' => $statusId, 'is_change' => 1));
        $this->saveWhyStatus(array(), $bookingId, $statusId, true, 'customer');



        if ($Updated['success']) {
            $data['msg'] = 'status updated successfully';
            $data['type'] = 'success';
        } else {
            $data['msg'] = 'No changes in booking status';
            $data['type'] = 'error';
        }




        $data['authrezed'] = 1;
        echo json_encode($data);
        exit;
    }

    public function getWeatherForecastAction() {

        $modelWeather = new Model_WeatherStatus();
        $weather = $modelWeather->getAllFromDB();


        $data['weather'] = $weather;
        echo json_encode($data);
        exit;
    }

    public function addComplaintByCustomerAction() {

        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $type = $this->request->getParam('type', 'customer');
        $customer_id = $this->request->getParam('customer_id', 0);


        $complaintTypeId = $this->request->getParam('complaint_type_id');
        $comment = $this->request->getParam('comment');
        $isToFollow = $this->request->getParam('to_follow', 0);
        $toFollow = $this->request->getParam('to_follow_date', 0);
        $bookingId = $this->request->getParam('booking_id', 0);


        $this->checkAccessToken($accessToken, 'customer', $customer_id);

        /* $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);
          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken, 'customer'))) {
          $data['msg'] = 'wrong access token';
          $data['type'] = 'error';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {
          $authrezed = $this->openNewSession($accessToken, 'customer', $customer_id);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */

        $modelComplaint = new Model_Complaint();
        $modelCustomer = new Model_Customer();
        $customer = $modelCustomer->getById($customer_id);
        $modelComplaintLog = new Model_ComplaintLog();
        $loggedUser = CheckAuth::getLoggedUser();
        $company_id = $customer['company_id'];

        $params = array(
            'complaint_type_id' => $complaintTypeId,
            'comment' => $comment,
            'booking_id' => $bookingId,
            'user_id' => $loggedUser['user_id'],
            'created' => time(),
            'company_id' => $company_id,
            'to_follow' => $toFollow,
            'is_to_follow' => $isToFollow
        );



        $newComplaintId = $modelComplaint->insert($params);
        $modelComplaintLog->insert($params);
        //$modelComplaint->sendComplaintAsEmailToContractor($newComplaintId);
        //$goToUrl = $this->router->assemble(array('id' => $newComplaintId), 'complaintView');
        //$url = $this->router->assemble(array('id' => $newComplaintId), 'sendComplaintAcknowledgementAsEmail');

        if ($newComplaintId) {
            //MobileNotification::notify($bookingId, 'new complaint');
            $modelUpdateFullTextSearch = new Model_UpdateFullTextSearch();
            $modelUpdateFullTextSearch->UpdateFullTextSearchByTypeAndTypeId('complaint', $newComplaintId);

            $data['msg'] = 'Complaint Added Successfully';
            $data['type'] = 'success';
        } else {
            $data['msg'] = 'Error in Adding Complaint';
            $data['type'] = 'error';
        }


        $data['authrezed'] = 1;
        echo json_encode($data);
        exit;
    }

    public function checkPaymentAction() {

        header('Content-Type: application/json');
        $bookingId = $this->request->getParam('booking_id', 0);
        $accessToken = $this->request->getParam('access_token', 0);
        $user_role = $this->request->getParam('user_role', 'contractor');
        $customer_id = $this->request->getParam('customer_id', 0);
        $modelBooking = new Model_Booking();
        $modelPayment = new Model_Payment();

        $this->checkAccessToken($accessToken, $user_role, $customer_id);

        /* $loggedUser = CheckAuth::getLoggedUser();
          if ($user_role == 'contractor') {
          $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];
          }
          $data = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken, $user_role))) {
          $data['msg'] = 'wrong access token';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {
          //open new session
          $authrezed = $this->openNewSession($accessToken, $user_role, $customer_id);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */

        $booking = $modelBooking->getById($bookingId);
        $data = array();

        $allPayment = $modelPayment->getTotalAmount(array('booking_id' => $booking['booking_id'], 'is_approved' => 'all'));
        $totalAmountDetails = $modelBooking->getTotalAmountBookingDetails($bookingId);

        if ($allPayment >= $totalAmountDetails['total_amount']) {
            $data['msg'] = "Invoice paid in full already, are you sure you want to continue?";
            $data['type'] = "error";
            echo json_encode($data);
            exit;
        }

        $data['msg'] = "success";
        $data['type'] = "success";
        echo json_encode($data);
        exit;
    }

    /* public function contractorDiscussionForMobileAction() {


      header('Content-Type: application/json');

      $accessToken = $this->request->getParam('access_token', 0);

      $loggedUser = CheckAuth::getLoggedUser();
      $modelIosUser = new Model_IosUser();
      $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
      $this->iosLoggedUser = $user['id'];

      $data = array('authrezed' => 0);
      if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
      $data['msg'] = 'wrong access token';
      echo json_encode($data);
      exit;
      }

      if (empty($loggedUser)) {
      //open new session
      $authrezed = $this->openNewSession($accessToken);
      if (!$authrezed) {
      echo json_encode($data);
      exit;
      }
      }
      $data['authrezed'] = 1;

      $loggedUser = CheckAuth::getLoggedUser();
      $contractor_id = $loggedUser['user_id'];
      //$contractor_id = 8;
      //
      // load model
      //
      $modelContractorDiscussion = new Model_ContractorDiscussion();
      $modelContractorInfo = new Model_ContractorInfo();
      $modelImage = new Model_Image();
      $modelUser = new Model_User();

      /* if (!$modelBooking->checkIfCanSeeBookingWithOutTimePeriod($bookingId)) {
      $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this Booking"));
      $this->_redirect($this->router->assemble(array(), 'booking'));
      } /
      $contractor = $modelUser->getById($contractor_id);
      $this->view->contractor = $contractor;

      if (!$contractor) {
      $data['msg'] = 'Contractor is not exist';
      echo json_encode($data);
      exit;
      }

      /* $ContractorDiscussions = $modelContractorDiscussion->getByContractorId($contractor_id);

      foreach ($ContractorDiscussions as $key=>&$ContractorDiscussion) {
      $user = $modelUser->getById($ContractorDiscussion['user_id']);
      $contractorImage = $modelImage->getContractorDiscussionImages($contractor_id,$ContractorDiscussion['discussion_id']);
      $contractorGroupImages = $modelImage->getContractorDiscussionImages($contractor_id,$ContractorDiscussion['discussion_id'],1);
      $ContractorDiscussion['images'] = $contractorGroupImages;
      $ContractorDiscussion['thumbnail_path'] = isset($contractorImage[0]['thumbnail_path']) ? $contractorImage[0]['thumbnail_path'] : '';
      $user = $modelUser->getById($ContractorDiscussion['user_id']);
      $ContractorDiscussion['username'] = ucwords($user['username']);
      $ContractorDiscussion['avatar'] = $_SERVER['HTTP_HOST'] . '/uploads/user_pic/thumb_medium/' . $user['avatar'];
      }
      $data['ContractorDiscussions'] = $ContractorDiscussions;
      /

      $modelContractorDiscussionMongo = new Model_ContractorDiscussionMongo();
      $contractorDiscussions = $modelContractorDiscussionMongo->getByContractorId($contractor_id, 1);
      if($contractorDiscussions){
      foreach($contractorDiscussions as $key => $row )
      $contractorDiscussions2[] = $row;

      foreach($contractorDiscussions2 as &$discussion){
      foreach($discussion['_id'] as $k => $v){
      //$discussion['discussion_id'] = $v;
      $discussion['discussion_id'] = 5;
      $discussion['_id'] = $v;
      }


      $user = $modelUser->getById($discussion['user_id']);
      if(count($discussion['thumbnail_images']) > 1){
      $discussion['images'] = $discussion['thumbnail_images'];
      $discussion['thumbnail_path'] = "";
      }else if(count($discussion['thumbnail_images']) == 1){
      $discussion['images'] = array();

      $discussion['thumbnail_path'] = $discussion['thumbnail_images'];
      }else{
      $discussion['thumbnail_path'] = '';
      $discussion['images'] = array();
      }
      $discussion['username'] = ucwords($user['username']);
      $discussion['avatar'] = $_SERVER['HTTP_HOST'] . '/uploads/user_pic/thumb_medium/' . $user['avatar'];
      $discussion['seen_flag'] = 0;
      $discussion['images'] = array();
      $discussion['thumbnail_path'] = '';
      }

      }
      $data['ContractorDiscussions'] = $contractorDiscussions2;





      echo json_encode($data);
      exit;
      } */

    public function contractorDiscussionForMobileAction() {


        header('Content-Type: application/json');

        $accessToken = $this->request->getParam('access_token', 0);
        $user_role = $this->request->getParam('user_role', 'contractor');
        $customer_id = $this->request->getParam('customer_id', 0);
        $this->checkAccessToken($accessToken, $user_role, $customer_id);
        /* $loggedUser = CheckAuth::getLoggedUser();
          if ($user_role == 'contractor') {
          $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];
          }
          $data = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken, $user_role))) {
          $data['msg'] = 'wrong access token';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {
          //open new session
          $authrezed = $this->openNewSession($accessToken, $user_role, $customer_id);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */

        $data['authrezed'] = 1;

        $loggedUser = CheckAuth::getLoggedUser();
        $contractor_id = $loggedUser['user_id'];
        //$contractor_id = 8;
        //
        // load model
        //
        $modelContractorDiscussionMongo = new Model_ContractorDiscussionMongo();
        $modelContractorDiscussion = new Model_ContractorDiscussion();
        $modelContractorInfo = new Model_ContractorInfo();
        $modelImage = new Model_Image();
        $modelUser = new Model_User();

        /* if (!$modelBooking->checkIfCanSeeBookingWithOutTimePeriod($bookingId)) {
          $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this Booking"));
          $this->_redirect($this->router->assemble(array(), 'booking'));
          } */
        $contractor = $modelUser->getById($contractor_id);
        $this->view->contractor = $contractor;

        if (!$contractor) {
            $data['msg'] = 'Contractor is not exist';
            echo json_encode($data);
            exit;
        }
        $filters = array();
        $lastFourLargeImages = array();
        $lastFourThumbnailImages = array();
        $imageAsArray = array();

        $contractorDiscussions2 = array();
        $contractorDiscussions = $modelContractorDiscussionMongo->getByContractorId($contractor_id, 1);
        if ($contractorDiscussions) {
            foreach ($contractorDiscussions as $key => $row)
                $contractorDiscussions2[] = $row;

            foreach ($contractorDiscussions2 as &$discussion) {
                $lastFourImagesAfterProcessing = array();
                $user = $modelUser->getById($discussion['user_id']);
                if (count($discussion['large_images'] > 4)) {
                    $lastFourLargeImages = array_slice($discussion['large_images'], 0, 4, true);
                    $lastFourThumbnailImages = array_slice($discussion['thumbnail_images'], 0, 4, true);
                    //$discussion['images'] = $images;
                } else {
                    $lastFourLargeImages = $discussion['large_images'];
                    $lastFourThumbnailImages = $discussion['thumbnail_images'];
                    //$discussion['images'] = $discussion['thumbnail_images'];
                }
                foreach ($lastFourLargeImages as $k => $img) {
                    $imageAsArray['large_path'] = $img;
                    $imageAsArray['thumbnail_path'] = $lastFourThumbnailImages[$k];
                    $lastFourImagesAfterProcessing[] = $imageAsArray;
                }
                $discussion['images'] = $lastFourImagesAfterProcessing;

                $discussion['username'] = ucwords($user['username']);
                $discussion['avatar'] = $_SERVER['HTTP_HOST'] . '/uploads/user_pic/thumb_medium/' . $user['avatar'];
                $discussion['seen_flag'] = 0;
                $discussion['discussion_id'] = 1;
                //foreach($discussion['_id'] as $k => $v)
                $discussion['_id'] = $discussion['_id']->{'$id'};
                $discussion['is_audio'] = isset($discussion['audio_path']) && !empty($discussion['audio_path']) ? 1 : 0;
            }
        }

        /* $ContractorDiscussions = $modelContractorDiscussion->getByContractorId($contractor_id);

          foreach ($ContractorDiscussions as $key=>&$ContractorDiscussion) {
          $user = $modelUser->getById($ContractorDiscussion['user_id']);
          $contractorImage = $modelImage->getContractorDiscussionImages($contractor_id,$ContractorDiscussion['discussion_id']);
          $contractorGroupImages = $modelImage->getContractorDiscussionImages($contractor_id,$ContractorDiscussion['discussion_id'],1);
          var_dump($contractorGroupImages);
          $ContractorDiscussion['images'] = $contractorGroupImages;
          $ContractorDiscussion['thumbnail_path'] = isset($contractorImage[0]['thumbnail_path']) ? $contractorImage[0]['thumbnail_path'] : '';
          $user = $modelUser->getById($ContractorDiscussion['user_id']);
          $ContractorDiscussion['username'] = ucwords($user['username']);
          $ContractorDiscussion['avatar'] = $_SERVER['HTTP_HOST'] . '/uploads/user_pic/thumb_medium/' . $user['avatar'];
          }
          exit; */

        $data['ContractorDiscussions'] = $contractorDiscussions2;
        echo json_encode($data);
        exit;
    }

    public function contractorDiscussionForMobileTemp() {


        header('Content-Type: application/json');

        $accessToken = $this->request->getParam('access_token', 0);
        $user_role = $this->request->getParam('user_role', 'contractor');
        $customer_id = $this->request->getParam('customer_id', 0);
        $this->checkAccessToken($accessToken, $user_role, $customer_id);
        /* $loggedUser = CheckAuth::getLoggedUser();
          if ($user_role == 'contractor') {
          $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];
          }
          $data = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken, $user_role))) {
          $data['msg'] = 'wrong access token';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {
          //open new session
          $authrezed = $this->openNewSession($accessToken, $user_role, $customer_id);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */

        $data['authrezed'] = 1;

        $loggedUser = CheckAuth::getLoggedUser();
        $contractor_id = $loggedUser['user_id'];
        //$contractor_id = 8;
        //
        // load model
        //
		$modelContractorDiscussionMongo = new Model_ContractorDiscussionMongo();
        $modelContractorDiscussion = new Model_ContractorDiscussion();
        $modelContractorInfo = new Model_ContractorInfo();
        $modelImage = new Model_Image();
        $modelUser = new Model_User();

        /* if (!$modelBooking->checkIfCanSeeBookingWithOutTimePeriod($bookingId)) {
          $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this Booking"));
          $this->_redirect($this->router->assemble(array(), 'booking'));
          } */
        $contractor = $modelUser->getById($contractor_id);
        $this->view->contractor = $contractor;

        if (!$contractor) {
            $data['msg'] = 'Contractor is not exist';
            echo json_encode($data);
            exit;
        }
        $filters = array();
        $lastFourImages = array();
        $imageAsArray = array();
        //$images3 = array();
        $contractorDiscussions2 = array();
        $contractorDiscussions = $modelContractorDiscussionMongo->getByContractorId($contractor_id, 1);
        if ($contractorDiscussions) {
            foreach ($contractorDiscussions as $key => $row)
                $contractorDiscussions2[] = $row;

            foreach ($contractorDiscussions2 as &$discussion) {
                $lastFourImagesAfterProcessing = array();
                $user = $modelUser->getById($discussion['user_id']);
                if (count($discussion['thumbnail_images'] > 4)) {
                    $images = array_slice($discussion['thumbnail_images'], 0, 4, true);
                    //$discussion['images'] = $images;
                } else {
                    $images = $discussion['thumbnail_images'];
                    //$discussion['images'] = $discussion['thumbnail_images'];
                }
                foreach ($images as $k => $img) {
                    $imageAsArray['thumbnail_path'] = $img;
                    $lastFourImagesAfterProcessing[] = $imageAsArray;
                }
                $discussion['images'] = $lastFourImagesAfterProcessing;

                $discussion['username'] = ucwords($user['username']);
                $discussion['avatar'] = $_SERVER['HTTP_HOST'] . '/uploads/user_pic/thumb_medium/' . $user['avatar'];
                $discussion['seen_flag'] = 0;
                $discussion['discussion_id'] = 1;
                $discussion['user_message'] = urldecode($discussion['user_message']);
                //foreach($discussion['_id'] as $k => $v)
                $discussion['_id'] = $discussion['_id']->{'$id'};
            }
        }

        /* $ContractorDiscussions = $modelContractorDiscussion->getByContractorId($contractor_id);

          foreach ($ContractorDiscussions as $key=>&$ContractorDiscussion) {
          $user = $modelUser->getById($ContractorDiscussion['user_id']);
          $contractorImage = $modelImage->getContractorDiscussionImages($contractor_id,$ContractorDiscussion['discussion_id']);
          $contractorGroupImages = $modelImage->getContractorDiscussionImages($contractor_id,$ContractorDiscussion['discussion_id'],1);
          var_dump($contractorGroupImages);
          $ContractorDiscussion['images'] = $contractorGroupImages;
          $ContractorDiscussion['thumbnail_path'] = isset($contractorImage[0]['thumbnail_path']) ? $contractorImage[0]['thumbnail_path'] : '';
          $user = $modelUser->getById($ContractorDiscussion['user_id']);
          $ContractorDiscussion['username'] = ucwords($user['username']);
          $ContractorDiscussion['avatar'] = $_SERVER['HTTP_HOST'] . '/uploads/user_pic/thumb_medium/' . $user['avatar'];
          }
          exit; */

        $data['ContractorDiscussions'] = $contractorDiscussions2;
        echo json_encode($data);
        exit;
    }

    public function changeBookingTimeAction() {

        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $type = $this->request->getParam('type', 'customer');
        $customer_id = $this->request->getParam('customer_id', 0);
        $bookingId = $this->request->getParam('booking_id', 0);
        $multipleDayId = $this->request->getParam('multipleDay_id', 0);
        $booking_start = $this->request->getParam('booking_start');
        $booking_end = $this->request->getParam('booking_end');
        $is_all_day_event = $this->request->getParam('is_all_day_event', 0);


        $modelBookingMultipleDaysTemp = new Model_BookingMultipleDaysTemp();
        $modelbookingTimeTemp = new Model_BookingTimeTemp();
        $modelBooking = new Model_Booking();
        $modelBookingMultipleDays = new Model_BookingMultipleDays();
        $booking = $modelBooking->getById($bookingId);

        $current = time();


        /* $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);
          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken, 'customer'))) {
          $data['msg'] = 'wrong access token';
          $data['type'] = 'error';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {
          $authrezed = $this->openNewSession($accessToken, 'customer', $customer_id);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */

        $this->checkAccessToken($accessToken, 'customer', $customer_id);
        $loggedUser = CheckAuth::getLoggedUser();

        if ($multipleDayId && isset($multipleDayId)) {
            $bookingMultipleDay = $modelBookingMultipleDays->getById($multipleDayId);

            if ((strtotime($bookingMultipleDay['booking_start']) != strtotime($booking_start)) || (strtotime($bookingMultipleDay['booking_end']) != strtotime($booking_end))) {
                if (strtotime($booking_start) > $current) {
                    $db_params['booking_start'] = php2MySqlTime(strtotime($booking_start));
                    $db_params['booking_end'] = php2MySqlTime(strtotime($booking_end));
                    $db_params['is_all_day_event'] = $is_all_day_event;
                    $db_params['booking_multiple_days_id'] = $multipleDayId;
                    $db_params['booking_id'] = $bookingId;
                    $db_params['user_id'] = $loggedUser['customer_id'];
                    $bookingMultipleDaysTemp = $modelBookingMultipleDaysTemp->getByMultipleDayId($multipleDayId);
                    if ($bookingMultipleDaysTemp) {
                        $saveMultipleDay = $modelBookingMultipleDaysTemp->updateById($bookingMultipleDaysTemp['id'], $db_params);
                    } else {
                        $saveMultipleDay = $modelBookingMultipleDaysTemp->insert($db_params);
                    }
                    $modelBookingMultipleDays->updateById($multipleDayId, array('is_change' => 1));
                    $updateBooking = $modelBooking->updateById($bookingId, array('is_change' => 1));
                }
            }
        } else {

            if ((strtotime($booking['booking_start']) != strtotime($booking_start)) || (strtotime($booking['booking_end']) != strtotime($booking_end))) {
                if (strtotime($booking_start) > $current) {
                    $db_params = array();
                    $db_params['booking_start'] = php2MySqlTime(strtotime($booking_start));
                    $db_params['booking_end'] = php2MySqlTime(strtotime($booking_end));
                    $db_params['is_all_day_event'] = $is_all_day_event;
                    $db_params['booking_id'] = $bookingId;
                    $db_params['user_id'] = $loggedUser['customer_id'];
                    $bookingTimeTemp = $modelbookingTimeTemp->getByBookingId($bookingId);
                    if ($bookingTimeTemp) {
                        $saveBookingTime = $modelbookingTimeTemp->updateById($bookingTimeTemp['booking_time_id'], $db_params);
                    } else {
                        $saveBookingTime = $modelbookingTimeTemp->insert($db_params);
                    }
                    $updateBooking = $modelBooking->updateById($bookingId, array('is_change' => 1));
                }
            }
        }


        if ((isset($saveBookingTime) && $saveBookingTime) || (isset($saveMultipleDay) && $saveMultipleDay)) {
            $data['msg'] = 'Booking date updated successfully';
            $data['type'] = 'success';

            if ($updateBooking) {
                require_once 'Zend/Cache.php';
                $company_id = CheckAuth::getCompanySession();
                $bookingViewDir = get_config('cache') . '/' . 'bookingsView' . '/' . $company_id;
                if (!is_dir($bookingViewDir)) {
                    mkdir($bookingViewDir, 0777, true);
                }
                $frontEndOption = array('lifetime' => NULL,
                    'automatic_serialization' => true);
                $backendOptions = array('cache_dir' => $bookingViewDir);
                $Cache = Zend_Cache::factory('Core', 'File', $frontEndOption, $backendOptions);
                $Cache->remove($bookingId . '_bookingDetails');
            }
        } else {
            $data['msg'] = 'No changes in Booking date';
            $data['type'] = 'error';
        }

        $data['authrezed'] = 1;
        echo json_encode($data);
        exit;
    }

    public function deleteAttachmentAction() {
        //
        $id = $this->request->getParam('id', 0);
        $type = $this->request->getParam('type', 0);
        $itemId = $this->request->getParam('itemId', 0);
        $deleteComments = $this->request->getParam('deleteComments', 0);

        $accessToken = $this->request->getParam('access_token', 0);

        /* $loggedUser = CheckAuth::getLoggedUser();
          $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];

          $data = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {
          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */
        $this->checkAccessToken($accessToken);
        $data['authrezed'] = 1;
        $ids = $this->request->getParam('ids', array());

        if ($id) {
            $ids[] = $id;
        }

        $modelImage = new Model_Image();
        $ModelItemImage = new Model_ItemImage();
        $ModelAttachment = new Model_Attachment();

        if (!($type)) {
            foreach ($ids as $id) {
                $ModelAttachment->updateById($id, array('is_deleted' => 1));
            }
        } else {
            foreach ($ids as $id) {
                $imageAttachment = $modelImage->getById($id, $type);
                if ($imageAttachment) {

                    if (file_exists(get_config('image_attachment') . '/' . $imageAttachment['original_path'])) {
                        unlink(get_config('image_attachment') . '/' . $imageAttachment['original_path']);
                    }
                    if (file_exists(get_config('image_attachment') . '/' . $imageAttachment['large_path'])) {
                        unlink(get_config('image_attachment') . '/' . $imageAttachment['large_path']);
                    }
                    if (file_exists(get_config('image_attachment') . '/' . $imageAttachment['small_path'])) {
                        unlink(get_config('image_attachment') . '/' . $imageAttachment['small_path']);
                    }
                    if (file_exists(get_config('image_attachment') . '/' . $imageAttachment['thumbnail_path'])) {
                        unlink(get_config('image_attachment') . '/' . $imageAttachment['thumbnail_path']);
                    }

                    $modelImage->deleteById($id);
                    $ModelItemImage->deleteById($id);
                    if ($deleteComments) {
                        if ($type == 'booking') {
                            $ModelBookingDiscussion = new Model_BookingDiscussion();
                            $imageDiscussions = $ModelBookingDiscussion->getByImageId($id);
                            foreach ($imageDiscussions as &$imageDiscussion) {
                                $ModelBookingDiscussion->deleteById($imageDiscussion['discussion_id']);
                            }
                        } elseif ($type == 'inquiry') {
                            $ModelInquiryDiscussion = new Model_InquiryDiscussion();
                            $imageDiscussions = $ModelInquiryDiscussion->getByImageId($id);
                            foreach ($imageDiscussions as &$imageDiscussion) {
                                $ModelInquiryDiscussion->deleteById($imageDiscussion['discussion_id']);
                            }
                        } elseif ($type == 'estimate') {
                            $ModelEstimateDiscussion = new Model_EstimateDiscussion();
                            $imageDiscussions = $ModelEstimateDiscussion->getByImageId($id);
                            foreach ($imageDiscussions as &$imageDiscussion) {
                                $ModelEstimateDiscussion->deleteById($imageDiscussion['discussion_id']);
                            }
                        }
                    }
                } else {
                    $data['msg'] = 'photo not found';
                    $data['type'] = 'error';
                    echo json_encode($data);
                    exit;
                }
            }


            $this->clearPhotoCache();
        }

        $data['msg'] = 'Photo deleted';
        $data['type'] = 'success';
        echo json_encode($data);
        exit;
    }

    public function saveCallLogAction() {

        header('Content-Type: application/json');

        $accessToken = $this->request->getParam('access_token', 0);
        $customer_id = $this->request->getParam('customer_id', 0);
        $caller_id = $this->request->getParam('caller_id', 0);
        $callee_id = $this->request->getParam('callee_id', 0);
        $callee_user_role = $this->request->getParam('callee_user_role', 0);
        $caller_user_role = $this->request->getParam('caller_user_role', 0);
        $duration = $this->request->getParam('duration', 0);
        $time = $this->request->getParam('time', 0);
        $call_sid = $this->request->getParam('call_sid', null);
        $os = $this->request->getParam('os', 'android');
        $customer_contact_id = $this->request->getParam('customer_contact_id', 0); //Rand
        $user_role = $caller_user_role;

        $this->checkAccessToken($accessToken, $user_role, $customer_id);

        /* $loggedUser = CheckAuth::getLoggedUser();
          if ($user_role == 'contractor') {
          $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];
          }
          $data = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken, $user_role))) {
          $data['msg'] = 'wrong access token';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {
          //open new session
          $authrezed = $this->openNewSession($accessToken, $user_role, $customer_id);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */



        $loggedUser = CheckAuth::getLoggedUser();
        $modelAuthRole = new Model_AuthRole();
        $modelCallLog = new Model_CallLog();

        $callee_user_role_id = $modelAuthRole->getRoleIdByName($callee_user_role);
        $caller_user_role_id = $modelAuthRole->getRoleIdByName($caller_user_role);

        $secondary_contact = 0;
        if ($callee_user_role == 'customer') {
            if ($customer_contact_id) {//
                $secondary_contact = $customer_contact_id; //
            }
        }

        //Rand
        else if ($callee_user_role == 'company') {
            if ($customer_contact_id) {//
                $secondary_contact = $customer_contact_id; //
            }
        }


        $param = array(
            'caller_id' => $caller_id,
            'callee_id' => $callee_id,
            'callee_user_role' => (isset($callee_user_role_id) && $callee_user_role_id != NULL) ? $callee_user_role_id : $callee_id, //Rand
            'caller_user_role' => $caller_user_role_id,
            'duration' => $duration,
            'time' => $time,
            'created' => time(),
            'user_id' => $loggedUser['user_id'],
            'os' => $os,
            'call_sid' => $call_sid,
            'contact_id' => $secondary_contact
        );

        $success = $modelCallLog->insert($param);


        $data['authrezed'] = 1;

        if ($success) {
            $data['msg'] = 'call log saved successfully';
            $data['type'] = 'success';
        } else {
            $data['msg'] = 'error try again';
            $data['type'] = 'error';
        }


        echo json_encode($data);
        exit;
    }

    public function markAllContractorCommentsAsSeenAction() {

        $accessToken = $this->request->getParam('access_token', 0);
        $loggedUser = CheckAuth::getLoggedUser();

        $this->checkAccessToken($accessToken);

        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];

          $data = array('authrezed' => 0);

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {
          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */

        $loggedUser = CheckAuth::getLoggedUser();
        $contractor_id = $loggedUser['user_id'];
        $isContractor = false;

        $modelAuthRole = new Model_AuthRole();
        $contractorRoleId = $modelAuthRole->getRoleIdByName('contractor');

        if ($loggedUser['role_id'] == $contractorRoleId) {
            $isContractor = true;
        }

        $modelContractorDiscussionMongo = new Model_ContractorDiscussionMongo();
        $success = $modelContractorDiscussionMongo->markAllAsSeen($contractor_id, $loggedUser['username'], $loggedUser['user_id'], $isContractor);

        /*

          $modelContractorDiscussion = new Model_ContractorDiscussion();
          $params = array('seen_flag' => 1);

          $where['contractor_id = ?'] = $contractor_id;
          $where['user_id != ?'] = $loggedUser['user_id'];
          $where['seen_flag = ?'] = 0;
          $success = $modelContractorDiscussion->updateByDifferentCriterias($where, $params); */

        $data['authrezed'] = 1;
        if ($success) {
            $data['msg'] = 'Comments seen successfully';
            $data['type'] = 'success';
        } else {
            $data['msg'] = 'Comments already seen';
            $data['type'] = 'error';
        }

        echo json_encode($data);
        exit;
    }

    public function addBookingAction() {

        header('Content-Type: application/json');
        $modelUser = new Model_User();
        $enquiriesUser = $modelUser->getByUsername('enquiries');
        $request_type = $this->request->getParam('request_type', 'app');

        $contractorId = $enquiriesUser['user_id'];
        if ($request_type == 'web') {

            $title = '';
            $customerTypeName = $this->request->getParam('customer_type_name');
            //$customerTypeId = isset($booking['customer_type_id']) ? $booking['customer_type_id'] : 0;

            $customer_comment = $this->request->getParam('customer_comment', '');
            $customerTypeId = $this->request->getParam('customer_type_id', '1');
            $businessName = $this->request->getParam('customer_business_name');
            $firstName = $this->request->getParam('first_name', '');
            $lastName = $this->request->getParam('last_name', '');
            $email1 = $this->request->getParam('email1', '');
            $email2 = $this->request->getParam('email2', '');
            $email3 = $this->request->getParam('email3', '');
            $mobile1 = $this->request->getParam('mobile1', '');
            $mobile2 = $this->request->getParam('mobile2', '');
            $mobile3 = $this->request->getParam('mobile3', '');
            $phone1 = $this->request->getParam('phone1', '');
            $phone2 = $this->request->getParam('phone2', '');
            $phone3 = $this->request->getParam('phone3', '');
            $is_same = $this->request->getParam('is_same', false);
            $customer_postcode = $this->request->getParam('customer_postcode', '');
            $customer_suburb = $this->request->getParam('customer_suburb', '');
            $customer_unitLotNumber = $this->request->getParam('customer_unitLotNumber', '');
            $customer_streetNumber = $this->request->getParam('customer_streetNumber', '');
            $customer_streetAddress = $this->request->getParam('customer_streetAddress', '');
            $customer_token_id = $this->request->getParam('customer_token_id', '');
            $booking_postcode = $this->request->getParam('postCode', '');
            $booking_suburb = $this->request->getParam('suburb', '');
            $booking_unitLotNumber = $this->request->getParam('unit_number', '');
            $booking_streetNumber = $this->request->getParam('street_number', '');
            $booking_streetAddress = $this->request->getParam('street_name', '');
            if ($is_same) {
                $customer_postcode = $booking_postcode;
                $customer_suburb = $booking_suburb;
                $customer_unitLotNumber = $booking_unitLotNumber;
                $customer_streetAddress = $booking_streetAddress;
                $customer_streetNumber = $booking_streetNumber;
            }
            $bookingStart = $this->request->getParam('booking_start', '');
            $bookingEnd = $this->request->getParam('booking_end', '');
            $propertyTypeId = $this->request->getParam('property_type_id', '1');
            $services = $this->request->getParam('services', '');
        } else {

            $data = $this->request->getPost();
            if (empty($data)) {
                $data = $_GET;
            }
            if (!empty($data)) {
                $booking = json_decode($data['booking_object']);
            }
            $result = array();
            if (!empty($booking)) {
                foreach ($booking as $key => $value) {
                    if (!is_array($value)) {
                        $value = urldecode($value);
                    }
                    $result[$key] = $value;
                }
            }

            if ($result['services']) {
                $services = $result['services'];
                if (!empty($services)) {
                    $result1 = array();
                    foreach ($services as $key => &$value) {
                        $result2 = array();
                        foreach ($value as $k => $v) {
                            if (!is_array($v)) {
                                $v = urldecode($v);
                            }
                            $result2[$k] = $v;
                        }
                        $result2['contractor_id'] = $contractorId;
                        $result1[$key] = $result2;
                    }
                }
            }
            $services = $result1;
            $result1 = array();
            if (!empty($services)) {
                foreach ($services as $key => &$value) {
                    $attributes = $value['attributes'];
                    $attributes_arr = array();
                    foreach ($attributes as $k => $v) {
                        $attribute = array();
                        foreach ($v as $k1 => $v1) {
                            if (!is_array($v1)) {
                                $v1 = urldecode($v1);
                            }
                            $attribute[$k1] = $v1;
                        }
                        $attributes_arr[$k] = $attribute;
                    }
                    $value['attributes'] = $attributes_arr;
                }
            }
            $result['services'] = $services;
            $booking = $result;




            $title = isset($booking['title']) ? $booking['title'] : '';

            $customerTypeName = isset($booking['customer_type']) ? $booking['customer_type'] : '';
            $customerTypeId = isset($booking['customer_type_id']) ? $booking['customer_type_id'] : 0;
            $title = isset($booking['title']) ? $booking['title'] : '';
            $firstName = isset($booking['first_name']) ? $booking['first_name'] : '';
            $lastName = isset($booking['last_name']) ? $booking['last_name'] : '';
            $email1 = isset($booking['email1']) ? $booking['email1'] : '';
            $email2 = isset($booking['email2']) ? $booking['email2'] : '';
            $mobile1 = isset($booking['mobile1']) ? $booking['mobile1'] : '';
            $mobile2 = isset($booking['mobile2']) ? $booking['mobile2'] : '';
            $phone1 = isset($booking['phone1']) ? $booking['phone1'] : '';
            $phone2 = isset($booking['phone2']) ? $booking['phone2'] : '';
            $customer_postcode = isset($booking['customer_postcode']) ? $booking['customer_postcode'] : '';
            $customer_suburb = isset($booking['customer_suburb']) ? $booking['customer_suburb'] : '';
            $customer_unitLotNumber = isset($booking['customer_unit_lot_number']) ? $booking['customer_unit_lot_number'] : '';
            $customer_streetNumber = isset($booking['customer_street_number']) ? $booking['customer_street_number'] : '';
            $customer_streetAddress = isset($booking['customer_street_address']) ? $booking['customer_street_address'] : '';
            $businessName = isset($booking['customer_business_name']) ? $booking['customer_business_name'] : '';
            $booking_postcode = isset($booking['booking_postcode']) ? $booking['booking_postcode'] : '';
            $booking_suburb = isset($booking['booking_suburb']) ? $booking['booking_suburb'] : '';
            $booking_unitLotNumber = isset($booking['booking_unit_lot_number']) ? $booking['booking_unit_lot_number'] : '';
            $booking_streetNumber = isset($booking['booking_street_number']) ? $booking['booking_street_number'] : '';
            $booking_streetAddress = isset($booking['booking_street_address']) ? $booking['booking_street_address'] : '';
            $bookingStart = isset($booking['booking_start']) ? $booking['booking_start'] : '';
            $bookingStart = php2MySqlTime($bookingStart);
            $bookingEnd = isset($booking['booking_end']) ? $booking['booking_end'] : '';
            $bookingEnd = php2MySqlTime($bookingEnd);
            $propertyTypeId = isset($booking['property_type_id']) ? $booking['property_type_id'] : 0;
            $services = isset($booking['services']) ? $booking['services'] : array();

            $modelCustomerType = new Model_CustomerType();
            if ($customerTypeName == 'Residential') {
                $customerType = $modelCustomerType->getByTypeName($customerTypeName);
                $customerTypeId = $customerType['customer_type_id'];
            }
        }




        $modelCustomer = new Model_Customer();
        $cityStateObj = $modelCustomer->getStateAndCityByPostCode($booking_postcode, true);
        $state = $cityStateObj['state'];
        $cityName = $cityStateObj['city'];
        $modelCities = new Model_Cities();
        $city = $modelCities->getByName($cityName);




        if (empty($city) || $cityName == 'unknown') {
            $returnData['msg'] = " No services available in this location ";
            $returnData['type'] = 'error';
            echo json_encode($returnData);
            exit;
        }



        if (isset($mobile1) && $mobile1 != "") {
            if ($request_type == 'web') {
                $mobile1 = '0061' . substr($mobile1, -9);
            }
            $mobile1 = $modelCustomer->search($mobile1, $state, 1);
        }
        if (isset($mobile2) && $mobile2 != "") {
            if ($request_type == 'web') {
                $mobile2 = '0061' . substr($mobile2, -9);
            }
            $mobile2 = $modelCustomer->search($mobile2, $state, 1);
        }
        if (isset($mobile3) && $mobile3 != "") {
            if ($request_type == 'web') {
                $mobile3 = '0061' . substr($mobile3, -9);
            }
            $mobile3 = $modelCustomer->search($mobile3, $state, 1);
        }
        if (isset($phone1) && $phone1 != "") {
            $phone1 = $modelCustomer->search($phone1, $state, 0);
        }
        if (isset($phone2) && $phone2 != "") {
            $phone2 = $modelCustomer->search($phone2, $state, 0);
        }
        if (isset($phone3) && $phone3 != "") {
            $phone3 = $modelCustomer->search($phone3, $state, 0);
        }


        if (get_config('remove_white_spacing')) {
            $mobile1 = preparer_number($mobile1);
            $mobile2 = preparer_number($mobile2);
            $phone1 = preparer_number($phone1);
            $phone2 = preparer_number($phone2);
        }



        if (empty($mobile1)) {
            $returnData['msg'] = "Invalid mobile number";
            $returnData['type'] = 'error';
            echo json_encode($returnData);
            exit;
        }

        $modelCompanies = new Model_Companies();
        $companyData = $modelCompanies->getByName('Tile Cleaners Pty Ltd');
        $companyId = $companyData['company_id'];


        $data = array(
            'title' => $title,
            'first_name' => $firstName,
            'last_name' => $lastName,
            'city_id' => $city['city_id'],
            'unit_lot_number' => $customer_unitLotNumber,
            'street_number' => $customer_streetNumber,
            'street_address' => $customer_streetAddress,
            'suburb' => $customer_suburb,
            'postcode' => $customer_postcode,
            'email1' => $email1,
            'phone1' => $phone1,
            'mobile1' => $mobile1,
            'email2' => $email2,
            'phone2' => $phone2,
            'mobile2' => $mobile2,
            'full_name_search' => $title . '.' . $firstName . ' ' . $lastName,
            'created' => time(),
            'customer_type_id' => $customerTypeId,
            'company_id' => $companyId,
        );



        $customerId = $modelCustomer->insert($data);
        if ($customerId) {

            if ($request_type == 'web') {
                $card_number = $this->request->getParam('eway_card_number');
                if ($card_number && !empty($card_number)) {
                    $payment_result = $this->sendEwayPaymentDetails($customerId);
                }
            }


            $customerContact = new Model_CustomerContact();
            for ($i = 0; $i < 2; $i++) {

                if ($request_type == 'web') {
                    $firstName = $this->request->getParam('first_name_' . $i, '');
                    $lastName = $this->request->getParam('last_name_' . $i, '');
                    $email1 = $this->request->getParam('email1_' . $i, '');
                    $email2 = $this->request->getParam('email2_' . $i, '');
                    $email3 = $this->request->getParam('email3_' . $i, '');
                    $mobile1 = $this->request->getParam('mobile1_' . $i, '');
                    $mobile2 = $this->request->getParam('mobile2_' . $i, '');
                    $mobile3 = $this->request->getParam('mobile3_' . $i, '');
                    $phone1 = $this->request->getParam('phone1_' . $i, '');
                    $phone2 = $this->request->getParam('phone2_' . $i, '');
                    $phone3 = $this->request->getParam('phone3_' . $i, '');
                } else {
                    $title = isset($booking['title_' . $i]) ? $booking['title_' . $i] : null;
                    $firstName = isset($booking['first_name_' . $i]) ? $booking['first_name_' . $i] : null;
                    $lastName = isset($booking['last_name_' . $i]) ? $booking['last_name_' . $i] : null;
                    $email1 = isset($booking['email1_' . $i]) ? $booking['email1_' . $i] : null;
                    $email2 = isset($booking['email2_' . $i]) ? $booking['email2_' . $i] : null;
                    $email3 = isset($booking['email3_' . $i]) ? $booking['email3_' . $i] : null;
                    $mobile1 = isset($booking['mobile1_' . $i]) ? $booking['mobile1_' . $i] : null;
                    $mobile2 = isset($booking['mobile2_' . $i]) ? $booking['mobile2_' . $i] : null;
                    $mobile3 = isset($booking['mobile3_' . $i]) ? $booking['mobile3_' . $i] : null;
                    $phone1 = isset($booking['phone1_' . $i]) ? $booking['phone1_' . $i] : null;
                    $phone2 = isset($booking['phone2_' . $i]) ? $booking['phone2_' . $i] : null;
                    $phone3 = isset($booking['phone3_' . $i]) ? $booking['phone3_' . $i] : null;
                }
                if (isset($mobile1) && $mobile1 != "") {
                    if ($request_type == 'web') {
                        $mobile1 = '0061' . substr($mobile1, -9);
                    }
                    $mobile1 = $modelCustomer->search($mobile1, $state, 1);
                }
                if (isset($mobile2) && $mobile2 != "") {
                    if ($request_type == 'web') {
                        $mobile2 = '0061' . substr($mobile2, -9);
                    }
                    $mobile2 = $modelCustomer->search($mobile2, $state, 1);
                }
                if (isset($mobile3) && $mobile3 != "") {
                    if ($request_type == 'web') {
                        $mobile3 = '0061' . substr($mobile3, -9);
                    }
                    $mobile3 = $modelCustomer->search($mobile3, $state, 1);
                }
                if (isset($phone1) && $phone1 != "") {
                    $phone1 = $modelCustomer->search($phone1, $state, 0);
                }
                if (isset($phone2) && $phone2 != "") {
                    $phone2 = $modelCustomer->search($phone2, $state, 0);
                }
                if (isset($phone3) && $phone3 != "") {
                    $phone3 = $modelCustomer->search($phone3, $state, 0);
                }

                if (get_config('remove_white_spacing')) {
                    $mobile1 = preparer_number($mobile1);
                    $mobile2 = preparer_number($mobile2);
                    $mobile3 = preparer_number($mobile3);
                    $phone1 = preparer_number($phone1);
                    $phone2 = preparer_number($phone2);
                    $phone3 = preparer_number($phone3);
                }

                if (isset($mobile1) && !empty($mobile1)) {
                    $contact_data = array('title' => $title,
                        'first_name' => $firstName,
                        'last_name' => $lastName,
                        'email1' => $email1,
                        'email2' => $email2,
                        'email3' => $email3,
                        'mobile1' => $mobile1,
                        'mobile2' => $mobile2,
                        'mobile3' => $mobile3,
                        'phone1' => $phone1,
                        'phone2' => $phone2,
                        'phone3' => $phone3,
                        'customer_id' => $customerId);

                    $customerContact->insert($contact_data);
                }
            }

            $modelCustomerCommercialInfo = new Model_CustomerCommercialInfo();
            $modelCustomerCommercialInfo->setCustomerCommercialInfo($customerId, $customerTypeId, $businessName);
        }

        $modelBookingStatus = new Model_BookingStatus();
        $tentative = $modelBookingStatus->getByStatusName('TENTATIVE');
        $statusId = $tentative['booking_status_id'];

        $modelAuthRole = new Model_AuthRole();
        $customer_role_id = $modelAuthRole->getRoleIdByName('customer');

        $modelBooking = new Model_Booking();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelRefund = new Model_Refund();
        $db_params = array();
        $db_params['status_id'] = $statusId;
        $db_params['customer_id'] = $customerId;
        $db_params['booking_start'] = $bookingStart;
        $db_params['booking_end'] = $bookingEnd;
        $db_params['title'] = '';
        $db_params['is_all_day_event'] = 0;
        $db_params['description'] = '';
        $db_params['city_id'] = $city['city_id'];
        $db_params['property_type_id'] = $propertyTypeId;
        $db_params['to_follow'] = 0;
        $db_params['is_to_follow'] = 0;
        $db_params['is_multiple_days'] = 0;
        $db_params['created'] = time();
        $db_params['company_id'] = $companyId;
        $db_params['created_by'] = $customerId;
        $db_params['user_role'] = $customer_role_id;



        $returnData = $modelBooking->addDetailedCalendar($db_params);




        $logId = $returnData['log'];

        if (isset($returnData['Data']) && $returnData['Data']) {
            $bookingId = (int) $returnData['Data'];

            $modelBookingStatusHistory = new Model_BookingStatusHistory();
            $modelBookingStatusHistory->addStatusHistory($bookingId, $statusId);
            if ($request_type == 'web') {
                $modelContractorServiceBooking->setServicesToBooking($bookingId, $services, $logId);
            } else {
                $modelContractorServiceBooking->setServicesToBooking($bookingId, $services, $logId, 'createByCustomer');
            }

            // save booking address 
            $booking_address = array('street_address' => $booking_streetAddress,
                'street_number' => $booking_streetNumber,
                'suburb' => $booking_suburb,
                'unit_lot_number' => $booking_unitLotNumber,
                'postcode' => $booking_postcode,
                'original_booking_id' => $bookingId
            );



            $this->saveAddress($booking_address, $bookingId);

            // save booking images 
            $modelImage = new Model_Image();
            $modelItemImage = new Model_ItemImage();

            if ($returnData['IsSuccess'] && $request_type == 'app') {
                $user_ip = $this->request->getParam('user_ip', '');
                $booking_images = $this->request->getParam('booking_images', array());

                $booking_images = json_decode($booking_images);
                $booking_images_after_processing = array();



                if (!empty($booking_images)) {
                    foreach ($booking_images as $key => $image) {
                        $image_data = array();
                        if (!empty($image)) {
                            foreach ($image as $k => $img) {
                                if (!is_array($img)) {
                                    $img = urldecode($img);
                                }
                                $image_data[$k] = $img;
                            }
                        }


                        $booking_images_after_processing[$key] = $image_data;
                    }
                }




                foreach ($booking_images_after_processing as $image) {

                    $data = array(
                        'user_ip' => $user_ip,
                        'created_by' => $customerId,
                        'user_role' => $customer_role_id
                    );
                    $modelImage->updateById($image['image_id'], $data);
                    $Itemdata = array(
                        'item_id' => $bookingId,
                        'service_id' => $image['service_id'],
                        'image_id' => $image['image_id'],
                        'type' => 'booking'
                    );

                    $Item_image_id = $modelItemImage->insert($Itemdata);
                }
            } else if ($request_type == 'web') {
                $booking_services = $modelContractorServiceBooking->getByBookingId($bookingId, true);
                foreach ($booking_services as $booking_service) {
                    $service_images = $this->request->getParam('image_' . $booking_service['service_id'], array());
                    if (isset($service_images) && !empty($service_images)) {
                        foreach ($service_images as $service_image) {
                            $data = array(
                                'user_ip' => $_SERVER['REMOTE_ADDR'],
                                'created_by' => $customerId,
                                'user_role' => $customer_role_id
                            );
                            $modelImage->updateById($service_image, $data);
                            $Itemdata = array(
                                'item_id' => $bookingId,
                                'service_id' => $booking_service['service_id'],
                                'image_id' => $service_image,
                                'type' => 'booking'
                            );

                            $Item_image_id = $modelItemImage->insert($Itemdata);
                        }
                    }
                }
            }

            $totalDiscountTemp = 0;
            $callOutFeeTemp = 0;
            $callOutFee = 0;
            $totalDiscount = 0;
            $db_params = array();
            /**
             * save calculation qoute
             */
            $totalRefund = $modelRefund->getTotalRefund(array('booking_id' => $bookingId, 'is_approved' => 'yes'));
            $subTotal = $modelContractorServiceBooking->getTotalBookingQoute($bookingId);
            $total = ($subTotal + $callOutFee);
            $gstTax = $total * get_config('gst_tax');
            $totalQoute = $total + $gstTax - $totalDiscount;
            $totalQoute = $totalQoute - $totalRefund;

            $db_params['sub_total'] = round($subTotal, 2);
            $db_params['gst'] = round($gstTax, 2);
            $db_params['qoute'] = round($totalQoute, 2);
            $db_params['call_out_fee'] = round($callOutFee, 2);
            $db_params['refund'] = round($totalRefund, 2);
            $db_params['total_discount'] = round($totalDiscount, 2);
            $db_params['total_discount_temp'] = round($totalDiscountTemp, 2);
            $db_params['call_out_fee_temp'] = round($callOutFeeTemp, 2);

            $modelBooking->updateById($bookingId, $db_params, false, $logId);

            $returnData = array();
            $booking = $modelBooking->getById($bookingId);

            $returnData['msg'] = 'added successfully';
            $returnData['type'] = 'success';
            $returnData['result'] = $booking['booking_num'];
            if ($request_type == 'web') {
                $returnData['booking_id'] = $booking['booking_id'];
                $modelBookingDiscussion = new Model_BookingDiscussion();
                if ($customer_comment && !empty($customer_comment)) {
                    $params = array(
                        'booking_id' => $booking['booking_id'],
                        'user_id' => $customerId,
                        'user_message' => $customer_comment,
                        'created' => time(),
                        'user_role' => 9
                    );


                    $success = $modelBookingDiscussion->insert($params);
                }
                unset($_SESSION['new_booking']);
            }
        } else {
            $returnData['msg'] = 'error please try again ';
            $returnData['type'] = 'error';
        }

        echo json_encode($returnData);
        exit;
    }

    //// test by TIPC
    public function uploadImageNewBookingAction() {
        $modelImage = new Model_Image();

        $data = file_get_contents("php://input");
        $responseObject = json_decode($data);
        if ($responseObject != null) {
            $iosPhotos = $responseObject->images;
            $count = $responseObject->count;
            $service_id = $responseObject->service_id;
            //$request_type = $responseObject->request_type; 
            $image_label = $responseObject->image_label;
            $request_type = 'ios';
        } else {

            $count = $this->request->getParam('count', 0);
            //$accessToken = $this->request->getParam('access_token', 0);
            //$user_role = $this->request->getParam('user_role', 'contractor');
            //$customer_id = $this->request->getParam('customer_id', 0);
            $success = 0;
            $service_id = $this->request->getParam('service_id', 0);
            $request_type = $this->request->getParam('request_type', 0);
            $image_label = $this->request->getParam('image_label', 0);
        }


        $dir = get_config('image_attachment') . '/';
        $subdir = date('Y/m/d/');
        $fullDir = $dir . $subdir;
        if (!is_dir($fullDir)) {
            mkdir($fullDir, 0777, true);
        }
        // ios save image
        $files = array();
        $successUpload = 0;
        if ((isset($iosPhotos) && !empty($iosPhotos))) {
            $image_group = 0;
            $success_discussion = 0;
            $count = count($iosPhotos);
            if ($count > 1) {
                $maxGroup = max($modelImage->getMaxGroup());
                $image_group = $maxGroup['group_id'] + 1;
            }

            foreach ($iosPhotos as $key => $imageData) {

                $successUpload = 0;

                if (isset($imageData) && !empty($imageData)) {
                    $image = str_replace('data:image/png;base64,', '', $imageData);
                    $image = str_replace(' ', '+', $image);
                    $image = base64_decode($image);
                    $fileName = "image_{$key}.png";
                    $file = $fullDir . $fileName;
                    $success = file_put_contents($file, $image);
                    if ($success) {

                        $successUpload = 1;

                        $data = array(
                            'created_by' => 0,
                            'image_types_id' => $image_label,
                            'group_id' => $image_group,
                            'user_role' => 0
                        );

                        $id = $modelImage->insert($data);
                        $image_id = $id;


                        $original_path = "original_{$image_id}.png";
                        $large_path = "large_{$image_id}.png";
                        $small_path = "small_{$image_id}.png";
                        $thumbnail_path = "thumbnail_{$image_id}.png";
                        $compressed_path = "compressed_{$image_id}.jpg";

                        $data = array(
                            'original_path' => $subdir . $original_path,
                            'large_path' => $subdir . $large_path,
                            'small_path' => $subdir . $small_path,
                            'thumbnail_path' => $subdir . $thumbnail_path,
                            'compressed_path' => $subdir . $compressed_path
                        );

                        $modelImage->updateById($image_id, $data);
                        $compressed_saved = copy($file, $fullDir . $compressed_path);
                        $image_saved = copy($file, $fullDir . $original_path);
                        ImageMagick::scale_image($file, $fullDir . $large_path, 510);
                        ImageMagick::scale_image($file, $fullDir . $small_path, 250);
                        ImageMagick::create_thumbnail($file, $fullDir . $thumbnail_path, 78, 64);
                        ImageMagick::convert($file, $fullDir . $compressed_path);
                        ImageMagick::compress_image($file, $fullDir . $compressed_path);

                        if (file_exists($file)) {
                            unlink($file);
                        }
                    }
                }
            }
        }

        ///For Android

        $tmp_files = array();
        if (isset($request_type) && $request_type == 'web') {
            $upload = new Zend_File_Transfer_Adapter_Http();
            $tmp_files = $upload->getFileInfo();
        } else if ($request_type != 'ios') {
            for ($i = 0; $i < $count; $i++) {
                $tmp_files[] = $_FILES['imageFile' . $i];
            }
        }


        //$this->checkAccessToken($accessToken,'customer',$customer_id);
        //$result['authrezed'] = 1;
        //$date = date('H:i:s Y-m-d');//jass add
        //////////


        $modelImage = new Model_Image();
        $image_group = 0;
        if ($count > 1) {
            $maxGroup = max($modelImage->getMaxGroup());
            $image_group = $maxGroup['group_id'] + 1;
        }



        if (!empty($tmp_files)) {
            foreach ($tmp_files as $tmp_file) {
                $source = $tmp_file['tmp_name'];
                //$extension = $this->request->getParam('ext' . $i, 'png');
                $imageInfo = pathinfo($source); //jass
                //$ext = $imageInfo['extension'];//jass
                $path = $tmp_file['name'];
                $ext = pathinfo($path, PATHINFO_EXTENSION);
                //$ext = $extension;
                //$successUpload[] =  0;
                //$fileName = time(). "_image{$i}.{$ext}";

                $data = array(
                    'created_by' => 0,
                    'image_types_id' => $image_label,
                    'group_id' => $image_group,
                    'user_role' => 0
                );

                $id = $modelImage->insert($data);
                $image_id = $id;

                $original_path = "original_{$image_id}.{$ext}";
                $large_path = "large_{$image_id}.{$ext}";
                $small_path = "small_{$image_id}.{$ext}";
                $thumbnail_path = "thumbnail_{$image_id}.{$ext}";
                $compressed_path = "compressed_{$image_id}.{$ext}";
                $data = array(
                    'original_path' => $subdir . $original_path,
                    'large_path' => $subdir . $large_path,
                    'small_path' => $subdir . $small_path,
                    'thumbnail_path' => $subdir . $thumbnail_path,
                    'compressed_path' => $subdir . $compressed_path
                );

                $modelImage->updateById($image_id, $data);
                $compressed_saved = copy($source, $fullDir . $compressed_path);
                $image_saved = copy($source, $fullDir . $original_path);
                ImageMagick::scale_image($source, $fullDir . $large_path, 510);
                ImageMagick::scale_image($source, $fullDir . $small_path, 250);
                ImageMagick::create_thumbnail($source, $fullDir . $thumbnail_path, 78, 64);
                ImageMagick::convert($source, $fullDir . $compressed_path);
                ImageMagick::compress_image($source, $fullDir . $compressed_path);

                if ($image_saved) {
                    if (file_exists($source)) {
                        unlink($source);
                    }
                    $files[] = $image_id;
                    $successUpload[] = 1;
                }
            }
        }


        //////
        $result['files'] = $files;
        // $this->clearPhotoCache();
        $result['result'] = $successUpload;
        $result['service_id'] = $service_id;
        echo json_encode($result);
        exit;
    }

///End test
    public function getContractorsWithUnavailableTimeAction() {


        $service_ids = $this->request->getParam('service_ids', array());
        //$cityId = $this->request->getParam('city_id',0);
        $postCode = $this->request->getParam('postCode', 0);
        $booking_date = $this->request->getParam('booking_date', '');
        $is_array = $this->request->getParam('is_array', 0);
        if (!$is_array) {
            $service_ids = json_decode($service_ids);
        }

        $day = date('l', strtotime($booking_date));
        $modelWorkingHours = new Model_WorkingHours();

        //$service_ids = array('33');

        $modelContractorService = new Model_ContractorService();
        $modelBooking = new Model_Booking();
        $modelBookingMultipleDays = new Model_BookingMultipleDays();
        $modelCustomer = new Model_Customer();
        $modelCity = new Model_Cities();


        $cityState = $modelCustomer->getStateAndCityByPostCode($postCode, true);
        $city = $modelCity->getByName($cityState['city']);
        $cityId = $city['city_id'];

        if (!isset($service_ids) || empty($service_ids)) {
            $saved_data = $_SESSION['new_booking'];
            foreach ($saved_data as $key => $item) {
                if (strpos($key, 'services_') !== false) {
                    $service_id = explode("_", $key);
					$service_id = $service_id[1];
                    $service_ids[] = $service_id;
                }
            }
        }


        foreach ($service_ids as $service_id) {
            $ContractorServices = $modelContractorService->getContractorByServiceIdAndCityId($service_id, $cityId);
            foreach ($ContractorServices as $ContractorService) {
                $contractor_ids[$ContractorService['contractor_id']] = $ContractorService['contractor_id'];
            }
        }


        $filters['booking_in_this_day'] = $booking_date;
        $availableContractors = array();
        $unavaiable_times = array();
        foreach ($contractor_ids as $contractor_id) {
            $all_booking = array();
            $filters['contractor_id'] = $contractor_id;
            $filters['new_booking'] = 1;
            $filters['withoutEstimateStatus'] = true;

            //echo 'contractor'.$contractor_id;
            $modelGoogleCalendarEvent = new Model_GoogleCalendarEvent();
            $googleEvent = $modelGoogleCalendarEvent->getGoogleEventByContractorIdAndDateRange($contractor_id, $filters['booking_in_this_day']);
            $all_booking = array_merge($all_booking, $googleEvent);



            $bookings = $modelBooking->getAll($filters);
            $all_booking = array_merge($all_booking, $bookings);

            $bookingMultipleDays = $modelBookingMultipleDays->getAll($filters);

            $all_booking = array_merge($all_booking, $bookingMultipleDays);
            $all_working_hours = $modelWorkingHours->getAll(array('contractor_id' => $contractor_id, 'day' => $day));

            foreach ($all_working_hours as $working_hours) {
                $unavaiable_times[] = array('booking_start' => '00:00', 'booking_end' => $working_hours['start_time']);
                $unavaiable_times[] = array('booking_start' => $working_hours['end_time'], 'booking_end' => '23:59');
            }

            $bookings = array();
            foreach ($all_booking as $key => $booking) {
                $bookings[] = array('booking_start' => $booking['booking_start'], 'booking_end' => $booking['booking_end']);
                $unavaiable_times[] = array('booking_start' => $booking['booking_start'], 'booking_end' => $booking['booking_end']);
            }



            $availableContractors[] = array(
                'contractor_id' => $contractor_id,
                'bookings' => $bookings
            );
        }



        $intersect_time = array();
        $intersect_keys = array();
        $orignal_times = array();


        foreach ($unavaiable_times as $key => $main_unavaiable_time) {
            $start_time1 = date("H:i", strtotime($main_unavaiable_time['booking_start']));
            $end_time1 = date("H:i", strtotime($main_unavaiable_time['booking_end']));
            if ($end_time1 == '00:00') {
                $end_time1 = '23:59';
            }
            $orignal_times[$key] = array('start_time' => $start_time1, 'end_time' => $end_time1);
            foreach ($unavaiable_times as $k => $unavaiable_time) {
                $start_time2 = date("H:i", strtotime($unavaiable_time['booking_start']));
                $end_time2 = date("H:i", strtotime($unavaiable_time['booking_end']));

                if (($start_time1 != $start_time2) && ($end_time1 != $end_time2)) {

                    if (($start_time2 > $start_time1 && $start_time2 < $end_time1) ||
                            ($start_time1 > $start_time2 && $start_time1 < $end_time2)) {
                        $intersect_keys[$key] = 1;
                        $intersect_keys[$k] = 1;
                        if (($start_time2 > $start_time1 && $start_time2 < $end_time1)) {
                            $start_new = $start_time1;
                            $end_new = $end_time1 > $end_time2 ? $end_time1 : $end_time2;
                        }

                        if (($start_time1 > $start_time2 && $start_time1 < $end_time2)) {
                            $start_new = $start_time2;
                            $end_new = $end_time1 > $end_time2 ? $end_time1 : $end_time2;
                        }

                        $intersect_time[] = array('start_time' => $start_new, 'end_time' => $end_new);
                    }
                }
            }
            unset($unavaiable_times[$key]);
        }
        $non_intersect_time = array_diff_key($orignal_times, $intersect_keys);
        $time = array_merge($intersect_time, $non_intersect_time);
        //$time =  array_unique($time);
        $input = array_map("unserialize", array_unique(array_map("serialize", $time)));
        $new_result = array();
        foreach ($input as $key => $sub_input) {
            $new_result[] = $sub_input;
        }
        $data['result'] = $new_result;
        echo json_encode($data);
        exit;
    }

    public function checkAccessToken($accessToken, $user_role = 'contractor', $customer_id = 0) {

        $loggedUser = CheckAuth::getLoggedUser();
        if ($user_role == 'contractor') {
            $modelIosUser = new Model_IosUser();
            $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
            $this->iosLoggedUser = $user['id'];
        }
        $data = array('authrezed' => 0);

        if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken, $user_role))) {
            $data['msg'] = 'wrong access token';
            echo json_encode($data);
            exit;
        }

        if (empty($loggedUser)) {
            //open new session
            $authrezed = $this->openNewSession($accessToken, $user_role, $customer_id);
            if (!$authrezed) {
                echo json_encode($data);
                exit;
            }
        }
    }

    public function getCounts($role_name = 'contractor', $contractor_id = 0) {
        $modelIosUser = new Model_IosUser();
        $modelBooking = new Model_Booking();
        $modelCompanies = new Model_Companies();
        $modelBookingStatus = new Model_BookingStatus();

        $BookingStatus = $modelBookingStatus->getAllWithoutPermission();

        $counts = array();
        foreach ($BookingStatus as $status) {
            $results = array();
            $total = $modelBooking->getcountBookingsByStatus($status['name'], $contractor_id);
            $results['name'] = $status['name'];
            $results['total'] = $total;
            array_push($counts, $results);
        }

        $results = array();

        $UnapprovedBooking = $modelBooking->getCountUnapprovedBooking();
        $results['name'] = 'AWaiting Approval';
        $results['total'] = $UnapprovedBooking;
        array_push($counts, $results);

        $countRejectedBookings = $modelBooking->getCountRejectedBookings($contractor_id);
        $results['name'] = 'Rejected';
        $results['total'] = $countRejectedBookings;
        array_push($counts, $results);

        $countNewReuests = $modelBooking->getCountNewRequest($contractor_id);

        $results['name'] = 'New Requests';
        $results['total'] = $countNewReuests;
        array_push($counts, $results);

        $modelComplaint = new Model_Complaint();
        $countOpenComplaint = $modelComplaint->getComplaintCount(array('complaintStatus' => 'open', 'is_approved' => '1'));
        $results['name'] = 'unresolved complaints';
        $results['total'] = $countOpenComplaint;
        array_push($counts, $results);
        /////By Islam
        if ($role_name != 'customer') {

            //By Nour
            $model_BookingEstimate = new Model_BookingEstimate();
            $countmyBookingEstimate = $model_BookingEstimate->getCount(array('to_follow' => true, 'to_follow_date' => 'today'));
            $results['name'] = 'countmyBookingEstimate';
            $results['total'] = $countmyBookingEstimate[0]['count'];
            array_push($counts, $results);



            $countToFollowEstimate = $model_BookingEstimate->getCount(array('to_follow' => true, 'estimate_type' => 'draft'));
            $results['name'] = 'countToFollowEstimate';
            $results['total'] = $countToFollowEstimate[0]['count'];
            array_push($counts, $results);


            $time = time();
            /* $countToDraft =  $model_BookingEstimate ->getCount(array('estimate_type' => 'draft'));

              $view->countToDraft = $countToDraft[0]['count']; */
            //18-11
            $model_Inquiry = new Model_Inquiry();
            $countToFollowInquiry = $model_Inquiry->getCount(array('status' => 'inquiry', 'to_follow' => true, 'to_follow_date' => 'past'));
            $results['name'] = 'countToFollowInquiry';
            $results['total'] = $countToFollowInquiry[0]['count'];
            array_push($counts, $results);


            // count inquiry 
            $model_inquiry = new Model_Inquiry();
            $countmyInquiry = $model_inquiry->getCountInquiry(true);
            $results['name'] = 'countmyInquiry';
            $results['total'] = $countmyInquiry;
            array_push($counts, $results);


            $model_BookingInvoice = new Model_BookingInvoice();
            $countOfUnpaid = $model_BookingInvoice->getCount(array('invoice_type' => 'unpaid'));
            $countOfOpen = $model_BookingInvoice->getCount(array('invoice_type' => 'open'));
            $countOfOverdue = $model_BookingInvoice->getCount(array('invoice_type' => 'overdue'));

            $results['name'] = 'countOfUnpaid';
            $results['total'] = $countOfUnpaid;
            array_push($counts, $results);

            $results['name'] = 'countOfOpen';
            $results['total'] = $countOfOpen;
            array_push($counts, $results);

            $results['name'] = 'countOfOverdue';
            $results['total'] = $countOfOverdue;
            array_push($counts, $results);



            $modelBooking = new Model_Booking();
            $modelComplaint = new Model_Complaint();
            $mongo = new Model_Mongo();

            $countAwaitingupdateBooking = $modelBooking->getCountAwaitingupdateBooking();
            $results['name'] = 'countAwaitingupdateBooking';
            $results['total'] = $countAwaitingupdateBooking;
            array_push($counts, $results);


            $filters = array('convert_status' => 'booking', 'to_follow' => true);

            $countToFollowBooking = $modelBooking->getCountToFollowBooking($filters);
            $results['name'] = 'countToFollowBooking';
            $results['total'] = $countToFollowBooking['count'];
            array_push($counts, $results);



            $countAwaitingacceptBooking = $modelBooking->getCountAwaitingAcceptBooking();
            $results['name'] = 'countAwaitingacceptBooking';
            $results['total'] = $countAwaitingacceptBooking;
            array_push($counts, $results);


            $countUnapprovedBooking = $modelBooking->getCountUnapprovedBooking();
            $results['name'] = 'countUnapprovedBooking';
            $results['total'] = $countUnapprovedBooking;
            array_push($counts, $results);


            //by Salim 

            $today = getTimePeriodByName('today');
            $tomorrow = getTimePeriodByName('tomorrow');

            $primaryTodayFilters = array(
                'booking_start_between' => $today['start'],
                'booking_end_between' => $today['end'],
                'withoutEstimateStatus' => 1
            );
            $multipleTodayFilter = array(
                'multiple_start_between' => $today['start'],
                'multiple_end_between' => $today['end'],
                'withoutEstimateStatus' => 1
            );

            $sameMultipleTodayFilter = array(
                'multiple_start_between' => $today['start'],
                'multiple_end_between' => $today['end'],
                'same_dates' => 1,
                'withoutEstimateStatus' => 1
            );

            $primaryTomorrowFilters = array(
                'booking_start_between' => $tomorrow['start'],
                'booking_end_between' => $tomorrow['end'],
                'withoutEstimateStatus' => 1
            );
            $multipleTomorrowFilter = array(
                'multiple_start_between' => $tomorrow['start'],
                'multiple_end_between' => $tomorrow['end'],
                'withoutEstimateStatus' => 1
            );

            $sameMultipleTomorrowFilter = array(
                'multiple_start_between' => $tomorrow['start'],
                'multiple_end_between' => $tomorrow['end'],
                'same_dates' => 1,
                'withoutEstimateStatus' => 1
            );

            $cancelledStatus = $modelBookingStatus->getByStatusName('CANCELLED');
            $onHoldStatus = $modelBookingStatus->getByStatusName('ON HOLD');

            $futureFilters = array(
                'booking_not_started_yet' => true,
                'multiple_not_started_yet' => true,
                'withoutEstimateStatus' => 1,
                'exclude_more_one_status' => $cancelledStatus['booking_status_id'] . ',' . $onHoldStatus['booking_status_id']
            );
            $pager = null;
//            echo "test";
//            $countToday = $modelBooking->getAll($todayFilters, null, $pager, 0, 0, 0, 1);
//            $countTomorrow = $modelBooking->getAll($tomorrowFilters, null, $pager, 0, 0, 0, 1);
            $countFuture = $modelBooking->getAll($futureFilters, null, $pager, 0, 0, 0, 1);
            $modelBookingStatus = new Model_BookingStatus();
            $inProcessStatus = $modelBookingStatus->getByStatusName('IN PROGRESS');
            $toDo = $modelBookingStatus->getByStatusName('TO DO');
            $toVisit = $modelBookingStatus->getByStatusName('TO VISIT');
            $notAssignedFilters = array(
                'withoutEstimateStatus' => 1,
                'more_one_status' => $inProcessStatus['booking_status_id'] . ',' . $toDo['booking_status_id'] . ',' . $toVisit['booking_status_id'],
                'notAssigned' => 1
            );




            $notAssignedCount = $modelBooking->getAll($notAssignedFilters, null, $pager, 0, 0, 0, 1);
            $countRejectBooking = $modelBooking->getCountRejectBooking();

            $results['name'] = 'countNotAssigned';
            $results['total'] = $notAssignedCount[0]['count'] + $countRejectBooking;
            array_push($counts, $results);


            $results['name'] = 'countRejectBooking';
            $results['total'] = $countRejectBooking;
            array_push($counts, $results);

            $newRequestsFilters = array(
                'acceptance' => 'notAcceptNorReject',
                'withoutEstimateStatus' => 1,
                'booking_not_started_yet' => true
            );
            $toConfirmFilters = array(
                'to_confirm' => true,
                'next_working_day' => true,
                'withoutEstimateStatus' => 1
            );
            $countNewRequests = $modelBooking->getAll($newRequestsFilters, null, $pager, 0, 0, 0, 1);
            $countToConfirm = $modelBooking->getAll($toConfirmFilters, null, $pager, 0, 0, 0, 1);

            $AllNotMultipleBookingToday = $modelBooking->getAll($primaryTodayFilters, null, $pager, 0, 0, 0, 1);
            $AllMultipleBookingToday = $modelBooking->getAll($multipleTodayFilter, null, $pager, 0, 0, 0, 1);
            $sameMultipleBookingToday = $modelBooking->getAll($sameMultipleTodayFilter, null, $pager, 0, 0, 0, 1);

            $AllNotMultipleBookingTomorrow = $modelBooking->getAll($primaryTomorrowFilters, null, $pager, 0, 0, 0, 1);
            $AllMultipleBookingTomorrow = $modelBooking->getAll($multipleTomorrowFilter, null, $pager, 0, 0, 0, 1);
            $sameMultipleBookingTomorrow = $modelBooking->getAll($sameMultipleTomorrowFilter, null, $pager, 0, 0, 0, 1);
            if ((!isset($sameMultipleBookingTomorrow[0]['count']))) {
                $sameData = 0;
            } else {
                $sameData = $sameMultipleBookingTomorrow[0]['count'];
            }

            if ((!isset($sameMultipleBookingToday[0]['count']))) {
                $sameDataToday = 0;
            } else {
                $sameDataToday = $sameMultipleBookingToday[0]['count'];
            }

            $results['name'] = 'countToday';
            $results['total'] = abs(($AllNotMultipleBookingToday[0]['count'] + $AllMultipleBookingToday[0]['count']) - $sameDataToday);
            array_push($counts, $results);


            $results['name'] = 'countTomorrow';
            $results['total'] = abs(($AllNotMultipleBookingTomorrow[0]['count'] + $AllMultipleBookingTomorrow[0]['count']) - $sameData);
            array_push($counts, $results);


            $countDuplicates = $model_BookingInvoice->getDuplicatedInvoiceNumbers(1);


            $results['name'] = 'countFuture';
            $results['total'] = $countFuture[0]['count'];
            array_push($counts, $results);

            $results['name'] = 'countNewRequests';
            $results['total'] = $countNewRequests[0]['count'];
            array_push($counts, $results);

            $results['name'] = 'countToConfirm';
            $results['total'] = $countToConfirm[0]['count'];
            array_push($counts, $results);

            $results['name'] = 'countDuplicates';
            $results['total'] = $countDuplicates;
            array_push($counts, $results);


            $countUapprovedCompalints = $modelComplaint->countUapprovedCompalints();
            $results['name'] = 'countUapprovedCompalints';
            $results['total'] = $countUapprovedCompalints;
            array_push($counts, $results);

            /* $countInProcessBooking = $modelBooking->getCountInProcessBooking();
              $view->countInProcessBooking = $countInProcessBooking;
              $results['name'] = 'countUapprovedCompalints';
              $results['total'] = $countUapprovedCompalints;
              array_push($counts,$results); */





            $countOpenComplaint = $modelComplaint->getComplaintCount(array('complaintStatus' => 'open', 'is_approved' => '1'));
            $results['name'] = 'countOpenComplaint';
            $results['total'] = $countOpenComplaint;
            array_push($counts, $results);



            $countOpenComplaint = $modelComplaint->getCount(array('complaintStatus' => 'open', 'is_approved' => '1'));
            $results['name'] = 'countOpenComplaintNew';
            $results['total'] = $countOpenComplaint;
            array_push($counts, $results);


            $results['name'] = 'countNotifincations';
            $results['total'] = $mongo->countNotifications();
            array_push($counts, $results);

            // request owners

            $modelClaimOwner = new Model_ClaimOwner();
            $countClaimOwner = $modelClaimOwner->getCountClaimOwner();
            $results['name'] = 'countClaimOwner';
            $results['total'] = $countClaimOwner;
            array_push($counts, $results);


            //missed call
            $modelMissedCalls = new Model_MissedCalls();
            $countMissedCalls = $modelMissedCalls->getCountMissedCalls();
            $results['name'] = 'countMissedCalls';
            $results['total'] = $countMissedCalls;
            array_push($counts, $results);

            //new refunds
            $modelRefund = new Model_Refund();
            $newRefundsFilters = array('is_approved' => 'no');
            $newRefunds = $modelRefund->getAll($newRefundsFilters);
            $results['name'] = 'countNewRefunds';
            $results['total'] = count($newRefunds);
            array_push($counts, $results);


            // Unapproved Account
            $modelPayment = new Model_Payment();
            $countUnapprovedPayments = $modelPayment->getCountUnapprovedPayments();
            $results['name'] = 'countUnapprovedPayments';
            $results['total'] = $countUnapprovedPayments;
            array_push($counts, $results);
        }

        return $counts;
    }

    public function getCalendarContentAction() {

        $month = $this->request->getParam('month', date("m", time()));
        $year = $this->request->getParam('year', date("Y", time()));

        Zend_Loader::loadFile('Calendar.php', APPLICATION_PATH . '/../library');

        //defined('PUBLIC_PATH') || define('PUBLIC_PATH', realpath(dirname(__FILE__) . '/../public'));
        //require(realpath(PUBLIC_PATH . '/Calendar.php'));
        $calendar = new Calendar();
        echo $calendar->show($month, $year);
        exit;
    }

    public function getFaqsAction() {

        $booking_id = $this->request->getParam('booking_id');
        $service_id = $this->request->getParam('service_id', 0);
        $modelbooking = new Model_Booking();
        $modelCustomer = new Model_Customer();
        $faq_html = "";
        $wheres = array();
        if (!$service_id) {
            $booking = $modelbooking->getById($booking_id);
            $mdoel_contractorServiceBooking = new Model_ContractorServiceBooking();
            $trading_namesObj = new Model_TradingName();
            $allItemService = $mdoel_contractorServiceBooking->getByBookingId($booking['booking_id']);
            $trading_names = $trading_namesObj->getById($booking['trading_name_id']);
            $customer = $modelCustomer->getById($booking['customer_id']);

            foreach ($allItemService as $key => $itemService) {
                $model_attributes = new Model_Attributes();
                $modelServiceAttribute = new Model_ServiceAttribute();
                $attribute = $model_attributes->getByAttributeName("Floor", $booking['company_id']);
                $allServiceAttribute = $modelServiceAttribute->getByAttributeIdAndServiceId($attribute['attribute_id'], $itemService['service_id']);
                $modelServiceAtrributeValue = new Model_ServiceAttributeValue();
                $serviceAttributeValue = $modelServiceAtrributeValue->getByBookingIdAndServiceAttributeIdAndClone($booking['booking_id'], $allServiceAttribute['service_attribute_id'], $itemService['clone']);

                if (!empty($serviceAttributeValue['value'])) {
                    $wheres[] = 'service_id =' . $itemService['service_id'] . ' and floor_id = ' . $serviceAttributeValue['value'];
                }
            }
        } else {
            $wheres[] = 'service_id =' . $service_id;
            $trading_names['website_url'] = 'www.tilecleaners.com.au';
        }

        if (!empty($wheres)) {
            $model_faq = new Model_Faq();
            $faq = $model_faq->getFaqByServiceIdAndFloorId($wheres);
            if (empty($faq)) {
                $faq = $model_faq->getFaqByServiceIdAndFloorId($wheres, 1);
            }
            if (!empty($faq)) {
                $faq_html .= '<h4>FREQUENTLY ASKED QUESTIONS:</h4>';
                foreach ($faq as $key => $faq_value) {
                    if (!$service_id) {
                        $question = str_replace('$custom_city', $customer['city_name'], $faq_value['question']);
                        $answer = str_replace('$custom_city', $customer['city_name'], $faq_value['answer']);
                    } else {
                        $question = str_replace('$custom_city', 'Sydney', $faq_value['question']);
                        $answer = str_replace('$custom_city', 'Sydney', $faq_value['answer']);
                    }
                    $faq_html .= '<ul><li><p><a href="http://' . $trading_names['website_url'] . '"> Q: ' . $question . '</a></p></li><li><p> A: ' . $answer . '</p></li></ul>';
                }
            }
        }

        echo json_encode($faq_html);
        exit;
    }

    public function sendEwayPaymentDetails($customerId, $for_andriod = 0) {



        $modelCustomer = new Model_Customer();

        $CardHolderName = $this->request->getParam('card_name', '');
        $CreditCardNumber = $this->request->getParam('eway_card_number', 0);
        $CVN = $this->request->getParam('eway_cvn', 0);
        $expiry_date = $this->request->getParam('expiry_date', '');
        $first_name = $this->request->getParam('first_name', '');
        $last_name = $this->request->getParam('last_name', '');
        $street_address = $this->request->getParam('customer_streetAddress', '');
        $street_number = $this->request->getParam('customer_streetNumber', '');
        $postcode = $this->request->getParam('customer_postCode', '');
        $email1 = $this->request->getParam('email1', '');
        $expiry_array = explode('/', $expiry_date);
        $ExpiryMonth = $expiry_array[0];
        $ExpiryYear = $expiry_array[1];



        if ($for_andriod) {
            $key = "";
            $loggedUser = CheckAuth::getLoggedUser();
            $user_password = $loggedUser['password'];
            $CardHolderNameRemoveKey = str_replace($key, "", $CardHolderName);
            $CardHolderNameFinal = str_replace($user_password, "", $CardHolderNameRemoveKey);

            $CreditCardNumberRemoveKey = str_replace($key, "", $CreditCardNumber);
            $CreditCardNumberFinal = str_replace($user_password, "", $CreditCardNumberRemoveKey);

            $CVNRemoveKey = str_replace($key, "", $CVN);
            $CVNFinal = str_replace($user_password, "", $CVNRemoveKey);

            $expiry_dateRemoveKey = str_replace($key, "", $expiry_date);
            $expiry_dateFinal = str_replace($user_password, "", $expiry_dateRemoveKey);

            $expiry_array = explode('/', $expiry_dateFinal);
            $ExpiryMonth = $expiry_array[0];
            $ExpiryYear = $expiry_array[1];
            $CardDetails = array(
                'Name' => $CardHolderNameFinal,
                'Number' => $CreditCardNumberFinal,
                'ExpiryMonth' => $ExpiryMonth,
                'ExpiryYear' => $ExpiryYear,
                'CVN' => $CVNFinal
            );
        } else {
            $CardDetails = array(
                'Name' => $CardHolderName,
                'Number' => $CreditCardNumber,
                'ExpiryMonth' => $ExpiryMonth,
                'ExpiryYear' => $ExpiryYear,
                'CVN' => $CVN
            );
            $expiry_array = explode('/', $expiry_date);
            $ExpiryMonth = $expiry_array[0];
            $ExpiryYear = $expiry_array[1];
        }
        $Customer = array(
            //'TokenCustomerID' => 915701697882,
            'FirstName' => $first_name,
            'LastName' => $last_name,
            'Street1' => $street_address . ' ' . $street_number,
            'PostalCode' => $postcode,
            'Email' => $email1,
            'Country' => 'AU',
            'CardDetails' => $CardDetails
        );



        Zend_Loader::loadFile('include_eway.php', APPLICATION_PATH . '/../library/eway-rapid-php-master');


        if (0) {
            $apiKey = 'C3AB9CgGmWUyn8oaghTUTDS2DyZmB/j5aPOWLaqtO2QT5/aaFD1UJnX9mDM697Vg7tAHWT';
            $apiPassword = 'bdtLryqv';
            $apiEndpoint = \Eway\Rapid\Client::MODE_SANDBOX;
        }
        $apiKey = '44DD7AhRBX08hfSoic2wv9mAF/Bs5St52K15ISLq6EaA29SCLz7GuY+G6eyiJkDLUvRKRU';
        $apiPassword = '72tQqX16';
        $apiEndpoint = 'production';
        $client = \Eway\Rapid::createClient($apiKey, $apiPassword, $apiEndpoint);



        $response = $client->createCustomer(\Eway\Rapid\Enum\ApiMethod::DIRECT, $Customer);
        $result = array();

        if ($response->getErrors()) {
            foreach ($response->getErrors() as $error) {
                $result['msg'] = "Error: " . \Eway\Rapid::getMessage($error);
                $result['type'] = "error";
                echo json_encode($result);
                exit;
            }
        } else {
            $customer_token_id = $response->Customer->TokenCustomerID;
            $modelCustomer = new Model_Customer();
            $modelCustomer->updateById($customerId, array('customer_token_id' => $customer_token_id));
        }
    }

    public function updateOldFilesAction() {
        $modelServiceAttachment = new Model_ServiceAttachment();
        $modelAttachment = new Model_Attachment();
        $modelAttributeListValueAttachment = new Model_AttributeListValueAttachment();
        $serviceAttachment = $modelServiceAttachment->getAttachmentsForUpdate();
        foreach ($serviceAttachment as $sa) {
            $path = explode('/', $sa['path']);

            $lastOfPath = end($path);

            if (count(explode(' ', $lastOfPath)) > 1) {
                $thumbnail = explode('/', $sa['thumbnail_file']);
                $large = $sa['large_file'] ? explode('/', $sa['large_file']) : null;
                //$compressed = $sa['compressed_file']?explode('/', $sa['compressed_file']):null;

                $lastOfThumbnail = end($thumbnail);
                $lastOfLarge = end($large);
                //$lastOfCompressed = end($compressed);

                $lastOfPathUpdated = $this->clean($lastOfPath);
                $path[count($path) - 1] = $lastOfPathUpdated;
                $pathField = implode('/', $path);
                rename($sa['path'], $pathField);

                //echo " " . $lastOfPathUpdated . " ";
                //echo " " . $pathField . " ";

                $lastOfThumbnailUpdated = $this->clean($lastOfThumbnail);
                $thumbnail[count($thumbnail) - 1] = $lastOfThumbnailUpdated;
                $thumbnailField = implode('/', $thumbnail);
                //copy($sa['thumbnail_file'], $thumbnailField);
                //unlink($sa['thumbnail_file']);

                rename($sa['thumbnail_file'], $thumbnailField);
                //echo " " . $thumbnailField . " ";

                if ($large) {
                    $lastOfLargeUpdated = $this->clean($lastOfLarge);
                    $large[count($large) - 1] = $lastOfLargeUpdated;
                    $largeField = implode('/', $large);
                    //echo " " . $largeField . " ";
                    //copy($sa['large_file'], $largeField);
                    //unlink($sa['large_file']);
                    rename($sa['large_file'], $largeField);
                }
                /* if($compressed){
                  $lastOfCompressedUpdated = $this->clean($lastOfCompressed);
                  $compressed[count($compressed)-1] = $lastOfCompressedUpdated;
                  $compressedField = implode('/',$compressed);
                  //echo " " . $compressedField . " ";
                  //copy($sa['compressed_file'], $compressedField);
                  //unlink($sa['compressed_file']);
                  rename($sa['compressed_file'], $compressedField);
                  } */

                $updatedFields = array(
                    'path' => $pathField,
                    'file_name' => $lastOfPathUpdated,
                    'thumbnail_file' => $thumbnailField,
                    'large_file' => isset($largeField) ? $largeField : null
                        //'compressed_file' => isset($compressedField)? $compressedField : null
                );
                //var_dump($updatedFields);


                $modelAttachment->updateById($sa['attachment_id'], $updatedFields);
            }
        }

        $attributeValueAttachment = $modelAttributeListValueAttachment->getAttachmentsForUpdate();
        foreach ($attributeValueAttachment as $sa) {
            $path = explode('/', $sa['path']);

            $lastOfPath = end($path);

            if (count(explode(' ', $lastOfPath)) > 1) {
                $thumbnail = $sa['thumbnail_file'] ? explode('/', $sa['thumbnail_file']) : null;
                $large = $sa['large_file'] ? explode('/', $sa['large_file']) : null;
                //$compressed = $sa['compressed_file']?explode('/', $sa['compressed_file']):null;

                $lastOfThumbnail = end($thumbnail);
                $lastOfLarge = end($large);
                //$lastOfCompressed = end($compressed);

                $lastOfPathUpdated = $this->clean($lastOfPath);
                $path[count($path) - 1] = $lastOfPathUpdated;
                $pathField = implode('/', $path);
                //copy($sa['path'], $pathField);
                //unlink($sa['path']);
                rename($sa['path'], $pathField);

                //echo " " . $lastOfPathUpdated . " ";
                //echo " " . $pathField . " ";
                if ($thumbnail) {
                    $lastOfThumbnailUpdated = $this->clean($lastOfThumbnail);
                    $thumbnail[count($thumbnail) - 1] = $lastOfThumbnailUpdated;
                    $thumbnailField = implode('/', $thumbnail);

                    //copy($sa['thumbnail_file'], $thumbnailField);
                    //unlink($sa['thumbnail_file']);
                    rename($sa['thumbnail_file'], $thumbnailField);
                    //echo " " . $thumbnailField . " ";
                }
                if ($large) {
                    $lastOfLargeUpdated = $this->clean($lastOfLarge);
                    $large[count($large) - 1] = $lastOfLargeUpdated;
                    $largeField = implode('/', $large);
                    //echo " " . $largeField . " ";
                    //copy($sa['large_file'], $largeField);
                    //unlink($sa['large_file']);
                    rename($sa['large_file'], $largeField);
                }
                /* if($compressed){
                  $lastOfCompressedUpdated = $this->clean($lastOfCompressed);
                  $compressed[count($compressed)-1] = $lastOfCompressedUpdated;
                  $compressedField = implode('/',$compressed);
                  //echo " " . $compressedField . " ";
                  //copy($sa['compressed_file'], $compressedField);
                  //unlink($sa['compressed_file']);
                  rename($sa['compressed_file'], $compressedField);
                  } */

                $updatedFields = array(
                    'path' => $pathField,
                    'file_name' => $lastOfPathUpdated,
                    'thumbnail_file' => isset($thumbnailField) ? $thumbnailField : null,
                    'large_file' => isset($largeField) ? $largeField : null
                        //'compressed_file' => isset($compressedField)? $compressedField : null
                );
                //var_dump($updatedFields);


                $modelAttachment->updateById($sa['attachment_id'], $updatedFields);
            }
        }


        exit;
    }

    public function clean($string) {
        $string = preg_replace('/\'":&*?><>|\//', '', $string); // Removes special chars.  
        return str_replace(' ', '_', $string); // Replaces all spaces with _.
    }

    public function contractorBookingAttendanceAction() {
        $accessToken = $this->request->getParam('access_token', 0);
        $this->checkAccessToken($accessToken);
        $loggedUser = CheckAuth::getLoggedUser();

        $modelBookingAttendance = new Model_BookingAttendance();
        $filters['contractor'] = $loggedUser['user_id'];
        $filters['group'] = 0;

        $contractorBookingAttendance = $modelBookingAttendance->getAll($filters);

        $result = array();

        foreach ($contractorBookingAttendance as $k => $v) {
            $result[$k]['booking_attendance_id'] = $v['booking_attendance_id'];
            $result[$k]['booking_id'] = $v['booking_id'];
            $result[$k]['user_id'] = $v['contractor_id'];
            $result[$k]['check_in_time'] = $v['check_in_time'] ? strtotime($v['check_in_time']) : 0;
            $result[$k]['check_in_distance'] = $v['check_in_distance'] ? $v['check_in_distance'] : 0;
            $result[$k]['check_in_lat'] = $v['check_in_lat'] ? $v['check_in_lat'] : 0;
            $result[$k]['check_in_lon'] = $v['check_in_lon'] ? $v['check_in_lon'] : 0;
            $result[$k]['check_out_time'] = $v['check_out_time'] ? strtotime($v['check_out_time']) : 0;
            $result[$k]['check_out_distance'] = $v['check_out_distance'] ? $v['check_out_distance'] : 0;
            $result[$k]['check_out_lat'] = $v['check_out_lat'] ? $v['check_out_lat'] : 0;
            $result[$k]['check_out_lon'] = $v['check_out_lon'] ? $v['check_out_lon'] : 0;
            $result[$k]['case'] = $v['check_out_time'] ? 'check-out' : 'check-in';
        }
        $data['result'] = $result;
        $data['authrezed'] = 1;
        echo json_encode($data);
        exit;
    }

    public function deleteDiscussionAction() {
        $accessToken = $this->request->getParam('access_token', 0);
        $discussion_id = $this->request->getParam('discussion_id');
        $type = $this->request->getParam('type');

        //$loggedUser = CheckAuth::getLoggedUser();
        $this->checkAccessToken($accessToken);

        $modelBookingDiscussion = new Model_BookingDiscussion();
        $modelComplaintDiscussion = new Model_ComplaintDiscussion();
        $modelContractorDiscussionMongo = new Model_ContractorDiscussionMongo();
        $modelItemImageDiscussion = new Model_ItemImageDiscussion();
        $modelImage = new Model_Image();
        $deleteDis = 0;
        $successDelete = 0;
        $successDeletes = array();

        if (!empty($discussion_id)) {
            if ($type == 'booking' || $type == 'complaint') {
                if ($type == 'booking') {
                    $deleteDis = $modelBookingDiscussion->deleteById($discussion_id);
                } else if ($type == 'complaint') {
                    $deleteDis = $modelComplaintDiscussion->deleteById($discussion_id);
                }
                $itemImageDiscussion = $modelItemImageDiscussion->getByDiscussionIdAndType($discussion_id, $type);

                if ($itemImageDiscussion[0]['group_id']) {
                    $discussionImages = $modelImage->getByGroupId($itemImageDiscussion[0]['group_id']);
                    foreach ($discussionImages as $k => $img) {
                        $successDeletes[$k] = $modelImage->deleteById($img['image_id']);
                        $filters['image_id'] = $img['image_id'];
                        $itemImageDiscussions = $modelItemImageDiscussion->getAll($filters);
                        $data = array(
                            'image_id' => 0
                        );
                        foreach ($itemImageDiscussions as $itemImageDiscussion) {
                            $update = $modelItemImageDiscussion->updateById($itemImageDiscussion['item_image_discussion_id'], $data);
                        }
                    }

                    $success = $deleteDis && !in_array(0, $successDeletes) ? 1 : 0;
                    $data['result'] = $success;
                    $data['authrezed'] = 1;
                    echo json_encode($data);
                    exit;
                } else if ($itemImageDiscussion[0]['image_id']) {
                    $successDelete = $modelImage->deleteById($image_id);
                    $filters['image_id'] = $image_id;
                    $itemImageDiscussions = $modelItemImageDiscussion->getAll($filters);
                    $data = array(
                        'image_id' => 0
                    );
                    foreach ($itemImageDiscussions as $itemImageDiscussion) {
                        $update = $modelItemImageDiscussion->updateById($itemImageDiscussion['item_image_discussion_id'], $data);
                    }

                    $success = $deleteDis && $successDelete ? 1 : 0;
                    $data['result'] = $success;
                    $data['authrezed'] = 1;
                    echo json_encode($data);
                    exit;
                }
            } else if ($type == 'contractor') {
                $deleteDis = $modelContractorDiscussionMongo->deleteById($discussion_id);
            }
        }

        $data['result'] = $deleteDis;
        $data['authrezed'] = 1;
        echo json_encode($data);
        exit;
    }

    public function uploadCustomerSignatureImageAction() {
        $accessToken = $this->request->getParam('access_token', 0);
        $signature_pic = $this->request->getParam('signature_pic', 0);
        $booking_id = $this->request->getParam('booking_id', 0);
        $customer_contact_id = $this->request->getParam('customer_contact_id', 0);
        $customer_id = $this->request->getParam('customer_id', 0);

        $this->checkAccessToken($accessToken);
        $loggedUser = CheckAuth::getLoggedUser();
        $stored_customer_contact_id = 0;
        $type = '';
        if ($customer_contact_id) {
            $stored_customer_contact_id = $customer_contact_id;
            $type = 'secandary';
        } else {
            $stored_customer_contact_id = $customer_id;
            $type = 'primary';
        }

        $modelCustomerBookingSignature = new Model_CustomerBookingSignature();

        $dir = get_config('customer_picture');
        $dir2 = get_config('customer_picture_thumb');
        $dir3 = get_config('customer_picture_thumb_medium');

        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }
        if (!is_dir($dir2)) {
            mkdir($dir2, 0777, true);
        }
        if (!is_dir($dir3)) {
            mkdir($dir3, 0777, true);
        }

        if (isset($signature_pic) && !empty($signature_pic) && !empty($booking_id) && $stored_customer_contact_id && !empty($type)) {
            $image = str_replace('data:image/png;base64,', '', $signature_pic);
            $extension = $this->request->getParam('extension', 'png');
            $image = str_replace(' ', '+', $image);
            $image = base64_decode($image);
            $fileName = "image." . $extension;
            $file = $dir . $fileName;
            $original_path = time() . "_" . $booking_id . "_" . $stored_customer_contact_id . '.' . $extension;
            $success = file_put_contents($file, $image);
            if ($success) {
                $image_saved = copy($copy, $dir . $original_path);
                ImageMagick::create_thumbnail($file, $dir2 . $original_path, 210, 210);
                ImageMagick::create_thumbnail($file, $dir3 . $original_path, 50, 50);

                if (file_exists($file)) {
                    unlink($file);
                }
            }
        }

        if (isset($_FILES['signature_pic']) && !empty($booking_id) && $stored_customer_contact_id && !empty($type)) {
            $source = $_FILES['signature_pic']['tmp_name'];
            $extension = $this->request->getParam('extension', 'png');
            $imageInfo = pathinfo($source);
            $ext = $extension;
            $original_path = time() . '_' . $booking_id . '_' . $stored_customer_contact_id . '.' . $ext;
            $image_saved = copy($source, $dir . $original_path);
            ImageMagick::create_thumbnail($source, $dir2 . $original_path, 210, 210);
            ImageMagick::create_thumbnail($source, $dir3 . $original_path, 50, 50);
        }

        if ($image_saved) {
            $customerBookingSignature = $modelCustomerBookingSignature->getByBookingIdAndCustomerContactId($booking_id, $stored_customer_contact_id);
            if ($customerBookingSignature) {
                $newData = array(
                    'signature_pic' => $original_path
                );

                $where = array();
                $where['booking_id = ?'] = $booking_id;
                $where['customer_contact_id = ?'] = $stored_customer_contact_id;

                $success = $modelCustomerBookingSignature->updateByDifferentCriterias($where, $newData);
            } else {
                $params = array('customer_contact_id' => $stored_customer_contact_id, 'booking_id' => $booking_id, 'type' => $type, 'signature_pic' => $original_path);
                $success = $modelCustomerBookingSignature->insert($params);
            }

            if ($success) {
                $data['msg'] = 'Saved Successfully';
                $data['type'] = 'success';
            } else {
                $data['msg'] = 'No Changes in Customer Booking Signature';
                $data['type'] = 'error';
            }
        }


        $data['authrezed'] = 1;
        echo json_encode($data);
        exit;
    }

    public function customerComplaintsAction() {
        $accessToken = $this->request->getParam('access_token', 0);
        $booking_id = $this->request->getParam('booking_id', 0);
        $customer_id = $this->request->getParam('customer_id', 0);

        $this->checkAccessToken($accessToken, 'customer', $customer_id);

        $modelBooking = new Model_Booking();
        $modelComplaintType = new Model_ComplaintType();
        $booking = $modelBooking->getById($booking_id);

        if ($booking) {
            if ($booking['customer_id'] == $customer_id) {
                $modelComplaint = new Model_Complaint();
                $bookingComplaints = $modelComplaint->getByBookingId($booking_id);
                foreach ($bookingComplaints as &$bookingComplaint) {
                    $complaintType = $modelComplaintType->getById($bookingComplaint['complaint_type_id']);
                    $bookingComplaint['complaint_type'] = $complaintType['name'];
                }
                $data['result'] = $bookingComplaints;
                $data['msg'] = 'success';
            } else {
                $data['result'] = array();
                $data['msg'] = "You don't have permission!";
            }
        } else {
            $data['result'] = array();
            $data['msg'] = "Booking doesn't exist";
        }

        $data['authrezed'] = 1;

        echo json_encode($data);
        exit;
    }

    public function addNewBookingDateByCustomerAction() {
        $accessToken = $this->request->getParam('access_token', 0);
        $booking_id = $this->request->getParam('booking_id', 0);
        $customer_id = $this->request->getParam('customer_id', 0);
        $date = $this->request->getParam('date');
        $start_time = $this->request->getParam('start_time');
        $end_time = $this->request->getParam('end_time');

        $this->checkAccessToken($accessToken, 'customer', $customer_id);

        $modelBookingMultipleDays = new Model_BookingMultipleDays();
        $modelBooking = new Model_Booking();

        $booking = $modelBooking->getById($booking_id);

        if ($booking) {
            if ($booking['customer_id'] == $customer_id) {
                if (!empty($date) && !empty($start_time) && !empty($end_time)) {
                    $startDate = $date . ' ' . $start_time;
                    $endDate = $date . ' ' . $end_time;

                    $bookingData = array(
                        'booking_id' => $booking_id,
                        'booking_start' => php2MySqlTime(strtotime($startDate)),
                        'booking_end' => php2MySqlTime(strtotime($endDate))
                    );

                    $success = $modelBookingMultipleDays->insert($bookingData);
                    if ($success) {
                        $data['msg'] = 'New date added successfully';
                        $data['type'] = 'success';
                    } else {
                        $data['msg'] = 'No changes in booking dates';
                        $data['type'] = 'error';
                    }
                    $data['result'] = $this->getBooking(false, array('booking_id' => $booking['booking_id']), 0, 'customer');
                } else {
                    $data['msg'] = "Please send required information";
                    $data['type'] = 'error';
                    $data['result'] = array();
                }
            } else {

                $data['msg'] = "You don't have permission!";
                $data['type'] = 'error';
                $data['result'] = array();
            }
        } else {
            $data['msg'] = "Booking doesn't exist";
            $data['type'] = 'error';
            $data['result'] = array();
        }

        $data['authrezed'] = 1;

        echo json_encode($data);
        exit;
    }

    public function getRatingDetailsForBookingAction() {
        header('Content-Type: application/json');

        $booking_id = $this->request->getParam('booking_id');
        $accessToken = $this->request->getParam('access_token', 0);

        $this->checkAccessToken($accessToken);

        $loggedUser = CheckAuth::getLoggedUser();

        $contractor_id = $loggedUser['user_id'];


        $modelContractorRate = new Model_ContractorRate();


        $result = $modelContractorRate->getRateDetails($contractor_id, $booking_id);

        $data['result'] = $result;

        echo json_encode($data);
        exit;
    }

    public function getRequestAccessAction() {
        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $this->checkAccessToken($accessToken);
        $bookingId = $this->request->getParam('booking_id', 0);
        MobileNotification::notify($bookingId, "request access");
        echo json_encode(array('request' => '1'));
        exit;
    }

    public function requestEditAccessAction() {

        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $this->checkAccessToken($accessToken);
        $booking_id = $this->request->getParam('booking_id', 0);
        $modelBooking = new Model_Booking();
        $booking = $modelBooking->getById($booking_id);
        // $url = $router->assemble(array('id' => $bookingId), 'requestAccess');
        MobileNotification::notify($booking_id, "request access");

        $data['type'] = "success";
        $data['msg'] = "You will notified when access is granted";

        echo json_encode($data);
        exit;
    }

    public function openAppAction() {
        $booking_id = $this->request->getParam('booking_id');
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
        $booking_view = $this->router->assemble(array('id' => $booking_id), 'bookingView');
        ?>
        <script type="text/javascript">
            var userAgent = navigator.userAgent || navigator.vendor || window.opera;

            if (/android/i.test(userAgent)) {
                window.location = "market://details?id=au.tilecleaners.app";
            } else if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
                window.location = "https://itunes.apple.com/nz/app/tilecleaners/id921943286?mt=8";
            } else {
                var url = "<?php echo $booking_view ?>";
                window.location = url;
            }
        </script>
        <?php
        exit;
    }

    public function salimTestAction() {

        $my_file = 'file2.txt';
        $handle = fopen($my_file, 'a') or die('Cannot open file:  ' . $my_file);
//        $data = '\n'.$_REQUEST.'\n';
        fwrite($handle, '//////////////////// Headers //////////////////////////<br/>');
        foreach (getallheaders() as $name => $value) {
            fwrite($handle, $name . ':' . $value . '<br/> ------------------- <br/>');
        }
        fwrite($handle, '//////////////////// Parameters //////////////////////////<br/>');
        foreach ($_REQUEST as $name2 => $value2) {
            fwrite($handle, $name2 . ':' . $value2 . '<br/> ------------------- <br/>');
        }

        fclose($handle);

        $handle2 = fopen($my_file, 'r');
        $data2 = fread($handle2, filesize($my_file));
        var_dump($data2);
        exit;
    }

    public function getAllCompaniesAction() {
        $modelCompany = new Model_Companies();
        $companies = $modelCompany->getAllForWebService();
        $result['companies'] = $companies;
        echo json_encode($result);
        exit;
    }

    public function inactiveContractorCredentialsAction() {
        $modelAuthRole = new Model_AuthRole();
        $modelAuthCredential = new Model_AuthRoleCredential();
        $contractorRoleId = $modelAuthRole->getRoleIdByName('contractor');
        $credentials = $modelAuthCredential->getByRoleId($contractorRoleId, 'FALSE');
        $result['credentials'] = $credentials;
        echo json_encode($result);
        exit;
    }

    public function addContractorDiscussionAction() {

        header('Content-Type: application/json');
        $accessToken = $this->request->getParam('access_token', 0);
        $user_message = $this->request->getParam('user_message', '');
        $count = $this->request->getParam('count', '');
        $discussionImages = $this->request->getParam('discussion_images', '');
        $is_audio = $this->request->getParam('is_audio', 0);
        $user_message = trim($user_message);

        $modelUser = new Model_User();
        $modelContractorDiscussionMongo = new Model_ContractorDiscussionMongo();

        /* $modelIosUser = new Model_IosUser();
          $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
          $this->iosLoggedUser = $user['id'];

          $loggedUser = CheckAuth::getLoggedUser();
          $data = array('authrezed' => 0);

          //////////by walaa
          $thumbnails_paths = array();
          $originals_paths = array();
          $larges_paths = array();
          $small_paths = array();
          $compressed_paths = array();
          //////////////

          if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
          $data['msg'] = 'wrong access token';
          $data['type'] = 'error';
          echo json_encode($data);
          exit;
          }

          if (empty($loggedUser)) {
          //open new session
          $authrezed = $this->openNewSession($accessToken);
          if (!$authrezed) {
          echo json_encode($data);
          exit;
          }
          } */

        $this->checkAccessToken($accessToken);

        $loggedUser = CheckAuth::getLoggedUser();


        ///new
        $modelContractorInfo = new Model_ContractorInfo();
        $modelImage = new Model_Image();
        $modelContractorDiscussion = new Model_ContractorDiscussion();
        $contractor_id = $loggedUser['user_id'];

        ////by walaa
        $user = $modelUser->getById($loggedUser['user_id']);
        if ($user['role_name'] == 'contractor') {
            $contractorInfo = $modelContractorInfo->getByContractorId($user['user_id']);
            $user['username'] = ucwords($user['username']) . ' - ' . ucwords($contractorInfo['business_name']);
        } else {
            $user['username'] = ucwords($user['username']);
        }
        $contractor = $modelUser->getById($contractor_id);

        $doc = array(
            'contractor_id' => (int) $contractor_id,
            'contractor_name' => $contractor['username'],
            'user_id' => $loggedUser['user_id'],
            'user_name' => $user['username'],
            'avatar' => $user['avatar'],
            'user_message' => $user_message,
            'created' => time(),
            'user_role_name' => $user['role_name'],
            'thumbnail_images' => array(),
            'original_images' => array(),
            'large_images' => array(),
            'small_images' => array(),
            'compressed_images' => array(),
            'seen_by_ids' => "",
            'seen_by_names' => "",
            'visibility' => 2,
            'type' => "contractor_discussion",
            'audio_path' => ""
        );

        $success = $modelContractorDiscussionMongo->insertDiscussion($doc);


        $newDocID = $doc['_id'];
        foreach ($newDocID as $key => $value)
            $disc_id = $value;
        //////end by walaa

        if (isset($user_message) && !empty($user_message)) {
            $params = array('contractor_id' => $contractor_id, 'user_id' => $loggedUser['user_id'], 'user_message' => trim($user_message), 'created' => time());
            $success_status = $modelContractorDiscussion->insert($params);

            $dir = get_config('image_attachment') . '/';
            $subdir = date('Y/m/d/');
            $fullDir = $dir . $subdir;
            if (!is_dir($fullDir)) {
                mkdir($fullDir, 0777, true);
            }


            if ((isset($count) && $count != '')) {
                $image_group = 0;
                if ($count > 1) {
                    $maxGroup = max($modelImage->getMaxGroup());
                    $image_group = $maxGroup['group_id'] + 1;
                }

                for ($i = 0; $i < $count; $i++) {
                    $x = $i + 1;
                    $source = $_FILES['imageDiscussion' . $i]['tmp_name'];
                    $extension = $this->request->getParam('ext' . $i, 'png');
                    $imageInfo = pathinfo($source);
                    $ext = $extension;
                    $successUpload = 0;
                    $fileName = "image_{$i}.{$ext}";
                    $file = $fullDir . $fileName;
                    $image_saved = copy($source, $fullDir . $fileName);
                    if ($image_saved) {
                        $successUpload = 1;
                        $data = array(
                            'user_ip' => ''
                            , 'created_by' => $loggedUser['user_id']
                            , 'image_types_id' => 0
                            , 'group_id' => $image_group
                            , 'user_role' => $loggedUser['role_id']
                        );

                        $id = $modelImage->insert($data);
                        $Itemdata = array(
                            'item_id' => $loggedUser['user_id'],
                            'service_id' => 0,
                            'discussion_id' => $success_status,
                            'image_id' => $id,
                            'type' => 'contractor_discussion',
                            'floor_id' => 0
                        );

                        $modelItemImage = new Model_ItemImage();
                        $Item_image_id = $modelItemImage->insert($Itemdata);
                        $original_path = "original_{$id}.{$ext}";
                        $large_path = "large_{$id}.{$ext}";
                        $small_path = "small_{$id}.{$ext}";
                        $thumbnail_path = "thumbnail_{$id}.{$ext}";
                        $compressed_path = "compressed_{$id}.{$ext}";

                        $data = array(
                            'original_path' => $subdir . $original_path,
                            'large_path' => $subdir . $large_path,
                            'small_path' => $subdir . $small_path,
                            'thumbnail_path' => $subdir . $thumbnail_path,
                            'compressed_path' => $subdir . $compressed_path
                        );
                        $modelImage->updateById($id, $data);
                        /////start added by walaa
                        $mongo_id = $disc_id . $x;
                        $original_path_mongo = "original_{$mongo_id}.{$ext}";
                        $large_path_mongo = "large_{$mongo_id}.{$ext}";
                        $small_path_mongo = "small_{$mongo_id}.{$ext}";
                        $thumbnail_path_mongo = "thumbnail_{$mongo_id}.{$ext}";
                        $compressed_path_mongo = "compressed_{$mongo_id}.{$ext}";

                        $thumbnails_paths[$mongo_id] = $subdir . $thumbnail_path_mongo;
                        $originals_paths[$mongo_id] = $subdir . $original_path_mongo;
                        $larges_paths[$mongo_id] = $subdir . $large_path_mongo;
                        $small_paths[$mongo_id] = $subdir . $small_path_mongo;
                        $compressed_paths[$mongo_id] = $subdir . $compressed_path_mongo;

                        $compressed_saved = copy($source, $fullDir . $compressed_path_mongo);
                        $image_saved = copy($source, $fullDir . $original_path_mongo);
                        ImageMagick::scale_image($source, $fullDir . $large_path_mongo, 510);
                        ImageMagick::scale_image($source, $fullDir . $small_path_mongo, 250);
                        ImageMagick::create_thumbnail($source, $fullDir . $thumbnail_path_mongo, 78, 64);
                        ImageMagick::convert($source, $fullDir . $compressed_path_mongo);
                        ImageMagick::compress_image($source, $fullDir . $compressed_path_mongo);
                        ////////////


                        $compressed_saved = copy($source, $fullDir . $compressed_path);
                        $image_saved2 = copy($source, $fullDir . $original_path);
                        ImageMagick::scale_image($source, $fullDir . $large_path, 510);
                        ImageMagick::scale_image($source, $fullDir . $small_path, 250);
                        ImageMagick::create_thumbnail($source, $fullDir . $thumbnail_path, 78, 64);
                        ImageMagick::convert($source, $fullDir . $compressed_path);
                        ImageMagick::compress_image($source, $fullDir . $compressed_path);
                    }


                    if ($image_saved) {
                        if (file_exists($source)) {
                            unlink($source);
                        }
                        if (file_exists($file)) {
                            unlink($file);
                        }
                    }
                }

                $modelContractorDiscussionMongo->updateDiscImages($disc_id, $thumbnails_paths, $originals_paths, $larges_paths, $small_paths, $compressed_paths);
            }


            if ((isset($discussionImages) && !empty($discussionImages))) {

                $image_group = 0;
                if ($count > 1) {
                    $maxGroup = max($modelImage->getMaxGroup());
                    $image_group = $maxGroup['group_id'] + 1;
                }

                foreach ($discussionImages as $key => $imageData) {
                    $y = $key + 1;

                    if (isset($imageData) && !empty($imageData)) {
                        $image = str_replace('data:image/png;base64,', '', $imageData);
                        $image = str_replace(' ', '+', $image);
                        $image = base64_decode($image);
                        $ext = 'png';
                        $fileName = "image_{$key}.png";
                        $file = $fullDir . $fileName;
                        $success_upload = file_put_contents($file, $image);



                        if ($success_upload) {
                            $successUpload = 1;
                            $data = array(
                                'user_ip' => ''
                                , 'created_by' => $loggedUser['user_id']
                                , 'image_types_id' => 0
                                , 'group_id' => $image_group
                                , 'user_role' => $loggedUser['role_id']
                            );



                            $id = $modelImage->insert($data);
                            $Itemdata = array(
                                'item_id' => $loggedUser['user_id'],
                                'service_id' => 0,
                                'discussion_id' => $success_status,
                                'image_id' => $id,
                                'type' => 'contractor_discussion',
                                'floor_id' => 0
                            );



                            $modelItemImage = new Model_ItemImage();
                            $Item_image_id = $modelItemImage->insert($Itemdata);
                            $original_path = "original_{$id}.png";
                            $large_path = "large_{$id}.png";
                            $small_path = "small_{$id}.png";
                            $thumbnail_path = "thumbnail_{$id}.png";
                            $compressed_path = "compressed_{$id}.png";

                            $data = array(
                                'original_path' => $subdir . $original_path,
                                'large_path' => $subdir . $large_path,
                                'small_path' => $subdir . $small_path,
                                'thumbnail_path' => $subdir . $thumbnail_path,
                                'compressed_path' => $subdir . $compressed_path
                            );

                            $modelImage->updateById($id, $data);
                            $compressed_saved = copy($file, $fullDir . $compressed_path);
                            $image_saved = copy($file, $fullDir . $original_path);
                            ImageMagick::scale_image($file, $fullDir . $large_path, 510);
                            ImageMagick::scale_image($file, $fullDir . $small_path, 250);
                            ImageMagick::create_thumbnail($file, $fullDir . $thumbnail_path, 78, 64);
                            ImageMagick::convert($file, $fullDir . $compressed_path);
                            ImageMagick::compress_image($file, $fullDir . $compressed_path);

                            /////start added by walaa
                            $mongo_id = $disc_id . $y;
                            $original_path_mongo = "original_{$mongo_id}.{$ext}";
                            $large_path_mongo = "large_{$mongo_id}.{$ext}";
                            $small_path_mongo = "small_{$mongo_id}.{$ext}";
                            $thumbnail_path_mongo = "thumbnail_{$mongo_id}.{$ext}";
                            $compressed_path_mongo = "compressed_{$mongo_id}.{$ext}";

                            $thumbnails_paths[$mongo_id] = $subdir . $thumbnail_path_mongo;
                            $originals_paths[$mongo_id] = $subdir . $original_path_mongo;
                            $larges_paths[$mongo_id] = $subdir . $large_path_mongo;
                            $small_paths[$mongo_id] = $subdir . $small_path_mongo;
                            $compressed_paths[$mongo_id] = $subdir . $compressed_path_mongo;

                            $compressed_saved = copy($file, $fullDir . $compressed_path_mongo);
                            $image_saved = copy($file, $fullDir . $original_path_mongo);
                            ImageMagick::scale_image($file, $fullDir . $large_path_mongo, 510);
                            ImageMagick::scale_image($file, $fullDir . $small_path_mongo, 250);
                            ImageMagick::create_thumbnail($file, $fullDir . $thumbnail_path_mongo, 78, 64);
                            ImageMagick::convert($file, $fullDir . $compressed_path_mongo);
                            ImageMagick::compress_image($file, $fullDir . $compressed_path_mongo);
                            ////////////


                            if (file_exists($file)) {
                                unlink($file);
                            }
                        }
                    }
                }
                $modelContractorDiscussionMongo->updateDiscImages($disc_id, $thumbnails_paths, $originals_paths, $larges_paths, $small_paths, $compressed_paths);
            }

            MobileNotification::notify(0, 'new contractor discussion', array('contractor_id' => $loggedUser['user_id'], 'discussion_id' => $disc_id));
        }

        //
        if (isset($is_audio) && $is_audio == 1) {
            $params = array('contractor_id' => $contractor_id, 'user_id' => $loggedUser['user_id'], 'user_message' => '', 'created' => time());
            $success_status = $modelContractorDiscussion->insert($params);
            $dir = get_config('audio_attachment') . '/';
            $subdir = date('Y/m/d/');
            $fullDir = $dir . $subdir;
            if (!is_dir($fullDir)) {
                mkdir($fullDir, 0777, true);
            }
            $source = $_FILES['audio_discussion']['tmp_name'];
            $extension = $this->request->getParam('audio_ext', 'mp3');
            $successAudioUpload = 0;
            $fileName = "audio.{$extension}";
            $file = $fullDir . $fileName;
            $audio_saved = copy($source, $fullDir . $fileName);

            if ($audio_saved) {
                $successAudioUpload = 1;
                /*
                  $data = array(
                  'user_ip' => ''
                  , 'created_by' => $loggedUser['user_id']
                  , 'user_role' => $loggedUser['role_id']
                  , 'created' => time()
                  , 'discussion_id' => $success_status
                  ); */
                //$modelAudio = new Model_Audio();
                //$id = $modelAudio->insert($data);
                $path = "audio_{$success_status}.{$extension}";
                $data = array(
                    'audio_path' => $_SERVER['HTTP_HOST'] . '/uploads/audio_attachment/' . $subdir . $path
                );
                //$modelAudio->updateById($id, $data);
                $audio_saved = copy($source, $fullDir . $path);

                $modelContractorDiscussionMongo->updateById($disc_id, $data);
            }
            if ($audio_saved) {
                if (file_exists($source)) {
                    unlink($source);
                }
                if (file_exists($file)) {
                    unlink($file);
                }
            }
            MobileNotification::notify(0, 'new contractor discussion', array('contractor_id' => $loggedUser['user_id'], 'discussion_id' => $disc_id));
        }

        $data = array();
        $data['authrezed'] = 1;
        if ($success || $user_message || $successAudioUpload) {
            $data['msg'] = 'Changed Successfully';
            $data['type'] = 'success';
        } else {
            $data['msg'] = 'No changes';
            $data['type'] = 'error';
        }

        echo json_encode($data);
        exit;
    }

    public function checkAppVersionAction() {
        $latestIosApp = 2.5;
        $update_type = 'optional';

        $currentVersion = $this->request->getParam('current_version', 0);

        if ($currentVersion < $latestIosApp) {
            $data['update_type'] = $update_type;
            $data['storeLink'] = 'https://itunes.apple.com/il/app/tilecleaners/id921943286?mt=8';
            $data['msg'] = 'There is a new Version ,Please update the application to be able to use it.';
            $data['type'] = 'success';
        } else {
             $update_type= "optional";
            $data['update_type'] = $update_type;
           
            $data['storeLink'] = 'https://itunes.apple.com/il/app/tilecleaners/id921943286?mt=8';
            $data['msg'] = 'Your app up to date.';
            $data['type'] = 'error';
        }
        echo json_encode($data);
        exit;
    }

}
?>
