<?php

class Invoices_PaymentController extends Zend_Controller_Action {

    private $request;
    private $router;
    private $loggedUser;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'invoices';
        $this->view->sub_menu = 'payment';

        //
        // check Auth for logged user
        //
        $this->loggedUser = CheckAuth::getLoggedUser();
    }

    /**
     * Items list action
     */
    public function indexAction() {
		
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('paymentList'));

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'received_date');
        $sortingMethod = $this->request->getParam('method', 'DESC');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());
        $bookingId = $this->request->getParam('booking_id');

        //
        // Load model
        //
        $modelPayment = new Model_Payment();
        $modelBookingInvoice = new Model_BookingInvoice();
		
        //
        // Trim Filters
        //
        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }
        if ($bookingId) {
            $filters['booking_id'] = $bookingId;
        }

        //
        //check if can see his or assigned invoice
        //
        if ($bookingId) {
            $invoice = $modelBookingInvoice->getByBookingId($bookingId);

            if (!$modelBookingInvoice->checkIfCanSeeHisOrAssignedInvoice($invoice['id'])) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        //
        // get data list
        //
		
		//Changed by Salim
       $paymentData = '';
		if(isset($filters['keywords'])){
			$paymentData = $modelPayment->getByRefernce($filters['keywords'] , "{$orderBy} {$sortingMethod}", $pager);
			if(empty($paymentData)){
				$paymentData = $modelPayment->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);
			}
		}else{
			$paymentData = $modelPayment->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);
		}
		
        $this->view->data = $paymentData;

        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
        $this->view->booking_id = $bookingId;
		
    }

    /**
     * Add new item action
     */
    public function addAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('paymentAdd'));

        //
        // get request parameters
        //
        $receivedDate = $this->request->getParam('received_date');
        $bankCharges = $this->request->getParam('bank_charges');
        $amount = $this->request->getParam('amount');
        $description = $this->request->getParam('description');
        $reference = $this->request->getParam('reference');
        $amountWithheld = $this->request->getParam('amount_withheld');
        $withholdingTax = $this->request->getParam('withholding_tax');
        $isAcknowledgment = $this->request->getParam('is_acknowledgment');
        $paymentTypeId = $this->request->getParam('payment_type_id');
        $bookingId = $this->request->getParam('booking_id', 0);
        $contractorId = $this->request->getParam('contractor_id', 0);

        //
        // Load MODEL
        //
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelBooking = new Model_Booking();
        $modelPayment = new Model_Payment();
        $modelContractorServiceBookingTemp = new Model_ContractorServiceBookingTemp();

        //
        //get booking 
        //
        $booking = $modelBooking->getById($bookingId);


        if (!$booking) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Invalid Booking"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }

        if (!$modelBooking->checkIfCanEditBooking($booking['booking_id'])) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }

        $totalAmountDetails = $modelBooking->getTotalAmountBookingDetails($bookingId);

        $approvedPayment = $modelPayment->getTotalAmount(array('booking_id' => $booking['booking_id'], 'is_approved' => 'yes'));
        if ($approvedPayment >= $totalAmountDetails['total_amount']) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "fully Paid"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $allPayment = $modelPayment->getTotalAmount(array('booking_id' => $booking['booking_id'], 'is_approved' => 'all'));
        if ($allPayment >= $totalAmountDetails['total_amount']) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "fully Paid,Check Unapproved Payment"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // init action form
        //
        $options = array(
            'booking_id' => $bookingId
        );
        if ($booking) {
            $options['amount'] = $totalAmountDetails['total_amount'] - $allPayment;
			$options['booking_end'] = $booking['booking_end'];
        }
        $form = new Invoices_Form_Payment($options);

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'received_date' => strtotime($receivedDate),
                    'bank_charges' => round($bankCharges, 2),
                    'amount' => round($amount, 2),
                    'description' => $description,
                    'payment_type_id' => $paymentTypeId,
                    'booking_id' => $booking['booking_id'],
                    'customer_id' => $booking['customer_id'],
                    'user_id' => $this->loggedUser['user_id'],
                    'created' => time(),
                    'reference' => $reference,
                    'amount_withheld' => round($amountWithheld, 2),
                    'withholding_tax' => $withholdingTax,
                    'is_acknowledgment' => $isAcknowledgment,
                    'contractor_id' => $contractorId
                );

                $success = $modelPayment->insert($data);

                if ($success) {
                    $modelBookingInvoice->updateByBookingId($booking['booking_id'], array('invoice_type' => 'open'));
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully, Please Approved this Payment"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Payment"));
                }

                if (CheckAuth::checkCredential(array('paymentList'))) {
                    $this->_redirect($this->router->assemble(array('booking_id' => $booking['booking_id']), 'bookingPaymentList'));
                } else {
                    $this->_redirect($this->router->assemble(array(), 'booking'));
                }
            }
        }

        //
        //send booking Invoice to view
        //
        $bookingInvoice = $modelBookingInvoice->getByBookingId($bookingId);
        $modelBookingInvoice->fill($bookingInvoice, array('booking', 'customer'));

        $this->view->bookingInvoice = $bookingInvoice;
        $this->view->form = $form;
    }

    public function deleteAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('paymentDelete'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $bookingId = $this->request->getParam('booking_id', 0);

        //
        // Load MODEL
        //
        $modelBooking = new Model_Booking();
        $modelPayment = new Model_Payment();

        //
        //Validation
        //
        if (!$modelBooking->checkIfCanEditBooking($bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }

        $payment = $modelPayment->getByIdAndBookingId($id, $bookingId);
        if ($payment['is_approved']) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Could not delete Approved Payment, Please change to Unapproved to delete"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        if ($payment) {
            //delete payment
            $modelPayment->deleteById($id);
        }

        $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
    }

    public function editAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('paymentEdit'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $receivedDate = $this->request->getParam('received_date');
        $bankCharges = $this->request->getParam('bank_charges');
        $amount = $this->request->getParam('amount');
        $description = $this->request->getParam('description');
        $reference = $this->request->getParam('reference');
        $amountWithheld = $this->request->getParam('amount_withheld');
        $withholdingTax = $this->request->getParam('withholding_tax');
        $isAcknowledgment = $this->request->getParam('is_acknowledgment');
        $paymentTypeId = $this->request->getParam('payment_type_id');
        $contractorId = $this->request->getParam('contractor_id', 0);

		
        //
        // Load Model
        //
        $modelPayment = new Model_Payment();
        $modelBooking = new Model_Booking();
        $modelBookingInvoice = new Model_BookingInvoice();

        //
        //Validation
        //
        $payment = $modelPayment->getById($id);
        if ($payment['is_approved']) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Could not edit Approved Payment, Please change to Unapproved to edit"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $booking = $modelBooking->getById($payment['booking_id']);
		// get company name for customer 
		
		
		
		
        if (!$booking) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Invalid Booking"));
            $this->_redirect($this->router->assemble(array(), 'invoices'));
        }

        if (!$modelBooking->checkIfCanEditBooking($booking['booking_id'])) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }

        $approvedPayment = $modelPayment->getTotalAmount(array('booking_id' => $booking['booking_id'], 'is_approved' => 'yes'));
        if ($approvedPayment >= $booking['qoute']) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "fully Paid"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        //calculate amount
        //
        $allPayment = $modelPayment->getTotalAmount(array('booking_id' => $booking['booking_id'], 'is_approved' => 'all'));
        $bookingAmount = $booking['qoute'] - $allPayment + $payment['amount'];

        //
        //
        // init action form
        //
        $options = array(
            'booking_id' => $booking['booking_id'],
            'mode' => 'update',
            'payment' => $payment,
            'amount' => $bookingAmount,
        );
		
        $form = new Invoices_Form_Payment($options);

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'received_date' => strtotime($receivedDate),
                    'bank_charges' => round($bankCharges, 2),
                    'amount' => round($amount, 2),
                    'description' => $description,
                    'payment_type_id' => $paymentTypeId,
                    'booking_id' => $booking['booking_id'],
                    'customer_id' => $booking['customer_id'],
                    'user_id' => $this->loggedUser['user_id'],
                    'created' => time(),
                    'reference' => $reference,
                    'amount_withheld' => round($amountWithheld, 2),
                    'withholding_tax' => $withholdingTax,
                    'is_acknowledgment' => $isAcknowledgment,
                    'contractor_id' => $contractorId
                );

                $success = $modelPayment->updateById($id, $data);
                if ($success) {
                    $modelBookingInvoice->updateByBookingId($booking['booking_id'], array('invoice_type' => 'open'));
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully, Please Approved this Payment"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Payment"));
                }

                if (CheckAuth::checkCredential(array('paymentList'))) {
                    $this->_redirect($this->router->assemble(array('booking_id' => $booking['booking_id']), 'bookingPaymentList'));
                } else {
                    $this->_redirect($this->router->assemble(array(), 'invoices'));
                }
            }
        }

        //
        //send booking Invoice to view
        //
        $bookingInvoice = $modelBookingInvoice->getByBookingId($booking['booking_id']);
        $modelBookingInvoice->fill($bookingInvoice, array('booking', 'customer'));
		
		
		if($_SERVER['REMOTE_ADDR'] == '176.106.46.142'){
		//	var_dump($bookingInvoice);
		//	exit;
		}
        $this->view->bookingInvoice = $bookingInvoice;
        $this->view->form = $form;
    }

    public function paymentApproveAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('paymentApprove'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $bookingId = $this->request->getParam('booking_id', 0);

        //
        // Load Model
        //
        $modelUser = new Model_User();
        $modelPayment = new Model_Payment();
        $modelBooking = new Model_Booking();
        $modelCustomer = new Model_Customer();
        $modelPaymentType = new Model_PaymentType();
        $modelBookingAddress = new Model_BookingAddress();
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();

        //
        //get booking 
        //
        $booking = $modelBooking->getById($bookingId);
        if (!$booking) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Invalid Booking"));
            $this->_redirect($this->router->assemble(array(), 'invoices'));
        }

        if ($booking['is_change'] == 1) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This invoice need To Approve Change Before Approve Payment "));
            $this->_redirect($this->router->assemble(array('id' => $booking['booking_id']), 'approvedService') . '?redirect_to_payment=true');
        }

        $invoice = $modelBookingInvoice->getByBookingId($bookingId);
        $payment = $modelPayment->getByIdAndBookingId($id, $booking['booking_id']);

        if ($payment) {
            $success = $modelPayment->updateById($id, array('is_approved' => 1));

            if ($success) {

                $paidAmount = $modelPayment->getTotalAmount(array('booking_id' => $booking['booking_id'], 'is_approved' => 'yes'));

                if ($paidAmount < 0) {
                    $paidAmount = 0;
                }

                $modelBooking->updateById($booking['booking_id'], array('paid_amount' => round($paidAmount, 2)));

                if ($paidAmount >= $booking['qoute']) {
                    $modelBookingInvoice->updateByBookingId($booking['booking_id'], array('invoice_type' => 'closed'));
                } else {
                    $modelBookingInvoice->updateByBookingId($booking['booking_id'], array('invoice_type' => 'open'));
                }

                // send email to customer
                if ($payment['is_acknowledgment']) {

                    //
                    // filling extra data
                    //
                    $customer = $modelCustomer->getById($booking['customer_id']);
                    $user = $modelUser->getById($booking['created_by']);
                    $paymentType = $modelPaymentType->getById($payment['payment_type_id']);

                    $template_params = array(
                        //payment
                        '{payment_type}' => $paymentType['payment_type'],
                        '{payment_received_date}' => date('Y-m-d', $payment['received_date']),
                        '{payment_amount}' => number_format($payment['amount'], 2),
                        '{payment_reference}' => $payment['reference'],
                        //booking
                        '{booking_num}' => $booking['booking_num'],
                        '{total_without_tax}' => number_format($booking['sub_total'], 2),
                        '{gst_tax}' => number_format($booking['gst'], 2),
                        '{total_with_tax}' => number_format($booking['qoute'], 2),
                        '{description}' => $booking['description'] ? $booking['description'] : '',
                        '{booking_created}' => date('d/m/Y', $booking['created']),
                        '{booking_created_by}' => ucwords($user['username']),
                        '{booking_start}' => date("F j, Y, g:i a", strtotime($booking['booking_start'])),
                        '{booking_address}' => get_line_address($modelBookingAddress->getByBookingId($booking['booking_id'])),
                        '{service}' => nl2br($modelContractorServiceBooking->getBookingAsText($booking['booking_id'])),
                        //customer
                        '{customer_name}' => get_customer_name($customer),
                        '{customer_first_name}' => isset($customer['first_name']) && $customer['first_name'] ? ucwords($customer['first_name']) : '',
                        '{customer_last_name}' => isset($customer['last_name']) && $customer['last_name'] ? ' ' . ucwords($customer['last_name']) : '',
                        '{customer_contacts}' => nl2br($modelCustomer->getCustomerContacts($booking['customer_id'])),
                    );

                    $customer = $modelCustomer->getById($booking['customer_id']);

                    $email_log = array(
                        'reference_id' => $invoice['id'],
                        'type' => 'invoice'
                    );

                    $to = array();
                    if ($customer['email1']) {
                        $to[] = $customer['email1'];
                    }
                    if ($customer['email2']) {
                        $to[] = $customer['email2'];
                    }
                    if ($customer['email2']) {
                        $to[] = $customer['email3'];
                    }
                    $to = implode(',', $to);

                    if ($to) {
                        //send email
                        EmailNotification::sendEmail(array('to' => $to), 'payment_acknowledgment', $template_params, $email_log);
                    }
                }
            }

            if ($success) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Payment Approved successfully"));
            } else {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Could not Approved the Payment"));
            }
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }


        $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Could not Approved the Payment"));
        $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
    }

    public function paymentUnapproveAction() {
		
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('paymentApprove'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $bookingId = $this->request->getParam('booking_id', 0);

        //
        // Load Model
        //
        $modelBooking = new Model_Booking();
        $modelPayment = new Model_Payment();
        $modelBookingInvoice = new Model_BookingInvoice();

        //
        //get booking 
        //
        $booking = $modelBooking->getById($bookingId);
        if (!$booking) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Invalid Booking"));
            $this->_redirect($this->router->assemble(array(), 'invoices'));
        }

        $payment = $modelPayment->getById($id);
        if ($payment) {
            $success = $modelPayment->updateById($id, array('is_approved' => 0));

            if ($success) {
                $paidAmount = $modelPayment->getTotalAmount(array('booking_id' => $booking['booking_id'], 'is_approved' => 'yes'));

                if ($paidAmount < 0) {
                    $paidAmount = 0;
                }

                $modelBooking->updateById($booking['booking_id'], array('paid_amount' => round($paidAmount, 2)));

                if ($paidAmount >= $booking['qoute']) {
                    $modelBookingInvoice->updateByBookingId($booking['booking_id'], array('invoice_type' => 'closed'));
                } else {
                    $modelBookingInvoice->updateByBookingId($booking['booking_id'], array('invoice_type' => 'open'));
                }
            }

            if ($success) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Payment Unapproved successfully"));
            } else {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Could not Unapproved the Payment"));
            }
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Could not Unapproved the Payment"));
        $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
    }

    public function statmentAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('statment'));

        //
        // get params 
        //
        $invoiceId = $this->request->getParam('id', 0);

        // Load Model
        $modelRefund = new Model_Refund();
        $modelPayment = new Model_Payment();
        $modelCustomer = new Model_Customer();
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();

        //
        // geting data
        //
        $invoice = $modelBookingInvoice->getById($invoiceId);
        $modelBookingInvoice->fill($invoice, array('booking'));

        //
        // validation
        //
        if (!$invoice) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Invoice not exist"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        if (!CheckAuth::checkIfCanHandelAllCompany('invoice', $invoiceId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        if (!$modelBookingInvoice->checkIfCanSeeHisOrAssignedInvoice($invoiceId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $customer = $modelCustomer->getById($invoice['booking']['customer_id']);
        $modelCustomer->fill($customer, array('customer_commercial_info', 'customer_contacts'));

        $data = array();
        $payments = $modelPayment->getAll(array('booking_id' => $invoice['booking_id'], 'is_approved' => 'yes'));
        foreach ($payments as $payment) {
            $data['payment_' . $payment['payment_id']] = $payment['received_date'];
        }
        $refunds = $modelRefund->getAll(array('booking_id' => $invoice['booking_id'], 'is_approved' => 'yes'));
        foreach ($refunds as $refund) {
            $data['refund_' . $refund['refund_id']] = $refund['received_date'];
        }
        array_multisort(array_values($data), $data);

        $paymentsAndRefunds = array();
        foreach ($data as $key => $value) {

            $type_id = explode("_", $key);
            $type = $type_id[0];
            $id = $type_id[1];

            if ($type == 'payment') {
                $payment = $modelPayment->getById($id);

                $payment['discount'] = false;
                $paymentsAndRefunds[] = array('type' => 'payment', 'data' => $payment);
            } else if ($type == 'refund') {
                $refund = $modelRefund->getById($id);

                $refund['discount'] = false;
                $paymentsAndRefunds[] = array('type' => 'refund', 'data' => $refund);

                $refund['discount'] = true;
                $paymentsAndRefunds[] = array('type' => 'payment', 'data' => $refund);
            }
        }

        $this->view->invoice = $invoice;
        $this->view->booking = $invoice['booking'];
        $this->view->bookingServices = $modelContractorServiceBooking->getByBookingId($invoice['booking']['booking_id']);
        $this->view->customer = $customer;
        $this->view->customer_commercial_info = $customer['customer_commercial_info'];
        $this->view->customerContacts = $customer['customer_contacts'];
        $this->view->paymentsAndRefunds = $paymentsAndRefunds;

        echo $this->view->render('payment/statment-body.phtml');
        exit;
    }

    public function sendStatmentsAsEmailAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('paymentStatmentSendAsEmail'));

        //
        // get params 
        //
        $invoiceId = $this->request->getParam('id', 0);

        //
        // load models
        //
        $modelUser = new Model_User();
        $modelRefund = new Model_Refund();
        $modelPayment = new Model_Payment();
        $modelCustomer = new Model_Customer();
        $modelBookingAddress = new Model_BookingAddress();
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();

        //
        // geting data
        //
        $invoice = $modelBookingInvoice->getById($invoiceId);
        $modelBookingInvoice->fill($invoice, array('booking', 'number_of_due_days', 'due_date'));

        //
        // validation
        //
        if (!$invoice) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Invoice not exist"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        if (!CheckAuth::checkIfCanHandelAllCompany('invoice', $invoiceId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        //check in can see his or assigned invoices
        if (!$modelBookingInvoice->checkIfCanSeeHisOrAssignedInvoice($invoiceId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //get extra data
        $customer = $modelCustomer->getById($invoice['booking']['customer_id']);
        $modelCustomer->fill($customer, array('customer_commercial_info', 'customer_contacts'));

        $user = $modelUser->getById($invoice['booking']['created_by']);


        $data = array();
        $payments = $modelPayment->getAll(array('booking_id' => $invoice['booking_id'], 'is_approved' => 'yes'));
        foreach ($payments as $payment) {
            $data['payment_' . $payment['payment_id']] = $payment['received_date'];
        }
        $refunds = $modelRefund->getAll(array('booking_id' => $invoice['booking_id'], 'is_approved' => 'yes'));
        foreach ($refunds as $refund) {
            $data['refund_' . $refund['refund_id']] = $refund['received_date'];
        }
        array_multisort(array_values($data), $data);

        $paymentsAndRefunds = array();
        foreach ($data as $key => $value) {

            $type_id = explode("_", $key);
            $type = $type_id[0];
            $id = $type_id[1];

            if ($type == 'payment') {
                $payment = $modelPayment->getById($id);

                $payment['discount'] = false;
                $paymentsAndRefunds[] = array('type' => 'payment', 'data' => $payment);
            } else if ($type == 'refund') {
                $refund = $modelRefund->getById($id);

                $refund['discount'] = false;
                $paymentsAndRefunds[] = array('type' => 'refund', 'data' => $refund);

                $refund['discount'] = true;
                $paymentsAndRefunds[] = array('type' => 'payment', 'data' => $refund);
            }
        }

        $view = new Zend_View();
        $view->setScriptPath(APPLICATION_PATH . '/modules/invoices/views/scripts/payment');
        $view->invoice = $invoice;
        $view->booking = $invoice['booking'];
        $view->bookingServices = $modelContractorServiceBooking->getByBookingId($invoice['booking']['booking_id']);
        $view->customer = $customer;
        $view->customer_commercial_info = $customer['customer_commercial_info'];
        $view->customerContacts = $customer['customer_contacts'];
        $view->paymentsAndRefunds = $paymentsAndRefunds;

        $statmentBody = $view->render('statment-body.phtml');

        $template_params = array(
            //invoice
            '{invoice_num}' => $invoice['invoice_num'],
            '{invoice_created}' => date('d/m/Y', $invoice['created']),
            '{due}' => $invoice['due'] > 1 ? $invoice['due'] . ' Days' : $invoice['due'] . ' Day',
            '{due_date}' => $invoice['due_date'] > 1 ? $invoice['due_date'] . ' Days' : $invoice['due_date'] . ' Day',
            '{statment_view}' => $statmentBody,
            //booking
            '{booking_num}' => $invoice['booking']['booking_num'],
            '{total_without_tax}' => number_format($invoice['booking']['sub_total'], 2),
            '{gst_tax}' => number_format($invoice['booking']['gst'], 2),
            '{total_with_tax}' => number_format($invoice['booking']['qoute'], 2),
            '{paid_amount}' => number_format($invoice['booking']['paid_amount'], 2),
            '{unpaid_amount}' => number_format($invoice['booking']['qoute'] - $invoice['booking']['paid_amount'], 2),
            '{description}' => $invoice['booking']['description'] ? $invoice['booking']['description'] : '',
            '{booking_created}' => date('d/m/Y', $invoice['booking']['created']),
            '{booking_created_by}' => ucwords($user['username']),
            '{booking_start}' => date("F j, Y, g:i a", strtotime($invoice['booking']['booking_start'])),
            '{booking_address}' => get_line_address($modelBookingAddress->getByBookingId($invoice['booking']['booking_id'])),
            '{service}' => nl2br($modelContractorServiceBooking->getBookingAsText($invoice['booking']['booking_id'], true)),
            //customer
            '{customer_name}' => get_customer_name($customer),
            '{customer_first_name}' => isset($customer['first_name']) && $customer['first_name'] ? ucwords($customer['first_name']) : '',
            '{customer_last_name}' => isset($customer['last_name']) && $customer['last_name'] ? ' ' . ucwords($customer['last_name']) : '',
            '{customer_contacts}' => nl2br($modelCustomer->getCustomerContacts($invoice['booking']['customer_id'])),
        );

        $modelEmailTemplate = new Model_EmailTemplate();
        $emailTemplate = $modelEmailTemplate->getEmailTemplate('send_invoice_statment_as_email', $template_params);

        $body = $emailTemplate['body'];
        $subject = $emailTemplate['subject'];
        $to = array();
        if ($customer['email1']) {
            $to[] = $customer['email1'];
        }
        if ($customer['email2']) {
            $to[] = $customer['email2'];
        }
        if ($customer['email2']) {
            $to[] = $customer['email3'];
        }
        $to = implode(',', $to);

        if ($this->request->isPost()) {
            $to = $this->request->getParam('to');
            $cc = $this->request->getParam('cc');
            $subject = $this->request->getParam('subject');
            $body = $this->request->getParam('body');

            $params = array(
                'to' => $to,
                'cc' => $cc,
                'body' => $body,
                'subject' => $subject
            );

            $error_mesages = array();
            if (EmailNotification::validation($params, $error_mesages)) {

                // create pdf
                $pdfPath = createPdfPath();
                $destination = $pdfPath['fullDir'] . 'Statment_' . $invoice['invoice_num'] . '.pdf';
                wkhtmltopdf($statmentBody, $destination);
                $params['attachment'] = $destination;

                $success = EmailNotification::sendEmail($params, '', array(), array('reference_id' => $invoice['id'], 'type' => 'invoice'));

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Invoice statment sent successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Could not send the Invoice statment"));
                }

                echo 1;
                exit;
            }
            $this->view->error_mesages = $error_mesages;
        }

        $this->view->invoice = $invoice;
        $this->view->to = $to;
        $this->view->subject = $subject;
        $this->view->body = $body;
        $this->view->cc = isset($cc) ? $cc : '';

        echo $this->view->render('payment/send-invoice-statment-as-email.phtml');
        exit;
    }

    public function checkStatusToAddPaymentAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('PaymentAdd'));

        $bookingId = $this->request->getParam('booking_id', 0);

        $modelBooking = new Model_Booking();
        $modelBookingStatus = new Model_BookingStatus();
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelDueDate = new Model_DueDate();
        $modelBookingDueDate = new Model_BookingDueDate();

        if ($modelBooking->checkBookingIfAccepted($bookingId)) {
            $booking = $modelBooking->getById($bookingId);

            $bookingStatus = $modelBookingStatus->getById($booking['status_id']);

            if ($bookingStatus['name'] == "COMPLETED" || $bookingStatus['name'] == "IN PROGRESS" || $bookingStatus['name'] == "FAILED") {
				$bookingInvoice = $modelBookingInvoice->getByBookingId($bookingId);
				/// we create new record if there is no previous booking for this booking
                if ($booking['convert_status'] != "invoice" && empty($bookingInvoice) ) {
				
                    $modelBooking->updateById($bookingId, array('convert_status' => 'invoice'));

                    $data = array(
                        'booking_id' => $bookingId,
                        'invoice_type' => 'open',
                        'created' => time(),
                    );
                    $modelBookingInvoice->insert($data);

                    $dueDateId = $modelDueDate->getDefaultId();
                    $data = array(
                        'due_date_id' => $dueDateId['id']
                    );

                    $bookingDueDate = $modelBookingDueDate->getByBookingId($bookingId);

                    if ($bookingDueDate) {
                        $modelBookingDueDate->updateById($bookingDueDate['id'], $data);
                    } else {
                        $data['booking_id'] = $bookingId;
                        $modelBookingDueDate->insert($data);
                    }
                }
                $this->_redirect($this->router->assemble(array('booking_id' => $bookingId), 'paymentAdd'));
            } else {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Only status (COMPLETED, IN PROGRESS, FAILD) can be convert to Invoice"));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            }
        } else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You dont Have Permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
    }

    public function markPaymentAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('markPayment'));

        $payment_id = $this->request->getParam('payment_id', 0);
        $comment = $this->request->getParam('comment', '');


        $modelPayment = new Model_Payment();
        $payment = $modelPayment->getById($payment_id);

        $form = new Invoices_Form_MarkPayment(array('payment' => $payment));

        //
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $success = $modelPayment->updateById($payment_id, array('is_mark' => 1, 'mark_comment' => $comment));

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "mark Payment"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in  Mark Payment"));
                }

                echo 1;
                exit;
            }
        }

        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('payment/mark-payment.phtml');
        exit;
    }

    public function unmarkPaymentAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('unmarkPayment'));

        $payment_id = $this->request->getParam('payment_id', 0);

        $modelPayment = new Model_Payment();
        $success = $modelPayment->updateById($payment_id, array('is_mark' => 0, 'mark_comment' => ''));

        if ($success) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Unmark Payment"));
        } else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in  Mark Payment"));
        }
        echo 1;
        exit;
    }

    public function modifyMarkCommentAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('markPayment'));

        $payment_id = $this->request->getParam('payment_id', 0);
        $comment = $this->request->getParam('comment', '');


        $modelPayment = new Model_Payment();
        $payment = $modelPayment->getById($payment_id);

        $form = new Invoices_Form_MarkPayment(array('payment' => $payment, 'mode' => 'update'));

        //
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $success = $modelPayment->updateById($payment_id, array('mark_comment' => $comment));

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "mark Payment"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in  Mark Payment"));
                }

                echo 1;
                exit;
            }
        }

        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('payment/mark-payment.phtml');
        exit;
    }

    public function markPaymentViewAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('markPaymentView'));

        $payment_id = $this->request->getParam('payment_id', 0);


        $modelPayment = new Model_Payment();
        $payment = $modelPayment->getById($payment_id);


        $this->view->payment = $payment;

        //
        // render views
        //
        echo $this->view->render('payment/mark-payment-view.phtml');
        exit;
    }

    public function changeReceivedDateAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('paymentApprove'));

        $paymentId = $this->request->getParam('payment_id', 0);
        $receivedDate = $this->request->getParam('received_date', 0);

        $receivedDate = strtotime($receivedDate);

        $modelPayment = new Model_Payment();
        $success = $modelPayment->updateById($paymentId, array('received_date' => $receivedDate));

        if ($success) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Received Date changed successfully"));
        } else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in  Received Date"));
        }
        echo 1;
        exit;
    }

}
