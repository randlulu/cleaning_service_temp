<?php

class Invoices_PaymentController extends Zend_Controller_Action {

    private $request;
    private $router;
    private $loggedUser;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'invoices';
        $this->view->sub_menu = 'payment';

        //
        // check Auth for logged user
        //
        $this->loggedUser = CheckAuth::getLoggedUser();
    }

    /**
     * Items list action
     */
    public function indexAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('paymentList'));

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'received_date');
        $sortingMethod = $this->request->getParam('method', 'DESC');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());
        $bookingId = $this->request->getParam('booking_id');
        $is_first_time = $this->request->getParam('is_first_time');
        $page_number = $this->request->getParam('page_number');

        if (isset($page_number)) {
            $perPage = 15;
            $currentPage = $page_number + 1;
        }

        //
        // Load model
        //
        $modelPayment = new Model_Payment();
        $modelBookingInvoice = new Model_BookingInvoice();

        //
        // Trim Filters
        //
        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }
        if ($bookingId) {
            $filters['booking_id'] = $bookingId;
        }

        //
        //check if can see his or assigned invoice
        //
        if ($bookingId) {
            $invoice = $modelBookingInvoice->getByBookingId($bookingId);

            if (!$modelBookingInvoice->checkIfCanSeeHisOrAssignedInvoice($invoice['id'])) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        //
        // get data list
        //
		
		//Changed by Salim

        if ($this->request->isPost()) {
            $paymentData = '';
            if (isset($filters['keywords'])) {
                $paymentData = $modelPayment->getByRefernce($filters['keywords'], "{$orderBy} {$sortingMethod}", $pager);
                if (empty($paymentData)) {
                    $paymentData = $modelPayment->getAll($filters, "{$orderBy} {$sortingMethod}", $pager, 0, $perPage, $currentPage);
                }
            } else if (isset($filters['is_matched']) && $filters['is_matched'] == 0) {
                $paymentData = $modelPayment->getNotMatchedPayments($filters, "{$orderBy} {$sortingMethod}", $pager, 0, $perPage, $currentPage);
            } else if (isset($filters['file_import']) && $filters['file_import'] == 1) {
               
                $paymentData = $modelPayment->getFileImportPayments($filters, "{$orderBy} {$sortingMethod}", $pager, 0, $perPage, $currentPage);
            } else {
                $paymentData = $modelPayment->getAll($filters, "{$orderBy} {$sortingMethod}", $pager, 0, $perPage, $currentPage);
            }
            $result = array();
            $this->view->data = $paymentData;
            $this->view->is_first_time = $is_first_time;
            $this->view->filters = $filters;
            $result['data'] = $this->view->render('payment/draw-node.phtml');
            if ($paymentData) {

                $result['is_last_request'] = 0;
            } else {

                $result['is_last_request'] = 1;
            }
            echo json_encode($result);
            exit;
        }




        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
        $this->view->booking_id = $bookingId;
    }

    public function checkPaymentsAction() {

        $bookingId = $this->request->getParam('booking_id', 0);
        $contractorId = $this->request->getParam('contractor_id', 0);

        if ($contractorId && $bookingId) {
            $modelPayment = new Model_Payment();
            $hasPayments = $modelPayment->getPaymentByBookingIdAndContractorId($bookingId, $contractorId);
            echo json_encode(array('hasPayments' => $hasPayments));
            exit;
        }
    }

    public function customerEwayPaymentAction() {


        $customer_id = $this->request->getParam('customer_id');
        $bookingId = $this->request->getParam('booking_id', '');

        if (isset($customer_id) && !empty($customer_id)) {

            $customer_model = new Model_Customer();
            $customer = $customer_model->getById($customer_id);
            $authRole_model = new Model_AuthRole();
            $auth = Zend_Auth::getInstance();
            $authStorge = $auth->getStorage();
            $default_page = $this->router->assemble(array('booking_id' => $bookingId, 'customer_id' => $customer_id), 'customerPaymentEwayAdd');
            $role = $authRole_model->getRoleIdByName('Customer');
            $customer['role_id'] = $role;
            $user_id = $customer['customer_id'];
            $customer['user_id'] = $user_id;
            $customer['username'] = $customer['first_name'];
            $customer['default_page'] = $default_page;
            $customer_obj = (object) $customer;
            $authStorge->write($customer_obj);
            CheckAuth::redirectCustomer($default_page);
        }
    }

    public function checkFullPaidPaymentAction() {

        $bookingId = $this->request->getParam('booking_id', 0);
        $loggedUser = CheckAuth::getLoggedUser();
        $modelBooking = new Model_Booking();
        $modelPayment = new Model_Payment();
        $booking = $modelBooking->getById($bookingId);
        $data = array();

        $modelAuthRole = new Model_AuthRole();
        $authRole = $modelAuthRole->getById($loggedUser['role_id']);
        $defaultPage = $authRole['default_page'];
        $EwayAddUrl = $this->router->assemble(array('booking_id' => $bookingId), 'paymentEwayAdd');
        $AddUrl = $this->router->assemble(array('booking_id' => $bookingId), 'paymentAdd');
        $allPayment = $modelPayment->getTotalAmount(array('booking_id' => $booking['booking_id'], 'is_approved' => 'all'));
        $totalAmountDetails = $modelBooking->getTotalAmountBookingDetails($bookingId);
        $amount = $totalAmountDetails['total_amount'];
        $data['amount'] = $amount;
        //var_dump($_SERVER);
        $redirectUrl = $this->router->assemble(array('id' => $bookingId), 'bookingView');


        if ($allPayment >= $totalAmountDetails['total_amount']) {
            $data['msg'] = "Invoice paid in full already, are you sure you want to continue?";
            $data['type'] = "error";
            $data['redirectUrl'] = $redirectUrl;
            echo json_encode($data);
            exit;
        }
        $data['msg'] = "success";
        $data['type'] = "success";

        echo json_encode($data);
        exit;
    }

    public function ewayAddAction() {



        $access_token = $this->request->getParam('access_token');
        $user_role = $this->request->getParam('user_role', 'customer');
        $customer_id = $this->request->getParam('customer_id');
        $bookingId = $this->request->getParam('booking_id', '');


        $this->view->access_token = $access_token;
        $this->view->user_role = $user_role;

        $loggedUser = CheckAuth::getLoggedUser();


        if (isset($access_token) && !empty($access_token)) {

            if ($user_role == 'contractor') {
                $modelIosUser = new Model_IosUser();
                $modelUser = new Model_User();
                $user = $modelIosUser->getByUserInfoByAccessToken($access_token);
                $contractorObj = $modelUser->getById($user['user_id']);
                $auth = Zend_Auth::getInstance();
                $authStorge = $auth->getStorage();
                $contractorObj = (object) $contractorObj;
                $authStorge->write($contractorObj);
                CheckAuth::afterlogin(false, 'app');
            } else if ($user_role == 'customer') {
                $modelCustomerAccessToken = new Model_CustomerAccessToken();
                $Customer = $modelCustomerAccessToken->getByUserInfoByAccessToken($access_token);
                $authRole_model = new Model_AuthRole();
                $auth = Zend_Auth::getInstance();
                $authStorge = $auth->getStorage();
                $role = $authRole_model->getRoleIdByName('customer');
                $Customer['role_id'] = $role;
                $Customer['user_id'] = '';

                $Customer['username'] = $Customer['first_name'];
                $customer_obj = (object) $Customer;
                $authStorge->write($customer_obj);
                CheckAuth::redirectCustomer();
				
            }
        } elseif ((isset($customer_id) && !empty($customer_id)) && !($this->request->isPost())) {

            if ($user_role == 'customer') {
                CheckAuth::logout();
                $modelCustomer = new Model_Customer();
                $Customer = $modelCustomer->getById($customer_id);

                $authRole_model = new Model_AuthRole();
                $auth = Zend_Auth::getInstance();
                $authStorge = $auth->getStorage();
                $role = $authRole_model->getRoleIdByName('customer');
                $Customer['role_id'] = $role;
                $Customer['user_id'] = '';

                $Customer['username'] = $Customer['first_name'];
                $customer_obj = (object) $Customer;
                $authStorge->write($customer_obj);
                CheckAuth::redirectCustomer();
            }
        } else {
            CheckAuth::checkLoggedIn();
        }


        $modelBooking = new Model_Booking();
        $modelPayment = new Model_Payment();
        $booking = $modelBooking->getById($bookingId);




        if (isset($customer_id) && !empty($customer_id)) {

            $loggedUser = CheckAuth::getLoggedUser();
            $authRole_model = new Model_AuthRole();
            $role = $authRole_model->getRoleIdByName('Customer');

            if (($booking['customer_id'] != $loggedUser['customer_id']) && ($role == $loggedUser['role_id'])) {
                CheckAuth::logout();
                $this->_redirect($this->router->assemble(array(), 'Login'));
            }
        }


        $allPayment = $modelPayment->getTotalAmount(array('booking_id' => $booking['booking_id'], 'is_approved' => 'all'));
        $totalAmountDetails = $modelBooking->getTotalAmountBookingDetails($bookingId);


        /* if (!$booking) {
          $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Invalid booking"));
          $this->_redirect($this->router->assemble(array(), 'Login'));
          }



          if (!$modelBooking->checkIfCanEditBooking($booking['booking_id'])) {
          $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
          $this->_redirect($this->router->assemble(array(), 'Login'));
          } */




        if (CheckAuth::checkCredential(array('paymentList'))) {
            $approvedPayment = $modelPayment->getTotalAmount(array('booking_id' => $booking['booking_id'], 'is_approved' => 'yes'));
            if ($approvedPayment >= $totalAmountDetails['total_amount']) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Failed to add payment, this invoice is already paid"));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            }


            if ($allPayment >= $totalAmountDetails['total_amount']) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Failed to add payment, this invoice is already paid. Check new payments"));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            }
        }

        $modelAuthRole = new Model_AuthRole();
        $authRole = $modelAuthRole->getById($loggedUser['role_id']);
        $defaultPage = $authRole['default_page'];

        if ($booking) {
            if (!(isset($access_token) && !empty($access_token))) {
                if ($allPayment >= $totalAmountDetails['total_amount']) {
                    $error_message = 'Invoice paid in full already, are you sure you want to continue?';
                    $this->view->error_message = $error_message;
                    $this->view->redirectUrl = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : $defaultPage;
                }
            }
            $this->view->amount = $totalAmountDetails['total_amount'] - $allPayment;
            $this->view->totalAmount = $totalAmountDetails['total_amount'];
        }



        $modelBookingInvoice = new Model_BookingInvoice();
        $bookingInvoice = $modelBookingInvoice->getByBookingId($bookingId);
        $this->view->bookingInvoice = $bookingInvoice;



        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $contractors = $modelContractorServiceBooking->getByBookingId($bookingId);
        $this->view->contractors = $contractors;

        $this->view->booking = $booking;

        $modelCustomer = new Model_Customer();
        $customer = $modelCustomer->getById($booking['customer_id']);
        $this->view->customer = $customer;


        if ($this->request->isPost()) {

            /* $totalAmountDetails = $modelBooking->getTotalAmountBookingDetails($bookingId);


              $approvedPayment = $modelPayment->getTotalAmount(array('booking_id' => $booking['booking_id'], 'is_approved' => 'yes'));
              if ($approvedPayment >= $totalAmountDetails['total_amount']) {
              $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "fully Paid"));
              $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
              }


              if ($allPayment >= $totalAmountDetails['total_amount']) {
              $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Failed to add payment, this invoice is already paid. Check new payments"));
              $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
              } */

            $this->sendTransaction();
        }
    }

    public function sendTransaction() {

        //$FirstName = $this->request->getParam('first_name','');
        $customer_id = $this->request->getParam('customer_id');
        //$LastName = $this->request->getParam('last_name','');
        //$StreetName = $this->request->getParam('street_address','');
        //$StreetNumber = $this->request->getParam('street_number','');
        //$City = $this->request->getParam('city','');
        //$State = $this->request->getParam('state','');
        //$PostalCode = $this->request->getParam('postcode',0);
        //$Email = $this->request->getParam('email','');

        $CardHolderName = $this->request->getParam('card_name', '');
        $CreditCardNumber = $this->request->getParam('card_number', 0);
        $CreditCardNumber1 = $this->request->getParam('EWAY_CARDNUMBER', 0);
        $CVN = $this->request->getParam('card_cvn', 0);
        $CVN1 = $this->request->getParam('EWAY_CARDCVN', 0);
        $expiry_date = $this->request->getParam('expiry_date', '12/202');
        $expiry_array = explode('/', $expiry_date);
        $ExpiryMonth = $expiry_array[0];
        $ExpiryYear = $expiry_array[1];
        //$ExpiryMonth = $this->request->getParam('expiry_month', 12);
        //$ExpiryYear = $this->request->getParam('expiry_year',25);
        $TotalAmount = $this->request->getParam('total_amount', 0);
        $InvoiceNumber = $this->request->getParam('invoice_number', 0);
        $InvoiceDescription = $this->request->getParam('invoice_description', '');
        $CurrencyCode = $this->request->getParam('currency_code', '');
        $bookingId = $this->request->getParam('booking_id', '');
        $contractorId = $this->request->getParam('contractor_id', '');
        $access_token = $this->request->getParam('access_token');
        $user_role = $this->request->getParam('user_role', 'customer');
        //$received_date = $this->request->getParam('received_date', '');
        //$received_date = date('d M Y');





        $modelBooking = new Model_Booking();
        $modelCustomer = new Model_Customer();
        $modelBookingInvoice = new Model_BookingInvoice();
        $booking = $modelBooking->getById($bookingId);
        $customerData = $modelCustomer->getById($customer_id);
        $amount = $TotalAmount * 100;

        $CardDetails = array(
            'Name' => $CardHolderName,
            'Number' => $CreditCardNumber1,
            'ExpiryMonth' => $ExpiryMonth,
            'ExpiryYear' => $ExpiryYear,
            'CVN' => $CVN1,
        );

        $Payment = array(
            'TotalAmount' => $amount,
            'InvoiceNumber' => $InvoiceNumber,
            'InvoiceDescription' => $InvoiceDescription,
            'CurrencyCode' => $CurrencyCode
        );

        $Customer = array(
            'FirstName' => $customerData['first_name'],
            'LastName' => $customerData['last_name'],
            'Street1' => $customerData['street_address'] . ' ' . $customerData['street_number'],
            'City' => $customerData['city_name'],
            'State' => $customerData['state'],
            'PostalCode' => $customerData['postcode'],
            'Email' => $customerData['email1'],
            'CardDetails' => $CardDetails
        );


        $transaction = array(
            'Customer' => $Customer,
            'Payment' => $Payment
        );


        //var_dump($transaction);
        //exit;


        $modelPayment = new Model_Payment();


        Zend_Loader::loadFile('include_eway.php', APPLICATION_PATH . '/../library/eway-rapid-php-master');

        if (0) {
            $apiKey = 'C3AB9CgGmWUyn8oaghTUTDS2DyZmB/j5aPOWLaqtO2QT5/aaFD1UJnX9mDM697Vg7tAHWT';
            $apiPassword = 'bdtLryqv';
            $apiEndpoint = \Eway\Rapid\Client::MODE_SANDBOX;
        }
        $apiKey = '44DD7AhRBX08hfSoic2wv9mAF/Bs5St52K15ISLq6EaA29SCLz7GuY+G6eyiJkDLUvRKRU';
        $apiPassword = '72tQqX16';
        $apiEndpoint = 'production';
        $client = \Eway\Rapid::createClient($apiKey, $apiPassword, $apiEndpoint);


        $transaction['TransactionType'] = \Eway\Rapid\Enum\TransactionType::PURCHASE;
        $response = $client->createTransaction(\Eway\Rapid\Enum\ApiMethod::DIRECT, $transaction);





        if ($response->TransactionStatus) {

            $data = array(
                'received_date' => time(),
                'bank_charges' => 0,
                'amount' => round($TotalAmount, 2),
                'description' => $InvoiceDescription,
                'payment_type_id' => 3,
                'booking_id' => $booking['booking_id'],
                'customer_id' => $booking['customer_id'],
                'user_id' => $this->loggedUser['user_id'],
                'created' => time(),
                'reference' => $response->TransactionID,
                'amount_withheld' => 0,
                'withholding_tax' => 0,
                'is_acknowledgment' => 0,
                'contractor_id' => $contractorId,
                'is_approved' => 1,
                'approved_by' => 'system'
            );

            $success = $modelPayment->insert($data);
            if ($success) {

                $paidAmount = $modelPayment->getTotalAmount(array('booking_id' => $booking['booking_id'], 'is_approved' => 'yes'));

                if ($paidAmount < 0) {
                    $paidAmount = 0;
                }

                $modelBooking->updateById($booking['booking_id'], array('paid_amount' => round($paidAmount, 2)));

                if ($paidAmount >= $booking['qoute']) {
                    $modelBookingInvoice->updateByBookingId($booking['booking_id'], array('invoice_type' => 'closed'));
                } else {
                    $modelBookingInvoice->updateByBookingId($booking['booking_id'], array('invoice_type' => 'open'));
                }
            }
            if (isset($access_token) && !empty($access_token)) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => 'Payment successful! ID: ' . $response->TransactionID . ' press Back to go back to the app '));
            } else {
                $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => 'Payment successful! ID: ' . $response->TransactionID));
            }


            $modelBookingInvoice = new Model_BookingInvoice();
            $invoice = $modelBookingInvoice->getByBookingId($booking['booking_id']);
			if(isset($access_token) && !empty($access_token)){
			 $this->_redirect($this->router->assemble(array(), 'paymentEwayAdd'));
			}else{
			  $this->_redirect($this->router->assemble(array('id' => $invoice['id']), 'invoiceView'));
			}
            
			//

            /* if (CheckAuth::checkPermission(array('paymentList')) && (!$modelBookingInvoice->checkIfCanSeeHisOrAssignedInvoice($invoice['id']))) {
              $this->_redirect($this->router->assemble(array('booking_id' => $booking['booking_id']), 'bookingPaymentList'));
              } else {
              if(isset($access_token) && !empty($access_token)){
              $this->_redirect($this->router->assemble(array(), 'paymentEwayAdd').'?access_token='.$access_token.'&user_role='.$user_role);
              }else{
              $this->_redirect($this->router->assemble(array(), 'paymentEwayAdd'));
              }
              } */
        } else {
            if ($response->getErrors()) {
                foreach ($response->getErrors() as $error) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Error: " . \Eway\Rapid::getMessage($error)));
                    $this->_redirect($this->router->assemble(array(), 'paymentEwayAdd'));
                }
            } else {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => 'Sorry, payment declined'));
                $this->_redirect($this->router->assemble(array(), 'paymentEwayAdd'));
            }
        }

        exit;
    }

    /**
     * Add new item action
     */
    public function addAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('paymentAdd'));

        //
        // get request parameters
        //
        $receivedDate = $this->request->getParam('received_date');
        $bankCharges = $this->request->getParam('bank_charges');
        $amount = $this->request->getParam('amount');
        $amount = str_replace(",", "", $amount);
        $description = $this->request->getParam('description');
        $reference = $this->request->getParam('reference');
        $amountWithheld = $this->request->getParam('amount_withheld');
        $withholdingTax = $this->request->getParam('withholding_tax');
        $isAcknowledgment = $this->request->getParam('is_acknowledgment');
        $paymentTypeId = $this->request->getParam('payment_type_id');
        $bookingId = $this->request->getParam('booking_id', 0);
        $contractorId = $this->request->getParam('contractor_id', 0);

        if ($_SERVER['REMOTE_ADDR'] == '188.161.105.197') {
            //echo '$amount : '.$amount;
            //exit;
        }
        //
        // Load MODEL
        //
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelBooking = new Model_Booking();
        $modelPayment = new Model_Payment();
        $modelContractorServiceBookingTemp = new Model_ContractorServiceBookingTemp();

        //
        //get booking 
        //
        $booking = $modelBooking->getById($bookingId);


        if (!$booking) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Invalid booking"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }

        /* if (!$modelBooking->checkIfCanEditBooking($booking['booking_id'])) {
          $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
          $this->_redirect($this->router->assemble(array(), 'Login'));
          } */

        $totalAmountDetails = $modelBooking->getTotalAmountBookingDetails($bookingId);

        $approvedPayment = $modelPayment->getTotalAmount(array('booking_id' => $booking['booking_id'], 'is_approved' => 'yes'));
        /* if ($approvedPayment >= $totalAmountDetails['total_amount']) {
          $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Failed to add payment, this invoice is already paid"));
          $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
          } */

        $allPayment = $modelPayment->getTotalAmount(array('booking_id' => $booking['booking_id'], 'is_approved' => 'all'));
        /* if ($allPayment >= $totalAmountDetails['total_amount']) {
          $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Failed to add payment, this invoice is already paid. Check new payments"));
          $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
          } */

        //
        // init action form
        //
        $options = array(
            'booking_id' => $bookingId
        );
        if ($booking) {
            $options['amount'] = $totalAmountDetails['total_amount'] - $allPayment;
            $options['booking_end'] = $booking['booking_end'];
        }
        $form = new Invoices_Form_Payment($options);

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'received_date' => strtotime($receivedDate),
                    'bank_charges' => round($bankCharges, 2),
                    'amount' => round($amount, 2),
                    'description' => $description,
                    'payment_type_id' => $paymentTypeId,
                    'booking_id' => $booking['booking_id'],
                    'customer_id' => $booking['customer_id'],
                    'user_id' => $this->loggedUser['user_id'],
                    'created' => time(),
                    'reference' => $reference,
                    'amount_withheld' => round($amountWithheld, 2),
                    'withholding_tax' => $withholdingTax,
                    'is_acknowledgment' => $isAcknowledgment,
                    'contractor_id' => $contractorId
                );

                $success = $modelPayment->insert($data);

                if ($success) {
                    $modelBookingInvoice->updateByBookingId($booking['booking_id'], array('invoice_type' => 'open'));
                    //$this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully, Please Approved this Payment"));
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Payment saved"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                }

                if (CheckAuth::checkCredential(array('paymentList'))) {
                    $this->_redirect($this->router->assemble(array('booking_id' => $booking['booking_id']), 'bookingPaymentList'));
                } else {
                    $this->_redirect($this->router->assemble(array(), 'booking'));
                }
            }
        }

        //
        //send booking Invoice to view
        //
        $bookingInvoice = $modelBookingInvoice->getByBookingId($bookingId);
        $modelBookingInvoice->fill($bookingInvoice, array('booking', 'customer'));

        $this->view->bookingInvoice = $bookingInvoice;
        $this->view->form = $form;
    }

    /*     * *******Check Duplicate Payment Reference *************IBM */

    public function checkDuplicatePaymentReferenceAction() {
        $reference = $this->request->getParam('reference', '');
        if (isset($reference) && $reference != "") {
            $modelPayment = new Model_Payment();
            $duplicate_reference = $modelPayment->getPaymentReference($reference);
            $data = array();
            if (isset($duplicate_reference) && $duplicate_reference != 0) {
                $data['msg'] = "Duplicate Payment Reference !!";
                $data['type'] = "error";
                $data['mm'] = $duplicate_reference;
                echo json_encode($data);
                exit;
            }
            $data['msg'] = "Correct";
            $data['type'] = "success";
        } else {
            $data['empty'] = "Value is required and can't be empty";
            $data['type'] = "error";
        }
        echo json_encode($data);
        exit;
    }

    /*     * ***END****** */

    public function deleteAction() {

        $os = $this->request->getParam('os', 'web');
        $paymentIds = $this->request->getParam('ids', array());

        if ($os != 'web') {
            $accessToken = $this->request->getParam('access_token', 0);
            $modelIosUser = new Model_IosUser();
            $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
            $this->iosLoggedUser = $user['id'];
            $loggedUser = CheckAuth::getLoggedUser();

            $mobileRetArr = array('authrezed' => 0);

            if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
                $mobileRetArr['msg'] = 'wrong access token';
                echo json_encode($mobileRetArr);
                exit;
            }

            if (empty($loggedUser)) {

                //open new session
                $authrezed = $this->openNewSession($accessToken);
                if (!$authrezed) {
                    echo json_encode($mobileRetArr);
                    exit;
                }
            }
            $mobileRetArr['authrezed'] = 1;
        }
        //
        // check Auth for logged user
        //
		if ($os == 'web') {
            CheckAuth::checkPermission(array('paymentDelete'));
        }
        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $bookingId = $this->request->getParam('booking_id', 0);

        //
        // Load MODEL
        //
        $modelBooking = new Model_Booking();
        $modelPayment = new Model_Payment();
        $deleted = 0;
        //
        //Validation
        //
        if (!$modelBooking->checkIfCanEditBooking($bookingId) && $os == 'web') {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        } else if (!$modelBooking->checkIfCanEditBooking($bookingId) && $os != 'web') {
            $mobileRetArr['msg'] = "You don't have permission";
            $mobileRetArr['type'] = "error";
            echo json_encode($mobileRetArr);
            exit;
        }
        if (empty($paymentIds)) {
            $payment = $modelPayment->getByIdAndBookingId($id, $bookingId);
            if ($payment['is_approved'] && $os == 'web') {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Could not delete Approved Payment, Please change to Unapproved to delete"));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            } else if ($payment['is_approved'] && $os != 'web') {
                $mobileRetArr['msg'] = 'Could not delete Approved Payment, Please change to Unapproved to delete';
                $mobileRetArr['type'] = 'error';
                echo json_encode($mobileRetArr);
                exit;
            }
            if ($payment) {
                $data = array(
                    'is_deleted' => 1
                );
                //delete payment
                $deleted = $modelPayment->updateById($id, $data);
            }
        } else {

            foreach ($paymentIds as $key => $PaymentId) {
                $data = array(
                    'is_deleted' => 1
                );
                //delete payment
                $deleted = $modelPayment->updateById($PaymentId, $data);
            }
        }

        if ($deleted && $os != 'web') {
            $mobileRetArr['msg'] = 'Payment deleted';
            $mobileRetArr['type'] = "success";
            echo json_encode($mobileRetArr);
            exit;
        }
        if ($deleted) {
            echo 1;
            exit;
        }
        exit;
    }

    public function editAction() {

        //
        //for mobile
        //
		$os = $this->request->getParam('os', 'web');
        if ($os != 'web') {
            $accessToken = $this->request->getParam('access_token', 0);
            $modelIosUser = new Model_IosUser();
            $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
            $this->iosLoggedUser = $user['id'];
            $loggedUser = CheckAuth::getLoggedUser();

            $invoiceId = $this->request->getParam('invoice_id', 0);
            $invoice_number = $this->request->getParam('invoice_num', 0);

            $mobileRetArr = array('authrezed' => 0);

            if (!empty($loggedUser) && !($this->checkAccessTokenOfLoggedUser($accessToken))) {
                $mobileRetArr['msg'] = 'wrong access token';
                echo json_encode($mobileRetArr);
                exit;
            }

            if (empty($loggedUser)) {

                //open new session
                $authrezed = $this->openNewSession($accessToken);
                if (!$authrezed) {
                    echo json_encode($mobileRetArr);
                    exit;
                }
            }
            $mobileRetArr['authrezed'] = 1;
        }
        // check Auth for logged user
        //
		if ($os == 'web') {
            CheckAuth::checkPermission(array('paymentEdit'));
            $loggedUser = CheckAuth::getLoggedUser();
        }


        ////////////////////////////////////
        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $receivedDate = $this->request->getParam('received_date');
        $bankCharges = $this->request->getParam('bank_charges');
        $amount = $this->request->getParam('amount');
        $amount = str_replace(",", "", $amount);
        $description = $this->request->getParam('description');
        $reference = $this->request->getParam('reference');
        $amountWithheld = $this->request->getParam('amount_withheld');
        $withholdingTax = $this->request->getParam('withholding_tax');
        $isAcknowledgment = $this->request->getParam('is_acknowledgment');
        $paymentTypeId = $this->request->getParam('payment_type_id');
        $contractorId = $this->request->getParam('contractor_id', 0);




        //
        // Load Model
        //
        $modelPayment = new Model_Payment();
        $modelBooking = new Model_Booking();
        $modelBookingInvoice = new Model_BookingInvoice();

        //
        //Validation
        //
        $payment = $modelPayment->getById($id);
        if ($payment['is_approved'] && $os == 'web') {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You can't edit approved payments, please change payment to unapproved and try again"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        } else if ($payment['is_approved'] && $os != 'web') {
            $mobileRetArr['msg'] = "You can't edit approved payments, please change payment to unapproved and try again";
            echo json_encode($mobileRetArr);
            exit;
        }

        $booking = $modelBooking->getById($payment['booking_id']);
        // get company name for customer 




        if (!$booking && $os == 'web') {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Invalid booking"));
            $this->_redirect($this->router->assemble(array(), 'invoices'));
        } else if (!$booking && $os != 'web') {
            $mobileRetArr['msg'] = 'Invalid booking';
            echo json_encode($mobileRetArr);
            exit;
        }

        if (!$modelBooking->checkIfCanEditBooking($booking['booking_id']) && $os == 'web') {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        } else if (!$modelBooking->checkIfCanEditBooking($booking['booking_id'], $loggedUser['user_id']) && $os != 'web') {
            $mobileRetArr['msg'] = "You don't have permission";
            echo json_encode($mobileRetArr);
            exit;
        }

        $approvedPayment = $modelPayment->getTotalAmount(array('booking_id' => $booking['booking_id'], 'is_approved' => 'yes'));
        if ($approvedPayment >= $booking['qoute'] && $os == 'web') {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "fully Paid"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        } else if ($approvedPayment >= $booking['qoute'] && $os != 'web') {
            $mobileRetArr['msg'] = "fully Paid";
            echo json_encode($mobileRetArr);
            exit;
        }

        //
        //calculate amount
        //
        $allPayment = $modelPayment->getTotalAmount(array('booking_id' => $booking['booking_id'], 'is_approved' => 'all'));
        $bookingAmount = $booking['qoute'] - $allPayment + $payment['amount'];

        //
        //
        // init action form
        //
        $options = array(
            'booking_id' => $booking['booking_id'],
            'mode' => 'update',
            'payment' => $payment,
            'amount' => $bookingAmount,
        );





        $form = new Invoices_Form_Payment($options);

        //
        // handling the insertion process
        //
        if ($this->request->isPost() || $os != 'web') { // check if POST request method
            if ($form->isValid($this->request->getPost()) || $os != 'web') { // validate form data
                if ($os != 'web') {
                    $loggedUser = CheckAuth::getLoggedUser();
                    $contractorId = $loggedUser['user_id'];
                    $user_id = $loggedUser['user_id'];
                } else {
                    $user_id = $this->loggedUser['user_id'];
                }
                $data = array(
                    'received_date' => strtotime($receivedDate),
                    'bank_charges' => round($bankCharges, 2),
                    'amount' => round($amount, 2),
                    'description' => $description,
                    'payment_type_id' => $paymentTypeId,
                    'booking_id' => $booking['booking_id'],
                    'customer_id' => $booking['customer_id'],
                    'user_id' => $user_id,
                    'created' => time(),
                    'reference' => $reference,
                    'amount_withheld' => round($amountWithheld, 2),
                    'withholding_tax' => $withholdingTax,
                    'is_acknowledgment' => $isAcknowledgment,
                    'is_rejected' => 0,
                    'contractor_id' => $contractorId
                );



                $success = $modelPayment->updateById($id, $data);

                if ($success && $os == 'web') {
                    $modelBookingInvoice->updateByBookingId($booking['booking_id'], array('invoice_type' => 'open'));
                    //$this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully, Please Approved this Payment"));
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Payment saved"));
                } else if ($success && $os != 'web') {
                    $mobileRetArr['msg'] = "Payment saved";
                    echo json_encode($mobileRetArr);
                    exit;
                } else if ($os != 'web') {
                    $mobileRetArr['msg'] = "No changes";
                    echo json_encode($mobileRetArr);
                    exit;
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                }

                if (CheckAuth::checkCredential(array('paymentList'))) {
                    $this->_redirect($this->router->assemble(array('booking_id' => $booking['booking_id']), 'bookingPaymentList'));
                } else {
                    $this->_redirect($this->router->assemble(array(), 'invoices'));
                }
            }
        }

        //
        //send booking Invoice to view
        //
        $bookingInvoice = $modelBookingInvoice->getByBookingId($booking['booking_id']);
        $modelBookingInvoice->fill($bookingInvoice, array('booking', 'customer'));


        if ($_SERVER['REMOTE_ADDR'] == '176.106.46.142') {
            //	var_dump($bookingInvoice);
            //	exit;
        }
        $this->view->bookingInvoice = $bookingInvoice;
        $this->view->form = $form;
    }

    public function paymentApproveAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('paymentApprove'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ajax = $this->request->getParam('ajax');
        $bookingId = $this->request->getParam('booking_id', 0);

        //
        // Load Model
        //
        $modelUser = new Model_User();
        $modelPayment = new Model_Payment();
        $modelBooking = new Model_Booking();
        $modelCustomer = new Model_Customer();
        $modelPaymentType = new Model_PaymentType();
        $modelBookingAddress = new Model_BookingAddress();
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $result = array();
        //
        //get booking 
        //
        $booking = $modelBooking->getById($bookingId);
        if (!$booking) {
            if (isset($ajax)) {
                $result['msg'] = 'Invalid Booking';
                $result['type'] = 'error';
                echo json_encode($result);
                exit;
            }
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Invalid booking"));
            $this->_redirect($this->router->assemble(array(), 'invoices'));
        }

        if ($booking['is_change'] == 1) {
            if (isset($ajax)) {
                $result['msg'] = 'This invoice has changes that must be approved first ';
                $result['type'] = 'error';
                echo json_encode($result);
                exit;
            }
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This invoice has changes that must be approved first "));
            $this->_redirect($this->router->assemble(array('id' => $booking['booking_id']), 'approvedService') . '?redirect_to_payment=true');
        }

        $invoice = $modelBookingInvoice->getByBookingId($bookingId);
        $payment = $modelPayment->getByIdAndBookingId($id, $booking['booking_id']);

        if ($payment) {
            $success = $modelPayment->updateById($id, array('is_approved' => 1));

            if ($success) {

                $paidAmount = $modelPayment->getTotalAmount(array('booking_id' => $booking['booking_id'], 'is_approved' => 'yes'));

                if ($paidAmount < 0) {
                    $paidAmount = 0;
                }

                $modelBooking->updateById($booking['booking_id'], array('paid_amount' => round($paidAmount, 2)));

                if ($paidAmount >= $booking['qoute']) {
                    $modelBookingInvoice->updateByBookingId($booking['booking_id'], array('invoice_type' => 'closed'));
                } else {
                    $modelBookingInvoice->updateByBookingId($booking['booking_id'], array('invoice_type' => 'open'));
                }

                // send email to customer
                if ($payment['is_acknowledgment']) {

                    //
                    // filling extra data
                    //
                    $customer = $modelCustomer->getById($booking['customer_id']);
                    $user = $modelUser->getById($booking['created_by']);
                    $paymentType = $modelPaymentType->getById($payment['payment_type_id']);

                    $template_params = array(
                        //payment
                        '{payment_type}' => $paymentType['payment_type'],
                        '{payment_received_date}' => date('Y-m-d', $payment['received_date']),
                        '{payment_amount}' => number_format($payment['amount'], 2),
                        '{payment_reference}' => $payment['reference'],
                        //booking
                        '{booking_num}' => $booking['booking_num'],
                        '{total_without_tax}' => number_format($booking['sub_total'], 2),
                        '{gst_tax}' => number_format($booking['gst'], 2),
                        '{total_with_tax}' => number_format($booking['qoute'], 2),
                        '{description}' => $booking['description'] ? $booking['description'] : '',
                        '{booking_created}' => date('d/m/Y', $booking['created']),
                        '{booking_created_by}' => ucwords($user['username']),
                        '{booking_start}' => date("F j, Y, g:i a", strtotime($booking['booking_start'])),
                        '{booking_address}' => get_line_address($modelBookingAddress->getByBookingId($booking['booking_id'])),
                        '{service}' => nl2br($modelContractorServiceBooking->getBookingAsText($booking['booking_id'])),
                        //customer
                        '{customer_name}' => get_customer_name($customer),
                        '{customer_first_name}' => isset($customer['first_name']) && $customer['first_name'] ? ucwords($customer['first_name']) : '',
                        '{customer_last_name}' => isset($customer['last_name']) && $customer['last_name'] ? ' ' . ucwords($customer['last_name']) : '',
                        '{customer_contacts}' => nl2br($modelCustomer->getCustomerContacts($booking['customer_id'])),
                    );

                    $customer = $modelCustomer->getById($booking['customer_id']);

                    $email_log = array(
                        'reference_id' => $invoice['id'],
                        'type' => 'invoice'
                    );

                    $to = array();
                    if ($customer['email1']) {
                        $to[] = $customer['email1'];
                    }
                    if ($customer['email2']) {
                        $to[] = $customer['email2'];
                    }
                    if ($customer['email2']) {
                        $to[] = $customer['email3'];
                    }
                    $to = implode(',', $to);

                    if ($to) {
                        //send email
                        EmailNotification::sendEmail(array('to' => $to), 'payment_acknowledgment', $template_params, $email_log);
                    }
                }
                // Notfication Notify 
                MobileNotification::notify($booking['booking_id'], 'booking paid');
            }



            if ($success) {
                if (isset($ajax)) {
                    $result['msg'] = 'Payment approved';
                    $result['type'] = 'success';
                    echo json_encode($result);
                    exit;
                }
                //$this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Payment Approved successfully"));
                $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Payment approved"));
            } else {
                if (isset($ajax)) {
                    $result['msg'] = 'Failed to approve payment';
                    $result['type'] = 'error';
                    echo json_encode($result);
                    exit;
                }
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Failed to approve payment"));
            }
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }


        if (isset($ajax)) {
            $result['msg'] = 'Failed to approve payment';
            $result['type'] = 'error';
            echo json_encode($result);
            exit;
        }
        $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Failed to approve payment"));
        $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
    }

    public function paymentUnapproveAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('paymentApprove'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ajax = $this->request->getParam('ajax', 0);
        $bookingId = $this->request->getParam('booking_id', 0);
        $result = array();
        //
        // Load Model
        //
        $modelBooking = new Model_Booking();
        $modelPayment = new Model_Payment();
        $modelBookingInvoice = new Model_BookingInvoice();

        //
        //get booking 
        //
        $booking = $modelBooking->getById($bookingId);
        if (!$booking) {
            if (isset($ajax)) {
                $result['msg'] = 'Invalid Booking';
                $result['type'] = 'error';
                echo json_encode($result);
                exit;
            }
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Invalid booking"));
            $this->_redirect($this->router->assemble(array(), 'invoices'));
        }

        $payment = $modelPayment->getById($id);
        if ($payment) {
            $success = $modelPayment->updateById($id, array('is_approved' => 0));

            if ($success) {
                $paidAmount = $modelPayment->getTotalAmount(array('booking_id' => $booking['booking_id'], 'is_approved' => 'yes'));

                if ($paidAmount < 0) {
                    $paidAmount = 0;
                }

                $modelBooking->updateById($booking['booking_id'], array('paid_amount' => round($paidAmount, 2)));

                if ($paidAmount >= $booking['qoute']) {
                    $modelBookingInvoice->updateByBookingId($booking['booking_id'], array('invoice_type' => 'closed'));
                } else {
                    $modelBookingInvoice->updateByBookingId($booking['booking_id'], array('invoice_type' => 'open'));
                }
            }

            if ($success) {
                if (isset($ajax)) {
                    $result['msg'] = 'Payment approval removed';
                    $result['type'] = 'success';
                    echo json_encode($result);
                    exit;
                }
                $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Payment approval removed"));
            } else {
                if (isset($ajax)) {
                    $result['msg'] = 'Failed to remove payment approval';
                    $result['type'] = 'error';
                    echo json_encode($result);
                    exit;
                }
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Failed to remove payment approval"));
            }
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        if (isset($ajax)) {
            $result['msg'] = 'Failed to remove payment approval';
            $result['type'] = 'error';
            echo json_encode($result);
            exit;
        }
        $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Failed to remove payment approval"));
        $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
    }

    public function statmentAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('statment'));

        //
        // get params 
        //
        $invoiceId = $this->request->getParam('id', 0);

        // Load Model
        $modelRefund = new Model_Refund();
        $modelPayment = new Model_Payment();
        $modelCustomer = new Model_Customer();
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();

        //
        // geting data
        //
        $invoice = $modelBookingInvoice->getById($invoiceId);
        $modelBookingInvoice->fill($invoice, array('booking'));

        //
        // validation
        //
        if (!$invoice) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Invoice not found"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        if (!CheckAuth::checkIfCanHandelAllCompany('invoice', $invoiceId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        if (!$modelBookingInvoice->checkIfCanSeeHisOrAssignedInvoice($invoiceId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $customer = $modelCustomer->getById($invoice['booking']['customer_id']);
        $modelCustomer->fill($customer, array('customer_commercial_info', 'customer_contacts'));

        $data = array();
        $payments = $modelPayment->getAll(array('booking_id' => $invoice['booking_id'], 'is_approved' => 'yes'));
        foreach ($payments as $payment) {
            $data['payment_' . $payment['payment_id']] = $payment['received_date'];
        }
        $refunds = $modelRefund->getAll(array('booking_id' => $invoice['booking_id'], 'is_approved' => 'yes'));
        foreach ($refunds as $refund) {
            $data['refund_' . $refund['refund_id']] = $refund['received_date'];
        }
        array_multisort(array_values($data), $data);

        $paymentsAndRefunds = array();
        foreach ($data as $key => $value) {

            $type_id = explode("_", $key);
            $type = $type_id[0];
            $id = $type_id[1];

            if ($type == 'payment') {
                $payment = $modelPayment->getById($id);

                $payment['discount'] = false;
                $paymentsAndRefunds[] = array('type' => 'payment', 'data' => $payment);
            } else if ($type == 'refund') {
                $refund = $modelRefund->getById($id);

                $refund['discount'] = false;
                $paymentsAndRefunds[] = array('type' => 'refund', 'data' => $refund);

                $refund['discount'] = true;
                $paymentsAndRefunds[] = array('type' => 'payment', 'data' => $refund);
            }
        }

        $this->view->invoice = $invoice;
        $this->view->booking = $invoice['booking'];
        $this->view->bookingServices = $modelContractorServiceBooking->getByBookingId($invoice['booking']['booking_id']);
        $this->view->customer = $customer;
        $this->view->customer_commercial_info = $customer['customer_commercial_info'];
        $this->view->customerContacts = $customer['customer_contacts'];
        $this->view->paymentsAndRefunds = $paymentsAndRefunds;

        echo $this->view->render('payment/statment-body.phtml');
        exit;
    }

    public function sendStatmentsAsEmailAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('paymentStatmentSendAsEmail'));

        //
        // get params 
        //
        $invoiceId = $this->request->getParam('id', 0);

        //
        // load models
        //
        $modelUser = new Model_User();
        $modelRefund = new Model_Refund();
        $modelPayment = new Model_Payment();
        $modelCustomer = new Model_Customer();
        $modelBookingAddress = new Model_BookingAddress();
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();

        //
        // geting data
        //
        $invoice = $modelBookingInvoice->getById($invoiceId);
        $modelBookingInvoice->fill($invoice, array('booking', 'number_of_due_days', 'due_date'));

        //
        // validation
        //
        if (!$invoice) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Invoice not found"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        if (!CheckAuth::checkIfCanHandelAllCompany('invoice', $invoiceId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        //check in can see his or assigned invoices
        if (!$modelBookingInvoice->checkIfCanSeeHisOrAssignedInvoice($invoiceId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //get extra data
        $customer = $modelCustomer->getById($invoice['booking']['customer_id']);
        $modelCustomer->fill($customer, array('customer_commercial_info', 'customer_contacts'));

        $user = $modelUser->getById($invoice['booking']['created_by']);


        $data = array();
        $payments = $modelPayment->getAll(array('booking_id' => $invoice['booking_id'], 'is_approved' => 'yes'));
        foreach ($payments as $payment) {
            $data['payment_' . $payment['payment_id']] = $payment['received_date'];
        }
        $refunds = $modelRefund->getAll(array('booking_id' => $invoice['booking_id'], 'is_approved' => 'yes'));
        foreach ($refunds as $refund) {
            $data['refund_' . $refund['refund_id']] = $refund['received_date'];
        }
        array_multisort(array_values($data), $data);

        $paymentsAndRefunds = array();
        foreach ($data as $key => $value) {

            $type_id = explode("_", $key);
            $type = $type_id[0];
            $id = $type_id[1];

            if ($type == 'payment') {
                $payment = $modelPayment->getById($id);

                $payment['discount'] = false;
                $paymentsAndRefunds[] = array('type' => 'payment', 'data' => $payment);
            } else if ($type == 'refund') {
                $refund = $modelRefund->getById($id);

                $refund['discount'] = false;
                $paymentsAndRefunds[] = array('type' => 'refund', 'data' => $refund);

                $refund['discount'] = true;
                $paymentsAndRefunds[] = array('type' => 'payment', 'data' => $refund);
            }
        }

        $view = new Zend_View();
        $view->setScriptPath(APPLICATION_PATH . '/modules/invoices/views/scripts/payment');
        $view->invoice = $invoice;
        $view->booking = $invoice['booking'];
        $view->bookingServices = $modelContractorServiceBooking->getByBookingId($invoice['booking']['booking_id']);
        $view->customer = $customer;
        $view->customer_commercial_info = $customer['customer_commercial_info'];
        $view->customerContacts = $customer['customer_contacts'];
        $view->paymentsAndRefunds = $paymentsAndRefunds;

        $statmentBody = $view->render('statment-body.phtml');

        $template_params = array(
            //invoice
            '{invoice_num}' => $invoice['invoice_num'],
            '{invoice_created}' => date('d/m/Y', $invoice['created']),
            '{due}' => $invoice['due'] > 1 ? $invoice['due'] . ' Days' : $invoice['due'] . ' Day',
            '{due_date}' => $invoice['due_date'] > 1 ? $invoice['due_date'] . ' Days' : $invoice['due_date'] . ' Day',
            '{statment_view}' => $statmentBody,
            //booking
            '{booking_num}' => $invoice['booking']['booking_num'],
            '{total_without_tax}' => number_format($invoice['booking']['sub_total'], 2),
            '{gst_tax}' => number_format($invoice['booking']['gst'], 2),
            '{total_with_tax}' => number_format($invoice['booking']['qoute'], 2),
            '{paid_amount}' => number_format($invoice['booking']['paid_amount'], 2),
            '{unpaid_amount}' => number_format($invoice['booking']['qoute'] - $invoice['booking']['paid_amount'], 2),
            '{description}' => $invoice['booking']['description'] ? $invoice['booking']['description'] : '',
            '{booking_created}' => date('d/m/Y', $invoice['booking']['created']),
            '{booking_created_by}' => ucwords($user['username']),
            '{booking_start}' => date("F j, Y, g:i a", strtotime($invoice['booking']['booking_start'])),
            '{booking_address}' => get_line_address($modelBookingAddress->getByBookingId($invoice['booking']['booking_id'])),
            '{service}' => nl2br($modelContractorServiceBooking->getBookingAsText($invoice['booking']['booking_id'], true)),
            //customer
            '{customer_name}' => get_customer_name($customer),
            '{customer_first_name}' => isset($customer['first_name']) && $customer['first_name'] ? ucwords($customer['first_name']) : '',
            '{customer_last_name}' => isset($customer['last_name']) && $customer['last_name'] ? ' ' . ucwords($customer['last_name']) : '',
            '{customer_contacts}' => nl2br($modelCustomer->getCustomerContacts($invoice['booking']['customer_id'])),
        );

        $modelEmailTemplate = new Model_EmailTemplate();
        $emailTemplate = $modelEmailTemplate->getEmailTemplate('send_invoice_statment_as_email', $template_params);

        $body = $emailTemplate['body'];
        $subject = $emailTemplate['subject'];
        $to = array();
        if ($customer['email1']) {
            $to[] = $customer['email1'];
        }
        if ($customer['email2']) {
            $to[] = $customer['email2'];
        }
        if ($customer['email2']) {
            $to[] = $customer['email3'];
        }
        $to = implode(',', $to);

        if ($this->request->isPost()) {
            $to = $this->request->getParam('to');
            $cc = $this->request->getParam('cc');
            $subject = $this->request->getParam('subject');
            $body = $this->request->getParam('body');

            $params = array(
                'to' => $to,
                'cc' => $cc,
                'body' => $body,
                'subject' => $subject
            );

            $error_mesages = array();
            if (EmailNotification::validation($params, $error_mesages)) {

                // create pdf
                $pdfPath = createPdfPath();
                $destination = $pdfPath['fullDir'] . 'Statment_' . $invoice['invoice_num'] . '.pdf';
                wkhtmltopdf($statmentBody, $destination);
                $params['attachment'] = $destination;

                $success = EmailNotification::sendEmail($params, '', array(), array('reference_id' => $invoice['id'], 'type' => 'invoice'));

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Invoice statment sent"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Failed to send invoice statement, pleas try again"));
                }

                echo 1;
                exit;
            }
            $this->view->error_mesages = $error_mesages;
        }

        $this->view->invoice = $invoice;
        $this->view->to = $to;
        $this->view->subject = $subject;
        $this->view->body = $body;
        $this->view->cc = isset($cc) ? $cc : '';

        echo $this->view->render('payment/send-invoice-statment-as-email.phtml');
        exit;
    }

    public function checkStatusToAddPaymentAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('PaymentAdd'));

        $bookingId = $this->request->getParam('booking_id', 0);
        $paymentWay = $this->request->getParam('payment_way', 'local');


        $modelBooking = new Model_Booking();
        $modelBookingStatus = new Model_BookingStatus();
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelDueDate = new Model_DueDate();
        $modelBookingDueDate = new Model_BookingDueDate();

        /* if ($modelBooking->checkBookingIfAccepted($bookingId)) { */
        $booking = $modelBooking->getById($bookingId);

        $bookingStatus = $modelBookingStatus->getById($booking['status_id']);

        //if ($bookingStatus['name'] == "COMPLETED" || $bookingStatus['name'] == "IN PROGRESS" || $bookingStatus['name'] == "FAILED" || $bookingStatus['name'] == "TO DO") {
        $bookingInvoice = $modelBookingInvoice->getByBookingId($bookingId);
        /// we create new record if there is no previous booking for this booking
        if ($booking['convert_status'] != "invoice" && empty($bookingInvoice)) {

            $modelBooking->updateById($bookingId, array('convert_status' => 'invoice'));

            $data = array(
                'booking_id' => $bookingId,
                'invoice_type' => 'open',
                'created' => time(),
            );
            $modelBookingInvoice->insert($data);

            $dueDateId = $modelDueDate->getDefaultId();
            $data = array(
                'due_date_id' => $dueDateId['id']
            );

            $bookingDueDate = $modelBookingDueDate->getByBookingId($bookingId);

            if ($bookingDueDate) {
                $modelBookingDueDate->updateById($bookingDueDate['id'], $data);
            } else {
                $data['booking_id'] = $bookingId;
                $modelBookingDueDate->insert($data);
            }
        }
        if ($paymentWay == 'eway') {
            $this->_redirect($this->router->assemble(array('booking_id' => $bookingId), 'paymentEwayAdd'));
        } else {
            $this->_redirect($this->router->assemble(array('booking_id' => $bookingId), 'paymentAdd'));
        }
        /* } else {
          $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Only bookings with status (COMPLETED, IN PROGRESS, FAILED, TO DO) can generate invoices"));
          $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
          } */
        /* } else {
          $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
          $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
          } */
    }

    public function markPaymentAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('markPayment'));

        $payment_id = $this->request->getParam('payment_id', 0);
        $comment = $this->request->getParam('comment', '');


        $modelPayment = new Model_Payment();
        $payment = $modelPayment->getById($payment_id);

        $form = new Invoices_Form_MarkPayment(array('payment' => $payment));

        //
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $success = $modelPayment->updateById($payment_id, array('is_mark' => 1, 'mark_comment' => $comment));

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Payment marked"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                }

                echo 1;
                exit;
            }
        }

        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('payment/mark-payment.phtml');
        exit;
    }

    public function unmarkPaymentAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('unmarkPayment'));

        $payment_id = $this->request->getParam('payment_id', 0);

        $modelPayment = new Model_Payment();
        $success = $modelPayment->updateById($payment_id, array('is_mark' => 0, 'mark_comment' => ''));

        if ($success) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Payment unmarked"));
        } else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
        }
        echo 1;
        exit;
    }

    public function modifyMarkCommentAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('markPayment'));

        $payment_id = $this->request->getParam('payment_id', 0);
        $comment = $this->request->getParam('comment', '');


        $modelPayment = new Model_Payment();
        $payment = $modelPayment->getById($payment_id);

        $form = new Invoices_Form_MarkPayment(array('payment' => $payment, 'mode' => 'update'));

        //
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $success = $modelPayment->updateById($payment_id, array('mark_comment' => $comment));

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Payment market"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                }

                echo 1;
                exit;
            }
        }

        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('payment/mark-payment.phtml');
        exit;
    }

    public function markPaymentViewAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('markPaymentView'));

        $payment_id = $this->request->getParam('payment_id', 0);


        $modelPayment = new Model_Payment();
        $payment = $modelPayment->getById($payment_id);


        $this->view->payment = $payment;

        //
        // render views
        //
        echo $this->view->render('payment/mark-payment-view.phtml');
        exit;
    }

    public function changeReceivedDateAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('paymentApprove'));

        $paymentId = $this->request->getParam('payment_id', 0);
        $receivedDate = $this->request->getParam('received_date', 0);

        $receivedDate = strtotime($receivedDate);

        $modelPayment = new Model_Payment();
        $success = $modelPayment->updateById($paymentId, array('received_date' => $receivedDate));

        if ($success) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Date changed"));
        } else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
        }
        echo 1;
        exit;
    }

    public function openNewSession($accessToken) {
        $modelIosUser = new Model_IosUser();
        //$user = $modelIosUser->getByUserInfoById($iosUserId);
        //echo 'accessToken   '.$accessToken;
        $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
        //echo 'tessssssssssssssssst';
        //print_r($user);
        //echo 'is empty '.empty($user);
        if (!empty($user)) {
            $email = $user['email1'];
            $password = $user['password'];
            $authrezed = $this->getAuthrezed();
            $authrezed->setIdentity($email);
            $authrezed->setCredential($password);

            $auth = Zend_Auth::getInstance();
            $authrezedResult = $auth->authenticate($authrezed);
            if ($authrezedResult->isValid()) {

                $identity = $authrezed->getResultRowObject();

                $authStorge = $auth->getStorage();
                $authStorge->write($identity);

                CheckAuth::afterlogin(false, 'app');
                //CheckAuth::afterlogin(false);

                $this->iosLoggedUser = $user['id'];

                return 1;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    public function getAuthrezed() {
        $modelAuthRole = new Model_AuthRole();
        $contractorRoleId = $modelAuthRole->getRoleIdByName('contractor');

        $authrezed = new Zend_Auth_Adapter_DbTable(Zend_Db_Table::getDefaultAdapter());
        $authrezed->setTableName('user')
                ->setIdentityColumn('email1')
                ->setCredentialColumn('password')
                ->setCredentialTreatment("? AND active = 'TRUE' And role_id={$contractorRoleId}");

        return $authrezed;
    }

    public function checkAccessTokenOfLoggedUser($accessToken) {
        $modelIosUser = new Model_IosUser();
        //$user = $modelIosUser->getByUserInfoById($iosUserId);
        $user = $modelIosUser->getByUserInfoByAccessToken($accessToken);
        $loggedUser = CheckAuth::getLoggedUser();
        if ($user['user_id'] == $loggedUser['user_id']) {
            return 1;
        }
        return 0;
    }

    public function changeInvoiceAction() {
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelPayment = new Model_Payment();
        $modelBooking = new Model_Booking();
        $unpaid_invoices = $modelBookingInvoice->getAll(array('invoice_type' => 'unpaid'), 'created DESC');
        $modelBookingInvoice->fills($unpaid_invoices, array('booking', 'customer', 'customer_commercial_info'));
//        if(my_ip()){
//            var_dump($unpaid_invoices);
//            exit;
//        }
        $this->view->invoices = $unpaid_invoices;
        $payment_id = $this->request->getParam('id', 0);
        $payment = $modelPayment->getById($payment_id);
        $filters = $this->request->getParam('fltr', array());
        $this->view->filters = $filters;
        $this->view->booking_id = $payment['booking_id'];
        $this->view->payment = $payment;
        if ($this->request->isPost()) {  // check if POST request method
            $booking_id = $this->request->getParam('booking_id', 0);

            $booking = $modelBooking->getById($booking_id);
            $modelContractorServiceBooking = new Model_ContractorServiceBooking();

            $contractors = $modelContractorServiceBooking->getByBookingId($booking_id);
            $modelPayment->deleteById($payment_id);
            foreach ($contractors as $key => $contractor) {
                if ($payment['is_matched'] == 0) {
                    $payment['is_matched'] = 1;
                }
                $payment['booking_id'] = $booking['booking_id'];
                $payment['customer_id'] = $booking['customer_id'];
                $payment['contractor_id'] = $contractor['contractor_id'];
                $modelPayment->insert($payment);
            }
            if ($payment['is_matched'] == 0) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Payment added to invoice successfully."));
            } else {
                $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Invoice Changed successfully for payment."));
            }

            echo 1;
            exit;
        }
        echo $this->view->render('payment/change-invoice.phtml');
        exit;
    }

    public function getPaymentInvoicesMatchAction() {
        $model_paymentInvoiceMatch = new Model_PaymentInvoiceMatch();
        $payment_id = $this->request->getParam('payment_id', 0);
        $paymentInvoicesMatch = $model_paymentInvoiceMatch->getAll(array('payment_id' => $payment_id));
        $this->view->payment_invoices = $paymentInvoicesMatch;
        $this->view->payment_id = $payment_id;
        if ($this->request->isPost()) {
            $checkedPaymentId = $this->request->getParam('paymentInvoiceMatchId', 0);
            $model_paymentInvoiceMatch = new Model_PaymentInvoiceMatch();
            $paymentInvoiceMatch = $model_paymentInvoiceMatch->getById($checkedPaymentId);
            $model_payment = new Model_Payment();
            $model_booking = new Model_Booking();
            $loggedUser = CheckAuth::getLoggedUser();
            $booking = $model_booking->getById($paymentInvoiceMatch['booking_id']);
            $payment = $model_payment->getById($paymentInvoiceMatch['payment_id']);
            $modelContractorServiceBooking = new Model_ContractorServiceBooking();
            $contractors = $modelContractorServiceBooking->getContractorsDataByBookingId($paymentInvoiceMatch['booking_id']);
//            foreach ($contractors as $key => $contractor) {
//                if ($key > 0) {
//                    $newPayment = array(
//                        'booking_id' => $booking['booking_id'],
//                        'customer_id' => $booking['customer_id'],
//                        'score' => $paymentInvoiceMatch['score'],
//                        'received_date' => strtotime($payment['received_date']),
//                        'bank_charges' => 0,
//                        'amount' => round($payment['amount'], 2),
//                        'description' => '',
//                        'payment_type_id' => 4,
//                        'user_id' => $loggedUser['user_id'],
//                        'created' => time(),
//                        'reference' => $payment['reference'],
//                        'amount_withheld' => 0,
//                        'withholding_tax' => 0,
//                        'is_acknowledgment' => 0,
//                        'file_import' => 1,
//                        'contractor_id' => $contractor['contractor_id'],
//                    );
//                    $model_payment->insert($newPayment);
//                } else {
                    $paymentMaxed = array(
                        'booking_id' => $paymentInvoiceMatch['booking_id'],
                        'customer_id' => $booking['customer_id'],
                        'score' => $paymentInvoiceMatch['score'],
                        'contractor_id' => $contractors[0]['contractor_id'],
                        'is_approved'=>1
                    );
                    $model_payment->updateById($paymentInvoiceMatch['payment_id'], $paymentMaxed);
//                }
//            }
            $model_paymentInvoiceMatch->deleteByPaymentId($paymentInvoiceMatch['payment_id']);
            $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => 'Payment approved to <b>'.$booking['booking_num'].'</b>'));
            echo 1;
            exit;
        }
        echo $this->view->render('payment/payment-invoice-match.phtml');
        exit;
    }

    public function ignoreUnknownPaymentAction() {
        $payment_id = $this->request->getParam('payment_id', 0);
        $model_payment = new Model_Payment();
        $model_ignoredPayment = new Model_IgnoredPayment();
        $payment = $model_payment->getById($payment_id);
        if ($payment) {
            $ignoredPayment = array(
                'reference' => $payment['reference'],
                'amount' => $payment['amount'],
                'received_date' => $payment['received_date']
            );
            $status = $model_ignoredPayment->insert($ignoredPayment);
            if($status){
                $model_payment->deleteById($payment_id);
                echo 1;
                exit;
            }
        }
        
    }
}
