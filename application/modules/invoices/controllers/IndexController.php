<?php

class Invoices_IndexController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'invoices';
        $this->view->sub_menu = 'invoice';

        $pageName = $this->request->getParam('page_name', '');
        $this->view->page_title = (!empty($pageName)) ? $pageName . " - Invoices" : "Invoices";
    }

    /**
     * Items list action
     */
    public function indexAction() {
//
// check Auth for logged user
//
        CheckAuth::checkPermission(array('invoices'));

//
// get request parameters
//
         
        $orderBy = $this->request->getParam('sort', 'id');
        $sortingMethod = $this->request->getParam('method', 'desc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());
        $is_first_time = $this->request->getParam('is_first_time');
        $page_number = $this->request->getParam('page_number');
		
        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }
     //     var_dump($filters);
//
// init pager and articles model object
//
        /* $pager = new Model_Pager();
          $pager->perPage = get_config('perPage');
          $pager->currentPage = $currentPage;
          $pager->url = $_SERVER['REQUEST_URI']; */


//
//load model
//
        $modelBookingInvoice = new Model_BookingInvoice();




//
// get data list
//

        if ($this->request->isPost()) {
            if (isset($page_number)) {
                $perPage = 15;
                $currentPage = $page_number + 1;
            }
            $data = $modelBookingInvoice->getAll($filters, "{$orderBy} {$sortingMethod}", $pager, 0, $perPage, $currentPage);
            $modelBookingInvoice->fills($data, array('booking', 'contractors', 'customer', 'city', 'labels', 'due_date', 'booking_users', 'have_attachment', 'services', 'payment'));
            $result = array();
            $this->view->data = $data;
            $this->view->filters = $filters;
            $this->view->is_first_time = $is_first_time;
            $result['data'] = $this->view->render('index/draw-node.phtml');
            if ($data) {
                $result['is_last_request'] = 0;
            } else {
                $result['is_last_request'] = 1;
            }

            echo json_encode($result);
            exit;
        }

//
// set view params
//
//$this->view->data = $data;
//$this->view->currentPage = $currentPage;
//$this->view->perPage = $pager->perPage;
//$this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
    }

    public function duplicatedInvoicesAction() {

//
// check Auth for logged user
//
        CheckAuth::checkPermission(array('invoices'));

        $loggedUser = CheckAuth::getLoggedUser();
        $modelAuthRole = new Model_AuthRole ();
        $contractorRoleId = $modelAuthRole->getRoleIdByName('contractor');
        if ($loggedUser['role_id'] == $contractorRoleId) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect($this->router->assemble(array(), 'invoices'));
        } else {
            $this->view->sub_menu = 'duplicatedInvoices';

//
//load model
//
            $modelBookingInvoice = new Model_BookingInvoice();

//
// get data list
//
            $data = $modelBookingInvoice->getDuplicatedInvoiceNumbers();
//$modelBookingInvoice->fills($data, array('booking', 'contractors', 'customer', 'city', 'labels', 'due_date', 'booking_users', 'have_attachment', 'services'));
//
// set view params
//
            $this->view->data = $data;
        }
    }

    public function deleteAction() {

//
// check Auth for logged user
//
        CheckAuth::checkPermission(array('invoiceDelete'));

//
// get request parameters
//
        $invoiceId = $this->request->getParam('id', 0);
        $invoiceIds = $this->request->getParam('ids', array());
        if ($invoiceId) {
            $invoiceIds[] = $invoiceId;
        }

//
// load model
//
        $modelBooking = new Model_Booking();
        $modelBookingInvoice = new Model_BookingInvoice();


        $is_draft = TRUE;
        $tables = array();
        foreach ($invoiceIds as $invoiceId) {
            $invoice = $modelBookingInvoice->getById($invoiceId);
            if ('draft' == $invoice['invoice_type']) {
                if (CheckAuth::checkIfCanHandelAllCompany('invoice', $invoiceId)) {
                    if ($modelBooking->checkIfCanDeleteBooking($invoice['booking_id'])) {
//check if the invoice used in other place
                        if ($modelBookingInvoice->checkBeforeDeleteInvoiceByBookingId($invoice['booking_id'])) {
                            $modelBookingInvoice->updateById($invoiceId, array('is_deleted' => 1));
                            $modelBooking->updateById($invoice['booking_id'], array('convert_status' => 'booking'));
                        } else {
                            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Failed to delete, invoice is used in (" . implode(',', $tables) . ')'));
                        }
                    }
                }
            } else {
                $is_draft = FALSE;
            }
        }

        if ($is_draft) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "The selected invoice(s) have been deleted"));
        } else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Only status of draft invoice(s) can be deleted"));
        }

        $this->_redirect($this->router->assemble(array(), 'invoices'));
    }

    public function convertToOpenAction() {

//
// check Auth for logged user
//
        CheckAuth::checkPermission(array('convertToOpen'));

//
// get request parameters
//
        $invoiceId = $this->request->getParam('id', 0);
        $invoiceIds = $this->request->getParam('ids', array());
        if ($invoiceId) {
            $invoiceIds[] = $invoiceId;
        }

//
// load model
//
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelBooking = new Model_Booking();

        $is_draft = TRUE;
        foreach ($invoiceIds as $invoiceId) {
            $invoice = $modelBookingInvoice->getById($invoiceId);
            if ('draft' == $invoice['invoice_type']) {
                if (CheckAuth::checkIfCanHandelAllCompany('invoice', $invoiceId)) {
                    if ($modelBooking->checkCanEditBookingDetails($invoice['booking_id'])) {
                        $modelBookingInvoice->updateById($invoiceId, array('invoice_type' => 'open'));
                    }
                }
            } else {
                $is_draft = FALSE;
            }
        }


        if ($is_draft) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Status of the invoice(s) has been changed to open."));
        } else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Only status of draft invoice(s) can be changed to open"));
        }

        $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
    }

    public function convertAction() {

//
// check Auth for logged user
//
        CheckAuth::checkPermission(array('convert'));


//
// get request parameters
//
        $invoiceId = $this->request->getParam('id', 0);
        $type = $this->request->getParam('type', 'draft');

        if (!CheckAuth::checkIfCanHandelAllCompany('invoice', $invoiceId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        if (!in_array($type, array('draft', 'void'))) {
            $type = 'draft';
        }

//
// load model
//
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelBooking = new Model_Booking();
        $modelBookingLog = new Model_BookingLog();

        $invoice = $modelBookingInvoice->getById($invoiceId);

        $no_change = TRUE;
        if ($type != $invoice['invoice_type']) {

            $no_change = FALSE;
            $modelPayment = new Model_Payment();

            $payment = $modelPayment->getAll(array('booking_id' => $invoice['booking_id']));

            $have_payment = FALSE;
            if (empty($payment)) {
                $have_payment = TRUE;
                if ($modelBooking->checkCanEditBookingDetails($invoice['booking_id'])) {

// add  data log
                    $modelBookingLog->addBookingLog($invoice['booking_id']);

                    $modelBookingInvoice->updateById($invoiceId, array('invoice_type' => $type));
                }
            }

            if ($have_payment) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Status of the invoice(s) has been changed to {$type}"));
            } else {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This invoice(s) have payment can't be changed to {$type}"));
            }
        }

        if ($no_change) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Status of the invoice(s) is {$type}"));
        }

        $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
    }

    /*
      public function viewAction() {

      //
      // check Auth for logged user
      //
      CheckAuth::checkPermission(array('invoiceView'));

      //
      // get params
      //
      $invoiceId = $this->request->getParam('id', 0);


      if (!CheckAuth::checkIfCanHandelAllCompany('invoice', $invoiceId)) {
      $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
      $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
      }
      //
      // load models
      //
      $modelBookingInvoice = new Model_BookingInvoice();
      $modelBooking = new Model_Booking();
      $modelCustomer = new Model_Customer();
      $modelCustomerType = new Model_CustomerType();

      //
      // geting data
      //
      $invoice = $modelBookingInvoice->getById($invoiceId);
      $this->view->invoice = $invoice;

      $modelPaymant = new Model_Payment();
      $payment = $modelPaymant->getByBookingId($invoice['booking_id']);
      $this->view->payment = $payment;
      //
      // validation
      //
      if (!$invoice) {
      $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Invoice not exist 1"));
      $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
      }
      //check in can see his or assigned invoices
      if (!$modelBookingInvoice->checkIfCanSeeHisOrAssignedInvoice($invoiceId)) {
      $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
      $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
      }

      $booking = $modelBooking->getById($invoice['booking_id']);
      $this->view->booking = $booking;
      $this->view->isAccepted = $modelBooking->checkBookingIfAccepted($invoice['booking_id']);

      //address

      $modelBookingAddress = new Model_BookingAddress();
      $bookingAddress = $modelBookingAddress->getByBookingId($invoice['booking_id']);
      $this->view->lineAddress = get_line_address($bookingAddress);


      $customer = $modelCustomer->getById($booking['customer_id']);

      //customerType
      $customerType = $modelCustomerType->getById($customer['customer_type_id']);
      $this->view->customerType = $customerType;

      // customer type work order is_required Message
      $isWorkOrder = false;
      $workOrder = $modelCustomerType->getCustomerTypeIsWorkOrder();

      if (in_array($customer['customer_type_id'], $workOrder)) {

      $modelBookingAttachment = new Model_BookingAttachment();

      $bookingAttachments = $modelBookingAttachment->getByBookingIdOrInquiryId($invoice['booking_id'], $booking['original_inquiry_id']);
      ;
      $isWorkOrder = true;
      if (!empty($bookingAttachments)) {
      foreach ($bookingAttachments as $attachment) {
      if ($attachment['work_order'] == 1) {
      $isWorkOrder = false;
      }
      }
      }
      }

      $this->view->isWorkOrder = $isWorkOrder;
      ///////////////By Islam get contractor payment info
      /////////at first we get contractor from Contractor_service_booking by booking_id of the invoice
      $modelContractorServiceBooking=new Model_ContractorServiceBooking();
      $ServiceBookingDetails=$modelContractorServiceBooking->getByBookingId($invoice['booking_id']);
      $contractorId=$ServiceBookingDetails[0]['contractor_id'];

      $modelBookingContractorPayment=new Model_BookingContractorPayment();
      $bookingContractorPayment = $modelBookingContractorPayment->getBybookingIdAndContractorId($invoice['booking_id'], $contractorId);
      $this->view->bookingContractorPayment = $bookingContractorPayment;

      /////////get contractor name from user table
      $modelUser = new Model_User();
      $contractor = $modelUser->getById($contractorId);
      $this->view->contractorName = $contractor['username'];
      $this->view->contractorId = $contractor['user_id'];
      //////////////
      //
      // get Invoice View Param
      //
      $this->getInvoiceViewParam($invoiceId);


      ////get all dates Extra Info
      $modelVisitedExtraInfo = new Model_VisitedExtraInfo();
      $extraInfo = $modelVisitedExtraInfo->getByBookingId2($invoice['booking_id']);
      $this->view->extraInfo = $extraInfo;

      $modelContractorInfo = new Model_ContractorInfo();
      $contractorInfo = $modelContractorInfo->getByContractorId($contractorId);
      $this->view->contractorInfo = $contractorInfo;
      //////////////

      $modelPayment = new Model_Payment();
      $cash_amount = $modelPayment->getCashPayments($invoice['booking_id']);

      $this->view->cash_amount = $cash_amount;

      }
     */

    public function viewAction() {

//
// check Auth for logged user
//
        CheckAuth::checkPermission(array('invoiceView'));

//
// get params 
//
        $invoiceId = $this->request->getParam('id', 0);


        if (!CheckAuth::checkIfCanHandelAllCompany('invoice', $invoiceId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        //Rand
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($invoiceId, 'booking_invoice', 'viewed');
// load models
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelBooking = new Model_Booking();
        $modelCustomer = new Model_Customer();
        $modelCustomerType = new Model_CustomerType();

//D.A 28/09/2015 Caching invoice view blocks
        require_once 'Zend/Cache.php';
        $company_id = CheckAuth::getCompanySession();
        $invoiceViewDir = get_config('cache') . '/' . 'invoicesView' . '/' . $company_id;
        if (!is_dir($invoiceViewDir)) {
            mkdir($invoiceViewDir, 0777, true);
        }
        $frontEndOption = array('lifetime' => NULL,
            'automatic_serialization' => true);
        $backendOptions = array('cache_dir' => $invoiceViewDir);
        $cache = Zend_Cache::factory('Core', 'File', $frontEndOption, $backendOptions);

        $cacheID = $invoiceId . '_invoiceDetails';
        if (($result = $cache->load($cacheID)) === false) {
// geting data
            $invoice = $modelBookingInvoice->getById($invoiceId);
// validation
            if (!$invoice) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Invoice not found"));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            }

//check in can see his or assigned invoices
            if (!$modelBookingInvoice->checkIfCanSeeHisOrAssignedInvoice($invoiceId)) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            }

            $booking = $modelBooking->getById($invoice['booking_id']);

//address
            $modelBookingAddress = new Model_BookingAddress();
            $bookingAddress = $modelBookingAddress->getByBookingId($invoice['booking_id']);

            $result = array(
                'invoice' => $invoice,
                'booking' => $booking,
                'bookingAddress' => $bookingAddress
            );
            if ($result) {
                $cache->save($result, $cacheID);
            }
        } else {
            $invoice = $result['invoice'];
            $booking = $result['booking'];
            $bookingAddress = $result['bookingAddress'];
        }

        $this->view->page_title = $invoice['invoice_num'] . ' - ' . $this->view->page_title;

        $this->view->invoice = $invoice;
        $this->view->booking = $booking;
        $this->view->lineAddress = get_line_address($bookingAddress);



        $modelPaymant = new Model_Payment();
        $payment = $modelPaymant->getByBookingId($invoice['booking_id']);
        $this->view->payment = $payment;

        $this->view->isAccepted = $modelBooking->checkBookingIfAccepted($invoice['booking_id']);

        $customer = $modelCustomer->getById($booking['customer_id']);



//customerType
        $customerType = $modelCustomerType->getById($customer['customer_type_id']);
        $this->view->customerType = $customerType;

// customer type work order is_required Message
        $isWorkOrder = false;
        $workOrder = $modelCustomerType->getCustomerTypeIsWorkOrder();

        if (in_array($customer['customer_type_id'], $workOrder)) {

            $modelBookingAttachment = new Model_BookingAttachment();

            $bookingAttachments = $modelBookingAttachment->getByBookingIdOrInquiryId($invoice['booking_id'], $booking['original_inquiry_id']);
            ;
            $isWorkOrder = true;
            if (!empty($bookingAttachments)) {
                foreach ($bookingAttachments as $attachment) {
                    if ($attachment['work_order'] == 1) {
                        $isWorkOrder = false;
                    }
                }
            }
        }
        $this->view->isWorkOrder = $isWorkOrder;

//D.A 28/09/2015 Invoice Technician Update Details cache
        $cacheID = $invoiceId . '_invoicePaymentToTechnician';
        if (($result = $cache->load($cacheID)) === false) {
//By Islam get contractor payment info
//at first we get contractor from Contractor_service_booking by booking_id of the invoice
            $modelContractorServiceBooking = new Model_ContractorServiceBooking();
            $ServiceBookingDetails = $modelContractorServiceBooking->getByBookingId($invoice['booking_id']);
            $contractorId = $ServiceBookingDetails[0]['contractor_id'];

            $modelBookingContractorPayment = new Model_BookingContractorPayment();
            $bookingContractorPayment = $modelBookingContractorPayment->getBybookingIdAndContractorId($invoice['booking_id'], $contractorId);

//get contractor name from user table
            $modelUser = new Model_User();
            $contractor = $modelUser->getById($contractorId);
            $contractorName = $contractor['username'];
            $contractorId = $contractor['user_id'];

            $modelPayment = new Model_Payment();
            $cash_amount = $modelPayment->getCashPayments($invoice['booking_id']);

            $result = array(
                'bookingContractorPayment' => $bookingContractorPayment,
                'contractorName' => $contractorName,
                'contractorId' => $contractorId,
                'cash_amount' => $cash_amount
            );
            if ($result) {
                $cache->save($result, $cacheID);
            }
        } else {
            $bookingContractorPayment = $result['bookingContractorPayment'];
            $contractorName = $result['contractorName'];
            $contractorId = $result['contractorId'];
            $cash_amount = $result['cash_amount'];
        }

        $this->view->bookingContractorPayment = $bookingContractorPayment;
        $this->view->contractorName = $contractorName;
        $this->view->contractorId = $contractorId;
        $this->view->cash_amount = $cash_amount;

        $modelContractorInfo = new Model_ContractorInfo();
        $contractorInfo = $modelContractorInfo->getByContractorId($contractorId);
        $this->view->contractorInfo = $contractorInfo;

// get Invoice View Param
        $this->getInvoiceViewParam($invoiceId);

//D.A 28/09/2015 Invoice Technician Update Details cache
        $cacheID = $invoiceId . '_invoiceTechnicianUpdateDetails';
        if (($result = $cache->load($cacheID)) === false) {
//get all dates Extra Info
            $modelVisitedExtraInfo = new Model_VisitedExtraInfo();
            $extraInfo = $modelVisitedExtraInfo->getByBookingId2($invoice['booking_id']);
            $result = $extraInfo;
            if ($result) {
                $cache->save($result, $cacheID);
            }
        } else {
            $extraInfo = $result;
        }
        $this->view->extraInfo = $extraInfo;

//D.A 13/10/2015		
        $cacheID = $invoiceId . '_invoiceScheduledVisits';
        if (($result = $cache->load($cacheID)) === false) {
            $modelBookingMultipleDays = new Model_BookingMultipleDays();
            $primaryDateExtraInfo = $modelBooking->getExtraInfoBookingPrimaryDatesByBooking($invoice['booking_id']);
            $multipleDaysWithVisitedExtraInfo = $modelBookingMultipleDays->getMultipleDaysWithVisitedByBookingId($invoice['booking_id']);
            $result = array(
                'primaryDateExtraInfo' => $primaryDateExtraInfo,
                'multipleDaysWithVisitedExtraInfo' => $multipleDaysWithVisitedExtraInfo
            );
            if ($result) {
                $cache->save($result, $cacheID);
            }
        } else {
            $primaryDateExtraInfo = $result['primaryDateExtraInfo'];
            $multipleDaysWithVisitedExtraInfo = $result['multipleDaysWithVisitedExtraInfo'];
        }

        $modelImage = new Model_Image();
        $pager = null;
        $photo = $modelImage->getAll($invoice['booking_id'], 'booking', "i.created desc", $pager, $filter = array(), $limit = 10);
        $photoCount = count($modelImage->getAll($invoice['booking_id'], 'booking', "i.created desc"));

        $this->view->type = 'booking';
        $this->view->photo = $photo;
        $this->view->photoCount = $photoCount;

        $this->view->primaryDateExtraInfo = $primaryDateExtraInfo;
        $this->view->multipleDaysWithVisitedExtraInfo = $multipleDaysWithVisitedExtraInfo;

        $trading_namesObj = new Model_TradingName();
        $trading_names = $trading_namesObj->getById($booking['trading_name_id']);
        $this->view->trading_name = $trading_names;
    }

    public function previewAction() {
//
// get params 
//
        $invoiceId = $this->request->getParam('id', 0);

        if (!CheckAuth::checkIfCanHandelAllCompany('invoice', $invoiceId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
//
// load models
//
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelBooking = new Model_Booking();
        $modelCustomer = new Model_Customer();

//
// geting data
//
        $invoice = $modelBookingInvoice->getById($invoiceId);

//
// validation
//
        if (!$invoice) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Invoice not found"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
//check in can see his or assigned invoices
        if (!$modelBookingInvoice->checkIfCanSeeHisOrAssignedInvoice($invoiceId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $booking = $modelBooking->getById($invoice['booking_id']);
        $this->view->booking = $booking;
        $this->view->isAccepted = $modelBooking->checkBookingIfAccepted($invoice['booking_id']);

        $customer = $modelCustomer->getById($booking['customer_id']);
        $modelCustomer->fill($customer, array('customer_commercial_info', 'customer_contacts'));
        $this->view->customer = $customer;
        $this->view->customerContacts = $customer['customer_contacts'];

        $this->getInvoiceViewParam($invoiceId);

        echo $this->view->render('index/preview.phtml');
        exit;
    }

    public function sendReminderOverdueInvoiceAsEmailAction() {

//
// check Auth for logged user
//
        CheckAuth::checkPermission(array('sendInvoiceAsEmail'));

//
// get params 
//
        $invoiceId = $this->request->getParam('id', 0);

        if (!CheckAuth::checkIfCanHandelAllCompany('invoice', $invoiceId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

//
// load models
//
        $modelUser = new Model_User();
        $modelCustomer = new Model_Customer();
        $modelBookingAddress = new Model_BookingAddress();
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelBooking = new Model_Booking();

//
// geting data
//
        $invoice = $modelBookingInvoice->getById($invoiceId);
        $modelBookingInvoice->fill($invoice, array('booking', 'number_of_due_days', 'due_date'));

//
// validation
//
        if (!$invoice) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Invoice not found"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
//check in can see his or assigned invoices
        if (!$modelBookingInvoice->checkIfCanSeeHisOrAssignedInvoice($invoiceId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

//
// filling extra data
//
        $customer = $modelCustomer->getById($invoice['booking']['customer_id']);
        $modelCustomer->fill($customer, array('customer_commercial_info', 'customer_contacts'));
        $this->view->customerContacts = $customer['customer_contacts'];

        $user = $modelUser->getById($invoice['booking']['created_by']);

        $viewParam = $this->getInvoiceViewParam($invoiceId, true);
        $view = new Zend_View();
        $view->setScriptPath(APPLICATION_PATH . '/modules/invoices/views/scripts/index');
        $view->bookingServices = $viewParam['bookingServices'];
        $view->thisBookingServices = $viewParam['thisBookingServices'];
        $view->priceArray = $viewParam['priceArray'];
        $view->invoice = $viewParam['invoice'];
        $view->customer = $customer;
        $view->isAccepted = $modelBooking->checkBookingIfAccepted($invoice['booking_id']);
        $view->booking = $invoice['booking'];
        $bodyInvoice = $view->render('invoice.phtml');
		$dateTimeObj= get_settings_date_format();
		if(!empty($dateTimeObj))
		{
          $invoice_created=getNewDateFormat($invoice['created']);
          $booking_created=getNewDateFormat($invoice['booking']['created']);
          $booking_start=getNewDateFormat(strtotime($invoice['booking']['booking_start']),'all');

			
		} 
		else{
			 $invoice_created=date('d/m/Y', $invoice['created']);
		     $booking_created=date('d/m/Y', $invoice['booking']['created']);
			 $booking_start=date("F j, Y, g:i a", strtotime($invoice['booking']['booking_start']));


		}

        $template_params = array(
//invoice
            '{invoice_num}' => $invoice['invoice_num'],
           // '{invoice_created}' => date('d/m/Y', $invoice['created']),
            //'{invoice_created}' => getNewDateFormat($invoice['created']),
           '{invoice_created}' => $invoice_created,
			'{due}' => $invoice['due'] > 1 ? $invoice['due'] . ' Days' : $invoice['due'] . ' Day',
            '{due_date}' => $invoice['due_date'] > 1 ? $invoice['due_date'] . ' Days' : $invoice['due_date'] . ' Day',
            //booking
            '{booking_num}' => $invoice['booking']['booking_num'],
            '{total_without_tax}' => number_format($invoice['booking']['sub_total'], 2),
            '{gst_tax}' => number_format($invoice['booking']['gst'], 2),
            '{total_with_tax}' => number_format($invoice['booking']['qoute'], 2),
            '{paid_amount}' => number_format($invoice['booking']['paid_amount'], 2),
            '{unpaid_amount}' => number_format($invoice['booking']['qoute'] - $invoice['booking']['paid_amount'], 2),
            '{description}' => $invoice['booking']['description'] ? $invoice['booking']['description'] : '',
            //'{booking_created}' => getNewDateFormat($invoice['booking']['created']),
            //'{booking_created}' => date('d/m/Y', $invoice['booking']['created']),
            '{booking_created}' => $booking_created,

			'{booking_created_by}' => ucwords($user['username']),
            //'{booking_start}' => getNewDateFormat(strtotime($invoice['booking']['booking_start']),'all'),
			//'{booking_start}' => date("F j, Y, g:i a", strtotime($invoice['booking']['booking_start'])),
			'{booking_start}' => $booking_start,
			'{booking_address}' => get_line_address($modelBookingAddress->getByBookingId($invoice['booking']['booking_id'])),
            '{service}' => nl2br($modelContractorServiceBooking->getBookingAsText($invoice['booking']['booking_id'], true)),
            '{invoice_view}' => $bodyInvoice,
            //customer
            '{customer_name}' => get_customer_name($customer),
            '{customer_first_name}' => isset($customer['first_name']) && $customer['first_name'] ? ucwords($customer['first_name']) : '',
            '{customer_last_name}' => isset($customer['last_name']) && $customer['last_name'] ? ' ' . ucwords($customer['last_name']) : '',
            '{customer_contacts}' => nl2br($modelCustomer->getCustomerContacts($invoice['booking']['customer_id'])),
        );

        $modelEmailTemplate = new Model_EmailTemplate();
        $emailTemplate = $modelEmailTemplate->getEmailTemplate('reminder_overdue_invoice', $template_params);

        $body = $emailTemplate['body'];
        $subject = $emailTemplate['subject'];
        $to = array();
        if ($customer['email1']) {
            $to[] = $customer['email1'];
        }
        if ($customer['email2']) {
            $to[] = $customer['email2'];
        }
        if ($customer['email2']) {
            $to[] = $customer['email3'];
        }
        $to = implode(',', $to);

        if ($this->request->isPost()) {
            $to = $this->request->getParam('to');
            $cc = $this->request->getParam('cc');
            $subject = $this->request->getParam('subject');
            $body = $this->request->getParam('body');
            $pdf_attachment = $this->request->getParam('pdf_attachment', 0);

            $trading_namesObj = new Model_TradingName();
            $trading_names = $trading_namesObj->getById($invoice['booking']['trading_name_id']);

            $params = array(
                'to' => 'jass-moh@hotmail.com',
                'cc' => $cc,
                'body' => $body,
                'subject' => $subject,
                'trading_name' => $trading_names['trading_name'],
                'from' => $trading_names['email'],
            );

            $error_mesages = array();
            if (EmailNotification::validation($params, $error_mesages)) {

                if (!empty($pdf_attachment)) {
// Create pdf
                    $pdfPath = createPdfPath();
                    $destination = $pdfPath['fullDir'] . $viewParam['invoice']['invoice_num'] . '.pdf';
                    wkhtmltopdf($bodyInvoice, $destination);
                    $params['attachment'] = $destination;
                }

// Send Email
                $success = EmailNotification::sendEmail($params, '', array(), array('reference_id' => $invoice['id'], 'type' => 'invoice'));

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Invoice was sent successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Failed to send invoice"));
                }

                echo 1;
                exit;
            }
            $this->view->error_mesages = $error_mesages;
        }

        $this->view->invoice = $invoice;
        $this->view->to = $to;
        $this->view->subject = $subject;
        $this->view->body = $body;
        $this->view->cc = isset($cc) ? $cc : '';

        echo $this->view->render('index/send-reminder-overdue-invoice-as-email.phtml');
        exit;
    }

    public function sendInvoiceAsEmailAction() {

//
// check Auth for logged user
//
        CheckAuth::checkPermission(array('sendInvoiceAsEmail'));

//
// get params 
//
        $invoiceId = $this->request->getParam('id', 0);

        if (!CheckAuth::checkIfCanHandelAllCompany('invoice', $invoiceId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

//
// load models
//
        $modelUser = new Model_User();
        $modelBooking = new Model_Booking();
        $modelCustomer = new Model_Customer();
        $modelBookingAddress = new Model_BookingAddress();
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelPayment = new Model_Payment();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();

//
// geting data
//
        $invoice = $modelBookingInvoice->getById($invoiceId);
        $modelBookingInvoice->fill($invoice, array('booking'));

//
// validation
//
        if (!$invoice) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Invoice not found"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
//check in can see his or assigned invoices
        if (!$modelBookingInvoice->checkIfCanSeeHisOrAssignedInvoice($invoiceId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

//
// filling extra data
//
        $customer = $modelCustomer->getById($invoice['booking']['customer_id']);
        $modelCustomer->fill($customer, array('customer_commercial_info', 'customer_contacts'));
        $this->view->customerContacts = $customer['customer_contacts'];

        $user = $modelUser->getById($invoice['booking']['created_by']);

        $viewParam = $this->getInvoiceViewParam($invoiceId, true);
        $view = new Zend_View();
        $view->setScriptPath(APPLICATION_PATH . '/modules/invoices/views/scripts/index');
        $view->bookingServices = $viewParam['bookingServices'];
        $view->thisBookingServices = $viewParam['thisBookingServices'];
        $view->priceArray = $viewParam['priceArray'];
        $view->invoice = $viewParam['invoice'];
        $view->customer = $customer;
        $view->isAccepted = $modelBooking->checkBookingIfAccepted($invoice['booking_id']);
        $view->booking = $invoice['booking'];
        $bodyInvoice = $view->render('invoice.phtml');

        /*         * ********allow customer to pay invoice directly if not fully paid***********IBM */
        $totalAmount = $modelBooking->getTotalAmountBookingDetails($invoice['booking_id']);
        $allPayment = $modelPayment->getTotalAmount(array('booking_id' => $invoice['booking_id'], 'is_approved' => 'all'));

        $is_fully_paid = $totalAmount['total_amount'] - $allPayment;
        if (isset($is_fully_paid) && $is_fully_paid > 0) {

            $eway_link = $this->router->assemble(array('booking_id' => $invoice['booking']['booking_id']), 'paymentEwayAdd') . "?customer_id=" . $invoice['booking']['customer_id'];
            $link = '<a style="box-shadow:inset 0px 1px 0px 0px #d9fbbe;background:linear-gradient(to bottom, #b8e356 5%, #a5cc52 100%);background-color:#b8e356;  border-radius:6px;border:1px solid #83c41a;display:inline-block;color:#ffffff;font-family:Arial;font-size:15px;font-weight:bold;padding:6px 24px;text-decoration:none;text-shadow:0px 1px 0px #86ae47;" href="' . $eway_link . '">Pay Invoice</a>';
        } else {
            $link = ' ';
        }
        /*         * ***********End************** */


        $totalAmountDetails = $modelBooking->getTotalAmountBookingDetails($invoice['booking']['booking_id'], true);
        $totalQoute = $totalAmountDetails['total'];
        $sub_total = $totalAmountDetails['sub_total'];

		$dateTimeObj= get_settings_date_format();
		if(!empty($dateTimeObj))
		{
            $invoice_created=getNewDateFormat($invoice['created']);
			$booking_created=getNewDateFormat($invoice['booking']['created']);
            $booking_start=getNewDateFormat(strtotime($invoice['booking']['booking_start']),'all');
	
		} 
		else{
			$invoice_created= date('d/m/Y', $invoice['created']);
			$booking_created=date('d/m/Y', $invoice['booking']['created']);
            $booking_start= date("F j, Y, g:i a", strtotime($invoice['booking']['booking_start']));
		   }


        $template_params = array(
//invoice
            '{invoice_num}' => $invoice['invoice_num'],
            //'{invoice_created}' => getNewDateFormat($invoice['created']),
			//'{invoice_created}' => date('d/m/Y', $invoice['created']),
			'{invoice_created}' => $invoice_created,


            //'{link}' => $link,
//booking
            '{booking_num}' => $invoice['booking']['booking_num'],
            '{total_without_tax}' => number_format($sub_total, 2),
            '{gst_tax}' => number_format($invoice['booking']['gst'], 2),
            '{total_with_tax}' => number_format($totalQoute, 2),
            '{description}' => $invoice['booking']['description'] ? $invoice['booking']['description'] : '',
			//'{booking_created}' => getNewDateFormat($invoice['booking']['created']),
			//'{booking_created}' => date('d/m/Y', $invoice['booking']['created']),
			'{booking_created}' => $booking_created,
			'{booking_created_by}' => ucwords($user['username']),
            //'{booking_start}' => getNewDateFormat(strtotime($invoice['booking']['booking_start']),'all'),
            //'{booking_start}' => date("F j, Y, g:i a", strtotime($invoice['booking']['booking_start'])),
            '{booking_start}' => $booking_start,
			'{booking_address}' => get_line_address($modelBookingAddress->getByBookingId($invoice['booking']['booking_id'])),
            '{service}' => nl2br($modelContractorServiceBooking->getBookingAsText($invoice['booking']['booking_id'], true)),
            '{invoice_view}' => $bodyInvoice,
            //customer
            '{customer_name}' => get_customer_name($customer),
            '{customer_first_name}' => isset($customer['first_name']) && $customer['first_name'] ? ucwords($customer['first_name']) : '',
            '{customer_last_name}' => isset($customer['last_name']) && $customer['last_name'] ? ' ' . ucwords($customer['last_name']) : '',
            '{customer_contacts}' => nl2br($modelCustomer->getCustomerContacts($invoice['booking']['customer_id'])),
        );




        if (my_ip()) {
			
            $template_params = array(
//invoice
                '{invoice_num}' => $invoice['invoice_num'],
                //'{invoice_created}' => getNewDateFormat($invoice['created']),
                '{invoice_created}' => $invoice_created,
                //'{invoice_created}' => date('d/m/Y', $invoice['created']),
                '{link}' => $link,
                //booking
                '{booking_num}' => $invoice['booking']['booking_num'],
                '{total_without_tax}' => number_format($sub_total, 2),
                '{gst_tax}' => number_format($invoice['booking']['gst'], 2),
                '{total_with_tax}' => number_format($totalQoute, 2),
                '{description}' => $invoice['booking']['description'] ? $invoice['booking']['description'] : '',
                //'{booking_created}' => getNewDateFormat($invoice['booking']['created']),
                //'{booking_created}' => date('d/m/Y', $invoice['booking']['created']),
                '{booking_created}' => $booking_created,
				'{booking_created_by}' => ucwords($user['username']),
                //'{booking_start}' => getNewDateFormat(strtotime($invoice['booking']['booking_start']),'all'),
				//'{booking_start}' => date("F j, Y, g:i a", strtotime($invoice['booking']['booking_start'])),
				'{booking_start}' => $booking_start,
				'{booking_address}' => get_line_address($modelBookingAddress->getByBookingId($invoice['booking']['booking_id'])),
                '{service}' => nl2br($modelContractorServiceBooking->getBookingAsText($invoice['booking']['booking_id'], true)),
                '{invoice_view}' => $bodyInvoice,
                //customer
                '{customer_name}' => get_customer_name($customer),
                '{customer_first_name}' => isset($customer['first_name']) && $customer['first_name'] ? ucwords($customer['first_name']) : '',
                '{customer_last_name}' => isset($customer['last_name']) && $customer['last_name'] ? ' ' . ucwords($customer['last_name']) : '',
                '{customer_contacts}' => nl2br($modelCustomer->getCustomerContacts($invoice['booking']['customer_id'])),
            );

            $modelEmailTemplate = new Model_EmailTemplate();
            $emailTemplate = $modelEmailTemplate->getEmailTemplate('send_invoice_as_email_test', $template_params);
        } else {
            $modelEmailTemplate = new Model_EmailTemplate();
            $emailTemplate = $modelEmailTemplate->getEmailTemplate('send_invoice_as_email', $template_params);
        }

        $body = $emailTemplate['body'];
        $subject = $emailTemplate['subject'];
        $to = array();
        if ($customer['email1']) {
            $to[] = $customer['email1'];
        }
        if ($customer['email2']) {
            $to[] = $customer['email2'];
        }
        if ($customer['email2']) {
            $to[] = $customer['email3'];
        }
        $to = implode(',', $to);

        if ($this->request->isPost()) {
            $to = $this->request->getParam('to');
            $cc = $this->request->getParam('cc');
            $subject = $this->request->getParam('subject');
            $body = $this->request->getParam('body');
            $pdf_attachment = $this->request->getParam('pdf_attachment', 0);
            $trading_namesObj = new Model_TradingName();
            $trading_names = $trading_namesObj->getById($invoice['booking']['trading_name_id']);


            $params = array(
                'to' => 'jass-moh@hotmail.com',
                'cc' => $cc,
                'body' => $body,
                'subject' => $subject,
                'trading_name' => $trading_names['trading_name'],
                'from' => $trading_names['email'],
            );

            $error_mesages = array();
            if (EmailNotification::validation($params, $error_mesages)) {

                if (!empty($pdf_attachment)) {
// Create pdf
                    $pdfPath = createPdfPath();
                    $destination = $pdfPath['fullDir'] . $viewParam['invoice']['invoice_num'] . '.pdf';
                    wkhtmltopdf($bodyInvoice, $destination);
                    $params['attachment'] = $destination;
                }

// Send Email
                $success = EmailNotification::sendEmail($params, '', array(), array('reference_id' => $invoice['id'], 'type' => 'invoice'));

                if ($success) {
                    MobileNotificationNew::notify('invoice email to client' , array('item_type'=>'invoice' , 'item_id'=>$invoice['id']));
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Invoice sent"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Failed to send invoice"));
                }

                echo 1;
                exit;
            }
            $this->view->error_mesages = $error_mesages;
        }

        $this->view->invoice = $invoice;
        $this->view->to = $to;
        $this->view->subject = $subject;
        $this->view->body = $body;
        $this->view->cc = isset($cc) ? $cc : '';

        echo $this->view->render('index/send-invoice-as-email.phtml');
        exit;
    }

    public function downloadInvoiceAction() {

//
// check Auth for logged user
//
        CheckAuth::checkPermission(array('downloadInvoice'));

//
// get params 
//
        $invoiceId = $this->request->getParam('id', 0);

        if (!CheckAuth::checkIfCanHandelAllCompany('invoice', $invoiceId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
//
// load models
//
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelBooking = new Model_Booking();
        $modelCustomer = new Model_Customer();

//
// geting data
//
        $invoice = $modelBookingInvoice->getById($invoiceId);

//
// validation
//
        if (!$invoice) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Invoice not found"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
//check in can see his or assigned invoices
        if (!$modelBookingInvoice->checkIfCanSeeHisOrAssignedInvoice($invoiceId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $booking = $modelBooking->getById($invoice['booking_id']);
        $customer = $modelCustomer->getById($booking['customer_id']);

/////Islam get customer_commercial_info
        $modelCustomer = new Model_Customer();
        $modelCustomer->fill($customer, array('customer_commercial_info', 'customer_contacts'));

/////////////////////*/
        $viewParam = $this->getInvoiceViewParam($invoiceId, true);

        $view = new Zend_View();
        $view->setScriptPath(APPLICATION_PATH . '/modules/invoices/views/scripts/index');
        $view->bookingServices = $viewParam['bookingServices'];
        $view->thisBookingServices = $viewParam['thisBookingServices'];
        $view->priceArray = $viewParam['priceArray'];
        $view->invoice = $viewParam['invoice'];
        $view->customer = $customer;
        $view->isAccepted = $modelBooking->checkBookingIfAccepted($invoice['booking_id']);
        $view->booking = $booking;
//$view->businessName=$customer['customer_commercial_info']['business_name'];


        $html = $view->render('invoice.phtml');

        $pdfPath = createPdfPath();

        $filename = $pdfPath['fullDir'] . $viewParam['invoice']['invoice_num'] . '.pdf';
        wkhtmltopdf($html, $filename);

        header("Pragma: public");
        header("Expires: 0");
        header("Pragma: no-cache");
        header("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header('Content-disposition: attachment; filename=' . basename($filename));
        header("Content-Type: application/pdf");
        header("Content-Transfer-Encoding: binary");
        header('Content-Length: ' . filesize($filename));
        @readfile($filename);
        exit(0);
    }

    public function convertBookingToInvoiceAction() {
//
// check Auth for logged user
//
        CheckAuth::checkPermission(array('convertBookingToInvoice'));

//
// get request parameters
//
        $bookingId = $this->request->getParam('id', 0);

        if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $modelBooking = new Model_Booking();
        $modelBookingStatus = new Model_BookingStatus();
        $modelBookingInvoice = new Model_BookingInvoice();

        $success = false;
        $booking = $modelBooking->getById($bookingId);
        $convertStatus = $booking['convert_status'];
        $bookingInvoice = $modelBookingInvoice->getByBookingId($bookingId);
        $booking = $modelBooking->fill($booking, array('is_accepted'));

        if (!empty($bookingInvoice) && $convertStatus == 'invoice') {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This booking already has an invoice generated"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        if (!$booking['is_accepted']) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect($this->router->assemble(array(), 'booking'));
        }

        if ($modelBookingStatus->checkConvertToInvoice($booking['status_id'])) {

            $success = $modelBookingInvoice->convertToInvoice($bookingId);
        }

        if ($success) {
//D.A 27/08/2015 Remove Booking Cache
            require_once 'Zend/Cache.php';
            $bookingDetailsCacheID = $bookingId . '_bookingDetails';
            $company_id = CheckAuth::getCompanySession();
            $bookingViewDir = get_config('cache') . '/' . 'bookingsView' . '/' . $company_id;
            if (!is_dir($bookingViewDir)) {
                mkdir($bookingViewDir, 0777, true);
            }
            $frontEndOption = array('lifetime' => NULL,
                'automatic_serialization' => true);
            $backendOptions = array('cache_dir' => $bookingViewDir);
            $cache = Zend_Cache::factory('Core', 'File', $frontEndOption, $backendOptions);
            $cache->remove($bookingDetailsCacheID);

//D.A 10/09/2015 Remove inquiry Details Cache
            $inquiryDetailsCacheID = $booking['original_inquiry_id'] . '_inquiryDetails';
            $inquiryViewDir = get_config('cache') . '/' . 'inquiriesView' . '/' . $company_id;
            if (!is_dir($inquiryViewDir)) {
                mkdir($inquiryViewDir, 0777, true);
            }
            $inquiryDetailsFrontEndOption = array('lifetime' => NULL,
                'automatic_serialization' => true);
            $inquiryDetailsBackendOptions = array('cache_dir' => $inquiryViewDir);
            $inquiryDetailsCache = Zend_Cache::factory('Core', 'File', $inquiryDetailsFrontEndOption, $inquiryDetailsBackendOptions);
            $inquiryDetailsCache->remove($inquiryDetailsCacheID);

            $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Invoice generated"));
            $this->_redirect($this->router->assemble(array(), 'invoices'));
        } else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Only bookings with status (COMPLETED, IN PROGRESS, FAILED, TO DO) can generate invoices"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
    }

    /*
      public function getInvoiceViewParam($invoiceId, $toBuffer = false) {

      //
      // load model
      //
      $modelContractorServiceBooking = new Model_ContractorServiceBooking();
      $modelBookingInvoice = new Model_BookingInvoice();
      $modelContractorServiceBookingTemp = new Model_ContractorServiceBookingTemp();
      $modelBooking = new Model_Booking();

      //
      // geting data
      //
      $invoice = $modelBookingInvoice->getById($invoiceId);


      //
      // validation
      //
      if (!$invoice) {
      $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Invoice not exist 7"));
      $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
      }

      //
      // filling extra data
      //
      $modelBookingInvoice->fill($invoice, array('booking', 'labels', 'due_date', 'multiple_days'));

      // $thisBookingServices : to put all service for this booking
      $thisBookingServices = array();

      // $priceArray : to put all price and service for this booking
      $priceArray = array();



      //$bookingServices : to put all booking service from  original booking service (or from temp if the contractor change values untill approved)


      //$bookingServices = $modelContractorServiceBooking->getByBookingId($invoice['booking_id']);

      $contractorServiceBookings = $modelContractorServiceBooking->getByBookingId($invoice['booking_id']);
      $contractorServiceBookingsTemp = $modelContractorServiceBookingTemp->getByBookingId($invoice['booking_id']);
      $isTemp = false;
      $bookingServices = array();
      if ($contractorServiceBookingsTemp && !$modelBooking->checkCanEditBookingDetails($invoice['booking_id']) && !$toBuffer) {
      $bookingServices = $contractorServiceBookingsTemp;
      $isTemp = true;
      foreach ($bookingServices as $bookingService) {

      $serviceId = $bookingService['service_id'];
      $clone = $bookingService['clone'];
      $bookingId = $bookingService['booking_id'];

      $service_and_clone = $serviceId . ($clone ? '_' . $clone : '');

      $thisBookingServices[] = $service_and_clone;

      $priceArray[$service_and_clone] = $modelContractorServiceBookingTemp->getTotalServiceBookingQoute($bookingId, $serviceId, $clone);
      }
      } elseif ($contractorServiceBookings) {
      $bookingServices = $contractorServiceBookings;
      foreach ($bookingServices as $bookingService) {

      $serviceId = $bookingService['service_id'];
      $clone = $bookingService['clone'];
      $bookingId = $bookingService['booking_id'];

      $service_and_clone = $serviceId . ($clone ? '_' . $clone : '');

      $thisBookingServices[] = $service_and_clone;

      $priceArray[$service_and_clone] = $modelContractorServiceBooking->getTotalServiceBookingQoute($bookingId, $serviceId, $clone);
      }
      }

      if (!$toBuffer) {

      $this->view->invoice = $invoice;
      $this->view->bookingServices = $bookingServices;
      $this->view->thisBookingServices = $thisBookingServices;
      $this->view->priceArray = $priceArray;
      $this->view->isTemp = $isTemp;
      } else {
      $viewParam = array();

      $viewParam['invoice'] = $invoice;

      $viewParam['bookingServices'] = $bookingServices;
      $viewParam['thisBookingServices'] = $thisBookingServices;
      $viewParam['priceArray'] = $priceArray;
      //$viewParam['isTemp'] = $isTemp;

      return $viewParam;
      }

      return false;
      }
     */

    public function getInvoiceViewParam($invoiceId, $toBuffer = false) {

//
// load model
//
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelContractorServiceBookingTemp = new Model_ContractorServiceBookingTemp();
        $modelBooking = new Model_Booking();

//D.A 28/09/2015 Caching invoice view blocks
        require_once 'Zend/Cache.php';
        $company_id = CheckAuth::getCompanySession();
        $invoiceViewDir = get_config('cache') . '/' . 'invoicesView' . '/' . $company_id;
        if (!is_dir($invoiceViewDir)) {
            mkdir($invoiceViewDir, 0777, true);
        }
        $frontEndOption = array('lifetime' => NULL,
            'automatic_serialization' => true);
        $backendOptions = array('cache_dir' => $invoiceViewDir);
        $cache = Zend_Cache::factory('Core', 'File', $frontEndOption, $backendOptions);

//D.A 28/09/2015 invoice Parameters Cache     
        $cacheID = $invoiceId . '_invoiceParams';
        if (($result = $cache->load($cacheID)) === false) {
// geting data
            $invoice = $modelBookingInvoice->getById($invoiceId);

// validation
            if (!$invoice) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Invoice not found"));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            }

// filling extra data
            $modelBookingInvoice->fill($invoice, array('booking', 'labels', 'due_date', 'multiple_days'));
            $result = $invoice;
            if ($result) {
                $cache->save($result, $cacheID);
            }
        } else {
            $invoice = $result;
        }

//D.A 28/09/2015 invoice Services Cache     
        $cacheID = $invoiceId . '_invoiceServices';
        if (($result = $cache->load($cacheID)) === false) {
// $thisBookingServices : to put all service for this booking 
            $thisBookingServices = array();
            $priceArray = array();
            $contractorServiceBookings = $modelContractorServiceBooking->getByBookingId($invoice['booking_id']);
            $contractorServiceBookingsTemp = $modelContractorServiceBookingTemp->getByBookingId($invoice['booking_id']);
            $isTemp = false;
            $bookingServices = array();
            if ($contractorServiceBookingsTemp && !$modelBooking->checkCanEditBookingDetails($invoice['booking_id']) && !$toBuffer) {
                $bookingServices = $contractorServiceBookingsTemp;
                $isTemp = true;
                foreach ($bookingServices as $bookingService) {

                    $serviceId = $bookingService['service_id'];
                    $clone = $bookingService['clone'];
                    $bookingId = $bookingService['booking_id'];

                    $service_and_clone = $serviceId . ($clone ? '_' . $clone : '');

                    $thisBookingServices[] = $service_and_clone;

                    $priceArray[$service_and_clone] = $modelContractorServiceBookingTemp->getTotalServiceBookingQoute($bookingId, $serviceId, $clone);
                }
            } elseif ($contractorServiceBookings) {
                $bookingServices = $contractorServiceBookings;
                foreach ($bookingServices as $bookingService) {

                    $serviceId = $bookingService['service_id'];
                    $clone = $bookingService['clone'];
                    $bookingId = $bookingService['booking_id'];

                    $service_and_clone = $serviceId . ($clone ? '_' . $clone : '');

                    $thisBookingServices[] = $service_and_clone;

                    $priceArray[$service_and_clone] = $modelContractorServiceBooking->getTotalServiceBookingQoute($bookingId, $serviceId, $clone);
                }
            }

            $result = array(
                'bookingServices' => $bookingServices,
                'thisBookingServices' => $thisBookingServices,
                'priceArray' => $priceArray,
                'isTemp' => $isTemp
            );
            if ($result) {
                $cache->save($result, $cacheID);
            }
        } else {
            $bookingServices = $result['bookingServices'];
            $thisBookingServices = $result['thisBookingServices'];
            $priceArray = $result['priceArray'];
            $isTemp = $result['isTemp'];
        }

        if (!$toBuffer) {
            $this->view->invoice = $invoice;
            $this->view->bookingServices = $bookingServices;
            $this->view->thisBookingServices = $thisBookingServices;
            $this->view->priceArray = $priceArray;
            $this->view->isTemp = $isTemp;
        } else {
            $viewParam = array();
            $viewParam['invoice'] = $invoice;
            $viewParam['bookingServices'] = $bookingServices;
            $viewParam['thisBookingServices'] = $thisBookingServices;
            $viewParam['priceArray'] = $priceArray;

            return $viewParam;
        }

        return false;
    }

//D.A 29/09/2015 clear invoice cache
    public function invoiceCacheClearAction() {

//get request parameters
        $invoiceId = $this->request->getParam('id');

        require_once 'Zend/Cache.php';
        $company_id = CheckAuth::getCompanySession();
        $invoiceViewDir = get_config('cache') . '/' . 'invoicesView' . '/' . $company_id;
        $frontEndOption = array('lifetime' => NULL,
            'automatic_serialization' => true);
        $backendOptions = array('cache_dir' => $invoiceViewDir);
        $Cache = Zend_Cache::factory('Core', 'File', $frontEndOption, $backendOptions);
        $Cache->remove($invoiceId . '_invoiceDetails');
        $Cache->remove($invoiceId . '_invoicePaymentToTechnician');
        $Cache->remove($invoiceId . '_invoiceParams');
        $Cache->remove($invoiceId . '_invoiceTechnicianUpdateDetails');
        $Cache->remove($invoiceId . '_invoiceServices');

        $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Cache cleared"));
        $this->_redirect($this->router->assemble(array('id' => $invoiceId), 'invoiceView'));
    }

    public function setDueDateAction() {

//
// check Auth for logged user
//
        CheckAuth::checkPermission(array('invoices', 'setDueDate'));

//
// get params
//
        $bookingId = $this->request->getParam('booking_id');
        $dueDateId = $this->request->getParam('due_date_id');


        if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
//
// load model
//
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelBookingDueDate = new Model_BookingDueDate();


        $bookingDueDate = $modelBookingDueDate->getByBookingId($bookingId);

        $options = array('booking_id' => $bookingId);

        if ($bookingDueDate) {
            $options['due_date_id'] = $bookingDueDate['due_date_id'];
        }

        $form = new Invoices_Form_DueDate($options);


        if ($this->request->isPost()) {
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array('due_date_id' => $dueDateId);

                if ($bookingDueDate) {
                    $modelBookingDueDate->updateById($bookingDueDate['id'], $data);
                } else {
                    $data['booking_id'] = $bookingId;
                    $modelBookingDueDate->insert($data);
                }

//Change To Overdue
                $modelBookingInvoice->cronJobChangeToOverdue();

//Change To Open
                $modelBookingInvoice->cronJobChangeToOpen();


                echo 1;
                exit;
            }
        }
        $this->view->form = $form;

//
// render views
//
        echo $this->view->render('index/set-due-date.phtml');
        exit;
    }

    public function editInvoiceNumberAction() {
//
// check Auth for logged user
//
        CheckAuth::checkPermission(array('editInvoiceNumber'));

//
// get params
//
        $invoiceId = $this->request->getParam('id');
        $invoice_number = $this->request->getParam('invoice_number');

//
// load model
//

        $modelBookingLog = new Model_BookingLog();
        $modelBookingInvoice = new Model_BookingInvoice();
        $invoice = $modelBookingInvoice->getById($invoiceId);
        $modelBooking = new Model_Booking();
        if (!$invoice) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Invoice not found"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        if (!CheckAuth::checkIfCanHandelAllCompany('invoice', $invoiceId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }


        if (!$this->request->isPost()) {
            $invoice_number = $invoice['invoice_num'];
        }
        $form = new Invoices_Form_EditInvoiceNumber(array('invoice_id' => $invoiceId, 'invoice_number' => $invoice_number));

        if ($this->request->isPost()) {
            if ($form->isValid($this->request->getPost())) { // validate form data
                if ((!$modelBooking->checkCanEditBookingDetails($invoice['booking_id'])) && ($invoice['invoice_num'] != $invoice_number )) {
                    $modelBooking->fillBookingAndServicesFieldTemp($invoice['booking_id']);
                    $modelBooking->updateById($invoice['booking_id'], array('is_change' => 1));
                    $dbParams = array(
                        'invoice_num' => $invoice_number,
                        'invoice_num_temp' => $invoice['invoice_num']
                    );
                } else {
                    $dbParams = array(
                        'invoice_num' => $invoice_number
                    );
                }

// add  data log
                $modelBookingLog->addBookingLog($invoice['booking_id']);

                $modelBookingInvoice->updateById($invoiceId, $dbParams);

                echo json_encode(array('invoiceId' => $invoiceId, 'invoice_number' => $invoice_number));
                exit;
            }
        }
        $this->view->form = $form;

//
// render views
//
        echo $this->view->render('index/edit-invoice-number.phtml');
        exit;
    }

    public function checkDuplicateInvoiceAction() {
//
// check Auth for logged user
//
        CheckAuth::checkPermission(array('editInvoiceNumber'));

//
// get params
//
        $invoiceId = $this->request->getParam('id');
        $invoice_number = $this->request->getParam('invoice_number');

//
// load model
//
        $modelBookingInvoice = new Model_BookingInvoice();
        $invoices = $modelBookingInvoice->getDuplicatedInvoiceByInvoiceNumber($invoice_number);
        $is_duplicate = 0;

        if (!empty($invoices)) {
            $is_duplicate = 1;
        }
        echo json_encode(array('is_duplicate' => $is_duplicate, 'invoice_number' => $invoice_number));
        exit;
    }

    public function advanceSearchAction() {

        echo $this->view->render('index/filters.phtml');
        exit;
    }

    /*     * ********* get contrator by  %name% ***************IBM */

    public function getInvoicesByContractorNameAction() {

        $contractor_name = $this->request->getParam('contractor_name', '');
        $modelUser = new Model_User();
        $modelAuthRole = new Model_AuthRole ();
        $contractorRoleId = $modelAuthRole->getRoleIdByName('contractor');
        $results = $modelUser->getContractorNameByNameSearch($contractor_name, $contractorRoleId);
        echo json_encode($results);
        exit;
    }

    public function importBankPaymentsAction() {
        $model_payment = new Model_Payment();
        if ($this->request->isPost()) {
            $upload = new Zend_File_Transfer_Adapter_Http();
            $files = $upload->getFileInfo();
            $loggedUser = CheckAuth::getLoggedUser();
            foreach ($files as $file => $fileInfo) {
                if ($upload->isUploaded($file)) {
                    if ($upload->receive($file)) {
                        $info = $upload->getFileInfo($file);
                        $source = $info[$file]['tmp_name'];
                        $imageInfo = pathinfo($source);
                        $ext = $imageInfo['extension'];
                        $dir = get_config('excel_files') . '/';
                        $subdir = date('Y/m/d/');
                        $fullDir = $dir . $subdir;
                        if (!is_dir($fullDir)) {
                            mkdir($fullDir, 0777, true);
                        }
                        $original_file_path = $info[$file]['name'];
                        $file_saved = copy($source, $fullDir . $original_file_path);
                        if ($file_saved) {
                            $file_number = time();
                            if (file_exists($source)) {
                                unlink($source);
                            }
                        }
                        if ($file_saved) {
                            $exc = PHPExcel_IOFactory::load($fullDir . $original_file_path);
                            $excelWorksheet = $exc->getActiveSheet();
                            $highestRow = $excelWorksheet->getHighestRow();
                            for ($row = 1; $row <= $highestRow; $row++) {
//                                //  Read a row of data into an array
                                $bankPayments[] = array(
                                    'date' => $excelWorksheet->getCell('A' . $row)->getValue(),
                                    'amount' => str_replace(',', '', $excelWorksheet->getCell('B' . $row)->getValue()),
                                    'reference' => $excelWorksheet->getCell('C' . $row)->getValue()
                                );
                            }



                            $model_invoice = new Model_BookingInvoice();
                            $model_booking = new Model_Booking();
                            $model_customer = new Model_Customer();
                            $model_ignoredPayment = new Model_IgnoredPayment();
                            $model_paymentInvoiceMatch = new Model_PaymentInvoiceMatch();
                            $unpaid_invoices = $model_invoice->getAll(array('invoice_type' => 'unpaid'));
                            $model_invoice->fills($unpaid_invoices, array('booking', 'customer'));
                            $notMatchedPayments = array();
//                            $matchedPaymentsArray = array();
                            $matchedPayments = array();
                            $deletedPayments = array();
                            $model_ignoredKeywords = new Model_IgnoredKeywords();
                            $company_id = CheckAuth::getCompanySession();
                            $keywords = $model_ignoredKeywords->getAll(array('company_id' => $company_id));


                            foreach ($bankPayments as $key2 => $unknownPayment) {

                                $keywordMatched = 0;

                                $bankPaymentReference = trim(preg_replace('!\s+!', ' ', $unknownPayment['reference']));
                                $dateOnly = explode(' ', $unknownPayment['date']);
                                $dateObj = DateTime::createFromFormat('d/m/Y', $dateOnly[0]);

                                $date = strtotime($dateObj->format('Y-m-d'));

                                $ignoredPaymentExists = $model_ignoredPayment->getPaymentByReferencAndDateAndAmount($unknownPayment['reference'], $date, $unknownPayment['amount']);
                                if (empty($ignoredPaymentExists)) {
                                    $data = array(
                                        'received_date' => $date,
                                        'bank_charges' => 0,
                                        'amount' => round($unknownPayment['amount'], 2),
                                        'description' => '',
                                        'payment_type_id' => 4,
                                        'user_id' => $loggedUser['user_id'],
                                        'created' => time(),
                                        'reference' => $unknownPayment['reference'],
                                        'amount_withheld' => 0,
                                        'withholding_tax' => 0,
                                        'is_acknowledgment' => 0,
                                        'file_import' => 1,
                                        'file_number' => $file_number
                                    );

                                    foreach ($keywords as $key => $keyword) {
                                        $keyword1 = $keyword['keyword'];
                                        if (preg_match("~\b$keyword1\b~", $bankPaymentReference)) {
                                            $keywordMatched = 1;
                                        }
                                    }
                                    if (!$keywordMatched && $unknownPayment['amount'] > 0) {
                                        $data['booking_id'] = 0;
                                        $data['customer_id'] = 0;
                                        $data['contractor_id'] = 0;
                                        $data['is_matched'] = 0;
// echo $unknownPayment['reference'].'<br/>';
                                        $matchedPayments_id = $model_payment->insert($data);
                                    }
                                }
                            }

                            $filters = array(
                                'file_number' => $file_number,
                            );
                            $unknownPayments = $model_payment->getNotMatchedPayments($filters);


                            foreach ($unknownPayments as $key => $bankPayment) {
                                $is_matched = 0;
                                $global_is_amount = 0;
                                $bankPaymentReference = trim(preg_replace('!\s+!', ' ', $bankPayment['reference']));
                                $fileDuplicatedPayments = $model_payment->getPaymentByReferencAndDateAndAmount($bankPayment['reference'], $bankPayment['received_date'], $bankPayment['amount'], 'all', 'bank', $file_number, 1);
                                $tableDuplicatedPayments = $model_payment->getPaymentByReferencAndDateAndAmount($bankPayment['reference'], $bankPayment['received_date'], $bankPayment['amount'], 'all', 'bank', $file_number);
//                                
                                if (count($tableDuplicatedPayments)) {
                                    if (count($fileDuplicatedPayments) <= count($tableDuplicatedPayments)) {
// delete Unknown Payment
                                        foreach ($fileDuplicatedPayments as $key => $fileDuplicatedPayment) {
                                            $deletedPayments[] = $model_payment->deleteById($fileDuplicatedPayment['payment_id']);
                                        }
                                    } else {
// add duplicated unknown payment after Matching 
                                        $insertTimes = count($fileDuplicatedPayments) - count($tableDuplicatedPayments);
                                        $data = array(
                                            'booking_id' => $tableDuplicatedPayments[0]['booking_id'],
                                            'contractor_id' => $tableDuplicatedPayments[0]['contractor_id'],
                                            'customer_id' => $tableDuplicatedPayments[0]['customer_id'],
                                            'reference' => $bankPayment['reference'],
                                            'amount' => $bankPayment['amount'],
                                            'user_id' => $tableDuplicatedPayments[0]['user_id'],
                                            'received_date' => $bankPayment['received_date'],
                                            'payment_type_id' => 4,
                                            'is_matched' => 1,
                                            'file_import' => 1,
                                            'file_number' => $file_number
                                        );
                                        for ($i = 0; $i < $insertTimes; $i++) {
//echo $bankPayment['reference'] . '----> Unknown Added (have duplicated) <br/>';
                                            $matchedPayments[] = $model_payment->insert($data);
                                        }
                                    }
                                } else {
                                    foreach ($unpaid_invoices as $key => $invoice) {
                                        $is_amount = 0;
                                        $status = 0;
//echo "Status : $status";
                                        $Model_BookingAddress = new Model_BookingAddress();
                                        $model_customerCommercialInfo = new Model_CustomerCommercialInfo();
                                        $booking_address = $Model_BookingAddress->getByBookingId($invoice['booking_id']);
                                        $customerCommericalInfo = $model_customerCommercialInfo->getByCustomerId($invoice['customer']['customer_id']);
                                        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
                                        $score = null;
                                        $score_count = 0;
                                        if (!empty($bankPayment)) {
                                            if ($bankPayment['amount'] <= $invoice['booking']['qoute']) {
                                                $invoiceNum2 = null;
                                                $invoiceNum3 = null;
                                                $invoiceNum1 = null;
                                                $invoiceNum5 = null;
                                                $bookingNum2 = null;
                                                $bookingNum3 = null;
                                                $bookingNum1 = null;
                                                $bookingNum5 = null;
                                                if (!ctype_digit($invoice['invoice_num'])) {
                                                    $invoiceNum5 = $invoice['invoice_num'];
                                                    $invoiceNum1 = str_replace('-', ' ', $invoice['invoice_num']);
                                                    $invoiceNum2 = str_replace('-', '', $invoice['invoice_num']);
                                                    $invoiceNum3 = 'Invoice ' . substr($invoice['invoice_num'], 4);
                                                    $invoiceNum4 = substr($invoice['invoice_num'], 4);
                                                } else {
                                                    $invoiceNum4 = $invoice['invoice_num'];
                                                }

                                                if (!ctype_digit($invoice['booking']['booking_num'])) {
                                                    $bookingNum5 = $invoice['booking']['booking_num'];
                                                    $bookingNum1 = str_replace('-', ' ', $invoice['booking']['booking_num']);
                                                    $bookingNum2 = str_replace('-', '', $invoice['booking']['booking_num']);
                                                    $bookingNum3 = 'Booking ' . substr($invoice['booking']['booking_num'], 4);
                                                    $bookingNum4 = substr($invoice['booking']['booking_num'], 4);
                                                } else {
                                                    $bookingNum4 = $invoice['booking']['booking_num'];
                                                }
//                                                if(my_ip()){
//                                                    echo $invoiceNum3;
//                                                }

                                                if ($bankPayment['amount'] == $invoice['booking']['qoute']) {
//echo$bankPayment['reference'] . ' matched ' . $bankPayment['amount'] . '<br/>';
                                                    $is_amount = 1;
                                                    $score .= ",Amount:" . $bankPayment['amount'];
                                                    $score_count++;
                                                    $status = 1;
                                                }
//if ((stripos($bankPaymentReference, $invoice['invoice_num']) || stripos($bankPaymentReference, $invoiceNum1) || stripos($bankPaymentReference, $invoiceNum2) || stripos($bankPaymentReference, $invoiceNum3) || preg_match("~\b$invoiceNum4\b~", $bankPaymentReference)) && $bankPayment['amount'] <= $invoice['booking']['qoute']) {
                                                if (!ctype_digit($invoice['invoice_num'])) {

                                                    if ((stripos($bankPaymentReference, $invoiceNum5) !== False || stripos($bankPaymentReference, $invoiceNum1) !== False || stripos($bankPaymentReference, $invoiceNum2) !== False || stripos($bankPaymentReference, $invoiceNum3) !== False || preg_match("~\b$invoiceNum4\b~", $bankPaymentReference))) {
//  if (!is_numeric($invoice['invoice_num'])) { 
                                                        $is_amount = 0;
                                                        $score .= ",Invoice Number:" . $invoice['invoice_num'];
                                                        $score_count++;
                                                        $status = 1;
                                                    }
                                                } else {
                                                    if (preg_match("~\b$invoiceNum4\b~", $bankPaymentReference)) {
                                                        $is_amount = 0;
                                                        $score .= ",Invoice Number1:" . $invoiceNum4;
                                                        $score_count++;
                                                        $status = 1;
                                                    }
                                                }

                                                if (!ctype_digit($invoice['booking']['booking_num'])) {
                                                    if ((stripos($bankPaymentReference, $bookingNum5) !== False || stripos($bankPaymentReference, $bookingNum1) !== False || stripos($bankPaymentReference, $bookingNum2) !== False || stripos($bankPaymentReference, $bookingNum3) !== False || preg_match("~\b$bookingNum4\b~", $bankPaymentReference))) {
                                                        $score .= ",Booking Number:" . $invoice['booking']['booking_num'];
                                                        $score_count++;
                                                        $status = 1;
                                                        $is_amount = 0;
                                                    }
                                                } else {
                                                    if (preg_match("~\b$bookingNum4\b~", $bankPaymentReference)) {
                                                        $score .= ",Booking Number:" . $bookingNum4;
                                                        $score_count++;
                                                        $status = 1;
                                                        $is_amount = 0;
                                                    }
                                                }

                                                if (stripos($bankPaymentReference, $booking_address['suburb']) !== False) {
                                                    if (!is_numeric($booking_address['suburb']) && strlen($booking_address['suburb']) > 2) {
//echo$bankPayment['reference'] . ' matched ' . $booking_address['suburb'] . '<br/>';

                                                        $score .= ",suburb:" . $booking_address['suburb'];
                                                        $score_count++;
                                                        $status = 1;
                                                        $is_amount = 0;
                                                    }
                                                }
                                                if (stripos($bankPaymentReference, $invoice['customer']['first_name']) !== False) {
//echo$bankPayment['reference'] . ' matched ' . $invoice['customer']['first_name'] . '<br/>';
                                                    if (strlen($invoice['customer']['first_name']) > 2) {
                                                        $score .= ",Customer First Name:" . $invoice['customer']['first_name'];
                                                        $score_count++;
                                                        $status = 1;
                                                        $is_amount = 0;
                                                    }
                                                }
                                                if (stripos($bankPaymentReference, $invoice['customer']['last_name']) !== False) {
//echo$bankPayment['reference'] . ' matched ' . $invoice['customer']['last_name'] . '<br/>';
                                                    if (strlen($invoice['customer']['last_name']) > 2) {
                                                        $score .= ",Customer Last Name:" . $invoice['customer']['last_name'];
                                                        $score_count++;
                                                        $status = 1;
                                                        $is_amount = 0;
                                                    }
                                                }
                                                if (stripos($bankPaymentReference, $customerCommericalInfo['business_name']) !== False) {
//echo$bankPayment['reference'] . ' matched ' . $customerCommericalInfo['business_name'] . '<br/>';
                                                    if (strlen($customerCommericalInfo['business_name']) > 2) {
                                                        $score .= ",Customer Business Name:" . $customerCommericalInfo['business_name'];
                                                        $score_count++;
                                                        $status = 1;
                                                        $is_amount = 0;
                                                    }
                                                }

                                                $global_is_amount = $is_amount;

//                                            //echo'payment_id ' . $matchedPayments_id . ' & Status ' . $status, '<br/>';
                                                if ($status && $bankPayment['amount'] > 0) {
                                                    $Multiple_data = array(
                                                        'payment_id' => $bankPayment['payment_id'],
                                                        'booking_id' => $invoice['booking']['booking_id'],
                                                        'created' => time(),
                                                        'score' => $score,
                                                        'score_count' => $score_count,
                                                    );
                                                    $model_paymentInvoiceMatch->insert($Multiple_data);
                                                    $is_matched = 1;
                                                }
                                            }
                                        }
                                    }


                                    if ($is_matched) {

                                        $maxScorePayment = $model_paymentInvoiceMatch->getMaxScoreOfPayments($bankPayment['payment_id']);

                                        $bookingData = $model_booking->getById($maxScorePayment['booking_id']);
                                        $contractors = $modelContractorServiceBooking->getContractorsDataByBookingId($maxScorePayment['booking_id']);

                                        if (!($maxScorePayment['score_count'] == 1 && stripos($maxScorePayment['score'], ",Amount:") !== False)) {
                                            if (!empty($contractors)) {
                                                foreach ($contractors as $key => $contractor) {
                                                    if ($key > 0) {
                                                        $paymentMaxed = array(
                                                            'booking_id' => $maxScorePayment['booking_id'],
                                                            'customer_id' => $bookingData['customer_id'],
                                                            'score' => $maxScorePayment['score'],
                                                            'score_count' => $maxScorePayment['score_count'],
                                                            'received_date' => $date,
                                                            'bank_charges' => 0,
                                                            'amount' => round($bankPayment['amount'], 2),
                                                            'description' => '',
                                                            'payment_type_id' => 4,
                                                            'user_id' => $loggedUser['user_id'],
                                                            'created' => time(),
                                                            'reference' => $bankPayment['reference'],
                                                            'amount_withheld' => 0,
                                                            'withholding_tax' => 0,
                                                            'is_acknowledgment' => 0,
                                                            'file_import' => 1,
                                                            'is_matched' => 1,
                                                            'contractor_id' => $contractor['contractor_id'],
                                                        );
                                                        $model_payment->insert($paymentMaxed);
                                                    } else {
//                                                    //echo"<<<<<<< Updated >>>>>>> <br/>";
                                                        $paymentMaxed = array(
                                                            'booking_id' => $maxScorePayment['booking_id'],
                                                            'customer_id' => $bookingData['customer_id'],
                                                            'score' => $maxScorePayment['score'],
                                                            'score_count' => $maxScorePayment['score_count'],
                                                            'contractor_id' => $contractor['contractor_id'],
                                                            'is_matched' => 1
                                                        );
                                                        $matchedPayments[] = $bankPayment['payment_id'];
//     var_dump($paymentMaxed);
//                                                    //echo'<br/>'.$bankPayment['payment_id'].'<br/>';
                                                        $model_payment->updateById($bankPayment['payment_id'], $paymentMaxed);
                                                    }
                                                }
                                                $model_paymentInvoiceMatch->deleteById($maxScorePayment['payment_invoice_match_id']);
                                            }
                                        }
                                    } else {
                                        $notMatchedPayments[] = $bankPayment['payment_id'];
                                    }
                                }
                            }
                        }
                    }
                }

                $matchedAndNotMatched = count($unknownPayments) - count($deletedPayments);
                $notMatched = $matchedAndNotMatched - count($matchedPayments);
                $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => 'File upload <a style="color:#3c763d" href="' . $this->router->assemble(array(), 'paymentList') . '?fltr[is_approved]=no&fltr[file_number]=' . $file_number . '&fltr[file_import]=1"><b>' . count($matchedPayments) . " matched bank payments</b></a> and <a style='color:#3c763d' href='" . $this->router->assemble(array(), 'paymentList') . "?fltr[is_approved]=no&fltr[file_import]=1&fltr[file_number]='.$file_number.'&fltr[is_matched]=0'><b>" . $notMatched . " Not Matched Payments</b></a>"));
                echo json_encode(array('status' => 1, 'file_number' => $file_number));
                exit;
            }
        }

        echo $this->view->render('index/import-bank-payments.phtml');
        exit;
    }

    public function importCreditCardPaymentsAction() {

        $model_payment = new Model_Payment();
        if ($this->request->isPost()) {
            $upload = new Zend_File_Transfer_Adapter_Http();
            $files = $upload->getFileInfo();
            $loggedUser = CheckAuth::getLoggedUser();

            foreach ($files as $file => $fileInfo) {
                if ($upload->isUploaded($file)) {
                    if ($upload->receive($file)) {
                        $info = $upload->getFileInfo($file);
                        $source = $info[$file]['tmp_name'];
                        $imageInfo = pathinfo($source);
                        $ext = $imageInfo['extension'];
                        $dir = get_config('excel_files') . '/';
                        $subdir = date('Y/m/d/');
                        $fullDir = $dir . $subdir;
                        if (!is_dir($fullDir)) {
                            mkdir($fullDir, 0777, true);
                        }
                        $original_file_path = $info[$file]['name'];

                        $file_saved = copy($source, $fullDir . $original_file_path);

                        if ($file_saved) {

                            if (file_exists($source)) {
                                unlink($source);
                            }
                        }

                        if ($file_saved) {
                            $fileNumber = time();
                            $exc = PHPExcel_IOFactory::load($fullDir . $original_file_path);
                            $excelWorksheet = $exc->getActiveSheet();
                            $highestRow = $excelWorksheet->getHighestRow();
                            for ($row = 1; $row <= $highestRow; $row++) {
//                                //  Read a row of data into an array
                                $creditCardPayments[] = array(
                                    'date' => $excelWorksheet->getCell('A' . $row)->getValue(),
                                    'transaction_id' => $excelWorksheet->getCell('B' . $row)->getValue(),
                                    'card_holder_name' => $excelWorksheet->getCell('D' . $row)->getValue(),
                                    'email' => $excelWorksheet->getCell('K' . $row)->getValue(),
                                    'invoice_reference' => $excelWorksheet->getCell('M' . $row)->getValue(),
                                    'amount' => $excelWorksheet->getCell('J' . $row)->getValue(),
                                    'result' => $excelWorksheet->getCell('G' . $row)->getValue(),
                                );
                            }

                            unset($creditCardPayments[0]);

                            $model_invoice = new Model_BookingInvoice();

                            $notMatchedPayments = array();
                            $model_customerContact = new Model_CustomerContact();
                            $model_paymentInvoiceMatch = new Model_PaymentInvoiceMatch();
                            $model_booking = new Model_Booking();
                            $model_ignoredPayment = new Model_IgnoredPayment();

                            $modelContractorServiceBooking = new Model_ContractorServiceBooking();
                            $matchedPayments = array();
                            $matchedNotApprovedPayments = array();
                            $unknownPayments = array();
//                            $model_ignoredKeywords = new Model_IgnoredKeywords();
//                            $company_id = CheckAuth::getCompanySession();
//                            $keywords = $model_ignoredKeywords->getAll(array('company_id' => $company_id));
                            $unpaid_invoices = $model_invoice->getAll(array('invoice_type' => 'unpaid'));
                            $model_invoice->fills($unpaid_invoices, array('booking', 'customer'));
                            $approvedPayments = array();
                            $fileApproved = array();
                            $allApprove = 0;

                            $matchedInvoiceArrayKey = 0;
                            $approve = 0;
                            $approveAndNotFully = 0;

                            foreach ($creditCardPayments as $key2 => $creditPayment) {
                                if ($creditPayment['result'] == 'Successful') {
                                    $creditTransactionId = $creditPayment['transaction_id'];
                                    $dateOnly = explode(' ', $creditPayment['date']);
                                    if (DateTime::createFromFormat('d/m/y', $dateOnly[0]) != false) {
                                        $dateObj = DateTime::createFromFormat('d/m/y', $dateOnly[0]);
                                    } else {
                                        $dateObj = DateTime::createFromFormat('d/m/Y', $dateOnly[0]);
                                    }
//                                $dateObj->setTimezone(new DateTimeZone('Australia/Sydney'));
// var_dump($dateObj);
                                    $date = strtotime($dateObj->format('Y-m-d'));
                                    $creditAmount = round(str_replace(',', '', $creditPayment['amount']), 2);
                                    $paymentExists = $model_payment->getPaymentByReferencAndDateAndAmount($creditTransactionId, $date, $creditAmount);
                                    $ignoredPaymentExists = $model_ignoredPayment->getPaymentByReferencAndDateAndAmount($creditTransactionId, $date, $creditAmount);

                                    if (empty($paymentExists) && $creditAmount > 0 && empty($ignoredPaymentExists)) {
                                        $unknownData = array(
                                            'received_date' => $date,
                                            'booking_id' => 0,
                                            'contractor_id' => 0,
                                            'customer_id' => 0,
                                            'amount' => $creditAmount,
                                            'description' => '',
                                            'payment_type_id' => 3,
                                            'user_id' => $loggedUser['user_id'],
                                            'created' => time(),
                                            'reference' => $creditTransactionId,
                                            'file_import' => 1,
                                            'is_test' => 10,
                                            'file_number' => $fileNumber,
                                            'card_holder_name' => $creditPayment['card_holder_name'],
                                            'card_email' => $creditPayment['email'],
                                            'card_invoice_reference' => $creditPayment['invoice_reference']
                                        );
                                        $unknownPayments[] = $model_payment->insert($unknownData);
                                    }
                                }
                            }

                            $filters = array(
                                'file_number' => $fileNumber,
                            );
                            $filePayments = $model_payment->getNotMatchedPayments($filters);
                            foreach ($filePayments as $key => $creditCardPayment) {
                                $is_matched = 0;
                                $is_amount = 0;
                                foreach ($unpaid_invoices as $key6 => $invoice) {

                                    $status2 = 0;
                                    $score = null;
                                    $score_count = 0;

                                    $creditCardReference = preg_replace('!\s+!', ' ', $creditCardPayment['card_invoice_reference']);
                                    $secondCustomerContact = $model_customerContact->getByCustomerId($invoice['customer']['customer_id']);
                                    $creditAmount = round(str_replace(',', '', $creditCardPayment['amount']), 2);
                                    if (!empty($creditCardPayment['card_invoice_reference'])) {
                                        if ($creditAmount <= $invoice['booking']['qoute']) {
                                            $invoiceNum2 = null;
                                            $invoiceNum3 = null;
                                            $invoiceNum1 = null;
                                            $invoiceNum5 = null;
                                            $bookingNum2 = null;
                                            $bookingNum3 = null;
                                            $bookingNum1 = null;
                                            $bookingNum5 = null;

                                            if (!ctype_digit(trim($invoice['invoice_num']))) {

                                                $invoiceNum5 = $invoice['invoice_num'];
                                                $invoiceNum1 = str_replace('-', ' ', $invoice['invoice_num']);
                                                $invoiceNum2 = str_replace('-', '', $invoice['invoice_num']);
                                                $invoiceNum3 = 'Invoice ' . substr($invoice['invoice_num'], 4);
                                                $invoiceNum4 = substr($invoice['invoice_num'], 4);
                                            } else {
                                                $invoiceNum4 = $invoice['invoice_num'];
                                            }

                                            if (!ctype_digit($invoice['booking']['booking_num'])) {
                                                $bookingNum5 = $invoice['booking']['booking_num'];
                                                $bookingNum1 = str_replace('-', ' ', $invoice['booking']['booking_num']);
                                                $bookingNum2 = str_replace('-', '', $invoice['booking']['booking_num']);
                                                $bookingNum3 = 'Booking ' . substr($invoice['booking']['booking_num'], 4);
                                                $bookingNum4 = substr($invoice['booking']['booking_num'], 4);
                                            } else {
                                                $bookingNum4 = $invoice['booking']['booking_num'];
                                            }

                                            if ($creditAmount == $invoice['booking']['qoute']) {
//echo$bankPayment['reference'] . ' matched ' . $bankPayment['amount'] . '<br/>';
                                                $is_amount = 1;
                                                $score .= ",Amount:" . $creditAmount;
                                                $score_count++;
                                                $status2 = 1;
                                            }
                                            if (!ctype_digit($invoice['invoice_num'])) {

                                                if ((stripos($creditCardReference, $invoiceNum5) !== False || stripos($creditCardReference, $invoiceNum1) !== False || stripos($creditCardReference, $invoiceNum2) !== False || stripos($creditCardReference, $invoiceNum3) !== False || preg_match("~\b$invoiceNum4\b~", $creditCardReference))) {
                                                    $is_amount = 0;
                                                    $score .= ",Invoice Number:" . $invoice['invoice_num'];
                                                    $score_count++;
                                                    $status2 = 1;
                                                }
                                            } else {
                                                if (preg_match("~\b$invoiceNum4\b~", $creditCardReference)) {
                                                    $is_amount = 0;
                                                    $score .= ",Invoice Number:" . $invoiceNum4;
                                                    $score_count++;
                                                    $status2 = 1;
                                                }
                                            }

                                            if (!ctype_digit($invoice['booking']['booking_num'])) {
                                                if ((stripos($creditCardReference, $bookingNum5) !== False || stripos($creditCardReference, $bookingNum1) !== False || stripos($creditCardReference, $bookingNum2) !== False || stripos($creditCardReference, $bookingNum3) !== False || preg_match("~\b$bookingNum4\b~", $creditCardReference))) {
                                                    $score .= ",Booking Number:" . $invoice['booking']['booking_num'];
                                                    $score_count++;
                                                    $status2 = 1;
                                                    $is_amount = 0;
                                                }
                                            } else {
                                                if (preg_match("~\b$bookingNum4\b~", $creditCardReference)) {
                                                    $score .= ",Booking Number:" . $bookingNum4;
                                                    $score_count++;
                                                    $status2 = 1;
                                                    $is_amount = 0;
                                                }
                                            }
                                            if (stripos($creditCardPayment['card_holder_name'], $invoice['customer']['first_name']) !== False) {
                                                $score .= ",Customer First Name:" . $invoice['customer']['first_name'];
                                                $score_count++;
                                                $status2 = 1;
                                                $is_amount = 0;
                                            }
                                            if (stripos($creditCardPayment['card_holder_name'], $invoice['customer']['first_name2']) !== False) {
                                                $score .= ",Customer First Name2:" . $invoice['customer']['first_name2'];
                                                $score_count++;
                                                $status2 = 1;
                                                $is_amount = 0;
                                            }
                                            if (stripos($creditCardPayment['card_holder_name'], $invoice['customer']['first_name3']) !== False) {
                                                $score .= ",Customer First Name3:" . $invoice['customer']['first_name3'];
                                                $score_count++;
                                                $status2 = 1;
                                                $is_amount = 0;
                                            }

                                            if (stripos($creditCardPayment['card_holder_name'], $invoice['customer']['last_name']) !== False) {
                                                $score .= ",Customer Last Name:" . $invoice['customer']['last_name'];
                                                $score_count++;
                                                $status2 = 1;
                                                $is_amount = 0;
                                            }
                                            if (stripos($creditCardPayment['card_holder_name'], $invoice['customer']['last_name2']) !== False) {
                                                $score .= ",Customer Last Name 2:" . $invoice['customer']['last_name2'];
                                                $score_count++;
                                                $status2 = 1;
                                                $is_amount = 0;
                                            }
                                            if (stripos($creditCardPayment['card_holder_name'], $invoice['customer']['last_name3']) !== False) {
                                                $score .= ",Customer Last Name 3:" . $invoice['customer']['last_name3'];
                                                $score_count++;
                                                $status2 = 1;
                                                $is_amount = 0;
                                            }
                                            if (!empty($creditCardPayment['card_email'])) {

                                                $customerEmail1 = $invoice['customer']['email1'];
                                                if (!filter_var($customerEmail1, FILTER_VALIDATE_EMAIL) === false) {
                                                    if (stripos(trim($creditCardPayment['card_email']), $customerEmail1) !== False) {
                                                        $score .= ",Email 1:" . $invoice['customer']['email1'];
                                                        $score_count++;
                                                        $status2 = 1;
                                                        $is_amount = 0;
                                                    }
                                                }
                                                $customerEmail2 = $invoice['customer']['email2'];
                                                if (!filter_var($customerEmail2, FILTER_VALIDATE_EMAIL) === false) {
                                                    if (stripos(trim($creditCardPayment['card_email']), $customerEmail2) !== False) {
                                                        $score .= ",Email 2:" . $invoice['customer']['email2'];
                                                        $score_count++;
                                                        $status2 = 1;
                                                        $is_amount = 0;
                                                    }
                                                }

                                                $customerEmail3 = $invoice['customer']['email3'];
                                                if (!filter_var($customerEmail3, FILTER_VALIDATE_EMAIL) === false) {
                                                    if (stripos(trim($creditCardPayment['card_email']), $customerEmail3) !== False) {
                                                        $score .= ",Email 3:" . $invoice['customer']['email3'];
                                                        $score_count++;
                                                        $status2 = 1;
                                                        $is_amount = 0;
                                                    }
                                                }
                                            }
                                            if (!empty($secondCustomerContact)) {
                                                foreach ($secondCustomerContact as $key => $secondContact) {
                                                    if (!empty($creditCardPayment['card_email'])) {
                                                        $customerSecondEmail1 = $secondContact['email1'];
                                                        if (!filter_var($customerSecondEmail1, FILTER_VALIDATE_EMAIL) === false) {

                                                            if (stripos(trim($creditCardPayment['card_email']), $customerSecondEmail1) !== False) {
                                                                $score .= ",Secondary Email 1:" . $secondContact['email1'];
                                                                $score_count++;
                                                                $status2 = 1;
                                                                $is_amount = 0;
                                                            }
                                                        }
                                                        $customerSecondEmail2 = $secondContact['email2'];
                                                        if (!filter_var($customerSecondEmail2, FILTER_VALIDATE_EMAIL) === false) {

                                                            if (stripos(trim($creditCardPayment['card_email']), $customerSecondEmail2) !== False) {
                                                                $score .= ",Secondary Email 2:" . $secondContact['email2'];
                                                                $score_count++;
                                                                $status2 = 1;
                                                                $is_amount = 0;
                                                            }
                                                        }
                                                        $customerSecondEmail3 = $secondContact['email3'];
                                                        if (!filter_var($customerSecondEmail3, FILTER_VALIDATE_EMAIL) === false) {
                                                            if (stripos(trim($creditCardPayment['card_email']), $customerSecondEmail3) !== False) {
                                                                $score .= ",Secondary Email 3:" . $secondContact['email3'];
                                                                $score_count++;
                                                                $status2 = 1;
                                                                $is_amount = 0;
                                                            }
                                                        }
                                                    }
                                                    if (stripos($creditCardPayment['card_holder_name'], $secondContact['last_name']) !== False) {
                                                        $score .= ",Customer Secondary Last Name:" . $secondContact['last_name'];
                                                        $score_count++;
                                                        $status2 = 1;
                                                        $is_amount = 0;
                                                    }
                                                    if (stripos($creditCardPayment['card_holder_name'], $secondContact['first_name']) !== False) {
                                                        $score .= ",Customer Secondary First Name:" . $secondContact['first_name'];
                                                        $score_count++;
                                                        $status2 = 1;
                                                        $is_amount = 0;
                                                    }
                                                }
                                            }



                                            if ($status2 && $creditAmount > 0) {
                                                $paidAmount = $model_payment->getTotalAmount(array('booking_id' => $invoice['booking']['booking_id'], 'is_approved' => 'yes'));
                                                $newPaidAmount = $paidAmount + $creditAmount;
                                                if ($newPaidAmount <= $invoice['booking']['qoute']) {
                                                    $Multiple_data = array(
                                                        'payment_id' => $creditCardPayment['payment_id'],
                                                        'booking_id' => $invoice['booking']['booking_id'],
                                                        'created' => time(),
                                                        'score' => $score,
                                                        'score_count' => $score_count,
                                                    );

                                                    $model_paymentInvoiceMatch->insert($Multiple_data);
                                                    if ($creditAmount == $invoice['booking']['qoute']) {
// approve
                                                        $approve = 1;
                                                    } else if ($creditAmount != $invoice['booking']['qoute']) {
// approve Paid amount and flag as problem not fully paid
                                                        $approveAndNotFully = 1;
                                                    }
                                                    $is_matched = 1;
                                                    $matchedInvoiceArrayKey = $key6;
                                                }
                                            }
                                        }
                                    }
                                }

                                if ($is_matched) {
                                    $maxScorePayment = $model_paymentInvoiceMatch->getMaxScoreOfPayments($creditCardPayment['payment_id']);
                                    $bookingData = $model_booking->getById($maxScorePayment['booking_id']);
                                    $contractors = $modelContractorServiceBooking->getContractorsDataByBookingId($maxScorePayment['booking_id']);
                                    if (!($maxScorePayment['score_count'] == 1 && stripos($maxScorePayment['score'], ",Amount:") !== False)) {
                                        if (!empty($contractors)) {
                                            foreach ($contractors as $key => $contractor) {
                                                if ($key > 0) {
                                                    $paymentMaxed = array(
                                                        'booking_id' => $maxScorePayment['booking_id'],
                                                        'customer_id' => $bookingData['customer_id'],
                                                        'score' => $maxScorePayment['score'],
                                                        'received_date' => $date,
                                                        'bank_charges' => 0,
                                                        'amount' => $creditAmount,
                                                        'description' => '',
                                                        'payment_type_id' => 3,
                                                        'user_id' => $loggedUser['user_id'],
                                                        'created' => time(),
                                                        'reference' => $creditCardReference,
                                                        'amount_withheld' => 0,
                                                        'withholding_tax' => 0,
                                                        'is_acknowledgment' => 0,
                                                        'file_import' => 1,
                                                        'is_matched' => 1,
                                                        'file_number' => $fileNumber,
                                                        'contractor_id' => $contractor['contractor_id'],
                                                        'is_test' => 20
                                                    );
                                                    $model_payment->insert($paymentMaxed);
                                                } else {
                                                    $paymentMaxed = array(
                                                        'booking_id' => $maxScorePayment['booking_id'],
                                                        'customer_id' => $bookingData['customer_id'],
                                                        'score' => $maxScorePayment['score'],
                                                        'contractor_id' => $contractor['contractor_id'],
                                                        'is_matched' => 1
                                                    );

                                                    if ($approve) {
//echo "Approve <br/> - ".$paymentMaxed['booking_id'];
                                                        $paymentMaxed['is_approved'] = 1;
                                                        $paymentMaxed['is_test'] = 30;
                                                        $approvedPayments[] = $creditCardPayment['payment_id'];
                                                    } else if ($approveAndNotFully) {
//  echo "ApproveAndNotFully <br/> - ".$paymentMaxed['booking_id'];
                                                        $paymentMaxed['is_test'] = 40;
                                                        $paymentMaxed['is_approved'] = 1;
                                                        $paymentMaxed['is_diff_amount'] = 1;
                                                        $approvedPayments[] = $creditCardPayment['payment_id'];
                                                    }

                                                    $matchedPayments[] = $creditCardPayment['payment_id']; //                                                            
                                                    $model_payment->updateById($creditCardPayment['payment_id'], $paymentMaxed);

                                                    if ($approve || $approveAndNotFully) {
                                                        $paidAmount = $model_payment->getTotalAmount(array('booking_id' => $bookingData['booking_id'], 'is_approved' => 'yes'));

                                                        if ($paidAmount < 0) {
                                                            $paidAmount = 0;
                                                        }

                                                        $model_booking->updateById($bookingData['booking_id'], array('paid_amount' => round($paidAmount, 2)));

                                                        if ($paidAmount >= $bookingData['qoute']) {
                                                            unset($unpaid_invoices[$matchedInvoiceArrayKey]);
// Remove All Different Amount for payment that base on invoice that closed
                                                            $allBookingPayments = $model_payment->getByBookingId($bookingData['booking_id']);
                                                            foreach ($allBookingPayments as $key => $bookingPayments) {
                                                                $model_payment->updateById($bookingPayments['payment_id'], array('is_diff_amount' => 0));
                                                            }
                                                            $model_invoice->updateByBookingId($bookingData['booking_id'], array('invoice_type' => 'closed'));
                                                        } else {
                                                            $model_invoice->updateByBookingId($bookingData['booking_id'], array('invoice_type' => 'open'));
                                                        }
                                                    }
                                                }
                                            }
                                            $model_paymentInvoiceMatch->deleteById($maxScorePayment['payment_invoice_match_id']);
                                        }
                                    }
                                }
                            }
//                            exit;
// approved unapprved payments by file

                            $unapprovedPayments = $model_payment->getAll(array('is_approved' => 'no'));
                            foreach ($creditCardPayments as $key => $filePayment) {
                                if ($filePayment['result'] == 'Successful') {

                                    $creditTransactionId = (int) $filePayment['transaction_id'];
                                    $dateOnly = explode(' ', $filePayment['date']);

                                    if (DateTime::createFromFormat('d/m/y', $dateOnly[0]) != false) {
                                        $dateObj = DateTime::createFromFormat('d/m/y', $dateOnly[0]);
                                    } else {
                                        $dateObj = DateTime::createFromFormat('d/m/Y', $dateOnly[0]);
                                    }
                                    $date = strtotime($dateObj->format('Y-m-d'));
                                    $creditAmount = round(str_replace(',', '', $filePayment['amount']), 2);


                                    foreach ($unapprovedPayments as $key => $unapprovedPayment) {

                                        $paymentReference = (int) trim($unapprovedPayment['reference']);
                                        if ($paymentReference) {

                                            if ((($paymentReference == $creditTransactionId) || preg_match("~\b$creditTransactionId\b~", $paymentReference)) && $unapprovedPayment['amount'] == $creditAmount) {

                                                $bookingData = $model_booking->getById($unapprovedPayment['booking_id']);
                                                $paidAmount = $model_payment->getTotalAmount(array('booking_id' => $unapprovedPayment['booking_id'], 'is_approved' => 'yes'));
                                                $newPaidAmount = $paidAmount + $creditAmount;

                                                if ($newPaidAmount <= $bookingData['qoute']) {
                                                    $paymentExists = $model_payment->getPaymentByReferencAndDateAndAmount($creditTransactionId, $date, $creditAmount, 'yes');
                                                    $duplicatedUnapprovedPayments = $model_payment->getPaymentByReferencAndDateAndAmount($creditTransactionId, $date, $creditAmount, 'no');
                                                    foreach ($duplicatedUnapprovedPayments as $key => $duplicatedUnapprovedPayment) {
                                                        $model_payment->updateById($duplicatedUnapprovedPayment['payment_id'], array('is_deleted' => 1, 'file_number' => $fileNumber, 'is_test' => 60));
                                                    }
                                                    if (empty($paymentExists)) {
//$refenece = $filePayment['transaction_id'];
                                                        $approvedData = array(
                                                            'received_date' => $date,
                                                            'bank_charges' => 0,
                                                            'booking_id' => $unapprovedPayment['booking_id'],
                                                            'contractor_id' => $unapprovedPayment['contractor_id'],
                                                            'customer_id' => $unapprovedPayment['customer_id'],
                                                            'amount' => $creditAmount,
                                                            'description' => '',
                                                            'payment_type_id' => 3,
                                                            'user_id' => $loggedUser['user_id'],
                                                            'created' => time(),
                                                            'reference' => $creditTransactionId,
                                                            'amount_withheld' => 0,
                                                            'withholding_tax' => 0,
                                                            'is_acknowledgment' => 0,
                                                            'file_import' => 1,
                                                            'is_approved' => 1,
                                                            'is_test' => 50,
                                                            'file_number' => $fileNumber
                                                        );
                                                        $fileApproved[] = $model_payment->insert($approvedData);

                                                        $bookingData = $model_booking->getById($approvedData['booking_id']);
                                                        $paidAmount = $model_payment->getTotalAmount(array('booking_id' => $approvedData['booking_id'], 'is_approved' => 'yes'));
                                                        if ($paidAmount < 0) {
                                                            $paidAmount = 0;
                                                        }

                                                        $model_booking->updateById($approvedData['booking_id'], array('paid_amount' => round($paidAmount, 2)));

                                                        if ($paidAmount >= $bookingData['qoute']) {
// Remove All Different Amount for payment that base on invoice that closed

                                                            $allBookingPayments = $model_payment->getByBookingId($bookingData['booking_id']);
                                                            foreach ($allBookingPayments as $key => $bookingPayments) {
                                                                $model_payment->updateById($bookingPayments['payment_id'], array('is_diff_amount' => 0));
                                                            }
                                                            $model_invoice->updateByBookingId($approvedData['booking_id'], array('invoice_type' => 'closed'));
                                                        } else {
                                                            $model_invoice->updateByBookingId($approvedData['booking_id'], array('invoice_type' => 'open'));
                                                        }
                                                    }
                                                }
                                            } else if ($paymentReference == $creditTransactionId) {
                                                $paymentExists = $model_payment->getPaymentByReferencAndDateAndAmount($creditTransactionId, $date, $creditAmount, 'yes');
                                                $duplicatedUnapprovedPayments = $model_payment->getPaymentByReferencAndDateAndAmount($creditTransactionId, $date, $creditAmount, 'no');
                                                foreach ($duplicatedUnapprovedPayments as $key => $duplicatedUnapprovedPayment) {
                                                    if (!$duplicatedUnapprovedPayment['booking_id']) {
                                                        $model_payment->updateById($duplicatedUnapprovedPayment['payment_id'], array('is_deleted' => 1, 'file_number' => $fileNumber, 'is_test' => 151));
                                                        unset($duplicatedUnapprovedPayments[$key]);
                                                    }
                                                }
                                               // var_dump($duplicatedUnapprovedPayments);
                                                if (empty($paymentExists) && empty($duplicatedUnapprovedPayments)) {
                                                  //  echo "salim";
                                                    $score = ",Transaction Num : " . $creditTransactionId;
                                                    $unapprovedData = array(
                                                        'received_date' => $date,
                                                        'booking_id' => $unapprovedPayment['booking_id'],
                                                        'contractor_id' => $unapprovedPayment['contractor_id'],
                                                        'customer_id' => $unapprovedPayment['customer_id'],
                                                        'amount' => $creditAmount,
                                                        'payment_type_id' => 3,
                                                        'user_id' => $loggedUser['user_id'],
                                                        'created' => time(),
                                                        'reference' => $creditTransactionId,
                                                        'file_import' => 1,
                                                        'is_approved' => 0,
                                                        'is_test' => 150,
                                                        'file_number' => $fileNumber,
                                                        'score' => $score,
                                                        'score_count' => 1
                                                    );
                                                    $matchedNotApprovedPayments[] = $model_payment->insert($unapprovedData);
                                                 //   var_dump($matchedPayments);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                           
                            if (count($unknownPayments) > 0) {
                                $notMatchedCount = count($unknownPayments) - (count($matchedPayments) + count($matchedNotApprovedPayments));
                            } else {
                                $notMatchedCount = 0;
                            }
                            $allApprove = count($approvedPayments) + count($fileApproved);
                            $message = 'File upload';
                            $message .= " <a style='color:#3c763d' href='" . $this->router->assemble(array(), 'paymentList') . "?fltr[is_approved]=no&fltr[file_import]=1&fltr[is_matched]=0&fltr[file_number]='.$fileNumber.''><b>" . $notMatchedCount . " unknown payments</b></a>";

                            if (count($matchedNotApprovedPayments)) {
                                $message .= ' and <a style="color:#3c763d" href="' . $this->router->assemble(array(), 'paymentList') . '?fltr[is_approved]=no&fltr[file_import]=1&fltr[file_number]=' . $fileNumber . '"><b>' . count($matchedNotApprovedPayments) . ' matched payments</b></a>';
                            }
                            if ($allApprove) {
                                $message .= " and <a style='color:#3c763d' href='" . $this->router->assemble(array(), "paymentList") . "?fltr[is_approved]=yes&fltr[file_number]='.$fileNumber.'><b>" . $allApprove . " payment approved</b></a>";
                            }
                            
                            $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => $message));
                            echo json_encode(array('status' => 1, 'fileNumber' => $fileNumber));
                            exit;
                        }
                    }
                }
            }
        }

        echo $this->view->render('payment/import-credit-card-payments.phtml');
        exit;
    }

    public function withholdReleasePaymentAction() {


        $invoiceId = $this->request->getParam('id');
        $process = $this->request->getParam('process', 'withhold');
        $asJson = $this->request->getParam('asJson');
        if (!CheckAuth::checkIfCanHandelAllCompany('invoice', $invoiceId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

// load models
        $modelBookingInvoice = new Model_BookingInvoice();

        if ($process == 'withhold') {
            $modelBookingInvoice->updateById($invoiceId, array('withhold_payment' => 1));
        } else if ($process == 'release') {
            $modelBookingInvoice->updateById($invoiceId, array('withhold_payment' => 0));
        }

        if (isset($asJson) && $asJson) {
            echo 1;
            exit;
        }

        $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
    }

    public function ignoredKeywordsAction() {
        $keyword = $this->request->getParam('keyword');
        $company_id = CheckAuth::getCompanySession();
        $model_ignoredKeywords = new Model_IgnoredKeywords();
        $keywords = $model_ignoredKeywords->getAll(array('company_id' => $company_id));
        $this->view->keywords = $keywords;
        if ($this->request->isPost()) {
            $keyword_id = $this->request->getParam('keyword_id', 0);
            if (!$keyword_id) {
                $data = array(
                    'keyword' => $keyword,
                    'company_id' => $company_id
                );

                $success = $model_ignoredKeywords->insert($data);
                if ($success) {
                    $data['id'] = $success;
                    echo json_encode(array('status' => 1, 'data' => $data));
                    exit;
                }
            } else {
                $model_ignoredKeywords->deleteById($keyword_id);
                echo 1;
                exit;
            }
        }
        echo $this->view->render('index/ignored-keywords.phtml');
        exit;
    }

    /*     * ************END************ */

    public function removeAssignAction() {
        $model_payment = new Model_Payment();
        if ($this->request->isPost()) {
            $payment_id = $this->request->getParam('payment_id');
            $selectedPayment = $model_payment->getById($payment_id);
            if ($selectedPayment['is_approved'] === 0) {
                if ($payment_id) {
                    $data = array(
                        'booking_id' => 0,
                        'customer_id' => 0,
                        'contractor_id' => 0,
                        'is_matched' => 0
                    );
                    $model_payment->updateById($payment_id, $data);
                }
            }
            echo 1;
            exit;
        }
    }

    public function sendInvoiceAsSmsAction() {

        CheckAuth::checkPermission(array('sendSmsTwilio'));
        $invoiceId = $this->request->getParam('id', 0);
        $loggedUser = CheckAuth::getLoggedUser();
        $user_log = $loggedUser['user_id'];
        //echo"invoice id".$invoiceId;
        /* if (!CheckAuth::checkIfCanHandelAllCompany('invoice', $invoiceId)) {
          $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
          $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
          }
          if (!$invoice) {
          $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Invoice not found"));
          $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
          }
          if (!$modelBookingInvoice->checkIfCanSeeHisOrAssignedInvoice($invoiceId)) {
          $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
          $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
          }
         */
        /*         * ************************************ */
        //CheckAuth::checkPermission(array('sendSmsTwilio'));
        $modelBooking = new Model_Booking();
        $modelCustomer = new Model_Customer();
        $modelSmsTemplate = new Model_SmsTemplate();
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelTradingName = new Model_TradingName();
        $invoiceInfo = $modelBookingInvoice->getById($invoiceId);
        $invoice_num = $invoiceInfo['invoice_num'];
        $booking_id = $invoiceInfo['booking_id'];
        $bookingInfo = $modelBooking->getById($booking_id);
        $trading_name_id = $bookingInfo['trading_name_id'];
        $mount = $modelBooking->getTotalAmountBookingDetails($booking_id);
        $unpaid_amount = $mount['balance_due'];
        $customerId = $bookingInfo['customer_id'];
        $fortwilioMobileFormat = 1;
        $customerInfo = $modelCustomer->getById($customerId, $fortwilioMobileFormat);
        $customerFirstName = $customerInfo['first_name'];
        $trading_info = $modelTradingName->getById($trading_name_id);
        $trading_name = $trading_info['trading_name'];
        $phone = $trading_info['phone'];
        $to_customer = array();
        array_push($to_customer, $customerInfo['mobile1'], $customerInfo['mobile2'], $customerInfo['mobile3']);
        $customer_first_names = explode(" ", $customerFirstName);

        $template_params = array(
            '{customerFirstName}' => $customer_first_names[0],
            '{invoice_num}' => $invoice_num,
            '{unpaid_amount}' => $unpaid_amount,
            '{trading_name}' => $trading_name,
            '{phone}' => $phone
        );
        $templateInfo = $modelSmsTemplate->getByName('send message to customer for invoice');
        $template_id = $templateInfo['id'];
        $smsTemplate = $modelSmsTemplate->getsmsTemplate($template_id, $template_params);
        $message = $smsTemplate['message'];
        if ($this->request->isPost()) {
            $mobile_no = $this->request->getParam('mobile_no');
            $messages = $this->request->getParam('message');
            $modelSmsHistory = new Model_SmsHistorty();
            $mobile = explode(",", $mobile_no);
            for ($i = 0; $i < count($mobile); $i++) {
                $fromNumber = "+61447075733";
                if ($mobile[$i] != '')
                    $toNumber = "$mobile[$i]";
                else
                    continue;
                $sms_id = $modelSmsHistory->sendSmsTwilio($fromNumber, $toNumber, $messages);
                if ($sms_id) {
                    $params = array(
                        'reference_id' => $template_id,
                        'from' => '+61447075733',
                        'to' => $toNumber,
                        'message_sid' => $sms_id,
                        'message' => $messages,
                        'receiver_id' => $customerId,
                        'status' => '',
                        'sms_type' => 'sent',
                        'sms_reason' => 'invoice',
                        'reason_id' => $invoiceId,
                        'template_type' => 'standard',
                        'created_by' => $user_log,
                    );
                    $modelSmsHistory->insert($params);
                }
            }
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        } else {
            $this->view->to_customer = $to_customer;
            $this->view->message = $message;
            $this->view->id = $invoiceId;
            $form = new Invoices_Form_sendSms();
            $this->view->form = $form;
            echo $this->view->render('index/send-invoice-as-sms.phtml');
            exit;
        }
    }

}
