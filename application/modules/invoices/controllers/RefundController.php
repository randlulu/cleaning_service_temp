<?php

class Invoices_RefundController extends Zend_Controller_Action {

    private $request;
    private $router;
    private $loggedUser;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'invoices';
        $this->view->sub_menu = 'refund';
        
        $pageName = $this->request->getParam('page_name', '');
        $this->view->page_title = (!empty($pageName))?$pageName." - Invoices":"Invoices";

        //
        // check Auth for logged user
        //
        $this->loggedUser = CheckAuth::getLoggedUser();
    }

    /**
     * Items list action
     */
    public function indexAction() {
        
		
        $this->view->page_title = "New Refunds - ".$this->view->page_title;
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('refundList'));

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'received_date');
        $sortingMethod = $this->request->getParam('method', 'DESC');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());
        $bookingId = $this->request->getParam('booking_id');

        //
        // Load model
        //
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelRefund = new Model_Refund();

        //
        // Trim Filters
        //
        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
			
			var_dump($filters);
        }
        if ($bookingId) {
            $this->view->page_title = 'Refund - Invoices';
            $filters['booking_id'] = $bookingId;
        }
        
        if(!empty($filters['is_approved'])){
            if($filters['is_approved'] == 'all'){
                $this->view->page_title = "All Refunds - ".$this->view->page_title;
            }
            else if($filters['is_approved'] == 'yes'){
                $this->view->page_title = "Approved Refunds - ".$this->view->page_title;
            }
            else if($filters['is_approved'] == 'no'){
                $this->view->page_title = "Unapproved Refunds - ".$this->view->page_title;
            }
        }

        //
        //check if can see his or assigned invoice
        //
        if ($bookingId) {
            $invoice = $modelBookingInvoice->getByBookingId($bookingId);

            if (!$modelBookingInvoice->checkIfCanSeeHisOrAssignedInvoice($invoice['id'])) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        //
        // get data list
        //
        $this->view->data = $modelRefund->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);

        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
        $this->view->booking_id = $bookingId;
    }

    /**
     * Add new item action
     */
    public function addAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('refundAdd'));

        //
        // get request parameters
        //
        $receivedDate = $this->request->getParam('received_date');
        $bankCharges = $this->request->getParam('bank_charges');
        $amount = $this->request->getParam('amount');
        $description = $this->request->getParam('description');
        $reference = $this->request->getParam('reference');
        $amountWithheld = $this->request->getParam('amount_withheld');
        $withholdingTax = $this->request->getParam('withholding_tax');
        $isAcknowledgment = $this->request->getParam('is_acknowledgment');
        $paymentTypeId = $this->request->getParam('payment_type_id');
        $bookingId = $this->request->getParam('booking_id', 0);
		$contractorId = $this->request->getParam('contractor_id', 0);

        //
        // Load MODEL
        //
        $modelBooking = new Model_Booking();
        $modelPayment = new Model_Payment();
        $modelRefund = new Model_Refund();
        $modelBookingInvoice = new Model_BookingInvoice();

        //
        //get booking 
        //
        $booking = $modelBooking->getById($bookingId);
        if (!$booking) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Invalid booking"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }

        if (!$modelBooking->checkIfCanEditBooking($booking['booking_id'])) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }

        $approvedPayment = $modelPayment->getTotalPayment(array('booking_id' => $booking['booking_id'], 'is_approved' => 'yes'));
        /*if ($approvedPayment <= 0) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Con't Add Refund , No Approved Payment"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }*/

        $approvedRefund = $modelRefund->getTotalRefund(array('booking_id' => $booking['booking_id'], 'is_approved' => 'yes'));
        if ($approvedRefund >= $approvedPayment) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This invoice has already been refunded in full"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $allRefund = $modelRefund->getTotalRefund(array('booking_id' => $booking['booking_id'], 'is_approved' => 'all'));
        if ($allRefund >= $approvedPayment) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This invoice has already been refunded in full. Check new refunds"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }


        //
        // init action form
        //
        $options = array(
            'booking_id' => $bookingId
        );
        if ($booking) {
            $options['amount'] = $approvedPayment - $approvedRefund;
        }
        $form = new Invoices_Form_Refund($options);
		
		$dateTimeObj= get_settings_date_format();
		if($receivedDate && !empty($dateTimeObj))
		{
		  $receivedDate = dateFormat_zend_to_purePhp("d M Y", $receivedDate);
		}
		
		
		
		  // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
	    		 
			    $data = array(
                    'received_date'   => strtotime($receivedDate),
                    'bank_charges'    => round($bankCharges, 2),
                    'amount'          => round($amount, 2),
                    'description'     => $description,
                    'payment_type_id' => $paymentTypeId,
                    'booking_id'      => $booking['booking_id'],
                    'customer_id'     => $booking['customer_id'],
                    'user_id' => $this->loggedUser['user_id'],
                    'created' => time(),
                    'reference' => $reference,
                    'amount_withheld' => round($amountWithheld, 2),
                    'withholding_tax' => $withholdingTax,
                    'is_acknowledgment' => $isAcknowledgment,
					'contractor_id' => $contractorId
                );

                $success = $modelRefund->insert($data);

                if ($success) {
                    $modelBookingInvoice->updateByBookingId($booking['booking_id'], array('invoice_type' => 'open'));
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Refund saved"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                }

                if (CheckAuth::checkCredential(array('refundList'))) {
                    $this->_redirect($this->router->assemble(array('booking_id' => $booking['booking_id']), 'bookingRefundList'));
                } else {
                    $this->_redirect($this->router->assemble(array(), 'booking'));
                }
            }
        }

        //
        //send booking Invoice to view
        //
        $bookingInvoice = $modelBookingInvoice->getByBookingId($bookingId);
        $modelBookingInvoice->fill($bookingInvoice, array('booking', 'customer'));

        $this->view->bookingInvoice = $bookingInvoice;
        $this->view->form = $form;
    }

	/************* Do Refund using Eway ********************IBM*/
    public function addRefundByEwayAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('refundAdd'));
        $loggedUser = CheckAuth::getLoggedUser();

        $bookingId = $this->request->getParam('booking_id', 0);
        $paymentId = $this->request->getParam('payment_id', 0);
        // $contractorId = $this->request->getParam('contractor_id', 0);

        //
        // Load MODEL
        //
        $modelBooking = new Model_Booking();
        $modelPayment = new Model_Payment();
        $modelRefund = new Model_Refund();
        $modelBookingInvoice = new Model_BookingInvoice();

        //
        //get booking 
        //
        $booking = $modelBooking->getById($bookingId);
        if (!$booking) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Invalid booking"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }

        if (!$modelBooking->checkIfCanEditBooking($booking['booking_id'])) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }

        $approvedPayment = $modelPayment->getTotalPayment(array('booking_id' => $booking['booking_id'], 'is_approved' => 'yes'));

        $payment_data = $modelPayment->getByIdAndBookingId($paymentId,$bookingId);
        $paymentTypeId = $payment_data['payment_type_id'];
        $bookingInvoice = $modelBookingInvoice->getByBookingId($bookingId);


        $totalApprovedRefund = $modelRefund->getTotalRefund(array('booking_id' => $booking['booking_id'],'payment_transaction_id' => $payment_data['reference'], 'is_approved' => 'yes'));
        if ($totalApprovedRefund >= $payment_data['amount']) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "fully Refund"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $totalAllRefund = $modelRefund->getTotalRefund(array('booking_id' => $booking['booking_id'],'payment_transaction_id' => $payment_data['reference'], 'is_approved' => 'all'));
        if ($totalAllRefund >= $payment_data['amount']) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "fully Refund,Check Unapproved Refund"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }



         if(isset($totalAllRefund) && !empty($totalAllRefund)) { 
            $totalAmountRefund = $totalAllRefund;

         }else{
            $totalAmountRefund = 0;

         }
        
        //
        // init action form
        //
        $options = array(
            'booking_id' => $bookingId,
            'paymentData' => $payment_data,
            'bookingInvoice' => $bookingInvoice,
            'booking' => $booking,       
            'mode' => 'eway_refund'
        );
        if ($booking) {
            // $options['amount'] = $payment_data['amount'] - $totalAmountRefund;
            $options['avalibleRefund'] = $payment_data['amount'] - $totalAmountRefund;
            $options['refundedToDate'] = $totalAmountRefund;
        }

        // $form = new Invoices_Form_EwayRefund($options);
        $form = new Invoices_Form_Refund($options);        
        $this->view->form = $form;

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
			exit;
                
                $this->sendRefundTransaction();
                exit;
            } else {
                echo json_encode(array(
                    'typeMsg' => 'validation_error',
                    // 'IfSuccess' => false, 
                    'view' => $this->view->render('refund/add_by_eway.phtml')
                    ));
                exit;
            }
        }

        echo $this->view->render('refund/add_by_eway.phtml');
        exit;        
        
    }
/**********End************/

	
/***********Start ************/
    public function sendRefundTransaction(){
        $bookingId = $this->request->getParam('booking_id', 0);
        $customer_id = $this->request->getParam('customer_id');
        $receivedDate = $this->request->getParam('received_date');
        $bankCharges = $this->request->getParam('bank_charges');
        $avalible_refund = $this->request->getParam('avalible_refund');
        $isAcknowledgment = $this->request->getParam('is_acknowledgment');
        $paymentTypeId = $this->request->getParam('payment_type_id');
        $withholdingTax = $this->request->getParam('withholding_tax');
        $paymentTransactionId = $this->request->getParam('payment_transaction_id');
        $amountWithheld = $this->request->getParam('amount_withheld');
        $contractorId = $this->request->getParam('contractor_id');
        $InvoiceNumber = $this->request->getParam('invoice_num', 0);
        $received_date = date('d M Y');

        $modelBooking = new Model_Booking();
        $modelCustomer = new Model_Customer();
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelRefund = new Model_Refund();

        $booking = $modelBooking->getById($bookingId);
        $customerData = $modelCustomer->getById($customer_id);
        $bookingInvoice = $modelBookingInvoice->getByBookingId($bookingId);

        $amount = $avalible_refund * 100;




        $Refund = array(
            'TransactionID' => $paymentTransactionId,
            'TotalAmount' => $amount,
            'InvoiceNumber' => $InvoiceNumber,
        );  

        // $Refund = array(
        //     'TransactionID' => 14047520,
        //     'TotalAmount' => 200,
        //     'InvoiceNumber' => "INV-8530",
        // );    

        $transaction = array(
            'Refund' => $Refund
        );  

// var_dump($transaction);
        $goToUrl = $this->router->assemble(array('booking_id' => $bookingId), 'bookingRefundList');
        $url = $this->router->assemble(array('id' => $bookingInvoice['id']), 'invoiceView');                         
        // $session = new Zend_Session_Namespace();
        // $session->popup = '1';
        $redirect = array(
            'typeMsg' => 'sucsses_refund',
            'goToUrl' => $goToUrl
            );
        $error_redirect = array(
            'typeMsg' => 'error_refund',
            'goToUrl' => $url
            );
  

        // if (0) {
        Zend_Loader::loadFile('include_eway.php', APPLICATION_PATH . '/../library/eway-rapid-php-master1');
        // Zend_Loader::loadFile('include_eway.php', APPLICATION_PATH . '/../library/eway-rapid-master');

          $ewayLogger = new \Eway\Rapid\Service\Logger();         
        $apiKey =      'A1001CbqrpVrgR/OJWzGztNull6mdN+cGg/+Pe6aJQtj4dpaByyXOtfhAEuoyj8G2TUDSM';
        $apiPassword = 'Eu26QdcT';
        $apiEndpoint = \Eway\Rapid\Client::MODE_SANDBOX;
		  
        // }
		
		// Zend_Loader::loadFile('include_eway.php', APPLICATION_PATH . '/../library/eway-rapid-php-master1');
  //       $apiKey = '44DD7AhRBX08hfSoic2wv9mAF/Bs5St52K15ISLq6EaA29SCLz7GuY+G6eyiJkDLUvRKRU';
  //       $apiPassword = '72tQqX16';
  //       $apiEndpoint = \Eway\Rapid\Client::MODE_PRODUCTION;//'production';

       $client = \Eway\Rapid::createClient($apiKey, $apiPassword, $apiEndpoint,$ewayLogger);


	   
         


        // $client = \Eway\Rapid::createClient($apiKey, $apiPassword, $apiEndpoint);

        // $transaction = array(
        //     'Refund' => $Refund
        // );


		/*echo 'bookingId'.$bookingId;
		print_r($bookingInvoice);
		echo 'test '.$bookingInvoice['id'];*/

        // $goToUrl = $this->router->assemble(array('booking_id' => $bookingId), 'bookingRefundList');
        // // $url = $this->router->assemble(array('id' => $bookingInvoice['id']), 'invoiceView');  
        // $url = $this->router->assemble(array('booking_id' => $bookingId), 'refundAddByEway');                       
        // //$url = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';                         
        // // $session = new Zend_Session_Namespace();
        // // $session->popup = '1';
        // $redirect = array(
        //      // 'typeMsg' => 'sucsses_refund',
        //     'IfSuccess' => true,
        //     'goToUrl' => $goToUrl
        //     );
        // $error = array(
        //     // 'typeMsg' => 'error_refund',
        //     'IfSuccess' => false,
        //     'url' => $url
        //     );

        $response = $client->refund($transaction);
// print_r($client); exit;  
            if ($response->TransactionStatus) {

                //insert to refund table **************IBM*/
                $data = array(
                'received_date' => strtotime($received_date),
                'bank_charges' => 0,
                'amount' => round($avalible_refund, 2),
                // 'description' => $InvoiceDescription,
                'payment_type_id' => $paymentTypeId, //payment type is creditCard
                'booking_id' => $booking['booking_id'],
                'customer_id' => $booking['customer_id'],
                'user_id' => $this->loggedUser['user_id'],
                'created' => time(),
                'reference' => $response->TransactionID,
                'payment_transaction_id' => $paymentTransactionId,
                'amount_withheld' => 0,
                'withholding_tax' => 0,
                'is_acknowledgment' => $isAcknowledgment,
                'contractor_id' => $contractorId,
                'is_approved' => 1
                );
                $success = $modelRefund->insert($data);

                $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => 'Refund successfully Added By Eway!        ID: ' . $response->TransactionID));

                if (CheckAuth::checkCredential(array('refundList'))) {
                   // $this->_redirect($this->router->assemble(array('booking_id' => $booking['booking_id']), 'bookingRefundList'));
                   echo json_encode($redirect);
                } 
                // else {
                //     echo json_encode($error_redirect);
                //     // $this->_redirect($this->router->assemble(array(), 'refundAddByEway'));
                // }
            } else {
                if ($response->getErrors()) {

                    foreach ($response->getErrors() as $error) {
                       $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Error: " . \Eway\Rapid::getMessage($error) ."<br>"));
                       // $error_response['error_msg'] = "Error: " . \Eway\Rapid::getMessage($error) ."<br>";
					}
                    echo json_encode($error_redirect);						
                        // $this->_redirect($this->router->assemble(array(), 'refundAddByEway'));
                    
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => 'Sorry, your Refund failed'));
                     // $error_response['error_msg'] = 'Sorry, your Refund failed';	

                    echo json_encode($error_redirect);
                    // $this->_redirect($this->router->assemble(array(), 'refundAddByEway'));
                }

            }
            exit;
    }
/**********END*********/


    public function deleteAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('refundDelete'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $bookingId = $this->request->getParam('booking_id', 0);

        //
        // Load MODEL
        //
        $modelBooking = new Model_Booking();
        $modelRefund = new Model_Refund();

        //
        //Validation
        //
        if (!$modelBooking->checkIfCanEditBooking($bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }

        $refund = $modelRefund->getByIdAndBookingId($id, $bookingId);
        if ($refund['is_approved']) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You can't delete approved refunds. Remove approval and try again"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        if ($refund) {
            //delete refund
            $modelRefund->deleteById($id);
        }

        $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
    }

    public function editAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('refundEdit'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $receivedDate = $this->request->getParam('received_date');
        $bankCharges = $this->request->getParam('bank_charges');
        $amount = $this->request->getParam('amount');
        $description = $this->request->getParam('description');
        $reference = $this->request->getParam('reference');
        $amountWithheld = $this->request->getParam('amount_withheld');
        $withholdingTax = $this->request->getParam('withholding_tax');
        $isAcknowledgment = $this->request->getParam('is_acknowledgment');
        $paymentTypeId = $this->request->getParam('payment_type_id');
		$contractorId = $this->request->getParam('contractor_id', 0);


        //
        // Load Model
        //
        $modelRefund = new Model_Refund();
        $modelBooking = new Model_Booking();
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelPayment = new Model_Payment();

        //
        //Validation
        //
        $refund = $modelRefund->getById($id);
        if ($refund['is_approved']) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You can't edit approved refunds. Remove approval and try again"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $booking = $modelBooking->getById($refund['booking_id']);
        if (!$booking) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Invalid booking"));
            $this->_redirect($this->router->assemble(array(), 'invoices'));
        }

        if (!$modelBooking->checkIfCanEditBooking($booking['booking_id'])) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }

        //
        //calculate amount
        //
        $approvedPayment = $modelPayment->getTotalPayment(array('booking_id' => $booking['booking_id'], 'is_approved' => 'yes'));
        $approvedRefund = $modelRefund->getTotalRefund(array('booking_id' => $booking['booking_id'], 'is_approved' => 'yes'));
        $bookingAmount = $approvedPayment - $approvedRefund;

		
        //
        // init action form
        //
        $options = array(
            'booking_id' => $booking['booking_id'],
            'mode' => 'update',
            'refund' => $refund,
            'amount' => $bookingAmount,
			'contractor_id' => $contractorId
        );
        $form = new Invoices_Form_Refund($options);
		$dateTimeObj= get_settings_date_format();
	    if($receivedDate && !empty($dateTimeObj))
	    {
			$receivedDate = dateFormat_zend_to_purePhp("d M Y", $receivedDate);
		}
			
				
        //
			
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
		        	$data = array(
                    'received_date' => strtotime($receivedDate),
                    'bank_charges' => round($bankCharges, 2),
                    'amount' => round($amount, 2),
                    'description' => $description,
                    'payment_type_id' => $paymentTypeId,
                    'booking_id' => $booking['booking_id'],
                    'customer_id' => $booking['customer_id'],
                    'user_id' => $this->loggedUser['user_id'],
                    'created' => time(),
                    'reference' => $reference,
                    'amount_withheld' => round($amountWithheld, 2),
                    'withholding_tax' => $withholdingTax,
                    'is_acknowledgment' => $isAcknowledgment,
					'contractor_id' => $contractorId
                );

			
                $success = $modelRefund->updateById($id, $data);
                if ($success) {
                    $modelBookingInvoice->updateByBookingId($booking['booking_id'], array('invoice_type' => 'open'));
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Refund saved"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                }

                if (CheckAuth::checkCredential(array('RefundList'))) {
                    $this->_redirect($this->router->assemble(array('booking_id' => $booking['booking_id']), 'bookingRefundList'));
                } else {
                    $this->_redirect($this->router->assemble(array(), 'invoices'));
                }
            }
        }

        //
        //send booking Invoice to view
        //
        $bookingInvoice = $modelBookingInvoice->getByBookingId($booking['booking_id']);
        $modelBookingInvoice->fill($bookingInvoice, array('booking', 'customer'));

        $this->view->bookingInvoice = $bookingInvoice;
        $this->view->form = $form;
    }

    public function refundApproveAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('refundApprove'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $bookingId = $this->request->getParam('booking_id', 0);

        //
        // Load Model
        //
        $modelUser = new Model_User();
        $modelPayment = new Model_Payment();
        $modelRefund = new Model_Refund();
        $modelBooking = new Model_Booking();
        $modelCustomer = new Model_Customer();
        $modelPaymentType = new Model_PaymentType();
        $modelBookingAddress = new Model_BookingAddress();
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();

        //
        //Validation
        //
        $booking = $modelBooking->getById($bookingId);
        if (!$booking) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Invalid booking"));
            $this->_redirect($this->router->assemble(array(), 'invoices'));
        }

        $invoice = $modelBookingInvoice->getByBookingId($bookingId);
        $refund = $modelRefund->getByIdAndBookingId($id, $booking['booking_id']);

        if ($refund) {
            $success = $modelRefund->updateById($id, array('is_approved' => 1));

            if ($success) {
                /**
                 * save calculation qoute
                 */
                $totalRefund = $modelRefund->getTotalRefund(array('booking_id' => $booking['booking_id'], 'is_approved' => 'yes'));
                $paidAmount = $modelPayment->getTotalAmount(array('booking_id' => $booking['booking_id'], 'is_approved' => 'yes'));

                if ($paidAmount < 0) {
                    $paidAmount = 0;
                }

                $subTotal = $modelContractorServiceBooking->getTotalBookingQoute($booking['booking_id']);
                $total = ($subTotal + $booking['call_out_fee']) - $booking['total_discount'];
                $gstTax = $total * get_config('gst_tax');
                $totalQoute = $total + $gstTax;
                $totalQoute = $totalQoute - $totalRefund;

                $db_params = array();
                $db_params['qoute'] = round($totalQoute, 2);
                $db_params['refund'] = round($totalRefund, 2);
                $db_params['gst'] = round($gstTax, 2);
                $db_params['paid_amount'] = round($paidAmount, 2);
                $modelBooking->updateById($booking['booking_id'], $db_params);

                $booking = $modelBooking->getById($booking['booking_id']);
                if ($paidAmount >= $booking['qoute']) {
                    $modelBookingInvoice->updateByBookingId($booking['booking_id'], array('invoice_type' => 'closed'));
                } else {
                    $modelBookingInvoice->updateByBookingId($booking['booking_id'], array('invoice_type' => 'open'));
                }

                // send email to customer
                if ($refund['is_acknowledgment']) {

                    //
                    // filling extra data
                    //
                    $customer = $modelCustomer->getById($booking['customer_id']);
                    $user = $modelUser->getById($booking['created_by']);
                    $paymentType = $modelPaymentType->getById($refund['payment_type_id']);

						$dateTimeObj= get_settings_date_format();
						if(!empty($dateTimeObj)){
						   $refund_received_date= getNewDateFormat($refund['received_date']);
						   $booking_created= getNewDateFormat($booking['created']);
						   $booking_start=getNewDateFormat(strtotime($booking['booking_start']),'all');

						}
						else{
                        $refund_received_date=date('Y-m-d', $refund['received_date']);
						$booking_created= date('d/m/Y', $booking['created']);
                        $booking_start=date("F j, Y, g:i a", strtotime($booking['booking_start']));

						}
					   $template_params = array(
                        //payment
                        '{payment_type}' => $paymentType['payment_type'],
                        //'{refund_received_date}' => getNewDateFormat($refund['received_date']),
                        //'{refund_received_date}' => date('Y-m-d', $refund['received_date']),
                        '{refund_received_date}' => $refund_received_date,
						'{refund_amount}' => number_format($refund['amount'], 2),
                        '{refund_reference}' => $refund['reference'],
                        //booking
                        '{booking_num}' => $booking['booking_num'],
                        '{total_without_tax}' => number_format($booking['sub_total'], 2),
                        '{gst_tax}' => number_format($booking['gst'], 2),
                        '{total_with_tax}' => number_format($booking['qoute'], 2),
                        '{description}' => $booking['description'] ? $booking['description'] : '',
                        //'{booking_created}' => getNewDateFormat($booking['created']),
						//'{booking_created}' => date('d/m/Y', $booking['created']),
						'{booking_created}' => $booking_created,
                        '{booking_created_by}' => ucwords($user['username']),
                        //'{booking_start}' => getNewDateFormat(strtotime($booking['booking_start']),'all'),
                        //'{booking_start}' => date("F j, Y, g:i a", strtotime($booking['booking_start'])),
                        '{booking_start}' => $booking_start,
					   '{booking_address}' => get_line_address($modelBookingAddress->getByBookingId($booking['booking_id'])),
                        '{service}' => nl2br($modelContractorServiceBooking->getBookingAsText($booking['booking_id'])),
                        //customer
                        '{customer_name}' => get_customer_name($customer),
                        '{customer_first_name}' => isset($customer['first_name']) && $customer['first_name'] ? ucwords($customer['first_name']) : '',
                        '{customer_last_name}' => isset($customer['last_name']) && $customer['last_name'] ? ' ' . ucwords($customer['last_name']) : '',
                        '{customer_contacts}' => nl2br($modelCustomer->getCustomerContacts($booking['customer_id'])),
                    );

                    $customer = $modelCustomer->getById($booking['customer_id']);

                    $email_log = array(
                        'reference_id' => $invoice['id'],
                        'type' => 'invoice'
                    );

                    $to = array();
                    if ($customer['email1']) {
                        $to[] = $customer['email1'];
                    }
                    if ($customer['email2']) {
                        $to[] = $customer['email2'];
                    }
                    if ($customer['email2']) {
                        $to[] = $customer['email3'];
                    }
                    $to = implode(',', $to);

                    if ($to) {
                        //send email
                        //EmailNotification::sendEmail(array('to' => $to), 'refund_acknowledgment', $template_params, $email_log);
                        EmailNotification::sendEmail(array('to' => 'jass-moh@hotmail.com'), 'refund_acknowledgment', $template_params, $email_log);

                    }
                }
            }

            if ($success) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Refund approved"));
            } else {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Failed to approve refund, please try again"));
            }
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }


        $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Failed to approve refund, please try again"));
        $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
    }

    public function refundUnapproveAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('refundApprove'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $bookingId = $this->request->getParam('booking_id', 0);

        //
        // Load Model
        //
        $modelBooking = new Model_Booking();
        $modelPayment = new Model_Payment();
        $modelRefund = new Model_Refund();
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();

        //
        //Validation
        //
        $booking = $modelBooking->getById($bookingId);
        if (!$booking) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Invalid booking"));
            $this->_redirect($this->router->assemble(array(), 'invoices'));
        }

        $refund = $modelRefund->getById($id);
        if ($refund) {
            $success = $modelRefund->updateById($id, array('is_approved' => 0));

            if ($success) {
                /**
                 * save calculation qoute
                 */
                $totalRefund = $modelRefund->getTotalRefund(array('booking_id' => $booking['booking_id'], 'is_approved' => 'yes'));
                $paidAmount = $modelPayment->getTotalAmount(array('booking_id' => $booking['booking_id'], 'is_approved' => 'yes'));

                if ($paidAmount < 0) {
                    $paidAmount = 0;
                }

                $subTotal = $modelContractorServiceBooking->getTotalBookingQoute($booking['booking_id']);
                $total = ($subTotal + $booking['call_out_fee']) - $booking['total_discount'];
                $gstTax = $total * get_config('gst_tax');
                $totalQoute = $total + $gstTax;
                $totalQoute = $totalQoute - $totalRefund;

                $db_params = array();
                $db_params['qoute'] = round($totalQoute, 2);
                $db_params['refund'] = round($totalRefund, 2);
                $db_params['gst'] = round($gstTax, 2);
                $db_params['paid_amount'] = round($paidAmount, 2);
                $modelBooking->updateById($booking['booking_id'], $db_params);

                $booking = $modelBooking->getById($booking['booking_id']);
                if ($paidAmount >= $booking['qoute']) {
                    $modelBookingInvoice->updateByBookingId($booking['booking_id'], array('invoice_type' => 'closed'));
                } else {
                    $modelBookingInvoice->updateByBookingId($booking['booking_id'], array('invoice_type' => 'open'));
                }
            }

            if ($success) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Refund approval removed"));
            } else {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Failed to remove refund approval, please try again"));
            }
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Failed to remove refund approval, please try again"));
        $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
    }

	
	/**Refund*******IBM*/
    public function refundEwayAction(){

          // Zend_Loader::loadFile('include_eway.php', APPLICATION_PATH . '/../library/eway-rapid-php');
          // Zend_Loader::loadFile('include_eway.php', APPLICATION_PATH . '/../library/eway-rapid-php-master1');
          Zend_Loader::loadFile('include_eway.php', APPLICATION_PATH . '/../library/eway-rapid-master');
          
          $ewayLogger = new \Eway\Rapid\Service\Logger();  
          $apiKey =      'A1001CbqrpVrgR/OJWzGztNull6mdN+cGg/+Pe6aJQtj4dpaByyXOtfhAEuoyj8G2TUDSM';
          $apiPassword = 'Eu26QdcT';
          $apiEndpoint = \Eway\Rapid\Client::MODE_SANDBOX;
          // $apiEndpoint = 'sandbox';


        $client = \Eway\Rapid::createClient($apiKey, $apiPassword, $apiEndpoint,$ewayLogger);

       $refund = array(
            'Refund' => array(
                'TransactionID' => 14047520,
                'TotalAmount' => 500
            ),
        );
		$refund='';

        $response = $client->refund($refund);
        
  //       var_dump($client);
		// echo '<br><br><br>';
        var_dump($response);
		/**********************************/
            if ($response->TransactionStatus) {
                echo 'Refund successful! ID: '.$response->TransactionID;
            } else {
                if ($response->getErrors()) {
                    foreach ($response->getErrors() as $error) {
                        echo "Error: ".\Eway\Rapid::getMessage($error)."<br>";
                    }
                } else {
                    echo 'Sorry, your refund failed';
                }
            }
		
    }
	

}
