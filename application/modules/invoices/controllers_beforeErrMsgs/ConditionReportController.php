<?php

class Invoices_ConditionReportController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
    }

    public function indexAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('conditionReport'));

        $invoiceId = $this->request->getParam('id');


        //
        //load model
        //
         $modelBookingInvoice = new Model_BookingInvoice();


        $bookingInvoice = $modelBookingInvoice->getById($invoiceId);


        $form = new Invoices_Form_ConditionReport(array('condition-report' => $bookingInvoice));
        $this->view->form = $form;

        if ($this->request->isPost()) {

            $conditionReport = $this->request->getParam('report');
            $data = array(
                'condition_report' => $conditionReport
            );
            $success = $modelBookingInvoice->updateById($invoiceId, $data);

            if ($success) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
            } else {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Condition Report"));
            }

            echo 1;
            exit;
        }

        $this->view->invoiceId = $invoiceId;


        echo $this->view->render('condition-report/index.phtml');
        exit;
    }

}

