<?php

class Invoices_DiscussionController extends Zend_Controller_Action {

    private $request;
    private $router;
    private $loggedUser;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        //
        //checkLoggedIn
        //
        CheckAuth::checkLoggedIn();

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('invoiceDiscussion'));

        //
        //get Logged User
        //
        $this->loggedUser = CheckAuth::getLoggedUser();
    }

    public function indexAction() {

        //
        //get Params
        //
        $invoiceId = $this->request->getParam('id', 0);

        //
        // load model
        //
        $modelInvoice = new Model_BookingInvoice();
        $modelInvoiceDiscussion = new Model_InvoiceDiscussion();
        $modelContractorInfo = new Model_ContractorInfo();
        $modelUser = new Model_User();

        $invoice = $modelInvoice->getById($invoiceId);
        $this->view->invoice = $invoice;

        if (!$invoice) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "There Is No Item Exists"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $invoiceDiscussions = $modelInvoiceDiscussion->getByInvoiceId($invoiceId);
        foreach ($invoiceDiscussions as &$invoiceDiscussion) {
            $invoiceDiscussion['user'] = $modelUser->getById($invoiceDiscussion['user_id']);
            if ($invoiceDiscussion['user']['role_name'] == 'contractor') {
                $contractorInfo = $modelContractorInfo->getByContractorId($invoiceDiscussion['user']['user_id']);
                $invoiceDiscussion['user']['username'] = ucwords($invoiceDiscussion['user']['username']) . ' - ' . ucwords($contractorInfo['business_name']);
            } else {
                $invoiceDiscussion['user']['username'] = ucwords($invoiceDiscussion['user']['username']);
            }
        }

        $this->view->invoiceDiscussions = $invoiceDiscussions;

        echo $this->view->render('discussion/index.phtml');
        exit;
    }

    public function submitAction() {

        //
        //get Params
        //
        $invoiceId = $this->request->getParam('id', 0);
        $discussion = $this->request->getParam('discussion', '');

        //
        // load model
        //
        $modelInvoiceDiscussion = new Model_InvoiceDiscussion();

        $success = 0;
        if ($discussion) {
            $data = array(
                'invoice_id' => $invoiceId,
                'user_id' => $this->loggedUser['user_id'],
                'user_message' => $discussion,
                'created' => time()
            );
            $success = $modelInvoiceDiscussion->insert($data);
        }

        if ($success) {
            echo json_encode(array('success' => 1));
        } else {
            echo json_encode(array('success' => 0, 'errMsg' => 'Invalid Discussion'));
        }
        exit;
    }

    public function getAllDiscussionAction() {

        //
        //get Params
        //
        $invoiceId = $this->request->getParam('id', 0);

        //
        // load model
        //
        $modelInvoiceDiscussion = new Model_InvoiceDiscussion();
        $modelContractorInfo = new Model_ContractorInfo();
        $modelUser = new Model_User();


        $invoiceDiscussions = $modelInvoiceDiscussion->getByInvoiceId($invoiceId);
        foreach ($invoiceDiscussions as &$invoiceDiscussion) {
            $invoiceDiscussion['user'] = $modelUser->getById($invoiceDiscussion['user_id']);
            if ($invoiceDiscussion['user']['role_name'] == 'contractor') {
                $contractorInfo = $modelContractorInfo->getByContractorId($invoiceDiscussion['user']['user_id']);
                $invoiceDiscussion['user']['username'] = ucwords($invoiceDiscussion['user']['username']) . ' - ' . ucwords($contractorInfo['business_name']);
            } else {
                $invoiceDiscussion['user']['username'] = ucwords($invoiceDiscussion['user']['username']);
            }
            $invoiceDiscussion['discussion_date'] = getDateFormating( $invoiceDiscussion['created']);
            $invoiceDiscussion['user_message'] = nl2br(htmlentities($invoiceDiscussion['user_message'], ENT_NOQUOTES, 'UTF-8'));
        }

        $json_array = array(
            'discussions' => $invoiceDiscussions,
            'count' => count($invoiceDiscussions)
        );

        echo json_encode($json_array);
        exit;
    }

}

