<?php

class Invoices_RefundController extends Zend_Controller_Action {

    private $request;
    private $router;
    private $loggedUser;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'invoices';
        $this->view->sub_menu = 'refund';

        //
        // check Auth for logged user
        //
        $this->loggedUser = CheckAuth::getLoggedUser();
    }

    /**
     * Items list action
     */
    public function indexAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('refundList'));

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'received_date');
        $sortingMethod = $this->request->getParam('method', 'DESC');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());
        $bookingId = $this->request->getParam('booking_id');

        //
        // Load model
        //
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelRefund = new Model_Refund();

        //
        // Trim Filters
        //
        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }
        if ($bookingId) {
            $filters['booking_id'] = $bookingId;
        }

        //
        //check if can see his or assigned invoice
        //
        if ($bookingId) {
            $invoice = $modelBookingInvoice->getByBookingId($bookingId);

            if (!$modelBookingInvoice->checkIfCanSeeHisOrAssignedInvoice($invoice['id'])) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        //
        // get data list
        //
        $this->view->data = $modelRefund->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);

        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
        $this->view->booking_id = $bookingId;
    }

    /**
     * Add new item action
     */
    public function addAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('refundAdd'));

        //
        // get request parameters
        //
        $receivedDate = $this->request->getParam('received_date');
        $bankCharges = $this->request->getParam('bank_charges');
        $amount = $this->request->getParam('amount');
        $description = $this->request->getParam('description');
        $reference = $this->request->getParam('reference');
        $amountWithheld = $this->request->getParam('amount_withheld');
        $withholdingTax = $this->request->getParam('withholding_tax');
        $isAcknowledgment = $this->request->getParam('is_acknowledgment');
        $paymentTypeId = $this->request->getParam('payment_type_id');
        $bookingId = $this->request->getParam('booking_id', 0);
		$contractorId = $this->request->getParam('contractor_id', 0);

        //
        // Load MODEL
        //
        $modelBooking = new Model_Booking();
        $modelPayment = new Model_Payment();
        $modelRefund = new Model_Refund();
        $modelBookingInvoice = new Model_BookingInvoice();

        //
        //get booking 
        //
        $booking = $modelBooking->getById($bookingId);
        if (!$booking) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Invalid Booking"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }

        if (!$modelBooking->checkIfCanEditBooking($booking['booking_id'])) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }

        $approvedPayment = $modelPayment->getTotalPayment(array('booking_id' => $booking['booking_id'], 'is_approved' => 'yes'));
        /*if ($approvedPayment <= 0) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Con't Add Refund , No Approved Payment"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }*/

        $approvedRefund = $modelRefund->getTotalRefund(array('booking_id' => $booking['booking_id'], 'is_approved' => 'yes'));
        if ($approvedRefund >= $approvedPayment) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "fully Refund"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $allRefund = $modelRefund->getTotalRefund(array('booking_id' => $booking['booking_id'], 'is_approved' => 'all'));
        if ($allRefund >= $approvedPayment) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "fully Refund,Check Unapproved Refund"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }


        //
        // init action form
        //
        $options = array(
            'booking_id' => $bookingId
        );
        if ($booking) {
            $options['amount'] = $approvedPayment - $approvedRefund;
        }

        $form = new Invoices_Form_Refund($options);

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'received_date' => strtotime($receivedDate),
                    'bank_charges' => round($bankCharges, 2),
                    'amount' => round($amount, 2),
                    'description' => $description,
                    'payment_type_id' => $paymentTypeId,
                    'booking_id' => $booking['booking_id'],
                    'customer_id' => $booking['customer_id'],
                    'user_id' => $this->loggedUser['user_id'],
                    'created' => time(),
                    'reference' => $reference,
                    'amount_withheld' => round($amountWithheld, 2),
                    'withholding_tax' => $withholdingTax,
                    'is_acknowledgment' => $isAcknowledgment,
					'contractor_id' => $contractorId
                );

                $success = $modelRefund->insert($data);

                if ($success) {
                    $modelBookingInvoice->updateByBookingId($booking['booking_id'], array('invoice_type' => 'open'));
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully, Please Approved this Refund"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Refund"));
                }

                if (CheckAuth::checkCredential(array('refundList'))) {
                    $this->_redirect($this->router->assemble(array('booking_id' => $booking['booking_id']), 'bookingRefundList'));
                } else {
                    $this->_redirect($this->router->assemble(array(), 'booking'));
                }
            }
        }

        //
        //send booking Invoice to view
        //
        $bookingInvoice = $modelBookingInvoice->getByBookingId($bookingId);
        $modelBookingInvoice->fill($bookingInvoice, array('booking', 'customer'));

        $this->view->bookingInvoice = $bookingInvoice;
        $this->view->form = $form;
    }

    public function deleteAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('refundDelete'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $bookingId = $this->request->getParam('booking_id', 0);

        //
        // Load MODEL
        //
        $modelBooking = new Model_Booking();
        $modelRefund = new Model_Refund();

        //
        //Validation
        //
        if (!$modelBooking->checkIfCanEditBooking($bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }

        $refund = $modelRefund->getByIdAndBookingId($id, $bookingId);
        if ($refund['is_approved']) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Could not delete Approved Refund, Please change to Unapproved to delete"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        if ($refund) {
            //delete refund
            $modelRefund->deleteById($id);
        }

        $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
    }

    public function editAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('refundEdit'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $receivedDate = $this->request->getParam('received_date');
        $bankCharges = $this->request->getParam('bank_charges');
        $amount = $this->request->getParam('amount');
        $description = $this->request->getParam('description');
        $reference = $this->request->getParam('reference');
        $amountWithheld = $this->request->getParam('amount_withheld');
        $withholdingTax = $this->request->getParam('withholding_tax');
        $isAcknowledgment = $this->request->getParam('is_acknowledgment');
        $paymentTypeId = $this->request->getParam('payment_type_id');
		$contractorId = $this->request->getParam('contractor_id', 0);


        //
        // Load Model
        //
        $modelRefund = new Model_Refund();
        $modelBooking = new Model_Booking();
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelPayment = new Model_Payment();

        //
        //Validation
        //
        $refund = $modelRefund->getById($id);
        if ($refund['is_approved']) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Could not edit Approved Refund, Please change to Unapproved to edit"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $booking = $modelBooking->getById($refund['booking_id']);
        if (!$booking) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Invalid Booking"));
            $this->_redirect($this->router->assemble(array(), 'invoices'));
        }

        if (!$modelBooking->checkIfCanEditBooking($booking['booking_id'])) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }

        //
        //calculate amount
        //
        $approvedPayment = $modelPayment->getTotalPayment(array('booking_id' => $booking['booking_id'], 'is_approved' => 'yes'));
        $approvedRefund = $modelRefund->getTotalRefund(array('booking_id' => $booking['booking_id'], 'is_approved' => 'yes'));

        $bookingAmount = $approvedPayment - $approvedRefund;

        //
        // init action form
        //
        $options = array(
            'booking_id' => $booking['booking_id'],
            'mode' => 'update',
            'refund' => $refund,
            'amount' => $bookingAmount,
			'contractor_id' => $contractorId
        );
        $form = new Invoices_Form_Refund($options);

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'received_date' => strtotime($receivedDate),
                    'bank_charges' => round($bankCharges, 2),
                    'amount' => round($amount, 2),
                    'description' => $description,
                    'payment_type_id' => $paymentTypeId,
                    'booking_id' => $booking['booking_id'],
                    'customer_id' => $booking['customer_id'],
                    'user_id' => $this->loggedUser['user_id'],
                    'created' => time(),
                    'reference' => $reference,
                    'amount_withheld' => round($amountWithheld, 2),
                    'withholding_tax' => $withholdingTax,
                    'is_acknowledgment' => $isAcknowledgment,
					'contractor_id' => $contractorId
                );

                $success = $modelRefund->updateById($id, $data);
                if ($success) {
                    $modelBookingInvoice->updateByBookingId($booking['booking_id'], array('invoice_type' => 'open'));
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully, Please Approved this Refund"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Refund"));
                }

                if (CheckAuth::checkCredential(array('RefundList'))) {
                    $this->_redirect($this->router->assemble(array('booking_id' => $booking['booking_id']), 'bookingRefundList'));
                } else {
                    $this->_redirect($this->router->assemble(array(), 'invoices'));
                }
            }
        }

        //
        //send booking Invoice to view
        //
        $bookingInvoice = $modelBookingInvoice->getByBookingId($booking['booking_id']);
        $modelBookingInvoice->fill($bookingInvoice, array('booking', 'customer'));

        $this->view->bookingInvoice = $bookingInvoice;
        $this->view->form = $form;
    }

    public function refundApproveAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('refundApprove'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $bookingId = $this->request->getParam('booking_id', 0);

        //
        // Load Model
        //
        $modelUser = new Model_User();
        $modelPayment = new Model_Payment();
        $modelRefund = new Model_Refund();
        $modelBooking = new Model_Booking();
        $modelCustomer = new Model_Customer();
        $modelPaymentType = new Model_PaymentType();
        $modelBookingAddress = new Model_BookingAddress();
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();

        //
        //Validation
        //
        $booking = $modelBooking->getById($bookingId);
        if (!$booking) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Invalid Booking"));
            $this->_redirect($this->router->assemble(array(), 'invoices'));
        }

        $invoice = $modelBookingInvoice->getByBookingId($bookingId);
        $refund = $modelRefund->getByIdAndBookingId($id, $booking['booking_id']);

        if ($refund) {
            $success = $modelRefund->updateById($id, array('is_approved' => 1));

            if ($success) {
                /**
                 * save calculation qoute
                 */
                $totalRefund = $modelRefund->getTotalRefund(array('booking_id' => $booking['booking_id'], 'is_approved' => 'yes'));
                $paidAmount = $modelPayment->getTotalAmount(array('booking_id' => $booking['booking_id'], 'is_approved' => 'yes'));

                if ($paidAmount < 0) {
                    $paidAmount = 0;
                }

                $subTotal = $modelContractorServiceBooking->getTotalBookingQoute($booking['booking_id']);
                $total = ($subTotal + $booking['call_out_fee']) - $booking['total_discount'];
                $gstTax = $total * get_config('gst_tax');
                $totalQoute = $total + $gstTax;
                $totalQoute = $totalQoute - $totalRefund;

                $db_params = array();
                $db_params['qoute'] = round($totalQoute, 2);
                $db_params['refund'] = round($totalRefund, 2);
                $db_params['gst'] = round($gstTax, 2);
                $db_params['paid_amount'] = round($paidAmount, 2);
                $modelBooking->updateById($booking['booking_id'], $db_params);

                $booking = $modelBooking->getById($booking['booking_id']);
                if ($paidAmount >= $booking['qoute']) {
                    $modelBookingInvoice->updateByBookingId($booking['booking_id'], array('invoice_type' => 'closed'));
                } else {
                    $modelBookingInvoice->updateByBookingId($booking['booking_id'], array('invoice_type' => 'open'));
                }

                // send email to customer
                if ($refund['is_acknowledgment']) {

                    //
                    // filling extra data
                    //
                    $customer = $modelCustomer->getById($booking['customer_id']);
                    $user = $modelUser->getById($booking['created_by']);
                    $paymentType = $modelPaymentType->getById($refund['payment_type_id']);

                    $template_params = array(
                        //payment
                        '{payment_type}' => $paymentType['payment_type'],
                        '{refund_received_date}' => date('Y-m-d', $refund['received_date']),
                        '{refund_amount}' => number_format($refund['amount'], 2),
                        '{refund_reference}' => $refund['reference'],
                        //booking
                        '{booking_num}' => $booking['booking_num'],
                        '{total_without_tax}' => number_format($booking['sub_total'], 2),
                        '{gst_tax}' => number_format($booking['gst'], 2),
                        '{total_with_tax}' => number_format($booking['qoute'], 2),
                        '{description}' => $booking['description'] ? $booking['description'] : '',
                        '{booking_created}' => date('d/m/Y', $booking['created']),
                        '{booking_created_by}' => ucwords($user['username']),
                        '{booking_start}' => date("F j, Y, g:i a", strtotime($booking['booking_start'])),
                        '{booking_address}' => get_line_address($modelBookingAddress->getByBookingId($booking['booking_id'])),
                        '{service}' => nl2br($modelContractorServiceBooking->getBookingAsText($booking['booking_id'])),
                        //customer
                        '{customer_name}' => get_customer_name($customer),
                        '{customer_first_name}' => isset($customer['first_name']) && $customer['first_name'] ? ucwords($customer['first_name']) : '',
                        '{customer_last_name}' => isset($customer['last_name']) && $customer['last_name'] ? ' ' . ucwords($customer['last_name']) : '',
                        '{customer_contacts}' => nl2br($modelCustomer->getCustomerContacts($booking['customer_id'])),
                    );

                    $customer = $modelCustomer->getById($booking['customer_id']);

                    $email_log = array(
                        'reference_id' => $invoice['id'],
                        'type' => 'invoice'
                    );

                    $to = array();
                    if ($customer['email1']) {
                        $to[] = $customer['email1'];
                    }
                    if ($customer['email2']) {
                        $to[] = $customer['email2'];
                    }
                    if ($customer['email2']) {
                        $to[] = $customer['email3'];
                    }
                    $to = implode(',', $to);

                    if ($to) {
                        //send email
                        EmailNotification::sendEmail(array('to' => $to), 'refund_acknowledgment', $template_params, $email_log);
                    }
                }
            }

            if ($success) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Refund Approved successfully"));
            } else {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Could not Approved the Refund"));
            }
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }


        $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Could not Approved the Refund"));
        $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
    }

    public function refundUnapproveAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('refundApprove'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $bookingId = $this->request->getParam('booking_id', 0);

        //
        // Load Model
        //
        $modelBooking = new Model_Booking();
        $modelPayment = new Model_Payment();
        $modelRefund = new Model_Refund();
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();

        //
        //Validation
        //
        $booking = $modelBooking->getById($bookingId);
        if (!$booking) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Invalid Booking"));
            $this->_redirect($this->router->assemble(array(), 'invoices'));
        }

        $refund = $modelRefund->getById($id);
        if ($refund) {
            $success = $modelRefund->updateById($id, array('is_approved' => 0));

            if ($success) {
                /**
                 * save calculation qoute
                 */
                $totalRefund = $modelRefund->getTotalRefund(array('booking_id' => $booking['booking_id'], 'is_approved' => 'yes'));
                $paidAmount = $modelPayment->getTotalAmount(array('booking_id' => $booking['booking_id'], 'is_approved' => 'yes'));

                if ($paidAmount < 0) {
                    $paidAmount = 0;
                }

                $subTotal = $modelContractorServiceBooking->getTotalBookingQoute($booking['booking_id']);
                $total = ($subTotal + $booking['call_out_fee']) - $booking['total_discount'];
                $gstTax = $total * get_config('gst_tax');
                $totalQoute = $total + $gstTax;
                $totalQoute = $totalQoute - $totalRefund;

                $db_params = array();
                $db_params['qoute'] = round($totalQoute, 2);
                $db_params['refund'] = round($totalRefund, 2);
                $db_params['gst'] = round($gstTax, 2);
                $db_params['paid_amount'] = round($paidAmount, 2);
                $modelBooking->updateById($booking['booking_id'], $db_params);

                $booking = $modelBooking->getById($booking['booking_id']);
                if ($paidAmount >= $booking['qoute']) {
                    $modelBookingInvoice->updateByBookingId($booking['booking_id'], array('invoice_type' => 'closed'));
                } else {
                    $modelBookingInvoice->updateByBookingId($booking['booking_id'], array('invoice_type' => 'open'));
                }
            }

            if ($success) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Refund Unapproved successfully"));
            } else {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Could not Unapproved the Refund"));
            }
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Could not Unapproved the Refund"));
        $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
    }


}
