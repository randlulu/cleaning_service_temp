<?php

class Invoices_IndexController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'invoices';
        $this->view->sub_menu = 'invoice';
    }

    /**
     * Items list action
     */
    public function indexAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('invoices'));

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'id');
        $sortingMethod = $this->request->getParam('method', 'desc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());
		$is_first_time = $this->request->getParam('is_first_time');
		$page_number = $this->request->getParam('page_number');

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
        //
        /*$pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];*/


        //
        //load model
        //
        $modelBookingInvoice = new Model_BookingInvoice();
		
		
		

        //
        // get data list
        //
		
		 if ($this->request->isPost()) {
		  if(isset($page_number)){
		   $perPage = 15;
		   $currentPage = $page_number +1;
	      }
          $data = $modelBookingInvoice->getAll($filters, "{$orderBy} {$sortingMethod}", $pager  , 0, $perPage, $currentPage);
          $modelBookingInvoice->fills($data, array('booking', 'contractors', 'customer', 'city', 'labels', 'due_date', 'booking_users', 'have_attachment', 'services','payment'));
		  $result = array();
		  $this->view->data = $data;
		  $this->view->filters = $filters;
		  $this->view->is_first_time = $is_first_time;
		  $result['data'] =  $this->view->render('index/draw-node.phtml');
		  if($data){
			   $result['is_last_request'] = 0;
			  }else{
			   $result['is_last_request'] = 1;
			  }
			  
		  echo json_encode($result);
		  exit;
		}

        //
        // set view params
        //
        
		//$this->view->data = $data;
        //$this->view->currentPage = $currentPage;
        //$this->view->perPage = $pager->perPage;
        //$this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
    }
	
	
	public function duplicatedInvoicesAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('invoices'));
		
		$loggedUser = CheckAuth::getLoggedUser ();
		$modelAuthRole = new Model_AuthRole ();
		$contractorRoleId = $modelAuthRole->getRoleIdByName('contractor');
		if($loggedUser['role_id'] == $contractorRoleId){ 
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission!!"));
			$this->_redirect($this->router->assemble(array(), 'invoices'));
		} else {
           $this->view->sub_menu = 'duplicatedInvoices';
       
			//
			//load model
			//
			$modelBookingInvoice = new Model_BookingInvoice();

			//
			// get data list
			//
			$data = $modelBookingInvoice->getDuplicatedInvoiceNumbers();
			//$modelBookingInvoice->fills($data, array('booking', 'contractors', 'customer', 'city', 'labels', 'due_date', 'booking_users', 'have_attachment', 'services'));


			//
			// set view params
			//
			$this->view->data = $data;
		}
		   
		
		
       
    }

    public function deleteAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('invoiceDelete'));

        //
        // get request parameters
        //
        $invoiceId = $this->request->getParam('id', 0);
        $invoiceIds = $this->request->getParam('ids', array());
        if ($invoiceId) {
            $invoiceIds[] = $invoiceId;
        }

        //
        // load model
        //
        $modelBooking = new Model_Booking();
        $modelBookingInvoice = new Model_BookingInvoice();


        $is_draft = TRUE;
        $tables = array();
        foreach ($invoiceIds as $invoiceId) {
            $invoice = $modelBookingInvoice->getById($invoiceId);
            if ('draft' == $invoice['invoice_type']) {
                if (CheckAuth::checkIfCanHandelAllCompany('invoice', $invoiceId)) {
                    if ($modelBooking->checkIfCanDeleteBooking($invoice['booking_id'])) {
                        //check if the invoice used in other place
                        if ($modelBookingInvoice->checkBeforeDeleteInvoiceByBookingId($invoice['booking_id'])) {
                            $modelBookingInvoice->updateById($invoiceId, array('is_deleted' => 1));
                            $modelBooking->updateById($invoice['booking_id'], array('convert_status' => 'booking'));
                        } else {
                            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Could not be Deleted ,This Invoice used in (" . implode(',', $tables) . ')'));
                        }
                    }
                }
            } else {
                $is_draft = FALSE;
            }
        }

        if ($is_draft) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "The selected invoice(s) have been deleted."));
        } else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Only status of draft invoice(s) is deleted."));
        }

        $this->_redirect($this->router->assemble(array(), 'invoices'));
    }

    public function convertToOpenAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('convertToOpen'));

        //
        // get request parameters
        //
        $invoiceId = $this->request->getParam('id', 0);
        $invoiceIds = $this->request->getParam('ids', array());
        if ($invoiceId) {
            $invoiceIds[] = $invoiceId;
        }

        //
        // load model
        //
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelBooking = new Model_Booking();

        $is_draft = TRUE;
        foreach ($invoiceIds as $invoiceId) {
            $invoice = $modelBookingInvoice->getById($invoiceId);
            if ('draft' == $invoice['invoice_type']) {
                if (CheckAuth::checkIfCanHandelAllCompany('invoice', $invoiceId)) {
                    if ($modelBooking->checkCanEditBookingDetails($invoice['booking_id'])) {
                        $modelBookingInvoice->updateById($invoiceId, array('invoice_type' => 'open'));
                    }
                }
            } else {
                $is_draft = FALSE;
            }
        }


        if ($is_draft) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Status of the invoice(s) has been changed to open."));
        } else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Only status of draft invoice(s) can be changed to open"));
        }

        $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
    }

    public function convertAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('convert'));


        //
        // get request parameters
        //
        $invoiceId = $this->request->getParam('id', 0);
        $type = $this->request->getParam('type', 'draft');

        if (!CheckAuth::checkIfCanHandelAllCompany('invoice', $invoiceId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        if (!in_array($type, array('draft', 'void'))) {
            $type = 'draft';
        }

        //
        // load model
        //
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelBooking = new Model_Booking();
        $modelBookingLog = new Model_BookingLog();

        $invoice = $modelBookingInvoice->getById($invoiceId);

        $no_change = TRUE;
        if ($type != $invoice['invoice_type']) {

            $no_change = FALSE;
            $modelPayment = new Model_Payment();

            $payment = $modelPayment->getAll(array('booking_id' => $invoice['booking_id']));

            $have_payment = FALSE;
            if (empty($payment)) {
                $have_payment = TRUE;
                if ($modelBooking->checkCanEditBookingDetails($invoice['booking_id'])) {

                    // add  data log
                    $modelBookingLog->addBookingLog($invoice['booking_id']);

                    $modelBookingInvoice->updateById($invoiceId, array('invoice_type' => $type));
                }
            }

            if ($have_payment) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Status of the invoice(s) has been changed to {$type}."));
            } else {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This invoice(s) have payment can't be changed to {$type}"));
            }
        }

        if ($no_change) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Status of the invoice(s) is {$type}."));
        }

        $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
    }

	/*
    public function viewAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('invoiceView'));

        //
        // get params 
        //
        $invoiceId = $this->request->getParam('id', 0);


        if (!CheckAuth::checkIfCanHandelAllCompany('invoice', $invoiceId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        //
        // load models
        //
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelBooking = new Model_Booking();
        $modelCustomer = new Model_Customer();
        $modelCustomerType = new Model_CustomerType();

        //
        // geting data
        //
        $invoice = $modelBookingInvoice->getById($invoiceId);
        $this->view->invoice = $invoice;

		$modelPaymant = new Model_Payment();
        $payment = $modelPaymant->getByBookingId($invoice['booking_id']);
        $this->view->payment = $payment;
        //
        // validation
        //
        if (!$invoice) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Invoice not exist 1"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        //check in can see his or assigned invoices
        if (!$modelBookingInvoice->checkIfCanSeeHisOrAssignedInvoice($invoiceId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $booking = $modelBooking->getById($invoice['booking_id']);
        $this->view->booking = $booking;
        $this->view->isAccepted = $modelBooking->checkBookingIfAccepted($invoice['booking_id']);
		
        //address

        $modelBookingAddress = new Model_BookingAddress();
        $bookingAddress = $modelBookingAddress->getByBookingId($invoice['booking_id']);
        $this->view->lineAddress = get_line_address($bookingAddress);


        $customer = $modelCustomer->getById($booking['customer_id']);

        //customerType
        $customerType = $modelCustomerType->getById($customer['customer_type_id']);
        $this->view->customerType = $customerType;

        // customer type work order is_required Message
        $isWorkOrder = false;
        $workOrder = $modelCustomerType->getCustomerTypeIsWorkOrder();

        if (in_array($customer['customer_type_id'], $workOrder)) {

            $modelBookingAttachment = new Model_BookingAttachment();

            $bookingAttachments = $modelBookingAttachment->getByBookingIdOrInquiryId($invoice['booking_id'], $booking['original_inquiry_id']);
            ;
            $isWorkOrder = true;
            if (!empty($bookingAttachments)) {
                foreach ($bookingAttachments as $attachment) {
                    if ($attachment['work_order'] == 1) {
                        $isWorkOrder = false;
                    }
                }
            }
        }

        $this->view->isWorkOrder = $isWorkOrder;
		///////////////By Islam get contractor payment info
		/////////at first we get contractor from Contractor_service_booking by booking_id of the invoice
		$modelContractorServiceBooking=new Model_ContractorServiceBooking();
		$ServiceBookingDetails=$modelContractorServiceBooking->getByBookingId($invoice['booking_id']);
		$contractorId=$ServiceBookingDetails[0]['contractor_id'];
				
		$modelBookingContractorPayment=new Model_BookingContractorPayment();
		$bookingContractorPayment = $modelBookingContractorPayment->getBybookingIdAndContractorId($invoice['booking_id'], $contractorId);
		$this->view->bookingContractorPayment = $bookingContractorPayment;  

		/////////get contractor name from user table
		$modelUser = new Model_User();
        $contractor = $modelUser->getById($contractorId);
		$this->view->contractorName = $contractor['username'];
		$this->view->contractorId = $contractor['user_id'];
		//////////////
        //
        // get Invoice View Param
        //
        $this->getInvoiceViewParam($invoiceId);
		
		
		////get all dates Extra Info
		$modelVisitedExtraInfo = new Model_VisitedExtraInfo();
		$extraInfo = $modelVisitedExtraInfo->getByBookingId2($invoice['booking_id']);
		$this->view->extraInfo = $extraInfo;
				
		$modelContractorInfo = new Model_ContractorInfo();
        $contractorInfo = $modelContractorInfo->getByContractorId($contractorId);
		$this->view->contractorInfo = $contractorInfo;
		//////////////
		
		$modelPayment = new Model_Payment();		
		$cash_amount = $modelPayment->getCashPayments($invoice['booking_id']);

		$this->view->cash_amount = $cash_amount;
		
    }
	*/
	
	public function viewAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('invoiceView'));

        //
        // get params 
        //
        $invoiceId = $this->request->getParam('id', 0);


        if (!CheckAuth::checkIfCanHandelAllCompany('invoice', $invoiceId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        // load models
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelBooking = new Model_Booking();
        $modelCustomer = new Model_Customer();
        $modelCustomerType = new Model_CustomerType();

		//D.A 28/09/2015 Caching invoice view blocks
		require_once 'Zend/Cache.php';	
		$company_id = CheckAuth::getCompanySession();					
		$invoiceViewDir=get_config('cache').'/'.'invoicesView'.'/'.$company_id;
		if (!is_dir($invoiceViewDir)) {
		mkdir($invoiceViewDir, 0777, true);
		}						
		$frontEndOption= array('lifetime'=> NULL,
		'automatic_serialization'=> true);
		$backendOptions = array('cache_dir'=>$invoiceViewDir );
		$cache = Zend_Cache::factory('Core','File',$frontEndOption,$backendOptions);
		
		$cacheID= $invoiceId.'_invoiceDetails';	
		if (($result = $cache->load($cacheID)) === false ) {
        // geting data
        $invoice = $modelBookingInvoice->getById($invoiceId);
        // validation
        if (!$invoice) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Invoice not exist 1"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
		
        //check in can see his or assigned invoices
        if (!$modelBookingInvoice->checkIfCanSeeHisOrAssignedInvoice($invoiceId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $booking = $modelBooking->getById($invoice['booking_id']);
       		
		//address
        $modelBookingAddress = new Model_BookingAddress();
        $bookingAddress = $modelBookingAddress->getByBookingId($invoice['booking_id']);

		$result = array(
						'invoice' =>$invoice,
						'booking' =>$booking,
						'bookingAddress' =>$bookingAddress
						);
		   if($result){
				$cache->save($result,$cacheID);	
			}
		}				
		else{			
			$invoice=$result['invoice'];	
			$booking=$result['booking'];	
			$bookingAddress=$result['bookingAddress'];		
		}	  		
		
        $this->view->invoice = $invoice;
		$this->view->booking = $booking;
	    $this->view->lineAddress = get_line_address($bookingAddress);

		$modelPaymant = new Model_Payment();
        $payment = $modelPaymant->getByBookingId($invoice['booking_id']);
        $this->view->payment = $payment;

        $this->view->isAccepted = $modelBooking->checkBookingIfAccepted($invoice['booking_id']);
		
        $customer = $modelCustomer->getById($booking['customer_id']);
        
		//customerType
        $customerType = $modelCustomerType->getById($customer['customer_type_id']);
        $this->view->customerType = $customerType;

        // customer type work order is_required Message
        $isWorkOrder = false;
        $workOrder = $modelCustomerType->getCustomerTypeIsWorkOrder();

        if (in_array($customer['customer_type_id'], $workOrder)) {

            $modelBookingAttachment = new Model_BookingAttachment();

            $bookingAttachments = $modelBookingAttachment->getByBookingIdOrInquiryId($invoice['booking_id'], $booking['original_inquiry_id']);
            ;
            $isWorkOrder = true;
            if (!empty($bookingAttachments)) {
                foreach ($bookingAttachments as $attachment) {
                    if ($attachment['work_order'] == 1) {
                        $isWorkOrder = false;
                    }
                }
            }
        }
        $this->view->isWorkOrder = $isWorkOrder;
		
		//D.A 28/09/2015 Invoice Technician Update Details cache
        $cacheID= $invoiceId.'_invoicePaymentToTechnician';	
		if (($result = $cache->load($cacheID)) === false ) {
		//By Islam get contractor payment info
		//at first we get contractor from Contractor_service_booking by booking_id of the invoice
		$modelContractorServiceBooking=new Model_ContractorServiceBooking();
		$ServiceBookingDetails=$modelContractorServiceBooking->getByBookingId($invoice['booking_id']);
		$contractorId=$ServiceBookingDetails[0]['contractor_id'];
				
		$modelBookingContractorPayment=new Model_BookingContractorPayment();
		$bookingContractorPayment = $modelBookingContractorPayment->getBybookingIdAndContractorId($invoice['booking_id'], $contractorId);

		//get contractor name from user table
		$modelUser = new Model_User();
        $contractor = $modelUser->getById($contractorId);
		$contractorName = $contractor['username'];
		$contractorId = $contractor['user_id'];
		
		$modelPayment = new Model_Payment();		
		$cash_amount = $modelPayment->getCashPayments($invoice['booking_id']);

		$result = array(
						'bookingContractorPayment' =>$bookingContractorPayment,
						'contractorName' =>$contractorName,
						'contractorId' =>$contractorId,
						'cash_amount' =>$cash_amount
						);
		   if($result){
				$cache->save($result,$cacheID);	
			}
		}				
		else{			
			$bookingContractorPayment=$result['bookingContractorPayment'];	
			$contractorName=$result['contractorName'];	
			$contractorId=$result['contractorId'];
			$cash_amount=$result['cash_amount'];			
		}
		
		$this->view->bookingContractorPayment=$bookingContractorPayment;
		$this->view->contractorName=$contractorName;
		$this->view->contractorId=$contractorId;
		$this->view->cash_amount = $cash_amount;		

		$modelContractorInfo = new Model_ContractorInfo();
        $contractorInfo = $modelContractorInfo->getByContractorId($contractorId);
		$this->view->contractorInfo = $contractorInfo;
		
        // get Invoice View Param
        $this->getInvoiceViewParam($invoiceId);
		
		//D.A 28/09/2015 Invoice Technician Update Details cache
        $cacheID= $invoiceId.'_invoiceTechnicianUpdateDetails';	
		if (($result = $cache->load($cacheID)) === false ) {
		//get all dates Extra Info
		$modelVisitedExtraInfo = new Model_VisitedExtraInfo();
		$extraInfo = $modelVisitedExtraInfo->getByBookingId2($invoice['booking_id']);		
		$result=$extraInfo;
			if($result){
				$cache->save($result,$cacheID);	
				}					
		}				
		else{			
			$extraInfo=$result;		
		}		
		$this->view->extraInfo = $extraInfo;	

		//D.A 13/10/2015		
		$cacheID= $invoiceId.'_invoiceScheduledVisits';	
		if (($result = $cache->load($cacheID)) === false ) {
		$modelBookingMultipleDays = new Model_BookingMultipleDays();
		$primaryDateExtraInfo = $modelBooking->getExtraInfoBookingPrimaryDatesByBooking($invoice['booking_id']);
		$multipleDaysWithVisitedExtraInfo = $modelBookingMultipleDays->getMultipleDaysWithVisitedByBookingId($invoice['booking_id']);
		$result = array(
			'primaryDateExtraInfo' =>$primaryDateExtraInfo,
			'multipleDaysWithVisitedExtraInfo' =>$multipleDaysWithVisitedExtraInfo
		);
		   if($result){
		    	$cache->save($result,$cacheID);	
			}
		}				
		else{			
		$primaryDateExtraInfo=$result['primaryDateExtraInfo'];	
		$multipleDaysWithVisitedExtraInfo=$result['multipleDaysWithVisitedExtraInfo'];	
		}
		
		  $modelImage = new Model_Image();
		  $pager = null ;
		  $photo = $modelImage->getAll($invoice['booking_id'] , 'booking', "i.created desc" , $pager, $filter = array() , $limit = 10);     
		  $photoCount = count($modelImage->getAll($invoice['booking_id'] , 'booking', "i.created desc"));
		  
		$this->view->type = 'booking';
	    $this->view->photo = $photo;
	    $this->view->photoCount = $photoCount; 
	
		$this->view->primaryDateExtraInfo=$primaryDateExtraInfo;
		$this->view->multipleDaysWithVisitedExtraInfo=$multipleDaysWithVisitedExtraInfo;
		
    }
	
    public function previewAction() {
        //
        // get params 
        //
        $invoiceId = $this->request->getParam('id', 0);

        if (!CheckAuth::checkIfCanHandelAllCompany('invoice', $invoiceId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        //
        // load models
        //
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelBooking = new Model_Booking();
        $modelCustomer = new Model_Customer();

        //
        // geting data
        //
        $invoice = $modelBookingInvoice->getById($invoiceId);

        //
        // validation
        //
        if (!$invoice) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Invoice not exist 2"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        //check in can see his or assigned invoices
        if (!$modelBookingInvoice->checkIfCanSeeHisOrAssignedInvoice($invoiceId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $booking = $modelBooking->getById($invoice['booking_id']);
        $this->view->booking = $booking;
        $this->view->isAccepted = $modelBooking->checkBookingIfAccepted($invoice['booking_id']);

        $customer = $modelCustomer->getById($booking['customer_id']);
        $modelCustomer->fill($customer, array('customer_commercial_info', 'customer_contacts'));
        $this->view->customer = $customer;
        $this->view->customerContacts = $customer['customer_contacts'];

        $this->getInvoiceViewParam($invoiceId);

        echo $this->view->render('index/preview.phtml');
        exit;
    }

    public function sendReminderOverdueInvoiceAsEmailAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('sendInvoiceAsEmail'));

        //
        // get params 
        //
        $invoiceId = $this->request->getParam('id', 0);

        if (!CheckAuth::checkIfCanHandelAllCompany('invoice', $invoiceId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // load models
        //
        $modelUser = new Model_User();
        $modelCustomer = new Model_Customer();
        $modelBookingAddress = new Model_BookingAddress();
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelBooking = new Model_Booking();

        //
        // geting data
        //
        $invoice = $modelBookingInvoice->getById($invoiceId);
        $modelBookingInvoice->fill($invoice, array('booking', 'number_of_due_days', 'due_date'));

        //
        // validation
        //
        if (!$invoice) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Invoice not exist 3"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        //check in can see his or assigned invoices
        if (!$modelBookingInvoice->checkIfCanSeeHisOrAssignedInvoice($invoiceId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // filling extra data
        //
        $customer = $modelCustomer->getById($invoice['booking']['customer_id']);
        $modelCustomer->fill($customer, array('customer_commercial_info', 'customer_contacts'));
        $this->view->customerContacts = $customer['customer_contacts'];

        $user = $modelUser->getById($invoice['booking']['created_by']);

        $viewParam = $this->getInvoiceViewParam($invoiceId, true);
        $view = new Zend_View();
        $view->setScriptPath(APPLICATION_PATH . '/modules/invoices/views/scripts/index');
        $view->bookingServices = $viewParam['bookingServices'];
        $view->thisBookingServices = $viewParam['thisBookingServices'];
        $view->priceArray = $viewParam['priceArray'];
        $view->invoice = $viewParam['invoice'];
        $view->customer = $customer;
        $view->isAccepted = $modelBooking->checkBookingIfAccepted($invoice['booking_id']);
        $view->booking = $invoice['booking'];
        $bodyInvoice = $view->render('invoice.phtml');

        $template_params = array(
            //invoice
            '{invoice_num}' => $invoice['invoice_num'],
            '{invoice_created}' => date('d/m/Y', $invoice['created']),
            '{due}' => $invoice['due'] > 1 ? $invoice['due'] . ' Days' : $invoice['due'] . ' Day',
            '{due_date}' => $invoice['due_date'] > 1 ? $invoice['due_date'] . ' Days' : $invoice['due_date'] . ' Day',
            //booking
            '{booking_num}' => $invoice['booking']['booking_num'],
            '{total_without_tax}' => number_format($invoice['booking']['sub_total'], 2),
            '{gst_tax}' => number_format($invoice['booking']['gst'], 2),
            '{total_with_tax}' => number_format($invoice['booking']['qoute'], 2),
            '{paid_amount}' => number_format($invoice['booking']['paid_amount'], 2),
            '{unpaid_amount}' => number_format($invoice['booking']['qoute'] - $invoice['booking']['paid_amount'], 2),
            '{description}' => $invoice['booking']['description'] ? $invoice['booking']['description'] : '',
            '{booking_created}' => date('d/m/Y', $invoice['booking']['created']),
            '{booking_created_by}' => ucwords($user['username']),
            '{booking_start}' => date("F j, Y, g:i a", strtotime($invoice['booking']['booking_start'])),
            '{booking_address}' => get_line_address($modelBookingAddress->getByBookingId($invoice['booking']['booking_id'])),
            '{service}' => nl2br($modelContractorServiceBooking->getBookingAsText($invoice['booking']['booking_id'], true)),
            '{invoice_view}' => $bodyInvoice,
            //customer
            '{customer_name}' => get_customer_name($customer),
            '{customer_first_name}' => isset($customer['first_name']) && $customer['first_name'] ? ucwords($customer['first_name']) : '',
            '{customer_last_name}' => isset($customer['last_name']) && $customer['last_name'] ? ' ' . ucwords($customer['last_name']) : '',
            '{customer_contacts}' => nl2br($modelCustomer->getCustomerContacts($invoice['booking']['customer_id'])),
        );

        $modelEmailTemplate = new Model_EmailTemplate();
        $emailTemplate = $modelEmailTemplate->getEmailTemplate('reminder_overdue_invoice', $template_params);

        $body = $emailTemplate['body'];
        $subject = $emailTemplate['subject'];
        $to = array();
        if ($customer['email1']) {
            $to[] = $customer['email1'];
        }
        if ($customer['email2']) {
            $to[] = $customer['email2'];
        }
        if ($customer['email2']) {
            $to[] = $customer['email3'];
        }
        $to = implode(',', $to);

        if ($this->request->isPost()) {
            $to = $this->request->getParam('to');
            $cc = $this->request->getParam('cc');
            $subject = $this->request->getParam('subject');
            $body = $this->request->getParam('body');
            $pdf_attachment = $this->request->getParam('pdf_attachment', 0);
			
			$trading_namesObj = new Model_TradingName();
			$trading_names = $trading_namesObj->getById($invoice['booking']['trading_name_id']);
		
            $params = array(
                'to' => $to,
                'cc' => $cc,
                'body' => $body,
                'subject' => $subject,
				'trading_name' => $trading_names['trading_name'],
				'from' => $trading_names['email'],	
            );

            $error_mesages = array();
            if (EmailNotification::validation($params, $error_mesages)) {

                if (!empty($pdf_attachment)) {
                    // Create pdf
                    $pdfPath = createPdfPath();
                    $destination = $pdfPath['fullDir'] . $viewParam['invoice']['invoice_num'] . '.pdf';
                    wkhtmltopdf($bodyInvoice, $destination);
                    $params['attachment'] = $destination;
                }

                // Send Email
                $success = EmailNotification::sendEmail($params, '', array(), array('reference_id' => $invoice['id'], 'type' => 'invoice'));

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Reminder Overdue Invoice sent successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Could not send the Reminder Overdue Invoice"));
                }

                echo 1;
                exit;
            }
            $this->view->error_mesages = $error_mesages;
        }

        $this->view->invoice = $invoice;
        $this->view->to = $to;
        $this->view->subject = $subject;
        $this->view->body = $body;
        $this->view->cc = isset($cc) ? $cc : '';

        echo $this->view->render('index/send-reminder-overdue-invoice-as-email.phtml');
        exit;
    }

    public function sendInvoiceAsEmailAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('sendInvoiceAsEmail'));

        //
        // get params 
        //
        $invoiceId = $this->request->getParam('id', 0);

        if (!CheckAuth::checkIfCanHandelAllCompany('invoice', $invoiceId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // load models
        //
        $modelUser = new Model_User();
        $modelBooking = new Model_Booking();
        $modelCustomer = new Model_Customer();
        $modelBookingAddress = new Model_BookingAddress();
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();

        //
        // geting data
        //
        $invoice = $modelBookingInvoice->getById($invoiceId);
        $modelBookingInvoice->fill($invoice, array('booking'));

        //
        // validation
        //
        if (!$invoice) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Invoice not exist 5"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        //check in can see his or assigned invoices
        if (!$modelBookingInvoice->checkIfCanSeeHisOrAssignedInvoice($invoiceId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // filling extra data
        //
        $customer = $modelCustomer->getById($invoice['booking']['customer_id']);
        $modelCustomer->fill($customer, array('customer_commercial_info', 'customer_contacts'));
        $this->view->customerContacts = $customer['customer_contacts'];

        $user = $modelUser->getById($invoice['booking']['created_by']);

        $viewParam = $this->getInvoiceViewParam($invoiceId, true);
        $view = new Zend_View();
        $view->setScriptPath(APPLICATION_PATH . '/modules/invoices/views/scripts/index');
        $view->bookingServices = $viewParam['bookingServices'];
        $view->thisBookingServices = $viewParam['thisBookingServices'];
        $view->priceArray = $viewParam['priceArray'];
        $view->invoice = $viewParam['invoice'];
        $view->customer = $customer;
        $view->isAccepted = $modelBooking->checkBookingIfAccepted($invoice['booking_id']);
        $view->booking = $invoice['booking'];
        $bodyInvoice = $view->render('invoice.phtml');

        $template_params = array(
            //invoice
            '{invoice_num}' => $invoice['invoice_num'],
            '{invoice_created}' => date('d/m/Y', $invoice['created']),
            //booking
            '{booking_num}' => $invoice['booking']['booking_num'],
            '{total_without_tax}' => number_format($invoice['booking']['sub_total'], 2),
            '{gst_tax}' => number_format($invoice['booking']['gst'], 2),
            '{total_with_tax}' => number_format($invoice['booking']['qoute'], 2),
            '{description}' => $invoice['booking']['description'] ? $invoice['booking']['description'] : '',
            '{booking_created}' => date('d/m/Y', $invoice['booking']['created']),
            '{booking_created_by}' => ucwords($user['username']),
            '{booking_start}' => date("F j, Y, g:i a", strtotime($invoice['booking']['booking_start'])),
            '{booking_address}' => get_line_address($modelBookingAddress->getByBookingId($invoice['booking']['booking_id'])),
            '{service}' => nl2br($modelContractorServiceBooking->getBookingAsText($invoice['booking']['booking_id'], true)),
            '{invoice_view}' => $bodyInvoice,
            //customer
            '{customer_name}' => get_customer_name($customer),
            '{customer_first_name}' => isset($customer['first_name']) && $customer['first_name'] ? ucwords($customer['first_name']) : '',
            '{customer_last_name}' => isset($customer['last_name']) && $customer['last_name'] ? ' ' . ucwords($customer['last_name']) : '',
            '{customer_contacts}' => nl2br($modelCustomer->getCustomerContacts($invoice['booking']['customer_id'])),
        );

        $modelEmailTemplate = new Model_EmailTemplate();
        $emailTemplate = $modelEmailTemplate->getEmailTemplate('send_invoice_as_email', $template_params);

        $body = $emailTemplate['body'];
        $subject = $emailTemplate['subject'];
        $to = array();
        if ($customer['email1']) {
            $to[] = $customer['email1'];
        }
        if ($customer['email2']) {
            $to[] = $customer['email2'];
        }
        if ($customer['email2']) {
            $to[] = $customer['email3'];
        }
        $to = implode(',', $to);

        if ($this->request->isPost()) {
            $to = $this->request->getParam('to');
            $cc = $this->request->getParam('cc');
            $subject = $this->request->getParam('subject');
            $body = $this->request->getParam('body');
            $pdf_attachment = $this->request->getParam('pdf_attachment', 0);
			$trading_namesObj = new Model_TradingName();
			$trading_names = $trading_namesObj->getById($invoice['booking']['trading_name_id']);
		
			
            $params = array(
                'to' => $to,
                'cc' => $cc,
                'body' => $body,
                'subject' => $subject,
				'trading_name' => $trading_names['trading_name'],
				'from' => $trading_names['email'],	

            );

            $error_mesages = array();
            if (EmailNotification::validation($params, $error_mesages)) {

                if (!empty($pdf_attachment)) {
                    // Create pdf
                    $pdfPath = createPdfPath();
                    $destination = $pdfPath['fullDir'] . $viewParam['invoice']['invoice_num'] . '.pdf';
                    wkhtmltopdf($bodyInvoice, $destination);
                    $params['attachment'] = $destination;
                }

                // Send Email
                $success = EmailNotification::sendEmail($params, '', array(), array('reference_id' => $invoice['id'], 'type' => 'invoice'));

                if ($success) {
                    MobileNotification::notify($invoice['booking']['booking_id'] , 'invoice email to client');
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Invoice sent successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Could not send the Invoice"));
                }

                echo 1;
                exit;
            }
            $this->view->error_mesages = $error_mesages;
        }

        $this->view->invoice = $invoice;
        $this->view->to = $to;
        $this->view->subject = $subject;
        $this->view->body = $body;
        $this->view->cc = isset($cc) ? $cc : '';

        echo $this->view->render('index/send-invoice-as-email.phtml');
        exit;
    }

    public function downloadInvoiceAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('downloadInvoice'));

        //
        // get params 
        //
        $invoiceId = $this->request->getParam('id', 0);

        if (!CheckAuth::checkIfCanHandelAllCompany('invoice', $invoiceId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        //
        // load models
        //
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelBooking = new Model_Booking();
        $modelCustomer = new Model_Customer();

        //
        // geting data
        //
        $invoice = $modelBookingInvoice->getById($invoiceId);

        //
        // validation
        //
        if (!$invoice) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Invoice not exist 6"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        //check in can see his or assigned invoices
        if (!$modelBookingInvoice->checkIfCanSeeHisOrAssignedInvoice($invoiceId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $booking = $modelBooking->getById($invoice['booking_id']);
        $customer = $modelCustomer->getById($booking['customer_id']);
		
		/////Islam get customer_commercial_info
		$modelCustomer = new Model_Customer();
		$modelCustomer->fill($customer, array('customer_commercial_info', 'customer_contacts'));

/////////////////////*/
        $viewParam = $this->getInvoiceViewParam($invoiceId, true);

        $view = new Zend_View();
        $view->setScriptPath(APPLICATION_PATH . '/modules/invoices/views/scripts/index');
        $view->bookingServices = $viewParam['bookingServices'];
        $view->thisBookingServices = $viewParam['thisBookingServices'];
        $view->priceArray = $viewParam['priceArray'];
        $view->invoice = $viewParam['invoice'];
        $view->customer = $customer;
        $view->isAccepted = $modelBooking->checkBookingIfAccepted($invoice['booking_id']);
        $view->booking = $booking;
		//$view->businessName=$customer['customer_commercial_info']['business_name'];
		

        $html = $view->render('invoice.phtml');

        $pdfPath = createPdfPath();

        $filename = $pdfPath['fullDir'] . $viewParam['invoice']['invoice_num'] . '.pdf';
        wkhtmltopdf($html, $filename);

        header("Pragma: public");
        header("Expires: 0");
        header("Pragma: no-cache");
        header("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header('Content-disposition: attachment; filename=' . basename($filename));
        header("Content-Type: application/pdf");
        header("Content-Transfer-Encoding: binary");
        header('Content-Length: ' . filesize($filename));
        @readfile($filename);
        exit(0);
    }

    public function convertBookingToInvoiceAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('convertBookingToInvoice'));

        //
        // get request parameters
        //
        $bookingId = $this->request->getParam('id', 0);

        if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $modelBooking = new Model_Booking();
        $modelBookingStatus = new Model_BookingStatus();
        $modelBookingInvoice = new Model_BookingInvoice();

        $success = false;
        $booking = $modelBooking->getById($bookingId);
		$convertStatus= $booking['convert_status'];
        $bookingInvoice = $modelBookingInvoice->getByBookingId($bookingId);
        $booking = $modelBooking->fill($booking, array('is_accepted'));

        if (!empty($bookingInvoice) && $convertStatus =='invoice' ) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "There exists Invoice to this Booking"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        if (!$booking['is_accepted']) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect($this->router->assemble(array(), 'booking'));
        }

        if ($modelBookingStatus->checkConvertToInvoice($booking['status_id'])) {
            
            $success = $modelBookingInvoice->convertToInvoice($bookingId);
            
        }

        if ($success) {
            //D.A 27/08/2015 Remove Booking Cache
			require_once 'Zend/Cache.php';
			$bookingDetailsCacheID= $bookingId.'_bookingDetails';
			$company_id = CheckAuth::getCompanySession();
			$bookingViewDir=get_config('cache').'/'.'bookingsView'.'/'.$company_id;
			if (!is_dir($bookingViewDir)) {
				mkdir($bookingViewDir, 0777, true);
			}								
			$frontEndOption= array('lifetime'=> NULL,
			'automatic_serialization'=> true);
			$backendOptions = array('cache_dir'=>$bookingViewDir );
			$cache = Zend_Cache::factory('Core','File',$frontEndOption,$backendOptions);
			$cache->remove($bookingDetailsCacheID);	
			
			//D.A 10/09/2015 Remove inquiry Details Cache
			$inquiryDetailsCacheID= $booking['original_inquiry_id'].'_inquiryDetails';							
			$inquiryViewDir=get_config('cache').'/'.'inquiriesView'.'/'.$company_id;
			if (!is_dir($inquiryViewDir)) {
			mkdir($inquiryViewDir, 0777, true);
			}						
			$inquiryDetailsFrontEndOption= array('lifetime'=> NULL,
			'automatic_serialization'=> true);
			$inquiryDetailsBackendOptions = array('cache_dir'=>$inquiryViewDir );
			$inquiryDetailsCache = Zend_Cache::factory('Core','File',$inquiryDetailsFrontEndOption,$inquiryDetailsBackendOptions);
			$inquiryDetailsCache->remove($inquiryDetailsCacheID);	
			 
            $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "convert successfully."));
            $this->_redirect($this->router->assemble(array(), 'invoices'));
        } else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Only status (COMPLETED, IN PROGRESS, FAILD) can be convert to Invoice"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
    }

	/*
    public function getInvoiceViewParam($invoiceId, $toBuffer = false) {

        //
        // load model
        //
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelContractorServiceBookingTemp = new Model_ContractorServiceBookingTemp();
        $modelBooking = new Model_Booking();

        //
        // geting data
        //
        $invoice = $modelBookingInvoice->getById($invoiceId);


        //
        // validation
        //
        if (!$invoice) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Invoice not exist 7"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // filling extra data
        //
        $modelBookingInvoice->fill($invoice, array('booking', 'labels', 'due_date', 'multiple_days'));

        // $thisBookingServices : to put all service for this booking 
        $thisBookingServices = array();

        // $priceArray : to put all price and service for this booking 
        $priceArray = array();


        
         //$bookingServices : to put all booking service from  original booking service (or from temp if the contractor change values untill approved)
 

        //$bookingServices = $modelContractorServiceBooking->getByBookingId($invoice['booking_id']);

        $contractorServiceBookings = $modelContractorServiceBooking->getByBookingId($invoice['booking_id']);
        $contractorServiceBookingsTemp = $modelContractorServiceBookingTemp->getByBookingId($invoice['booking_id']);
        $isTemp = false;
        $bookingServices = array();
        if ($contractorServiceBookingsTemp && !$modelBooking->checkCanEditBookingDetails($invoice['booking_id']) && !$toBuffer) {
            $bookingServices = $contractorServiceBookingsTemp;
            $isTemp = true;
            foreach ($bookingServices as $bookingService) {

                $serviceId = $bookingService['service_id'];
                $clone = $bookingService['clone'];
                $bookingId = $bookingService['booking_id'];

                $service_and_clone = $serviceId . ($clone ? '_' . $clone : '');

                $thisBookingServices[] = $service_and_clone;

                $priceArray[$service_and_clone] = $modelContractorServiceBookingTemp->getTotalServiceBookingQoute($bookingId, $serviceId, $clone);
            }
        } elseif ($contractorServiceBookings) {
            $bookingServices = $contractorServiceBookings;
            foreach ($bookingServices as $bookingService) {

                $serviceId = $bookingService['service_id'];
                $clone = $bookingService['clone'];
                $bookingId = $bookingService['booking_id'];

                $service_and_clone = $serviceId . ($clone ? '_' . $clone : '');

                $thisBookingServices[] = $service_and_clone;

                $priceArray[$service_and_clone] = $modelContractorServiceBooking->getTotalServiceBookingQoute($bookingId, $serviceId, $clone);
            }
        }

        if (!$toBuffer) {

            $this->view->invoice = $invoice;
            $this->view->bookingServices = $bookingServices;
            $this->view->thisBookingServices = $thisBookingServices;
            $this->view->priceArray = $priceArray;
            $this->view->isTemp = $isTemp;
        } else {
            $viewParam = array();

            $viewParam['invoice'] = $invoice;

            $viewParam['bookingServices'] = $bookingServices;
            $viewParam['thisBookingServices'] = $thisBookingServices;
            $viewParam['priceArray'] = $priceArray;
            //$viewParam['isTemp'] = $isTemp;

            return $viewParam;
        }

        return false;
    }
	*/

		public function getInvoiceViewParam($invoiceId, $toBuffer = false) {

        //
        // load model
        //
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelContractorServiceBookingTemp = new Model_ContractorServiceBookingTemp();
        $modelBooking = new Model_Booking();

		//D.A 28/09/2015 Caching invoice view blocks
		require_once 'Zend/Cache.php';	
		$company_id = CheckAuth::getCompanySession();					
		$invoiceViewDir=get_config('cache').'/'.'invoicesView'.'/'.$company_id;
		if (!is_dir($invoiceViewDir)) {
		mkdir($invoiceViewDir, 0777, true);
		}						
		$frontEndOption= array('lifetime'=> NULL,
		'automatic_serialization'=> true);
		$backendOptions = array('cache_dir'=>$invoiceViewDir );
		$cache = Zend_Cache::factory('Core','File',$frontEndOption,$backendOptions);
		
		//D.A 28/09/2015 invoice Parameters Cache     
		$cacheID= $invoiceId.'_invoiceParams';	
		if (($result = $cache->load($cacheID)) === false ) {
			// geting data
			$invoice = $modelBookingInvoice->getById($invoiceId);

			// validation
			if (!$invoice) {
				$this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Invoice not exist 7"));
				$this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
			}

			// filling extra data
			$modelBookingInvoice->fill($invoice, array('booking', 'labels', 'due_date', 'multiple_days'));
			$result=$invoice;
			if($result){
				$cache->save($result,$cacheID);	
			}					
		}				
		else{			
			$invoice=$result;		
		}	
		
        //D.A 28/09/2015 invoice Services Cache     
		$cacheID= $invoiceId.'_invoiceServices';	
		if (($result = $cache->load($cacheID)) === false ) {
			// $thisBookingServices : to put all service for this booking 
			$thisBookingServices = array();			
			$priceArray = array();
			$contractorServiceBookings = $modelContractorServiceBooking->getByBookingId($invoice['booking_id']);
			$contractorServiceBookingsTemp = $modelContractorServiceBookingTemp->getByBookingId($invoice['booking_id']);
			$isTemp = false;
			$bookingServices = array();
			if ($contractorServiceBookingsTemp && !$modelBooking->checkCanEditBookingDetails($invoice['booking_id']) && !$toBuffer) {
				$bookingServices = $contractorServiceBookingsTemp;
				$isTemp = true;
				foreach ($bookingServices as $bookingService) {

					$serviceId = $bookingService['service_id'];
					$clone = $bookingService['clone'];
					$bookingId = $bookingService['booking_id'];

					$service_and_clone = $serviceId . ($clone ? '_' . $clone : '');

					$thisBookingServices[] = $service_and_clone;

					$priceArray[$service_and_clone] = $modelContractorServiceBookingTemp->getTotalServiceBookingQoute($bookingId, $serviceId, $clone);
				}
			} elseif ($contractorServiceBookings) {
				$bookingServices = $contractorServiceBookings;
				foreach ($bookingServices as $bookingService) {

					$serviceId = $bookingService['service_id'];
					$clone = $bookingService['clone'];
					$bookingId = $bookingService['booking_id'];

					$service_and_clone = $serviceId . ($clone ? '_' . $clone : '');

					$thisBookingServices[] = $service_and_clone;

					$priceArray[$service_and_clone] = $modelContractorServiceBooking->getTotalServiceBookingQoute($bookingId, $serviceId, $clone);
				}
			}
			
			$result = array(
						'bookingServices' =>$bookingServices,
						'thisBookingServices' =>$thisBookingServices,
						'priceArray' =>$priceArray,
						'isTemp' =>$isTemp					
						);
		   if($result){
				$cache->save($result,$cacheID);	
			}
		}				
		else{			
			$bookingServices=$result['bookingServices'];	
			$thisBookingServices=$result['thisBookingServices'];	
			$priceArray=$result['priceArray'];	
			$isTemp=$result['isTemp'];			
		}

        if (!$toBuffer) {
            $this->view->invoice = $invoice;
            $this->view->bookingServices = $bookingServices;
            $this->view->thisBookingServices = $thisBookingServices;
            $this->view->priceArray = $priceArray;
            $this->view->isTemp = $isTemp;
        } else {
            $viewParam = array();
            $viewParam['invoice'] = $invoice;
            $viewParam['bookingServices'] = $bookingServices;
            $viewParam['thisBookingServices'] = $thisBookingServices;
            $viewParam['priceArray'] = $priceArray;
			
            return $viewParam;
        }

        return false;
    }
	
	//D.A 29/09/2015 clear invoice cache
	public function invoiceCacheClearAction() {
		
        //get request parameters
        $invoiceId = $this->request->getParam('id');

		require_once 'Zend/Cache.php';
		$company_id = CheckAuth::getCompanySession();		
		$invoiceViewDir=get_config('cache').'/'.'invoicesView'.'/'.$company_id;								
		$frontEndOption= array('lifetime'=> NULL,
		'automatic_serialization'=> true);
		$backendOptions = array('cache_dir'=>$invoiceViewDir );
		$Cache = Zend_Cache::factory('Core','File',$frontEndOption,$backendOptions);	
		$Cache->remove($invoiceId.'_invoiceDetails');
		$Cache->remove($invoiceId.'_invoicePaymentToTechnician');
		$Cache->remove($invoiceId.'_invoiceParams');
		$Cache->remove($invoiceId.'_invoiceTechnicianUpdateDetails');
		$Cache->remove($invoiceId.'_invoiceServices');
		
		$this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Cache Cleared Successfully"));
		$this->_redirect($this->router->assemble(array('id' => $invoiceId), 'invoiceView'));	
    }
	
    public function setDueDateAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('invoices', 'setDueDate'));

        //
        // get params
        //
        $bookingId = $this->request->getParam('booking_id');
        $dueDateId = $this->request->getParam('due_date_id');


        if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        //
        // load model
        //
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelBookingDueDate = new Model_BookingDueDate();


        $bookingDueDate = $modelBookingDueDate->getByBookingId($bookingId);

        $options = array('booking_id' => $bookingId);

        if ($bookingDueDate) {
            $options['due_date_id'] = $bookingDueDate['due_date_id'];
        }

        $form = new Invoices_Form_DueDate($options);


        if ($this->request->isPost()) {
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array('due_date_id' => $dueDateId);

                if ($bookingDueDate) {
                    $modelBookingDueDate->updateById($bookingDueDate['id'], $data);
                } else {
                    $data['booking_id'] = $bookingId;
                    $modelBookingDueDate->insert($data);
                }

                //Change To Overdue
                $modelBookingInvoice->cronJobChangeToOverdue();

                //Change To Open
                $modelBookingInvoice->cronJobChangeToOpen();


                echo 1;
                exit;
            }
        }
        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('index/set-due-date.phtml');
        exit;
    }

    public function editInvoiceNumberAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('editInvoiceNumber'));

        //
        // get params
        //
        $invoiceId = $this->request->getParam('id');
        $invoice_number = $this->request->getParam('invoice_number');

        //
        // load model
        //
        
        $modelBookingLog = new Model_BookingLog();
        $modelBookingInvoice = new Model_BookingInvoice();
        $invoice = $modelBookingInvoice->getById($invoiceId);
        $modelBooking = new Model_Booking();
        if (!$invoice) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Invoice not exist 8"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        if (!CheckAuth::checkIfCanHandelAllCompany('invoice', $invoiceId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }


        if (!$this->request->isPost()) {
            $invoice_number = $invoice['invoice_num'];
        }
        $form = new Invoices_Form_EditInvoiceNumber(array('invoice_id' => $invoiceId, 'invoice_number' => $invoice_number));

        if ($this->request->isPost()) {
            if ($form->isValid($this->request->getPost())) { // validate form data
                if ((!$modelBooking->checkCanEditBookingDetails($invoice['booking_id'])) && ($invoice['invoice_num'] != $invoice_number )) {
                    $modelBooking->fillBookingAndServicesFieldTemp($invoice['booking_id']);
                    $modelBooking->updateById($invoice['booking_id'], array('is_change' => 1));
                    $dbParams = array(
                        'invoice_num' => $invoice_number,
                        'invoice_num_temp' => $invoice['invoice_num']
                    );
                } else {
                    $dbParams = array(
                        'invoice_num' => $invoice_number
                    );
                }

                // add  data log
                $modelBookingLog->addBookingLog($invoice['booking_id']);

                $modelBookingInvoice->updateById($invoiceId, $dbParams);

                echo json_encode(array('invoiceId' => $invoiceId, 'invoice_number' => $invoice_number));
                exit;
            }
        }
        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('index/edit-invoice-number.phtml');
        exit;
    }
	
	
	
	public function checkDuplicateInvoiceAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('editInvoiceNumber'));

        //
        // get params
        //
        $invoiceId = $this->request->getParam('id');
        $invoice_number = $this->request->getParam('invoice_number');

        //
        // load model
        //
        $modelBookingInvoice = new Model_BookingInvoice();
        $invoices = $modelBookingInvoice->getDuplicatedInvoiceByInvoiceNumber($invoice_number);
        $is_duplicate = 0;
		
        if (!empty($invoices)) {
			$is_duplicate = 1;
            
        }
		echo json_encode(array('is_duplicate' => $is_duplicate, 'invoice_number' => $invoice_number));
        exit;
        
     
        
    }
	
	
	public function advanceSearchAction() {
			
				echo $this->view->render('index/filters.phtml');
			exit;
			}

}