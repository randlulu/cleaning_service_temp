<?php

class Invoices_LabelController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
    }

    public function selectLabelAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('invoiceLabel'));

        $invoiceId = $this->request->getParam('id');
        $label_ids = $this->request->getParam('label_ids', array());

        //
        //load model
        //
        $modelLabel = new Model_Label();
        $modelInvoiceLabel = new Model_InvoiceLabel();
        $invoiceLabels = $modelInvoiceLabel->getByInvoiceId($invoiceId);

        $labelIds = array();
        if ($invoiceLabels) {
            foreach ($invoiceLabels as $invoiceLabel) {
                $labelIds[] = $invoiceLabel['label_id'];
            }
        }

        //
        //get data
        //
        $labels = $modelLabel->getAll();

        $this->view->labels = $labels;

        if ($this->request->isPost()) {
            $modelInvoiceLabel->setLabelsToInvoice($invoiceId, $label_ids);

            if (!empty($label_ids)) {
                foreach ($label_ids as $label_id) {
                    $label = $modelLabel->getById($label_id);
                    $json_labels[$label_id] = $label['label_name'];
                }
                echo json_encode(array('result' => 1, 'labels' => $json_labels, 'entityId' => $invoiceId));
            } else {
                echo json_encode(array('result' => 0, 'entityId' => $invoiceId));
            }
            exit;
        }

        $this->view->invoiceId = $invoiceId;
        $this->view->label_ids = $labelIds;

        echo $this->view->render('label/select-label.phtml');
        exit;
    }

    public function searchLabelAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('invoiceLabel'));

        $modelLabel = new Model_Label();

        $filters['keywords'] = $this->request->getParam('label-like');
        $labels = $modelLabel->getAll($filters);
        $this->view->labels = $labels;

        echo $this->view->render('label/search-label.phtml');
        exit;
    }

    public function filterLabelAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('invoiceFilterLabel'));

        $label_ids = $this->request->getParam('label_ids', array());

        //
        //load model
        //
        $modelLabel = new Model_Label();

        $labels = $modelLabel->getAll();
        $this->view->labels = $labels;

        if ($this->request->isPost()) {

            $fltrLabels = array();
            if ($label_ids) {
                foreach ($label_ids as $label_id) {
                    $fltrLabels[] = 'fltr[invoice_label_ids][]=' . $label_id;
                }
            }

            $fltr = implode('&', $fltrLabels);

            echo $this->router->assemble(array(), 'invoices') . ($fltr ? '?' . $fltr : '');
            exit;
        }

        echo $this->view->render('label/filter-label.phtml');
        exit;
    }

    public function addLabelAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('invoiceAddLabel'));

        //
        // get request parameters
        //
        $labelName = $this->request->getParam('label_name', '');
        $invoiceId = $this->request->getParam('id');

        //
        //load model
        //
        $modelLabel = new Model_Label();
        $modelInvoiceLabel = new Model_InvoiceLabel();

        //
        // init action form
        //
        $form = new Invoices_Form_Label();


        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'label_name' => $labelName,
                    'company_id' => CheckAuth::getCompanySession()
                );

                $labelId = $modelLabel->insert($data);

                $params = array(
                    'invoice_id' => $invoiceId,
                    'label_id' => $labelId
                );
                $modelInvoiceLabel->assignLabelToInvoice($params);

                $invoiceLabels = $modelInvoiceLabel->getByInvoiceId($invoiceId);
                foreach ($invoiceLabels AS $invoiceLabel) {
                    $label = $modelLabel->getById($invoiceLabel['label_id']);
                    $json_labels[$invoiceLabel['label_id']] = $label['label_name'];
                }
                echo json_encode(array('result' => 1, 'labels' => $json_labels, 'entityId' => $invoiceId));
                exit;
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('label/add_edit.phtml');
        exit;
    }

}

