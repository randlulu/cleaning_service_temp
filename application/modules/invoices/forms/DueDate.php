<?php

class Invoices_Form_DueDate extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $due_date_id = isset($options['due_date_id']) ? $options['due_date_id'] : '';
        $booking_id = isset($options['booking_id']) ? $options['booking_id'] : '';

        $this->setName('DueDate');

        $router = Zend_Controller_Front::getInstance()->getRouter();

        $dueDateId = new Zend_Form_Element_Select('due_date_id');
        $dueDateId->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'select_field form-control'))
                ->setValue($due_date_id)
                ->setErrorMessages(array('Required' => 'Please select Due Date.'));


        $modelDueDate = new Model_DueDate();
        $allDueDate = $modelDueDate->getDueDateAsArray();

        $dueDateId->addMultiOption('', 'Select One');
        $dueDateId->addMultiOptions($allDueDate);



        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));


        $this->addElements(array($dueDateId, $button));

        $this->setMethod('post');

        $this->setAction($router->assemble(array('booking_id' => $booking_id), 'setDueDate'));
    }

}