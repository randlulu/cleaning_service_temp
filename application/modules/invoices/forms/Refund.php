<?php

class Invoices_Form_Refund extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

		
        $this->setName('Refund');
		$dateTimeObj= get_settings_date_format();
		if(!empty($dateTimeObj) && isset($options['booking_end']))
		$booking_end_value=getNewDateFormat((int)strtotime($options['booking_end']));
		else{
		if(isset($options['booking_end']))
		$booking_end_value=date("d M Y", strtotime($options['booking_end']));
		}
        $booking_id = (isset($options['booking_id']) ? $options['booking_id'] : '');
        $booking_amount = (isset($options['amount']) ? $options['amount'] : 0);
        //$booking_end = (isset($options['booking_end']) ? date("d M Y", strtotime($options['booking_end'])) : '');
        $booking_end = (isset($options['booking_end']) ? $booking_end_value : '');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $refund = (isset($options['refund']) ? $options['refund'] : '');
        $booking = (isset($options['booking']) ? $options['booking'] : '');
        $bookingInvoice = (isset($options['bookingInvoice']) ? $options['bookingInvoice'] : '');
        $paymentData = (isset($options['paymentData']) ? $options['paymentData'] : '');            
        $avalibleRefund = (isset($options['avalibleRefund']) ? $options['avalibleRefund'] : 0);
        $refundedToDate = (isset($options['refundedToDate']) ? $options['refundedToDate'] : 0);


        // var_dump($booking);
        // echo '<br>' . "invoice_num:" . $bookingInvoice['invoice_num'];

		$contractorIdVal = (isset($options['contractor_id']) ? $options['contractor_id'] : '');
        if(!empty($dateTimeObj)&& !empty($refund)){
		$received_date_value=getNewDateFormat($refund['received_date']);
		}
        else if(!empty($refund) && empty($dateTimeObj)){
		$received_date_value=date("d M Y", $refund['received_date']);
		}
		else if(empty($dateTimeObj)&& empty($refund))
		$received_date_value=date("d M Y", time());
	    else
		$received_date_value=getNewDateFormat(time());
	
/////////////////////empty($paymentData['received_date'])
        if(!empty($dateTimeObj)&& !empty($paymentData['received_date']))
		{
		$paymentDataValue=getNewDateFormat($paymentData['received_date']);
		}
        if(!empty($paymentData['received_date']) && empty($dateTimeObj)){
		$paymentDataValue=date("d M Y", $paymentData['received_date']);
		}
		
        $router = Zend_Controller_Front::getInstance()->getRouter();
        $request = Zend_Controller_Front::getInstance()->getRequest();
        //getNewDateFormat
        $received_date = new Zend_Form_Element_Text('received_date');
        $received_date->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
               //->setValue(empty($refund) ? date("d M Y", time()) : date("d M Y", $refund['received_date']))
				//->setValue(empty($refund) && !empty($dateTimeObj) ? getNewDateFormat(time()) : getNewDateFormat($refund['received_date']))
                //->setValue(empty($refund) && empty($dateTimeObj)  ? date("d M Y", time()) : date("d M Y", $refund['received_date']))
                ->setValue($received_date_value)
				->setAttribs(array('class' => 'text_field form-control'/*, 'readonly' => 'readonly'*/ , 'id' => 'received_date'))
                ->setErrorMessages(array('Required' => 'Please enter the received date'));

        $bank_charges = new Zend_Form_Element_Text('bank_charges');
        $bank_charges->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setValue((!empty($refund['bank_charges']) ? $refund['bank_charges'] : ''))
                ->setAttribs(array('class' => 'text_field'));

        $amount_withheld = new Zend_Form_Element_Text('amount_withheld');
        $amount_withheld->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setValue((!empty($refund['amount_withheld']) ? $refund['amount_withheld'] : ''))
                ->setAttribs(array('class' => 'text_field'));

        $reference = new Zend_Form_Element_Text('reference');
        $reference->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                 ->setAttribs(array('class' => 'text_field form-control'))
                ->setValue((!empty($refund['reference']) ? $refund['reference'] : ''));               
                if('eway_refund' != $mode){
                $reference->setRequired();
                }


        $withholding_tax = new Zend_Form_Element_Checkbox('withholding_tax');
        $withholding_tax->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setValue((!empty($refund['withholding_tax']) ? $refund['withholding_tax'] : ''))
                ->setAttribs(array('class' => 'checkbox_field', 'onclick' => "$('#amount_withheld_block').toggle();"));


        $is_acknowledgment = new Zend_Form_Element_Checkbox('is_acknowledgment');
        $is_acknowledgment->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setValue((!empty($refund['is_acknowledgment']) ? $refund['is_acknowledgment'] : ''))
                ->setAttribs(array('class' => 'checkbox_field'));

        $amount = new Zend_Form_Element_Text('amount');
        $amount->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'text_field form-control'))
                ->setRequired()
                ->addErrorMessage('Error in the Refund, it must not exceed $' . number_format($booking_amount, 2, '.', ''))
                ->addValidator('LessThan', FALSE, array(number_format($booking_amount, 2, '.', '') + 0.01));
        if ('update' == $mode) {
            $amount->setValue(number_format($refund['amount'], 2, '.', ''));
        } else {
            $amount->setValue(number_format($booking_amount, 2, '.', ''));
        }



        $invoice_num = new Zend_Form_Element_Text('invoice_num');
        $invoice_num->setDecorators(array('ViewHelper'))
            ->addDecorator('Errors', array('class' => 'errors'))
            ->setValue((!empty($bookingInvoice['invoice_num']) ? $bookingInvoice['invoice_num'] : ''))
            ->setAttribs(array('class' => 'text_field form-control', 'readonly' => 'readonly' , 'style' => "background-color: gainsboro"));

        $invoice_amount = new Zend_Form_Element_Text('invoice_amount');
        $invoice_amount->setDecorators(array('ViewHelper'))
            ->addDecorator('Errors', array('class' => 'errors'))
            ->setValue((!empty($booking['qoute']) ? number_format($booking['qoute'], 2, '.', '') : ''))
            ->setAttribs(array('class' => 'text_field form-control', 'readonly' => 'readonly' , 'style' => "background-color: gainsboro"));

        $payment_transaction_id = new Zend_Form_Element_Text('payment_transaction_id');
        $payment_transaction_id->setDecorators(array('ViewHelper'))
            ->addDecorator('Errors', array('class' => 'errors'))
            ->setValue((!empty($paymentData['reference']) ? $paymentData['reference'] : ''))
            ->setAttribs(array('class' => 'text_field form-control', 'readonly' => 'readonly' , 'style' => "background-color: gainsboro"));


        $payment_amount = new Zend_Form_Element_Text('payment_amount');
        $payment_amount->setDecorators(array('ViewHelper'))
            ->addDecorator('Errors', array('class' => 'errors'))
            ->setValue((!empty($paymentData['amount']) ? number_format($paymentData['amount'], 2, '.', '') : ''))
            ->setAttribs(array('class' => 'text_field form-control', 'readonly' => 'readonly' , 'style' => "background-color: gainsboro"));

        $payment_date = new Zend_Form_Element_Text('payment_date');
        $payment_date->setDecorators(array('ViewHelper'))
            ->addDecorator('Errors', array('class' => 'errors'))
            ->setValue((!empty($paymentData['received_date'])? $paymentDataValue : ''))
            ->setAttribs(array('class' => 'text_field form-control', 'readonly' => 'readonly' , 'style' => "background-color: gainsboro"));

        $customer_name = new Zend_Form_Element_Text('customer_name');
        $customer_name->setDecorators(array('ViewHelper'))
            ->addDecorator('Errors', array('class' => 'errors'))
            ->setValue((!empty($bookingInvoice['customer_name']) ? $bookingInvoice['customer_name'] : ''))
            ->setAttribs(array('class' => 'text_field form-control', 'readonly' => 'readonly' , 'style' => "background-color: gainsboro"));         


        $refunded_to_date = new Zend_Form_Element_Text('refunded_to_date');
        $refunded_to_date->setDecorators(array('ViewHelper'))
                ->setAttribs(array('class' => 'text_field form-control' , 'readonly' => 'readonly' , 'style' => "background-color: gainsboro"))
                ->setValue(number_format($refundedToDate, 2, '.', ''));

        $avalible_refund = new Zend_Form_Element_Text('avalible_refund');
        $avalible_refund->setDecorators(array('ViewHelper'))
            ->addDecorator('Errors', array('class' => 'errors'))
            ->setAttribs(array('class' => 'text_field form-control'))
            ->setRequired()
            ->addValidator('LessThan', FALSE, array(number_format($avalibleRefund, 2, '.', '') + 0.01))
            // ->addErrorMessage('Error in the Refund, it must not exceed $' . number_format($booking_amount, 2, '.', ''));
            ->setValue(number_format($avalibleRefund, 2, '.', ''));
    



        $description = new Zend_Form_Element_Textarea('description');
        $description->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setValue((!empty($refund['description']) ? $refund['description'] : ''))
                ->setAttribs(array('class' => 'textarea_field form-control'));


        
        $modelPaymentType = new Model_PaymentType();
        $payment_type_id = new Zend_Form_Element_Select('payment_type_id');
        $payment_type_id->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'select_field form-control'));
                if('eway_refund' == $mode){
                    $payment_type_id->setValue((!empty($paymentData['payment_type_id']) ? $paymentData['payment_type_id'] : ''));
                    $PaymentTypeName = $modelPaymentType->getById($paymentData['payment_type_id']);
                    $payment_type_id->addMultiOption($paymentData['payment_type_id'], $PaymentTypeName['payment_type']);
                    
                }else{
                $payment_type_id->setValue((!empty($refund['payment_type_id']) ? $refund['payment_type_id'] : ''))
                ->setRequired()
                ->setErrorMessages(array('Required' => 'Please select the payment type'));
                                
                $allPaymentType = $modelPaymentType->getAll();
                $payment_type_id->addMultiOption('', 'Select One');
                foreach ($allPaymentType as $paymentType) {
                    $payment_type_id->addMultiOption($paymentType['id'], $paymentType['payment_type']);
                }
            }
		
		$modelUser = new Model_User();
        $contractor_id = new Zend_Form_Element_Select('contractor_id');
        $contractor_id->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'form-control'));

                if('eway_refund' == $mode){
                     // $contractor_id->setValue($paymentData['contractor_id']);
                    $contractor_id->setValue((!empty($paymentData['contractor_id']) ? $paymentData['contractor_id'] : ''));
                    $user = $modelUser->getById($paymentData['contractor_id']);
                    $contractor_id->addMultiOption($paymentData['contractor_id'], $user['username']);

                }else{
                $contractor_id->setValue((!empty($contractorIdVal) ? $contractorIdVal : ''))
                ->setRequired()
                ->setErrorMessages(array('Required' => 'Please select the Technician'));
				
        		$modelContractorServiceBooking = new Model_ContractorServiceBooking();
                
                $contractors = $modelContractorServiceBooking->getByBookingId($booking_id);
                $count = count($contractors);
                if ($count > 1 || empty($contractorIdVal) || $contractorIdVal == 0 ) {
                    $contractor_id->addMultiOption('', 'Select One');
                }
                foreach ($contractors as $contractor) {
                    $user = $modelUser->getById($contractor['contractor_id']);
                    $contractor_id->addMultiOption($contractor['contractor_id'], $user['username']);
                }
    }



        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'button btn btn-primary'));

        $refund_button = new Zend_Form_Element_Submit('refund_button');
        $refund_button->setDecorators(array('ViewHelper'));
        $refund_button->setLabel('Process Refund');
        $refund_button->setAttribs(array('class' => 'button btn btn-primary'));

        $this->addElements(array($received_date, $bank_charges, $withholding_tax, $amount_withheld, $reference, $is_acknowledgment, $amount,$invoice_num,$invoice_amount,$payment_transaction_id,$payment_amount,$payment_date,$customer_name,$avalible_refund, $refunded_to_date, $description, $payment_type_id,$contractor_id,$button,$refund_button));
        $this->setMethod('post');
        if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $refund['refund_id']), 'editRefund'));
        } elseif('eway_refund' == $mode) {
            $this->setAction($router->assemble(array('booking_id' => $booking_id), 'refundAddByEway'));
        }else{
            $this->setAction($router->assemble(array('booking_id' => $booking_id), 'refundAdd'));
        }
    }

}

?>