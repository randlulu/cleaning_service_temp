<?php

class Invoices_Form_Refund extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('Refund');
        $booking_id = (isset($options['booking_id']) ? $options['booking_id'] : '');
        $booking_amount = (isset($options['amount']) ? $options['amount'] : '');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $refund = (isset($options['refund']) ? $options['refund'] : '');
		$contractorIdVal = (isset($options['contractor_id']) ? $options['contractor_id'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();
        $request = Zend_Controller_Front::getInstance()->getRequest();

        $received_date = new Zend_Form_Element_Text('received_date');
        $received_date->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setValue(empty($refund) ? getNewDateFormat(time()) : getNewDateFormat($refund['received_date']) )
                //->setValue(empty($refund) ? date('d M Y', time()) : date('d M Y', $refund['received_date']) )
				->setAttribs(array('class' => 'text_field form-control'))
                ->setErrorMessages(array('Required' => 'Please enter the received date'));

        $bank_charges = new Zend_Form_Element_Text('bank_charges');
        $bank_charges->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setValue((!empty($refund['bank_charges']) ? $refund['bank_charges'] : ''))
                ->setAttribs(array('class' => 'text_field'));

        $amount_withheld = new Zend_Form_Element_Text('amount_withheld');
        $amount_withheld->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setValue((!empty($refund['amount_withheld']) ? $refund['amount_withheld'] : ''))
                ->setAttribs(array('class' => 'text_field'));

        $reference = new Zend_Form_Element_Text('reference');
        $reference->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setValue((!empty($refund['reference']) ? $refund['reference'] : ''))
                ->setAttribs(array('class' => 'text_field form-control'))
                ->setRequired();


        $withholding_tax = new Zend_Form_Element_Checkbox('withholding_tax');
        $withholding_tax->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setValue((!empty($refund['withholding_tax']) ? $refund['withholding_tax'] : ''))
                ->setAttribs(array('class' => 'checkbox_field', 'onclick' => "$('#amount_withheld_block').toggle();"));


        $is_acknowledgment = new Zend_Form_Element_Checkbox('is_acknowledgment');
        $is_acknowledgment->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setValue((!empty($refund['is_acknowledgment']) ? $refund['is_acknowledgment'] : ''))
                ->setAttribs(array('class' => 'checkbox_field'));


        $amount = new Zend_Form_Element_Text('amount');
        $amount->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'text_field form-control'))
                ->setRequired()
                ->addErrorMessage('Error in the Refund, it must not exceed $' . number_format($booking_amount, 2, '.', ''))
                ->addValidator('LessThan', FALSE, array(number_format($booking_amount, 2, '.', '') + 0.01));
        if ('update' == $mode) {
            $amount->setValue(number_format($refund['amount'], 2, '.', ''));
        } else {
            $amount->setValue(number_format($booking_amount, 2, '.', ''));
        }

        $description = new Zend_Form_Element_Textarea('description');
        $description->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setValue((!empty($refund['description']) ? $refund['description'] : ''))
                ->setAttribs(array('class' => 'textarea_field form-control'));


        $payment_type_id = new Zend_Form_Element_Select('payment_type_id');
        $payment_type_id->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'select_field form-control'))
                ->setValue((!empty($refund['payment_type_id']) ? $refund['payment_type_id'] : ''))
                ->setRequired()
                ->setErrorMessages(array('Required' => 'Please select the payment type'));

        $modelPaymentType = new Model_PaymentType();
        $allPaymentType = $modelPaymentType->getAll();
        $payment_type_id->addMultiOption('', 'Select One');
        foreach ($allPaymentType as $paymentType) {
            $payment_type_id->addMultiOption($paymentType['id'], $paymentType['payment_type']);
        }
		
		$contractor_id = new Zend_Form_Element_Select('contractor_id');
        $contractor_id->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($contractorIdVal) ? $contractorIdVal : ''))
                ->setRequired()
                ->setErrorMessages(array('Required' => 'Please select the Technician'));
				
		$modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelUser = new Model_User();
        $contractors = $modelContractorServiceBooking->getByBookingId($booking_id);
        $count = count($contractors);
        if ($count > 1 || empty($contractorIdVal) || $contractorIdVal == 0 ) {
            $contractor_id->addMultiOption('', 'Select One');
        }
        foreach ($contractors as $contractor) {
            $user = $modelUser->getById($contractor['contractor_id']);
            $contractor_id->addMultiOption($contractor['contractor_id'], $user['username']);
        }
				

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'button'));

        $this->addElements(array($received_date, $bank_charges, $withholding_tax, $amount_withheld, $reference, $is_acknowledgment, $amount, $description, $payment_type_id,$contractor_id,$button));
        $this->setMethod('post');
        if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $refund['refund_id']), 'editRefund'));
        } else {
            $this->setAction($router->assemble(array('booking_id' => $booking_id), 'refundAdd'));
        }
    }

}

