<?php

class Invoices_Form_Payment extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

		$dateTimeObj= get_settings_date_format();

		if(!empty($dateTimeObj) && isset($options['booking_end']))
		$booking_end_value=getNewDateFormat((int)strtotime($options['booking_end']));
		else{
			if(isset($options['booking_end']))
		$booking_end_value=date("d M Y", strtotime($options['booking_end']));
		}
        $this->setName('Payment');
        $booking_id = (isset($options['booking_id']) ?  $options['booking_id'] : '');
        $booking_amount = (isset($options['amount']) ?  $options['amount'] : '');
		$booking_end =(isset($options['booking_end'])? $booking_end_value: '');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $payment = (isset($options['payment']) ? $options['payment'] : '');
        $contractorIdVal = (isset($payment['contractor_id']) ? $payment['contractor_id'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();
        $request = Zend_Controller_Front::getInstance()->getRequest();

		//convert received_date from timestamp to date
		if(isset($payment['received_date'])){	
			$received_date_value = date_create();
			date_timestamp_set($received_date_value,$payment['received_date']);
		}

        $received_date = new Zend_Form_Element_Text('received_date');
        $received_date->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setErrorMessages(array('Required' => 'Please enter the received date'));

        $bank_charges = new Zend_Form_Element_Text('bank_charges');
        $bank_charges->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setValue((!empty($payment['bank_charges']) ? $payment['bank_charges'] : ''))
                ->setAttribs(array('class' => 'form-control'));

        $amount_withheld = new Zend_Form_Element_Text('amount_withheld');
        $amount_withheld->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setValue((!empty($payment['amount_withheld']) ? $payment['amount_withheld'] : ''))
                ->setAttribs(array('class' => 'form-control'));

        /*$reference = new Zend_Form_Element_Text('reference');
        $reference->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setValue((!empty($payment['reference']) ? $payment['reference'] : ''))
                ->setAttribs(array('class' => 'form-control'))
                ->setRequired();*/
		
		/*$reference = new Zend_Form_Element_Text('reference');
        $reference->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setValue((!empty($payment['reference']) ? $payment['reference'] : ''))
                ->setAttribs(array('class' => 'form-control' , 'id'=>'reference','onBlur' => 'checkDuplicatePaymentReference()'))
                ->setRequired();*/

        $reference = new Zend_Form_Element_Text('reference');
        $reference->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setValue((!empty($payment['reference']) ? $payment['reference'] : ''))
                ->setRequired();
                 if ('update' == $mode) {
                   $reference->setAttribs(array('class' => 'form-control' , 'id'=>'reference','onChange' => 'checkDuplicatePaymentReference()')); 
                } else {
                    $reference->setAttribs(array('class' => 'form-control' , 'id'=>'reference','onBlur' => 'checkDuplicatePaymentReference()'));
                }


        $withholding_tax = new Zend_Form_Element_Checkbox('withholding_tax');
        $withholding_tax->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setValue((!empty($payment['withholding_tax']) ? $payment['withholding_tax'] : ''))
                ->setAttribs(array('class' => 'checkbox_field', 'onclick' => "$('#amount_withheld_block').toggle();"));


        $is_acknowledgment = new Zend_Form_Element_Checkbox('is_acknowledgment');
        $is_acknowledgment->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setValue((!empty($payment['is_acknowledgment']) ? $payment['is_acknowledgment'] : ''))
                ->setAttribs(array('class' => 'checkbox_field'));


        $amount = new Zend_Form_Element_Text('amount');
        $amount->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'form-control'))
                ->setRequired();
                //->addErrorMessage('Error in the Payment, it must not exceed $' . number_format($booking_amount, 2, '.', ''))
                //->addValidator('LessThan', FALSE, array(number_format($booking_amount, 2, '.', '') + 0.01));
        if ('update' == $mode) {

            $amount->setValue(number_format($payment['amount'],2,".",","));
			if(!empty($dateTimeObj))
			$received_date->setValue(getNewDateFormat($payment['received_date']));
            else
			$received_date->setValue(date_format($received_date_value, 'd M Y'));

        } else {
		    $amount->setValue(number_format($booking_amount,2,".",","));
			$received_date->setValue($booking_end);
        }

        $description = new Zend_Form_Element_Textarea('description');
        $description->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setValue((!empty($payment['description']) ? $payment['description'] : ''))
                ->setAttribs(array('class' => 'form-control'));


        $payment_type_id = new Zend_Form_Element_Select('payment_type_id');
        $payment_type_id->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($payment['payment_type_id']) ? $payment['payment_type_id'] : ''))
                ->setRequired()
                ->setErrorMessages(array('Required' => 'Please select the payment type'));

        $modelPaymentType = new Model_PaymentType();
        $allPaymentType = $modelPaymentType->getAll();
        $payment_type_id->addMultiOption('', 'Select One');
        foreach ($allPaymentType as $paymentType) {
            $payment_type_id->addMultiOption($paymentType['id'], $paymentType['payment_type']);
        }

        $contractor_id = new Zend_Form_Element_Select('contractor_id');
        $contractor_id->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($contractorIdVal) ? $contractorIdVal : ''))
                ->setRequired()
                ->setErrorMessages(array('Required' => 'Please select the Technician'));

        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelUser = new Model_User();
        $contractors = $modelContractorServiceBooking->getByBookingId($booking_id);
        $count = count($contractors);
        if ($count > 1) {
            $contractor_id->addMultiOption('', 'Select One');
        }
        foreach ($contractors as $contractor) {
            $user = $modelUser->getById($contractor['contractor_id']);
            $contractor_id->addMultiOption($contractor['contractor_id'], $user['username']);
        }

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'button'));

        $this->addElements(array($received_date, $bank_charges, $withholding_tax, $amount_withheld, $reference, $is_acknowledgment, $amount, $description, $payment_type_id, $contractor_id, $button));
        $this->setMethod('post');
        if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $payment['payment_id']), 'editPayment'));
        } else {
            $this->setAction($router->assemble(array('booking_id' => $booking_id), 'paymentAdd'));
        }
    }

}

