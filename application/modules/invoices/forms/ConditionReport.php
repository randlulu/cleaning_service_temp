<?php
class Invoices_Form_ConditionReport extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('condition-report');
        $conditionReport = (isset($options['condition-report']) ? $options['condition-report'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();
        
        
        $report = new Zend_Form_Element_Textarea('report');
        $report->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'textarea_field form-control'))
                ->setValue((!empty($conditionReport['condition_report']) ? $conditionReport['condition_report'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the report condition'));



        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));

        $this->addElements(array($report, $button));

        $this->setMethod('post');
        $this->setAction($router->assemble(array('id' =>$conditionReport['id']), 'invoiceConditionReport'));
       
    }

}

