<?php

class Invoices_Form_EditInvoiceNumber extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $invoice_id = isset($options['invoice_id']) ? $options['invoice_id'] : '';
        $invoiceNumber = isset($options['invoice_number']) ? $options['invoice_number'] : '';

        $this->setName('EditInvoiceNumber');

        $router = Zend_Controller_Front::getInstance()->getRouter();

        $invoice_number = new Zend_Form_Element_Text('invoice_number');
        $invoice_number->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue($invoiceNumber)
                ->setErrorMessages(array('Required' => 'Please Enter Invoice Number.'));

        
        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'button'));


        $this->addElements(array($invoice_number, $button));

        $this->setMethod('post');

        $this->setAction($router->assemble(array('id' => $invoice_id), 'editInvoiceNumber'));
    }

}