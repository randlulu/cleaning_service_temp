<?php

class Invoices_Form_MarkPayment extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('MarkPayment');

        $payment = (isset($options['payment']) ? $options['payment'] : '');
        $mode = (isset($options['mode']) ? $options['mode'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();
        $request = Zend_Controller_Front::getInstance()->getRequest();


        $commentVal = (!empty($payment['mark_comment']) ? $payment['mark_comment'] : '');
        $comment = new Zend_Form_Element_Textarea('comment');
        $comment->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'textarea_field', 'cols' => '40', 'rows' => '10'))
                ->setValue($commentVal);

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'button'));

        $this->addElements(array($comment, $button));
        $this->setMethod('post');

        if ('update' == $mode) {
            $this->setAction($router->assemble(array('payment_id' => $payment['payment_id']), 'modifyMarkComment'));
        } else {
            $this->setAction($router->assemble(array('payment_id' => $payment['payment_id']), 'markPayment'));
        }
    }

}

