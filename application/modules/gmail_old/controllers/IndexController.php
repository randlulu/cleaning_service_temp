<?php

class Gmail_IndexController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'gmail';
    }

    /**
     * Items list action
     */
    public function indexAction() {

       $this->view->main_menu = 'gmail';
		$this->view->sub_menu = 'inbox';
		//
        // check Auth for logged user
        //
        //CheckAuth::checkPermission(array('contractors'));

        //
        // get request parameters
        //
		
        $model = new Model_GmailServiceAccount(8);
		$emails = $model->getAllEmails();
		$service = $model->getService();
        $this->view->emails = $emails;
        $this->view->service = $service;
    }
	public function viewAction(){
		$id = $this->request->getParam('id');
		
		$model = new Model_GmailServiceAccount(8);
		$emails = $model->getAllEmails();
		$service = $model->getService();
		$this->view->id = $id;
        $this->view->emails = $emails;
        $this->view->service = $service;
			
	}
	

    
}