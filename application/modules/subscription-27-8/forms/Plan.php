<?php

class Subscription_Form_Plan extends Zend_Form
{

    public function __construct($options = null) {
        parent::__construct($options);

    	 $this->setName('Plan');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $plan = (isset($options['plan']) ? $options['plan'] : '');
        $router = Zend_Controller_Front::getInstance()->getRouter();
		$name = new Zend_Form_Element_Text('name');
        $name->setDecorators(array('ViewHelper'))
        		->setRequired()
                ->addDecorator('Errors', array('class' => 'errors'))
				->setValue((!empty($plan['name']) ? $plan['name'] : ''))
                ->setAttribs(array('class' => 'form-control'));

        $planCode = new Zend_Form_Element_Text('planCode');
        $planCode->setDecorators(array('ViewHelper'))
        		->setRequired()
                ->addDecorator('Errors', array('class' => 'errors'))
				->setValue((!empty($plan['plan_code']) ? $plan['plan_code'] : ''))
                ->setAttribs(array('class' => 'form-control'));

        $planType = new Zend_Form_Element_Select('planType');
        $planType->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'select_field form-control'))
                ->setValue((!empty($plan['plan_type_id']) ? $plan['plan_type_id'] : ''))
                ->setErrorMessages(array('Required' => 'Please select plan type.'));

        $planTypeModel = new Model_PlanType();
        $allPlanType = $planTypeModel->getPlanTypeAsArray();
        $planType->addMultiOption('', 'Select one');
        $planType->addMultiOptions($allPlanType);

        $planDescription = new Zend_Form_Element_Textarea('planDescription');
        $planDescription->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'textarea_field form-control','rows'=>'12'))
                ->setValue((!empty($plan['plan_description']) ? $plan['plan_description'] : ''))
                ->setErrorMessages(array('Required' => 'Please fill the description plan !'));

    	$maxUsers = new Zend_Form_Element_Text('maxUsers');
        $maxUsers->setDecorators(array('ViewHelper'))
        		->addValidator('Digits')
        		->setRequired()
                ->addDecorator('Errors', array('class' => 'errors'))
				->setValue((!empty($plan['max_users']) ? $plan['max_users'] : ''))
                ->setAttribs(array('class' => 'form-control'));
                // ->setErrorMessages(array('Required' => 'Please enter the max users for this plan !'));
		$maxUsers->addValidator('Digits');
        $maxUsers->getValidator('Digits')->setMessage('Max users field must be number !');

        $chargePeriod = new Zend_Form_Element_Select('chargePeriod');
        $chargePeriod->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'select_field form-control'))
                ->setValue((!empty($plan['charge_period']) ? $plan['charge_period'] : ''))
                ->setErrorMessages(array('Required' => 'Please select charge period.'));
        $chargePeriod->addMultiOption('', 'Select one');
        $chargePeriod->addMultiOption('monthly', 'Monthly');
        $chargePeriod->addMultiOption('annually', 'Annually'); 

    	$chargeAmount = new Zend_Form_Element_Text('chargeAmount');
        $chargeAmount->setDecorators(array('ViewHelper'))
        		->addValidator('Digits')
        		->setRequired()
                ->addDecorator('Errors', array('class' => 'errors'))
				->setValue((!empty($plan['charge_amount']) ? $plan['charge_amount'] : ''))
                ->setAttribs(array('class' => 'form-control'));
                // ->setErrorMessages(array('Required' => 'Please enter the cost every charge period  !'));  
		$chargeAmount->addValidator('Digits');
        $chargeAmount->getValidator('Digits')->setMessage('Charge amount field must be number !');

    	$trialPeriod = new Zend_Form_Element_Text('trialPeriod');
        $trialPeriod->setDecorators(array('ViewHelper'))
        		->setRequired()
        		// ->addValidator('NotEmpty', true)
                ->addDecorator('Errors', array('class' => 'errors'))
				->setValue((!empty($plan['trial_period']) ? $plan['trial_period'] : ''))
        		->setAttribs(array('class' => 'text_field form-control'));
                // ->setErrorMessages(array('Required' => 'Please enter trial period  !'));
        // $trialPeriod->get('Required')->setMessage('Please enter trial period  !');
        		// $trialPeriod->getValidator('NotEmpty')->setMessage('Please enter trial period  !');
		$trialPeriod->addValidator('Digits');
        $trialPeriod->getValidator('Digits')->setMessage('Trial period field must be number');
        

    	$monthlyPrice = new Zend_Form_Element_Text('monthlyPrice');
        $monthlyPrice->setDecorators(array('ViewHelper'))
        		// ->addValidator('Digits')
        		// ->setRequired()
                ->addDecorator('Errors', array('class' => 'errors'))
				->setValue((!empty($plan['monthly_price']) ? $plan['monthly_price'] : ''))
                 ->setAttribs(array('class' => 'text_field form-control'))
                ->setErrorMessages(array('Required' => 'Please enter monthly price  !')); 

    	$annuallyPrice = new Zend_Form_Element_Text('annuallyPrice');
        $annuallyPrice->setDecorators(array('ViewHelper'))
        		// ->addValidator('Digits', true)
        		
        		// ->setRequired()
                ->addDecorator('Errors', array('class' => 'errors'))
				->setValue((!empty($plan['annually_price']) ? $plan['annually_price'] : ''))
                ->setAttribs(array('class' => 'text_field form-control'))
                ->setErrorMessages(array('Required' => 'Please enter annually price !'));



        $status = new Zend_Form_Element_Select('status');
        $status->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'select_field form-control'))
                ->setValue((!empty($plan['status']) ? $plan['status'] : ''))
                ->setErrorMessages(array('Required' => 'Please select plan status.'));
        $status->addMultiOption('active', 'Active');
        $status->addMultiOption('notactive', 'Not Active');

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'button btn btn-primary'));

        $this->addElements(array($name, $planCode,$planType, $planDescription, $maxUsers, $chargePeriod,$chargeAmount,$trialPeriod,$monthlyPrice,$annuallyPrice,$status,$button));


        $this->setMethod('post');
        if ('update' == $mode) {
            $this->setAction($router->assemble(array('plan_id' => $plan['plan_id']), 'editPlan'));
        } else {
            $this->setAction($router->assemble(array(), 'addPlan'));
        }

    }


}

