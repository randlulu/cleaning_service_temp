<?php

class Subscription_Form_AddUsersCompany extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        

        $router = Zend_Controller_Front::getInstance()->getRouter();

        $firstName = new Zend_Form_Element_Text('first_name');
        $firstName->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                
                ->setErrorMessages(array('Required' => 'Please enter the first name'));
				
		$lastName = new Zend_Form_Element_Text('last_name');
        $lastName->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                
                ->setErrorMessages(array('Required' => 'Please enter the last name'));		
				
		$Email = new Zend_Form_Element_Text('email');
        $Email->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->addValidator(new Zend_Validate_EmailAddress())
                ->setAttribs(array('class' => 'form-control'))
            	->setErrorMessages(array('Required' => 'Please enter the email'));



        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Add');
        $button->setAttribs(array('class' => 'btn btn-primary'));
		
		$buttonskip = new Zend_Form_Element_Button('buttonskip');
        $buttonskip->setDecorators(array('ViewHelper'));
        $buttonskip->setLabel('skip');
        $buttonskip->setAttribs(array('class' => 'btn btn-default' , 'id' =>'skip_to'));

        $this->addElements(array($firstName, $lastName , $Email  , $button ,$buttonskip));
        $this->setMethod('post');
        
        $this->setAction($router->assemble(array('id' => 2), 'toaddusers'));
        
    }

}

