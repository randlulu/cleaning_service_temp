<?php

class Calendar_Form_Unavailable extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('Unavailable');
        $start = (isset($options['start']) ? $options['start'] : '');
        $end = (isset($options['end']) ? $options['end'] : '');
        $contractor_id = (isset($options['contractor']) ? $options['contractor'] : '');
		
        $router = Zend_Controller_Front::getInstance()->getRouter();
        $startdate = new Zend_Form_Element_Text('startdate');
        $startdate->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors',' style'=>'position: absolute;top: -19px'))
                ->setRequired()
                ->setValue((!empty($start) ? $start : ''))
				->setAttribs(array('class' => 'form-control','readonly' => 'true'))
				->setErrorMessages(array('Required' => 'Please Enter Start date'));
				
	    $enddate = new Zend_Form_Element_Text('enddate');
        $enddate->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors',' style'=>'position: absolute;top: -19px'))
                ->setRequired()
                ->setValue((!empty($end) ? $end : ''))
				->setAttribs(array('class' => 'form-control','readonly' => 'true'))
				->setErrorMessages(array('Required' => 'Please Enter End date'));
				
	    $contractor = new Zend_Form_Element_Hidden('contractor');
        $contractor->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setValue((!empty($contractor_id) ? $contractor_id : ''));


		$starttime = new Zend_Form_Element_Text('starttime');
        $starttime->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors',' style'=>'position: absolute;top: -19px'))
				->setValue('')
				->setRequired()
				->setErrorMessages(array('Required' => 'Please Enter Start time'))
                ->setAttribs(array('class' => 'text_field date_time form-control','readonly' => 'true'));
				
				
	    $endtime = new Zend_Form_Element_Text('endtime');
        $endtime->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors',' style'=>'position: absolute;top: -19px'))
				->setValue('')
				->setRequired()
				->setErrorMessages(array('Required' => 'Please Enter End time'))
                ->setAttribs(array('class' => 'text_field date_time form-control','readonly' => 'true'));
				
				
	    $title = new Zend_Form_Element_Text('title');
        $title->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors',' style'=>'position: absolute;top: -19px'))
				->setValue('unavailable')
				->setRequired()
				->setErrorMessages(array('Required' => 'Please Enter title'))
                ->setAttribs(array('class' => 'form-control'));
				
	    $postcode = new Zend_Form_Element_Text('postcode');
        $postcode->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors',' style'=>'position: absolute;top: -19px'))
				->addValidator('regex',true,array('#^\\d{4}$#'))
				->setErrorMessages(array('Required' => 'Please Enter correct postcode'))
                ->setAttribs(array('class' => 'form-control'));



        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Send');
        $button->setAttribs(array('class' => 'button btn btn-primary'));


        $this->addElements(array($startdate, $enddate, $contractor, $starttime, $endtime,$title,$postcode, $button));

        $this->setMethod('post');

    }

}

