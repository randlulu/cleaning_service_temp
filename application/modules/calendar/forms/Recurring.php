<?php
		class Calendar_Form_Recurring extends Zend_Form {
			public function __construct($options = null) {
			parent::__construct($options);
			$this->setName('Repeat Booking');
			$router = Zend_Controller_Front::getInstance()->getRouter();
			$recurrencesTypeId = new Zend_Form_Element_Select('recurrencesTypeId');
			$recurrencesTypeId->setDecorators(array('ViewHelper'))
							  ->addDecorator('Errors', array('class' => 'errors'))
							  ->setRequired()
							  ->setAttribs(array('class' => 'select_field form-control'))
							  ->setMultiOptions(array('' => 'Select One'))
							  ->setErrorMessages(array('Required' => 'Please select Interval Type.'));

			$recurrencesTypeId->addMultiOption('1', 'Daily');
			$recurrencesTypeId->addMultiOption('2', 'Weekly');
			$recurrencesTypeId->addMultiOption('3', 'fortnight');
			$recurrencesTypeId->addMultiOption('4', 'Monthly');
			$fromDate = new Zend_Form_Element_Text('fromDate');
			$fromDate->setDecorators(array('ViewHelper'))
			         ->addDecorator('Errors', array('class' => 'errors'))
			         ->setAttribs(array('class' => 'text_field date_time form-control'));

			$toDate = new Zend_Form_Element_Text('toDate');
			$toDate->setDecorators(array('ViewHelper'))
				   ->addDecorator('Errors', array('class' => 'errors'))
				   ->setAttribs(array('class' => 'text_field date_time form-control'));

			
			$this->addElement ( 
				'multiCheckbox', 'work_days[]', 
				array (
				'separator'    => ('&nbsp;&nbsp;&nbsp;' . PHP_EOL),
				'multiOptions' => array(
						'0'    => 'Sun',
						'1'    => 'Mon',
						'2'    => 'Tue',
						'3'    => 'Wed',
						'4'    => 'Thu',
						'5'    => 'Fri',
						'6'    => 'Sat'						
						)
				)
			);

			$button = new Zend_Form_Element_Button('button');
			$button->setDecorators(array('ViewHelper'));
			$button->setLabel('Save');
			$button->setAttribs(array('class' => 'button btn btn-primary'));
			$this->addElements(array($recurrencesTypeId,$fromDate,$toDate,$button));
			$this->setMethod('post');
			$this->setAction($router->assemble(array('id' => Null), 'recurring'));
 
			}

		}

