<?php

class Settings_Form_CronJobAddDescription extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('CronJob');

        $oldCronJob = (isset($options['oldCronJob']) ? $options['oldCronJob'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();

        $descriptionVal = (!empty($oldCronJob['description']) ? $oldCronJob['description'] : '');
        $description = new Zend_Form_Element_Textarea('description');
        $description->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'textarea_field', 'cols' => '40', 'rows' => '10'))
                ->setValue($descriptionVal);

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'button'));

        $this->addElements(array($description, $button));
        $this->setAction($router->assemble(array('id' => $oldCronJob['id']), 'settingsCronjobAddDescription'));
    }

}

