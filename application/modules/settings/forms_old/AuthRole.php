<?php

class Settings_Form_AuthRole extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('AuthRole');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $authRole = (isset($options['authRole']) ? $options['authRole'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();
        $request = Zend_Controller_Front::getInstance()->getRequest();

        $roleName = new Zend_Form_Element_Text('role_name');
        $roleName->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($authRole['view_role_name']) ? $authRole['view_role_name'] : ''));
        $defaultPage = new Zend_Form_Element_Text('default_page');
        $defaultPage->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($authRole['default_page']) ? $authRole['default_page'] : ''));

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'button'));

        $this->addElements(array($roleName, $defaultPage, $button));
        $this->setMethod('post');
        if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $authRole['role_id']), 'settingsAuthRoleEdit'));
        } else {
            $this->setAction($router->assemble(array(), 'settingsAuthRoleAdd'));
        }
    }

}

