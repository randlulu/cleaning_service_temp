<?php

class Settings_Form_ImageAttachment extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('ImageAttachment');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $imageAttachment = (isset($options['imageAttachment']) ? $options['imageAttachment'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();

        $image = new Zend_Form_Element_File('image');
        $image->setDestination(get_config('temp_uploads'))
                ->setDecorators(array('File', 'Errors', array('Description', array('tag' => 'p', 'class' => 'hint'))))
                ->setMaxFileSize(get_config('upload_max_filesize')) // limits the imagesize on the client side
                ->setDescription('Click Browse and click on the  image you would like to upload');
        $image->addValidator('Count', false, 1);  // ensure only 1 image
        $image->addValidator('Size', false, get_config('upload_max_filesize')); //10240000 limit to 10 meg
        $image->addValidator('Extension', false, 'jpeg,jpg,png,gif');

        if ($mode == 'update') {
            $request = Zend_Controller_Front::getInstance()->getRequest();
            $change_image = $request->getParam('change_image', 0);
            $this->setAttrib('change_image', $change_image);

            if ($change_image) {
                $image->setRequired(true);
            }
        } else {
            $image->setRequired(true);
        }

        $modelServices = new Model_Services();
        $allService = $modelServices->getAllService();

        $service = new Zend_Form_Element_Select('service_id');
        $service->setDecorators(array('ViewHelper'));
        $service->setAttrib("class", "select_field");
        $service->setValue((!empty($imageAttachment['service_id']) ? $imageAttachment['service_id'] : ''));
        $service->addMultiOption('', 'Select One');
        $service->addMultiOptions($allService);

        $modelAttributes = new Model_Attributes();
        $attribute = $modelAttributes->getByVariableName('Floor');

        $modelAttributeListValue = new Model_AttributeListValue();
        $attributeListValue = $modelAttributeListValue->getByAttributeIdAsArray($attribute['attribute_id']);

        $floor = new Zend_Form_Element_Select('attribute_list_value_id');
        $floor->setDecorators(array('ViewHelper'));
        $floor->setAttrib("class", "select_field");
        $floor->setValue((!empty($imageAttachment['attribute_list_value_id']) ? $imageAttachment['attribute_list_value_id'] : ''));
        $floor->addMultiOption('', 'Select One');
        $floor->addMultiOptions($attributeListValue);

        $description = new Zend_Form_Element_Textarea('description');
        $description->setDecorators(array('ViewHelper', 'Errors'))
                ->setAttribs(array('class' => 'textarea_field', 'cols' => '40', 'rows' => '10'))
                ->setValue((!empty($imageAttachment['description']) ? $imageAttachment['description'] : ''));


        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'button'));

        $this->addElements(array($image, $service, $floor, $description, $button));
        $this->setMethod(Zend_Form::METHOD_POST);
        $this->setEnctype(Zend_Form::ENCTYPE_MULTIPART);

        if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $imageAttachment['id']), 'settingsImageAttachmentEdit'));
        } else {
            $this->setAction($router->assemble(array(), 'settingsImageAttachmentAdd'));
        }
    }

}