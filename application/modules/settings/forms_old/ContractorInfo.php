<?php

class Settings_Form_ContractorInfo extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('ContractorInfo');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $contractorInfo = (isset($options['contractorInfo']) ? $options['contractorInfo'] : '');
        $contractor_id = (isset($options['contractor_id']) ? $options['contractor_id'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();

        $businessName = new Zend_Form_Element_Text('business_name');
        $businessName->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($contractorInfo['business_name']) ? $contractorInfo['business_name'] : ''));
        //->setErrorMessages(array('Required' => 'Please enter the Business Name'));


        $abn = new Zend_Form_Element_Text('abn');
        $abn->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($contractorInfo['abn']) ? $contractorInfo['abn'] : ''));
        //->setErrorMessages(array('Required' => 'Please enter the ABN'));

        $tfn = new Zend_Form_Element_Text('tfn');
        $tfn->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($contractorInfo['tfn']) ? $contractorInfo['tfn'] : ''));
        //->setErrorMessages(array('Required' => 'Please enter the TFN'));


        $acn = new Zend_Form_Element_Text('acn');
        $acn->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($contractorInfo['acn']) ? $contractorInfo['acn'] : ''));
        // ->setErrorMessages(array('Required' => 'Please enter the ACN'));

        $gst = new Zend_Form_Element_Checkbox('gst');
        $gst->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'checkbox_field'))
                ->setValue((!empty($contractorInfo['gst']) ? $contractorInfo['gst'] : ''));
        //->setErrorMessages(array('Required' => 'Please enter the GST'));


        $gstDateRegistered = new Zend_Form_Element_Text('gst_date_registered');
        $gstDateRegistered->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setAttrib('readonly', TRUE)
                ->setValue((!empty($contractorInfo['gst_date_registered']) ? date('d-m-Y', $contractorInfo['gst_date_registered']) : ''));
        //->setErrorMessages(array('Required' => 'Please enter the  GST Date Registered'));

        $insurance_policy_number = new Zend_Form_Element_Text('insurance_policy_number');
        $insurance_policy_number->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                // ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($contractorInfo['insurance_policy_number']) ? $contractorInfo['insurance_policy_number'] : ''));
        //  ->setErrorMessages(array('Required' => 'Please enter the Insurance Policy Number'));

        $insurance_policy_start = new Zend_Form_Element_Text('insurance_policy_start');
        $insurance_policy_start->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                // ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($contractorInfo['insurance_policy_start']) ? $contractorInfo['insurance_policy_start'] : ''));
        //  ->setErrorMessages(array('Required' => 'Please enter the Insurance Policy Start'));

        $insurance_policy_expiry = new Zend_Form_Element_Text('insurance_policy_expiry');
        $insurance_policy_expiry->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                // ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($contractorInfo['insurance_policy_expiry']) ? $contractorInfo['insurance_policy_expiry'] : ''));
        //->setErrorMessages(array('Required' => 'Please enter the Insurance Policy Expiry'));


        $insurance_listed_services_covered = new Zend_Form_Element_Text('insurance_listed_services_covered');
        $insurance_listed_services_covered->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($contractorInfo['insurance_listed_services_covered']) ? $contractorInfo['insurance_listed_services_covered'] : ''));
        //->setErrorMessages(array('Required' => 'Please enter the Insurance listed services covered'));

        $drivers_licence_number = new Zend_Form_Element_Text('drivers_licence_number');
        $drivers_licence_number->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                // ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($contractorInfo['drivers_licence_number']) ? $contractorInfo['drivers_licence_number'] : ''));
        // ->setErrorMessages(array('Required' => 'Please enter the Drivers licence Number'));

        $drivers_licence_expiry = new Zend_Form_Element_Text('drivers_licence_expiry');
        $drivers_licence_expiry->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($contractorInfo['drivers_licence_expiry']) ? $contractorInfo['drivers_licence_expiry'] : ''));
        // ->setErrorMessages(array('Required' => 'Please enter the Drivers licence Expiry'));

        $bond_to_be_withheld = new Zend_Form_Element_Text('bond_to_be_withheld');
        $bond_to_be_withheld->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($contractorInfo['bond_to_be_withheld']) ? $contractorInfo['bond_to_be_withheld'] : ''));
        //->setErrorMessages(array('Required' => 'Please enter the Bond to be Withheld'));

        $commission = new Zend_Form_Element_Text('commission');
        $commission->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($contractorInfo['commission']) ? $contractorInfo['commission'] : ''));
        //->setErrorMessages(array('Required' => 'Please enter the Commission'));


        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'button'));

        $this->addElements(array($businessName, $abn, $tfn, $acn, $gst, $gstDateRegistered, $insurance_listed_services_covered, $insurance_policy_expiry, $insurance_policy_number, $insurance_policy_start, $drivers_licence_expiry, $drivers_licence_number, $bond_to_be_withheld, $commission, $button));
        $this->setMethod('post');

        if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $contractorInfo['contractor_info_id'], 'contractor_id' => $contractor_id), 'settingsContractorInfoEdit'));
        } else {
            $this->setAction($router->assemble(array('contractor_id' => $contractor_id), 'settingsContractorInfoAdd'));
        }
    }

}

