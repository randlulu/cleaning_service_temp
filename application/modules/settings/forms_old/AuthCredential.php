<?php

class Settings_Form_AuthCredential extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('AuthCredential');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $authCredential = (isset($options['authCredential']) ? $options['authCredential'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();
        $request = Zend_Controller_Front::getInstance()->getRequest();

        $credentialNameVal = (!empty($authCredential['credential_name']) ? $authCredential['credential_name'] : '');
        $credentialName = new Zend_Form_Element_Text('credential_name');
        $credentialName->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue($credentialNameVal);
        if ($credentialNameVal != $request->getParam('credential_name')) {
            $credentialName->addValidator(new Zend_Validate_Db_NoRecordExists('auth_credential', 'credential_name'));
        }

        $parentIdVal = (!empty($authCredential['parent_id']) ? $authCredential['parent_id'] : '');
        $parent_id = new Zend_Form_Element_Select('parent_id');
        $parent_id->setDecorators(array('ViewHelper'));
        $parent_id->setValue($parentIdVal);
        $parent_id->addMultiOption('', 'Select One');
        $modelAuthCredential = new Model_AuthCredential();
        $options = $modelAuthCredential->getAllAuthCredential(true);
        $parent_id->addMultiOptions($options);

        $isHiddenVal = (!empty($authCredential['is_hidden']) ? $authCredential['is_hidden'] : '');
        $isHidden = new Zend_Form_Element_Checkbox('is_hidden');
        $isHidden->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'checkbox'))
                ->setValue($isHiddenVal);

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'button'));

        $this->addElements(array($credentialName, $parent_id, $isHidden, $button));
        $this->setMethod('post');
        if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $authCredential['credential_id']), 'settingsAuthCredentialEdit'));
        } else {
            $this->setAction($router->assemble(array(), 'settingsAuthCredentialAdd'));
        }
    }

}

