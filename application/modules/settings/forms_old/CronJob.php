<?php

class Settings_Form_CronJob extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('CronJob');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $oldCronJob = (isset($options['oldCronJob']) ? $options['oldCronJob'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();

        $cronJobName = new Zend_Form_Element_Text('cronjob_name');
        $cronJobName->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'text_field', 'style' => 'width: 376px;'))
                ->setValue((!empty($oldCronJob['cronjob_name']) ? $oldCronJob['cronjob_name'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the cron job'));
        if (!get_config('under_construction')) {
            $cronJobName->setAttrib('disabled', 'disabled');
            $cronJobName->setAttrib('style', 'width: 376px; background-color: lightgray;');
        } else {
            $cronJobName->setRequired();
        }

        if (get_config('under_construction')) {
            $phpCode = new Zend_Form_Element_Textarea('php_code');
            $phpCode->setDecorators(array('ViewHelper'))
                    ->addDecorator('Errors', array('class' => 'errors'))
                    ->setRequired()
                    ->setAttribs(array('class' => 'text_field', 'style' => 'width: 376px; height:100px;'))
                    ->setValue((!empty($oldCronJob['php_code']) ? $oldCronJob['php_code'] : ''))
                    ->setErrorMessages(array('Required' => 'Please enter the php code'));
        }

        $every = new Zend_Form_Element_Hidden('every');
        $every->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'text_field'))
                ->setRequired()
                ->setValue((!empty($oldCronJob['every']) ? $oldCronJob['every'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the every'));

        $running = new Zend_Form_Element_Checkbox('running');
        $running->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setValue((!empty($oldCronJob['running']) ? $oldCronJob['running'] : ''));

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'button'));

        if (get_config('under_construction')) {
            $this->addElements(array($cronJobName, $phpCode, $every, $running, $button));
        } else {
            $this->addElements(array($cronJobName, $every, $running, $button));
        }

        $this->setMethod('post');
        if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $oldCronJob['id']), 'settingsCronJobEdit'));
        } else {
            $this->setAction($router->assemble(array(), 'settingsCronJobAdd'));
        }
    }

}

