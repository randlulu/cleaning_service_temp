<?php

class Settings_Form_Countries extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('Countries');
        $mode          = (isset($options['mode']) ? $options['mode'] : '');
        $countries = (isset($options['countries']) ? $options['countries'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();
         

        $name = new Zend_Form_Element_Text('name');
        $name->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue( (!empty($countries['country_name']) ? $countries['country_name'] : '') )
                ->setErrorMessages(array('Required' => 'Please enter the country name'));

        $code = new Zend_Form_Element_Text('code');
        $code->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue( (!empty($countries['country_code']) ? $countries['country_code'] : '') )
                ->setErrorMessages(array('Required' => 'Please enter the country code'));

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'button'));

        $this->addElements(array($name, $code, $button));
        $this->setMethod('post');
        if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $countries['country_id']), 'settingsCountriesEdit'));
        } else {
            $this->setAction($router->assemble(array(), 'settingsCountriesAdd'));
        }
    }

}

