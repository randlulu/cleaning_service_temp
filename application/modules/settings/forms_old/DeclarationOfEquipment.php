<?php

class Settings_Form_DeclarationOfEquipment extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('DeclarationOfEquipment');
        $contractor_info_id = (isset($options['contractor_info_id']) ? $options['contractor_info_id'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();

        $equipment = new Zend_Form_Element_Text('equipment');
        $equipment->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setErrorMessages(array('Required' => 'Please enter the Equipment'));


        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'button'));

        $this->addElements(array($equipment, $button));
        $this->setMethod('post');
        $this->setAction($router->assemble(array('contractor_info_id' => $contractor_info_id), 'settingsDeclarationOfEquipmentAdd'));
    }

}

