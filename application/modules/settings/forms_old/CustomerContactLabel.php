<?php

class Settings_Form_CustomerContactLabel extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('CustomerContactLabel');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $contactLabel = (isset($options['contact_label']) ? $options['contact_label'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();
        $request = Zend_Controller_Front::getInstance()->getRequest();

        $ContactLabelVal = (!empty($contactLabel['contact_label']) ? $contactLabel['contact_label'] : '');
        $contact_label = new Zend_Form_Element_Text('contact_label');
        $contact_label->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue($ContactLabelVal);
        $value = $request->getParam('contact_label');
        if ($ContactLabelVal != $value) {
         
            $modelCustomerContactLabel = new Model_CustomerContactLabel();
            $notValid = $modelCustomerContactLabel->getByContactLabelAndCompany($value);
            if ($notValid) {
                $contact_label->addError("A record matching '{$value}' was found");
            }
        }

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'button'));

        $this->addElements(array($contact_label, $button));
        $this->setMethod('post');
        if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $contactLabel['id']), 'settingsCustomerContactLabelEdit'));
        } else {
            $this->setAction($router->assemble(array(), 'settingsCustomerContactLabelAdd'));
        }
    }

}


