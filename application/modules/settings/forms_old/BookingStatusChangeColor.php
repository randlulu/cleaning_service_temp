<?php

class Settings_Form_BookingStatusChangeColor extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('BookingStatusChangeColor');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $bookingStatus = (isset($options['bookingStatus']) ? $options['bookingStatus'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();
        $request = Zend_Controller_Front::getInstance()->getRequest();

        $colorVal = (!empty($bookingStatus['color']) ? $bookingStatus['color'] : 0);
        $color = new Zend_Form_Element_Hidden('color');
        $color->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setValue($colorVal);
        
        
        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'button'));

        $this->addElements(array($color, $button));
        $this->setMethod('post');
        if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $bookingStatus['booking_status_id']), 'settingsBookingStatusChangeColor'));
        } 
    }

}

