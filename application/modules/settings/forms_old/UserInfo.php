<?php

class Settings_Form_UserInfo extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('UserInfo');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $userInfo = (isset($options['userInfo']) ? $options['userInfo'] : '');
        $user_id = (isset($options['user_id']) ? $options['user_id'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();


        $firstName = new Zend_Form_Element_Text('first_name');
        $firstName->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($userInfo['first_name']) ? $userInfo['first_name'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the First name'));

        $lastName = new Zend_Form_Element_Text('last_name');
        $lastName->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($userInfo['last_name']) ? $userInfo['last_name'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the Last name'));


        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'button'));

        $this->addElements(array($firstName, $lastName, $button));
        $this->setMethod('post');
        if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $userInfo['user_info_id'], 'user_id' => $user_id), 'settingsUserInfoEdit'));
        } else {
            $this->setAction($router->assemble(array('user_id' => $user_id), 'settingsUserInfoAdd'));
        }
    }

}

