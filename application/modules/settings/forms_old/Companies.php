<?php

class Settings_Form_Companies extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('Companies');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $company = (isset($options['company']) ? $options['company'] : '');
        $countryId = (isset($options['country_id']) ? $options['country_id'] : 0);

        $router = Zend_Controller_Front::getInstance()->getRouter();

        $companyName = new Zend_Form_Element_Text('company_name');
        $companyName->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($company['company_name']) ? $company['company_name'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the company name'));


        $companyBusinessName = new Zend_Form_Element_Text('company_business_name');
        $companyBusinessName->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($company['company_business_name']) ? $company['company_business_name'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the company business name'));


        $companyAbn = new Zend_Form_Element_Text('company_abn');
        $companyAbn->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($company['company_abn']) ? $company['company_abn'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the company ABN'));



        $companyAcn = new Zend_Form_Element_Text('company_acn');
        $companyAcn->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($company['company_acn']) ? $company['company_acn'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the company ACN'));

        $companyGst = new Zend_Form_Element_Checkbox('company_gst');
        $companyGst->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'checkbox_field'))
                ->setValue((!empty($company['company_gst']) ? $company['company_gst'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the company GST'));


        $companyGstDateRegistered = new Zend_Form_Element_Text('company_gst_date_registered');
        $companyGstDateRegistered->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setAttrib('readonly', TRUE)
                ->setValue((!empty($company['company_gst_date_registered']) ? date('d-m-Y', $company['company_gst_date_registered']) : ''))
                ->setErrorMessages(array('Required' => 'Please enter the company GST Date Registered'));


        $companyWebsite = new Zend_Form_Element_Text('company_website');
        $companyWebsite->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($company['company_website']) ? $company['company_website'] : ''));

        $companyEnquiriesEmail = new Zend_Form_Element_Text('company_enquiries_email');
        $companyEnquiriesEmail->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->addValidator(new Zend_Validate_EmailAddress())
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($company['company_enquiries_email']) ? $company['company_enquiries_email'] : ''));
        //->setErrorMessages(array('Required' => 'Please enter the enquiries email', 'EmailAddress' => 'Invalid email Address'));

        $companyInvoicesEmail = new Zend_Form_Element_Text('company_invoices_email');
        $companyInvoicesEmail->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->addValidator(new Zend_Validate_EmailAddress())
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($company['company_invoices_email']) ? $company['company_invoices_email'] : ''));

        $companyAccountsEmail = new Zend_Form_Element_Text('company_accounts_email');
        $companyAccountsEmail->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->addValidator(new Zend_Validate_EmailAddress())
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($company['company_accounts_email']) ? $company['company_accounts_email'] : ''));


        $companyComplaintsEmail = new Zend_Form_Element_Text('company_complaints_email');
        $companyComplaintsEmail->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->addValidator(new Zend_Validate_EmailAddress())
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($company['company_complaints_email']) ? $company['company_complaints_email'] : ''));


        $companyMobile1 = new Zend_Form_Element_Text('company_mobile1');
        $companyMobile1->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($company['company_mobile1']) ? $company['company_mobile1'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the company mobile'));


        $companyMobile2 = new Zend_Form_Element_Text('company_mobile2');
        $companyMobile2->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($company['company_mobile2']) ? $company['company_mobile2'] : ''));

        $companyMobile3 = new Zend_Form_Element_Text('company_mobile3');
        $companyMobile3->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($company['company_mobile3']) ? $company['company_mobile3'] : ''));
        //->setErrorMessages(array('Required' => 'Please enter the company mobile'));


        $companyPhone1 = new Zend_Form_Element_Text('company_phone1');
        $companyPhone1->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($company['company_phone1']) ? $company['company_phone1'] : ''));
        //->setErrorMessages(array('Required' => 'Please enter the company phone number'));


        $companyPhone2 = new Zend_Form_Element_Text('company_phone2');
        $companyPhone2->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($company['company_phone2']) ? $company['company_phone2'] : ''));
        //->setErrorMessages(array('Required' => 'Please enter the company phone number'));

        $companyPhone3 = new Zend_Form_Element_Text('company_phone3');
        $companyPhone3->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($company['company_phone3']) ? $company['company_phone3'] : ''));
        //->setErrorMessages(array('Required' => 'Please enter the company phone number'));


        $companyFax = new Zend_Form_Element_Text('company_fax');
        $companyFax->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($company['company_fax']) ? $company['company_fax'] : ''));
        //->setErrorMessages(array('Required' => 'Please enter the company Fax'));



        $companyEmergencyPhone = new Zend_Form_Element_Text('company_emergency_phone');
        $companyEmergencyPhone->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($company['company_emergency_phone']) ? $company['company_emergency_phone'] : ''));
        //->setErrorMessages(array('Required' => 'Please enter the company emergency phone'));

        $companyUnitLotNo = new Zend_Form_Element_Text('company_unit_lot_no');
        $companyUnitLotNo->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($company['company_unit_lot_no']) ? $company['company_unit_lot_no'] : ''));
        //->setErrorMessages(array('Required' => 'Please enter the street address'));

        $companyStreetNo = new Zend_Form_Element_Text('company_street_no');
        $companyStreetNo->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($company['company_street_no']) ? $company['company_street_no'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the street no'));

        $companyStreetAddress = new Zend_Form_Element_Text('company_street_address');
        $companyStreetAddress->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($company['company_street_address']) ? $company['company_street_address'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the street address'));


        $companySuburb = new Zend_Form_Element_Text('company_suburb');
        $companySuburb->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($company['company_suburb']) ? $company['company_suburb'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the suburb'));


        //
        //get country & city for ajax
        //
        $city_obj = new Model_Cities();
        $city = $city_obj->getById((!empty($company['city_id']) ? $company['city_id'] : CheckAuth::getCityId()));

        $country_id = new Zend_Form_Element_Select('country_id');
        $country_id->setDecorators(array('ViewHelper'))
                ->setAttribs(array('class' => 'select_field', 'onchange' => 'getCities();'))
                ->setRequired()
                ->setValue((!empty($city['country_id']) ? $city['country_id'] : ''))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setErrorMessages(array('Required' => 'Please select Country'));

        $table = new Model_Countries();
        $country_id->addMultiOption('', 'Select One');
        foreach ($table->getCountriesAsArray() as $c) {
            $country_id->addMultiOption($c['id'], $c['name']);
        }

        $city_id = new Zend_Form_Element_Select('city_id');
        $city_id->setDecorators(array('ViewHelper'))
                ->setAttribs(array('class' => 'select_field'))
                ->setRequired()
                ->setValue((!empty($city['city_id']) ? $city['city_id'] : ''))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setErrorMessages(array('Required' => 'Please select City'));

        $city_id->addMultiOption('', 'Select One');
        foreach ($city_obj->getByCountryId((!empty($countryId) ? $countryId : $city['country_id'])) as $c) {
            $city_id->addMultiOption($c['city_id'], $c['city_name']);
        }


        $companyState = new Zend_Form_Element_Text('company_state');
        $companyState->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($company['company_state']) ? $company['company_state'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the state'));


        $companyPostcode = new Zend_Form_Element_Text('company_postcode');
        $companyPostcode->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($company['company_postcode']) ? $company['company_postcode'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the postcode'));


        $companyPoBox = new Zend_Form_Element_Text('company_po_box');
        $companyPoBox->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($company['company_po_box']) ? $company['company_po_box'] : ''));
        //->setErrorMessages(array('Required' => 'Please enter the company PO Box'));



        /*
          $companyLogo = new Zend_Form_Element_File('company_logo');
          $companyLogo->addDecorator('Errors', array('class' => 'errors'))
          ->setDestination(UPLOADS_PATH . '/company_logo')
          ->setMaxFileSize(get_config('upload_max_filesize'))
          ->addValidator('Count', FALSE, 1)
          ->addValidator('Size', FALSE, get_config('upload_max_filesize'))
          ->addValidator('Extension', FALSE, 'jpg.jpeg,png,gif')
          ->removeDecorator('HtmlTag')
          ->removeDecorator('Label')
          ->setRequired()
          ->setAttribs(array('class' => 'text_field'))
          ->setErrorMessages(array('Required' => 'Please enter the company Logo'));
         * 
         */


        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'button'));

        $this->addElements(array($companyName, $companyWebsite, $companyBusinessName, $companyAbn, $companyAcn, $companyGst, $companyGstDateRegistered, $companyEnquiriesEmail, $companyInvoicesEmail, $companyAccountsEmail, $companyComplaintsEmail, $companyMobile1, $companyMobile2, $companyMobile3, $companyPhone1, $companyPhone2, $companyPhone3, $companyFax, $companyEmergencyPhone, $companyUnitLotNo, $companyStreetNo, $companyStreetAddress, $companySuburb, $country_id, $city_id, $companyState, $companyPostcode, $companyPoBox, $button));
        $this->setMethod('post');
        if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $company['company_id']), 'settingsCompaniesEdit'));
        } else {
            $this->setAction($router->assemble(array(), 'settingsCompaniesAdd'));
        }
    }

}

