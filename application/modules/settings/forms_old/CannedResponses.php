<?php

class Settings_Form_CannedResponses extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('CannedResponses');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $cannedResponses = (isset($options['cannedResponses']) ? $options['cannedResponses'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();


        $name = new Zend_Form_Element_Text('name');
        $name->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($cannedResponses['name']) ? $cannedResponses['name'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the email template name'));
        if ('update' == $mode && (!get_config('under_construction'))) {
            $name->setAttribs(array('readonly' => 'readonly', 'style' => 'background-color: lightgray;'));
        }

        $subject = new Zend_Form_Element_Text('subject');
        $subject->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($cannedResponses['subject']) ? $cannedResponses['subject'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the email subject'));

        $body = new Zend_Form_Element_Textarea('body');
        $body->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'textarea_field'))
                ->setValue((!empty($cannedResponses['body']) ? $cannedResponses['body'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the email body'));

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'button'));

        $this->addElements(array($name, $subject, $body, $button));
        $this->setMethod('post');
        if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $cannedResponses['id']), 'settingsCannedResponsesEdit'));
        } else {
            $this->setAction($router->assemble(array(), 'settingsCannedResponsesAdd'));
        }
    }

}

