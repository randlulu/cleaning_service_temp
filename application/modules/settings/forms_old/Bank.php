<?php

class Settings_Form_Bank extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('Bank');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $bank = (isset($options['bank']) ? $options['bank'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();

        $companyId = new Zend_Form_Element_Select('company_id');
        $companyId->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'select_field'))
                ->setValue((!empty($bank['company_id']) ? $bank['company_id'] : ''))
                ->setErrorMessages(array('Required' => 'Please select the company'));
        $table = new Model_Companies();
        foreach ($table->getCompaniesAsArray() as $c) {
            $companyId->addMultiOption($c['id'], $c['name']);
        }

        $bsb = new Zend_Form_Element_Text('bsb');
        $bsb->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($bank['bsb']) ? $bank['bsb'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the bank bsb'));

        $bankName = new Zend_Form_Element_Text('bank_name');
        $bankName->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($bank['bank_name']) ? $bank['bank_name'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the bank Name'));


        $account_name = new Zend_Form_Element_Text('account_name');
        $account_name->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($bank['account_name']) ? $bank['account_name'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the account Name'));


        $account_number = new Zend_Form_Element_Text('account_number');
        $account_number->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($bank['account_number']) ? $bank['account_number'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the account number'));

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'button'));

        $this->addElements(array($bsb, $companyId, $bankName, $button, $account_number, $account_name));
        $this->setMethod('post');
        if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $bank['bank_id']), 'settingsBankEdit'));
        } else {
            $this->setAction($router->assemble(array(), 'settingsBankAdd'));
        }
    }

}

