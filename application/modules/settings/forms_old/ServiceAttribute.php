<?php

class Settings_Form_ServiceAttribute extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('ServiceAttribute');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $serviceAttribute = (isset($options['serviceAttribute']) ? $options['serviceAttribute'] : '');
        $service_id = (isset($options['service_id']) ? $options['service_id'] : '');
		$attribute_type = (isset($serviceAttribute['attribute_type']) ? $serviceAttribute['attribute_type'] : '');
		
        $router = Zend_Controller_Front::getInstance()->getRouter();
        $view = new Zend_View();

        if($attribute_type == 'long_text'){
			$defaultPrice = new Zend_Form_Element_Textarea('default_value');
			$defaultPrice->setDecorators(array('ViewHelper'))
						->addDecorator('Errors', array('class' => 'errors'))
						->setAttribs(array('class' => 'long_text'))
						->setValue(!empty($serviceAttribute['default_value']) ? $view->escape($serviceAttribute['default_value']) : '');	
		}
		else{
			$defaultPrice = new Zend_Form_Element_Text('default_value');
			$defaultPrice->setDecorators(array('ViewHelper'))
						->addDecorator('Errors', array('class' => 'errors'))
						->setAttribs(array('class' => 'text_field'))
						->setValue(!empty($serviceAttribute['default_value']) ? $view->escape($serviceAttribute['default_value']) : '');

		}
        $is_readonly = new Zend_Form_Element_Checkbox('is_readonly');
        $is_readonly->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'checkbox_field'))
                ->setValue(!empty($serviceAttribute['is_readonly']) ? $view->escape($serviceAttribute['is_readonly']) : '');

        $attributeId = new Zend_Form_Element_Select('attribute_id');
        $attributeId->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'select_field'))
                ->setValue(!empty($serviceAttribute['attribute_id']) ? $view->escape($serviceAttribute['attribute_id']) : '')
                ->setErrorMessages(array('Required' => 'Please select the attribute'));

        $table = new Model_Attributes();
        $attributeId->addMultiOption('', 'Select One');
        foreach ($table->getAttributeAsArray() as $a) {
            $attributeId->addMultiOption($a['id'], $a['name']);
        }

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'button'));

        $this->addElements(array($is_readonly, $defaultPrice, $attributeId, $button));
        $this->setMethod('post');

        if ('update' == $mode) {
            $this->setAction($router->assemble(array('service_id' => $service_id, 'id' => !empty($serviceAttribute['service_attribute_id']) ? $view->escape($serviceAttribute['service_attribute_id']) : 0), 'settingsServiceAttributeEdit'));
        } else {
            $this->setAction($router->assemble(array('service_id' => $service_id), 'settingsServiceAttributeAdd'));
        }
    }

}

