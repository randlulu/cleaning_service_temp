<?php

class Settings_Form_ComplaintType extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('ComplaintType');
        $mode          = (isset($options['mode']) ? $options['mode'] : '');
        $complaintType = (isset($options['complaintType']) ? $options['complaintType'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();
         

        $name = new Zend_Form_Element_Text('name');
        $name->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue( (!empty($complaintType['name']) ? $complaintType['name'] : '') )
                ->setErrorMessages(array('Required' => 'Please enter the complaint type name'));

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'button'));

        $this->addElements(array($name, $button));
        $this->setMethod('post');
        if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $complaintType['complaint_type_id']), 'settingsComplaintTypeEdit'));
        } else {
            $this->setAction($router->assemble(array(), 'settingsComplaintTypeAdd'));
        }
    }

}

