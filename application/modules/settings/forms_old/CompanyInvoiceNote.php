<?php

class Settings_Form_CompanyInvoiceNote extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('CompanyInvoiceNote');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $companyId = (isset($options['company_id']) ? $options['company_id'] : '');
        $companyInvoiceNote = (isset($options['companyInvoiceNote']) ? $options['companyInvoiceNote'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();
        $request = Zend_Controller_Front::getInstance()->getRequest();

        $noteVal = (!empty($companyInvoiceNote['note']) ? $companyInvoiceNote['note'] : '');
        $note = new Zend_Form_Element_Textarea('note');
        $note->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'textarea_field'))
                ->setValue($noteVal);
        if ($noteVal != $request->getParam('note')) {
            $note->addValidator(new Zend_Validate_Db_NoRecordExists('company_invoice_note', 'note'));
        }

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'button'));

        $this->addElements(array($note, $button));
        $this->setMethod('post');
        if ('update' == $mode) {
            $this->setAction($router->assemble(array('company_id' => $companyId, 'id' => $companyInvoiceNote['id']), 'settingsCompanyInvoiceNoteEdit'));
        } else {
            $this->setAction($router->assemble(array('company_id' => $companyId), 'settingsCompanyInvoiceNoteAdd'));
        }
    }

}

