<?php

class Settings_Form_CustomerType extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('CustomerType');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $customerType = (isset($options['customerType']) ? $options['customerType'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();
        $request = Zend_Controller_Front::getInstance()->getRequest();

        $customerTypeVal = (!empty($customerType['customer_type']) ? $customerType['customer_type'] : '');
        $customer_type = new Zend_Form_Element_Text('customer_type');
        $customer_type->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue($customerTypeVal);
        $value = $request->getParam('customer_type');
        if ($customerTypeVal != $value) {
            //$customer_type->addValidator(new Zend_Validate_Db_NoRecordExists('customer_type', 'customer_type'));
            $modelCustomerType = new Model_CustomerType();

            $notValid = $modelCustomerType->getByTypeNameAndCompany($value);
            if ($notValid) {
                $customer_type->addError("A record matching '{$value}' was found");
            }
        }

        $workOrderVal = (!empty($customerType['is_work_order']) ? $customerType['is_work_order'] : '');
        $workOrder = new Zend_Form_Element_Checkbox('is_work_order');
        $workOrder->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'checkbox_field'))
                ->setValue($workOrderVal);

//
//        $companyId = new Zend_Form_Element_Select('company_id');
//        $companyId->setDecorators(array('ViewHelper'))
//                ->addDecorator('Errors', array('class' => 'errors'))
//                ->setRequired()
//                ->setAttribs(array('class' => 'select_field'))
//                ->setValue((!empty($customerType['company_id']) ? $customerType['company_id'] : ''))
//                ->setErrorMessages(array('Required' => 'Please select the company'));
//
//        $table = new Model_Companies();
//
//        $companyId->addMultiOption('0', 'All Company');
//        foreach ($table->getCompaniesAsArray() as $c) {
//            $companyId->addMultiOption($c['id'], $c['name']);
//        }

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'button'));

        $this->addElements(array($customer_type,$workOrder, $button));
        $this->setMethod('post');
        if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $customerType['customer_type_id']), 'settingsCustomerTypeEdit'));
        } else {
            $this->setAction($router->assemble(array(), 'settingsCustomerTypeAdd'));
        }
    }

}

