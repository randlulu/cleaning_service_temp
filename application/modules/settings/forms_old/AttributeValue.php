<?php

class Settings_Form_AttributeValue extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('AttributeValue');
        $attribute_id = (isset($options['attribute_id']) ? $options['attribute_id'] : '');
        $attributeListValue = (isset($options['attributeListValue']) ? $options['attributeListValue'] : '');
        $mode = (isset($options['mode']) ? $options['mode'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();

        $attributeValue = new Zend_Form_Element_Text('attribute_value');
        $attributeValue->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($attributeListValue['attribute_value']) ? $attributeListValue['attribute_value'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the attribute value'));


        $unitPrice = new Zend_Form_Element_Text('unit_price');
        $unitPrice->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setValue((!empty($attributeListValue['unit_price']) ? $attributeListValue['unit_price'] : ''))
                ->setAttribs(array('class' => 'text_field'));
        if (isset($_POST['unit_price'])) {
            $unitPrice->setRequired();
            $unitPrice->setErrorMessages(array('Required' => 'Please enter the unit price'));
        }

        $extraInfo = new Zend_Form_Element_Textarea('extra_info');
        $extraInfo->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'textarea_field', 'cols' => '40', 'rows' => '10'))
                ->setValue((!empty($attributeListValue['extra_info']) ? $attributeListValue['extra_info'] : ''));

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'button'));

        $this->addElements(array($attributeValue, $unitPrice, $extraInfo, $button));
        $this->setMethod('post');
        
        if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $attributeListValue['attribute_value_id']), 'settingsAttributeValueEdit'));
        } else {
            $this->setAction($router->assemble(array('attribute_id' => $attribute_id), 'settingsAttributeValueAdd'));
        }
    }

}

