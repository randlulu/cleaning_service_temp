<?php

class Settings_Form_ContractorGmailAccounts extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('ContractorGmailAccounts');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $contractorAccount = (isset($options['contractorAccount']) ? $options['contractorAccount'] : '');
        $contractor_id = (isset($options['contractor_id']) ? $options['contractor_id'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();


        $email = new Zend_Form_Element_Text('email');
        $email->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($contractorAccount['email']) ? $contractorAccount['email'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the email'));

        $password = new Zend_Form_Element_Text('password');
        $password->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($contractorAccount['password']) ? $contractorAccount['password'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the password'));


        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'button'));

        $this->addElements(array($email, $password, $button));
        $this->setMethod('post');

        if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $contractorAccount['id'], 'contractor_id' => $contractor_id), 'settingsContractorGmailEdit'));
        } else {
            $this->setAction($router->assemble(array('contractor_id' => $contractor_id), 'settingsContractorGmailAdd'));
        }
    }

}

