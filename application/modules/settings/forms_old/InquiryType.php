<?php

class Settings_Form_InquiryType extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('InquiryType');
        $mode        = (isset($options['mode']) ? $options['mode'] : '');
        $inquiryType = (isset($options['inquiryType']) ? $options['inquiryType'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();
         

        $name = new Zend_Form_Element_Text('name');
        $name->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue( (!empty($inquiryType['inquiry_name']) ? $inquiryType['inquiry_name'] : '') )
                ->setErrorMessages(array('Required' => 'Please enter the inquiry type name'));

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'button'));

        $this->addElements(array($name, $button));
        $this->setMethod('post');
        if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $inquiryType['inquiry_type_id']), 'settingsInquiryTypeEdit'));
        } else {
            $this->setAction($router->assemble(array(), 'settingsInquiryTypeAdd'));
        }
    }

}

