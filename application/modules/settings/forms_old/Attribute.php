<?php

class Settings_Form_Attribute extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('Attribute');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $attribute = (isset($options['attribute']) ? $options['attribute'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();
        $request = Zend_Controller_Front::getInstance()->getRequest();


        $attributeName = new Zend_Form_Element_Text('attribute_name');
        $attributeName->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($attribute['attribute_name']) ? $attribute['attribute_name'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the attribute name'));

        $companyId = new Zend_Form_Element_Select('company_id');
        $companyId->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'select_field'))
                ->setValue((!empty($attribute['company_id']) ? $attribute['company_id'] : ''))
                ->setErrorMessages(array('Required' => 'Please select the company'));
        $table = new Model_Companies();
        $companyId->addMultiOption('0', 'All Company');
        foreach ($table->getCompaniesAsArray() as $c) {
            $companyId->addMultiOption($c['id'], $c['name']);
        }

        $attributeTypeId = new Zend_Form_Element_Select('attribute_type_id');
        $attributeTypeId->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'select_field', 'onchange' => "setValue(this.value)"))
                ->setValue((!empty($attribute['attribute_type_id']) ? $attribute['attribute_type_id'] : ''))
                ->setErrorMessages(array('Required' => 'Please select the attribute type'));
        $table = new Model_AttributeType();
        foreach ($table->getAttributeTypeAsArray() as $c) {
            $attributeTypeId->addMultiOption($c['id'], $c['name']);
        }

        $extraInfo = new Zend_Form_Element_Textarea('extra_info');
        $extraInfo->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'textarea_field', 'cols' => '40', 'rows' => '10'))
                ->setValue((!empty($attribute['extra_info']) ? $attribute['extra_info'] : ''));

        $attributeVariableNameVar = (!empty($attribute['attribute_variable_name']) ? $attribute['attribute_variable_name'] : '');

        $attributeVariableName = new Zend_Form_Element_Text('attribute_variable_name');
        $attributeVariableName->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue($attributeVariableNameVar);
        
        if ($attributeVariableNameVar != $request->getParam('attribute_variable_name')) {
            $attributeVariableName->addValidator(new Zend_Validate_Db_NoRecordExists('attribute', 'attribute_variable_name'));
        }
        

        $isPriceAttribute = new Zend_Form_Element_Checkbox('is_price_attribute');
        $isPriceAttribute->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'checkbox_field'))
                ->setValue((!empty($attribute['is_price_attribute']) ? $attribute['is_price_attribute'] : ''));


        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'button'));

        $this->addElements(array($attributeName, $companyId, $isPriceAttribute, $attributeVariableName, $attributeTypeId, $extraInfo, $button));
        $this->setMethod('post');
        if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $attribute['attribute_id']), 'settingsAttributeEdit'));
        } else {
            $this->setAction($router->assemble(array(), 'settingsAttributeAdd'));
        }
    }

}

