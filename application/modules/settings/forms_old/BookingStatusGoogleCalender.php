<?php

class Settings_Form_BookingStatusGoogleCalender extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('BookingStatusGoogleCalender');

        $bookingStatus = (isset($options['bookingStatus']) ? $options['bookingStatus'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();
        

        $push_google_calender_val = (!empty($bookingStatus['push_google_calender']) ? $bookingStatus['push_google_calender'] : '');
        $push_google_calender = new Zend_Form_Element_Checkbox('push_google_calender');
        $push_google_calender->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'checkbox_field'))
                ->setValue($push_google_calender_val);


        $delete_google_calender_val = (!empty($bookingStatus['delete_google_calender']) ? $bookingStatus['delete_google_calender'] : '');
        $delete_google_calender = new Zend_Form_Element_Checkbox('delete_google_calender');
        $delete_google_calender->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'checkbox_field'))
                ->setValue($delete_google_calender_val);

        $request_feedback_val = (!empty($bookingStatus['request_feedback']) ? $bookingStatus['request_feedback'] : '');
        $request_feedback = new Zend_Form_Element_Checkbox('request_feedback');
        $request_feedback->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'checkbox_field'))
                ->setValue($request_feedback_val);
		////By Islam 
		$consider_for_nearest_booking_feature_val = (!empty($bookingStatus['consider_for_nearest_booking_feature']) ? $bookingStatus['consider_for_nearest_booking_feature'] : '');
        $consider_for_nearest_booking_feature = new Zend_Form_Element_Checkbox('consider_for_nearest_booking_feature');
        $consider_for_nearest_booking_feature->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'checkbox_field'))
                ->setValue($consider_for_nearest_booking_feature_val);
				

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'button'));

        $this->addElements(array($push_google_calender, $delete_google_calender,$request_feedback,$consider_for_nearest_booking_feature, $button));
        $this->setMethod('post');
        $this->setAction($router->assemble(array('id' => $bookingStatus['booking_status_id']), 'settingsBookingStatusGoogleCalender'));
    }

}

