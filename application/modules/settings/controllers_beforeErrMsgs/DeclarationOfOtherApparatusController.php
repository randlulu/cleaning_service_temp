<?php

class Settings_DeclarationOfOtherApparatusController extends Zend_Controller_Action {

    private $request;
    private $router;
    private $contractor_info_id;
    
    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router  = Zend_Controller_Front::getInstance()->getRouter();
        $this->contractor_info_id = $this->request->getParam('contractor_info_id');
        
        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsUser';
        
        
        //
        // get data
        //
        $contractorInfoObj = new Model_ContractorInfo();
        $contractorInfo = $contractorInfoObj->getById($this->contractor_info_id);
        
        BreadCrumbs::setLevel(4, 'Declaration Of Other Apparatus');
    }


    /**
     * Items list action
     */
    public function indexAction() {
        
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsDeclarationOfOtherApparatusList'));
        
        //
        // get request parameters
        //
        $orderBy        = $this->request->getParam('sort', 'id');
        $sortingMethod  = $this->request->getParam('method', 'asc');
        $currentPage    = $this->request->getParam('page', 1);
        $filters        = $this->request->getParam('fltr', array());
        
        

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage     = 20;
        $pager->currentPage = $currentPage;
        $pager->url         = $_SERVER['REQUEST_URI'];

        
        //
        //get user email by id
        //
        $filters['contractor_info_id'] = $this->contractor_info_id;
        
        
        //
        // get data list
        //
        $declarationOfOtherApparatusObj = new Model_DeclarationOfOtherApparatus();
        $this->view->data = $declarationOfOtherApparatusObj->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);
        
        //
        // set view params
        //
        $this->view->currentPage   = $currentPage;
        $this->view->perPage       = $pager->perPage;
        $this->view->pageLinks     = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy       = $orderBy;
        $this->view->filters       = $filters;
        $this->view->contractor_info_id = $this->contractor_info_id;
        
    }


    /**
     * Add new item action
     */
    public function addAction() {
        
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsDeclarationOfOtherApparatusAdd'));
        
        //
        // get request parameters
        //
        $other_apparatus    = $this->request->getParam('other_apparatus');
        $router = Zend_Controller_Front::getInstance()->getRouter();
        
        //
        // init action form
        //
        $form = new Settings_Form_DeclarationOfOtherApparatus(array('contractor_info_id'=>  $this->contractor_info_id));

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $declarationOfOtherApparatusObj = new Model_DeclarationOfOtherApparatus();
                $data = array(
                    'other_apparatus'   => $other_apparatus,
                    'contractor_info_id' => $this->contractor_info_id
                );

                $success = $declarationOfOtherApparatusObj->insert($data);
                
                if($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Declaration Of Other Apparatus"));
                }
                //$this->_redirect($this->router->assemble(array(), 'settingsUserEmailList'));
                echo 1;
                exit;
            }
        }

        $this->view->form       = $form;
        //
        // render views
        //
        echo $this->view->render('declaration-of-other-apparatus/add_edit.phtml');
        exit;
    }



    public function deleteAction() {
        
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsDeclarationOfOtherApparatusDelete'));
        
        //
        // get request parameters
        //
        $id  = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array() );
        if($id) {
            $ids[] = $id;
        }

        $declarationOfOtherApparatusObj = new Model_DeclarationOfOtherApparatus();
        foreach($ids as $id) {
            $declarationOfOtherApparatusObj->deleteById($id);
        }
        $this->_redirect($this->router->assemble(array('contractor_info_id' => $this->contractor_info_id), 'settingsDeclarationOfOtherApparatusList'));

    }
}

