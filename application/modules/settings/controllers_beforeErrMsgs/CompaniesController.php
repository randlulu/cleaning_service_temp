<?php

class Settings_CompaniesController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsUser';

        BreadCrumbs::setLevel(2, 'Companies');
    }

    /**
     * Items list action
     */
    public function indexAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsCompaniesList'));

        //
        // get request parameters
        //

        $orderBy = $this->request->getParam('sort', 'company_name');
        $sortingMethod = $this->request->getParam('method', 'desc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        //
        // get data list
        //
        $companiesObj = new Model_Companies();
        $countiresObj = new Model_Countries();
        $this->view->data = $companiesObj->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);
        $this->view->countries = $countiresObj->getAll(array(), "country_name ASC");

        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
    }

    /**
     * Add new item action
     */
    public function addAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsCompaniesAdd'));

        //
        // get request parameters
        //
        $companyName = $this->request->getParam('company_name');
        $companyBusinessName = $this->request->getParam('company_business_name');
        $companyAbn = $this->request->getParam('company_abn');
        $companyAcn = $this->request->getParam('company_acn');
        $companyGst = $this->request->getParam('company_gst');
        $companyGstDateRegistered = $this->request->getParam('company_gst_date_registered');
        $companyWebsite = $this->request->getParam('company_website');
        $companyEnquiriesEmail = $this->request->getParam('company_enquiries_email');
        $companyInvoicesEmail = $this->request->getParam('company_invoices_email');
        $companyAccountsEmail = $this->request->getParam('company_accounts_email');
        $companyComplaintsEmail = $this->request->getParam('company_complaints_email');
        $companyMobile1 = $this->request->getParam('company_mobile1');
        $companyMobile2 = $this->request->getParam('company_mobile2');
        $companyMobile3 = $this->request->getParam('company_mobile3');
        $companyPhone1 = $this->request->getParam('company_phone1');
        $companyPhone2 = $this->request->getParam('company_phone2');
        $companyPhone3 = $this->request->getParam('company_phone3');
        $companyFax = $this->request->getParam('company_fax');
        $companyEmergencyPhone = $this->request->getParam('company_emergency_phone');
        $companyUnitLotNo = $this->request->getParam('company_unit_lot_no');
        $companyStreetNo = $this->request->getParam('company_street_no');
        $companyStreetAddress = $this->request->getParam('company_street_address');
        $companySuburb = $this->request->getParam('company_suburb');
        $cityId = $this->request->getParam('city_id');
        $countryId = $this->request->getParam('country_id');
        $companyState = $this->request->getParam('company_state');
        $companyPostcode = $this->request->getParam('company_postcode');
        $companyPoBox = $this->request->getParam('company_po_box');

        if (get_config('remove_white_spacing')) {
            $companyMobile1 = preparer_number($companyMobile1);
            $companyMobile2 = preparer_number($companyMobile2);
            $companyMobile3 = preparer_number($companyMobile3);
            $companyPhone1 = preparer_number($companyPhone1);
            $companyPhone2 = preparer_number($companyPhone2);
            $companyPhone3 = preparer_number($companyPhone3);
        }

        $router = Zend_Controller_Front::getInstance()->getRouter();

        //
        // init action form
        //
        $form = new Settings_Form_Companies(array('country_id' => $countryId));

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $companiesObj = new Model_Companies();
                $data = array(
                    'company_name' => trim($companyName),
                    'company_abn' => $companyAbn,
                    'company_acn' => $companyAcn,
                    'city_id' => $cityId,
                    'company_enquiries_email' => $companyEnquiriesEmail,
                    'company_postcode' => $companyPostcode,
                    'company_street_address' => $companyStreetAddress,
                    'company_unit_lot_no' => $companyUnitLotNo,
                    'company_street_no' => $companyStreetNo,
                    'company_suburb' => $companySuburb,
                    'company_state' => $companyState,
                    'company_po_box' => $companyPoBox,
                    'company_website' => $companyWebsite,
                    'company_invoices_email' => $companyInvoicesEmail,
                    'company_accounts_email' => $companyAccountsEmail,
                    'company_complaints_email' => $companyComplaintsEmail,
                    'company_phone1' => $companyPhone1,
                    'company_phone2' => $companyPhone2,
                    'company_phone3' => $companyPhone3,
                    'company_mobile1' => $companyMobile1,
                    'company_mobile2' => $companyMobile2,
                    'company_mobile3' => $companyMobile3,
                    'company_gst' => $companyGst,
                    'company_gst_date_registered' => strtotime($companyGstDateRegistered),
                    'company_fax' => $companyFax,
                    'company_emergency_phone' => $companyEmergencyPhone,
                    'company_business_name' => $companyBusinessName
                );

                $success = $companiesObj->insert($data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Company"));
                }
                //$this->_redirect($this->router->assemble(array(), 'settingsCompaniesList'));
                echo 1;
                exit;
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('companies/add_edit.phtml');
        exit;
    }

    public function deleteAction() {
         
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsCompaniesDelete'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
        if ($id) {
            $ids[] = $id;
        }
		
		

        $companiesObj = new Model_Companies();
        foreach ($ids as $id) {
            $isNotRelated = $companiesObj->checkBeforeDeleteCompany($id);
            if ($isNotRelated) {
                $companiesObj->deleteById($id);
            } else {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "could not be deleted ,This Item used in other Place "));
            }
        }
		
		
		
        $this->_redirect($this->router->assemble(array(), 'settingsCompaniesList'));
    }

    public function editAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsCompaniesEdit'));



        //
        // get request parameters
        //
        $id = $this->request->getParam('id');
        $companyName = $this->request->getParam('company_name');
        $companyBusinessName = $this->request->getParam('company_business_name');
        $companyAbn = $this->request->getParam('company_abn');
        $companyAcn = $this->request->getParam('company_acn');
        $companyGst = $this->request->getParam('company_gst');
        $companyGstDateRegistered = $this->request->getParam('company_gst_date_registered');
        $companyWebsite = $this->request->getParam('company_website');
        $companyEnquiriesEmail = $this->request->getParam('company_enquiries_email');
        $companyInvoicesEmail = $this->request->getParam('company_invoices_email');
        $companyAccountsEmail = $this->request->getParam('company_accounts_email');
        $companyComplaintsEmail = $this->request->getParam('company_complaints_email');
        $companyMobile1 = $this->request->getParam('company_mobile1');
        $companyMobile2 = $this->request->getParam('company_mobile2');
        $companyMobile3 = $this->request->getParam('company_mobile3');
        $companyPhone1 = $this->request->getParam('company_phone1');
        $companyPhone2 = $this->request->getParam('company_phone2');
        $companyPhone3 = $this->request->getParam('company_phone3');
        $companyFax = $this->request->getParam('company_fax');
        $companyEmergencyPhone = $this->request->getParam('company_emergency_phone');
        $companyStreetAddress = $this->request->getParam('company_street_address');
        $companyUnitLotNo = $this->request->getParam('company_unit_lot_no');
        $companyStreetNo = $this->request->getParam('company_street_no');
        $companySuburb = $this->request->getParam('company_suburb');
        $cityId = $this->request->getParam('city_id');
        $countryId = $this->request->getParam('country_id');
        $companyState = $this->request->getParam('company_state');
        $companyPostcode = $this->request->getParam('company_postcode');
        $companyPoBox = $this->request->getParam('company_po_box');

        if (get_config('remove_white_spacing')) {
            $companyMobile1 = preparer_number($companyMobile1);
            $companyMobile2 = preparer_number($companyMobile2);
            $companyMobile3 = preparer_number($companyMobile3);
            $companyPhone1 = preparer_number($companyPhone1);
            $companyPhone2 = preparer_number($companyPhone2);
            $companyPhone3 = preparer_number($companyPhone3);
        }

        if ($id != CheckAuth::getCompanySession()) {
            If (!CheckAuth::checkCredential(array('canHandleAllCompanies'))) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            }
        }
        //
        // validation
        //
        $companiesObj = new Model_Companies();
        $company = $companiesObj->getById($id);
        if (!$company) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect($this->router->assemble(array(), 'settingsCompaniesList'));
            return;
        }


        //
        // init action form
        //
        $form = new Settings_Form_Companies(array('mode' => 'update', 'company' => $company, 'country_id' => $countryId));


        //
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'company_name' => trim($companyName),
                    'company_abn' => $companyAbn,
                    'company_acn' => $companyAcn,
                    'city_id' => $cityId,
                    'company_enquiries_email' => $companyEnquiriesEmail,
                    'company_postcode' => $companyPostcode,
                    'company_street_address' => $companyStreetAddress,
                    'company_unit_lot_no' => $companyUnitLotNo,
                    'company_street_no' => $companyStreetNo,
                    'company_suburb' => $companySuburb,
                    'company_state' => $companyState,
                    'company_po_box' => $companyPoBox,
                    'company_po_box' => $companyPoBox,
                    'company_website' => $companyWebsite,
                    'company_accounts_email' => $companyAccountsEmail,
                    'company_complaints_email' => $companyComplaintsEmail,
                    'company_invoices_email' => $companyInvoicesEmail,
                    'company_phone1' => $companyPhone1,
                    'company_phone2' => $companyPhone2,
                    'company_phone3' => $companyPhone3,
                    'company_mobile1' => $companyMobile1,
                    'company_mobile2' => $companyMobile2,
                    'company_mobile3' => $companyMobile3,
                    'company_gst' => $companyGst,
                    'company_gst_date_registered' => strtotime($companyGstDateRegistered),
                    'company_fax' => $companyFax,
                    'company_emergency_phone' => $companyEmergencyPhone,
                    'company_business_name' => $companyBusinessName
                );
                $success = $companiesObj->updateById($id, $data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Company"));
                }
                echo 1;
                exit;
            }
        }

        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('companies/add_edit.phtml');
        exit;
    }

    public function logoUploadAction() {

        //
        // get request parameters
        //
        $id = $this->request->getParam('id');

        //
        // validation
        //
        $companiesObj = new Model_Companies();
        $company = $companiesObj->getById($id);
        if (!$company) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect($this->router->assemble(array(), 'settingsCompaniesList'));
            return;
        }

        //init action form
        $form = new Settings_Form_Logo(array('company_id' => $id));
        $this->view->image = '<img src="' . $this->view->escape($this->view->baseUrl('/uploads/company_logo/' . $company['company_logo'])) . '" />';
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            if (!$form->isValid($this->getRequest()->getParams())) {
                return $this->render('logo-upload');
            }

            if (!$form->image->receive()) {
                $this->view->message = '<div class="popup-warning">Errors Receiving File.</div>';
                return $this->render('logo-upload');
            }

            if ($form->image->isUploaded()) {
                $values = $form->getValues();
                $source = $form->image->getFileName();

                $ext = pathinfo($source);
                $ext = $ext['extension'];

                //to re-name the image, all you need to do is save it with a new name, instead of the name they uploaded it with. Normally, I use the primary key of the database row where I'm storing the name of the image. For example, if it's an image of Person 1, I call it 1.jpg. The important thing is that you make sure the image name will be unique in whatever directory you save it to.
                //get sub dir
                $dir = get_config('company_logo') . '/';
                $subdir = date('Y/m/d/');

                //check if file exists or not
                $fullDir = $dir . $subdir;
                if (!is_dir($fullDir)) {
                    mkdir($fullDir, 0777, true);
                }

                $new_image_name = "logo_{$id}.{$ext}";
                $original_image_name = "original_{$id}.{$ext}";
                $favicon_image_name = "favicon_{$id}.{$ext}";

                $data = array(
                    'company_logo' => $subdir . $new_image_name,
                    'favicon' => $subdir . $favicon_image_name
                );
                $companiesObj->updateById($id, $data);

                //save image to database and filesystem here
                $image_saved = copy($source, $fullDir . $original_image_name);
                ImageMagick::scale_image($source, $fullDir . $new_image_name, 78, 64);
                ImageMagick::scale_image($source, $fullDir . $favicon_image_name, 16, 16);

                if ($image_saved) {

                    if (file_exists($source)) {
                        unlink($source);
                    }

                    $this->view->image = '<img src="' . $this->view->escape($this->view->baseUrl('/uploads/company_logo/' . $subdir . $new_image_name)) . '" />';
                    $form->reset(); //only do this if it saved ok and you want to re-display the fresh empty form
                }
            }
        }
    }

    public function invoiceLogoUploadAction() {

        //
        // get request parameters
        //
        $id = $this->request->getParam('id');

        //
        // validation
        //
        $companiesObj = new Model_Companies();
        $company = $companiesObj->getById($id);
        if (!$company) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect($this->router->assemble(array(), 'settingsCompaniesList'));
            return;
        }

        //init action form
        $form = new Settings_Form_InvoiceLogo(array('company_id' => $id));
        $this->view->image = '<img src="' . $this->view->escape($this->view->baseUrl('/uploads/company_logo/' . $company['company_invoice_logo'])) . '" />';
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            if (!$form->isValid($this->getRequest()->getParams())) {
                return $this->render('logo-upload');
            }

            if (!$form->image->receive()) {
                $this->view->message = '<div class="popup-warning">Errors Receiving File.</div>';
                return $this->render('logo-upload');
            }

            if ($form->image->isUploaded()) {
                $values = $form->getValues();
                $source = $form->image->getFileName();

                $ext = pathinfo($source);
                $ext = $ext['extension'];

                //to re-name the image, all you need to do is save it with a new name, instead of the name they uploaded it with. Normally, I use the primary key of the database row where I'm storing the name of the image. For example, if it's an image of Person 1, I call it 1.jpg. The important thing is that you make sure the image name will be unique in whatever directory you save it to.
                //get sub dir
                $dir = get_config('company_logo') . '/';
                $subdir = date('Y/m/d/');

                //check if file exists or not
                $fullDir = $dir . $subdir;
                if (!is_dir($fullDir)) {
                    mkdir($fullDir, 0777, true);
                }

                $new_image_name = "invoice_logo_{$id}.{$ext}";
                $original_image_name = "invoice_original_{$id}.{$ext}";

                $data = array(
                    'company_invoice_logo' => $subdir . $new_image_name
                );
                $companiesObj->updateById($id, $data);

                //save image to database and filesystem here
                $image_saved = copy($source, $fullDir . $original_image_name);
                ImageMagick::scale_image($source, $fullDir . $new_image_name, 439, 99);

                if ($image_saved) {

                    if (file_exists($source)) {
                        unlink($source);
                    }

                    $this->view->image = '<img src="' . $this->view->escape($this->view->baseUrl('/uploads/company_logo/' . $subdir . $new_image_name)) . '" />';
                    $form->reset(); //only do this if it saved ok and you want to re-display the fresh empty form
                }
            }
        }
    }

    public function signatureLogoUploadAction() {

        //
        // get request parameters
        //
        $id = $this->request->getParam('id');

        //
        // validation
        //
        $companiesObj = new Model_Companies();
        $company = $companiesObj->getById($id);

        if (!$company) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect($this->router->assemble(array(), 'settingsCompaniesList'));
            return;
        }

        //init action form
        $form = new Settings_Form_SignatureLogo(array('company_id' => $id));
        $this->view->image = '<img src="' . $this->view->escape($this->view->baseUrl('/uploads/company_logo/' . $company['company_signature_logo'])) . '" />';
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            if (!$form->isValid($this->getRequest()->getParams())) {
                return $this->render('signature-logo-upload');
            }

            if (!$form->image->receive()) {
                $this->view->message = '<div class="popup-warning">Errors Receiving File.</div>';
                return $this->render('signature-logo-upload');
            }

            if ($form->image->isUploaded()) {
                $values = $form->getValues();
                $source = $form->image->getFileName();

                $ext = pathinfo($source);
                $ext = $ext['extension'];

                //to re-name the image, all you need to do is save it with a new name, instead of the name they uploaded it with. Normally, I use the primary key of the database row where I'm storing the name of the image. For example, if it's an image of Person 1, I call it 1.jpg. The important thing is that you make sure the image name will be unique in whatever directory you save it to.
                //get sub dir
                $dir = get_config('company_logo') . '/';
                $subdir = date('Y/m/d/');

                //check if file exists or not
                $fullDir = $dir . $subdir;
                if (!is_dir($fullDir)) {
                    mkdir($fullDir, 0777, true);
                }

                $new_image_name = "signature_logo_{$id}.{$ext}";
                $original_image_name = "signature_original_{$id}.{$ext}";


                $data = array(
                    'company_signature_logo' => $subdir . $new_image_name,
                );
                $companiesObj->updateById($id, $data);

                //save image to database and filesystem here
                $image_saved = copy($source, $fullDir . $original_image_name);
                ImageMagick::scale_image($source, $fullDir . $new_image_name, 228, 214);


                if ($image_saved) {

                    if (file_exists($source)) {
                        unlink($source);
                    }

                    $this->view->image = '<img src="' . $this->view->escape($this->view->baseUrl('/uploads/company_logo/' . $subdir . $new_image_name)) . '" />';
                    $form->reset(); //only do this if it saved ok and you want to re-display the fresh empty form
                }
            }
        }
    }

}

