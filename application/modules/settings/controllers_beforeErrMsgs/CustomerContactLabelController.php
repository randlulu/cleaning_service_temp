<?php

class Settings_CustomerContactLabelController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();


        // main menu selected item
        $this->view->main_menu = 'settings';
        // sub menu selected item
        $this->view->sub_menu = 'settingsLabel';

        BreadCrumbs::setLevel(2, 'Customer Contact Label');
    }

    /**
     * Items list action
     */
    public function indexAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsCustomerContactLabelList'));

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        //
        // get data list
        //
        
        $modelCustomerContactLabel = new Model_CustomerContactLabel();
        $this->view->data = $modelCustomerContactLabel->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);

        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
    }

    /**
     * Add new item action
     */
    public function addAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsCustomerContactLabelAdd'));

        //
        // get request parameters
        //
        $contactLabel = $this->request->getParam('contact_label');
        $router = Zend_Controller_Front::getInstance()->getRouter();

        //
        // init action form
        //
        $form = new Settings_Form_CustomerContactLabel();

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $modelCustomerContactLabel = new Model_CustomerContactLabel();

                $data = array(
                    'contact_label' => $contactLabel,
                    'company_id' => CheckAuth::getCompanySession()
                );


                $success = $modelCustomerContactLabel->insert($data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Customer Contact Label "));
                }
                echo 1;
                exit;
            } else {

                $messages = $form->getElement('contact_label')->getErrorMessages();
                if ($messages) {

                    $form->getElement('contact_label')->clearErrorMessages();
                    foreach ($messages as $message) {
                        $form->getElement('contact_label')->addError($message);
                    }
                }
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('customer-contact-label/add_edit.phtml');
        exit;
    }

    public function deleteAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsCustomerContactLabelDelete'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
        if ($id) {
            $ids[] = $id;
        }

        $modelCustomerContactLabel = new Model_CustomerContactLabel();
        foreach ($ids as $id) {
            if (CheckAuth::checkIfCanHandelAllCompany('customer_contact_label', $id)) {

                $isNotRelated = $modelCustomerContactLabel->checkBeforeDeleteCustomerContactLabelType($id);
                if ($isNotRelated) {
                    $modelCustomerContactLabel->deleteById($id);
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "could not be deleted ,This Item used in other Place "));
                }
            }
        }
        $this->_redirect($this->router->assemble(array(), 'settingsCustomerContactLabelList'));
    }

    public function editAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsCustomerContactLabelEdit'));

        //
        // get request parameters
        //
        $contact_label = $this->request->getParam('contact_label');
        $id = $this->request->getParam('id');


        if (!CheckAuth::checkIfCanHandelAllCompany('customer_contact_label', $id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // validation
        //
        $modelCustomerContactLabel = new Model_CustomerContactLabel();
        $modelCustomerContactLabelOld = $modelCustomerContactLabel->getById($id);
        if (!$modelCustomerContactLabelOld) {

            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));

            $this->_redirect($this->router->assemble(array(), 'settingsCustomerContactLabelList'));
            return;
        }


        //
        // init action form
        //
         $form = new Settings_Form_CustomerContactLabel(array('mode' => 'update', 'contact_label' => $modelCustomerContactLabelOld));


        //
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'contact_label' => $contact_label,
                    'company_id' => CheckAuth::getCompanySession()
                );

                $success = $modelCustomerContactLabel->updateById($id, $data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Customer Contact Label "));
                }

                echo 1;
                exit;
            }
        }


        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('customer-contact-label/add_edit.phtml');
        exit;
    }

}

