<?php

class Settings_PropertyTypeController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsType';

        BreadCrumbs::setLevel(3, 'Prperty Types');
    }

    /**
     * Items list action
     */
    public function indexAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsPropertyTypes'));

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        //
        // get data list
        //
        $modelPropertyType = new Model_PropertyType();
        $this->view->data = $modelPropertyType->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);

        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
    }

    /**
     * Add new item action
     */
    public function addAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsPropertyTypeAdd'));

        //
        // get request parameters
        //
        $router = Zend_Controller_Front::getInstance()->getRouter();
        $propertyType = $this->request->getParam('property_type', '');
        //
        // init action form
        //
        $form = new Settings_Form_PropertyType();

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $modelPropertyType = new Model_PropertyType();

                $data = array(
                    'property_type' => $propertyType,
                    'company_id' => CheckAuth::getCompanySession(),
                );


                $success = $modelPropertyType->insert($data);


                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in  Property Type"));
                }

                echo 1;
                exit;
            } else {
                $messages = $form->getElement('property_type')->getErrorMessages();
                if ($messages) {
                    $form->getElement('property_type')->clearErrorMessages();
                    foreach ($messages as $message) {
                        $form->getElement('property_type')->addError($message);
                    }
                }
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('property-type/add_edit.phtml');
        exit;
    }

    /**
     * edit the item action
     */
    public function editAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsPropertyTypeEdit'));

        //
        // get request parameters
        //
        $router = Zend_Controller_Front::getInstance()->getRouter();

        $propertyType = $this->request->getParam('property_type', '');
        $propertyTypeId = $this->request->getParam('id', '');
        //
        // init action form
        //
        $modelPropertyType = new Model_PropertyType();
        $property = $modelPropertyType->getById($propertyTypeId);

        $options = array(
            'mode' => 'update',
            'property' => $property
        );
        $form = new Settings_Form_PropertyType($options);

        //
        // handling the insertion process
        //                       

        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'property_type' => $propertyType,
                    'company_id' => CheckAuth::getCompanySession()
                );
                $success = $modelPropertyType->updateById($propertyTypeId, $data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Changed successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Property Type"));
                }
                echo 1;
                exit;
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('property-type/add_edit.phtml');
        exit;
    }

    public function deleteAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsPropertyTypeDelete'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);

        $modelPropertyType = new Model_PropertyType();

        $isNotRelated = $modelPropertyType->checkBeforeDeletePropertyType($id);

        if ($isNotRelated) {
            $modelPropertyType->deleteById($id);
        } else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "could not be deleted ,This Item used in other Place "));
        }

        $this->_redirect($this->router->assemble(array(), 'settingsPropertyTypes'));
    }

}

