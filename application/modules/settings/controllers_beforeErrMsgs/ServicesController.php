<?php

class Settings_ServicesController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsServices';

        BreadCrumbs::setLevel(2, 'Service List');
    }

    /**
     * Items list action
     */
    public function indexAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsServicesList'));

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'service_id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        //
        // get data list
        //
        $serviceObj = new Model_Services();
        $this->view->data = $serviceObj->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);

        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
    }

    /**
     * Add new item action
     */
    public function addAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsServicesAdd'));

        //
        // get request parameters
        //
        $serviceName = $this->request->getParam('service_name');
        $companyId = CheckAuth::getCompanySession();
        $minPrice = $this->request->getParam('min_price');
        $priceEquasion = $this->request->getParam('price_equasion');
        $router = Zend_Controller_Front::getInstance()->getRouter();

        //
        // init action form
        //
        $form = new Settings_Form_Services();

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $serviceObj = new Model_Services();
                $data = array(
                    'service_name' => $serviceName,
                    'company_id' => $companyId,
                    'min_price' => $minPrice,
                    'created' => time(),
                    'price_equasion' => $priceEquasion
                );

                $success = $serviceObj->insert($data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Service"));
                }

                //update General Contractor
                $modelUser = new Model_User();
                $modelUser->updateGeneralContractor();

                //$this->_redirect($this->router->assemble(array(), 'settingsServiceList'));
                echo 1;
                exit;
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('services/add_edit.phtml');
        exit;
    }

    public function deleteAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsServicesDelete'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
        if ($id) {
            $ids[] = $id;
        }

        $serviceObj = new Model_Services();
        foreach ($ids as $id) {
            if (CheckAuth::checkIfCanHandelAllCompany('service', $id)) {
                $isNotRelated = $serviceObj->checkBeforeDeleteService($id);
                if ($isNotRelated) {
                    $serviceObj->deleteById($id);
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "could not be deleted ,This Item used in other Place "));
                }
            }
        }
        $this->_redirect($this->router->assemble(array(), 'settingsServicesList'));
    }

    public function editAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsServicesEdit'));

        //
        // get request parameters
        //
        $serviceName = $this->request->getParam('service_name');
        $minPrice = $this->request->getParam('min_price');
        $priceEquasion = $this->request->getParam('price_equasion');
        $id = $this->request->getParam('id');


        if (!CheckAuth::checkIfCanHandelAllCompany('service', $id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }


        //
        // validation
        //
        $serviceObj = new Model_Services();
        $service = $serviceObj->getById($id);
        if (!$service) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect($this->router->assemble(array(), 'settingsServicesList'));
            return;
        }


        //
        // init action form
        //
        $form = new Settings_Form_Services(array('mode' => 'update', 'service' => $service));


        //
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'service_name' => $serviceName,
                    'min_price' => $minPrice,
                    'price_equasion' => $priceEquasion
                );

                $success = $serviceObj->updateById($id, $data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Service"));
                }

                //update General Contractor
                $modelUser = new Model_User();
                $modelUser->updateGeneralContractor();

                //$this->view->successMessage = 'Updated Successfully';
                echo 1;
                exit;
                //$this->_redirect($this->router->assemble(array(), 'settingsServiceList'));
                //return;
            }
        }

        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('services/add_edit.phtml');
        exit;
    }

}

