<?php

class Settings_SettingsController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settings'));

        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        BreadCrumbs::setHomeLink('Settings', '/settings');
    }

    /**
     * Items list action
     */
    public function indexAction() {
        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsHome';
    }

    public function settingsLocationAction() {
        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsLocation';
        BreadCrumbs::setLevel(1, 'Settings Location');
    }

    public function settingsPaymentAction() {
        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsPayment';
        BreadCrumbs::setLevel(1, 'Settings Payment');
    }

    public function settingsUserAction() {
        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsUser';
        BreadCrumbs::setLevel(1, 'Settings User');
    }
	public function settingsCompanyAction() {
        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsCompany';
        BreadCrumbs::setLevel(1, 'Settings Companies');
    }

    public function settingsServicesAction() {
        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsServices';
        BreadCrumbs::setLevel(1, 'Settings Services');
    }

    public function settingsTypeAction() {
        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsType';
        BreadCrumbs::setLevel(1, 'Settings Type');
    }

    public function settingsEmailAction() {
        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsEmail';
        BreadCrumbs::setLevel(1, 'Settings Email');
    }
    
    public function settingsLabelAction() {
        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsLabel';
        BreadCrumbs::setLevel(1, 'Settings Label');
    }
	
	public function settingsSecurityAction() {
        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsSecurity';
        BreadCrumbs::setLevel(1, 'Settings Security');
    }

}

