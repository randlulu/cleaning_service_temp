<?php

class Settings_BankController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsPayment';

        BreadCrumbs::setLevel(2, 'Bank Accounts');
    }

    /**
     * Items list action
     */
    public function indexAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsBankList'));

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'bank_id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if($filters){
            foreach ($filters as &$filter){
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }
        
        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        //
        // get data list
        //
        $bankObj = new Model_Bank();
        $this->view->data = $bankObj->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);

        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
    }

    /**
     * Add new item action
     */
    public function addAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsBankAdd'));

        //
        // get request parameters
        //
        $companyId = $this->request->getParam('company_id');
        $bsb = $this->request->getParam('bsb');
        $bankName = $this->request->getParam('bank_name');
        $account_name = $this->request->getParam('account_name');
        $account_number = $this->request->getParam('account_number');
        $router = Zend_Controller_Front::getInstance()->getRouter();

        //
        // init action form
        //
        $form = new Settings_Form_Bank();

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $bankObj = new Model_Bank();
                $data = array(
                    'company_id' => $companyId,
                    'bsb' => $bsb,
                    'bank_name' => $bankName,
                    'account_name' => $account_name,
                    'account_number' => $account_number
                );
				
				

                $success = $bankObj->insert($data);
				

                if ($success) {
				
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
				
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Bank"));
                }
                //$this->_redirect($this->router->assemble(array(), 'settingsBankList'));
                echo 1;
                exit;
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('bank/add_edit.phtml');
        exit;
    }

    public function deleteAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsBankDelete'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
        if ($id) {
            $ids[] = $id;
        }

        $bankObj = new Model_Bank();
        foreach ($ids as $id) {
            $bankObj->deleteById($id);
        }
        $this->_redirect($this->router->assemble(array(), 'settingsBankList'));
    }

    public function editAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsBankEdit'));

        //
        // get request parameters
        //
        $companyId = $this->request->getParam('company_id');
        $bsb = $this->request->getParam('bsb');
        $bankName = $this->request->getParam('bank_name');
        $id = $this->request->getParam('id');
        $account_name = $this->request->getParam('account_name');
        $account_number = $this->request->getParam('account_number');

        //
        // validation
        //
        $bankObj = new Model_Bank();
        $bank = $bankObj->getById($id);
        if (!$bank) {
            $this->_redirect($this->router->assemble(array(), 'settingsBankList'));
            return;
        }


        //
        // init action form
        //
        $form = new Settings_Form_Bank(array('mode' => 'update', 'bank' => $bank));


        //
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'company_id' => $companyId,
                    'bsb' => $bsb,
                    'bank_name' => $bankName,
                    'account_name' => $account_name,
                    'account_number' => $account_number
                );

                $success = $bankObj->updateById($id, $data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Bank"));
                }

                echo 1;
                exit;
                //$this->_redirect($this->router->assemble(array(), 'settingsBankList'));
                //return;
            }
        }

        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('bank/add_edit.phtml');
        exit;
    }

}

