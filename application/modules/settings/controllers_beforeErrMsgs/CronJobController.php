<?php

class Settings_CronJobController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsEmail';

        BreadCrumbs::setLevel(2, 'Cron Jobs');
    }

    /**
     * Items list action
     */
    public function indexAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsCronJobList'));

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        //
        // get data list
        //
        $modelCronJob = new Model_CronJob();
        $this->view->data = $modelCronJob->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);

        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
    }

    /**
     * Add new item action
     */
    public function addAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsCronJobAdd'));

        if (!get_config('under_construction')) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permession."));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // get request parameters
        //
        $cronJobName = $this->request->getParam('cronjob_name');
        $phpCode = $this->request->getParam('php_code');
        $every = $this->request->getParam('every');
        $running = $this->request->getParam('running');

        //
        // init action form
        //
        $form = new Settings_Form_CronJob();

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $modelCronJob = new Model_CronJob();
                $data = array(
                    'cronjob_name' => $cronJobName,
                    'php_code' => str_replace("\r\n", "\n", $phpCode),
                    'every' => $every,
                    'running' => $running
                );

                $success = $modelCronJob->insert($data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in CronJob"));
                }

                echo 1;
                exit;
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('cron-job/add_edit.phtml');
        exit;
    }

    public function deleteAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsCronJobDelete'));

        if (!get_config('under_construction')) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permession."));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
        if ($id) {
            $ids[] = $id;
        }

        $modelCronJob = new Model_CronJob();
        foreach ($ids as $id) {
            $modelCronJob->deleteCronJob($id);
        }
        $this->_redirect($this->router->assemble(array(), 'settingsCronJobList'));
    }

    public function editAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsCronJobEdit'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id');
        $cronJobName = $this->request->getParam('cronjob_name');
        $phpCode = $this->request->getParam('php_code');
        $every = $this->request->getParam('every');
        $running = $this->request->getParam('running');

        //
        // validation
        //
        $modelCronJob = new Model_CronJob();
        $oldCronJob = $modelCronJob->getById($id);
        if (!$oldCronJob) {
            $this->_redirect($this->router->assemble(array(), 'settingsCronJobList'));
            return;
        }


        //
        // init action form
        //
        $form = new Settings_Form_CronJob(array('mode' => 'update', 'oldCronJob' => $oldCronJob));


        //
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'every' => $every,
                    'running' => $running
                );

                if (get_config('under_construction')) {
                    $data['cronjob_name'] = $cronJobName;
                    $data['php_code'] = str_replace("\r\n", "\n", $phpCode);
                }

                $success = $modelCronJob->updateById($id, $data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in CronJob"));
                }

                echo 1;
                exit;
            }
        }

        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('cron-job/add_edit.phtml');
        exit;
    }

    public function addDescriptionAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsCronjobAddDescription'));

        //
        // get request parameters
        //
        $description = $this->request->getParam('description');
        $id = $this->request->getParam('id');

        //
        // validation
        //
        $modelCronJob = new Model_CronJob();
        $oldCronJob = $modelCronJob->getById($id);
        if (!$oldCronJob) {
            $this->_redirect($this->router->assemble(array(), 'settingsCronJobList'));
            return;
        }

        //
        // init action form
        //
        $form = new Settings_Form_CronJobAddDescription(array('oldCronJob' => $oldCronJob));

        //
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $success = $modelCronJob->updateById($id, array('description' => $description), false);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in CronJob"));
                }

                echo 1;
                exit;
            }
        }

        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('cron-job/add-description.phtml');
        exit;
    }

    public function viewAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsCronJobView'));

        //
        // get request parameters
        //
      
        $id = $this->request->getParam('id');

        //
        // validation
        //
       
        $modelCronJob = new Model_CronJob();
        $cronJob = $modelCronJob->getById($id);

        if (!$cronJob) {
            $this->_redirect($this->router->assemble(array(), 'settingsCronJobList'));
            return;
        }

        $this->view->cronJob = $cronJob;


        //
        // render views
        //
        echo $this->view->render('cron-job/view.phtml');
        exit;
    }
	
	public function runNowAction() {
		
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsCronJobRunNow'));

        //
        // get request parameters
        //
      
        $id = $this->request->getParam('id');
		
        //
        // validation
        //
       
        $modelCronJob = new Model_CronJob();
        $cronJob = $modelCronJob->getById($id);
		$cronJobCode=$cronJob['php_code'];
		
		eval($cronJobCode);
		$this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "run successfully"));
	
		$this->_redirect($this->router->assemble(array(), 'settingsCronJobList'));
        return;
      
    }

}

