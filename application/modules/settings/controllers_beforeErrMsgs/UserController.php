<?php

class Settings_UserController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
		$this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsUser';
		BreadCrumbs::setLevel(2, 'Users');
        

        
    }

    /**
     * Items list action
     */
    public function indexAction() {
	
		
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsUserList'));

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'user_id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        //
        // get data list
        //
        $modelUser = new Model_User();
        $this->view->data = $modelUser->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);

        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
    }

    /**
     * Add new item action
     */
    public function addAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsUserAdd'));

        //
        // get request parameters
        //
        $username = $this->request->getParam('username');
        $first_name = $this->request->getParam('first_name');
        $last_name = $this->request->getParam('last_name');
        $display_name = $this->request->getParam('display_name');
        $password = $this->request->getParam('password');
        $roleId = $this->request->getParam('role_id');
        $email1 = $this->request->getParam('email1');
        $email2 = $this->request->getParam('email2');
        $email3 = $this->request->getParam('email3');
		$systemEmail = $this->request->getParam('systemEmail');
        $mobile1 = $this->request->getParam('mobile1');
        $mobile2 = $this->request->getParam('mobile2');
        $mobile3 = $this->request->getParam('mobile3');
        $phone1 = $this->request->getParam('phone1');
        $phone2 = $this->request->getParam('phone2');
        $phone3 = $this->request->getParam('phone3');
        $fax = $this->request->getParam('fax');
        $emergencyPhone = $this->request->getParam('emergency_phone');
        $unitLotNumber = $this->request->getParam('unit_lot_number');
        $streetNumber = $this->request->getParam('street_number');
        $streetAddress = $this->request->getParam('street_address');
        $suburb = $this->request->getParam('suburb');
        $state = $this->request->getParam('state');
        $postcode = $this->request->getParam('postcode');
        $po_box = $this->request->getParam('po_box');
        $cityId = $this->request->getParam('city_id');
        $countryId = $this->request->getParam('country_id');
        $companyId = $this->request->getParam('company_id');

        if (get_config('remove_white_spacing')) {
            $mobile1 = preparer_number($mobile1);
            $mobile2 = preparer_number($mobile2);
            $mobile3 = preparer_number($mobile3);
            $phone1 = preparer_number($phone1);
            $phone2 = preparer_number($phone2);
            $phone3 = preparer_number($phone3);
        }

        $router = Zend_Controller_Front::getInstance()->getRouter();

        if (isset($roleId)) {
            $modelAuthRole = new Model_AuthRole();
            $role = $modelAuthRole->getById($roleId);
            if ($role['role_name'] == 'super_admin' && !CheckAuth::checkCredential(array('canHandleAllCompanies'))) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You have entered a bad option !!"));
                echo 1;
                exit;
            }
        }
        //
        // init action form
        //
        $form = new Settings_Form_User(array('country_id' => $countryId, 'state' => $state));

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) {
            // // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $modelUser = new Model_User();
                $data = array(
                    'display_name' => $display_name,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'username' => $username,
                    'password' => sha1($password),
                    'user_code' => sha1($username),
                    'last_login' => time(),
                    'created' => time(),
                    'city_id' => $cityId,
                    'role_id' => $roleId,
                    'email1' => $email1,
                    'email2' => $email2,
                    'email3' => $email3,
					'system_email' => $systemEmail,
                    'mobile1' => $mobile1,
                    'mobile2' => $mobile2,
                    'mobile3' => $mobile3,
                    'phone1' => $phone1,
                    'phone2' => $phone2,
                    'phone3' => $phone3,
                    'fax' => $fax,
                    'emergency_phone' => $emergencyPhone,
                    'unit_lot_number' => $unitLotNumber,
                    'street_number' => $streetNumber,
                    'street_address' => $streetAddress,
                    'suburb' => $suburb,
                    'state' => $state,
                    'postcode' => $postcode,
                    'po_box' => $po_box
                );

                //
                //get role id for contractor
                //
                $userId = $modelUser->insert($data);

                $dataCompany = array(
                    'user_id' => $userId,
                    'company_id' => $companyId,
                    'created' => time()
                );
                $userCompaniesModel = new Model_UserCompanies();
                $userCompaniesModel->insert($dataCompany);

                if ($userId) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in User"));
                }

                echo 1;
                exit;
            }
        }

        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('user/add_edit.phtml');
        exit;
    }

    public function deleteAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsUserDelete'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
        if ($id) {
            $ids[] = $id;
        }

        $modelUser = new Model_User();
        foreach ($ids as $id) {
            if (CheckAuth::checkIfCanHandelAllCompany('user', $id)) {
                $isChecked = $modelUser->checkBeforeDeleteUser($id);
                if ($isChecked) {
                    $modelUser->deleteById($id);
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "could not be deleted ,This Item used in other Place "));
                }
            }
        }
        $this->_redirect($this->router->assemble(array(), 'settingsUserList'));
    }

    public function editAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsUserEdit'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id');
        $display_name = $this->request->getParam('display_name');
        $first_name = $this->request->getParam('first_name');
        $last_name = $this->request->getParam('last_name');
        $username = $this->request->getParam('username');
        //$password = $this->request->getParam('password');
        $roleId = $this->request->getParam('role_id');
        $email1 = $this->request->getParam('email1');
        $email2 = $this->request->getParam('email2');
        $email3 = $this->request->getParam('email3');
		$systemEmail = $this->request->getParam('systemEmail');
        $mobile1 = $this->request->getParam('mobile1');
        $mobile2 = $this->request->getParam('mobile2');
        $mobile3 = $this->request->getParam('mobile3');
        $phone1 = $this->request->getParam('phone1');
        $phone2 = $this->request->getParam('phone2');
        $phone3 = $this->request->getParam('phone3');
        $fax = $this->request->getParam('fax');
        $emergencyPhone = $this->request->getParam('emergency_phone');
        $unitLotNumber = $this->request->getParam('unit_lot_number');
        $streetNumber = $this->request->getParam('street_number');
        $streetAddress = $this->request->getParam('street_address');
        $suburb = $this->request->getParam('suburb');
        $state = $this->request->getParam('state');
        $postcode = $this->request->getParam('postcode');
        $po_box = $this->request->getParam('po_box');
        $cityId = $this->request->getParam('city_id');
        $countryId = $this->request->getParam('country_id');
        $companyId = $this->request->getParam('company_id');

        if (get_config('remove_white_spacing')) {
            $mobile1 = preparer_number($mobile1);
            $mobile2 = preparer_number($mobile2);
            $mobile3 = preparer_number($mobile3);
            $phone1 = preparer_number($phone1);
            $phone2 = preparer_number($phone2);
            $phone3 = preparer_number($phone3);
        }

        if (isset($roleId)) {
            $modelAuthRole = new Model_AuthRole();
            $role = $modelAuthRole->getById($roleId);
            if ($role['role_name'] == 'super_admin' && !CheckAuth::checkCredential(array('canHandleAllCompanies'))) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You have entered a bad option !!"));
                echo 1;
                exit;
            }
        }
        if (!CheckAuth::checkIfCanHandelAllCompany('user', $id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            echo 1;
            exit;
        }

        //
        // validation
        //
        $modelUser = new Model_User();
        $user = $modelUser->getById($id);
        if (!$user) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));

            $this->_redirect($this->router->assemble(array(), 'settingsUserList'));
            return;
        }

        //
        //get user company
        //
        $userCompaniesModel = new Model_UserCompanies();
        $userCompanies = $userCompaniesModel->getByUserId($id);


        //
        // init action form
        //
        $form = new Settings_Form_User(array('mode' => 'update', 'user' => $user, 'country_id' => $countryId, 'state' => $state, 'company_id' => $userCompanies['company_id']));


        //
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'display_name' => $display_name,
                    'username' => $username,
                    'city_id' => $cityId,
                    'role_id' => $roleId,
                    'email1' => $email1,
                    'email2' => $email2,
                    'email3' => $email3,
					'system_email' => $systemEmail,
                    'mobile1' => $mobile1,
                    'mobile2' => $mobile2,
                    'mobile3' => $mobile3,
                    'phone1' => $phone1,
                    'phone2' => $phone2,
                    'phone3' => $phone3,
                    'fax' => $fax,
                    'emergency_phone' => $emergencyPhone,
                    'unit_lot_number' => $unitLotNumber,
                    'street_number' => $streetNumber,
                    'street_address' => $streetAddress,
                    'suburb' => $suburb,
                    'state' => $state,
                    'postcode' => $postcode,
                    'po_box' => $po_box
                );

                $success = $modelUser->updateById($id, $data);

                $userCompaniesModel = new Model_UserCompanies();
                $userCompanies = $userCompaniesModel->getByUserId($id);

                $dataCompany = array(
                    'user_id' => $id,
                    'company_id' => $companyId,
                    'created' => time()
                );
                if ($userCompanies) {
                    $userCompaniesModel->updateById($userCompanies['id'], $dataCompany);
                } else {
                    $userCompaniesModel->insert($dataCompany);
                }

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in User"));
                }

                echo 1;
                exit;
            }
        }

        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('user/add_edit.phtml');
        exit;
    }


       public function editContractorAddressAction() {
       
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsUserEdit'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id');
        $display_name = $this->request->getParam('display_name');
        $username = $this->request->getParam('username');
        //$password = $this->request->getParam('password');
        //$roleId = $this->request->getParam('role_id');
        //echo $roleId ;
        //exit;
        $email1 = $this->request->getParam('email1');
        $email2 = $this->request->getParam('email2');
        $email3 = $this->request->getParam('email3');
        $systemEmail = $this->request->getParam('systemEmail');
        $mobile1 = $this->request->getParam('mobile1');
        $mobile2 = $this->request->getParam('mobile2');
        $mobile3 = $this->request->getParam('mobile3');
        $phone1 = $this->request->getParam('phone1');
        $phone2 = $this->request->getParam('phone2');
        $phone3 = $this->request->getParam('phone3');
        $fax = $this->request->getParam('fax');
        $emergencyPhone = $this->request->getParam('emergency_phone');
        $unitLotNumber = $this->request->getParam('unit_lot_number');
        $streetNumber = $this->request->getParam('street_number');
        $streetAddress = $this->request->getParam('street_address');
        $suburb = $this->request->getParam('suburb');
        $state = $this->request->getParam('state');
        $postcode = $this->request->getParam('postcode');
        $po_box = $this->request->getParam('po_box');
        $cityId = $this->request->getParam('city_id');
        $countryId = $this->request->getParam('country_id');
        //$companyId = $this->request->getParam('company_id');

        if (get_config('remove_white_spacing')) {
            $mobile1 = preparer_number($mobile1);
            $mobile2 = preparer_number($mobile2);
            $mobile3 = preparer_number($mobile3);
            $phone1 = preparer_number($phone1);
            $phone2 = preparer_number($phone2);
            $phone3 = preparer_number($phone3);
        }

        /*if (isset($roleId)) {
            $modelAuthRole = new Model_AuthRole();
            $role = $modelAuthRole->getById($roleId);
            if ($role['role_name'] == 'super_admin' && !CheckAuth::checkCredential(array('canHandleAllCompanies'))) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You have entered a bad option !!"));
                echo 1;
                exit;
            }
        }*/
        if (!CheckAuth::checkIfCanHandelAllCompany('user', $id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            echo 1;
            exit;
        }

        //
        // validation
        //
        $modelUser = new Model_User();
        $user = $modelUser->getById($id);
        
        if (!$user) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));

            $this->_redirect($this->router->assemble(array(), 'settingsUserList'));
            return;
        }

        //
        //get user company
        //
        $userCompaniesModel = new Model_UserCompanies();
        $userCompanies = $userCompaniesModel->getByUserId($id);


        //
        // init action form
        //
        $form = new Settings_Form_editContractorAddress(array('user' => $user, 'country_id' => $countryId, 'state' => $state));

        
            //
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'email1' => $email1,
                    'email2' => $email2,
                    'email3' => $email3,
                    'system_email' => $systemEmail,
                    'mobile1' => $mobile1,
                    'mobile2' => $mobile2,
                    'mobile3' => $mobile3,
                    'phone1' => $phone1,
                    'phone2' => $phone2,
                    'phone3' => $phone3,
                    'fax' => $fax,
                    'emergency_phone' => $emergencyPhone,
                    'unit_lot_number' => $unitLotNumber,
                    'street_number' => $streetNumber,
                    'street_address' => $streetAddress,
                    'suburb' => $suburb,
                    'state' => $state,
                    'postcode' => $postcode,
                    'po_box' => $po_box
                );

                $success = $modelUser->updateById($id, $data);

                /*$userCompaniesModel = new Model_UserCompanies();
                $userCompanies = $userCompaniesModel->getByUserId($id);

                $dataCompany = array(
                    'user_id' => $id,
                    'company_id' => $companyId,
                    'created' => time()
                );
                if ($userCompanies) {
                    $userCompaniesModel->updateById($userCompanies['id'], $dataCompany);
                } else {
                    $userCompaniesModel->insert($dataCompany);
                }*/

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in User"));
                }

                echo 1;
                exit;
            }
        }
        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('user/edit_contractor_address.phtml');
        exit;
       
   }

    public function changePasswordAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settings'));


        $id = $this->request->getParam('id');
        $new_password = $this->request->getParam('new_password');
        $confirm_new_password = $this->request->getParam('confirm_new_password');

        if (!CheckAuth::checkIfCanHandelAllCompany('user', $id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            echo 1;
            exit;
        }


        $modelUser = new Model_User();
        $user = $modelUser->getById($id);
        if (!$user) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
            return;
        }


        $form = new Settings_Form_ChangePassword(array('user' => $user));
        $this->view->form = $form;

        if ($this->request->isPost()) {
            if ($form->isValid($this->request->getPost())) {

                $data = array(
                    'password' => sha1($new_password)
                );
                $modelUser->updateById($id, $data);

                echo 1;
                exit;
            }
        }

        //
        // render views
        //
        echo $this->view->render('user/change_password.phtml');
        exit;
    }

    public function updateGeneralContractorAction() {

        //update General Contractor
        $modelUser = new Model_User();
        $modelUser->updateGeneralContractor();
    }

    public function loginAsThisUserAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('loginAsThisUser'));

        $id = $this->request->getParam('id');

        $modelUser = new Model_User();
        $user = $modelUser->getById($id);
		/*if($_SERVER['REMOTE_ADDR'] == '176.106.46.142'){
                                          print_r($user );
										  exit;
        }*/ 
        $authrezed = $this->getAuthrezed();

        $authrezed->setIdentity($user['email1']);
        $authrezed->setCredential($user['password']);

        $auth = Zend_Auth::getInstance();
        $result = $auth->authenticate($authrezed);

        if ($result->isValid()) {
            $identity = $authrezed->getResultRowObject();

            $authStorge = $auth->getStorage();
            $authStorge->write($identity);

            CheckAuth::afterlogin();
        }

        $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
        $this->_redirect($this->router->assemble(array(), 'Login'));
    }

    public function getAuthrezed() {
        $authrezed = new Zend_Auth_Adapter_DbTable(Zend_Db_Table::getDefaultAdapter());
        $authrezed->setTableName('user')
                ->setIdentityColumn('email1')
                ->setCredentialColumn('password');
                //->setCredentialTreatment('? AND active = "TRUE"');

        return $authrezed;
    }

    public function activateOrDeactivateUsersAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('activateOrDeactivateUsers'));

        //
        //Convert complaint type status
        //
        $id = $this->request->getParam('id', 0);

        if (!CheckAuth::checkIfCanHandelAllCompany('user', $id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This user not belongs to your Company"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $modelUser = new Model_User();
        $user = $modelUser->getById($id);

        if ($user['active'] == 'TRUE') {
            $data['active'] = 'FALSE';
        } else {
            $data['active'] = 'TRUE';
        }

        $modelUser->updateById($id, $data);


        $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
    }
	
	
	public function getCompanyInquiryEmailAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('activateOrDeactivateUsers'));

        //
        //Convert complaint type status
        //
        $company_id = $this->request->getParam('company_id', 0);

        /*if (!CheckAuth::checkIfCanHandelAllCompany('user', $id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This user not belongs to your Company"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }*/
		
		$companies_model = new Model_Companies();
		$companyEnquiriesEmail = $companies_model->getEnquiryEmailByCompanyId($company_id);

        
		echo json_encode($companyEnquiriesEmail);
        exit;
    }
	
	
	public function toggleActivateOrDeactivateUsersAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('activateOrDeactivateUsers'));

        //
        //Convert complaint type status
        //
        $id = $this->request->getParam('id', 0);

        if (!CheckAuth::checkIfCanHandelAllCompany('user', $id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This user not belongs to your Company"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $modelUser = new Model_User();
        $user = $modelUser->getById($id);

        if ($user['active'] == 'TRUE') {
            $data['active'] = 'FALSE';
        } else {
            $data['active'] = 'TRUE';
        }

        $modelUser->updateById($id, $data);

        echo json_encode($data);
        exit;
    }
	

	
}

