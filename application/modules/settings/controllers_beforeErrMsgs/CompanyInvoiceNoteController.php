<?php

class Settings_CompanyInvoiceNoteController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsType';

        BreadCrumbs::setLevel(2, 'Company Invoice Note');
    }

    /**
     * Items list action
     */
    public function indexAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsCompanyInvoiceNoteList'));



        //
        // get request parameters
        //
        $companyId = $this->request->getParam('company_id');
        $orderBy = $this->request->getParam('sort', 'id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if($filters){
            foreach ($filters as &$filter){
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        if ($companyId != CheckAuth::getCompanySession()) {
            If (!CheckAuth::checkCredential(array('canHandleAllCompanies'))) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        //
        // get data list
        //
        $filters['company_id'] = $companyId;
        $modelCompanyInvoiceNote = new Model_CompanyInvoiceNote();
        $this->view->data = $modelCompanyInvoiceNote->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);

        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
        $this->view->company_id = $companyId;
    }

    /**
     * Add new item action
     */
    public function addAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsCompanyInvoiceNoteAdd'));

        //
        // get request parameters
        //
        $companyId = $this->request->getParam('company_id');
        $note = $this->request->getParam('note');
        $router = Zend_Controller_Front::getInstance()->getRouter();


        if ($companyId != CheckAuth::getCompanySession()) {
            If (!CheckAuth::checkCredential(array('canHandleAllCompanies'))) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            }
        }

        //
        // init action form
        //
        $form = new Settings_Form_CompanyInvoiceNote(array('company_id' => $companyId));

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $modelCompanyInvoiceNote = new Model_CompanyInvoiceNote();
                $data = array(
                    'note' => $note,
                    'company_id' => $companyId
                );

                $success = $modelCompanyInvoiceNote->insert($data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Company Invoice Note"));
                }
                echo 1;
                exit;
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('company-invoice-note/add_edit.phtml');
        exit;
    }

    public function deleteAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsCompanyInvoiceNoteDelete'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
        $companyId = $this->request->getParam('company_id');


        if ($companyId != CheckAuth::getCompanySession()) {
            If (!CheckAuth::checkCredential(array('canHandleAllCompanies'))) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            }
        }

        if ($id) {
            if (CheckAuth::checkIfCanHandelAllCompany('company_invoice_note', $id)) {
                $ids[] = $id;
            }
        }

        $modelCompanyInvoiceNote = new Model_CompanyInvoiceNote();


        foreach ($ids as $id) {
            $modelCompanyInvoiceNote->deleteById($id);
        }
        $this->_redirect($this->router->assemble(array('company_id' => $companyId), 'settingsCompanyInvoiceNoteList'));
    }

    public function editAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsCompanyInvoiceNoteEdit'));



        //
        // get request parameters
        //
        $note = $this->request->getParam('note');
        $id = $this->request->getParam('id');
        $companyId = $this->request->getParam('company_id');


        if ($companyId != CheckAuth::getCompanySession()) {
            If (!CheckAuth::checkCredential(array('canHandleAllCompanies'))) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            }
        }

        if (!CheckAuth::checkIfCanHandelAllCompany('company_invoice_note', $id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // validation
        //
        $modelCompanyInvoiceNote = new Model_CompanyInvoiceNote();
        $companyInvoiceNote = $modelCompanyInvoiceNote->getById($id);
        if (!$companyInvoiceNote) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect($this->router->assemble(array(), 'settingsCompanyInvoiceNoteList'));
            return;
        }


        //
        // init action form
        //
        $form = new Settings_Form_CompanyInvoiceNote(array('mode' => 'update', 'company_id' => $companyId, 'companyInvoiceNote' => $companyInvoiceNote));


        //
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'note' => $note,
                    'company_id' => $companyId
                );

                $success = $modelCompanyInvoiceNote->updateById($id, $data);

                //$this->view->successMessage = 'Updated Successfully';
                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Company Invoice Note"));
                }
                echo 1;
                exit;
            }
        }

        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('company-invoice-note/add_edit.phtml');
        exit;
    }

}

