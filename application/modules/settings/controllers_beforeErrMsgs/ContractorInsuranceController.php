<?php

class Settings_ContractorInsuranceController extends Zend_Controller_Action {

    private $request;
    private $router;
    private $contractor_info_id;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
        $this->contractor_info_id = $this->request->getParam('contractor_info_id');

        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsUser';

        //
        // get data
        //
        $contractorInfoObj = new Model_ContractorInfo();
        $contractorInfo = $contractorInfoObj->getById($this->contractor_info_id);

        BreadCrumbs::setLevel(4, 'Contractor Insurance');
    }

    /**
     * Items list action
     */
    public function indexAction() {
        //
        // check Auth for logged user
        //
        ///CheckAuth::checkPermission(array('settingsContractorInsuranceList'));

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'contractor_insurance_id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if($filters){
            foreach ($filters as &$filter){
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];


        //
        //get user email by id
        //
        $filters['contractor_info_id'] = $this->contractor_info_id;


        //
        // get data list
        //
		
        $contractorInsuranceObj = new Model_ContractorInsurance();
        $this->view->data = $contractorInsuranceObj->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);

        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
        $this->view->contractor_info_id = $this->contractor_info_id;
		
    }

    /**
     * Add new item action
     */
    public function addAction() {

        //
        // check Auth for logged user
        //
        //CheckAuth::checkPermission(array('settingsContractorInsuranceAdd'));

        //
        // get request parameters
        //
        
		$insurance_policy_number = $this->request->getParam('insurance_policy_number');
        $insurance_policy_start = $this->request->getParam('insurance_policy_start');
        $insurance_policy_expiry = $this->request->getParam('insurance_policy_expiry');
        $insurance_listed_services_covered = $this->request->getParam('insurance_listed_services_covered');
        $insurance_type = $this->request->getParam('insurance_type');
		
        $router = Zend_Controller_Front::getInstance()->getRouter();
        
		

        if (!CheckAuth::checkIfCanHandelAllCompany('contractor_info', $this->contractor_info_id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // init action form
        //
         $form = new Settings_Form_InsuranceInfo(array('contractor_info_id' => $this->contractor_info_id));
         $attachmentObj = new Model_Attachment();
         $ContractorInsuranceAttachmentObj = new Model_ContractorInsuranceAttachment();
		 
        //
        // handling the insertion process
        //
		
		$loggedUser = CheckAuth::getLoggedUser();
		
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $contractorInsuranceObj = new Model_ContractorInsurance();
                $data = array(
                    'insurance_policy_number' => $insurance_policy_number,
                    'insurance_policy_start' => strtotime($insurance_policy_start),
                    'insurance_policy_expiry' => strtotime($insurance_policy_expiry),
                    'insurance_listed_services_covered' => $insurance_listed_services_covered,
                    'insurance_type' => $insurance_type,
                    'contractor_info_id' => $this->contractor_info_id,
                    'created' => time(),
                    'created_by' => $loggedUser['user_id'],
                );

                $success = $contractorInsuranceObj->insert($data);

                if ($success) {
				    $upload = new Zend_File_Transfer_Adapter_Http();
                    $files  = $upload->getFileInfo();
					$counter = 0 ;
					foreach($files as $file => $fileInfo) {
					  if ($upload->isUploaded($file)){
                        if ($upload->receive($file)){
						   $counter = $counter + 1;
						   $info = $upload->getFileInfo($file);
                           $source  = $info[$file]['tmp_name'];
                           $imageInfo = pathinfo($source);
                           $ext = $imageInfo['extension'];
						   $size = $info[$file]['size'];
						   $type = $info[$file]['type'];
				           $dir = get_config('attachment') . '/';
                           $subdir = date('Y/m/d/');
                           $fullDir = $dir . $subdir;
                           if (!is_dir($fullDir)) {
                              mkdir($fullDir, 0777, true);
                           }
                        $loggedUser = CheckAuth::getLoggedUser();							
						$data = array(
							 'created_by'=>$loggedUser['user_id'] 
							 ,'created'=>time()
							 ,'size' => $size
							 ,'type'=>$type
						 );
						 
						
						 
						 $id = $attachmentObj->insert($data);
						 $ContractorInsuranceAttachmentObj->insert(array('contractor_insurance_id'=>$success,'attachment_id'=>$id));
						 $fileName = $success.'_'.$counter.'.'. $ext;
						 $image_saved = copy($source, $fullDir . $fileName);
						 $typeParts = explode("/",$type);
						 if($typeParts[0] == 'image'){
						    $thumbName = $success.'_thumbnail_'.$counter.'.'. $ext;
						    ImageMagick::create_thumbnail($source, $fullDir . $thumbName, 78, 64);
						 }else{
						   $thumbName = $success.'_'.$counter.'.jpg';
						   ImageMagick::convertFile($source.'[0]', $fullDir . $thumbName);
						 }
						 
						 $Updatedata = array(
                          'path' => $fullDir . $fileName,
                          'file_name' => $fileName,
						  'thumbnail_file' => $fullDir . $thumbName
                          );
						 $attachmentObj->updateById($id,$Updatedata);
						 
						  if ($image_saved) {
								if (file_exists($source)) {
									unlink($source);
								}               
							}
						}
					 }	
					}
				   
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Contractor Insurance"));
                }
                //$this->_redirect($this->router->assemble(array(), 'settingsUserEmailList'));
                echo 1;
                exit;
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('contractor-insurance/add-edit.phtml');
        exit;
    }

    public function deleteAction() {

        //
        // check Auth for logged user
        //
        //CheckAuth::checkPermission(array('settingsContractorInsuranceDelete'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
        if ($id) {
            $ids[] = $id;
        }

        $contractorInsuranceObj = new Model_ContractorInsurance();
		$modelAttachment = new Model_Attachment();
        foreach ($ids as $id) {
            if (CheckAuth::checkIfCanHandelAllCompany('contractorInsurance', $id)) {
			        $filter = array('type'=>'insurance','itemid'=>$id);  
                    $pager = null;					
					$attachments =  $modelAttachment->getAll('a.created desc',$pager, $filter);
					if($attachments ){
					  foreach($attachments as $attachment){
						$modelAttachment->updateById($attachment['attachment_id'] , array('is_deleted' => '1'));
					  }
					}
                $contractorInsuranceObj->deleteById($id);
            }
        }
        $this->_redirect($this->router->assemble(array('contractor_info_id' => $this->contractor_info_id), 'settingsContractorInsuranceList'));
    }
	
	public function editAction() {
	
				
        $business_name = $this->request->getParam('business_name');
		$insurance_policy_number = $this->request->getParam('insurance_policy_number');
        $insurance_policy_start = $this->request->getParam('insurance_policy_start');
        $insurance_policy_expiry = $this->request->getParam('insurance_policy_expiry');
        $insurance_listed_services_covered = $this->request->getParam('insurance_listed_services_covered');
        $insurance_type = $this->request->getParam('insurance_type');
		$id = $this->request->getParam('id');
        // validation
        //
        $contractorInsuranceObj = new Model_ContractorInsurance();
        $modelContractorInsuranceAttachment = new Model_ContractorInsuranceAttachment();
		$attachmentObj = new Model_Attachment();
        $insuranceInfo = $contractorInsuranceObj->getById($id);
		
       if (!CheckAuth::checkIfCanHandelAllCompany('contractor_info', $this->contractor_info_id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
		
		$form = new Settings_Form_InsuranceInfo(array('insuranceInfo' => $insuranceInfo,'mode'=>'update'));
		
	
		if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data			  
			   $data = array(
                    'insurance_policy_number' => $insurance_policy_number,
                    'insurance_policy_start' => strtotime($insurance_policy_start),
                    'insurance_policy_expiry' => strtotime($insurance_policy_expiry),
                    'insurance_listed_services_covered' => $insurance_listed_services_covered,
                    'insurance_type' => $insurance_type,
                );
				
				$deleted_ids = $this->request->getParam('deleted_ids','');
				$successDeleted = 0;
				if(!empty($deleted_ids)){
				 $deleted_ids = explode(",",$deleted_ids);
				 $modelAttachment = new Model_Attachment();
				 foreach($deleted_ids as $deleted_id){
                    $successDeleted = $modelAttachment->updateById($deleted_id , array('is_deleted' => '1'));
				 } 
				}
				
				$success = $contractorInsuranceObj->updateById($id, $data);
				
				$updateAttachment = 0;				
				$upload = new Zend_File_Transfer_Adapter_Http();
                $files  = $upload->getFileInfo();
				$Attachments = $contractorInsuranceObj->getAllAttachmentById($id);
				$counter = count($Attachments);
					foreach($files as $file => $fileInfo) {
					  if ($upload->isUploaded($file)){
                        if ($upload->receive($file)){
                           $counter = $counter + 1;						
						   $info = $upload->getFileInfo($file);						  
                           $source  = $info[$file]['tmp_name'];
                           $imageInfo = pathinfo($source);
                           $ext = $imageInfo['extension'];
						   $size = $info[$file]['size'];
						   $type = $info[$file]['type'];
				           $dir = get_config('attachment') . '/';
                           $subdir = date('Y/m/d/');
                           $fullDir = $dir . $subdir;
                           if (!is_dir($fullDir)) {
                              mkdir($fullDir, 0777, true);
                          }
                        $loggedUser = CheckAuth::getLoggedUser();							
						$data = array(
							 'created_by'=>$loggedUser['user_id'] 
							 ,'created'=>time()
							 ,'size' => $size
							 ,'type' =>$type
						 );
						 
						
						 
						 $Attachid = $attachmentObj->insert($data);
						 $contractorInfoAttachmentId = $modelContractorInsuranceAttachment->insert(array('contractor_insurance_id'=>$id,'attachment_id'=>$Attachid));					 
						 $fileName = $id.'_'.$counter.'_'.'insurance'.'.'. $ext;
						 $image_saved = copy($source, $fullDir . $fileName);
						 $typeParts = explode("/",$type);
						 if($typeParts[0] == 'image'){
						    $thumbName = $id.'_'.$counter.'_'.'insurance_thumbnail'.'.'. $ext;
						    ImageMagick::create_thumbnail($source, $fullDir . $thumbName, 78, 64);
						 }else{
						   $thumbName = $id.'_'.$counter.'_'.'insurance'.'.jpg';
						   ImageMagick::convertFile($source.'[0]', $fullDir . $thumbName);
						 }
						 
						 
						 
						 $Updatedata = array(
                          'path' => $fullDir . $fileName,
                          'file_name' => $fileName,
						  'thumbnail_file' => $fullDir . $thumbName
                          );
						  
						  
						 $updateAttachment = $attachmentObj->updateById($Attachid,$Updatedata);						 
						  if ($image_saved) {
								if (file_exists($source)) {
									unlink($source);
								}               
							}
						}
					 }	
					}
                if ($success || $updateAttachment) {    

                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Contractor Insurance"));
                }
                //$this->view->successMessage = 'Updated Successfully';
                echo 1;
                exit;
                //$this->_redirect($this->router->assemble(array(), 'settingsContractorInfoList'));
                //return;
			
			}
		}
		
        // init action form
		
        //contractor_id
       


        //
		//
        // handling the updating process
        //

		
	   $this->view->form = $form;
       $this->view->id = $id;
	   
        //
        // render views
        //
        echo $this->view->render('contractor-insurance/add-edit.phtml');
        exit;
	}
	
	
	public function getImagesAction(){
	  
	  
	 $contractor_insurance_id = $this->request->getParam('contractor_insurance_id', 0);
	 $modelContractorInsurance = new Model_ContractorInsurance();
     $Attachemnts = $modelContractorInsurance->getAllAttachmentById($contractor_insurance_id);
	 
	 header('Content-type: text/json');              
     header('Content-type: application/json');
	 echo json_encode($Attachemnts);
	 exit;
	 
	}

}

