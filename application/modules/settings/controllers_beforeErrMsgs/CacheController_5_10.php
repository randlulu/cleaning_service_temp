<?php

class Settings_CacheController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsPublic';

        BreadCrumbs::setLevel(3, 'Clear Cache');
    }

    /**
     * Items list action
     */
    public function indexAction() {

        //
        // check Auth for logged user
        //
        //CheckAuth::checkPermission(array('settingsBookingStatusList'));

        //
        // get request parameters
        //
    
    }


	public function deleteAllCacheAction() {
		   $cacheDir=get_config('cache');
            if (is_dir($cacheDir)) {
            $di = new RecursiveDirectoryIterator($cacheDir, FilesystemIterator::SKIP_DOTS);
            $ri = new RecursiveIteratorIterator($di, RecursiveIteratorIterator::CHILD_FIRST);
            foreach ( $ri as $file ) {
                $file->isDir() ?  rmdir($file) : unlink($file);
            }
            //rmdir($cacheDir);
            }
			
            $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Cache Deleted Successfully"));
            $this->_redirect($this->router->assemble(array(), 'settingsCache'));
    }
	
	
	public function deleteBookingCacheAction() {
            $bookingViewDir=get_config('cache').'/'.'bookingsView';
            if (is_dir($bookingViewDir)) {
            $di = new RecursiveDirectoryIterator($bookingViewDir, FilesystemIterator::SKIP_DOTS);
            $ri = new RecursiveIteratorIterator($di, RecursiveIteratorIterator::CHILD_FIRST);
            foreach ( $ri as $file ) {
                $file->isDir() ?  rmdir($file) : unlink($file);
            }
           // rmdir($bookingViewDir);
            }

            $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Cache Deleted Successfully"));
            $this->_redirect($this->router->assemble(array(), 'settingsCache'));
    }
	
	public function deleteInquiryCacheAction() {
            $bookingViewDir=get_config('cache').'/'.'inquiriesView';
            if (is_dir($bookingViewDir)) {
            $di = new RecursiveDirectoryIterator($bookingViewDir, FilesystemIterator::SKIP_DOTS);
            $ri = new RecursiveIteratorIterator($di, RecursiveIteratorIterator::CHILD_FIRST);
            foreach ( $ri as $file ) {
                $file->isDir() ?  rmdir($file) : unlink($file);
            }
            }

            $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Cache Cleared Successfully"));
            $this->_redirect($this->router->assemble(array(), 'settingsCache'));
    }
	
	public function deleteCalendarCacheAction() {
            $localEventsDir=get_config('cache').'/'.'localEvents';
            if (is_dir($localEventsDir)) {
            $di = new RecursiveDirectoryIterator($localEventsDir, FilesystemIterator::SKIP_DOTS);
            $ri = new RecursiveIteratorIterator($di, RecursiveIteratorIterator::CHILD_FIRST);
            foreach ( $ri as $file ) {
                $file->isDir() ?  rmdir($file) : unlink($file);
            }
            //rmdir($localEventsDir);
            }  

			$this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Cache Deleted Successfully"));
            $this->_redirect($this->router->assemble(array(), 'settingsCache'));
    }

	public function deleteEstimateCacheAction() {
            $estimateViewDir=get_config('cache').'/'.'estimatesView';
            if (is_dir($estimateViewDir)) {
            $di = new RecursiveDirectoryIterator($estimateViewDir, FilesystemIterator::SKIP_DOTS);
            $ri = new RecursiveIteratorIterator($di, RecursiveIteratorIterator::CHILD_FIRST);
            foreach ( $ri as $file ) {
                $file->isDir() ?  rmdir($file) : unlink($file);
            }
            }

            $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Cache Cleared Successfully"));
            $this->_redirect($this->router->assemble(array(), 'settingsCache'));
    }
	
	
}