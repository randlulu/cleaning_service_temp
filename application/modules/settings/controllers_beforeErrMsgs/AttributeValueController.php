<?php

class Settings_AttributeValueController extends Zend_Controller_Action {

    private $request;
    private $router;
    private $attribute_id;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
        $this->attribute_id = $this->request->getParam('attribute_id');

        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsServices';

        //
        //get attribute
        //
        $attributeObj = new Model_Attributes();
        $attribute = $attributeObj->getById($this->attribute_id);
        $this->view->attribute = $attribute;

        BreadCrumbs::setLevel(3, 'Values of ' . $attribute['attribute_name']);
    }

    /**
     * Items list action
     */
    public function indexAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsAttributeValueList'));

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'attribute_value_id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];


        //
        //get attribute value by id
        //
        $filters['attribute_id'] = $this->attribute_id;


        //
        // get data list
        //
        $attributeValueObj = new Model_AttributeListValue();
        $this->view->data = $attributeValueObj->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);

        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
        $this->view->attribute_id = $this->attribute_id;
    }
	
	public function getImagesAction(){
	  
	  
	 $id = $this->request->getParam('attribute_value_id', 0);
	 $modelAttributeListValueAttachment = new Model_AttributeListValueAttachment();
     $attributeListValueAttachemnts = $modelAttributeListValueAttachment->getByAttributeValueId($id);
	 
	 header('Content-type: text/json');              
     header('Content-type: application/json');
	 echo json_encode($attributeListValueAttachemnts);
	 exit;
	 
	}

    /**
     * Add new item action
     */
    public function addAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsAttributeValueAdd'));

        //
        // get request parameters
        //
        $attributeValue = $this->request->getParam('attribute_value');
        $unitPrice = $this->request->getParam('unit_price', 0);
        $extra_info = $this->request->getParam('extra_info', '');


        $router = Zend_Controller_Front::getInstance()->getRouter();

        //
        // init action form
        //
        $form = new Settings_Form_AttributeValue(array('attribute_id' => $this->attribute_id));

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $attributeValueObj = new Model_AttributeListValue();
				$attributeListValueAttachmentObj = new Model_AttributeListValueAttachment();
		        $attachmentObj = new Model_Attachment();
                $data = array(
                    'attribute_value' => $attributeValue,
                    'unit_price' => $unitPrice,
                    'extra_info' => $extra_info,
                    'attribute_id' => $this->attribute_id
                );

                $success = $attributeValueObj->insert($data);

                if ($success) {
				    $upload = new Zend_File_Transfer_Adapter_Http();
                    $files  = $upload->getFileInfo();
					$counter = 0 ;
					foreach($files as $file => $fileInfo) {
					  if ($upload->isUploaded($file)){
                        if ($upload->receive($file)){
						   $counter = $counter + 1;
						   $info = $upload->getFileInfo($file);
                           $source  = $info[$file]['tmp_name'];
                           $imageInfo = pathinfo($source);
                           $ext = $imageInfo['extension'];
						   $size = $info[$file]['size'];
						   $type = $info[$file]['type'];
				           $dir = get_config('attachment') . '/';
                           $subdir = date('Y/m/d/');
                           $fullDir = $dir . $subdir;
                           if (!is_dir($fullDir)) {
                              mkdir($fullDir, 0777, true);
                           }
                        $loggedUser = CheckAuth::getLoggedUser();							
						$data = array(
							 'created_by'=>$loggedUser['user_id'] 
							 ,'created'=>time()
							 ,'size' => $size
							 ,'type'=>$type
						 );
						 
						
						 
						 $id = $attachmentObj->insert($data);
						 $attributeListValueAttachmentId = $attributeListValueAttachmentObj->insert(array('attribute_list_value_id'=>$success,'attachment_id'=>$id));
						 $fileName = $success.'_'.$counter.'.'. $ext;
						 $image_saved = copy($source, $fullDir . $fileName);
						 $Updatedata = array(
                          'path' => $fullDir . $fileName,
                          'file_name' => $fileName 
                          );
						 $attachmentObj->updateById($id,$Updatedata);
						 
						  if ($image_saved) {
								if (file_exists($source)) {
									unlink($source);
								}               
							}
						}
					 }	
					}
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Attribute Value"));
                }

                //$this->_redirect($this->router->assemble(array(), 'settingsAttributeValueList'));
                echo 1;
                exit;
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('attribute-value/add_edit.phtml');
        exit;
    }

    /**
     * Edit an  item action
     */
    public function editAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsAttributeValueEdit'));

        $router = Zend_Controller_Front::getInstance()->getRouter();

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $attributeValue = $this->request->getParam('attribute_value');
        $unitPrice = $this->request->getParam('unit_price', 0);
        $extra_info = $this->request->getParam('extra_info', '');

        // get old data
        $modelAttributeListValue = new Model_AttributeListValue();
		$attributeListValueAttachmentObj = new Model_AttributeListValueAttachment();
		$attachmentObj = new Model_Attachment();
        $attributeListValue = $modelAttributeListValue->getById($id);

        //
        // init action form
        //
        $form = new Settings_Form_AttributeValue(array('mode' => 'update', 'attributeListValue' => $attributeListValue));

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
				$deleted_ids = $this->request->getParam('deleted_ids','');
				$successDeleted = 0;
				if(!empty($deleted_ids)){
				 $deleted_ids = explode(",",$deleted_ids);
				 $modelAttachment = new Model_Attachment();
				 foreach($deleted_ids as $deleted_id){
                    $successDeleted = $modelAttachment->updateById($deleted_id , array('is_deleted' => '1'));
				 } 
				}
                $data = array(
                    'attribute_value' => $attributeValue,
                    'unit_price' => $unitPrice,
                    'extra_info' => $extra_info
                );

                $success = $modelAttributeListValue->updateById($id, $data);
				$attributeListValueNew = $modelAttributeListValue->getById($id);
                $updateAttachment = 0;				
				$upload = new Zend_File_Transfer_Adapter_Http();
                $files  = $upload->getFileInfo();
				$attributeAttachments = $attributeListValueAttachmentObj->getByAttributeValueId($id);
				$counter = count($attributeAttachments);
					foreach($files as $file => $fileInfo) {
					  if ($upload->isUploaded($file)){
                        if ($upload->receive($file)){
                           $counter = $counter + 1;						
						   $info = $upload->getFileInfo($file);						  
                           $source  = $info[$file]['tmp_name'];
                           $imageInfo = pathinfo($source);
                           $ext = $imageInfo['extension'];
						   $size = $info[$file]['size'];
						   $type = $info[$file]['type'];
				           $dir = get_config('attachment') . '/';
                           $subdir = date('Y/m/d/');
                           $fullDir = $dir . $subdir;
                           if (!is_dir($fullDir)) {
                              mkdir($fullDir, 0777, true);
                           }
                        $loggedUser = CheckAuth::getLoggedUser();							
						$data = array(
							 'created_by'=>$loggedUser['user_id'] 
							 ,'created'=>time()
							 ,'size' => $size
							 ,'type' =>$type
						 );
						 
						
						 
						 $Attachid = $attachmentObj->insert($data);
						 $attributeListValueAttachmentId = $attributeListValueAttachmentObj->insert(array('attribute_list_value_id'=>$id,'attachment_id'=>$Attachid));
						 $fileName = $attributeListValueNew['attribute_value'].'_'.$counter.'.'. $ext;
						 $image_saved = copy($source, $fullDir . $fileName);
						 
						 $Updatedata = array(
                          'path' => $fullDir . $fileName,
                          'file_name' => $fileName
                          );
						  
						  
						 $updateAttachment = $attachmentObj->updateById($Attachid,$Updatedata);						 
						  if ($image_saved) {
								if (file_exists($source)) {
									unlink($source);
								}               
							}
						}
					 }	
					}

                if ($success || $updateAttachment || $successDeleted) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Attribute Value"));
                }
                echo 1;
                exit;
            }
        }

        $this->view->form = $form;
        $this->view->id = $id;
		
        //
        // render views
        //
        echo $this->view->render('attribute-value/add_edit.phtml');
        exit;
    }

    public function deleteAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsAttributeValueDelete'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
        if ($id) {
            $ids[] = $id;
        }

        $attributeValueObj = new Model_AttributeListValue();
		$modelAttachment = new Model_Attachment();
     
        foreach ($ids as $id) {

            $isNotRelated = $attributeValueObj->checkBeforeDeleteServiceAttributeValue($id);

            if ($isNotRelated) {
                $isFloorAndHaveAttachment = $attributeValueObj->checkIfFloorTypeAndHaveAttachment($id);
                if ($isFloorAndHaveAttachment) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "could not be deleted ,This Item used in other Place "));
                } else {
				    $modelAttachment = new Model_Attachment();
				    $attributeListValueAttachmentObj = new Model_AttributeListValueAttachment();
				    $attributeAttachments = $attributeListValueAttachmentObj->getByAttributeValueId($id);
					foreach($attributeAttachments as $attributeAttachment){
                      $success = $modelAttachment->updateById($attributeAttachment['attachment_id'] , array('is_deleted' => '1'));					  
					}
                    $attributeValueObj->deleteById($id);
                }
            } else {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "could not be deleted ,This Item used in other Place "));
            }
        }
        $this->_redirect($this->router->assemble(array('attribute_id' => $this->attribute_id), 'settingsAttributeValueList'));
    }

}

