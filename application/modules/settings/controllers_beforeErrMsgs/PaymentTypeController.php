<?php

class Settings_PaymentTypeController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsPayment';

        BreadCrumbs::setLevel(2, 'Payment Type');
    }

    /**
     * Items list action
     */
    public function indexAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsPaymentTypeList'));

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        //
        // get data list
        //
        $paymentTypeObj = new Model_PaymentType();
        $this->view->data = $paymentTypeObj->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);

        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
    }

    /**
     * Add new item action
     */
    public function addAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsPaymentTypeList'));

        //
        // get request parameters
        //
        $paymentType = $this->request->getParam('payment_type');
        $router = Zend_Controller_Front::getInstance()->getRouter();

        //
        // init action form
        //
        $form = new Settings_Form_PaymentType();

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $paymentTypeObj = new Model_PaymentType();

                $data = array(
                    'payment_type' => $paymentType,
                    'slug' => $paymentTypeObj->createSlug($paymentType),
                    'company_id' => CheckAuth::getCompanySession()
                );

                $success = $paymentTypeObj->insert($data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Payment Type"));
                }
                echo 1;
                exit;
            } else {
                $messages = $form->getElement('payment_type')->getErrorMessages();
                if ($messages) {
                    $form->getElement('payment_type')->clearErrorMessages();
                    foreach ($messages as $message) {
                        $form->getElement('payment_type')->addError($message);
                    }
                }
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('payment-type/add_edit.phtml');
        exit;
    }

    public function deleteAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsPaymentTypeDelete'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
        if ($id) {
            $ids[] = $id;
        }

        $paymentTypeObj = new Model_PaymentType();

        foreach ($ids as $id) {
            $isCore = $paymentTypeObj->getById($id);
            if (!$isCore['is_core']) {
                if (CheckAuth::checkIfCanHandelAllCompany('payment_type', $id)) {

                    $isNotRelated= $paymentTypeObj->checkBeforeDeletePaymentType($id);
                    if ($isNotRelated) {
                        $paymentTypeObj->deleteById($id);
                    } else {
                        $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "could not be deleted ,This Item used in other Place "));
                    }
                }
            }
        }
        $this->_redirect($this->router->assemble(array(), 'settingsPaymentTypeList'));
    }

    public function editAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsPaymentTypeEdit'));

        //
        // get request parameters
        //
        $paymentType = $this->request->getParam('payment_type');
        $id = $this->request->getParam('id');


        if (!CheckAuth::checkIfCanHandelAllCompany('payment_type', $id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // validation
        //
        $paymentTypeObj = new Model_PaymentType();
        $paymentTypeOld = $paymentTypeObj->getById($id);
        if (!$paymentTypeOld) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));

            $this->_redirect($this->router->assemble(array(), 'settingsPaymentTypeList'));
            return;
        }

        //
        // init action form
        //
        $form = new Settings_Form_PaymentType(array('mode' => 'update', 'paymentType' => $paymentTypeOld));

        //
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                if ($paymentType['is_core']) {
                    $data = array(
                        'payment_type' => $paymentType,
                        'company_id' => CheckAuth::getCompanySession()
                    );

                    if ($paymentTypeOld['payment_type'] != $paymentType) {
                        $data['slug'] = $paymentTypeObj->createSlug($paymentType);
                    }

                    $success = $paymentTypeObj->updateById($id, $data);

                    if ($success) {
                        $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                    } else {
                        $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in PaymentType"));
                    }
                }
                echo 1;
                exit;
            }
        }

        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('payment-type/add_edit.phtml');
        exit;
    }

}

