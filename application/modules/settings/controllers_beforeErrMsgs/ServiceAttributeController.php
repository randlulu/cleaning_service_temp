<?php

class Settings_ServiceAttributeController extends Zend_Controller_Action {

    private $request;
    private $router;
    private $service_id;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
        $this->service_id = $this->request->getParam('service_id');

        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsServices';

        $Model_Services = new Model_Services();
        $Services = $Model_Services->getById($this->service_id);

        BreadCrumbs::setLevel(3, 'Attribute to ' . $Services['service_name']);
    }

    /**
     * Items list action
     */
    public function indexAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsServiceAttributeList'));

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'service_attribute_id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        if (!CheckAuth::checkIfCanHandelAllCompany('service', $this->service_id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];


        //
        //get service attribute by id
        //
        $filters['service_id'] = $this->service_id;


        //
        // get data list
        //
        $serviceAttributeObj = new Model_ServiceAttribute();
        $this->view->data = $serviceAttributeObj->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);

        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
        $this->view->service_id = $this->service_id;
    }

    /**
     * Add new item action
     */
    public function addAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsServiceAttributeAdd'));

        //
        // get request parameters
        //
        $attribute_id = $this->request->getParam('attribute_id');
        $default_value = $this->request->getParam('default_value');
        $is_readonly = $this->request->getParam('is_readonly');
        $router = Zend_Controller_Front::getInstance()->getRouter();


        if (!CheckAuth::checkIfCanHandelAllCompany('service', $this->service_id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        //
        // init action form
        //
        $form = new Settings_Form_ServiceAttribute(array('service_id' => $this->service_id));

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $serviceAttributeObj = new Model_ServiceAttribute();
                $data = array(
                    'attribute_id' => $attribute_id,
                    'service_id' => $this->service_id,
                    'default_value' => $default_value,
                    'is_readonly' => $is_readonly
                );
                if ($serviceAttributeObj->getByAttributeIdAndServiceId($attribute_id, $this->service_id)) {
                    $success = false;
                } else {
                    $success = $serviceAttributeObj->insert($data);
                }

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Service Attribute"));
                }

                //$this->_redirect($this->router->assemble(array(), 'settingsServiceAttributeList'));
                echo 1;
                exit;
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('service-attribute/add_edit.phtml');
        exit;
    }

    public function deleteAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsServiceAttributeDelete'));


        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
        if ($id) {

            $ids[] = $id;
        }

        $serviceAttributeObj = new Model_ServiceAttribute();
        foreach ($ids as $id) {

            if (CheckAuth::checkIfCanHandelAllCompany('service_attribute', $id)) {
                $isNotRelated= $serviceAttributeObj->checkBeforeDeleteServiceAttribute($id);
                if ($isNotRelated) {
                    $serviceAttributeObj->deleteById($id);
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "could not be deleted ,This Item used in other Place "));
                }
            }
        }
        $this->_redirect($this->router->assemble(array('service_id' => $this->service_id), 'settingsServiceAttributeList'));
    }

    /**
     * Edit item action
     */
    public function editAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsServiceAttributeEdit'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id');
        $attribute_id = $this->request->getParam('attribute_id');
        $default_value = $this->request->getParam('default_value');
        $is_readonly = $this->request->getParam('is_readonly');
        $router = Zend_Controller_Front::getInstance()->getRouter();



        if (!CheckAuth::checkIfCanHandelAllCompany('service', $this->service_id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        if (!CheckAuth::checkIfCanHandelAllCompany('service_attribute', $id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }





        $serviceAttributeObj = new Model_ServiceAttribute();
        $serviceAttribute = $serviceAttributeObj->getById($id);

        //
        // init action form
        //
        $form = new Settings_Form_ServiceAttribute(array('mode' => 'update', 'serviceAttribute' => $serviceAttribute, 'service_id' => $this->service_id));

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'attribute_id' => $attribute_id,
                    'service_id' => $this->service_id,
                    'default_value' => $default_value,
                    'is_readonly' => $is_readonly
                );

                $serviceAttribute = $serviceAttributeObj->getByAttributeIdAndServiceId($attribute_id, $this->service_id);
                if ($serviceAttribute['service_attribute_id'] != $id) {
                    $success = false;
                } else {
                    $success = $serviceAttributeObj->updateById($id, $data);
                }

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Service Attribute"));
                }

                echo 1;
                exit;
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('service-attribute/add_edit.phtml');
        exit;
    }

}

