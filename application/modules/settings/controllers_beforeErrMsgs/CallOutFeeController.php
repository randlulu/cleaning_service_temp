<?php

class Settings_CallOutFeeController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();


        // main menu selected item
        $this->view->main_menu = 'settings';
        // sub menu selected item
        $this->view->sub_menu = 'settingsPayment';

        BreadCrumbs::setLevel(2, 'Call Out Fee');
    }

    public function indexAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsCallOutFee'));

        $modelCompanies = new Model_Companies();

        //
        // get request parameters
        //
        $callOutFee = $this->request->getParam('call_out_fee');


        $router = Zend_Controller_Front::getInstance()->getRouter();

        $companies = $modelCompanies->getById(CheckAuth::getCompanySession());

        //
        // init action form
        // 

        $form = new Settings_Form_CallOutFee(array('callOutFee' => $companies['call_out_fee']));

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'Call_out_fee' => $callOutFee,
                );
                $modelCompanies->updateById(CheckAuth::getCompanySession(), $data);

                $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));

                echo 1;
                exit;
            } else {
                $messages = $form->getElement('call_out_fee')->getErrorMessages();
                if ($messages) {
                    $form->getElement('call_out_fee')->clearErrorMessages();
                    foreach ($messages as $message) {
                        $form->getElement('call_out_fee')->addError($message);
                    }
                }
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('call-out-fee/index.phtml');
        exit;
    }

}

?>