<?php

class Settings_SecurityController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsSecurity';

        BreadCrumbs::setLevel(2, 'Users');
    }

    /**
     * Items list action
     */
    public function indexAction() {
		
    }

}

