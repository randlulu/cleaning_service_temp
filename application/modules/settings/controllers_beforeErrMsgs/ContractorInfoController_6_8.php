<?php

class Settings_ContractorInfoController extends Zend_Controller_Action {

    private $request;
    private $router;
    private $contractor_id;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
        $this->contractor_id = $this->request->getParam('contractor_id');

        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsUser';

        //
        // get data list
        //
        $modelContractorInfo = new Model_ContractorInfo();
        $contractorInfo = $modelContractorInfo->getByContractorId($this->contractor_id);

        BreadCrumbs::setLevel(3, 'Contractor Info');
    }

    /**
     * Items list action
     */
    public function indexAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsContractorInfoList'));



        if (!CheckAuth::checkIfCanHandelAllCompany('user', $this->contractor_id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // get data list
        //
        $modelContractorInfo = new Model_ContractorInfo();
        $contractorInfo = $modelContractorInfo->getByContractorId($this->contractor_id);



        //get user
        $userObj = new Model_User();
        $user = $userObj->getById($this->contractor_id);

        if ($contractorInfo) {
            //get contractorOwner
            $contractorOwnerObj = new Model_ContractorOwner();
            $contractorOwner = $contractorOwnerObj->getAll(array('contractor_info_id' => $contractorInfo['contractor_info_id']));

            $this->view->contractorOwner = $contractorOwner;

            //get contractorEmployee
            $contractorEmployeeObj = new Model_ContractorEmployee();
            $contractorEmployee = $contractorEmployeeObj->getAll(array('contractor_info_id' => $contractorInfo['contractor_info_id']));

            $this->view->contractorEmployee = $contractorEmployee;

            //get DeclarationOfChemicals
            $declarationOfChemicalsObj = new Model_DeclarationOfChemicals();
            $declarationOfChemicals = $declarationOfChemicalsObj->getByContractorInfoId($contractorInfo['contractor_info_id']);

            $this->view->declarationOfChemicals = $declarationOfChemicals;

            //get DeclarationOfEquipment
            $declarationOfEquipmentObj = new Model_DeclarationOfEquipment();
            $declarationOfEquipment = $declarationOfEquipmentObj->getByContractorInfoId($contractorInfo['contractor_info_id']);

            $this->view->declarationOfEquipment = $declarationOfEquipment;

            //get DeclarationOfOtherApparatus
            $declarationOfOtherApparatusObj = new Model_DeclarationOfOtherApparatus();
            $declarationOfOtherApparatus = $declarationOfOtherApparatusObj->getByContractorInfoId($contractorInfo['contractor_info_id']);

            $this->view->declarationOfOtherApparatus = $declarationOfOtherApparatus;

            //get Vehicle
            $contractorVehicleObj = new Model_ContractorVehicle();
            $contractorVehicle = $contractorVehicleObj->getByContractorInfoId($contractorInfo['contractor_info_id']);

            $this->view->contractorVehicle = $contractorVehicle;
        }

        $this->view->contractorInfo = $contractorInfo;
        $this->view->user = $user;
        $this->view->contractor_id = $this->contractor_id;
		//By Abdallah
		//echo '$this->contractor_id  '.$this->contractor_id;
		//get awaiting accept  booking for contarctor 
	    $AwB=new Model_Booking();
	    $CM = new Model_Complaint();
    
        $CP = new Model_Payment(); // Unapproved Account
        // request owners
        $CO = new Model_ClaimOwner();
        $MC = new Model_MissedCalls();
    
            $countMissedCallsContractor = $MC->getCountMissedCallsForContractor($this->contractor_id);
            $countClaimOwnerContractor = $CO->getCountClaimOwnerByContractor($this->contractor_id);
            $countUnapprovedPaymentsForContractor = $CP->getCountUnapprovedPaymentsForContractor($this->contractor_id);
	   $countAwaitingaccept = $AwB->getCountAwaitingAcceptBookingForContractor($this->contractor_id); 
	   $countAwaitingUpdate = $AwB->getCountAwaitingupdateBookingForContractor($this->contractor_id); 
	   $countUnapprovedBookingContractor = $AwB->getCountUnapprovedBookingForContractor($this->contractor_id); 
	   $countRejectBookingContractor = $AwB->getCountRejectBookingForContractor($this->contractor_id); 
	   $countInprocesstBookingContractor = $AwB->getCountInProcessBookingForContractor($this->contractor_id); 
	   
	   $countOpenComplaintForContractors = $CM->getComplaintCountForContractor($this->contractor_id);
	   $countUnapprovedComplaintForContractors = $CM->countUapprovedCompalintsForContractor($this->contractor_id);
			  
	   
	   $this->view->contractorInfo = $contractorInfo;
	   $this->view->user = $user;
	   $this->view->contractor_id = $this->contractor_id;
	   $this->view->countAwaitingUpdate = $countAwaitingUpdate;
	   $this->view->countAwaitingaccept = $countAwaitingaccept;
	   $this->view->countUnapprovedBookingContractor = $countUnapprovedBookingContractor;
	   $this->view->countRejectBookingContractor = $countRejectBookingContractor;
	   $this->view->countInprocesstBookingContractors = $countInprocesstBookingContractor;
	   $this->view->countUnapprovedPaymentsForContractors = $countUnapprovedPaymentsForContractor;
	   $this->view->countClaimOwnerContractor = $countClaimOwnerContractor;
       $this->view->countMissedCallsContaractor = $countMissedCallsContractor;
       $this->view->countOpenComplaintForContractor = $countOpenComplaintForContractors;
       $this->view->countUnapprovedComplaintForContractors = $countUnapprovedComplaintForContractors;
	  
    }

    /**
     * Add new item action
     */
    public function addAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsContractorInfoAdd'));

        if (!CheckAuth::checkIfCanHandelAllCompany('user', $this->contractor_id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $modelContractorInfo = new Model_ContractorInfo();
        $contractorInfo = $modelContractorInfo->getByContractorId($this->contractor_id);
        if ($contractorInfo) {
            $this->_redirect($this->router->assemble(array('contractor_id' => $this->contractor_id, 'id' => $contractorInfo['contractor_info_id']), 'settingsContractorInfoEdit'));
        }
        //
        // get request parameters
        //
        $business_name = $this->request->getParam('business_name');
        $acn = $this->request->getParam('acn');
        $abn = $this->request->getParam('abn');
        $tfn = $this->request->getParam('tfn');
        $gst = $this->request->getParam('gst');
        $gst_date_registered = $this->request->getParam('gst_date_registered');
        $insurance_policy_number = $this->request->getParam('insurance_policy_number');
        $insurance_policy_start = $this->request->getParam('insurance_policy_start');
        $insurance_policy_expiry = $this->request->getParam('insurance_policy_expiry');
        $insurance_listed_services_covered = $this->request->getParam('insurance_listed_services_covered');
        $drivers_licence_number = $this->request->getParam('drivers_licence_number');
        $drivers_licence_expiry = $this->request->getParam('drivers_licence_expiry');
        $bond_to_be_withheld = $this->request->getParam('bond_to_be_withheld');
        $commission = $this->request->getParam('commission');


        $router = Zend_Controller_Front::getInstance()->getRouter();

        //
        // init action form
        //
        $form = new Settings_Form_ContractorInfo(array('contractor_id' => $this->contractor_id));

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $modelContractorInfo = new Model_ContractorInfo();
                $data = array(
                    'business_name' => $business_name,
                    'acn' => $acn,
                    'abn' => $abn,
                    'tfn' => $tfn,
                    'gst' => $gst,
                    'gst_date_registered' => strtotime($gst_date_registered),
                    'insurance_policy_number' => $insurance_policy_number,
                    'insurance_policy_start' => $insurance_policy_start,
                    'insurance_policy_expiry' => $insurance_policy_expiry,
                    'insurance_listed_services_covered' => $insurance_listed_services_covered,
                    'drivers_licence_number' => $drivers_licence_number,
                    'drivers_licence_expiry' => $drivers_licence_expiry,
                    'bond_to_be_withheld' => $bond_to_be_withheld,
                    'commission' => $commission,
                    'contractor_id' => $this->contractor_id
                );

                $success = $modelContractorInfo->insert($data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Contractor Info"));
                }
                //$this->_redirect($this->router->assemble(array(), 'settingsUserEmailList'));

                echo 1;
                exit;
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        
        echo $this->view->render('contractor-info/add_edit.phtml');
        exit;
    }

    public function deleteAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsContractorInfoDelete'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
        if ($id) {
            $ids[] = $id;
        }

        $modelContractorInfo = new Model_ContractorInfo();
        foreach ($ids as $id) {
            if (CheckAuth::checkIfCanHandelAllCompany('contractor_info', $id)) {
                $modelContractorInfo->deleteById($id);
            }
        }
        $this->_redirect($this->router->assemble(array('contractor_id' => $this->contractor_id), 'settingsContractorInfoList'));
    }

    public function editAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsContractorInfoEdit'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id');
        $business_name = $this->request->getParam('business_name');
        $acn = $this->request->getParam('acn');
        $abn = $this->request->getParam('abn');
        $tfn = $this->request->getParam('tfn');
        $gst = $this->request->getParam('gst');
        $gst_date_registered = $this->request->getParam('gst_date_registered');
        $insurance_policy_number = $this->request->getParam('insurance_policy_number');
        $insurance_policy_start = $this->request->getParam('insurance_policy_start');
        $insurance_policy_expiry = $this->request->getParam('insurance_policy_expiry');
        $insurance_listed_services_covered = $this->request->getParam('insurance_listed_services_covered');
        $drivers_licence_number = $this->request->getParam('drivers_licence_number');
        $drivers_licence_expiry = $this->request->getParam('drivers_licence_expiry');
        $bond_to_be_withheld = $this->request->getParam('bond_to_be_withheld');
        $commission = $this->request->getParam('commission');


        //
        // validation
        //
        $modelContractorInfo = new Model_ContractorInfo();
        $contractorInfo = $modelContractorInfo->getById($id);
        if (!$contractorInfo) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect($this->router->assemble(array(), 'settingsContractorInfoList'));
            return;
        }


        //
        // init action form
        //contractor_id
        $form = new Settings_Form_ContractorInfo(array('mode' => 'update', 'contractorInfo' => $contractorInfo, 'contractor_id' => $this->contractor_id));


        //
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'business_name' => $business_name,
                    'acn' => $acn,
                    'abn' => $abn,
                    'tfn' => $tfn,
                    'gst' => $gst,
                    'gst_date_registered' => strtotime($gst_date_registered),
                    'insurance_policy_number' => $insurance_policy_number,
                    'insurance_policy_start' => $insurance_policy_start,
                    'insurance_policy_expiry' => $insurance_policy_expiry,
                    'insurance_listed_services_covered' => $insurance_listed_services_covered,
                    'drivers_licence_number' => $drivers_licence_number,
                    'drivers_licence_expiry' => $drivers_licence_expiry,
                    'bond_to_be_withheld' => $bond_to_be_withheld,
                    'commission' => $commission,
                );

                $success = $modelContractorInfo->updateById($id, $data);

                if ($success) {

                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Contractor Info"));
                }
                //$this->view->successMessage = 'Updated Successfully';
                echo 1;
                exit;
                //$this->_redirect($this->router->assemble(array(), 'settingsContractorInfoList'));
                //return;
            }
        }

        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('contractor-info/add_edit.phtml');
        exit;
    }

    public function addGmailAccountsAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsContractorGmailAdd'));

        //create new model contractor
        $modelContractorGmailAccounts = new Model_ContractorGmailAccounts();
        $contractorAccounts = $modelContractorGmailAccounts->getByContractorId($this->contractor_id);

        if ($contractorAccounts) {
            $this->_redirect($this->router->assemble(array('contractor_id' => $this->contractor_id, 'id' => $contractorAccounts['id']), 'settingsContractorGmailEdit'));
        }


        //
        // get request parameters
        //
     
        $email = $this->request->getParam('email');
        $password = $this->request->getParam('password');


        //
        // init action form
        //
        $form = new Settings_Form_ContractorGmailAccounts(array('contractor_id' => $this->contractor_id));

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'contractor_id' => $this->contractor_id,
                    'email' => $email,
                    'password' => $password
                );

                $success = $modelContractorGmailAccounts->insert($data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Contractor Info"));
                }

                echo 1;
                exit;
            }
        }

        $this->view->form = $form;
        //
        // render view  $router->addRoute('settingsContractorGmailAdd', new Zend_Controller_Router_Route('settings/contractor-info/add-gmail-accounts/:contractor_id', array('module' => 'settings', 'controller' => 'contractor-info', 'action' => 'add-gmail-accounts')));s
        //
        
        echo $this->view->render('contractor-info/add-edit-gmail-accounts.phtml');
        exit;
    }

    public function editGmailAccountsAction() {


        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsContractorGmailEdit'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id');
        $email = $this->request->getParam('email');
        $password = $this->request->getParam('password');


        //create new model contractor
        $modelContractorGmailAccounts = new Model_ContractorGmailAccounts();
        $contractorAccount = $modelContractorGmailAccounts->getById($id);

        //
        // init action form
        //
        $form = new Settings_Form_ContractorGmailAccounts(array('mode' => 'update', 'contractor_id' => $this->contractor_id, 'contractorAccount' => $contractorAccount));


        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) {

                $data = array(
                    'email' => $email,
                    'password' => $password
                );

                $success = $modelContractorGmailAccounts->updateById($contractorAccount['id'], $data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Contractor Info"));
                }

                echo 1;
                exit;
            }
        }


        $this->view->form = $form;
        echo $this->view->render('contractor-info/add-edit-gmail-accounts.phtml');
        exit;
    }
	public function contractorPicAction() {
		
	
		
		$id = $this->request->getParam('contractor_id');
		if ($this->request->isPost()) {
			
			$upload = new Zend_File_Transfer_Adapter_Http();
			
			$files  = $upload->getFileInfo();
			$countFiles = count($files);
			
			foreach($files as $file => $fileInfo) {
			  if ($upload->isUploaded($file)) {
                if ($upload->receive($file)) {
				 $info = $upload->getFileInfo($file);
                 $source  = $info[$file]['tmp_name'];
                 $imageInfo = pathinfo($source);
                 $ext = $imageInfo['extension'];
				 $dir = get_config('contractor_picture');
                  if (!is_dir($dir)) {
                        mkdir($dir, 0777, true);
                    }	
					$original_path = time()."_".$id.'.'.$ext;
					$image_saved = copy($source, $dir.$original_path);
				 $modelContractorInfo = new Model_ContractorInfo();
				 if($image_saved){
					 $data=array('profile_pic'=>$original_path);
					 //print_r($data);
					 //echo '$this->contractor_id   '.$this->contractor_id;
					 if($modelContractorInfo->updateByContractorId($this->contractor_id,$data)){
						 
						      
							echo '<img class="img-responsive" src="'.$this->getRequest()->getBaseUrl().'/uploads/contractors_pic/'.$original_path.'" />';
					
					 }
				     
				 }
				}
			  } 
			}
		}
			
		 exit;
		 
	}
	
	public function editInsuranceAction() {
	
	 
        // check Auth for logged user
        
		

      
        // get request parameters
        $id = $this->request->getParam('contractor_id');
		
		
		
        $business_name = $this->request->getParam('business_name');
		$insurance_policy_number = $this->request->getParam('insurance_policy_number');
        $insurance_policy_start = $this->request->getParam('insurance_policy_start');
        $insurance_policy_expiry = $this->request->getParam('insurance_policy_expiry');
        $insurance_listed_services_covered = $this->request->getParam('insurance_listed_services_covered');
        // validation
        //
        $modelContractorInfo = new Model_ContractorInfo();
        $contractorInfo = $modelContractorInfo->getByContractorId($id);
		
        if (!$contractorInfo) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect($this->router->assemble(array(), 'settingsContractorInfoList'));
            return;
        }
		
		
        // init action form
		
        //contractor_id
        $form = new Settings_Form_ContractorInfo(array('mode' => 'update', 'contractorInfo' => $contractorInfo, 'contractor_id' => $this->contractor_id));


        //
		//
        // handling the updating process
        //

		
	   $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('contractor-info/edit_insurance.phtml');
        exit;
	}
	
	public function editLicenceAction() {
		
        // get request parameters
        $id = $this->request->getParam('contractor_id');
		
		$drivers_licence_number = $this->request->getParam('drivers_licence_number');
        $drivers_licence_expiry = $this->request->getParam('drivers_licence_expiry');
        // validation
      
        $modelContractorInfo = new Model_ContractorInfo();
        $contractorInfo = $modelContractorInfo->getByContractorId($id);
		
        if (!$contractorInfo) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect($this->router->assemble(array(), 'settingsContractorInfoList'));
            return;
        }
		
        // init action form
        //contractor_id
		
        $form = new Settings_Form_ContractorInfo(array('mode' => 'update', 'contractorInfo' => $contractorInfo, 'contractor_id' => $this->contractor_id));

        // handling the updating process
    
    
		
	   $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('contractor-info/edit_licence.phtml');
        exit;
		
		
		
	}
	
	public function editPaymentAction() {
		
		
		   // get request parameters
        $id = $this->request->getParam('contractor_id');
		
		   $bond_to_be_withheld = $this->request->getParam('bond_to_be_withheld');
        $commission = $this->request->getParam('commission');
        // validation
      
        $modelContractorInfo = new Model_ContractorInfo();
        $contractorInfo = $modelContractorInfo->getByContractorId($id);
		
        if (!$contractorInfo) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect($this->router->assemble(array(), 'settingsContractorInfoList'));
            return;
        }
		
        // init action form
        //contractor_id
		
        $form = new Settings_Form_ContractorInfo(array('mode' => 'update', 'contractorInfo' => $contractorInfo, 'contractor_id' => $this->contractor_id));

        // handling the updating process
    
    
		
	   $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('contractor-info/edit_payment.phtml');
        exit;
		
		
		
	}

}


