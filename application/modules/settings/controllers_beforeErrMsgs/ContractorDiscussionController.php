<?php

class Settings_ContractorDiscussionController extends Zend_Controller_Action {

    private $request;
    private $router;
    private $loggedUser;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        //
        //checkLoggedIn
        //
        CheckAuth::checkLoggedIn();

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('bookingDiscussion'));

        //
        //get Logged User
        //
        $this->loggedUser = CheckAuth::getLoggedUser();
    }

    public function indexAction() {

        //
        //get Params
        //
        $contractor_id = $this->request->getParam('contractor_id', 0);

        //
        // load model
        //
        $modelContractorDiscussion = new Model_ContractorDiscussion();
        $modelContractorInfo = new Model_ContractorInfo();
        $modelUser = new Model_User();

        /*if (!$modelBooking->checkIfCanSeeBookingWithOutTimePeriod($bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this Booking"));
            $this->_redirect($this->router->assemble(array(), 'booking'));
        }*/
        $contractor = $modelUser->getById($contractor_id);
        $this->view->contractor = $contractor;

        if (!$contractor) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Contractor is not exist"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $ContractorDiscussions = $modelContractorDiscussion->getByContractorId($contractor_id);
        foreach ($ContractorDiscussions as &$ContractorDiscussion) {
            $ContractorDiscussion['user'] = $modelUser->getById($ContractorDiscussion['user_id']);
            if ($ContractorDiscussion['user']['role_name'] == 'contractor') {
                $contractorInfo = $modelContractorInfo->getByContractorId($ContractorDiscussion['user']['user_id']);
                $ContractorDiscussion['user']['username'] = ucwords($ContractorDiscussion['user']['username']) . ' - ' . ucwords($contractorInfo['business_name']);
            } else {
                $ContractorDiscussion['user']['username'] = ucwords($ContractorDiscussion['user']['username']);
            }
        }

        $this->view->ContractorDiscussions = $ContractorDiscussions;

        echo $this->view->render('contractor-discussion/index.phtml');
        exit;
    }

    public function submitContractorDiscussionAction() {

        //
        //get Params
        //
        $contractorId = $this->request->getParam('contractor_id', 0);
        $discussion = $this->request->getParam('discussion', '');
        
        //
        // load model
        //
        $modelContractorDiscussion = new Model_ContractorDiscussion();

        $success = 0;
        if ($discussion) {
            $data = array(
                'contractor_id' => $contractorId,
                'user_id' => $this->loggedUser['user_id'],
                'user_message' => $discussion,
                'created' => time()
            );
            $success = $modelContractorDiscussion->insert($data);
        }

        if ($success) {
            echo json_encode(array('success' => 1));
        } else {
            echo json_encode(array('success' => 0, 'errMsg' => 'Invalid Discussion'));
        }
        exit;
    }

    public function allContractorDiscussionAction() {

        //
        //get Params
        //
        $contractorId = $this->request->getParam('contractor_id', 0);

        //
        // load model
        //
        $modelContractorDiscussion = new Model_ContractorDiscussion();
		$modelContractorInfo = new Model_ContractorInfo();
        $modelUser = new Model_User();

		$ContractorDiscussions = $modelContractorDiscussion->getByContractorId($contractorId);
		 foreach ($ContractorDiscussions as &$ContractorDiscussion) {
            $ContractorDiscussion['user'] = $modelUser->getById($ContractorDiscussion['user_id']);
            $ContractorDiscussion['discussion_date'] = getDateFormating($ContractorDiscussion['created']);
            $ContractorDiscussion['user_message'] = nl2br(htmlentities($ContractorDiscussion['user_message'], ENT_NOQUOTES, 'UTF-8'));
        }
		
       
        $json_array = array(
            'discussions' => $ContractorDiscussions,
            'count' => count($ContractorDiscussions)
        );

        echo json_encode($json_array);
        exit;
    }
	
	
	
}

