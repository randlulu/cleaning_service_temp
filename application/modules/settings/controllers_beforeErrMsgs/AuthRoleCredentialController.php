<?php

class Settings_AuthRoleCredentialController extends Zend_Controller_Action {

    private $request;
    private $router;
    private $role_id;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
        $this->role_id = $this->request->getParam('role_id');

        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsUser';

        $modelAuthRole = new Model_AuthRole();
        $authRole = $modelAuthRole->getById($this->role_id);

        BreadCrumbs::setLevel(3, 'Set Credential to ' . $authRole['view_role_name']);
    }

    /**
     * Items list action
     */
    public function indexAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsAuthRoleCredentialList'));

        //
        // get request parameters
        //
        $credentials = $this->request->getParam('credentials', array());

        //
        //get all credential to render in view
        //
        $filters['sortCriteria1']=true;
        $authCredential_obj = new Model_AuthCredential();
        $all_credential = $authCredential_obj->getAllCredentialTree();
        
        $this->view->all_credential = $all_credential;

        //
        //prepar old role credentials
        //
        $authRoleCredential_obj = new Model_AuthRoleCredential();
        $role_credentials = $authRoleCredential_obj->getByRoleId($this->role_id);

        $old_credentials = array();
        if ($role_credentials) {
            foreach ($role_credentials as $role_credential) {
                $old_credentials[] = $role_credential['credential_id'];
            }
        }


        if ($this->request->isPost()) {
            $authRoleCredential_obj->deleteByRoleId($this->role_id);

            $successArray = array();
            foreach ($credentials as $credential) {
                $data = array(
                    'role_id' => $this->role_id,
                    'credential_id' => $credential
                );
                $success = $authRoleCredential_obj->insert($data);
                if ($success) {
                    $successArray [$credential][1] = $credential;
                } else {
                    $successArray [$credential][0] = $credential;
                }
            }

            foreach ($credentials as $credential) {
                if (empty($successArray[$credential][1])) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in all Credential in this Role"));
                }
            }
            $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Add all Credential to Role successfully"));



            $this->_redirect($this->router->assemble(array(), 'settingsAuthRoleList'));
        }

        $this->view->role_id = $this->role_id;
        $this->view->old_credentials = $old_credentials;
    }

}

