<?php

class Settings_LabelTypeController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();


        // main menu selected item
        $this->view->main_menu = 'settings';
        // sub menu selected item
        $this->view->sub_menu = 'settingsLabel';

        BreadCrumbs::setLevel(2, 'Label Type');
    }

    /**
     * Items list action
     */
    public function indexAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsLabelTypeList'));

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        //
        // get data list
        //
        
        $modelLabel = new Model_Label();
        $this->view->data = $modelLabel->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);

        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
    }

    /**
     * Add new item action
     */
    public function addAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsLabelTypeAdd'));

        //
        // get request parameters
        //
        $labelName = $this->request->getParam('label_name');
        $router = Zend_Controller_Front::getInstance()->getRouter();

        //
        // init action form
        //
        $form = new Settings_Form_LabelType();

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $modelLabel = new Model_Label();
                $data = array(
                    'label_name' => $labelName,
                    'company_id' => CheckAuth::getCompanySession()
                );


                $success = $modelLabel->insert($data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Label Type"));
                }
                echo 1;
                exit;
            } else {

                $messages = $form->getElement('label_name')->getErrorMessages();
                if ($messages) {

                    $form->getElement('label_name')->clearErrorMessages();
                    foreach ($messages as $message) {
                        $form->getElement('label_name')->addError($message);
                    }
                }
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('label-type/add_edit.phtml');
        exit;
    }

    public function deleteAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsLabelTypeDelete'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
        if ($id) {
            $ids[] = $id;
        }

        $modelLabel = new Model_Label();
        foreach ($ids as $id) {
            if (CheckAuth::checkIfCanHandelAllCompany('label', $id)) {

                $isNotRelated = $modelLabel->checkBeforeDeleteLabel($id);
                if ($isNotRelated) {
                    $modelLabel->deleteById($id);
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "could not be deleted ,This Item used in other Place "));
                }
            }
        }
        $this->_redirect($this->router->assemble(array(), 'settingsLabelTypeList'));
    }

    public function editAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsLabelTypeEdit'));

        //
        // get request parameters
        //
        $labelType = $this->request->getParam('label_name');
        $id = $this->request->getParam('id');


        if (!CheckAuth::checkIfCanHandelAllCompany('label', $id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // validation
        //
       $modelLabel = new Model_Label();
        $modelLabelOld = $modelLabel->getById($id);
        if (!$modelLabelOld) {

            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));

            $this->_redirect($this->router->assemble(array(), 'settingsLabelTypeList'));
            return;
        }


        //
        // init action form
        //
        $form = new Settings_Form_LabelType(array('mode' => 'update', 'label_name' => $modelLabelOld));


        //
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'label_name' => $labelType,
                    'company_id' => CheckAuth::getCompanySession()
                );

                $success = $modelLabel->updateById($id, $data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in LabelType"));
                }

                echo 1;
                exit;
            }
        }


        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('label-type/add_edit.phtml');
        exit;
    }

}

