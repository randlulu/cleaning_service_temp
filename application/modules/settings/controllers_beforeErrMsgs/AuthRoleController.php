<?php

class Settings_AuthRoleController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsUser';

        BreadCrumbs::setLevel(2, 'User Role');
    }

    /**
     * Items list action
     */
    public function indexAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsAuthRoleList'));

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'role_id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        //
        // get data list
        //
        $authRoleObj = new Model_AuthRole();
        $this->view->data = $authRoleObj->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);

        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
    }

    /**
     * Add new item action
     */
    public function addAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsAuthRoleAdd'));


        //
        // get request parameters
        //
        $roleName = $this->request->getParam('role_name');
        $defaultPage = $this->request->getParam('default_page');
        $router = Zend_Controller_Front::getInstance()->getRouter();

        //
        // init action form
        //
        $form = new Settings_Form_AuthRole();

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $authRoleObj = new Model_AuthRole();
                $data = array(
                    'view_role_name' => $roleName,
                    'role_name' => $authRoleObj->createRoleName($roleName),
                    'default_page' => $defaultPage
                );

                $success = $authRoleObj->insert($data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Role"));
                }

                //$this->_redirect($this->router->assemble(array(), 'settingsAuthRoleList'));
                echo 1;
                exit;
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('auth-role/add_edit.phtml');
        exit;
    }

    public function deleteAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsAuthRoleDelete'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
        if ($id) {
            $ids[] = $id;
        }

        $authRoleObj = new Model_AuthRole();
        foreach ($ids as $id) {
            $authRole = $authRoleObj->getById($id);
            if (!$authRole['is_core']) {

                $isNotRelated = $authRoleObj->checkBeforeDeleteAuthRole($id);
                if ($isNotRelated) {
                    $authRoleObj->deleteById($id);
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "could not be deleted ,This Item used in other Place "));
                }
            }
        }
        $this->_redirect($this->router->assemble(array(), 'settingsAuthRoleList'));
    }

    public function editAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsAuthRoleEdit'));

        //
        // get request parameters
        //
        $roleName = $this->request->getParam('role_name');
        $defaultPage = $this->request->getParam('default_page');
        $id = $this->request->getParam('id');


        //
        // validation
        //
        $authRoleObj = new Model_AuthRole();
        $authRole = $authRoleObj->getById($id);
        if (!$authRole) {
            $this->_redirect($this->router->assemble(array(), 'settingsAuthRoleList'));
            return;
        }


        //
        // init action form
        //
        $form = new Settings_Form_AuthRole(array('mode' => 'update', 'authRole' => $authRole));


        //
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'view_role_name' => $roleName,
                    'default_page' => $defaultPage
                );
                if ($authRole['view_role_name'] != $roleName) {
                    $data['role_name'] = $authRoleObj->createRoleName($roleName);
                }

                $success = $authRoleObj->updateById($id, $data);
                //$this->view->successMessage = 'Updated Successfully';

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Role"));
                }

                echo 1;
                exit;
                //$this->_redirect($this->router->assemble(array(), 'settingsAuthRoleList'));
                //return;
            }
        }

        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('auth-role/add_edit.phtml');
        exit;
    }

}

