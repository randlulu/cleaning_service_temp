<?php

class Settings_RateTagController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'settings';
//        $this->view->sub_menu = 'settingsPayment';
//        BreadCrumbs::setLevel(2, 'Bank Accounts');
    }

    /**
     * Items list action
     */
    public function indexAction() {

        //
        // check Auth for logged user
        //
//        CheckAuth::checkPermission(array('settingsBankList'));
        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'bank_id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        //
        // get data list
        //
        $model_ratingTag = new Model_RatingTag();
        $this->view->data = $model_ratingTag->getAll();

        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
    }

    /**
     * Add new item action
     */
    public function addAction() {

        //
        // check Auth for logged user
        //
//        CheckAuth::checkPermission(array('settingsBankAdd'));
        //
        // get request parameters
        //
        $model_ratingtag = new Model_RatingTag();
//    
        $router = Zend_Controller_Front::getInstance()->getRouter();

        //
        // init action form
        //
        $form = new Settings_Form_RateTag();

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $tag_name = $this->request->getParam("name");
                $data = array(
                    'tag_name' => $tag_name
                );



                $success = $model_ratingtag->insert($data);


                if ($success) {

                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {

                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Bank"));
                }
                //$this->_redirect($this->router->assemble(array(), 'settingsBankList'));
                echo 1;
                exit;
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('rate-tag/add_edit.phtml');
        exit;
    }

    public function deleteAction() {

        //
        // check Auth for logged user
        //
//        CheckAuth::checkPermission(array('settingsBankDelete'));
        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);


        $model_ratingtag = new Model_RatingTag();

        $model_ratingtag->deleteById($id);

        $this->_redirect($this->router->assemble(array(), 'rateTag'));
    }

    public function editAction() {


        $id = $this->request->getParam('id');
        $model_ratingtag = new Model_RatingTag();
//      

        $router = Zend_Controller_Front::getInstance()->getRouter();

        //
        // init action form
        //
//        $form = new Settings_Form_RateTag();
        //
        // validation
        //
//        $bankObj = new Model_Bank();
        $rateTag = $model_ratingtag->getById($id);
        if (!$rateTag) {
            $this->_redirect($this->router->assemble(array(), 'rateTag'));
            return;
        }

        //
        // init action form
        //
        $form = new Settings_Form_RateTag(array('mode' => 'update', 'ratingTag' => $rateTag));


        //
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $tag_name = $this->request->getParam("name");
                $data = array(
                    'tag_name' => $tag_name
                );


                $success = $model_ratingtag->update($data, "rating_tag_id = '{$id}'");

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Bank"));
                }

                echo 1;
                exit;
                //$this->_redirect($this->router->assemble(array(), 'settingsBankList'));
                //return;
            }
        }

        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('rate-tag/add_edit.phtml');
        exit;
    }

}
