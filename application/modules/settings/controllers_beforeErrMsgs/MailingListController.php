<?php

class Settings_MailingListController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
	
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
		
		$this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsMailingList';

        BreadCrumbs::setLevel(2, 'Mailing List');
		
		$this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

    }
	

    /**
     * Items list action
     */
    public function indexAction() {
	
	   //
        // check Auth for logged user
        //
		
        CheckAuth::checkPermission(array('settingsMailingList'));

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'mailing_list_id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
 
        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];
		$filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // get data list
        //
        $mailingListObj = new Model_MailingList();
        $this->view->data = $mailingListObj->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);

        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
	  
	
	}
	
	public function addAction() {
	
	  CheckAuth::checkPermission(array('settingsMailingListAdd'));
	  $name = $this->request->getParam('name','');
	  $filters = $this->request->getParam('fltr',array());
	  $form = new Settings_Form_MailingList();
	  
	  
	   if ($this->request->isPost()) { // check if POST request method
	      if(empty($name)){
				$is_valid = $form->isValid($this->request->getPost());
			 }else{
			   $is_valid = true;
			 }
			if ($is_valid) { // validate form data
			  
			 
			  $customer = new Model_Customer();
			  if($filters['booking_date']){			    
			    $filters['booking_date'] = strtotime($filters['booking_date']);
			  }
			  if($filters['enquiry_date']){
			    $filters['enquiry_date'] = strtotime($filters['enquiry_date']);
			  }
			  if($filters['estimate_date']){
			    $filters['estimate_date'] = strtotime($filters['estimate_date']);
			  }
			  if($filters['deferred_date']){
			    $filters['deferred_date'] = strtotime($filters['deferred_date']);
			  }
			  
			  //$customers = $customer->getCustomersId($filters);
              $country_id = CheckAuth::getCountryId();
              $booking_date = $deferred_date = $enquiry_date = $estimate_date = '';	
              $company_id = CheckAuth::getCompanySession();
			  $loggedUser = CheckAuth::getLoggedUser();
			  $userId = $loggedUser['user_id'];
			  $mailing_list_data = array('name'=>$name,
			                              'country_id'=>$country_id,
										  'state'=>$filters['state'],
										  'city_id'=>$filters['city_id'],
										  'suburb'=>$filters['suburb'],
										  'customer_type_id'=>$filters['customer_type_id'],
										  'status_id'=>$filters['status'],
										  'booking_date'=>$filters['booking_date'],
										  'inquiry_date'=>$filters['enquiry_date'],
										  'estimate_date'=>$filters['estimate_date'],
										  'deferred_date'=>$filters['deferred_date'],
										  'service_id'=>$filters['service_id'],
										  'label_id'=>$filters['label_id'],
										  'company_id'=>$company_id,
										  'created'=>time(),
										  'created_by'=>$userId,
										  );
										  
			
			
			  //insert into mailing list
			  $mailingListObj = new Model_MailingList();
			  $mailing_list_id = $mailingListObj->insert($mailing_list_data);
			   
			  if($mailing_list_id){
			   /* foreach($customers as $customer){
			    $data = array('customer_id' => $customer['customer_id'] ,
				              'mailing_list_id' => $mailing_list_id

				);			
				// insert into mailing_list_emails
				$mailingListEmailsObj = new Model_MailingListEmails();
				$mailingListEmailsObj->insert($data);
			    }*/
			     $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
              } else {
                 $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Mailing List"));
              }
			  
			  $this->_redirect($this->router->assemble(array(), 'settingsMailingList'));
			}
		}	
	  
	  $this->view->form = $form;

	}
	
	public function editAction() {
	
	 CheckAuth::checkPermission(array('settingsMailingListEdit'));
	 
	  $name = $this->request->getParam('name','');
	  $filters = $this->request->getParam('fltr',array());
	  $id = $this->request->getParam('id');
	  //get mailing list by Id;
	   $mailingListObj = new Model_MailingList();
       $mailingList = $mailingListObj->getById($id);
	   $country_id = CheckAuth::getCountryId();
        if (!$mailingList) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect($this->router->assemble(array(), 'settingsMailingList'));
            return;
        }
	  $form = new Settings_Form_MailingList(array('mode' => 'update', 'mailingList' => $mailingList));
	  if ($this->request->isPost()) { // check if POST request method
	       if(empty($name)){
				$is_valid = $form->isValid($this->request->getPost());
			 }else{
			   $is_valid = true;
			 }
            if ($is_valid) { // validate form data
			if($filters['booking_date']){			    
			    $filters['booking_date'] = strtotime($filters['booking_date']);
			  }
			  if($filters['enquiry_date']){
			    $filters['enquiry_date'] = strtotime($filters['enquiry_date']);
			  }
			  if($filters['estimate_date']){
			    $filters['estimate_date'] = strtotime($filters['estimate_date']);
			  }
			  if($filters['deferred_date']){
			    $filters['deferred_date'] = strtotime($filters['deferred_date']);
			  }
			
                $mailing_list_data = array('name'=>$name,
			                              'country_id'=>$country_id,
										  'state'=>$filters['state'],
										  'city_id'=>$filters['city_id'],
										  'suburb'=>$filters['suburb'],
										  'customer_type_id'=>$filters['customer_type_id'],
										  'status_id'=>$filters['status'],
										  'booking_date'=>$filters['booking_date'],
										  'inquiry_date'=>$filters['enquiry_date'],
										  'deferred_date'=>$filters['deferred_date'],
										  'service_id'=>$filters['service_id'],
										  'label_id'=>$filters['label_id'],
										  );

                $success = $mailingListObj->updateById($id, $mailing_list_data);               
                if ($success) {
				    // delete all customers in mailing list 
					/*$mailingListEmailsObj = new Model_MailingListEmails();
					$mailingListEmailsObj->deleteByMailingListId($id);
					
				    $customer = new Model_Customer();
			        $customers = $customer->getCustomersId($filters);
                    foreach($customers as $customer){
			            $data = array('customer_id' => $customer['customer_id'] ,
				              'mailing_list_id' => $id);
				       
				        // insert into mailing_list_emails	
                        $mailingListEmailsObj->insert($data);				
			          }*/					
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Mailing List"));
                }


                $this->_redirect($this->router->assemble(array(), 'settingsMailingList'));
            }
        }
		
		$this->view->form = $form;
		$this->view->mailingList = $mailingList;
		
	}
	
	    public function deleteAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsMailingListDelete'));


        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
        if ($id) {
            $ids[] = $id;
        }

        $mailingListObj = new Model_MailingList();
        //$mailingListEmailsObj = new Model_MailingListEmails();
						
        foreach ($ids as $id) {
            $mailingListObj->deleteById($id);
			// delete mailing list emails by mailing_list_id
			//$mailingListEmailsObj->deleteByMailingListId($id);	
        }
        $this->_redirect($this->router->assemble(array(), 'settingsMailingList'));
    }
	
	
	public function sendEmailAction(){
	
	
	    CheckAuth::checkPermission(array('sendEmail'));
		
	    $mailing_list_id = $this->request->getParam('mailing_list_id','');		
		$id = $this->request->getParam('id');
		if(isset($id)){
		  $modelMarketingEmailLog = new Model_MarketingEmailLog();
		  $marketingEmailLog = $modelMarketingEmailLog->getById($id);
		  $form = new Settings_Form_SendEmailMailingList(array('marketingEmailLog' => $marketingEmailLog));
		}else{
		 $form = new Settings_Form_SendEmailMailingList();
		}
		
		$mailingListObj = new Model_MailingList();
		$customertObj = new Model_Customer();
		$EmailTemplateObj = new Model_EmailTemplate();
		$mailingListUnsubscribedObj = new Model_MailingListUnsubscribed();
		
		if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
			  $mailingList  = $mailingListObj->getById($mailing_list_id);
			  $mailingListFilters = array('state'=>$mailingList['state'],
			                               'city_id'=>$mailingList['city_id'],
			                               'suburb'=>$mailingList['suburb'],
			                               'customer_type_id'=>$mailingList['customer_type_id'],
			                               'status_id'=>$mailingList['status_id'],
			                               'booking_date'=>$mailingList['booking_date'],
			                               'inquiry_date'=>$mailingList['inquiry_date'],
			                               'estimate_date'=>$mailingList['estimate_date'],
			                               'deferred_date'=>$mailingList['deferred_date'],
			                               'service_id'=>$mailingList['service_id'],
			                               'label_id'=>$mailingList['label_id'],
			                       );
				$unsubscribedCustomerEmails = $mailingListUnsubscribedObj->getByMailingListId($mailing_list_id);
				

				
				$not_customer_ids = array();
				$unsubscribe_customers = array();
				foreach($unsubscribedCustomerEmails as $unsubscribedCustomerEmail){
				  if(count($mailingListUnsubscribedObj->getByCustomerId($unsubscribedCustomerEmail['customer_id'])) == 3){
				    $not_customer_ids[$unsubscribedCustomerEmail['customer_id']] = $unsubscribedCustomerEmail['customer_id'];
				   }else{
				    $unsubscribe_customers[$unsubscribedCustomerEmail['customer_id']] = $unsubscribedCustomerEmail;
				   }
				}
				if($not_customer_ids){
				 $mailingListFilters['not_customer_ids'] = $not_customer_ids;
				}else{
				 $mailingListFilters['not_customer_ids'] = '';
				}
				
				$matchCustomers = $customertObj->allCustomers($mailingListFilters);
				//var_dump($matchCustomers);
				//exit;
				
				
				
				
				if($matchCustomers){
				$emailsCount = 0;
				foreach($matchCustomers as $key=>$matchCustomer){
				  //$customer = $customertObj->getById($customerId['customer_id']);
				   $to = array();
					if ($matchCustomer['email1']) {
						$to[0]['email'] = $matchCustomer['email1'];
						$to[0]['number'] = '1';
						
					}
					if ($matchCustomer['email2']) {
						$to[1]['email'] = $matchCustomer['email2'];
						$to[1]['number'] = '2';
						
					}
					if ($matchCustomer['email3']) {
						$to[2]['email'] = $matchCustomer['email3'];
						$to[2]['number'] = '3';
						
					}
					
				
				    
					
					if($unsubscribe_customers){
					 foreach($unsubscribe_customers as $unsubscribe_customer){
					   if($unsubscribe_customer['customer_id'] == $matchCustomer['customer_id']){					     
					     if($matchCustomer['email1'] == $unsubscribe_customer['customer_email']){
						    unset($to[0]);
						 }else if($matchCustomer['email2'] == $unsubscribe_customer['customer_email']){
						    unset($to[1]);
						 }else if($matchCustomer['email3'] == $unsubscribe_customer['customer_email']){
						    unset($to[2]);
						 }
						break; 
					   }
					 }
					}
					
					
					if (count($to) == 0){
					 
					 unset($matchCustomers[$key]);
					 
					}
					
					$emailsCount = $emailsCount + count($to);
	

            
			
		  }
		  


		  
		}
	    
		if(!empty($matchCustomers)){
         $this->buildCustomerFile($matchCustomers);	
       	}	
		
		echo json_encode(array('customers'=>$matchCustomers , 'CustomerCount'=>count($matchCustomers) , 'EmailsCount'=>$emailsCount));
		exit;
	  }else{
        $messages = $form->getMessages();
		echo json_encode(array('errors'=>$messages));
	    exit;
      }
    
    	
	 
   }

        $this->view->form = $form;	
}
	
	
	public function buildCustomerFile($customers){
	  

		
	  $view = new Zend_View();
	  $view->setScriptPath(APPLICATION_PATH . '/modules/settings/views/scripts/mailing-list');
      $view->customers = $customers;
      $bodyCustomers = $view->render('all-customers.phtml');
	  $company_id = CheckAuth::getCompanySession();
      $subdir = date('Y/m/d/');
      $dir = get_config('attachment');
	  $fullDir = $dir . '/' . $subdir;
      $loggedUser = CheckAuth::getLoggedUser();	
	  $user_id = $loggedUser['user_id'];
        //check if file exists or not
        if (!is_dir($fullDir)) {
            mkdir($fullDir, 0777, true);
        }

        $fileName = "{$fullDir}customers_{$user_id}_{$company_id}.xls";

        $mode = 'x+';
        if (file_exists($fileName)) {
            $mode = 'w+';
			chmod($fileName, 0777);
        }

		
        $fhandler = fopen($fileName, $mode);
        fwrite($fhandler, $bodyCustomers);
        fclose($fhandler);
	    
	}
	
	public function saveCustomersXlAction(){
	
	  
        $loggedUser = CheckAuth::getLoggedUser();
        $company_id = CheckAuth::getCompanySession();
		
	    $user_id = $loggedUser['user_id'];
		$subdir = date('Y/m/d/');
        $dir = get_config('attachment');
	    $fullDir = $dir . '/' . $subdir;
		$fileName = "{$fullDir}customers_{$user_id}_{$company_id}.xls";
		
		
		$mailing_list_id = $this->request->getParam('id','');
		if(isset($mailing_list_id) && !empty($mailing_list_id)){
	            $this->getMatchCustomers($mailing_list_id);
			}
			
		if (file_exists($fileName)){
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename="'.basename($fileName).'"');
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . filesize($fileName));
			readfile($fileName);
			exit;
       }          
   exit;
  }
	
	
	public function getMatchCustomers($mailing_list_id){
	
	          $mailingListObj = new Model_MailingList();
		      $customertObj = new Model_Customer();
		      $EmailTemplateObj = new Model_EmailTemplate();
		      $mailingListUnsubscribedObj = new Model_MailingListUnsubscribed();
		
		      $mailingList  = $mailingListObj->getById($mailing_list_id);
			  $mailingListFilters = array('state'=>$mailingList['state'],
			                               'city_id'=>$mailingList['city_id'],
			                               'suburb'=>$mailingList['suburb'],
			                               'customer_type_id'=>$mailingList['customer_type_id'],
			                               'status_id'=>$mailingList['status_id'],
			                               'booking_date'=>$mailingList['booking_date'],
			                               'inquiry_date'=>$mailingList['inquiry_date'],
			                               'estimate_date'=>$mailingList['estimate_date'],
			                               'deferred_date'=>$mailingList['deferred_date'],
			                               'service_id'=>$mailingList['service_id'],
			                               'label_id'=>$mailingList['label_id'],
			                       );
				$unsubscribedCustomerEmails = $mailingListUnsubscribedObj->getByMailingListId($mailing_list_id);
				

				
				$not_customer_ids = array();
				$unsubscribe_customers = array();
				foreach($unsubscribedCustomerEmails as $unsubscribedCustomerEmail){
				  if(count($mailingListUnsubscribedObj->getByCustomerId($unsubscribedCustomerEmail['customer_id'])) == 3){
				    $not_customer_ids[$unsubscribedCustomerEmail['customer_id']] = $unsubscribedCustomerEmail['customer_id'];
				   }else{
				    $unsubscribe_customers[$unsubscribedCustomerEmail['customer_id']] = $unsubscribedCustomerEmail;
				   }
				}
				if($not_customer_ids){
				 $mailingListFilters['not_customer_ids'] = $not_customer_ids;
				}else{
				 $mailingListFilters['not_customer_ids'] = '';
				}
				
				$matchCustomers = $customertObj->allCustomers($mailingListFilters);
				if($matchCustomers){
				$emailsCount = 0;
				foreach($matchCustomers as $key=>$matchCustomer){
				   $to = array();
					if ($matchCustomer['email1']) {
						$to[0]['email'] = $matchCustomer['email1'];
						$to[0]['number'] = '1';						
					}
					if ($matchCustomer['email2']) {
						$to[1]['email'] = $matchCustomer['email2'];
						$to[1]['number'] = '2';						
					}
					if ($matchCustomer['email3']) {
						$to[2]['email'] = $matchCustomer['email3'];
						$to[2]['number'] = '3';						
					}
					if($unsubscribe_customers){
					 foreach($unsubscribe_customers as $unsubscribe_customer){
					   if($unsubscribe_customer['customer_id'] == $matchCustomer['customer_id']){					     
					     if($matchCustomer['email1'] == $unsubscribe_customer['customer_email']){
						    unset($to[0]);
						 }else if($matchCustomer['email2'] == $unsubscribe_customer['customer_email']){
						    unset($to[1]);
						 }else if($matchCustomer['email3'] == $unsubscribe_customer['customer_email']){
						    unset($to[2]);
						 }
						break; 
					   }
					 }
					}										
					if (count($to) == 0){					 
					 unset($matchCustomers[$key]);					 
					}
					$emailsCount = $emailsCount + count($to);
		  }
		}
		
		if(!empty($matchCustomers)){
         $this->buildCustomerFile($matchCustomers);	
		}
	
	
	}
	
	public function confirmSendEmailAction() {
	
	    
		CheckAuth::checkPermission(array('sendEmail'));
		
		$customersIds = $this->request->getParam('customers');
		$matchCustomers = json_decode($customersIds);
		
		$result = array();
		  if(!empty($matchCustomers)){
			foreach ($matchCustomers as $key => &$value) {
			  $result2 = array();	  
			  foreach($value as $k=>$v){
				$result2[$k] = $v; 
			  }
			  $result[$key] = $result2;
			}
		  }
	  
		$matchCustomers = $result;
        $mailing_list_id = $this->request->getParam('mailing_list_id','');
		$email_template_id = $this->request->getParam('emailTemplate','');
		$body = $this->request->getParam('body','');
		$subject = $this->request->getParam('subject','');
		
		$id = $this->request->getParam('id');
		if(isset($id)){
		  $modelMarketingEmailLog = new Model_MarketingEmailLog();
		  $marketingEmailLog = $modelMarketingEmailLog->getById($id);
		  $form = new Settings_Form_SendEmailMailingList(array('marketingEmailLog' => $marketingEmailLog));
		}else{
		 $form = new Settings_Form_SendEmailMailingList();
		}
		
		$mailingListObj = new Model_MailingList();
		$customertObj = new Model_Customer();
		$EmailTemplateObj = new Model_EmailTemplate();
		$mailingListUnsubscribedObj = new Model_MailingListUnsubscribed();
		
		if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
			  /*$mailingList  = $mailingListObj->getById($mailing_list_id);
			  $mailingListFilters = array('state'=>$mailingList['state'],
			                               'city_id'=>$mailingList['city_id'],
			                               'suburb'=>$mailingList['suburb'],
			                               'customer_type_id'=>$mailingList['customer_type_id'],
			                               'status_id'=>$mailingList['status_id'],
			                               'booking_date'=>$mailingList['booking_date'],
			                               'inquiry_date'=>$mailingList['inquiry_date'],
			                               'estimate_date'=>$mailingList['estimate_date'],
			                               'deferred_date'=>$mailingList['deferred_date'],
			                               'service_id'=>$mailingList['service_id'],
			                               'label_id'=>$mailingList['label_id'],
			                       );*/
				$unsubscribedEmails = $mailingListUnsubscribedObj->getByMailingListId($mailing_list_id);
				$not_customer_ids = array();
				$customer_ids = array();
				foreach($unsubscribedEmails as $email){
				  if(count($mailingListUnsubscribedObj->getByCustomerId($email['customer_id'])) == 3){
				    $not_customer_ids[$email['customer_id']] = $email['customer_id'];
				   }else{
				    $customer_ids[$email['customer_id']] = $email;
				   }
				}
				if($not_customer_ids){
				 $mailingListFilters['not_customer_ids'] = $not_customer_ids;
				}else{
				 $mailingListFilters['not_customer_ids'] = '';
				}
				
				//$customersIds = $customertObj->test($mailingListFilters);
				
				
				
				
				$emailTemplateData  = $EmailTemplateObj->getById($email_template_id);
				//$modelCannedResponses = new Model_CannedResponses();
                //$cannedResponse = $modelCannedResponses->getById($email_template_id);
				if($matchCustomers){
				foreach($matchCustomers as $matchCustomer){
				  
				  //$customer = $customertObj->getById($customerId);
				   $to = array();
					if ($matchCustomer['email1']) {
						$to[0]['email'] = $matchCustomer['email1'];
						$to[0]['number'] = '1';
					}
					if ($matchCustomer['email2']) {
						$to[1]['email'] = $matchCustomer['email2'];
						$to[1]['number'] = '2';
					}
					if ($matchCustomer['email3']) {
						$to[2]['email'] = $matchCustomer['email3'];
						$to[2]['number'] = '3';
					}
					
				
				    
					
					if($customer_ids){
					 foreach($customer_ids as $customer_id){
					   if($customer_id['customer_id'] == $matchCustomer['customer_id']){					     
					     if($matchCustomer['email1'] == $customer_id['customer_email']){
						    unset($to[0]);
						 }else if($matchCustomer['email2'] == $customer_id['customer_email']){
						    unset($to[1]);
						 }else if($matchCustomer['email3'] == $customer_id['customer_email']){
						    unset($to[2]);
						 }
						break; 
					   }
					 }
					}
					
					
					
					
					
					
					
					
					
				  
				  $template_params = array(
					//customer
					'{customer_name}' => get_customer_name($matchCustomer),
					'{customer_first_name}' => isset($matchCustomer['first_name']) && $matchCustomer['first_name'] ? ucwords($matchCustomer['first_name']) : '',
					'{customer_last_name}' => isset($matchCustomer['last_name']) && $matchCustomer['last_name'] ? ' ' . ucwords($matchCustomer['last_name']) : '',
					'{customer_contacts}' => nl2br($customertObj->getCustomerContacts($matchCustomer['customer_id']))
					);
					
				  foreach($to as $toC){
					   
					   $template_params['{unsubscribe_link}'] = $this->router->assemble(array('mailing_id' => $mailing_list_id , 'customer_id'=> $matchCustomer['customer_id'] , 'email'=> $toC['number']), 'settingsMailingListUnsubscribe');
					   
					    $emailTemplate = $EmailTemplateObj->getEmailTemplate($emailTemplateData['name'], $template_params);

				       if(empty($subject)){
				        $subject = $emailTemplate['subject'];
				      }
				    
				      $body = $emailTemplate['body'];
			          $body = $body;
					   
					  
					   //$body = $body . "<a style='background-color: #1ab394;border-color: #1ab394;color: #FFFFFF;border-radius:3px;padding:6px 12px;text-decoration:none;' href='".$this->router->assemble(array('mailing_id' => $mailing_list_id , 'customer_id'=> $customer['customer_id'] , 'email'=> $toC['number']), 'settingsMailingListUnsubscribe')."'>Unsubscribe</a>" ; 
					   
					   $params = array(
							'to' => $toC['email'],
							'body' => $body,
							'subject' => $subject
							//'cc' => 'ayman@tilecleaners.com.au'
							//'layout' => 'designed_email_template'
						);
						
						$error_mesages = array();
            
			
						if (EmailNotification::validation($params, $error_mesages)) {
							// Send Email
							$success = EmailNotification::sendEmail($params, '', array(), array());
						}
					  
					}
					
					
				  
			
			
		
			
			  /*$body = $cannedResponse['body'];
		      $subjectTemplate = $cannedResponse['subject'];
			  $subject = $this->request->getParam('subject',$subjectTemplate);
			  //$to = $customer['email1'];
			
			  $params = array(
                'to' => $to,
                'body' => $body,
                'subject' => $subject
            );*/

            
			
		  }
		  
		  if ($success){
					$this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Email sent successfully"));
				} else{
					$this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Email not sent"));
				}

		  
		}else{
		  $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "There is no customer in this mailing list"));
		}
		
		echo 1;
		exit;
	}
   }

        $this->view->form = $form;		

	}
	
	public function unsubscribeAction(){
	  
	    $this->view->main_menu = 'Mailing List';
        $this->view->sub_menu = 'settingsMailingList';

        BreadCrumbs::setLevel(2, 'Unsubscribe User');
		
	    $email_address = $this->request->getParam('email_address','');
		$mailing_list_id = $this->request->getParam('mailing_id','');
		$other_reason = $this->request->getParam('other-reason',''); 
		$why = $this->request->getParam('why',$other_reason);
		$email = $this->request->getParam('email','');
		$customer_id = $this->request->getParam('customer_id','');
		$cronjob_id = $this->request->getParam('cronjob_id',0);
		$email_template_id = $this->request->getParam('email_template_id',0);
		$trading_name_id = $this->request->getParam('trading_name_id',0);
		
		$customerObj = new Model_Customer();
		$customer = $customerObj->getById($customer_id);
		
		$mailingListObj = new Model_MailingList();
		$mailingList = $mailingListObj->getById($mailing_list_id);
		
		if($cronjob_id){
		  $mode = 'cronjob';
		}else if($email_template_id){
		   $mode = 'emailtemplate';
		}else{
		 $mode = 'mailing_list';
		}
		
		
		$form = new Settings_Form_Unsubscribe(array('customer'=>$customer ,'email'=>$email, 'mode' => $mode));
		
			
		$mailingListUnsubscribedObj = new Model_MailingListUnsubscribed();
		if ($this->request->isPost()) { // check if POST request method
		  
            if ($form->isValid($this->request->getPost())) { // validate form data
			 
			  if($why == 'Other'){
			   $other_reason = $this->request->getParam('other-reason',$why); 
			   $why = $other_reason;
			  }
			 
			  $data = array('customer_id'=> $customer_id,
			  'mailing_list_id'=>$mailing_list_id,
			  'created'=>time(),
			  'cronjob_id'=>$cronjob_id,
			  'email_template_id'=>$email_template_id,
			  'user_id'=> $customer_id,
			  'customer_email'=>$email_address,
			  'unsubscribe_reason_id'=>$why
			  );
			  
			  if($cronjob_id){
			    $unsubscribeEmail = $mailingListUnsubscribedObj->getByCustomerIdAndEmailAndCronJob($cronjob_id ,$customer_id ,$email_address);
			  }else if($email_template_id){
			    $unsubscribeEmail = $mailingListUnsubscribedObj->getByCustomerIdAndEmailAndEmailTemplate($email_template_id ,$customer_id ,$email_address);			  
			  }else{
			    $unsubscribeEmail = $mailingListUnsubscribedObj->getByCustomerIdAndEmailAndMailingList($mailing_list_id ,$customer_id ,$email_address);
			  }
			  
			 
			 if($unsubscribeEmail){
			   $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This email is already unsubscribed "));
			 }else{
			 $success = $mailingListUnsubscribedObj->insert($data); 
			 if($success){
			  $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Unsubscribed successfully"));
              } else {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "you don't have permission to unsubscribed"));
              }
			 } 
			  
			  $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
			}
		}
		
		
		$trading_namesObj = new Model_TradingName();
	    $trading_name = $trading_namesObj->getById($trading_name_id);
		
		
		$this->view->form = $form;
		$this->view->trading_name = $trading_name;
		$this->view->mailingList = $mailingList;
	
	}
	
	
	public function mailingListEmailsAction(){
	
	    $id = $this->request->getParam('id','');
	   
	    $mailingListObj = new Model_MailingList();
		$mailingListUnsubscribedObj = new Model_MailingListUnsubscribed();
		$customertObj = new Model_Customer();
		
	    $mailingList  = $mailingListObj->getById($id);
	    $mailingListFilters = array('state'=>$mailingList['state'],
			                               'city_id'=>$mailingList['city_id'],
			                               'suburb'=>$mailingList['suburb'],
			                               'customer_type_id'=>$mailingList['customer_type_id'],
			                               'status_id'=>$mailingList['status_id'],
			                               'booking_date'=>$mailingList['booking_date'],
			                               'inquiry_date'=>$mailingList['inquiry_date'],
			                               'estimate_date'=>$mailingList['estimate_date'],
			                               'deferred_date'=>$mailingList['deferred_date'],
			                               'service_id'=>$mailingList['service_id'],
			                               'label_id'=>$mailingList['label_id'],
			                       );
		$unsubscribedEmails = $mailingListUnsubscribedObj->getByMailingListId($id);
		$not_customer_ids = array();
				foreach($unsubscribedEmails as $email){
				  if(count($mailingListUnsubscribedObj->getByCustomerId($email['customer_id'])) == 3){
				    $not_customer_ids[$email['customer_id']] = $email['customer_id'];
				   }
				}
				if($not_customer_ids){
				 $mailingListFilters['not_customer_ids'] = $not_customer_ids;
				}else{
				 $mailingListFilters['not_customer_ids'] = '';
				}
				
		$customersIds = $customertObj->allCustomers($mailingListFilters);
	   
	   $this->view->customers = $customersIds; 
	   $this->view->name = $mailingList['name']; 
	   echo $this->view->render('mailing-list/emails.phtml');
       exit;
	
	}
	
    
	
}
?>