<?php

class Settings_ContractorVehicleController extends Zend_Controller_Action {

    private $request;
    private $router;
    private $contractor_info_id;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
        $this->contractor_info_id = $this->request->getParam('contractor_info_id');

        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsUser';

        //
        // get data
        //
        $contractorInfoObj = new Model_ContractorInfo();
        $contractorInfo = $contractorInfoObj->getById($this->contractor_info_id);

        BreadCrumbs::setLevel(4, 'Contractor Vehicles');
    }

    /**
     * Items list action
     */
    public function indexAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsContractorVehicleList'));

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'vehicle_id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if($filters){
            foreach ($filters as &$filter){
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];


        //
        //get user email by id
        //
        $filters['contractor_info_id'] = $this->contractor_info_id;


        //
        // get data list
        //
        $contractorVehicleObj = new Model_ContractorVehicle();
        $this->view->data = $contractorVehicleObj->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);

        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
        $this->view->contractor_info_id = $this->contractor_info_id;
    }

    /**
     * Add new item action
     */
    public function addAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsContractorVehicleAdd'));

        //
        // get request parameters
        //
        $registrationNumber = $this->request->getParam('registration_number');
        $make = $this->request->getParam('make');
        $model = $this->request->getParam('model');
        $colour = $this->request->getParam('colour');
        $router = Zend_Controller_Front::getInstance()->getRouter();


        if (!CheckAuth::checkIfCanHandelAllCompany('contractor_info', $this->contractor_info_id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // init action form
        //
        $form = new Settings_Form_ContractorVehicle(array('contractor_info_id' => $this->contractor_info_id));

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $contractorVehicleObj = new Model_ContractorVehicle();
                $data = array(
                    'registration_number' => $registrationNumber,
                    'make' => $make,
                    'model' => $model,
                    'colour' => $colour,
                    'contractor_info_id' => $this->contractor_info_id
                );

                $success = $contractorVehicleObj->insert($data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Contractor Vehicle"));
                }
                //$this->_redirect($this->router->assemble(array(), 'settingsUserEmailList'));
                echo 1;
                exit;
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('contractor-vehicle/add_edit.phtml');
        exit;
    }

    public function deleteAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsContractorVehicleDelete'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
        if ($id) {
            $ids[] = $id;
        }

        $contractorVehicleObj = new Model_ContractorVehicle();
        foreach ($ids as $id) {
            if (CheckAuth::checkIfCanHandelAllCompany('contractorVehicle', $id)) {
                $contractorVehicleObj->deleteById($id);
            }
        }
        $this->_redirect($this->router->assemble(array('contractor_info_id' => $this->contractor_info_id), 'settingsContractorVehicleList'));
    }

}

