<?php

class Settings_AttributeController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsServices';

        BreadCrumbs::setLevel(2, 'Attribute');
    }

    /**
     * Items list action
     */
    public function indexAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsAttributeList'));

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'attribute_id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        //
        // get data list
        //
        $attributeObj = new Model_Attributes();
		
        $this->view->data = $attributeObj->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);
			
        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
    }

    /**
     * Add new item action
     */
    public function addAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsAttributeAdd'));

        //
        // get request parameters
        //
        $attributeName = $this->request->getParam('attribute_name');
        $attributeTypeId = $this->request->getParam('attribute_type_id');
        $extraInfo = $this->request->getParam('extra_info');
        $attributeVariableName = $this->request->getParam('attribute_variable_name');
        $isPriceAttribute = $this->request->getParam('is_price_attribute');
        $router = Zend_Controller_Front::getInstance()->getRouter();
        $companyId = $this->request->getParam('company_id');

        //
        // init action form
        //
        $form = new Settings_Form_Attribute();

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $attributeObj = new Model_Attributes();
                $data = array(
                    'attribute_name' => $attributeName,
                    'attribute_type_id' => $attributeTypeId,
                    'extra_info' => $extraInfo,
                    'company_id' => $companyId,
                    'attribute_variable_name' => $attributeVariableName,
                    'is_price_attribute' => $isPriceAttribute
                );

                $success = $attributeObj->insert($data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Attribute"));
                }

                //$this->_redirect($this->router->assemble(array(), 'settingsAttributeList'));
                echo 1;
                exit;
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('attribute/add_edit.phtml');
        exit;
    }

    public function deleteAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsAttributeDelete'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
        if ($id) {
            $ids[] = $id;
        }

        $attributeObj = new Model_Attributes();
        foreach ($ids as $id) {

            if (CheckAuth::checkIfCanHandelAllCompany('attribute', $id)) {
                $isNotRelated = $attributeObj->checkBeforeDeleteAttribute($id);
                if ($isNotRelated) {
                    $attributeObj->deleteById($id);
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "could not be deleted ,This Item used in other Place "));
                }
            }
        }
        $this->_redirect($this->router->assemble(array(), 'settingsAttributeList'));
    }

    public function editAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsAttributeEdit'));

        //
        // get request parameters
        //
        $attributeName = $this->request->getParam('attribute_name');
        $attributeTypeId = $this->request->getParam('attribute_type_id');
        $extraInfo = $this->request->getParam('extra_info');
        $attributeVariableName = $this->request->getParam('attribute_variable_name');
        $isPriceAttribute = $this->request->getParam('is_price_attribute');
        $companyId = $this->request->getParam('company_id');
        $id = $this->request->getParam('id');





        //
        // validation
        //
        $attributeObj = new Model_Attributes();
        $attribute = $attributeObj->getById($id);

        if (!CheckAuth::checkIfCanHandelAllCompany('attribute', $id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        if (!$attribute) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));

            $this->_redirect($this->router->assemble(array(), 'settingsAttributeList'));
            return;
        }


        //
        // init action form
        //
        $form = new Settings_Form_Attribute(array('mode' => 'update', 'attribute' => $attribute));


        //
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'attribute_name' => $attributeName,
                    'attribute_type_id' => $attributeTypeId,
                    'extra_info' => $extraInfo,
                    'company_id' => $companyId,
                    'attribute_variable_name' => $attributeVariableName,
                    'is_price_attribute' => $isPriceAttribute
                );

                $success = $attributeObj->updateById($id, $data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Attribute"));
                }
                //$this->view->successMessage = 'Updated Successfully';
                echo 1;
                exit;
                //$this->_redirect($this->router->assemble(array(), 'settingsAttributeList'));
                //return;
            }
        }

        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('attribute/add_edit.phtml');
        exit;
    }

	public function orderAttributeAction() {
		$modelAttributes = new Model_Attributes();
		$attributes = $this->request->getParam('attributeOrder',array());
		$attributes = json_decode($attributes);
		//print_r($attributes);
		$result = array();
		
		foreach ($attributes as $key => $value){
						//echo 'key '.$key;
						//echo 'value '.$value;
						$att = array();
			foreach ($value as $k => $v){
							
							$att[$k] = $v;
						
			}
			
			$data = array('order'=>$att['order']);
			$modelAttributes->updateByAttributeName($att['attributeName'], $data);
			//$result[$key] = $att;
		}
		
		echo json_encode(array('msg' => 'done'));
        exit;
	}
	
	
}

