<?php

class Settings_CitiesController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsLocation';

        BreadCrumbs::setLevel(2, 'Cities');
    }

    /**
     * Items list action
     */
    public function indexAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsCitiesList'));

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'city_id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        //
        // get data list
        //
        $CitiesObj = new Model_Cities();
        $this->view->data = $CitiesObj->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);

        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
    }

    /**
     * Add new item action
     */
    public function addAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsCitiesAdd'));

        //
        // get request parameters
        //
        $name = $this->request->getParam('name');
        $phone_key = $this->request->getParam('phone_key');
        $country_id = $this->request->getParam('country_id');
        $postcode_key = $this->request->getParam('postcode_key');
        $state = $this->request->getParam('state');
        $router = Zend_Controller_Front::getInstance()->getRouter();

        //
        // init action form
        //
        $form = new Settings_Form_Cities();

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $CitiesObj = new Model_Cities();
                $data = array(
                    'city_name' => $name,
                    'phone_key' => $phone_key,
                    'country_id' => $country_id,
                    'postcode_key' => $postcode_key,
                    'state' => $state
                );

                $success = $CitiesObj->insert($data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in City"));
                }

                //update General Contractor
                $modelUser = new Model_User();
                $modelUser->updateGeneralContractor();

                //$this->_redirect($this->router->assemble(array(), 'settingsCitiesList'));
                echo 1;
                exit;
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('cities/add_edit.phtml');
        exit;
    }

    public function deleteAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsCitiesDelete'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
        if ($id) {
            $ids[] = $id;
        }

        $CitiesObj = new Model_Cities();
        foreach ($ids as $id) {
            $isNotRelated = $CitiesObj->checkBeforeDeleteCity($id);
            if ($isNotRelated) {
                $CitiesObj->deleteById($id);
            } else {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "could not be deleted ,This Item used in other Place "));
            }
        }
        $this->_redirect($this->router->assemble(array(), 'settingsCitiesList'));
    }

    public function editAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsCitiesEdit'));


        //
        // get request parameters
        //
        $phone_key = $this->request->getParam('phone_key');
        $country_id = $this->request->getParam('country_id');
        $postcode_key = $this->request->getParam('postcode_key');
        $state = $this->request->getParam('state');
        $name = $this->request->getParam('name');
        $id = $this->request->getParam('id');


        //
        // validation
        //
        $CitiesObj = new Model_Cities();
        $Cities = $CitiesObj->getById($id);
        if (!$Cities) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect($this->router->assemble(array(), 'settingsCitiesList'));
            return;
        }


        //
        // init action form
        //
        $form = new Settings_Form_Cities(array('mode' => 'update', 'Cities' => $Cities));


        //
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'city_name' => $name,
                    'phone_key' => $phone_key,
                    'country_id' => $country_id,
                    'postcode_key' => $postcode_key,
                    'state' => $state
                );

                $success = $CitiesObj->updateById($id, $data);
                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in City"));
                }

                //update General Contractor
                $modelUser = new Model_User();
                $modelUser->updateGeneralContractor();

                //$this->view->successMessage = 'Updated Successfully';
                echo 1;
                exit;
                //$this->_redirect($this->router->assemble(array(), 'settingsCitiesList'));
                //return;
            }
        }

        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('cities/add_edit.phtml');
        exit;
    }

}

