<?php

class Settings_DeclarationOfChemicalsController extends Zend_Controller_Action {

    private $request;
    private $router;
    private $contractor_info_id;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router  = Zend_Controller_Front::getInstance()->getRouter();
        $this->contractor_info_id = $this->request->getParam('contractor_info_id');
        
        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsUser';
        
        //
        // get data
        //
        $contractorInfoObj = new Model_ContractorInfo();
        $contractorInfo = $contractorInfoObj->getById($this->contractor_info_id);
        
        BreadCrumbs::setLevel(4, 'Declaration Of Chemicals');
    }


    /**
     * Items list action
     */
    public function indexAction() {
        
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsDeclarationOfChemicalsList'));
        
        //
        // get request parameters
        //
        $orderBy        = $this->request->getParam('sort', 'id');
        $sortingMethod  = $this->request->getParam('method', 'asc');
        $currentPage    = $this->request->getParam('page', 1);
        $filters        = $this->request->getParam('fltr', array());
        
        

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage     = 20;
        $pager->currentPage = $currentPage;
        $pager->url         = $_SERVER['REQUEST_URI'];

        
        //
        //get user email by id
        //
        $filters['contractor_info_id'] = $this->contractor_info_id;
        
        
        //
        // get data list
        //
        $declarationOfChemicalsObj = new Model_DeclarationOfChemicals();
        $this->view->data = $declarationOfChemicalsObj->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);
        
        //
        // set view params
        //
        $this->view->currentPage   = $currentPage;
        $this->view->perPage       = $pager->perPage;
        $this->view->pageLinks     = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy       = $orderBy;
        $this->view->filters       = $filters;
        $this->view->contractor_info_id = $this->contractor_info_id;
        
    }


    /**
     * Add new item action
     */
    public function addAction() {
        
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsDeclarationOfChemicalsAdd'));
        
        //
        // get request parameters
        //
        $chemicals    = $this->request->getParam('chemicals');
        $router = Zend_Controller_Front::getInstance()->getRouter();
        
        //
        // init action form
        //
        $form = new Settings_Form_DeclarationOfChemicals(array('contractor_info_id'=>  $this->contractor_info_id));

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $declarationOfChemicalsObj = new Model_DeclarationOfChemicals();
                $data = array(
                    'chemicals'   => $chemicals,
                    'contractor_info_id' => $this->contractor_info_id
                );

                $success = $declarationOfChemicalsObj->insert($data);
                
                if($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Declaration Of Chemicals"));
                }
                //$this->_redirect($this->router->assemble(array(), 'settingsUserEmailList'));
                echo 1;
                exit;
            }
        }

        $this->view->form       = $form;
        //
        // render views
        //
        echo $this->view->render('declaration-of-chemicals/add_edit.phtml');
        exit;
    }



    public function deleteAction() {
        
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsDeclarationOfChemicalsDelete'));
        
        //
        // get request parameters
        //
        $id  = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array() );
        if($id) {
            $ids[] = $id;
        }

        $declarationOfChemicalsObj = new Model_DeclarationOfChemicals();
        foreach($ids as $id) {
            $declarationOfChemicalsObj->deleteById($id);
        }
        $this->_redirect($this->router->assemble(array('contractor_info_id' => $this->contractor_info_id), 'settingsDeclarationOfChemicalsList'));

    }
}

