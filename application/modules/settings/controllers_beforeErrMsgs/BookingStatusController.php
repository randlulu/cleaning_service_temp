<?php

class Settings_BookingStatusController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsType';

        BreadCrumbs::setLevel(2, 'Booking Status');
    }

    /**
     * Items list action
     */
    public function indexAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsBookingStatusList'));

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'booking_status_id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        //
        // get data list
        //
        $modelBookingStatus = new Model_BookingStatus();
        $this->view->data = $modelBookingStatus->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);

        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
 
    }

    /**
     * Add new item action
     */
    public function addAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsBookingStatusAdd'));

        //
        // get request parameters
        //
        $name = $this->request->getParam('name');


        $router = Zend_Controller_Front::getInstance()->getRouter();

        //
        // init action form
        //
        $form = new Settings_Form_BookingStatus();

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $modelBookingStatus = new Model_BookingStatus();
                $data = array(
                    'name' => $name,
                    'company_id' => CheckAuth::getCompanySession()
                );

                $success = $modelBookingStatus->insert($data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Booking Status"));
                }
                //$this->_redirect($this->router->assemble(array(), 'settingsBookingStatusList'));
                echo 1;
                exit;
            } else {
                $messages = $form->getElement('name')->getErrorMessages();
                if ($messages) {
                    $form->getElement('name')->clearErrorMessages();
                    foreach ($messages as $message) {
                        $form->getElement('name')->addError($message);
                    }
                }
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('booking-status/add_edit.phtml');
        exit;
    }

    public function deleteAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsBookingStatusDelete'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());


        if ($id) {
            $ids[] = $id;
        }

        $modelBookingStatus = new Model_BookingStatus();

        foreach ($ids as $id) {
            $bookingStatus = $modelBookingStatus->getById($id);
            if (!$bookingStatus['is_core']) {

                $isNotRelated = $modelBookingStatus->checkBeforeDeleteBookingStatus($id);
                if ($isNotRelated) {
                    $modelBookingStatus->deleteById($id);
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "could not be deleted ,This Item used in other Place "));
                }
            }
        }
        $this->_redirect($this->router->assemble(array(), 'settingsBookingStatusList'));
    }

    public function editAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsBookingStatusEdit'));

        //
        // get request parameters
        //
        $name = $this->request->getParam('name');
        $id = $this->request->getParam('id');

        //
        // validation
        //
        $modelBookingStatus = new Model_BookingStatus();
        $bookingStatus = $modelBookingStatus->getById($id);
        if (!$bookingStatus) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect($this->router->assemble(array(), 'settingsBookingStatusList'));
            return;
        }


        //
        // init action form
        //
        $form = new Settings_Form_BookingStatus(array('mode' => 'update', 'bookingStatus' => $bookingStatus));


        //
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                if (!$bookingStatus['is_core']) {
                    $data = array(
                        'name' => $name,
                        'company_id' => CheckAuth::getCompanySession()
                    );

                    $success = $modelBookingStatus->updateById($id, $data);

                    //$this->view->successMessage = 'Updated Successfully';
                    if ($success) {
                        $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                    } else {
                        $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Booking Status"));
                    }
                }
                echo 1;
                exit;
                //$this->_redirect($this->router->assemble(array(), 'settingsBookingStatusList'));
                //return;
            }
        }

        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('booking-status/add_edit.phtml');
        exit;
    }

    public function changeColorAction() {
        //
        // check Auth for logged user
        //
       CheckAuth::checkPermission(array('settingsBookingStatusChangeColor'));

        //
        // get request parameters
        //
        $color = $this->request->getParam('color');
        $id = $this->request->getParam('id');


        //
        // validation
        //
        $modelBookingStatus = new Model_BookingStatus();
        $bookingStatus = $modelBookingStatus->getById($id);
        if (!$bookingStatus) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect($this->router->assemble(array(), 'settingsBookingStatusChangeColor'));
            return;
        }


        //
        // init action form
        //
        $form = new Settings_Form_BookingStatusChangeColor(array('mode' => 'update', 'bookingStatus' => $bookingStatus));


        //
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'color' => $color
                );

                $success = $modelBookingStatus->updateById($id, $data);

                //$this->view->successMessage = 'Updated Successfully';
                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Booking Status"));
                }

                echo 1;
                exit;
                //$this->_redirect($this->router->assemble(array(), 'settingsBookingStatusList'));
                //return;
            }
        }

        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('booking-status/change-color.phtml');
        exit;
    }

    public function addDescriptionAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsBookingStatusAddDescription'));

        //
        // get request parameters
        //
        $description = $this->request->getParam('description');
        $id = $this->request->getParam('id');

        //
        // validation
        //
        $modelBookingStatus = new Model_BookingStatus();
        $bookingStatus = $modelBookingStatus->getById($id);
        if (!$bookingStatus) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect($this->router->assemble(array(), 'settingsBookingStatusList'));
            return;
        }


        //
        // init action form
        //
        $form = new Settings_Form_BookingStatusAddDescription(array('bookingStatus' => $bookingStatus));


        //
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $success = $modelBookingStatus->updateById($id, array('description' => $description));

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Booking Status"));
                }

                echo 1;
                exit;
            }
        }

        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('booking-status/add-description.phtml');
        exit;
    }

    public function googleCalenderAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsBookingStatusGoogleCalender'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id');
        $push_google_calender = $this->request->getParam('push_google_calender');
        $delete_google_calender = $this->request->getParam('delete_google_calender');
        $requestFeedback = $this->request->getParam('request_feedback');
        $consider_for_nearest_booking_feature = $this->request->getParam('consider_for_nearest_booking_feature');

        //
        // validation
        //
        $modelBookingStatus = new Model_BookingStatus();
        $bookingStatus = $modelBookingStatus->getById($id);
        if (!$bookingStatus) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect($this->router->assemble(array(), 'settingsBookingStatusList'));
            return;
        }


        //
        // init action form
        //
        $form = new Settings_Form_BookingStatusGoogleCalender(array('bookingStatus' => $bookingStatus));

        //
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $success = $modelBookingStatus->updateById($id, array('push_google_calender' => $push_google_calender, 'delete_google_calender' => $delete_google_calender, 'request_feedback' => $requestFeedback , 'consider_for_nearest_booking_feature' => $consider_for_nearest_booking_feature));

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Booking Status"));
                }

                echo 1;
                exit;
            }
        }

        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('booking-status/google-calender.phtml');
        exit;
    }

    public function showDescriptionAction() {

        //
        // check Auth for logged user
        //
        //CheckAuth::checkPermission(array('settingsBookingStatusAddDescription'));

        //
        // get request parameters
        //
      
        $id = $this->request->getParam('id');

        //
        // validation
        //
        $modelBookingStatus = new Model_BookingStatus();
        $bookingStatus = $modelBookingStatus->getById($id);
        if (!$bookingStatus) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect($this->router->assemble(array(), 'settingsBookingStatusList'));
            return;
        }

        $this->view->bookingStatus = $bookingStatus;


        //
        // render views
        //
        echo $this->view->render('booking-status/show-description.phtml');
        exit;
    }

}

