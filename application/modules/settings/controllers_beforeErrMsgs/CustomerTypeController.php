<?php

class Settings_CustomerTypeController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();


        // main menu selected item
        $this->view->main_menu = 'settings';
        // sub menu selected item
        $this->view->sub_menu = 'settingsType';

        BreadCrumbs::setLevel(2, 'Customer Type');
    }

    /**
     * Items list action
     */
    public function indexAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsCustomerTypeList'));

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'customer_type_id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        //
        // get data list
        //
        $customerTypeObj = new Model_CustomerType();
        $this->view->data = $customerTypeObj->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);

        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
    }

    /**
     * Add new item action
     */
    public function addAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsCustomerTypeAdd'));

        //
        // get request parameters
        //
        $customerType = $this->request->getParam('customer_type');
        $WorkOrder = $this->request->getParam('is_work_order', 0);

        $router = Zend_Controller_Front::getInstance()->getRouter();

        //
        // init action form
        //
        $form = new Settings_Form_CustomerType();

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $customerTypeObj = new Model_CustomerType();
                $data = array(
                    'customer_type' => $customerType,
                    'company_id' => CheckAuth::getCompanySession(),
                    'is_work_order' => $WorkOrder
                );

                $success = $customerTypeObj->insert($data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Customer Type"));
                }
                echo 1;
                exit;
            } else {
                $messages = $form->getElement('customer_type')->getErrorMessages();
                if ($messages) {
                    $form->getElement('customer_type')->clearErrorMessages();
                    foreach ($messages as $message) {
                        $form->getElement('customer_type')->addError($message);
                    }
                }
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('customer-type/add.phtml');
        exit;
    }

    public function deleteAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsCustomerTypeDelete'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
        if ($id) {
            $ids[] = $id;
        }

        $customerTypeObj = new Model_CustomerType();
        foreach ($ids as $id) {
            $isCore = $customerTypeObj->getById($id);
            if (!$isCore['is_core'] && CheckAuth::checkIfCanHandelAllCompany('customer_type', $id)) {
                $isNotRelated = $customerTypeObj->checkBeforeDeleteInquiryCustomerType($id);
                if ($isNotRelated) {
                    $customerTypeObj->deleteById($id);
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "could not be deleted ,This Item used in other Place "));
                }
            }
        }
        $this->_redirect($this->router->assemble(array(), 'settingsCustomerTypeList'));
    }

    public function editAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsCustomerTypeEdit'));

        //
        // get request parameters
        //
        $customerType = $this->request->getParam('customer_type');
        $id = $this->request->getParam('id');

        if (!CheckAuth::checkIfCanHandelAllCompany('customer_type', $id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // validation
        //
        $customerTypeObj = new Model_CustomerType();
        $customerTypeOld = $customerTypeObj->getById($id);
        if (!$customerTypeOld) {

            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));

            $this->_redirect($this->router->assemble(array(), 'settingsCustomerTypeList'));
            return;
        }


        //
        // init action form
        //
        $form = new Settings_Form_CustomerType(array('mode' => 'update', 'customerType' => $customerTypeOld));


        //
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                if (!$customerTypeOld['is_core']) {
                    $data = array(
                        'customer_type' => $customerType,
                        'company_id' => CheckAuth::getCompanySession(),
                    );

                    $success = $customerTypeObj->updateById($id, $data);

                    if ($success) {
                        $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                    } else {
                        $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in CustomerType"));
                    }
                }
                echo 1;
                exit;
            }
        }


        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('customer-type/edit.phtml');
        exit;
    }

    public function changeWorkOrderAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsCustomerTypeChangeWorkOrder'));

        //
        // get request parameters
        //
     
        $id = $this->request->getParam('id');
        $WorkOrder = $this->request->getParam('is_work_order', 0);


        if (!CheckAuth::checkIfCanHandelAllCompany('customer_type', $id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // validation
        //
        $customerTypeObj = new Model_CustomerType();
        $customerTypeOld = $customerTypeObj->getById($id);
        if (!$customerTypeOld) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect($this->router->assemble(array(), 'settingsCustomerTypeList'));
            return;
        }


        //
        // init action form
        //
        $form = new Settings_Form_CustomerTypeChangeWorkOrder(array('mode' => 'update', 'customerType' => $customerTypeOld));


        //
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'is_work_order' => $WorkOrder
                );

                $success = $customerTypeObj->updateById($id, $data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Work Order"));
                }

                echo 1;
                exit;
            }
        }


        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('customer-type/change-work-order.phtml');
        exit;
    }

    public function asBookingAddressAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsCustomerTypeAsBookingAddress'));

        //
        // get request parameters
        //
     
        $id = $this->request->getParam('id');
        $asBookingAddress = $this->request->getParam('as_booking_address', 0);


        if (!CheckAuth::checkIfCanHandelAllCompany('customer_type', $id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // validation
        //
        $customerTypeObj = new Model_CustomerType();
        $customerTypeOld = $customerTypeObj->getById($id);
        if (!$customerTypeOld) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect($this->router->assemble(array(), 'settingsCustomerTypeList'));
            return;
        }


        //
        // init action form
        //
        $form = new Settings_Form_CustomerTypeAsBookingAddress(array('mode' => 'update', 'customerType' => $customerTypeOld));


        //
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'as_booking_address' => $asBookingAddress
                );

                $success = $customerTypeObj->updateById($id, $data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in As Booking Address"));
                }

                echo 1;
                exit;
            }
        }


        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('customer-type/as-booking-address.phtml');
        exit;
    }

}

