<?php

class Settings_FaqController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'F&Q';

        // BreadCrumbs::setLevel(2, 'Bank Accounts');
    } 

    public function indexAction() {
        $model_faq = new Model_Faq();
/**************Get $filters*************IBM*/
        $filters = $this->request->getParam('fltr', array());

          if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter); 
                }
            }
        }
        /****************************/          

        /***Filter FAQs By Services**********IBM*/

        $modelServices = new Model_Services();
        $allService = $modelServices->getAllService();
        $select = new Zend_Form_Element_Select('fltr');        
        $select->setBelongsTo('service');
        $select->setDecorators(array('ViewHelper'));
        $select->setValue(isset($filters['service']) ? $filters['service'] : '');
        $select->setAttrib('onchange', "fltr('" . $this->router->assemble(array(), 'faq') . "?fltr[service]=','service-fltr')");
        $select->setAttribs(array("class" => "select_field form-control"/*, 'id' => 'service_id'*/));
        $select->addMultiOption('', 'Select Service');
        $select->addMultiOptions($allService);
        $this->view->allServices = $select;
        /************End Filter*****************/


        $this->view->faq = $model_faq->getAll($filters);
    }

    public function addAction() {

        $form = new Settings_Form_Faq();
        $this->view->form = $form;
        if ($this->request->isPost()) {
            if ($form->isValid($this->request->getPost())) { // validate form data
                //get logged user 
                $loggedUser = CheckAuth::getLoggedUser();
                $loggedUserId = $loggedUser['user_id'];
                $question = $this->request->getParam('question');
                $answer = $this->request->getParam('answer');
                //$service = $this->request->getParam('service_id');
                //$floor = $this->request->getParam('floor_id');
                $faq_data = array(
                    'question' => $question,
                    'answer' => $answer,
                    'created' => time(),
                    'user_id' => $loggedUserId
                );
                $model_faq = new Model_Faq();
                $faq_id = $model_faq->insert($faq_data);
                /* $faq_service_data = array(
                  'faq_id' => $faq_id,
                  'service_id' => $service,
                  'floor_id' => $floor
                  );
                  $model_faqService = new Model_FaqService();
                  $success = $model_faqService->insert($faq_service_data); */

                $success = $faq_id;

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in F&Q"));
                }
                echo 1;
                exit;
            }
        }

        echo $this->view->render('faq/add_edit.phtml');
        exit;
    }

    public function editAction() {
        $faq_id = $this->request->getParam('id');

        $model_faq = new Model_Faq();
        $faqService = $model_faq->getById($faq_id);


        $form = new Settings_Form_Faq(array('mode' => 'update', 'faqService' => $faqService));
        $this->view->form = $form;
        if ($this->request->isPost()) {
            if ($form->isValid($this->request->getPost())) { // validate form data
                $question = $this->request->getParam('question');
                $answer = $this->request->getParam('answer');
                //$service = $this->request->getParam('service_id');
                //$floor = $this->request->getParam('floor_id');
                $loggedUser = CheckAuth::getLoggedUser();
                $loggedUserId = $loggedUser['user_id'];
                $faq_data = array(
                    'question' => $question,
                    'answer' => $answer
                );
                $model_faq = new Model_Faq();
                $success = $model_faq->updateById($faq_id, $faq_data);

                /* $faq_service_data = array(
                  'service_id' => $service,
                  'floor_id' => $floor
                  );
                  $faqServiceData = $model_faq->getById($faq_id);

                  $model_faqService = new Model_FaqService();
                  $success = $model_faqService->updateById($faqServiceData['faq_service_id'], $faq_service_data); */

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in F&Q"));
                }
                echo 1;
                exit;
            }
        }

        echo $this->view->render('faq/add_edit.phtml');
        exit;
    }

    public function deleteAction() {

        $id = $this->request->getParam('id', 0);
        $modelfaq = new Model_Faq();
        $modelFaqService = new Model_FaqService();

        if ($id) {
            $modelfaq->deleteById($id);
            $modelFaqService->deleteByFaqId($id);
        }

        $this->_redirect($this->router->assemble(array(), 'faq'));
    }

    public function addServiceFloorAction() {

        $service = $this->request->getParam('service_id');
        $faq_id = $this->request->getParam('faq_id');
        $floor = $this->request->getParam('floor_id');
        $model_faq = new Model_Faq();
        $model_attributeListValue = new Model_AttributeListValue();
        $faqService = $model_faq->getById($faq_id);
        $form = new Settings_Form_Faq(array('mode' => 'addService', 'faqService' => $faqService));



        $this->view->form = $form;
        if ($this->request->isPost()) {
            $floorData = $model_attributeListValue->getAllFloorByServiceID($service);
            $floor_array = "";
            foreach ($floorData as $floorElement) {
                $floor_array[$floorElement['attribute_value_id']] = $floorElement['attribute_value'];
            }
            $form->getElement('floor_id')->addMultiOptions($floor_array);

            if ($form->isValid($this->request->getPost())) { // validate form data
                $faq_service_data = array(
                    'faq_id' => $faq_id,
                    'service_id' => $service,
                    'floor_id' => $floor
                );
                $model_faqService = new Model_FaqService();
                $success = $model_faqService->insert($faq_service_data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in F&Q"));
                }
                echo 1;
                exit;
            }
        }

        echo $this->view->render('faq/add-service-floor.phtml');
        exit;
    }

    public function deleteServiceFloorAction() {

        $id = $this->request->getParam('id', 0);
        $modelFaqService = new Model_FaqService();

        if ($id) {
            $modelFaqService->deleteById($id);
        }

        $this->_redirect($this->router->assemble(array(), 'faq'));
    }

}
