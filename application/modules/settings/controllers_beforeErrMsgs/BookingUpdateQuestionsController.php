<?php

class Settings_BookingUpdateQuestionsController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsType';

        BreadCrumbs::setLevel(3, 'Update Booking Questions');
    }

    /**
     * Items list action
     */
    public function indexAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsBookingStatusList'));

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'update_booking_question_id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        //
        // get data list
        //
        $modelUpdateBookingQuestion = new Model_UpdateBookingQuestion();
        $this->view->data = $modelUpdateBookingQuestion->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);

        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
    }

    public function viewBookingStatusAction() {

        $router = Zend_Controller_Front::getInstance()->getRouter();
        $update_booking_question_id = $this->request->getParam('question_id', '0');
		$this->view->update_booking_question_id = $update_booking_question_id;
		
		$modelUpdateBookingQuestionStatus = new Model_UpdateBookingQuestionStatus();
		$status = $modelUpdateBookingQuestionStatus->getByQuestionId($update_booking_question_id);
        
        if ($this->request->isPost()){ // check if POST request method
           //print_r($_POST);
		   $modelUpdateBookingQuestionStatus->deleteByQuestionId($update_booking_question_id);
		   $success = 1;
		   foreach($_POST as $key=>$value){
				//echo '$key '.$key;
				//echo '$value '.$value;
				$success = $modelUpdateBookingQuestionStatus->insert(array('update_booking_question_id'=>$update_booking_question_id,'booking_status_id'=>$key));
		   }
		   if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Error, please try again"));
                }
		    echo 1;
            exit;
        
        }

		$this->view->status = $status;
        echo $this->view->render('booking-update-questions/view-booking-status.phtml');
        exit;
    }
	
	public function addUpdateQuestionAction() {

       
        $router = Zend_Controller_Front::getInstance()->getRouter();
        $update_booking_question = $this->request->getParam('question', '0');
        $update_booking_question_type = $this->request->getParam('type', '0');
		$modelUpdateBookingQuestion = new Model_UpdateBookingQuestion();
		
        if ($this->request->isPost()){ // check if POST request method
			$company_id = CheckAuth::getCompanySession();
			$loggedUser = CheckAuth::getLoggedUser();
			$created_by = $loggedUser['user_id'];
			$created = time();
			$data = array(
				'question_text'=>$update_booking_question,
				'question_type'=>$update_booking_question_type,
				'created_by'=>$created_by,
				'created'=>$created,
				'company_id'=>$company_id
			);
			  $success = $modelUpdateBookingQuestion->insert($data);
			if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Booking Status"));
                }
			//$this->_redirect($this->router->assemble(array(), 'settingsBookingUpdateQuestions'));
                echo 1;
                exit;
        }

        
        echo $this->view->render('booking-update-questions/add-edit-update-question.phtml');
        exit;
    }

	public function editUpdateQuestionAction() {

        //
        // get request parameters
        //
        $router = Zend_Controller_Front::getInstance()->getRouter();
        $update_booking_question_id = $this->request->getParam('question_id', '0');
        $update_booking_question = $this->request->getParam('question', '0');
        $update_booking_question_type = $this->request->getParam('type', '0');
		$modelUpdateBookingQuestion = new Model_UpdateBookingQuestion();
		$question = $modelUpdateBookingQuestion->getByQuestionId($update_booking_question_id);
		$this->view->update_booking_question = $question;
		
        if ($this->request->isPost()){ // check if POST request method
		$company_id = CheckAuth::getCompanySession();
			$loggedUser = CheckAuth::getLoggedUser();
			$created_by = $loggedUser['user_id'];
			$created = time();
			$data = array(
				'question_text'=>$update_booking_question,
				'question_type'=>$update_booking_question_type,
				'created_by'=>$created_by,
				'created'=>$created,
				'company_id'=>$company_id
			);
          $success = $modelUpdateBookingQuestion->updateByQuestionId($update_booking_question_id,$data);
			if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Booking Status"));
                }
			echo 1;
			exit;
        }
        echo $this->view->render('booking-update-questions/add-edit-update-question.phtml');
        exit;
    }

	public function deleteUpdateQuestionAction() {

        //
        // check Auth for logged user
        //
        //CheckAuth::checkPermission(array('settingsInquiryRequiredTypeAdd'));

        //
        // get request parameters
        //
        $router = Zend_Controller_Front::getInstance()->getRouter();
        $update_booking_question_id = $this->request->getParam('question_id', '0');
        $modelUpdateBookingQuestion = new Model_UpdateBookingQuestion();
		
        if ($update_booking_question_id){ // check if POST request method
          $success = $modelUpdateBookingQuestion->deleteByQuestionId($update_booking_question_id);
			if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Deleted successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Delete failed, try again"));
                }
			$this->_redirect($this->router->assemble(array(), 'settingsBookingUpdateQuestions'));
			exit;
        }
		//exit;
        
    }
	

}

