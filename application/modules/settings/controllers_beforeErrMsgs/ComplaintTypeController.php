<?php

class Settings_ComplaintTypeController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsType';

        BreadCrumbs::setLevel(2, 'Complaint Type');
    }

    /**
     * Items list action
     */
    public function indexAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsComplaintTypeList'));

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'complaint_type_id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        //
        // get data list
        //
        $complaintTypeObj = new Model_ComplaintType();
        $this->view->data = $complaintTypeObj->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);

        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
    }

    /**
     * Add new item action
     */
    public function addAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsComplaintTypeAdd'));

        //
        // get request parameters
        //
        $name = $this->request->getParam('name');
        $router = Zend_Controller_Front::getInstance()->getRouter();

        //
        // init action form
        //
        $form = new Settings_Form_ComplaintType();

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $complaintTypeObj = new Model_ComplaintType();
                $data = array(
                    'name' => $name,
                    'company_id' => CheckAuth::getCompanySession()
                );

                $success = $complaintTypeObj->insert($data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Complaint Type"));
                }
                //$this->_redirect($this->router->assemble(array(), 'settingsComplaintTypeList'));
                echo 1;
                exit;
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('complaint-type/add_edit.phtml');
        exit;
    }

    public function deleteAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsComplaintTypeDelete'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
        if ($id) {
            $ids[] = $id;
        }

        $complaintTypeObj = new Model_ComplaintType();
        foreach ($ids as $id) {
            if (CheckAuth::checkIfCanHandelAllCompany('complaint_type', $id)) {
                $isNotRelated = $complaintTypeObj->checkBeforeDeleteInquiryComplaintType($id);
                if ($isNotRelated) {
                    $complaintTypeObj->deleteById($id);
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "could not be deleted ,This Item used in other Place "));
                }
            }
        }
        $this->_redirect($this->router->assemble(array(), 'settingsComplaintTypeList'));
    }

    public function editAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsComplaintTypeEdit'));

        //
        // get request parameters
        //
        $name = $this->request->getParam('name');
        $id = $this->request->getParam('id');


        if (!CheckAuth::checkIfCanHandelAllCompany('complaint_type', $id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // validation
        //
        $complaintTypeObj = new Model_ComplaintType();
        $complaintType = $complaintTypeObj->getById($id);
        if (!$complaintType) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect($this->router->assemble(array(), 'settingsComplaintTypeList'));
            return;
        }


        //
        // init action form
        //
        $form = new Settings_Form_ComplaintType(array('mode' => 'update', 'complaintType' => $complaintType));


        //
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'name' => $name,
                    'company_id' => CheckAuth::getCompanySession()
                );

                $success = $complaintTypeObj->updateById($id, $data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Complaint Type"));
                }
                //$this->view->successMessage = 'Updated Successfully';
                echo 1;
                exit;
                //$this->_redirect($this->router->assemble(array(), 'settingsComplaintTypeList'));
                //return;
            }
        }

        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('complaint-type/add_edit.phtml');
        exit;
    }

}

