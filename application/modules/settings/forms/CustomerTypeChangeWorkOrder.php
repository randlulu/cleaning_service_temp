<?php

class Settings_Form_CustomerTypeChangeWorkOrder extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('CustomerTypeChangeWorkOrder');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $customerType = (isset($options['customerType']) ? $options['customerType'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();
        $request = Zend_Controller_Front::getInstance()->getRequest();


        $workOrderVal = (!empty($customerType['is_work_order']) ? $customerType['is_work_order'] : '');
        $workOrder = new Zend_Form_Element_Checkbox('is_work_order');
        $workOrder->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'checkbox_field'))
                ->setValue($workOrderVal);

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));

        $this->addElements(array($workOrder, $button));
        $this->setMethod('post');
        $this->setAction($router->assemble(array('id' => $customerType['customer_type_id']), 'settingsCustomerTypeChangeWorkOrder'));
   
    }

}


