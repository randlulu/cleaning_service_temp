<?php

class Settings_Form_ContractorVehicle extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('ContractorVehicle');
        $contractor_info_id = (isset($options['contractor_info_id']) ? $options['contractor_info_id'] : '');
		$contractor_Vehicle = (isset($options['contractor_Vehicle']) ? $options['contractor_Vehicle'] : '');
		$mode = (isset($options['mode']) ? $options['mode'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();

        $registrationNumber = new Zend_Form_Element_Text('registration_number');
        $registrationNumber->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setValue(isset($contractor_Vehicle['registration_number'])?$contractor_Vehicle['registration_number']:'')
                ->setAttribs(array('class' => 'form-control'))
                ->setErrorMessages(array('Required' => 'Please enter the registration number'));

        $make = new Zend_Form_Element_Text('make');
        $make->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
				->setValue(isset($contractor_Vehicle['make'])?$contractor_Vehicle['make']:'')
                ->setAttribs(array('class' => 'form-control'))
                ->setErrorMessages(array('Required' => 'Please enter the make'));

        $model = new Zend_Form_Element_Text('model');
        $model->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
				->setValue(isset($contractor_Vehicle['model'])?$contractor_Vehicle['model']:'')			
                ->setAttribs(array('class' => 'form-control'))
                ->setErrorMessages(array('Required' => 'Please enter the Model'));

        $colour = new Zend_Form_Element_Text('colour');
        $colour->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
				->setValue(isset($contractor_Vehicle['colour'])?$contractor_Vehicle['colour']:'')								
                ->setAttribs(array('class' => 'form-control'))
                ->setErrorMessages(array('Required' => 'Please enter the Colour'));


        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));

        $this->addElements(array($registrationNumber, $make, $model, $colour, $button));
        $this->setMethod('post');
		if($mode == 'update'){
		  $this->setAction($router->assemble(array('contractor_info_id' => $contractor_info_id,'id'=>$contractor_Vehicle['vehicle_id']), 'settingsContractorVehicleEdit'));
		}else{
		    $this->setAction($router->assemble(array('contractor_info_id' => $contractor_info_id), 'settingsContractorVehicleAdd'));
		}

    }

}

