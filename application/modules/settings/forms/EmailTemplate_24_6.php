<?php

class Settings_Form_EmailTemplate extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('EmailTemplate');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $emailTemplate = (isset($options['emailTemplate']) ? $options['emailTemplate'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();


        $name = new Zend_Form_Element_Text('name');
        $name->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($emailTemplate['name']) ? $emailTemplate['name'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the email template name'));
        if ('update' == $mode && (!get_config('under_construction'))) {
            $name->setAttribs(array('readonly' => 'readonly', 'style' => 'background-color: lightgray;'));
        }

        $companyId = CheckAuth::getCompanySession();
        $trading_name = new Zend_Form_Element_Select('trading_name');
        $trading_name->setDecorators(array('ViewHelper'))
            ->addDecorator('Errors', array('class' => 'errors'))
            ->setRequired()
            ->setAttribs(array('class' => 'form-control'))
            ->setValue((!empty($emailTemplate['trading_name_id']) ? $emailTemplate['trading_name_id'] : ''))
            ->setErrorMessages(array('Required' => 'Please select the trading name'));
        $table = new Model_TradingName();
        foreach ($table->getByCompanyId($companyId) as $c) {
            $trading_name->addMultiOption($c['trading_name_id'], $c['trading_name']);
        }

        $subject = new Zend_Form_Element_Text('subject');
        $subject->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($emailTemplate['subject']) ? $emailTemplate['subject'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the email subject'));



        if ('update' == $mode && (!get_config('under_construction'))) {
            $placeHolder = new Zend_Form_Element_Hidden('placeholder');
        } else {
            $placeHolder = new Zend_Form_Element_Text('placeholder');
        }
        $placeHolder->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($emailTemplate['placeholder']) ? $emailTemplate['placeholder'] : ''));



        $imagePlaceHolder = new Zend_Form_Element_Text('imagePlaceholder');
        $imagePlaceHolder->setDecorators(array('ViewHelper'))
            ->addDecorator('Errors', array('class' => 'errors'))
            ->setAttribs(array('class' => 'form-control'))
            ->setValue((!empty($emailTemplate['imagePlaceholder']) ? $emailTemplate['imagePlaceholder'] : ''));


        $body = new Zend_Form_Element_Textarea('body');
        $body->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($emailTemplate['body']) ? $emailTemplate['body'] : ' '))
                ->setErrorMessages(array('Required' => 'Please enter the email body'));


        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-default btn-primary'));

        $this->addElements(array($name, $subject, $body, $button, $placeHolder, $imagePlaceHolder ,$trading_name));
        $this->setMethod('post');
        if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $emailTemplate['id']), 'settingsEmailTemplateEdit'));
        } else {
            $this->setAction($router->assemble(array(), 'settingsEmailTemplateAdd'));
        }
    }

}

