<?php

class Settings_Form_Cities extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('Cities');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $cities = (isset($options['Cities']) ? $options['Cities'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();


        $name = new Zend_Form_Element_Text('name');
        $name->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($cities['city_name']) ? $cities['city_name'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the city name'));

        $phone_key = new Zend_Form_Element_Text('phone_key');
        $phone_key->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($cities['phone_key']) ? $cities['phone_key'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the phone key'));

        $country_id = new Zend_Form_Element_Select('country_id');
        $country_id->setDecorators(array('ViewHelper'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setValue((!empty($cities['country_id']) ? $cities['country_id'] : CheckAuth::getCountryId()))
                ->setErrorMessages(array('Required' => 'Please enter the Country'))
                ->addMultiOption('select one');
        $table = new Model_Countries();
        foreach ($table->getCountriesAsArray() as $c) {
            $country_id->addMultiOption($c['id'], $c['name']);
        }

        $postcode_key = new Zend_Form_Element_Text('postcode_key');
        $postcode_key->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($cities['postcode_key']) ? $cities['postcode_key'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the postcode key'));
		//********
		
		
		$timezone = new Zend_Form_Element_Select('timezone');
        $timezone->setDecorators(array('ViewHelper'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setValue((!empty($cities['timezone']) ? $cities['timezone'] : 1))
                ->setErrorMessages(array('Required' => 'Please enter the Country'))
                ->addMultiOption('select one');
        $tzlist = DateTimeZone::listIdentifiers(DateTimeZone::ALL);
        foreach ($tzlist as $c) {
			$timezone->addMultiOption($c,$c);
		}
		//********

        $state = new Zend_Form_Element_Text('state');
        $state->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($cities['state']) ? $cities['state'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the state'));

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));
        $this->addElements(array($name, $phone_key, $country_id, $postcode_key, $timezone, $state, $button));
        $this->setMethod('post');
        if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $cities['city_id']), 'settingsCitiesEdit'));
        } else {
            $this->setAction($router->assemble(array(), 'settingsCitiesAdd'));
        }
    }

}

