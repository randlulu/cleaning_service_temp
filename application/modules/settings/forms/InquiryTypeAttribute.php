<?php

class Settings_Form_InquiryTypeAttribute extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('InquiryTypeAttribute');
        $inquiry_type_id = (isset($options['inquiry_type_id']) ? $options['inquiry_type_id'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();


        $attributeId = new Zend_Form_Element_Select('attribute_id');
        $attributeId->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setErrorMessages(array('Required' => 'Please select the attribute'));

        $table = new Model_Attributes();
        $attributeId->addMultiOption('', 'Select One');
        foreach ($table->getAttributeAsArray() as $a) {
            $attributeId->addMultiOption($a['id'], $a['name']);
        }

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));

        $this->addElements(array($attributeId, $button));
        $this->setMethod('post');
        $this->setAction($router->assemble(array('inquiry_type_id' => $inquiry_type_id), 'settingsInquiryTypeAttributeAdd'));
    }

}

