<?php

class Settings_Form_ImageTypes extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('ImageTypes');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $ImageType = (isset($options['ImageType']) ? $options['ImageType'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();
        $request = Zend_Controller_Front::getInstance()->getRequest();



        $nameVal = (!empty($ImageType['name']) ? $ImageType['name'] : '');
        $name = new Zend_Form_Element_Text('name');
        $name->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field form-control'))
                ->setValue($nameVal);
        $value = $request->getParam('name');
        if ($nameVal != $value) {
            $modelImageTypes = new Model_ImageTypes();
            
            $notValid = $modelImageTypes->getByStatusNameAndCompany($value);
            if ($notValid) {
                $name->addError("A record matching '{$value}' was found");
            }
        }


        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));

        $this->addElements(array($name, $button));
        $this->setMethod('post');
        if ('update' == $mode) {

            $this->setAction($router->assemble(array('id' => $ImageType['image_types_id']), 'settingsImageTypesEdit'));
        } else {
            $this->setAction($router->assemble(array(), 'settingsImageTypesAdd'));
        }
    }

}

