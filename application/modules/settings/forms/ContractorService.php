<?php

class Settings_Form_ContractorService extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('ContractorService');
        $contractor_id = (isset($options['contractor_id']) ? $options['contractor_id'] : '');
		$old_services = (isset($options['old_services']) ? $options['old_services'] : '');
		$selected_values = array();
		foreach($old_services as $key=>$old_service){
		 $selected_values[$key] = $old_service['service_id'];
		}

        $router = Zend_Controller_Front::getInstance()->getRouter();

        $serviceId = new Zend_Form_Element_MultiCheckbox('service_id');
        $serviceId->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'checkbox_field'))
                ->setErrorMessages(array('Required' => 'Please Select at Least One Service'));
        $table = new Model_Services();
        $serviceId->addMultiOption('all', 'Select All');
        foreach ($table->getServiceAsArray() as $c) {
            $serviceId->addMultiOption($c['id'], $c['name']);
        }
		
		$serviceId->setValue($selected_values);

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));

        $this->addElements(array($serviceId, $button));
        $this->setMethod('post');
        $this->setAction($router->assemble(array('contractor_id' => $contractor_id), 'settingsContractorServiceAdd'));
    }

}

