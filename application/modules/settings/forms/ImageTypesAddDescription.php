<?php
class Settings_Form_ImageTypesAddDescription extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('ImageTypesAddDescription');
      
        $ImageType = (isset($options['ImageType']) ? $options['ImageType'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();
        $request = Zend_Controller_Front::getInstance()->getRequest();




        $descriptionVal = (!empty($ImageType['description']) ? $ImageType['description'] : '');
        $description = new Zend_Form_Element_Textarea('description');
        $description->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'textarea_field form-control', 'cols' => '40', 'rows' => '10'))
                ->setValue($descriptionVal);
       
        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));

        $this->addElements(array($description, $button));
        $this->setMethod('post');
        $this->setAction($router->assemble(array('id' => $ImageType['image_types_id']), 'settingsImageTypesAddDescription'));
     
    }

}

