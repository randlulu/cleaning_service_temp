<?php

class Settings_Form_Faq extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('faq');
        $faqService = $options['faqService'];
        $service_value = isset($faqService['service_id']) ? $faqService['service_id'] : '';
        $floor_value = isset($faqService['floor_id']) ? $faqService['floor_id'] : '';
        $mode = $options['mode'];
        $router = Zend_Controller_Front::getInstance()->getRouter();


        $question = new Zend_Form_Element_Textarea('question');
        $question->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control', 'rows' => '10'))
                ->setValue((!empty($faqService['question']) ? $faqService['question'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the question'));

        $answer = new Zend_Form_Element_Textarea('answer');
        $answer->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control', 'rows' => '10'))
                ->setValue((!empty($faqService['answer']) ? $faqService['answer'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the answer'));

        $modelServices = new Model_Services();
        $allService = $modelServices->getAllService();
        $service = new Zend_Form_Element_Select('service_id');
        $service->setDecorators(array('ViewHelper'));
        $service->setAttribs(array("class" => "select_field form-control", 'id' => 'service_field'));
        $service->setValue($service_value);
        $service->addMultiOption('', 'Select One');
        $service->addMultiOptions($allService);
		$service->setRequired();
        $service->setErrorMessages(array('Required' => 'Please select service '));
        $service->addDecorator('Errors', array('class' => 'errors'));


        $model_attributeListValue = new Model_AttributeListValue();
        $floorData = $model_attributeListValue->getAllFloorByServiceID($service_value);
        $floor_array = "";


        foreach ($floorData as $floor) {
            $floor_array[$floor['attribute_value_id']] = $floor['attribute_value'];
        }
       

        $floor = new Zend_Form_Element_Select('floor_id');
        $floor->setDecorators(array('ViewHelper'));
        $floor->setAttribs(array("class" => "select_field form-control"));
        $floor->setValue($floor_value);
        $floor->addMultiOption('', 'Select One');
        /*if ($mode == 'update') {
            $floor->addMultiOptions($floor_array);
        }*/
       // $floor->setRequired();
       // $floor->setErrorMessages(array('Required' => 'Please select Floor '));
       // $floor->addDecorator('Errors', array('class' => 'errors'));


        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));      
        $this->setMethod('post');

        if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $faqService['faq_id']), 'editFaq'));
			$this->addElements(array($question, $answer, $button));
        } else if('addService' == $mode){
		    $this->setAction($router->assemble(array('id' => $faqService['faq_id']), 'addServiceFloor'));
			$this->addElements(array($service, $floor, $button));
		}else {
            $this->setAction($router->assemble(array(), 'addFaq'));
			$this->addElements(array($question, $answer,$button));
        }
    }

}
