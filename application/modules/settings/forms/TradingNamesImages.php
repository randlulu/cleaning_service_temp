<?php

class Settings_Form_TradingNamesImages extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('TradingNamesImages');
        $router = Zend_Controller_Front::getInstance()->getRouter();
        $trading_name_id = (isset($options['trading_name_id']) ? $options['trading_name_id'] : '');
        $mode = (isset($options['mode']) ? $options['mode'] : '');


        $template_name = new Zend_Form_Element_Select('template_name');
        $template_name->setDecorators(array('ViewHelper'))
            //->addDecorator('Errors', array('class' => 'errors'))
            //->setRequired()
            ->setAttribs(array('class' => 'form-control'));
            //->setErrorMessages(array('Required' => 'Please select the Email template'));
        $table = new Model_EmailTemplate();
        foreach ($table->getAll() as $c) {
            $template_name->addMultiOption($c['id'], $c['name']);
        }

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));

        $this->addElements(array($template_name,$button));

        $this->setMethod('post');
        $this->setMethod(Zend_Form::METHOD_POST);
        $this->setEnctype(Zend_Form::ENCTYPE_MULTIPART);

        if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $trading_name_id), 'settingsCompaniesEdit'));
        } else {
            $this->setAction($router->assemble(array(), 'settingsCompaniesTradingNamesImages'));
        }
    }

}

