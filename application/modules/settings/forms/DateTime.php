<?php

class Settings_Form_DateTime extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('DateTimeConfiguration');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $dateTime = (isset($options['date_time']) ? $options['date_time'] : '');
        // var_dump($date_time);

        $router = Zend_Controller_Front::getInstance()->getRouter();

        $countryModel = new Model_Countries();

        $country_id = new Zend_Form_Element_Select('country_id');
        $country_id->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setAttribs(array('class' => 'form-control', 'onchange' => 'getTimeZone();getDateFormat;', 'id' => 'country_id'))
                ->setValue((!empty($dateTime['country_id']) ? $dateTime['country_id'] : ''))
                ->setErrorMessages(array('Required' => 'Please select the country'));
                $country_id->addMultiOption('', 'select');
        foreach ($countryModel->getCountriesAsArray() as $country) {
            $country_id->addMultiOption($country['id'], $country['name']);
            
        } 

// $table = new Model_Companies();
// $companyId = CheckAuth::getCompanySession();
// $country_ID = 9;
// $country = $countryModel->getById($country_id);


        if(isset($dateTime['country_id']) && !empty($dateTime['country_id'])){
            $country = $countryModel->getById($dateTime['country_id']);
            $timezone = DateTimeZone::listIdentifiers(DateTimeZone::PER_COUNTRY, $country['country_code']/*'AU'*/);
            $locale = Zend_Locale::getLocaleToTerritory($country['country_code']);
            // $locale = 'de_DE';
        }else{
            $timezone = timezone_identifiers_list();
            $locale = 'en_AU';

        }

        $time_zone = new Zend_Form_Element_Select('time_zone');
        $time_zone->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control', 'onchange' => 'getDateFormat();getTimeFormat();' , 'id' => 'time_zone'))
                ->setValue((!empty($dateTime['time_zone']) ? $dateTime['time_zone'] : ''));
        $time_zone->setRegisterInArrayValidator(false);
        $time_zone->addMultiOption('', 'Select timezone');
        // $time_zone->addMultiOptions($city_obj->getStateByCountryId((!empty($countryId) ? $countryId : $city['country_id'])));
            $localeDataDateFormats = Zend_Locale_Data::getList(Zend_Locale::findLocale(), 'date'); // Date formats for auto Locale (@see Zend_Locale::findLocale())
            $mm = zend_Locale::findLocale();
            $formatd = Zend_Locale_Data::getContent($locale, 'time');
            

            // $locale = new Zend_Locale('en_AU');
// Zend_Date::setOptions(array('format_type' => 'php'));
// $date = new Zend_Date('2012-05-28 02:43:00', false, $locale);
// print $date->toString('F j, Y, g:i a');
// var_dump($date);

        foreach($timezone as $timezone_identifier){
            $date_time_zone = new DateTimeZone($timezone_identifier);
            $date_time = new DateTime('now', $date_time_zone);
            // $timezone_offsets[$timezone_identifier] = $date_time_zone->getOffset($date_time);
            $timezone_offsets[$timezone_identifier] =  '(' . $date_time->format('P') . ' GMT) ' . '  ' . $timezone_identifier;
            // arsort($timezone_offsets);
            $time_zone->addMultiOption($timezone_identifier, $timezone_offsets[$timezone_identifier]);
        }



        $date_format = new Zend_Form_Element_Select('date_format');
        $date_format->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($dateTime['date_format']) ? $dateTime['date_format'] : ''))
                ->setErrorMessages(array('Required' => 'Please select the date format'));
        $date_format->addMultiOption('', 'Select date format');

        $localeObj = new Zend_Locale($locale);
        $dateFormatList = $localeObj->getTranslationList('date');
        $date = new Zend_Date();
        // foreach ($dateFormatList as $key => $dateFormat) {
        foreach ($dateFormatList as $key => $dateFormat) {
            
            
            // $formatType = "Zend_Date::DATE_". strtoupper($key); //Zend_Date::TIME_MEDIUM Zend_Date::DATE_FULL
            // $date = Zend_Date();
            // echo $key;
            $now = $key  . '   | ex: ' .$date->now($localeObj)->toString($dateFormat); 
            

            // $locale = new Zend_Locale('de_AT');
            // $date2 = new Zend_Date($date_now, false, $locale);
            // $date_now_ex =  $date2->toString("'Era:GGGG='GGGG, ' Date:yy.MMMM.dd'yy.MMMM.dd");

            // $date_now_ex = Zend_Locale_Format::getDate($date_now, array('date_format' =>$dateFormat));
            $date_format->addMultiOption($dateFormat , $now);
            // $date_format->addMultiOption($formatType , $formatType);           
           
        }
        // $date_format->addMultiOption('YYYY-MM-dd', 'YYYY-MM-dd | ex: 2016-10-05');
        // $date_format->addMultiOption('dd-MM-YYYY', 'dd-MM-YYYY | ex: 05-10-2016');

        $time_format = new Zend_Form_Element_Select('time_format');
        $time_format->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($dateTime['time_format']) ? $dateTime['time_format'] : ''))
                ->setErrorMessages(array('Required' => 'Please select the time format'));
        $zone = $time_zone->getValue('time_zone');
        // var_dump($values); exit;
        $time_format->addMultiOption('', 'Select date format');

        $localeObj = new Zend_Locale($locale);
        $timeFormatList = $localeObj->getTranslationList('time');
        $date = new Zend_Date();

        // echo $date->getTimezone();
        // $date->setTimezone('Brazil/Acre');
        // date_default_timezone_set('Europe/Vienna');
        if($zone){
            date_default_timezone_set($zone);
        }
        foreach ($timeFormatList as $key => $timeFormat) {
             // foreach ($timeFormatList as $timeFormat) {
            $now = $key  . '   | ex: ' .$date->now($localeObj)->toString($timeFormat); 
            $time_format->addMultiOption($timeFormat , $now);
        }

        // $time_format->addMultiOption('HH:mm:ss', 'HH:mm:ss | ex: 15:20:55 ');
        // $time_format->addMultiOption('HH:mm', 'HH:mm | ex: 15:20');
        
        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));

        $this->addElements(array($country_id, $time_zone,$date_format,$time_format, $button));
        $this->setMethod('post');
        if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $dateTime['id']), 'settingsEditDateTime'));
        } else {
            $this->setAction($router->assemble(array(), 'settingsAddDateTime'));
        }
    }

}

