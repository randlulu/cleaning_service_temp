<?php

class Settings_Form_RateTag extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('RatingTag');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $ratingTag = (isset($options['ratingTag']) ? $options['ratingTag'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();
        $request = Zend_Controller_Front::getInstance()->getRequest();


        $name = new Zend_Form_Element_Text('name');
        $name->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($ratingTag['tag_name']) ? $ratingTag['tag_name'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the tag name'));

        $question = new Zend_Form_Element_Text('question');
        $question->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($ratingTag['tag_question']) ? $ratingTag['tag_question'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the tag question'));
       /* 
       $image = new Zend_Form_Element_File('file_uploader');
       $image->setDestination(get_config('temp_uploads'))
          // ->setLabel('Upload an image:')
           ->addValidator('Count', false, 1)  // ensure only 1 file
           ->addValidator('Size', false, get_config('upload_max_filesize')) //10240000 limit to 10 meg
            ->addValidator('Extension', false, 'jpg,jpeg,png,gif'); // only JPEG, PNG, and GIFs
               // ->setDecorators(array('File', 'Errors', array('Description', array('tag' => 'p', 'class' => 'hint'))));
                //->setMaxFileSize(get_config('upload_max_filesize')) // limits the filesize on the client side
             //   ->setDescription('Click Browse and click on the  file you would like to upload'); 
*/
        
        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));
        
        
        $this->setMethod('post');
        $this->setMethod(Zend_Form::METHOD_POST);
        $this->setEnctype(Zend_Form::ENCTYPE_MULTIPART);
        
        if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $ratingTag['rating_tag_id']), 'rateTagEdit'));
        } else {
            //$image->setRequired(true);
            $this->setAction($router->assemble(array(), 'rateTagAdd'));
        }
        $this->addElements(array($name, $question,  $button));//$image,
    }

}
