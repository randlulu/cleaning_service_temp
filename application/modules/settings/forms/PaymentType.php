<?php

class Settings_Form_PaymentType extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('PaymentType');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $paymentType = (isset($options['paymentType']) ? $options['paymentType'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();
        $request = Zend_Controller_Front::getInstance()->getRequest();

        $paymentTypeVal = (!empty($paymentType['payment_type']) ? $paymentType['payment_type'] : '');
        $payment_type = new Zend_Form_Element_Text('payment_type');
        $payment_type->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue($paymentTypeVal);
        $value = $request->getParam('payment_type');
        if ($paymentTypeVal != $value) {
            $modelPaymentType = new Model_PaymentType();
            $notValid = $modelPaymentType->getByPaymentTypeAndCompany($value);
            if ($notValid) {
               $payment_type->addError("A record matching '{$value}' was found");
            }
        }

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));

        $this->addElements(array($payment_type, $button));

        $this->setMethod('post');
        if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $paymentType['id']), 'settingsPaymentTypeEdit'));
        } else {
            $this->setAction($router->assemble(array(), 'settingsPaymentTypeAdd'));
        }
    }

}

