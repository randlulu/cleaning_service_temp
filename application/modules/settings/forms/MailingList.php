<?php

class Settings_Form_MailingList extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);
		
		
		$this->setName('MailingList');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $mailingList = (isset($options['mailingList']) ? $options['mailingList'] : '');
        $router = Zend_Controller_Front::getInstance()->getRouter();
		$country = (!empty($mailingList['country_id']) ? $mailingList['country_id'] :'');
		$name = new Zend_Form_Element_Text('name');
        $name->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($mailingList['name']) ? $mailingList['name'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the mailing list name'));
				
		$modelCountries = new Model_Countries();
        $allCountries = $modelCountries->getAllCountries();
        $country_id = new Zend_Form_Element_Select('country_id');
        $country_id->setBelongsTo('fltr');
        $country_id->setDecorators(array('ViewHelper'));
        $country_id->setAttribs(array("id" => "country", "class" => "form-control", "onChange" => "getStateByCountryId();"));
        $country_id->setValue((!empty($mailingList['country_id']) ? $mailingList['country_id'] : ''));
        $country_id->addMultiOption('', 'Select One');
        $country_id->addMultiOptions($allCountries);	

       
        $modelCities = new Model_Cities();
        $allState = $modelCities->getStateByCountryId(($country), TRUE);
		$state = new Zend_Form_Element_Select('state');
        $state->setBelongsTo('fltr');
        $state->setDecorators(array('ViewHelper'));
        $state->setAttribs(array("id" => "fltr-state", "class" => "form-control", "onChange" => "getCitiesByCountryId();"));
        $state->setValue((!empty($mailingList['state']) ? $mailingList['state'] : ''));
        $state->addMultiOption('', 'Select One');
        $state->addMultiOptions($allState);	   

		$allCities = $modelCities->getCitiesByCountryIdAndState($country, (!empty($mailingList['state']) ? $mailingList['state'] : 'VIC'), TRUE);
		$cities = new Zend_Form_Element_Select('city_id');
        $cities->setBelongsTo('fltr');
        $cities->setDecorators(array('ViewHelper'));
        $cities->setAttribs(array("class" => "form-control"));
        $cities->setValue((!empty($mailingList['city_id']) ? $mailingList['city_id'] : ''));
        $cities->addMultiOption('', 'Select One');
        
        $cities->addMultiOptions($allCities);
		
		
		$suburb = new Zend_Form_Element_Text('suburb');
        $suburb->setDecorators(array('ViewHelper'))
                ->setDecorators(array('ViewHelper'))
                ->setBelongsTo('fltr')
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($mailingList['suburb']) ? $mailingList['suburb'] : ''));
		

        $modelCustomerType = new Model_CustomerType();
        $customer_types = $modelCustomerType->getCustomerTypeAsArray();
		$customer_type_id = new Zend_Form_Element_Select('customer_type_id');
        $customer_type_id->setBelongsTo('fltr');
        $customer_type_id->setDecorators(array('ViewHelper'));
        $customer_type_id->setAttrib("class", "form-control");
        $customer_type_id->setValue((!empty($mailingList['customer_type_id']) ? $mailingList['customer_type_id'] : ''));
        $customer_type_id->addMultiOption('', 'Select One');
        $customer_type_id->addMultiOptions($customer_types);


        $modelBookingStatus = new Model_BookingStatus();
        $allStatus = $modelBookingStatus->getAllStatusAsArray();
		$all = new Zend_Form_Element_Select('status');
        $all->setBelongsTo('fltr');
        $all->setDecorators(array('ViewHelper'));
        $all->setAttrib("class", "form-control");
        $all->setValue((!empty($mailingList['status_id']) ? $mailingList['status_id'] : ''));
        $all->addMultiOption('', 'Select One');
        $all->addMultiOptions($allStatus);		
		
		$company_id = CheckAuth::getCompanySession();
        $modelLabel = new Model_Label();
        $allLabel = $modelLabel->getAllLabelAsArray(array('company_id'=>$company_id));
		$label = new Zend_Form_Element_Select('label_id');
        $label->setBelongsTo('fltr');
        $label->setDecorators(array('ViewHelper'));
        $label->setAttrib("class", "form-control");
        $label->setValue((!empty($mailingList['label_id']) ? $mailingList['label_id'] : ''));
        $label->addMultiOption('', 'Select One');
        $label->addMultiOptions($allLabel);
		
		$modelServices = new Model_Services();
        $allService = $modelServices->getAllService();
		$service = new Zend_Form_Element_Select('service_id');
        $service->setBelongsTo('fltr');
        $service->setDecorators(array('ViewHelper'));
        $service->setAttrib("class", "form-control");
        $service->setValue((!empty($mailingList['service_id']) ? $mailingList['service_id'] : ''));
        $service->addMultiOption('', 'Select One');
        $service->addMultiOptions($allService);
		
		
		$enquiry_date = new Zend_Form_Element_Text('enquiry_date');
        $enquiry_date->setDecorators(array('ViewHelper'))
                ->setDecorators(array('ViewHelper'))
                ->setBelongsTo('fltr')
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($mailingList['enquiry_date']) ? date('d-m-Y H:i',$mailingList['enquiry_date']) : ''));
				
		$booking_date = new Zend_Form_Element_Text('booking_date');
        $booking_date->setDecorators(array('ViewHelper'))
                ->setDecorators(array('ViewHelper'))
                ->setBelongsTo('fltr')
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($mailingList['booking_date']) ? date('d-m-Y H:i',$mailingList['booking_date']) : ''));
				
	    $estimate_date = new Zend_Form_Element_Text('estimate_date');
        $estimate_date->setDecorators(array('ViewHelper'))
                ->setDecorators(array('ViewHelper'))
                ->setBelongsTo('fltr')
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($mailingList['estimate_date']) ? date('d-m-Y H:i',$mailingList['estimate_date']) : ''));
				
				
	    $deferred_date = new Zend_Form_Element_Text('deferred_date');
        $deferred_date->setDecorators(array('ViewHelper'))
                ->setDecorators(array('ViewHelper'))
                ->setBelongsTo('fltr')
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($mailingList['deferred_date']) ? date('d-m-Y H:i',$mailingList['deferred_date']) : ''));
		
		
		
		$button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-default btn-primary'));

        $this->addElements(array($name, $deferred_date, $estimate_date, $button, $booking_date, $enquiry_date , $service , $label , $all , $customer_type_id , $suburb , $cities , $state , $country_id));
        $this->setMethod('post');
        if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $mailingList['mailing_list_id']), 'settingsMailingListEdit'));
        } else {
            $this->setAction($router->assemble(array(), 'settingsMailingListAdd'));
        }
		
	}
	
}

?>	