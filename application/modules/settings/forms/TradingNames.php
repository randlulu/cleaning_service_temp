<?php

class Settings_Form_TradingNames extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('Trading');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $updatedTrading_names = (isset($options['trading_names']) ? $options['trading_names'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();

        $companyId = new Zend_Form_Element_Select('company_id');
        $companyId->setDecorators(array('ViewHelper'))
            ->addDecorator('Errors', array('class' => 'errors'))
            ->setRequired()
            ->setAttribs(array('class' => 'form-control'))
            ->setValue((!empty($updatedTrading_names['company_id']) ? $updatedTrading_names['company_id'] : ''))
            ->setErrorMessages(array('Required' => 'Please select the company'));
        $table = new Model_Companies();
        foreach ($table->getCompaniesAsArray() as $c) {
            $companyId->addMultiOption($c['id'], $c['name']);
        }

        $trading_name = new Zend_Form_Element_Text('trading_name');
        $trading_name->setDecorators(array('ViewHelper'))
            ->addDecorator('Errors', array('class' => 'errors'))
            ->setRequired()
            ->setAttribs(array('class' => 'form-control'))
            ->setValue((!empty($updatedTrading_names['trading_name']) ? $updatedTrading_names['trading_name'] : ''))
            ->setErrorMessages(array('Required' => 'Please enter Trading Name'));

        $website_url = new Zend_Form_Element_Text('website_url');
        $website_url->setDecorators(array('ViewHelper'))
            ->addDecorator('Errors', array('class' => 'errors'))
            ->setRequired()
            ->setAttribs(array('class' => 'form-control'))
            ->setValue((!empty($updatedTrading_names['website_url']) ? $updatedTrading_names['website_url'] : ''))
            ->setErrorMessages(array('Required' => 'Please enter Website URL'));


        $phone = new Zend_Form_Element_Text('phone');
        $phone->setDecorators(array('ViewHelper'))
            ->addDecorator('Errors', array('class' => 'errors'))
            ->setRequired()
            ->setAttribs(array('class' => 'text_field form-control'))
            ->setValue((!empty($updatedTrading_names['phone']) ? $updatedTrading_names['phone'] : ''))
            ->setErrorMessages(array('Required' => 'Please enter Trading Name Phone'));

        $notEmpty = new Zend_Validate_NotEmpty();
        $notEmpty->setMessage('Please enter Trading Name Email');

        $email_validate = new Zend_Validate_EmailAddress();
        $email_validate->setMessage("Invalid email address");


        $email = new Zend_Form_Element_Text('email');
        $email->setDecorators(array('ViewHelper'))
            ->addDecorator('Errors', array('class' => 'errors'))
            ->setRequired()
            ->setAttribs(array('class' => 'text_field form-control'))
            ->setValue((!empty($updatedTrading_names['email']) ? $updatedTrading_names['email'] : ''))
            ->addValidator($notEmpty, true)
            ->addValidator($email_validate ,TRUE);

        $isDefault = new Zend_Form_Element_Checkbox('isDefault');
        $isDefault->setDecorators(array('ViewHelper'))
            ->addDecorator('Errors', array('class' => 'errors'))
            ->setAttribs(array('class' => 'text_field' , 'checked'=>(!empty($updatedTrading_names['is_default']) ? 'checked' : '')));


        $colorVal = (!empty($updatedTrading_names['color']) ? $updatedTrading_names['color'] : 0);
        $color = new Zend_Form_Element_Hidden('color');
        $color->setDecorators(array('ViewHelper'))
            ->addDecorator('Errors', array('class' => 'errors'))
            ->setAttribs(array('id' => 'ColorInput'));
        $this->setAttrib('colorVal', $colorVal);


        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));


        $this->addElements(array( $phone , $email ,$companyId , $trading_name , $website_url , $button , $isDefault, $color  ));
        $this->setMethod('post');
        $this->setMethod(Zend_Form::METHOD_POST);
        $this->setEnctype(Zend_Form::ENCTYPE_MULTIPART);


        if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $updatedTrading_names['trading_name_id']), 'settingsCompaniesTradingNamesEdit'));
        } else {
            $this->setAction($router->assemble(array(), 'settingsCompaniesTradingNamesAdd'));
        }
    }

}

