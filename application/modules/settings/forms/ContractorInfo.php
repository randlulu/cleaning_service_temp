<?php

class Settings_Form_ContractorInfo extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('ContractorInfo');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $contractorInfo = (isset($options['contractorInfo']) ? $options['contractorInfo'] : '');
        $contractor_id = (isset($options['contractor_id']) ? $options['contractor_id'] : '');
		$type = (isset($options['type']) ? $options['type'] : '');
		
		$user = (isset($options['user']) ? $options['user'] : '');
        $countryId = (isset($options['country_id']) ? $options['country_id'] : 0);
        $optionState = (isset($options['state']) ? $options['state'] : '');
        $companyId = (isset($options['company_id']) ? $options['company_id'] : 0);

        $router = Zend_Controller_Front::getInstance()->getRouter();
		
		
		
				$user_name = (!empty($user['username']) ? $user['username'] : '');

        $username = new Zend_Form_Element_Text('username');
        $username->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'form-control'))
                ->setValue($user_name);
				  $username->setRequired();
//        if ($user_name != $request->getParam('username')) {
//            $username->addValidator(new Zend_Validate_Db_NoRecordExists('user', 'username'));
//        }

        $first_name = new Zend_Form_Element_Text('first_name');
        $first_name->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($user['first_name']) ? $user['first_name'] : ''));
				 $first_name->setRequired();
		
		$last_name = new Zend_Form_Element_Text('last_name');
        $last_name->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($user['last_name']) ? $user['last_name'] : ''));
				 $last_name->setRequired();


        if ('update' != $mode) {
            $password = new Zend_Form_Element_Password('password');
            $password->setDecorators(array('ViewHelper'))
                    ->addDecorator('Errors', array('class' => 'errors'))
                    ->setRequired()
                    ->setAttribs(array('class' => 'form-control'))
                    ->setValue((!empty($user['password']) ? $user['password'] : ''))
                    ->addValidator(new Zend_Validate_StringLength(array('min' => 6, 'max' => 20)));
        }


        $display_name = new Zend_Form_Element_Text('display_name');
        $display_name->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($user['display_name']) ? $user['display_name'] : ''));
				  $display_name->setRequired();
        $roleId = new Zend_Form_Element_Select('role_id');
        $roleId->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'form-control', 'onchange' => 'getFeildByRoleId()'))
                ->setValue((!empty($user['role_id']) ? $user['role_id'] : ''));
				  $roleId->setRequired();	
        $table = new Model_AuthRole();
        $roleId->addMultiOption('', 'Select One');
        foreach ($table->getRoleAsArray() as $r) {
            $roleId->addMultiOption($r['id'], $r['name']);
        }

        $company_id = new Zend_Form_Element_Select('company_id');
        $company_id->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($companyId) ? $companyId : ''));
        $table = new Model_Companies();
        $company_id->setRequired();
        $company_id->addMultiOption('', 'Select One');
        foreach ($table->getCompaniesAsArray() as $r) {
            $company_id->addMultiOption($r['id'], $r['name']);
        }
		

        $businessName = new Zend_Form_Element_Text('business_name');
        $businessName->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($contractorInfo['business_name']) ? $contractorInfo['business_name'] : ''));
        //->setErrorMessages(array('Required' => 'Please enter the Business Name'));


        $abn = new Zend_Form_Element_Text('abn');
        $abn->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($contractorInfo['abn']) ? $contractorInfo['abn'] : ''));
        //->setErrorMessages(array('Required' => 'Please enter the ABN'));

       /* $tfn = new Zend_Form_Element_Text('tfn');
        $tfn->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($contractorInfo['tfn']) ? $contractorInfo['tfn'] : ''));*/
        
        //->setErrorMessages(array('Required' => 'Please enter the TFN'));
        
        /******Start***/
        $loggedUser = CheckAuth::getLoggedUser(); 
        $modelAuthRole = new Model_AuthRole();
        $adminRoleId = $modelAuthRole->getRoleIdByName('account_admin');
        $superAdminRoleId = $modelAuthRole->getRoleIdByName('super_admin');
         if($loggedUser['user_id'] == $contractor_id){
            $tfn = new Zend_Form_Element_Text('tfn');
            $tfn->setDecorators(array('ViewHelper'))
                    ->addDecorator('Errors', array('class' => 'errors'))
                    //->setRequired()
                    ->setAttribs(array('class' => 'form-control'))
                    ->setValue((!empty($contractorInfo['tfn']) ? $contractorInfo['tfn'] : ''));
            //->setErrorMessages(array('Required' => 'Please enter the TFN'));
        }elseif(($loggedUser['role_id'] == $adminRoleId || $loggedUser['role_id'] == $superAdminRoleId) && CheckAuth::checkCredential(array('settingsUserInfoViewTFN'))){
                $tfn = new Zend_Form_Element_Text('tfn');
                $tfn->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($contractorInfo['tfn']) ? $contractorInfo['tfn'] : ''));

        }else{
                $tfn = new Zend_Form_Element_Password('tfn');
                $tfn->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'form-control','readonly' => 'true'));
                $tfn->renderPassword = true;
                $tfn->setValue((!empty($contractorInfo['tfn']) ? $contractorInfo['tfn'] : ''));
        }
/****END*****/


        $acn = new Zend_Form_Element_Text('acn');
        $acn->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($contractorInfo['acn']) ? $contractorInfo['acn'] : ''));
        // ->setErrorMessages(array('Required' => 'Please enter the ACN'));

        $gst = new Zend_Form_Element_Checkbox('gst');
        $gst->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'checkbox_field'))
                ->setValue((!empty($contractorInfo['gst']) ? $contractorInfo['gst'] : ''));
        //->setErrorMessages(array('Required' => 'Please enter the GST'));
		$db_format = 0;
		$dateTimeObj= get_settings_date_format();
		if($dateTimeObj){
			$db_format = 1;
		}
		$dateFormated = '';
		if(!empty($contractorInfo['gst_date_registered']))
			$dateFormated = getNewDateFormat($contractorInfo['gst_date_registered']); 
        $gstDateRegistered = new Zend_Form_Element_Text('gst_date_registered');
        $gstDateRegistered->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setAttrib('readonly', TRUE)
				->setValue((!empty($contractorInfo['gst_date_registered'] ) && ($contractorInfo['gst_date_registered'] > 0) ? ($dateFormated ? $dateFormated : date('d-m-Y', $contractorInfo['gst_date_registered'])) : ($db_format?getNewDateFormat(strtotime(date('d-m-Y'))):date('d-m-Y'))));
                //->setValue((!empty($contractorInfo['gst_date_registered'] ) && ($contractorInfo['gst_date_registered'] > 0)  ? date('d-m-Y', $contractorInfo['gst_date_registered']) : date('d-m-Y')));
        //->setErrorMessages(array('Required' => 'Please enter the  GST Date Registered'));



        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));

 
		  $this->addElements(array($businessName,$first_name, $last_name, $abn, $tfn, $acn, $gst, $gstDateRegistered, $button ,$username , $company_id , $display_name , $roleId));
		
		
        $this->setMethod('post');

        if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $contractorInfo['contractor_info_id'], 'contractor_id' => $contractor_id), 'settingsContractorInfoEdit'));
        } else {
            $this->setAction($router->assemble(array('contractor_id' => $contractor_id), 'settingsContractorInfoAdd'));
        }
    }

}


