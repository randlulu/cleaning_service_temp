<?php

class Settings_Form_ContractorServiceAvailability extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('ContractorServiceAvailability');
        $contractor_service_id = (isset($options['contractor_service_id']) ? $options['contractor_service_id'] : '');
        $countryId = (isset($options['country_id']) ? $options['country_id'] : 0);
        $old_cities = (isset($options['old_cities']) ? $options['old_cities'] : '');
		$selected_values = array();
		foreach($old_cities as $key=>$old_city){
		 $selected_values[$key] = $old_city['city_id'];
		}
		
        $router = Zend_Controller_Front::getInstance()->getRouter();

        //
        //get country & city for ajax
        //
        $defaultCountryId=CheckAuth::getCountryId();
        $country_id = new Zend_Form_Element_Select('country_id');
        $country_id->setDecorators(array('ViewHelper'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control', 'onchange' => "getCities('{$router->assemble(array(), 'dropDownCity')}','#country_id','#city_block','#city')"))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setErrorMessages(array('Required' => 'Please select Country'))
                ->setValue($defaultCountryId);
        $table = new Model_Countries();
        $country_id->addMultiOption('', 'Select One');
        foreach ($table->getCountriesAsArray() as $c) {
            $country_id->addMultiOption($c['id'], $c['name']);
        }

        $city_id = new Zend_Form_Element_MultiCheckbox('city_id');
        $city_id->setDecorators(array('ViewHelper'))
                ->setRequired()
                ->setAttribs(array('class' => 'checkbox_field'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setErrorMessages(array('Required' => 'Please Select at Least One City'));

        $city_obj = new Model_Cities();
        $city_id->addMultiOption('all', 'Select All');
        foreach ($city_obj->getByCountryId((!empty($countryId) ? $countryId : $defaultCountryId)) as $c) {
            $city_id->addMultiOption($c['city_id'], $c['city_name']);
        }
		
		$city_id->setValue($selected_values);

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));

        $this->addElements(array($country_id, $city_id, $button));
        $this->setMethod('post');
        $this->setAction($router->assemble(array('contractor_service_id' => $contractor_service_id), 'settingsContractorServiceAvailabilityAdd'));
    }

}

