<?php

class Settings_Form_UnsubscribeReason extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('UnsubscribeReason');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $UnsubscribeReason = (isset($options['UnsubscribeReason']) ? $options['UnsubscribeReason'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();
        $request = Zend_Controller_Front::getInstance()->getRequest();



        $reasonVal = (!empty($UnsubscribeReason['reason_text']) ? $UnsubscribeReason['reason_text'] : '');
        $reason_text = new Zend_Form_Element_Text('reason_text');
        $reason_text->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field form-control'))
                ->setValue($reasonVal);


        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));

        $this->addElements(array($reason_text, $button));
        $this->setMethod('post');
        if ('update' == $mode) {

            $this->setAction($router->assemble(array('id' => $UnsubscribeReason['unsubscribe_reason_id']), 'settingsUnsubscribeReasonsEdit'));
        } else {
            $this->setAction($router->assemble(array(), 'settingsUnsubscribeReasonsAdd'));
        }
    }

}

