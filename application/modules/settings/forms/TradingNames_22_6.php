<?php

class Settings_Form_TradingNames extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('Trading');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $updatedTrading_names = (isset($options['trading_names']) ? $options['trading_names'] : '');
			//var_dump($updatedTrading_names);
			//exit;
        $router = Zend_Controller_Front::getInstance()->getRouter();

        $companyId = new Zend_Form_Element_Select('company_id');
        $companyId->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($updatedTrading_names['company_id']) ? $updatedTrading_names['company_id'] : ''))
                ->setErrorMessages(array('Required' => 'Please select the company'));
        $table = new Model_Companies();
        foreach ($table->getCompaniesAsArray() as $c) {
            $companyId->addMultiOption($c['id'], $c['name']);
        }

        $trading_name = new Zend_Form_Element_Text('trading_name');
        $trading_name->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($updatedTrading_names['trading_name']) ? $updatedTrading_names['trading_name'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter Trading Name'));
//
        $website_url = new Zend_Form_Element_Text('website_url');
        $website_url->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($updatedTrading_names['website_url']) ? $updatedTrading_names['website_url'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter Website URL'));
//
//
	
        $isDefault = new Zend_Form_Element_Checkbox('isDefault');
        $isDefault->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'text_field' , 'checked'=>(!empty($updatedTrading_names['is_default']) ? 'checked' : '')));
                //->setAttribs('checked',(!empty($updatedTrading_names['isDefault']) ? 'checked' : ''));
                


        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));

        $this->addElements(array( $companyId , $trading_name , $website_url , $button , $isDefault));
        $this->setMethod('post');
        if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $updatedTrading_names['trading_name_id']), 'settingsCompaniesTradingNamesEdit'));
        } else {
            $this->setAction($router->assemble(array(), 'settingsCompaniesTradingNamesAdd'));
        }
    }

}

