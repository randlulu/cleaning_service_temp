<?php

class Settings_Form_DeclarationOfOtherApparatus extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('DeclarationOfOtherApparatus');
        $contractor_info_id = (isset($options['contractor_info_id']) ? $options['contractor_info_id'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();

        $other_apparatus = new Zend_Form_Element_Text('other_apparatus');
        $other_apparatus->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setErrorMessages(array('Required' => 'Please enter the Contractor Owner Name'));


        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));

        $this->addElements(array($other_apparatus, $button));
        $this->setMethod('post');
        $this->setAction($router->assemble(array('contractor_info_id' => $contractor_info_id), 'settingsDeclarationOfOtherApparatusAdd'));
    }

}

