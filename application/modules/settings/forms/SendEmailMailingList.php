<?php

class Settings_Form_SendEmailMailingList extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);
		
		
		$this->setName('SendEmailMailingList');
        $router = Zend_Controller_Front::getInstance()->getRouter();
		$marketingEmailLog = (isset($options['marketingEmailLog']) ? $options['marketingEmailLog'] : '');
		
		
		$mailingListObj = new Model_MailingList();
        $allMailinglist = $mailingListObj->getAllMailingListAsArray();
        $mailingList = new Zend_Form_Element_Select('mailing_list_id');
        $mailingList->setDecorators(array('ViewHelper'));
        $mailingList->setRequired();
		$mailingList->setValue((!empty($marketingEmailLog['mailing_list_id']) ? $marketingEmailLog['mailing_list_id'] : ''));
		$mailingList->addDecorator('Errors', array('class' => 'errors'));
		$mailingList->setErrorMessages(array('Required' => 'Please choose the mailing list '));
        $mailingList->setAttribs(array("class" => "form-control"));
        $mailingList->addMultiOption('', 'Select One');
        $mailingList->addMultiOptions($allMailinglist);	
				
		$emailTemplateObj = new Model_EmailTemplate();
        $allemailTemplate = $emailTemplateObj->getEmailTemplateAsArray();
        $emailTemplate = new Zend_Form_Element_Select('emailTemplate');
        $emailTemplate->setDecorators(array('ViewHelper'));
		$emailTemplate->addDecorator('Errors', array('class' => 'errors'));
        $emailTemplate->setAttribs(array("class" => "form-control" , "onChange" => "getEmailTemplateById();", "id" => "emailTemplate"));
		$emailTemplate->setRequired();
		$emailTemplate->setValue((!empty($marketingEmailLog['email_template_id']) ? $marketingEmailLog['email_template_id'] : ''));
		$emailTemplate->setErrorMessages(array('Required' => 'Please choose the email template '));
        $emailTemplate->addMultiOption('', 'Select One');
        $emailTemplate->addMultiOptions($allemailTemplate);
	    
		/*
		$cannedResponsesObj = new Model_CannedResponses();
        $allcannedResponses = $cannedResponsesObj->getAll();
		$data = array();
            foreach ($allcannedResponses as $cannedResponse) {
                $data[$cannedResponse['id']] = $cannedResponse['name'];
            }
        $emailTemplate = new Zend_Form_Element_Select('emailTemplate');
        $emailTemplate->setDecorators(array('ViewHelper'));
		$emailTemplate->addDecorator('Errors', array('class' => 'errors'));
        $emailTemplate->setAttribs(array("class" => "form-control" , "onChange" => "getEmailTemplateById();", "id" => "email_template_id"));
		$emailTemplate->setRequired();
		$emailTemplate->setErrorMessages(array('Required' => 'Please choose the email template '));
        $emailTemplate->addMultiOption('', 'Select One');
        $emailTemplate->addMultiOptions($data);
        */
       
		$subject = new Zend_Form_Element_Text('subject');
        $subject->setDecorators(array('ViewHelper'))
                ->setDecorators(array('ViewHelper'))
                ->setAttribs(array('class' => 'form-control'))
		        ->setErrorMessages(array('Required' => 'Please choose the email template '))
                ->setValue((!empty($marketingEmailLog['subject']) ? $marketingEmailLog['subject'] : ''));
		
		
		$body = new Zend_Form_Element_Textarea('body');
        $body->setDecorators(array('ViewHelper'))
                ->setDecorators(array('ViewHelper'))
                ->setAttribs(array('class' => 'form-control','rows'=>'15','id'=>'body'))
                ->setValue((!empty($marketingEmailLog['body']) ? $marketingEmailLog['body'] : ''));


        $this->addElements(array($mailingList, $subject, $emailTemplate , $body));
        $this->setMethod('post');
        $this->setAction($router->assemble(array(), 'settingsMailingListSendEmail'));
        
		
	}
	
}

?>	