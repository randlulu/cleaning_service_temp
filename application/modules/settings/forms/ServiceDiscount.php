<?php

class Settings_Form_ServiceDiscount extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('ServiceDiscount');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $service_discount = (isset($options['service_discount']) ? $options['service_discount'] : '');
        $service_id = (isset($options['service_id']) ? $options['service_id'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();

        $min_size_range = new Zend_Form_Element_Text('min_size_range');
        $min_size_range->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($service_discount['min_size_range']) ? $service_discount['min_size_range'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter minimum size range'));


        $max_size_range = new Zend_Form_Element_Text('max_size_range');
        $max_size_range->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($service_discount['max_size_range']) ? $service_discount['max_size_range'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter maximum size range'));
				
		$min_discount_range = new Zend_Form_Element_Text('min_discount_range');
        $min_discount_range->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($service_discount['min_discount_range']) ? $service_discount['min_discount_range'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter minimum discount range'));


        $max_discount_range = new Zend_Form_Element_Text('max_discount_range');
        $max_discount_range->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($service_discount['max_discount_range']) ? $service_discount['max_discount_range'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter maximum discount range'));
				
		
		
		
		
		
        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));

        $this->addElements(array($min_size_range, $max_size_range, $max_discount_range,$min_discount_range, $button));
        $this->setMethod('post');
        if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $service_discount['id'],'service_id' => $service_id), 'settingsDiscountRangeEdit'));
        } else {
            $this->setAction($router->assemble(array('service_id' => $service_id), 'settingsDiscountRangeAdd'));
        }
    }

}

