<?php

class Settings_Form_PaymentInfo extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('ContractorInfo');
        $contractorInfo = (isset($options['contractorInfo']) ? $options['contractorInfo'] : '');
        $contractor_id = (isset($options['contractor_id']) ? $options['contractor_id'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();
		
	
       

        $bond_to_be_withheld = new Zend_Form_Element_Text('bond_to_be_withheld');
        $bond_to_be_withheld->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($contractorInfo['bond_to_be_withheld']) ? $contractorInfo['bond_to_be_withheld'] : ''));
        //->setErrorMessages(array('Required' => 'Please enter the Bond to be Withheld'));

        $commission = new Zend_Form_Element_Text('commission');
        $commission->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($contractorInfo['commission']) ? $contractorInfo['commission'] : ''));
        //->setErrorMessages(array('Required' => 'Please enter the Commission'));

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));


		  $this->addElements(array($bond_to_be_withheld, $commission, $button ));
				
        $this->setMethod('post');
		$this->setAction($router->assemble(array('contractor_id' => $contractor_id), 'settingsContractorInfoEditPayment'));
        
    }

}

