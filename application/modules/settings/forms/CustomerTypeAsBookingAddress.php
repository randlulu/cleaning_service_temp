<?php

class Settings_Form_CustomerTypeAsBookingAddress extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('CustomerTypeAsBookingAddress');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $customerType = (isset($options['customerType']) ? $options['customerType'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();
        $request = Zend_Controller_Front::getInstance()->getRequest();


        $asBookingAddressVal = (!empty($customerType['as_booking_address']) ? $customerType['as_booking_address'] : '');
        $asBookingAddress = new Zend_Form_Element_Checkbox('as_booking_address');
        $asBookingAddress->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'checkbox_field'))
                ->setValue($asBookingAddressVal);

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));

        $this->addElements(array($asBookingAddress, $button));
        $this->setMethod('post');
        $this->setAction($router->assemble(array('id' => $customerType['customer_type_id']), 'settingsCustomerTypeAsBookingAddress'));
   
    }

}


