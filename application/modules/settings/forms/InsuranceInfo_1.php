<?php

class Settings_Form_InsuranceInfo extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('ContractorInfo');
        $contractorInfo = (isset($options['contractorInfo']) ? $options['contractorInfo'] : '');
        $contractor_id = (isset($options['contractor_id']) ? $options['contractor_id'] : '');
		
		
        $router = Zend_Controller_Front::getInstance()->getRouter();
		
	
       $insurance_policy_number = new Zend_Form_Element_Text('insurance_policy_number');
        $insurance_policy_number->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                // ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($contractorInfo['insurance_policy_number']) ? $contractorInfo['insurance_policy_number'] : ''));
        //  ->setErrorMessages(array('Required' => 'Please enter the Insurance Policy Number'));

        $insurance_policy_start = new Zend_Form_Element_Text('insurance_policy_start');
        $insurance_policy_start->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                // ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($contractorInfo['insurance_policy_start']) ? (!empty($contractorInfo['insurance_policy_start']) ? (strlen($contractorInfo['insurance_policy_start']) < 10)? $contractorInfo['insurance_policy_start']:date('d-m-Y',$contractorInfo['insurance_policy_start']): '') : ''));
        //  ->setErrorMessages(array('Required' => 'Please enter the Insurance Policy Start'));

        $insurance_policy_expiry = new Zend_Form_Element_Text('insurance_policy_expiry');
        $insurance_policy_expiry->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                // ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($contractorInfo['insurance_policy_expiry']) ? (!empty($contractorInfo['insurance_policy_expiry']) ? (strlen($contractorInfo['insurance_policy_expiry']) < 10)? $contractorInfo['insurance_policy_expiry']:date('d-m-Y',$contractorInfo['insurance_policy_expiry']): '') : ''));
        //->setErrorMessages(array('Required' => 'Please enter the Insurance Policy Expiry'));


        $insurance_listed_services_covered = new Zend_Form_Element_Text('insurance_listed_services_covered');
        $insurance_listed_services_covered->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($contractorInfo['insurance_listed_services_covered']) ? $contractorInfo['insurance_listed_services_covered'] : ''));
				
        //->setErrorMessages(array('Required' => 'Please enter the Insurance listed services covered'));

		
		$insurance_type = new Zend_Form_Element_Select('insurance_type');
        $insurance_type->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => ' form-control'))
                ->setValue((!empty($contractorInfo['insurance_type']) ? $contractorInfo['insurance_type'] : ''));
		
        $insurnce_type_values = array(''=>'Select One','Workers Compensation'=>'Workers Compensation','Public Liability'=>'Public Liability','Vehicle Insurance'=>'Vehicle Insurance');		

        $insurance_type->addMultiOptions($insurnce_type_values);
		
        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));


		  $this->addElements(array($insurance_listed_services_covered, $insurance_policy_expiry, $insurance_policy_number, $insurance_policy_start, $insurance_type , $button ));
				
        $this->setMethod('post');
		$this->setAction($router->assemble(array('contractor_id' => $contractor_id), 'settingsContractorInfoEditInsurance'));
        
    }

}

