<?php

class Settings_Form_BookingStatus extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('BookingStatus');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $bookingStatus = (isset($options['bookingStatus']) ? $options['bookingStatus'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();
        $request = Zend_Controller_Front::getInstance()->getRequest();


//        $companyId = new Zend_Form_Element_Select('company_id');
//        $companyId->setDecorators(array('ViewHelper'))
//                ->addDecorator('Errors', array('class' => 'errors'))
//                ->setRequired()
//                ->setAttribs(array('class' => 'select_field'))
//                ->setValue((!empty($bookingStatus['company_id']) ? $bookingStatus['company_id'] : ''))
//                ->setErrorMessages(array('Required' => 'Please select the company'));
//
//        $table = new Model_Companies();
//
//        $companyId->addMultiOption('0', 'All Company');
//        foreach ($table->getCompaniesAsArray() as $c) {
//            $companyId->addMultiOption($c['id'], $c['name']);
//        }


        $nameVal = (!empty($bookingStatus['name']) ? $bookingStatus['name'] : '');
        $name = new Zend_Form_Element_Text('name');
        $name->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field form-control'))
                ->setValue($nameVal);
        $value = $request->getParam('name');
        if ($nameVal != $value) {
            $modelBookingStatus = new Model_BookingStatus();
            
            $notValid = $modelBookingStatus->getByStatusNameAndCompany($value);
            if ($notValid) {
                $name->addError("A record matching '{$value}' was found");
            }
        }


        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));

        $this->addElements(array($name, $button));
        $this->setMethod('post');
        if ('update' == $mode) {

            $this->setAction($router->assemble(array('id' => $bookingStatus['booking_status_id']), 'settingsBookingStatusEdit'));
        } else {
            $this->setAction($router->assemble(array(), 'settingsBookingStatusAdd'));
        }
    }

}

