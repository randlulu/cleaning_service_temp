<?php

class Settings_Form_InquiryRequiredType extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('InquiryRequiredType');
        $required = (isset($options['required']) ? $options['required'] : '');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $router = Zend_Controller_Front::getInstance()->getRouter();
        $request = Zend_Controller_Front::getInstance()->getRequest();

        $requiredTypeValue = (!empty($required['required_type']) ? $required['required_type'] : '');


        $requiredType = new Zend_Form_Element_Text('required_type');
        $requiredType->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setValue((!empty($required['required_type']) ? $required['required_type'] : ''))
                ->setAttribs(array('class' => 'form-control'));

        $value = $request->getParam('required_type');
        if ($requiredTypeValue != $value) {
            $modelInquiryRequiredType = new Model_InquiryRequiredType();
            $notValid = $modelInquiryRequiredType->getByRequiredTypeAndCompany($value);
            if ($notValid) {
                $requiredType->addError("A record matching '{$value}' was found");
            }
        }



        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));

        $this->addElements(array($requiredType, $button));
        $this->setMethod('post');
        if ($mode == 'update') {
            $this->setAction($router->assemble(array('id' => $required['id']), 'settingsInquiryRequiredTypeEdit'));
        } else {
            $this->setAction($router->assemble(array(), 'settingsInquiryRequiredTypeAdd'));
        }
    }

}

