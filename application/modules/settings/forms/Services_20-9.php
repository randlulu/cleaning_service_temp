<?php

class Settings_Form_Services extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('Services');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $service = (isset($options['service']) ? $options['service'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();

        $serviceName = new Zend_Form_Element_Text('service_name');
        $serviceName->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($service['service_name']) ? $service['service_name'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the service name'));


        $minPrice = new Zend_Form_Element_Text('min_price');
        $minPrice->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($service['min_price']) ? $service['min_price'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the minimum price'));

        $priceEquasion = new Zend_Form_Element_Text('price_equasion');
        $priceEquasion->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($service['price_equasion']) ? $service['price_equasion'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the price equasion'));

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));

        $this->addElements(array($serviceName, $priceEquasion, $minPrice, $button));
        $this->setMethod('post');
        if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $service['service_id']), 'settingsServicesEdit'));
        } else {
            $this->setAction($router->assemble(array(), 'settingsServicesAdd'));
        }
    }

}

