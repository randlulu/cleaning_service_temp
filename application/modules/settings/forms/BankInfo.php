<?php

class Settings_Form_BankInfo extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('ContractorInfo');
        $contractorInfo = (isset($options['contractorInfo']) ? $options['contractorInfo'] : '');
        $contractor_id = (isset($options['contractor_id']) ? $options['contractor_id'] : '');
		
		
        $router = Zend_Controller_Front::getInstance()->getRouter();
		
	
       $bank_name = new Zend_Form_Element_Text('bank_name');
        $bank_name->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($contractorInfo['bank_name']) ? $contractorInfo['bank_name'] : ''));

        $account_name = new Zend_Form_Element_Text('account_name');
        $account_name->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($contractorInfo['account_name']) ? $contractorInfo['account_name'] : ''));

        $account_number = new Zend_Form_Element_Text('account_number');
        $account_number->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($contractorInfo['account_number']) ? $contractorInfo['account_number'] : ''));



        $bsb = new Zend_Form_Element_Text('bsb');
        $bsb->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($contractorInfo['bsb']) ? $contractorInfo['bsb'] : ''));
				
      
		
        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));


		  $this->addElements(array($bank_name, $account_name, $account_number, $bsb,$button ));
				
        $this->setMethod('post');
		$this->setAction($router->assemble(array('contractor_id' => $contractor_id), 'settingsContractorInfoEditBankDetails'));
        
    }

}

