<?php

class Settings_Form_SmsTemplate extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('SmsTemplate');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $smsTemplate = (isset($options['smsTemplate']) ? $options['smsTemplate'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();
        $name = new Zend_Form_Element_Text('name');
        $name->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($smsTemplate['name']) ? $smsTemplate['name'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the Sms template name'));
        if ('update' == $mode && (!get_config('under_construction'))) {
            $name->setAttribs(array('readonly' => 'readonly', 'style' => 'background-color: lightgray;'));
        }

		$type = new Zend_Form_Element_Select('type');
		$type->setDecorators(array('ViewHelper'))
							  ->addDecorator('Errors', array('class' => 'errors'))
							  ->setRequired()
							  ->setAttribs(array('class' => 'select_field form-control'))
							  ->setMultiOptions(array('' => 'Select One'))
							  ->setValue((!empty($smsTemplate['type']) ? $smsTemplate['type'] : ' '))
							  ->setErrorMessages(array('Required' => 'Please select Message Type.'));

		$type->addMultiOption('customer', 'customer');
		$type->addMultiOption('contractor', 'contractor');   
		
        if ('update' == $mode && (!get_config('under_construction'))) {
            $placeHolder = new Zend_Form_Element_Hidden('placeholder');
        } else {
            $placeHolder = new Zend_Form_Element_Text('placeholder');
        }
        $placeHolder->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($smsTemplate['placeholder']) ? $smsTemplate['placeholder'] : ''));

       $message = new Zend_Form_Element_Textarea('message');
       $message->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($smsTemplate['message']) ? $smsTemplate['message'] : ' '))
                ->setErrorMessages(array('Required' => 'Please enter the Sms message'));

       $button = new Zend_Form_Element_Submit('button');
       $button->setDecorators(array('ViewHelper'));
       $button->setLabel('Save');
       $button->setAttribs(array('class' => 'btn btn-default btn-primary'));

        $this->addElements(array($name,$message,$type,$placeHolder,$button));
        $this->setMethod('post');
        if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $smsTemplate['id']), 'settingsSmsTemplateEdit'));
        } else {
            $this->setAction($router->assemble(array(), 'settingsSmsTemplateAdd'));
        }
    }

}

