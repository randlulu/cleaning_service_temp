<?php

class Settings_Form_InquiryPropertyType extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('InquiryPropertyType');
        $property = (isset($options['property']) ? $options['property'] : '');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $router = Zend_Controller_Front::getInstance()->getRouter();
        $request = Zend_Controller_Front::getInstance()->getRequest();

        $propertyTypeValue = (!empty($property['property_type']) ? $property['property_type'] : '');

        $propertyType = new Zend_Form_Element_Text('property_type');
        $propertyType->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setValue((!empty($property['property_type']) ? $property['property_type'] : ''))
                ->setAttribs(array('class' => 'text_field'));
        $value = $request->getParam('property_type');
        if ($propertyTypeValue != $value) {
            $modelInquiryPropertyType = new Model_InquiryPropertyType();
            $notValid = $modelInquiryPropertyType->getByPropertyTypeAndCompany($value);
            if ($notValid) {
                $propertyType->addError("A record matching '{$value}' was found");
            }
        }

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'button'));

        $this->addElements(array($propertyType, $button));
        $this->setMethod('post');
        if ($mode == 'update') {
            $this->setAction($router->assemble(array('id' => $property['id']), 'settingsInquiryPropertyTypeEdit'));
        } else {
            $this->setAction($router->assemble(array(), 'settingsInquiryPropertyTypeAdd'));
        }
    }

}

