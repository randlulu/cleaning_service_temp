<?php

class Settings_Form_CallOutFee extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('CallOutFee');
        
        $callOutFeeVal = (isset($options['callOutFee']) ? $options['callOutFee'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();
        $request = Zend_Controller_Front::getInstance()->getRequest();

        $call_out_fee = new Zend_Form_Element_Text('call_out_fee');
        $call_out_fee->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue($callOutFeeVal);
        $value = $request->getParam('call_out_fee');
        if ($callOutFeeVal != $value) {
            $modelCompanies = new Model_Companies();
            $notValid = $modelCompanies->getByCallOutFeeAndCompany($value);
            if ($notValid) {
                $call_out_fee->addError("A record matching '{$value}' was found");
            }
        }

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));


        $this->addElements(array($call_out_fee, $button));
        $this->setMethod('post');

        $this->setAction($router->assemble(array(), 'settingsCallOutFee'));
    }

}

