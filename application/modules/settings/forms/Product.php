<?php

class Settings_Form_Product extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('Product');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $optionsProduct = (isset($options['product']) ? $options['product'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();

        $product = new Zend_Form_Element_Text('product');
        $product->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($optionsProduct['product']) ? $optionsProduct['product'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the Product'));

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));

        $this->addElements(array($product, $button));
        $this->setMethod('post');
        if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $optionsProduct['id']), 'settingsProductEdit'));
        } else {
            $this->setAction($router->assemble(array(), 'settingsProductAdd'));
        }
    }

}

