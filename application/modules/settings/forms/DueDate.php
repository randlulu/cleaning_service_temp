<?php

class Settings_Form_DueDate extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('DueDate');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $dueDate = (isset($options['dueDate']) ? $options['dueDate'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();
        $request = Zend_Controller_Front::getInstance()->getRequest();

        $dueDateVal = (!empty($dueDate['due_date']) ? $dueDate['due_date'] : '');

        $due_date = new Zend_Form_Element_Text('due_date');
        $due_date->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue($dueDateVal);
        $value =$request->getParam('due_date');
        if ($dueDateVal != $value) {
            $modelDueDate = new Model_DueDate();
            $notValid = $modelDueDate->getByDueDateAndCompany($value);
            if ($notValid) {
                $due_date->addError("A record matching '{$value}' was found");
            }
        }

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));

        $isDefault = new Zend_Form_Element_Checkbox('is_default');
        $isDefault->setDecorators(array('ViewHelper'))
                ->setAttribs(array('class' => 'checkbox_field'))
                ->setValue((!empty($dueDate['is_default']) ? $dueDate['is_default'] : ''));

        $this->addElements(array($due_date, $button, $isDefault));
        $this->setMethod('post');
        if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $dueDate['id']), 'settingsDueDateEdit'));
        } else {
            $this->setAction($router->assemble(array(), 'settingsDueDateAdd'));
        }
    }

}

