<?php

class Settings_Form_ServiceAttributeDiscount extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('ServiceAttributePrice');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $attribute_discount = (isset($options['service_attribute_discount']) ? $options['service_attribute_discount'] : '');
		$service_id = (isset($options['service_id']) ? $options['service_id'] : '');
		$attribute_id = (isset($options['attribute_id']) ? $options['attribute_id'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();

        $min_size_range = new Zend_Form_Element_Text('min_size_range');
        $min_size_range->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($attribute_discount['min_size_range']) ? $attribute_discount['min_size_range'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter minimum size range'));


        $max_size_range = new Zend_Form_Element_Text('max_size_range');
        $max_size_range->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($attribute_discount['max_size_range']) ? $attribute_discount['max_size_range'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter maximum size range'));
				
		
		$min_discount_range = new Zend_Form_Element_Text('min_discount_range');
        $min_discount_range->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($attribute_discount['min_discount_range']) ? $attribute_discount['min_discount_range'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter minimum discount range'));


        $max_discount_range = new Zend_Form_Element_Text('max_discount_range');
        $max_discount_range->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($attribute_discount['max_discount_range']) ? $attribute_discount['max_discount_range'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter maximum discount range'));
								
		
        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));

        $this->addElements(array($min_size_range, $max_size_range, $max_discount_range,$min_discount_range, $button));
        $this->setMethod('post');
        if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $attribute_discount['id'],'attribute_id'=>$attribute_discount['attribute_id'],'service_id'=>$attribute_discount['service_id']), 'settingsServiceAttributeDiscountEdit'));
        } else {
            $this->setAction($router->assemble(array(), 'settingsServiceAttributeDiscountAdd'));
        }
    }

}

