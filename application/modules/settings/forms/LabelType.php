<?php

class Settings_Form_LabelType extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('LabelType');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $labelName = (isset($options['label_name']) ? $options['label_name'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();
        $request = Zend_Controller_Front::getInstance()->getRequest();

        $labelNameVal = (!empty($labelName['label_name']) ? $labelName['label_name'] : '');
        $label_type = new Zend_Form_Element_Text('label_name');
        $label_type->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field form-control'))
                ->setValue($labelNameVal);
        $value = $request->getParam('label_name');
        if ($labelNameVal != $value) {
            // $label_type->addValidator(new Zend_Validate_Db_NoRecordExists('label', 'label_name'));
            $modelLabel = new Model_Label();
            $notValid = $modelLabel->getByLabelNameAndCompany($value);
            if ($notValid) {
                $label_type->addError("A record matching '{$value}' was found");
            }
        }

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));

        $this->addElements(array($label_type, $button));
        $this->setMethod('post');
        if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $labelName['id']), 'settingsLabelTypeEdit'));
        } else {
            $this->setAction($router->assemble(array(), 'settingsLabelTypeAdd'));
        }
    }

}

