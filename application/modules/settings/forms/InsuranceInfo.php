<?php

class Settings_Form_InsuranceInfo extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('ContractorInfo');
        $insuranceInfo = (isset($options['insuranceInfo']) ? $options['insuranceInfo'] : '');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
		$contractor_info_id = (isset($options['contractor_info_id']) ? $options['contractor_info_id'] : '');
		
		
		

		
		
        $router = Zend_Controller_Front::getInstance()->getRouter();
		
	
       $insurance_policy_number = new Zend_Form_Element_Text('insurance_policy_number');
        $insurance_policy_number->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                // ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($insuranceInfo['insurance_policy_number']) ? $insuranceInfo['insurance_policy_number'] : ''));
        //  ->setErrorMessages(array('Required' => 'Please enter the Insurance Policy Number'));
		
		$dateFormated = '';
		if(!empty($insuranceInfo['insurance_policy_start']))
			$dateFormated = getNewDateFormat($insuranceInfo['insurance_policy_start']); 
		
        $insurance_policy_start = new Zend_Form_Element_Text('insurance_policy_start');
        $insurance_policy_start->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                // ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($insuranceInfo['insurance_policy_start']) ? (strlen($insuranceInfo['insurance_policy_start']) < 10)? $insuranceInfo['insurance_policy_start']: ($dateFormated ? $dateFormated : date('d-m-Y',$insuranceInfo['insurance_policy_start'])) : ''));
        //  ->setErrorMessages(array('Required' => 'Please enter the Insurance Policy Start'));
		$dateFormated = '';
		if(!empty($insuranceInfo['insurance_policy_expiry']))
			$dateFormated = getNewDateFormat($insuranceInfo['insurance_policy_expiry']);
		
        $insurance_policy_expiry = new Zend_Form_Element_Text('insurance_policy_expiry');
        $insurance_policy_expiry->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                // ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($insuranceInfo['insurance_policy_expiry']) ? (strlen($insuranceInfo['insurance_policy_expiry']) < 10)? $insuranceInfo['insurance_policy_expiry']: ($dateFormated ? $dateFormated : date('d-m-Y',$insuranceInfo['insurance_policy_expiry'])) : ''));
        //->setErrorMessages(array('Required' => 'Please enter the Insurance Policy Expiry'));


        $insurance_listed_services_covered = new Zend_Form_Element_Text('insurance_listed_services_covered');
        $insurance_listed_services_covered->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($insuranceInfo['insurance_listed_services_covered']) ? $insuranceInfo['insurance_listed_services_covered'] : ''));
		
        
		$insurance_type = new Zend_Form_Element_Select('insurance_type');
        $insurance_type->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => ' form-control'))
                ->setValue((!empty($insuranceInfo['insurance_type']) ? $insuranceInfo['insurance_type'] : ''));
		
        $insurnce_type_values = array(''=>'Select One','Workers Compensation'=>'Workers Compensation','Public Liability'=>'Public Liability','Vehicle Insurance'=>'Vehicle Insurance');		

        $insurance_type->addMultiOptions($insurnce_type_values);		
       


		
        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));


		  $this->addElements(array($insurance_listed_services_covered, $insurance_policy_expiry, $insurance_policy_number, $insurance_policy_start, $insurance_type , $button ));
				
        $this->setMethod('post');
		//$this->setAction($router->assemble(array('contractor_id' => $contractor_id), 'settingsContractorInfoEditInsurance'));
		
		if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $insuranceInfo['contractor_insurance_id']), 'settingsContractorInsuranceEdit'));
        } else {
		    
            $this->setAction($router->assemble(array('contractor_info_id' => $contractor_info_id), 'settingsContractorInsuranceAdd'));
        }
        
    }

}

