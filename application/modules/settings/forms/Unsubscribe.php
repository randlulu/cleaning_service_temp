<?php

class Settings_Form_Unsubscribe extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);
		
		
		$this->setName('Unsubscribe');
        $router = Zend_Controller_Front::getInstance()->getRouter();
		$customer = (isset($options['customer']) ? $options['customer'] : '');
		$email = (isset($options['email']) ? $options['email'] : '');
		$customer_email = $customer['email'.$email];
		$mode = (isset($options['mode']) ? $options['mode'] : '');
		
					
  
		$email_address = new Zend_Form_Element_Text('email_address');
        $email_address->setDecorators(array('ViewHelper'))
                ->setDecorators(array('ViewHelper'))
                ->setAttribs(array('class' => 'form-control'))
                ->setValue($customer_email);
				

		


        $this->addElements(array($email_address));
        $this->setMethod('post');
        if($mode == 'cronjob'){
		  $this->setAction($router->assemble(array(), 'UnsubscribeEmails'));
		}else if($mode == 'emailtemplate'){
		  $this->setAction($router->assemble(array(), 'UnsubscribeLink'));
		}else{
		 $this->setAction($router->assemble(array(), 'settingsMailingListUnsubscribe'));
		}
        
		
	}
	
}

?>	