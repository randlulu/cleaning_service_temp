<?php

class Settings_BookingRejectQuestionsController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsType';

        BreadCrumbs::setLevel(2, 'Reject Booking Questions');
        $pageName = $this->request->getParam('page_name', '');
        $this->view->page_title = (!empty($pageName))?$pageName." - Reject Booking Questions Settings":"Reject Booking Questions Settings";
    }

    /**
     * Items list action
     */
    public function indexAction() {

        //
        // check Auth for logged user
        //
        //CheckAuth::checkPermission(array('settingsBookingStatusList'));

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        //
        // get data list
        //

        $modelRejectBookingQuestion = new Model_RejectBookingQuestion();
        $this->view->data = $modelRejectBookingQuestion->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);

        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
    }


	public function addRejectQuestionAction() {

        $router = Zend_Controller_Front::getInstance()->getRouter();
        $reject_booking_question = $this->request->getParam('question', '0');
        $reject_booking_question_type = $this->request->getParam('type', '0');
        $reject_booking_question_option = $this->request->getParam('option');

        if ($this->request->isPost()){ // check if POST request method
			$company_id = CheckAuth::getCompanySession();

			$data = array(
				'question_text'=>$reject_booking_question,
				'question_type'=>$reject_booking_question_type,
				'company_id'=>$company_id
			);
            $modelRejectBookingQuestion = new Model_RejectBookingQuestion();
            $success = $modelRejectBookingQuestion->insert($data);

            if(isset($reject_booking_question_option)){
                foreach($reject_booking_question_option as $key => $value) {
                    $dataOption = array(
                        'question_id'=>$success,
                        'option'=>$value
                    );
                    $modelRejectBookingQuestionOptions = new Model_RejectBookingQuestionOptions();
                    $modelRejectBookingQuestionOptions->insert($dataOption);
                }
            }

			if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                }

                echo 1;
                exit;
        }

        echo $this->view->render('booking-reject-questions/add-edit-reject-question.phtml');
        exit;
    }

	public function editRejectQuestionAction() {

        //
        // get request parameters
        //
        $router = Zend_Controller_Front::getInstance()->getRouter();
        $reject_booking_question_id = $this->request->getParam('question_id', '0');
        $reject_booking_question = $this->request->getParam('question', '0');
        $reject_booking_question_type = $this->request->getParam('type', '0');
        $reject_booking_question_option = $this->request->getParam('option');

		$modelRejectBookingQuestion = new Model_RejectBookingQuestion();
		$question = $modelRejectBookingQuestion->getByQuestionId($reject_booking_question_id);
		$this->view->reject_booking_question = $question;

        $modelRejectBookingQuestionOptions = new Model_RejectBookingQuestionOptions();
        $options = $modelRejectBookingQuestionOptions->getByQuestionId($reject_booking_question_id);
        $this->view->reject_booking_options = $options;
		
        if ($this->request->isPost()){ // check if POST request method
		$company_id = CheckAuth::getCompanySession();

			$data = array(
				'question_text'=>$reject_booking_question,
				'question_type'=>$reject_booking_question_type,
				'company_id'=>$company_id
			);

            $success = $modelRejectBookingQuestion->updateByQuestionId($reject_booking_question_id,$data);
            if(isset($reject_booking_question_option)){
            foreach($reject_booking_question_option as $key => $value) {
                $dataOption = array(
                    'question_id'=>$reject_booking_question_id,
                    'option'=>$value
                );

                $modelRejectBookingQuestionOptions->insert($dataOption);
            }
            }

            if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                }
			echo 1;
			exit;
        }
        echo $this->view->render('booking-reject-questions/add-edit-reject-question.phtml');
        exit;
    }

	public function deleteRejectQuestionAction() {

        //
        // check Auth for logged user
        //
        //CheckAuth::checkPermission(array('settingsInquiryRequiredTypeAdd'));

        //
        // get request parameters
        //
        $router = Zend_Controller_Front::getInstance()->getRouter();
        $reject_booking_question_id = $this->request->getParam('question_id', '0');
        $modelRejectBookingQuestion = new Model_RejectBookingQuestion();
		
        if ($reject_booking_question_id){ // check if POST request method

          $rejectBookingQuestion = $modelRejectBookingQuestion->getByQuestionId($reject_booking_question_id);
          if($rejectBookingQuestion['question_type'] == "multiple"){
              $modelRejectBookingQuestionOptions = new Model_RejectBookingQuestionOptions();
              $modelRejectBookingQuestionOptions->deleteByQuestionId($reject_booking_question_id);
          }

          $success = $modelRejectBookingQuestion->deleteByQuestionId($reject_booking_question_id);
			if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Deleted"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Delete failed, try again"));
                }
			$this->_redirect($this->router->assemble(array(), 'settingsBookingRejectQuestions'));
			exit;
        }
    }

    /**
     * 26/07/2015 D.A
     * Delete optiond of reject booking question action
    */
    public function deleteRejectQuestionOptionAction() {
        //
        // check Auth for logged user
        //
        //CheckAuth::checkPermission(array('settingsCompaniesTradingNamesDelete'));

        $option_id = $this->request->getParam('option_id');
        $modelRejectBookingQuestionOptions = new Model_RejectBookingQuestionOptions();
        $success=$modelRejectBookingQuestionOptions->deleteById($option_id);

        if ($success) {
            $msg='Option deleted';
        } else {
            $msg='Failed to delete, item in use';
        }

        $result= array(
            'msg' =>$msg,
        );
        echo $this->_helper->json($result);exit;
    }


    /**
     * 26/07/2015 D.A
     * Edit option of reject booking question action
     */
    public function editRejectQuestionOptionAction() {
        //
        // check Auth for logged user
        //
        //CheckAuth::checkPermission(array('settingsCompaniesTradingNamesDelete'));

        $option_id = $this->request->getParam('option_id');
        $option = $this->request->getParam('option_val');

        $dataOption = array(
            'option'=>$option
        );

        $modelRejectBookingQuestionOptions = new Model_RejectBookingQuestionOptions();
        $success=$modelRejectBookingQuestionOptions->updateById($option_id,$dataOption);

        if ($success) {
            $msg='Option updated';
        } else {
            $msg='Failed to update, item in use';
        }

        $result= array(
            'msg' =>$msg,
        );
        echo $this->_helper->json($result);exit;
    }

}

