<?php

class Settings_RateTagController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'settings';
//        $this->view->sub_menu = 'settingsPayment';
//        BreadCrumbs::setLevel(2, 'Bank Accounts');
        
        $pageName = $this->request->getParam('page_name', '');
        $this->view->page_title = (!empty($pageName))?$pageName." - Rating Tags Tag Settings":"Rating Tags Tag Settings";
    }

    /**
     * Items list action
     */
    public function indexAction() {

        //
        // check Auth for logged user
        //
//        CheckAuth::checkPermission(array('settingsBankList'));
        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'bank_id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        //
        // get data list
        //

        $filters2['with_image_attachment_join'] = 1;
        $filters2['company_id'] = CheckAuth::getCompanySession();
        $model_ratingTag = new Model_RatingTag();
        $this->view->data = $model_ratingTag->getAll2($filters2);

        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
    }

    /**
     * Add new item action
     */
    public function addAction() {

        //
        // check Auth for logged user
        //
//        CheckAuth::checkPermission(array('settingsBankAdd'));
        //
        // get request parameters
        //
        $model_ratingtag = new Model_RatingTag();
        
        $model_image_attachment = new Model_ImageAttachment();
//    
        $router = Zend_Controller_Front::getInstance()->getRouter();

        //
        // init action form
        //
        $form = new Settings_Form_RateTag();

        //
        // handling the insertion process
        //
        $id=0;
        $fileUpload = 0;
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $tag_name = $this->request->getParam("name");
                $tag_question = $this->request->getParam("question");
            
                $loggedUser = CheckAuth::getLoggedUser();
                $created_by = $loggedUser['user_id'];
            
                $created = time();
                
                $company_id = CheckAuth::getCompanySession();
                
                 /*
                $id = $model_image_attachment->insert(array('description' => $tag_name));
               
                $source = $_FILES['imageFile']['tmp_name'];
                $imageInfo = pathinfo($source);
                            $ext = $imageInfo['extension'];
                            $dir = get_config('image_attachment') . '/';
                            $subdir = date('Y/m/d/');
                            $fullDir = $dir . $subdir;
                 if (!is_dir($fullDir)) {
                                mkdir($fullDir, 0777, true);
                            }
                            $original_path = "original_{$id}.{$ext}";
                            $large_path = "large_{$id}.{$ext}";
                            $small_path = "small_{$id}.{$ext}";
                            $thumbnail_path = "thumbnail_{$id}.{$ext}";
                            $compressed_path = "compressed_{$id}.jpg";

                            $data = array(
                                'original_path' => $subdir . $original_path,
                                'large_path' => $subdir . $large_path,
                                'small_path' => $subdir . $small_path,
                                'thumbnail_path' => $subdir . $thumbnail_path,
                                'compressed_path' => $subdir . $compressed_path
                            );

                            $model_image_attachment->updateById($id, $data);

                            //save image to database and filesystem here
                            $image_saved = copy($source, $fullDir . $original_path);
                            $compressed_saved = copy($source, $fullDir . $compressed_path);
                            ImageMagick::scale_image($source, $fullDir . $large_path, 510);
                            ImageMagick::scale_image($source, $fullDir . $small_path, 250);
                            ImageMagick::create_thumbnail($source, $fullDir . $thumbnail_path, 78, 64);
                            ImageMagick::convert($source, $fullDir . $compressed_path);
                            ImageMagick::compress_image($source, $fullDir . $compressed_path);
                            
       

                            if ($image_saved) {
                                if (file_exists($source)) {
                                    unlink($source);
                                     $fileUpload = 1;
                                      $attachments[] = $fullDir . $original_path;         
                                }
                            }
                */
                
              
                /*
                 if ($form->image->isUploaded()) {
                //$source = $form->image->getFileName();
                     $img=$_FILES['image']['name'];
                      $source = $_FILES['image']['tmp_name'];
                            $fileInfo = pathinfo($img);
                $ext = $fileInfo['extension'];
                */
                
                     /*
                     echo 'source'.$source.'<br>';
                     
                     if(isset($_FILES['image']))
{
    $img=$_FILES['image']['name'];
    $ruta= $_FILES['image']['tmp_name'];
                         echo 'img '.$img.'<br>';
                         echo 'ruta '.$ruta.'<br>';
}
                     */
                
                $upload = new Zend_File_Transfer_Adapter_Http();
                $files = $upload->getFileInfo();
                foreach ($files as $file => $fileInfo) {
                    if ($upload->isUploaded($file)) {
                        if ($upload->receive($file)) {
                            $info = $upload->getFileInfo($file);
                            $source = $info[$file]['tmp_name'];
                            $imageInfo = pathinfo($source);
                            $ext = $imageInfo['extension'];                
                            $id = $model_image_attachment->insert(array('description' => $tag_name));
                            $dir = get_config('image_attachment') . '/';
                            $subdir = date('Y/m/d/');
                            $fullDir = $dir . $subdir;
                            if (!is_dir($fullDir)) {
                                mkdir($fullDir, 0777, true);
                            }
                            $original_path = "original_{$id}.{$ext}";
                            $large_path = "large_{$id}.{$ext}";
                            $small_path = "small_{$id}.{$ext}";
                            $thumbnail_path = "thumbnail_{$id}.{$ext}";

                            $data = array(
                                'original_path' => $subdir . $original_path,
                                'large_path' => $subdir . $large_path,
                                'small_path' => $subdir . $small_path,
                                'thumbnail_path' => $subdir . $thumbnail_path
                            );

                          //save image to database and filesystem here
                            $image_saved = copy($source, $fullDir . $original_path);
                            ImageMagick::scale_image($source, $fullDir . $large_path, 510);
                            ImageMagick::scale_image($source, $fullDir . $small_path, 250);
                            ImageMagick::create_thumbnail($source, $fullDir . $thumbnail_path, 78, 64);
                            
                            if ($image_saved) {
                                if (file_exists($source)) {
                                    $model_image_attachment->updateById($id, $data);
                                    unlink($source);
                                    $fileUpload = 1;       
                                }
                            }
                        }
                    }
                }
              
                $data = array(
                    'tag_name' => $tag_name,
                    'tag_question' => $tag_question,
                    'image_id' => $id,
                    'created' => $created,
                    'created_by' => $created_by,
                    'company_id' => $company_id
                );

                $success = $model_ratingtag->insert($data);
                
                if ($success && $fileUpload) {

                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {

                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Bank"));
                }
                //$this->_redirect($this->router->assemble(array(), 'rateTag'));
                echo 1;
                exit;
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('rate-tag/add_edit.phtml');
        exit;
    }

    public function deleteAction() {
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        //
        // check Auth for logged user
        //
//        CheckAuth::checkPermission(array('settingsBankDelete'));
        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);

        $model_ratingtag = new Model_RatingTag();
        
        $rateTag = $model_ratingtag->getById($id);
        
        $image_id = $rateTag['image_id'];
        

        $success1= $model_ratingtag->deleteById($id);
        
        $model_image_attachment = new Model_ImageAttachment();
        
        $imageAttachment = $model_image_attachment->getById($image_id);
        
                if ($imageAttachment) {
                  
                    if (file_exists(get_config('image_attachment') . '/' . $imageAttachment['original_path'])) {
                        unlink(get_config('image_attachment') . '/' . $imageAttachment['original_path']);
                    }
                    if (file_exists(get_config('image_attachment') . '/' . $imageAttachment['large_path'])) {
                        unlink(get_config('image_attachment') . '/' . $imageAttachment['large_path']);
                    }
                    if (file_exists(get_config('image_attachment') . '/' . $imageAttachment['small_path'])) {
                        unlink(get_config('image_attachment') . '/' . $imageAttachment['small_path']);
                    }
                    if (file_exists(get_config('image_attachment') . '/' . $imageAttachment['thumbnail_path'])) {
                        unlink(get_config('image_attachment') . '/' . $imageAttachment['thumbnail_path']);
                    }
                       
                }
        
        $success2 = $model_image_attachment->deleteById($image_id);
        /*
        if ($success1 && $success2) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Deleted successfully"));
        } else if ($success1 && !$success2) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => " Image could not be deleted"));
        }
        else  {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Could not be deleted"));
        }
        */

        //$this->_redirect($this->router->assemble(array(), 'rateTag'));
        
        
        if ($success1 && $success2) {
            echo "Deleted successfully";
        } else {
            echo "Error .. Could not be deleted";
            
        }


        exit;

    }

    public function editAction() {


        $id = $this->request->getParam('id');
        $model_ratingtag = new Model_RatingTag();
//      
        $router = Zend_Controller_Front::getInstance()->getRouter();

        //
        // init action form
        //
//        $form = new Settings_Form_RateTag();
        //
        // validation
        //
//        $bankObj = new Model_Bank();
        $rateTag = $model_ratingtag->getById($id);
        if (!$rateTag) {
            $this->_redirect($this->router->assemble(array(), 'rateTag'));
            return;
        }
        $image_id = $rateTag['image_id'];
        $model_image_attachment = new Model_ImageAttachment();
        $image = $model_image_attachment->getById($image_id)['small_path'];

        //
        // init action form
        //
        $form = new Settings_Form_RateTag(array('mode' => 'update', 'ratingTag' => $rateTag));

        //
        // handling the updating process
        //
        

        $fileUpload = 0;
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $tag_name = $this->request->getParam("name");
                $tag_question = $this->request->getParam("question");
                
                $upload = new Zend_File_Transfer_Adapter_Http();
                $files = $upload->getFileInfo();
                foreach ($files as $file => $fileInfo) {
                    if ($upload->isUploaded($file)) {
                        if ($upload->receive($file)) {
                
                            $info = $upload->getFileInfo($file);
                            $source = $info[$file]['tmp_name'];
                            $imageInfo = pathinfo($source);
                            $ext = $imageInfo['extension'];
                            
                            
                    //or update old image
                            
                   $imageAttachment = $model_image_attachment->getById($image_id);
        
                if ($imageAttachment) {
                  
                    if (file_exists(get_config('image_attachment') . '/' . $imageAttachment['original_path'])) {
                        unlink(get_config('image_attachment') . '/' . $imageAttachment['original_path']);
                    }
                    if (file_exists(get_config('image_attachment') . '/' . $imageAttachment['large_path'])) {
                        unlink(get_config('image_attachment') . '/' . $imageAttachment['large_path']);
                    }
                    if (file_exists(get_config('image_attachment') . '/' . $imageAttachment['small_path'])) {
                        unlink(get_config('image_attachment') . '/' . $imageAttachment['small_path']);
                    }
                    if (file_exists(get_config('image_attachment') . '/' . $imageAttachment['thumbnail_path'])) {
                        unlink(get_config('image_attachment') . '/' . $imageAttachment['thumbnail_path']);
                    }
                       
                }
                       $model_image_attachment->deleteById($image_id);
                            
                        $image_id = $model_image_attachment->insert(array('description' => $tag_name));
                           // $info = $upload->getFileInfo($file);
                           // $source = $info[$file]['tmp_name'];
                           // $imageInfo = pathinfo($source);
                          //  $ext = $imageInfo['extension'];
                            $dir = get_config('image_attachment') . '/';
                            $subdir = date('Y/m/d/');
                            $fullDir = $dir . $subdir;
                            if (!is_dir($fullDir)) {
                                mkdir($fullDir, 0777, true);
                            }
                            $original_path = "original_{$image_id}.{$ext}";
                            $large_path = "large_{$image_id}.{$ext}";
                            $small_path = "small_{$image_id}.{$ext}";
                            $thumbnail_path = "thumbnail_{$image_id}.{$ext}";

                            $data = array(
                                'original_path' => $subdir . $original_path,
                                'large_path' => $subdir . $large_path,
                                'small_path' => $subdir . $small_path,
                                'thumbnail_path' => $subdir . $thumbnail_path
                            );

                            
                          //save image to database and filesystem here
                            $image_saved = copy($source, $fullDir . $original_path);
                            ImageMagick::scale_image($source, $fullDir . $large_path, 510);
                            ImageMagick::scale_image($source, $fullDir . $small_path, 250);
                            ImageMagick::create_thumbnail($source, $fullDir . $thumbnail_path, 78, 64);
                            
       

                            if ($image_saved) {
                                if (file_exists($source)) {
                                    $model_image_attachment->updateById($image_id, $data);
                                    unlink($source);
                                     $fileUpload = 1;
                                               
                                }
                            }
                       } 
                        } 
                    }
                

                $data = array(
                    'tag_name' => $tag_name,
                    'tag_question' => $tag_question,
                    'image_id' => $image_id
                );



                $success = $model_ratingtag->update($data, "rating_tag_id = '{$id}'");

                if ($success || $fileUpload) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Bank"));
                }

                echo 1;
                //$this->_redirect($this->router->assemble(array(), 'rateTag'));
                exit;
                //$this->_redirect($this->router->assemble(array(), 'settingsBankList'));
                //return;
            }
        }

        $this->view->form = $form;
        $this->view->image = $image;
        $this->view->id = $id;

        //
        // render views
        //
        echo $this->view->render('rate-tag/add_edit.phtml');
        exit;
    }
    
    public function removeTagImageAction() {

        $id = $this->request->getParam('id');
        
        $model_ratingtag = new Model_RatingTag();
        $rateTag = $model_ratingtag->getById($id);
        
        $image_id = $rateTag['image_id'];
        
        $model_image_attachment = new Model_ImageAttachment();
        
        $imageAttachment = $model_image_attachment->getById($image_id);
        
                if ($imageAttachment) {
                    
                  
                    if (file_exists(get_config('image_attachment') . '/' . $imageAttachment['original_path'])) {
                        unlink(get_config('image_attachment') . '/' . $imageAttachment['original_path']);
                    }
                    if (file_exists(get_config('image_attachment') . '/' . $imageAttachment['large_path'])) {
                        unlink(get_config('image_attachment') . '/' . $imageAttachment['large_path']);
                    }
                    if (file_exists(get_config('image_attachment') . '/' . $imageAttachment['small_path'])) {
                        unlink(get_config('image_attachment') . '/' . $imageAttachment['small_path']);
                    }
                    if (file_exists(get_config('image_attachment') . '/' . $imageAttachment['thumbnail_path'])) {
                        unlink(get_config('image_attachment') . '/' . $imageAttachment['thumbnail_path']);
                    }
                       
                }
        
        $success = $model_image_attachment->deleteById($image_id);
        
        if ($success) {
            echo "Image Removed Successfully";
        } else {
            echo "Error .. Image Not Reomved";
            
        }
        exit;
    }

}
