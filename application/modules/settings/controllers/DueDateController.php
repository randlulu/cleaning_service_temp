<?php

class Settings_DueDateController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();


        // main menu selected item
        $this->view->main_menu = 'settings';
        // sub menu selected item
        $this->view->sub_menu = 'settingsPayment';

        BreadCrumbs::setLevel(2, 'Due Dates');
    }

    /**
     * Items list action
     */
    public function indexAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsDueDateList'));
        
        $pageName = $this->request->getParam('page_name', '');
        $this->view->page_title = (!empty($pageName))?$pageName." - Due Dates Settings":"Due Dates Settings";

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        //
        // get data list
        //
        $duetoTypeObj = new Model_DueDate();
        $this->view->data = $duetoTypeObj->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);

        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
    }

    public function addAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsDueDateAdd'));

        //
        // get request parameters
        //
        $dueDate = $this->request->getParam('due_date');
        $isDefault = $this->request->getParam('is_default');

        $router = Zend_Controller_Front::getInstance()->getRouter();

        //
        // init action form
        //
        $form = new Settings_Form_DueDate();

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $modelDueDate = new Model_DueDate();

                if ($isDefault) {
                    //set all is_default = 0
                    $modelDueDate->update(array('is_default' => 0), '');
                }

                $data = array(
                    'due_date' => $dueDate,
                    'is_default' => $isDefault,
                    'company_id' => CheckAuth::getCompanySession()
                );
                $success = $modelDueDate->insert($data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                }

                if (!$modelDueDate->fetchRow("is_default = 1")) {
                    $modelDueDate->update(array('is_default' => 1), 'due_date = 15');
                }

                echo 1;
                exit;
            } else {
                $messages = $form->getElement('due_date')->getErrorMessages();
                if ($messages) {
                    $form->getElement('due_date')->clearErrorMessages();
                    foreach ($messages as $message) {
                        $form->getElement('due_date')->addError($message);
                    }
                }
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('due-date/add_edit.phtml');
        exit;
    }

    public function deleteAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsDueDateDelete'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());

        if ($id) {
            $ids[] = $id;
        }

        $modelDueDate = new Model_DueDate();
        foreach ($ids as $id) {
            $dueDate = $modelDueDate->getById($id);
            if (!$dueDate['is_core'] && CheckAuth::checkIfCanHandelAllCompany('due_date', $id)) {

                $isNotRelated = $modelDueDate->checkBeforeDeleteDueDate($id);
                if ($isNotRelated) {
                    $success = $modelDueDate->deleteById($dueDate['id']);
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Error: Failed to delete, item in use"));
                }
            }
        }

        if ($success) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Deleted"));
        }

        if (!$modelDueDate->fetchRow("is_default = 1")) {
            $modelDueDate->update(array('is_default' => 1), 'due_date = 15');
        }

        $this->_redirect($this->router->assemble(array(), 'settingsDueDateList'));
    }

    public function editAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsDueDateEdit'));

        //
        // get request parameters
        //
        $dueDate = $this->request->getParam('due_date');
        $isDefault = $this->request->getParam('is_default');

        $id = $this->request->getParam('id');


        if (!CheckAuth::checkIfCanHandelAllCompany('due_date', $id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // validation
        //
        $modelDueDate = new Model_DueDate();
        $dueDateOld = $modelDueDate->getById($id);

        if (!$dueDateOld) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));

            $this->_redirect($this->router->assemble(array(), 'settingsDueDateList'));
            return;
        }


        //
        // init action form
        //
     
        $form = new Settings_Form_DueDate(array('mode' => 'update', 'dueDate' => $dueDateOld));


        //
        // handling the updating process
        //
        
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) {
                if (!$dueDateOld['is_core']) {

                    if ($isDefault) {
                        //set all is_default = 0
                        $modelDueDate->update(array('is_default' => 0), '');
                    }

                    // validate form data
                    $data = array(
                        'due_date' => $dueDate,
                        'is_default' => $isDefault,
                        'company_id' => CheckAuth::getCompanySession()
                    );
                    $success = $modelDueDate->updateById($id, $data);

                    if ($success) {
                        $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved"));
                    } else {
                        $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                    }
                }

                if (!$modelDueDate->fetchRow("is_default = 1")) {
                    $modelDueDate->update(array('is_default' => 1), 'due_date = 15');
                }

                echo 1;
                exit;
            }
        }

        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('due-date/add_edit.phtml');
        exit;
    }

    public function setAsDefaultAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsDueDateAsDefault'));

        $id = $this->request->getParam('id');



        $modelDueDate = new Model_DueDate();

        $dueDate = $modelDueDate->getById($id);

        if ($dueDate['company_id'] != 0) {
            if (!CheckAuth::checkIfCanHandelAllCompany('due_date', $id)) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            }
        }

        //set all is_default = 0
        $modelDueDate->update(array('is_default' => 0), '');
        $modelDueDate->updateById($id, array('is_default' => 1));


        $this->_redirect($this->router->assemble(array(), 'settingsDueDateList'));
    }

}
