<?php

class Settings_InquiryRequiredTypeController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsType';

        BreadCrumbs::setLevel(2, 'Required Types');
        
        $pageName = $this->request->getParam('page_name', '');
        $this->view->page_title = (!empty($pageName))?$pageName." - Inquiry Required Types Settings":"Inquiry Required Types Settings";
    }

    /**
     * Items list action
     */
    public function indexAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsRequiredTypes'));

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        //
        // get data list
        //
        $modelInquiryRequiredType = new Model_InquiryRequiredType();
        $this->view->data = $modelInquiryRequiredType->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);

        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
    }

    /**
     * Add new item action
     */
    public function addAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsInquiryRequiredTypeAdd'));

        //
        // get request parameters
        //
        $router = Zend_Controller_Front::getInstance()->getRouter();
        $requiredType = $this->request->getParam('required_type', '');

        //
        // init action form
        //
        $form = new Settings_Form_InquiryRequiredType();

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $modelInquiryRequiredType = new Model_InquiryRequiredType();

                $data = array(
                    'required_type' => $requiredType,
                    'company_id' => CheckAuth::getCompanySession()
                );

//                if ($modelInquiryRequiredType->getByType($requiredType)) {
//                    $success = false;
//                } else {
                $success = $modelInquiryRequiredType->insert($data);
//                }

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                }
                echo 1;
                exit;
            } else {
                $messages = $form->getElement('required_type')->getErrorMessages();
                if ($messages) {
                    $form->getElement('required_type')->clearErrorMessages();
                    foreach ($messages as $message) {
                        $form->getElement('required_type')->addError($message);
                    }
                }
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('inquiry-required-type/add_edit.phtml');
        exit;
    }

    /**
     * edit the item action
     */
    public function editAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsInquiryRequiredTypeEdit'));

        //
        // get request parameters
        //
        $router = Zend_Controller_Front::getInstance()->getRouter();

        $requiredType = $this->request->getParam('required_type', '');
        $requiredTypeId = $this->request->getParam('id', '');


        if (!CheckAuth::checkIfCanHandelAllCompany('inquiry_required_type', $requiredTypeId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // init action form
        //
        $modelInquiryRequiredType = new Model_InquiryRequiredType();
        $required = $modelInquiryRequiredType->getById($requiredTypeId);

        $options = array(
            'mode' => 'update',
            'required' => $required
        );
        $form = new Settings_Form_InquiryRequiredType($options);

        //
        // handling the insertion process
        //                       

        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'required_type' => $requiredType,
                    'company_id' => CheckAuth::getCompanySession()
                );
                $success = $modelInquiryRequiredType->updateById($requiredTypeId, $data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Changed"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                }
                echo 1;
                exit;
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('inquiry-required-type/add_edit.phtml');
        exit;
    }

    public function deleteAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsInquiryRequiredTypeDelete'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);

        if (!CheckAuth::checkIfCanHandelAllCompany('inquiry_required_type', $requiredTypeId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $modelInquiryRequiredType = new Model_InquiryRequiredType();

        $isNotRelated = $modelInquiryRequiredType->checkBeforeDeleteInquiryRequiredType($id);
        if ($isNotRelated) {
            $modelInquiryRequiredType->deleteById($id);
        } else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Error: Failed to delete, item in use "));
        }


        $this->_redirect($this->router->assemble(array(), 'settingsInquiryRequiredTypes'));
    }

}

