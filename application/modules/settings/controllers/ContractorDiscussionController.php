<?php

class Settings_ContractorDiscussionController extends Zend_Controller_Action {

    private $request;
    private $router;
    private $loggedUser;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        //
        //checkLoggedIn
        //
        CheckAuth::checkLoggedIn();

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('bookingDiscussion'));

        //
        //get Logged User
        //
        $this->loggedUser = CheckAuth::getLoggedUser();
    }

    public function indexAction() {


        //
        //get Params
        //
        $contractor_id = $this->request->getParam('contractor_id', 0);


        $loggedUser = CheckAuth::getLoggedUser();
        $contractorDiscussions = array();
        $isContractor = false;

        $modelAuthRole = new Model_AuthRole();
        $contractorRoleId = $modelAuthRole->getRoleIdByName('contractor');

        //
        // load model
        //
        $modelContractorDiscussionMongo = new Model_ContractorDiscussionMongo();
        $modelUser = new Model_User();

        $contractor = $modelUser->getById($contractor_id);
        $this->view->contractor = $contractor;

        if (!$contractor) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Contractor is not exist"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        if ($loggedUser['role_id'] == $contractorRoleId) {
            $isContractor = true;
        }

        $this->view->contractor_id = $contractor_id;
        $this->view->ContractorDiscussions = $modelContractorDiscussionMongo->getByContractorId($contractor_id, $isContractor);

        echo $this->view->render('contractor-discussion/index.phtml');
        exit;
    }

    public function submitContractorDiscussionAction() {

        //
        //get Params
        //
        $contractorId = $this->request->getParam('contractor_id', 0);
        $discussion = $this->request->getParam('discussion', '');
        $visibility = $this->request->getParam('visibility', 1);
        $notifyPersons = $this->request->getParam('notifyPersons', '');
        $notifyusersString = $notifyCustomersString = '';
        $notifyCustomers = array();
        $notifyUsers = array();
        if ($notifyPersons) {
            $notify = explode(',', $notifyPersons);


            foreach ($notify as $value) {
                if (substr($value, 0, 1) == 'c') {
                    $id = substr($value, 1);
                    $notifyCustomers[] = $id;
                } else {
                    $notifyUsers[] = $value;
                }
            }

            $notifyusersString = implode(',', $notifyUsers);
            $notifyCustomersString = implode(',', $notifyCustomers);
        }

        $thumbnails_paths = array();
        $originals_paths = array();
        $larges_paths = array();
        $small_paths = array();
        $compressed_paths = array();

        //
        // load model
        //
        $modelContractorDiscussionMongo = new Model_ContractorDiscussionMongo();
        $modelContractorInfo = new Model_ContractorInfo();
        $modelUser = new Model_User();
        $modelImage = new Model_Image();

        $success = 0;
        if ($discussion) {

            $user = $modelUser->getById($this->loggedUser['user_id']);
            if ($user['role_name'] == 'contractor') {
                $contractorInfo = $modelContractorInfo->getByContractorId($user['user_id']);
                $user['username'] = ucwords($user['username']) . ' - ' . ucwords($contractorInfo['business_name']);
            } else {
                $user['username'] = ucwords($user['username']);
            }
            $contractor = $modelUser->getById($contractorId);
            /* $contractorInfo2 = $modelContractorInfo->getByContractorId($contractor['user_id']);
              $contractor['username'] = ucwords($contractor['username']) . ' - ' . ucwords($contractorInfo2['business_name']); */

            $doc = array(
                'contractor_id' => (int) $contractorId,
                'contractor_name' => $contractor['username'],
                'user_id' => $this->loggedUser['user_id'],
                'user_name' => $user['username'],
                'avatar' => $user['avatar'],
                'user_message' => $discussion,
                'created' => time(),
                'user_role_name' => $user['role_name'],
                'thumbnail_images' => array(),
                'original_images' => array(),
                'large_images' => array(),
                'small_images' => array(),
                'compressed_images' => array(),
                'seen_by_ids' => "",
                'seen_by_names' => "",
                'visibility' => (int) $visibility,
                'type' => "contractor_discussion"
                    //,'audio_path' => ""
            );

            $success = $modelContractorDiscussionMongo->insertDiscussion($doc);
            //$modelContractorDiscussionMongo->insertDiscussion($doc);
            $newDocID = $doc['_id'];
            foreach ($newDocID as $key => $value)
                $disc_id = $value;
            //echo "id: " . $idd;
            //echo "by walaa " ;print_r($newDocID);

            $upload = new Zend_File_Transfer_Adapter_Http();
            $files = $upload->getFileInfo();
            $countFiles = count($files);


            $image_group = 0;
            if ($countFiles > 1) {
                $maxGroup = max($modelImage->getMaxGroup());
                $image_group = $maxGroup['group_id'] + 1;
            }
            if (isset($files) && !empty($files)) {
                $i = 1;
                foreach ($files as $file => $fileInfo) {
                    if ($upload->isUploaded($file)) {
                        if ($upload->receive($file)) {
                            $info = $upload->getFileInfo($file);
                            $source = $info[$file]['tmp_name'];
                            $imageInfo = pathinfo($source);
                            $ext = $imageInfo['extension'];
                            $dir = get_config('image_attachment') . '/';
                            $subdir = date('Y/m/d/');
                            $fullDir = $dir . $subdir;
                            if (!is_dir($fullDir)) {
                                mkdir($fullDir, 0777, true);
                            }

                            $loggedUser = CheckAuth::getLoggedUser();

                            $id = $disc_id . $i;

                            $original_path = "original_{$id}.{$ext}";
                            $large_path = "large_{$id}.{$ext}";
                            $small_path = "small_{$id}.{$ext}";
                            $thumbnail_path = "thumbnail_{$id}.{$ext}";
                            $compressed_path = "compressed_{$id}.jpg";

                            $thumbnails_paths[$id] = $subdir . $thumbnail_path;
                            $originals_paths[$id] = $subdir . $original_path;
                            $larges_paths[$id] = $subdir . $large_path;
                            $small_paths[$id] = $subdir . $small_path;
                            $compressed_paths[$id] = $subdir . $compressed_path;

                            $image_saved = copy($source, $fullDir . $original_path);
                            $compressed_saved = copy($source, $fullDir . $compressed_path);
                            ImageMagick::scale_image($source, $fullDir . $large_path, 510);
                            ImageMagick::scale_image($source, $fullDir . $small_path, 250);
                            ImageMagick::create_thumbnail($source, $fullDir . $thumbnail_path, 78, 64);
                            ImageMagick::convert($source, $fullDir . $compressed_path);
                            ImageMagick::compress_image($source, $fullDir . $compressed_path);

                            if ($image_saved) {
                                if (file_exists($source)) {
                                    unlink($source);
                                }
                            }
                        } else {
                            echo json_encode(array('success' => 0, 'errMsg' => 'Errors Receiving File'));
                            exit;
                        }
                    }
                    $i++;
                }
                //////
                $modelContractorDiscussionMongo->updateDiscImages($disc_id, $thumbnails_paths, $originals_paths, $larges_paths, $small_paths, $compressed_paths);
            }

            $notifyContractor = 1;
            if ($visibility == 1) {
                $notifyContractor = 0;
            }
            MobileNotification::notify(0, 'new contractor discussion', array('contractor_id' => $contractorId, 'discussion_id' => $disc_id, 'notidy_multiple' => $notifyusersString, 'notify_contractor' => $notifyContractor));

            if ($notifyCustomers) {
                $modelCustomer = new Model_Customer();
                $modelEmailTemplate = new Model_EmailTemplate();
                foreach ($notifyCustomers as $notifyCustomer) {
                    $customer = $modelCustomer->getById($notifyCustomer);
                    if ($customer['email1'] && filter_var($customer['email1'], FILTER_VALIDATE_EMAIL)) {
                        $to[0] = array('email' => $customer['email1'], 'number' => '1');
                    }
                    if ($customer['email2'] && filter_var($customer['email2'], FILTER_VALIDATE_EMAIL)) {
                        $to[1] = array('email' => $customer['email2'], 'number' => '2');
                    }
                    if ($customer['email3'] && filter_var($customer['email3'], FILTER_VALIDATE_EMAIL)) {
                        $to[2] = array('email' => $customer['email3'], 'number' => '3');
                    }

                    $template_params = array(
                        "{customer_display_name}" => get_customer_name($customer),
                        "{logged_username}" => $this->loggedUser['username'],
                        "{technician_display_name}" => $contractor['username'],
                        "{user_message}" => nl2br(htmlentities($discussion, ENT_NOQUOTES, 'UTF-8'))
                    );
                    $emailTemplate = $modelEmailTemplate->getEmailTemplate('new_discussion', $template_params);

                    $params = array(
                        'to' => $to,
                        'body' => $emailTemplate['body'],
                        'subject' => $emailTemplate['subject']
                    );

                    $email_log = array('reference_id' => $contractorId, 'type' => 'discussion');

                    if ($to) {
                        //var_dump($to);
                        foreach ($to as $toEmail) {
                            $params['to'] = $toEmail['email'];
                            EmailNotification::sendEmail($params, 'new_discussion', $template_params, $email_log);
                        }
                    }
                }
            }
        }
        $modelContractorDiscussionMongo->closeConnection();
        if ($success) {
            echo json_encode(array('success' => 1));
        } else {
            echo json_encode(array('success' => 0, 'errMsg' => 'Invalid Discussion'));
        }
        exit;
    }

    public function allContractorDiscussionAction() {

        //
        //get Params
        //
        $contractor_id = $this->request->getParam('contractor_id', 0);
        $loggedUser = CheckAuth::getLoggedUser();
        $contractorDiscussions = array();
        $contractorDiscussions2 = array();
        $isContractor = false;
        //
        // load model
        //
		$modelAuthRole = new Model_AuthRole();
        $contractorRoleId = $modelAuthRole->getRoleIdByName('contractor');


        if ($loggedUser['role_id'] == $contractorRoleId) {
            $isContractor = true;
        }
        $modelContractorDiscussionMongo = new Model_ContractorDiscussionMongo();
        $contractorDiscussions = $modelContractorDiscussionMongo->getByContractorId($contractor_id, $isContractor);
        if ($contractorDiscussions) {
            foreach ($contractorDiscussions as $key => $row)
                $contractorDiscussions2[] = $row;


            foreach ($contractorDiscussions2 as &$ContractorDiscussion) {
                $lastFourImages = array();
                if (count($ContractorDiscussion['thumbnail_images']) > 4) {
                    $lastFourImages = array_slice($ContractorDiscussion['thumbnail_images'], 0, 4, true);
                } else {
                    $lastFourImages = $ContractorDiscussion['thumbnail_images'];
                }
                $ContractorDiscussion['thumbnail_images'] = $lastFourImages;
                $ContractorDiscussion['seen_ids'] = explode(',', $ContractorDiscussion['seen_by_ids']);
                $ContractorDiscussion['seen_names'] = explode(',', $ContractorDiscussion['seen_by_names']);
                $ContractorDiscussion['discussion_date'] = time_ago($ContractorDiscussion['created']);
                $ContractorDiscussion['user_message'] = urldecode($ContractorDiscussion['user_message']);
                //var_dump( $ContractorDiscussion['seen_ids']);
            }
        }

        $adminRoleId = $modelAuthRole->getRoleIdByName('super_admin');
        $manggerRoleId = $modelAuthRole->getRoleIdByName('manager');
        $salesRoleId = $modelAuthRole->getRoleIdByName('sales');
        //echo $adminRoleId . $manggerRoleId . $salesRoleId;exit;

        $filters['roles'] = array($adminRoleId, $manggerRoleId, $salesRoleId);
        $filters['active'] = 'TRUE';
        $filters['logged_user_id'] = $loggedUser['user_id'];

        $modelUser = new Model_User();
        $users = $modelUser->getAll($filters);
        //$this->view->users = $users;*/

        $json_array = array(
            'discussions' => $contractorDiscussions2,
            'count' => count($contractorDiscussions2),
            'users' => $users
        );

        echo json_encode($json_array);
        exit;
    }

    public function changeCommentSeenFlagAction() {
        /* $id = $this->request->getParam("discussion_id");
          $contractor_id = $this->request->getParam("contractor_id"); */


        $id = $this->request->getParam("discussion_id");
        //$logged_user_id = $this->request->getParam("logged_user_id");

        $loggedUser = CheckAuth::getLoggedUser();

        $modelContractorDiscussionMongo = new Model_ContractorDiscussionMongo();

        $success = $modelContractorDiscussionMongo->changeCommentSeenFlagById($id, $loggedUser['user_id'], $loggedUser['username']);

        if ($success) {
            echo 1;
        }
        exit;
    }

    public function markAllCommentsAsSeenAction() {
        $contractor_id = $this->request->getParam("contractor_id");
        $logged_user_id = $this->request->getParam("logged_user_id");

        $loggedUser = CheckAuth::getLoggedUser();
        $isContractor = false;

        $modelAuthRole = new Model_AuthRole();
        $contractorRoleId = $modelAuthRole->getRoleIdByName('contractor');

        if ($loggedUser['role_id'] == $contractorRoleId) {
            $isContractor = true;
        }

        $modelContractorDiscussionMongo = new Model_ContractorDiscussionMongo();
        //$modelUser = new Model_User();
        //$contractor = $modelUser->getById($contractor_id);
        $success = $modelContractorDiscussionMongo->markAllAsSeen($contractor_id, $loggedUser['username'], $logged_user_id, $isContractor);

        if ($success) {
            echo 1;
        }
        exit;
    }

    public function updateDeletedCommentAction() {
        $discussion_id = $this->request->getParam("discussion_id");

        $modelContractorDiscussionMongo = new Model_ContractorDiscussionMongo();

        $deleteDis = $modelContractorDiscussionMongo->deleteById($discussion_id);
        if ($deleteDis) {
            echo 1;
        }
        exit;
    }

    public function getUsersAction() {

        $loggedUser = CheckAuth::getLoggedUser();
        $filters = array();

        $filters['active'] = 'TRUE';
        $filters['logged_user_id'] = $loggedUser['user_id'];
        $modelAuthRole = new Model_AuthRole();
        $adminRoleId = $modelAuthRole->getRoleIdByName('super_admin');
        $manggerRoleId = $modelAuthRole->getRoleIdByName('manager');
        $salesRoleId = $modelAuthRole->getRoleIdByName('sales');

        $filters['roles'] = array($adminRoleId, $manggerRoleId, $salesRoleId);

        $modelUser = new Model_User();
        $users = $modelUser->getAll($filters);

        $json_array = array(
            'users' => $users
        );

        echo json_encode($json_array);
        exit;
    }

    public function getCustomersAction() {
        //$is_first_time = $this->request->getParam('is_first_time');
        $page_number = $this->request->getParam('page_number');

        $modelCustomer = new Model_Customer();
        $customers = array();

        if ($this->request->isPost()) {
            if (isset($page_number)) {
                $perPage = 50;
                $currentPage = $page_number + 1;
            }

            $customers = $modelCustomer->getAll(array(), null, $pager, 0, $perPage, $currentPage);

            foreach ($customers as &$customer) {
                $customer['new_name'] = get_customer_name($customer);
                $customer['new_customer_id'] = 'c' . $customer['customer_id'];
            }

            $result = array();
            $result['data'] = $customers;
            if ($customers) {
                $result['is_last_request'] = 0;
            } else {
                $result['is_last_request'] = 1;
            }
            echo json_encode($result);
            exit;
        }
        exit;
    }

    public function editDiscussionVisibilityAction() {
        $discussion_id = $this->request->getParam('discussion_id');
        $newVisibility = $this->request->getParam('newVisibility');
        $newVisibility = (int) $newVisibility;

        $modelContractorDiscussionMongo = new Model_ContractorDiscussionMongo();


        $data = array('visibility' => $newVisibility);


        $success = $modelContractorDiscussionMongo->updateById($discussion_id, $data);


        //echo $discussion_id;
        echo $success;
        exit;
    }

}
