<?php

class Settings_UserInfoController extends Zend_Controller_Action {

    private $request;
    private $router;
    private $user_id;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
        $this->user_id = $this->request->getParam('user_id');

        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsUser';

        $Model_User = new Model_User();
        $User = $Model_User->getById($this->user_id);

        BreadCrumbs::setLevel(3, $User['username'] . ' info');
        $this->view->page_title = $User['username'].' - User Settings';
    }

    /**
     * Items list action
     */
    public function indexAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsUserInfoList'));

        //
        // get data list
        //
        $userInfoObj = new Model_UserInfo();
        $userInfo = $userInfoObj->getByUserId($this->user_id);

        if (!CheckAuth::checkIfCanHandelAllCompany('user', $this->user_id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $userObj = new Model_User();
        $user = $userObj->getById($this->user_id);


        $this->view->userInfo = $userInfo;
        $this->view->user_id = $this->user_id;
        $this->view->user = $user;
    }

    /**
     * Add new item action
     */
    public function addAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsUserInfoAdd'));

        $userInfoObj = new Model_UserInfo();
        $userInfo = $userInfoObj->getByUserId($this->user_id);

        if (!CheckAuth::checkIfCanHandelAllCompany('user', $this->user_id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        if ($userInfo) {
            $this->_redirect($this->router->assemble(array('user_id' => $this->user_id, 'id' => $userInfo['user_info_id']), 'settingsUserInfoEdit'));
        }
        //
        // get request parameters
        //
        $firstName = $this->request->getParam('first_name');
        $lastName = $this->request->getParam('last_name');


        $router = Zend_Controller_Front::getInstance()->getRouter();

        //
        // init action form
        //
        $form = new Settings_Form_UserInfo(array('user_id' => $this->user_id));

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $userInfoObj = new Model_UserInfo();
                $data = array(
                    'first_name' => $firstName,
                    'last_name' => $lastName,
                    'user_id' => $this->user_id
                );

                $success = $userInfoObj->insert($data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                }
                //$this->_redirect($this->router->assemble(array(), 'settingsUserEmailList'));

                echo 1;
                exit;
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        
        echo $this->view->render('user-info/add_edit.phtml');
        exit;
    }

    public function deleteAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsUserInfoDelete'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
        if ($id) {
            $ids[] = $id;
        }

        $userInfoObj = new Model_UserInfo();
        foreach ($ids as $id) {
            $userInfoObj->deleteById($id);
        }
        $this->_redirect($this->router->assemble(array('user_id' => $this->user_id), 'settingsUserInfoList'));
    }

    public function editAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsUserInfoEdit'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id');
        $firstName = $this->request->getParam('first_name');
        $lastName = $this->request->getParam('last_name');


        //
        // validation
        //
        $userInfoObj = new Model_UserInfo();
        $userInfo = $userInfoObj->getById($id);
        if (!$userInfo) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect($this->router->assemble(array(), 'settingsUserInfoList'));
            return;
        }


        //
        // init action form
        //
        $form = new Settings_Form_UserInfo(array('mode' => 'update', 'userInfo' => $userInfo, 'user_id' => $this->user_id));


        //
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'first_name' => $firstName,
                    'last_name' => $lastName,
                );

                $success = $userInfoObj->updateById($id, $data);
                //$this->view->successMessage = 'Updated Successfully';
                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                }

                echo 1;
                exit;
                //$this->_redirect($this->router->assemble(array(), 'settingsUserInfoList'));
                //return;
            }
        }

        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('user-info/add_edit.phtml');
        exit;
    }

}

