<?php

class Settings_ImageAttachmentController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsServices';

        BreadCrumbs::setLevel(2, 'Image Attachment');
    }

    /**
     * Items list action
     */
    public function indexAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsImageAttachmentList'));

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        //
        // get data list
        //
        $modelImageAttachment = new Model_ImageAttachment();
        $this->view->data = $modelImageAttachment->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);


        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
    }

    /**
     * Add new item action
     */
    public function addAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsImageAttachmentAdd'));

        //
        // get request parameters
        //
        $description = $this->request->getParam('description');
        $service_id = $this->request->getParam('service_id', 0);
        $attribute_list_value_id = $this->request->getParam('attribute_list_value_id', 0);
        $company_id = CheckAuth::getCompanySession();

        //
        // init action form
        //
        $form = new Settings_Form_ImageAttachment();
        $this->view->form = $form;

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                if (!$form->image->receive()) {
                    $this->view->message = '<div class="errors">Errors Receiving File.</div>';
                    echo $this->view->render('image-attachment/add_edit.phtml');
                    exit;
                }

                if ($form->image->isUploaded()) {
                    $source = $form->image->getFileName();
                    $imageInfo = pathinfo($source);
                    $ext = $imageInfo['extension'];

                    //to re-name the image, all you need to do is save it with a new name, instead of the name they uploaded it with. Normally, I use the primary key of the database row where I'm storing the name of the image. For example, if it's an image of Person 1, I call it 1.jpg. The important thing is that you make sure the image name will be unique in whatever directory you save it to.
                    //get sub dir
                    $dir = get_config('image_attachment') . '/';
                    $subdir = date('Y/m/d/');

                    //check if image exists or not
                    $fullDir = $dir . $subdir;

                    if (!is_dir($fullDir)) {
                        mkdir($fullDir, 0777, true);
                    }

                    $data = array(
                        'description' => $description,
                        'service_id' => $service_id,
                        'attribute_list_value_id' => $attribute_list_value_id,
                        'company_id' => $company_id
                    );

                    $modelImageAttachment = new Model_ImageAttachment();
                    $id = $modelImageAttachment->insert($data);

                    $original_path = "original_{$id}.{$ext}";
                    $large_path = "large_{$id}.{$ext}";
                    $small_path = "small_{$id}.{$ext}";
                    $thumbnail_path = "thumbnail_{$id}.{$ext}";

                    $data = array(
                        'original_path' => $subdir . $original_path,
                        'large_path' => $subdir . $large_path,
                        'small_path' => $subdir . $small_path,
                        'thumbnail_path' => $subdir . $thumbnail_path
                    );

                    $modelImageAttachment->updateById($id, $data);

                    //save image to database and filesystem here
                    $image_saved = copy($source, $fullDir . $original_path);
                    ImageMagick::scale_image($source, $fullDir . $large_path, 510);
                    ImageMagick::scale_image($source, $fullDir . $small_path, 250);
                    ImageMagick::create_thumbnail($source, $fullDir . $thumbnail_path, 78, 64);
                    
                    if ($image_saved) {

                        if (file_exists($source)) {
                            unlink($source);
                        }

                        $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved"));

                        echo 1;
                        exit;
                    }
                }
            }
        }

        //
        // render views
        //
        echo $this->view->render('image-attachment/add_edit.phtml');
        exit;
    }

    public function deleteAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsImageAttachmentDelete'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());


        if ($id) {
            $ids[] = $id;
        }

        $modelImageAttachment = new Model_ImageAttachment();

        foreach ($ids as $id) {
            $imageAttachment = $modelImageAttachment->getById($id);
            if ($imageAttachment) {

                if (file_exists(get_config('image_attachment') . '/' . $imageAttachment['original_path'])) {
                    unlink(get_config('image_attachment') . '/' . $imageAttachment['original_path']);
                }
                if (file_exists(get_config('image_attachment') . '/' . $imageAttachment['large_path'])) {
                    unlink(get_config('image_attachment') . '/' . $imageAttachment['large_path']);
                }
                if (file_exists(get_config('image_attachment') . '/' . $imageAttachment['small_path'])) {
                    unlink(get_config('image_attachment') . '/' . $imageAttachment['small_path']);
                }
                if (file_exists(get_config('image_attachment') . '/' . $imageAttachment['thumbnail_path'])) {
                    unlink(get_config('image_attachment') . '/' . $imageAttachment['thumbnail_path']);
                }

                $modelImageAttachment->deleteById($id);
            }
        }

        $this->_redirect($this->router->assemble(array(), 'settingsImageAttachmentList'));
    }

    public function editAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsImageAttachmentEdit'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id');
        $description = $this->request->getParam('description');
        $service_id = $this->request->getParam('service_id', 0);
        $attribute_list_value_id = $this->request->getParam('attribute_list_value_id', 0);
        $change_image = $this->request->getParam('change_image', 0);

        //
        // validation
        //
        $modelImageAttachment = new Model_ImageAttachment();
        $imageAttachment = $modelImageAttachment->getById($id);
        if (!$imageAttachment) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect($this->router->assemble(array(), 'settingsImageAttachmentList'));
            return;
        }

        if (file_exists(get_config('image_attachment') . '/' . $imageAttachment['thumbnail_path'])) {
            $this->view->image = $imageAttachment['thumbnail_path'];
        }

        //
        // init action form
        //
        $form = new Settings_Form_ImageAttachment(array('mode' => 'update', 'imageAttachment' => $imageAttachment));
        $this->view->form = $form;


        //
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                if ($change_image) {
                    if (!$form->image->receive()) {
                        $this->view->message = '<div class="errors">Errors Receiving File.</div>';
                        echo $this->view->render('image-attachment/add_edit.phtml');
                        exit;
                    }

                    if ($form->image->isUploaded()) {
                        $source = $form->image->getFileName();
                        $imageInfo = pathinfo($source);
                        $ext = $imageInfo['extension'];

                        //to re-name the image, all you need to do is save it with a new name, instead of the name they uploaded it with. Normally, I use the primary key of the database row where I'm storing the name of the image. For example, if it's an image of Person 1, I call it 1.jpg. The important thing is that you make sure the image name will be unique in whatever directory you save it to.
                        //get sub dir
                        $dir = get_config('image_attachment') . '/';
                        $subdir = date('Y/m/d/');

                        //check if image exists or not
                        $fullDir = $dir . $subdir;

                        if (!is_dir($fullDir)) {
                            mkdir($fullDir, 0777, true);
                        }

                        $original_path = "original_{$id}.{$ext}";
                        $large_path = "large_{$id}.{$ext}";
                        $small_path = "small_{$id}.{$ext}";
                        $thumbnail_path = "thumbnail_{$id}.{$ext}";

                        $data = array(
                            'service_id' => $service_id,
                            'attribute_list_value_id' => $attribute_list_value_id,
                            'description' => $description,
                            'original_path' => $subdir . $original_path,
                            'large_path' => $subdir . $large_path,
                            'small_path' => $subdir . $small_path,
                            'thumbnail_path' => $subdir . $thumbnail_path
                        );

                        $modelImageAttachment = new Model_ImageAttachment();
                        $modelImageAttachment->updateById($id, $data);

                        //save image to database and filesystem here
                        $image_saved = copy($source, $fullDir . $original_path);
                        ImageMagick::scale_image($source, $fullDir . $large_path, 510);
                        ImageMagick::scale_image($source, $fullDir . $small_path, 250);
                        ImageMagick::create_thumbnail($source, $fullDir . $thumbnail_path, 78, 64);

                        if ($image_saved) {

                            if (file_exists($source)) {
                                unlink($source);
                            }

                            $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved"));

                            echo 1;
                            exit;
                        }
                    }
                } else {

                    $data = array(
                        'service_id' => $service_id,
                        'attribute_list_value_id' => $attribute_list_value_id,
                        'description' => $description
                    );

                    $modelImageAttachment = new Model_ImageAttachment();
                    $modelImageAttachment->updateById($id, $data);

                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved"));

                    echo 1;
                    exit;
                }
            }
        }


        //
        // render views
        //
        echo $this->view->render('image-attachment/add_edit.phtml');
        exit;
    }

    public function descriptionAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsImageAttachmentDescription'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id');

        //
        // validation
        //
        $modelImageAttachment = new Model_ImageAttachment();
        $imageAttachment = $modelImageAttachment->getById($id);

        if (!$imageAttachment) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect($this->router->assemble(array(), 'settingsImageAttachmentList'));
            return;
        }

        $this->view->imageAttachment = $imageAttachment;

        //
        // render views
        //
        echo $this->view->render('image-attachment/description.phtml');
        exit;
    }

}

