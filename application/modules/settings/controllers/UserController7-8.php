<?php

class Settings_UserController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsUser';
        BreadCrumbs::setLevel(2, 'Users');
    }

    /**
     * Items list action
     */
    public function indexAction() {


        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsUserList'));

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'user_id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        //
        // get data list
        //
        $modelUser = new Model_User();
        $this->view->data = $modelUser->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);

        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
    }

    /**
     * Add new item action
     */
    public function goUpgradeAction() {

        echo $this->view->render('user/go-upgrade.phtml');
        exit;
    }

    public function checkAddPayAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsUserAdd'));

        //
        // get request parameters
        //
        
        if (CheckAuth::getRoleName() != 'super_admin') {

            $userr = CheckAuth::getLoggedUser();
            $accountModel = new Model_Account();
            $userCompaniesModel = new Model_UserCompanies();
            $userModel = new Model_User();
            $userAdmin = $userModel->getById($userr['user_id']);
            $account = $accountModel->getByCreatedBy($userr['user_id']);

            // echo "mm" ;exit;

            if ($account) {

                $planModel = new Model_Plan();
                $Plan = $planModel->getByID($account['plan_id']);
                ////// get all users with this account /////////////////////////////////
                $users = $userCompaniesModel->getUsersByCompanyId($account['company_id']);

                if (count($users) == $Plan['max_users']) {
                    $return['success'] = 'false_1';
                    // $return['url'] = $this->router->assemble(array(), 'calculateRealAccountSubscription');
                    echo json_encode($return);
                    exit;
                }

                /// check for status of account  if not trial will check
                // how many users have already and how many users have in users account


                if ($account['account_status'] == 'Subscriber-paid' and ( count($users) < $account['paid_user_count'])) {

                    $return['success'] = false;
                    echo json_encode($return);

                    exit;
                } else if ($account['account_status'] == 'Overdue') {
                    echo 'Your Account Status is Overdue , You can not add users now';
                    exit;
                } else {

                    $return['success'] = true;
                    $return['url'] = $this->router->assemble(array(), 'calculateRealAccountSubscription');
                    echo json_encode($return);
                    exit;
                }
            }
        }
        exit;

        //   $this->_redirect($this->router->assemble(array(), 'calculateRealAccountSubscription'));
    }

    /**
     * Add new item action
     */
    /* public function addAction() {

      //
      // check Auth for logged user
      //
      CheckAuth::checkPermission(array('settingsUserAdd'));

      //
      // get request parameters
      //
      $username = $this->request->getParam('username');
      $first_name = $this->request->getParam('first_name');
      $last_name = $this->request->getParam('last_name');
      $display_name = $this->request->getParam('display_name');
      $password = $this->request->getParam('password');
      $roleId = $this->request->getParam('role_id');
      $email1 = $this->request->getParam('email1');
      $email2 = $this->request->getParam('email2');
      $email3 = $this->request->getParam('email3');
      $systemEmail = $this->request->getParam('systemEmail');
      $mobile1 = $this->request->getParam('mobile1');
      $mobile2 = $this->request->getParam('mobile2');
      $mobile3 = $this->request->getParam('mobile3');
      $phone1 = $this->request->getParam('phone1');
      $phone2 = $this->request->getParam('phone2');
      $phone3 = $this->request->getParam('phone3');

      $internation_key = $this->request->getParam('international_key');
      $mobile_key = $this->request->getParam('mobile_key');
      $phone_key = $this->request->getParam('phone_key');

      $fax = $this->request->getParam('fax');
      $emergencyPhone = $this->request->getParam('emergency_phone');
      $unitLotNumber = $this->request->getParam('unit_lot_number');
      $streetNumber = $this->request->getParam('street_number');
      $streetAddress = $this->request->getParam('street_address');
      $suburb = $this->request->getParam('suburb');
      $state = $this->request->getParam('state');
      $postcode = $this->request->getParam('postcode');
      $po_box = $this->request->getParam('po_box');
      $cityId = $this->request->getParam('city_id');
      $countryId = $this->request->getParam('country_id');
      $companyId = $this->request->getParam('company_id');


      /********Check if user put phone or mobile Number? and put full format***********IBM/
      if(isset($mobile1) && $mobile1 != ""){
      $mobile1_key = substr($mobile_key, 1, 1);
      $mobile1 = $internation_key . $mobile1_key . $mobile1;
      }
      if(isset($mobile2) && $mobile2 != ""){
      $mobile2_key = substr($mobile_key, 1, 1);
      $mobile2 = $internation_key . $mobile2_key . $mobile2;
      }
      if(isset($mobile3) && $mobile3 != ""){
      $mobile3_key = substr($mobile_key, 1, 1);
      $mobile3 = $internation_key . $mobile3_key . $mobile3;
      }
      if(isset($phone1) && $phone1 != ""){
      $phone1_key = substr($phone_key, 1, 1);
      $phone1 = $internation_key . $phone1_key . $phone1;

      }
      if(isset($phone2) && $phone2 != ""){
      $phone2_key = substr($phone_key, 1, 1);
      $phone2 = $internation_key . $phone2_key . $phone2;
      }
      if(isset($phone3) && $phone3 != ""){
      $phone3_key = substr($phone_key, 1, 1);
      $phone3 = $internation_key . $phone3_key . $phone3;
      }

      /*******End***********


      if (get_config('remove_white_spacing')) {
      $mobile1 = preparer_number($mobile1);
      $mobile2 = preparer_number($mobile2);
      $mobile3 = preparer_number($mobile3);
      $phone1 = preparer_number($phone1);
      $phone2 = preparer_number($phone2);
      $phone3 = preparer_number($phone3);
      }

      $router = Zend_Controller_Front::getInstance()->getRouter();

      if (isset($roleId)) {
      $modelAuthRole = new Model_AuthRole();
      $role = $modelAuthRole->getById($roleId);
      if ($role['role_name'] == 'super_admin' && !CheckAuth::checkCredential(array('canHandleAllCompanies'))) {
      $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You have entered an invalid option"));
      echo 1;
      exit;
      }
      }
      //
      // init action form
      //
      $form = new Settings_Form_User(array('country_id' => $countryId, 'state' => $state));

      //
      // handling the insertion process
      //
      if ($this->request->isPost()) {
      // // check if POST request method
      if ($form->isValid($this->request->getPost())) { // validate form data
      $modelUser = new Model_User();
      $data = array(
      'display_name' => $display_name,
      'first_name' => $first_name,
      'last_name' => $last_name,
      'username' => $username,
      'password' => sha1($password),
      'user_code' => sha1($username),
      'last_login' => time(),
      'created' => time(),
      'city_id' => $cityId,
      'role_id' => $roleId,
      'email1' => $email1,
      'email2' => $email2,
      'email3' => $email3,
      'system_email' => $systemEmail,
      'mobile1' => $mobile1,
      'mobile2' => $mobile2,
      'mobile3' => $mobile3,
      'phone1' => $phone1,
      'phone2' => $phone2,
      'phone3' => $phone3,
      'fax' => $fax,
      'emergency_phone' => $emergencyPhone,
      'unit_lot_number' => $unitLotNumber,
      'street_number' => $streetNumber,
      'street_address' => $streetAddress,
      'suburb' => $suburb,
      'state' => $state,
      'postcode' => $postcode,
      'po_box' => $po_box
      );

      //
      //get role id for contractor
      //
      $userId = $modelUser->insert($data);

      //if the user role is contractor then add new record in contractor info
      if ($role['role_name'] == 'contractor'){
      $data = array(
      'contractor_id' => $userId
      );
      $modelContractorInfo = new Model_ContractorInfo();
      $success = $modelContractorInfo->insert($data);

      }
      //


      $dataCompany = array(
      'user_id' => $userId,
      'company_id' => $companyId,
      'created' => time()
      );
      $userCompaniesModel = new Model_UserCompanies();
      $userCompaniesModel->insert($dataCompany);

      if ($userId) {
      $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved"));
      } else {
      $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
      }

      echo 1;
      exit;
      }
      }

      $this->view->form = $form;

      //
      // render views
      //
      echo $this->view->render('user/add_edit.phtml');
      exit;
      } */

    public function addAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsUserAdd'));


        $userCompaniesModel = new Model_UserCompanies();
        $userModel = new Model_User();
        $modelAuthRole = new Model_AuthRole();


        /* created by mohammed mkheamar */
        /* this for to check if he can add user normally or will pay for it  */
        $username = $this->request->getParam('username');
        $display_name = $this->request->getParam('display_name');
        $password = $this->request->getParam('password');
        $roleId = $this->request->getParam('role_id');
        $first_name = $this->request->getParam('first_name');
        $last_name = $this->request->getParam('last_name');
        $email1 = $this->request->getParam('email1');
        $email2 = $this->request->getParam('email2');
        $email3 = $this->request->getParam('email3');
        $systemEmail = $this->request->getParam('systemEmail');

        $internation_key = $this->request->getParam('international_key');
        $mobile_key = $this->request->getParam('mobile_key');
        $phone_key = $this->request->getParam('phone_key');

        $mobile1 = $this->request->getParam('mobile1');
        $mobile2 = $this->request->getParam('mobile2');
        $mobile3 = $this->request->getParam('mobile3');
        $phone1 = $this->request->getParam('phone1');
        $phone2 = $this->request->getParam('phone2');
        $phone3 = $this->request->getParam('phone3');
        $fax = $this->request->getParam('fax');
        $emergencyPhone = $this->request->getParam('emergency_phone');
        $unitLotNumber = $this->request->getParam('unit_lot_number');
        $streetNumber = $this->request->getParam('street_number');
        $streetAddress = $this->request->getParam('street_address');
        $suburb = $this->request->getParam('suburb');
        $state = $this->request->getParam('state');
        $postcode = $this->request->getParam('postcode');
        $po_box = $this->request->getParam('po_box');
        $cityId = $this->request->getParam('city_id');
        $countryId = $this->request->getParam('country_id');
        $companyId = $this->request->getParam('company_id');


        /*         * ******Check if user put phone or mobile Number? and put full format***********IBM */
        if (isset($mobile1) && $mobile1 != "") {
            $mobile1_key = substr($mobile_key, 1, 1);
            $mobile1 = $internation_key . $mobile1_key . $mobile1;
        }
        if (isset($mobile2) && $mobile2 != "") {
            $mobile2_key = substr($mobile_key, 1, 1);
            $mobile2 = $internation_key . $mobile2_key . $mobile2;
        }
        if (isset($mobile3) && $mobile3 != "") {
            $mobile3_key = substr($mobile_key, 1, 1);
            $mobile3 = $internation_key . $mobile3_key . $mobile3;
        }
        if (isset($phone1) && $phone1 != "") {
            $phone1_key = substr($phone_key, 1, 1);
            $phone1 = $internation_key . $phone1_key . $phone1;
        }
        if (isset($phone2) && $phone2 != "") {
            $phone2_key = substr($phone_key, 1, 1);
            $phone2 = $internation_key . $phone2_key . $phone2;
        }
        if (isset($phone3) && $phone3 != "") {
            $phone3_key = substr($phone_key, 1, 1);
            $phone3 = $internation_key . $phone3_key . $phone3;
        }

        /*         * *****End********** */

        if (get_config('remove_white_spacing')) {
            $mobile1 = preparer_number($mobile1);
            $mobile2 = preparer_number($mobile2);
            $mobile3 = preparer_number($mobile3);
            $phone1 = preparer_number($phone1);
            $phone2 = preparer_number($phone2);
            $phone3 = preparer_number($phone3);
        }

        $router = Zend_Controller_Front::getInstance()->getRouter();

        if (isset($roleId)) {

            $role = $modelAuthRole->getById($roleId);
            if ($role['role_name'] == 'super_admin' && !CheckAuth::checkCredential(array('canHandleAllCompanies'))) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You have entered a bad option !!"));
                echo 1;
                exit;
            }
        }

        //
        // init action form
        //
        $form = new Settings_Form_User(array('country_id' => $countryId, 'state' => $state)); //,'phone_key'=> $phone_key

        if (isset($roleId)) {

            $role = $modelAuthRole->getById($roleId);
            if (!($role['role_name'] == 'contractor')) {
                $form->getElement('first_name')->setRequired(false);
                $form->getElement('last_name')->setRequired(false);
            }
        }

        //check if user is OCTUP


        if (CheckAuth::getRoleName() == 'account_admin') {

            $userr = CheckAuth::getLoggedUser();
            $accountModel = new Model_Account();
            $planModel = new Model_Plan();
            $SubscriptionPaymentObj = new Model_SubscriptionPayment();

            $account = $accountModel->getByCreatedBy($userr['user_id']);
            $Plan = $planModel->getByID($account['plan_id']);
            $users = $userCompaniesModel->getUsersByCompanyId($account['company_id']);
            $paymentResult = $SubscriptionPaymentObj->calculateTotalAmountAddUser($account['to'], $Plan['charge_amount']);
            //if user in paid mode or suspend mode.
            if ($account && (strtotime($account['trial_end_date']) < time())) {
                //the user didn't add his billing information yet.
                if ($account['customer_token_id'] == null) {
                    $this->view->flagName = 'add_billing';
                    echo $this->view->render('user/information-messege.phtml');
                    exit;
                }
            }
            //the user reatch his max allowed user in the actual plan.
            if (count($users) >= $Plan['max_users']) {
                $this->view->endTrialDate = $account['trial_end_date'];
                $this->view->flagName = 'upgrade_plan';
                echo $this->view->render('user/information-messege.phtml');
                exit;
            }
        }

        $isApproved = $this->request->getParam('isApproved', 'no');
        $this->view->isApproved = $isApproved;



        if ($this->request->isPost()) {

            if ($isApproved == 'yes') {

                $invoicemodelObj = new Model_InvoiceSubscription();
                $checkData = $invoicemodelObj->checkInvoiceExistInCurrentPeriod($account['id']);

                //1-check invoice----------------------------------------------------------
                if ($checkData['exist'] == 'no') {
                    $invoiceData = array(
                        'invoice_num' => $checkData['invoice_num'],
                        'account_id' => $account['id'],
                        'created_by' => $account['created_by'],
                        'status' => 'pending',
                        'from' => $account['from'],
                        'to' => $account['to']
                    );
                    $invoicemodelObj->insert($invoiceData);
                }
                //2-make transaction-------------------------------------------------------
                $response = $SubscriptionPaymentObj->makeTransaction($account['customer_token_id'], $paymentResult['amount'], $checkData['invoice_num']);
                if ($response->getErrors()) {
                    foreach ($response->getErrors() as $error) {
                        $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Error: " . \Eway\Rapid::getMessage($error)));
                        $this->_redirect($this->router->assemble(array(), 'settingsUserList'));
                    }
                    exit;
                } else {// transaction success........
                    //3-payment-------------------------------------------------------------
                    $paymentData = array(
                        'account_id' => $account['id'],
                        'created_by' => $account['created_by'],
                        'positive_amount' => 0.00,
                        'amount' => $paymentResult['amount'],
                        'from' => date("Y-m-d", time()),
                        'to' => $account['to'],
                        'transaction_id' => $response->TransactionID,
                        'description' => 'Add User',
                        'period' => $paymentResult['period'],
                        'new_plan_id' => null,
                        'invoice_num' => $checkData['invoice_num']
                    );
                    //4-update invoice table------------------------------------------------
                    $updateInvoiceData = array(
                        'amount' => $paymentResult['amount']
                    );

                    $updateInvoice = $invoicemodelObj->updateByInvoiceNum($checkData['invoice_num'], $updateInvoiceData);
                    if (!$updateInvoice) {
                        $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "no update in table invoice"));
                        $this->_redirect($this->router->assemble(array(), 'settingsUserList'));
                        exit;
                    } else {
                        $paymentInsert = $SubscriptionPaymentObj->insert($paymentData);
                        if ($paymentInsert) {
                            //add user record here...........                                                        
                            $userDataReverseKeys = $this->request->getParam('userDatakeys', '');
                            $userDataReverseValues = $this->request->getParam('userDatavalue', '');
                            $arrayUserKeys = explode(",", $userDataReverseKeys);
                            $arrayUserValues = explode(",", $userDataReverseValues);

                            $userId = $userModel->insert(array_combine($arrayUserKeys, $arrayUserValues));
                            $dataCompany = array(
                                'user_id' => $userId,
                                'company_id' => $account['company_id'],
                                'created' => time()
                            );

                            $userCompaniesModel->insert($dataCompany);

                            if ($userId) {
                                $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved payment,user successfully"));
                                $this->_redirect($this->router->assemble(array(), 'settingsUserList'));
                            } else {
                                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in User"));
                                $this->_redirect($this->router->assemble(array(), 'settingsUserList'));
                                exit;
                            }
                        }
                    }
                }

                exit;
            }
            // // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $dataUser = array(
                    'display_name' => $display_name,
                    'username' => $username,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'password' => sha1($password),
                    'user_code' => sha1($username),
                    'last_login' => time(),
                    'created' => time(),
                    'city_id' => $cityId,
                    'role_id' => $roleId,
                    'email1' => $email1,
                    'email2' => $email2,
                    'email3' => $email3,
                    'system_email' => $systemEmail,
                    'mobile1' => $mobile1,
                    'mobile2' => $mobile2,
                    'mobile3' => $mobile3,
                    'phone1' => $phone1,
                    'phone2' => $phone2,
                    'phone3' => $phone3,
                    'fax' => $fax,
                    'emergency_phone' => $emergencyPhone,
                    'unit_lot_number' => $unitLotNumber,
                    'street_number' => $streetNumber,
                    'street_address' => $streetAddress,
                    'suburb' => $suburb,
                    'state' => $state,
                    'postcode' => $postcode,
                    'po_box' => $po_box
                );
                //   echo '<script type="text/javascript">alert("Data has been submitted to ' . $phone1_key . '");</script>';
                // echo '<script type="text/javascript">alert("Data has been submitted to ' . $data . '");</script>';
                // var_dump($phone_key) ; exit;
                //
            
                
                //**************start send payment data that will pay to popup**********************
                if ($isApproved == 'no' && CheckAuth::getRoleName() == 'account_admin' && (strtotime($account['trial_end_date']) <= time())) {

                    if (!empty($paymentResult)) {
                        $this->view->planName = $Plan['name'];
                        $this->view->chargeAmount = $Plan['charge_amount'];

                        $this->view->paymentData = $paymentResult;
                        $this->view->userData = $dataUser;
                        $this->view->flagName = 'approve_payment';
                        echo $this->view->render('user/information-messege.phtml');
                        exit;
                    } else {
                        $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "There are problem in calculating the payment amount"));
                        exit;
                    }
                }
                //**************end sending payment data that will pay to popup**********************
                $userId = $userModel->insert($dataUser);
                $dataCompany = array(
                    'user_id' => $userId,
                    'company_id' => $companyId,
                    'created' => time()
                );

                $userCompaniesModel->insert($dataCompany);

                if ($userId) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in User"));
                }

                echo 1;
                exit;
            }
        }

        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('user/add_edit.phtml');
        exit;
    }

    public function deleteAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsUserDelete'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
        if ($id) {
            $ids[] = $id;
        }

        $modelUser = new Model_User();
        foreach ($ids as $id) {
            if (CheckAuth::checkIfCanHandelAllCompany('user', $id)) {
                $isChecked = $modelUser->checkBeforeDeleteUser($id);
                if ($isChecked) {
                    $modelUser->deleteById($id);
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Error: Failed to delete, item in use"));
                }
            }
        }
        $this->_redirect($this->router->assemble(array(), 'settingsUserList'));
    }

    public function editAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsUserEdit'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id');
        $display_name = $this->request->getParam('display_name');
        $first_name = $this->request->getParam('first_name');
        $last_name = $this->request->getParam('last_name');
        $username = $this->request->getParam('username');
        //$password = $this->request->getParam('password');
        $roleId = $this->request->getParam('role_id');
        $email1 = $this->request->getParam('email1');
        $email2 = $this->request->getParam('email2');
        $email3 = $this->request->getParam('email3');
        $systemEmail = $this->request->getParam('systemEmail');
        $mobile1 = $this->request->getParam('mobile1');
        $mobile2 = $this->request->getParam('mobile2');
        $mobile3 = $this->request->getParam('mobile3');
        $phone1 = $this->request->getParam('phone1');
        $phone2 = $this->request->getParam('phone2');
        $phone3 = $this->request->getParam('phone3');
        $internation_key = $this->request->getParam('international_key');
        $mobile_key = $this->request->getParam('mobile_key');
        $phone_key = $this->request->getParam('phone_key');

        $fax = $this->request->getParam('fax');
        $emergencyPhone = $this->request->getParam('emergency_phone');
        $unitLotNumber = $this->request->getParam('unit_lot_number');
        $streetNumber = $this->request->getParam('street_number');
        $streetAddress = $this->request->getParam('street_address');
        $suburb = $this->request->getParam('suburb');
        $state = $this->request->getParam('state');
        $postcode = $this->request->getParam('postcode');
        $po_box = $this->request->getParam('po_box');
        $cityId = $this->request->getParam('city_id');
        $countryId = $this->request->getParam('country_id');
        $companyId = $this->request->getParam('company_id');

        /*         * ******Check if user put phone or mobile Number? and put full format***********IBM */
        if (isset($mobile1) && $mobile1 != "") {
            $mobile1_key = substr($mobile_key, 1, 1);
            $mobile1 = $internation_key . $mobile1_key . $mobile1;
        }
        if (isset($mobile2) && $mobile2 != "") {
            $mobile2_key = substr($mobile_key, 1, 1);
            $mobile2 = $internation_key . $mobile2_key . $mobile2;
        }
        if (isset($mobile3) && $mobile3 != "") {
            $mobile3_key = substr($mobile_key, 1, 1);
            $mobile3 = $internation_key . $mobile3_key . $mobile3;
        }
        if (isset($phone1) && $phone1 != "") {
            $phone1_key = substr($phone_key, 1, 1);
            $phone1 = $internation_key . $phone1_key . $phone1;
        }
        if (isset($phone2) && $phone2 != "") {
            $phone2_key = substr($phone_key, 1, 1);
            $phone2 = $internation_key . $phone2_key . $phone2;
        }
        if (isset($phone3) && $phone3 != "") {
            $phone3_key = substr($phone_key, 1, 1);
            $phone3 = $internation_key . $phone3_key . $phone3;
        }

        /*         * *****End********** */


        if (get_config('remove_white_spacing')) {
            $mobile1 = preparer_number($mobile1);
            $mobile2 = preparer_number($mobile2);
            $mobile3 = preparer_number($mobile3);
            $phone1 = preparer_number($phone1);
            $phone2 = preparer_number($phone2);
            $phone3 = preparer_number($phone3);
        }

        if (isset($roleId)) {
            $modelAuthRole = new Model_AuthRole();
            $role = $modelAuthRole->getById($roleId);
            if ($role['role_name'] == 'super_admin' && !CheckAuth::checkCredential(array('canHandleAllCompanies'))) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You have entered an invalid option"));
                echo 1;
                exit;
            }
        }
        if (!CheckAuth::checkIfCanHandelAllCompany('user', $id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            echo 1;
            exit;
        }

        //
        // validation
        //
        $modelUser = new Model_User();
        $user = $modelUser->getById($id);
        if (!$user) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));

            $this->_redirect($this->router->assemble(array(), 'settingsUserList'));
            return;
        }

        //
        //get user company
        //
        $userCompaniesModel = new Model_UserCompanies();
        $userCompanies = $userCompaniesModel->getByUserId($id);


        //
        // init action form
        //
        $form = new Settings_Form_User(array('mode' => 'update', 'user' => $user, 'country_id' => $countryId, 'state' => $state, 'company_id' => $userCompanies['company_id']));


        //
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'display_name' => $display_name,
                    'username' => $username,
                    'city_id' => $cityId,
                    'role_id' => $roleId,
                    'email1' => $email1,
                    'email2' => $email2,
                    'email3' => $email3,
                    'system_email' => $systemEmail,
                    'mobile1' => $mobile1,
                    'mobile2' => $mobile2,
                    'mobile3' => $mobile3,
                    'phone1' => $phone1,
                    'phone2' => $phone2,
                    'phone3' => $phone3,
                    'fax' => $fax,
                    'emergency_phone' => $emergencyPhone,
                    'unit_lot_number' => $unitLotNumber,
                    'street_number' => $streetNumber,
                    'street_address' => $streetAddress,
                    'suburb' => $suburb,
                    'state' => $state,
                    'postcode' => $postcode,
                    'po_box' => $po_box
                );

                $success = $modelUser->updateById($id, $data);

                $userCompaniesModel = new Model_UserCompanies();
                $userCompanies = $userCompaniesModel->getByUserId($id);

                $dataCompany = array(
                    'user_id' => $id,
                    'company_id' => $companyId,
                    'created' => time()
                );
                if ($userCompanies) {
                    $userCompaniesModel->updateById($userCompanies['id'], $dataCompany);
                } else {
                    $userCompaniesModel->insert($dataCompany);
                }

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                }

                echo 1;
                exit;
            }
        }

        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('user/add_edit.phtml');
        exit;
    }

    public function editContractorAddressAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsUserEdit'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id');
        $display_name = $this->request->getParam('display_name');
        $username = $this->request->getParam('username');
        //$password = $this->request->getParam('password');
        //$roleId = $this->request->getParam('role_id');
        //echo $roleId ;
        //exit;
        $email1 = $this->request->getParam('email1');
        $email2 = $this->request->getParam('email2');
        $email3 = $this->request->getParam('email3');
        $systemEmail = $this->request->getParam('systemEmail');
        $mobile1 = $this->request->getParam('mobile1');
        $mobile2 = $this->request->getParam('mobile2');
        $mobile3 = $this->request->getParam('mobile3');
        $phone1 = $this->request->getParam('phone1');
        $phone2 = $this->request->getParam('phone2');
        $phone3 = $this->request->getParam('phone3');
        $fax = $this->request->getParam('fax');
        $emergencyPhone = $this->request->getParam('emergency_phone');
        $unitLotNumber = $this->request->getParam('unit_lot_number');
        $streetNumber = $this->request->getParam('street_number');
        $streetAddress = $this->request->getParam('street_address');
        $suburb = $this->request->getParam('suburb');
        $state = $this->request->getParam('state');
        $postcode = $this->request->getParam('postcode');
        $po_box = $this->request->getParam('po_box');
        $cityId = $this->request->getParam('city_id');
        $countryId = $this->request->getParam('country_id');
        //$companyId = $this->request->getParam('company_id');

        if (get_config('remove_white_spacing')) {
            $mobile1 = preparer_number($mobile1);
            $mobile2 = preparer_number($mobile2);
            $mobile3 = preparer_number($mobile3);
            $phone1 = preparer_number($phone1);
            $phone2 = preparer_number($phone2);
            $phone3 = preparer_number($phone3);
        }

        /* if (isset($roleId)) {
          $modelAuthRole = new Model_AuthRole();
          $role = $modelAuthRole->getById($roleId);
          if ($role['role_name'] == 'super_admin' && !CheckAuth::checkCredential(array('canHandleAllCompanies'))) {
          $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You have entered an invalid option"));
          echo 1;
          exit;
          }
          } */
        if (!CheckAuth::checkIfCanHandelAllCompany('user', $id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            echo 1;
            exit;
        }

        //
        // validation
        //
        $modelUser = new Model_User();
        $user = $modelUser->getById($id);

        if (!$user) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));

            $this->_redirect($this->router->assemble(array(), 'settingsUserList'));
            return;
        }

        //
        //get user company
        //
        $userCompaniesModel = new Model_UserCompanies();
        $userCompanies = $userCompaniesModel->getByUserId($id);


        //
        // init action form
        //
        $form = new Settings_Form_editContractorAddress(array('user' => $user, 'country_id' => $countryId, 'state' => $state));


        //
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'email1' => $email1,
                    'email2' => $email2,
                    'email3' => $email3,
                    'system_email' => $systemEmail,
                    'mobile1' => $mobile1,
                    'mobile2' => $mobile2,
                    'mobile3' => $mobile3,
                    'phone1' => $phone1,
                    'phone2' => $phone2,
                    'phone3' => $phone3,
                    'fax' => $fax,
                    'emergency_phone' => $emergencyPhone,
                    'unit_lot_number' => $unitLotNumber,
                    'street_number' => $streetNumber,
                    'street_address' => $streetAddress,
                    'suburb' => $suburb,
                    'state' => $state,
                    'postcode' => $postcode,
                    'city_id' => $cityId,
                    'po_box' => $po_box
                );

                $success = $modelUser->updateById($id, $data);

                /* $userCompaniesModel = new Model_UserCompanies();
                  $userCompanies = $userCompaniesModel->getByUserId($id);

                  $dataCompany = array(
                  'user_id' => $id,
                  'company_id' => $companyId,
                  'created' => time()
                  );
                  if ($userCompanies) {
                  $userCompaniesModel->updateById($userCompanies['id'], $dataCompany);
                  } else {
                  $userCompaniesModel->insert($dataCompany);
                  } */

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                }

                echo 1;
                exit;
            }
        }
        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('user/edit_contractor_address.phtml');
        exit;
    }

    public function changePasswordAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settings'));


        $id = $this->request->getParam('id');
        $new_password = $this->request->getParam('new_password');
        $confirm_new_password = $this->request->getParam('confirm_new_password');

        if (!CheckAuth::checkIfCanHandelAllCompany('user', $id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            echo 1;
            exit;
        }


        $modelUser = new Model_User();
        $user = $modelUser->getById($id);
        if (!$user) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
            return;
        }


        $form = new Settings_Form_ChangePassword(array('user' => $user));
        $this->view->form = $form;

        if ($this->request->isPost()) {
            if ($form->isValid($this->request->getPost())) {

                $data = array(
                    'password' => sha1($new_password)
                );
                $modelUser->updateById($id, $data);

                echo 1;
                exit;
            }
        }

        //
        // render views
        //
        echo $this->view->render('user/change_password.phtml');
        exit;
    }

    public function updateGeneralContractorAction() {

        //update General Contractor
        $modelUser = new Model_User();
        $modelUser->updateGeneralContractor();
    }

    public function loginAsThisUserAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('loginAsThisUser'));

        $id = $this->request->getParam('id');

        $modelUser = new Model_User();
        $user = $modelUser->getById($id);
        /* if($_SERVER['REMOTE_ADDR'] == '176.106.46.142'){
          print_r($user );
          exit;
          } */
        $authrezed = $this->getAuthrezed();

        $authrezed->setIdentity($user['email1']);
        $authrezed->setCredential($user['password']);

        $auth = Zend_Auth::getInstance();
        $result = $auth->authenticate($authrezed);

        if ($result->isValid()) {
            $identity = $authrezed->getResultRowObject();

            $authStorge = $auth->getStorage();
            $authStorge->write($identity);

            CheckAuth::afterlogin();
        }

        $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
        $this->_redirect($this->router->assemble(array(), 'Login'));
    }

    public function getAuthrezed() {
        $authrezed = new Zend_Auth_Adapter_DbTable(Zend_Db_Table::getDefaultAdapter());
        $authrezed->setTableName('user')
                ->setIdentityColumn('email1')
                ->setCredentialColumn('password');
        //->setCredentialTreatment('? AND active = "TRUE"');

        return $authrezed;
    }

    public function activateOrDeactivateUsersAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('activateOrDeactivateUsers'));

        //
        //Convert complaint type status
        //
        $id = $this->request->getParam('id', 0);

        if (!CheckAuth::checkIfCanHandelAllCompany('user', $id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This user does not belong to your company"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $modelUser = new Model_User();
        $user = $modelUser->getById($id);

        if ($user['active'] == 'TRUE') {
            $data['active'] = 'FALSE';
        } else {
            $data['active'] = 'TRUE';
        }

        $modelUser->updateById($id, $data);


        $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
    }

    public function getCompanyInquiryEmailAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('activateOrDeactivateUsers'));

        //
        //Convert complaint type status
        //
        $company_id = $this->request->getParam('company_id', 0);

        /* if (!CheckAuth::checkIfCanHandelAllCompany('user', $id)) {
          $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This user does not belong to your company"));
          $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
          } */

        $companies_model = new Model_Companies();
        $companyEnquiriesEmail = $companies_model->getEnquiryEmailByCompanyId($company_id);


        echo json_encode($companyEnquiriesEmail);
        exit;
    }

    public function toggleActivateOrDeactivateUsersAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('activateOrDeactivateUsers'));

        //
        //Convert complaint type status
        //
        $id = $this->request->getParam('id', 0);

        if (!CheckAuth::checkIfCanHandelAllCompany('user', $id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This user does not belong to your company"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $modelUser = new Model_User();
        $user = $modelUser->getById($id);

        if ($user['active'] == 'TRUE') {
            $data['active'] = 'FALSE';
        } else {
            $data['active'] = 'TRUE';
        }

        $modelUser->updateById($id, $data);

        echo json_encode($data);
        exit;
    }

}
