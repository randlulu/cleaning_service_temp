<?php

class Settings_EmailTemplateController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsEmail';

        BreadCrumbs::setLevel(2, 'Email Template');
    }

    /**
     * Items list action
     */
    public function indexAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsEmailTemplateList'));

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        //
        // get data list
        //
        $emailTemplateObj = new Model_EmailTemplate();
        $this->view->data = $emailTemplateObj->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);

        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
    }


    /**
     * 07/06/2015 D.A
     * get trading images by Template ID action
     */
    public function tradingImagesAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsEmailTemplateList'));

        $images=array();
        $trading_names=array();
        $trading_name_imgs=array();
        $trading_name_ids=array();
        $imgs_name_part=array();
        $template_name_id = $this->_request->getParam('template_name_id');
        $trading_name_id = $this->_request->getParam('trading_name_id');
        $emailTemplateObj = new Model_EmailTemplate();
        $result = $emailTemplateObj->getById($template_name_id);
        $images=explode(',', $result['imagePlaceholder']);
        $template_name=$result['name'];
        $trading_namesImagesobj = new Model_TradingNameImages();
        $trading_namesImages = $trading_namesImagesobj->getByTradingnameIdandTemplateId($trading_name_id,$template_name_id);
        if($trading_namesImages){
        foreach ($trading_namesImages as $trading_namesImage) {
            $trading_name_imgs[]= $trading_namesImage['trading_name_img'];
            $trading_name_ids[]= $trading_namesImage['id'];
            $imgname=explode('.', $trading_namesImage['trading_name_img']);
            $imgs_name_part[]= $imgname[0];
        }
        }

        $trading_namesObj = new Model_TradingName();
        $trading_names = $trading_namesObj->getById($trading_name_id);
        $trading_name= $trading_names['trading_name'];
        $trading_name_folder= str_ireplace(" ","_", $trading_name);

        $results = array(
            'imagePlaceholder' =>$images,
            'trading_name_imgs' =>$trading_name_imgs,
            'trading_name_ids' =>$trading_name_ids,
            'trading_name' =>$trading_name_folder,
            'template_name' =>$template_name,
            'imgs_name_part' =>$imgs_name_part
        );
        echo $this->_helper->json($results);exit;
    }


    /**
     * Add new item action
     */
   
    public function addAction() {
        
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsEmailTemplateAdd'));
        
        //
        // get request parameters
        //
        $name = $this->request->getParam('name');
        $body = $this->request->getParam('body');
        $subject = $this->request->getParam('subject');
        $placeHolder = $this->request->getParam('placeholder');
        $imagePlaceholder = $this->request->getParam('imagePlaceholder');
        
        $router = Zend_Controller_Front::getInstance()->getRouter();
        
        $attachmentObj = new Model_Attachment();
        $modelEmailTemplateAttachment = new Model_EmailTemplateAttachment();
        
        //
        // init action form
        //
        $form = new Settings_Form_EmailTemplate();
        
        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $emailTemplateObj = new Model_EmailTemplate();
                //echo $body;exit;
                $data = array(
                              'name' => trim($name),
                              'placeholder' => trim($placeHolder),
                              'body' => $body,
                              'subject' => $subject,
                              'imagePlaceholder' => trim($imagePlaceholder)
                              );
                
                $success = $emailTemplateObj->insert($data);
                
                $upload = new Zend_File_Transfer_Adapter_Http();
                $files = $upload->getFileInfo();
                if(isset($files) && !empty($files)){
                    $counter = 0;
                    foreach ($files as $file => $fileInfo) {
                        if ($upload->isUploaded($file)) {
                            if ($upload->receive($file)) {
                                $counter = $counter + 1;
                                $info = $upload->getFileInfo($file);
                                $source = $info[$file]['tmp_name'];
                                $imageInfo = pathinfo($source);
                                $ext = $imageInfo['extension'];
                                $size = $info[$file]['size'];
                                $type = $info[$file]['type'];
                                $dir = get_config('attachment') . '/';
                                $subdir = date('Y/m/d/');
                                $fullDir = $dir . $subdir;
                                if (!is_dir($fullDir)) {
                                    mkdir($fullDir, 0777, true);
                                }
                                $loggedUser = CheckAuth::getLoggedUser();
                                $data = array(
                                              'created_by' => $loggedUser['user_id']
                                              , 'created' => time()
                                              , 'size' => $size
                                              , 'type' => $type
                                              );
                                
                                $Attachid = $attachmentObj->insert($data);
                                $emailTemplateAttachmentId = $modelEmailTemplateAttachment->insert(array('attachment_id' => $Attachid, 'email_template_id' => $success));
                                
                                $fileName = $success . '_' . $counter . '_' . 'email_template' . '.' . $ext;
                                //$fileName = $info[$file]['name'];
                                $image_saved = copy($source, $fullDir . $fileName);
                                
                                $typeParts = explode("/", $type);
                                if ($typeParts[0] == 'image') {
                                    $thumbName = $success . '_' . $counter . '_' . 'email_template_thumbnail' . '.' . $ext;
                                    ImageMagick::create_thumbnail($source, $fullDir . $thumbName, 78, 64);
                                } else {
                                    $thumbName = $success . '_' . $counter . '_' . 'email_template' . '.jpg';
                                    ImageMagick::convertFile($source . '[0]', $fullDir . $thumbName);
                                }
                                
                                $Updatedata = array(
                                                    'path' => $fullDir . $fileName,
                                                    'file_name' => $fileName,
                                                    'thumbnail_file' => $fullDir . $thumbName
                                                    );
                                
                                $updateAttachment = $attachmentObj->updateById($Attachid, $Updatedata);
                                if ($image_saved) {
                                    if (file_exists($source)) {
                                        unlink($source);
                                    }
                                }
                            }
                        }
                    }
                }
                
                
                
                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Email Template"));
                }
                
                //$this->_redirect($this->router->assemble(array(), 'settingsEmailTemplateList'));
                echo 1;
                exit;
            }
        }
        
        $this->view->form = $form;
    }
    
    public function deleteAction() {
        
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsEmailTemplateDelete'));
        
        if (!get_config('under_construction')) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "could not be deleted ,This Item used in other Place "));
            $this->_redirect($this->router->assemble(array(), 'settingsEmailTemplateList'));
        }
        
        //
        // get request parameters
        //
		
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
        if ($id) {
            $ids[] = $id;
        }
        
        $emailTemplateObj = new Model_EmailTemplate();
        foreach ($ids as $id) {
            $emailTemplateObj->deleteById($id);
            $Attachemnts = $emailTemplateObj->getAllAttachmentById($id);
            foreach ($Attachemnts as $Attachemnt) {
                $modelAttachment->updateById($Attachemnt['attachment_id'], array('is_deleted' => '1'));
            }
        }
        
        $this->_redirect($this->router->assemble(array(), 'settingsEmailTemplateList'));
    }
    
    public function editAction() {
        /*$modelContractorInfo = new Model_ContractorInfo();
         $result = $modelContractorInfo->getByContractorId(15);
         var_dump($result);exit;
         /*$modelUnavailableEvent = new Model_UnavailableEvent();
         $result = $modelUnavailableEvent->getAll();
         var_dump(time());exit;*/
        /*$modelUser = new Model_User();
         echo $modelUser->isAvailable(124);
         exit;
         /*$modelEmailTemplate = new Model_EmailTemplate();
         $r = $modelEmailTemplate->getAllAttachmentByIdAsString(75);
         var_dump($r);
         exit;
         
         $success = $modelEmailTemplate->deleteById(73);
         echo $success;
         exit;
         /*$emailTemplateAttachmentObj = new Model_EmailTemplateAttachment();
         $attachments = $emailTemplateAttachmentObj->getAll();
         var_dump($attachments);
         exit;*/
        
        
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsEmailTemplateEdit'));
        
        //
        // get request parameters
        //
        $name = $this->request->getParam('name');
        $body = $this->request->getParam('body');
        $subject = $this->request->getParam('subject');
        $placeHolder = $this->request->getParam('placeholder');
        $imagePlaceholder = $this->request->getParam('imagePlaceholder');
        
        $id = $this->request->getParam('id');
        
        
        //
        // validation
        //
        $emailTemplateObj = new Model_EmailTemplate();
        $emailTemplateAttachmentObj = new Model_EmailTemplateAttachment();
        $attachmentObj = new Model_Attachment();
        $emailTemplate = $emailTemplateObj->getById($id);
        if (!$emailTemplate) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect($this->router->assemble(array(), 'settingsEmailTemplateList'));
            return;
        }
        $this->view->placeholder = $emailTemplate['placeholder'];
        $this->view->template_id = $id;
        
        
        //
        // init action form
        //
        $form = new Settings_Form_EmailTemplate(array('mode' => 'update', 'emailTemplate' => $emailTemplate));
        
        
        //
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                              'name' => trim($name),
                              'placeholder' => trim($placeHolder),
                              'body' => $body,
                              'subject' => $subject,
                              
                              'imagePlaceholder' => trim($imagePlaceholder)
                              );
                
                //var_dump($data);exit;
                
                $deleted_ids = $this->request->getParam('deleted_ids', '');
                $successDeleted = 0;
                if (!empty($deleted_ids)) {
                    $deleted_ids = explode(",", $deleted_ids);
                    $modelAttachment = new Model_Attachment();
                    foreach ($deleted_ids as $deleted_id) {
                        $successDeleted = $modelAttachment->updateById($deleted_id, array('is_deleted' => '1'));
                    }
                }
                
                $success = $emailTemplateObj->updateById($id, $data);
                
                
                $updateAttachment = 0;
                $upload = new Zend_File_Transfer_Adapter_Http();
                $files = $upload->getFileInfo();
                //var_dump($files);exit;
                $Attachments = $emailTemplateObj->getAllAttachmentById($emailTemplate['id']);
                $counter = count($Attachments);
                foreach ($files as $file => $fileInfo) {
                    if ($upload->isUploaded($file)) {
                        if ($upload->receive($file)) {
                            $counter = $counter + 1;
                            $info = $upload->getFileInfo($file);
                            $source = $info[$file]['tmp_name'];
                            //$dest = $info[$file]['name'];
                            $imageInfo = pathinfo($source);
                            $ext = $imageInfo['extension'];
                            $size = $info[$file]['size'];
                            $type = $info[$file]['type'];
                            $dir = get_config('attachment') . '/';
                            $subdir = date('Y/m/d/');
                            $fullDir = $dir . $subdir;
                            if (!is_dir($fullDir)) {
                                mkdir($fullDir, 0777, true);
                            }
                            $loggedUser = CheckAuth::getLoggedUser();
                            $data = array(
                                          'created_by' => $loggedUser['user_id']
                                          , 'created' => time()
                                          , 'size' => $size
                                          , 'type' => $type
                                          );
                            
                            
                            
                            $Attachid = $attachmentObj->insert($data);
                            $emailTemplateAttachmentId = $emailTemplateAttachmentObj->insert(array('attachment_id' => $Attachid, 'email_template_id' => $emailTemplate['id']));
                            
                            $fileName = $emailTemplate['id'] . '_' . $counter . '_' . 'email_template' . '.' . $ext;
                            //$fileName = $info[$file]['name'];
                            $image_saved = copy($source, $fullDir . $fileName);
                            
                            $typeParts = explode("/", $type);
                            if ($typeParts[0] == 'image') {
                                $thumbName = $emailTemplate['id'] . '_' . $counter . '_' . 'email_template_thumbnail' . '.' . $ext;
                                ImageMagick::create_thumbnail($source, $fullDir . $thumbName, 78, 64);
                            } else {
                                $thumbName = $emailTemplate['id'] . '_' . $counter . '_' . 'email_template' . '.jpg';
                                ImageMagick::convertFile($source . '[0]', $fullDir . $thumbName);
                            }
                            
                            $Updatedata = array(
                                                'path' => $fullDir . $fileName,
                                                'file_name' => $fileName,
                                                'thumbnail_file' => $fullDir . $thumbName
                                                );
                            
                            $updateAttachment = $attachmentObj->updateById($Attachid, $Updatedata);
                            if ($image_saved) {
                                if (file_exists($source)) {
                                    unlink($source);
                                }
                            }
                        }
                    }
                }
                
                if ($success || $updateAttachment || $successDeleted) {
                    
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Email Template"));
                }
                //$this->view->successMessage = 'Updated Successfully';
                
                echo 1;
                exit;
                
            }
        }
        
        $this->view->form = $form;
        
        
    }
    
    
    public function getAttachmentsAction() {
        $email_template_id = $this->request->getParam('email_template_id', 0);
        $modelEmailTemplate = new Model_EmailTemplate();
        $Attachemnts = $modelEmailTemplate->getAllAttachmentById($email_template_id);
        
        header('Content-type: text/json');
        header('Content-type: application/json');
        echo json_encode($Attachemnts);
        exit;
    }
}
    
