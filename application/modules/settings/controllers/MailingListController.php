<?php

class Settings_MailingListController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
	
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
		
		$this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsMailingList';

        BreadCrumbs::setLevel(2, 'Mailing List');
		
		$this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
        
        $pageName = $this->request->getParam('page_name', '');
        $this->view->page_title = (!empty($pageName))?$pageName." - Mailing List Settings":"Mailing List Settings";

    }
	

    /**
     * Items list action
     */
    public function indexAction() {
	
	   //
        // check Auth for logged user
        //
		
        CheckAuth::checkPermission(array('settingsMailingList'));

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'mailing_list_id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
 
        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];
		$filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // get data list
        //
        $mailingListObj = new Model_MailingList();
        $this->view->data = $mailingListObj->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);

        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
	  
	
	}
	
	public function addAction() {
	
	  CheckAuth::checkPermission(array('settingsMailingListAdd'));
	  $name = $this->request->getParam('name','');
	  $filters = $this->request->getParam('fltr',array());
	  $form = new Settings_Form_MailingList();
	  
	  
	   if ($this->request->isPost()) { // check if POST request method
	      if(empty($name)){
				$is_valid = $form->isValid($this->request->getPost());
			 }else{
			   $is_valid = true;
			 }
			if ($is_valid) { // validate form data
			  
			 
			  $customer = new Model_Customer();
			  if($filters['booking_date']){			    
			    $filters['booking_date'] = strtotime($filters['booking_date']);
			  }
			  if($filters['enquiry_date']){
			    $filters['enquiry_date'] = strtotime($filters['enquiry_date']);
			  }
			  if($filters['estimate_date']){
			    $filters['estimate_date'] = strtotime($filters['estimate_date']);
			  }
			  if($filters['deferred_date']){
			    $filters['deferred_date'] = strtotime($filters['deferred_date']);
			  }
			  
			  //$customers = $customer->getCustomersId($filters);
              $country_id = CheckAuth::getCountryId();
              $booking_date = $deferred_date = $enquiry_date = $estimate_date = '';	
              $company_id = CheckAuth::getCompanySession();
			  $loggedUser = CheckAuth::getLoggedUser();
			  $userId = $loggedUser['user_id'];
			  $mailing_list_data = array('name'=>$name,
			                              'country_id'=>$country_id,
										  'state'=>$filters['state'],
										  'city_id'=>$filters['city_id'],
										  'suburb'=>$filters['suburb'],
										  'customer_type_id'=>$filters['customer_type_id'],
										  'status_id'=>$filters['status'],
										  'booking_date'=>$filters['booking_date'],
										  'inquiry_date'=>$filters['enquiry_date'],
										  'estimate_date'=>$filters['estimate_date'],
										  'deferred_date'=>$filters['deferred_date'],
										  'service_id'=>$filters['service_id'],
										  'label_id'=>$filters['label_id'],
										  'company_id'=>$company_id,
										  'created'=>time(),
										  'created_by'=>$userId,
										  );
										  
			
			
			  //insert into mailing list
			  $mailingListObj = new Model_MailingList();
			  $mailing_list_id = $mailingListObj->insert($mailing_list_data);
			   
			  if($mailing_list_id){
			   /* foreach($customers as $customer){
			    $data = array('customer_id' => $customer['customer_id'] ,
				              'mailing_list_id' => $mailing_list_id

				);			
				// insert into mailing_list_emails
				$mailingListEmailsObj = new Model_MailingListEmails();
				$mailingListEmailsObj->insert($data);
			    }*/
			     $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved"));
              } else {
                 $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
              }
			  
			  $this->_redirect($this->router->assemble(array(), 'settingsMailingList'));
			}
		}	
	  
	  $this->view->form = $form;

	}
	
	public function editAction() {
	
	 CheckAuth::checkPermission(array('settingsMailingListEdit'));
	 
	  $name = $this->request->getParam('name','');
	  $filters = $this->request->getParam('fltr',array());
	  $id = $this->request->getParam('id');
	  //get mailing list by Id;
	   $mailingListObj = new Model_MailingList();
       $mailingList = $mailingListObj->getById($id);
	   $country_id = CheckAuth::getCountryId();
        if (!$mailingList) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect($this->router->assemble(array(), 'settingsMailingList'));
            return;
        }
	  $form = new Settings_Form_MailingList(array('mode' => 'update', 'mailingList' => $mailingList));
	  if ($this->request->isPost()) { // check if POST request method
	       if(empty($name)){
				$is_valid = $form->isValid($this->request->getPost());
			 }else{
			   $is_valid = true;
			 }
            if ($is_valid) { // validate form data
			if($filters['booking_date']){			    
			    $filters['booking_date'] = strtotime($filters['booking_date']);
			  }
			  if($filters['enquiry_date']){
			    $filters['enquiry_date'] = strtotime($filters['enquiry_date']);
			  }
			  if($filters['estimate_date']){
			    $filters['estimate_date'] = strtotime($filters['estimate_date']);
			  }
			  if($filters['deferred_date']){
			    $filters['deferred_date'] = strtotime($filters['deferred_date']);
			  }
			
                $mailing_list_data = array('name'=>$name,
			                              'country_id'=>$country_id,
										  'state'=>$filters['state'],
										  'city_id'=>$filters['city_id'],
										  'suburb'=>$filters['suburb'],
										  'customer_type_id'=>$filters['customer_type_id'],
										  'status_id'=>$filters['status'],
										  'booking_date'=>$filters['booking_date'],
										  'inquiry_date'=>$filters['enquiry_date'],
										  'deferred_date'=>$filters['deferred_date'],
										  'service_id'=>$filters['service_id'],
										  'label_id'=>$filters['label_id'],
										  );

                $success = $mailingListObj->updateById($id, $mailing_list_data);               
                if ($success) {
				    // delete all customers in mailing list 
					/*$mailingListEmailsObj = new Model_MailingListEmails();
					$mailingListEmailsObj->deleteByMailingListId($id);
					
				    $customer = new Model_Customer();
			        $customers = $customer->getCustomersId($filters);
                    foreach($customers as $customer){
			            $data = array('customer_id' => $customer['customer_id'] ,
				              'mailing_list_id' => $id);
				       
				        // insert into mailing_list_emails	
                        $mailingListEmailsObj->insert($data);				
			          }*/					
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                }


                $this->_redirect($this->router->assemble(array(), 'settingsMailingList'));
            }
        }
		
		$this->view->form = $form;
		$this->view->mailingList = $mailingList;
		
	}
	
	    public function deleteAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsMailingListDelete'));


        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
        if ($id) {
            $ids[] = $id;
        }

        $mailingListObj = new Model_MailingList();
        //$mailingListEmailsObj = new Model_MailingListEmails();
						
        foreach ($ids as $id) {
            $mailingListObj->deleteById($id);
			// delete mailing list emails by mailing_list_id
			//$mailingListEmailsObj->deleteByMailingListId($id);	
        }
        $this->_redirect($this->router->assemble(array(), 'settingsMailingList'));
    }
	
	
	public function sendEmailAction(){
	
	
	    CheckAuth::checkPermission(array('sendEmail'));
		
	    $mailing_list_id = $this->request->getParam('mailing_list_id','');		
		$id = $this->request->getParam('id');
		if(isset($id)){
		  $modelMarketingEmailLog = new Model_MarketingEmailLog();
		  $marketingEmailLog = $modelMarketingEmailLog->getById($id);
		  $form = new Settings_Form_SendEmailMailingList(array('marketingEmailLog' => $marketingEmailLog));
		}else{
		 $form = new Settings_Form_SendEmailMailingList();
		}
		
		$mailingListObj = new Model_MailingList();
		$customertObj = new Model_Customer();
		$EmailTemplateObj = new Model_EmailTemplate();
		$mailingListUnsubscribedObj = new Model_MailingListUnsubscribed();
		
		if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
			  /*$mailingList  = $mailingListObj->getById($mailing_list_id);
			  $mailingListFilters = array('state'=>$mailingList['state'],
			                               'city_id'=>$mailingList['city_id'],
			                               'suburb'=>$mailingList['suburb'],
			                               'customer_type_id'=>$mailingList['customer_type_id'],
			                               'status_id'=>$mailingList['status_id'],
			                               'booking_date'=>$mailingList['booking_date'],
			                               'inquiry_date'=>$mailingList['inquiry_date'],
			                               'estimate_date'=>$mailingList['estimate_date'],
			                               'deferred_date'=>$mailingList['deferred_date'],
			                               'service_id'=>$mailingList['service_id'],
			                               'label_id'=>$mailingList['label_id'],
			                       );
				$unsubscribedCustomerEmails = $mailingListUnsubscribedObj->getByMailingListId($mailing_list_id);
				

				
				$not_customer_ids = array();
				$unsubscribe_customers = array();
				foreach($unsubscribedCustomerEmails as $unsubscribedCustomerEmail){
				  if(count($mailingListUnsubscribedObj->getByCustomerId($unsubscribedCustomerEmail['customer_id'])) == 3){
				    $not_customer_ids[$unsubscribedCustomerEmail['customer_id']] = $unsubscribedCustomerEmail['customer_id'];
				   }else{
				    $unsubscribe_customers[$unsubscribedCustomerEmail['customer_id']] = $unsubscribedCustomerEmail;
				   }
				}
				if($not_customer_ids){
				 $mailingListFilters['not_customer_ids'] = $not_customer_ids;
				}else{
				 $mailingListFilters['not_customer_ids'] = '';
				}
				
				$matchCustomers = $customertObj->allCustomers($mailingListFilters);
				//var_dump($matchCustomers);
				//exit;
				
				
				
				
				if($matchCustomers){
				$emailsCount = 0;
				foreach($matchCustomers as $key=>$matchCustomer){
				  //$customer = $customertObj->getById($customerId['customer_id']);
				   $to = array();
					if ($matchCustomer['email1']) {
						$to[0]['email'] = $matchCustomer['email1'];
						$to[0]['number'] = '1';
						
					}
					if ($matchCustomer['email2']) {
						$to[1]['email'] = $matchCustomer['email2'];
						$to[1]['number'] = '2';
						
					}
					if ($matchCustomer['email3']) {
						$to[2]['email'] = $matchCustomer['email3'];
						$to[2]['number'] = '3';
						
					}
					
				
				    
					
					if($unsubscribe_customers){
					 foreach($unsubscribe_customers as $unsubscribe_customer){
					   if($unsubscribe_customer['customer_id'] == $matchCustomer['customer_id']){					     
					     if($matchCustomer['email1'] == $unsubscribe_customer['customer_email']){
						    unset($to[0]);
						 }else if($matchCustomer['email2'] == $unsubscribe_customer['customer_email']){
						    unset($to[1]);
						 }else if($matchCustomer['email3'] == $unsubscribe_customer['customer_email']){
						    unset($to[2]);
						 }
						break; 
					   }
					 }
					}
					
					
					if (count($to) == 0){
					 
					 unset($matchCustomers[$key]);
					 
					}
					
					$emailsCount = $emailsCount + count($to);
	

            
			
		  }
		  


		  
		}*/
		
		$result = $this->getMatchCustomers($mailing_list_id);
		$matchCustomers = $result['matchCustomers'];
		$emailsCount = $result['emailsCount'];
		
		
		
		if(!empty($matchCustomers)){
         $this->buildCustomerFile($matchCustomers,$mailing_list_id);	
       	}	
		
		$downloadUrl = $this->router->assemble(array('mailing_list_id'=>$mailing_list_id), 'downloadCustomersMailingList'); 
		echo json_encode(array('customers'=>$matchCustomers , 'CustomerCount'=>count($matchCustomers) , 'EmailsCount'=>$emailsCount,'downloadUrl'=>$downloadUrl));
		exit;
	  }else{
        $messages = $form->getMessages();
		echo json_encode(array('errors'=>$messages));
	    exit;
      }
    
    	
	 
   }

        $this->view->form = $form;	
}
	
	
	public function buildCustomerFile($customers,$mailing_list_id){
	  

		
	  $view = new Zend_View();
	  $view->setScriptPath(APPLICATION_PATH . '/modules/settings/views/scripts/mailing-list');
      $view->customers = $customers;
      $bodyCustomers = $view->render('all-customers.phtml');
	  $company_id = CheckAuth::getCompanySession();
      $subdir = date('Y/m/d/');
      $dir = get_config('attachment');
	  $fullDir = $dir . '/' . $subdir;
      $loggedUser = CheckAuth::getLoggedUser();	
	  $user_id = $loggedUser['user_id'];
        //check if file exists or not
        if (!is_dir($fullDir)) {
            mkdir($fullDir, 0777, true);
        }

        $fileName = "{$fullDir}customers_{$user_id}_{$company_id}_{$mailing_list_id}.xls";

        $mode = 'x+';
        if (file_exists($fileName)) {
            $mode = 'w+';
			chmod($fileName, 0777);
        }

		
        $fhandler = fopen($fileName, $mode);
        fwrite($fhandler, $bodyCustomers);
        fclose($fhandler);
	    
	}
	
	public function downloadCustomersXlAction(){
	
	  
        $loggedUser = CheckAuth::getLoggedUser();
        $company_id = CheckAuth::getCompanySession();
		$mailing_list_id = $this->request->getParam('mailing_list_id');
		
	    $user_id = $loggedUser['user_id'];
		$subdir = date('Y/m/d/');
        $dir = get_config('attachment');
	    $fullDir = $dir . '/' . $subdir;
		$fileName = "{$fullDir}customers_{$user_id}_{$company_id}_{$mailing_list_id}.xls";
		
		
		//$mailing_list_id = $this->request->getParam('id','');
		if(isset($mailing_list_id) && !empty($mailing_list_id)){
	            $this->getMatchCustomers($mailing_list_id,true);
			}
			
		if (file_exists($fileName)){
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename="'.basename($fileName).'"');
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . filesize($fileName));
			readfile($fileName);
			exit;
       }          
   exit;
  }
	
	
	public function getMatchCustomers($mailing_list_id , $toFile = false){
	
	          $mailingListObj = new Model_MailingList();
		      $customertObj = new Model_Customer();
		      $EmailTemplateObj = new Model_EmailTemplate();
			  $mailingListUnsubscribedObj = new Model_MailingListUnsubscribed();

		
		      $mailingList  = $mailingListObj->getById($mailing_list_id);
			  $mailingListFilters = array('state'=>$mailingList['state'],
			                               'city_id'=>$mailingList['city_id'],
			                               'suburb'=>$mailingList['suburb'],
			                               'customer_type_id'=>$mailingList['customer_type_id'],
			                               'status_id'=>$mailingList['status_id'],
			                               'booking_date'=>$mailingList['booking_date'],
			                               'inquiry_date'=>$mailingList['inquiry_date'],
			                               'estimate_date'=>$mailingList['estimate_date'],
			                               'deferred_date'=>$mailingList['deferred_date'],
			                               'service_id'=>$mailingList['service_id'],
			                               'label_id'=>$mailingList['label_id'],
			                       );
				$unsubscribedCustomerEmails = $mailingListUnsubscribedObj->getByMailingListId($mailing_list_id);
				

				
				$not_customer_ids = array();
				$unsubscribe_customers = array();
				foreach($unsubscribedCustomerEmails as $unsubscribedCustomerEmail){
				  if(count($mailingListUnsubscribedObj->getByCustomerId($unsubscribedCustomerEmail['customer_id'])) == 3){
				    $not_customer_ids[$unsubscribedCustomerEmail['customer_id']] = $unsubscribedCustomerEmail['customer_id'];
				   }else{
				    $unsubscribe_customers[$unsubscribedCustomerEmail['customer_id']] = $unsubscribedCustomerEmail;
				   }
				}
				if($not_customer_ids){
				 $mailingListFilters['not_customer_ids'] = $not_customer_ids;
				}else{
				 $mailingListFilters['not_customer_ids'] = '';
				}
				
				$matchCustomers = $customertObj->allCustomers($mailingListFilters);
				//var_dump($matchCustomers);
				//exit;
				
				
				
				
				if($matchCustomers){
				$emailsCount = 0;
				foreach($matchCustomers as $key=>$matchCustomer){
				  //$customer = $customertObj->getById($customerId['customer_id']);
				   $to = array();
					if ($matchCustomer['email1']) {
						$to[0]['email'] = $matchCustomer['email1'];
						$to[0]['number'] = '1';
						
					}
					if ($matchCustomer['email2']) {
						$to[1]['email'] = $matchCustomer['email2'];
						$to[1]['number'] = '2';
						
					}
					if ($matchCustomer['email3']) {
						$to[2]['email'] = $matchCustomer['email3'];
						$to[2]['number'] = '3';
						
					}
					
				
				    
					
					if($unsubscribe_customers){
					 foreach($unsubscribe_customers as $unsubscribe_customer){
					   if($unsubscribe_customer['customer_id'] == $matchCustomer['customer_id']){					     
					     if($matchCustomer['email1'] == $unsubscribe_customer['customer_email']){
						    unset($to[0]);
						 }else if($matchCustomer['email2'] == $unsubscribe_customer['customer_email']){
						    unset($to[1]);
						 }else if($matchCustomer['email3'] == $unsubscribe_customer['customer_email']){
						    unset($to[2]);
						 }
						break; 
					   }
					 }
					}
					
					
					if (count($to) == 0){
					 
					 unset($matchCustomers[$key]);
					 
					}
					
					$emailsCount = $emailsCount + count($to);
	

            
			
		  }
		  


		  
		}
		
		if($toFile){
		  if(!empty($matchCustomers)){
            $this->buildCustomerFile($matchCustomers,$mailing_list_id);	
		  }
		}else{
		  return array('matchCustomers'=>$matchCustomers , 'emailsCount'=>$emailsCount);
		}
		
		
	
	}
	
	public function confirmSendEmailAction() {
	
	    
		CheckAuth::checkPermission(array('sendEmail'));
		
		$mailing_list_id = $this->request->getParam('mailing_list_id','');		  
		$result = $this->getMatchCustomers($mailing_list_id,false);
		$matchCustomers = $result['matchCustomers'];
		$emailsCount = $result['emailsCount'];
		
		
		        
		$email_template_id = $this->request->getParam('emailTemplate','');
		$body = $this->request->getParam('body','');
		$subject = $this->request->getParam('subject','');
		
		$id = $this->request->getParam('id');
		if(isset($id)){
		  $modelMarketingEmailLog = new Model_MarketingEmailLog();
		  $marketingEmailLog = $modelMarketingEmailLog->getById($id);
		  $form = new Settings_Form_SendEmailMailingList(array('marketingEmailLog' => $marketingEmailLog));
		}else{
		 $form = new Settings_Form_SendEmailMailingList();
		}
		
		$mailingListObj = new Model_MailingList();
		$customertObj = new Model_Customer();
		$EmailTemplateObj = new Model_EmailTemplate();
		$mailingListUnsubscribedObj = new Model_MailingListUnsubscribed();
		
		if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
			  
				$unsubscribedEmails = $mailingListUnsubscribedObj->getByMailingListId($mailing_list_id);
				$not_customer_ids = array();
				$customer_ids = array();
				foreach($unsubscribedEmails as $email){
				    $customer_ids[$email['customer_id']] = $email;				   
				}
							
				$emailTemplateData  = $EmailTemplateObj->getById($email_template_id);
				//$modelCannedResponses = new Model_CannedResponses();
                //$cannedResponse = $modelCannedResponses->getById($email_template_id);
				
				
				
				if($matchCustomers){
				foreach($matchCustomers as $matchCustomer){
				  
				  //$customer = $customertObj->getById($customerId);
				   $to = array();
					if ($matchCustomer['email1']) {
						$to[0]['email'] = $matchCustomer['email1'];
						$to[0]['number'] = '1';
					}
					if ($matchCustomer['email2']) {
						$to[1]['email'] = $matchCustomer['email2'];
						$to[1]['number'] = '2';
					}
					if ($matchCustomer['email3']) {
						$to[2]['email'] = $matchCustomer['email3'];
						$to[2]['number'] = '3';
					}
					
				
				    
					
					if($customer_ids){
					 foreach($customer_ids as $customer_id){
					   if($customer_id['customer_id'] == $matchCustomer['customer_id']){					     
					     if($matchCustomer['email1'] == $customer_id['customer_email']){
						    unset($to[0]);
						 }else if($matchCustomer['email2'] == $customer_id['customer_email']){
						    unset($to[1]);
						 }else if($matchCustomer['email3'] == $customer_id['customer_email']){
						    unset($to[2]);
						 }
						break; 
					   }
					 }
					}
					
					
					
					

					
					
					
					
				  
				  $template_params = array(
					//customer
					'{customer_name}' => get_customer_name($matchCustomer),
					'{customer_first_name}' => isset($matchCustomer['first_name']) && $matchCustomer['first_name'] ? ucwords($matchCustomer['first_name']) : '',
					'{customer_last_name}' => isset($matchCustomer['last_name']) && $matchCustomer['last_name'] ? ' ' . ucwords($matchCustomer['last_name']) : '',
					'{customer_contacts}' => nl2br($customertObj->getCustomerContacts($matchCustomer['customer_id']))
					);
					
				  foreach($to as $toC){
					   
					   $template_params['{unsubscribe_link}'] = $this->router->assemble(array('mailing_id' => $mailing_list_id , 'customer_id'=> $matchCustomer['customer_id'] , 'email'=> $toC['number']), 'settingsMailingListUnsubscribe');
					   
					    $emailTemplate = $EmailTemplateObj->getEmailTemplate($emailTemplateData['name'], $template_params);

				       if(empty($subject)){
				        $subject = $emailTemplate['subject'];
				      }
				    
				      $body = $emailTemplate['body'];
			          $body = $body;
					   
					  
					   //$body = $body . "<a style='background-color: #1ab394;border-color: #1ab394;color: #FFFFFF;border-radius:3px;padding:6px 12px;text-decoration:none;' href='".$this->router->assemble(array('mailing_id' => $mailing_list_id , 'customer_id'=> $customer['customer_id'] , 'email'=> $toC['number']), 'settingsMailingListUnsubscribe')."'>Unsubscribe</a>" ; 
					   
					   $params = array(
							'to' => $toC['email'],
							'body' => $body,
							'subject' => $subject
							//'cc' => 'ayman@tilecleaners.com.au'
							//'layout' => 'designed_email_template'
						);
						
						$error_mesages = array();
            
			
						if (EmailNotification::validation($params, $error_mesages)) {
							// Send Email
							$success = EmailNotification::sendEmail($params, '', array(), array());
						}
					  
					}
					
					
				  
			
			
		
			
			  /*$body = $cannedResponse['body'];
		      $subjectTemplate = $cannedResponse['subject'];
			  $subject = $this->request->getParam('subject',$subjectTemplate);
			  //$to = $customer['email1'];
			
			  $params = array(
                'to' => $to,
                'body' => $body,
                'subject' => $subject
            );*/

            
			
		  }
		  
		  if ($success){
					$this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "{$emailsCount} emails sent"));
				} else{
					$this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Failed to send email"));
				}

		  
		}else{
		  $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "There is no customer in this mailing list"));
		}
		
		echo 1;
		exit;
	}
   }

        $this->view->form = $form;		

	}
	
	public function unsubscribeAction(){
	  
	    $this->view->main_menu = 'Mailing List';
        $this->view->sub_menu = 'settingsMailingList';

        BreadCrumbs::setLevel(2, 'Unsubscribe User');
		
	    $email_address = $this->request->getParam('email_address','');
		$mailing_list_id = $this->request->getParam('mailing_id','');
		$other_reason = $this->request->getParam('other-reason',''); 
		$why = $this->request->getParam('why',$other_reason);
		$email = $this->request->getParam('email','');
		$customer_id = $this->request->getParam('customer_id','');
		$cronjob_id = $this->request->getParam('cronjob_id',0);
		$email_template_id = $this->request->getParam('email_template_id',0);
		$trading_name_id = $this->request->getParam('trading_name_id',0);
		
		$customerObj = new Model_Customer();
		$customer = $customerObj->getById($customer_id);
		
		$mailingListObj = new Model_MailingList();
		$mailingList = $mailingListObj->getById($mailing_list_id);
		
		if($cronjob_id){
		  $mode = 'cronjob';
		}else if($email_template_id){
		   $mode = 'emailtemplate';
		}else{
		 $mode = 'mailing_list';
		}
		
		
		$form = new Settings_Form_Unsubscribe(array('customer'=>$customer ,'email'=>$email, 'mode' => $mode));
		
			
		$mailingListUnsubscribedObj = new Model_MailingListUnsubscribed();
		if ($this->request->isPost()) { // check if POST request method
		  
            if ($form->isValid($this->request->getPost())) { // validate form data
			 
			  if($why == 'Other'){
			   $other_reason = $this->request->getParam('other-reason',$why); 
			   $why = $other_reason;
			  }
			 
			  $data = array('customer_id'=> $customer_id,
			  'mailing_list_id'=>$mailing_list_id,
			  'created'=>time(),
			  'cronjob_id'=>$cronjob_id,
			  'email_template_id'=>$email_template_id,
			  'user_id'=> $customer_id,
			  'customer_email'=>$email_address,
			  'unsubscribe_reason_id'=>$why
			  );
			  
			  if($cronjob_id){
			    $unsubscribeEmail = $mailingListUnsubscribedObj->getByCustomerIdAndEmailAndCronJob($cronjob_id ,$customer_id ,$email_address);
			  }else if($email_template_id){
			    $unsubscribeEmail = $mailingListUnsubscribedObj->getByCustomerIdAndEmailAndEmailTemplate($email_template_id ,$customer_id ,$email_address);			  
			  }else{
			    $unsubscribeEmail = $mailingListUnsubscribedObj->getByCustomerIdAndEmailAndMailingList($mailing_list_id ,$customer_id ,$email_address);
			  }
			  
			 
			 if($unsubscribeEmail){
			   $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This email is already unsubscribed"));
			 }else{
			 $success = $mailingListUnsubscribedObj->insert($data); 
			 if($success){
			  $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Unsubscribed"));
              } else {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
              }
			 } 
			  
			  $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
			}
		}
		
		
		$trading_namesObj = new Model_TradingName();
	    $trading_name = $trading_namesObj->getById($trading_name_id);
		
		
		$this->view->form = $form;
		$this->view->trading_name = $trading_name;
		$this->view->mailingList = $mailingList;
	
	}
	
	
	public function mailingListEmailsAction(){
	
	    $id = $this->request->getParam('id','');
	   
	    $mailingListObj = new Model_MailingList();
		$mailingListUnsubscribedObj = new Model_MailingListUnsubscribed();
		$customertObj = new Model_Customer();
		
	    $mailingList  = $mailingListObj->getById($id);
	    $mailingListFilters = array('state'=>$mailingList['state'],
			                               'city_id'=>$mailingList['city_id'],
			                               'suburb'=>$mailingList['suburb'],
			                               'customer_type_id'=>$mailingList['customer_type_id'],
			                               'status_id'=>$mailingList['status_id'],
			                               'booking_date'=>$mailingList['booking_date'],
			                               'inquiry_date'=>$mailingList['inquiry_date'],
			                               'estimate_date'=>$mailingList['estimate_date'],
			                               'deferred_date'=>$mailingList['deferred_date'],
			                               'service_id'=>$mailingList['service_id'],
			                               'label_id'=>$mailingList['label_id'],
			                       );
		$unsubscribedEmails = $mailingListUnsubscribedObj->getByMailingListId($id);
		$not_customer_ids = array();
				foreach($unsubscribedEmails as $email){
				  if(count($mailingListUnsubscribedObj->getByCustomerId($email['customer_id'])) == 3){
				    $not_customer_ids[$email['customer_id']] = $email['customer_id'];
				   }
				}
				if($not_customer_ids){
				 $mailingListFilters['not_customer_ids'] = $not_customer_ids;
				}else{
				 $mailingListFilters['not_customer_ids'] = '';
				}
				
		$customersIds = $customertObj->allCustomers($mailingListFilters);
	   
	   $this->view->customers = $customersIds; 
	   $this->view->name = $mailingList['name']; 
	   echo $this->view->render('mailing-list/emails.phtml');
       exit;
	
	}
        
        public function syncEmailsToMailChimpAction(){ //getMailChimpListByMaillistName

		$maillist_name = $this->request->getParam('name');
		$modelMailingList = new Model_MailingList();
		$localMaillist = $modelMailingList->getByName($maillist_name);
            
        $company_id = CheckAuth::getCompanySession();
        $api_name = 'mailchimp';

        $model_api_parameters = new Model_ApiParameters();

        $parameter_name = 'api key';
        $parameter = $model_api_parameters->getParameterValue($company_id, $api_name, $parameter_name);
        $apikey = $parameter['parameter_value'];

	    //$apikey = '53c9bfbfcafaf2fa35a830109d4dd4a2-us13'; //  API key
	    $dc = substr($apikey,strpos($apikey,'-')+1); // $dc = 'us13'; //  data center

	    $ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://".$dc.".api.mailchimp.com/3.0/lists/");
		curl_setopt($ch, CURLOPT_USERPWD, "apikey:".$apikey);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		$output = curl_exec($ch);

		$json_a=json_decode($output,true);

		/** Check the name of list available in MailChimp Or not **/
		/** if found it it will get (ID) of this Maillist and make sync emails to it **/
		/** else if not found Miallist in mailChimp it will Create new one with same name **/
		foreach ($json_a['lists'] as $list) {			
				if($list['name'] == $maillist_name){
					$mailListId  =  $list['id'];
					if(!isset($localMaillist['mailchimp_list_id']) && empty($localMaillist['mailchimp_list_id'])){
						$mailing_list_data = array('mailchimp_list_id'=>$mailListId);
						$modelMailingList->updateById($localMaillist['mailing_list_id'], $mailing_list_data);
					}
					$this->pushEmailsToMailChimpList($mailListId,$localMaillist['mailing_list_id']);
					break;
				}
		}
		//Create new mailChimp list 
		if(!isset($mailListId) && empty($mailListId)){
			$this->createNewMailChimpList($maillist_name);
				// echo "Now We Must Create New MailChimpList !!";
		}

		curl_close($ch);
	}


	public function createNewMailChimpList($maillist_name) { 
        
        $company_id = CheckAuth::getCompanySession();
        $api_name = 'mailchimp';

        $model_api_parameters = new Model_ApiParameters();

        $parameter_name = 'api key';
        $parameter = $model_api_parameters->getParameterValue($company_id, $api_name, $parameter_name);
        $apikey = $parameter['parameter_value'];

	    //$apikey = '53c9bfbfcafaf2fa35a830109d4dd4a2-us13'; //  API key

	    $dc = substr($apikey,strpos($apikey,'-')+1);

	    $modelMailingList = new Model_MailingList();
	    $modelUser = new Model_User();

		$localMaillist = $modelMailingList->getByName($maillist_name);
		$userData = $modelUser->getById($localMaillist['created_by']);


		// the information for your new list--not all is required
	    $data = array( 
	        "name" => $maillist_name,
	        "contact" => array (
	            "company" => "",
	            "address1" => "",
	            "city" => "",
	            "state" => "",
	            "zip" => "",
	            "country" => "",
	            "phone" => "",
	        ),
	        "permission_reminder" => 'this is permission_reminder',// $permission_reminder,
	        // "use_archive_bar" => true,//;$archive_bars,
	        "campaign_defaults" => array(
	            "from_name" => $userData['display_name'], //isset($userData['display_name'])?$userData['display_name']:$userData['email1']),//"Ibrahim",//$from_name,
	            "from_email" => $userData['email1'], //"ibm4tech@gmail.com",//$from_email,
	            "subject" => "",//$subject,
	            "language" => "en",//$language
	        ),
	        // "notify_on_subscribe" => '',//$notify_subs,
	        // "notify_on_unsubscribe" => '',//$notify_unsubs,
	        // "email_type_option" => true, //$type,
	        // "visibility" => 'pub',// $visibility
	        "email_type_option"=>true,
	    );

	    $data = json_encode($data); // API requires JSON objects
	    $ch = curl_init(); 
	    curl_setopt($ch, CURLOPT_URL, "http://".$dc.".api.mailchimp.com/3.0/lists/"); // ../lists/ URL to create new list resource
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
	    curl_setopt($ch, CURLOPT_POST, 1); //curl_setopt($ch, CURLOPT_POSTREQUEST, 1); // declare request is POST type
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $data); // set POST data
	    curl_setopt($ch, CURLOPT_USERPWD, "apikey:".$apikey); // HTML Basic Auth
	    $output = curl_exec($ch); // execute and capture response

	    curl_close($ch); 
	    // echo "New Maillist Was careated";

	    $json=json_decode($output,true);
	     if(!isset($json['type'])){
	     	if(isset($json['id']) && !empty($json['id'])){
	     		$mailing_list_data = array('mailchimp_list_id'=>$json['id']);
	     		$modelMailingList->updateById($localMaillist['mailing_list_id'], $mailing_list_data);

	    		$this->pushEmailsToMailChimpList($json['id'],$localMaillist['mailing_list_id']);
	     	}
	    }else{
	    	$this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Error: " . $json_a['title'] . "   ,Code: ". $json_a['status']  ."<br>" .  $json_a['detail']));
			$this->_redirect($this->router->assemble(array(), 'settingsMailingList'));

	    }

	    // var_dump($output); // display API response*/    

	}
	public function pushEmailsToMailChimpList($list_id,$localMaillist_id) {
		$company_id = CheckAuth::getCompanySession();
        $api_name = 'mailchimp';

        $model_api_parameters = new Model_ApiParameters();

        $parameter_name = 'api key';
        $parameter = $model_api_parameters->getParameterValue($company_id, $api_name, $parameter_name);
        $apikey = $parameter['parameter_value'];

	    //$apikey = '53c9bfbfcafaf2fa35a830109d4dd4a2-us13'; //  API key
		$dc = substr($apikey,strpos($apikey,'-')+1);   		// $dc = 'us13';

		$auth = base64_encode('user:'.$apikey);
			/************************************/
 		// CheckAuth::checkPermission(array('sendEmail'));			
	  //   $mailing_list_id = $this->request->getParam('mailing_list_id',9);		
		// $id = $this->request->getParam('id');
		$mailingListObj = new Model_MailingList();
		$customertObj = new Model_Customer();
		$EmailTemplateObj = new Model_EmailTemplate();
		$mailingListUnsubscribedObj = new Model_MailingListUnsubscribed();

	  	$mailingList  = $mailingListObj->getById($localMaillist_id);
	  	$mailingListFilters = array('state'=>$mailingList['state'],
	                               'city_id'=>$mailingList['city_id'],
	                               'suburb'=>$mailingList['suburb'],
	                               'customer_type_id'=>$mailingList['customer_type_id'],
	                               'status_id'=>$mailingList['status_id'],
	                               'booking_date'=>$mailingList['booking_date'],
	                               'inquiry_date'=>$mailingList['inquiry_date'],
	                               'estimate_date'=>$mailingList['estimate_date'],
	                               'deferred_date'=>$mailingList['deferred_date'],
	                               'service_id'=>$mailingList['service_id'],
	                               'label_id'=>$mailingList['label_id'],
	                       );
		$unsubscribedCustomerEmails = $mailingListUnsubscribedObj->getByMailingListId($localMaillist_id);
		$matchCustomers = $customertObj->allCustomers($mailingListFilters);
		
		$not_customer_ids = array();
		$unsubscribe_customers = array();
		foreach($unsubscribedCustomerEmails as $unsubscribedCustomerEmail){
			if(count($mailingListUnsubscribedObj->getByCustomerId($unsubscribedCustomerEmail['customer_id'])) == 3){
			    $not_customer_ids[$unsubscribedCustomerEmail['customer_id']] = $unsubscribedCustomerEmail['customer_id'];
			}else{
			    $unsubscribe_customers[$unsubscribedCustomerEmail['customer_id']] = $unsubscribedCustomerEmail;
			}

		}

		if($not_customer_ids){
		 $mailingListFilters['not_customer_ids'] = $not_customer_ids;
		}else{
		 $mailingListFilters['not_customer_ids'] = '';
		}
		
		$matchCustomers = $customertObj->allCustomers($mailingListFilters);
		if($matchCustomers){

			$emailsCount = 0;
			$emails = array();
			foreach($matchCustomers as $key=>$matchCustomer){
			  //$customer = $customertObj->getById($customerId['customer_id']);
			   $to = array();

				if ($matchCustomer['email1']) {
					$to[0]['email'] = $matchCustomer['email1'];
					$to[0]['number'] = '1';
					$to[0]['first_name'] = $matchCustomer['first_name'];
					$to[0]['last_name'] = $matchCustomer['last_name'];
					$to[0]['status'] = 'subscribed';
					// $emails[0] = $to[0]['email'];
					
				}
				if ($matchCustomer['email2']) {
					$to[1]['email'] = $matchCustomer['email2'];
					$to[1]['number'] = '2';
					$to[1]['first_name'] = $matchCustomer['first_name'];
					$to[1]['last_name'] = $matchCustomer['last_name'];
					$to[1]['status'] = 'subscribed';
					// $emails[1] = $to[1]['email'];
					
				}
				if ($matchCustomer['email3']) {
					$to[2]['email'] = $matchCustomer['email3'];
					$to[2]['number'] = '3';
					$to[2]['first_name'] = $matchCustomer['first_name'];
					$to[2]['last_name'] = $matchCustomer['last_name'];
					$to[2]['status'] = 'subscribed';
					// $emails[2] = $to[2]['email'];									
				}							
				
				if($unsubscribe_customers){
					foreach($unsubscribe_customers as $unsubscribe_customer){
					   	if($unsubscribe_customer['customer_id'] == $matchCustomer['customer_id']){					     
						    if($matchCustomer['email1'] == $unsubscribe_customer['customer_email']){
							    unset($to[0]);
							    // /unset ($emails[0]);;
							}else if($matchCustomer['email2'] == $unsubscribe_customer['customer_email']){
							    unset($to[1]);
							    // unset($emails[1]);
							}else if($matchCustomer['email3'] == $unsubscribe_customer['customer_email']){
							    unset($to[2]);
							    // /unset($emails[2]);
							}
					   	}
					}
				}
				
				//$emailsCount = $emailsCount + count($to);	

				foreach ($to as $key => $value) {
					$sub['email_address'] = $value['email'];
					$sub['status'] = 'subscribed';
					$sub['merge_fields']  = array(
       				 'FNAME' => $value['first_name'],
       				 'LNAME' => $value['last_name']
    				);

					$memberId = md5(strtolower($sub['email_address']));
					$url = "http://".$dc.".api.mailchimp.com/3.0/lists/" .$list_id . "/members/" . $memberId;

					$subscribe = json_encode($sub);

			    	$ch = curl_init($url); 
				    // curl_setopt($ch, CURLOPT_URL, "http://".$dc.".api.mailchimp.com/3.0/lists/" .$list_id . "/members/"); // ../lists/ URL to add  new members resource
				    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Authorization: Basic '.$auth));
				    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
				    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT'); //'DELETE'
				    curl_setopt($ch, CURLOPT_POSTFIELDS, $subscribe); // set POST data
				
				    $output = curl_exec($ch); // execute and capture response
				    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
				 //   if(curl_exec($ch) === false){
					//     echo 'Curl error: ' . curl_error($ch);
					// }else{
					//     echo 'Operation completed without any errors';
					// }
				    curl_close($ch);
					$Count = $Count + count($value['email']);

				    $sync_status = true;
				    $error_message = array();

				    $json_a=json_decode($output,true);

				    if(isset($json_a['type']) && !empty($json_a['type'])){
				    	$sync_status = false;
				    	if($json_a['status'] == 401){
				    	$this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Error: " . $json_a['title'] . "   ,Code: ". $json_a['status']  ."<br>" .  $json_a['detail']));
							$this->_redirect($this->router->assemble(array(), 'settingsMailingList'));
							break;
							}else{
								$sync_status = false;
								$error_message = $json_a['status']  . " " . $json_a['title'] ."<br>" .  $json_a['detail'] . "<br>";
							}
				    }

				}								
			}

			if(isset($sync_status) && $sync_status == false){
				$this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Error: " . $error_message ."<br>"));
				$this->_redirect($this->router->assemble(array(), 'settingsMailingList'));
			}else{
				$this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Successful: ". $Count . " emails in " . $mailingList['name'] ." list was sync to mailChimp. "));
				$this->_redirect($this->router->assemble(array(), 'settingsMailingList'));
			}
		}

	}
	
    
	
}
?>