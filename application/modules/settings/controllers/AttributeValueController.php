<?php

class Settings_AttributeValueController extends Zend_Controller_Action {

    private $request;
    private $router;
    private $attribute_id;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
        $this->attribute_id = $this->request->getParam('attribute_id');

        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsServices';

        //
        //get attribute
        //
        $attributeObj = new Model_Attributes();
        $attribute = $attributeObj->getById($this->attribute_id);
        $this->view->attribute = $attribute;

        BreadCrumbs::setLevel(3, 'Values of ' . $attribute['attribute_name']);
        
        $pageName = $this->request->getParam('page_name', '');
        $this->view->page_title = (!empty($pageName))?$pageName." - Attribute Value Settings":"Attribute Value Settings";
    }

    /**
     * Items list action
     */
    public function indexAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsAttributeValueList'));

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'attribute_value_id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];


        //
        //get attribute value by id
        //
        $filters['attribute_id'] = $this->attribute_id;


        //
        // get data list
        //
        $attributeValueObj = new Model_AttributeListValue();
        $this->view->data = $attributeValueObj->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);

        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
        $this->view->attribute_id = $this->attribute_id;
    }
	
	public function getImagesAction(){
	  
	  
	 $id = $this->request->getParam('attribute_value_id', 0);
	 $modelAttributeListValueAttachment = new Model_AttributeListValueAttachment();
     $attributeListValueAttachemnts = $modelAttributeListValueAttachment->getByAttributeValueId($id);
	 
	 header('Content-type: text/json');              
     header('Content-type: application/json');
	 echo json_encode($attributeListValueAttachemnts);
	 exit;
	 
	}

    /**
     * Add new item action
     */
    public function addAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsAttributeValueAdd'));

        //
        // get request parameters
        //
        $attributeValue = $this->request->getParam('attribute_value');
        $unitPrice = $this->request->getParam('unit_price', 0);
        $extra_info = $this->request->getParam('extra_info', '');
		$default_image_id = $this->request->getParam('default_image_id', '');


        $router = Zend_Controller_Front::getInstance()->getRouter();

        //
        // init action form
        //
        $form = new Settings_Form_AttributeValue(array('attribute_id' => $this->attribute_id));

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $attributeValueObj = new Model_AttributeListValue();
				$attributeListValueAttachmentObj = new Model_AttributeListValueAttachment();
		        $attachmentObj = new Model_Attachment();
                $data = array(
                    'attribute_value' => $attributeValue,
                    'unit_price' => $unitPrice,
                    'extra_info' => $extra_info,
                    'attribute_id' => $this->attribute_id
                );

                $success = $attributeValueObj->insert($data);

                if ($success) {
				    
					if(isset($default_image_id) && !empty($default_image_id)) {
                        $data_attributeListValueAttachment = array(
                            'attachment_id' => $default_image_id,
                            'attribute_list_value_id'=> $success,
                            'is_default'=> 1
                            );

                        $attributeListValueAttachmentObj->insert($data_attributeListValueAttachment);
                        }
						
				    $upload = new Zend_File_Transfer_Adapter_Http();
                    $files  = $upload->getFileInfo();
					$counter = 0 ;
					foreach($files as $file => $fileInfo) {
					  if ($upload->isUploaded($file)){
                        if ($upload->receive($file)){
						   $counter = $counter + 1;
						   $info = $upload->getFileInfo($file);
                           $source  = $info[$file]['tmp_name'];
                           $imageInfo = pathinfo($source);
                           $ext = $imageInfo['extension'];
						   $size = $info[$file]['size'];
						   $type = $info[$file]['type'];
				           $dir = get_config('attachment') . '/';
                           $subdir = date('Y/m/d/');
                           $fullDir = $dir . $subdir;
                           if (!is_dir($fullDir)) {
                              mkdir($fullDir, 0777, true);
                           }
                        $loggedUser = CheckAuth::getLoggedUser();							
						$data = array(
							 'created_by'=>$loggedUser['user_id'] 
							 ,'created'=>time()
							 ,'size' => $size
							 ,'type'=>$type
						 );
						 
						
						 
						 $id = $attachmentObj->insert($data);
						 $attributeListValueAttachmentId = $attributeListValueAttachmentObj->insert(array('attribute_list_value_id'=>$success,'attachment_id'=>$id));
						 //$fileName = $success.'_'.$counter.'.'. $ext;
                         $fileName = $id.'_'.$counter.'_'.time().'.'. $ext;
						 $image_saved = copy($source, $fullDir . $fileName);
						 $Updatedata = array(
                          'path' => $fullDir . $fileName,
                          'file_name' => $fileName 
                          );
						 $attachmentObj->updateById($id,$Updatedata);
						 
						  if ($image_saved) {
								if (file_exists($source)) {
									unlink($source);
								}               
							}
						}
					 }	
					}
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Attribute saved"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                }

                //$this->_redirect($this->router->assemble(array(), 'settingsAttributeValueList'));
                echo 1;
                exit;
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('attribute-value/add_edit.phtml');
        exit;
    }

    /**
     * Edit an  item action
     */
    public function editAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsAttributeValueEdit'));

        $router = Zend_Controller_Front::getInstance()->getRouter();

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $attributeValue = $this->request->getParam('attribute_value');
        $unitPrice = $this->request->getParam('unit_price', 0);
        $extra_info = $this->request->getParam('extra_info', '');
		$default_image_id = $this->request->getParam('default_image_id', '');

        // get old data
        $modelAttributeListValue = new Model_AttributeListValue();
		$attributeListValueAttachmentObj = new Model_AttributeListValueAttachment();
		$attachmentObj = new Model_Attachment();
        $attributeListValue = $modelAttributeListValue->getById($id);
		$default_image_obj = $attributeListValueAttachmentObj->getByAttributeValueIdAndIsDefault($id);

        //
        // init action form
        //
        $form = new Settings_Form_AttributeValue(array('mode' => 'update', 'attributeListValue' => $attributeListValue));

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
				$deleted_ids = $this->request->getParam('deleted_ids','');
				$successDeleted = 0;
				if(!empty($deleted_ids)){
				 $deleted_ids = explode(",",$deleted_ids);
				 $modelAttachment = new Model_Attachment();
				 foreach($deleted_ids as $deleted_id){
                    $successDeleted = $modelAttachment->updateById($deleted_id , array('is_deleted' => '1'));
				 } 
				}
                $data = array(
                    'attribute_value' => $attributeValue,
                    'unit_price' => $unitPrice,
                    'extra_info' => $extra_info
                );

                $success = $modelAttributeListValue->updateById($id, $data);
				$attributeListValueNew = $modelAttributeListValue->getById($id);
                $updateAttachment = 0;				
				$upload = new Zend_File_Transfer_Adapter_Http();
                $files  = $upload->getFileInfo();
				$attributeAttachments = $attributeListValueAttachmentObj->getByAttributeValueId($id);
				$counter = count($attributeAttachments);
					foreach($files as $file => $fileInfo) {
					  if ($upload->isUploaded($file)){
                        if ($upload->receive($file)){
                           $counter = $counter + 1;						
						   $info = $upload->getFileInfo($file);						  
                           $source  = $info[$file]['tmp_name'];
                           $imageInfo = pathinfo($source);
                           $ext = $imageInfo['extension'];
						   $size = $info[$file]['size'];
						   $type = $info[$file]['type'];
				           $dir = get_config('attachment') . '/';
                           $subdir = date('Y/m/d/');
                           $fullDir = $dir . $subdir;
                           if (!is_dir($fullDir)) {
                              mkdir($fullDir, 0777, true);
                           }
                        $loggedUser = CheckAuth::getLoggedUser();							
						$data = array(
							 'created_by'=>$loggedUser['user_id'] 
							 ,'created'=>time()
							 ,'size' => $size
							 ,'type' =>$type
						 );
						 
						
						 
						 $Attachid = $attachmentObj->insert($data);
						 $attributeListValueAttachmentId = $attributeListValueAttachmentObj->insert(array('attribute_list_value_id'=>$id,'attachment_id'=>$Attachid));
						 //$fileName = $attributeListValueNew['attribute_value'].'_'.$counter.'.'. $ext;
                         $fileName = $Attachid.'_'.$counter.'_'.time().'.'. $ext;
						 $image_saved = copy($source, $fullDir . $fileName);
						 
						 $Updatedata = array(
                          'path' => $fullDir . $fileName,
                          'file_name' => $fileName
                          );
						  
						  
						 $updateAttachment = $attachmentObj->updateById($Attachid,$Updatedata);						 
						  if ($image_saved) {
								if (file_exists($source)) {
									unlink($source);
								}               
							}
						}
					 }	
					}
					
					 if($default_image_obj){
						   $attributeListValueAttachmentObj->updateById($default_image_obj['attribute_list_value_attachment_id'],array('attachment_id'=>$default_image_id));
						 }else{
							$attributeListValueAttachmentObj->insert(array('attribute_list_value_id'=>$id,'attachment_id'=>$default_image_id,'is_default'=>1));
						 }

                if ($success || $updateAttachment || $successDeleted || ($default_image_obj['attachment_id'] != $default_image_id)) {
				     
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Attribute saved"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                }
                echo 1;
                exit;
            }
        }

        $this->view->form = $form;
        $this->view->id = $id;
		$this->view->default_image = $default_image_obj;
		
        //
        // render views
        //
        echo $this->view->render('attribute-value/add_edit.phtml');
        exit;
    }

    public function deleteAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsAttributeValueDelete'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
        if ($id) {
            $ids[] = $id;
        }

        $attributeValueObj = new Model_AttributeListValue();
		$modelAttachment = new Model_Attachment();
     
        foreach ($ids as $id) {

            $isNotRelated = $attributeValueObj->checkBeforeDeleteServiceAttributeValue($id);

            if ($isNotRelated) {
                $isFloorAndHaveAttachment = $attributeValueObj->checkIfFloorTypeAndHaveAttachment($id);
                if ($isFloorAndHaveAttachment) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Failed to delete, item in use"));
                } else {
				    $modelAttachment = new Model_Attachment();
				    $attributeListValueAttachmentObj = new Model_AttributeListValueAttachment();
				    $attributeAttachments = $attributeListValueAttachmentObj->getByAttributeValueId($id,true);
					foreach($attributeAttachments as $attributeAttachment){
                      $success = $modelAttachment->updateById($attributeAttachment['attachment_id'] , array('is_deleted' => '1'));					  
					}
                    $attributeValueObj->deleteById($id);
                }
            } else {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Failed to delete, item in use"));
            }
        }
        $this->_redirect($this->router->assemble(array('attribute_id' => $this->attribute_id), 'settingsAttributeValueList'));
    }
	
		/******Image for AttributeValue***IBM*/
    public function attributeValueImageAction() {
	
    	CheckAuth::checkPermission(array('settingsAttributeValueAdd'));
    	$id = $this->request->getParam('attribute_value_id' , 0);
    	$attributeValueObj = new Model_AttributeListValue();
		$attributeListValueAttachmentObj = new Model_AttributeListValueAttachment();
        $attachmentObj = new Model_Attachment();

        if ($this->request->isPost()) {

            $upload = new Zend_File_Transfer_Adapter_Http();

            $files = $upload->getFileInfo();
            $countFiles = count($files);
            
            foreach ($files as $file => $fileInfo) {
            	
                if ($upload->isUploaded($file)) {                	
                    if ($upload->receive($file)) {
                    	$counter = $countFiles++ ;	
                        $info = $upload->getFileInfo($file);
                        $source = $info[$file]['tmp_name'];
                        $imageInfo = pathinfo($source);
                        $ext = $imageInfo['extension'];
                        $size = $info[$file]['size'];
                        $type = $info[$file]['type'];
                        $file_name = $info[$file]['name'];
                        $dir = get_config('attachment') . '/';
                        $dir2 = get_config('attachment_medium') . '/';
                        $subdir = date('Y/m/d/');
                        $fullDir = $dir . $subdir;
                        $fullDir2 = $dir2 . $subdir;
                        $loggedUser = CheckAuth::getLoggedUser();                           
                        $data = array(
                             'created_by'=>$loggedUser['user_id'] 
                             ,'created'=>time()
                             ,'size' => $size
                             ,'type' =>$type
                         );
                                            

                        $Attachid = $attachmentObj->insert($data);
                        $fileName = 'default_image_'.$Attachid.'.'. $ext;

                       if (!is_dir($fullDir)) {
                              mkdir($fullDir, 0777, true);
                           }
                           if (!is_dir($fullDir2)) {
                              mkdir($fullDir2, 0777, true);
                           }
						$image_saved = copy($source, $fullDir . $fileName);
                        ImageMagick::create_thumbnail($source, $fullDir2 . $fileName, 78, 64);

                        if ($image_saved) {

                        $Updatedata = array(
                          'path' => $fullDir . $fileName,
                          'file_name' => $fileName,
                          'thumbnail_file' => $fullDir2 . $fileName
                          );
                          
                         $updateAttachment = $attachmentObj->updateById($Attachid,$Updatedata);
                        
                         	if (file_exists($source)) {
								unlink($source);
							}
                    		$full_path =  $fullDir . $fileName;
                            $thumb_path = $fullDir2 . $fileName;

                            $image = '<img width="180" height="150" class="class="img-responsive alt="Image" style="border-radius: 10%;" src="'  . $this->getRequest()->getBaseUrl() . '/uploads/attachment_files/thumb_medium/' . $subdir . $fileName . '" />
							<div id="loader_view" style="display: none;">
                            <img class="img-responsive" src="'.$this->getRequest()->getBaseUrl().'/pic/loading-4.gif'.'" />
                            </div>';  

	                    		$path_data = array(
	                    			'image' => $image,
									'path' => $full_path,
                                    'thumb' => $thumb_path,
                                    'attachment_id' => $Attachid
									);
	                    	
                    		 echo json_encode($path_data);

                        	           
                        }
                    }
                }
            }
            
        }

        exit;
    }
    /**********END************/

}

