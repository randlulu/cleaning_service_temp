<?php

class Settings_CountriesController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsLocation';

	// BreadCrumbs::setLevel(1, 'Settings');
        BreadCrumbs::setLevel(2, 'Countries');
    }

    /**
     * Items list action
     */
    public function indexAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsCountriesList'));
        
        $pageName = $this->request->getParam('page_name', '');
        $this->view->page_title = (!empty($pageName))?$pageName." - Countries Settings":"Countries Settings";

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'country_id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = 20;
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        //
        // get data list
        //
        $countriesObj = new Model_Countries();
        $this->view->data = $countriesObj->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);

        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
    }

    /**
     * Add new item action
     */
    public function addAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsCountriesAdd'));

        //
        // get request parameters
        //
        $name = $this->request->getParam('name');
        $code = $this->request->getParam('code');
        $router = Zend_Controller_Front::getInstance()->getRouter();

        //
        // init action form
        //
        $form = new Settings_Form_Countries();

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $countriesObj = new Model_Countries();
                $data = array(
                    'country_name' => $name,
                    'country_code' => $code
                );

                $success = $countriesObj->insert($data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                }
                //$this->_redirect($this->router->assemble(array(), 'settingsCountriesList'));
                echo 1;
                exit;
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('countries/add_edit.phtml');
        exit;
    }

    public function deleteAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsCountriesDelete'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
        if ($id) {
            $ids[] = $id;
        }

        $countriesObj = new Model_Countries();
        foreach ($ids as $id) {

            $isNotRelated = $countriesObj->checkBeforeDeleteCountry($id);
            if ($isNotRelated) {
                $countriesObj->deleteById($id);
            } else {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Failed to delete, item in use"));
            }
        }
        $this->_redirect($this->router->assemble(array(), 'settingsCountriesList'));
    }

    public function editAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsCountriesEdit'));

        //
        // get request parameters
        //
        $code = $this->request->getParam('code');
        $name = $this->request->getParam('name');
        $id = $this->request->getParam('id');


        //
        // validation
        //
        $countriesObj = new Model_Countries();
        $countries = $countriesObj->getById($id);
        if (!$countries) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect($this->router->assemble(array(), 'settingsCountriesList'));
            return;
        }


        //
        // init action form
        //
        $form = new Settings_Form_Countries(array('mode' => 'update', 'countries' => $countries));


        //
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'country_name' => $name,
                    'country_code' => $code
                );

                $success = $countriesObj->updateById($id, $data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                }
                //$this->view->successMessage = 'Updated Successfully';
                echo 1;
                exit;
                //$this->_redirect($this->router->assemble(array(), 'settingsCountriesList'));
                //return;
            }
        }

        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('countries/add_edit.phtml');
        exit;
    }

}

