<?php

class Settings_TradingNamesController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsUser';

        BreadCrumbs::setLevel(2, 'Trading Names');
    }

    /**
     * Items list action
     */
    public function indexAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsCompaniesTradingNames'));
        
        $pageName = $this->request->getParam('page_name', '');
        $this->view->page_title = (!empty($pageName))?$pageName." - Trading Names Settings":"Trading Names Settings";

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'trading_name_id');
        $company_orderBy = $this->request->getParam('sort', 'company_id');
        $sortingMethod = $this->request->getParam('method', 'desc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());


        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        $trading_namesObj = new Model_TradingName();
        $companyObje = New Model_Companies();
        $this->view->data = $trading_namesObj->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);
        $this->view->company = $companyObje->getAll($filters, "{$company_orderBy} {$sortingMethod}", $pager);

        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
    }

    /**
     * 27/05/2015 D.A
     * get trading name by ID action
     */
    public function tradingNamesTemplateImagesAction() {
        //
        // check Auth for logged user
        //
        //CheckAuth::checkPermission(array('settingsCompaniesTradingNames'));

        $trading_id = $this->_request->getParam('trading_id');
        $template_id = $this->_request->getParam('template_id');

        $tradingNameImgs=array();
        $tradingNameObj = new Model_TradingName();
        $result = $tradingNameObj->getById($trading_id);

        $trading_name_folder= str_ireplace(" ","_", $result['trading_name']);

        $modelEmailTemplate = new Model_EmailTemplate();
        $emailTemplate = $modelEmailTemplate->getById($template_id);


        $tradingNameImages = new Model_TradingNameImages();
        $tradingNameImgarray=$tradingNameImages->getByTradingnameIdandTemplateId($trading_id,$template_id);

        if($tradingNameImgarray){
            foreach ($tradingNameImgarray as $tradingNameImage) {
                $tradingNameImgs[]=$tradingNameImage['trading_name_img'];
            }
        }

        $results = array(
            'trading_name' =>$trading_name_folder,
            'email_template' =>$emailTemplate['name'],
            'tradingNameImages' =>$tradingNameImgs
        );
        echo $this->_helper->json($results);exit;
    }

    /**
     * 27/05/2015 D.A
     * Add new item action
     */
    public function addAction() {

        //
        // check Auth for logged user
        //
        //CheckAuth::checkPermission(array('settingsCompaniesTradingNamesAdd'));

        $router = Zend_Controller_Front::getInstance()->getRouter();

        //
        // init action form
        //
        $form = new Settings_Form_TradingNames();
        $this->view->form = $form;
        //
        // handling the insertion process
        //
        if ($this->request->isPost()) {

            $companyId = $this->request->getParam('company_id');
            $trading_name = $this->request->getParam('trading_name');
            $isDefault = $this->request->getParam('isDefault');
            $website_url = $this->request->getParam('website_url');
            $color = $this->request->getParam('pick-a-color-0');
            $phone = $this->request->getParam('phone');
            $email = $this->request->getParam('email');

            if ($form->isValid($this->request->getPost())) { // validate form data


                $tradingNameObj = new Model_TradingName();
                $data = array(
                    'company_id' => $companyId,
                    'trading_name' => $trading_name,
                    'is_default' => $isDefault,
                    'website_url' => $website_url,
                    'phone' => $phone,
                    'email' => $email,
                    'color' => $color
                );

                if ($_FILES) {
                    $dir = get_config('trading_names');
                    $subdir = 'Logos';
                    $fullDir = $dir . '/' . $subdir . '/';

                    if (!is_dir($fullDir)) {
                        mkdir($fullDir, 0777, true);
                    }

                    foreach ($_FILES as $key => $files) {

                        if (move_uploaded_file($files['tmp_name'], $fullDir . basename($files['name']))) {
                            $data['logo'] = $files['name'];
                        }
                    }
                }

                $success = $tradingNameObj->insert($data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                }
               echo 1;
               exit;
            }
        }

        echo $this->view->render('trading-names/add_edit.phtml');
        exit;
    }

    /**
     * 09/06/2015 D.A
     * Update exist item action
     */
    public function editAction() {
        //
        // check Auth for logged user
        //
        //CheckAuth::checkPermission(array('settingsCompaniesTradingNamesEdit'));

        $companyId = $this->request->getParam('company_id');
        $trading_name = $this->request->getParam('trading_name');
        $isDefault = $this->request->getParam('isDefault');
        $website_url = $this->request->getParam('website_url');
        $color = $this->request->getParam('pick-a-color-0');
        $phone = $this->request->getParam('phone');
        $email = $this->request->getParam('email');
        $id = $this->request->getParam('id');

        $trading_namesObj = new Model_TradingName();
        $trading_names = $trading_namesObj->getById($id);
        if (!$trading_names) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect($this->router->assemble(array(), 'settingsCompaniesTradingNames'));
            return;
        }
         $this->view->id = $trading_names['trading_name_id'];
        $this->view->logo = $trading_names['logo'];
        $last_trading_name = $trading_names['trading_name'];
        $form = new Settings_Form_TradingNames(array('mode' => 'update', 'trading_names' => $trading_names, 'company_id' => $companyId));
        $this->view->form = $form;

        if ($this->request->isPost()) {

            if ($form->isValid($this->request->getPost())) {

                $data = array(
                    'company_id' => $companyId,
                    'trading_name' => $trading_name,
                    'is_default' => $isDefault,
                    'website_url' => $website_url,
                    'phone' => $phone,
                    'email' => $email,
                    'color' => $color
                );


                if ($_FILES) {
                    $dir = get_config('trading_names');
                    $subdir = 'Logos';
                    $fullDir = $dir . '/' . $subdir . '/';

                    if (!is_dir($fullDir)) {
                        mkdir($fullDir, 0777, true);
                    }

                    foreach ($_FILES as $key => $files) {
                        if (move_uploaded_file($files['tmp_name'], $fullDir . basename($files['name']))) {
                            $data['logo'] = $files['name'];
                        }
                    }
                }

                $trading_namesObj = new Model_TradingName();
                $success = $trading_namesObj->updateById($id, $data);
                $last_trading_name_folder= str_ireplace(" ","_", $last_trading_name);
                $trading_name_folder= str_ireplace(" ","_", $trading_name);

                if ($success) {
                    if($last_trading_name != $trading_name ){
                        $dir = get_config('trading_names') . '/';
                        $oldName = $dir . $last_trading_name_folder;
                        $newName = $dir . $trading_name_folder;
                        if (is_dir($oldName)) {
                            rename($oldName,$newName);
                        }
                    }

                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                }
                echo 1;
                exit;
            }
        }

        echo $this->view->render('trading-names/add_edit.phtml');
        exit;
    }

    /**
     * 27/05/2015 D.A
     * Delete item action
     */
    public function deleteAction() {

        //
        // check Auth for logged user
        //
        // CheckAuth::checkPermission(array('settingsCompaniesTradingNamesDelete'));
        //
        // get request parameters
        //
        $id = $this->request->getParam('id');

        $tradingNameObjs = new Model_TradingName();
        $tradingName = $tradingNameObjs->getById($id);
        $trading_name_folder= str_ireplace(" ","_", $tradingName['trading_name']);

        if ($tradingName) {

            $dir=get_config('trading_names') . '/' . $trading_name_folder;
            if (is_dir($dir)) {
            $di = new RecursiveDirectoryIterator($dir, FilesystemIterator::SKIP_DOTS);
            $ri = new RecursiveIteratorIterator($di, RecursiveIteratorIterator::CHILD_FIRST);
            foreach ( $ri as $file ) {
                $file->isDir() ?  rmdir($file) : unlink($file);
            }
            rmdir($dir);
            }
        }

        $tradingNameObj = new Model_TradingName();
        $success = $tradingNameObj->deleteById($id);
        if ($success) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Deleted"));
        } else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Error: Failed to delete, item in use"));
        }

        $this->_redirect($this->router->assemble(array(), 'settingsCompaniesTradingNames'));
    }

    /**
     * 09/06/2015 D.A
     * Add images of item action
     */
    public function assignImagesAction() {

        //
        // check Auth for logged user
        //
        //CheckAuth::checkPermission(array('settingsCompaniesTradingNamesAdd'));

        $trading_name_id = $this->request->getParam('id');
        $router = Zend_Controller_Front::getInstance()->getRouter();
        $form = new Settings_Form_TradingNamesImages(array('mode' => 'add', 'trading_name_id' => $trading_name_id));
        $this->view->form = $form;

        $trading_namesObj = new Model_TradingName();
        $trading_names = $trading_namesObj->getById($trading_name_id);
        $trading_name= $trading_names['trading_name'];
        $trading_name_folder= str_ireplace(" ","_",$trading_name);

        if ($this->request->isPost()) {
            $template_id = $this->request->getParam('template_name');
            $modelEmailTemplate = new Model_EmailTemplate();
            $emailTemplate = $modelEmailTemplate->getById($template_id);

            if ($this->request->getPost()) {
                $dir = get_config('trading_names') . '/';
                $fullDir = $dir . $trading_name_folder  .'/'. $emailTemplate['name'] ;
                if (!is_dir($fullDir)) {
                    mkdir($fullDir, 0775, true);
                }

                $success='';
                foreach ($_FILES as $key => $value){
                    $tmp_name = $_FILES[$key]["tmp_name"];
                    $name = $_FILES[$key]["name"];
                    $file_ext     = strtolower(strrchr($name,'.'));

                    $imgName =  $key . $file_ext;
                    move_uploaded_file($tmp_name, "$fullDir/$imgName");
                    $tradingNameImages = new Model_TradingNameImages();
                    $data = array(
                        'trading_name_id' => $trading_name_id,
                        'template_id' => $template_id,
                        'trading_name_img' => $imgName
                    );
                    $success = $tradingNameImages->insert($data);
                }
                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Images saved"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                }
                echo 1;
                exit;
            }
        }
        echo $this->view->render('trading-names/assigm_images.phtml');
        exit;
    }

    /**
     * 09/06/2015 D.A
     * Update images of item action
     */
    public function updateTrdadingNameImageAction() {
        //
        // check Auth for logged user
        //
        //CheckAuth::checkPermission(array('settingsCompaniesTradingNamesEdit'));

        $id = $this->request->getParam('img_id');
		
        $img='';
        $tradingName='';
        $tradingNameImages = new Model_TradingNameImages();
        $result=$tradingNameImages->getById($id);
		
        if ($result) {
            $img = $result['trading_name_img'];
            $imgname=explode('.', $result['trading_name_img']);
            $imgs_name= $imgname[0];
            $trading_name_id=  $result['trading_name_id'];
            $tradingNameObjs = new Model_TradingName();
            $tradingNameresult = $tradingNameObjs->getById($trading_name_id);
            if ($tradingNameresult) {
                $tradingName=$tradingNameresult['trading_name'];
            }
        }

        $modelEmailTemplate = new Model_EmailTemplate();
        $emailTemplate = $modelEmailTemplate->getById($result['template_id']);

        $trading_name_folder= str_ireplace(" ","_",$tradingName);

        $dir = get_config('trading_names') . '/';
        $fullDir = $dir . $trading_name_folder .'/'.$emailTemplate['name'];
        if (!is_dir($fullDir)) {
            mkdir($fullDir, 0775, true);
        }
		
        foreach ($_FILES as $key => $value){
            $tmp_name = $_FILES[$key]["tmp_name"];
            $name = $_FILES[$key]["name"];
            $file_ext     = strtolower(strrchr($name,'.'));

            $imgName =  $imgs_name . $file_ext;
                $data = array(
                    'trading_name_img' => $imgName
                );

            
             $tradingNameImages->updateById($id, $data);

            if (file_exists(get_config('trading_names') . '/' . $trading_name_folder.'/'.$emailTemplate['name'])) {
              unlink(get_config('trading_names') . '/' .$trading_name_folder.'/'.$emailTemplate['name']. '/' .$img );
           }
              move_uploaded_file($tmp_name, "$fullDir/$imgName");
        }
		echo '/uploads/trading_names_img/'.$trading_name_folder.'/'.$emailTemplate['name'].'/'.$imgName;
        //echo $this->view->render('trading-names/assigm_images.phtml');
        exit;
    }

    /**
     * 27/05/2015 D.A
     * Delete images of item action
     */
    public function deleteTrdadingNameImageAction() {
        //
        // check Auth for logged user
        //
        //CheckAuth::checkPermission(array('settingsCompaniesTradingNamesDelete'));

        $id = $this->request->getParam('img_id');
        $img='';
        $tradingName='';
        $tradingNameImages = new Model_TradingNameImages();
        $result=$tradingNameImages->getById($id);
        if ($result) {
            $img = $result['trading_name_img'];
            $trading_name_id=  $result['trading_name_id'];
            $tradingNameObjs = new Model_TradingName();
            $tradingNameresult = $tradingNameObjs->getById($trading_name_id);
            if ($tradingNameresult) {
                $tradingName=$tradingNameresult['trading_name'];
            }
        }
		 $trading_name_folder= str_ireplace(" ","_",$tradingName);
        $modelEmailTemplate = new Model_EmailTemplate();
        $emailTemplate = $modelEmailTemplate->getById($result['template_id']);

        $tradingNameImages = new Model_TradingNameImages();
        $success =$tradingNameImages->deleteById($id);

        if ($success) {
           if (file_exists(get_config('trading_names') . '/' . $trading_name_folder.'/'.$emailTemplate['name']. '/' .$img)) {
                unlink(get_config('trading_names') . '/' . $trading_name_folder.'/'.$emailTemplate['name']. '/' .$img );
            }
            $msg='Image deleted';
        } else {
            $msg='Error: Failed to delete, item in use';
        }

        $result= array(
            'msg' =>$msg,
        );
        echo $this->_helper->json($result);exit;
    }

    public function removeLogoAction() {
        $logo = $this->request->getParam('logo');
        $id = $this->request->getParam('id');
        $dir = get_config('trading_names');
        $subdir = 'Logos';
        $fullDir = $dir . '/' . $subdir . '/';
        if (!unlink($fullDir . '' . $logo)) {
            echo "Erro .. Logo Not Reomved";
        } else {
            $tradingNameObj = new Model_TradingName();
            $data = array(
                'logo'=>''
            );
            $result = $tradingNameObj->updateById($id, $data);
            echo "Logo Removed";
        }
        exit;
    }

}
