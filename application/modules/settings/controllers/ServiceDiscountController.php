<?php

class Settings_ServiceDiscountController extends Zend_Controller_Action {

    private $request;
    private $router;
	private $service_id;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsServicesDiscounts';

        BreadCrumbs::setLevel(2, 'Discount Ranges List');
		
		$this->service_id = $this->request->getParam('service_id');
		$Model_Services = new Model_Services();
        $Services = $Model_Services->getById($this->service_id);
        
        $pageName = $this->request->getParam('page_name', '');
        $this->view->page_title = (!empty($pageName))?$pageName." - Service Discount Settings":"Service Discount Settings";
    }
	
	 public function indexAction() {

        //
        // check Auth for logged user
        //
        //CheckAuth::checkPermission(array('settingsDiscountRangeList'));

        //$modelServiceAttachment = new Model_ServiceAttachment();
        //$modelServiceAttachment->executeQuery();
        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());
		
		$filters['service_id'] = $this->service_id;

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        //
        // get data list
        //
        $serviceDiscountObj = new Model_ServiceDiscount();
        $this->view->data = $serviceDiscountObj->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);

        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
		$this->view->service_id = $this->service_id;
    }
	
	
	 public function deleteAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingDiscountRangeDelete'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
        if ($id) {
            $ids[] = $id;
        }

        $serviceDiscountObj = new Model_ServiceDiscount();
        foreach ($ids as $id) {
		  $serviceDiscountObj->deleteById($id);
            if (CheckAuth::checkIfCanHandelAllCompany('service', $id)) {
               $serviceDiscountObj->deleteById($id); 
            }
        }
        $this->_redirect($this->router->assemble(array('service_id' => $this->service_id), 'settingsDiscountRangeList'));
    }
	
	
	public function addAction(){
	  
	  //CheckAuth::checkPermission(array('settingsDiscountRangeAdd'));

        //
        // get request parameters
        //
        $service_id = $this->request->getParam('service_id');
        $min_size_range = $this->request->getParam('min_size_range');
        $max_size_range = $this->request->getParam('max_size_range');
        $min_discount_range = $this->request->getParam('min_discount_range');
        $max_discount_range = $this->request->getParam('max_discount_range');
		

        $modelServiceDiscount = new Model_ServiceDiscount();

        //
        // init action form
        //
        $form = new Settings_Form_ServiceDiscount(array('service_id'=>$service_id));


        //
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                
               $data = array(
                    'min_size_range' => $min_size_range,
                    'max_size_range' => $max_size_range,
                    'min_discount_range' => $min_discount_range,
                    'max_discount_range' => $max_discount_range,
					'service_id' => $service_id
                );

                $success = $modelServiceDiscount->insert($data);

                
                if ($success) {
                   $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Service Discount"));
                }

                echo 1;
                exit;
                
            }
        }

        $this->view->form = $form;
        // render views
        //
        echo $this->view->render('service-discount/add-edit.phtml');
        exit; 
	
	
	}
	
	public function editAction(){
	
	  //
        // check Auth for logged user
        //
        //CheckAuth::checkPermission(array('settingsDiscountRangeEdit'));

        //
        // get request parameters
        //
        $service_id = $this->request->getParam('service_id');
        $min_size_range = $this->request->getParam('min_size_range');
        $max_size_range = $this->request->getParam('max_size_range');
        $max_discount_range = $this->request->getParam('max_discount_range');
        $min_discount_range = $this->request->getParam('min_discount_range');
		
        $id = $this->request->getParam('id');

        $modelServiceDiscount = new Model_ServiceDiscount();

        //
        // validation
        //
        $serviceDiscount = $modelServiceDiscount->getById($id);
        if (!$serviceDiscount) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect($this->router->assemble(array(), 'settingsServicesList'));
            return;
        }


        //
        // init action form
        //
        $form = new Settings_Form_ServiceDiscount(array('mode' => 'update', 'service_discount' => $serviceDiscount,'service_id'=>$service_id));


        //
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                
               $data = array(
                    'min_size_range' => $min_size_range,
                    'max_size_range' => $max_size_range,
                    'min_discount_range' => $min_discount_range,
                    'max_discount_range' => $max_discount_range
                );

                $success = $modelServiceDiscount->updateById($id, $data);

                
                if ($success) {
                   $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Service Discount"));
                }

                echo 1;
                exit;
                
            }
        }

        $this->view->form = $form;
        $this->view->id = $id;
        echo $this->view->render('service-discount/add-edit.phtml');
        exit;
	 
	
	}
	
	
	

	
}	