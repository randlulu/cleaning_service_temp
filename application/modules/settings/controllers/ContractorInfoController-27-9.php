<?php

class Settings_ContractorInfoController extends Zend_Controller_Action {

    private $request;
    private $router;
    private $contractor_id;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
        $this->contractor_id = $this->request->getParam('contractor_id');

        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsUser';

        //
        // get data list
        //
		
		
		
        $modelContractorInfo = new Model_ContractorInfo();
        $contractorInfo = $modelContractorInfo->getByContractorId($this->contractor_id);



        BreadCrumbs::setLevel(3, 'Contractor Info');
    }

    /**
     * Items list action
     */
    public function indexAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsContractorInfoList'));



        if (!CheckAuth::checkIfCanHandelAllCompany('user', $this->contractor_id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // get data list
        //
        $modelContractorInfo = new Model_ContractorInfo();
        $contractorInfo = $modelContractorInfo->getByContractorId($this->contractor_id);

		//by islam
		if(!$contractorInfo){
			$data = array(
				'contractor_id' => $this->contractor_id
			);
			$success = $modelContractorInfo->insert($data);
			$contractorInfo = $modelContractorInfo->getByContractorId($this->contractor_id);
		}
		//
				
		//
        //get user
        $userObj = new Model_User();
        $user = $userObj->getById($this->contractor_id);

        $line_address = get_line_address($user);
        $is_address = false;

        if (empty($line_address)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "There are no Address for this Booking"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        } else {
            $MAP_OBJECT_Booking = new GoogleMapAPI();

            $MAP_OBJECT_Booking->setHeight(200);
            $MAP_OBJECT_Booking->setWidth('100%');
            $MAP_OBJECT_Booking->setMapType('map');

            $MAP_OBJECT_Booking->addMarkerByAddress($line_address);

            $this->view->MAP_OBJECT_Booking = $MAP_OBJECT_Booking;
            $is_address = true;
        }
        $this->view->is_address = $is_address;
        $this->view->line_address = $line_address;
//        var_dump($line_address);
//        exit;
////        exit;
        $modelContractorRate = new Model_ContractorRate();
        $contractorRate = $modelContractorRate->getRateByContractor($this->contractor_id);

        if ($contractorInfo) {

            //get contractorOwner
            $contractorOwnerObj = new Model_ContractorOwner();
            $contractorOwner = $contractorOwnerObj->getAll(array('contractor_info_id' => $contractorInfo['contractor_info_id']));

            $this->view->contractorOwner = $contractorOwner;

            //get contractorEmployee
            $contractorEmployeeObj = new Model_ContractorEmployee();
            $contractorEmployee = $contractorEmployeeObj->getAll(array('contractor_info_id' => $contractorInfo['contractor_info_id']));

            $this->view->contractorEmployee = $contractorEmployee;

            //get DeclarationOfChemicals
            $declarationOfChemicalsObj = new Model_DeclarationOfChemicals();
            $declarationOfChemicals = $declarationOfChemicalsObj->getByContractorInfoId($contractorInfo['contractor_info_id']);

            $this->view->declarationOfChemicals = $declarationOfChemicals;

            $contractorInsuranceObj = new Model_ContractorInsurance();
            $contractorInsurances = $contractorInsuranceObj->getAll(array('contractor_info_id' => $contractorInfo['contractor_info_id']));

            $this->view->contractorInsurance = $contractorInsurances;

            //get DeclarationOfEquipment
            $declarationOfEquipmentObj = new Model_DeclarationOfEquipment();
            $declarationOfEquipment = $declarationOfEquipmentObj->getByContractorInfoId($contractorInfo['contractor_info_id']);

            $this->view->declarationOfEquipment = $declarationOfEquipment;

            //get DeclarationOfOtherApparatus
            $declarationOfOtherApparatusObj = new Model_DeclarationOfOtherApparatus();
            $declarationOfOtherApparatus = $declarationOfOtherApparatusObj->getByContractorInfoId($contractorInfo['contractor_info_id']);

            $this->view->declarationOfOtherApparatus = $declarationOfOtherApparatus;

            //get Vehicle
            $contractorVehicleObj = new Model_ContractorVehicle();
            $contractorVehicle = $contractorVehicleObj->getByContractorInfoId($contractorInfo['contractor_info_id']);

            $this->view->contractorVehicle = $contractorVehicle;
            //get working hours for each contractor 

            $model_workingHours = new Model_WorkingHours();
            $filters = array(
                'contractor_id' => $contractorInfo['contractor_id']
            );
            $working_hours = $model_workingHours->getAll($filters);
            $this->view->working_hours = $working_hours;

            // get insurance attachment
            $modelAttachment = new Model_Attachment();
            $filter = array('type' => 'insurance', 'itemid' => $contractorInfo['contractor_info_id']);
            $pager = null;
            $insuranceAttachments = $modelAttachment->getAll('a.created desc', $pager, $filter);
            if ($insuranceAttachments) {
                $this->view->insuranceAttachments = $insuranceAttachments;
            }




            $model_callLog = new Model_CallLog();
            $callLog = $model_callLog->getAll(array('contractor_id' => $this->contractor_id));
            $this->view->callLog = $callLog;
			
			//for sms to contractor
			$modelSmsHistory = new Model_SmsHistorty();
            $smsLog = $modelSmsHistory->getSmsByContractorId($this->contractor_id);
			$this->view->smsLog = $smsLog;
            $smsIncomingReplyLog = $modelSmsHistory->getSmsIncomingReplyByContractorId($this->contractor_id);
			$this->view->smsIncomeReplyLog = $smsIncomingReplyLog;


            // get licence attachment
            $filter = array('type' => 'licence', 'itemid' => $contractorInfo['contractor_info_id']);
            $pager = null;
            $licenceAttachments = $modelAttachment->getAll('a.created desc', $pager, $filter);
            if ($licenceAttachments) {
                $this->view->licenceAttachments = $licenceAttachments;
            }
            $paymentFilter = array('type' => 'payment', 'itemid' => $contractorInfo['contractor_info_id']);

            $paymentAttachments = $modelAttachment->getAll('a.created desc', $pager, $paymentFilter);
            if ($paymentAttachments) {
                $this->view->paymentAttachment = $paymentAttachments;
            }
        }

        $this->view->contractorInfo = $contractorInfo;
        $this->view->user = $user;
        $this->view->contractor_id = $this->contractor_id;
        $this->view->contractorRate = $contractorRate;

        //By Abdallah

        $modelBookingStatus = new Model_BookingStatus();
        $modelBooking = new Model_Booking();
        $BookingStatus = $modelBookingStatus->getAllWithoutPermission();
        $modelClaimOwner = new Model_ClaimOwner();
        $modelMissedCalls = new Model_MissedCalls();
        $modelComplaint = new Model_Complaint();
        $modelPayment = new Model_Payment();
        $modelBookingContractorPayment = new Model_BookingContractorPayment();
		$modelBookingInvoice = new Model_BookingInvoice();


        //$allContractorBooking = count($modelBooking->getAll(array('contractor_id'=>$this->contractor_id)));
        //$allContractorPaidBooking = count($modelBookingContractorPayment->getPaymentByContractorId($this->contractor_id));
        //$countUnpaidBooking = $allContractorBooking - $allContractorPaidBooking;


        $counts = array();
        foreach ($BookingStatus as $status) {
            $results = array();
            $total = $modelBooking->getcountBookingsByStatus($status['name'], $this->contractor_id);
            $results['name'] = $status['name'];
            $results['booking_status_id'] = $status['booking_status_id'];
            $results['color'] = $status['color'];
            $results['total'] = $total;
            $counts[] = $results;
        }

        /*******get devices info (that installed app and access to his account) for contractor********IBM*/

        $modelContractorMobileInfo = new Model_ContractorMobileInfo();
        $contractorMobileInfo = $modelContractorMobileInfo->getAllByContractorId($this->contractor_id);
        $this->view->contractorMobileInfo = $contractorMobileInfo;

        /*****End*****/
        
        $modelContractorGmailAccounts = new Model_ContractorGmailAccounts();
        $contractorAccounts = $modelContractorGmailAccounts->getByContractorId($this->contractor_id);

        $countAwaitingaccept = $modelBooking->getCountAwaitingAcceptBooking($this->contractor_id);
        $countAwaitingUpdate = $modelBooking->getCountAwaitingupdateBooking($this->contractor_id);
        $countUnapprovedBookingContractor = $modelBooking->getCountUnapprovedBooking($this->contractor_id);
        $countRejectBookingContractor = $modelBooking->getCountRejectedBookings($this->contractor_id);

        $countOpenComplaintForContractors = $modelComplaint->getComplaintCount(array('complaintStatus' => 'open', 'contractor_id' => $this->contractor_id));
        $countUnapprovedComplaintForContractors = $modelComplaint->countUapprovedCompalints($this->contractor_id);
        $countClaimOwnerContractor = $modelClaimOwner->getCountClaimOwner($this->contractor_id);
        $countMissedCallsContractor = $modelMissedCalls->getCountMissedCalls($this->contractor_id);
        $countUnapprovedPaymentsForContractor = $modelPayment->getCountUnapprovedPayments($this->contractor_id);
		$countUnpaidInvoiveForContractor = $modelBookingInvoice->getCount(array('invoice_type' => 'unpaid', 'contractor_id' => $this->contractor_id));


        $this->view->contractorInfo = $contractorInfo;
        $this->view->user = $user;
        $this->view->contractor_id = $this->contractor_id;
        $this->view->countAwaitingUpdate = $countAwaitingUpdate;
        $this->view->countAwaitingaccept = $countAwaitingaccept;
        $this->view->countUnapprovedBookingContractor = $countUnapprovedBookingContractor;
        $this->view->countRejectBookingContractor = $countRejectBookingContractor;
        $this->view->countUnapprovedPaymentsForContractors = $countUnapprovedPaymentsForContractor;
        $this->view->countClaimOwnerContractor = $countClaimOwnerContractor;
        $this->view->countMissedCallsContaractor = $countMissedCallsContractor;
        $this->view->countOpenComplaintForContractor = $countOpenComplaintForContractors;
        $this->view->countUnapprovedComplaintForContractors = $countUnapprovedComplaintForContractors;
        $this->view->bookingStatusCounts = $counts;
        $this->view->contractorGmailAccount = $contractorAccounts['email'];
		$this->view->countUnpaidInvoiveForContractor = $countUnpaidInvoiveForContractor;
        // $this->view->bookingStatus  = $BookingStatus ;
        //$this->view->countContractorServiceBooking = $modelContractorServiceBookingCounter;
        //$this->view->sumContractorServiceBooking = $modelContractorServiceBookingsum;
    }

    public function changeContractorStatusAction() {
        $modelContractorInfo = new Model_ContractorInfo();
        $contractor_id = $this->request->getParam('contractor_id');
        $status_id = $this->request->getParam('status_id');
        $status_text = $this->request->getParam('status_text');
        if (isset($status_id)) {
            $data = array(
                'status' => trim($status_id),
            );
        }
        if (isset($status_text)) {
            $data = array(
                'status_text' => $status_text
            );
        }
        $modelContractorInfo->updateByContractorId($contractor_id, $data);
        if (isset($status_id)) {
            echo json_encode(trim($status_id));
        } else {
            echo 1;
        }
        exit;
    }

    /**
     * Add new item action
     */
    public function addAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsContractorInfoAdd'));

        if (!CheckAuth::checkIfCanHandelAllCompany('user', $this->contractor_id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $modelContractorInfo = new Model_ContractorInfo();
        $contractorInfo = $modelContractorInfo->getByContractorId($this->contractor_id);
        if ($contractorInfo) {
            $this->_redirect($this->router->assemble(array('contractor_id' => $this->contractor_id, 'id' => $contractorInfo['contractor_info_id']), 'settingsContractorInfoEdit'));
        }


        //
        // get request parameters
        //
        $contractor_id = $this->request->getParam('contractor_id');
        $id = $this->request->getParam('id');
        $business_name = $this->request->getParam('business_name');
        $acn = $this->request->getParam('acn');
        $abn = $this->request->getParam('abn');
        $tfn = $this->request->getParam('tfn');
        $gst = $this->request->getParam('gst');
        $gst_date_registered = $this->request->getParam('gst_date_registered');


        $display_name = $this->request->getParam('display_name');
        $username = $this->request->getParam('username');
        $roleId = $this->request->getParam('role_id');
        $companyId = $this->request->getParam('company_id');
        $first_name = $this->request->getParam('first_name');
        $last_name = $this->request->getParam('last_name');



        if (isset($roleId)) {
            $modelAuthRole = new Model_AuthRole();
            $role = $modelAuthRole->getById($roleId);
            if ($role['role_name'] == 'super_admin' && !CheckAuth::checkCredential(array('canHandleAllCompanies'))) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You have entered an invalid option"));
                echo 1;
                exit;
            }
        }
        if (!CheckAuth::checkIfCanHandelAllCompany('user', $contractor_id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            echo 1;
            exit;
        }

        //
        // validation
        //
        $modelUser = new Model_User();
        $user = $modelUser->getById($contractor_id);




        if (!$user) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));

            $this->_redirect($this->router->assemble(array(), 'settingsUserList'));
            return;
        }

        //
        //get user company
        //
        $userCompaniesModel = new Model_UserCompanies();
        $userCompanies = $userCompaniesModel->getByUserId($contractor_id);



        $router = Zend_Controller_Front::getInstance()->getRouter();


        //
        // init action form
        //
        $form = new Settings_Form_ContractorInfo(array('contractor_id' => $this->contractor_id, 'user' => $user, 'company_id' => $userCompanies['company_id']));


        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $modelContractorInfo = new Model_ContractorInfo();
                $data = array(
                    'business_name' => $business_name,
                    'acn' => $acn,
                    'abn' => $abn,
                    'tfn' => $tfn,
                    'gst' => $gst,
                    'gst_date_registered' => strtotime($gst_date_registered),
                    'contractor_id' => $this->contractor_id
                );

                //save user information
                $userdata = array(
                    'display_name' => $display_name,
                    'username' => $username,
                    'role_id' => $roleId,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                );
                $success = $modelUser->updateById($contractor_id, $userdata);
                $userCompaniesModel = new Model_UserCompanies();
                $userCompanies = $userCompaniesModel->getByUserId($contractor_id);

                $dataCompany = array(
                    'user_id' => $contractor_id,
                    'company_id' => $companyId,
                    'created' => time()
                );
                if ($userCompanies) {
                    $userCompaniesModel->updateById($userCompanies['id'], $dataCompany);
                } else {
                    $userCompaniesModel->insert($dataCompany);
                }


                $success = $modelContractorInfo->insert($data);



                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                }
                //$this->_redirect($this->router->assemble(array(), 'settingsUserEmailList'));

                echo 1;
                exit;
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        
        echo $this->view->render('contractor-info/add_edit.phtml');
        exit;
    }

    public function deleteAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsContractorInfoDelete'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
        if ($id) {
            $ids[] = $id;
        }

        $modelContractorInfo = new Model_ContractorInfo();
        foreach ($ids as $id) {
            if (CheckAuth::checkIfCanHandelAllCompany('contractor_info', $id)) {
                $modelContractorInfo->deleteById($id);
            }
        }
        $this->_redirect($this->router->assemble(array('contractor_id' => $this->contractor_id), 'settingsContractorInfoList'));
    }

    public function editAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsContractorInfoEdit'));

        //
        // get request parameters
        //
        $contractor_id = $this->request->getParam('contractor_id');
        $id = $this->request->getParam('id');
        $business_name = $this->request->getParam('business_name');
        $acn = $this->request->getParam('acn');
        $abn = $this->request->getParam('abn');
        $tfn = $this->request->getParam('tfn');
        $gst = $this->request->getParam('gst');
        $gst_date_registered = $this->request->getParam('gst_date_registered');

        //$type = $this->request->getParam('contractorInfo' , 0);
        // get user information 
        $display_name = $this->request->getParam('display_name');
        $username = $this->request->getParam('username');
        $roleId = $this->request->getParam('role_id');
        $companyId = $this->request->getParam('company_id');
        $first_name = $this->request->getParam('first_name');
        $last_name = $this->request->getParam('last_name');

        if (isset($roleId)) {
            $modelAuthRole = new Model_AuthRole();
            $role = $modelAuthRole->getById($roleId);
            if ($role['role_name'] == 'super_admin' && !CheckAuth::checkCredential(array('canHandleAllCompanies'))) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You have entered an invalid option"));
                echo 1;
                exit;
            }
        }
        if (!CheckAuth::checkIfCanHandelAllCompany('user', $contractor_id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            echo 1;
            exit;
        }

        //
        // validation
        //
        $modelUser = new Model_User();
        $user = $modelUser->getById($contractor_id);


        if (!$user) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));

            $this->_redirect($this->router->assemble(array(), 'settingsUserList'));
            return;
        }

        //
        //get user company
        //
        $userCompaniesModel = new Model_UserCompanies();
        $userCompanies = $userCompaniesModel->getByUserId($contractor_id);


        //
        // validation
        //
        $modelContractorInfo = new Model_ContractorInfo();
        $contractorInfo = $modelContractorInfo->getById($id);
        if (!$contractorInfo) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect($this->router->assemble(array('contractor_id' => $this->contractor_id), 'settingsContractorInfoList'));
            return;
        }


        //
        // init action form
        //contractor_id
        $form = new Settings_Form_ContractorInfo(array('mode' => 'update', 'contractorInfo' => $contractorInfo, 'contractor_id' => $contractor_id, 'user' => $user, 'company_id' => $userCompanies['company_id']));


        //
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'business_name' => $business_name,
                    'acn' => $acn,
                    'abn' => $abn,
                    'tfn' => $tfn,
                    'gst' => $gst,
                    'gst_date_registered' => strtotime($gst_date_registered),
                );

                //save user information
                $userdata = array(
                    'display_name' => $display_name,
                    'username' => $username,
                    'role_id' => $roleId,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                );
				
                $userData_success = $modelUser->updateById($contractor_id, $userdata);
                $userCompaniesModel = new Model_UserCompanies();
                $userCompanies = $userCompaniesModel->getByUserId($contractor_id);

                $dataCompany = array(
                    'user_id' => $contractor_id,
                    'company_id' => $companyId,
                    'created' => time()
                );
                if ($userCompanies) {
                    $userCompaniesModel->updateById($userCompanies['id'], $dataCompany);
                } else {
                    $userCompaniesModel->insert($dataCompany);
                }



                $contractorInfo_success = $modelContractorInfo->updateByContractorId($contractor_id, $data);

                if ($userData_success || $contractorInfo_success) {

                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                }
                //$this->view->successMessage = 'Updated Successfully';
                echo 1;
                exit;
                //$this->_redirect($this->router->assemble(array(), 'settingsContractorInfoList'));
                //return;
            }
        }

        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('contractor-info/add_edit.phtml');
        exit;
    }

    /* public function editAction() {
      //
      // check Auth for logged user
      //
      CheckAuth::checkPermission(array('settingsContractorInfoEdit'));

      //
      // get request parameters
      //
      $id = $this->request->getParam('id');
      $business_name = $this->request->getParam('business_name');
      $acn = $this->request->getParam('acn');
      $abn = $this->request->getParam('abn');
      $tfn = $this->request->getParam('tfn');
      $gst = $this->request->getParam('gst');
      $gst_date_registered = $this->request->getParam('gst_date_registered');
      $insurance_policy_number = $this->request->getParam('insurance_policy_number');
      $insurance_policy_start = $this->request->getParam('insurance_policy_start');
      $insurance_policy_expiry = $this->request->getParam('insurance_policy_expiry');
      $insurance_listed_services_covered = $this->request->getParam('insurance_listed_services_covered');
      $drivers_licence_number = $this->request->getParam('drivers_licence_number');
      $drivers_licence_expiry = $this->request->getParam('drivers_licence_expiry');
      $bond_to_be_withheld = $this->request->getParam('bond_to_be_withheld');
      $commission = $this->request->getParam('commission');


      //
      // validation
      //
      $modelContractorInfo = new Model_ContractorInfo();
      $contractorInfo = $modelContractorInfo->getById($id);
      if (!$contractorInfo) {
      $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
      $this->_redirect($this->router->assemble(array(), 'settingsContractorInfoList'));
      return;
      }


      //
      // init action form
      //contractor_id
      $form = new Settings_Form_ContractorInfo(array('mode' => 'update', 'contractorInfo' => $contractorInfo, 'contractor_id' => $this->contractor_id));


      //
      // handling the updating process
      //
      if ($this->request->isPost()) { // check if POST request method
      if ($form->isValid($this->request->getPost())) { // validate form data
      $data = array(
      'business_name' => $business_name,
      'acn' => $acn,
      'abn' => $abn,
      'tfn' => $tfn,
      'gst' => $gst,
      'gst_date_registered' => strtotime($gst_date_registered),
      'insurance_policy_number' => $insurance_policy_number,
      'insurance_policy_start' => $insurance_policy_start,
      'insurance_policy_expiry' => $insurance_policy_expiry,
      'insurance_listed_services_covered' => $insurance_listed_services_covered,
      'drivers_licence_number' => $drivers_licence_number,
      'drivers_licence_expiry' => $drivers_licence_expiry,
      'bond_to_be_withheld' => $bond_to_be_withheld,
      'commission' => $commission,
      );

      $success = $modelContractorInfo->updateById($id, $data);

      if ($success) {

      $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved"));
      } else {
      $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
      }
      //$this->view->successMessage = 'Updated Successfully';
      echo 1;
      exit;
      //$this->_redirect($this->router->assemble(array(), 'settingsContractorInfoList'));
      //return;
      }
      }

      $this->view->form = $form;

      //
      // render views
      //
      echo $this->view->render('contractor-info/add_edit.phtml');
      exit;
      } */

    public function addGmailAccountsAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsContractorGmailAdd'));

        //create new model contractor
        $modelContractorGmailAccounts = new Model_ContractorGmailAccounts();
        $contractorAccounts = $modelContractorGmailAccounts->getByContractorId($this->contractor_id);

        if ($contractorAccounts) {
            $this->_redirect($this->router->assemble(array('contractor_id' => $this->contractor_id, 'id' => $contractorAccounts['id']), 'settingsContractorGmailEdit'));
        }


        //
        // get request parameters
        //
     
        $email = $this->request->getParam('email');
        $password = $this->request->getParam('password');


        //
        // init action form
        //
        $form = new Settings_Form_ContractorGmailAccounts(array('contractor_id' => $this->contractor_id));

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'contractor_id' => $this->contractor_id,
                    'email' => $email,
                    'password' => $password
                );

                $success = $modelContractorGmailAccounts->insert($data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                }

                echo 1;
                exit;
            }
        }

        $this->view->form = $form;
        //
        // render view  $router->addRoute('settingsContractorGmailAdd', new Zend_Controller_Router_Route('settings/contractor-info/add-gmail-accounts/:contractor_id', array('module' => 'settings', 'controller' => 'contractor-info', 'action' => 'add-gmail-accounts')));s
        //
        
        echo $this->view->render('contractor-info/add-edit-gmail-accounts.phtml');
        exit;
    }

    public function editGmailAccountsAction() {


        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsContractorGmailEdit'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id');
        $email = $this->request->getParam('email');
        $password = $this->request->getParam('password');


        //create new model contractor
        $modelContractorGmailAccounts = new Model_ContractorGmailAccounts();
        $contractorAccount = $modelContractorGmailAccounts->getById($id);

        //
        // init action form
        //
        $form = new Settings_Form_ContractorGmailAccounts(array('mode' => 'update', 'contractor_id' => $this->contractor_id, 'contractorAccount' => $contractorAccount));


        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) {

                $data = array(
                    'email' => $email,
                    'password' => $password
                );

                $success = $modelContractorGmailAccounts->updateById($contractorAccount['id'], $data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                }

                echo 1;
                exit;
            }
        }


        $this->view->form = $form;
        echo $this->view->render('contractor-info/add-edit-gmail-accounts.phtml');
        exit;
    }

    /* public function contractorPicAction() {



      $id = $this->request->getParam('contractor_id');
      if ($this->request->isPost()) {

      $upload = new Zend_File_Transfer_Adapter_Http();

      $files  = $upload->getFileInfo();
      $countFiles = count($files);


      foreach($files as $file => $fileInfo) {
      if ($upload->isUploaded($file)) {
      if ($upload->receive($file)) {
      $info = $upload->getFileInfo($file);
      $source  = $info[$file]['tmp_name'];
      $imageInfo = pathinfo($source);
      $ext = $imageInfo['extension'];
      $dir = get_config('contractor_picture');
      if (!is_dir($dir)) {
      mkdir($dir, 0777, true);
      }
      $original_path = time()."_".$id.'.'.$ext;
      $image_saved = copy($source, $dir.$original_path);
      $modelContractorInfo = new Model_ContractorInfo();
      if($image_saved){
      $data=array('profile_pic'=>$original_path);
      //print_r($data);
      //echo '$this->contractor_id   '.$this->contractor_id;
      if($modelContractorInfo->updateByContractorId($this->contractor_id,$data)){


      echo '<img class="img-responsive" src="'.$this->getRequest()->getBaseUrl().'/uploads/contractors_pic/'.$original_path.'" />';

      }

      }
      }
      }
      }
      }

      exit;

      } */

    // by abdallah

    public function contractorPicAction() {



        $id = $this->request->getParam('contractor_id');
        if ($this->request->isPost()) {

            $upload = new Zend_File_Transfer_Adapter_Http();

            $files = $upload->getFileInfo();
            $countFiles = count($files);


            foreach ($files as $file => $fileInfo) {
                if ($upload->isUploaded($file)) {
                    if ($upload->receive($file)) {
                        $info = $upload->getFileInfo($file);
                        $source = $info[$file]['tmp_name'];
                        $imageInfo = pathinfo($source);
                        $ext = $imageInfo['extension'];
                        $dir = get_config('contractor_picture');
                        $dir2 = get_config('contractor_picture_thumb');
                        $dir3 = get_config('contractor_picture_thumb_medium');

                        $dir4 = get_config('user_picture');
                        $dir5 = get_config('user_picture_medium');

                        if (!is_dir($dir)) {
                            mkdir($dir, 0777, true);
                        }
                        if (!is_dir($dir2)) {
                            mkdir($dir2, 0777, true);
                        }
                        if (!is_dir($dir3)) {
                            mkdir($dir3, 0777, true);
                        }
                        if (!is_dir($dir4)) {
                            mkdir($dir3, 0777, true);
                        }
                        if (!is_dir($dir5)) {
                            mkdir($dir3, 0777, true);
                        }
                        $original_path = time() . "_" . $id . '.' . $ext;
                        $image_saved = copy($source, $dir . $original_path);
                        $image_saved2 = copy($source, $dir4 . $original_path);
                        ImageMagick::create_thumbnail($source, $dir2 . $original_path, 50, 50);
                        ImageMagick::create_thumbnail($source, $dir3 . $original_path, 170, 170);
                        ImageMagick::create_thumbnail($source, $dir5 . $original_path, 170, 170);

                        $modelContractorInfo = new Model_ContractorInfo();
                        $modeluser = new Model_User();
                        if ($image_saved) {
                            $data = array('profile_pic' => $original_path);
                            $data1 = array('avatar' => $original_path);
                            //print_r($data);
                            //echo '$this->contractor_id   '.$this->contractor_id;
                            $modeluser->updateById($this->contractor_id, $data1);
                            if ($modelContractorInfo->updateByContractorId($this->contractor_id, $data)) {


                                echo '<img class="img-responsive" src="' . $this->getRequest()->getBaseUrl() . '/uploads/contractors_pic/thumb_medium/' . $original_path . '" />';
                            }
                        }
                    }
                }
            }
        }

        exit;
    }

    public function editBankAction() {

        // get request parameters
        $id = $this->request->getParam('contractor_id');



        $bank_name = $this->request->getParam('bank_name');
        $account_name = $this->request->getParam('account_name');
        $account_number = $this->request->getParam('account_number');
        $bsb = $this->request->getParam('bsb');

        $modelContractorInfo = new Model_ContractorInfo();
        $contractorInfo = $modelContractorInfo->getByContractorId($id);

        if (!$contractorInfo) {
			$data = array(
                    'contractor_id' => $id
                );
			$success = $modelContractorInfo->insert($data);
			$contractorInfo = $modelContractorInfo->getByContractorId($id);
			/*
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect($this->router->assemble(array('contractor_id' => $this->contractor_id), 'settingsContractorInfoList'));
            return;*/
        }
		

        $form = new Settings_Form_BankInfo(array('contractorInfo' => $contractorInfo, 'contractor_id' => $id));


        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data			  
                $data = array(
                    'bank_name' => $bank_name,
                    'account_name' => $account_name,
                    'account_number' => $account_number,
                    'bsb' => $bsb
                );

				
					$success = $modelContractorInfo->updateByContractorId($id, $data);
				

                
                if ($success) {

                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                }
                echo 1;
                exit;
            }
        }


        $this->view->form = $form;
        $this->view->id = $contractorInfo['contractor_info_id'];

        //
        // render views
        //
        echo $this->view->render('contractor-info/edit_bank.phtml');
        exit;
    }

    public function editInsuranceAction() {


        // check Auth for logged user
        // get request parameters
        $id = $this->request->getParam('contractor_id');



        $business_name = $this->request->getParam('business_name');
        $insurance_policy_number = $this->request->getParam('insurance_policy_number');
        $insurance_policy_start = $this->request->getParam('insurance_policy_start');
        $insurance_policy_expiry = $this->request->getParam('insurance_policy_expiry');
        $insurance_listed_services_covered = $this->request->getParam('insurance_listed_services_covered');
        $insurance_type = $this->request->getParam('insurance_type');
        // validation
        //
        $modelContractorInfo = new Model_ContractorInfo();
        $modelContractorInfoAttachment = new Model_ContractorInfoAttachment();
        $attachmentObj = new Model_Attachment();
        $contractorInfo = $modelContractorInfo->getByContractorId($id);

        if (!$contractorInfo) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect($this->router->assemble(array('contractor_id' => $this->contractor_id), 'settingsContractorInfoList'));
            return;
        }

        $form = new Settings_Form_InsuranceInfo(array('contractorInfo' => $contractorInfo, 'contractor_id' => $id));


        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data			  
                $data = array(
                    'insurance_policy_number' => $insurance_policy_number,
                    'insurance_policy_start' => strtotime($insurance_policy_start),
                    'insurance_policy_expiry' => strtotime($insurance_policy_expiry),
                    'insurance_listed_services_covered' => $insurance_listed_services_covered,
                    'insurance_type' => $insurance_type,
                );

                $deleted_ids = $this->request->getParam('deleted_ids', '');
                $successDeleted = 0;
                if (!empty($deleted_ids)) {
                    $deleted_ids = explode(",", $deleted_ids);
                    $modelAttachment = new Model_Attachment();
                    foreach ($deleted_ids as $deleted_id) {
                        $successDeleted = $modelAttachment->updateById($deleted_id, array('is_deleted' => '1'));
                    }
                }
                $success = $modelContractorInfo->updateByContractorId($id, $data);

                $updateAttachment = 0;
                $upload = new Zend_File_Transfer_Adapter_Http();
                $files = $upload->getFileInfo();
                $Attachments = $modelContractorInfo->getAllAttachmentByTypeAndId($contractorInfo['contractor_info_id'], 'insurance');
                $counter = count($Attachments);
                foreach ($files as $file => $fileInfo) {
                    if ($upload->isUploaded($file)) {
                        if ($upload->receive($file)) {
                            $counter = $counter + 1;
                            $info = $upload->getFileInfo($file);
                            $source = $info[$file]['tmp_name'];
                            $imageInfo = pathinfo($source);
                            $ext = $imageInfo['extension'];
                            $size = $info[$file]['size'];
                            $type = $info[$file]['type'];
                            $dir = get_config('attachment') . '/';
                            $subdir = date('Y/m/d/');
                            $fullDir = $dir . $subdir;
                            if (!is_dir($fullDir)) {
                                mkdir($fullDir, 0777, true);
                            }
                            $loggedUser = CheckAuth::getLoggedUser();
                            $data = array(
                                'created_by' => $loggedUser['user_id']
                                , 'created' => time()
                                , 'size' => $size
                                , 'type' => $type
                            );



                            $Attachid = $attachmentObj->insert($data);
                            $contractorInfoAttachmentId = $modelContractorInfoAttachment->insert(array('contractor_info_id' => $contractorInfo['contractor_info_id'], 'attachment_id' => $Attachid, 'type' => 'insurance'));
                            $fileName = $contractorInfo['contractor_info_id'] . '_' . $counter . '_' . 'insurance' . '.' . $ext;
                            $image_saved = copy($source, $fullDir . $fileName);
                            $typeParts = explode("/", $type);
                            if ($typeParts[0] == 'image') {
                                $thumbName = $contractorInfo['contractor_info_id'] . '_' . $counter . '_' . 'insurance_thumbnail' . '.' . $ext;
                                ImageMagick::create_thumbnail($source, $fullDir . $thumbName, 78, 64);
                            } else {
                                $thumbName = $contractorInfo['contractor_info_id'] . '_' . $counter . '_' . 'insurance' . '.jpg';
                                ImageMagick::convertFile($source . '[0]', $fullDir . $thumbName);
                            }



                            $Updatedata = array(
                                'path' => $fullDir . $fileName,
                                'file_name' => $fileName,
                                'thumbnail_file' => $fullDir . $thumbName
                            );


                            $updateAttachment = $attachmentObj->updateById($Attachid, $Updatedata);
                            if ($image_saved) {
                                if (file_exists($source)) {
                                    unlink($source);
                                }
                            }
                        }
                    }
                }
                if ($success || $updateAttachment || $successDeleted) {

                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                }
                //$this->view->successMessage = 'Updated Successfully';
                echo 1;
                exit;
                //$this->_redirect($this->router->assemble(array(), 'settingsContractorInfoList'));
                //return;
            }
        }

        // init action form
        //contractor_id
        //
		//
        // handling the updating process
        //

		
	   $this->view->form = $form;
        $this->view->id = $contractorInfo['contractor_info_id'];

        //
        // render views
        //
        echo $this->view->render('contractor-info/edit_insurance.phtml');
        exit;
    }

    public function getAttacmentsAction() {


        $contractor_info_id = $this->request->getParam('contractor_info_id', 0);
        $type = $this->request->getParam('type', 0);
        $modelContractorInfo = new Model_ContractorInfo();
        $Attachemnts = $modelContractorInfo->getAllAttachmentByTypeAndId($contractor_info_id, $type);

        header('Content-type: text/json');
        header('Content-type: application/json');
        echo json_encode($Attachemnts);
        exit;
    }

    public function editLicenceAction() {

        // get request parameters
        $id = $this->request->getParam('contractor_id');

        $drivers_licence_number = $this->request->getParam('drivers_licence_number');
        $drivers_licence_expiry = $this->request->getParam('drivers_licence_expiry');
        $driver_licence_type = $this->request->getParam('driver_licence_type');

        // validation

        $modelContractorInfo = new Model_ContractorInfo();
        $modelContractorInfoAttachment = new Model_ContractorInfoAttachment();
        $attachmentObj = new Model_Attachment();
        $contractorInfo = $modelContractorInfo->getByContractorId($id);

        if (!$contractorInfo) {
			//by islam		
			$data = array(
				'contractor_id' => $id
			);
			$success = $modelContractorInfo->insert($data);
			$contractorInfo = $modelContractorInfo->getByContractorId($id);
		
           /* $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect($this->router->assemble(array('contractor_id' => $this->contractor_id), 'settingsContractorInfoList'));
            return;*/
        }


        $form = new Settings_Form_LicenceInfo(array('contractorInfo' => $contractorInfo, 'contractor_id' => $this->contractor_id));

        // init action form
        //contractor_id

        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data			  
                $data = array(
                    'drivers_licence_number' => $drivers_licence_number,
                    'driver_licence_type' => $driver_licence_type,
                    'drivers_licence_expiry' => strtotime($drivers_licence_expiry),
                );


                $deleted_ids = $this->request->getParam('deleted_ids', '');
                $successDeleted = 0;
                if (!empty($deleted_ids)) {
                    $deleted_ids = explode(",", $deleted_ids);
                    $modelAttachment = new Model_Attachment();
                    foreach ($deleted_ids as $deleted_id) {
                        $successDeleted = $modelAttachment->updateById($deleted_id, array('is_deleted' => '1'));
                    }
                }
                $success = $modelContractorInfo->updateByContractorId($id, $data);

                $updateAttachment = 0;
                $upload = new Zend_File_Transfer_Adapter_Http();
                $files = $upload->getFileInfo();
                $Attachments = $modelContractorInfo->getAllAttachmentByTypeAndId($contractorInfo['contractor_info_id'], 'licence');
                $counter = count($Attachments);
                foreach ($files as $file => $fileInfo) {
                    if ($upload->isUploaded($file)) {
                        if ($upload->receive($file)) {
                            $counter = $counter + 1;
                            $info = $upload->getFileInfo($file);
                            $source = $info[$file]['tmp_name'];
                            $imageInfo = pathinfo($source);
                            $ext = $imageInfo['extension'];
                            $size = $info[$file]['size'];
                            $type = $info[$file]['type'];
                            $dir = get_config('attachment') . '/';
                            $subdir = date('Y/m/d/');
                            $fullDir = $dir . $subdir;
                            if (!is_dir($fullDir)) {
                                mkdir($fullDir, 0777, true);
                            }
                            $loggedUser = CheckAuth::getLoggedUser();
                            $data = array(
                                'created_by' => $loggedUser['user_id']
                                , 'created' => time()
                                , 'size' => $size
                                , 'type' => $type
                            );



                            $Attachid = $attachmentObj->insert($data);
                            $contractorInfoAttachmentId = $modelContractorInfoAttachment->insert(array('contractor_info_id' => $contractorInfo['contractor_info_id'], 'attachment_id' => $Attachid, 'type' => 'licence'));
                            $fileName = $contractorInfo['contractor_info_id'] . '_' . $counter . '_' . 'licence' . '.' . $ext;
                            $image_saved = copy($source, $fullDir . $fileName);

                            $typeParts = explode("/", $type);
                            if ($typeParts[0] == 'image') {
                                $thumbName = $contractorInfo['contractor_info_id'] . '_' . $counter . '_' . 'licence_thumbnail' . '.' . $ext;
                                ImageMagick::create_thumbnail($source, $fullDir . $thumbName, 78, 64);
                            } else {
                                $thumbName = $contractorInfo['contractor_info_id'] . '_' . $counter . '_' . 'licence' . '.jpg';
                                ImageMagick::convertFile($source . '[0]', $fullDir . $thumbName);
                            }

                            $Updatedata = array(
                                'path' => $fullDir . $fileName,
                                'file_name' => $fileName,
                                'thumbnail_file' => $fullDir . $thumbName
                            );




                            $updateAttachment = $attachmentObj->updateById($Attachid, $Updatedata);
                            if ($image_saved) {
                                if (file_exists($source)) {
                                    unlink($source);
                                }
                            }
                        }
                    }
                }

                if ($success || $updateAttachment || $successDeleted) {

                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                }
                //$this->view->successMessage = 'Updated Successfully';
                echo 1;
                exit;
                //$this->_redirect($this->router->assemble(array(), 'settingsContractorInfoList'));
                //return;
            }
        }


        // handling the updating process



        $this->view->form = $form;
        $this->view->id = $contractorInfo['contractor_info_id'];

        //
        // render views
        //
        echo $this->view->render('contractor-info/edit_licence.phtml');
        exit;
    }

    public function editPaymentAction() {


        // get request parameters
        $id = $this->request->getParam('contractor_id');

        $bond_to_be_withheld = $this->request->getParam('bond_to_be_withheld');
        $commission = $this->request->getParam('commission');
        // validation
        $modelContractorInfoAttachment = new Model_ContractorInfoAttachment();
        $attachmentObj = new Model_Attachment();
        $modelContractorInfo = new Model_ContractorInfo();
        $contractorInfo = $modelContractorInfo->getByContractorId($id);

        if (!$contractorInfo) {
			$data = array(
                    'contractor_id' => $id
                );
			$success = $modelContractorInfo->insert($data);
			$contractorInfo = $modelContractorInfo->getByContractorId($id);
			
            /*$this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect($this->router->assemble(array('contractor_id' => $this->contractor_id), 'settingsContractorInfoList'));
            return;*/
        }

        // init action form
        //contractor_id

        $form = new Settings_Form_PaymentInfo(array('contractorInfo' => $contractorInfo, 'contractor_id' => $this->contractor_id));

        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data			  
                $data = array(
                    'bond_to_be_withheld' => $bond_to_be_withheld,
                    'commission' => $commission,
					'contractor_id' => $this->contractor_id
                );

                $deleted_ids = $this->request->getParam('deleted_ids', '');
                $successDeleted = 0;
                if (!empty($deleted_ids)) {
                    $deleted_ids = explode(",", $deleted_ids);
                    $modelAttachment = new Model_Attachment();
                    foreach ($deleted_ids as $deleted_id) {
                        $successDeleted = $modelAttachment->updateById($deleted_id, array('is_deleted' => '1'));
                    }
                }

			
					$success = $modelContractorInfo->updateByContractorId($id, $data);
				
               

                $updateAttachment = 0;
                $upload = new Zend_File_Transfer_Adapter_Http();
                $files = $upload->getFileInfo();
                $Attachments = $modelContractorInfo->getAllAttachmentByTypeAndId($contractorInfo['contractor_info_id'], 'payment');
                $counter = count($Attachments);
                foreach ($files as $file => $fileInfo) {
                    if ($upload->isUploaded($file)) {
                        if ($upload->receive($file)) {
                            $counter = $counter + 1;
                            $info = $upload->getFileInfo($file);
                            $source = $info[$file]['tmp_name'];
                            $imageInfo = pathinfo($source);
                            $ext = $imageInfo['extension'];
                            $size = $info[$file]['size'];
                            $type = $info[$file]['type'];
                            $dir = get_config('attachment') . '/';
                            $subdir = date('Y/m/d/');
                            $fullDir = $dir . $subdir;
                            if (!is_dir($fullDir)) {
                                mkdir($fullDir, 0777, true);
                            }
                            $loggedUser = CheckAuth::getLoggedUser();
                            $data = array(
                                'created_by' => $loggedUser['user_id']
                                , 'created' => time()
                                , 'size' => $size
                                , 'type' => $type
                            );



                            $Attachid = $attachmentObj->insert($data);
                            $contractorInfoAttachmentId = $modelContractorInfoAttachment->insert(array('contractor_info_id' => $contractorInfo['contractor_info_id'], 'attachment_id' => $Attachid, 'type' => 'payment'));
                            $fileName = $contractorInfo['contractor_info_id'] . '_' . $counter . '_' . 'payment' . '.' . $ext;
                            $image_saved = copy($source, $fullDir . $fileName);

                            $typeParts = explode("/", $type);
                            if ($typeParts[0] == 'image') {
                                $thumbName = $contractorInfo['contractor_info_id'] . '_' . $counter . '_' . 'payment_thumbnail' . '.' . $ext;
                                ImageMagick::create_thumbnail($source, $fullDir . $thumbName, 78, 64);
                            } else {
                                $thumbName = $contractorInfo['contractor_info_id'] . '_' . $counter . '_' . 'payment' . '.jpg';
                                ImageMagick::convertFile($source . '[0]', $fullDir . $thumbName);
                            }

                            $Updatedata = array(
                                'path' => $fullDir . $fileName,
                                'file_name' => $fileName,
                                'thumbnail_file' => $fullDir . $thumbName
                            );




                            $updateAttachment = $attachmentObj->updateById($Attachid, $Updatedata);
                            if ($image_saved) {
                                if (file_exists($source)) {
                                    unlink($source);
                                }
                            }
                        }
                    }
                }

                if ($success || $updateAttachment || $successDeleted) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                }
                //$this->view->successMessage = 'Updated Successfully';
                echo 1;
                exit;
                //$this->_redirect($this->router->assemble(array(), 'settingsContractorInfoList'));
                //return;
            }
        }


        // handling the updating process



        $this->view->form = $form;
        $this->view->id = $contractorInfo['contractor_info_id'];

        //
        // render views
        //
        echo $this->view->render('contractor-info/edit_payment.phtml');
        exit;
    }

    public function logActivityAction() {


        $orderBy = $this->request->getParam('sort', 'log_id');
        $sortingMethod = $this->request->getParam('method', 'desc');
        $is_first_time = $this->request->getParam('is_first_time');
        $page_number = $this->request->getParam('page_number');
        $contractor_id = $this->request->getParam('contractor_id');




        $modelBookingLog = new Model_BookingLog();
        $bookingLogs = array();


        if ($this->request->isPost()) {


            if (isset($page_number)) {
                $perPage = 5;
                $currentPage = $page_number + 1;
            }


            $bookingLogs = $modelBookingLog->getAll(array('log_user_id' => $contractor_id), "{$orderBy} {$sortingMethod}", $pager, 0, $perPage, $currentPage);
            $modelBookingLog->fills($bookingLogs, array('history_details', 'status'));
            $result = array();
            $this->view->bookingLogs = $bookingLogs;
            $this->view->is_first_time = $is_first_time;
            $this->view->contractor_id = $contractor_id;
            $result['data'] = $this->view->render('contractor-info/log-activity.phtml');
            if ($bookingLogs) {
                $result['is_last_request'] = 0;
            } else {
                $result['is_last_request'] = 1;
            }

            echo json_encode($result);
            exit;
        }
        exit;
    }
    
    
    public function getAllContractorDeviceInfoAction() {

        $contractor_id = $this->request->getParam('contractor_id');

        $modelContractorMobileInfo = new Model_ContractorMobileInfo();
        $contractorDeviceInfo = $modelContractorMobileInfo->getAllByContractorId($contractor_id);

        $this->view->contractorDeviceInfo = $contractorDeviceInfo;


        echo $this->view->render('contractor-info/device_info.phtml');
        exit;
    }
	
	
}
