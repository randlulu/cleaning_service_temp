<?php

class Settings_DateTimeController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsDateTime';

        BreadCrumbs::setLevel(2, 'Date Time Configration');
    }

    /**
     * Items list action
     */
    public function indexAction() {

        // check Auth for logged user
        // CheckAuth::checkPermission(array('settingsDateTimeConfigurationView'));
        CheckAuth::checkCredential(array('settingsDateTimeConfigurationView'));
        
        $companyId = CheckAuth::getCompanySession();
        
        $modelDateTime = new Model_DateTime();
        $dateTimeObj = $modelDateTime->getByCompanyId($companyId);
        // var_dump($dateTimeObj);exit;
        $this->view->data = $dateTimeObj;

    }

    /**
     * Add new item action
     */
    public function addAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkCredential(array('settingsDateTimeConfigurationAdd'));
        $companyId = CheckAuth::getCompanySession();
        //
        // get request parameters
        //
        
        
        // $companyId = $this->request->getParam('company_id');
        $countryId = $this->request->getParam('country_id');
        $time_zone = $this->request->getParam('time_zone');
        $date_format = $this->request->getParam('date_format');
        $time_format = $this->request->getParam('time_format');

        $router = Zend_Controller_Front::getInstance()->getRouter();

        //
        // init action form
        //
        
            $form = new Settings_Form_DateTime();        
            // handling the insertion process
            //
            if ($this->request->isPost()) { // check if POST request method
                if ($form->isValid($this->request->getPost())) { // validate form data
                    $dateTimeObj = new Model_DateTime();
                    $data = array(
                        'company_id' => $companyId,
                        'country_id' => $countryId,
                        'time_zone' => $time_zone,
                        'date_format' => $date_format,
                        'time_format' => $time_format
                    );
                    $success = $dateTimeObj->insert($data);
                    if ($success) {
                    
                        $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Date and time Configration Saved"));
                    } else {
                    
                        $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                    }
                    // $this->_redirect($this->router->assemble(array(), 'settingsDateTime'));
                    echo 1;
                    exit;
                }
            }

            $this->view->form = $form;
            // render views
            //
            echo $this->view->render('date-time/date_time.phtml');
        exit;
        
    }

    public function editAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkCredential(array('settingsDateTimeConfigurationEdit'));

        //
        // get request parameters
        //

        $companyId = CheckAuth::getCompanySession();
        // $companyId = $this->request->getParam('company_id');
        $countryId = $this->request->getParam('country_id');
        $time_zone = $this->request->getParam('time_zone');
        $date_format = $this->request->getParam('date_format');
        $time_format = $this->request->getParam('time_format');
        $id = $this->request->getParam('id');
        //
        // validation
        //
        $dateTimeModel = new Model_DateTime();
        $dateTimeObj = $dateTimeModel->getById($id);
        // if (!$dateTimeObj) {
        //     $this->_redirect($this->router->assemble(array(), 'settingsAddDateTime'));
        //     return;
        // }


        //
        // init action form
        //
        $form = new Settings_Form_DateTime(array('mode' => 'update', 'date_time' => $dateTimeObj));
        //
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                    $data = array(
                        'company_id' => $companyId,
                        'country_id' => $countryId,
                        'time_zone' => $time_zone,
                        'date_format' => $date_format,
                        'time_format' => $time_format
                    );

                $success = $dateTimeModel->updateById($id, $data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Date time configration updated successfully"));
                        // $this->_redirect($this->router->assemble(array(), 'settingsDateTime'));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                    // $this->_redirect($this->router->assemble(array(), 'settingsDateTime'));
                }

                echo 1;
                exit;
                 
                //return;
            }
        }

        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('date-time/date_time.phtml');
        exit;
    }

    public function deleteAction() {

// Date Time
    }

    public function getTimeZoneByCountryAction() {
        $countryId = $this->request->getParam('country_id', 0);
        $name = $this->request->getParam('name', 'time_zone');
        $class = $this->request->getParam('class', 'form-control required');


        $countryModel = new Model_Countries();
        $country = $countryModel->getById($countryId);

        $timeZonedropDown = new Zend_Form_Element_Select($name);
        // if ($isArrayName) {
        //     $dropDown->setBelongsTo($arrayName);
        // }
        $timeZonedropDown->setDecorators(array('ViewHelper'));
        $timeZonedropDown->setRequired();
        $timeZonedropDown->setAttribs(array('class' => 'form-control','onchange' => 'getDateFormat();getTimeFormat();' , 'id' => 'time_zone'));
        // $timeZonedropDown->setValue((!empty($dateTime['time_zone']) ? $dateTime['time_zone'] : ''));
        $timeZonedropDown->setErrorMessages(array('Required' => 'Please select the time zone'));

        $timezone = DateTimeZone::listIdentifiers(DateTimeZone::PER_COUNTRY, $country['country_code']/*'AU'*/);
        $timezone_offsets = array();
        $timeZonedropDown->addMultiOption('', 'Select timezone');
        foreach(/*timezone_identifiers_list()*/ $timezone as $timezone_identifier){
            $date_time_zone = new DateTimeZone($timezone_identifier);
            $date_time = new DateTime('now', $date_time_zone);
            // $timezone_offsets[$timezone_identifier] = $date_time_zone->getOffset($date_time);
            $timezone_offsets[$timezone_identifier] =  '(' . $date_time->format('P') . ' GMT) ' . '  ' . $timezone_identifier;
            // arsort($timezone_offsets);
            $timeZonedropDown->addMultiOption($timezone_identifier, $timezone_offsets[$timezone_identifier]);
        }

        echo $timeZonedropDown->render();
        exit;
    }

    public function getDateFormatByCountryAction() {
        $countryId = $this->request->getParam('country_id', 0);
        $time_zone = $this->request->getParam('time_zone', '');
        $name = $this->request->getParam('name', 'date_format');
        $class = $this->request->getParam('class', 'form-control required');
        // $isArrayName = $this->request->getParam('isArrayName', 0);
        // $arrayName = $this->request->getParam('arrayName', '');


        $countryModel = new Model_Countries();
        $country = $countryModel->getById($countryId);

        $dateFormatDropdown = new Zend_Form_Element_Select($name);
        // if ($isArrayName) {
        //     $dropDown->setBelongsTo($arrayName);
        // }
        $dateFormatDropdown->setDecorators(array('ViewHelper'));
        $dateFormatDropdown->setRequired();
        $dateFormatDropdown->setAttribs(array('class' => 'form-control'));
        // $timeZonedropDown->setValue((!empty($dateTime['time_zone']) ? $dateTime['time_zone'] : ''));
        $dateFormatDropdown->setErrorMessages(array('Required' => 'Please select the date format'));


        $locale = Zend_Locale::getLocaleToTerritory($country['country_code']);
        $localeObj = new Zend_Locale($locale);
        $dateFormatList = $localeObj->getTranslationList('date');

        //1451649600 time stamp
        $dateFormatDropdown->addMultiOption('', 'Select date format');
        $date = new Zend_Date();
        // $formatType = "Zend_Date::DATE_". strtoupper($key);

        date_default_timezone_set($time_zone);
        foreach ($dateFormatList as $key => $dateFormat) {
            $now = $key  . '   | ex: ' .$date->now($localeObj)->toString($dateFormat); 
            $dateFormatDropdown->addMultiOption($dateFormat , $now);

            // $now = $dateFormat /*. '   | ex: ' .$date->now($localeObj)->toString(call_user_func($formatType))*/;
            // $now = $dateFormat . 'ex: ' . $date->now($localeObj)->toString(Zend_Date::DATE_.strtoupper($key));
             // $dateFormatDropdown->addMultiOption($key , $now);
        }

        // $timezone = DateTimeZone::listIdentifiers(DateTimeZone::PER_COUNTRY, $country['country_code']/*'AU'*/);
        // $timezone_offsets = array();
        // $timeZonedropDown->addMultiOption('', 'Select timezone');
        // foreach(/*timezone_identifiers_list()*/ $timezone as $timezone_identifier){
        //     $date_time_zone = new DateTimeZone($timezone_identifier);
        //     $date_time = new DateTime('now', $date_time_zone);
        //     // $timezone_offsets[$timezone_identifier] = $date_time_zone->getOffset($date_time);
        //     $timezone_offsets[$timezone_identifier] =  '(' . $date_time->format('P') . ' GMT) ' . '  ' . $timezone_identifier;
        //     // arsort($timezone_offsets);
        //     $timeZonedropDown->addMultiOption($timezone_identifier, $timezone_offsets[$timezone_identifier]);
        // }

        echo $dateFormatDropdown->render();
        exit;
    }


    public function getTimeFormatByCountryAction() {
        $countryId = $this->request->getParam('country_id', 0);
        $time_zone = $this->request->getParam('time_zone', '');
        $name = $this->request->getParam('name', 'time_format');
        $class = $this->request->getParam('class', 'form-control required');
        // $isArrayName = $this->request->getParam('isArrayName', 0);
        // $arrayName = $this->request->getParam('arrayName', '');


        $countryModel = new Model_Countries();
        $country = $countryModel->getById($countryId);

        $timeFormatDropdown = new Zend_Form_Element_Select($name);
        // if ($isArrayName) {
        //     $dropDown->setBelongsTo($arrayName);
        // }
        $timeFormatDropdown->setDecorators(array('ViewHelper'));
        $timeFormatDropdown->setRequired();
        $timeFormatDropdown->setAttribs(array('class' => 'form-control'));
        // $timeZonedropDown->setValue((!empty($dateTime['time_zone']) ? $dateTime['time_zone'] : ''));
        $timeFormatDropdown->setErrorMessages(array('Required' => 'Please select the time format'));


        $locale = Zend_Locale::getLocaleToTerritory($country['country_code']);
        $localeObj = new Zend_Locale($locale);
        $timeFormatList = $localeObj->getTranslationList('time');

        //1451649600 time stamp
        $timeFormatDropdown->addMultiOption('', 'Select time format');
        $date = new Zend_Date();
        // $formatType = "Zend_Date::DATE_". strtoupper($key);

        date_default_timezone_set($time_zone);
        foreach ($timeFormatList as $key => $timeFormat) {
            $now = $key  . '   | ex: ' .$date->now($localeObj)->toString($timeFormat); 
            $timeFormatDropdown->addMultiOption($timeFormat , $now);

        }

        echo $timeFormatDropdown->render();
        exit;
    }



}

