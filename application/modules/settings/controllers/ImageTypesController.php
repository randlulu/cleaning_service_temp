<?php

class Settings_ImageTypesController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
		
		$this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsType';
		
		BreadCrumbs::setHomeLink('settings', '/settings');
        BreadCrumbs::setLevel(2, 'Image Types');
        
        $pageName = $this->request->getParam('page_name', '');
        $this->view->page_title = (!empty($pageName))?$pageName." - Image Types Settings":"Image Types Settings";

    }

    /**
     * Items list action
     */
    public function indexAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsImageTypesList'));

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'image_types_id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        //
        // get data list
        //
        $modelImageTypes = new Model_ImageTypes();
        $this->view->data = $modelImageTypes->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);

        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
 
    }

    /**
     * Add new item action
     */
    public function addAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsImageTypesAdd'));

        //
        // get request parameters
        //
        $name = $this->request->getParam('name');


        $router = Zend_Controller_Front::getInstance()->getRouter();

        //
        // init action form
        //
        $form = new Settings_Form_ImageTypes();

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $modelImageTypes = new Model_ImageTypes();
                $data = array(
                    'name' => $name,
                    'company_id' => CheckAuth::getCompanySession()
                );

                $success = $modelImageTypes->insert($data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                }
                //$this->_redirect($this->router->assemble(array(), 'settingsBookingStatusList'));
                echo 1;
                exit;
            } else {
                $messages = $form->getElement('name')->getErrorMessages();
                if ($messages) {
                    $form->getElement('name')->clearErrorMessages();
                    foreach ($messages as $message) {
                        $form->getElement('name')->addError($message);
                    }
                }
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('image-types/add_edit.phtml');
        exit;
    }

    public function deleteAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsImageTypesDelete'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());


        if ($id) {
            $ids[] = $id;
        }

        $modelImageTypes = new Model_ImageTypes();

        foreach ($ids as $id) {
            $ImageType = $modelImageTypes->getById($id);
            if (!$ImageType['is_core']) {

                $isNotRelated = $modelImageTypes->checkBeforeDeleteImageTypes($id);
                if ($isNotRelated) {
                    $modelImageTypes->deleteById($id);
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Error: Failed to delete, item in use"));
                }
            }
        }
        $this->_redirect($this->router->assemble(array(), 'settingsImageTypesList'));
    }

    public function editAction() {
	
        //
        // check Auth for logged user
        //
		
        CheckAuth::checkPermission(array('settingsImageTypesEdit'));

        //
        // get request parameters
        //
        $name = $this->request->getParam('name');
        $id = $this->request->getParam('id');
		
		

        //
        // validation
        //
        $modelImageTypes = new Model_ImageTypes();
        $ImageType= $modelImageTypes->getById($id);
		
		
		
        if (!$ImageType) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect($this->router->assemble(array(), 'settingsImageTypesList'));
            return;
        }


        //
        // init action form
        //
        $form = new Settings_Form_ImageTypes(array('mode' => 'update', 'ImageType' => $ImageType));
		
		


        //
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                if (!$ImageType['is_core']) {
                    $data = array(
                        'name' => $name,
                        'company_id' => CheckAuth::getCompanySession()
                    );

                    $success = $modelImageTypes->updateById($id, $data);

                    //$this->view->successMessage = 'Updated Successfully';
                    if ($success) {
                        $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved"));
                    } else {
                        $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                    }
                }
                echo 1;
                exit;
                
            }
        }

        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('image-types/add_edit.phtml');
        exit;
    }


    public function addDescriptionAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsImageTypesAddDescription'));

        //
        // get request parameters
        //
        $description = $this->request->getParam('description');
        $id = $this->request->getParam('id');

        //
        // validation
        //
        $modelImageTypes = new Model_ImageTypes();
        $ImageType = $modelImageTypes->getById($id);
        if (!$ImageType) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect($this->router->assemble(array(), 'settingsImageTypesList'));
            return;
        }


        //
        // init action form
        //
        $form = new Settings_Form_ImageTypesAddDescription(array('ImageType' => $ImageType));


        //
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $success = $modelImageTypes->updateById($id, array('description' => $description));

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                }

                echo 1;
                exit;
            }
        }

        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('image-types/add-description.phtml');
        exit;
    }


    public function showDescriptionAction() {


      
        $id = $this->request->getParam('id');

        //
        // validation
        //
        $modelImageTypes = new Model_ImageTypes();
        $ImageType = $modelImageTypes->getById($id);
        if (!$ImageType) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect($this->router->assemble(array(), 'settingsImageTypesList'));
            return;
        }

        $this->view->ImageType = $ImageType;


        //
        // render views
        //
        echo $this->view->render('image-types/show-description.phtml');
        exit;
    }

}

