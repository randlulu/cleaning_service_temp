<?php

class Settings_AttributeValueController extends Zend_Controller_Action {

    private $request;
    private $router;
    private $attribute_id;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
        $this->attribute_id = $this->request->getParam('attribute_id');

        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsServices';

        //
        //get attribute
        //
        $attributeObj = new Model_Attributes();
        $attribute = $attributeObj->getById($this->attribute_id);
        $this->view->attribute = $attribute;

        BreadCrumbs::setLevel(3, 'Values of ' . $attribute['attribute_name']);
    }

    /**
     * Items list action
     */
    public function indexAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsAttributeValueList'));

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'attribute_value_id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];


        //
        //get attribute value by id
        //
        $filters['attribute_id'] = $this->attribute_id;


        //
        // get data list
        //
        $attributeValueObj = new Model_AttributeListValue();
        $this->view->data = $attributeValueObj->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);

        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
        $this->view->attribute_id = $this->attribute_id;
    }

    /**
     * Add new item action
     */
    public function addAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsAttributeValueAdd'));

        //
        // get request parameters
        //
        $attributeValue = $this->request->getParam('attribute_value');
        $unitPrice = $this->request->getParam('unit_price', 0);
        $extra_info = $this->request->getParam('extra_info', '');


        $router = Zend_Controller_Front::getInstance()->getRouter();

        //
        // init action form
        //
        $form = new Settings_Form_AttributeValue(array('attribute_id' => $this->attribute_id));

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $attributeValueObj = new Model_AttributeListValue();
                $data = array(
                    'attribute_value' => $attributeValue,
                    'unit_price' => $unitPrice,
                    'extra_info' => $extra_info,
                    'attribute_id' => $this->attribute_id
                );

                $success = $attributeValueObj->insert($data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Attribute Value"));
                }

                //$this->_redirect($this->router->assemble(array(), 'settingsAttributeValueList'));
                echo 1;
                exit;
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('attribute-value/add_edit.phtml');
        exit;
    }

    /**
     * Edit an  item action
     */
    public function editAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsAttributeValueEdit'));

        $router = Zend_Controller_Front::getInstance()->getRouter();

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $attributeValue = $this->request->getParam('attribute_value');
        $unitPrice = $this->request->getParam('unit_price', 0);
        $extra_info = $this->request->getParam('extra_info', '');

        // get old data
        $modelAttributeListValue = new Model_AttributeListValue();
        $attributeListValue = $modelAttributeListValue->getById($id);

        //
        // init action form
        //
        $form = new Settings_Form_AttributeValue(array('mode' => 'update', 'attributeListValue' => $attributeListValue));

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'attribute_value' => $attributeValue,
                    'unit_price' => $unitPrice,
                    'extra_info' => $extra_info
                );

                $success = $modelAttributeListValue->updateById($id, $data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Attribute Value"));
                }
                echo 1;
                exit;
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('attribute-value/add_edit.phtml');
        exit;
    }

    public function deleteAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsAttributeValueDelete'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
        if ($id) {
            $ids[] = $id;
        }

        $attributeValueObj = new Model_AttributeListValue();
        foreach ($ids as $id) {

            $isNotRelated = $attributeValueObj->checkBeforeDeleteServiceAttributeValue($id);

            if ($isNotRelated) {
                $isFloorAndHaveAttachment = $attributeValueObj->checkIfFloorTypeAndHaveAttachment($id);
                if ($isFloorAndHaveAttachment) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "could not be deleted ,This Item used in other Place "));
                } else {
                    $attributeValueObj->deleteById($id);
                }
            } else {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "could not be deleted ,This Item used in other Place "));
            }
        }
        $this->_redirect($this->router->assemble(array('attribute_id' => $this->attribute_id), 'settingsAttributeValueList'));
    }

}

