<?php

class Settings_TradingNamesController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsUser';

        BreadCrumbs::setLevel(2, 'Trading Names');
    }

    /**
     * Items list action
     */
    public function indexAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsCompaniesTradingNames'));

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'trading_name_id');
        $company_orderBy = $this->request->getParam('sort', 'company_id');
        $sortingMethod = $this->request->getParam('method', 'desc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());


        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        $trading_namesObj = new Model_TradingName();
        $companyObje = New Model_Companies();
        $this->view->data = $trading_namesObj->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);
        $this->view->company = $companyObje->getAll($filters, "{$company_orderBy} {$sortingMethod}", $pager);

        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
    }

    /**
     * Add new item action
     */
    public function addAction() {

        $companyId = $this->request->getParam('company_id');
        $trading_name = $this->request->getParam('trading_name');
        $isDefault = $this->request->getParam('isDefault');
        $website_url = $this->request->getParam('website_url');




        $router = Zend_Controller_Front::getInstance()->getRouter();

        //
        // init action form
        //
        $form = new Settings_Form_TradingNames();

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
		
			  //$modelBooking = new Model_Booking();
				//$modelBooking->alterTable();
		
            if ($form->isValid($this->request->getPost())) { // validate form data
                $tradingNameObj = new Model_TradingName();
                $data = array(
                    'company_id' => $companyId,
                    'trading_name' => $trading_name,
                    'is_default' => $isDefault,
                    'website_url' => $website_url,
                );

                $success = $tradingNameObj->insert($data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Company"));
                }
                //$this->_redirect($this->router->assemble(array(), 'settingsCompaniesList'));
                echo 1;
                exit;
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('trading-names/add_edit.phtml');
        exit;
    }

    public function deleteAction() {

        //
        // check Auth for logged user
        //
       // CheckAuth::checkPermission(array('settingsCompaniesTradingNamesDelete'));
        //
        // get request parameters
        //
        $id = $this->request->getParam('id');


        $trading_namesObj = new Model_TradingName();
        $success = $trading_namesObj->deleteById($id);
        if ($success) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Deleted Successfully"));
        } else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "could not be deleted ,This Item used in other Place "));
        }


        $this->_redirect($this->router->assemble(array(), 'settingsCompaniesTradingNames'));
    }

    public function editAction() {

        //
        // check Auth for logged user
        //
        //CheckAuth::checkPermission(array('settingsCompaniesTradingNamesEdit'));



        $companyId = $this->request->getParam('company_id');
        $trading_name = $this->request->getParam('trading_name');
        $isDefault = $this->request->getParam('isDefault');
        $website_url = $this->request->getParam('website_url');
        $id = $this->request->getParam('id');



//        if ($id != CheckAuth::getCompanySession()) {
//            If (!CheckAuth::checkCredential(array('canHandleAllCompanies'))) {
//                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
//                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
//            }
//        }
        //
        // validation
        //
       $trading_namesObj = new Model_TradingName();
        $trading_names = $trading_namesObj->getById($id);
        if (!$trading_names) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect($this->router->assemble(array(), 'settingsCompaniesTradingNames'));
            return;
        }


        //
        // init action form
        //
        $form = new Settings_Form_TradingNames(array('mode' => 'update', 'trading_names' => $trading_names, 'company_id' => $companyId));


        //
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'company_id' => $companyId,
                    'trading_name' => $trading_name,
                    'is_default' => $isDefault,
                    'website_url' => $website_url,
                );

                $success = $trading_namesObj->updateById($id, $data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Company"));
                }
                echo 1;
                exit;
            }
        }

        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('trading-names/add_edit.phtml');
        exit;
    }

  

}
