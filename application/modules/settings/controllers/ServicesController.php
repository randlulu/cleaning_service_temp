<?php

class Settings_ServicesController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsServices';

        BreadCrumbs::setLevel(2, 'Service List');
        
        $pageName = $this->request->getParam('page_name', '');
        $this->view->page_title = (!empty($pageName))?$pageName." - Service List Settings":"Service List Settings";

    }

    /**
     * Items list action
     */
    public function indexAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsServicesList'));

        //$modelServiceAttachment = new Model_ServiceAttachment();
        //$modelServiceAttachment->executeQuery();
        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'service_id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        //
        // get data list
        //
        $serviceObj = new Model_Services();
        $this->view->data = $serviceObj->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);

        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
    }

    /**
     * Add new item action
     */
    public function addAction() {

        //
        // check Auth for logged user
        //

      //  echo "hh" ; exit ;
        CheckAuth::checkPermission(array('settingsServicesAdd'));

        //
        // get request parameters
        //
        $serviceName = $this->request->getParam('service_name');
        $companyId = CheckAuth::getCompanySession();
        $minPrice = $this->request->getParam('min_price');
        $priceEquasion = $this->request->getParam('price_equasion');
        $estimateTime = $this->request->getParam('estimate_time');
        //$areaOfEstimateTime = $this->request->getParam('area_of_estimate_time');
        $estimateTimeUnit = $this->request->getParam('estimate_time_unit');
        $default_image_id = $this->request->getParam('default_image_id', '');
        $router = Zend_Controller_Front::getInstance()->getRouter();

        $attachmentObj = new Model_Attachment();
        $modelServiceAttachment = new Model_ServiceAttachment();

        //
        // init action form
        //
        $form = new Settings_Form_Services();

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $serviceObj = new Model_Services();

                if ($estimateTimeUnit == 'm') {
                    $estimateTime = $estimateTime / 60;
                } else if ($estimateTimeUnit == 's') {
                    $estimateTime = $estimateTime / 3600;
                }

                $data = array(
                    'service_name' => $serviceName,
                    'company_id' => $companyId,
                    'min_price' => $minPrice,
                    'created' => time(),
                    'price_equasion' => $priceEquasion,
                    //'area_of_estimate_time'=>$areaOfEstimateTime,
                    'estimate_hours' => $estimateTime
                );

                $success = $serviceObj->insert($data);

                if ($success) {
                    if (isset($default_image_id) && !empty($default_image_id)) {
                        $defaultImageArray = array(
                            'attachment_id' => $default_image_id,
                            'service_id' => $success,
                            'is_default' => 1
                        );

                        $modelServiceAttachment->insert($defaultImageArray);
                    }

                    $upload = new Zend_File_Transfer_Adapter_Http();
                    $files = $upload->getFileInfo();
                    $counter = 0;
                    foreach ($files as $file => $fileInfo) {
                        if ($upload->isUploaded($file)) {
                            if ($upload->receive($file)) {
                                $counter = $counter + 1;
                                $info = $upload->getFileInfo($file);
                                $source = $info[$file]['tmp_name'];
                                $imageInfo = pathinfo($source);
                                $ext = $imageInfo['extension'];
                                $size = $info[$file]['size'];
                                $type = $info[$file]['type'];
                                $dir = get_config('attachment') . '/';
                                $subdir = date('Y/m/d/');
                                $fullDir = $dir . $subdir;
                                if (!is_dir($fullDir)) {
                                    mkdir($fullDir, 0777, true);
                                }
                                $loggedUser = CheckAuth::getLoggedUser();
                                $data = array(
                                    'created_by' => $loggedUser['user_id']
                                    , 'created' => time()
                                    , 'size' => $size
                                    , 'type' => $type
                                );



                                $id = $attachmentObj->insert($data);
                                $serviceAttachmentId = $modelServiceAttachment->insert(array('service_id' => $success, 'attachment_id' => $id));
                                //$fileName = $serviceName.'_'.$success.'_'.$counter.'_'.time().'.'. $ext;
                                $fileName = $success . '_' . $counter . '_' . time() . '.' . $ext;
                                //$thumbName = $serviceName.'_'.$success.'_thumbnail_'.$counter.'_'.time().'.'. $ext;
                                //$largeName = $serviceName.'_'.$success.'_large_'.$counter.'_'.time().'.'. $ext;

                                $thumbName = $success . '_thumbnail_' . $counter . '_' . time() . '.' . $ext;
                                $largeName = $success . '_large_' . $counter . '_' . time() . '.' . $ext;
                                $compressedName = $success . '_compressed_' . $counter . '_' . time() . '.' . $ext;
                                $image_saved = copy($source, $fullDir . $fileName);
                                $compressed_saved = copy($source, $fullDir . $compressedName);

                                ImageMagick::create_thumbnail($source, $fullDir . $thumbName, 78, 64);
                                ImageMagick::scale_image($source, $fullDir . $largeName, 510);
                                ImageMagick::convert($source, $fullDir . $compressedName);
                                ImageMagick::compress_image($source, $fullDir . $compressedName);
                                if ($image_saved) {
                                    $Updatedata = array(
                                        'path' => $fullDir . $fileName,
                                        'file_name' => $fileName,
                                        'thumbnail_file' => $fullDir . $thumbName,
                                        'large_file' => $fullDir . $largeName,
                                        'compressed_file' => $fullDir . $compressedName
                                    );
                                    $attachmentObj->updateById($id, $Updatedata);


                                    if (file_exists($source)) {
                                        unlink($source);
                                    }
                                }
                            }
                        }
                    }
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Service"));
                }

                //update General Contractor
                $modelUser = new Model_User();
                $modelUser->updateGeneralContractor();

                //$this->_redirect($this->router->assemble(array(), 'settingsServiceList'));
                echo 1;
                exit;
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('services/add_edit.phtml');
        exit;
    }

    public function deleteAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsServicesDelete'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
        if ($id) {
            $ids[] = $id;
        }

        $serviceObj = new Model_Services();
        foreach ($ids as $id) {
            if (CheckAuth::checkIfCanHandelAllCompany('service', $id)) {
                $isNotRelated = $serviceObj->checkBeforeDeleteService($id);
                if ($isNotRelated) {
                    $modelAttachment = new Model_Attachment();
                    $ServiceAttachmentObj = new Model_ServiceAttachment();
                    $serviceAttachments = $ServiceAttachmentObj->getByServiceId($id);
                    foreach ($serviceAttachments as $serviceAttachment) {
                        $success = $modelAttachment->updateById($serviceAttachment['attachment_id'], array('is_deleted' => '1'));
                    }
                    $serviceObj->deleteById($id);
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "could not be deleted ,This Item used in other Place "));
                }
            }
        }
        $this->_redirect($this->router->assemble(array(), 'settingsServicesList'));
    }

    public function editAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsServicesEdit'));

        //
        // get request parameters
        //
        $serviceName = $this->request->getParam('service_name');
        $minPrice = $this->request->getParam('min_price');
        $priceEquasion = $this->request->getParam('price_equasion');
        $estimateTime = $this->request->getParam('estimate_time');
        //$areaOfEstimateTime = $this->request->getParam('area_of_estimate_time');
        $estimateTimeUnit = $this->request->getParam('estimate_time_unit');
        $id = $this->request->getParam('id');
        $default_image_id = $this->request->getParam('default_image_id', '');


        $attachmentObj = new Model_Attachment();
        $modelServiceAttachment = new Model_ServiceAttachment();




        if (!CheckAuth::checkIfCanHandelAllCompany('service', $id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }


        //
        // validation
        //
        $serviceObj = new Model_Services();
        $service = $serviceObj->getById($id);
        $defaultImageObj = $modelServiceAttachment->getByServiceId($id, 1);
        if (!$service) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect($this->router->assemble(array(), 'settingsServicesList'));
            return;
        }


        //
        // init action form
        //
        $form = new Settings_Form_Services(array('mode' => 'update', 'service' => $service));


        //
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $deleted_ids = $this->request->getParam('deleted_ids', '');
                $successDeleted = 0;
                if (!empty($deleted_ids)) {
                    $deleted_ids = explode(",", $deleted_ids);
                    $modelAttachment = new Model_Attachment();
                    foreach ($deleted_ids as $deleted_id) {
                        $successDeleted = $modelAttachment->updateById($deleted_id, array('is_deleted' => '1'));
                    }
                }

                if ($estimateTimeUnit == 'm') {
                    $estimateTime = $estimateTime / 60;
                } else if ($estimateTimeUnit == 's') {
                    $estimateTime = $estimateTime / 3600;
                }

                $data = array(
                    'service_name' => $serviceName,
                    'min_price' => $minPrice,
                    'price_equasion' => $priceEquasion,
                    //'area_of_estimate_time'=>$areaOfEstimateTime,
                    'estimate_hours' => $estimateTime
                );

                $success = $serviceObj->updateById($id, $data);

                $attachment_id = 0;

                if ($id) {
                    $upload = new Zend_File_Transfer_Adapter_Http();
                    $files = $upload->getFileInfo();
                    $serviceAttachments = $modelServiceAttachment->getByServiceId($id);
                    $counter = count($serviceAttachments);
                    foreach ($files as $file => $fileInfo) {
                        if ($upload->isUploaded($file)) {
                            if ($upload->receive($file)) {
                                $counter = $counter + 1;
                                $info = $upload->getFileInfo($file);
                                $source = $info[$file]['tmp_name'];
                                $imageInfo = pathinfo($source);
                                $ext = $imageInfo['extension'];
                                $size = $info[$file]['size'];
                                $type = $info[$file]['type'];
                                $dir = get_config('attachment') . '/';
                                $subdir = date('Y/m/d/');
                                $fullDir = $dir . $subdir;
                                if (!is_dir($fullDir)) {
                                    mkdir($fullDir, 0777, true);
                                }
                                $loggedUser = CheckAuth::getLoggedUser();
                                $data = array(
                                    'created_by' => $loggedUser['user_id']
                                    , 'created' => time()
                                    , 'size' => $size
                                    , 'type' => $type
                                );



                                $attachment_id = $attachmentObj->insert($data);
                                $serviceAttachmentId = $modelServiceAttachment->insert(array('service_id' => $id, 'attachment_id' => $attachment_id));


                                //$fileName = $serviceName.'_'.$id.'_'.$counter.'_'.time().'.'.$ext;
                                $fileName = $id . '_' . $counter . '_' . time() . '.' . $ext;
                                $image_saved = copy($source, $fullDir . $fileName);
                                //$thumbName = $serviceName.'_'.$id.'_thumbnail_'.$counter.'_'.time().'.'. $ext;
                                //$largeName = $serviceName.'_'.$id.'_large_'.$counter.'_'.time().'.'. $ext;

                                $thumbName = $id . '_thumbnail_' . $counter . '_' . time() . '.' . $ext;
                                $largeName = $id . '_large_' . $counter . '_' . time() . '.' . $ext;
                                $compressedName = $id . '_compressed_' . $counter . '_' . time() . '.' . $ext;
                                $compressed_saved = copy($source, $fullDir . $compressedName);
                                ImageMagick::create_thumbnail($source, $fullDir . $thumbName, 78, 64);
                                ImageMagick::scale_image($source, $fullDir . $largeName, 510);
                                ImageMagick::convert($source, $fullDir . $compressedName);
                                ImageMagick::compress_image($source, $fullDir . $compressedName);

                                if ($image_saved) {
                                    $Updatedata = array(
                                        'path' => $fullDir . $fileName,
                                        'file_name' => $fileName,
                                        'thumbnail_file' => $fullDir . $thumbName,
                                        'large_file' => $fullDir . $largeName,
                                        'compressed_file' => $fullDir . $compressedName
                                    );

                                    $attachmentObj->updateById($attachment_id, $Updatedata);


                                    if (file_exists($source)) {
                                        unlink($source);
                                    }
                                }
                            }
                        }
                    }

                    if ($defaultImageObj) {
                        $modelServiceAttachment->updateById($defaultImageObj['service_attachment_id'], array('attachment_id' => $default_image_id));
                    } else {
                        $modelServiceAttachment->insert(array('service_id' => $id, 'attachment_id' => $default_image_id, 'is_default' => 1));
                    }




                    if ($success || $attachment_id || ($defaultImageObj['attachment_id'] != $default_image_id)) {
                        $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                    } else {
                        $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Service"));
                    }

                    //update General Contractor
                    $modelUser = new Model_User();
                    $modelUser->updateGeneralContractor();

                    //$this->view->successMessage = 'Updated Successfully';
                    echo 1;
                    exit;
                    //$this->_redirect($this->router->assemble(array(), 'settingsServiceList'));
                    //return;
                }
            }
        }

        $this->view->form = $form;
        $this->view->id = $id;
        $this->view->default_image = $defaultImageObj;


        //
        // render views
        //
        echo $this->view->render('services/add_edit.phtml');
        exit;
    }

    public function getServiceImagesAction() {


        $id = $this->request->getParam('service_id', 0);
        $modelServiceAttachment = new Model_ServiceAttachment();
        $serviceAttachemnts = $modelServiceAttachment->getByServiceId($id, 0);

        header('Content-type: text/json');
        header('Content-type: application/json');
        echo json_encode($serviceAttachemnts);
        exit;
    }

    public function setServiceDefaultImageAction() {
        $attachmentObj = new Model_Attachment();
        if ($this->request->isPost()) {

            $upload = new Zend_File_Transfer_Adapter_Http();
            $files = $upload->getFileInfo();
            foreach ($files as $file => $fileInfo) {

                if ($upload->isUploaded($file)) {
                    if ($upload->receive($file)) {
                        $info = $upload->getFileInfo($file);
                        $source = $info[$file]['tmp_name'];
                        $imageInfo = pathinfo($source);
                        $ext = $imageInfo['extension'];
                        $size = $info[$file]['size'];
                        $type = $info[$file]['type'];
                        $file_name = $info[$file]['name'];
                        $dir = get_config('attachment') . '/';
                        $subdir = date('Y/m/d/');
                        $fullDir = $dir . $subdir;
                        $loggedUser = CheckAuth::getLoggedUser();
                        $data = array(
                            'created_by' => $loggedUser['user_id']
                            , 'created' => time()
                            , 'size' => $size
                            , 'type' => $type
                        );


                        $Attachid = $attachmentObj->insert($data);
                        $fileName = 'default_image_' . $Attachid . '.' . $ext;
                        $thumbName = 'default_image_' . $Attachid . '_thumbnail_' . $ext;
                        $largeName = 'default_image_' . $Attachid . '_large_' . $ext;
                        $compressedName = 'default_image_' . $Attachid . '_compressed_' . $ext;
                        if (!is_dir($fullDir)) {
                            mkdir($fullDir, 0777, true);
                        }


                        $image_saved = copy($source, $fullDir . $fileName);
                        ImageMagick::create_thumbnail($source, $fullDir . $thumbName, 78, 64);
                        ImageMagick::scale_image($source, $fullDir . $largeName, 510);
                        ImageMagick::convert($source, $fullDir . $compressedName);
                        ImageMagick::compress_image($source, $fullDir . $compressedName);
                        if ($image_saved) {

                            $Updatedata = array(
                                'path' => $fullDir . $fileName,
                                'file_name' => $fileName,
                                'thumbnail_file' => $fullDir . $thumbName,
                                'thumbnail_file' => $fullDir . $thumbName,
                                'large_file' => $fullDir . $largeName,
                                'compressed_file' => $fullDir . $compressedName
                            );

                            $updateAttachment = $attachmentObj->updateById($Attachid, $Updatedata);

                            if (file_exists($source)) {
                                unlink($source);
                            }
                            $full_path = $fullDir . $fileName;
                            $thumb_path = $fullDir . $thumbName;

                            $image = '<img width="180" height="150" class="class="img-responsive alt="Image" style="border-radius: 10%;" src="' . $this->getRequest()->getBaseUrl() . '/uploads/attachment_files/' . $subdir . $thumbName . '" />
							<div id="loader_view" style="display: none;">
                            <img class="img-responsive" src="' . $this->getRequest()->getBaseUrl() . '/pic/loading-4.gif' . '" />
                            </div>';

                            $path_data = array(
                                'image' => $image,
                                'attachment_id' => $Attachid
                            );

                            echo json_encode($path_data);
                        }
                    }
                }
            }
        }

        exit;
    }

    public function changeServiceStatusAction() {
        $serviceID = $this->request->getParam('service_id');
        $serviceStatus = $this->request->getParam('status');

        $servicesModel = new Model_Services();
        $data = array(
            'enable' => $serviceStatus
        );
        $userID = $servicesModel->updateById($serviceID, $data);
        exit;
    }

	 /**
     * Add new item action
     */
	 // By RAND
    public function addServiceDiscountAction() {

        //
        // check Auth for logged user
        //

      //  echo "hh" ; exit ;
        CheckAuth::checkPermission(array('settingsServicesAdd'));

        //
        // get request parameters
        //
        $service_id = $this->request->getParam('service_id');
        $min_size_range = $this->request->getParam('min_size_range');
        $max_size_range = $this->request->getParam('max_size_range');
        $unit_discount = $this->request->getParam('unit_discount');
		
        $router = Zend_Controller_Front::getInstance()->getRouter();
		
        $modelServiceDiscount = new Model_ServiceDiscount();

        //
        // init action form
        //
        $form = new Settings_Form_ServiceDiscount();

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $serviceObj = new Model_Services();

            
                $data = array(
                    'id' => '',
                    'service_id' => $service_id,
                    'min_size_range' => $min_size_range,
                    'max_size_range' => $max_size_range,
                    'unit_discount' => $unit_discount
                );

                $success = $modelServiceDiscount->insert($data);

                if ($success) {
                   $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Service Discount"));
                }

                echo 1;
                exit;
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('services/add_edit_discount.phtml');
        exit;
    }
	
	 public function editServiceDiscountAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsServicesEdit'));

        //
        // get request parameters
        //
        $service_id = $this->request->getParam('service_id');
        $min_size_range = $this->request->getParam('min_size_range');
        $max_size_range = $this->request->getParam('max_size_range');
        $unit_discount = $this->request->getParam('unit_discount');
		
        $id = $this->request->getParam('id');

        $modelServiceDiscount = new Model_ServiceDiscount();




        if (!CheckAuth::checkIfCanHandelAllCompany('service', $id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }


        //
        // validation
        //
        $serviceDiscount = $modelServiceDiscount->getById($id);
        if (!$serviceDiscount) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect($this->router->assemble(array(), 'settingsServicesList'));
            return;
        }


        //
        // init action form
        //
        $form = new Settings_Form_ServiceDiscount(array('mode' => 'update', 'service_discount' => $serviceDiscount));


        //
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                
               $data = array(
                    'min_size_range' => $min_size_range,
                    'max_size_range' => $max_size_range,
                    'unit_discount' => $unit_discount
                );

                $success = $modelServiceDiscount->updateById($id, $data);

                
                if ($success) {
                   $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Service Discount"));
                }

                echo 1;
                exit;
                
            }
        }

        $this->view->form = $form;
        $this->view->id = $id;
   //
        // render views
        //
        echo $this->view->render('services/add_edit_discount.phtml');
        exit;
    }
	
	
	//End RAND
	
}
