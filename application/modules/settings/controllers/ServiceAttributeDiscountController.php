<?php

class Settings_ServiceAttributeDiscountController extends Zend_Controller_Action {

    private $request;
    private $router;
    private $service_id;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
        $this->service_id = $this->request->getParam('service_id');
		$this->attribute_id = $this->request->getParam('attribute_id');

        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsServices';

        $Model_Services = new Model_Services();
        $Services = $Model_Services->getById($this->service_id);

        BreadCrumbs::setLevel(3, 'Attribute Discount to ' . $Services['service_name']);
        
        $pageName = $this->request->getParam('page_name', '');
        $this->view->page_title = (!empty($pageName))?$pageName." - Service Attribute Discount Settings":"Service Attribute Discount Settings";
    }
	
	 public function indexAction() {

        //
        // check Auth for logged user
        //
        //CheckAuth::checkPermission(array('settingsDiscountRangeList'));

        //$modelServiceAttachment = new Model_ServiceAttachment();
        //$modelServiceAttachment->executeQuery();
        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());
		
		$filters['service_id'] = $this->service_id;
		$filters['attribute_id'] = $this->attribute_id;

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        //
        // get data list
        //
        $ServiceAttributeDiscountObj = new Model_ServiceAttributeDiscount();
        $this->view->data = $ServiceAttributeDiscountObj->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);

        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
		$this->view->service_id = $this->service_id;
		$this->view->attribute_id = $this->attribute_id;
    }
	
	 public function deleteAction() {

        //
        // check Auth for logged user
        //
        
		//CheckAuth::checkPermission(array('settingDiscountRangeDelete'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
        if ($id) {
            $ids[] = $id;
        }

        $serviceAttributeDiscountObj = new Model_ServiceAttributeDiscount();
        foreach ($ids as $id) {
		  $serviceAttributePriceObj->deleteById($id);
            if (CheckAuth::checkIfCanHandelAllCompany('service', $id)) {
               $serviceAttributePriceObj->deleteById($id); 
            }
        }
        $this->_redirect($this->router->assemble(array('service_id' => $this->service_id,'attribute_id'=>$this->attribute_id), 'settingsServiceAttributeDiscountList'));
    }
	
		public function addAction(){
	  
	  //CheckAuth::checkPermission(array('settingsDiscountRangeAdd'));

        //
        // get request parameters
        //
        $attribute_id = $this->request->getParam('attribute_id');
        $min_size_range = $this->request->getParam('min_size_range');
        $max_size_range = $this->request->getParam('max_size_range');
        $min_discount_range = $this->request->getParam('min_discount_range');
        $max_discount_range = $this->request->getParam('max_discount_range');
		$service_id = $this->request->getParam('service_id');
		
		
        $router = Zend_Controller_Front::getInstance()->getRouter();
		
        $modelServiceAttributeDiscount= new Model_ServiceAttributeDiscount();

        //
        // init action form
        //
        $form = new Settings_Form_ServiceAttributeDiscount(array('service_id' => $service_id, 'attribute_id'=> $attribute_id));


        //
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                
               $data = array(
                    'min_size_range' => $min_size_range,
                    'max_size_range' => $max_size_range,
                    'min_discount_range' => $min_discount_range,
                    'max_discount_range' => $max_discount_range,
					'service_id' => $service_id,
					'attribute_id' => $attribute_id,
                );

                $success = $modelServiceAttributeDiscount->insert($data);

                
                if ($success) {
                   $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Service Discount"));
                }

                echo 1;
                exit;
                
            }
        }

        $this->view->form = $form;
        // render views
        //
        echo $this->view->render('service-attribute-discount/add-edit.phtml');
        exit; 
	
	
	}
	
	public function editAction(){
	
	  //
        // check Auth for logged user
        //
        //CheckAuth::checkPermission(array('settingsDiscountRangeEdit'));

        //
        // get request parameters
        //
		
        
        $service_id = $this->request->getParam('service_id');
        $attribute_id = $this->request->getParam('attribute_id');
        $min_size_range = $this->request->getParam('min_size_range');
        $max_size_range = $this->request->getParam('max_size_range');
        $max_discount_range = $this->request->getParam('max_discount_range');
        $min_discount_range = $this->request->getParam('min_discount_range');
		
        $id = $this->request->getParam('id');

        $modelServiceAttributeDiscount = new Model_ServiceAttributeDiscount();

        //
        // validation
        //
        $serviceAttributeDiscount = $modelServiceAttributeDiscount->getById($id);
        if (!$serviceAttributeDiscount) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            return;
        }


        //
        // init action form
        //
        $form = new Settings_Form_ServiceAttributeDiscount(array('mode' => 'update', 'service_attribute_discount' => $serviceAttributeDiscount,'service_id'=>$service_id,'attribute_id'=>$attribute_id));


        //
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                
               $data = array(
                    'min_size_range' => $min_size_range,
                    'max_size_range' => $max_size_range,
                    'min_discount_range' => $min_discount_range,
                    'max_discount_range' => $max_discount_range,
                );

                $success = $modelServiceAttributeDiscount->updateById($id, $data);

                
                if ($success) {
                   $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Service Attribute Price"));
                }

                echo 1;
                exit;
                
            }
        }

        $this->view->form = $form;
        $this->view->id = $id;
        echo $this->view->render('service-attribute-discount/add-edit.phtml');
        exit;
	 
	
	}
	
}