<?php
class Settings_SmsTemplateController extends Zend_Controller_Action {
    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsSms';
        BreadCrumbs::setLevel(2, 'SMS Template');
        
        $pageName = $this->request->getParam('page_name', '');
        $this->view->page_title = (!empty($pageName))?$pageName." - SMS Template Settings":"SMS Template Settings";
    }

    public function indexAction() {
		
		CheckAuth::checkPermission(array('settingsSmsTemplateList'));
        $orderBy = $this->request->getParam('sort', 'id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];
        $smsTemplateObj = new Model_SmsTemplate();
        $this->view->data = $smsTemplateObj->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
    }
    public function addAction() {
       CheckAuth::checkPermission(array('settingsSmsTemplateAdd'));
        $name = $this->request->getParam('name');
        $message = $this->request->getParam('message');
        $type = $this->request->getParam('type');
        $placeHolder = $this->request->getParam('placeholder');
        $router = Zend_Controller_Front::getInstance()->getRouter();
        $form = new Settings_Form_SmsTemplate();
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $smsTemplateObj = new Model_SmsTemplate();
                $data = array(
                    'name'        => trim($name),
                    'placeholder' => trim($placeHolder),
                    'message'     => $message,
                    'type'        => $type,
                );

                $success = $smsTemplateObj->insert($data);
							
                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in SMS Template"));
                }

                $this->_redirect($this->router->assemble(array(), 'settingsSmsTemplateList'));
            }
        }

        $this->view->form = $form;
    }

    public function deleteAction() {
        CheckAuth::checkPermission(array('settingsSmsTemplateDelete'));
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
        if ($id) {
            $ids[] = $id;
        }

        $smsTemplateObj = new Model_SmsTemplate();
        foreach ($ids as $id) {
            $smsTemplateObj->deleteById($id);
        }
        $this->_redirect($this->router->assemble(array(), 'settingsSmsTemplateList'));
    }

    public function editAction() {
		
		CheckAuth::checkPermission(array('settingsSmsTemplateEdit'));
		$id = $this->request->getParam('id');
        $name = $this->request->getParam('name');
        $message = $this->request->getParam('message');
        $type = $this->request->getParam('type');
        $router = Zend_Controller_Front::getInstance()->getRouter();
        $form = new Settings_Form_SmsTemplate();
		$smsTemplateObj = new Model_SmsTemplate();
        $smsTemplate = $smsTemplateObj->getById($id);
        if (!$smsTemplate) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect($this->router->assemble(array(), 'settingsSmsTemplateList'));
            return;
        }
        $this->view->placeholder = $smsTemplate['placeholder'];
        $this->view->template_id = $id;
        $form = new Settings_Form_SmsTemplate(array('mode' => 'update', 'smsTemplate' => $smsTemplate));
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'name'          => trim($name),
                    'message'       => $message,
                    'type'          => $type,
               );
        $success = $smsTemplateObj->updateById($id, $data);
        if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Sms Template"));
                }
			
                $this->_redirect($this->router->assemble(array(), 'settingsSmsTemplateList'));
            }
        }

        $this->view->form = $form;
    }
	
}
