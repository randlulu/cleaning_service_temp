<?php

class Settings_EmailTemplateController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsEmail';

        BreadCrumbs::setLevel(2, 'Email Template');
    }

    /**
     * Items list action
     */
    public function indexAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsEmailTemplateList'));

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        //
        // get data list
        //
        $emailTemplateObj = new Model_EmailTemplate();
        $this->view->data = $emailTemplateObj->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);

        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
    }

    /**
     * Add new item action
     */
    public function addAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsEmailTemplateAdd'));

        //
        // get request parameters
        //
        $name = $this->request->getParam('name');
        $body = $this->request->getParam('body');
        $subject = $this->request->getParam('subject');
        $placeHolder = $this->request->getParam('placeholder');
        $router = Zend_Controller_Front::getInstance()->getRouter();

        //
        // init action form
        //
        $form = new Settings_Form_EmailTemplate();

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $emailTemplateObj = new Model_EmailTemplate();
                $data = array(
                    'name' => trim($name),
                    'placeholder' => trim($placeHolder),
                    'body' => $body,
                    'subject' => $subject
                );

                $success = $emailTemplateObj->insert($data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Email Template"));
                }

                $this->_redirect($this->router->assemble(array(), 'settingsEmailTemplateList'));
            }
        }

        $this->view->form = $form;
    }

    public function deleteAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsEmailTemplateDelete'));

        if (!get_config('under_construction')) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "could not be deleted ,This Item used in other Place "));
            $this->_redirect($this->router->assemble(array(), 'settingsEmailTemplateList'));
        }

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
        if ($id) {
            $ids[] = $id;
        }

        $emailTemplateObj = new Model_EmailTemplate();
        foreach ($ids as $id) {
            $emailTemplateObj->deleteById($id);
        }
        $this->_redirect($this->router->assemble(array(), 'settingsEmailTemplateList'));
    }

    public function editAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsEmailTemplateEdit'));

        //
        // get request parameters
        //
        $name = $this->request->getParam('name');
        $body = $this->request->getParam('body');
        $subject = $this->request->getParam('subject');
        $placeHolder = $this->request->getParam('placeholder');
        $id = $this->request->getParam('id');


        //
        // validation
        //
        $emailTemplateObj = new Model_EmailTemplate();
        $emailTemplate = $emailTemplateObj->getById($id);
        if (!$emailTemplate) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect($this->router->assemble(array(), 'settingsEmailTemplateList'));
            return;
        }
        $this->view->placeholder = $emailTemplate['placeholder'];


        //
        // init action form
        //
        $form = new Settings_Form_EmailTemplate(array('mode' => 'update', 'emailTemplate' => $emailTemplate));


        //
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'name' => trim($name),
                    'placeholder' => trim($placeHolder),
                    'body' => $body,
                    'subject' => $subject
                );

                $success = $emailTemplateObj->updateById($id, $data);
                //$this->view->successMessage = 'Updated Successfully';
                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Email Template"));
                }


                $this->_redirect($this->router->assemble(array(), 'settingsEmailTemplateList'));
            }
        }

        $this->view->form = $form;
    }
	
	/**
     * 22/06/2015 D.A
     * get trading images by Template ID action
     */
    public function tradingImagesAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsEmailTemplateList'));

        $images=array();
        $trading_names=array();
        $trading_name_imgs=array();
        $trading_name_ids=array();
        $imgs_name_part=array();
        $template_name_id = $this->_request->getParam('template_name_id');
        $trading_name_id = $this->_request->getParam('trading_name_id');
        $emailTemplateObj = new Model_EmailTemplate();
        $result = $emailTemplateObj->getById($template_name_id);
        $images=explode(',', $result['imagePlaceholder']);
        $template_name=$result['name'];
        $trading_namesImagesobj = new Model_TradingNameImages();
        $trading_namesImages = $trading_namesImagesobj->getByTradingnameIdandTemplateId($trading_name_id,$template_name_id);
        if($trading_namesImages){
        foreach ($trading_namesImages as $trading_namesImage) {
            $trading_name_imgs[]= $trading_namesImage['trading_name_img'];
            $trading_name_ids[]= $trading_namesImage['id'];
            $imgname=explode('.', $trading_namesImage['trading_name_img']);
            $imgs_name_part[]= $imgname[0];
        }
        }

        $trading_namesObj = new Model_TradingName();
        $trading_names = $trading_namesObj->getById($trading_name_id);
        $trading_name= $trading_names['trading_name'];

        $results = array(
            'imagePlaceholder' =>$images,
            'trading_name_imgs' =>$trading_name_imgs,
            'trading_name_ids' =>$trading_name_ids,
            'trading_name' =>$trading_name,
            'template_name' =>$template_name,
            'imgs_name_part' =>$imgs_name_part
        );
        echo $this->_helper->json($results);exit;
    }


}

