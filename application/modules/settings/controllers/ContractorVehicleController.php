<?php

class Settings_ContractorVehicleController extends Zend_Controller_Action {

    private $request;
    private $router;
    private $contractor_info_id;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
        $this->contractor_info_id = $this->request->getParam('contractor_info_id');

        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsUser';

        //
        // get data
        //
        $contractorInfoObj = new Model_ContractorInfo();
        $contractorInfo = $contractorInfoObj->getById($this->contractor_info_id);

        BreadCrumbs::setLevel(4, 'Contractor Vehicles');
    }

    /**
     * Items list action
     */
    public function indexAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsContractorVehicleList'));

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'vehicle_id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if($filters){
            foreach ($filters as &$filter){
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];


        //
        //get user email by id
        //
        $filters['contractor_info_id'] = $this->contractor_info_id;


        //
        // get data list
        //
        $contractorVehicleObj = new Model_ContractorVehicle();
        $this->view->data = $contractorVehicleObj->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);

        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
        $this->view->contractor_info_id = $this->contractor_info_id;
    }

    /**
     * Add new item action
     */
    public function addAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsContractorVehicleAdd'));

        //
        // get request parameters
        //
        $registrationNumber = $this->request->getParam('registration_number');
        $make = $this->request->getParam('make');
        $model = $this->request->getParam('model');
        $colour = $this->request->getParam('colour');
        $router = Zend_Controller_Front::getInstance()->getRouter();


        if (!CheckAuth::checkIfCanHandelAllCompany('contractor_info', $this->contractor_info_id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // init action form
        //
        $form = new Settings_Form_ContractorVehicle(array('contractor_info_id' => $this->contractor_info_id));
         $attachmentObj = new Model_Attachment();
         $vehicleAttachmentObj = new Model_VehicleAttachment();
        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $contractorVehicleObj = new Model_ContractorVehicle();
                $data = array(
                    'registration_number' => $registrationNumber,
                    'make' => $make,
                    'model' => $model,
                    'colour' => $colour,
                    'contractor_info_id' => $this->contractor_info_id
                );

                $success = $contractorVehicleObj->insert($data);
				
				//calculate profile completness.....
                $modelUser = new Model_User();
                $modelContractorInfo = new Model_ContractorInfo();
                $contractorInfo = $modelContractorInfo->getById($this->contractor_info_id);
                $modelUser->calculateProfileCompleteness($contractorInfo['contractor_id']);

                if ($success) {
				    $upload = new Zend_File_Transfer_Adapter_Http();
                    $files  = $upload->getFileInfo();
					$counter = 0 ;
					foreach($files as $file => $fileInfo) {
					  if ($upload->isUploaded($file)){
                        if ($upload->receive($file)){
						   $counter = $counter + 1;
						   $info = $upload->getFileInfo($file);
                           $source  = $info[$file]['tmp_name'];
                           $imageInfo = pathinfo($source);
                           $ext = $imageInfo['extension'];
						   $size = $info[$file]['size'];
						   $type = $info[$file]['type'];
				           $dir = get_config('attachment') . '/';
                           $subdir = date('Y/m/d/');
                           $fullDir = $dir . $subdir;
                           if (!is_dir($fullDir)) {
                              mkdir($fullDir, 0777, true);
                           }
                        $loggedUser = CheckAuth::getLoggedUser();							
						$data = array(
							 'created_by'=>$loggedUser['user_id'] 
							 ,'created'=>time()
							 ,'size' => $size
							 ,'type'=>$type
						 );
						 
						
						 
						 $id = $attachmentObj->insert($data);
						 $vehicleAttachmentObj->insert(array('vehicle_id'=>$success,'attachment_id'=>$id));
						 $fileName = $success.'_'.$counter.'.'. $ext;
						 $image_saved = copy($source, $fullDir . $fileName);
						 $typeParts = explode("/",$type);
						 if($typeParts[0] == 'image'){
						    $thumbName = $success.'_thumbnail_'.$counter.'.'. $ext;
						    ImageMagick::create_thumbnail($source, $fullDir . $thumbName, 78, 64);
						 }else{
						   $thumbName = $success.'_'.$counter.'.jpg';
						   ImageMagick::convertFile($source.'[0]', $fullDir . $thumbName);
						 }
						 
						 $Updatedata = array(
                          'path' => $fullDir . $fileName,
                          'file_name' => $fileName,
						  'thumbnail_file' => $fullDir . $thumbName
                          );
						 $attachmentObj->updateById($id,$Updatedata);
						 
						  if ($image_saved) {
								if (file_exists($source)) {
									unlink($source);
								}               
							}
						}
					 }	
					}
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                }
                //$this->_redirect($this->router->assemble(array(), 'settingsUserEmailList'));
                echo 1;
                exit;
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('contractor-vehicle/add_edit.phtml');
        exit;
    }
	
	
	
	
	   public function editAction() {

        //
        // check Auth for logged user
        //
        //CheckAuth::checkPermission(array('settingsContractorVehicleEdit'));

        //
        // get request parameters
        //
        $registrationNumber = $this->request->getParam('registration_number');
        $make = $this->request->getParam('make');
        $model = $this->request->getParam('model');
        $colour = $this->request->getParam('colour');
        $id = $this->request->getParam('id');
        $router = Zend_Controller_Front::getInstance()->getRouter();
		$contractorVehicleObj = new Model_ContractorVehicle();


        if (!CheckAuth::checkIfCanHandelAllCompany('contractor_info', $this->contractor_info_id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // init action form
        //
		
		$contractor_Vehicle = $contractorVehicleObj->getById($id);
        $form = new Settings_Form_ContractorVehicle(array('contractor_info_id' => $this->contractor_info_id,'contractor_Vehicle'=>$contractor_Vehicle,'mode'=>'update'));
         $attachmentObj = new Model_Attachment();
         $vehicleAttachmentObj = new Model_VehicleAttachment();
        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                
				$deleted_ids = $this->request->getParam('deleted_ids','');
				$successDeleted = 0;
				if(!empty($deleted_ids)){
				 $deleted_ids = explode(",",$deleted_ids);
				 $modelAttachment = new Model_Attachment();
				 foreach($deleted_ids as $deleted_id){
                    $successDeleted = $modelAttachment->updateById($deleted_id , array('is_deleted' => '1'));
				 } 
				}
				
                $data = array(
                    'registration_number' => $registrationNumber,
                    'make' => $make,
                    'model' => $model,
                    'colour' => $colour,
                    'contractor_info_id' => $this->contractor_info_id
                );

                $success = $contractorVehicleObj->updateById($id,$data);
				
				//calculate profile completness.....
                $modelUser = new Model_User();
                $modelContractorInfo = new Model_ContractorInfo();
                $contractorInfo = $modelContractorInfo->getById($this->contractor_info_id);
                $modelUser->calculateProfileCompleteness($contractorInfo['contractor_id']);

                if ($id) {
				    $upload = new Zend_File_Transfer_Adapter_Http();
                    $files  = $upload->getFileInfo();
					$Attachemnts = $attachmentObj->getAll(null,$pager,array('type'=>'vehicle','itemid'=>$id));
					$counter = count($Attachemnts) ;
					foreach($files as $file => $fileInfo) {
					  if ($upload->isUploaded($file)){
                        if ($upload->receive($file)){
						   $counter = $counter + 1;
						   $info = $upload->getFileInfo($file);
                           $source  = $info[$file]['tmp_name'];
                           $imageInfo = pathinfo($source);
                           $ext = $imageInfo['extension'];
						   $size = $info[$file]['size'];
						   $type = $info[$file]['type'];
				           $dir = get_config('attachment') . '/';
                           $subdir = date('Y/m/d/');
                           $fullDir = $dir . $subdir;
                           if (!is_dir($fullDir)) {
                              mkdir($fullDir, 0777, true);
                           }
                        $loggedUser = CheckAuth::getLoggedUser();							
						$data = array(
							 'created_by'=>$loggedUser['user_id'] 
							 ,'created'=>time()
							 ,'size' => $size
							 ,'type'=>$type
						 );
						 
						
						 
						 $attach_id = $attachmentObj->insert($data);
						 $vehicleAttachmentObj->insert(array('vehicle_id'=>$id,'attachment_id'=>$attach_id));
						 $fileName = $id.'_'.$counter.'.'. $ext;
						 $image_saved = copy($source, $fullDir . $fileName);
						 $typeParts = explode("/",$type);
						 if($typeParts[0] == 'image'){
						    $thumbName = $id.'_thumbnail_'.$counter.'.'. $ext;
						    ImageMagick::create_thumbnail($source, $fullDir . $thumbName, 78, 64);
						 }else{
						   $thumbName = $success.'_'.$counter.'.jpg';
						   ImageMagick::convertFile($source.'[0]', $fullDir . $thumbName);
						 }
						 
						 $Updatedata = array(
                          'path' => $fullDir . $fileName,
                          'file_name' => $fileName,
						  'thumbnail_file' => $fullDir . $thumbName
                          );
						 $attachmentObj->updateById($attach_id,$Updatedata);
						 
						  if ($image_saved) {
								if (file_exists($source)) {
									unlink($source);
								}               
							}
						}
					 }	
					}
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                }
                //$this->_redirect($this->router->assemble(array(), 'settingsUserEmailList'));
                echo 1;
                exit;
            }
        }
        $this->view->id = $id;
        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('contractor-vehicle/add_edit.phtml');
        exit;
    }

    public function deleteAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsContractorVehicleDelete'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
        if ($id) {
            $ids[] = $id;
        }

        $contractorVehicleObj = new Model_ContractorVehicle();
		$modelAttachment = new Model_Attachment();
        foreach ($ids as $id) {
            if (CheckAuth::checkIfCanHandelAllCompany('contractorVehicle', $id)) {
			        $filter = array('type'=>'vehicle','itemid'=>$id);        
					$pager = null;
					$attachments =  $modelAttachment->getAll('a.created desc',$pager, $filter);
					if($attachments ){
					  foreach($attachments as $attachment){
						$modelAttachment->updateById($attachment['attachment_id'] , array('is_deleted' => '1'));
					  }
					}
                $contractorVehicleObj->deleteById($id);
            }
        }
        $this->_redirect($this->router->assemble(array('contractor_info_id' => $this->contractor_info_id), 'settingsContractorVehicleList'));
    }

}

