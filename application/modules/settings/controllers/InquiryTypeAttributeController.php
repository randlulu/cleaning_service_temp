<?php

class Settings_InquiryTypeAttributeController extends Zend_Controller_Action {

    private $request;
    private $router;
    private $inquiry_type_id;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
        $this->inquiry_type_id = $this->request->getParam('inquiry_type_id');

        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsType';

        $Model_InquiryType = new Model_InquiryType();
        $InquiryType = $Model_InquiryType->getById($this->inquiry_type_id);

        BreadCrumbs::setLevel(3, 'Attribute to ' . $InquiryType['inquiry_name']);
    }

    /**
     * Items list action
     */
    public function indexAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsInquiryTypeAttributeList'));

        if (!CheckAuth::checkIfCanHandelAllCompany('inquiry_type', $this->inquiry_type_id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'inquiry_type_attribute_id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if($filters){
            foreach ($filters as &$filter){
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];


        //
        //get inquiry type attribute by id
        //
        $filters['inquiry_type_id'] = $this->inquiry_type_id;


        //
        // get data list
        //
        $inquiryTypeAttributeObj = new Model_InquiryTypeAttribute();
        $this->view->data = $inquiryTypeAttributeObj->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);

        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
        $this->view->inquiry_type_id = $this->inquiry_type_id;
    }

    /**
     * Add new item action
     */
    public function addAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsInquiryTypeAttributeAdd'));


        if (!CheckAuth::checkIfCanHandelAllCompany('inquiry_type', $this->inquiry_type_id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // get request parameters
        //
        $attribute_id = $this->request->getParam('attribute_id');
        $router = Zend_Controller_Front::getInstance()->getRouter();

        //
        // init action form
        //
        $form = new Settings_Form_InquiryTypeAttribute(array('inquiry_type_id' => $this->inquiry_type_id));

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $inquiryTypeAttributeObj = new Model_InquiryTypeAttribute();
                $data = array(
                    'attribute_id' => $attribute_id,
                    'inquiry_type_id' => $this->inquiry_type_id
                );
                if ($inquiryTypeAttributeObj->getByAttributeIdAndInquiryTypeId($attribute_id, $this->inquiry_type_id)) {
                    $success = false;
                } else {
                    $success = $inquiryTypeAttributeObj->insert($data);
                }

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                }
                //$this->_redirect($this->router->assemble(array(), 'settingsInquiryTypeAttributeList'));
                echo 1;
                exit;
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('inquiry-type-attribute/add_edit.phtml');
        exit;
    }

    public function deleteAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsInquiryTypeAttributeDelete'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
        if ($id) {
            $ids[] = $id;
        }

        $inquiryTypeAttributeObj = new Model_InquiryTypeAttribute();
        foreach ($ids as $id) {
            if (CheckAuth::checkIfCanHandelAllCompany('inquiry_type_attribute', $id)) {
                
                $isNotRelated = $inquiryTypeAttributeObj->checkBeforeDeleteIinquiryTypeAttribute($id);
                if ($isNotRelated) {
                    $inquiryTypeAttributeObj->deleteById($id);
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Error: Failed to delete, item in use "));
                }
                
            }
        }
        $this->_redirect($this->router->assemble(array('inquiry_type_id' => $this->inquiry_type_id), 'settingsInquiryTypeAttributeList'));
    }

}

