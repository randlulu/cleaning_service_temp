<?php

class Settings_SettingsController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settings'));

        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
		
        BreadCrumbs::resetBreadCrumb();
        BreadCrumbs::setHomeLink('Home', '/');
		BreadCrumbs::setLevel(1, 'Settings');
    }

    /**
     * Items list action
     */
    public function indexAction() {
        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsHome';
        $pageName = $this->request->getParam('page_name', '');
        $this->view->page_title = (!empty($pageName))?$pageName." - Settings":"Settings";
    }

    public function settingsLocationAction() {
        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsLocation';
        BreadCrumbs::setLevel(1, 'Settings Location');
        $pageName = $this->request->getParam('page_name', '');
        $this->view->page_title = (!empty($pageName))?$pageName." - Location Settings":"Location Settings";
    }

    public function settingsPaymentAction() {
        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsPayment';
        BreadCrumbs::setLevel(1, 'Settings Payment');
        $pageName = $this->request->getParam('page_name', '');
        $this->view->page_title = (!empty($pageName))?$pageName." - Payment Settings":"Payment Settings";
    }

    public function settingsUserAction() {
        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsUser';
        BreadCrumbs::setLevel(1, 'Settings User');
        $pageName = $this->request->getParam('page_name', '');
        $this->view->page_title = (!empty($pageName))?$pageName." - User Settings":"User Settings";
    }
	public function settingsCompanyAction() {
        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsCompany';
        BreadCrumbs::setLevel(1, 'Settings Companies');
        $pageName = $this->request->getParam('page_name', '');
        $this->view->page_title = (!empty($pageName))?$pageName." - Companies Settings":"Companies Settings";
    }

    public function settingsServicesAction() {
        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsServices';
        BreadCrumbs::setLevel(1, 'Settings Services');
        $pageName = $this->request->getParam('page_name', '');
        $this->view->page_title = (!empty($pageName))?$pageName." - Services Settings":"Services Settings";
    }

    public function settingsTypeAction() {
        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsType';
        BreadCrumbs::setLevel(1, 'Settings Type');
        $pageName = $this->request->getParam('page_name', '');
        $this->view->page_title = (!empty($pageName))?$pageName." - Type Settings":"Type Settings";
    }

    public function settingsEmailAction() {
        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsEmail';
        BreadCrumbs::setLevel(1, 'Settings Email');
        $pageName = $this->request->getParam('page_name', '');
        $this->view->page_title = (!empty($pageName))?$pageName." - Public Settings":"Public Settings";
    }
    
    public function settingsLabelAction() {
        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsLabel';
        BreadCrumbs::setLevel(1, 'Settings Label');
        $pageName = $this->request->getParam('page_name', '');
        $this->view->page_title = (!empty($pageName))?$pageName." - Label Settings":"Label Settings";
    }
	
	public function settingsSecurityAction() {
        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsSecurity';
        BreadCrumbs::setLevel(1, 'Settings Security');
        $pageName = $this->request->getParam('page_name', '');
        $this->view->page_title = (!empty($pageName))?$pageName." - Security Settings":"Security Settings";
    }

}

