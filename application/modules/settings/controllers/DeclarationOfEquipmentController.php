<?php

class Settings_DeclarationOfEquipmentController extends Zend_Controller_Action {

    private $request;
    private $router;
    private $contractor_info_id;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router  = Zend_Controller_Front::getInstance()->getRouter();
        $this->contractor_info_id = $this->request->getParam('contractor_info_id');
        
        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsUser';
        
        //
        // get data
        //
        $contractorInfoObj = new Model_ContractorInfo();
        $contractorInfo = $contractorInfoObj->getById($this->contractor_info_id);
        
        BreadCrumbs::setLevel(4, 'Declaration Of Equipment');
    }


    /**
     * Items list action
     */
    public function indexAction() {
        
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsDeclarationOfEquipmentList'));
        
        //
        // get request parameters
        //
        $orderBy        = $this->request->getParam('sort', 'id');
        $sortingMethod  = $this->request->getParam('method', 'asc');
        $currentPage    = $this->request->getParam('page', 1);
        $filters        = $this->request->getParam('fltr', array());
        
        

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage     = 20;
        $pager->currentPage = $currentPage;
        $pager->url         = $_SERVER['REQUEST_URI'];

        
        //
        //get user email by id
        //
        $filters['contractor_info_id'] = $this->contractor_info_id;
        
        
        //
        // get data list
        //
        $declarationOfEquipmentObj = new Model_DeclarationOfEquipment();
        $this->view->data = $declarationOfEquipmentObj->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);
        
        //
        // set view params
        //
        $this->view->currentPage   = $currentPage;
        $this->view->perPage       = $pager->perPage;
        $this->view->pageLinks     = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy       = $orderBy;
        $this->view->filters       = $filters;
        $this->view->contractor_info_id = $this->contractor_info_id;
        
    }


    /**
     * Add new item action
     */
    public function addAction() {
        
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsDeclarationOfEquipmentAdd'));
        
        //
        // get request parameters
        //
        $equipment    = $this->request->getParam('equipment');
        $router = Zend_Controller_Front::getInstance()->getRouter();
        
        //
        // init action form
        //
		$attachmentObj = new Model_Attachment();
		$declarationOfEquipmentAttachmentObj = new Model_DeclarationOfEquipmentAttachment();
        $form = new Settings_Form_DeclarationOfEquipment(array('contractor_info_id'=>  $this->contractor_info_id));

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $declarationOfEquipmentObj = new Model_DeclarationOfEquipment();
                $data = array(
                    'equipment'   => $equipment,
                    'contractor_info_id' => $this->contractor_info_id
                );

                $success = $declarationOfEquipmentObj->insert($data);
				
				  //calculate profile completness.....
                $modelUser = new Model_User();
                $modelContractorInfo = new Model_ContractorInfo();
                $contractorInfo = $modelContractorInfo->getById($this->contractor_info_id);
                $modelUser->calculateProfileCompleteness($contractorInfo['contractor_id']);
                
                if($success) {
				    $upload = new Zend_File_Transfer_Adapter_Http();
                    $files  = $upload->getFileInfo();
					$counter = 0 ;
					foreach($files as $file => $fileInfo) {
					  if ($upload->isUploaded($file)){
                        if ($upload->receive($file)){
						   $counter = $counter + 1;
						   $info = $upload->getFileInfo($file);
                           $source  = $info[$file]['tmp_name'];
                           $imageInfo = pathinfo($source);
                           $ext = $imageInfo['extension'];
						   $size = $info[$file]['size'];
						   $type = $info[$file]['type'];
				           $dir = get_config('attachment') . '/';
                           $subdir = date('Y/m/d/');
                           $fullDir = $dir . $subdir;
                           if (!is_dir($fullDir)) {
                              mkdir($fullDir, 0777, true);
                           }
                        $loggedUser = CheckAuth::getLoggedUser();							
						$data = array(
							 'created_by'=>$loggedUser['user_id'] 
							 ,'created'=>time()
							 ,'size' => $size
							 ,'type'=>$type
						 );
						 
						
						 
						 $id = $attachmentObj->insert($data);
						 $declarationOfEquipmentAttachmentObj->insert(array('equipment_id'=>$success,'attachment_id'=>$id));
						 $fileName = $success.'_'.$counter.'.'. $ext;
						 $image_saved = copy($source, $fullDir . $fileName);
						 
						 $typeParts = explode("/",$type);
						 if($typeParts[0] == 'image'){
						    $thumbName = $success.'_thumbnail_'.$counter.'.'. $ext;
						    ImageMagick::create_thumbnail($source, $fullDir . $thumbName, 78, 64);
				
						 }else{
						   
						   $thumbName = $success.'_'.$counter.'.jpg';
						   ImageMagick::convertFile($source.'[0]', $fullDir . $thumbName);
						 }
						 
						 $Updatedata = array(
                          'path' => $fullDir . $fileName,
                          'file_name' => $fileName,
						  'thumbnail_file' => $fullDir . $thumbName
                          );
						 $attachmentObj->updateById($id,$Updatedata);
						 
						  if ($image_saved) {
								if (file_exists($source)) {
									unlink($source);
								}               
							}
						}
					 }	
					}
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                }
                //$this->_redirect($this->router->assemble(array(), 'settingsUserEmailList'));
                echo 1;
                exit;
            }
        }

        $this->view->form       = $form;
        //
        // render views
        //
        echo $this->view->render('declaration-of-equipment/add_edit.phtml');
        exit;
    }



    public function deleteAction() {
        
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsDeclarationOfEquipmentDelete'));
        
        //
        // get request parameters
        //
        $id  = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array() );
        if($id) {
            $ids[] = $id;
        }

        $declarationOfEquipmentObj = new Model_DeclarationOfEquipment();
		$modelAttachment = new Model_Attachment();
        foreach($ids as $id) {
		    $filter = array('type'=>'equipment','itemid'=>$id);    
            $pager = null;			
		    $attachments =  $modelAttachment->getAll('a.created desc',$pager, $filter);
			if($attachments ){
			  foreach($attachments as $attachment){
			    $modelAttachment->updateById($attachment['attachment_id'] , array('is_deleted' => '1'));
			  }
			}
            $declarationOfEquipmentObj->deleteById($id);
        }
        $this->_redirect($this->router->assemble(array('contractor_info_id' => $this->contractor_info_id), 'settingsDeclarationOfEquipmentList'));

    }
    
    	public function getImagesAction(){
	  
	  
	 $equipment_id = $this->request->getParam('id', 0);
	 $declarationOfEquipmentObj = new Model_DeclarationOfEquipment();
     $Attachemnts = $declarationOfEquipmentObj->getAllAttachmentById($equipment_id);
     
   
	 
	 header('Content-type: text/json');              
     header('Content-type: application/json');
	 echo json_encode($Attachemnts);
	 exit;
	 
	}
         public function viewAction() {
        //
        // check Auth for logged user
        //
       // CheckAuth::checkPermission(array('settingsContractorOwnerview'));

        //
        // get request parameters
        //
      
        $equipmentId = $this->request->getParam('id');

        // load model

        $declarationOfEquipmentObj = new Model_DeclarationOfEquipment();

        $equipmentInfo = $declarationOfEquipmentObj->getById($equipmentId);

//        if (!$contractorOwner) {
//            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
//            $this->_redirect($this->router->assemble(array('contractor_info_id' => $contractorOwner['contractor_info_id']), 'settingsContractorOwnerList'));
//            return;
//        }
//        if (!CheckAuth::checkIfCanHandelAllCompany('contractor_info', $contractorOwner['contractor_info_id'])) {
//            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
//            $this->_redirect($this->router->assemble(array('contractor_info_id' => $contractorOwner['contractor_info_id']), 'settingsContractorOwnerList'));
//            return;
//        }
                
 
     $Attachemnts = $declarationOfEquipmentObj->getAllAttachmentById($equipmentId);
     
        $this->view->equipmentInfo = $equipmentInfo;
        $this->view->Attachemnts = $Attachemnts;

        //
        // render views
        //
        echo $this->view->render('declaration-of-equipment/view.phtml');
        exit;
    }
    
    public function editAction() {
	
				
        
        $equipment = $this->request->getParam('equipment');
		$id = $this->request->getParam('id');
        // validation
        //
        $declarationOfEquipmentObj = new Model_DeclarationOfEquipment();
        $modelDeclarationOfEquipmentAttachment = new Model_DeclarationOfEquipmentAttachment();
		$attachmentObj = new Model_Attachment();
        $equipmentInfo = $declarationOfEquipmentObj->getById($id);
        
		
       if (!CheckAuth::checkIfCanHandelAllCompany('contractor_info', $this->contractor_info_id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
		
		$form = new Settings_Form_DeclarationOfEquipment(array('equipmentInfo' => $equipmentInfo,'mode'=>'update'));
		   
	
		if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data			  
			   $data = array(
                  
                    'equipment' => $equipment,
                               
                );
				
				$deleted_ids = $this->request->getParam('deleted_ids','');
				$successDeleted = 0;
				if(!empty($deleted_ids)){
				 $deleted_ids = explode(",",$deleted_ids);
				 $modelAttachment = new Model_Attachment();
				 foreach($deleted_ids as $deleted_id){
                    $successDeleted = $modelAttachment->updateById($deleted_id , array('is_deleted' => '1'));
				 } 
				}
				
				$success = $declarationOfEquipmentObj->updateById($id, $data);
				
				$updateAttachment = 0;				
				$upload = new Zend_File_Transfer_Adapter_Http();
                $files  = $upload->getFileInfo();
				$Attachments = $declarationOfEquipmentObj->getAllAttachmentById($id);
				$counter = count($Attachments);
					foreach($files as $file => $fileInfo) {
					  if ($upload->isUploaded($file)){
                        if ($upload->receive($file)){
                           $counter = $counter + 1;						
						   $info = $upload->getFileInfo($file);						  
                           $source  = $info[$file]['tmp_name'];
                           $imageInfo = pathinfo($source);
                           $ext = $imageInfo['extension'];
						   $size = $info[$file]['size'];
						   $type = $info[$file]['type'];
				           $dir = get_config('attachment') . '/';
                                       
                           $subdir = date('Y/m/d/');
                           $fullDir = $dir . $subdir;
                           if (!is_dir($fullDir)) {
                              mkdir($fullDir, 0777, true);
                          }
                        $loggedUser = CheckAuth::getLoggedUser();							
						$data = array(
							 'created_by'=>$loggedUser['user_id'] 
							 ,'created'=>time()
							 ,'size' => $size
							 ,'type' =>$type
						 );
						 
						
						 
						 $Attachid = $attachmentObj->insert($data);
						 $equipmentInfoAttachmentId = $modelDeclarationOfEquipmentAttachment->insert(array('equipment_id'=>$id,'attachment_id'=>$Attachid));					 
						 $fileName = $id.'_'.$counter.'_'.'equipment'.'.'. $ext;
						 $image_saved = copy($source, $fullDir . $fileName);
						 $typeParts = explode("/",$type);
						 if($typeParts[0] == 'image'){
						    $thumbName = $id.'_'.$counter.'_'.'equipment_thumbnail'.'.'. $ext;
						    ImageMagick::create_thumbnail($source, $fullDir . $thumbName, 78, 64);
						 }else{
						   $thumbName = $id.'_'.$counter.'_'.'equipment'.'.jpg';
						   ImageMagick::convertFile($source.'[0]', $fullDir . $thumbName);
						 }
						 
						 
						 
						 $Updatedata = array(
                          'path' => $fullDir . $fileName,
                          'file_name' => $fileName,
						  'thumbnail_file' => $fullDir . $thumbName
                          );
						  
						  
						 $updateAttachment = $attachmentObj->updateById($Attachid,$Updatedata);						 
						  if ($image_saved) {
								if (file_exists($source)) {
									unlink($source);
								}               
							}
						}
					 }	
					}
                if ($success || $updateAttachment) {    

                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                }
                //$this->view->successMessage = 'Updated Successfully';
                echo 1;
                exit;
                //$this->_redirect($this->router->assemble(array(), 'settingsContractorInfoList'));
                //return;
			
			}
		}


		
	   $this->view->form = $form;
       $this->view->id = $id;
       $this->view->contractorID = $equipmentInfo['contractor_info_id'];
	   
       
        echo $this->view->render('declaration-of-equipment/add_edit.phtml');
        exit;
	}
}

