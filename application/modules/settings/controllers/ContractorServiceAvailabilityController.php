<?php

class Settings_ContractorServiceAvailabilityController extends Zend_Controller_Action {

    private $request;
    private $router;
    private $contractor_service_id;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
        $this->contractor_service_id = $this->request->getParam('contractor_service_id', 0);

        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsUser';

        BreadCrumbs::setLevel(5, 'Service Availability');
        
        $pageName = $this->request->getParam('page_name', '');
        $this->view->page_title = (!empty($pageName))?$pageName." - Contractor Service Availability - Settings":"Contractor Service Availability - Settings";
    }

    /**
     * Items list action
     */
    public function indexAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsContractorServiceAvailabilityList'));
        $contractor_id = $this->request->getParam('contractor_id', 0);

//        var_dump($contractorServices);
        if ($contractor_id) {
            $model_contractorService = new Model_ContractorService();
            $contractorServices = $model_contractorService->getAll(array('contractor_id' => $contractor_id));
            $id = $contractorServices[0]['contractor_service_id'];
        } else {
            $id = $this->contractor_service_id;
        }

        if (!CheckAuth::checkIfCanHandelAllCompany('contractor_service', $id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // get request parameters
        //

        $orderBy = $this->request->getParam('sort', 'id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];
        $contractorServiceAvailabilityObj = new Model_ContractorServiceAvailability();

        if ($this->contractor_service_id) {
            //
            //get user email by id
            //
        $filters['contractor_service_id'] = $this->contractor_service_id;
            //
            // get data list
            //
            $this->view->data = $contractorServiceAvailabilityObj->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);
        } else {
            $filters['contractor_id'] = $contractor_id;
            $filters['group_by'] = 'city_id';
            $this->view->data = $contractorServiceAvailabilityObj->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);
        }
        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
        $this->view->contractor_service_id = $this->contractor_service_id;
        $this->view->contractor_id = $contractor_id;
    }

    /**
     * Add new item action
     */
    public function addAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsContractorServiceAvailabilityAdd'));

        //
        // get request parameters
        //
        $cityIds = $this->request->getParam('city_id', array());
        $countryId = $this->request->getParam('country_id');
        $router = Zend_Controller_Front::getInstance()->getRouter();

        $contractor_id = $this->request->getParam('contractor_id', 0);
        $model_contractorService = new Model_ContractorService();
//        var_dump($contractorServices);
        if ($contractor_id) {
            $contractorServicesFirst = $model_contractorService->getAll(array('contractor_id' => $contractor_id));

            $id = $contractorServicesFirst[0]['contractor_service_id'];
        } else {
            $id = $this->contractor_service_id;
        }

        if (!CheckAuth::checkIfCanHandelAllCompany('contractor_service', $id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // init action form
        //
        if ($contractor_id) {
            $filters['contractor_id'] = $contractor_id;
        } else {
            $filters['contractor_service_id'] = $this->contractor_service_id;
        }
        $contractorServiceAvailabilityObj = new Model_ContractorServiceAvailability();
        $old_cities = $contractorServiceAvailabilityObj->getAll($filters);
        $form = new Settings_Form_ContractorServiceAvailability(array('contractor_service_id' => $this->contractor_service_id, 'contractor_id' => $contractor_id, 'country_id' => $countryId, 'old_cities' => $old_cities));

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $contractorServiceAvailabilityObj = new Model_ContractorServiceAvailability();

                $success = false;

                if ($contractor_id) {
                    $fitlers['contractor_id'] = $contractor_id;
                    $contractorServices = $model_contractorService->getAll($fitlers);
                    foreach ($contractorServices as $key => $service) {
                        $contractorServiceAvailabilityObj->deleteByContractorServiceId($service['contractor_service_id']);
                        foreach ($cityIds as $key => $city) {
                            $data = array(
                                'city_id' => $city,
                                'contractor_service_id' => $service['contractor_service_id']
                            );
                            if (!$contractorServiceAvailabilityObj->getByCityIdAndContractorServiceId($city, $service['contractor_service_id'])) {
                                $success = $contractorServiceAvailabilityObj->insert($data);
                            }
                        }
                    }
                } else {

//                var_dump($contractorServices[0]);
//                exit;

                    foreach ($cityIds as $cityId) {
                        $data = array(
                            'city_id' => $cityId,
                            'contractor_service_id' => $this->contractor_service_id
                        );

                        if (!$contractorServiceAvailabilityObj->getByCityIdAndContractorServiceId($cityId, $this->contractor_service_id)) {
                            $success = $contractorServiceAvailabilityObj->insert($data);
                        }
                    }
                }

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                }

                //$this->_redirect($this->router->assemble(array(), 'settingsUserEmailList'));
                echo 1;
                exit;
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('contractor-service-availability/add_edit.phtml');
        exit;
    }

    public function deleteAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsContractorServiceAvailabilityDelete'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
        if ($id) {
            $ids[] = $id;
        }

        $contractorServiceAvailabilityObj = new Model_ContractorServiceAvailability();
        foreach ($ids as $id) {
            if (CheckAuth::checkIfCanHandelAllCompany('contractor_service_availability', $id)) {
                $contractorServiceAvailabilityObj->deleteById($id);
            }
        }
        $this->_redirect($this->router->assemble(array('contractor_service_id' => $this->contractor_service_id), 'settingsContractorServiceAvailabilityList'));
    }

}
