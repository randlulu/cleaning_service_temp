<?php

class Settings_ApiParametersController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'settings';
        
        $pageName = $this->request->getParam('page_name', '');
        $this->view->page_title = (!empty($pageName))?$pageName." - API Parameters Settings":"API Parameters Settings";
    }

    /**
     * Items list action
     */
    public function indexAction() {

        // 
        // check Auth for logged user
        //
        CheckAuth::checkCredential(array('settingsCompaniesApis'));
        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'api_name');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        //
        // get data list
        //

        $group_by_api = true;
        $filters['company_id'] = CheckAuth::getCompanySession();
        $model_api_parameters = new Model_ApiParameters();
        $this->view->data = $model_api_parameters->getAll($filters, $group_by_api);

        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
    }

    /**
     * Add new item action
     */
    public function addEditAction() {
        
        $this->view->layout()->disableLayout();
        //
        // check Auth for logged user
        //
        CheckAuth::checkCredential(array('settingsCompaniesApis'));
        //
        // get request parameters
        $api_name_edit = $this->request->getParam("api_name_edit", "");
        $this->view->api_name = $api_name_edit;
        
        //
        $model_api_parameters = new Model_ApiParameters();
        
        if(!empty($api_name_edit)){
            $filters['api_name'] = $api_name_edit;
            $filters['company_id'] = CheckAuth::getCompanySession();
            $this->view->data = $model_api_parameters->getAll($filters);
        }
        

        //
        // handling the insertion process
        //

        
        if ($this->request->isPost()) { // check if POST request method
            $api_name = $this->request->getParam("api_name");
            $params_name = $this->request->getParam("name");
            $params_value = $this->request->getParam("value");
            $api_name_edit = $this->request->getParam("api_name_edit", "");

            $company_id = CheckAuth::getCompanySession();
            
            if(!empty($api_name_edit)){
                $model_api_parameters->deleteByApiName($api_name_edit);
            }
            
            foreach ($params_name as $key => $param_name){
                $source = $_FILES['file'.$key]['tmp_name'];
                if(!empty($source)){
                    $file_name = $_FILES['file'.$key]['name'];
                    $file_saved = copy($source, APPLICATION_PATH .'/'. $file_name);
                }
                if ($file_saved && file_exists($source)) {
                    unlink($source);
                }
                $data = array(
                    'api_name' => $api_name,
                    'parameter_name' => $param_name,
                    'parameter_value' => $params_value[$key],
                    'company_id' => $company_id
                );
                $success = $model_api_parameters->insert($data);
            }
            
            if ($success) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
            } else {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes"));
            }
            
            $this->_redirect($this->router->assemble(array(), 'settingsCompaniesApis'));
        }


    }
     public function deleteAction() {
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        //
        // check Auth for logged user
        //
        CheckAuth::checkCredential(array('settingsCompaniesApis'));
        //
        // get request parameters
        //
         $api_name = $this->request->getParam("api_name", "");
         
         $model_api_parameters = new Model_ApiParameters();
         $success = $model_api_parameters->deleteByApiName($api_name);

        if ($success) {
            echo "Deleted successfully";
        } else {
            echo "Error .. Could not be deleted";
            
        }

        exit;

    }

}