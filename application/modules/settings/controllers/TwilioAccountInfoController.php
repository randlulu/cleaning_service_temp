<?php

class Settings_TwilioAccountInfoController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsUser';

        BreadCrumbs::setLevel(3, 'Twilio Account  information');
    }

    /**
     * Items list action
     */
    public function indexAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsCompaniesList'));
        
        $pageName = $this->request->getParam('page_name', '');
        $this->view->page_title = (!empty($pageName))?$pageName." - Twilio Account information Settings":"Twilio Account information Settings";

		$filters=array();
		$company_id = CheckAuth::getCompanySession();
		$modelTwilioAccountInfo=new Model_TwilioAccountInfo();
	    $twilio_info=$modelTwilioAccountInfo->getBycompanyId($company_id);
		
        $this->view->twilio_info = $twilio_info;

    }
	 public function smsInboxAction() {
         
         $pageName = $this->request->getParam('page_name', '');
        $this->view->page_title = (!empty($pageName))?$pageName." - SMS Inbox":"SMS Inbox";
		 
		
		$sms_id = $this->request->getParam('sms_id',0);
		$user_id = $this->request->getParam('user_id');
		$modelSmsHistorty=new Model_SmsHistorty();								
		if($sms_id==-1){
		$reference_id=-1;
		}
		elseif($sms_id==-2){
		$reference_id=-2;
		}
		else{
        $data=array();
		$data=$modelSmsHistorty->getById($sms_id);
		$reference_id=$data['reference_id'];
		}
		$sms_inbox=$modelSmsHistorty->getSmsInbox($reference_id,$user_id);
		if($reference_id==-1 )
		{
			$model_user = new Model_User();
			$userInfo = $model_user->getById($user_id);
			$userName = $userInfo['username'];
		} 
		else 
		{
            $modelCustomer = new Model_Customer();
			$customerInfo = $modelCustomer->getById($user_id);
			$userName = $customerInfo['first_name'];
		}
			
        $this->view->sms_inbox = $sms_inbox;
        $this->view->userName = $userName;
		$this->view->reference_id = $reference_id;

		
		
		$this->view->render('twilio-account-info/sms-inbox.phtml');
	 }

   

}
