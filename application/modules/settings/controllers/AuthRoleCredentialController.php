<?php

class Settings_AuthRoleCredentialController extends Zend_Controller_Action {

    private $request;
    private $router;
    private $role_id;
    private $authRole;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
        $this->role_id = $this->request->getParam('role_id');

        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsUser';

        $modelAuthRole = new Model_AuthRole();
        $this->authRole = $modelAuthRole->getById($this->role_id);

        BreadCrumbs::setLevel(3, 'Set Credential to ' . $this->authRole['view_role_name']);
    }

    /**
     * Items list action
     */
    public function indexAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsAuthRoleCredentialList'));
        
        $pageName = $this->request->getParam('page_name', '');
        $this->view->page_title = (!empty($pageName))?$pageName." - User Role Credential Settings":"User Role Credential Settings";


        //
        // get request parameters
        //
        $credentials = $this->request->getParam('credentials', array());
        $credentials2 = $this->request->getParam('credentials2', array());

        //
        //get all credential to render in view
        //
        $filters['sortCriteria1']=true;
        $authCredential_obj = new Model_AuthCredential();
        $all_credential = $authCredential_obj->getAllCredentialTree();
        
        $this->view->all_credential = $all_credential;

        //
        //prepar old role credentials
        //
        $authRoleCredential_obj = new Model_AuthRoleCredential();
        $role_credentials = $authRoleCredential_obj->getByRoleId($this->role_id);

        $old_credentials = array();
        if ($role_credentials) {
            foreach ($role_credentials as $role_credential) {
                $old_credentials[] = $role_credential['credential_id'];
            }
        }
        
        //prepar old role credentials for inactive contractor
        //
        
        if($this->authRole['role_name'] == 'contractor'){
            $role_credentials2 = $authRoleCredential_obj->getByRoleId($this->role_id, 'FALSE');

            $old_credentials2 = array();
            if ($role_credentials2) {
                foreach ($role_credentials2 as $role_credential2) {
                    $old_credentials2[] = $role_credential2['credential_id'];
                }
            }
            $this->view->old_credentials2 = $old_credentials2;
        }
        



        if ($this->request->isPost()) {
            $authRoleCredential_obj->deleteByRoleId($this->role_id);

            $successArray = array();
            foreach ($credentials as $credential) {
                $data = array(
                    'role_id' => $this->role_id,
                    'credential_id' => $credential,
                    'is_active' => 'TRUE'
                );
                $success = $authRoleCredential_obj->insert($data);
                if ($success) {
                    $successArray [$credential][1] = $credential;
                } else {
                    $successArray [$credential][0] = $credential;
                }
            }

            foreach ($credentials as $credential) {
                if (empty($successArray[$credential][1])) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                }
            }
            
            if($this->authRole['role_name'] == 'contractor'){
                $authRoleCredential_obj->deleteByRoleId($this->role_id, 'FALSE');
                $successArray = array();
                foreach ($credentials2 as $credential) {
                    $data = array(
                        'role_id' => $this->role_id,
                        'credential_id' => $credential,
                        'is_active' => 'FALSE'
                    );
                    $success = $authRoleCredential_obj->insert($data);
                    if ($success) {
                        $successArray [$credential][1] = $credential;
                    } else {
                        $successArray [$credential][0] = $credential;
                    }
                }

                foreach ($credentials2 as $credential) {
                    if (empty($successArray[$credential][1])) {
                        $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                    }
                }
                
            }
            
            $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Credentials added"));

            $this->_redirect($this->router->assemble(array(), 'settingsAuthRoleList'));
        }

        $this->view->role_id = $this->role_id;
        $this->view->old_credentials = $old_credentials;
        
        
        
    }
    
    public function activeOrInactiveAction() {
        
        
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsAuthRoleCredentialList'));

        //
        // get request parameters
        //
        $this->is_active = $this->request->getParam('is_active','TRUE');
        
        //
        //get all credential to render in view
        //
        $filters['sortCriteria1']=true;
        $authCredential_obj = new Model_AuthCredential();
        $all_credential = $authCredential_obj->getAllCredentialTree();
        
        $this->view->all_credential = $all_credential;

        //
        //prepar old role credentials
        //
        $authRoleCredential_obj = new Model_AuthRoleCredential();
        $role_credentials = $authRoleCredential_obj->getByRoleId($this->role_id, $this->is_active);

        $old_credentials = array();
        if ($role_credentials) {
            foreach ($role_credentials as $role_credential) {
                $old_credentials[] = $role_credential['credential_id'];
            }
        }
        $this->view->role_id = $this->role_id;
        $this->view->old_credentials = $old_credentials;
        $this->view->active = $this->is_active;
        echo $this->view->render('auth-role-credential/index.phtml');
        exit;
    }

}

