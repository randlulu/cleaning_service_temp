<?php

class Settings_CannedResponsesController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsEmail';

        BreadCrumbs::setLevel(2, 'Canned Responses');
        
        $pageName = $this->request->getParam('page_name', '');
        $this->view->page_title = (!empty($pageName))?$pageName." - Canned Responses Settings":"Canned Responses Settings";
    }

    /**
     * Items list action
     */
    public function indexAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsCannedResponsesList'));

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());
        
        if($filters){
            foreach ($filters as &$filter){
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        //
        // get data list
        //
        $modelCannedResponses = new Model_CannedResponses();
        $this->view->data = $modelCannedResponses->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);

        //
        // set view params
        //
        $this->view->pageLinks = $pager->getPager();
        $this->view->filters = $filters;
    }

    /**
     * Add new item action
     */
    /**
     * Add new item action
     */
    public function addAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsCannedResponsesAdd'));

        //
        // get request parameters
        //
        $name = $this->request->getParam('name');
        $body = $this->request->getParam('body','');
        $subject = $this->request->getParam('subject');
        $template_type = $this->request->getParam('template_type','');
        $receiver_type = $this->request->getParam('receiver_type','');
        $sms_body = $this->request->getParam('sms_body','');
        $router = Zend_Controller_Front::getInstance()->getRouter();
		//
        // init action form
        //
        $form = new Settings_Form_CannedResponses();
        //
        // handling the insertion process
        //
        if ($this->request->isPost()) {
              	if($template_type=='sms'){
				$form->body->setRequired(false);
                $body=html_entity_decode(strip_tags($sms_body));
				
				}
				else 
				{$receiver_type="";
				$template_type="email";
				}
				
		// check if POST request method
            if ($form->isValid($this->request->getPost())) {
				// validate form data
			    $modelCannedResponses = new Model_CannedResponses();
                $data = array(
                    'name'          => trim($name),
                    'body'          => $body,
                    'subject'       => $subject,
                    'type'          => $template_type,
                    'receriver_type' => $receiver_type,
                    'company_id' => CheckAuth::getCompanySession()
                );
                $success = $modelCannedResponses->insert($data);
               if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in The Response"));
                }

                $this->_redirect($this->router->assemble(array(), 'settingsCannedResponsesList'));
            }
        }

        $this->view->form = $form;
    }

    public function editAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsCannedResponsesEdit'));

        //
        // get request parameters
        //
        $name = $this->request->getParam('name');
        $body = $this->request->getParam('body');
        $subject = $this->request->getParam('subject');
        $id = $this->request->getParam('id');
		$template_type = $this->request->getParam('template_type','');
        $receiver_type = $this->request->getParam('receiver_type','');
		
		
		
        //
        // validation
        //
        $modelCannedResponses = new Model_CannedResponses();
        $cannedResponses = $modelCannedResponses->getById($id);
        if (!$cannedResponses) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect($this->router->assemble(array(), 'settingsCannedResponsesList'));
            return;
        }


        //
        // init action form
        //
        $form = new Settings_Form_CannedResponses(array('mode' => 'update', 'cannedResponses' => $cannedResponses));


        //
        // handling the updating process
        //
        if ($this->request->isPost()) {
				$form->body->setRequired(false);
			// check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
              	if($template_type=='sms')
				$body=html_entity_decode(strip_tags($body));
				if($template_type=='email'||$template_type=='' ){
					$receiver_type='';
					$template_type='email';
				}

    				$data = array(
                    'name'           => trim($name),
                    'body'           => $body,
                    'subject'        => $subject,
                    'type'           => $template_type,
                    'receriver_type' => $receiver_type,
                    'company_id'     => CheckAuth::getCompanySession()
                );
				
                $success = $modelCannedResponses->updateById($id, $data);
                //$this->view->successMessage = 'Updated Successfully';
                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in The Response"));
                }


                $this->_redirect($this->router->assemble(array(), 'settingsCannedResponsesList'));
            }
        }

        $this->view->cannedResponses = $cannedResponses;

        $this->view->form = $form;
    }


    public function deleteAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsCannedResponsesDelete'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
        if ($id) {
            $ids[] = $id;
        }

        $modelCannedResponses = new Model_CannedResponses();
        foreach ($ids as $id) {
            $modelCannedResponses->deleteById($id);
        }
        $this->_redirect($this->router->assemble(array(), 'settingsCannedResponsesList'));
    }

}

