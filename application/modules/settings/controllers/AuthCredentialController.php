<?php

class Settings_AuthCredentialController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsUser';

        BreadCrumbs::setLevel(2, 'Auth Credential');
    }

    /**
     * Items list action
     */
    public function indexAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsAuthCredentialList'));

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', array('parent_id ASC', 'credential_id ASC'));
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if($filters){
            foreach ($filters as &$filter){
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }
        
        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        //
        // get data list
        //
        $modelAuthCredential = new Model_AuthCredential();
        $this->view->data = $modelAuthCredential->getAll($filters, $orderBy, $pager);

        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;

        $authCredential_obj = new Model_AuthCredential();
        $all_credential = $authCredential_obj->getAllCredentialTree();

        $this->view->all_credential = $all_credential;
    }

    /**
     * Add new item action
     */
    public function addAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsAuthCredentialAdd'));

        //
        // get request parameters
        //
        $credentialName = $this->request->getParam('credential_name');
        $isHidden = $this->request->getParam('is_hidden');
        $parent_id = $this->request->getParam('parent_id');
        $router = Zend_Controller_Front::getInstance()->getRouter();

        //
        // init action form
        //
        $authCredential = array();
        if ($parent_id) {
            $authCredential['parent_id'] = $parent_id;
        }
        $form = new Settings_Form_AuthCredential(array('authCredential' => $authCredential));

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $modelAuthCredential = new Model_AuthCredential();
                $data = array(
                    'credential_name' => $credentialName,
                    'is_hidden' => $isHidden,
                    'parent_id' => $parent_id
                );

                $success = $modelAuthCredential->insert($data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Attribute saved"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                }
                echo 1;
                exit;
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('auth-credential/add_edit.phtml');
        exit;
    }

    public function deleteAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsAuthCredentialDelete'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());


        if ($id) {
            $ids[] = $id;
        }

        $modelAuthCredential = new Model_AuthCredential();


        foreach ($ids as $id) {
            $modelAuthCredential->deleteById($id);
        }
        $this->_redirect($this->router->assemble(array(), 'settingsAuthCredentialList'));
    }

    public function editAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsAuthCredentialEdit'));

        //
        // get request parameters
        //
        $credentialName = $this->request->getParam('credential_name');
        $isHidden = $this->request->getParam('is_hidden');
        $id = $this->request->getParam('id');
        $parent_id = $this->request->getParam('parent_id');


        //
        // validation
        //
        $modelAuthCredential = new Model_AuthCredential();
        $authCredential = $modelAuthCredential->getById($id);
        if (!$authCredential) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect($this->router->assemble(array(), 'settingsAuthCredentialList'));
            return;
        }

        //
        // init action form
        //
        $form = new Settings_Form_AuthCredential(array('mode' => 'update', 'authCredential' => $authCredential));


        //
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'credential_name' => $credentialName,
                    'is_hidden' => $isHidden,
                    'parent_id' => $parent_id
                );

                $success = $modelAuthCredential->updateById($id, $data);

                //$this->view->successMessage = 'Updated Successfully';
                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                }
                echo 1;
                exit;
                //$this->_redirect($this->router->assemble(array(), 'settingsAuthCredentialList'));
                //return;
            }
        }

        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('auth-credential/add_edit.phtml');
        exit;
    }

}

