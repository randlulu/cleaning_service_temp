<?php

class Settings_UnsubscribeReasonController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

         BreadCrumbs::setLevel(2, 'Unsubscribe Reasons');
        
        $pageName = $this->request->getParam('page_name', '');
        $this->view->page_title = (!empty($pageName))?$pageName." - Unsubscribe Reasons Settings":"Unsubscribe Reasons Settings";
    }

    /**
     * Items list action
     */
    public function indexAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsUnsubscribeReasonList'));

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'unsubscribe_reason_id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        //
        // get data list
        //
        $modelUnsubscribeReason = new Model_UnsubscribeReason();
        $this->view->data = $modelUnsubscribeReason->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);

        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
 
    }

    /**
     * Add new item action
     */
    public function addAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsUnsubscribeReasonAdd'));

        //
        // get request parameters
        //
        $reason_text = $this->request->getParam('reason_text');


        $router = Zend_Controller_Front::getInstance()->getRouter();

        //
        // init action form
        //
        $form = new Settings_Form_UnsubscribeReason();

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $modelUnsubscribeReason = new Model_UnsubscribeReason();
                $data = array(
                    'reason_text' => $reason_text,
                    'company_id' => CheckAuth::getCompanySession()
                );

                $success = $modelUnsubscribeReason->insert($data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                }
                //$this->_redirect($this->router->assemble(array(), 'settingsBookingStatusList'));
                echo 1;
                exit;
            } else {
                $messages = $form->getElement('reason_text')->getErrorMessages();
                if ($messages) {
                    $form->getElement('reason_text')->clearErrorMessages();
                    foreach ($messages as $message) {
                        $form->getElement('reason_text')->addError($message);
                    }
                }
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('unsubscribe-reason/add_edit.phtml');
        exit;
    }

    public function deleteAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsUnsubscribeReasonsDelete'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());


        if ($id) {
            $ids[] = $id;
        }

        $modelUnsubscribeReason = new Model_UnsubscribeReason();

        foreach ($ids as $id) {
    
                $isNotRelated = $modelUnsubscribeReason->checkBeforeDeleteUnsubscribeReason($id);
                if ($isNotRelated) {
                    $modelUnsubscribeReason->deleteById($id);
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Error: Failed to delete, item in use"));
                }
            
        }
        $this->_redirect($this->router->assemble(array(), 'settingsUnsubscribeReasonsList'));
    }

    public function editAction() {

        //
        // check Auth for logged user
        //
		
        CheckAuth::checkPermission(array('settingsUnsubscribeReasonEdit'));

        //
        // get request parameters
        //
        $reason_text = $this->request->getParam('reason_text');
        $id = $this->request->getParam('id');
		
		

        //
        // validation
        //
       $modelUnsubscribeReason = new Model_UnsubscribeReason();
        $unsubscribeReason= $modelUnsubscribeReason->getById($id);
		
		
		
        if (!$unsubscribeReason) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect($this->router->assemble(array(), 'settingsUnsubscribeReasonsList'));
            return;
        }


        //
        // init action form
        //
        $form = new Settings_Form_UnsubscribeReason(array('mode' => 'update', 'UnsubscribeReason' => $unsubscribeReason));
		
		


        //
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                    $data = array(
                        'reason_text' => $reason_text,
                        'company_id' => CheckAuth::getCompanySession()
                    );

                    $success = $modelUnsubscribeReason->updateById($id, $data);

                    //$this->view->successMessage = 'Updated Successfully';
                    if ($success) {
                        $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved"));
                    } else {
                        $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                    }
                
                echo 1;
                exit;
                
            }
        }

        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('unsubscribe-reason/add_edit.phtml');
        exit;
    }



}

