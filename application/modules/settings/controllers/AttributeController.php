<?php

class Settings_AttributeController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsServices';

        BreadCrumbs::setLevel(2, 'Attribute');
        
        $pageName = $this->request->getParam('page_name', '');
        $this->view->page_title = (!empty($pageName))?$pageName." - Service Attribute Settings":"Service Attribute Settings";
    }

    /**
     * Items list action
     */
    public function indexAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsAttributeList'));

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'attribute_id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        //
        // get data list
        //
        $attributeObj = new Model_Attributes();
		
        $this->view->data = $attributeObj->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);
			
        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
    }

    /**
     * Add new item action
     */
    public function addAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsAttributeAdd'));

        //
        // get request parameters
        //
        $attributeName = $this->request->getParam('attribute_name');
        $attributeTypeId = $this->request->getParam('attribute_type_id');
        $extraInfo = $this->request->getParam('extra_info');
        $attributeVariableName = $this->request->getParam('attribute_variable_name');
        $isPriceAttribute = $this->request->getParam('is_price_attribute');
        $router = Zend_Controller_Front::getInstance()->getRouter();
        $companyId = $this->request->getParam('company_id');
		$default_image_id = $this->request->getParam('default_image_id', '');
		
		$attachmentObj = new Model_Attachment();
        $modelAttributeAttachment = new Model_AttributeAttachment();

        //
        // init action form
        //
        $form = new Settings_Form_Attribute();

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $attributeObj = new Model_Attributes();
                $data = array(
                    'attribute_name' => $attributeName,
                    'attribute_type_id' => $attributeTypeId,
                    'extra_info' => $extraInfo,
                    'company_id' => $companyId,
                    'attribute_variable_name' => $attributeVariableName,
                    'is_price_attribute' => $isPriceAttribute
                );

                $success = $attributeObj->insert($data);

                if ($success) {
					if (isset($default_image_id) && !empty($default_image_id)) {
                        $defaultImageArray = array(
                            'attachment_id' => $default_image_id,
                            'attribute_id' => $success,
                            'is_default' => 1
                        );

                        $modelAttributeAttachment->insert($defaultImageArray);
                    }
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Attribute saved"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes in attribute"));
                }

                //$this->_redirect($this->router->assemble(array(), 'settingsAttributeList'));
                echo 1;
                exit;
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('attribute/add_edit.phtml');
        exit;
    }

    public function deleteAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsAttributeDelete'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
        if ($id) {
            $ids[] = $id;
        }

        $attributeObj = new Model_Attributes();
        foreach ($ids as $id) {

            if (CheckAuth::checkIfCanHandelAllCompany('attribute', $id)) {
                $isNotRelated = $attributeObj->checkBeforeDeleteAttribute($id);
                if ($isNotRelated) {
					$modelAttachment = new Model_Attachment();
                    $modelAttributeAttachment = new Model_AttributeAttachment();
                    $attributeAttachments = $modelAttributeAttachment->getByAttributeId($id);
                    foreach ($attributeAttachments as $attributeAttachment) {
                        $success = $modelAttachment->updateById($attributeAttachment['attachment_id'], array('is_deleted' => '1'));
                    }
                    $attributeObj->deleteById($id);
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Failed to delete, attribute in use"));
                }
            }
        }
        $this->_redirect($this->router->assemble(array(), 'settingsAttributeList'));
    }

    public function editAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsAttributeEdit'));

        //
        // get request parameters
        //
        $attributeName = $this->request->getParam('attribute_name');
        $attributeTypeId = $this->request->getParam('attribute_type_id');
        $extraInfo = $this->request->getParam('extra_info');
        $attributeVariableName = $this->request->getParam('attribute_variable_name');
        $isPriceAttribute = $this->request->getParam('is_price_attribute');
        $companyId = $this->request->getParam('company_id');
        $id = $this->request->getParam('id');
		$default_image_id = $this->request->getParam('default_image_id', '');

		$attachmentObj = new Model_Attachment();
        $modelAttributeAttachment = new Model_AttributeAttachment();




        //
        // validation
        //
        $attributeObj = new Model_Attributes();
        $attribute = $attributeObj->getById($id);
		$defaultImageObj = $modelAttributeAttachment->getByAttributeId($id, 1);

        if (!CheckAuth::checkIfCanHandelAllCompany('attribute', $id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        if (!$attribute) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));

            $this->_redirect($this->router->assemble(array(), 'settingsAttributeList'));
            return;
        }


        //
        // init action form
        //
        $form = new Settings_Form_Attribute(array('mode' => 'update', 'attribute' => $attribute));


        //
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'attribute_name' => $attributeName,
                    'attribute_type_id' => $attributeTypeId,
                    'extra_info' => $extraInfo,
                    'company_id' => $companyId,
                    'attribute_variable_name' => $attributeVariableName,
                    'is_price_attribute' => $isPriceAttribute
                );

                $success = $attributeObj->updateById($id, $data);
				
				if ($defaultImageObj) {
					$modelAttributeAttachment->updateById($defaultImageObj['attribute_attachment_id'], array('attachment_id' => $default_image_id));
				} else {
					$modelAttributeAttachment->insert(array('attribute_id' => $id, 'attachment_id' => $default_image_id, 'is_default' => 1));
				}

                if ($success || ($defaultImageObj['attachment_id'] != $default_image_id)) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Attribute saved"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                }
                //$this->view->successMessage = 'Updated Successfully';
                echo 1;
                exit;
                //$this->_redirect($this->router->assemble(array(), 'settingsAttributeList'));
                //return;
            }
        }

        $this->view->form = $form;
		$this->view->default_image = $defaultImageObj;

        //
        // render views
        //
        echo $this->view->render('attribute/add_edit.phtml');
        exit;
    }

	public function orderAttributeAction() {
		$modelAttributes = new Model_Attributes();
		$attributes = $this->request->getParam('attributeOrder',array());
		$attributes = json_decode($attributes);
		//print_r($attributes);
		$result = array();
		
		foreach ($attributes as $key => $value){
						//echo 'key '.$key;
						//echo 'value '.$value;
						$att = array();
			foreach ($value as $k => $v){
							
							$att[$k] = $v;
						
			}
			
			$data = array('order'=>$att['order']);
			$modelAttributes->updateByAttributeName($att['attributeName'], $data);
			//$result[$key] = $att;
		}
		
		echo json_encode(array('msg' => 'done'));
        exit;
	}
	
	public function setAttributeDefaultImageAction() {
        $attachmentObj = new Model_Attachment();
        if ($this->request->isPost()) {

            $upload = new Zend_File_Transfer_Adapter_Http();
            $files = $upload->getFileInfo();
            foreach ($files as $file => $fileInfo) {

                if ($upload->isUploaded($file)) {
                    if ($upload->receive($file)) {
                        $info = $upload->getFileInfo($file);
                        $source = $info[$file]['tmp_name'];
                        $imageInfo = pathinfo($source);
                        $ext = $imageInfo['extension'];
                        $size = $info[$file]['size'];
                        $type = $info[$file]['type'];
                        $file_name = $info[$file]['name'];
                        $dir = get_config('attachment') . '/';
                        $subdir = date('Y/m/d/');
                        $fullDir = $dir . $subdir;
                        $loggedUser = CheckAuth::getLoggedUser();
                        $data = array(
                            'created_by' => $loggedUser['user_id']
                            , 'created' => time()
                            , 'size' => $size
                            , 'type' => $type
                        );


                        $Attachid = $attachmentObj->insert($data);
                        $fileName = 'default_image_' . $Attachid . '.' . $ext;
                        $thumbName = 'default_image_' . $Attachid . '_thumbnail' . '.' . $ext;
                        $largeName = 'default_image_' . $Attachid . '_large' . '.' . $ext;
                        $compressedName = 'default_image_' . $Attachid . '_compressed' . '.' . $ext;
                        if (!is_dir($fullDir)) {
                            mkdir($fullDir, 0777, true);
                        }


                        $image_saved = copy($source, $fullDir . $fileName);
                        ImageMagick::create_thumbnail($source, $fullDir . $thumbName, 78, 64);
                        ImageMagick::scale_image($source, $fullDir . $largeName, 510);
                        ImageMagick::convert($source, $fullDir . $compressedName);
                        ImageMagick::compress_image($source, $fullDir . $compressedName);
                        if ($image_saved) {

                            $Updatedata = array(
                                'path' => $fullDir . $fileName,
                                'file_name' => $fileName,
                                'thumbnail_file' => $fullDir . $thumbName,
                                'large_file' => $fullDir . $largeName,
                                'compressed_file' => $fullDir . $compressedName
                            );

                            $updateAttachment = $attachmentObj->updateById($Attachid, $Updatedata);

                            if (file_exists($source)) {
                                unlink($source);
                            }

                            $image = '<img width="180" height="150" class="class="img-responsive alt="Image" style="border-radius: 10%;" src="' . $this->getRequest()->getBaseUrl() . '/uploads/attachment_files/' . $subdir . $thumbName . '" />
							<div id="loader_view" style="display: none;">
                            <img class="img-responsive" src="' . $this->getRequest()->getBaseUrl() . '/pic/loading-4.gif' . '" />
                            </div>';

                            $path_data = array(
                                'image' => $image,
                                'attachment_id' => $Attachid
                            );

                            echo json_encode($path_data);
                        }
                    }
                }
            }
        }

        exit;
    }
	
	
}

