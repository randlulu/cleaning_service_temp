<?php

class Complaint_DiscussionController extends Zend_Controller_Action {

    private $request;
    private $router;
    private $loggedUser;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        //
        //checkLoggedIn
        //
        CheckAuth::checkLoggedIn();

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('complaintDiscussion'));

        //
        //get Logged User
        //
        $this->loggedUser = CheckAuth::getLoggedUser();
    }

    public function indexAction() {

        //
        //get Params
        //
        $complaintId = $this->request->getParam('id', 0);
        $process = $this->request->getParam('process', 'send');

        //
        // load model
        //
        $modelComplaint = new Model_Complaint();
        $modelComplaintDiscussion = new Model_ComplaintDiscussion();
        $modelUser = new Model_User();

        $complaint = $modelComplaint->getById($complaintId);
        $this->view->complaint = $complaint;

        if (!$complaint) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Complaint not exist"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $complaintDiscussions = $modelComplaintDiscussion->getByComplaintId($complaintId);
        foreach ($complaintDiscussions as &$complaintDiscussion) {
            $complaintDiscussion['user'] = $modelUser->getById($complaintDiscussion['user_id']);
        }

        $this->view->complaintDiscussions = $complaintDiscussions;
        $this->view->process = $process;

        echo $this->view->render('discussion/index.phtml');
        exit;
    }

    public function submitAction() {
		
        //
        //get Params
        //
        $complaintId = $this->request->getParam('id', 0);
        $discussion = $this->request->getParam('discussion', '');
        $process = $this->request->getParam('process', 'send');
		
		if($process == 'open' || $process == 'close'){
			//
			// check Auth for logged user
			//
			CheckAuth::checkPermission(array('complaintConvertStatus'));
			
			if (!CheckAuth::checkIfCanHandelAllCompany('complaint', $complaintId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Booking not belongs to your Company"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
			}

		}
        //
        // load model
        //
        $modelComplaintDiscussion = new Model_ComplaintDiscussion();
        $modelComplaint = new Model_Complaint();

        $success = 0;
        if ($discussion) {
            $data = array(
                'complaint_id' => $complaintId,
                'user_id' => $this->loggedUser['user_id'],
                'user_message' => $discussion,
                'created' => time()
            );
            $success = $modelComplaintDiscussion->insert($data);
            $modelComplaint->sendComplaintAsEmailToContractor($complaintId);
			
			////change status
			$updated = 0;
			if ($process == 'open') {
				$data = array(
					'complaint_status' => 'open'
				);
				$updated = $modelComplaint->updateById($complaintId, $data);
				
			} elseif($process == 'close') {
				$data = array(
					'complaint_status' => 'closed'
				);
				$updated = $modelComplaint->updateById($complaintId, $data);
				
			}
        }

        if ($success) {
            echo json_encode(array('success' => 1,'updated' => $updated));
        } else {
            echo json_encode(array('success' => 0, 'errMsg' => 'Invalid Discussion'));
        }
        exit;
    }
    public function getAllDiscussionAction() {

        //
        //get Params
        //
        $complaintId = $this->request->getParam('id', 0);

        //
        // load model
        //
        $modelComplaintDiscussion = new Model_ComplaintDiscussion();
        $modelUser = new Model_User();


        $complaintDiscussions = $modelComplaintDiscussion->getByComplaintId($complaintId);
        foreach ($complaintDiscussions as &$complaintDiscussion) {
            $complaintDiscussion['user'] = $modelUser->getById($complaintDiscussion['user_id']);
            $complaintDiscussion['discussion_date'] = getDateFormating($complaintDiscussion['created']);
            $complaintDiscussion['user_message'] = nl2br(htmlentities($complaintDiscussion['user_message'], ENT_NOQUOTES, 'UTF-8'));
        }

        $json_array = array(
            'discussions' => $complaintDiscussions,
            'count' => count($complaintDiscussions)
        );

        echo json_encode($json_array);
        exit;
    }

}

