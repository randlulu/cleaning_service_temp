<?php

class Complaint_IndexController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'complaints';
    }

    /**
     * List All complaint  action
     *
     */
    public function indexAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('complaint'));

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'created');
        $sortingMethod = $this->request->getParam('method', 'desc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //get all booking Status for drop dawn menu
        $modelComplaintType = new Model_ComplaintType();
        $allComplaintType = $modelComplaintType->getAllComplaintTypeAsArray();

        $select = new Zend_Form_Element_Select('fltr');
        $select->setBelongsTo('complaintType');
        $select->setDecorators(array('ViewHelper'));
        $select->setValue(isset($filters['complaintType']) ? $filters['complaintType'] : '');
        $select->setAttrib('onchange', "fltr('" . $this->router->assemble(array(), 'complaint') . "','complaintType-fltr')");
        $select->setAttrib('style', "width: 150px;");
        $select->addMultiOption('', 'Complaint Type');
        $select->addMultiOptions($allComplaintType);
        $this->view->allComplaintType = $select;

        //
        // init pager model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        if (!isset($filters['complaintStatus'])) {
            $filters['complaintStatus'] = 'open';
        }

        //
        // get data list
        //
        $modelComplaint = new Model_Complaint();
        $complaints = $modelComplaint->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);


        //
        //get the customer info 
        //    
        $modelBooking = new Model_Booking();
        $modelCustomer = new Model_Customer();
        $modelCities = new Model_Cities();
        $modelContractorInfo = new Model_ContractorInfo();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelBookingLog = new Model_BookingLog();
        $modelUser = new Model_User();

        //to add the info on the data array of the complaint
        foreach ($complaints AS &$complaint) {
            //getting the booking
            $booking = $modelBooking->getById($complaint['booking_id']);

            //customer details
            $complaint['customer'] = $modelCustomer->getById($booking['customer_id']);
            $city = $modelCities->getById($complaint['customer']['city_id']);
            $complaint['customer']['city'] = $city['city_name'];

            //contractor info
            $allContractorServiceBooking = $modelContractorServiceBooking->getByBookingId($booking['booking_id']);
            $allContractors = array();
            foreach ($allContractorServiceBooking as $contractorServiceBooking) {
                $contractorInfo = $modelContractorInfo->getByContractorId($contractorServiceBooking['contractor_id']);
                $allContractors[$contractorServiceBooking['contractor_id']] = $contractorInfo['business_name'];
            }

            $complaint['contractors'] = $allContractors;

            // booking users
            $bookingUsers = $modelBookingLog->getUsersByBookingId($booking['booking_id']);
            $modelUser->fills($bookingUsers, array('user'));
            $complaint['booking_users'] = $bookingUsers;
        }
        $this->view->data = $complaints;


        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
    }

	public function complaintAddFullScreenAction() {
		//
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('complaintAdd'));

        //
        // get request parameters
        //
        $complaintTypeId = $this->request->getParam('complaint_type_id');
        $comment = $this->request->getParam('comment');
		$isToFollow = $this->request->getParam('to_follow',0);
		$toFollow = $this->request->getParam('to_follow_date');
		$toFollow = $toFollow ? strtotime($toFollow) : 0;
		
        $bookingId = (int) $this->request->getParam('booking_id');

        if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Booking not belongs to your Company"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }


        // check if the booking exist 
        $modelBooking = new Model_Booking();
        $booking = $modelBooking->getById($bookingId);
        if (!$booking) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Booking not exist"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // init action form
        //
        $form = new Complaint_Form_Complaint(array('booking_id' => $bookingId));
        $this->view->form = $form;

        //
        // handling the insertion process via post
        //
        if ($this->request->isPost()) {
            if ($form->isValid($this->request->getPost())) {

                $modelComplaint = new Model_Complaint();
                $loggedUser = CheckAuth::getLoggedUser();
                $company_id = CheckAuth::getCompanySession();

                $data = array(
                    'complaint_type_id' => $complaintTypeId,
                    'comment' => $comment,
                    'booking_id' => $bookingId,
                    'user_id' => $loggedUser['user_id'],
                    'created' => time(),
                    'company_id' => $company_id,
					'to_follow' => $toFollow,					
					'is_to_follow' => $isToFollow
                );

                $newComplaintId = $modelComplaint->insert($data);
                $modelComplaint->sendComplaintAsEmailToContractor($newComplaintId);

                ///$goToUrl = $this->router->assemble(array('id' => $newComplaintId), 'complaintView');
                //$url = $this->router->assemble(array('id' => $newComplaintId), 'sendComplaintAcknowledgementAsEmail');

                $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Complaint is added successfully"));
                $this->_redirect($this->router->assemble(array('id' => $newComplaintId), 'complaintView'));
				
            } else {
               $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Fail, please try again"));
               $this->_redirect($this->router->assemble(array('booking_id' => $bookingId), 'complaintAddFullScreen'));
            }
        }
	
	}
	
    public function addAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('complaintAdd'));

        //
        // get request parameters
        //
        $complaintTypeId = $this->request->getParam('complaint_type_id');
        $comment = $this->request->getParam('comment');
		$isToFollow = $this->request->getParam('to_follow',0);
		$toFollow = $this->request->getParam('to_follow_date');
		$toFollow = $toFollow ? strtotime($toFollow) : 0;
		
        $bookingId = (int) $this->request->getParam('booking_id');

        if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Booking not belongs to your Company"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }


        // check if the booking exist 
        $modelBooking = new Model_Booking();
        $booking = $modelBooking->getById($bookingId);
        if (!$booking) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Booking not exist"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // init action form
        //
        $form = new Complaint_Form_Complaint(array('booking_id' => $bookingId));
        $this->view->form = $form;

        //
        // handling the insertion process via post
        //
        if ($this->request->isPost()) {
            if ($form->isValid($this->request->getPost())) {

                $modelComplaint = new Model_Complaint();
                $loggedUser = CheckAuth::getLoggedUser();
                $company_id = CheckAuth::getCompanySession();

                $data = array(
                    'complaint_type_id' => $complaintTypeId,
                    'comment' => $comment,
                    'booking_id' => $bookingId,
                    'user_id' => $loggedUser['user_id'],
                    'created' => time(),
                    'company_id' => $company_id,
					'to_follow' => $toFollow,					
					'is_to_follow' => $isToFollow
                );

                $newComplaintId = $modelComplaint->insert($data);
                //$modelComplaint->sendComplaintAsEmailToContractor($newComplaintId);

                $goToUrl = $this->router->assemble(array('id' => $newComplaintId), 'complaintView');
                $url = $this->router->assemble(array('id' => $newComplaintId), 'sendComplaintAcknowledgementAsEmail');

                echo json_encode(array('IsSuccess' => true, 'goToUrl' => $goToUrl, 'url' => $url));
                exit;
            } else {
                echo json_encode(array('IsSuccess' => false, 'view' => $this->view->render('index/add_edit.phtml')));
                exit;
            }
        }

        echo $this->view->render('index/add_edit.phtml');
        exit;
    }

    public function deleteAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('complaintDelete'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
        if ($id) {
            $ids[] = $id;
        }

        $modelComplaint = new Model_Complaint();
        foreach ($ids as $id) {
            if (CheckAuth::checkIfCanHandelAllCompany('complaint', $id)) {
                $modelComplaint->deleteById($id);
            }
        }
        $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
    }

    public function convertStatusAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('complaintConvertStatus'));

        //
        //Convert complaint type status
        //
        $id = $this->request->getParam('id', 0);

        if (!CheckAuth::checkIfCanHandelAllCompany('complaint', $id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Booking not belongs to your Company"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $modelComplaint = new Model_Complaint();

        $complaint = $modelComplaint->getById($id);

        if ($complaint['complaint_status'] == 'open') {
            $data = array(
                'complaint_status' => 'closed'
            );
        } else {
            $data = array(
                'complaint_status' => 'open'
            );
        }

        $modelComplaint->updateById($id, $data);


        $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
    }
	
	////////by islam
	public function convertStatusByAjaxAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('complaintConvertStatus'));

        //
        //Convert complaint type status
        //
        $id = $this->request->getParam('id', 0);

        if (!CheckAuth::checkIfCanHandelAllCompany('complaint', $id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Booking not belongs to your Company"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $modelComplaint = new Model_Complaint();

        $complaint = $modelComplaint->getById($id);
		$oldComplaintStatus=$complaint['complaint_status'];

        if ($complaint['complaint_status'] == 'open') {
            $data = array(
                'complaint_status' => 'closed'
            );
        } else {
            $data = array(
                'complaint_status' => 'open'
            );
        }

        $isConverted=$modelComplaint->updateById($id, $data);
		
		if($isConverted){
			echo json_encode(array('isConverted' => true, 'oldComplaintStatus' =>$oldComplaintStatus,'complaintId' =>$id));
               
		
		}
		else{
		  echo json_encode(array('isConverted' => false ,'oldComplaintStatus' =>$oldComplaintStatus,'complaintId' =>$id));
                
		}
		 
            exit;
        
    }

    public function editAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('complaintEdit'));

        // get request parameters
        $id = $this->request->getParam('id');
        $complaintTypeId = $this->request->getParam('complaint_type_id');
        $comment = $this->request->getParam('comment');

        if (!CheckAuth::checkIfCanHandelAllCompany('complaint', $id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Booking not belongs to your Company"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        // validation
        $modelComplaint = new Model_Complaint();
        $complaint = $modelComplaint->getById($id);
        if (!$complaint) {
            $this->_redirect($this->router->assemble(array(), 'complaint'));
            return;
        }

        // init action form
        $form = new Complaint_Form_Complaint(array('mode' => 'update', 'complaint' => $complaint));
        $this->view->form = $form;


        // handling the updating process
        if ($this->request->isPost()) {
            if ($form->isValid($this->request->getPost())) {

                $loggedUser = CheckAuth::getLoggedUser();
                $data = array(
                    'complaint_type_id' => $complaintTypeId,
                    'comment' => $comment,
                    'user_id' => $loggedUser['user_id'],
                );

                $modelComplaint->updateById($id, $data);
                $modelComplaint->sendComplaintAsEmailToContractor($id);

                $goToUrl = $this->router->assemble(array('id' => $id), 'complaintView');
                $url = $this->router->assemble(array('id' => $id), 'sendComplaintAcknowledgementAsEmail');

                echo json_encode(array('IsSuccess' => true, 'goToUrl' => $goToUrl, 'url' => $url));
                exit;
            } else {
                echo json_encode(array('IsSuccess' => false, 'view' => $this->view->render('index/add_edit.phtml')));
                exit;
            }
        }

        echo $this->view->render('index/add_edit.phtml');
        exit;
    }

    public function viewAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('complaintView'));

        // get request parameters
        $id = $this->request->getParam('id');

        $modelComplaint = new Model_Complaint();
        $complaint = $modelComplaint->getById($id);
        $this->view->complaint = $complaint;

        if (!CheckAuth::checkIfCanHandelAllCompany('complaint', $id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Booking not belongs to your Company"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        if (!$complaint) {
            $this->_redirect($this->router->assemble(array(), 'complaint'));
            return;
        }

        $modelBooking = new Model_Booking();
        $booking = $modelBooking->getById($complaint['booking_id']);
        $modelBooking->fill($booking, array('is_accepted'));
        $this->view->booking = $booking;

        $bookingStatus = new Model_BookingStatus();
        $status = $bookingStatus->getById($booking['status_id']);
        $this->view->status = $status;

        $ModelUserInfo = new Model_UserInfo();
        $createdBy = $ModelUserInfo->getByUserId($booking['created_by']);
        $this->view->createdBy = $createdBy;

        $ModelComplaintTyple = new Model_ComplaintType();
        $complaintType = $ModelComplaintTyple->getById($complaint['complaint_type_id']);
        $this->view->complaintType = $complaintType;
    }

    public function replayComplaintAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('complaintEmailReplay'));

        $complaintId = $this->request->getParam('id', 0);

        $complaintModel = new Model_Complaint;
        $complaint = $complaintModel->getById($complaintId);

        if (!CheckAuth::checkIfCanHandelAllCompany('complaint', $complaintId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Booking not belongs to your Company"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        if (!$complaint) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Complaint not exist"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }

        $bookingModel = new Model_Booking();
        $booking = $bookingModel->getById($complaint['booking_id']);

        $modelCustomer = new Model_Customer();
        $customer = $modelCustomer->getById($booking['customer_id']);

        $modelUser = new Model_User();
        $user = $modelUser->getById($booking['created_by']);

        $template_params = array(
            //booking
            '{booking_num}' => $booking['booking_num'],
            '{booking_created}' => date('d/m/Y', $booking['created']),
            '{booking_created_by}' => ucwords($user['username']),
            '{booking_start}' => date("F j, Y, g:i a", strtotime($booking['booking_start'])),
            //complaint
            '{complaint}' => $complaint['comment'],
            '{complaint_num}' => $complaint['complaint_num'],
            '{complaint_view}' => $complaintModel->getComplaintViewAsPlaceHolder($complaintId),
            //customer
            '{customer_name}' => get_customer_name($customer),
            '{customer_first_name}' => isset($customer['first_name']) && $customer['first_name'] ? ucwords($customer['first_name']) : '',
            '{customer_last_name}' => isset($customer['last_name']) && $customer['last_name'] ? ' ' . ucwords($customer['last_name']) : ''
        );

        $modelEmailTemplate = new Model_EmailTemplate();
        $emailTemplate = $modelEmailTemplate->getEmailTemplate('complaint_reply', $template_params);

        $body = $emailTemplate['body'];
        $subject = $emailTemplate['subject'];
        $to = array();
        if ($customer['email1']) {
            $to[] = $customer['email1'];
        }
        if ($customer['email2']) {
            $to[] = $customer['email2'];
        }
        if ($customer['email3']) {
            $to[] = $customer['email3'];
        }
        $to = implode(',', $to);

        if ($this->request->isPost()) {
            $to = $this->request->getParam('to');
            $cc = $this->request->getParam('cc');
            $subject = $this->request->getParam('subject');
            $body = $this->request->getParam('body');

            $params = array(
                'to' => $to,
                'cc' => $cc,
                'body' => $body,
                'subject' => $subject
            );

            $error_mesages = array();
            if (EmailNotification::validation($params, $error_mesages)) {

                // Send Email
                $success = EmailNotification::sendEmail($params, '', array(), array('reference_id' => $complaint['complaint_id'], 'type' => 'complaint'));

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Email sent successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Email not sent"));
                }

                echo 1;
                exit;
            }
            $this->view->error_mesages = $error_mesages;
        }


        $this->view->complaint = $complaint;
        $this->view->booking = $booking;
        $this->view->to = $to;
        $this->view->subject = $subject;
        $this->view->body = $body;
        $this->view->cc = isset($cc) ? $cc : '';

        echo $this->view->render('index/replay-complaint.phtml');
        exit;
    }

    public function replayComplaintToContractorAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('complaintEmailReplay'));

        $complaintId = $this->request->getParam('id', 0);

        $complaintModel = new Model_Complaint;
        $complaint = $complaintModel->getById($complaintId);

        if (!CheckAuth::checkIfCanHandelAllCompany('complaint', $complaintId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Booking not belongs to your Company"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        if (!$complaint) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Complaint not exist"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }

        $complaintModel->fill($complaint, array('booking', 'email_contractors', 'customer', 'complaint_type'));

        $modelUser = new Model_User();
        $user = $modelUser->getById($complaint['booking']['created_by']);


        $template_params = array(
            //booking
            '{booking_num}' => $complaint['booking']['booking_num'],
            '{booking_created}' => date('d/m/Y', $complaint['booking']['created']),
            '{booking_created_by}' => ucwords($user['username']),
            '{booking_start}' => date("F j, Y, g:i a", strtotime($complaint['booking']['booking_start'])),
            //complaint
            '{complaint}' => $complaint['comment'],
            '{complaint_num}' => $complaint['complaint_num'],
            '{complaint_type}' => $complaint['complaint_type']['name'],
            '{complaint_view}' => $complaintModel->getComplaintViewAsPlaceHolder($complaintId),
            '{complaint_discussion_view}' => $complaintModel->getComplaintViewDiscussionAsPlaceHolder($complaintId),
            //customer
            '{customer_name}' => get_customer_name($complaint['customer']),
            '{customer_first_name}' => isset($complaint['customer']['first_name']) && $complaint['customer']['first_name'] ? ucwords($complaint['customer']['first_name']) : '',
            '{customer_last_name}' => isset($complaint['customer']['last_name']) && $complaint['customer']['last_name'] ? ' ' . ucwords($complaint['customer']['last_name']) : ''
        );

        $modelEmailTemplate = new Model_EmailTemplate();
        $emailTemplate = $modelEmailTemplate->getEmailTemplate('send_complaint_as_email_to_contractor', $template_params);

        $body = $emailTemplate['body'];
        $subject = $emailTemplate['subject'];
        $to = array();
        foreach ($complaint['email_contractors'] as $contractorEmail) {
            $to[] = $contractorEmail;
        }

        $to = implode(',', $to);

        if ($this->request->isPost()) {
            $to = $this->request->getParam('to');
            $cc = $this->request->getParam('cc');
            $subject = $this->request->getParam('subject');
            $body = $this->request->getParam('body');

            $params = array(
                'to' => $to,
                'cc' => $cc,
                'body' => $body,
                'subject' => $subject
            );

            $error_mesages = array();
            if (EmailNotification::validation($params, $error_mesages)) {

                // Send Email
                $success = EmailNotification::sendEmail($params, '', array(), array('reference_id' => $complaint['complaint_id'], 'type' => 'complaint'));

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Email sent successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Email not sent"));
                }

                echo 1;
                exit;
            }
            $this->view->error_mesages = $error_mesages;
        }


        $this->view->complaint = $complaint;
        $this->view->booking = $complaint['booking'];
        $this->view->to = $to;
        $this->view->subject = $subject;
        $this->view->body = $body;
        $this->view->cc = isset($cc) ? $cc : '';

        echo $this->view->render('index/replay-complaint-to-contractor.phtml');
        exit;
    }

    public function createComplaintAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('booking'));

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'booking_id');
        $sortingMethod = $this->request->getParam('method', 'desc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array('convert_status' => 'booking'));

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        $isDeleted = $this->request->getParam('is_deleted', 0);
        $this->view->isDeleted = $isDeleted;

        if ($isDeleted) {
            $this->view->sub_menu = 'deleted_booking';
            //
            //get all deleted booking
            //
            $filters['is_deleted'] = $isDeleted;
        }



        //
        // Load Model
        //
        $modelCustomer = new Model_Customer();
        $modelCities = new Model_Cities();
        $modelLabel = new Model_Label();
        $modelBookingLabel = new Model_BookingLabel();

        //
        //get all booking Status for drop dawn menu
        //
        $modelBookingStatus = new Model_BookingStatus();
        $allStatus = $modelBookingStatus->getAllStatusAsArray();

        $select = new Zend_Form_Element_Select('fltr');
        $select->setBelongsTo('status');
        $select->setDecorators(array('ViewHelper'));
        $select->setValue(isset($filters['status']) ? $filters['status'] : '');
        $select->setAttrib('onchange', "fltr('" . $this->router->assemble(array(), 'booking') . "?fltr[status]=','status-fltr')");
        $select->addMultiOption('', 'Booking Status');
        $select->addMultiOptions($allStatus);
        $this->view->allStatus = $select;

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        // 
        // get data list
        //
        $bookingObj = new Model_Booking();

        $data = $bookingObj->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);

        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelContractorInfo = new Model_ContractorInfo();

        foreach ($data as &$row) {
            $allContractorServiceBooking = $modelContractorServiceBooking->getByBookingId($row['booking_id']);

            $allContractors = array();
            foreach ($allContractorServiceBooking as $contractorServiceBooking) {
                $contractorInfo = $modelContractorInfo->getByContractorId($contractorServiceBooking['contractor_id']);
                $allContractors[$contractorServiceBooking['contractor_id']] = $contractorInfo['business_name'];
            }
            $row['contractors'] = $allContractors;

            $customer = $modelCustomer->getById($row['customer_id']);
            $row['customer_name'] = get_customer_name($customer);

            $cities = $modelCities->getById($row['city_id']);
            $row['city_name'] = $cities['city_name'];


            $LabelIds = $modelBookingLabel->getByBookingId($row['booking_id']);
            $labels = array();

            foreach ($LabelIds as $LabelId) {
                $label = $modelLabel->getById($LabelId['label_id']);
                $labels[$label['id']] = $label['label_name'];
            }
            $row['labels'] = $labels;

            $row['status'] = $modelBookingStatus->getById($row['status_id']);
        }


        $this->view->data = $data;

        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
    }

    public function sendComplaintAcknowledgementAsEmailAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermissionOr(array('complaintAdd', 'complaintEdit'));

        $complaintId = $this->request->getParam('id', 0);
        $goToUrl = $this->request->getParam('goToUrl', isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '/');

        $complaintModel = new Model_Complaint;
        $complaint = $complaintModel->getById($complaintId);

        if (!CheckAuth::checkIfCanHandelAllCompany('complaint', $complaintId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Booking not belongs to your Company"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        if (!$complaint) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Complaint not exist"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }

        $bookingModel = new Model_Booking();
        $booking = $bookingModel->getById($complaint['booking_id']);

        $modelCustomer = new Model_Customer();
        $customer = $modelCustomer->getById($booking['customer_id']);

        $modelUser = new Model_User();
        $user = $modelUser->getById($booking['created_by']);

        $template_params = array(
            //booking
            '{booking_num}' => $booking['booking_num'],
            '{booking_created}' => date('d/m/Y', $booking['created']),
            '{booking_created_by}' => ucwords($user['username']),
            '{booking_start}' => date("F j, Y, g:i a", strtotime($booking['booking_start'])),
            //complaint
            '{complaint}' => $complaint['comment'],
            '{complaint_num}' => $complaint['complaint_num'],
            '{complaint_view}' => $complaintModel->getComplaintViewAsPlaceHolder($complaintId),
            //customer
            '{customer_name}' => get_customer_name($customer),
            '{customer_first_name}' => isset($customer['first_name']) && $customer['first_name'] ? ucwords($customer['first_name']) : '',
            '{customer_last_name}' => isset($customer['last_name']) && $customer['last_name'] ? ' ' . ucwords($customer['last_name']) : ''
        );

        $modelEmailTemplate = new Model_EmailTemplate();
        $emailTemplate = $modelEmailTemplate->getEmailTemplate('complaint_acknowledgement', $template_params);

        $body = $emailTemplate['body'];
        $subject = $emailTemplate['subject'];
        $to = array();
        if ($customer['email1']) {
            $to[] = $customer['email1'];
        }
        if ($customer['email2']) {
            $to[] = $customer['email2'];
        }
        if ($customer['email3']) {
            $to[] = $customer['email3'];
        }
        $to = implode(',', $to);

        if ($this->request->isPost()) {
            $to = $this->request->getParam('to');
            $cc = $this->request->getParam('cc');
            $subject = $this->request->getParam('subject');
            $body = $this->request->getParam('body');

            $params = array(
                'to' => $to,
                'cc' => $cc,
                'body' => $body,
                'subject' => $subject
            );

            $error_mesages = array();
            if (EmailNotification::validation($params, $error_mesages)) {

                // Send Email
                $success = EmailNotification::sendEmail($params, '', array(), array('reference_id' => $complaint['complaint_id'], 'type' => 'complaint'));


                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Email sent successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Email not sent"));
                }
                echo 1;
                exit;
            }
            $this->view->error_mesages = $error_mesages;
        }

        $this->view->goToUrl = $goToUrl;
        $this->view->complaint = $complaint;
        $this->view->booking = $booking;
        $this->view->to = $to;
        $this->view->subject = $subject;
        $this->view->body = $body;
        $this->view->cc = isset($cc) ? $cc : '';

        echo $this->view->render('index/send-complaint-acknowledgement-as-email.phtml');
        exit;
    }

}

