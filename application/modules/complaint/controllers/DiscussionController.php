<?php

class Complaint_DiscussionController extends Zend_Controller_Action {

    private $request;
    private $router;
    private $loggedUser;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        //
        //checkLoggedIn
        //
        CheckAuth::checkLoggedIn();

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('complaintDiscussion'));

        //
        //get Logged User
        //
        $this->loggedUser = CheckAuth::getLoggedUser();
    }

    public function indexAction() {

        //
        //get Params
        //
        $complaintId = $this->request->getParam('id', 0);
        $process = $this->request->getParam('process', 'send');

        //
        // load model
        //
        $modelComplaint = new Model_Complaint();
        $modelComplaintDiscussion = new Model_ComplaintDiscussion();
        $modelUser = new Model_User();

        $complaint = $modelComplaint->getById($complaintId);
        $this->view->complaint = $complaint;

        if (!$complaint) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Complaint not found"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $complaintDiscussions = $modelComplaintDiscussion->getByComplaintId($complaintId);
        foreach ($complaintDiscussions as &$complaintDiscussion) {
            $complaintDiscussion['user'] = $modelUser->getById($complaintDiscussion['user_id']);
        }

        $this->view->complaintDiscussions = $complaintDiscussions;
        $this->view->process = $process;

        echo $this->view->render('discussion/index.phtml');
        exit;
    }

    public function submitAction() {
        //
        //get Params
        //
        $complaintId = $this->request->getParam('id', 0);
        $discussion = $this->request->getParam('discussion', '');
        $imageId = $this->request->getParam('imageId', 0);
        $process = $this->request->getParam('process', 'send');

        if ($process == 'open' || $process == 'close') {
            //
            // check Auth for logged user
            //
			CheckAuth::checkPermission(array('complaintConvertStatus'));

            if (!CheckAuth::checkIfCanHandelAllCompany('complaint', $complaintId)) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this booking"));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            }
        }
        //
        // load model
        //
        $modelComplaintDiscussion = new Model_ComplaintDiscussion();
        $modelComplaint = new Model_Complaint();
        $modelComplaintTemp = new Model_ComplaintTemp();
        $modelBookingInvoice = new Model_BookingInvoice();
		
		$complaint = $modelComplaint->getById($complaintId);

        $success = 0;
        if ($discussion) {
            $data = array(
                'complaint_id' => $complaintId,
                'user_id' => $this->loggedUser['user_id'],
                'user_message' => $discussion,
                'created' => time()
            );
            $success = $modelComplaintDiscussion->insert($data);

            $modelItemImageDiscussion = new Model_ItemImageDiscussion();

            $dataDiscssion = array(
                'image_id' => $imageId,
                'group_id' => 0,
                'item_id' => $success,
                'type' => 'complaint'
            );
            $modelItemImageDiscussion->insert($dataDiscssion);

            $modelComplaint->sendComplaintAsEmailToContractor($complaintId);

            ////change status
            $updated = 0;
            if ($process == 'open') {
                $data = array(
                    'complaint_status' => 'open'
                );
            } elseif ($process == 'close') {
                $data = array(
                    'complaint_status' => 'closed'
                );
            }
            if ($process == 'close') {

                $loggedUser = CheckAuth::getLoggedUser();
                $user_id = !empty($loggedUser) ? $loggedUser['user_id'] : 0;

                $modelAuthRole = new Model_AuthRole();
                $contractor_role_id = $modelAuthRole->getRoleIdByName('contractor');
                if ($contractor_role_id == $loggedUser['role_id']) {
                    $db_params = array();
                    $db_params['is_approved'] = 0;
                    $modelComplaint->updateById($complaintId, $db_params);
                    $complaintTemp = $modelComplaintTemp->getByComplaintId($complaintId);
                    $data['user_id'] = $this->loggedUser['user_id'];
                    if ($complaintTemp) {
                        $updated = $modelComplaintTemp->updateById($complaintTemp['id'], $data);
                    } else {
                        $added = $modelComplaintTemp->addComplaintTemp($complaintId, $data);
                        if ($added) {
                            $updated = 1;
                        }
                    }
                } else {
                    $updated = $modelComplaint->updateById($complaintId, $data);
					if($updated){
						 $bookingInvoice = $modelBookingInvoice->getByBookingId($complaint['booking_id']);
						 if($bookingInvoice){
						   $modelBookingInvoice->updateById($bookingInvoice['id'],array('withhold_payment'=>0));
						 }
						}
                }
            } else if ($process == 'open') {

                $complaintTemp = $modelComplaintTemp->getByComplaintId($complaintId);
                if ($complaintTemp) {
                    $modelComplaintTemp->deleteById($complaintTemp['id']);
                }
                $updated = $modelComplaint->updateById($complaintId, array('complaint_status' => 'open', 'is_approved' => 1));
				if($updated){
					$bookingInvoice = $modelBookingInvoice->getByBookingId($complaint['booking_id']);
					 if($bookingInvoice){
						$modelBookingInvoice->updateById($bookingInvoice['id'],array('withhold_payment'=>1));
					}
				}
            }
        }

        if ($success) {
            MobileNotification::notify(0, 'complaint discussion', array('complaint_id' => $complaintId, 'discussion_id' => $success));
            echo json_encode(array('success' => 1, 'updated' => $updated));
        } else {
            echo json_encode(array('success' => 0, 'errMsg' => 'Invalid discussion'));
        }
        exit;
    }

    public function getAllImageDiscussionAction() {

        //get Params
        //
       $imageId = $this->request->getParam('id', 0);

        //
        // load model
        //
        $modelComplaintDiscussion = new Model_ComplaintDiscussion();
        $modelContractorInfo = new Model_ContractorInfo();
        $modelUser = new Model_User();

        $imageDiscussions = $modelComplaintDiscussion->getDiscussionByImageId($imageId, 'Desc');

        foreach ($imageDiscussions as &$imageDiscussion) {
            $imageDiscussion['user'] = $modelUser->getById($imageDiscussion['user_id']);
            $imageDiscussion['discussion_date'] = getDateFormating($imageDiscussion['created']);
            $imageDiscussion['user_message'] = nl2br(htmlentities($imageDiscussion['user_message'], ENT_NOQUOTES, 'UTF-8'));
            if ($imageDiscussion['user']['role_name'] == 'contractor') {
                $contractorInfo = $modelContractorInfo->getByContractorId($imageDiscussion['user']['user_id']);
                $imageDiscussion['user']['username'] = ucwords($imageDiscussion['user']['username']) . ' - ' . ucwords($contractorInfo['business_name']);
            } else {
                $imageDiscussion['user']['username'] = ucwords($imageDiscussion['user']['username']);
            }
        }

        $json_array = array(
            'discussions' => $imageDiscussions,
            'count' => count($imageDiscussions)
        );



        echo json_encode($json_array);
        exit;
    }

    public function getAllDiscussionAction() {

        //
        //get Params
        //
        $complaintId = $this->request->getParam('id', 0);

        //
        // load model
        //
        $modelComplaintDiscussion = new Model_ComplaintDiscussion();
        $modelUser = new Model_User();


        $complaintDiscussions = $modelComplaintDiscussion->getByComplaintId($complaintId);
        foreach ($complaintDiscussions as &$complaintDiscussion) {
            $complaintDiscussion['user'] = $modelUser->getById($complaintDiscussion['user_id']);
            $complaintDiscussion['discussion_date'] = getDateFormating($complaintDiscussion['created']);
            $complaintDiscussion['user_message'] = nl2br(htmlentities($complaintDiscussion['user_message'], ENT_NOQUOTES, 'UTF-8'));
        }

        $json_array = array(
            'discussions' => $complaintDiscussions,
            'count' => count($complaintDiscussions)
        );

        echo json_encode($json_array);
        exit;
    }
	
	public function updateDeletedCommentAction(){
		$discussion_id = $this->request->getParam('discussion_id');
		$complaint_id = $this->request->getParam('complaint_id');
		$have_images = $this->request->getParam('have_images', 0);
		$image_id = $this->request->getParam('image_id',0);
		$group_id = $this->request->getParam('group_id',0);
		$success = 0;
		
		$modelComplaintDiscussion = new Model_ComplaintDiscussion();
		
		$data = array(
            'is_deleted' => 1             
        );
		
		$success = $modelComplaintDiscussion->updateById($discussion_id, $data);
		if($have_images){
			
			$successDelete = 0;
			$successDeletes = array();
			$modelImage = new Model_Image();
			
			if($image_id){
				
				$successDelete = $modelImage->deleteById($image_id);
			}else if($group_id){
							
				$complaint_grouped_images = $modelImage->getByGroupIdAndType($group_id, $complaint_id, 'complaint');
 
				foreach($complaint_grouped_images as $k => $img){
					$successDeletes[$k] = $modelImage->deleteById($img['image_id']);
				}
			}
			
			if($success && ($successDelete || !in_array(0,$successDeletes))){
				echo 1;				
			}
			exit;
		}
		
		if($success){
			echo 1;
		}
		exit;
		
	}

}
