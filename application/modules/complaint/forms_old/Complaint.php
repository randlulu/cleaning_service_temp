<?php

class Complaint_Form_Complaint extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('Complaint');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $complaint = (isset($options['complaint']) ? $options['complaint'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();
        $bookingId = new Zend_Form_Element_Hidden('booking_id');
        $bookingId->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($complaint['booking_id']) ? $complaint['booking_id'] : $options['booking_id']))
                ->setErrorMessages(array('Required' => 'Invalid booking id.'));

        $complaintTypeId = new Zend_Form_Element_Select('complaint_type_id');
        $complaintTypeId->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'select_field'))
                ->setValue((!empty($complaint['complaint_type_id']) ? $complaint['complaint_type_id'] : ''))
                ->setErrorMessages(array('Required' => 'Please select complaint type.'));

        $complaintTypeObj = new Model_ComplaintType();
        $allComplaintType = $complaintTypeObj->getAllComplaintTypeAsArray();
        $complaintTypeId->addMultiOption('', 'Select One');
        $complaintTypeId->addMultiOptions($allComplaintType);


        $comment = new Zend_Form_Element_Textarea('comment');
        $comment->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'textarea_field'))
                ->setValue((!empty($complaint['comment']) ? $complaint['comment'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the comment'));



		$toFollow = new Zend_Form_Element_Checkbox('to_follow');
        $toFollow->setDecorators(array('ViewHelper'))
				->setValue((!empty($complaint['is_to_follow']) ? $complaint['is_to_follow'] : ''))
                ->addDecorator('Errors', array('class' => 'errors'));
		
		$toFollowDate = new Zend_Form_Element_Text('to_follow_date');
        $toFollowDate->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
				->setValue((!empty($complaint['to_follow']) ? date('d-m-Y',$complaint['to_follow']) : ''))
                ->setAttribs(array('class' => 'text_field date_time','style'=>'margin:5px 0px;'));
                



        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'button'));


        $this->addElements(array($bookingId, $complaintTypeId, $toFollow, $toFollowDate, $comment, $button));

        $this->setMethod('post');
        if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $complaint['complaint_id']), 'complaintEdit'));
        } else {
            $this->setAction($router->assemble(array('booking_id' => $options['booking_id']), 'complaintAdd'));
        }
    }

}

