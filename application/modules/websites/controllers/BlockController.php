<?php

class Websites_BlockController extends Zend_Controller_Action {

    private $request;
    private $router;
    private $LoggedUser;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        CheckAuth::checkLoggedIn();

        $this->LoggedUser = CheckAuth::getLoggedUser();
    }

    public function indexAction() {

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'order');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if($filters){
            foreach ($filters as &$filter){
                $filter = trim($filter);
            }
        }
        
        $pageId = $this->request->getParam('id', 0);

        $modelPageBlock = new Websites_Model_PageBlock();
        $pageBlock = $modelPageBlock->getAll(array('page_id' => $pageId));
        $this->view->pageId = $pageId;
        $this->view->data = $pageBlock;
    }

    public function addAction() {

        //
        //getting the view params
        //
        $pageId = $this->request->getParam('page_id');
        $blockTypeId = $this->request->getParam('block_type');
        $order = $this->request->getParam('order');
        $fontSize = $this->request->getParam('fontSize');
        $isBold = $this->request->getParam('is_bold', 0);
        $isItalic = $this->request->getParam('isItalic', 0);
        $link = $this->request->getParam('link', '');
        $align = $this->request->getParam('align', 'left');
        $margin = $this->request->getParam('margin', 0);
        $marginPlace = $this->request->getParam('marginPlace', 0);
        $padding = $this->request->getParam('padding', 0);
        $paddingPlace = $this->request->getParam('paddingPlace', 0);
        $color = $this->request->getParam('color', 0);


        $form = new Websites_Form_block();

        //
        // re-filling
        //
        if ($blockTypeId) {
            $drawTypeObjects = $this->drawTypes($blockTypeId, true);
            $form->addElements($drawTypeObjects);
            $this->view->formObjects = $drawTypeObjects;
        }


        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $modelPageBlock = new Websites_Model_PageBlock();
                $loggedUser = CheckAuth::getLoggedUser();
                $success = false;

                $data = array(
                    'page_id' => $pageId,
                    'created_by' => $loggedUser['user_id'],
                    'order' => $order,
                    'block_type_id' => $blockTypeId,
                    'date_added' => time(),
                    'date_modified' => time(),
                    'font_size' => $fontSize,
                    'is_bold' => $isBold,
                    'is_italic' => $isItalic,
                    'link' => $link,
                    'align' => $align,
                    'margin' => $marginValue,
                    'padding' => $paddingValue,
                    'color' => $color,
                );
                $PageBlockId = $modelPageBlock->insert($data);



                $modelBlockType = new Websites_Model_BlockType();
                $blockType = $modelBlockType->getById($blockTypeId);
                $data = array();
                switch ($blockType['type']) {
                    case 'title':
                        $modelBlockTypeText = new Websites_Model_BlockTypeText();

                        $title = $this->request->getParam('title', 0);
                        $data = array(
                            'page_block_id' => $PageBlockId,
                            'text' => $title
                        );

                        $success = $modelBlockTypeText->insert($data);
                        break;

                    case 'description':
                        $modelBlockTypeText = new Websites_Model_BlockTypeText();

                        $text = $this->request->getParam('text', 0);
                        $data = array(
                            'page_block_id' => $PageBlockId,
                            'text' => $text
                        );

                        $success = $modelBlockTypeText->insert($data);
                        break;

                    case 'description_image':

                        $image = $this->request->getParam('image');
                        $imageWidth = $this->request->getParam('imageWidth');
                        $imageHeight = $this->request->getParam('imageHeight');
                        $align = $this->request->getParam('imageAlign');

                        $modelImage = new Websites_Model_Image();
                        $imageData = array(
                            'path' => $image,
                            'width' => $imageWidth,
                            'height' => $imageHeight,
                            'align' => $align
                        );
                        $imageId = $modelImage->insert($imageData);
                        if ($imageId) {

                            $modelBlockTypeText = new Websites_Model_BlockTypeText();

                            $text = $this->request->getParam('text', 0);
                            $data = array(
                                'page_block_id' => $PageBlockId,
                                'text' => $text
                            );
                            $success = $modelBlockTypeText->insert($data);
                        }
                        break;

                    case 'unordered_list':
                    case 'ordered_list':
                        $modelBlockTypeList = new Websites_Model_BlockTypeList();

                        $list = $this->request->getParam('list', 0);
                        $data = array(
                            'page_block_id' => $PageBlockId,
                            'list' => $list
                        );
                        $success = $modelBlockTypeList->insert($data);
                        break;

                    case 'unordered_list_with_description':
                    case 'ordered_list_with_description':
                        $modelBlockTypeText = new Websites_Model_BlockTypeText();
                        $modelBlockTypeList = new Websites_Model_BlockTypeList();

                        $list = $this->request->getParam('list_with_description', 0);
                        $test = explode("\n", $list);
                        foreach ($test AS &$row) {
                            $row = explode(":", $row);
                        }

                        foreach ($test AS $listRow) {
                            for ($i = 1; $i < count($listRow); $i +=2) {
                                $data = array(
                                    'page_block_id' => $PageBlockId,
                                    'text' => $listRow[$i]
                                );
                                $listDescId = $modelBlockTypeText->insert($data);
                            }
                            $data = array(
                                'page_block_id' => $PageBlockId,
                                'block_type_text_id' => isset($listDescId) ? $listDescId : '0',
                                'list' => $listRow[0]
                            );
                            $success = $modelBlockTypeList->insert($data);
                        }
                        break;
                }

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "the page block is saved successfully."));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "the page block could not be saved successfully."));
                }
                echo 1;
                exit;
            }
        }

        $this->view->form = $form;
        echo $this->view->render('block/add_block.phtml');
        exit;
    }

    public function getTypeAction() {

        //
        //getting the view params
        //
        $typeId = $this->request->getParam('type_id');

        echo $this->drawTypes($typeId);
        exit;
    }

    public function drawTypes($typeId, $asObjects = false) {

        $modelBlockType = new Websites_Model_BlockType();
        $blockType = $modelBlockType->getById($typeId);

        $formObjects = array();

        switch ($blockType['type']) {
            case 'unordered_list_with_description':
            case 'ordered_list_with_description':

                $listWithDescription = new Zend_Form_Element_Textarea('list_with_description');
                $listWithDescription->setDecorators(array('ViewHelper'))
                        ->addDecorator('Errors', array('class' => 'errors'))
                        ->setRequired()
                        ->setAttribs(array('class' => 'block_type_text_area'));

                $formObjects['list_with_description'] = $listWithDescription;
                break;

            case 'unordered_list':
            case 'ordered_list':

                $list = new Zend_Form_Element_Textarea('list');
                $list->setDecorators(array('ViewHelper'))
                        ->addDecorator('Errors', array('class' => 'errors'))
                        ->setRequired()
                        ->setAttribs(array('class' => 'block_type_text_area'));

                $formObjects['list'] = $list;
                break;

            case 'description':

                $text = new Zend_Form_Element_Textarea('text');
                $text->setDecorators(array('ViewHelper'))
                        ->addDecorator('Errors', array('class' => 'errors'))
                        ->setRequired()
                        ->setAttribs(array('class' => 'block_type_text_area'));

                $formObjects['text'] = $text;
                break;

            case 'description_image':

                $text = new Zend_Form_Element_Textarea('text');
                $text->setDecorators(array('ViewHelper'))
                        ->addDecorator('Errors', array('class' => 'errors'))
                        ->setRequired()
                        ->setAttribs(array('class' => 'block_type_text_area'));

                $formObjects['text'] = $text;

                $image = new Zend_Form_Element_File('image');
                $image->setDestination(get_config('attachment'))
                        ->addDecorator('Errors', array('class' => 'errors'))
                        ->addValidator('Extension', false, 'jpg,png,gif')
                        ->setRequired();

                $formObjects['image'] = $image;

                $imageWidth = new Zend_Form_Element_Text('imageWidth');
                $imageWidth->setDecorators(array('ViewHelper'))
                        ->addDecorator('Errors', array('class' => 'errors'))
                        ->setRequired()
                        ->setAttribs(array('class' => 'block_type_text'));

                $formObjects['imageWidth'] = $imageWidth;

                $imageHeight = new Zend_Form_Element_Text('imageHeight');
                $imageHeight->setDecorators(array('ViewHelper'))
                        ->addDecorator('Errors', array('class' => 'errors'))
                        ->setRequired()
                        ->setAttribs(array('class' => 'block_type_text'));

                $formObjects['imageHeight'] = $imageHeight;

                $imageAlign = new Zend_Form_Element_Radio('imageAlign');
                $imageAlign->addMultiOption('left', 'left')
                        ->addMultiOption('center', 'center')
                        ->addMultiOption('right', 'right')
                        ->setSeparator(' ')
                        ->setRequired()
                        ->setDecorators(array('ViewHelper'))
                        ->addDecorator('Errors', array('class' => 'errors'))
                        ->setAttribs(array('class' => 'block_type_radio'));
                $formObjects['imageAlign'] = $imageAlign;


                break;

            case 'image':

                $image = new Zend_Form_Element_File('image');
                $image->setDestination(get_config('attachment'))
                        ->addDecorator('Errors', array('class' => 'errors'))
                        ->addValidator('Extension', false, 'jpg,png,gif')
                        ->setRequired();

                $formObjects['image'] = $image;
                break;

            case 'title':

                $title = new Zend_Form_Element_Text('title');
                $title->setDecorators(array('ViewHelper'))
                        ->addDecorator('Errors', array('class' => 'errors'))
                        ->setRequired()
                        ->setAttribs(array('class' => 'block_type_text'));

                $formObjects['title'] = $title;
                break;

            case 'video':

                $video = new Zend_Form_Element_File('video');
                $video->setDestination(get_config('attachment'))
                        ->addDecorator('Errors', array('class' => 'errors'))
                        ->addValidator('Extension', false, 'jpg,png,gif')
                        ->setRequired();

                $formObjects['video'] = $video;
                break;

            case 'table':

                $rows = new Zend_Form_Element_Text('rows');
                $rows->setDecorators(array('ViewHelper'))
                        ->addDecorator('Errors', array('class' => 'errors'))
                        ->setRequired()
                        ->setAttribs(array('class' => 'block_type_text'));
                $formObjects['rows'] = $rows;

                $columns = new Zend_Form_Element_Text('columns');
                $columns->setDecorators(array('ViewHelper'))
                        ->addDecorator('Errors', array('class' => 'errors'))
                        ->setRequired()
                        ->setAttribs(array('class' => 'block_type_text'));

                $formObjects['columns'] = $columns;
                break;
        };

        //
        //default form settings
        //
        $order = new Zend_Form_Element_Text('order');
        $order->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'block_type_text'));
        $formObjects['order'] = $order;

        $fontSize = new Zend_Form_Element_Text('fontSize');
        $fontSize->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'block_type_text'));
        $formObjects['fontSize'] = $fontSize;

        $isBold = new Zend_Form_Element_Checkbox('isBold');
        $fontSize->setDecorators(array('ViewHelper'))
                ->setLabel('check if you want a bold font')
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('id' => 'isBold', 'class' => 'block_type_check_box'));
        $formObjects['isBold'] = $isBold;

        $isItalic = new Zend_Form_Element_Checkbox('isItalic');
        $fontSize->setDecorators(array('ViewHelper'))
                ->setLabel('check if you want an italic font')
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('id' => 'isItalic', 'class' => 'block_type_check_box'));
        $formObjects['isItalic'] = $isItalic;

        $link = new Zend_Form_Element_Text('link');
        $link->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'block_type_text'));
        $formObjects['link'] = $link;

        $align = new Zend_Form_Element_Radio('align');
        $align->addMultiOption('left', 'left')
                ->addMultiOption('center', 'center')
                ->addMultiOption('right', 'right')
                ->setSeparator(' ')
                ->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'block_type_radio'));
        $formObjects['align'] = $align;

        $margin = new Zend_Form_Element_Text('margin');
        $margin->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'block_type_text'));
        $formObjects['margin'] = $margin;

        $marginPlace = new Zend_Form_Element_MultiCheckbox('marginPlace');
        $marginPlace->addFilter('StripTags')
                ->setLabel('choose the margin place(s)')
                ->setSeparator(' ')
                ->addMultiOption('left', 'left')
                ->addMultiOption('right', 'right')
                ->addMultiOption('top', 'top')
                ->addMultiOption('bottom', 'bottom');

        $formObjects['marginPlace'] = $marginPlace;

        $padding = new Zend_Form_Element_Text('padding');
        $padding->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'block_type_text'));
        $formObjects['padding'] = $padding;

        $paddingPlace = new Zend_Form_Element_MultiCheckbox('paddingPlace');
        $paddingPlace->addFilter('StripTags')
                ->setLabel('choose the padding place(s)')
                ->setSeparator(' ')
                ->addMultiOption('left', 'left')
                ->addMultiOption('right', 'right')
                ->addMultiOption('top', 'top')
                ->addMultiOption('bottom', 'bottom');

        $formObjects['paddingPlace'] = $paddingPlace;

        $color = new Zend_Form_Element_Text('color');
        $color->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'block_type_text'));

        $formObjects['color'] = $color;

        if ($asObjects) {
            return $formObjects;
        } else {
            $this->view->formObjects = $formObjects;
            return $this->view->render('block/block_type.phtml');
        }
    }

}