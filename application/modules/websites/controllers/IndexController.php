<?php

class Websites_IndexController extends Zend_Controller_Action {

    private $request;
    private $router;
    private $LoggedUser;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        CheckAuth::checkLoggedIn();
        
        $this->LoggedUser = CheckAuth::getLoggedUser();
    }

    public function indexAction() {

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'company_name');
        $sortingMethod = $this->request->getParam('method', 'desc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if($filters){
            foreach ($filters as &$filter){
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }
        
        $modelCompanies = new Model_Companies();
        $this->view->data = $modelCompanies->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);
    }

    public function pageAddAction() {

        //
        //getting the view params
        //
        $title = $this->request->getParam('title');
        $menuTitle = $this->request->getParam('menu_title');
        $company = $this->request->getParam('company_id');
        $description = $this->request->getParam('description');
        $order = $this->request->getParam('order');

        $form = new Websites_Form_Page();

        //
        // handling the insertion process
        //
       
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $modelPage = new Websites_Model_Page();
                $loggedUser = CheckAuth::getLoggedUser();
                $data = array(
                    'title' => $title,
                    'menu_title' => $menuTitle,
                    'order' => $order,
                    'created_by' => $loggedUser['user_id'],
                    'company_id' => $company,
                    'description' => $description,
                    'date_modified' => time(),
                    'date_added' => time()
                );

                $success = $modelPage->insert($data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "the page is saved successfully."));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "the page could not be saved successfully."));
                }
                $this->_redirect($this->router->assemble(array('id' => $company), 'companyWebsite'));
            }
        }
        $this->view->form = $form;

        $this->_helper->viewRenderer->setRender('add-edit');
    }

    public function pageEditAction() {

        //
        //getting the view params
        //
        $title = $this->request->getParam('title');
        $menuTitle = $this->request->getParam('menu_title');
        $company = $this->request->getParam('company_id');
        $description = $this->request->getParam('description');
        $order = $this->request->getParam('order');

        $pageId = $this->request->getParam('id');

        //
        //getting the page info
        //
        $modelPage = new Websites_Model_Page();
        $page = $modelPage->getById($pageId);

        $options = array(
            'page' => $page,
            'mode' => 'update'
        );
        $form = new Websites_Form_Page($options);

        //
        // handling the insertion process
        //
       
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $loggedUser = CheckAuth::getLoggedUser();
                $data = array(
                    'title' => $title,
                    'menu_title' => $menuTitle,
                    'order' => $order,
                    'created_by' => $loggedUser['user_id'],
                    'company_id' => $company,
                    'description' => $description,
                    'date_modified' => time(),
                    'date_added' => time()
                );

                $success = $modelPage->updateById($pageId, $data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "the page is saved successfully."));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "the page could not be saved successfully."));
                }

                $this->_redirect($this->router->assemble(array('id' => $company), 'companyWebsite'));
            }
        }
        $this->view->form = $form;

        $this->_helper->viewRenderer->setRender('add-edit');
    }

    public function pageDeleteAction() {

        //get request parameters
        $id = $this->request->getParam('id', 0);

        $modelPage = new Websites_Model_Page();
        $page = $modelPage->getById($id);
        $success = false;

        if ($page) {
            $success = $modelPage->updateById($id, array('is_deleted' => 1));
        }
        if ($success) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "The selected booking have been deleted."));
        } else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "The selected Booking can not be Deleted."));
        }
        $this->_redirect($this->router->assemble(array('id' => $page['company_id']), 'companyWebsite'));
    }

    public function companyWebsiteAction() {

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'order');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if($filters){
            foreach ($filters as &$filter){
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }
        
        $companyId = $this->request->getParam('id', 0);

        $modelPage = new Websites_Model_Page();
        $this->view->data = $modelPage->getAll(array('company_id' => $companyId), "{$orderBy} {$sortingMethod}");
    }

    public function pageBlockAction() {

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'order');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if($filters){
            foreach ($filters as &$filter){
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }
        
        $pageId = $this->request->getParam('id', 0);

        $modelPageBlock = new Websites_Model_PageBlock();
        $this->view->data = $modelPageBlock->getAll(array('page_id' => $pageId));
    }

    public function blockAddAction() {

        //
        //getting the view params
        //
        $title = $this->request->getParam('title');
        $menuTitle = $this->request->getParam('menu_title');
        $company = $this->request->getParam('company_id');
        $description = $this->request->getParam('description');
        $order = $this->request->getParam('order');

        $form = new Websites_Form_block();

        //
        // handling the insertion process
        //

        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $modelPage = new Websites_Model_Page();
                $loggedUser = CheckAuth::getLoggedUser();
                $data = array(
                    'title' => $title,
                    'menu_title' => $menuTitle,
                    'order' => $order,
                    'created_by' => $loggedUser['user_id'],
                    'company_id' => $company,
                    'description' => $description,
                    'date_modified' => time(),
                    'date_added' => time()
                );

                $success = $modelPage->insert($data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "the page is saved successfully."));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "the page could not be saved successfully."));
                }
                $this->_redirect($this->router->assemble(array('id' => $company), 'companyWebsite'));

                echo 1;
                exit;
            }
        }
        $this->view->form = $form;

//        $this->_helper->viewRenderer->setRender('add_block');
        echo $this->view->render('index/add_block.phtml');
        exit;
    }

    public function getTypeAction() {

        //
        //getting the view params
        //
        $type_id = $this->request->getParam('type_id');
        $modelBlockType = new Websites_Model_BlockType();
        $blockType = $modelBlockType->getById($type_id);

        echo $this->drawTypes($blockType['type']);
        exit;
    }

    public function drawTypes($type) {

        switch ($type) {
            case 'unordered_list_with_description':
            case 'ordered_list_with_description':

                $listDescription = new Zend_Form_Element_Text('list_description');
                $listDescription->setDecorators(array('ViewHelper'))
                        ->addDecorator('Errors', array('class' => 'errors'))
                        ->setRequired()
                        ->setAttribs(array('class' => 'text_area'))
                        ->setValue((!empty($block['list_description']) ? $block['list_description'] : ''));

                $list = new Zend_Form_Element_Textarea('list');
                $list->setDecorators(array('ViewHelper'))
                        ->addDecorator('Errors', array('class' => 'errors'))
                        ->setRequired()
                        ->setAttribs(array('class' => 'text_area'))
                        ->setValue((!empty($block['list']) ? $block['list'] : ''));

                $this->view->list_description = $listDescription;
                $this->view->list = $list;

                echo $this->view->render('index/listDesc_type.phtml');
                exit;

            case 'unordered_list':
            case 'ordered_list':

                $list = new Zend_Form_Element_Textarea('list');
                $list->setDecorators(array('ViewHelper'))
                        ->addDecorator('Errors', array('class' => 'errors'))
                        ->setRequired()
                        ->setAttribs(array('class' => 'text_area'))
                        ->setValue((!empty($block['list']) ? $block['list'] : ''));

                $this->view->list = $list;

                echo $this->view->render('index/list_type.phtml');
                exit;

            case 'description_image':

                $description = new Zend_Form_Element_Textarea('description');
                $description->setDecorators(array('ViewHelper'))
                        ->addDecorator('Errors', array('class' => 'errors'))
                        ->setRequired()
                        ->setAttribs(array('class' => 'text_area'))
                        ->setValue((!empty($block['list']) ? $block['list'] : ''));


                $image = new Zend_Form_Element_File('image');
                $image->setDestination(get_config('attachment'))
                        ->addDecorator('Errors', array('class' => 'errors'))
                        ->addValidator('Extension', false, 'jpg,png,gif')
                        ->setRequired();

                $this->view->description = $description;
                $this->view->image = $image;

                echo $this->view->render('index/imageDesc_type.phtml');
                exit;

            case 'image':

                $image = new Zend_Form_Element_File('image');
                $image->setDestination(get_config('attachment'))
                        ->addDecorator('Errors', array('class' => 'errors'))
                        ->addValidator('Extension', false, 'jpg,png,gif')
                        ->setRequired();

                $this->view->image = $image;

                echo $this->view->render('index/image_type.phtml');
                exit;

            case 'title':
            case 'italic_title':

                $title = new Zend_Form_Element_Text('title');
                $title->setDecorators(array('ViewHelper'))
                        ->addDecorator('Errors', array('class' => 'errors'))
                        ->setRequired()
                        ->setAttribs(array('class' => 'text_area'))
                        ->setValue((!empty($block['title']) ? $block['title'] : ''))
                        ->setErrorMessages(array('Required' => 'Please enter the block title'));

                $this->view->title = $title;

                echo $this->view->render('index/title_type.phtml');
                exit;


            case 'description':

                $description = new Zend_Form_Element_Textarea('description');
                $description->setDecorators(array('ViewHelper'))
                        ->addDecorator('Errors', array('class' => 'errors'))
                        ->setRequired()
                        ->setAttribs(array('class' => 'text_area'))
                        ->setValue((!empty($block['list']) ? $block['list'] : ''));

                $this->view->description = $description;

                echo $this->view->render('index/description_type.phtml');
                exit;

            case 'video':

                $video = new Zend_Form_Element_File('video');
                $video->setDestination(get_config('attachment'))
                        ->addDecorator('Errors', array('class' => 'errors'))
                        ->addValidator('Extension', false, 'jpg,png,gif')
                        ->setRequired();

                $this->view->video = $video;

                echo $this->view->render('index/video_type.phtml');
                exit;

            case 'table':

                $rows = new Zend_Form_Element_Text('rows');
                $rows->setDecorators(array('ViewHelper'))
                        ->addDecorator('Errors', array('class' => 'errors'))
                        ->setRequired()
                        ->setAttribs(array('class' => 'text_area', 'size' => 5))
                        ->setValue((!empty($block['title']) ? $block['title'] : ''))
                        ->setErrorMessages(array('Required' => 'Please enter the block title'));

                $columns = new Zend_Form_Element_Text('columns');
                $columns->setDecorators(array('ViewHelper'))
                        ->addDecorator('Errors', array('class' => 'errors'))
                        ->setRequired()
                        ->setAttribs(array('class' => 'text_area', 'size' => 5))
                        ->setValue((!empty($block['title']) ? $block['title'] : ''))
                        ->setErrorMessages(array('Required' => 'Please enter the block title'));
                
                $this->view->columns = $columns;
                $this->view->rows = $rows; 
                
                echo $this->view->render('index/table_type.phtml');
                exit;

        };

        $html = '<div style="color :red;">Wrong Value</div>';
        return $html;
    }

}