<?php

class Websites_Form_Page extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('Page');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $page = (isset($options['page']) ? $options['page'] : '');
        
        $router = Zend_Controller_Front::getInstance()->getRouter();
        $request = Zend_Controller_Front::getInstance()->getRequest();


        $title = new Zend_Form_Element_Text('title');
        $title->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($page['title']) ? $page['title'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the page title'));

        $menuTitle = new Zend_Form_Element_Text('menu_title');
        $menuTitle->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($page['menu_title']) ? $page['menu_title'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the page menu title'));

        $order = new Zend_Form_Element_Text('order');
        $order->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->addValidator(new Zend_Validate_Int())
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($page['order']) ? $page['order'] : ''));

        $description = new Zend_Form_Element_Textarea('description');
        $description->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'text_area'))
                ->setValue((!empty($page['description']) ? $page['description'] : ''));

        $companyId = new Zend_Form_Element_Select('company_id');
        $companyId->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'select_field'))
                ->setValue((!empty($page['company_id']) ? $page['company_id'] : ''))
                ->setErrorMessages(array('Required' => 'Please select the company'));

        $modelCompanies = new Model_Companies();
        $companyId->addMultiOption('', 'Select One');
        foreach ($modelCompanies->getCompaniesAsArray() as $c) {
            $companyId->addMultiOption($c['id'], $c['name']);
        }


        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'button'));

        $this->addElements(array($title, $title, $menuTitle, $description, $companyId, $button ,$order));

        $this->setMethod('post');
        if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $page['id']), 'pageEdit'));
        } else {
            $this->setAction($router->assemble(array(), 'pageAdd'));
        }
    }

}

