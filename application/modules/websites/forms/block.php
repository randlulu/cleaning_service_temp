<?php

class Websites_Form_Block extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('Block');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $block = (isset($options['block']) ? $options['block'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();
        $request = Zend_Controller_Front::getInstance()->getRequest();
        
        $blockType = new Zend_Form_Element_Select('block_type');
        $blockType->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_area', 'onChange' => 'getTypes();'))
                ->setValue((!empty($block['block_type']) ? $block['block_type'] : ''))
                ->setErrorMessages(array('Required' => 'Please select the block type'));

        $modelBlockType = new Websites_Model_BlockType();
        $blockType->addMultiOption('', 'Block Type');
        foreach ($modelBlockType->getAll() as $c) {
            $blockType->addMultiOption($c['id'], $c['type']);
        }


        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'button'));

        $this->addElements(array($blockType, $button));

        $this->setMethod('post');
        if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $block['id']), 'blockEdit'));
        } else {
            $this->setAction($router->assemble(array(), 'blockAdd'));
        }
    }

}

