<?php
/*	Class to send push notifications using Google Cloud Messaging for Android

	Example usage
	-----------------------
	$an = new GCMPushMessage($apiKey);
	$an->setDevices($devices);
	$response = $an->send($message);
	-----------------------
	
	$apiKey Your GCM api key
	$devices An array or string of registered device tokens
	$message The mesasge you want to push out

	@author Matt Grundy

	Adapted from the code available at:
	http://stackoverflow.com/questions/11242743/gcm-with-php-google-cloud-messaging

*/
class Model_IOSPushMessage { 
	
	var $apns = null;
	
	/*
		Constructor
		@param $apiKeyIn the server API key
	*/
	public function __construct($certificate_file_name, $pass_phrase){
		
		$apns = new Zend_Mobile_Push_Apns();
		$apns->setCertificate($certificate_file_name); 
		// if you have a passphrase on your certificate:
		$apns->setCertificatePassphrase($pass_phrase);
		 
		try {
			$apns->connect(Zend_Mobile_Push_Apns::SERVER_SANDBOX_URI);
			$this->apns = $apns;
		} catch (Zend_Mobile_Push_Exception_ServerUnavailable $e) {
			// you can either attempt to reconnect here or try again later
			exit(1);
		} catch (Zend_Mobile_Push_Exception $e) {
			echo 'APNS Connection Error:' . $e->getMessage();
			exit(1);
		}
		
	}

	/*
		Set the devices to send to
		@param $deviceIds array of device tokens to send to
	*/
	/*function setDevices($device_tokens){
	
		if(is_array($device_tokens)){
			$this->devices = $device_tokens;
		} else {
			$this->devices = array($device_tokens);
		}
	
	}*/

	/*
		Send the message to the device
		@param $message The message to send
		@param $data Array of data to accompany the message
	*/
	function send($message_txt, $device_token){
		
		
		$message = new Zend_Mobile_Push_Message_Apns();
		$message->setAlert($message_txt);
		$message->setBadge(1);
		$message->setSound('default');
		$message->setId(time());
		$message->setToken($device_token);

		try {
			$this->apns->send($message);
			return 1;
			
		} catch (Zend_Mobile_Push_Exception_InvalidToken $e) {
			// you would likely want to remove the token from being sent to again
			echo $e->getMessage();
			return 0;
		} catch (Zend_Mobile_Push_Exception $e) {
			// all other exceptions only require action to be sent
			echo $e->getMessage();
			return 0;
		}
		
		
	}
	
	function close(){
		$this->apns->close();
	}
	
	function error($msg){
		echo "Android send notification failed with error:";
		echo "\t" . $msg;
		exit(1);
	}
}

?>

