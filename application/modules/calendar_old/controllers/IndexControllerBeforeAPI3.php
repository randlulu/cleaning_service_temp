<?php

class Calendar_IndexController extends Zend_Controller_Action {

    private $router;
    private $request;

    /**
     * init function
     */
    public function init() {
        parent::init();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
        $this->request = $this->getRequest();

        $this->view->main_menu = 'calendar';
    }

    /**
     * default controller action view calendar
     */
    public function indexAction() {

        /**
         * check Auth for logged user
         */
        CheckAuth::checkPermission(array('booking'));

        /**
         * get all booking Status for drop dawn menu
         */
        $modelBookingStatus = new Model_BookingStatus();
        $allStatus = $modelBookingStatus->getAllStatusAsArray();

        /**
         * preparing url
         */
        $status = $this->request->getParam('status', 'current');
        $status_url = $status ? "status={$status}" : "";

        $contractor = $this->request->getParam('contractor');
        $contractor_url = $contractor ? "contractor={$contractor}" : "";

        $city = $this->request->getParam('city');
        $city_url = $city ? "city={$city}" : "";

        $url = $this->router->assemble(array(), 'calendar') . "?";

        /**
         * Create status Element
         */
        $select_status = new Zend_Form_Element_Select('status');
        $select_status->setDecorators(array('ViewHelper'));
        $select_status->setValue($status);
        $select_status->setAttrib('onchange', "changeUrl('" . $url . $contractor_url . $city_url . "','status')");
        $select_status->setAttrib('style', "width: 150px;");
        $select_status->addMultiOption('all', 'ALL STATUS');
        $select_status->addMultiOption('current', 'CURRENT');
        $select_status->addMultiOptions($allStatus);
        $this->view->select_status = $select_status;


        if (CheckAuth::checkCredential(array('canSeeAllContractorBooking'))) {
            /**
             * get all contractor for drop dawn menu
             */
            $modelUser = new Model_User();
            $allContractor = $modelUser->getAllContractor(true);

            /**
             * Create contractor Element
             */
            $select_contractor = new Zend_Form_Element_Select('contractor');
            $select_contractor->setDecorators(array('ViewHelper'));
            $select_contractor->setValue($contractor);
            $select_contractor->setAttrib('onchange', "changeUrl('" . $url . $status_url . $city_url . "','contractor')");
            $select_contractor->setAttrib('style', "width: 150px;");
            $select_contractor->addMultiOption('', 'ALL TECHNICIANS');
            $select_contractor->addMultiOptions($allContractor);
            $this->view->select_contractor = $select_contractor;
        }



        /**
         * Create city Element
         */
        $modelCities = new Model_Cities();
        $cities = $modelCities->getCitiesAsArray();
        $select_city = new Zend_Form_Element_Select('city');
        $select_city->setDecorators(array('ViewHelper'));
        $select_city->setValue($city);
        $select_city->setAttrib('onchange', "changeUrl('" . $url . $status_url . $contractor_url . "','city')");
        $select_city->setAttrib('style', "width: 150px;");
        $select_city->addMultiOption('', 'ALL CITIES');
        $select_city->addMultiOptions($cities);
        $this->view->select_city = $select_city;
    }

    /**
     * add new item action
     */
    public function addAction() {

        /**
         * check Auth for logged user
         */
        CheckAuth::checkPermission(array('bookingAdd'));

        /**
         * change main menu selected botton by booking
         */
        $this->view->main_menu = 'bookings';

        /**
         * get all data required in view
         */
        $this->getViewParams();
		
		/**
		get customer info if there is customer id, this case used when we add booking from customer view page
		**/
		//////by islam
		$customerId = $this->request->getParam('customer_id');
		 if ($customerId) {
			$modelCustomer = new Model_Customer();
            $customer = $modelCustomer->getById($customerId);
            $customerName = get_customer_name($customer);
			$this->view->customer_name = $customerName;
			/////put new booking address to be the customer address
			$this->view->bookingAddress=$customer;
			$event = array();
			$event['customer_id']= $customerId;
			$this->view->event=$event;
			
			
        }
		/////end

        /**
         * if in this request inquiry_id
         * the action will be change to
         * Convert Inquiry To Booking 
         */
        $inquiryId = $this->request->getParam('inquiry_id', 0);
        $modelInquiry = new Model_Inquiry();

        if ($inquiryId) {
            if (!$modelInquiry->checkIfCanSeeInquiry($inquiryId)) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this Inquiry"));
                $this->_redirect($this->router->assemble(array(), 'inquiry'));
            }
            if (!CheckAuth::checkIfCanHandelAllCompany('inquiry', $inquiryId)) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Inquiry not belongs to your Company"));
                $this->_redirect($this->router->assemble(array(), 'inquiry'));
            }

            /**
             * refill Inquiry Data
             */
            $this->refillInquiryData($inquiryId);
        }

        /**
         * if in this request toEstimate 
         * the action will be change to
         * Convert Inquiry To Estimate
         */
        $toEstimate = $this->request->getParam('toEstimate', 0);
        if ($toEstimate) {
            $this->view->toEstimate = $toEstimate;
            $this->view->main_menu = 'estimates';
        }

        /**
         * get property_type
         */
        $modelPropertyType = new Model_PropertyType();
        $this->view->propertyTypes = $modelPropertyType->getAll();


        if ($this->request->isPost()) {
            $this->save('create');
        }
    }

    /**
     * edit item action
     */
    public function editAction() {

        /**
         * check Auth for logged user
         */
        CheckAuth::checkPermission(array('bookingEdit'));

        /**
         * change main menu selected botton by booking
         */
        $this->view->main_menu = 'bookings';

        /**
         * get all data required in view
         */
        $this->getViewParams();

        /**
         * get request parameters
         */
        $bookingId = $this->request->getParam('booking_id', 0);
        $statusId = $this->request->getParam('status_id', 0);


        /**
         * refill data
         */
        $this->refillViewData($bookingId, $statusId);

        /**
         * load models
         */
        $modelBooking = new Model_Booking();

        /**
         * check if Can Edit Booking
         */
        if (!$modelBooking->checkIfCanEditBooking($bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have edit  permission in this Booking"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Booking not belongs to your Company"));
            $this->_redirect($this->router->assemble(array(), 'inquiry'));
        }

        /**
         * check if Can Edit Booking Details
         */
        $canEditDetails = $modelBooking->checkCanEditBookingDetails($bookingId);
        $this->view->canEditDetails = $canEditDetails;

        /**
         * get property_type
         */
        $modelPropertyType = new Model_PropertyType();
        $this->view->propertyTypes = $modelPropertyType->getAll();

		////get all questions
		$modelUpdateBookingQuestionAnswer = new Model_UpdateBookingQuestionAnswer();
		$booking = $modelBooking->getById($bookingId);
		$status_id = $booking['status_id'];
		$questions = $modelUpdateBookingQuestionAnswer->getByBookingIdAndStatusId($status_id,$bookingId);
		$this->view->questions = $questions ;


        if ($this->request->isPost()) {
            $this->save('update');
        }
    }
/////////////by Islam copy the current estimate to add new one
	public function copyAction() {

        /**
         * check Auth for logged user
         */
        CheckAuth::checkPermission(array('bookingEdit'));

        /**
         * change main menu selected botton by booking
         */
        $this->view->main_menu = 'bookings';

        /**
         * get all data required in view
         */
        $this->getViewParams();

        /**
         * get request parameters
         */
        $bookingId = $this->request->getParam('booking_id', 0);
        $statusId = $this->request->getParam('status_id', 0);


        /**
         * refill data
         */
        $this->refillViewData($bookingId, $statusId);

        /**
         * load models
         */
        $modelBooking = new Model_Booking();

        /**
         * check if Can Edit Booking
         */
        if (!$modelBooking->checkIfCanEditBooking($bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have edit  permission in this Booking"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Booking not belongs to your Company"));
            $this->_redirect($this->router->assemble(array(), 'inquiry'));
        }

        /**
         * check if Can Edit Booking Details
         */
        $canEditDetails = $modelBooking->checkCanEditBookingDetails($bookingId);
        $this->view->canEditDetails = $canEditDetails;

        /**
         * get property_type
         */
        $modelPropertyType = new Model_PropertyType();
        $this->view->propertyTypes = $modelPropertyType->getAll();


        if ($this->request->isPost()) {
            $this->save('create');
			$this->_redirect($this->router->assemble(array(), 'estimates'));
        }
    }
    /**
     * save item action (add/edit actions)
     *
     * @param type $mode 
     */
    public function save($mode) {

        /**
         * get request Params
         */
        $bookingId = $this->request->getParam('booking_id', 0);
        $inquiryId = $this->request->getParam('inquiry_id', 0);
        $toEstimate = $this->request->getParam('toEstimate', 0);
        $stpartdate = $this->request->getParam('stpartdate', '0000-00-00');
        $stparttime = $this->request->getParam('stparttime', '00:00:00');
        $st = $stpartdate . " " . $stparttime;
        $etpartdate = $this->request->getParam('etpartdate', '0000-00-00');
        $etparttime = $this->request->getParam('etparttime', '00:00:00');
        $et = $etpartdate . " " . $etparttime;
        $title = '';
        $isAllDayEvent = $this->request->getParam('isAllDayEvent', 0);
        $description_booking = $this->request->getParam('description', '');
        $cityId = $this->request->getParam('city_id', 0);
        $statusId = $this->request->getParam('bookingStatus', 0);
        $customer_id = $this->request->getParam('customer_id', 0);
        $totalDiscount = $this->request->getParam('total_discount', 0);
        $onsiteClientName = $this->request->getParam('onsite_client_name', '');
        $satisfaction = $this->request->getParam('satisfaction', 1);
        $jobStartTime = $this->request->getParam('job_start_time', $st);
        $jobFinishTime = $this->request->getParam('job_finish_time', $et);
        $services = $this->request->getParam('services', array());
        $product_ids = $this->request->getParam('product', array());
        $ltrs = $this->request->getParam('ltr', array());
        $propertyTypeId = $this->request->getParam('property_type', 0);
        $callOutFee = $this->request->getParam('call_out_fee', 0);
        $to_follow = $this->request->getParam('to_follow', '');
        $is_to_follow = $this->request->getParam('is_to_follow', 0);
        $multi_stpartdate = $this->request->getParam('multi_stpartdate');
        $confirmResetAttribut = $this->request->getParam('confirmResetAttribut', 0);

		////By Islam save update question answer questions
		
		$modelUpdateBookingQuestion = new Model_UpdateBookingQuestion();
		$questions = $modelUpdateBookingQuestion->getByBookingStatusId($statusId);
		
		if(!empty($questions)){
			foreach($questions as $question){
				$paramter_name = 'question_'.$question['update_booking_question_id'];
				$parm = $this->request->getParam($paramter_name, 0);
				$modelUpdateBookingQuestionAnswer = new Model_UpdateBookingQuestionAnswer();
				$data = array(
					'update_booking_question_status_id'=>$question['update_booking_question_status_id'],
					'booking_id'=> $bookingId,
					'answer_text'=>$parm
				);
				$oldAnswer = $modelUpdateBookingQuestionAnswer->getByQuestionStatusIdAndBookingId($question['update_booking_question_status_id'],$bookingId);
				if(!empty($oldAnswer)){
					$modelUpdateBookingQuestionAnswer->updateById($oldAnswer['id'],$data);
				}
				else{
					$modelUpdateBookingQuestionAnswer->insert($data);
				
				}
			}
		}
		//////////
        /**
         * get Logged User && Company
         */
        $loggedUser = CheckAuth::getLoggedUser();
        $companyId = CheckAuth::getCompanySession();

        /**
         * load models
         */
        $modelBooking = new Model_Booking();
        $modelBookingStatus = new Model_BookingStatus();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelRefund = new Model_Refund();
        $modelBookingEstimate = new Model_BookingEstimate();
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelPayment = new Model_Payment();
        $modelCompanies = new Model_Companies();
        $modelBookingStatusHistory = new Model_BookingStatusHistory();
		$modelInquiry = new Model_Inquiry();
        /**
         * Get Booking Status
         */
        $completed = $modelBookingStatus->getByStatusName('COMPLETED');
        $faild = $modelBookingStatus->getByStatusName('FAILED');
        $to_do = $modelBookingStatus->getByStatusName('TO DO');
        $quoted = $modelBookingStatus->getByStatusName('QUOTED');
        $toVisit = $modelBookingStatus->getByStatusName('TO VISIT');

        /**
         * get old Booking
         */
        $oldBooking = $modelBooking->getById($bookingId);

        /**
         * previous
         */
        $oldStatusId = isset($oldBooking['status_id']) ? $oldBooking['status_id'] : 0;
        $oldCallOutFee = isset($oldBooking['call_out_fee']) ? $oldBooking['call_out_fee'] : 0;
        $oldTotalDiscount = isset($oldBooking['total_discount']) ? $oldBooking['total_discount'] : 0;

        /**
         * validation
         */
        $errorMessages = array();
        if ($this->validater($errorMessages)) {

            /**
             * prepare data to be saved on database
             */
            $db_params = array();
            $db_params['status_id'] = $statusId;
            $db_params['onsite_client_name'] = $onsiteClientName;
            $db_params['job_start_time'] = strtotime($jobStartTime);
            $db_params['job_finish_time'] = strtotime($jobFinishTime);
            $db_params['satisfaction'] = $satisfaction;

            /**
             * contractor can't change booking Details
             * in update mode
             */
            if (($mode == 'create') || (($mode == 'update') && $modelBooking->checkCanEditBookingDetails($bookingId))) {
                $db_params['customer_id'] = $customer_id;
                $db_params['booking_start'] = php2MySqlTime(js2PhpTime($st));
                $db_params['booking_end'] = php2MySqlTime(js2PhpTime($et));
                $db_params['title'] = $title;
                $db_params['is_all_day_event'] = $isAllDayEvent ? 1 : 0;
                $db_params['description'] = $description_booking;
                $db_params['city_id'] = $cityId;
                $db_params['property_type_id'] = $propertyTypeId;
                $db_params['to_follow'] = $to_follow ? strtotime($to_follow) : 0;
                $db_params['is_to_follow'] = $is_to_follow ? 1 : 0;
            }

            /**
             * delete google Calendar event if status is check as delete google calender like on hold or canceled 
             */
            $deleteGoogleCalender = $modelBookingStatus->getDeleteGoogleCalender();

            /*if (in_array($statusId, $deleteGoogleCalender)) {
                $modelGoogleCalendarEvent = new Model_GoogleCalendarEvent();
                $modelGoogleCalendarEvent->deleteGoogleCalendarEventByBookingId($bookingId);
            }*/

            /**
             * if booking changed from TO Visit to TO DO update created
             */
            if (($mode == 'update') && $statusId == $to_do['booking_status_id'] && $oldStatusId) {
                if ($oldStatusId == $toVisit['booking_status_id']) {
                    $db_params['created'] = time();
                }
            }

            /**
             * is_multiple_days
             */
            if ($multi_stpartdate) {
                $db_params['is_multiple_days'] = 1;
            } else {
                $db_params['is_multiple_days'] = 0;
            }

            /**
             * save item in the database
             */
            if ($mode == 'create') {
                $db_params['created_by'] = $loggedUser['user_id'];
                $db_params['created'] = time();
                $db_params['company_id'] = $companyId;
                $db_params['original_inquiry_id'] = $inquiryId;
				/////by islam to calcuate number of hours between inquiry creation and convert date
				if($inquiryId != 0){
					$inquiry = $modelInquiry->getById($inquiryId);
					$inquiryCreateDate = $inquiry['created'];
					$convertDurationInSeconds = $db_params['created'] - $inquiry['created'];
					$convertDurationInHrs = $convertDurationInSeconds/3600;
					
					$db_params['convert_to_booking_duration'] = $convertDurationInHrs;
				}
				else{
					$db_params['convert_to_booking_duration'] = 0;
				}
				$returnData = $modelBooking->addDetailedCalendar($db_params);
            } else {
                $returnData = $modelBooking->updateDetailedCalendar($bookingId, $db_params);
            }

            if (isset($returnData['Data']) && $returnData['Data']) {

                $bookingId = (int) $returnData['Data'];

                /**
                 * save  booking status history
                 */
                if ($mode == 'create') {
                    $modelBookingStatusHistory = new Model_BookingStatusHistory();
                    $modelBookingStatusHistory->addStatusHistory($bookingId, $statusId);
                }

                /**
                 * insert booking services and his quote and quantity
                 */
                if (($mode == 'create') || (($mode == 'update') && $modelBooking->checkCanEditBookingDetails($bookingId))) {
                    $modelContractorServiceBooking->setServicesToBooking($bookingId, $services);
                }

                /**
                 * save multiple days
                 */
                $modelBookingMultipleDays = new Model_BookingMultipleDays();
                $modelBookingMultipleDays->saveMultipleDays($bookingId);

                /**
                 * save address
                 */
                $this->saveAddress($bookingId, $mode);

                /**
                 * set product to booking
                 */
                if ($product_ids) {
                    $modelBookingProduct = new Model_BookingProduct();
                    $modelBookingProduct->setProductsToBooking($bookingId, $product_ids, $ltrs);
                }

                /**
                 * save why booking status
                 */
                $this->saveWhyStatus($bookingId, $statusId, false);

                /**
                 * convert To Estimate if status is qouted
                 */
                if ($statusId == $quoted['booking_status_id']) {
                    $estimateId = $modelBookingEstimate->convertToEstimate($bookingId, false, false);
                }

                /**
                 * delete Estimate if status is not quoted
                 */
                if ($mode == 'update' && $statusId != $quoted['booking_status_id']) {
                    $modelBookingEstimate->changedEstimateToBooking($bookingId, $statusId, false);
                }
				
				/**
				* if this booking has an invoice the convert_status should remain invoice
				* but if not we should convert it to booking
				*/
				if ($mode == 'update' && $statusId == $in_process['booking_status_id'] && $oldStatusId ==$completed['booking_status_id']) {
					
					$bookingInvoice = $modelBookingInvoice->getByBookingId($bookingId);
					if (empty($bookingInvoice)) {
						$modelBooking->updateById($bookingId, array('convert_status'=> 'booking'));
					}
                    //$modelBookingInvoice->deleteForeverByBookingId($bookingId);
                }

                /**
                 * convert To Invoice if booking status is complete or faild
                 */
                if ($statusId == $completed['booking_status_id'] || $statusId == $faild['booking_status_id']) {
                    $invoiceId = $modelBookingInvoice->convertToInvoice($bookingId, false);
                }

                /**
                 * set Quantity and Discount attribute value to Zero when booking status is faild
                 */
                /*if ($statusId == $faild['booking_status_id']) {
                    if ($confirmResetAttribut) {
                        $modelContractorServiceBooking->changeAttributIfFaild($bookingId, $services);
                    }
                } else {
                    $callOutFee = 0;
                }*/


                /**
                 *  insert services in  the temp until approved when the booking is update by contractor
                 */
                $totalDiscountTemp = 0;
                $callOutFeeTemp = 0;
                $db_params = array();
                if ((($mode == 'update') && !$modelBooking->checkCanEditBookingDetails($bookingId))) {

                    $modelContractorServiceBookingTemp = new Model_ContractorServiceBookingTemp();
                    $modelContractorServiceBookingTemp->setServicesToBooking($bookingId, $services);

                    /**
                     * if the contractor change call out fee value ,it will saved in the temp ,
                     * the original call out fee get from it's company  and saved in the booking
                     */
                    $callOutFeeTemp = $callOutFee;
                    $companies = $modelCompanies->getById(CheckAuth::getCompanySession());
                    if ($callOutFee) {
                        if ($oldCallOutFee) {
                            $callOutFee = $oldCallOutFee;
                        } else {
                            $callOutFee = $companies['call_out_fee'];
                        }
                        if ($callOutFeeTemp != $callOutFee) {
                            $db_params['is_change'] = 1;
                        }
                    }

                    // get the total discount from original booking because the gst and the total calculate from it.
                    $totalDiscountTemp = $totalDiscount;

                    if (isset($totalDiscount) && $oldTotalDiscount != $totalDiscount) {
                        $totalDiscount = $oldTotalDiscount;
                        $db_params['is_change'] = 1;
                    }

                    if ($statusId != $oldStatusId) {
                        $db_params['is_change'] = 1;
                    }
                }


                /**
                 * save calculation qoute
                 */
                $totalRefund = $modelRefund->getTotalRefund(array('booking_id' => $bookingId, 'is_approved' => 'yes'));
                $subTotal = $modelContractorServiceBooking->getTotalBookingQoute($bookingId);
                $total = ($subTotal + $callOutFee) - $totalDiscount;
                $gstTax = $total * get_config('gst_tax');
                $totalQoute = $total + $gstTax;
                $totalQoute = $totalQoute - $totalRefund;



                $db_params['sub_total'] = round($subTotal, 2);
                $db_params['gst'] = round($gstTax, 2);
                $db_params['qoute'] = round($totalQoute, 2);
                $db_params['call_out_fee'] = round($callOutFee, 2);
                $db_params['refund'] = round($totalRefund, 2);
                $db_params['total_discount'] = round($totalDiscount, 2);
                $db_params['total_discount_temp'] = round($totalDiscountTemp, 2);
                $db_params['call_out_fee_temp'] = round($callOutFeeTemp, 2);


                /**
                 * generate Title
                 */
                $title = $this->generateTitle($services, $cityId, number_format($totalQoute, 2));
                $db_params['title'] = $title;


                /**
                 * update saved Booking
                 */
                $modelBooking->updateById($bookingId, $db_params, false);

                /**
                 * send Booking To Gmail Acc if status is completed or faild or to_do or in_process
                 */
                /*$pushGoogleCalender = $modelBookingStatus->getPushGoogleCalender();
                if (in_array($statusId, $pushGoogleCalender)) {
                    $modelGoogleCalendarEvent = new Model_GoogleCalendarEvent();
                    $modelGoogleCalendarEvent->sendCreatedBooking($bookingId);
                }*/
            }

            if ($mode == 'create' && $inquiryId) {
                $modelInquiry = new Model_Inquiry();

                $data = array(
                    'status' => 'booking',
                    'deferred_date' => 0
                );
                $modelInquiry->updateById($inquiryId, $data);

                if ($toEstimate) {
                    $data = array(
                        'status' => 'estimate',
                        'deferred_date' => 0
                    );
                    $modelInquiry->updateById($inquiryId, $data);
                }
            }

            // contractor can send booking confirm just for his booking
            if (CheckAuth::checkCredential(array('sendBookingAsEmail'))) {
                $returnData['send_booking_confirm'] = true;
            } else {
                $returnData['send_booking_confirm'] = false;
            }

            /**
             * prepare redirect url
             */
            $bookingStatus = $modelBookingStatus->getById($statusId);
            $returnData['status'] = $bookingStatus['name'];
            $returnData['url'] = false;
            if ($statusId == $completed['booking_status_id'] || $statusId == $faild['booking_status_id']) {
                $allPayment = $modelPayment->getTotalAmount(array('booking_id' => $bookingId, 'is_approved' => 'all'));
                if (CheckAuth::checkCredential(array('paymentAdd')) && ($allPayment < $totalQoute)) {
                    $returnData['url'] = $this->router->assemble(array('booking_id' => $bookingId), 'paymentAdd');
                } else {
                    $returnData['url'] = $this->router->assemble(array('id' => $invoiceId), 'invoiceView');
                }
            } else if ($statusId == $quoted['booking_status_id']) {
                $returnData['url'] = $this->router->assemble(array('id' => $estimateId), 'estimateView');
            } else if ($statusId == $to_do['booking_status_id']) {
                $returnData['url'] = $this->router->assemble(array('id' => $bookingId), 'bookingView');
            } else {
                if ($inquiryId) {
                    if ($toEstimate) {
                        $returnData['url'] = $this->router->assemble(array('id' => $estimateId), 'estimateView');
                    } else {
                        $returnData['url'] = $this->router->assemble(array('id' => $bookingId), 'bookingView');
                    }
                } else {
                    $returnData['url'] = $this->router->assemble(array('id' => $bookingId), 'bookingView');
                }
            }
        } else {
            $returnData = array();
            $returnData['IsSuccess'] = false;
            $returnData['Msg'] = $errorMessages;
        }



        header('Content-type:text/javascript;charset=UTF-8');
        echo json_encode($returnData);
        exit;
    }

    /**
     * validater
     * 
     * @param type $errorMessages
     * @return type boolean
     */
    public function validater(&$errorMessages) {

        $postData = $this->request->getPost();

        $bookingId = $this->request->getParam('booking_id');
        $statusId = $this->request->getParam('bookingStatus', 0);

        $modelBooking = new Model_Booking();
        $message = '';
        $isCanChangeBookingStatus = $modelBooking->checkIfCanChangeBookingStatus($bookingId, $statusId, $message);

        if (!$isCanChangeBookingStatus) {
            $errorMessages['bookingStatus'] = $message;
        }

        foreach ($postData as $key => $value) {

            /**
             * is empty validater
             */
            $required_filed = array('country', 'city_id', 'bookingStatus', 'sub_total', 'total_qoute', 'stpartdate', 'etpartdate', 'customer_id', 'to_follow', 'why', 'street_number', 'street_address', 'suburb', 'postcode', 'state');
            if (in_array($key, $required_filed)) {
                if (empty($value)) {
                    $errorMessages[$key] = 'Field required';
                }
            }

            /**
             * time format
             */
            $is_all_day_event = isset($postData["is_all_day_event"]) ? 1 : 0;
            if (!$is_all_day_event) {
                $time_format = array('stparttime', 'etparttime');
                if (in_array($key, $time_format)) {
                    if (empty($value)) {
                        $errorMessages[$key] = 'Field required';
                    }
                }
            }
        }



        $services = isset($postData['services']) ? $postData['services'] : array();

        if (count($services) === 0) {
            $errorMessages['services'] = 'Services cannot be empty';
        } else {
            foreach ($services as $service) {
                $contractor = isset($postData['contractor_' . $service]) ? $postData['contractor_' . $service] : '';
                if (empty($contractor)) {
                    $errorMessages['contractor_' . $service] = 'Field required';
                } else {
                    $modelContractorService = new Model_ContractorService();
                    $contractorService = $modelContractorService->getByContractorIdAndServiceId($contractor, $service);
                    if (empty($contractorService)) {
                        $errorMessages['contractor_' . $service] = "This Technician dosn't Provide this Service";
                    } else {
                        $city_id = isset($postData['city_id']) ? $postData['city_id'] : 0;

                        $modelContractorServiceAvailability = new Model_ContractorServiceAvailability();
                        $contractorServiceAvailability = $modelContractorServiceAvailability->getByCityIdAndContractorServiceId($city_id, $contractorService['contractor_service_id']);
                        if (empty($contractorServiceAvailability)) {
                            $errorMessages['contractor_' . $service] = "This Technician dosn't Provide this Service in this City";
                        }
                    }
                }
            }
        }

        $customerId = isset($postData['customer_id']) ? $postData['customer_id'] : 0;

        if ('contractor' == CheckAuth::getRoleName() && $customerId) {
            $loggedUser = CheckAuth::getLoggedUser();
            $modelCustomer = new Model_Customer();
            if (!$modelCustomer->getByCustomerIdAndCreatedBy($customerId, $loggedUser['user_id'])) {
                $errorMessages['customer_id'] = 'You dont have permission to select this customer';
            }
        }

        if (count($errorMessages) !== 0) {
            return false;
        }
        return true;
    }

    /**
     * saveAddress
     *
     * @param type $bookingId
     * @param type $mode 
     */
    public function saveAddress($bookingId, $mode = 'create') {

        $modelBookingAddress = new Model_BookingAddress();
        $modelBookingAddressTemp = new Model_BookingAddressTemp();
        $modelBooking = new Model_Booking();


        $data = $this->fillAddressParam($bookingId);

        $bookingAddress = $modelBookingAddress->getByBookingId($bookingId);

        $orginalAddress = $modelBookingAddress->getByBookingIdWithOutLatAndLon($bookingId);
        $newAddress = $this->fillAddressParam($bookingId, false);

        if (($mode == 'create') || (($mode == 'update') && $modelBooking->checkCanEditBookingDetails($bookingId))) {
            if ($bookingAddress) {
                $modelBookingAddress->updateById($bookingAddress['booking_address_id'], $data);
            } else {
                $modelBookingAddress->insert($data);
            }
        } else {
            if ($orginalAddress != $newAddress) {
                $db_params = array();
                $db_params['is_change'] = 1;
                $modelBooking->updateById($bookingId, $db_params);

                $bookingAddressTemp = $modelBookingAddressTemp->getByBookingId($bookingId);
                if ($bookingAddressTemp) {
                    $modelBookingAddressTemp->updateById($bookingAddressTemp['booking_address_id'], $data);
                } else {
                    $modelBookingAddressTemp->insert($data);
                }
            }
        }
    }

    /**
     * fill Address Param
     * 
     * @param type $bookingid
     * @return type array
     */
    public function fillAddressParam($bookingid, $with_lat_lon = true) {
        $streetAddress = $this->request->getParam('street_address', '');
        $streetNumber = $this->request->getParam('street_number', '');
        $suburb = $this->request->getParam('suburb', '');
        $state = $this->request->getParam('state', '');
        $unitLotNumber = $this->request->getParam('unit_lot_number', '');
        $postcode = $this->request->getParam('postcode', '');
        $poBox = $this->request->getParam('po_box', '');

        $address = array();
        $address['unit_lot_number'] = trim($unitLotNumber);
        $address['street_number'] = trim($streetNumber);
        $address['street_address'] = trim($streetAddress);
        $address['suburb'] = trim($suburb);
        $address['state'] = trim($state);
        $address['postcode'] = trim($postcode);
        $address['po_box'] = trim($poBox);
        $address['booking_id'] = (int) $bookingid;

        if ($with_lat_lon) {
            $modelBookingAddress = new Model_BookingAddress();
            $geocode = $modelBookingAddress->getLatAndLon($address);
            $address['lat'] = $geocode['lat'] ? $geocode['lat'] : 0;
            $address['lon'] = $geocode['lon'] ? $geocode['lon'] : 0;
        }

        return $address;
    }

    /**
     * saveWhyStatus
     * 
     * @param type $bookingId
     * @param type $statusId 
     */
    public function saveWhyStatus($bookingId, $statusId, $addLog = true) {

        $loggedUser = CheckAuth::getLoggedUser();

        $whyBookingStatus = $this->request->getParam('why', $this->request->getParam('extra_comments', ''));

        if ($whyBookingStatus) {
            $modelBookingStatusDiscussion = new Model_BookingStatusDiscussion();
            $modelBookingDiscussion = new Model_BookingDiscussion();
            $modelBooking = new Model_Booking();

            $db_params = array();
            $db_params['booking_id'] = $bookingId;
            $db_params['user_id'] = $loggedUser['user_id'];
            $db_params['user_message'] = $whyBookingStatus;
            $db_params['created'] = time();

            $lastDiscussion = $modelBookingStatusDiscussion->getLastDiscussionByBookingIdAndStatusId($bookingId, $statusId);
            if (!$lastDiscussion) {
                $discussionId = $modelBookingDiscussion->insert($db_params);
            } else {
                $discussionId = $lastDiscussion['discussion_id'];
                $modelBookingDiscussion->updateById($discussionId, $db_params);
            }

            if ($discussionId) {

                $db_params = array();
                $db_params['discussion_id'] = $discussionId;
                $db_params['booking_id'] = $bookingId;
                $db_params['status_id'] = $statusId;
                $db_params['created'] = time();

                $bookingStatusDiscussion = $modelBookingStatusDiscussion->getByBookingIdAndStatusIdAndDiscussionId($bookingId, $statusId, $discussionId);
                if ($bookingStatusDiscussion) {
                    $modelBookingStatusDiscussion->updateById($bookingStatusDiscussion['id'], $db_params);
                } else {
                    $modelBookingStatusDiscussion->insert($db_params);
                }
            }

            if ($bookingId && $whyBookingStatus) {
                $modelBooking->updateById($bookingId, array('why' => $whyBookingStatus), $addLog);
            }
        }
    }

    /**
     * generateTitle
     *
     * @param type $services
     * @param type $cityId
     * @param type $totalQoute
     * @return string 
     */
    public function generateTitle($services, $cityId, $totalQoute) {

        $modelServices = new Model_Services();
        $modelUser = new Model_User();
        $modelCities = new Model_Cities();
        $modelContractorInfo = new Model_ContractorInfo();

        /**
         * get The First service;
         */
        $serviceAndClone = explode('_', $services[0]);
        $serviceId = (int) (isset($serviceAndClone[0]) ? $serviceAndClone[0] : 0);
        $clone = (int) (isset($serviceAndClone[1]) && $serviceAndClone[1] ? $serviceAndClone[1] : 0);
        $contractorId = (int) $this->request->getParam('contractor_' . $serviceId . ($clone ? '_' . $clone : ''));

        $generat_title_service = $modelServices->getById($serviceId);
        $servicesName = $generat_title_service['service_name'];

        $generat_title_city = $modelCities->getById($cityId);
        $cityName = strtoupper($generat_title_city['city_name']);

        $contractor = $modelUser->getById($contractorId);
        $modelContractorInfo->fill($contractor, array('contractor_info_by_user_id'));
        $contractorName = ucwords($contractor['username']);

        $title = "{$cityName}" . ' ' . "$servicesName" . ' - ' . "\${$totalQoute}" . ' - ' . "$contractorName";

        return $title;
    }

    /**
     * Get View Params action
     */
    public function getViewParams() {

        /**
         * load models
         */
        $modelCountries = new Model_Countries();
        $modelBookingStatus = new Model_BookingStatus();
        $modelProduct = new Model_Product();
        $modelCities = new Model_Cities();
        $modelContractorServiceAvailability = new Model_ContractorServiceAvailability();

        /**
         * check if loged user is contractor
         */
        $filters = array();
        $loggedUser = CheckAuth::getLoggedUser();
        if (!CheckAuth::checkCredential(array('canSeeAllContractorBooking'))) {
            $filters['created_by'] = $loggedUser['user_id'];
        }

//        /**
//         * get all customers for drop dawn menu
//         */
//        $customers = $modelCustomer->getAll($filters);
//        $this->view->customers = $customers;

        /**
         * get all country for drop dawn menu
         */
        $countries = $modelCountries->getAll();
        $this->view->countries = $countries;

        /**
         * get all booking Status for drop dawn menu
         */
        $bookingStatus = $modelBookingStatus->getAll();
        $this->view->bookingStatus = $bookingStatus;

        /**
         * get all Product for drop dawn menu
         */
        $products = $modelProduct->getAll(array(), "product ASC");
        $this->view->products = $products;

        /**
         * fill default city and country by company or contractor city and country
         */
        if (CheckAuth::getCityId()) {

            /**
             * get all booking City
             */
            $bookingCity = $modelCities->getById(CheckAuth::getCityId());
            $this->view->bookingCity = $bookingCity;

            /**
             * get all states
             */
            $states = $modelCities->getStateByCountryId(CheckAuth::getCountryId());
            $this->view->states = $states;

            /**
             * get all city
             */
            $cities = $modelCities->getCitiesByCountryIdAndState(CheckAuth::getCountryId(), $bookingCity['state']);
            $this->view->cities = $cities;

            /**
             * get service availability
             */
            $serviceAvailable = $modelContractorServiceAvailability->getServiceByCityId(CheckAuth::getCityId());
            $this->view->serviceAvailable = $serviceAvailable;
        }
    }

    /**
     * refill Inquiry Data action
     * 
     * @param type $inquiryId 
     */
    public function refillInquiryData($inquiryId) {

        /**
         * load models
         */
        $modelInquiry = new Model_Inquiry();
        $modelInquiryAddress = new Model_InquiryAddress();
        $modelCities = new Model_Cities();
        $modelContractorServiceAvailability = new Model_ContractorServiceAvailability();
        $modelInquiryService = new Model_InquiryService();
        $modelCustomer = new Model_Customer();
        /**
         * get Inquiry By Id
         */
        $inquiry = $modelInquiry->getById($inquiryId);


        /**
         * get customer name by  customer id to edit
         */
        $customer = $modelCustomer->getById($inquiry['customer_id']);
        $this->view->customer_name = get_customer_name($customer);

        /**
         * inquiry deferred date
         * will be booking_start and booking_end
         */
        if (empty($inquiry['deferred_date'])) {
            $inquiry['booking_start'] = ''; //date('m/d/Y H:i', time());
            $inquiry['booking_end'] = ''; //date('m/d/Y H:i', strtotime('+3 hour'));
        } else {
            $inquiry['booking_start'] = date('m/d/Y H:i', $inquiry['deferred_date']);
            $inquiry['booking_end'] = date('m/d/Y H:i', $inquiry['deferred_date'] + (60 * 60 * 3));
        }

        /**
         * inquiry comment
         * will be booking description
         */
        $inquiry['description'] = $inquiry['comment'];

        /**
         * pass variable event to view
         */
        $this->view->event = $inquiry;

        /**
         * get customer info By Id
         * from inquiry to prepare booking Address
         */
        $inquiryAddress = $modelInquiryAddress->getByInquiryId($inquiry['inquiry_id']);

        $bookingAddress = array(
            'street_address' => $inquiryAddress['street_address'],
            'street_number' => $inquiryAddress['street_number'],
            'suburb' => $inquiryAddress['suburb'],
            'state' => $inquiryAddress['state'],
            'unit_lot_number' => $inquiryAddress['unit_lot_number'],
            'postcode' => $inquiryAddress['postcode'],
            'po_box' => $inquiryAddress['po_box']
        );
        $this->view->bookingAddress = $bookingAddress;

        /**
         * get drop down for city and country from inquiry
         */
        if ($inquiry['city_id']) {
            /**
             * get all booking City
             */
            $bookingCity = $modelCities->getById($inquiry['city_id']);
            $this->view->bookingCity = $bookingCity;

            /**
             * get all states
             */
            $states = $modelCities->getStateByCountryId($bookingCity['country_id']);
            $this->view->states = $states;

            /**
             * get all city
             */
            $cities = $modelCities->getCitiesByCountryIdAndState($bookingCity['country_id'], $bookingCity['state']);
            $this->view->cities = $cities;

            /**
             * get service availability
             */
            $serviceAvailable = $modelContractorServiceAvailability->getServiceByCityId($inquiry['city_id']);
            $this->view->serviceAvailable = $serviceAvailable;
        }


        $inquiryServices = $modelInquiryService->getByInquiryId($inquiryId);

        /**
         * $thisBookingServices : to put all service for this booking 
         */
        $thisBookingServices = array();

        /**
         * $priceArray : to put all price and service for this booking 
         */
        $priceArray = array();

        if ($inquiryServices) {
            foreach ($inquiryServices as $inquiryService) {
                $service_id = $inquiryService['service_id'];
                $clone = $inquiryService['clone'];
                $serviceAndClone = $service_id . ($clone ? '_' . $clone : '');

                $thisBookingServices[] = $serviceAndClone;
                $priceArray[$serviceAndClone] = 0;
            }
        }

        $this->view->bookingServices = $inquiryServices;
        $this->view->thisBookingServices = $thisBookingServices;
        $this->view->priceArray = $priceArray;
    }

    /**
     * refill View Data
     * 
     * @param type $bookingId 
     */
    public function refillViewData($bookingId, $statusId = 0) {

        /**
         * load models
         */
        $modelBooking = new Model_Booking();
        $modelBookingAddress = new Model_BookingAddress();
        $modelBookingProduct = new Model_BookingProduct();
        $modelCities = new Model_Cities();
        $modelContractorServiceAvailability = new Model_ContractorServiceAvailability();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelContractorServiceBookingTemp = new Model_ContractorServiceBookingTemp();
        $modelBookingStatusDiscussion = new Model_BookingStatusDiscussion();
        $modelBookingAddressTemp = new Model_BookingAddressTemp();
        $modelBookingMultipleDays = new Model_BookingMultipleDays();
        $modelCustomer = new Model_Customer();
        /**
         * get booking by id to edit
         */
        $booking = $modelBooking->getById($bookingId);

        if ($statusId) {
            $booking['status_id'] = $statusId;
        }
        $this->view->event = $booking;

        if ($booking['convert_status'] == 'estimate') {
            $this->view->fromEstimate = true;
            $this->view->main_menu = 'estimates';
        }

        /**
         * get customer name by  customer id to edit
         */
        $customer = $modelCustomer->getById($booking['customer_id']);
        $this->view->customer_name = get_customer_name($customer);


        /**
         * get why Discussion
         */
        $whyDiscussion = $modelBookingStatusDiscussion->getLastDiscussionByBookingIdAndStatusId($bookingId, $booking['status_id']);
        $this->view->whyDiscussion = $whyDiscussion;

        /**
         * get estimate_num
         */
        if ($booking['convert_status'] == 'estimate') {
            $modelBookingEstimate = new Model_BookingEstimate;
            $estimate = $modelBookingEstimate->getByBookingId($bookingId);
            $this->view->estimate = $estimate;
        }

        /**
         * MultipleDays
         */
        $multipleDays = $modelBookingMultipleDays->getByBookingId($bookingId);
        $this->view->multipleDays = $multipleDays;

        /**
         * get the booking address
         */
        $bookingAddressTemp = $modelBookingAddressTemp->getByBookingId($bookingId);

        if (!$modelBooking->checkCanEditBookingDetails($bookingId) && $bookingAddressTemp && $booking['is_change'] == 1) {
            $bookingAddress = $bookingAddressTemp;
            $isTempAddress = true;
        } else {
            $bookingAddress = $modelBookingAddress->getByBookingId($bookingId);
            $isTempAddress = false;
        }

        $this->view->bookingAddress = $bookingAddress;
        $this->view->isTempAddress = $isTempAddress;

        /**
         * get Product by id to edit
         */
        $allBookingProduct = $modelBookingProduct->getByBookingId($bookingId);
        $this->view->allBookingProduct = $allBookingProduct;

        /**
         * get drop down for city and country from booking
         */
        if ($booking['city_id']) {

            /**
             * get all bookingCity
             */
            $bookingCity = $modelCities->getById($booking['city_id']);
            $this->view->bookingCity = $bookingCity;

            /**
             * get all states
             */
            $states = $modelCities->getStateByCountryId($bookingCity['country_id']);
            $this->view->states = $states;

            /**
             * get all cities
             */
            $cities = $modelCities->getCitiesByCountryIdAndState($bookingCity['country_id'], $bookingCity['state']);
            $this->view->cities = $cities;

            /**
             * get service availability
             */
            $serviceAvailable = $modelContractorServiceAvailability->getServiceByCityId($booking['city_id']);
            $this->view->serviceAvailable = $serviceAvailable;
        }

        /**
         * get all service for this booking 
         */
        $bookingServicesTemp = $modelContractorServiceBookingTemp->getByBookingId($bookingId);

        if (!$modelBooking->checkCanEditBookingDetails($bookingId) && $bookingServicesTemp && $booking['is_change'] == 1) {
            $bookingServices = $bookingServicesTemp;
            $isTempService = true;
        } else {
            $bookingServices = $modelContractorServiceBooking->getByBookingId($bookingId);
            $isTempService = false;
        }

        /**
         * $thisBookingServices : to put all service for this booking
         */
        $thisBookingServices = array();

        /**
         * $priceArray : to put all price and service for this booking 
         */
        $priceArray = array();

        if ($bookingServices) {
            foreach ($bookingServices as $bookingService) {
                $serviceId = $bookingService['service_id'];
                $clone = $bookingService['clone'];
                $service_and_clone = $serviceId . ($clone ? '_' . $clone : '');
                $thisBookingServices[] = $service_and_clone;

                if ($isTempService) {
                    $priceArray[$service_and_clone] = $modelContractorServiceBookingTemp->getServiceBookingQoute($bookingId, $serviceId, $clone);
                } else {
                    $priceArray[$service_and_clone] = $modelContractorServiceBooking->getServiceBookingQoute($bookingId, $serviceId, $clone);
                }
            }
        }

        $totalAmountDetails = $modelBooking->getTotalAmountBookingDetails($bookingId);

        $totalDiscountTemp = false;
        if ($booking['total_discount_temp'] != $booking['total_discount'] && $booking['is_change'] == 1) {
            $totalDiscountTemp = true;
        }

        $callOutFeeTemp = false;
        if ($booking['call_out_fee_temp'] != $booking['call_out_fee'] && $booking['is_change'] == 1) {
            $callOutFeeTemp = true;
        }

        $is_temp = false;
        if ($booking['is_change'] == 1) {
            $is_temp = true;
        }


        $this->view->subTotal = $totalAmountDetails['sub_total'];
        $this->view->totalDiscount = $totalAmountDetails['total_discount'];
        $this->view->callOutFee = $totalAmountDetails['call_out_fee'];
        $this->view->gstTax = $totalAmountDetails['gst'];
        $this->view->totalQoute = $totalAmountDetails['total'];
        $this->view->bookingServices = $bookingServices;
        $this->view->thisBookingServices = $thisBookingServices;
        $this->view->priceArray = $priceArray;


        $this->view->is_temp = $is_temp;
        $this->view->isTempService = $isTempService;
        $this->view->totalDiscountTemp = $totalDiscountTemp;
        $this->view->callOutFeeTemp = $callOutFeeTemp;
    }

    /**
     * manage Action
     */
    public function manageAction() {

        /**
         * load models
         */
        $modelBooking = new Model_Booking();

        $method = $this->request->getParam('method');

        switch ($method) {
            case "add":
                $ret = array('IsSuccess' => false, 'Msg' => "You don't have Add  permission in this Booking");
                break;
            case "list":
                if (!CheckAuth::checkCredential(array('booking'))) {
                    $ret = array('IsSuccess' => false, 'Msg' => "You don't have permission to view Bookings");
                    break;
                }

                $viewtype = $this->request->getParam('viewtype');
                $showdate = $this->request->getParam('showdate');
                $status = $this->request->getParam('status');
                $contractor = $this->request->getParam('contractor');
                $cityId = $this->request->getParam('city');

                $filters = array(
                    'status' => $status,
                    'contractor_id' => $contractor,
                    'city_id' => $cityId
                );

                $ret = $modelBooking->listCalendar($showdate, $viewtype, $filters);
                break;
            case "update":
                if (!CheckAuth::checkCredential(array('bookingEdit'))) {
                    $ret = array('IsSuccess' => false, 'Msg' => "You don't have edit  permission in this Booking");
                    break;
                }

                $calendarStartTime = $this->request->getParam('CalendarStartTime', '0000-00-00 00:00:00');
                $calendarEndTime = $this->request->getParam('CalendarEndTime', '0000-00-00 00:00:00');
                $eventId = $this->request->getParam('calendarId', 0);
                $eventType = $this->request->getParam('eventType', 'booking');

                if (!$modelBooking->checkBookingTimePeriod($eventId)) {
                    $ret = array('IsSuccess' => false, 'Msg' => "You Don't Have Permission ,Check Time Period");
                    break;
                }
                if (!CheckAuth::checkIfCanHandelAllCompany('booking', $eventId)) {
                    $ret = array('IsSuccess' => false, 'Msg' => "This Booking not belongs to your Company");
                    break;
                }

                /**
                 * prepare data to update
                 */
                $data = array(
                    'booking_start' => php2MySqlTime(js2PhpTime($calendarStartTime)),
                    'booking_end' => php2MySqlTime(js2PhpTime($calendarEndTime)),
                );

                $ret = $modelBooking->updateCalendar($eventId, $data, $eventType);
                break;
            case "remove":
                if (!CheckAuth::checkCredential(array('bookingDelete'))) {
                    $ret = array('IsSuccess' => false, 'Msg' => "You Don't Have Permission To Delete This Booking");
                    break;
                }

                $eventId = $this->request->getParam('calendarId', 0);
                $eventType = $this->request->getParam('eventType', 'booking');

                if (!$modelBooking->checkBookingTimePeriod($eventId)) {
                    $ret = array('IsSuccess' => false, 'Msg' => "You Don't Have Permission ,Check Time Period");
                    break;
                }
                if (!CheckAuth::checkIfCanHandelAllCompany('booking', $eventId)) {
                    $ret = array('IsSuccess' => false, 'Msg' => "This Booking not belongs to your Company");
                    break;
                }
                break;
        }

        header('Content-type:text/javascript;charset=UTF-8');
        echo json_encode($ret);
        exit;
    }

}