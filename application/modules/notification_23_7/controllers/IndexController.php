<?php

class Notification_IndexController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'Notifications';
    }

    /**
     * Items list action
     */
    public function indexAction() {

		$this->view->main_menu = 'Notifications';
		$this->view->sub_menu = 'Notifications';
		//
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('contractors'));

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'user_id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());
         
        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = 30;
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        //
        // get data list
        //
		$filters['role_id'] = 1;
		if(!isset($filters['active'])){
			$filters['active'] = 'TRUE';
		}
		$filters['not_username'] = 'enquiries';
        $modelUser = new Model_User();
		$mongo = new Model_Mongo();
		//$mongo->insertNotification();
							
        $this->view->data = $mongo->getNotificationsByContractorId();

        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
    }

	 public function uploadContractorPhotoAction() {
	 
		//
        // get params
        //
        $contractor_id = $this->request->getParam('contractor_id');
        
		
		$form = new Contractor_Form_ContractorPhoto(array('contractor_id' => $contractor_id));
		//$form = new Reports_Form_ContractorInvoiceNumber(array('flag' => 0));
		
		//$form = new Contractor_Form_UploadContractorPhoto(array());
		/*if ($this->request->isPost()) {
            if ($form->isValid($this->request->getPost())) { // validate form data
                //$is_updated = $modelPaymentToContractors->updateById($payment_to_contractor_id, array('contractor_invoice_num' => $invoice_number)); 
				//echo json_encode(array('is_updated' => $is_updated,'payment_to_contractor_id'=> $payment_to_contractor_id,'new_invoice_number' => $invoice_number));
                //exit;
            }
			else{
				echo json_encode(array('is_updated' => 0));
			}
        }*/
		$this->view->form = $form;

		echo $this->view->render('index/upload-contractor-photo.phtml');
        exit;
	 }
    
}