<?php

class Contractor_Form_ContractorPhoto extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('InvoiceNumber');

        $router = Zend_Controller_Front::getInstance()->getRouter();

        $invoiceNumber = new Zend_Form_Element_Text('invoice_number');
        $invoiceNumber->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'text_field','style'=>'margin:5px 0px;'))
                ->setRequired();
        $datePaid = new Zend_Form_Element_Text('date_paid');
        $datePaid->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'text_field','style'=>'margin:5px 0px;'))
                ->setRequired();
		// By Islam
		$amountPaid = new Zend_Form_Element_Text('amount_paid');
        $amountPaid->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'text_field','style'=>'margin:5px 0px;'))
                ->setRequired();
		$cashPaid = new Zend_Form_Element_Text('cash_paid');
        $cashPaid->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'text_field','style'=>'margin:5px 0px;'));
                
				
		$invoiceTotalAmount = new Zend_Form_Element_Text('invoice_total_amount');
        $invoiceTotalAmount->setDecorators(array('ViewHelper'))
							->addDecorator('Errors', array('class' => 'errors'))
							->setAttribs(array('class' => 'text_field','style'=>'margin:5px 0px;'));
				
		$reference = new Zend_Form_Element_Text('reference');
        $reference->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'text_field','style'=>'margin:5px 0px;'));
                
				
		 $file = new Zend_Form_Element_File('file');
        $file->setDestination(get_config('temp_uploads'))
                ->setDecorators(array('File', 'Errors', array('Description', array('tag' => 'p', 'class' => 'hint'))))
                ->setMaxFileSize(get_config('upload_max_filesize')) // limits the filesize on the client side
                ->setDescription('Click Browse .........');
        $file->addValidator('Count', false, 1);  // ensure only 1 file
        $file->addValidator('Size', false, get_config('upload_max_filesize')); //10240000 limit to 10 meg


        $description = new Zend_Form_Element_Textarea('description');
        $description->setDecorators(array('ViewHelper', 'Errors'))
                ->setAttribs(array('class' => 'textarea_field', 'cols' => '40', 'rows' => '10'));
		
		/*$bookingPayments = new Zend_Form_Element_Textarea('booking_payments');
        $bookingPayments->setDecorators(array('ViewHelper', 'Errors'))
                ->setAttribs(array('class' => 'textarea_field', 'cols' => '40', 'rows' => '10', 'type'=>'hidden'));*/
		
		$bookingPayments = new Zend_Form_Element_Text('booking_payments');
        $bookingPayments->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'textarea_field','style'=>'margin:5px 0px; display: none;'));
                
		///////end
		


        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'button'));


        $this->addElements(array($invoiceNumber,$datePaid,$amountPaid,$cashPaid,$invoiceTotalAmount,$reference,$file,$description,$bookingPayments,$button));
       
		//$this->setName('payment_attachment');
        $this->setMethod(Zend_Form::METHOD_POST);
        $this->setEnctype(Zend_Form::ENCTYPE_MULTIPART);
        //$this->setAction($router->assemble(array('flag'=> $options['flag']), 'addContractorInvoiceNumber'));
		
    }

}
