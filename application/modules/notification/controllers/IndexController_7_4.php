<?php

class Notification_IndexController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'Notifications';
    }

    /**
     * Items list action
     */
    public function indexAction() {
        

        $this->view->main_menu = 'Notifications';
        $this->view->sub_menu = 'Notifications';
        //
        // check Auth for logged user
        //
        //CheckAuth::checkPermission(array('contractors'));
        //
        // get request parameters
        //
        $mongo = new Model_Mongo();

        $type = $this->request->getParam('type', array());
        $contractor_id = $this->request->getParam('contractor_id', 0);





        //
        // get data list
        //
        $filters['role_id'] = 1;
        if (!isset($filters['active'])) {
            $filters['active'] = 'TRUE';
        }
        $filters['not_username'] = 'enquiries';
        $modelUser = new Model_User();
        $this->view->contractors = $modelUser->getAllContractor();
        //$mongo->insertNotification();
       
        if ($contractor_id) {
            $this->view->contractor_id = $contractor_id;
            $this->view->data = $mongo->getNotificationsByContractorId( FALSE ,$contractor_id);
        } else {
            $this->view->data = $mongo->getNotificationsByContractorId();
        }
        $this->view->countNotifincations = $mongo->countNotifications();

        //
        // set view params
    //
      
    }

    public function changeStatusAction() {
        $redirect_status = $this->request->getParam("status");
        $id = $this->request->getParam("notification_id");
        $booking_id = $this->request->getParam("booking_id");
        //echo $id;


        $mongo = new Model_Mongo();
        $status = $mongo->updateNotification($id);

        if ($status && $redirect_status) {
            $this->_redirect($this->router->assemble(array('id' => $booking_id), 'bookingView'));
            exit;
        }
        exit;
    }

    public function markAllAsReadAction() {
        $mongo = new Model_Mongo();
        $withCases = $this->request->getParam('withCases', 0);
       
        if ($withCases) {
            $cases[0] = array('credential_name' => 'newContractorDiscussion');
            $cases[1] = array('credential_name' => 'newDiscussion');
            $status = $mongo->markAllAsRead($cases);
        } else {
            $status = $mongo->markAllAsRead();
        }
        if ($status) {
            echo 1;
            exit;
        }
        echo 0;
    }

    public function notificationsSettingsAction() {
        $modelAuthCredential = new Model_AuthRoleCredential();

        $loggedUser = CheckAuth::getLoggedUser();
        $mongo = new Model_Mongo();
        $loggedUserRole = $loggedUser['role_id'];
        $user_id = $loggedUser['user_id'];
        $userRoles = $modelAuthCredential->getCredentialsByParentName('notifications', $loggedUserRole);

        $this->view->userRoles = $userRoles;
        if ($this->request->isPost()) {
            $credential_name = $this->request->getParam('credential_name');
            $status = $this->request->getParam('status');

            $credential_name = strtolower(preg_replace('/(?<!\ )[A-Z]/', ' $0', $credential_name));

            // $mongo->getByTitleAndUserId($credential_name)

            if ($status == 'true') {

                $mongo->updateShowByTitle($credential_name);
            } else {

                $mongo->deleteByUserIdByCredentialNameAndUserID($credential_name, $user_id);
            }

            echo $status;
            exit;
        }
    }

    public function discussionNotificationsAction() {
        $modelAuthCredential = new Model_AuthRoleCredential();
        $loggedUser = CheckAuth::getLoggedUser();
        $mongo = new Model_Mongo();
        $modelUser = new Model_User();
        $contractor_id = $this->request->getParam('contractor_id', 0);
        $this->view->contractors = $modelUser->getAllContractor();
        $cases[0] = array('credential_name' => 'newContractorDiscussion');
        $cases[1] = array('credential_name' => 'newDiscussion');
        if ($contractor_id) {
            $this->view->contractor_id = $contractor_id;
            $this->view->data = $mongo->getNotificationsByContractorId(FALSE, $contractor_id, $cases);
        } else {
            $this->view->data = $mongo->getNotificationsByContractorId(FALSE, 0, $cases);
        }
//        $this->view->data = $discussionNotifications;
    }

    public function clearCountAction() {
        $withCases = $this->request->getParam('withCases', 0);
        $mongo = new Model_Mongo();
        if ($withCases) {
            $cases[0] = array('credential_name' => 'newContractorDiscussion');
            $cases[1] = array('credential_name' => 'newDiscussion');
            $status = $mongo->clearCountstatusNotification($cases);
            $count = $mongo->countNotifications($cases);
        } else {
            $status = $mongo->clearCountstatusNotification();
            $count = $mongo->countNotifications();
        }
        if ($status) {
            echo $count;
        }

        exit;
    }

}
