<?php

class Notification_IndexController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'Notifications';
        $sess = new Zend_Session_Namespace();
        if (isset($sess->is_employee)) {
            $this->_helper->layout->setLayout('layout_sub');
        }

        $pageName = $this->request->getParam('page_name', '');
        $this->view->page_title = (!empty($pageName)) ? $pageName . " - Notifications" : "Notifications";
    }

    /**
     * Items list action
     */
    public function indexAction() {

        
        
        
        $this->view->main_menu = 'Notifications';
        $this->view->sub_menu = 'Notifications';
//
// check Auth for logged user
//
        //CheckAuth::checkPermission(array('contractors'));
//
        // get request parameters
//
        $mongoTest = new Model_MongoTest();

        $type = $this->request->getParam('type', array());
        $contractor_id = $this->request->getParam('contractor_id', 0);
        $notification_type = $this->request->GetParam('notification_type', 'notification');
        $active = $this->request->getParam('active', 'TRUE');
        $page_number = $this->request->getParam('page_number', 0);
        $is_first_time = $this->request->getParam('is_first_time');
//
// get data list
//



        $filters['role_id'] = 1;
        if (!isset($filters['active'])) {
            $filters['active'] = 'TRUE';
        }
        $filters['not_username'] = 'enquiries';
        $modelUser = new Model_User();
        $this->view->contractors = $modelUser->getAllContractor();
        $this->view->active = $active;
        $this->view->notification_type = $notification_type;
//$mongo->insertNotification();

        if ($this->request->isPost()) {

            if ($contractor_id) {

                $this->view->contractor_id = $contractor_id;
                $data = $mongoTest->getAll(array('user_id' => $contractor_id, 'notification_type' => $notification_type));
            } else {
                $loggedUser = CheckAuth::getLoggedUser();
                $data = $mongoTest->getAll(array('user_id' => $loggedUser['user_id'], 'notification_type' => $notification_type));
            }

            if (empty($data)) {
                $result['is_empty'] = 1;
            } else {
                $result['is_empty'] = 0;
            }
            $this->view->data = $data;
            $this->view->is_first_time = $is_first_time;
            $result_data = $this->view->render('index/draw-node.phtml');
            $result_data = trim(preg_replace('/\s\s+/', ' ', $result_data));
            if ($result_data) {
                $result['is_last_request'] = 0;
            } else {
                $result['is_last_request'] = 1;
            }
            $result['data'] = $result_data;
            echo json_encode($result);
            exit;
        }



        if ($contractor_id) {
            $this->view->contractor_id = $contractor_id;
        }
//$this->view->countNotifincations = $mongo->countNotifications();
//
        // set view params
//
    }

    public function readAction() {
        $model_mongoTest = new Model_MongoTest();
        $id = $this->request->getParam("notification_id");
        $notification_type = $this->request->GetParam('notification_type', 'notification');
        $loggedUser = CheckAuth::getLoggedUser();
        $filters = array(
            'notification_type' => $notification_type,
            'user_id' => (int) $loggedUser['user_id'],
            'notification_id' => $id,
        );
        $status = $model_mongoTest->read($filters);
        exit;
    }

    public function resetCountAction() {
        $model_mongoTest = new Model_MongoTest();
        $notification_type = $this->request->GetParam('notification_type', 'notification');

        $loggedUser = CheckAuth::getLoggedUser();
        $filters = array(
            'notification_type' => $notification_type,
            'user_id' => (int) $loggedUser['user_id']
        );
        $status = $model_mongoTest->resetCount($filters);

        if ($status) {
            echo $status;
        }
        exit;
    }

    public function markAllAsReadAction() {
        $mongoTest = new Model_MongoTest();
        $notification_type = $this->request->GetParam('notification_type', 'notification');
        $loggedUser = CheckAuth::getLoggedUser();
        $filters = array(
            'notification_type' => $notification_type,
            'user_id' => (int) $loggedUser['user_id']
        );
        $status = $mongoTest->read($filters);

        if ($status) {
            echo 1;
            exit;
        }
        echo 0;
    }

    public function notificationsSettingsAction() {
        $this->view->page_title = 'Settings - ' . $this->view->page_title;
        $modelAuthCredential = new Model_AuthRoleCredential();
        $model_notificationSetting = new Model_NotificationSetting();
        $loggedUser = CheckAuth::getLoggedUser();
        $mongoTest = new Model_MongoTest();
        $loggedUserRole = $loggedUser['role_id'];
        $user_id = $loggedUser['user_id'];

        $userRoles = $modelAuthCredential->getCredentialsByParentName('notifications', $loggedUserRole, $loggedUser['active']);

        $this->view->userRoles = $userRoles;
        if ($this->request->isPost()) {
            $credential_id = $this->request->getParam('credential_id');
            $status = $this->request->getParam('status');

            $data = array(
                'user_id' => $user_id,
                'credential_id' => $credential_id,
            );
            if ($status == 'true') {
                $model_notificationSetting->insert($data);
            } else {

                $setting = $model_notificationSetting->getByUserIdAndCredentialId($user_id, $credential_id);
                if ($setting) {
                    $model_notificationSetting->deleteById($setting['notification_setting_id']);
                }
            }
           if ($status == 'true') {
                $status = 1;
            } else {
                $status = 0;
            }
            $mongoTest->changeShowByCreadentailIdAndNotifyUserId($credential_id, $user_id, $status);
            echo $status;
            exit;
        }
    }

//    public function discussionNotificationsAction() {
//        $this->view->page_title = 'Discussion ' . $this->view->page_title;
//        $modelAuthCredential = new Model_AuthRoleCredential();
//        $loggedUser = CheckAuth::getLoggedUser();
//        $mongo = new Model_Mongo();
//        $modelUser = new Model_User();
//        $contractor_id = $this->request->getParam('contractor_id', 0);
//        $active = $this->request->getParam('active', 'TRUE');
//        //$this->view->contractors = $modelUser->getAllContractor();
//        $this->view->active = $active;
//        $cases[0] = array('credential_name' => 'invoiceDiscussion');
//        $cases[1] = array('credential_name' => 'estimateDiscussion');
//        $cases[2] = array('credential_name' => 'complaintDiscussion');
//        $cases[3] = array('credential_name' => 'newContractorDiscussion');
//        $cases[4] = array('credential_name' => 'newDiscussion');
//        if ($contractor_id) {
//            $this->view->contractor_id = $contractor_id;
//            $this->view->data = $mongo->getNotificationsByContractorId(FALSE, $contractor_id, $cases);
//        } else {
//            $this->view->data = $mongo->getNotificationsByContractorId(FALSE, 0, $cases);
//        }
////        $this->view->data = $discussionNotifications;
//    }

    /* public function clearCountAction() {
      $withCases = $this->request->getParam('withCases', 0);
      $mongo = new Model_Mongo();
      if ($withCases) {
      $cases[0] = array('credential_name' => 'invoiceDiscussion');
      $cases[1] = array('credential_name' => 'estimateDiscussion');
      $cases[2] = array('credential_name' => 'complaintDiscussion');
      $cases[3] = array('credential_name' => 'newContractorDiscussion');
      $cases[4] = array('credential_name' => 'newDiscussion');
      $status = $mongo->clearCountstatusNotification($cases);
      $count = $mongo->countNotifications($cases);
      } else {
      $status = $mongo->clearCountstatusNotification();
      $count = $mongo->countNotifications();
      }
      if ($status) {
      echo $count;
      }

      exit;
      } */

    public function indexSmsAction() {
        $this->view->page_title = 'SMS ' . $this->view->page_title;
        $page_number = $this->request->getParam('page_number', 0);
        $is_first_time = $this->request->getParam('is_first_time');
        CheckAuth::checkLoggedIn();
        $this->view->main_menu = 'Notifications';
        $this->view->sub_menu = 'Notifications';
        $mongo = new Model_Mongo();
        $modelAuthRole = new Model_AuthRole ();
        $loggedUser = CheckAuth::getLoggedUser();
        $contractorRoleId = $modelAuthRole->getRoleIdByName('contractor');

        if ($this->request->isPost()) {
//   $data = $mongo->getNotificationsByTitle($lastMonth = false, $cases = array(), $page_number);
//add
            if ($loggedUser['role_id'] == $contractorRoleId)
                $data = $mongo->getNotificationsBySmsContractorId($lastMonth = false, $loggedUser['user_id'], $cases = array(), $page_number);
            else
                $data = $mongo->getNotificationsByTitle($lastMonth = false, $cases = array(), $page_number);

/////end add
            $this->view->data = $data;
            if (empty($data)) {
                $result['is_empty'] = 1;
            } else {
                $result['is_empty'] = 0;
            }
            $this->view->is_first_time = $is_first_time;
            $result_data = $this->view->render('index/sms-draw-node.phtml');
            $result_data = trim(preg_replace('/\s\s+/', ' ', $result_data));
            if ($result_data) {
                $result['is_last_request'] = 0;
            } else {
                $result['is_last_request'] = 1;
            }
            $result['data'] = $result_data;
            echo json_encode($result);
            exit;
        }
        $type_sms = 1;
        $this->view->countSmsNotifincations = $mongo->countNotifications($cases = array(), $type_sms);
    }

}
