<?php

class Contractor_Form_UploadContractorPhoto extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('Upload Contractor photo');

        $router = Zend_Controller_Front::getInstance()->getRouter();
				
		$file = new Zend_Form_Element_File('file');
        $file->setDestination(get_config('temp_uploads'))
                ->setDecorators(array('File', 'Errors', array('Description', array('tag' => 'p', 'class' => 'hint'))))
                ->setMaxFileSize(get_config('upload_max_filesize')) // limits the filesize on the client side
                ->setDescription('Click Browse .........');
        $file->addValidator('Count', false, 1);  // ensure only 1 file
        $file->addValidator('Size', false, get_config('upload_max_filesize')); //10240000 limit to 10 meg

		///////end

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'button'));


        $this->addElements(array($file,$button));
       
		//$this->setName('payment_attachment');
        $this->setMethod(Zend_Form::METHOD_POST);
        $this->setEnctype(Zend_Form::ENCTYPE_MULTIPART);
        //$this->setAction($router->assemble(array('contractor_id' => $options['contractor_id']), 'uploadContractorPhoto'));
		
    }

}

