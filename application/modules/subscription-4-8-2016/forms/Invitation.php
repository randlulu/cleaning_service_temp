<?php

class Subscription_Form_Invitation extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $user = (isset($options['user']) ? $options['user'] : '');
        $email = (isset($options['email']) ? $options['email'] : 0);
        $hashcode = (isset($options['hashcode']) ? $options['hashcode'] : '');
       
        
		
        $router = Zend_Controller_Front::getInstance()->getRouter();
        
		
		$hashcode_field = new Zend_Form_Element_Hidden('hashcode');
        $hashcode_field ->setDecorators(array('ViewHelper'))
                        ->addDecorator('Errors', array('class' => 'errors'))
                        ->setAttribs(array('class' => 'text_field','style'=>'margin:5px 0px;','placeholder' => 'hashcode'))
                        ->setValue((!empty($hashcode) ? $hashcode : ''));
				
		$user_name = new Zend_Form_Element_Text('email1');
        $user_name ->setDecorators(array('ViewHelper'))
                    ->addDecorator('Errors', array('class' => 'errors'))
                    ->setAttribs(array('class' => 'inputstyle','placeholder' => 'username'))
                    ->setRequired()
					->setValue((!empty($user['email1']) ? $user['email1'] : ''));
					
        $first_name = new Zend_Form_Element_Text('first_name');
        $first_name ->setDecorators(array('ViewHelper'))
                    ->addDecorator('Errors', array('class' => 'errors'))
                    ->setAttribs(array('class' => 'inputstyle','style'=>'margin:5px 0px;','placeholder' => 'First Name'))
                    ->setRequired()
					->setValue((!empty($user['first_name']) ? $user['first_name'] : ''));
					
				
        $last_name = new Zend_Form_Element_Text('last_name');
        $last_name->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'inputstyle','style'=>'margin:5px 0px;','placeholder' => 'Last Name'))
                ->setRequired()
				->setValue((!empty($user['last_name']) ? $user['last_name'] : ''));
				
				
	
		$password = new Zend_Form_Element_Password('password');
        $password ->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'inputstyle','style'=>'margin:5px 0px;','placeholder'=>'Password'))
                ->setRequired();	
        $password->addValidator(new Zend_Validate_StringLength(array('min' => 6, 'max' => 20)));
		
		
        $password_confirm = new Zend_Form_Element_Password('password_confirm');
        $password_confirm->setDecorators(array('ViewHelper'))
                         ->addDecorator('Errors', array('class' => 'errors'))
                         ->setAttribs(array('class' => 'inputstyle','style'=>'margin:5px 0px;','placeholder' => 'Confirm Password'))
                         ->setRequired();		
		$password_confirm->addValidator(new Zend_Validate_StringLength(array('min' => 6, 'max' => 20)));

		
        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Sign up');
        $button->setAttribs(array('class' => 'buttonSub'));

        $this->addElements(array($user_name,$first_name,$last_name,$password,$password_confirm,$hashcode_field ,$button));
        $this->setMethod(Zend_Form::METHOD_POST);
        $this->setAction($router->assemble(array(), 'invitationform'));
		
    }

}

