<?php

class Subscription_PlanController extends Zend_Controller_Action
{

    public function init()
    {
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
        CheckAuth::checkEmployeePermission();
        $this->_helper->layout->setLayout('layout_sub');
        /* Initialize action controller here */
    }

    public function indexAction(){

        $orderBy = $this->request->getParam('sort', 'plan_id');
        $sortingMethod = $this->request->getParam('method', 'desc');
        //$currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());
        $is_first_time = $this->request->getParam('is_first_time');
        $page_number = $this->request->getParam('page_number');        

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        if ($this->request->isPost()) {
            if (isset($page_number)) {
                $perPage = 10;
                $currentPage = $page_number + 1;
            }

            $modelPlan = new Model_Plan();
            $loggedEmployee = CheckAuth::getLoggedEmployee();

            $plans = $modelPlan->getAll($filters, "{$orderBy} {$sortingMethod}", $pager, 0, $perPage, $currentPage);
           
           

            foreach ($plans as &$plan) {
                //accounts details
                $plan['accounts'] = $modelPlan->getAccountsByPlanId($plan['plan_id']);

            }
            $result = array();
            $this->view->data = $plans;
            $this->view->is_first_time = $is_first_time;
            $result['data'] = $this->view->render('plan/draw-node.phtml');

            if ($plans) {
                $result['is_last_request'] = 0;
            } else {
                $result['is_last_request'] = 1;
            }

            echo json_encode($result);
            exit;
        }
    }

    public function addAction() {

        // get request parameters
        $name = $this->request->getParam('name');
        $planCode = $this->request->getParam('planCode');
        $planTypeId = $this->request->getParam('planType');
        $planDescription = $this->request->getParam('planDescription');
        $maxUsers = $this->request->getParam('maxUsers');
        $chargePeriod = $this->request->getParam('chargePeriod');
        $chargeAmount = $this->request->getParam('chargeAmount');
        $trialPeriod = $this->request->getParam('trialPeriod');
        $monthlyPrice = $this->request->getParam('monthlyPrice');
        $annuallyPrice = $this->request->getParam('annuallyPrice');
        $status = $this->request->getParam('status');

        //
        // init action form
        //

        $form = new Subscription_Form_Plan();
        $this->view->form = $form;

        //
        // handling the insertion process via post
        //
        if ($this->request->isPost()) {
            if ($form->isValid($this->request->getPost())) {

                $data = array(
                	'name' => $name,
                    'plan_code' => $planCode,
                    'plan_type_id' => $planTypeId,
                    'plan_description' => $planDescription,
                    'max_users' => $maxUsers,
                    'charge_period' => $chargePeriod,
                    'charge_amount' => $chargeAmount,
                    'trial_period' => $trialPeriod,
                    'monthly_price' => $monthlyPrice,                    
                    'annually_price' => $annuallyPrice,                  
                    'status' => $status
                );

                $modelPlan = new Model_Plan();
                $newPlanId = $modelPlan->insert($data);

                
                $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "New plan added successfully"));

                $redirect = $this->router->assemble(array(), 'plan');

                $session = new Zend_Session_Namespace();
                // $session->popup = '1';

                echo json_encode(array('IsSuccess' => true, 'redirect' => $redirect));
                exit;
            } else {
                echo json_encode(array('IsSuccess' => false, 'view' => $this->view->render('plan/add_edit.phtml')));
                exit;
            }
        }

        echo $this->view->render('plan/add_edit.phtml');
        exit;
    }

    public function editAction() {
        // get request parameters
        $plan_id = $this->request->getParam('plan_id');
        $name = $this->request->getParam('name');
        $planCode = $this->request->getParam('planCode');
        $planTypeId = $this->request->getParam('planType');
        $planDescription = $this->request->getParam('planDescription');
        $maxUsers = $this->request->getParam('maxUsers');
        $chargePeriod = $this->request->getParam('chargePeriod');
        $chargeAmount = $this->request->getParam('chargeAmount');
        $trialPeriod = $this->request->getParam('trialPeriod');
        $monthlyPrice = $this->request->getParam('monthlyPrice');
        $annuallyPrice = $this->request->getParam('annuallyPrice');
        $status = $this->request->getParam('status');

        $modelPlan = new Model_Plan();
        $plan = $modelPlan->getById($plan_id);

        if (!$plan) {
            $this->_redirect($this->router->assemble(array(), 'plan'));
            return;
        }

        $form = new Subscription_Form_Plan(array('mode' => 'update', 'plan' => $plan));
        $this->view->form = $form;
// var_dump($plan); exit;
                // handling the updating process
        if ($this->request->isPost()) {
            if ($form->isValid($this->request->getPost())) {


                $data = array(
                	'name' => $name,
                    'plan_code' => $planCode,
                    'plan_type_id' => $planTypeId,
                    'plan_description' => $planDescription,
                    'max_users' => $maxUsers,
                    'charge_period' => $chargePeriod,
                    'charge_amount' => $chargeAmount,
                    'trial_period' => $trialPeriod,
                    'monthly_price' => $monthlyPrice,                    
                    'annually_price' => $annuallyPrice,                  
                    'status' => $status
                );
                
                $action = $modelPlan->updateById($plan_id, $data);
                if($action){
                	$this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' =>"Plan updated successfully.."));
        		}else{
        			$this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No change happened.."));

            	}

                $redirect = $this->router->assemble(array(), 'plan');

                $session = new Zend_Session_Namespace();
                $session->popup = '1';

                echo json_encode(array('IsSuccess' => true, 'redirect' => $redirect));
                exit;
            } else {
                echo json_encode(array('IsSuccess' => false, 'view' => $this->view->render('plan/add_edit.phtml')));
                exit;
            }
        }

        echo $this->view->render('plan/add_edit.phtml');
        exit;

    }

    public function viewAction(){
    	$plan_id = $this->request->getParam('plan_id');
    	$modelPlan = new Model_Plan();
    	$plan = $modelPlan->getById($plan_id);
    	$this->view->data = $plan;
    }




}

