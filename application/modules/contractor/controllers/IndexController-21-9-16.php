<?php

class Contractor_IndexController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        // $this->view->main_menu = 'contractor';

        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsLocation';

        BreadCrumbs::setLevel(2, 'Cities');
    }

    /**
     * Items list action
     */
    public function indexAction() {




        //$this->view->main_menu = 'contractors';
        //$this->view->sub_menu = 'contractors';
        //
        // check Auth for logged user
        //
		
		
        CheckAuth::checkPermission(array('contractors'));

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'user_id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        //$currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());
        $is_first_time = $this->request->getParam('is_first_time');
        $page_number = $this->request->getParam('page_number');
        $modelBooking = new Model_Booking();
        $modelClaimOwner = new Model_ClaimOwner();
        $modelMissedCalls = new Model_MissedCalls();
        $modelComplaint = new Model_Complaint();
        $modelPayment = new Model_Payment();
        $modelContractorRate = new Model_ContractorRate();

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
        //
        /* $pager = new Model_Pager();
          $pager->perPage = 30;
          $pager->currentPage = $currentPage;
          $pager->url = $_SERVER['REQUEST_URI']; */

        //
        // get data list
        //
		$filters['role_id'] = 1;
        if (!isset($filters['active'])) {
            $filters['active'] = 'TRUE';
        }
        $filters['not_username'] = 'enquiries';

        if ($this->request->isPost()) {
            if (isset($page_number)) {
                $perPage = 15;
                $currentPage = $page_number + 1;
            }

            $modelUser = new Model_User();
            $data = $modelUser->getAll($filters, "{$orderBy} {$sortingMethod}", $pager, 0, $perPage, $currentPage);
            foreach ($data as $key => $row) {

                $data[$key]['countAwaitingaccept'] = $modelBooking->getCountAwaitingAcceptBooking($row['user_id']);
                $data[$key]['countAwaitingUpdate'] = $modelBooking->getCountAwaitingupdateBooking($row['user_id']);
                $data[$key]['countUnapprovedBookingContractor'] = $modelBooking->getCountUnapprovedBooking($row['user_id']);
                $data[$key]['countRejectBookingContractor'] = $modelBooking->getCountRejectedBookings($row['user_id']);
                $data[$key]['countOpenComplaintForContractor'] = $modelComplaint->getComplaintCount(array('complaintStatus' => 'open', 'contractor_id' => $row['user_id']));
                $data[$key]['countUnapprovedComplaintForContractor'] = $modelComplaint->countUapprovedCompalints($row['user_id']);
                $data[$key]['countClaimOwnerContractor'] = $modelClaimOwner->getCountClaimOwner($row['user_id']);
                $data[$key]['countMissedCallsContractor'] = $modelMissedCalls->getCountMissedCalls($row['user_id']);
                $data[$key]['countUnapprovedPaymentsForContractor'] = $modelPayment->getCountUnapprovedPayments($row['user_id']);
                $data[$key]['contractorRate'] = $modelContractorRate->getRateByContractor($row['user_id']);
            }

            $result = array();
            $this->view->data = $data;
            $this->view->is_first_time = $is_first_time;
            $result['data'] = $this->view->render('index/draw-node.phtml');
            if ($data) {
                $result['is_last_request'] = 0;
            } else {
                $result['is_last_request'] = 1;
            }

            echo json_encode($result);

            exit;
        }


        //
        // set view params
        //
        //$this->view->currentPage = $currentPage;
        //$this->view->perPage = $pager->perPage;
        //$this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
    }

    public function sendEmailToAllAction() {


        CheckAuth::checkPermission(array('sendEmail'));

        $type = $this->request->getParam('type', 'contractor');
        $referenceId = $this->request->getParam('reference_id', 'all');
        if ($this->request->isPost()) {
            $dir = get_config('attachment');
            $subdir = date('Y/m/d/');
            $fullDir = $dir . '/' . $subdir;

            if (!is_dir($fullDir)) {
                mkdir($fullDir, 0777, true);
            }

            $to = $this->request->getParam('to');
            $cc = $this->request->getParam('cc');
            $bcc = $this->request->getParam('bcc');
            $subject = $this->request->getParam('subject');
            $body = $this->request->getParam('body');
            $cannedResponsesValue = $this->request->getParam('canned-responses');



            $params = array(
                'to'=>$to,
                'bcc' => $bcc,
                'body' => $body,
                'subject' => $subject
            );

            if ($_FILES) {
                $file_name = array();
                $file_tmp_name = array();
                foreach ($_FILES['file'] as $key => $file) {

                    if ($key == 'name') {
                        foreach ($file as $key => $value) {
                            $file_name[] = $value;
                        }
                    } else if ($key == 'tmp_name') {
                        foreach ($file as $key => $value) {
                            $file_tmp_name[] = $value;
                        }
                    }
                }

                $paths = array();
                foreach ($file_name as $key => $file_name_value) {
                    if (move_uploaded_file($file_tmp_name[$key], $fullDir . basename($file_name_value))) {
                        $paths[] = $fullDir . $file_name_value;
                    }
                }
                //var_dump($paths);
                $params['attachment'] = implode(',', $paths);
            }
            $email_log = array(
                'type' => $type,
                'reference_id' => $referenceId
            );

            $error_mesages = array();
            $toBcc = 1;
            if (EmailNotification::validation($params, $error_mesages , $toBcc)) {
                $success = EmailNotification::sendEmail($params, '', array(), $email_log);
                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Email sent successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Email not sent"));
                }

                echo 1;
                exit;
            }
            $this->view->error_mesages = $error_mesages;
        }
       
        $filters = array(
            'active' => 'true',
            'role_id' => 1,
            'not_username' => 'enquiries'
        );

        $modelUser = new Model_User();
        $modelCannedResponses = new Model_CannedResponses();
        $cannedResponses = $modelCannedResponses->getAll();
        $this->view->cannedResponses = $cannedResponses;
        $contractors = $modelUser->getAll($filters);
        $emails = array();
        foreach ($contractors as $key => $contractor) {
            $emails[] = $contractor['email1'];
        }
        $bcc = implode(',', $emails);
        $this->view->bcc = $bcc;
        $this->view->cc = isset($cc) ? $cc : '';
        $this->view->subject = isset($subject) ? $subject : '';
        $this->view->body = isset($body) ? $body : '';
        $this->view->type = $type;
        $this->view->reference_id = $referenceId;
        $this->view->cannedResponsesValue = isset($cannedResponsesValue) ? $cannedResponsesValue : 0;


        echo $this->view->render('index/send-email.phtml');
        exit;
    }

}
