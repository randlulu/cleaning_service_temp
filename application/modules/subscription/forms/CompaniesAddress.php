<?php

class Subscription_Form_CompaniesAddress extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('Companies');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $company = (isset($options['company']) ? $options['company'] : '');
        $countryId = (isset($options['country_id']) ? $options['country_id'] : 0);

        $router = Zend_Controller_Front::getInstance()->getRouter();

      


        


       





        $companyUnitLotNo = new Zend_Form_Element_Text('company_unit_lot_no');
        $companyUnitLotNo->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($company['company_unit_lot_no']) ? $company['company_unit_lot_no'] : ''));
        //->setErrorMessages(array('Required' => 'Please enter the street address'));

        $companyStreetNo = new Zend_Form_Element_Text('company_street_no');
        $companyStreetNo->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($company['company_street_no']) ? $company['company_street_no'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the street no'));

        $companyStreetAddress = new Zend_Form_Element_Text('company_street_address');
        $companyStreetAddress->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($company['company_street_address']) ? $company['company_street_address'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the street address'));


        $companySuburb = new Zend_Form_Element_Text('company_suburb');
        $companySuburb->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($company['company_suburb']) ? $company['company_suburb'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the suburb'));


        //
        //get country & city for ajax
        //
        $city_obj = new Model_Cities();
        $city = $city_obj->getById((!empty($company['city_id']) ? $company['city_id'] : 14 ));

        $country_id = new Zend_Form_Element_Select('country_id');
        $country_id->setDecorators(array('ViewHelper'))
                ->setAttribs(array('class' => 'form-control', 'onchange' => 'getCities();'))
                ->setRequired()
                ->setValue((!empty($city['country_id']) ? $city['country_id'] : ''))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setErrorMessages(array('Required' => 'Please select Country'));

        $table = new Model_Countries();
        $country_id->addMultiOption('', 'Select One');
        foreach ($table->getCountriesAsArray() as $c) {
            $country_id->addMultiOption($c['id'], $c['name']);
        }

        $city_id = new Zend_Form_Element_Select('city_id');
        $city_id->setDecorators(array('ViewHelper'))
                ->setAttribs(array('class' => 'form-control'))
                ->setRequired()
                ->setValue((!empty($city['city_id']) ? $city['city_id'] : ''))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setErrorMessages(array('Required' => 'Please select City'));

        $city_id->addMultiOption('', 'Select One');
        foreach ($city_obj->getByCountryId((!empty($countryId) ? $countryId : $city['country_id'] )) as $c) {
            $city_id->addMultiOption($c['city_id'], $c['city_name']);
        }


        /*$companyState = new Zend_Form_Element_Text('company_state');
        $companyState->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($company['company_state']) ? $company['company_state'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the state'));*/
			$companyState = new Zend_Form_Element_Select('company_state');
			$companyState->setDecorators(array('ViewHelper'))
					->addDecorator('Errors', array('class' => 'errors'))
					->setRequired()
					->setAttribs(array('class' => 'form-control', 'onchange' => 'getCitiesByState();' , 'id'=>'state'))
					->setValue((!empty($company['company_state']) ? $company['company_state'] : 'Select One'))
					->addMultiOption('', 'Select One')
					->addMultiOptions($city_obj->getStateByCountryId((!empty($countryId) ? $countryId : $city['country_id'])));		
		

        $companyPostcode = new Zend_Form_Element_Text('company_postcode');
        $companyPostcode->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($company['company_postcode']) ? $company['company_postcode'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the postcode'));


        $companyPoBox = new Zend_Form_Element_Text('company_po_box');
        $companyPoBox->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($company['company_po_box']) ? $company['company_po_box'] : ''));
        //->setErrorMessages(array('Required' => 'Please enter the company PO Box'));



        /*
          $companyLogo = new Zend_Form_Element_File('company_logo');
          $companyLogo->addDecorator('Errors', array('class' => 'errors'))
          ->setDestination(UPLOADS_PATH . '/company_logo')
          ->setMaxFileSize(get_config('upload_max_filesize'))
          ->addValidator('Count', FALSE, 1)
          ->addValidator('Size', FALSE, get_config('upload_max_filesize'))
          ->addValidator('Extension', FALSE, 'jpg.jpeg,png,gif')
          ->removeDecorator('HtmlTag')
          ->removeDecorator('Label')
          ->setRequired()
          ->setAttribs(array('class' => 'form-control'))
          ->setErrorMessages(array('Required' => 'Please enter the company Logo'));
         * 
         */


        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));

        $this->addElements(array( $companyUnitLotNo, $companyStreetNo, $companyStreetAddress, $companySuburb, $country_id, $city_id, $companyState, $companyPostcode, $companyPoBox, $button));
        $this->setMethod('post');
        if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $company['company_id']), 'completecompanypanel'));
        } else {
            $this->setAction($router->assemble(array(), 'completecompanypanel'));
        }
    }

}

