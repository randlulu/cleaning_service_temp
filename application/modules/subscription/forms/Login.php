<?php

class Subscription_Form_Login extends Zend_Form
{

    public function init()
    {
        /* Form Elements & Other Definitions Here ... */
    }

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('Login');
        $router = Zend_Controller_Front::getInstance()->getRouter();

        $username = new Zend_Form_Element_Text('username');
        $username->setDecorators(array('ViewHelper'));
        $username->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'form-control','placeholder' => 'Email'))
                ->setRequired()				
				->addValidator(new Zend_Validate_EmailAddress());
				
				
		$password = new Zend_Form_Element_Password('password');
        $password ->setDecorators(array('ViewHelper'));
                $password->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'form-control','placeholder'=>'Password'))
                ->setRequired();

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('login');
        $button->setAttribs(array('class' => 'btn btn-primary block full-width m-b'));


        $this->addElements(array($username,$password,$button));
       		
        // $this->setMethod(Zend_Form::METHOD_POST);
        $this->setMethod('post');
        $this->setAction($router->assemble(array(), 'employeeLogin'));
		
    }


}

