<?php

class Subscription_Form_PhoneCapability extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

    $router = Zend_Controller_Front::getInstance()->getRouter();
        $request = Zend_Controller_Front::getInstance()->getRequest();
        $this->setName('PhoneCapability');

        $type = new Zend_Form_Element_Select('country_id');
        $type->removeDecorator('HtmlTag')
                ->removeDecorator('Label')
                ->setAttribs(array('class' => 'form-control', 'id' => 'country_id'))
                ->setRequired()
                ->addDecorator('Errors', array('class' => 'errors'));
        $type->addMultiOption('', 'Select One');
        $type->addMultiOption('', 'Local');
        $type->addMultiOption('', 'Mobile');
        $type->addMultiOption('', 'Toll Free');  


        $capability = new Zend_Form_Element_Select('country_id');
        $capability->removeDecorator('HtmlTag')
                ->removeDecorator('Label')
                ->setAttribs(array('class' => 'form-control', 'id' => 'country_id'))
                ->setRequired()
                ->addDecorator('Errors', array('class' => 'errors'));
  $capability->addMultiOption('', 'Select One');
        $capability->addMultiOption('', 'Dual(SMS and Voice)');
        $capability->addMultiOption('', 'SMS');
        $capability->addMultiOption('', 'Voice');




        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));


        $this->setMethod('post');

        $this->addElements(array($type,$capability, $button));

       
     
        $this->setAction($router->assemble(array(), 'CheckAvailabilityTwilioNumber'));
    }

}
