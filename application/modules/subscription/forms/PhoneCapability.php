<?php

class Subscription_Form_PhoneCapability extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);


        $this->setName('PhoneCapability');

        $router = Zend_Controller_Front::getInstance()->getRouter();
        $request = Zend_Controller_Front::getInstance()->getRequest();
        
        
        $type = new Zend_Form_Element_Select('capability');
        $type->removeDecorator('HtmlTag')
                ->removeDecorator('Label')
                ->setAttribs(array('class' => 'form-control'))
                ->setRequired()
                ->addDecorator('Errors', array('class' => 'errors'));
        $type->addMultiOption('', 'Select One');
        $type->addMultiOption('', 'Local');
        $type->addMultiOption('', 'Mobile');
        $type->addMultiOption('', 'Toll Free');  


        $capability = new Zend_Form_Element_Select('capability');
        $capability->removeDecorator('HtmlTag')
                ->removeDecorator('Label')
                ->setAttribs(array('class' => 'form-control'))
                ->setRequired()
                ->addDecorator('Errors', array('class' => 'errors'));
  $capability->addMultiOption('', 'Select One');
        $capability->addMultiOption('', 'Dual(SMS and Voice)');
        $capability->addMultiOption('', 'SMS');
        $capability->addMultiOption('', 'Voice');




        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));


        $this->setMethod('post');

        $this->addElements(array($type,$capability, $button));

        $this->setAction($router->assemble(array(), 'CheckAvailabilityTwilioNumber'));
    }

}
