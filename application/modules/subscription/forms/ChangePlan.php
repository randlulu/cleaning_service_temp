<?php

class Subscription_Form_ChangePlan extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);




        $plan = (isset($options['plan_id']) ? $options['plan_id'] : '');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        
        
        $plan_id = new Zend_Form_Element_Select('plan_id');
        $plan_id->removeDecorator('HtmlTag')
                ->removeDecorator('Label')
                ->setAttribs(array('class' => 'form-control', 'id' => 'plan_id','onchange'=>'calculateTotalAmountOnChangePlan(this.value,'.$plan.');'))
                ->setRequired()
                ->setValue((!empty($plan) ? $plan : ''))
                ->addDecorator('Errors', array('class' => 'errors'));

        $table = new Model_Plan();
        foreach ($table->getAll() as $c) {
            $plan_id->addMultiOption($c['plan_id'], $c['name']);
        }
        
        
        
         $this->addElements(array($plan_id));
        $this->setMethod('post');
        
        //$this->setAction($router->assemble(array('id' => 2), 'toaddusers'));
    }

}
