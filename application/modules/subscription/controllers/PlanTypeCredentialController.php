<?php

class Subscription_PlanTypeCredentialController extends Zend_Controller_Action {

    private $request;
    private $router;
    private $plan_type_id;

    public function init() {
        // parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
        $this->plan_type_id = $this->request->getParam('plan_type_id');
         CheckAuth::login_check();
        $this->_helper->layout->setLayout('layout_sub');

        // $this->view->main_menu = 'plan type';

        $modelPlanType = new Model_PlanType();
        $planType = $modelPlanType->getById($this->plan_type_id);

    }

    /**
     * Items list action
     */
    public function indexAction() {

        // get request parameters
        $credentials = $this->request->getParam('credentials', array());

        //
        //get all credential to render in view
        //
        $filters['sortCriteria1']=true;
        $authCredential_obj = new Model_AuthCredential();
        // $all_credential = $authCredential_obj->getAllCredentialTree();
        $all_credential = $authCredential_obj->getAllParentCredential();
        
        $this->view->all_credential = $all_credential;

        //
        //prepar old plan type credentials
        //
        $planTypeCredentialModel = new Model_PlanTypeCredential();
        $plan_type_credentials = $planTypeCredentialModel->getByPlanTypeId($this->plan_type_id);

        $old_credentials = array();
        if ($plan_type_credentials) {
            foreach ($plan_type_credentials as $plan_type_credential) {
                $old_credentials[] = $plan_type_credential['auth_credential_id'];
            }
        }


        if ($this->request->isPost()) {
            $planTypeCredentialModel->deleteByPlanTypeId($this->plan_type_id);

            $successArray = array();
            foreach ($credentials as $credential) {
                $data = array(
                    'plan_type_id' => $this->plan_type_id,
                    'auth_credential_id' => $credential
                );
                $success = $planTypeCredentialModel->insert($data);
                if ($success) {
                    $successArray [$credential][1] = $credential;
                } else {
                    $successArray [$credential][0] = $credential;
                }
            }

            foreach ($credentials as $credential) {
                if (empty($successArray[$credential][1])) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes in all credential in this plan type"));
                }
            }
            $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Add all credential to plan type successfully"));

            $this->_redirect($this->router->assemble(array(), 'planTypeList'));
        }

        $this->view->plan_type_id = $this->plan_type_id;
        $this->view->old_credentials = $old_credentials;
    }

}

