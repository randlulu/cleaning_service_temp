<?php

class Subscription_EmployeeController extends Zend_Controller_Action
{

    public function init()
    {
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
        $redirect = new Zend_Controller_Action_Helper_Redirector();
    }

    public function indexAction()
    {
  
        // get request parameters 
        $keepSign = $this->request->getParam('keep_sign', 1);
        $username = $this->request->getParam('username');
        $password = $this->request->getParam('password');
//
// init action form
//
        $form = new Subscription_Form_Login();

        if ($this->request->isPost()) { // check if POST request method

            $employeeobj = new Model_Employee();
            $employeeData = $employeeobj->getByEmailandPassword($username,sha1($password));
            if(!empty($employeeData)){
                    $user_browser = $_SERVER['HTTP_USER_AGENT'];
                    $employee_idenc = preg_replace("/[^0-9]+/", "", $employeeData['employee_id']);
                    $is_employeeenc = preg_replace("/[^a-zA-Z0-9_\-]+/", "", $employeeData['is_employee']);                    
                    $usernameenc    = preg_replace("/[^a-zA-Z0-9_\-]+/", "", $employeeData['username']);
                    
                    $login_string = hash('sha512', $employeeData['password'] . $user_browser);
                    $employee_id  = $employee_idenc;                                     
                    $username     = $usernameenc;                    
                    $company_name = $employeeData['company_name'];
                    $is_employee  = $is_employeeenc;
                    $avatar       = $employeeData['avatar'];
                    $displayName       = $employeeData['display_name'];
                CheckAuth::sec_session_start($login_string,$employee_id,$employeeData['email'],$username,$company_name,$is_employee,$avatar,$displayName);
             
                $redirect = new Zend_Controller_Action_Helper_Redirector();
                $redirect->gotoUrl('/subscription/account');
            //}
            }else{
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => 'invalid email or password'));
                $this->_redirect($this->router->assemble(array(), 'employeeLogin'));
            }

        }
        $this->view->form = $form;   
        // action body
    }


    public function logoutAction() {
        CheckAuth::employeeLogout();
        $this->_redirect($this->router->assemble(array(), 'employeeLogin'));

    }

  


}

