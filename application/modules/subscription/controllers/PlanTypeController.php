<?php

class Subscription_PlanTypeController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        // parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
         CheckAuth::login_check();
        $this->_helper->layout->setLayout('layout_sub');

        // $this->view->main_menu = 'settings';
        // BreadCrumbs::setLevel(2, 'User Role');
    }

    /**
     * Items list action
     */
    public function indexAction() {
        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'plan_type_id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        //
        // get data list
        //
        $planTypeModel = new Model_PlanType();
        $this->view->data = $planTypeModel->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);

        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
    }

    /**
     * Add new item action
     */
    public function addAction() {
        //
        // get request parameters
        //
        $planTypeName = $this->request->getParam('view_plan_type_name');
        // $defaultPage = $this->request->getParam('default_page');
        $router = Zend_Controller_Front::getInstance()->getRouter();

        //
        // init action form
        //
        $form = new Subscription_Form_PlanType();
        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $planTypeModel = new Model_PlanType();
                $data = array(
                    'view_plan_type_name' => $planTypeName,
                    'plan_type_name' => $planTypeModel->createPlanTypeName($planTypeName)
                    // ,'default_page' => $defaultPage
                );

                $success = $planTypeModel->insert($data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes happened in plan type !!"));
                }
                echo 1;
                exit;
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('plan-type/add_edit.phtml');
        exit;
    }



    public function editAction() {

        // get request parameters
        //
        $planTypeName = $this->request->getParam('view_plan_type_name');
        // $defaultPage = $this->request->getParam('default_page');
        $id = $this->request->getParam('plan_type_id');

        // validation
        $planTypeModel = new Model_PlanType();
        $planType = $planTypeModel->getById($id);
        if (!$planType) {
            $this->_redirect($this->router->assemble(array(), 'planTypeList'));
            return;
        }

        // init action form
        $form = new Subscription_Form_PlanType(array('mode' => 'update', 'planType' => $planType));
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'view_plan_type_name' => $planTypeName
                    //,'default_page' => $defaultPage
                );
                if ($planType['view_plan_type_name'] != $planTypeName) {
                    $data['plan_type_name'] = $planTypeModel->createPlanTypeName($planTypeName);
                }

                $success = $planTypeModel->updateById($id, $data);
                $this->view->successMessage = 'Updated Successfully';

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes in plan type"));
                }

                echo 1;
                exit;
            }
        }

        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('plan-type/add_edit.phtml');
        exit;
    }

    public function deleteAction() {


        //
        // get request parameters
        //
        $id = $this->request->getParam('plan_type_id', 0);
        $ids = $this->request->getParam('plan_type_ids', array());
        if ($id) {
            $ids[] = $id;
        }

        $planTypeModel = new Model_PlanType();
        foreach ($ids as $id) {
            $planType = $planTypeModel->getById($id);
            // if (!$planType['is_core']) {
                $isNotRelated = $planTypeModel->checkBeforeDeletePlanType($id);
                if ($isNotRelated) {
                    $planTypeModel->deleteById($id);
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Plan type deleted successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "could not be deleted ,This plan type used !!"));
                }
            // }
        }
        $this->_redirect($this->router->assemble(array(), 'planTypeList'));
    }

}

