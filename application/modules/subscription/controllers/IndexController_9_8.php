<?php

class Subscription_IndexController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {

        //  parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
        $this->_helper->layout->setLayout('layout_subscription');
    }

    /**
     * Items list action
     */
    public function indexAction() {

        $planID = (int) $this->_getParam('id');
        $geolocation = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip=' . $_SERVER['REMOTE_ADDR']));
        $code = $geolocation['geoplugin_countryCode'];   // get country code  and then redirect it new url with    country code  /1/AU/signup
        (isset($code) ? $code : $code = 'au');
        if(isset($planID) and $planID > 0 ){
           $this->_redirect($this->router->assemble(array('code' => strtolower($code), 'id' => $planID), 'signupupwithcode')); 
       }else{
           $this->_redirect($this->router->assemble(array('code' => strtolower($code), 'id' => 1), 'signupupwithcode'));
       } 
        exit;
    }

    public function coutriesJsonAction() {
        /// read file with countries 
        $file = file_get_contents('../library/countries.json', true);
        echo $file;
        exit;
    }

    public function signupCodeAction() {

        $plan_id = (int) $this->_getParam('id');
        if (!is_int($plan_id) | $plan_id <= 0) {     // check is (integer) id of plan
            exit;
        }

        $PlanModel = new Model_Plan();
        $get_dataPlan = $PlanModel->getById($plan_id);
        if (!$get_dataPlan) {            ///  check if plan id is exist in table plan
            exit;
        }

        $free_time = $get_dataPlan["trial_period"];
        $request = $this->getRequest();
        $form = new Subscription_Form_newaccount();



        if ($this->getRequest()->isPost()) {
            $isResendForm = $this->request->getParam('isResendForm');
            if ($isResendForm == 'goResendform') {  ///  current page is   page after sign up ( check your email and has it resend button )
                $emailAccount = $this->request->getParam('emailAccount');
                $modelUser = new Model_User();
                $user = $modelUser->getByEmail($emailAccount);
                if (!$user)
                    exit;
                $accountModel = new Model_Account();
                $account = $accountModel->getByCreatedBy($user['user_id']);
                // var_dump($account); exit;
                if (!$account or $account['account_status'] != 'Non-subscriber')
                    die(' no account');
                $companiesObj = new Model_Companies();
                $comp = $companiesObj->getById($account['company_id']);
                $company_name = $comp['company_name'];
                // this will open the form with data of exist  account  to update if he want 
                $form = new Subscription_Form_newaccount(array('company_name' => $company_name, 'username_email' => $emailAccount, 'mode' => 'update'
                    , 'idAccount' => $account['id']));
            }
            else {
                if ($form->isValid($request->getPost())) {    /// if he get post data from  sign up account page 
                    $roleuser = new Model_AuthRole();
                    $roleadmin = $roleuser->getRoleIdByName('account_admin');
                    $end_trial = date('Y-m-d H:i:s', strtotime("+" . $free_time . " days"));   // get end trial date 

                    $company_name = $this->request->getParam('company_name');
                    $username_email = $this->request->getParam('username_email');
                    $password = $this->request->getParam('password');
                    $password_confirm = $this->request->getParam('password_confirm');
                    $mode = $this->request->getParam('mode');
                    $modelUser = new Model_User();
                    ///    the  form  we can use it for  insert new sign up data  or  update data  for resend button 
                    if ($mode == 'update') {

                        // if upddate we will check if can updte data sign up or not ...check security 
                        $accountID = $this->request->getParam('idAccount');
                        $accountModel = new Model_Account();
                        $account = $accountModel->getById($accountID);
                        $modelUser = new Model_User();
                        // $user = $modelUser->getById($account['created_by']);

                        $user = $modelUser->haveOthersTheSameEmail($username_email, $account['created_by']);
                        /// haveOthersTheSameEmail()  function check if this email have to anybody else 

                        $id = $this->_getParam('id');
                        if ($user) {
                            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "email is used"));
                            $this->_redirect($this->router->assemble(array('id' => $id), 'signup'));
                            exit;
                        }

                        if ($password == $password_confirm) {

                            $data = array(
                                'email1' => $username_email,
                                'password' => sha1($password),
                                'user_code' => sha1($username_email),
                                'created' => time(),
                                'role_id' => $roleadmin, // set admin to this account  // get by name is better 
                                'active' => 'FALSE'
                            );

                            $hash_code = sha1($username_email);
                            $userID = $modelUser->updateById($account['created_by'], $data);   /// update user admin for his account 
                            $companiesObj = new Model_Companies();
                            $dataCompany = array(
                                'company_name' => $company_name
                            );

                            $companyID = $companiesObj->updateById($account['company_id'], $dataCompany);
                            $accountModel->sendEmailNewAccount($username_email, $hash_code);
                            //////////////////////////////////////////////////////////////////////////////////////////////////       
                            ///////////////////////////////////  redirect with success ///////////////////////////////////////
                            $this->view->assign('data', 'true');
                            $this->view->assign('email', $username_email);
                            $this->view->assign('idAccount', $accountID);
                            return;
                        } else {
                            $code = $this->_getParam('code');
                            $id = $this->_getParam('id');
                            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "password dont match"));
                            $this->_redirect($this->router->assemble(array('id' => $id, 'code' => $code), 'signupupwithcode'));
                        }
                    } else {
                        /// this is first time for sign up inser data 
                        $user = $modelUser->getByEmail($username_email);
                        $code = $this->_getParam('code');
                        $id = $this->_getParam('id');
                        if ($user) {
                            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "email is used "));
                            $this->_redirect($this->router->assemble(array('id' => $id, 'code' => $code), 'signupupwithcode'));
                            exit;
                        }

                        if ($password == $password_confirm) {

                            $data = array(
                                'email1' => $username_email,
                                'password' => sha1($password),
                                'user_code' => sha1($username_email),
                                'created' => time(),
                                'role_id' => (isset($roleadmin) ? $roleadmin : 10 ), // set admin to this account  // get by name is better 
                                'active' => 'FALSE'
                            );

                            $hash_code = sha1($username_email);
                            $userID = $modelUser->insert($data);   /// inser user admin for new account 
                            $companiesObj = new Model_Companies();
                            $dataCompany = array(
                                'company_name' => $company_name
                            );

                            $companyID = $companiesObj->insert($dataCompany);
                            $data = array(
                                'company_id' => $companyID,
                                'created_by' => $userID,
                                'account_status' => 'Non-subscriber', //  not active
                                'plan_id' => $plan_id, //  plan id
                                'trial_end_date' => $end_trial,
                                'renew_automatic' => 0, //  not renew automatically
                                'renew_automatic_mode' => 'monthly'
                            );

                            $accountModel = new Model_Account();
                            $accountID = $accountModel->insert($data);
                            $dataCompany = array(
                                'user_id' => $userID,
                                'company_id' => $companyID,
                                'created' => time()
                            );

                            $userCompaniesModel = new Model_UserCompanies();
                            $userCompaniesModel->insert($dataCompany);
                            //////////////////////  end  success add ////////////////////////////////////
                            //////////////////////////////  send email to new user account ////////////////
                            $accountModel->sendEmailNewAccount($username_email, $hash_code);
                            //////////////////////////////////////////////////////////////////////////////////////////////////       
                            ///////////////////////////////////  redirect with success ///////////////////////////////////////
                            // $accountID = 55;
                            $this->view->assign('data', 'true');
                            $this->view->assign('email', $username_email);
                            $this->view->assign('idAccount', $accountID);
                            return;
                        } else {
                            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "password dont match"));
                            $this->_redirect($this->router->assemble(array('id' => $id, 'code' => $code), 'signupupwithcode'));
                        }
                    }
                }
            }
        }

        $id = $this->_getParam('id');
        $this->view->form = $form;
    }

    public function checkValidateAccountAction() {

        /// check validate account  this  when  email received to admin account and ask to validate his account from url in email
        // and after click email url  this action will receive that

        if (Zend_Auth::getInstance()->hasIdentity()) {  // logout if he has previous account 
            CheckAuth::logout();
        }
        $email = $this->_getParam('email');
        $hashcode = $this->_getParam('hashcode');
        $userModel = new Model_User();
        $user = $userModel->getByEmail($email);


        if ($user) {
            if ($user['active'] == 'TRUE') {
                $this->view->assign('data', 'active_prev');
            } else {

                if ($user['user_code'] == $hashcode) {

                    $data = array(
                        'active' => 'TRUE',
                        'user_code' => ''           ///  empty the hash code to not get active if he return again to url . 
                    );

                    $userModel->updateById($user['user_id'], $data);
                    $this->_redirect($this->router->assemble(array(), 'Login'));
                }
                exit;
            }
        }
        exit;
    }

    /*
      public function getAuthrezed() {
      $authrezed = new Zend_Auth_Adapter_DbTable(Zend_Db_Table::getDefaultAdapter());
      $authrezed->setTableName('user')
      ->setIdentityColumn('email1')
      ->setCredentialColumn('password')
      ;

      return $authrezed;
      } */

    public function panelFirstLoginAction() {
        //// this is the  first page is shown in after  first login 

        CheckAuth::checkLoggedIn();
        $userr = CheckAuth::getLoggedUser();

        if (CheckAuth::checkAccountSteps($userr['user_id']) == 1) {
            $step = 1;   //  1  this  is activate and not  complete for his company data
            $this->view->assign('step', $step);
        } else if (CheckAuth::checkAccountSteps($userr['user_id']) == 2) {
            $step = 2;   // 2 this is complete comapny data and not complete his information 
            $this->view->assign('step', $step);
        } else if (CheckAuth::checkAccountSteps($userr['user_id']) == 3) {
            $step = 3;   // 2 this is complete comapny data and not complete his information 
            $this->view->assign('step', $step);
        } else if (CheckAuth::checkAccountSteps($userr['user_id']) == 4) {
            $step = 4;   // 2 this is complete comapny data and not complete his information 
            $this->view->assign('step', $step);
        } else if (CheckAuth::checkAccountSteps($userr['user_id']) == 5) {
            $step = 5;  //  this  complete his information and company infromation and not send invitation for new users 
            // var_dump($step); exit; 
            $this->view->assign('step', $step);
            //  $userr = CheckAuth::getLoggedUser();
            $accountModel = new Model_Account();
            $account = $accountModel->getByCreatedBy($userr['user_id']);
            $userCompaniesModel = new Model_UserCompanies();

            /////////////////  chech if it has permission to add new user to his account ////////////////
            ///////////////////////// get max users from paln who choose it before 
            $planModel = new Model_Plan();
            $Plan = $planModel->getByID($account['plan_id']);
            ////// get all users with this account /////////////////////////////////
            $users = $userCompaniesModel->getUsersByCompanyId($account['company_id']);
            if (count($users) == $Plan['max_users']) {
                // move it to  next      4   that mean has finish all sign up steps 
                $step = 6;
                // var_dump($step); exit;
                $this->view->assign('step', $step);
            }
        } else {
            $this->_redirect($this->router->assemble(array(), 'settings'));
        }
        $this->_helper->layout->setLayout('layout');
    }

    public function completeCompanyAddressAction() {
        CheckAuth::checkLoggedIn();

        $userr = CheckAuth::getLoggedUser();
        $accountModel = new Model_Account();
        $account = $accountModel->getByCreatedBy($userr['user_id']);   // check if user has create account
        $companiesObj = new Model_Companies();
        $company = $companiesObj->getById($account['company_id']);
        $form = new Subscription_Form_CompaniesAddress(array('mode' => 'update', 'company' => $company));


        $this->view->form = $form;
        echo $this->view->render('index/complete-company-address.phtml');
        // echo "kkkk"; 
        exit;
    }

    public function completeCompanyContactAction() {
        CheckAuth::checkLoggedIn();

        $userr = CheckAuth::getLoggedUser();
        $accountModel = new Model_Account();
        $account = $accountModel->getByCreatedBy($userr['user_id']);   // check if user has create account
        $companiesObj = new Model_Companies();
        $company = $companiesObj->getById($account['company_id']);
        $form = new Subscription_Form_CompaniesContact(array('mode' => 'update', 'company' => $company));


        $this->view->form = $form;
        echo $this->view->render('index/complete-company-contact.phtml');
        // echo "kkkk";
        exit;
    }

    public function completeCompanyInformationAction() {
        CheckAuth::checkLoggedIn();

        $userr = CheckAuth::getLoggedUser();
        $accountModel = new Model_Account();
        $account = $accountModel->getByCreatedBy($userr['user_id']);   // check if user has create account
        $companiesObj = new Model_Companies();
        $company = $companiesObj->getById($account['company_id']);
        $form = new Subscription_Form_CompaniesInformation(array('mode' => 'update', 'company' => $company));


        $this->view->form = $form;
        echo $this->view->render('index/complete-company-information.phtml');
        // echo "kkkk"; 
        exit;
    }

    public function completeCompanyAction() {
        CheckAuth::checkLoggedIn();
        $userr = CheckAuth::getLoggedUser();
        $accountModel = new Model_Account();
        $account = $accountModel->getByCreatedBy($userr['user_id']);   // check if user has create account
        $companiesObj = new Model_Companies();
        $company = $companiesObj->getById($account['company_id']);
        $id = $account['company_id'];

        $companyProcessPhase = $this->request->getParam('company-process');  // check  if 1 it will add company information 
        ///  if 2 will add comapny contact  if 3 add compnany address

        if ($companyProcessPhase == 1) {
            $companyName = $this->request->getParam('company_name');
            $companyBusinessName = $this->request->getParam('company_business_name');
            $companyAbn = $this->request->getParam('company_abn');
            $companyAcn = $this->request->getParam('company_acn');
            $companyGst = $this->request->getParam('company_gst');
            $companyGstDateRegistered = $this->request->getParam('company_gst_date_registered');
            $form = new Subscription_Form_CompaniesInformation(array('mode' => 'update', 'company' => $company));

            if ($this->request->isPost()) { // check if POST request method
                //  var_dump('post and will check validate'); exit;
                if ($form->isValid($this->request->getPost())) { // validate form data
                    $companiesObj = new Model_Companies();
                    $company = $companiesObj->getById($id);

                    $data = array(
                        'company_name' => trim($companyName),
                        'company_abn' => $companyAbn,
                        'company_acn' => $companyAcn,
                        'company_gst' => $companyGst,
                        'company_business_name' => $companyBusinessName,
                        'company_gst_date_registered' => strtotime($companyGstDateRegistered)
                    );
                    $success = $companiesObj->updateById($id, $data);

                    if ($success) {

                        $return['success'] = true;
                        $return['url'] = 'panel-first-login';
                        echo json_encode($return);
                        exit;
                    } else {

                        $return['success'] = true;
                        $return['url'] = 'panel-first-login';
                        echo json_encode($return);
                        exit;
                    }
                } else {
                    $this->view->form = $form;
                    echo $this->view->render('index/complete-company-information.phtml');
                    exit;
                }
            }
        } else if ($companyProcessPhase == 2) {

            $companyWebsite = $this->request->getParam('company_website');
            $companyEnquiriesEmail = $this->request->getParam('company_enquiries_email');
            $companyInvoicesEmail = $this->request->getParam('company_invoices_email');
            $companyAccountsEmail = $this->request->getParam('company_accounts_email');
            $companyComplaintsEmail = $this->request->getParam('company_complaints_email');
            $companyMobile1 = $this->request->getParam('company_mobile1');
            $companyMobile2 = $this->request->getParam('company_mobile2');
            $companyMobile3 = $this->request->getParam('company_mobile3');
            $companyPhone1 = $this->request->getParam('company_phone1');
            $companyPhone2 = $this->request->getParam('company_phone2');
            $companyPhone3 = $this->request->getParam('company_phone3');
            $companyFax = $this->request->getParam('company_fax');
            $companyEmergencyPhone = $this->request->getParam('company_emergency_phone');
            if (get_config('remove_white_spacing')) {
                $companyMobile1 = preparer_number($companyMobile1);
                $companyMobile2 = preparer_number($companyMobile2);
                $companyMobile3 = preparer_number($companyMobile3);
                $companyPhone1 = preparer_number($companyPhone1);
                $companyPhone2 = preparer_number($companyPhone2);
                $companyPhone3 = preparer_number($companyPhone3);
            }

            $form = new Subscription_Form_CompaniesContact(array('mode' => 'update', 'company' => $company));

            if ($this->request->isPost()) { // check if POST request method
                //  var_dump('post and will check validate'); exit;
                if ($form->isValid($this->request->getPost())) { // validate form data
                    $companiesObj = new Model_Companies();
                    $company = $companiesObj->getById($id);

                    $data = array(
                        'company_website' => $companyWebsite,
                        'company_accounts_email' => $companyAccountsEmail,
                        'company_complaints_email' => $companyComplaintsEmail,
                        'company_invoices_email' => $companyInvoicesEmail,
                        'company_phone1' => $companyPhone1,
                        'company_phone2' => $companyPhone2,
                        'company_phone3' => $companyPhone3,
                        'company_mobile1' => $companyMobile1,
                        'company_mobile2' => $companyMobile2,
                        'company_mobile3' => $companyMobile3,
                        'company_fax' => $companyFax,
                        'company_emergency_phone' => $companyEmergencyPhone
                    );
                    $success = $companiesObj->updateById($id, $data);

                    if ($success) {

                        $return['success'] = true;
                        $return['url'] = 'panel-first-login';
                        echo json_encode($return);
                        exit;
                    } else {

                        $return['success'] = true;
                        $return['url'] = 'panel-first-login';
                        echo json_encode($return);
                        exit;
                    }
                } else {
                    $this->view->form = $form;
                    echo $this->view->render('index/complete-company-contact.phtml');
                    exit;
                }
            }
        } else {

            $companyStreetAddress = $this->request->getParam('company_street_address');
            $companyUnitLotNo = $this->request->getParam('company_unit_lot_no');
            $companyStreetNo = $this->request->getParam('company_street_no');
            $companySuburb = $this->request->getParam('company_suburb');
            $cityId = $this->request->getParam('city_id');
            $countryId = $this->request->getParam('country_id');
            $companyState = $this->request->getParam('company_state');
            $companyPostcode = $this->request->getParam('company_postcode');
            $companyPoBox = $this->request->getParam('company_po_box');

            $form = new Subscription_Form_CompaniesAddress(array('mode' => 'update', 'company' => $company));
            if ($this->request->isPost()) { // check if POST request method
                //  var_dump('post and will check validate'); exit;
                if ($form->isValid($this->request->getPost())) { // validate form data
                    $companiesObj = new Model_Companies();
                    $company = $companiesObj->getById($id);

                    $data = array(
                        'company_street_address' => $companyStreetAddress,
                        'company_unit_lot_no' => $companyUnitLotNo,
                        'company_street_no' => $companyStreetNo,
                        'company_suburb' => $companySuburb,
                        'city_id' => $cityId,
                        //  'country_id' => $countryId,
                        'company_state' => $companyState,
                        'company_postcode' => $companyPostcode,
                        'company_po_box' => $companyPoBox
                    );
                    $success = $companiesObj->updateById($id, $data);

                    if ($success) {

                        $return['success'] = true;
                        $return['url'] = 'panel-first-login';
                        echo json_encode($return);
                        exit;
                    } else {

                        $return['success'] = true;
                        $return['url'] = 'panel-first-login';
                        echo json_encode($return);
                        exit;
                    }
                } else {
                    echo "not valid";
                    exit;
                    $this->view->form = $form;
                    echo $this->view->render('index/complete-company-address.phtml');
                    exit;
                }
            }
        }
        exit;
        /*
          else if($companyProcessPhase == 2){
          $companyWebsite = $this->request->getParam('company_website');
          $companyEnquiriesEmail = $this->request->getParam('company_enquiries_email');
          $companyInvoicesEmail = $this->request->getParam('company_invoices_email');
          $companyAccountsEmail = $this->request->getParam('company_accounts_email');
          $companyComplaintsEmail = $this->request->getParam('company_complaints_email');
          $companyMobile1 = $this->request->getParam('company_mobile1');
          $companyMobile2 = $this->request->getParam('company_mobile2');
          $companyMobile3 = $this->request->getParam('company_mobile3');
          $companyPhone1 = $this->request->getParam('company_phone1');
          $companyPhone2 = $this->request->getParam('company_phone2');
          $companyPhone3 = $this->request->getParam('company_phone3');
          $companyFax = $this->request->getParam('company_fax');
          $companyEmergencyPhone = $this->request->getParam('company_emergency_phone');
          if (get_config('remove_white_spacing')) {
          $companyMobile1 = preparer_number($companyMobile1);
          $companyMobile2 = preparer_number($companyMobile2);
          $companyMobile3 = preparer_number($companyMobile3);
          $companyPhone1 = preparer_number($companyPhone1);
          $companyPhone2 = preparer_number($companyPhone2);
          $companyPhone3 = preparer_number($companyPhone3);
          }
          //

          }else {


          $companyStreetAddress = $this->request->getParam('company_street_address');
          $companyUnitLotNo = $this->request->getParam('company_unit_lot_no');
          $companyStreetNo = $this->request->getParam('company_street_no');
          $companySuburb = $this->request->getParam('company_suburb');
          $cityId = $this->request->getParam('city_id');
          $countryId = $this->request->getParam('country_id');
          $companyState = $this->request->getParam('company_state');
          $companyPostcode = $this->request->getParam('company_postcode');
          $companyPoBox = $this->request->getParam('company_po_box');


          }

          $data = array(
          'company_name' => trim($companyName),
          'company_abn' => $companyAbn,
          'company_acn' => $companyAcn,
          'city_id' => $cityId,
          'company_enquiries_email' => $companyEnquiriesEmail,
          'company_postcode' => $companyPostcode,
          'company_street_address' => $companyStreetAddress,
          'company_unit_lot_no' => $companyUnitLotNo,
          'company_street_no' => $companyStreetNo,
          'company_suburb' => $companySuburb,
          'company_state' => $companyState,
          'company_po_box' => $companyPoBox,
          'company_po_box' => $companyPoBox,
          'company_website' => $companyWebsite,
          'company_accounts_email' => $companyAccountsEmail,
          'company_complaints_email' => $companyComplaintsEmail,
          'company_invoices_email' => $companyInvoicesEmail,
          'company_phone1' => $companyPhone1,
          'company_phone2' => $companyPhone2,
          'company_phone3' => $companyPhone3,
          'company_mobile1' => $companyMobile1,
          'company_mobile2' => $companyMobile2,
          'company_mobile3' => $companyMobile3,
          'company_gst' => $companyGst,
          'company_gst_date_registered' => strtotime($companyGstDateRegistered),
          'company_fax' => $companyFax,
          'company_emergency_phone' => $companyEmergencyPhone,
          'company_business_name' => $companyBusinessName
          );







         */
    }

    public function completeAdminAccountAction() {

        CheckAuth::checkLoggedIn();
        $this->_helper->layout->setLayout('layout');

        $userr = CheckAuth::getLoggedUser();
        $id = $userr['user_id'];

        $display_name = $this->request->getParam('display_name');
        $username = $this->request->getParam('username');
        //$password = $this->request->getParam('password');
        //  $roleId = $this->request->getParam('role_id');
        $first_name = $this->request->getParam('first_name');
        $last_name = $this->request->getParam('last_name');
        $email1 = $this->request->getParam('email1');
        $email2 = $this->request->getParam('email2');
        $email3 = $this->request->getParam('email3');
        $systemEmail = $this->request->getParam('systemEmail');
        $internation_key = $this->request->getParam('international_key');
        $mobile_key = $this->request->getParam('mobile_key');
        $phone_key = $this->request->getParam('phone_key');
        $mobile1 = $this->request->getParam('mobile1');
        $mobile2 = $this->request->getParam('mobile2');
        $mobile3 = $this->request->getParam('mobile3');
        $phone1 = $this->request->getParam('phone1');
        $phone2 = $this->request->getParam('phone2');
        $phone3 = $this->request->getParam('phone3');
        $fax = $this->request->getParam('fax');
        $emergencyPhone = $this->request->getParam('emergency_phone');
        $unitLotNumber = $this->request->getParam('unit_lot_number');
        $streetNumber = $this->request->getParam('street_number');
        $streetAddress = $this->request->getParam('street_address');
        $suburb = $this->request->getParam('suburb');
        $state = $this->request->getParam('state');
        $postcode = $this->request->getParam('postcode');
        $po_box = $this->request->getParam('po_box');
        $cityId = $this->request->getParam('city_id');
        $countryId = $this->request->getParam('country_id');
        $companyId = $this->request->getParam('company_id');


        /*         * ******Check if user put phone or mobile Number? and put full format***********IBM */
        if (isset($mobile1) && $mobile1 != "") {
            $mobile1_key = substr($mobile_key, 1, 1);
            $mobile1 = $internation_key . $mobile1_key . $mobile1;
        }
        if (isset($mobile2) && $mobile2 != "") {
            $mobile2_key = substr($mobile_key, 1, 1);
            $mobile2 = $internation_key . $mobile2_key . $mobile2;
        }
        if (isset($mobile3) && $mobile3 != "") {
            $mobile3_key = substr($mobile_key, 1, 1);
            $mobile3 = $internation_key . $mobile3_key . $mobile3;
        }
        if (isset($phone1) && $phone1 != "") {
            $phone1_key = substr($phone_key, 1, 1);
            $phone1 = $internation_key . $phone1_key . $phone1;
        }
        if (isset($phone2) && $phone2 != "") {
            $phone2_key = substr($phone_key, 1, 1);
            $phone2 = $internation_key . $phone2_key . $phone2;
        }
        if (isset($phone3) && $phone3 != "") {
            $phone3_key = substr($phone_key, 1, 1);
            $phone3 = $internation_key . $phone3_key . $phone3;
        }

        if (get_config('remove_white_spacing')) {
            $mobile1 = preparer_number($mobile1);
            $mobile2 = preparer_number($mobile2);
            $mobile3 = preparer_number($mobile3);
            $phone1 = preparer_number($phone1);
            $phone2 = preparer_number($phone2);
            $phone3 = preparer_number($phone3);
        }

        $modelUser = new Model_User();
        $user = $modelUser->getPureDataById($id);
        // var_dump($user ); exit;

        if (!$user) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect($this->router->assemble(array(), 'completeadminaccount'));
            return;
        }

        $userCompaniesModel = new Model_UserCompanies();
        $userCompanies = $userCompaniesModel->getByUserId($id);
        $form = new Subscription_Form_Useradmin(array('user' => $user, 'country_id' => $countryId, 'state' => $state, 'company_id' => $userCompanies['company_id']));
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'display_name' => $display_name,
                    'username' => $username,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'email1' => $email1,
                    'email2' => $email2,
                    'email3' => $email3,
                    'system_email' => $systemEmail,
                    'mobile1' => $mobile1,
                    'mobile2' => $mobile2,
                    'city_id' => $cityId,
                    'mobile3' => $mobile3,
                    'phone1' => $phone1,
                    'phone2' => $phone2,
                    'phone3' => $phone3,
                    'fax' => $fax,
                    'emergency_phone' => $emergencyPhone,
                    'unit_lot_number' => $unitLotNumber,
                    'street_number' => $streetNumber,
                    'street_address' => $streetAddress,
                    'suburb' => $suburb,
                    'state' => $state,
                    'postcode' => $postcode,
                    'po_box' => $po_box
                );

                $success = $modelUser->updateById($id, $data);
                $userCompaniesModel = new Model_UserCompanies();
                $userCompanies = $userCompaniesModel->getByUserId($id);
                $accountModel = new Model_Account();
                $account = $accountModel->getByCreatedBy($id);   // check if user has create account	
                $dataCompany = array(
                    'user_id' => $id,
                    'company_id' => $account['company_id'],
                    'created' => time()
                );

                if ($userCompanies) {
                    $userCompaniesModel->updateById($userCompanies['id'], $dataCompany);
                } else {
                    $userCompaniesModel->insert($dataCompany);
                }

                if ($success) {
                    //// if sucess  update status of account to complete admin info  and then
                    //// then redirect to panel to add users 
                    $accountModel = new Model_Account();
                    $account = $accountModel->getByCreatedBy($userr['user_id']);
                    if ($account) {
                        $emailtemplatemodel = new Model_EmailTemplate();
                        $emailtemplatemodel->duplicateMainEmailTemplate($account['company_id']);
                    }


                    $return['success'] = true;
                    $return['url'] = 'panel-first-login';
                    echo json_encode($return);
                    exit;
                    //////////////////////////////////////////////////////////////////////////
                }
            }
        }

        $this->view->form = $form;
        echo $this->view->render('index/complete-admin-account.phtml');
        exit;
    }

    public function addUsersPageAction() {

        /// this is page  popup  that to have form  firstname , lastname and  email  to add invitation for new user on his users company
        CheckAuth::checkLoggedIn();
        //$this->_helper->layout->setLayout('layout');
        $first_name = $this->request->getParam('first_name');
        $last_name = $this->request->getParam('last_name');
        $email = $this->request->getParam('email');
        $form = new Subscription_Form_AddUsersCompany();
        $userr = CheckAuth::getLoggedUser();
        $accountModel = new Model_Account();
        $userModel = new Model_User();
        $userAdmin = $userModel->getById($userr['user_id']);
        // var_dump($userr);exit;
        $account = $accountModel->getByCreatedBy($userr['user_id']);
        $userCompaniesModel = new Model_UserCompanies();
        $accountModel = new Model_Account();
        $account = $accountModel->getByCreatedBy($userr['user_id']);

        if ($account) {
            $data = array(
                'account_status' => 'Subscriber-trial'
            );
            $accountModel->updateById($account['id'], $data);  // active the account 	
        }

        //////////////////// this to duplicate system email template for him company ////////////////
        /////////////////  chech if it has permission to add new user to his account ////////////////
        ///////////////////////// get max users from paln who choose it before 
        $planModel = new Model_Plan();
        $Plan = $planModel->getByID($account['plan_id']);
        ////// get all users with this account /////////////////////////////////
        $users = $userCompaniesModel->getUsersByCompanyId($account['company_id']);
        if (count($users) == $Plan['max_users']) {

            $return['success'] = true;
            $return['url'] = 'panel-first-login';
            echo json_encode($return);
            exit;
        }

        //////////////////////////////////////////////////////////////////////////////////////////////

        if ($this->getRequest()->isPost()) {

            if ($form->isValid($this->request->getPost())) {

                $modelUser = new Model_User();
                $city = new Model_Cities();
                $citySydny = $city->getIDByName('Sydney');
                $user = $modelUser->getByEmail($email);

                if ($user) {
                    $return['success'] = false;
                    $return['msg'] = 'email is used ';
                    echo json_encode($return);
                    exit;
                }

                $data = array(
                    'email1' => $email,
                    'user_code' => sha1($email),
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'created' => time(),
                    'role_id' => 1, // set admin to this account
                    'active' => 'FALSE',
                    'city_id' => $citySydny
                );

                $hash_code = sha1($email);
                $userID = $modelUser->insert($data);   /// inser user admin for new account 
                // var_dump($userID) ;exit; 

                $dataCompany = array(
                    'user_id' => $userID,
                    'company_id' => $account['company_id'],
                    'created' => time()
                );

                $insertff = $userCompaniesModel->insert($dataCompany);

                /////////////////////////////send email invitation to new user //////////////////////////////////////////

                $accountModel->sendEmailInvitation($email, $hash_code, $userAdmin ['first_name']);

                $users = $userCompaniesModel->getUsersByCompanyId($account['company_id']);
                if (count($users) == $Plan['max_users']) {
                    $return['success'] = true;
                    $return['url'] = 'panel-first-login';
                    echo json_encode($return);
                    exit;
                }

                $return['success'] = 5;
                $return['msg'] = 'add user successfully ';
                echo json_encode($return);
                exit;
            }
        }

        $userCompaniesModel = new Model_UserCompanies();
        $users = $userCompaniesModel->getUsersByCompanyId($account['company_id']);
        //var_dump($users); exit;
        $this->view->form = $form;
        $this->view->assign('users', $users);

        echo $this->view->render('index/add-users-page.phtml');
        exit;
    }

    public function getInvitationAction() {


        if (Zend_Auth::getInstance()->hasIdentity()) {
            CheckAuth::logout();
        }


        $form = new Subscription_Form_Invitation();
        $email = $this->_getParam('email');
        $hashcode = $this->_getParam('hashcode');
        $userModel = new Model_User();
        $user = $userModel->getByEmail($email);
        $hashcode = $this->request->getParam('hashcode');

        if ($user) {
            if ($user['active'] == 'TRUE') {
                $this->view->assign('data', 'active_prev');
            } else {
                if ($user['user_code'] == $hashcode) {
                    $request = $this->getRequest();
                    $form = new Subscription_Form_invitation(array('user' => $user, 'hashcode' => $hashcode));


                    if ($this->getRequest()->isPost()) {
                        if ($form->isValid($request->getPost())) {

                            $username_email = $this->request->getParam('email1');
                            $first_name = $this->request->getParam('first_name');
                            $last_name = $this->request->getParam('last_name');
                            $password = $this->request->getParam('password');
                            $password_confirm = $this->request->getParam('password_confirm');

                            $modelUser = new Model_User();
                            $user = $modelUser->getByEmail($username_email);

                            if ($user and $user['email1'] != $email) {

                                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "email is used "));
                                $this->_redirect($this->router->assemble(array(), 'invitationform'));

                                exit;
                            }


                            if ($password == $password_confirm) {

                                $data = array(
                                    'email1' => $username_email,
                                    'password' => sha1($password),
                                    'user_code' => '',
                                    'active' => 'TRUE',
                                    'created' => time(),
                                    'first_name' => $first_name, // set admin to this account
                                    'last_name' => $last_name
                                );

                                $userID = $modelUser->updateById($user['user_id'], $data);   /// inser user admin for new account 
                                $this->_redirect($this->router->assemble(array(), 'Login'));
                            } else {
                                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "password dont match"));
                                $this->_redirect($this->router->assemble(array(), 'invitationform'));
                            }
                        }
                    }
                } else {
                    exit;
                }
            }
        } else {
            exit;
        }

        $this->view->form = $form;
    }

    public function checkCompleteNextAction() {

        $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Must Complete This Steps"));
        $this->_redirect($this->router->assemble(array(), 'panelFirstLogin'));
        exit;
    }

    public function forTestAction() {

        /*
          $countriesModel = new Model_Countries();
          $file = file_get_contents('../library/countries.json', true);
          $countries = json_decode($file);
          foreach ($countries as $key => $value) {
          foreach ($value as $key => $v) {
          $exist = $countriesModel-> getByname($v->name);
          //var_dump($exist); exit;
          if(!$exist){
          //  var_dump($exist.'ff'); exit;
          $data = array(
          'country_name' => $v->name,
          'country_code' => $v->code

          );
          $countriesModel->insert($data);
          }

          // break;
          }
          }
          //var_dump();
          exit;

         */
    }

//by mohamed
    public function subscriptionPageAction() {

//           $modelSubscriptionPayment = new Model_SubscriptionPayment();
//        $modelSubscriptionPaymentData  = $modelSubscriptionPayment->cronJobMakeAccountRenew();
//        if(!empty($modelSubscriptionPaymentData)){
//        print_r($modelSubscriptionPaymentData);}
//        else{
//            echo 'empty fucken array';
//        }
        $this->_helper->layout->setLayout('layout');
        CheckAuth::checkLoggedIn();
        $userr = CheckAuth::getLoggedUser();
        $accountModel = new Model_Account();
        $planModel = new Model_Plan();
        $userCompaniesModel = new Model_UserCompanies();
        $SubscriptionPaymentModel = new Model_SubscriptionPayment();
        $account = $accountModel->getByCreatedBy($userr['user_id']);

        if ($account) {


            $companiesObj = new Model_Companies();
            $company = $companiesObj->getById($account['company_id']);
            $dataPlan = $planModel->getById($account['plan_id']);
            $users = $userCompaniesModel->getUsersByCompanyId($account['company_id']);

            // var_dump( $users); exit;


            switch ($account['account_status']) {
                case 'Subscriber-trial':
                    $statusAccount = 'Trial';
                    break;
                case 'Subscriber-paid':
                    $statusAccount = 'Active';
                    break;
                case 'Suspended':
                    $statusAccount = 'Suspended';
                    break;
                default:
                    $statusAccount = 'undefined';
            }


            $stop_date1 = new DateTime($account['trial_end_date']);
            $trialEndDate = $stop_date1->format('Y-m-d');

            $stop_date2 = new DateTime($account['from']);
            $subcription_from = $stop_date2->format('Y-m-d');

            $stop_date3 = new DateTime($account['to']);
            $subcription_to = $stop_date3->format('Y-m-d');

            if (strtotime($trialEndDate) >= time()) {

                $remind_days = $accountModel->remindDays($trialEndDate);
                ( ( $remind_days < 0 ) ? $remind_days = 0 : $remind_days );
            } else {

                $remind_days = $accountModel->remindDays($subcription_to);
                ( ( $remind_days < 0 ) ? $remind_days = 0 : $remind_days );
                $price = $SubscriptionPaymentModel->getPaymentByAccountID($account['id']);
                $price = $price['amount'] / 100;
                $price = money_format('%i', $price);
                $this->view->assign('last_payment', $price);
            }

            $this->view->assign('company_name', $company['company_name']);
            $this->view->assign('plan_name', $dataPlan['name']);
            $this->view->assign('max_plan', $dataPlan['max_users']);
            $this->view->assign('account_status', $statusAccount);
            $this->view->assign('trial_end_date', $trialEndDate);
            $this->view->assign('from', $subcription_from);
            $this->view->assign('to', $subcription_to);
            $this->view->assign('company_users_active', count($users));
            $this->view->assign('remind_days', $remind_days);
            $this->view->assign('user_id', $userr['user_id']);
            $this->view->assign('account_id', $account['id']);
            $this->view->assign('CustomerTokenID', $account['customer_token_id']);

            //$this->view->assign('paidforusers', $account['paid_user_count'] );
        } else {
            exit;
        }
    }

//by mohamed 
    public function changePlanAction() {


        $this->_helper->layout->setLayout('layout');
        CheckAuth::checkLoggedIn();
        $userr = CheckAuth::getLoggedUser();
        $accountModel = new Model_Account();
        $modelUser = new Model_User();
        $account = $accountModel->getByCreatedBy($userr['user_id']);
        $accountId = $account['id'];


        $company = $accountModel->getCompanyById($account['company_id']);
        $plan = $accountModel->getPlanById($account['plan_id']);
        $user = $modelUser->getPureDataById($userr['user_id']);
        $users = $accountModel->getUsersByCompanyId($company['company_id']);
        $nbrOfuser = count($users);


        $form = new Subscription_Form_ChangePlan(array('plan_id' => $account['plan_id']));

        switch ($account['account_status']) {
            case 'Subscriber-trial':
                $statusAccount = 'Trial';
                break;
            case 'No-subscriber':
                $statusAccount = 'Trial';
                break;
            case 'Subscriber-paid':
                $statusAccount = 'Active';
                break;
            case 'Overdue':
                $statusAccount = 'Suspended';
                break;
            default:
                $statusAccount = 'undefined';
        }

        $this->view->form = $form;
        $this->view->accountId = $accountId;
        $this->view->statusAccount = $statusAccount;
        $this->view->trialEndDate = $account['trial_end_date'];
        $this->view->CustomerTokenID = $account['customer_token_id'];
        $this->view->to = $account['to'];
        $this->view->nbrOfRegisteredUsers = $nbrOfuser;
        $this->view->planId = $plan['plan_id'];
        $this->view->planMaxUser = $plan['max_users'];
        $this->view->planName = $plan['name'];
        $this->view->planChargeAmount = $plan['charge_amount'];

        $new_plan_id = $this->request->getParam('newPlanId', 0);


        if ($this->request->isPost()) {


            $is_approve = $this->request->getParam('isApprove', '');

            $SubscriptionPaymentObj = new Model_SubscriptionPayment();
            $result = $SubscriptionPaymentObj->calculateTotalAmountChangePlan($new_plan_id, $account['trial_end_date'], $account['to'], $plan['charge_amount'], $nbrOfuser, $plan['plan_id'], $account['positive_amount']);

            foreach ($result as $a) {
                echo $a . ",";
            }

            //**********************************the payment is approved*****************************************/
            if (strcmp($is_approve, 'approved') == 0) {


                $invoicemodelObj = new Model_InvoiceSubscription();
                $checkData = $invoicemodelObj->checkInvoiceExistInCurrentPeriod($account['id']);

                //1-check invoice----------------------------------------------------------
                if ($checkData['exist'] == 'no') {
                    $invoiceData = array(
                        'invoice_num' => $checkData['invoice_num'],
                        'account_id' => $account['id'],
                        'created_by' => $account['created_by'],
                        'status' => 'pending'
                    );
                    $invoicemodelObj->insert($invoiceData);
                }


                if ($result['amount'] > 0) {
                    //2-make transaction
                    $response = $SubscriptionPaymentObj->makeTransaction($account['customer_token_id'], $result['amount'], $checkData['invoice_num']);
                } elseif ($result['amount'] < 0) {
                    $response = $SubscriptionPaymentObj->makeTransaction($account['customer_token_id'], ($result['amount'] * -1), $checkData['invoice_num']);
                } else {
                    exit();
                }
                if ($response->getErrors()) {
                    //insert new record in payment with status fail
                    $paymentData = array(
                        'account_id' => $account['id'],
                        'created_by' => $account['created_by'],
                        'positive_amount' => $result['positive_amount'],
                        'amount' => abs($result['amount']),
                        'from' => date("Y-m-d", time()),
                        'to' => date('Y-m-d', strtotime('+1 month')),
                        'transaction_id' => $response->TransactionID,
                        'description' => 'Change plan',
                        'period' => $result['period'],
                        'new_plan_id' => $result['new_plan_id'],
                        'invoice_num' => $checkData['invoice_num'],
                        'status' => 'fail'
                    );
                    $paymentInsert = $SubscriptionPaymentObj->insert($paymentData);

                    //update status in account
                    $accountData = array(
                        'account_status' => 'Overdue'
                    );
                    $updateSuccess = $accountModel->updateById($account['id'], $accountData);
                    foreach ($response->getErrors() as $error) {
                        $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Error: " . \Eway\Rapid::getMessage($error)));
                        $this->_redirect($this->router->assemble(array(), 'pagesubscription'));
                    }
                    exit;
                } else {// transaction success........
                    //***********************************change plan ***********************************************/
                    if ($new_plan_id != 0) {
                        //3-account
                        $accountData = array(
                            'plan_id' => $new_plan_id,
                            'from' => date("Y-m-d", time()),
                            'to' => date('Y-m-d', strtotime('+1 month')),
                            'positive_amount' => 0.00
                        );
                        if ($result['amount'] < 0) {
                            $accountData = array(
                                'plan_id' => $new_plan_id,
                                'from' => date("Y-m-d", time()),
                                'to' => date('Y-m-d', strtotime('+1 month')),
                                'positive_amount' => $result['account_positive_amount']
                            );
                        }


                        //4-payment
                        $paymentData = array(
                            'account_id' => $account['id'],
                            'created_by' => $account['created_by'],
                            'positive_amount' => $result['positive_amount'],
                            'amount' => abs($result['amount']),
                            'from' => date("Y-m-d", time()),
                            'to' => date('Y-m-d', strtotime('+1 month')),
                            'transaction_id' => $response->TransactionID,
                            'description' => 'Change plan',
                            'period' => $result['period'],
                            'new_plan_id' => $result['new_plan_id'],
                            'invoice_num' => $checkData['invoice_num'],
                            'status' => 'paid'
                        );
                        //5-update invoice table
//                        if ($checkData['exist'] == 'yes') {
//                            $invoiceInfo = $invoicemodelObj->getInvoiceByInvoiceNum($checkData['invoice_num']);
//                            $res = $result['amount'] + $invoiceInfo['amount'];
//                            $updateInvoiceData = array(
//                                'amount' => $res
//                            );
//                        } else {
                        $updateInvoiceData = array(
                            'amount' => abs($result['amount']),
                        );
                        //}
                    }
                    //***********************************************************************************************/

                    $updateSuccess = $accountModel->updateById($account['id'], $accountData);
                    $updateInvoice = $invoicemodelObj->updateByInvoiceNum($checkData['invoice_num'], $updateInvoiceData);
                    $paymentInsert = $SubscriptionPaymentObj->insert($paymentData);
                    if (!$updateSuccess || !$updateInvoice || !$paymentInsert) {
                        $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes applied"));
                        $this->_redirect($this->router->assemble(array(), 'pagesubscription'));
                        exit;
                    }



//                else {
//                    //3-account
//                    $accountData = array(
//                        'plan_id' => $new_plan_id,
//                        'from' => date("Y-m-d", time()),
//                        'to' => date('Y-m-d', strtotime('+1 month')),
//                        'positive_amount' => $result['amount'] * - 1
//                    );
//
//                    //4-payment
//                    $paymentData = array(
//                        'account_id' => $account['id'],
//                        'created_by' => $account['created_by'],
//                        'amount' => 0.00,
//                        'from' => date("Y-m-d", time()),
//                        'to' => date('Y-m-d', strtotime('+1 month')),
//                        'description' => 'Change plan',
//                        'period' => $result['period'],
//                        'new_plan_id' => $result['new_plan_id'],
//                        'invoice_num' => $checkData['invoice_num']
//                    );
//                    $updateSuccess = $accountModel->updateById($account['id'], $accountData);
//                    $paymentInsert = $SubscriptionPaymentObj->insert($paymentData);
//                    if (!$updateSuccess || !$paymentInsert) {
//                        $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "no update in table account or Payment"));
//                        $this->_redirect($this->router->assemble(array(), 'pagesubscription'));
//                        exit;
//                    }
//                }
                    $emailData = array(
                        'invoice_num' => $checkData['invoice_num'],
                        'subscription_payment_id' => $paymentInsert,
                        'issued_date' => date("d/m/Y", time()),
                        'first_name' => $user['first_name'],
                        'last_name' => $user['last_name'],
                        'mobile1' => $user['mobile1'],
                        'email1' => $user['email1'],
                        'company_name' => $company['company_name'],
                        'company_suburb' => $company['company_suburb'],
                        'company_street_address' => $company['company_street_address'],
                        'description' => 'you make change plan in your account,<br> the previous plan is ' . $plan['name'] . '<br>and the new plan is ' . $result['name'],
                        'number_of_user' => $nbrOfuser,
                        'new_price_per_user' => $result['charge_amount'],
                        'old_price_per_user' => $plan['charge_amount'],
                        'from_old' => date('d/m/Y', strtotime($account['from'])),
                        'to_old' => date("d/m/Y", time()),
                        'from_new' => date("d/m/Y", time()),
                        'to_new' => date('d/m/Y', strtotime('+1 month')),
                        'account_positive_amount' => $account['positive_amount'],
                        'positive_amount' => $result['positive_amount'],
                        'gst_tax_amount' => $result['gst_tax_amount'],
                        'amount_without_tax' => $result['amount_without_tax'],
                        'total' => $result['amount']
                    );

                    $emailSuccess = $SubscriptionPaymentObj->sendPaymentInvoiceChangePlanAsEmail($emailData);
                    if ($emailSuccess) {
                        $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Invoice sent Successfully"));
                        $this->_redirect($this->router->assemble(array(), 'pagesubscription'));
                    } else {
                        $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Failed to send invoice"));
                        $this->_redirect($this->router->assemble(array(), 'pagesubscription'));
                        exit;
                    }
                }//end response from transaction 
            }// end if is approved

            exit;
        }//end is post
    }

//by mohamed
    public function changePlanTrialAction() {

        CheckAuth::checkLoggedIn();
        $userr = CheckAuth::getLoggedUser();
        $accountModel = new Model_Account();
        $account = $accountModel->getByCreatedBy($userr['user_id']);
        $accountId = $account['id'];

        $form = new Subscription_Form_ChangePlan(array('plan_id' => $account['plan_id']));

        $this->view->form = $form;
        $this->view->planId = $account['plan_id'];

        if ($this->request->isPost()) {

            $newPlanId = $this->request->getParam('newPlanId', 0);

            $dataForUpdate = array(
                'plan_id' => $newPlanId
            );
            $success = $accountModel->updateById($accountId, $dataForUpdate);

            if ($success) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "You have successfully changed your plan type"));
                $this->_redirect($this->router->assemble(array(), 'pagesubscription'));
            } else {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Problem with changing plan type"));
                $this->_redirect($this->router->assemble(array(), 'pagesubscription'));
            }
        }
        echo $this->view->render('index/change-plan-trial.phtml');
        exit;
    }

//by mohamed    
    public function checkMaxForSelectedPlanAction() {

        CheckAuth::checkLoggedIn();
        $userr = CheckAuth::getLoggedUser();
        $accountModel = new Model_Account();
        $account = $accountModel->getByCreatedBy($userr['user_id']);

        $users = $accountModel->getUsersByCompanyId($account['company_id']);
        $nbrOfuser = count($users);

        $newPlanIdSelected = $this->request->getParam('newPlanIdSelected', 0);
        $planModel = new Model_Plan();
        $newPlanSelected = $planModel->getById($newPlanIdSelected);

        if ($newPlanSelected['max_users'] < $nbrOfuser) {
            echo 1;
        } else {
            echo 2;
        }
        exit;
    }

}
