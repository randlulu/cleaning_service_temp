<?php

class EmailDetailsController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
    }

   public function indexAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('emailDetails'));

        //
        //get Params
        //
        $emailId = $this->request->getParam('id', 0);

        //
        // load model
        //
        $modelEmailLog = new Model_EmailLog();
        $emailLog = $modelEmailLog->getById($emailId);
        
	$this->view->emailLog = $emailLog;

		//D.A 09/09/2015 Mark email as read 
		$data=array('is_read'=>'1');
        $emailLog = $modelEmailLog->updateById($emailId,$data);

        echo $this->view->render('email-details/index.phtml');
        exit;
    }
	
	
	public function gmailMsgDetailsAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('emailDetails'));

        //
        //get Params
        //
        $messageId = $this->request->getParam('id', 0);
		$emailLog = array();
        
       
		
		$model = new Model_GmailServiceAccount(1); 
		$emails = $model->getAllEmails();
		$service = $model->getService();
		
		$optParamsGet = array();
        $optParamsGet['format'] = 'full'; // Display message in payload
        $message = $service->users_messages->get('me',$messageId,$optParamsGet);
        $messagePayload = $message->getPayload();
        $headers = $message->getPayload()->getHeaders();
		
		foreach( $headers as $header ) {
			if('To' == $header->name){
				$emailLog['To'] = $header->value;
			}
			if('Subject' == $header->name){
				$emailLog['Subject'] = $header->value;
			}
		
		}
        $parts = $message->getPayload()->getParts();
		
		//foreach($parts as $part){

        
		if($parts[0]['parts'][1]['body']){
		$body = $parts[0]['parts'][1]['body'];
		} 
		elseif($parts[0]['parts'][0]['body']){
			$body = $parts[0]['parts'][0]['body'];			
		}
		elseif($parts[1]['parts'][1]['body']){
			$body = $parts[1]['parts'][1]['body'];			
		}
		elseif($parts[1]['parts'][0]['body']){
			$body = $parts[1]['parts'][0]['body'];			
		}
		elseif($parts[0]['modelData']['body']){
			$body = $parts[0]['modelData']['body'];			
		}
		elseif($parts[1]['body']) {
			$body = $parts[1]['body'];
		} elseif($parts[0]['body']){
			$body = $parts[0]['body'];
		}
        $rawData = $body->data;
		
	    $sanitizedData = strtr($rawData,'-_', '+/');
        $decodedMessage = base64_decode($sanitizedData);	

		$emailLog['Subject'] = $decodedMessage;
		$this->view->emailLog = $emailLog;
		 
        echo $this->view->render('email-details/index.phtml');
        exit;
    }

}

