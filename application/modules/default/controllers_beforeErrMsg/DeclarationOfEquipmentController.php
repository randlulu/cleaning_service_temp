<?php

class DeclarationOfEquipmentController extends Zend_Controller_Action {

    private $request;
    private $router;
    private $contractor_info_id;

    public function init() {
        parent::init();

        CheckAuth::checkLoggedIn();

        if ('contractor' != CheckAuth::getRoleName()) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }

        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $loggedUser = CheckAuth::getLoggedUser();

        $contractorInfoObj = new Model_ContractorInfo();
        $contractorInfo = $contractorInfoObj->getByContractorId($loggedUser['user_id']);

        if (empty($contractorInfo)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "please fill all Contractor Info"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        $this->contractor_info_id = $contractorInfo['contractor_info_id'];
    }

    /**
     * Items list action
     */
    public function indexAction() {


        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if($filters){
            foreach ($filters as &$filter){
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];


        //
        //get user email by id
        //
        $filters['contractor_info_id'] = $this->contractor_info_id;


        //
        // get data list
        //
        $declarationOfEquipmentObj = new Model_DeclarationOfEquipment();
        $this->view->data = $declarationOfEquipmentObj->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);

        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
    }

    /**
     * Add new item action
     */
    public function addAction() {

        //
        // get request parameters
        //
        $equipment = $this->request->getParam('equipment');
        $router = Zend_Controller_Front::getInstance()->getRouter();

        //
        // init action form
        //
		$attachmentObj = new Model_Attachment();
		$declarationOfEquipmentAttachmentObj = new Model_DeclarationOfEquipmentAttachment();
		
        $form = new Form_DeclarationOfEquipment();

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $declarationOfEquipmentObj = new Model_DeclarationOfEquipment();
                $data = array(
                    'equipment' => $equipment,
                    'contractor_info_id' => $this->contractor_info_id
                );

                $success = $declarationOfEquipmentObj->insert($data);

                if ($success) {
				     $upload = new Zend_File_Transfer_Adapter_Http();
                    $files  = $upload->getFileInfo();
					$counter = 0 ;
					foreach($files as $file => $fileInfo) {
					  if ($upload->isUploaded($file)){
                        if ($upload->receive($file)){
						   $counter = $counter + 1;
						   $info = $upload->getFileInfo($file);
                           $source  = $info[$file]['tmp_name'];
                           $imageInfo = pathinfo($source);
                           $ext = $imageInfo['extension'];
						   $size = $info[$file]['size'];
						   $type = $info[$file]['type'];
				           $dir = get_config('attachment') . '/';
                           $subdir = date('Y/m/d/');
                           $fullDir = $dir . $subdir;
                           if (!is_dir($fullDir)) {
                              mkdir($fullDir, 0777, true);
                           }
                        $loggedUser = CheckAuth::getLoggedUser();							
						$data = array(
							 'created_by'=>$loggedUser['user_id'] 
							 ,'created'=>time()
							 ,'size' => $size
							 ,'type'=>$type
						 );
						 
						
						 
						 $id = $attachmentObj->insert($data);
						 $declarationOfEquipmentAttachmentObj->insert(array('equipment_id'=>$success,'attachment_id'=>$id));
						 $fileName = $success.'_'.$counter.'.'. $ext;
						 $image_saved = copy($source, $fullDir . $fileName);
						 
						 $typeParts = explode("/",$type);
						 if($typeParts[0] == 'image'){
						    $thumbName = $success.'_thumbnail_'.$counter.'.'. $ext;
						    ImageMagick::create_thumbnail($source, $fullDir . $thumbName, 78, 64);
				
						 }else{
						   
						   $thumbName = $success.'_'.$counter.'.jpg';
						   ImageMagick::convertFile($source.'[0]', $fullDir . $thumbName);
						 }
						 
						 $Updatedata = array(
                          'path' => $fullDir . $fileName,
                          'file_name' => $fileName,
						  'thumbnail_file' => $fullDir . $thumbName
                          );
						 $attachmentObj->updateById($id,$Updatedata);
						 
						  if ($image_saved) {
								if (file_exists($source)) {
									unlink($source);
								}               
							}
						}
					 }	
					}
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Declaration Of Equipment"));
                }
                //$this->_redirect($this->router->assemble(array(), 'settingsUserEmailList'));
                echo 1;
                exit;
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('declaration-of-equipment/add_edit.phtml');
        exit;
    }

    public function deleteAction() {

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
        if ($id) {
            $ids[] = $id;
        }

        $declarationOfEquipmentObj = new Model_DeclarationOfEquipment();
		$modelAttachment = new Model_Attachment();
        foreach ($ids as $id) {
            $declarationOfEquipment = $declarationOfEquipmentObj->getById($id);
            if ($this->contractor_info_id == $declarationOfEquipment['contractor_info_id']) {
			$filter = array('type'=>'equipment','itemid'=>$id);    
            $pager = null;			
		    $attachments =  $modelAttachment->getAll('a.created desc',$pager, $filter);
			if($attachments ){
			  foreach($attachments as $attachment){
			    $modelAttachment->updateById($attachment['attachment_id'] , array('is_deleted' => '1'));
			  }
			}
			
               $declarationOfEquipmentObj->deleteById($id);
            }
        }
        $this->_redirect($this->router->assemble(array(), 'declarationOfEquipmentList'));
    }

}

