<?php

class CommonController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
    }

    public function getEmailTemplateAction() {

        $emailTemplateId = $this->request->getParam('emailTemplate_id');
        $emailTemplateObj = new Model_EmailTemplate();
        $emailTemplate = $emailTemplateObj->getById($emailTemplateId);
        echo $emailTemplate['body'];
        exit;
    }

    public function citiesByCountryIdAction() {
        $country_id = $this->request->getParam('country_id', 0);
        $state = $this->request->getParam('state', 0);
        $isOnChange = $this->request->getParam('isOnChange', 0);
        $onChange = $this->request->getParam('onChange', '');
        $name = $this->request->getParam('name', 'city_id');
        $isArrayName = $this->request->getParam('isArrayName', 0);
        $arrayName = $this->request->getParam('arrayName', '');
        $class = $this->request->getParam('class', 'form-control required');
        $multiCheckbox = $this->request->getParam('MultiCheckbox', 0);

        $cities_obj = new Model_Cities();
        $cities = $cities_obj->getCitiesByCountryIdAndState($country_id, $state, true);

        if ($multiCheckbox) {
            $dropDown = new Zend_Form_Element_MultiCheckbox($name);
            $class = 'checkbox_field';
        } else {
            $dropDown = new Zend_Form_Element_Select($name);
        }
        if ($isArrayName) {
            $dropDown->setBelongsTo($arrayName);
        }
        $dropDown->setDecorators(array('ViewHelper'));
        $dropDown->setAttrib('class', $class);
        if ($isOnChange) {
            $dropDown->setAttrib('onchange', $onChange . '();');
        }

        if ($multiCheckbox) {
            $dropDown->addMultiOption('all', 'Select All');
        } else {
            $dropDown->addMultiOption('', 'Select One');
        }
        $dropDown->addMultiOptions($cities);

        if ($multiCheckbox) {
            echo '<div id="' . $name . '-cont">' . $dropDown->render() . '</div>';
            echo '<script type="text/javascript"> $(document).ready(function(){ $("label[for=\'' . $name . '-all\']").css("font-weight","bold"); $("#' . $name . '-all").click(function(){ var checked_status = this.checked; $("#' . $name . '-cont input[type=\'checkbox\']").each(function() { this.checked = checked_status; }); }); }); </script>';
        } else {
            echo $dropDown->render();
        }
        exit;
    }

    public function getCitiesByStateAction() {
        $state = $this->request->getParam('state', 0);

        $modelCities = new Model_Cities();
        $cities = $modelCities->getByState2($state);

        echo json_encode($cities);
        exit;
    }

    public function getByStateAction() {
        $countryId = $this->request->getParam('country_id', 0);
        $onChange = $this->request->getParam('onChange', 'getCities');
        $name = $this->request->getParam('name', 'state');
        $class = $this->request->getParam('class', 'form-control required');
        $isArrayName = $this->request->getParam('isArrayName', 0);
        $arrayName = $this->request->getParam('arrayName', '');


        $cities_obj = new Model_Cities();
        $states = $cities_obj->getStateByCountryId($countryId);

        $dropDown = new Zend_Form_Element_Select($name);
        if ($isArrayName) {
            $dropDown->setBelongsTo($arrayName);
        }
        $dropDown->setDecorators(array('ViewHelper'));
        $dropDown->setAttrib('class', $class);
        $dropDown->setAttrib('onchange', $onChange . '();');
        $dropDown->addMultiOption('', 'Select One');
        $dropDown->addMultiOptions($states);

        echo $dropDown->render();
        exit;
    }

    public function servicesAvailableByCityIdAction() {
        $city_id = $this->request->getParam('city_id', 0);
        $type = $this->request->getParam('type', 'booking');


        $contractorServiceAvailability_obj = new Model_ContractorServiceAvailability();
        $services = $contractorServiceAvailability_obj->getServiceByCityId($city_id);

        $this->view->services = $services;

        echo $this->view->render('common/searchServicesResult.phtml');
        exit;
    }

    public function searchServicesAction() {
        $city_id = $this->request->getParam('city_id', 0);
        $keywords = $this->request->getParam('keywords', '');
        $services = $this->request->getParam('services', array());

        $filters = array('city_id' => $city_id);

        if ($keywords) {
            $filters['keywords'] = $keywords;
        }

        if ($services) {
            $filters['services'] = $services;
        }

        $contractorServiceAvailability_obj = new Model_ContractorServiceAvailability();
        $services = $contractorServiceAvailability_obj->getAllService($filters);

        $this->view->services = $services;
        $this->view->keywords = $keywords;

        echo $this->view->render('common/searchServicesResult.phtml');
        exit;
    }

    public function contractorAvailableByServiceIdAction() {
        $service_id = $this->request->getParam('service_id');
        $cityId = $this->request->getParam('city_id', 0);

        $clone = $this->request->getParam('clone', 0);
        $isOnChange = $this->request->getParam('isOnChange', 0);
        $onChange = $this->request->getParam('onChange', '');

        $contractorService_obj = new Model_ContractorService();

        $contractorService = $contractorService_obj->getContractorByServiceIdAndCityId($service_id, $cityId, true);

        if (!CheckAuth::checkCredential(array('CanAssignContractorToBooking'))) {
            $user = CheckAuth::getLoggedUser();
            $modelContractorInfo = new Model_ContractorInfo();
            $contractorInfo = $modelContractorInfo->getByContractorId($user['user_id']);
            echo htmlentities((ucwords($user['username']) . (!empty($contractorInfo['business_name']) ? ' - ' . ucwords($contractorInfo['business_name']) : '')), ENT_COMPAT, 'UTF-8');
            echo '<input type="hidden" value="' . htmlentities($user['user_id'], ENT_COMPAT, 'UTF-8') . '" name="contractor_' . htmlentities($service_id, ENT_COMPAT, 'UTF-8') . ($clone ? '_' . $clone : '') . '">';
        } else {
            $dropDown = new Zend_Form_Element_Select('contractor_' . $service_id . ($clone ? '_' . $clone : ''));
            $dropDown->setDecorators(array('ViewHelper'));
            $dropDown->setAttrib('class', 'required dropdown form-control');
            if ($isOnChange) {
                $dropDown->setAttrib('id', 'contractor');
                $dropDown->setAttrib('onchange', $onChange . '();');
            }
            $dropDown->addMultiOption('', 'Select One');
            $dropDown->addMultiOptions($contractorService);

            echo $dropDown->render();
        }
        exit;
    }

    public function customerListAction() {

        echo $this->view->render('common/customerList.phtml');
        exit;
    }

    public function customerSearchAction() {
        $modelCustomer = new Model_Customer();
        $loggedUser = CheckAuth::getLoggedUser();

        $filters = array();
        //check if loged user is contractor
        if (!CheckAuth::checkCredential(array('CanSeeAllBooking'))) {
            if (CheckAuth::checkCredential(array('CanSeeOnlyHisBooking'))) {
                if (CheckAuth::checkCredential(array('CanSeeAssignedBooking'))) {
                    $filters['created_by'] = $loggedUser['user_id'];
                }
            }
        }
        $filters['full_name_search'] = $this->request->getParam('customer-like');

        //get all customer
        $customers = $modelCustomer->getAll($filters);
        $this->view->customers = $customers;

        echo $this->view->render('common/customerSearch.phtml');
        exit;
    }

    public function autoCompleteCustomerAction() {
        //$fullNameSearch = $this->request->getParam('full_name_search', '');
        $limit = $this->request->getParam('limit', 0);
        $filters = $this->request->getParam('fltr', array());

        $pager = null;
        $modelCustomer = new Model_Customer();
        $customers = $modelCustomer->getAll($filters, null, $pager, $limit);

        $selectCustomers = array();
        foreach ($customers as $customer) {
            $selectCustomers[] = array('customer_id' => $customer['customer_id'], 'customer_name' => get_customer_name($customer));
        }

        echo json_encode($selectCustomers);
        exit;
    }

    public function customerLikeAction() {
        $modelCustomer = new Model_Customer();
        $loggedUser = CheckAuth::getLoggedUser();

        $filters = array();
        //check if loged user is contractor
        if (!CheckAuth::checkCredential(array('CanSeeAllBooking'))) {
            if (CheckAuth::checkCredential(array('CanSeeOnlyHisBooking'))) {
                if (CheckAuth::checkCredential(array('CanSeeAssignedBooking'))) {
                    $filters['created_by'] = $loggedUser['user_id'];
                }
            }
        }
        $filters['email'] = $this->request->getParam('customer-email');
        $filters['phone'] = $this->request->getParam('customer-phone');
        $filters['mobile'] = $this->request->getParam('customer-mobile');

        //get all customer
        $customers = $modelCustomer->getAll($filters);
        $this->view->customers = $customers;

        $asJson = $this->request->getParam('asJson', 0);
        $this->view->asJson = $asJson;

        echo $this->view->render('common/customerLike.phtml');
        exit;
    }

    public function customerMobileLikeAction() {
        $modelCustomer = new Model_Customer();
        $loggedUser = CheckAuth::getLoggedUser();

        $filters = array();
        //check if loged user is contractor
        if (!CheckAuth::checkCredential(array('CanSeeAllBooking'))) {
            if (CheckAuth::checkCredential(array('CanSeeOnlyHisBooking'))) {
                if (CheckAuth::checkCredential(array('CanSeeAssignedBooking'))) {
                    $filters['created_by'] = $loggedUser['user_id'];
                }
            }
        }
        $filters['mobile'] = $this->request->getParam('customer-like');
        //get all customer
        $customers = $modelCustomer->getAll($filters);
        $this->view->customers = $customers;

        $asJson = $this->request->getParam('asJson', 0);
        $this->view->asJson = $asJson;

        echo $this->view->render('common/customerLike.phtml');
        exit;
    }

    public function checkBookingStatusAction() {

        //load model
        $modelBookingStatus = new Model_BookingStatus();
        $modelBooking = new Model_Booking();

        //params
        $statusId = $this->request->getParam('status_id');
        $start = $this->request->getParam('start');
        $end = $this->request->getParam('end');
        $bookingId = $this->request->getParam('booking_id');

        /// get booking to get old status 
        $booking = $modelBooking->getById($bookingId);
        $oldStatusId = $booking['status_id'];

        // get status by name
        $completed = $modelBookingStatus->getByStatusName('COMPLETED');
        $faild = $modelBookingStatus->getByStatusName('FAILED');
        $on_hold = $modelBookingStatus->getByStatusName('ON HOLD');
        $quoted = $modelBookingStatus->getByStatusName('QUOTED');
        $inProcess = $modelBookingStatus->getByStatusName('IN PROGRESS');
        $toDo = $modelBookingStatus->getByStatusName('TO DO');

        $isSuccess = true;
        $message = '';
        $body = '';

        $isSuccess = $modelBooking->checkIfCanChangeBookingStatus($bookingId, $statusId, $message);

        /////By Islam get answers of the questions 
        //$modelBookingStatus = new Model_BookingStatus();
        //$failedStatus = $modelBookingStatus->getByStatusName('FAILED');

        $modelUpdateBookingQuestionAnswer = new Model_UpdateBookingQuestionAnswer();
        $questions = $modelUpdateBookingQuestionAnswer->getByBookingIdAndStatusId($statusId, $bookingId);
        $this->view->questions = $questions;
        ///if there are no questions we should hide questions div
        $hideQuestionDiv = 0;
        if (empty($questions)) {
            $hideQuestionDiv = 1;
        }
        /////
        if ($isSuccess) {
            if ($statusId != $inProcess['booking_status_id'] && $statusId != $toDo['booking_status_id']) {
                if ($completed['booking_status_id'] == $statusId) {
                    //get all Product for drop dawn menu
                    $modelProduct = new Model_Product();
                    $products = $modelProduct->getAll(array(), "product ASC");
                    $this->view->products = $products;
                    $this->view->start = date('d-m-Y h:i A', strtotime($start));
                    $this->view->end = date('d-m-Y h:i A', strtotime($end));

                    //  $body = $this->view->render('common/bookingStatusCompletedForm.phtml');
                } elseif ($faild['booking_status_id'] == $statusId) {
                    //get all Product for drop dawn menu
                    $modelProduct = new Model_Product();
                    $products = $modelProduct->getAll(array(), "product ASC");
                    $this->view->products = $products;
                    $this->view->start = date('d-m-Y h:i A', strtotime($start));
                    $this->view->end = date('d-m-Y h:i A', strtotime($end));
                    $this->view->bookingStatus = $modelBookingStatus->getById($statusId);

                    $body = $this->view->render('common/bookingStatusFaildForm.phtml');
                } elseif ($on_hold['booking_status_id'] == $statusId) {
                    $this->view->bookingStatus = $modelBookingStatus->getById($statusId);
                    $body = $this->view->render('common/bookingStatusOnHoldForm.phtml');
                } elseif ($quoted['booking_status_id'] == $statusId) {
                    ///check if the old status is to_do we should view why text area else we shouldn't
                    if ($oldStatusId == $toDo['booking_status_id']) {
                        $this->view->showWhy = 1;
                    } else {
                        $this->view->showWhy = 0;
                    }
                    $this->view->bookingStatus = $modelBookingStatus->getById($statusId);
                    $body = $this->view->render('common/bookingStatusQuotedForm.phtml');
                } else {
                    $this->view->bookingStatus = $modelBookingStatus->getById($statusId);
                    $body = $this->view->render('common/bookingStatusWhyForm.phtml');
                }
            }
        }

        $json = array(
            'is_success' => $isSuccess,
            'msg' => $message,
            'body' => $body,
            'hideQuestionDiv' => $hideQuestionDiv
        );

        echo json_encode($json);
        exit;
    }

    public function setMessageAction() {
        $type = $this->request->getParam('type', 'error');
        $message = $this->request->getParam('message', '');

        echo $message;
        echo $type;

        $this->_helper->flashMessenger->addMessage(array('type' => $type, 'message' => $message));
        exit;
    }

    public function getBookingAddressAction() {
        $customerId = $this->request->getParam('customer_id', 0);
        $type = $this->request->getParam('type', 'booking');

        $modelCustomer = new Model_Customer();
        $customer = $modelCustomer->getById($customerId);

        $this->view->customer = $customer;
        $this->view->type = $type;

        echo $this->view->render('common/bookingAddress.phtml');
        exit;
    }

    ////By islam to set customer state as booking state
    public function getBookingStateAction() {
        $customerId = $this->request->getParam('customer_id', 0);

        $modelCustomer = new Model_Customer();
        $customer = $modelCustomer->getById($customerId);
        $data = array(
            'city_id' => $customer['city_id'],
            'state' => $customer['state']
        );
        echo json_encode($data);
        exit;
    }

    public function getInquiryAddressAction() {
        $customerId = $this->request->getParam('customer_id', 0);

        $modelCustomer = new Model_Customer();
        $customer = $modelCustomer->getById($customerId);

        $this->view->customer = $customer;

        echo $this->view->render('common/bookingAddress.phtml');
        exit;
    }

    public function inquiryTypeAttributesAction() {
        $inquiryTypeId = $this->request->getParam('inquiry_type_id', 0);

        $modelInquiryTypeAttribute = new Model_InquiryTypeAttribute();
        $inquiryTypeAttribute = $modelInquiryTypeAttribute->getAttributeByInquiryTypeId($inquiryTypeId);

        if ($inquiryTypeAttribute) {
            foreach ($inquiryTypeAttribute as $key => $attribute) {
                $html = '';
                if ($key == 0) {
                    $html .= '<div>';
                } else {
                    $html .= '<div style="margin-top:5px;">';
                }
                $html .= '<div class="calender_slct_customer_txt">' . $attribute['attribute_name'] . '</div>';
                $html .= '<div class="calender_slct_customer_dropdown">';
                $html .= drowInquiryTypeAttribute($attribute);
                $html .= '</div>';
                $html .= '<div style="clear: both;"></div>';
                $html .= '</div>';

                echo $html;
            }
            exit;
        } else {
            echo 0;
            exit;
        }
    }

    public function setCompanySessionAction() {

        $loggedUser = CheckAuth::getLoggedUser();

        $modelUserCompanies = new Model_UserCompanies();
        $userCompanies = $modelUserCompanies->getCompaniesByUserId($loggedUser['user_id']);

        if (CheckAuth::checkCredential(array('canHandleAllCompanies')) || (count($userCompanies) > 1)) {
            //
            // get request parameters
            //
            $companySession = $this->request->getParam('company_session', 0);

            if (!$companySession) {
                $companySession = CheckAuth::getCompanySession();
            }

            CheckAuth::setCompanySession($companySession);
        }

        echo 1;
        exit;
    }

    public function setDashboardStatusSessionAction() {

        if (CheckAuth::checkCredential(array('canSeeAllBookingStatistics'))) {
            //
            // get request parameters
            //
            $dashboardSession = $this->request->getParam('dashboard_status', 0);

            CheckAuth::setDashboardStatusSession($dashboardSession);
        }

        echo 1;
        exit;
    }

    public function saveAsXlsAction() {

        $loggedUser = CheckAuth::getLoggedUser();
        $company_id = CheckAuth::getCompanySession();


        $tempHtml1 = $this->request->getParam('html1', '');
        $tempHtml2 = $this->request->getParam('html2', '');
        $tempHtml3 = $this->request->getParam('html3', '');
        $tempHtml4 = $this->request->getParam('html4', '');
        $html = $tempHtml1 . $tempHtml2 . $tempHtml3 . $tempHtml4;



        $type = $this->request->getParam('type', 'general');
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        $validation = false;

        switch ($type) {

            case 'sales_by_customer':
                if (isset($filters['customer_id']) && $filters['customer_id']) {
                    $validation = true;
                }
                break;

            case 'reconciliation':
                if (isset($filters['contractor_id']) && $filters['contractor_id']) {
                    $validation = true;
                }
                break;

            default :
                $validation = true;
                break;
        }

        if (empty($html) || !$validation) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => 'Please Select Filter.'));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }


        $html = urldecode($html);


//        $html = str_replace("\n", " ", $html);
//        $html = str_replace("\t", " ", $html);
//        $html = preg_replace('#[\s]{2,}#', '', trim($html));
//        $html = preg_replace('#\<div(.*?)\>#', "\n\t", $html);
//        $html = preg_replace('#\<tr(.*?)\>#', "\n", $html);
//        $html = preg_replace('#\<td(.*?)\>#', "\t", $html);
//        $html = preg_replace('#\<table(.*?)\>#', '', $html);
//        $html = preg_replace('#\<tbody(.*?)\>#', '', $html);
//        $html = preg_replace('#\<b(.*?)\>#', '', $html);
//
        $html = str_replace('<br>', "", $html);
//        $html = str_replace('</div>', '', $html);
//        $html = str_replace('</tr>', '', $html);
//        $html = str_replace('</td>', '', $html);
//        $html = str_replace('</table>', '', $html);
//        $html = str_replace('</tbody>', '', $html);
//        $html = str_replace('</b>', '', $html);
//        $html = str_replace('&amp;', '&', $html);
//
        //$html = preg_replace('#[\n]{2,}#', '', trim($html));
        //$html = str_replace("\n\t", "\n", $html);

        $pos = strpos($html, '<img');

        while ($pos !== false) {
            //echo 'in WHILE';
            // string needle NOT found in haystack
            $startOfImgTag = strpos($html, '<img');
            $endOfImgTag = strpos($html, '>');
            $str1 = substr($html, $startOfImgTag, $endOfImgTag);
            //$str2 = substr($html,$endOfImgTag,strlen($html)-1);
            $html = str_replace($str1, "", $html);
            $pos = strpos($html, '<img');
        }
        //echo $html;

        $html = str_replace('Delete Payment', '', $html);
        $html = str_replace('open</a>', '</a>', $html);
        $html = str_replace('close</a>', '</a>', $html);
        /* $pos = strpos($html,'<div style="padding-top: 5px;" id="unpaid_link_');

          while($pos !== false) {
          //echo 'delete';
          // string needle NOT found in haystack
          $startOfDeletePaymentTag = strpos($html, '<div style="padding-top: 5px;" id="unpaid_link_');
          //$endOfDeletePaymentTag = strpos($html, '</div>');
          $str1 = substr($html,$startOfDeletePaymentTag,strlen($html)-1);
          //echo 'str1 Berore'.$str1;
          $endOfDeletePaymentTag = strpos($str1, '</div>');
          $str1 = substr($str1,0,$endOfDeletePaymentTag);
          //echo 'str1'.$str1;
          //$str2 = substr($html,$endOfImgTag,strlen($html)-1);
          $html = str_replace($str1, "", $html);
          $pos = strpos($html,'<div style="padding-top: 5px;" id="unpaid_link_');
          } */



        $db_params = array();
        $db_params['report_type'] = $type;
        $db_params['created'] = time();
        $db_params['filters'] = serialize($filters);
        $db_params['company_id'] = $company_id;
        $db_params['user_id'] = $loggedUser['user_id'];
        $db_params['xls_body'] = $html;

        $modelReport = new Model_Report();
        $report_id = $modelReport->insert($db_params);

        $dir = get_config('report_file') . "/{$type}";

        //check if file exists or not
        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }

        $fileName = "{$dir}/{$report_id}.xls";

        $mode = 'x+';
        if (file_exists($fileName)) {
            $mode = 'w+';
        }

        $fhandler = fopen($fileName, $mode);
        fwrite($fhandler, $html);
        fclose($fhandler);

        header("Content-type: text/html");
        header("Content-Disposition: attachment; filename={$type}_report.xls");
        header("Pragma: no-cache");
        header("Expires: 0");
        print "$html";
        exit;
    }

    public function getRelatedAddressAction() {

        $unit_lot_number = trim($this->request->getParam('unit_lot_number', ''));
        $street_number = trim($this->request->getParam('street_number', ''));
        $street_address = trim($this->request->getParam('street_address', ''));
        $suburb = trim($this->request->getParam('suburb', ''));
        $state = trim($this->request->getParam('state', ''));
        $postcode = trim($this->request->getParam('postcode', ''));
        $stpartdate = $this->request->getParam('stpartdate', '');
        $stparttime = $this->request->getParam('stparttime', '00:00:00');
        $booking_id = $this->request->getParam('booking_id', 0);
        $cityId = $this->request->getParam('city_id', 0);
        $order_by = $this->request->getParam('order_by', 'distance');
        $method = $this->request->getParam('method', 'ASC');

        $address = array();
        $address['unit_lot_number'] = $unit_lot_number;
        $address['street_number'] = $street_number;
        $address['street_address'] = $street_address;
        $address['suburb'] = $suburb;
        $address['state'] = $state;
        $address['postcode'] = $postcode;

        $filters = array();
        $filters['booking_start'] = date('Y-m-d H:i:s', time());

        if ($booking_id) {
            $filters['not_booking_id'] = $booking_id;
        }
        if ($cityId) {
            $filters['city_id'] = $cityId;
        }

        $modelBookingAddress = new Model_BookingAddress();
        $modelBooking = new Model_Booking();

        if ($_SERVER['REMOTE_ADDR'] == '176.58.76.6') {
            $relatedAddresses = $modelBookingAddress->getAllRelatedAddress2($address, $filters, "$order_by $method");
        } else {
            $relatedAddresses = $modelBookingAddress->getAllRelatedAddress($address, $filters, "$order_by $method");
        }


        $modelBooking->fills($relatedAddresses, array('contractors', 'booking', 'address'));


        $this->view->relatedAddresses = $relatedAddresses;
        $this->view->order_by = $order_by;
        $this->view->method = $method;
        $this->view->unit_lot_number = $unit_lot_number;
        $this->view->street_number = $street_number;
        $this->view->street_address = $street_address;
        $this->view->suburb = $suburb;
        $this->view->state = $state;
        $this->view->postcode = $postcode;
        $this->view->stpartdate = $stpartdate;
        $this->view->stparttime = $stparttime;
        $this->view->booking_id = $booking_id;
        $this->view->city_id = $cityId;
        $this->view->bookingAddress = $address;

        echo $this->view->render('common/relatedAddresses.phtml');
        exit;
    }

    public function findAvailabilityAction() {

        $unit_lot_number = trim($this->request->getParam('unit_lot_number', ''));
        $street_number = trim($this->request->getParam('street_number', ''));
        $street_address = trim($this->request->getParam('street_address', ''));
        $suburb = trim($this->request->getParam('suburb', ''));
        $state = trim($this->request->getParam('state', ''));
        $postcode = trim($this->request->getParam('postcode', ''));

        $address = array();
        $address['unit_lot_number'] = $unit_lot_number;
        $address['street_number'] = $street_number;
        $address['street_address'] = $street_address;
        $address['suburb'] = $suburb;
        $address['state'] = $state;
        $address['postcode'] = $postcode;


        $booking_id = $this->request->getParam('booking_id', 0);
        $stpartdate = $this->request->getParam('stpartdate', '');
        $cityId = $this->request->getParam('city_id', 0);
        $service_ids = $this->request->getParam('services', array());
        $contractorId = $this->request->getParam('contractor_id', 0);
        $row_num = $this->request->getParam('row_num', 0);


        // Load Model
        $modelContractorService = new Model_ContractorService();
        $modelUser = new Model_User();
        $modelBooking = new Model_Booking();
        $modelBookingMultipleDays = new Model_BookingMultipleDays();


        // check service if is serliaze when return from tb_show
        $service_ids = $this->is_serial($service_ids) ? unserialize($service_ids) : $service_ids;

        // Remove Redundancy
        $removeRedundancy = array();
        foreach ($service_ids as $service_id) {
            $removeRedundancy[$service_id] = $service_id;
        }
        $service_ids = $removeRedundancy;

        $filters = array();

        if (!empty($stpartdate)) {
            $filters['booking_in_this_day'] = $stpartdate;
        } else {
            $filters['booking_in_this_day'] = date('Y-m-d', time());
        }

        $contractor_ids = array();
        if (CheckAuth::checkCredential(array('canSeeAllBooking'))) {
            if ($contractorId) {
                //Selected contractor from nearest booking
                $contractor_ids[$contractorId] = $contractorId;
            } else {
			   

                if (!empty($service_ids)) {
                    foreach ($service_ids as $service_id) {
                        if ($cityId) {
                            $ContractorServices = $modelContractorService->getContractorByServiceIdAndCityId($service_id, $cityId);
                        } else {
                            $ContractorServices = $modelContractorService->getContractorByServiceId($service_id);
                        }

                        foreach ($ContractorServices as $ContractorService) {
                            $contractor_ids[$ContractorService['contractor_id']] = $ContractorService['contractor_id'];
                        }
                    }
                }else if($cityId){
				   $ContractorServices = $modelContractorService->getContractorByCityId($cityId);
				   foreach ($ContractorServices as $ContractorService) {
                            $contractor_ids[$ContractorService['contractor_id']] = $ContractorService['contractor_id'];
                        }						
				}else {		   
                    $contractors = $modelUser->getAllContractor();
                    foreach ($contractors as $contractor) {
                        $contractor_ids[$contractor['user_id']] = $contractor['user_id'];
                    }
                }
            }
        } else {
            $loggedUser = CheckAuth::getLoggedUser();

            $contractor_ids[$loggedUser['user_id']] = $loggedUser['user_id'];
        }

        $availableContractors = array();
        foreach ($contractor_ids as $contractor_id) {

            $all_booking = array();

            $modelGoogleCalendarEvent = new Model_GoogleCalendarEvent();
            $googleEvent = $modelGoogleCalendarEvent->getGoogleEventByContractorIdAndDateRange($contractor_id, $filters['booking_in_this_day']);
            $all_booking = array_merge($all_booking, $googleEvent);

            $filters['contractor_id'] = $contractor_id;
            $user = $modelUser->getById($contractor_id);

            $bookings = $modelBooking->getAll($filters);
            $all_booking = array_merge($all_booking, $bookings);

            $bookingMultipleDays = $modelBookingMultipleDays->getAll($filters);
            $all_booking = array_merge($all_booking, $bookingMultipleDays);

            $availableContractors[] = array(
                'contractor_id' => $contractor_id,
                'username' => $user['username'],
                'booking' => $all_booking
            );
        }

        $this->view->availableContractors = $availableContractors;
        $this->view->booking_id = $booking_id;
        $this->view->city_id = $cityId;
        $this->view->service_ids = serialize($service_ids);
        $this->view->filter_date = $filters['booking_in_this_day'];
        $this->view->row_num = $row_num;
        $this->view->bookingAddress = $address;

        if ($contractorId) {
            // nearest booking view(ContractorAvailability)
            echo $this->view->render('common/contractorAvailability.phtml');
        } else {
            echo $this->view->render('common/findAvailability.phtml');
        }
        exit;
    }

    public function is_serial($data) {
        $data = @unserialize($data);
        if ($data === false) {
            return false;
        } else {
            return true;
        }
    }

    public function fillCannedResponseAction() {


        //
        //get request param
        //
        $id = $this->request->getParam('id', 0);
        $type_id = $this->request->getParam('type_id');
        $type = $this->request->getParam('type');
        $modelCannedResponses = new Model_CannedResponses();
        $cannedResponse = $modelCannedResponses->getById($id);
        $model_booking = new Model_Booking();
        $model_tradingName = new Model_TradingName();
        $body = $cannedResponse['body'];
        $subject = $cannedResponse['subject'];
        if (!empty($type) && !empty($type_id)) {
            if ($type == 'inquiry') {
                $model_inquiry = new Model_Inquiry();
                $inquiry_data = $model_inquiry->getById($type_id);
                $model_originalInquiry = new Model_OriginalInquiry();
                if ($inquiry_data['original_inquiry_id'] == 0) {
                    $tradingNames = $model_tradingName->getById($inquiry_data['trading_name_id']);
                } else {
                    $OriginalInquiryData = $model_originalInquiry->getById($inquiry_data['original_inquiry_id']);
                    $tradingNames = $model_tradingName->getTradingNameByWebsite($OriginalInquiryData['website']);
                }
            } else if ($type == 'booking') {
                $booking_data = $model_booking->getById($id);

                $tradingNames = $model_tradingName->getById($booking_data['trading_name_id']);
            } else if ($type == 'estimate') { 
                $model_estimate = new Model_BookingEstimate();
                $estimate_data = $model_estimate->getById($id);
                $booking_estimate_data = $model_booking->getById($estimate_data['booking_id']);
                $tradingNames = $model_tradingName->getById($booking_estimate_data['trading_name_id']);
            }
            $add_photo_link = 'http://cm.tilecleaners.com.au/addPhoto/' . $type . '/' . $type_id;
            $subject = str_replace('{trading_name}', $tradingNames['trading_name'], $subject);
            $body = str_replace("{add_photo_link}", $add_photo_link, $body);
            $body = str_replace("{type}", $type, $body);
            $body = str_replace("{trading_name}", $tradingNames['trading_name'], $body);
            $body = str_replace("{trading_name_website}", $tradingNames['website_url'], $body);
            $body = str_replace("{trading_name_phone}", $tradingNames['phone'], $body);
        }


        echo json_encode(array('body' => $body, 'subject' => $subject));
        exit;
    }

    //added by mona

    public function getContractorsByStatusAction() {
        $status = $this->request->getParam('status', 'ALL');

        $modelUser = new Model_User();
        $contracotrs = $modelUser->getContractorsByStatus($status);
        $results = array();

        foreach ($contracotrs as &$contracotr) {
            $user_id = (int) $contracotr['user_id'];
            $modelUser->fill($contracotr, array('contractor_info'));
            $results[$user_id] = $contracotr['contractor_name'];
        }
        echo json_encode($results);
        exit;
    }

    public function getTabContentAction() {

        //load model
        $modelBookingStatus = new Model_BookingStatus();
        $modelBooking = new Model_Booking();
        $modelUser = new Model_User();
        $modelAuthRole = new Model_AuthRole();
        $modelEmailLog = new Model_EmailLog();
        $modelContractorInfo = new Model_ContractorInfo();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();

        //params
        $tab = $this->request->getParam('tab');
        $bookingId = $this->request->getParam('bookingId');
        $invoiceId = $this->request->getParam('invoiceId');
        $estimateId = $this->request->getParam('estimateId');
        $inquiryId = $this->request->getParam('inquiryId');



        $bookingEmailLogs = array();
        $invoiceEmailLogs = array();
        $estimateEmailLogs = array();
        $inquiryEmailLogs = array();
        $complaintEmailLogs = array();

        //EmailLog
        $bookingEmailLogs = $modelEmailLog->getAll(array('type' => 'booking', 'reference_id' => $bookingId), 'email_log_id DESC');
        if ($invoiceId) {
            $invoiceEmailLogs = $modelEmailLog->getAll(array('type' => 'invoice', 'reference_id' => $invoiceId), 'email_log_id  DESC');
        }
        if ($estimateId) {
            $estimateEmailLogs = $modelEmailLog->getAll(array('type' => 'estimate', 'reference_id' => $estimateId), 'email_log_id  DESC');
        }
        if ($inquiryId) {
            $inquiryEmailLogs = $modelEmailLog->getAll(array('type' => 'inquiry', 'reference_id' => $inquiryId), 'email_log_id  DESC');
        }

        $this->view->bookingEmailLogs = $bookingEmailLogs;
        $this->view->invoiceEmailLogs = $invoiceEmailLogs;
        $this->view->estimateEmailLogs = $estimateEmailLogs;
        $this->view->inquiryEmailLogs = $inquiryEmailLogs;
        //EmailLog Gmail
        $contractors = $modelContractorServiceBooking->getContractorIdsByBookingId($bookingId);
        $customer_emails = $modelBooking->getCustomerByBookingId($bookingId);
        $allGmailEmails = array();
        ///get emails from inquiry email
        $model = new Model_GmailServiceAccount(1);
        $emails = $model->getAllEmails();
        $service = $model->getService();
        foreach ($emails as $email) {
            $messageId = $email->getId(); // Grab first Message
            $optParamsGet = array();
            $optParamsGet['format'] = 'full'; // Display message in payload
            $message = $service->users_messages->get('me', $messageId, $optParamsGet);
            $messagePayload = $message->getPayload();
            $headers = $message->getPayload()->getHeaders();
            $headerArry['id'] = $messageId;
            foreach ($headers as $header) {
                if ('Date' == $header->name) {
                    $headerArry['date'] = $header->value;
                } elseif ('From' == $header->name) {
                    $headerArry['from'] = $header->value;
                } elseif ('To' == $header->name) {
                    $headerArry['to'] = $header->value;
                } elseif ('Subject' == $header->name) {
                    $headerArry['sub'] = $header->value;
                } elseif ('Delivered-To' == $header->name) {
                    $headerArry['deliver'] = $header->value;
                }
            }
            if (
                    (!empty($customer_emails['email1']) && strpos($headerArry['from'], $customer_emails['email1'])) || (!empty($customer_emails['email2']) && strpos($headerArry['from'], $customer_emails['email2'])) || (!empty($customer_emails['email3']) && strpos($headerArry['from'], $customer_emails['email3'])) || (!empty($customer_emails['email1']) && strpos($headerArry['to'], $customer_emails['email1'])) || (!empty($customer_emails['email2']) && strpos($headerArry['to'], $customer_emails['email2'])) || (!empty($customer_emails['email3']) && strpos($headerArry['to'], $customer_emails['email3']))
            ) {
                $allGmailEmails[] = $headerArry;
            }
        }
        /////////////////////////
        foreach ($contractors as $contractor) {
            if ($contractor['contractor_id'] != 1) {
                $model = new Model_GmailServiceAccount($contractor['contractor_id']);
                $emails = $model->getAllEmails();
                $service = $model->getService();
                foreach ($emails as $email) {
                    $messageId = $email->getId(); // Grab first Message
                    $optParamsGet = array();
                    $optParamsGet['format'] = 'full'; // Display message in payload
                    $message = $service->users_messages->get('me', $messageId, $optParamsGet);
                    $messagePayload = $message->getPayload();
                    $headers = $message->getPayload()->getHeaders();
                    $headerArry['id'] = $messageId;

                    foreach ($headers as $header) {
                        if ('Date' == $header->name) {
                            $headerArry['date'] = $header->value;
                        } elseif ('From' == $header->name) {
                            $headerArry['from'] = $header->value;
                        } elseif ('To' == $header->name) {
                            $headerArry['to'] = $header->value;
                        } elseif ('Subject' == $header->name) {
                            $headerArry['sub'] = $header->value;
                        } elseif ('Delivered-To' == $header->name) {
                            $headerArry['deliver'] = $header->value;
                        }
                    }
                    if (
                            (!empty($customer_emails['email1']) && strpos($headerArry['from'], $customer_emails['email1'])) || (!empty($customer_emails['email2']) && strpos($headerArry['from'], $customer_emails['email2'])) || (!empty($customer_emails['email3']) && strpos($headerArry['from'], $customer_emails['email3'])) || (!empty($customer_emails['email1']) && strpos($headerArry['to'], $customer_emails['email1'])) || (!empty($customer_emails['email2']) && strpos($headerArry['to'], $customer_emails['email2'])) || (!empty($customer_emails['email3']) && strpos($headerArry['to'], $customer_emails['email3']))
                    ) {
                        $allGmailEmails[] = $headerArry;
                    }
                }
            }
        }

        $this->view->allGmailEmails = $allGmailEmails;

        $isSuccess = true;
        $message = '';
        $body = '';

        $body = $this->view->render('common-views/emails.phtml');
        $json = array(
            'is_success' => $isSuccess,
            'msg' => $message,
            'body' => $body
        );

        echo json_encode($json);
        exit;
    }

    public function getFloorDropdownAction() {
        $service_id = $this->request->getParam("service_id");
        $model_attributeListValue = new Model_AttributeListValue();
        $model_attribute = new Model_Attributes();

        $floorData = $model_attributeListValue->getAllFloorByServiceID($service_id);
        $data = "<select class='form-control' name='floor_id'>";
        foreach ($floorData as $key => $floor) {
            $data .="<option value='" . $floor['attribute_value_id'] . "'>" . $floor['attribute_value'] . "</option>";
        }
        $data .="</select>";
        echo $data;
        exit;
    }

}
