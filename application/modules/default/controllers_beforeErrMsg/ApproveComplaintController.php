<?php

class ApproveComplaintController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
    
	}
	
	
	public function unapprovedAction() {
	  
	    CheckAuth::checkLoggedIn();
        CheckAuth::checkPermission(array('canApproveComplaint'));
		
		$orderBy = $this->request->getParam('sort', 'created');
        $sortingMethod = $this->request->getParam('method', 'desc');
        $currentPage = $this->request->getParam('page', 1);
		
		$pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];
		
	    $filters['is_approved'] = 0;
		$modelComplaint = new Model_Complaint();
		$unapprovedComplaint = $modelComplaint->getAll($filters,"{$orderBy} {$sortingMethod}", $pager);
		$this->view->countUnapprovedComplaint = count($unapprovedComplaint);
		$this->view->unapprovedComplaint = $unapprovedComplaint;
		
		$this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
		
		
	
	}
	
	
	public function approvedComplaintAction(){
	  
        CheckAuth::checkLoggedIn();
        CheckAuth::checkPermission(array('canApproveComplaint'));

        		
		
		$id = $this->request->getParam('id', 0);
		$modelComplaint = new Model_Complaint();
        $complaint = $modelComplaint->getById($id);
		if($complaint){
		  $this->view->old_complaint = $complaint;
		}
		
		$modelComplaintTemp = new Model_ComplaintTemp();
		$complaintTemp  = $modelComplaintTemp->getByComplaintId($id);
		if($complaintTemp){
		 $this->view->new_complaint = $complaintTemp;
		}
		
		if(!($complaintTemp)){
		  
		  $this->_redirect($this->router->assemble(array('id' =>$id), 'complaintView'));
		  
		}
		
		$this->view->complaintId = $id;
		$isValid = true;
		$status = $this->request->getParam('status_id');
		if(empty($status)){
		  $isValid = false;
		}
		
		
		if ($this->request->isPost()) {
		  if ($isValid) {
		    $status = $this->request->getParam('status_id', 'old_status');
			if ($status == 'new_status' && $complaintTemp) {
			

                    $newComplaint = array(
                        'complaint_status' => $complaintTemp['complaint_status'],
						'is_approved'=>1
                    );
					
				
                    $modelComplaint->updateById($id, $newComplaint);
					
				 if (isset($complaintTemp['id']) && $complaintTemp['id']) {
                    $modelComplaintTemp->deleteById($complaintTemp['id']);
                   }

                }else if ($status == 'old_status' && $complaint){
				  
                    $modelComplaint->updateById($id, array('is_approved'=>1));
                    $modelComplaintTemp->deleteById($complaintTemp['id']);
				} 
			
		        $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "The selected complaint have been approved."));
                $this->_redirect($this->router->assemble(array('id' =>$id), 'complaintView'));
            } else {
                $this->view->messages[] = array('type' => 'error', 'message' => 'Please select ALL items to approve');
            }
		
		}

		
	}
	
}
?>