<?php

class ContractorServiceController extends Zend_Controller_Action {

    private $request;
    private $router;
    private $contractor_id;

    public function init() {
        parent::init();
        
		CheckAuth::checkLoggedIn();
		
		if ('contractor' != CheckAuth::getRoleName()) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }

		$this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
        
		
		$loggedUser = CheckAuth::getLoggedUser();

        //
        // get data list
        //
        $contractorInfoObj = new Model_ContractorInfo();
        $contractorInfo = $contractorInfoObj->getByContractorId($loggedUser['user_id']);
		$this->contractor_id = $loggedUser['user_id'];

        BreadCrumbs::setLevel(4, 'Contractor Services');
    }

    /**
     * Items list action
     */
    public function indexAction() {


        $orderBy = $this->request->getParam('sort', 'contractor_service_id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());
        
        if($filters){
            foreach ($filters as &$filter){
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];


        //
        //get user email by id
        //
        $filters['contractor_id'] = $this->contractor_id;


        //
        // get data list
        //
        $contractorServiceObj = new Model_ContractorService();
        $this->view->data = $contractorServiceObj->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);

        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
        $this->view->contractor_id = $this->contractor_id;
    }

    /**
     * Add new item action
     */
    public function addAction(){

        //
        // get request parameters
        //
        $serviceIds = $this->request->getParam('service_id');
        $router = Zend_Controller_Front::getInstance()->getRouter();

        //
        // init action form
        //
		$filters['contractor_id'] = $this->contractor_id;
        $contractorServiceObj = new Model_ContractorService();
        $old_services = $contractorServiceObj->getAll($filters);
        $form = new Form_ContractorService(array('contractor_id' => $this->contractor_id , 'old_services'=>$old_services));

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $contractorServiceObj = new Model_ContractorService();

                $success = false;
                foreach ($serviceIds as $serviceId) {
                    $data = array(
                        'service_id' => $serviceId,
                        'contractor_id' => $this->contractor_id
                    );

                    if (!$contractorServiceObj->getByContractorIdAndServiceId($this->contractor_id, $serviceId)) {
                        $success = $contractorServiceObj->insert($data);
                    }
                }

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Contractor Service"));
                }
                //$this->_redirect($this->router->assemble(array(), 'settingsUserEmailList'));
                echo 1;
                exit;
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('contractor-service/add_edit.phtml');
        exit;
    }

    public function deleteAction() {

       
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
        if ($id) {
            $ids[] = $id;
        }

        $contractorServiceObj = new Model_ContractorService();
        foreach ($ids as $id) {
		    $contractorServic = $contractorServiceObj->getById($id);
            if ($this->contractor_id == $contractorServic['contractor_id']) {
                $contractorServiceObj->deleteById($id);
            }
        }
        $this->_redirect($this->router->assemble(array(), 'contractorServiceList'));
    }

	
	public function contractorServicesAction() {

       
        $orderBy = $this->request->getParam('sort', 'contractor_service_id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        //$currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());
        
        if($filters){
            foreach ($filters as &$filter){
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
      /*
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

*/
        //
        //get user email by id
        //
        $filters['contractor_id'] = $this->contractor_id;


        //
        // get data list
        //
        $contractorServiceObj = new Model_ContractorService();
        $this->view->data = $contractorServiceObj->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);

        //
        // set view params
        //
       // $this->view->currentPage = $currentPage;
       // $this->view->perPage = $pager->perPage;
       // $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
        $this->view->contractor_id = $this->contractor_id;
		  echo $this->view->render('contractor-service/contractorservices.phtml');
        exit;
    }
}

