<?php

class RateController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
    }

    /**
     * Items list action
     */
    public function indexAction() {
//        echo "salim";
//        exit;
    }

    public function customerRateAction() {

        $model_ratingTag = new Model_RatingTag();
        $model_contractorRate = new Model_ContractorRate();
        $ratingTag = array();
        $rating_info = array();
        $model_ratingInfo = new Model_RatingInfo();
        $item_id = $this->request->getParam('item_id');
        $item_type = $this->request->getParam('type');

//        $item_id = $this->request->getParam("booking_id");
//        var_dump($bookingId);
//        exit
//        $item_type = $this->request->getParam("type");
//        if ($item_type == 'booking') {

        $model_contractorServiceBooking = new Model_ContractorServiceBooking();
        $contractors = $model_contractorServiceBooking->getContractorsDataByBookingId($item_id , 1);
         foreach ($contractors as $key => $contractor) {
            $rating_info[$contractor['contractor_id']] = $model_ratingInfo->getByContractorIdAndBookingId($contractor['contractor_id'], $item_id);
            $ratingTag[$contractor['contractor_id']] = $model_ratingTag->getAll($contractor['contractor_id'], 1, $item_id);
        }
      
        
        $this->view->item_id = $item_id;
        $this->view->contractors = $contractors;
        $this->view->ratingTag = $ratingTag;
        $this->view->rating_info = $rating_info;
        if ($this->request->isPost()) {
            $model_ratingInfo = new Model_RatingInfo();
            $rate = $this->request->getParam('rate');
            $tag_id = $this->request->getParam('tag_id');
            $contractor_id = $this->request->getParam('contractor_id');
            $item_id = $this->request->getParam('item_id');
            $item_type = $this->request->getParam('type');
            $loggedUser = CheckAuth::getLoggedUser();
            $rated_by = $loggedUser['user_id'];
            $role_id = $loggedUser['role_id'];
            $desc = $this->request->getParam('rate_desc');
            $rating_info_id = $this->request->getParam("rating_info_id", 0);
            $is_desc = $this->request->getParam("is_desc", 0);

            if (isset($is_desc) && ($is_desc == 1)) {
                $rate_info = array(
                    'contractor_id' => $contractor_id,
                    'item_id' => $item_id,
                    'desc' => $desc,
                );
                if ($rating_info_id) {
                    $status = $model_ratingInfo->updateById($rating_info_id, $rate_info);
                    if ($status) {
                        echo json_encode(array("rating_info_id" => $rating_info_id));
                        exit;
                    }
                } else {
                    $status = $model_ratingInfo->insert($rate_info);
                    if ($status) {
                        echo json_encode(array("rating_info_id" => $status));
                        exit;
                    }
                }
            } else {
            $rate_data = array(
                'contractor_id' => $contractor_id,
                'rated_by' => $rated_by,
                'rate' => $rate,
                'user_role' => $role_id,
                'created' => time(),
                'rating_tag_id' => $tag_id,
                'item_id' => $item_id,
                'item_type' => $item_type
            );

                if (!$rating_info_id) {
                    $rating_info_id = $model_ratingInfo->insert(array('contractor_id' => $contractor_id, 'item_id' => $item_id, 'desc' => $desc));
                } else {
                    $model_ratingInfo->updateById($rating_info_id, array('contractor_id' => $contractor_id, 'item_id' => $item_id, 'desc' => $desc));
                }
                $previousRate = $model_contractorRate->getByContractorIdAndRatedByAndItemIdAndTagId($contractor_id, $rated_by, $item_id, $tag_id);
                if (!empty($previousRate)) {
                    $success = $model_contractorRate->updateById($previousRate[0]['contractor_rate_id'], $rate_data);
                } else {
                    $success = $model_contractorRate->insert($rate_data);
                }
                if ($success) {
                    echo json_encode(array("rate" => $rate, "rating_info_id" => $rating_info_id));
                }
            }
            exit;
        }
    }

    public function customerRateEmailAction() {
        $model_booking = new Model_Booking();
        $id = $this->request->getParam('item_id', 0);
//        $type = $this->request->getParam('type');
     
        $model_tradingName = new Model_TradingName();

        $booking_data = $model_booking->getById($id);
        $customer_id = $booking_data['customer_id'];
        $default_page = $this->router->assemble(array('type'=> 'booking' ,'item_id' => $id), 'customerRate');
        $customer_model = new Model_Customer();
        $customer = $customer_model->getById($customer_id);
        $authRole_model = new Model_AuthRole();
        $auth = Zend_Auth::getInstance();
        $authStorge = $auth->getStorage();
        $role = $authRole_model->getRoleIdByName('Customer');
        $customer['role_id'] = $role;
        $user_id = $customer['customer_id'];
        $customer['user_id'] = $user_id;
        $customer['username'] = $customer['first_name'];
        $customer['default_page'] = $default_page;
        $customer_obj = (object) $customer;
        $authStorge->write($customer_obj);
        CheckAuth::redirectCustomer($default_page);
    }

}
