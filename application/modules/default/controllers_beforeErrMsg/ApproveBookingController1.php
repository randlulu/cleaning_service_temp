<?php

class ApproveBookingController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
		$this->view->sub_menu = 'approve-booking';
    
	}

    public function unapprovedAction() {
	

        //$this->view->sub_menu = 'approve-booking';
		//check login
        CheckAuth::checkLoggedIn();

        //check Auth for logged user
        CheckAuth::checkPermission(array('canApproveBooking'));

		
        //
        // load model
        //
        $modelBooking = new Model_Booking();
		////By islam
		$form = new Form_UnapprovedFilters(array('action_url' => $this->router->assemble(array(), 'unapproved'), 'type' => 'contractors_bookings_summary'));
        $this->view->form = $form;
		
		$filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }
		$filters['is_change'] = 1;
		
		$onlyMyBooking = $this->request->getParam('onlyMyBooking', false);
		$loggedUser = CheckAuth::getLoggedUser();
		$modelAuthRole = new Model_AuthRole();
		///////////test if the user is saler
		if($loggedUser['role_id']==$modelAuthRole->getRoleIdByName('sales') && $onlyMyBooking){
        
            $this->view->sub_menu = 'my_unapproved_booking';
			$filters['created_by'] = $loggedUser['user_id'];
			
			$unapprovedBooking = $modelBooking->getAll($filters);			
			
		}
		/////end
		else{
			$this->view->sub_menu = 'all_unapproved_booking';
			$unapprovedBooking = $modelBooking->getAll($filters);
		}
		
		/////get time of last update from contractor_service_booking_log
		$ContractorServiceBookingLogModel = new Model_ContractorServiceBookingLog();
		foreach($unapprovedBooking as &$booking){
			$updateDate = $ContractorServiceBookingLogModel->getLastLogByContractorAndBookingId($booking['booking_id']);
			$booking['update_date'] = $updateDate['MAX(log_created)'];
			$booking['username'] = $updateDate['username'];
			$booking['contractor_id'] = $updateDate['contractor_id'];
		}
		
		
        $this->view->unapprovedBooking = $unapprovedBooking;
        $this->view->countUnapprovedBooking = count($unapprovedBooking);
		$this->view->filters = $filters;
		
		
		
		
		
    }
	
	
    public function approvedServiceAction() {

        //
		//By Islam
		//check login
        CheckAuth::checkLoggedIn();

        //check Auth for logged user
        CheckAuth::checkPermission(array('canApproveBooking'));

        $this->view->main_menu = 'bookings';

        $bookingId = $this->request->getParam('id', 0);

        // check if the request come from approve payment
        $redirectToPayment = $this->request->getParam('redirect_to_payment', false);

        $this->view->redirect_to_payment = $redirectToPayment;


        //
        // load models
        //
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelContractorServiceBookingTemp = new Model_ContractorServiceBookingTemp();
        $modelServiceAttribute = new Model_ServiceAttribute();
        $modelServiceAttributeValueTemp = new Model_ServiceAttributeValueTemp();
        $modelServiceAttributeValue = new Model_ServiceAttributeValue();
        $modelBooking = new Model_Booking();
        $modelBookingAddress = new Model_BookingAddress();
        $modelBookingAddressTemp = new Model_BookingAddressTemp();
        $modelAttributeListValue = new Model_AttributeListValue();
        $modelAttributeType = new Model_AttributeType();
        $modelBookingInvoice = new Model_BookingInvoice();
        $booking = $modelBooking->getById($bookingId);


        //
        // check if Can Edit Booking
        //
        if (!$booking) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }


        //
        //get all service for this booking 
        //
        $bookingServices = $modelContractorServiceBooking->getByBookingId($bookingId);

        $oldBookingServices = array();
        if ($bookingServices) {
            foreach ($bookingServices as $bookingService) {
                $key = "{$bookingService['booking_id']}_{$bookingService['contractor_id']}_{$bookingService['service_id']}_{$bookingService['clone']}";
                $oldBookingServices[$key] = $bookingService;
            }
        }

        //
        //get all service temp for this booking 
        //
        $bookingServicesTemp = $modelContractorServiceBookingTemp->getByBookingId($bookingId);

        $newBookingServices = array();
        if ($bookingServicesTemp) {
            foreach ($bookingServicesTemp as $bookingServiceTemp) {
                $key = "{$bookingServiceTemp['booking_id']}_{$bookingServiceTemp['contractor_id']}_{$bookingServiceTemp['service_id']}_{$bookingServiceTemp['clone']}";
                $newBookingServices[$key] = $bookingServiceTemp;
            }
        }

        // merge old and new
        $allBookingServices = array_merge($oldBookingServices, $newBookingServices);

        $this->view->allBookingServices = $allBookingServices;
        $this->view->bookingId = $bookingId;
        $this->view->booking = $booking;

        // get old booking Address
        $oldAddress = $modelBookingAddress->getByBookingId($bookingId);
        $newAddress = $modelBookingAddressTemp->getByBookingId($bookingId);

        $this->view->oldAddress = $oldAddress;
        $this->view->newAddress = $newAddress;



        //
        //Validation
        //
        $isValid = true;
        foreach ($allBookingServices as $key => $bookingService) {
            $value = $this->request->getParam($key);
            if (empty($value) && $bookingService['is_change']) {
                $isValid = false;
            }
        }
        if ($newAddress) {
            $address = $this->request->getParam('address');
            if (empty($address)) {
                $isValid = false;
            }
        }
        if ($booking['total_discount'] != $booking['total_discount_temp']) {
            $totalDiscount = $this->request->getParam('total_discount');
            if (empty($totalDiscount)) {
                $isValid = false;
            }
        }
        if ($booking['call_out_fee'] != $booking['call_out_fee_temp']) {
            $callOutFee = $this->request->getParam('call_out_fee');
            if (empty($callOutFee)) {
                $isValid = false;
            }
        }
        // invoice nubmber
        $invoiceNum = $this->request->getParam('invoice_num', '');
        $invoice = $modelBookingInvoice->getByBookingId($bookingId);
        if ($booking['convert_status'] == 'invoice' && !empty($invoice['invoice_num_temp']) && empty($invoiceNum)) {
              $isValid = false;
        }
		//////////by islam to view payment details
		//
        // Load Model
        //
        $modelPayment = new Model_Payment();
        
        //
        //Validation
        //
		
        //$payments = $modelPayment->getByBookingId($booking['booking_id']);
		$payments = $modelPayment->getAll(array('booking_id' => $booking['booking_id']));
		$this->view->test=$payments;
		foreach($payments as $payment){
			$this->view->payment=$payment;
			//echo $payment['received_date'];
		}
			
		
		
		/*//get contractor name
		$modelUser=new Model_User();
		$contractor=$modelUser->getById($payment['contractor_id']);
		$this->view->contractor=$contractor;
		
		////get payment type
		$modelPaymentType=new Model_PaymentType();
		$paymentType=$modelPaymentType->getById($payment['payment_type_id']);
		$this->view->paymentType=$paymentType;
		//////////////end*/
		/**
         * get why Discussion
         */
		 $modelBookingStatusDiscussion = new Model_BookingStatusDiscussion();
        $whyDiscussion = $modelBookingStatusDiscussion->getLastDiscussionByBookingIdAndStatusId($bookingId, $booking['status_id']);
        $this->view->whyDiscussion = $whyDiscussion;
		
		/**
         * get Product by id to edit
         */
		 $modelBookingProduct = new Model_BookingProduct();
        $allBookingProduct = $modelBookingProduct->getByBookingId($bookingId);
        $this->view->allBookingProduct = $allBookingProduct;


        if ($this->request->isPost()) {
            if ($isValid) {
                // approve address
                $address = $this->request->getParam('address', 'old-address');

                if ($address == 'new-address' && $newAddress) {

                    $newAddressData = array(
                        'street_address' => $newAddress['street_address'],
                        'street_number' => $newAddress['street_number'],
                        'suburb' => $newAddress['suburb'],
                        'unit_lot_number' => $newAddress['unit_lot_number'],
                        'postcode' => $newAddress['postcode'],
                        'po_box' => $newAddress['po_box'],
                        'booking_id' => $newAddress['booking_id']
                    );
                    $modelBookingAddress->updateByBookingId($bookingId, $newAddressData);

                }



                if (isset($newAddress['booking_address_id']) && $newAddress['booking_address_id']) {
                    $modelBookingAddressTemp->deleteById($newAddress['booking_address_id']);
                }


                // approve Invoice Number


                if (!empty($invoiceNum)) {
                    if ($invoiceNum == 'old_invoice_num') {
                        $invoice = $modelBookingInvoice->getByBookingId($bookingId);
                        $modelBookingInvoice->updateByBookingId($bookingId, array('invoice_num' => $invoice['invoice_num_temp'], 'invoice_num_temp' => ''));
                    } else {
                        $modelBookingInvoice->updateByBookingId($bookingId, array('invoice_num_temp' => ''));
                    }

                    $modelBooking->updateById($bookingId, array('is_change' => 0));
                }

                // approve service
                foreach ($allBookingServices as $key => $bookingService) {

                    $value = $this->request->getParam($key, 'old');


                    $bookingId = $bookingService['booking_id'];
                    $contractorId = $bookingService['contractor_id'];
                    $serviceId = $bookingService['service_id'];
                    $clone = $bookingService['clone'];
                    $consider_min_price = $bookingService['consider_min_price'];

                    $servicesAttribute = $modelServiceAttribute->getAttributeByServiceId($serviceId);
                    $contractorServiceBooking = $modelContractorServiceBooking->getByBookingAndServiceAndClone($bookingId, $serviceId, $clone);
                    $contractorServiceBookingTemp = $modelContractorServiceBookingTemp->getByBookingAndServiceAndClone($bookingId, $serviceId, $clone);
                    
                    if ($value == 'new') {

                        if ($contractorServiceBookingTemp) {

                            $contractorServiceBookingData = array(
                                'booking_id' => $bookingId,
                                'contractor_id' => $contractorId,
                                'service_id' => $serviceId,
                                'clone' => $clone,
                                'consider_min_price' => $consider_min_price,
                                'is_accepted' => $contractorServiceBookingTemp['is_accepted']
                            );
                            if (empty($contractorServiceBooking)) {
                                //add service
                                $modelContractorServiceBooking->insert($contractorServiceBookingData);
                            } else {
                                $modelContractorServiceBooking->updateById($contractorServiceBooking['id'], $contractorServiceBookingData);
                            }

                            foreach ($servicesAttribute as $attribute) {
                                $attributeType = $modelAttributeType->getById($attribute['attribute_type_id']);
                                $serviceAttributeValueTemp = $modelServiceAttributeValueTemp->getByBookingIdAndServiceAttributeIdAndClone($bookingId, $attribute['service_attribute_id'], $clone);
                                $dataTemp = array(
                                    'service_attribute_id' => $serviceAttributeValueTemp['service_attribute_id'],
                                    'booking_id' => $serviceAttributeValueTemp['booking_id'],
                                    'value' => $serviceAttributeValueTemp['value'],
                                    'is_serialized_array' => $serviceAttributeValueTemp['is_serialized_array'],
                                    'clone' => $serviceAttributeValueTemp['clone']
                                );
                                $oldServiceAttributeValue = $modelServiceAttributeValue->getByBookingIdAndServiceAttributeIdAndClone($bookingId, $attribute['service_attribute_id'], $clone);
                                $delete_add_values = 0;

                                $attributeValueTemp = $serviceAttributeValueTemp['value'];
                                if ($oldServiceAttributeValue) {
                                    $oldAttributeValue = $oldServiceAttributeValue['value'];
                                }

                                if (empty($serviceAttributeValueTemp['value']) && $oldServiceAttributeValue['is_serialized_array'] == 1) {
                                    $deletevalues = unserialize($oldServiceAttributeValue['value']);
                                    foreach ($deletevalues as &$delete_value) {
                                        $attributeListValue = $modelAttributeListValue->getById($delete_value);
                                        $delete_value = $attributeListValue['attribute_value'];
                                    }
                                    $delete_add_values = 1;
                                }


                                // check  value if is searlized and list  for history
                                if ($attributeType['is_list'] == 1) {
                                    if ($serviceAttributeValueTemp['is_serialized_array'] == 1) {

                                        $serializeValues = unserialize($serviceAttributeValueTemp['value']);

                                        foreach ($serializeValues as &$serializeValue) {
                                            $attributeListValue = $modelAttributeListValue->getById($serializeValue);
                                            $serializeValue = $attributeListValue['attribute_value'];
                                        }

                                        if ($oldServiceAttributeValue) {

                                            $oldSerializeValues = unserialize($oldServiceAttributeValue['value']);
                                            foreach ($oldSerializeValues as &$oldSerializeValue) {

                                                $oldAttributeListValue = $modelAttributeListValue->getById($oldSerializeValue);
                                                $oldSerializeValue = $oldAttributeListValue['attribute_value'];
                                            }
                                            $addvalues = array_diff($serializeValues, $oldSerializeValues);
                                            $deletevalues = array_diff($oldSerializeValues, $serializeValues);
                                        }
                                        ////check if old value empty in serialize status
                                        if (empty($oldServiceAttributeValue['value'])) {
                                            $addvalues = $serializeValues;

                                            $delete_add_values = 1;
                                        }
                                    } else {
                                        $attributeListValueTemp = $modelAttributeListValue->getById($serviceAttributeValueTemp['value']);
                                        $attributeValueTemp = $attributeListValueTemp['attribute_value'];

                                        if ($oldServiceAttributeValue) {
                                            $attributeListValue = $modelAttributeListValue->getById($oldServiceAttributeValue['value']);
                                            $oldAttributeValue = $attributeListValue['attribute_value'];
                                        }
                                    }
                                }

                                if ($oldServiceAttributeValue) {
                                    $modelServiceAttributeValue->updateById($oldServiceAttributeValue['service_attribute_value_id'], $dataTemp);
                                } else {
                                    $modelServiceAttributeValue->insert($dataTemp);
                                }
                            }
                            $modelBookingAddressTemp->deleteById($newAddress['booking_address_id']);

                            if ($contractorServiceBooking) {
                                $modelContractorServiceBooking->updateById($contractorServiceBooking['id'], array('is_change' => 0));
                            }

                            $modelContractorServiceBookingTemp->deleteByBookingAndServiceAndClone($bookingId, $serviceId, $clone);
                            $modelServiceAttributeValueTemp->deleteByBookingIdServiceIdAndClone($bookingId, $serviceId, $clone);
                        } else {
                            //delete service
                            $modelContractorServiceBooking->deleteByBookingAndServiceAndClone($bookingId, $serviceId, $clone);

                            //delete attribute
                            $modelServiceAttributeValue->deleteByBookingIdServiceIdAndClone($bookingId, $serviceId, $clone);
                        }
                    } elseif ($value == 'old') {
                        if ($contractorServiceBooking) {
                            $modelContractorServiceBooking->updateById($contractorServiceBooking['id'], array('is_change' => 0));
                        }

                        $modelContractorServiceBookingTemp->deleteByBookingAndServiceAndClone($bookingId, $serviceId, $clone);
                        $modelServiceAttributeValueTemp->deleteByBookingIdServiceIdAndClone($bookingId, $serviceId, $clone);
                    } else {
                        if ($contractorServiceBooking) {
                            $modelContractorServiceBooking->updateById($contractorServiceBooking['id'], array('is_change' => 0));
                        }
                        //
                        // check Auth for logged user
                        //
                        $this->view->main_menu = 'bookings';

                        $bookingId = $this->request->getParam('id', 0);
                        $modelBookingAddressTemp->deleteById($newAddress['booking_address_id']);


                        $booking = $modelBooking->getById($bookingId);
                        $modelContractorServiceBookingTemp->deleteByBookingAndServiceAndClone($bookingId, $serviceId, $clone);
                        $modelServiceAttributeValueTemp->deleteByBookingIdServiceIdAndClone($bookingId, $serviceId, $clone);
                    }
                }


                //
                //save calculation qoute
                //
            
                //Total Discount
                $totalDiscount = $this->request->getParam('total_discount', 'old_discount');
                if ($totalDiscount == 'new_discount') {
                    $totalDiscount = $booking['total_discount_temp'];
                } elseif ($totalDiscount == 'old_discount') {
                    $totalDiscount = $booking['total_discount'];
                } else {
                    $totalDiscount = $booking['total_discount'];
                }


                //Call out Fee
                $callOutFee = $this->request->getParam('call_out_fee', 'old_call_out_fee');
                if ($callOutFee == 'new_call_out_fee') {
                    $callOutFee = $booking['call_out_fee_temp'];
                } elseif ($callOutFee == 'old_call_out_fee') {
                    $callOutFee = $booking['call_out_fee'];
                } else {
                    $callOutFee = $booking['call_out_fee'];
                }


                $subTotal = $modelContractorServiceBooking->getTotalBookingQoute($bookingId);
                $gstTax = ($subTotal + $callOutFee - $totalDiscount) * get_config('gst_tax');
                $totalQoute = ($subTotal + $callOutFee - $totalDiscount) + $gstTax;

                $db_params = array();
                $db_params['sub_total'] = $subTotal;
                $db_params['gst'] = $gstTax;
                $db_params['qoute'] = $totalQoute;
                $db_params['total_discount'] = $totalDiscount;
                $db_params['total_discount_temp'] = 0;
                $db_params['call_out_fee'] = $callOutFee;
                $db_params['call_out_fee_temp'] = 0;
                $db_params['is_change'] = 0;

                $modelBooking->updateById($bookingId, $db_params);

                if ($redirectToPayment) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "The selected booking have been Approved change."));
                    $this->_redirect($this->router->assemble(array('booking_id' => $bookingId), 'bookingPaymentList'));
                }

                $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "The selected service have been approved."));
                $this->_redirect($this->router->assemble(array('id' =>$bookingId), 'bookingView'));
            } else {
                $this->view->messages[] = array('type' => 'error', 'message' => 'Please select ALL items to approve');
            }
        }
    }

    public function ignoreAllAction() {

        //
        // check Auth for logged user
        //
        $this->view->main_menu = 'bookings';

        $bookingId = $this->request->getParam('id', 0);


        //
        // load models
        //
       
        $modelContractorServiceBookingTemp = new Model_ContractorServiceBookingTemp();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelServiceAttributeValueTemp = new Model_ServiceAttributeValueTemp();
        $modelBooking = new Model_Booking();
        $modelBookingAddressTemp = new Model_BookingAddressTemp();


        // delete booking Address temp
        $bookingAddressTemp = $modelBookingAddressTemp->getByBookingId($bookingId);
        $modelBookingAddressTemp->deleteById($bookingAddressTemp['booking_address_id']);

        // delete service  and his attripute from temp
        $modelContractorServiceBookingTemp->deleteBybookingId($bookingId);
        $modelServiceAttributeValueTemp->deleteByBookingId($bookingId);

        // set total discount , call out fee and is_change in booking and contractor service booking to  false
        $data = array('is_change' => 0);
        $modelContractorServiceBooking->updateByBookingId($bookingId, $data);

        $data = array(
            'is_change' => 0,
            'total_discount_temp' => 0,
            'call_out_fee_temp' => 0
        );
        $modelBooking->updateById($bookingId, $data);


        $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "The selected booking have been ignore all change."));
        $this->_redirect($this->router->assemble(array('id' => $bookingId), 'bookingEdit'));
    }

    public function ignoreBookingAddressAction() {

        //
        // check Auth for logged user
        //
        $this->view->main_menu = 'bookings';

        $bookingId = $this->request->getParam('id', 0);


        //
        // load models
        //

        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelBooking = new Model_Booking();
        $modelBookingAddressTemp = new Model_BookingAddressTemp();


        // delete booking Address temp

        $bookingAddressTemp = $modelBookingAddressTemp->getByBookingId($bookingId);
        $modelBookingAddressTemp->deleteById($bookingAddressTemp['booking_address_id']);

        //check if the contractor change total discount

        $booking = $modelBooking->getById($bookingId);
        $totalDiscount = $booking['total_discount'];
        $totalDiscountTemp = $booking['total_discount_temp'];

        // check if the contractor change call out fee 

        $callOutFee = $booking['call_out_fee'];
        $callOutFeeTemp = $booking['call_out_fee_temp'];

        //check if the contractor change service

        $ServicesBooking = $modelContractorServiceBooking->getByBookingId($bookingId);
        $isNotChange = true;
        foreach ($ServicesBooking as $ServiceBooking) {
            if ($ServiceBooking['is_change'] == 1) {
                $isNotChange = false;
            }
        }

        if ($isNotChange && ($totalDiscount == $totalDiscountTemp) && ($callOutFee == $callOutFeeTemp )) {
            $data = array('is_change' => 0);
            $modelBooking->updateById($bookingId, $data);
        }
        $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "The selected booking have been ignore booking address."));
        $this->_redirect($this->router->assemble(array('id' => $bookingId), 'bookingEdit'));
    }

    public function ignoreTotalDiscountAction() {

        //
        // check Auth for logged user
        //
        $this->view->main_menu = 'bookings';

        $bookingId = $this->request->getParam('id', 0);

        //
        // load models
        //
        $modelBooking = new Model_Booking();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelBookingAddressTemp = new Model_BookingAddressTemp();


        // delete total discount change

        $booking = $modelBooking->getById($bookingId);
        $totalDiscount = $booking['total_discount'];
        $data = array('total_discount_temp' => $totalDiscount);
        $modelBooking->updateById($bookingId, $data);

        //check if the contractor change address

        $bookingAddressTemp = $modelBookingAddressTemp->getByBookingId($bookingId);

        // check if the contractor change call out fee 

        $callOutFee = $booking['call_out_fee'];
        $callOutFeeTemp = $booking['call_out_fee_temp'];


        //check if the contractor change service

        $ServicesBooking = $modelContractorServiceBooking->getByBookingId($bookingId);
        $isNotChange = true;
        foreach ($ServicesBooking as $ServiceBooking) {
            if ($ServiceBooking['is_change'] == 1) {
                $isNotChange = false;
            }
        }

        if ($isNotChange && !$bookingAddressTemp && ($callOutFee == $callOutFeeTemp )) {

            $data = array('is_change' => 0);
            $modelBooking->updateById($bookingId, $data);
        }


        $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "The selected booking have been ignore Total Discount."));
        $this->_redirect($this->router->assemble(array('id' => $bookingId), 'bookingEdit'));
    }

    public function ignoreCallOutFeeAction() {

        //
        // check Auth for logged user
        //
        $this->view->main_menu = 'bookings';

        $bookingId = $this->request->getParam('id', 0);

        //
        // load models
        //
        $modelBooking = new Model_Booking();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelBookingAddressTemp = new Model_BookingAddressTemp();


        // delete call out fee  change

        $booking = $modelBooking->getById($bookingId);
        $callOutFee = $booking['call_out_fee'];
        $data = array('call_out_fee_temp' => $callOutFee);
        $modelBooking->updateById($bookingId, $data);

        // check if the contractor change total discount 

        $totalDiscount = $booking['total_discount'];
        $totalDiscountTemp = $booking['total_discount_temp'];

        //check if the contractor change address

        $bookingAddressTemp = $modelBookingAddressTemp->getByBookingId($bookingId);

        //check if the contractor change service

        $ServicesBooking = $modelContractorServiceBooking->getByBookingId($bookingId);
        $isNotChange = true;
        foreach ($ServicesBooking as $ServiceBooking) {
            if ($ServiceBooking['is_change'] == 1) {
                $isNotChange = false;
            }
        }

        if ($isNotChange && !$bookingAddressTemp && ($totalDiscount == $totalDiscountTemp)) {

            $data = array('is_change' => 0);
            $modelBooking->updateById($bookingId, $data);
        }


        $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "The selected booking have been ignore Total Discount."));
        $this->_redirect($this->router->assemble(array('id' => $bookingId), 'bookingEdit'));
    }

    public function ignoreServiceAction() {

        //
        // check Auth for logged user
        //
        $this->view->main_menu = 'bookings';

        $bookingId = $this->request->getParam('id', 0);
        $service_id = $this->request->getParam('service_id', 0);
        $clone = $this->request->getParam('clone', 0);

        //
        // load models
        //
        $modelBooking = new Model_Booking();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelBookingAddressTemp = new Model_BookingAddressTemp();
        $modelContractorServiceBookingTemp = new Model_ContractorServiceBookingTemp();
        $modelServiceAttributeValue = new Model_ServiceAttributeValue();
        $modelServiceAttributeValueTemp = new Model_ServiceAttributeValueTemp();
        $modelServiceAttribute = new Model_ServiceAttribute();

        //ignore service change

        $contractorServiceBookingTemp = $modelContractorServiceBookingTemp->getByBookingAndServiceAndClone($bookingId, $service_id, $clone);
        $contractorServiceBooking = $modelContractorServiceBooking->getByBookingAndServiceAndClone($bookingId, $service_id, $clone);
        $serviceAttribute = $modelServiceAttribute->getAttributeByServiceId($service_id);

        if ($contractorServiceBookingTemp) {
            // if the contractor update service
            if ($contractorServiceBooking) {
                foreach ($serviceAttribute as $attribute) {

                    $serviceAttributeValue = $modelServiceAttributeValue->getByBookingIdAndServiceAttributeIdAndClone($bookingId, $attribute['service_attribute_id'], $clone);
                    $serviceAttributeValueTemp = $modelServiceAttributeValueTemp->getByBookingIdAndServiceAttributeIdAndClone($bookingId, $attribute['service_attribute_id'], $clone);
                    $data = array(
                        'service_attribute_id' => $serviceAttributeValue['service_attribute_id'],
                        'booking_id' => $serviceAttributeValue['booking_id'],
                        'value' => $serviceAttributeValue['value'],
                        'is_serialized_array' => $serviceAttributeValue['is_serialized_array'],
                        'clone' => $serviceAttributeValue['clone']
                    );
                    $modelServiceAttributeValueTemp->updateById($serviceAttributeValueTemp['service_attribute_value_id'], $data);
                }

//                $db_params = array();
//                $db_params['is_change'] = 0;
                $modelContractorServiceBooking->updateById($contractorServiceBooking['id'], array('is_change' => 0));
                $modelContractorServiceBookingTemp->updateById($contractorServiceBookingTemp['id'], array('is_change' => 0, 'consider_min_price' => $contractorServiceBooking['consider_min_price']));
            }  // if the contractor Add service
            else {
                $modelContractorServiceBookingTemp->deleteById($contractorServiceBookingTemp['id']);
                $modelServiceAttributeValueTemp->deleteByBookingIdServiceIdAndClone($bookingId, $service_id, $clone);
            }
        }

        //check if the contractor change address

        $bookingAddressTemp = $modelBookingAddressTemp->getByBookingId($bookingId);

        //check if the contractor change total discount

        $booking = $modelBooking->getById($bookingId);
        $totalDiscount = $booking['total_discount'];
        $totalDiscountTemp = $booking['total_discount_temp'];

        // check if the contractor change call out fee 

        $callOutFee = $booking['call_out_fee'];
        $callOutFeeTemp = $booking['call_out_fee_temp'];

        //check if another service change

        $ServicesBooking = $modelContractorServiceBooking->getByBookingId($bookingId);
        $isNotChange = true;
        foreach ($ServicesBooking as $ServiceBooking) {
            if ($ServiceBooking['is_change'] == 1) {
                $isNotChange = false;
            }
        }

        if ($isNotChange && !$bookingAddressTemp && ($totalDiscount == $totalDiscountTemp) && ($callOutFee == $callOutFeeTemp )) {
            $data = array('is_change' => 0);
            $modelBooking->updateById($bookingId, $data);
        }
        $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "The selected booking have been ignore service."));
        $this->_redirect($this->router->assemble(array('id' => $bookingId), 'bookingEdit'));
    }

}