<?php

class BookingAttendanceController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
    
	}
	
	
	public function indexAction(){
	  
	  
	  CheckAuth::checkLoggedIn();
	  CheckAuth::checkCredential(array('canSeeBookingChecks'));
	  
	  $orderBy = $this->request->getParam('sort', 'booking_attendance_id');
      $sortingMethod = $this->request->getParam('method', 'asc');
      //$currentPage = $this->request->getParam('page', 1);
      $filters = $this->request->getParam('fltr', array());
      $is_first_time = $this->request->getParam('is_first_time');
	  $page_number = $this->request->getParam('page_number');
	  
        if($filters){
            foreach ($filters as &$filter){
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }
	  
	  /*$pager = new Model_Pager();
      $pager->perPage = get_config('perPage');
      $pager->currentPage = $currentPage;
      $pager->url = $_SERVER['REQUEST_URI'];*/
		
	   if ($this->request->isPost()) {
		  if(isset($page_number)){
		   $perPage = 15;
		   $currentPage = $page_number +1;
	      }
		
		 $modelBookingAttendance = new Model_BookingAttendance();
         $data = $modelBookingAttendance->getAll($filters, "{$orderBy} {$sortingMethod}", $pager , 0 ,$perPage,$currentPage);	  
	     $result = array();
	  
	  
	  
	  
	  foreach($data as $key=>$row){	    
		$checkINOUT = $modelBookingAttendance->getByBookingId($row['booking_id']);
		$result[$key]['booking_id'] = $row['booking_id'];
	    $result[$key]['booking_num'] = $row['booking_num'];
	    $result[$key]['username'] = $row['username'];
	    $result[$key]['contractor_id'] = $row['contractor_id'];
	    $result[$key]['checkList'] = $checkINOUT;
	  }
	  
	    $this->view->is_first_time = $is_first_time;  
		$this->view->data = $result;
        $this->view->filters = $filters;  
		echo $this->view->render('booking-attendance/draw-node.phtml');
		exit;  
		
		}  
	 
	  
	  
	  $modelUser = new Model_User();
	  $activeContractor = $modelUser->getContractorsByStatus('TRUE');	
	    foreach ($activeContractor as $ContractorResult) {
			$modelUser->fill($ContractorResult, array('contractor_info'));
            $Contractordata[$ContractorResult['user_id']] = $ContractorResult['contractor_name'];
        }

            /**
             * Create contractor Element
             */
			 
		$select_contractor = new Zend_Form_Element_Select('fltr');
        $select_contractor->setBelongsTo('contractor');
        $select_contractor->setDecorators(array('ViewHelper'));
        $select_contractor->setValue(isset($filters['contractor']) ? $filters['contractor'] : '');
		
		$url = $this->router->assemble(array(), 'bookingCheckInCheckOutList');
		if(isset($filters['time'])){
	      $url = $url.'?fltr[time]='.$filters['time'].'&fltr[contractor]=';
	     }else{
		  $url = $url.'?fltr[contractor]=';
		 }
		
		$select_contractor->setAttrib('onchange', "fltr('".$url."','contractor-fltr')");
		
        
		$select_contractor->setAttrib('class', "form-control");
        $select_contractor->addMultiOption('', 'ALL TECHNICIANS');
        $select_contractor->addMultiOptions($Contractordata);
		
		
		$select_time = new Zend_Form_Element_Select('fltr');
        $select_time->setBelongsTo('time');
        $select_time->setDecorators(array('ViewHelper'));
		
		$url = $this->router->assemble(array(), 'bookingCheckInCheckOutList');
		if(isset($filters['contractor'])){
	      $url = $url.'?fltr[contractor]='.$filters['contractor'].'&fltr[time]=';
	     }else{
		  $url = $url.'?fltr[time]=';
		 }
		
        $select_time->setAttrib('onchange', "fltr('".$url."','time-fltr')");
        $select_time->setValue(isset($filters['time']) ? $filters['time'] : '');
		$select_time->setAttrib('class', "form-control");
        $select_time->addMultiOption('', 'ALL');
        $select_time->addMultiOption('current', 'CURRENT');
        $select_time->addMultiOption('previous', 'HISTORY');
		
		
		
        $this->view->select_contractor = $select_contractor;
        $this->view->select_time = $select_time;

		
		
		
	  
	  //$this->view->data = $result;
	  //$this->view->currentPage = $currentPage;
      //$this->view->perPage = $pager->perPage;
      //$this->view->pageLinks = $pager->getPager();
      $this->view->sortingMethod = $sortingMethod;
      $this->view->orderBy = $orderBy;
      $this->view->filters = $filters;
	  
	}
	
}
?>