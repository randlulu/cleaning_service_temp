<?php

class ContractorOwnerController extends Zend_Controller_Action {

    private $request;
    private $router;
    private $contractor_info_id;

    public function init() {
        parent::init();

        CheckAuth::checkLoggedIn();

        if ('contractor' != CheckAuth::getRoleName()) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }

        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $loggedUser = CheckAuth::getLoggedUser();

        $contractorInfoObj = new Model_ContractorInfo();
        $contractorInfo = $contractorInfoObj->getByContractorId($loggedUser['user_id']);

        if (empty($contractorInfo)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "please fill all Contractor Info"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $this->contractor_info_id = $contractorInfo['contractor_info_id'];
    }

    /**
     * Items list action
     */
    public function indexAction() {

        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'contractor_owner_id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if($filters){
            foreach ($filters as &$filter){
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];


        //
        //get user email by id
        //
        $filters['contractor_info_id'] = $this->contractor_info_id;


        //
        // get data list
        //
        $contractorOwnerObj = new Model_ContractorOwner();
        $this->view->data = $contractorOwnerObj->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);

        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
    }

    /**
     * Add new item action
     */
    public function addAction() {

        //
        // get request parameters
        //
        $name = $this->request->getParam('name');
        $email1 = $this->request->getParam('email1');
        $email2 = $this->request->getParam('email2');
        $email3 = $this->request->getParam('email3');
        $mobile1 = $this->request->getParam('mobile1');
        $mobile2 = $this->request->getParam('mobile2');
        $mobile3 = $this->request->getParam('mobile3');
        $phone1 = $this->request->getParam('phone1');
        $phone2 = $this->request->getParam('phone2');
        $phone3 = $this->request->getParam('phone3');
        $fax = $this->request->getParam('fax');
        $emergencyPhone = $this->request->getParam('emergency_phone');
        $unitLotNumber = $this->request->getParam('unit_lot_number');
        $streetNumber = $this->request->getParam('street_number');
        $streetAddress = $this->request->getParam('street_address');
        $suburb = $this->request->getParam('suburb');
        $state = $this->request->getParam('state');
        $postcode = $this->request->getParam('postcode');
        $po_box = $this->request->getParam('po_box');
        $cityId = $this->request->getParam('city_id');
        $countryId = $this->request->getParam('country_id');

        if (get_config('remove_white_spacing')) {
            $mobile1 = preparer_number($mobile1);
            $mobile2 = preparer_number($mobile2);
            $mobile3 = preparer_number($mobile3);
            $phone1 = preparer_number($phone1);
            $phone2 = preparer_number($phone2);
            $phone3 = preparer_number($phone3);
        }
        
        
        //
        // init action form
        //
        $form = new Form_ContractorOwner(array('country_id' => $countryId, 'state' => $state));

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $contractorOwnerObj = new Model_ContractorOwner();
              
                $data = array(
                    'name' => $name,
                    'contractor_info_id' => $this->contractor_info_id,
                    'created' => time(),
                    'city_id' => $cityId,
                    'email1' => $email1,
                    'email2' => $email2,
                    'email3' => $email3,
                    'mobile1' => $mobile1,
                    'mobile2' => $mobile2,
                    'mobile3' => $mobile3,
                    'phone1' => $phone1,
                    'phone2' => $phone2,
                    'phone3' => $phone3,
                    'fax' => $fax,
                    'emergency_phone' => $emergencyPhone,
                    'unit_lot_number' => $unitLotNumber,
                    'street_number' => $streetNumber,
                    'street_address' => $streetAddress,
                    'suburb' => $suburb,
                    'state' => $state,
                    'postcode' => $postcode,
                    'po_box' => $po_box
                );


                $success = $contractorOwnerObj->insert($data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Contractor Owner"));
                }
                echo 1;
                exit;
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('contractor-owner/add_edit.phtml');
        exit;
    }

    public function deleteAction() {

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
        if ($id) {
            $ids[] = $id;
        }

        $contractorOwnerObj = new Model_ContractorOwner();
        foreach ($ids as $id) {
            $contractorOwner = $contractorOwnerObj->getById($id);

            if ($this->contractor_info_id == $contractorOwner['contractor_info_id']) {
              $contractorOwnerObj->updateById($id, array('is_deleted' => 1));
            }
        }
        $this->_redirect($this->router->assemble(array(), 'contractorOwnerList'));
    }
    
    public function editAction() {


        //
        // get request parameters
        //
        $id = $this->request->getParam('id');
        $name = $this->request->getParam('name');
        $email1 = $this->request->getParam('email1');
        $email2 = $this->request->getParam('email2');
        $email3 = $this->request->getParam('email3');
        $mobile1 = $this->request->getParam('mobile1');
        $mobile2 = $this->request->getParam('mobile2');
        $mobile3 = $this->request->getParam('mobile3');
        $phone1 = $this->request->getParam('phone1');
        $phone2 = $this->request->getParam('phone2');
        $phone3 = $this->request->getParam('phone3');
        $fax = $this->request->getParam('fax');
        $emergencyPhone = $this->request->getParam('emergency_phone');
        $unitLotNumber = $this->request->getParam('unit_lot_number');
        $streetNumber = $this->request->getParam('street_number');
        $streetAddress = $this->request->getParam('street_address');
        $suburb = $this->request->getParam('suburb');
        $state = $this->request->getParam('state');
        $postcode = $this->request->getParam('postcode');
        $po_box = $this->request->getParam('po_box');
        $cityId = $this->request->getParam('city_id');
        $countryId = $this->request->getParam('country_id');


        if (get_config('remove_white_spacing')) {
            $mobile1 = preparer_number($mobile1);
            $mobile2 = preparer_number($mobile2);
            $mobile3 = preparer_number($mobile3);
            $phone1 = preparer_number($phone1);
            $phone2 = preparer_number($phone2);
            $phone3 = preparer_number($phone3);
        }

        $router = Zend_Controller_Front::getInstance()->getRouter();


        $modelContractorOwner = new Model_ContractorOwner();
        $contractorOwner = $modelContractorOwner->getById($id);

       
        //
        // init action form
        //
        $form = new Form_ContractorOwner(array('mode' => 'update', 'contractorOwner' => $contractorOwner, 'country_id' => $countryId, 'state' => $state));

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'name' => $name,
                    'created' => time(),
                    'city_id' => $cityId,
                    'email1' => $email1,
                    'email2' => $email2,
                    'email3' => $email3,
                    'mobile1' => $mobile1,
                    'mobile2' => $mobile2,
                    'mobile3' => $mobile3,
                    'phone1' => $phone1,
                    'phone2' => $phone2,
                    'phone3' => $phone3,
                    'fax' => $fax,
                    'emergency_phone' => $emergencyPhone,
                    'unit_lot_number' => $unitLotNumber,
                    'street_number' => $streetNumber,
                    'street_address' => $streetAddress,
                    'suburb' => $suburb,
                    'state' => $state,
                    'postcode' => $postcode,
                    'po_box' => $po_box
                );

                $success = $modelContractorOwner->updateById($id, $data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Contractor Owner"));
                }

                echo 1;
                exit;
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('contractor-owner/add_edit.phtml');
        exit;
    }
    
        public function photoUploadAction() {


        //
        // get request parameters
        //
       $contractorOwnerId = $this->request->getParam('id');

        // load model

        $modelContractorOwner = new Model_ContractorOwner();

        $contractorOwner = $modelContractorOwner->getById($contractorOwnerId);

        if (!$contractorOwner) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }


        //init action form
        $form = new Form_ContractorOwnerPhotoUpload(array('contractorOwnerId' => $contractorOwnerId));

        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {

            if (!$form->isValid($this->getRequest()->getParams())) {
                return $this->render('photo-upload');
            }

            if (!$form->image->receive()) {
                $this->view->message = '<div class="errors">Errors Receiving File.</div>';
                return $this->render('photo-upload');
            }

            if ($form->image->isUploaded()) {
                $source = $form->image->getFileName();
                $fileInfo = pathinfo($source);
                $ext = $fileInfo['extension'];


                //to re-name the file, all you need to do is save it with a new name, instead of the name they uploaded it with. Normally, I use the primary key of the database row where I'm storing the name of the image. For example, if it's an image of Person 1, I call it 1.jpg. The important thing is that you make sure the image name will be unique in whatever directory you save it to.
                //get sub dir
                $dir = get_config('avatar') . '/';
                $subdir = date('Y/m/d/');

                //check if file exists or not
                $fullDir = $dir . $subdir;

                if (!is_dir($fullDir)) {
                    mkdir($fullDir, 0777, true);
                }

                $orginal_avatar_owner = "orginal_avatar_owner_{$contractorOwnerId}.{$ext}";
                $small_avatar_owner = "small_avatar_owner_{$contractorOwnerId}.{$ext}";
                $large_avatar_owner = "large_avatar_owner_{$contractorOwnerId}.{$ext}";

                $data = array(
                    'small_avatar_path' => $subdir . $small_avatar_owner,
                    'large_avatar_path' => $subdir . $large_avatar_owner
                );

                $modelContractorOwner->updateById($contractorOwnerId, $data);

                
                //save image to database and filesystem here
                $file_saved = copy($source, $fullDir . $orginal_avatar_owner);
                ImageMagick::create_thumbnail($source, $fullDir . $small_avatar_owner, 73, 90);
                ImageMagick::create_thumbnail($source, $fullDir . $large_avatar_owner, 150, 185);
                if ($file_saved) {
                    if (file_exists($source)) {
                        unlink($source);
                    }
                    $this->_redirect($this->router->assemble(array(), 'contractorOwnerList'));
                }
            }
        }
    }
    public function viewAction() {
   
        //
        // get request parameters
        //
      
        $contractorOwnerId = $this->request->getParam('id');

        // load model

        $modelContractorOwner = new Model_ContractorOwner();

        $contractorOwner = $modelContractorOwner->getById($contractorOwnerId);

        if (!$contractorOwner) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect($this->router->assemble(array('contractor_info_id' => $contractorOwner['contractor_info_id']), 'settingsContractorOwnerList'));
            return;
        }
        

        $this->view->contractorOwner = $contractorOwner;

        //
        // render views
        //
        echo $this->view->render('contractor-owner/view.phtml');
        exit;
    }

}

