<?php

class AttributeController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
    }

    public function drowAttributeAction() {
        $service_id = $this->request->getParam('service_id');
        $clone = $this->request->getParam('clone', 0);
        $without_pricing = $this->request->getParam('without_pricing', 0);
		
        $serviceAttribute_obj = new Model_ServiceAttribute();

        $serviceAttribute = $serviceAttribute_obj->getAttributeByServiceId($service_id, $without_pricing);
		//var_dump($serviceAttribute);
		//exit;
        $this->view->serviceAttribute = $serviceAttribute;
        $this->view->clone = $clone;

        echo $this->view->render('attribute/drowAttribute.phtml');
        exit;
    }

    public function countServicePriceAction() {
        $serviceId = $this->request->getParam('service_id');
        $clone = $this->request->getParam('clone', 0);

        $serviceObj = new Model_Services();
        $attributesObj = new Model_Attributes();
        $attributeListValueObj = new Model_AttributeListValue();

        //get service by id to get price_equasion
        $service = $serviceObj->getById($serviceId);

        if (preg_match_all('#\[(.*?)\]#', $service['price_equasion'], $matches)) {
            $param = array();
            foreach ($matches[1] as $attributeVariableName) {
                $attribute = $attributesObj->getByVariableName($attributeVariableName);
                $attributeId = isset($attribute['attribute_id']) && $attribute['attribute_id'] ? $attribute['attribute_id'] : 0;

                $attribute_value = $this->request->getParam("attribute_{$serviceId}{$attributeId}" . ($clone ? '_' . $clone : ''), 0);

                if ($attribute['is_list']) {
                    $attributeListValue = $attributeListValueObj->getById($attribute_value);
                    $param[$attributeVariableName] = isset($attributeListValue['unit_price']) && $attributeListValue['unit_price'] ? $attributeListValue['unit_price'] : 0;
                } else {
                    $param[$attributeVariableName] = $attribute_value;
                }
            }

            $equasion = $service['price_equasion'];

            //build getPrice function
            createPriceEquasionFunction('getPrice', $equasion);

            echo getPrice($param);
            exit;
        } else {
            echo 0;
            exit;
        }
    }

}

?>
