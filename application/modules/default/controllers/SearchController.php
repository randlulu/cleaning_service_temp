<?php

class SearchController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
    }

    /**
     * Items list action
     */
    public function indexAction() {

        //check Auth for logged user
		
		
		
		
        CheckAuth::checkPermission(array('search'));

        //get request parameters
        $keywords = $this->request->getParam('keywords');
        $inquiryCurrentPage = $this->request->getParam('inquiryPage', 1);
        $bookingCurrentPage = $this->request->getParam('bookingPage', 1);
        $estimateCurrentPage = $this->request->getParam('estimatePage', 1);
        $invoiceCurrentPage = $this->request->getParam('invoicePage', 1);
        $customerCurrentPage = $this->request->getParam('customerPage', 1);
        $complaintCurrentPage = $this->request->getParam('complaintPage', 1);
		$fullKeyWords = $keywords;
		if(preg_match("/^[0-9]{10}$/", $keywords)) {
		  $keywords = substr($keywords,-9);
        }
		
		
		
		

        $filters = $this->request->getParam('fltr', array());

        if (trim($keywords)) {
            $filters['keywords'] = trim($keywords);
        }

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }
		
		
		
        
        

        //inquiry
        $inquiryPager = new Model_Pager();
        $inquiryPager->perPage = get_config('perPage');
        $inquiryPager->currentPage = $inquiryCurrentPage;
        $inquiryPager->url = $_SERVER['REQUEST_URI'];
        $inquiryPager->pageLabel = 'inquiryPage';
        $inquiryPager->pageRange = 4;
        $inquiryPager->pageFirst = '&laquo;';
        $inquiryPager->pageLast = '&raquo;';
		
		
		if($filters && stripos($filters['keywords'] , 'INQ')  === 0){
		  $modelInquiry = new Model_Inquiry();
		  $inquiry = $modelInquiry->getByNum($filters['keywords']);
		  if($inquiry){
		    $this->_redirect($this->router->assemble(array('id'=> $inquiry['inquiry_id']), 'inquiryView'));
		  }
		}
		
		if($filters && stripos($filters['keywords'] , 'BOK')  === 0){
		  $modelBooking = new Model_Booking();
		  $booking = $modelBooking->getByNum($filters['keywords']);
		  if($booking){
		    $this->_redirect($this->router->assemble(array('id'=> $booking['booking_id']), 'bookingView'));
		  }
		}
		
		if ($filters && stripos($filters['keywords'] , 'EST')  === 0) {
            $modelBookingEstimate = new Model_BookingEstimate();
            $estimate = $modelBookingEstimate->getByNum($filters['keywords']);
			if($estimate){
		    $this->_redirect($this->router->assemble(array('id'=> $estimate['id']), 'estimateView'));
		  }		
        }
		
		
		if ($filters && stripos($filters['keywords'] , 'INV')  === 0) {
            $modelBookingInvoice = new Model_BookingInvoice();
            $invoice = $modelBookingInvoice->getByNum($filters['keywords']);
			if($invoice){
		    $this->_redirect($this->router->assemble(array('id'=> $invoice['id']), 'invoiceView'));
		  }		
        }
		
		if ($filters && stripos($filters['keywords'] , 'CMP')  === 0) {
            $modelComplaint = new Model_Complaint();
            $complaint = $modelComplaint->getByNum($filters['keywords']);
			if($complaint){
		    $this->_redirect($this->router->assemble(array('id'=> $complaint['complaint_id']), 'complaintView'));
		  }		
        }

		
		
        $inquiryData = array();
        if ($filters) {
		  
            $modelInquiry = new Model_Inquiry();
            $inquiryData = $modelInquiry->getAll($filters, "inquiry_id desc", $inquiryPager);
        }
		
		
		
		
		/*if(stripos($filters['keywords'] , 'INQ')  === 0 && $inquiryData && count($inquiryData) == 1){
		 $this->_redirect($this->router->assemble(array('id'=> $inquiryData[0]['inquiry_id']), 'inquiryView'));
		}*/
		
        $this->view->inquiryData = $inquiryData;
        $this->view->inquiryCurrentPage = $inquiryCurrentPage;
        $this->view->inquiryPerPage = $inquiryPager->perPage;
        $this->view->inquiryPageLinks = $inquiryPager->getPager();
        $this->view->inquiryDataCounter = count($inquiryData);


        //booking
        $bookingPager = new Model_Pager();
        $bookingPager->perPage = get_config('perPage');
        $bookingPager->currentPage = $bookingCurrentPage;
        $bookingPager->url = $_SERVER['REQUEST_URI'];
        $bookingPager->pageLabel = 'bookingPage';
        $bookingPager->pageRange = 4;
        $bookingPager->pageFirst = '&laquo;';
        $bookingPager->pageLast = '&raquo;';

        $bookingData = array();
        if ($filters) {
            $modelBooking = new Model_Booking();
            $bookingData = $modelBooking->getAll($filters, "booking_id desc", $bookingPager);
        }
		
		/*if(stripos($filters['keywords'] , 'BOK')  === 0 && $bookingData && count($bookingData) == 1){
		 $this->_redirect($this->router->assemble(array('id'=> $bookingData[0]['booking_id']), 'bookingView'));
		}*/

        $this->view->bookingData = $bookingData;
        $this->view->bookingCurrentPage = $bookingCurrentPage;
        $this->view->bookingPerPage = $bookingPager->perPage;
        $this->view->bookingPageLinks = $bookingPager->getPager();
        $this->view->bookingDataCounter = count($bookingData);


        //estimate
        $estimatePager = new Model_Pager();
        $estimatePager->perPage = get_config('perPage');
        $estimatePager->currentPage = $estimateCurrentPage;
        $estimatePager->url = $_SERVER['REQUEST_URI'];
        $estimatePager->pageLabel = 'estimatePage';
        $estimatePager->pageRange = 4;
        $estimatePager->pageFirst = '&laquo;';
        $estimatePager->pageLast = '&raquo;';

        $estimateData = array();
        if ($filters) {
            $modelBookingEstimate = new Model_BookingEstimate();
            $estimateData = $modelBookingEstimate->getAll($filters, "id desc", $estimatePager);
        }
		
		/*if(stripos($filters['keywords'] , 'EST')  === 0 && $estimateData && count($estimateData) == 1){
		 $this->_redirect($this->router->assemble(array('id'=> $estimateData[0]['id']), 'estimateView'));
		}*/

        $this->view->estimateData = $estimateData;
        $this->view->estimateCurrentPage = $estimateCurrentPage;
        $this->view->estimatePerPage = $estimatePager->perPage;
        $this->view->estimatePageLinks = $estimatePager->getPager();
        $this->view->estimateDataCounter = count($estimateData);


        //invoice
        $invoicePager = new Model_Pager();
        $invoicePager->perPage = get_config('perPage');
        $invoicePager->currentPage = $invoiceCurrentPage;
        $invoicePager->url = $_SERVER['REQUEST_URI'];
        $invoicePager->pageLabel = 'invoicePage';
        $invoicePager->pageRange = 4;
        $invoicePager->pageFirst = '&laquo;';
        $invoicePager->pageLast = '&raquo;';

        $invoiceData = array();
        if ($filters) {
            $modelBookingInvoice = new Model_BookingInvoice();
            $invoiceData = $modelBookingInvoice->getAll($filters, "id desc", $invoicePager);
        }
		
		/*if(stripos($filters['keywords'] , 'INV')  === 0 && $invoiceData && count($invoiceData) == 1){
		 $this->_redirect($this->router->assemble(array('id'=> $invoiceData[0]['id']), 'invoiceView'));
		}*/

        $this->view->invoiceData = $invoiceData;
        $this->view->invoiceCurrentPage = $invoiceCurrentPage;
        $this->view->invoicePerPage = $invoicePager->perPage;
        $this->view->invoicePageLinks = $invoicePager->getPager();
        $this->view->invoiceDataCounter = count($invoiceData);


        //customer
        $customerPager = new Model_Pager();
        $customerPager->perPage = get_config('perPage');
        $customerPager->currentPage = $customerCurrentPage;
        $customerPager->url = $_SERVER['REQUEST_URI'];
        $customerPager->pageLabel = 'customerPage';
        $customerPager->pageRange = 4;
        $customerPager->pageFirst = '&laquo;';
        $customerPager->pageLast = '&raquo;';

        $customerData = array();
        if ($filters) {
            $modelCustomer = new Model_Customer();
            $customerData = $modelCustomer->getAll($filters, "customer_id desc", $customerPager);
        }

        $this->view->customerData = $customerData;
        $this->view->customerCurrentPage = $customerCurrentPage;
        $this->view->customerPerPage = $customerPager->perPage;
        $this->view->customerPageLinks = $customerPager->getPager();
        $this->view->customerDataCounter = count($customerData);


        //complaint
        $complaintPager = new Model_Pager();
        $complaintPager->perPage = get_config('perPage');
        $complaintPager->currentPage = $complaintCurrentPage;
        $complaintPager->url = $_SERVER['REQUEST_URI'];
        $complaintPager->pageLabel = 'complaintPage';
        $complaintPager->pageRange = 4;
        $complaintPager->pageFirst = '&laquo;';
        $complaintPager->pageLast = '&raquo;';

        $complaintData = array();
        if ($filters) {
            $modelComplaint = new Model_Complaint();
            $complaintData = $modelComplaint->getAll($filters, "created desc", $complaintPager);
        }
		
		/*if(stripos($filters['keywords'] , 'CMP')  === 0 && $complaintData && count($complaintData) == 1){
		 $this->_redirect($this->router->assemble(array('id'=> $complaintData[0]['complaint_id']), 'complaintView'));
		}*/

        $this->view->complaintData = $complaintData;
        $this->view->complaintCurrentPage = $complaintCurrentPage;
        $this->view->complaintPerPage = $complaintPager->perPage;
        $this->view->complaintPageLinks = $complaintPager->getPager();
        $this->view->complaintDataCounter = count($complaintData);
		
		
		
		$this->view->filters = $filters;
		$filters['keywords'] = trim($fullKeyWords);
        $this->view->keywords = $keywords;
		
		
    }
	
	
	public function updateFullTextSearchAction() {
		//Update Full Text Search
		$modelUpdateFullTextSearch = new Model_UpdateFullTextSearch();
		$modelUpdateFullTextSearch->cronJobUpdateFullTextSearch();
		 echo json_encode(array('success' => 1));
        
        exit;
	}
	
	public function updateFullTextSearchByTypeAndTypeIdAction() {
		$type = $this->request->getParam('type', 'booking');
        $type_id = $this->request->getParam('type_id', 0);
        
		$modelUpdateFullTextSearch = new Model_UpdateFullTextSearch();
		$modelUpdateFullTextSearch->UpdateFullTextSearchByTypeAndTypeId($type, $type_id);
		echo json_encode(array('success' => 1));
        
        exit;
	}
	

}