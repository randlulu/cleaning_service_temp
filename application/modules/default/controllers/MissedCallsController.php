<?php

class MissedCallsController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
        
        $pageName = $this->request->getParam('page_name', '');
        $this->view->page_title = (!empty($pageName))?$pageName." - Messages":"Messages";
    }

    public function indexAction() {
        //check Auth for logged user
        
        
        
		CheckAuth::checkPermission(array('missedCall'));


        $loggedUser = CheckAuth::getLoggedUser();

        //get request parameters
        $orderBy = $this->request->getParam('sort', 'created');
        $sortingMethod = $this->request->getParam('method', 'desc');
        $currentPage = $this->request->getParam('page', 1);
        $isRead = $this->request->getParam('is_read');

        $filters = array();
        $filters['is_read'] = $isRead;
        $filters['user_id'] = $loggedUser['user_id'];


        //init pager and articles model object
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];


        //Load Model
        $modelMissedCalls = new Model_MissedCalls();

        //get data list
        $data = $modelMissedCalls->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);
        $modelMissedCalls->fills($data, array('customer', 'created_by'));

        //set view params
        $this->view->data = $data;
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
        $this->view->sub_menu = !empty($isRead) ? (($isRead == 'yes') ? 'read_missedCalls' : 'unread_missedCalls') : 'missedCalls';
    }

    /**
     * add new item action
     */
    public function addAction() {

        $this->view->page_title = "New Message";
        /**
         * check Auth for logged user
         */
        CheckAuth::checkPermission(array('missedCallAdd'));

        $customerId = $this->request->getParam('customer_id');
        $employeeId = $this->request->getParam('employee_id');
        $comment = $this->request->getParam('comment');



        // Load Model
        $modelMissedCalls = new Model_MissedCalls();
        $modelAuthRole = new Model_AuthRole();

        $form = new Form_MissedCalls();

        $this->view->form = $form;

        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) {

                $loggedUser = CheckAuth::getLoggedUser();
                $authRole = $modelAuthRole->getById($loggedUser->role_id);
                $defaultPage = $authRole['default_page'];

                $data = array(
                    'customer_id' => $customerId,
                    'user_id' => $employeeId,
                    'comment' => $comment,
                    'created' => time(),
                    'created_by' => $loggedUser['user_id']
                );
                $success = $modelMissedCalls->insert($data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved"));
                    $this->_redirect($defaultPage);
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                    $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
                }
            }
        }
    }

    public function viewAction() {

        //check Auth for logged user
        CheckAuth::checkPermission(array('missedCall'));

        $id = $this->request->getParam('id');

        //Load Model
        $modelMissedCalls = new Model_MissedCalls();

        $missedCalls = $modelMissedCalls->getById($id);
        $modelMissedCalls->fill($missedCalls, array('customer', 'created_by'));
        $this->view->missedCalls = $missedCalls;


        $modelMissedCalls->updateById($id, array('is_read' => 1));
    }

    public function missedCallSentAction() {
        //check Auth for logged user
        CheckAuth::checkPermission(array('missedCallSent'));


        $loggedUser = CheckAuth::getLoggedUser();

        //get request parameters
        $orderBy = $this->request->getParam('sort', 'created');
        $sortingMethod = $this->request->getParam('method', 'desc');
        $currentPage = $this->request->getParam('page', 1);
       

        $filters = array();
        $filters['created'] = $loggedUser['user_id'];


        //init pager and articles model object
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];


        //Load Model
        $modelMissedCalls = new Model_MissedCalls();

        //get data list
        $data = $modelMissedCalls->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);
        $modelMissedCalls->fills($data, array('customer', 'user_id'));

        //set view params
        $this->view->data = $data;
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
        $this->view->sub_menu = 'missedCallSent';
    }

}