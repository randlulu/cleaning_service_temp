<?php

class IndexController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {

        /* Initialize action controller here */
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
    }

    public function indexAction() {

        if (Zend_Auth::getInstance()->hasIdentity()) {
            $loggedUser = Zend_Auth::getInstance()->getIdentity();
            $modelAuthRole = new Model_AuthRole();
            $authRole = $modelAuthRole->getById($loggedUser->role_id);
            $defaultPage = $authRole['default_page'];

            $messages = $this->_helper->flashMessenger->getMessages();
            if (count($messages)) {
                foreach ($messages as $message) {
                    $this->_helper->flashMessenger->addMessage(array('type' => $message['type'], 'message' => $message['message']));
                }
            }

            $this->_redirect($defaultPage);
        }

        //
        // get request parameters
        //
        $keepSign = $this->request->getParam('keep_sign', 0);
        $email = $this->request->getParam('email');
        $password = $this->request->getParam('password');

        //
        // init action form
        //
        $form = new Form_Login();

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $authrezed = $this->getAuthrezed();

                $authrezed->setIdentity($email);
                $authrezed->setCredential(sha1($password));

                $auth = Zend_Auth::getInstance();
                $result = $auth->authenticate($authrezed);

                if ($result->isValid()) {
                    $identity = $authrezed->getResultRowObject();

                    $authStorge = $auth->getStorage();
                    $authStorge->write($identity);

                    if ($keepSign) {
                        Zend_Session::rememberMe();
                    }

                    CheckAuth::afterlogin();
                } else {
                    $form->getElement('email')->setErrors(array('invalid email or password'));
                }
            }
        }

        $this->view->form = $form;
    }

    public function getAuthrezed() {
        $authrezed = new Zend_Auth_Adapter_DbTable(Zend_Db_Table::getDefaultAdapter());
        $authrezed->setTableName('user')
                ->setIdentityColumn('email1')
                ->setCredentialColumn('password')
                ->setCredentialTreatment('? AND active = "TRUE"');

        return $authrezed;
    }

    public function logoutAction() {
        CheckAuth::logout();
        $this->_redirect($this->router->assemble(array(), 'Login'));
    }

    public function myAccountAction() {
        //
        //check login
        //
        CheckAuth::checkLoggedIn();

        $loggedUser = CheckAuth::getLoggedUser();

        $modelUser = new Model_User();
        $user = $modelUser->getById($loggedUser['user_id']);
        if ($user) {
            $this->view->user = $user;

            $modelAuthRole = new Model_AuthRole();
            $role = $modelAuthRole->getById($user['role_id']);
            $this->view->role = $role;


            if ('contractor' == $role['role_name']) {
                $contractorInfoObj = new Model_ContractorInfo();
                $contractorInfo = $contractorInfoObj->getByContractorId($loggedUser['user_id']);

                $this->view->contractorInfo = $contractorInfo;

                if ($contractorInfo) {
                    //get contractorOwner
                    $contractorOwnerObj = new Model_ContractorOwner();
                    $contractorOwner = $contractorOwnerObj->getAll(array('contractor_info_id' => $contractorInfo['contractor_info_id']));

                    $this->view->contractorOwner = $contractorOwner;

                    //get contractorEmployee
                    $contractorEmployeeObj = new Model_ContractorEmployee();
                    $contractorEmployee = $contractorEmployeeObj->getAll(array('contractor_info_id' => $contractorInfo['contractor_info_id']));

                    $this->view->contractorEmployee = $contractorEmployee;

                    //get DeclarationOfChemicals
                    $declarationOfChemicalsObj = new Model_DeclarationOfChemicals();
                    $declarationOfChemicals = $declarationOfChemicalsObj->getByContractorInfoId($contractorInfo['contractor_info_id']);

                    $this->view->declarationOfChemicals = $declarationOfChemicals;

                    //get DeclarationOfEquipment
                    $declarationOfEquipmentObj = new Model_DeclarationOfEquipment();
                    $declarationOfEquipment = $declarationOfEquipmentObj->getByContractorInfoId($contractorInfo['contractor_info_id']);

                    $this->view->declarationOfEquipment = $declarationOfEquipment;

                    //get DeclarationOfOtherApparatus
                    $declarationOfOtherApparatusObj = new Model_DeclarationOfOtherApparatus();
                    $declarationOfOtherApparatus = $declarationOfOtherApparatusObj->getByContractorInfoId($contractorInfo['contractor_info_id']);

                    $this->view->declarationOfOtherApparatus = $declarationOfOtherApparatus;

                    //get Vehicle
                    $contractorVehicleObj = new Model_ContractorVehicle();
                    $contractorVehicle = $contractorVehicleObj->getByContractorInfoId($contractorInfo['contractor_info_id']);

                    $this->view->contractorVehicle = $contractorVehicle;
                }
            } else {
                $userInfoObj = new Model_UserInfo();
                $userInfo = $userInfoObj->getByUserId($loggedUser['user_id']);

                $this->view->userInfo = $userInfo;
            }
        } else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }
    }

    public function changeEmailAction() {
        //
        //check login
        //
        CheckAuth::checkLoggedIn();

        //
        // get request parameters
        //
        $old_email = $this->request->getParam('old_email');
        $new_email = $this->request->getParam('new_email');
        $confirm_new_email = $this->request->getParam('confirm_new_email');

        $loggedUser = CheckAuth::getLoggedUser();

        $modelUser = new Model_User();
        $user = $modelUser->getById($loggedUser['user_id']);

        //
        // init action form
        //
        $form = new Form_ChangeEmail(array('user' => $user));
        $this->view->form = $form;

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $old_user_email = $modelUser->getByEmail($old_email);
                if ($loggedUser['user_id'] == $old_user_email['user_id']) {
                    if ($new_email == $confirm_new_email) {
                        $modelUser = new Model_User();

                        $data = array(
                            'temp_email' => $new_email
                        );
                        $modelUser->updateById($user['user_id'], $data);

                        //create code
                        $activation_code = sha1(uniqid());

                        //save code
                        $params = array(
                            'code' => $activation_code,
                            'created' => time(),
                            'type' => 'change_email',
                            'ip_address' => $_SERVER['REMOTE_ADDR'],
                            'user_id' => $user['user_id']
                        );
                        $modelCode = new Model_Code();
                        $modelCode->insert($params);

                        $activation_link = $this->router->assemble(array('code' => $activation_code, 'id' => $user['user_id']), 'changeEmailStep2');

                        $template_params = array(
                            '{username}' => ucwords($user['username']),
                            '{new_email}' => $new_email,
                            '{activation_link}' => '<a href="' . $activation_link . '">' . $activation_link . '</a>'
                        );

                        //send email
                        $sucess = EmailNotification::sendEmail(array('to' => $user['email1']), 'change_email', $template_params);

                        if ($sucess) {
                            $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Please Check Your Email address and Complete the Instruction"));
                        } else {
                            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Could Not Send change Email"));
                        }

                        $this->_redirect($this->router->assemble(array(), 'myAccount'));
                    } else {
                        $form->getElement('confirm_new_email')->setErrors(array('new email and confirm new email not match'));
                    }
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
                    $this->_redirect($this->router->assemble(array(), 'settingsUserList'));
                }
            }
        }
    }

    public function changeEmailStep2Action() {
        //
        // get request parameters
        //
        $code = $this->request->getParam('code');
        $id = $this->request->getParam('id');

        $modelCode = new Model_Code();
        $modelUser = new Model_User();

        $userCode = $modelCode->getByCode($code);
        $user = $modelUser->getById($id);

        if (empty($user) || empty($userCode)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Can't Change Email!"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }

        $expirationTime = $userCode['created'] + (24 * 60 * 60);
        if (time() > $expirationTime) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Can't Change Email!, Your Change Email Link has been expired ."));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }

        if ($user['user_id'] != $userCode['user_id']) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Can't Change Email!"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }

        if ('change_email' != $userCode['type']) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Can't Change Email!"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }

        $data = array(
            'email1' => $user['temp_email'],
            'temp_email' => ''
        );
        $sucess = $modelUser->updateById($user['user_id'], $data);

        //delete code
        $modelCode->deleteById($userCode['id']);

        if ($sucess) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Your Email is Changed successfully"));
        } else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Could Not Change Your Email"));
        }


        if (Zend_Auth::getInstance()->hasIdentity()) {
            $this->_redirect($this->router->assemble(array(), 'myAccount'));
        } else {
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }
    }

    public function forgetPasswordAction() {

        //
        // get request parameters
        //
        $email = $this->request->getParam('email');

        //
        // init action form
        //
        $form = new Form_ForgetPassword();

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $modelUser = new Model_User();
                $user = $modelUser->getByEmail($email);
                if ($user) {

                    //create code
                    $activation_code = sha1(uniqid());

                    //save code
                    $params = array(
                        'code' => $activation_code,
                        'created' => time(),
                        'type' => 'forgot_password',
                        'ip_address' => $_SERVER['REMOTE_ADDR'],
                        'user_id' => $user['user_id']
                    );
                    $modelCode = new Model_Code();
                    $modelCode->insert($params);

                    $activation_link = $this->router->assemble(array('code' => $activation_code, 'id' => $user['user_id']), 'forgetPasswordStep2');

                    $template_params = array(
                        '{username}' => ucwords($user['username']),
                        '{activation_link}' => '<a href="' . $activation_link . '">' . $activation_link . '</a>'
                    );

                    //send email
                    $sucess = EmailNotification::sendEmail(array('to' => $email), 'forget_password', $template_params);

                    if ($sucess) {
                        $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Please Check Your Email address and Complete the Instruction"));
                    } else {
                        $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Could Not Send Reset Passward Email"));
                    }

                    $this->_redirect($this->router->assemble(array(), 'Login'));
                }
            }
        }
        $this->view->form = $form;
    }

    public function forgetPasswordStep2Action() {
        //
        // get request parameters
        //
        $code = $this->request->getParam('code');
        $id = $this->request->getParam('id');

        $modelCode = new Model_Code();
        $modelUser = new Model_User();

        $userCode = $modelCode->getByCode($code);
        $user = $modelUser->getById($id);

        if (empty($user) || empty($userCode)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Can't Reset Passward!"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }

        $expirationTime = $userCode['created'] + (24 * 60 * 60);
        if (time() > $expirationTime) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Can't Reset Passward!, Your Reset Passward Link has been expired ."));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }

        if ($user['user_id'] != $userCode['user_id']) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Can't Reset Passward!"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }

        if ('forgot_password' != $userCode['type']) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Can't Reset Passward!"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }

        //
        // init action form
        //
        $form = new Form_ForgetPasswordStep2();

        $password = $this->request->getParam('password');
        $password_confirm = $this->request->getParam('password_confirm');

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                if ($password == $password_confirm) {

                    $data = array(
                        'password' => sha1($password)
                    );
                    $modelUser->updateById($user['user_id'], $data);

                    //delete code
                    $modelCode->deleteById($userCode['id']);

                    $this->_redirect($this->router->assemble(array(), 'Login'));
                } else {
                    $form->getElement('password_confirm')->setErrors(array('password and confirm password not match'));
                }
            }
        }
        $this->view->form = $form;
    }

    public function changeAccountInfoAction() {

        //
        //chech if the user loged in or not
        //
        CheckAuth::checkLoggedIn();

        //
        // get request parameters
        //
        $username = $this->request->getParam('username');
        $email2 = $this->request->getParam('email2');
        $email3 = $this->request->getParam('email3');
        $mobile1 = $this->request->getParam('mobile1');
        $mobile2 = $this->request->getParam('mobile2');
        $mobile3 = $this->request->getParam('mobile3');
        $phone1 = $this->request->getParam('phone1');
        $phone2 = $this->request->getParam('phone2');
        $phone3 = $this->request->getParam('phone3');
        $fax = $this->request->getParam('fax');
        $emergencyPhone = $this->request->getParam('emergency_phone');
        $unitLotNumber = $this->request->getParam('unit_lot_number');
        $streetNumber = $this->request->getParam('street_number');
        $streetAddress = $this->request->getParam('street_address');
        $suburb = $this->request->getParam('suburb');
        $state = $this->request->getParam('state');
        $postcode = $this->request->getParam('postcode');
        $po_box = $this->request->getParam('po_box');
        $cityId = $this->request->getParam('city_id');
        $countryId = $this->request->getParam('country_id');
        $companyId = $this->request->getParam('company_id');

        if (get_config('remove_white_spacing')) {
            $mobile1 = preparer_number($mobile1);
            $mobile2 = preparer_number($mobile2);
            $mobile3 = preparer_number($mobile3);
            $phone1 = preparer_number($phone1);
            $phone2 = preparer_number($phone2);
            $phone3 = preparer_number($phone3);
        }

        $loggedUser = CheckAuth::getLoggedUser();
        //
        // validation
        //
        $modelUser = new Model_User();
        $user = $modelUser->getById($loggedUser['user_id']);
        if (!$user) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
            return;
        }

        //
        //get user company
        //
        $userCompaniesModel = new Model_UserCompanies();
        $userCompanies = $userCompaniesModel->getByUserId($user['user_id']);


        //
        // init action form
        //
        $form = new Form_ChangeAccountInfo(array('user' => $user, 'country_id' => $countryId, 'company_id' => $userCompanies['company_id']));
        $this->view->form = $form;

        //
        // handling the updating process
        //
       if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'username' => $username,
                    'city_id' => $cityId,
                    'email2' => $email2,
                    'email3' => $email3,
                    'mobile1' => $mobile1,
                    'mobile2' => $mobile2,
                    'mobile3' => $mobile3,
                    'phone1' => $phone1,
                    'phone2' => $phone2,
                    'phone3' => $phone3,
                    'fax' => $fax,
                    'emergency_phone' => $emergencyPhone,
                    'unit_lot_number' => $unitLotNumber,
                    'street_number' => $streetNumber,
                    'street_address' => $streetAddress,
                    'suburb' => $suburb,
                    'state' => $state,
                    'postcode' => $postcode,
                    'po_box' => $po_box
                );


                $success = $modelUser->updateById($user['user_id'], $data);


                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in User"));
                }


                //$this->_redirect($this->router->assemble(array(), 'myAccount'));
                echo 1;
                exit;
            }
        }

        echo $this->view->render('my-account/change-account-info.phtml');
        exit;
    }

    //
    //change password
    //
    public function changePasswordAction() {

        CheckAuth::checkLoggedIn();

        $loggedUser = CheckAuth::getLoggedUser();

        $modelUser = new Model_User();
        $user = $modelUser->getById($loggedUser['user_id']);
        if (!$user) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
            return;
        }

        $old_password = $this->request->getParam('old_password');
        $new_password = $this->request->getParam('new_password');
        $confirm_new_password = $this->request->getParam('confirm_new_password');

        $form = new Form_ChangePassword(array('user' => $user));
        $this->view->form = $form;

        if ($this->request->isPost()) {
            if ($form->isValid($this->request->getPost())) {

                $data = array(
                    'password' => sha1($new_password)
                );
                $modelUser->updateById($user['user_id'], $data);

                $this->_redirect($this->router->assemble(array(), 'Login'));
            }
        }
    }

    public function modfyUserInfoAction() {

        //
        //check login
        //
        CheckAuth::checkLoggedIn();

        if (!CheckAuth::checkCredential(array('canModifyUserInfo'))) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }

        $loggedUser = CheckAuth::getLoggedUser();

        $userInfoObj = new Model_UserInfo();
        $userInfo = $userInfoObj->getByUserId($loggedUser['user_id']);

        //
        // get request parameters
        //
        $firstName = $this->request->getParam('first_name');
        $lastName = $this->request->getParam('last_name');


        //
        // init action form
        //
        if (!$userInfo) {
            $form = new Form_UserInfo(array('user_id' => $loggedUser['user_id']));
        } else {
            $form = new Form_UserInfo(array('user_id' => $loggedUser['user_id'], 'userInfo' => $userInfo));
        }

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $userInfoObj = new Model_UserInfo();
                $data = array(
                    'first_name' => $firstName,
                    'last_name' => $lastName
                );

                if (!$userInfo) {
                    $data['user_id'] = $loggedUser['user_id'];
                    $success = $userInfoObj->insert($data);
                } else {
                    $success = $userInfoObj->updateById($userInfo['user_info_id'], $data);
                }

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in User Info"));
                }
                //$this->_redirect($this->router->assemble(array(), 'settingsUserEmailList'));

                echo 1;
                exit;
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        
        echo $this->view->render('my-account/user_info.phtml');
        exit;
    }

    public function modfyContractorInfoAction() {

        //
        //check login
        //
        CheckAuth::checkLoggedIn();

        if ('contractor' != CheckAuth::getRoleName()) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }

        $loggedUser = CheckAuth::getLoggedUser();

        $contractorInfoObj = new Model_ContractorInfo();
        $contractorInfo = $contractorInfoObj->getByContractorId($loggedUser['user_id']);

        //
        // get request parameters
        //
        $business_name = $this->request->getParam('business_name');
        $acn = $this->request->getParam('acn');
        $abn = $this->request->getParam('abn');
        $tfn = $this->request->getParam('tfn');
        $gst = $this->request->getParam('gst');
        $gst_date_registered = $this->request->getParam('gst_date_registered');
        $insurance_policy_number = $this->request->getParam('insurance_policy_number');
        $insurance_policy_start = $this->request->getParam('insurance_policy_start');
        $insurance_policy_expiry = $this->request->getParam('insurance_policy_expiry');
        $insurance_listed_services_covered = $this->request->getParam('insurance_listed_services_covered');
        $drivers_licence_number = $this->request->getParam('drivers_licence_number');
        $drivers_licence_expiry = $this->request->getParam('drivers_licence_expiry');


        //
        // init action form
        //
        if (!$contractorInfo) {
            $form = new Form_ContractorInfo(array('contractor_id' => $loggedUser['user_id']));
        } else {
            $form = new Form_ContractorInfo(array('contractor_id' => $loggedUser['user_id'], 'contractorInfo' => $contractorInfo));
        }

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'business_name' => $business_name,
                    'acn' => $acn,
                    'abn' => $abn,
                    'tfn' => $tfn,
                    'gst' => $gst,
                    'gst_date_registered' => strtotime($gst_date_registered),
                    'insurance_policy_number' => $insurance_policy_number,
                    'insurance_policy_start' => $insurance_policy_start,
                    'insurance_policy_expiry' => $insurance_policy_expiry,
                    'insurance_listed_services_covered' => $insurance_listed_services_covered,
                    'drivers_licence_number' => $drivers_licence_number,
                    'drivers_licence_expiry' => $drivers_licence_expiry,
                );

                if (!$contractorInfo) {
                    $data['contractor_id'] = $loggedUser['user_id'];
                    $success = $contractorInfoObj->insert($data);
                } else {
                    $success = $contractorInfoObj->updateById($contractorInfo['contractor_info_id'], $data);
                }

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Contractor Info"));
                }

                echo 1;
                exit;
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        
        echo $this->view->render('my-account/contractor_info.phtml');
        exit;
    }

    public function pageNotFoundAction() {
        
    }

    public function cancelBookingAction() {

        //get request parameters
        $bookingId = $this->request->getParam('booking_id');
        $cancelHashcode = $this->request->getParam('cancel_hashcode');
        $statusId = $this->request->getParam('status_id');

        $modelBooking = new Model_Booking();
        $booking = $modelBooking->getById($bookingId);

        $modelBookingStatus = new Model_BookingStatus();
        $bookingStatus = $modelBookingStatus->getById($statusId);

        if (!$booking) {
            $this->_redirect($this->router->assemble(array(), 'pageNotFound'));
        }
        if (!$bookingStatus) {
            $this->_redirect($this->router->assemble(array(), 'pageNotFound'));
        }
        if ($booking['status_id'] != $statusId) {
            $this->_redirect($this->router->assemble(array(), 'pageNotFound'));
        }
        if ($cancelHashcode != $booking['cancel_hashcode']) {
            $this->_redirect($this->router->assemble(array(), 'pageNotFound'));
        }

        //CANCELL QUOTED
        $quoted = $modelBookingStatus->getByStatusName('QUOTED');
        if ($quoted['booking_status_id'] == $statusId) {
            $modelBookingEstimate = new Model_BookingEstimate();
            $estimate = $modelBookingEstimate->getByBookingId($bookingId);
            if ($estimate) {
                if ($estimate['estimate_type'] == 'draft') {
                    $modelBookingEstimate->updateById($estimate['id'], array('is_deleted' => 1));
                }
            }
        }

        //CANCELL ON HOLD
        $onHold = $modelBookingStatus->getByStatusName('ON HOLD');
        if ($onHold['booking_status_id'] == $statusId) {
            $cancelled = $modelBookingStatus->getByStatusName('CANCELLED');
            $modelBooking->updateById($bookingId, array('status_id' => $cancelled['booking_status_id']));
        }

        //CANCELL TENTATIVE
        $tentative = $modelBookingStatus->getByStatusName('TENTATIVE');
        if ($tentative['booking_status_id'] == $statusId) {
            $cancelled = $modelBookingStatus->getByStatusName('CANCELLED');
            $modelBooking->updateById($bookingId, array('status_id' => $cancelled['booking_status_id']));
        }

        $modelCompanies = new Model_Companies();
        $company = $modelCompanies->getById($booking['company_id']);
        $this->view->company = $company;

        $modelBooking->updateById($bookingId, array('cancel_hashcode' => sha1(uniqid())));
    }

    public function feedbackBookingAction() {

        //get request parameters
        $bookingId = $this->request->getParam('booking_id');
        $feedbackHashcode = $this->request->getParam('feedback_hashcode');
        $statusId = $this->request->getParam('status_id');
        $feedback = $this->request->getParam('feedback', '');

        $modelBooking = new Model_Booking();
        $booking = $modelBooking->getById($bookingId);

        $modelCustomerFeedback = new Model_CustomerFeedback();
        $customerFeedback = $modelCustomerFeedback->getBybookingAndstatusId($bookingId, $statusId);

        if (!$booking) {
            $this->_redirect($this->router->assemble(array(), 'pageNotFound'));
        }

        if ($customerFeedback) {
            if ($feedbackHashcode != $customerFeedback['hashcode']) {
                $this->_redirect($this->router->assemble(array(), 'pageNotFound'));
            }
        } else {
            $this->_redirect($this->router->assemble(array(), 'pageNotFound'));
        }

        $modelCompanies = new Model_Companies();
        $company = $modelCompanies->getById($booking['company_id']);
        $this->view->company = $company;
        $this->view->feedback_hashcode = $feedbackHashcode;
        $this->view->booking_id = $bookingId;
        $this->view->status_id = $statusId;

        if ($this->request->isPost()) {
            $feedback_hashcode = sha1(uniqid());
            $this->view->suceess = true;
            $modelCustomerFeedback->updateById($customerFeedback['id'], array('feedback' => $feedback, 'hashcode' => $feedback_hashcode));
        }
    }

}

