<?php

class DashboardController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        /* Initialize action controller here */
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
        
        $pageName = $this->request->getParam('page_name', '');
        $this->view->page_title = (!empty($pageName))?$pageName." - Dashboard":"Dashboard";

    }

    public function indexAction() {
//        var_dump(Zend_Auth::getInstance()->getIdentity());        
        $this->view->sub_menu = 'future_bookings';
        
        $this->view->page_title = 'Future Bookings - '.$this->view->page_title;

        CheckAuth::checkPermission(array('dashboard'));

        //check login
        CheckAuth::checkLoggedIn();

        //
        // load model
        //
        $modelUser = new Model_User();
        $modelBooking = new Model_Booking();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelContractorInfo = new Model_ContractorInfo();
        $modelContractorEmployee = new Model_ContractorEmployee();

        //get logged user
        $loggedUser = CheckAuth::getLoggedUser();

        // role of logged user
        $role = CheckAuth::getRoleName();
        $this->view->role = $role;


        //logged user details
        $user = $modelUser->getById($loggedUser['user_id']);
        $this->view->user = $user;


        if (!$user) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }


        //#############################//
        //Contractor Info and employees//
        //#############################//
        $contractorInfo = $modelContractorInfo->getByContractorId($loggedUser['user_id']);
        $this->view->contractorInfo = $contractorInfo;

        $contractorEmployee = $modelContractorEmployee->getByContractorInfoId($contractorInfo['contractor_info_id']);
        $this->view->contractorEmployee = $contractorEmployee;

        //##########################//
        //        Statistics        //
        //##########################//
        //
        //Get Count Complete and inComplete
        //
        $myDashboard = false;

        if (!CheckAuth::getDashboardStatusSession()) {
            if (CheckAuth::checkCredential(array('canSeeAllBookingStatistics'))) {
                CheckAuth::setDashboardStatusSession('all_dashboard');
            } else {
                CheckAuth::setDashboardStatusSession('my_dashboard');
            }
        }

        $dasboardStatus = CheckAuth::getDashboardStatusSession();
        if ($dasboardStatus == 'my_dashboard') {
            $myDashboard = true;
        }

        $filters = array();
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $totalBooking = $modelBooking->getBookingCount($filters);
        $this->view->totalBooking = $totalBooking;

        $modelBookingStatus = new Model_BookingStatus();
        $allBookingStatus = $modelBookingStatus->getAll();

        foreach ($allBookingStatus as &$bookingStatus) {
            $filters['status'] = $bookingStatus['booking_status_id'];
            $bookingStatus['total'] = $modelBooking->getBookingCount($filters);
        }

        $this->view->allBookingStatus = $allBookingStatus;



        //get invoice information
        $modelBooking = new Model_Booking();
        $filters = array();
        $filters['invoice_type'] = 'unpaid';
        $filters['convert_status'] = 'invoice';
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }

        $totalQoute = $modelBooking->total($filters, 'qoute');
        $totalPaidAmount = $modelBooking->total($filters, 'paid_amount');

        $invoicesTotalAmount = $totalQoute - $totalPaidAmount;

        $this->view->invoicesTotalAmount = $invoicesTotalAmount;

        $modelBookingInvoice = new Model_BookingInvoice();
        $filters = array();
        $filters['invoice_type'] = 'unpaid';
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $invoices = $modelBookingInvoice->getAll($filters);
        $modelBookingInvoice->fills($invoices, array('number_of_due_days', 'paid_amount'));

        $due15 = 0;
        $due30 = 0;
        $due45 = 0;
        $dueAbove = 0;

        if ($invoices) {
            foreach ($invoices AS $invoice) {
                if ($invoice['due'] <= 15) {
                    $newdue15 = $invoice['amount'] - $invoice['paid_amount'];
                    $due15 += $newdue15;
                } elseif ($invoice['due'] <= 30) {
                    $newdue30 = $invoice['amount'] - $invoice['paid_amount'];
                    $due30 += $newdue30;
                } elseif ($invoice['due'] <= 45) {
                    $newdue45 = $invoice['amount'] - $invoice['paid_amount'];
                    $due45 += $newdue45;
                } else {
                    $newdueAbove = $invoice['amount'] - $invoice['paid_amount'];
                    $dueAbove += $newdueAbove;
                }
            }
        }

        $this->view->due15 = $due15;
        $this->view->due30 = $due30;
        $this->view->due45 = $due45;
        $this->view->dueAbove = $dueAbove;




        //
        //Future Bookings 
        // TO DO,  IN PROGRESS, TO VISIT,  TENTATIVE, ON HOLD
        //$futureBookingStatus = $modelBookingStatus->getAll(array('more_one_status_name' => array('IN PROGRESS', 'TO DO', 'ON HOLD', 'TENTATIVE', 'TO VISIT')));
        $futureBookingStatus = $modelBookingStatus->getAll(array('more_one_status_name' => array('TO DO', 'IN PROGRESS', 'TO VISIT', 'TENTATIVE', 'ON HOLD')));
        //, 'CANCELLED'
        $this->view->futureBookingStatus = $futureBookingStatus;

        // Future Today
        $today = getTimePeriodByName('today');

        $filters = array();
        $filters['booking_start_between'] = date('Y-m-d H:i:s', time());
        $filters['booking_end_between'] = $today['end'];
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $todayAllBookingStatus = $futureBookingStatus;
        foreach ($todayAllBookingStatus as &$todayBookingStatus) {
            $filters['status'] = $todayBookingStatus['booking_status_id'];
            $todayBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $todayBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');

            $url = $this->router->assemble(array(), 'booking');
            $todayBookingStatus['url'] = FilterLink::generate_link('(' . $todayBookingStatus['count'] . ')', array('fltr' => $filters), false, true, $url);
        }
        $this->view->todayAllBookingStatus = $todayAllBookingStatus;


        // Future Tomorow
        $tomorrow = getTimePeriodByName('tomorrow');

        $filters = array();
        $filters['booking_start_between'] = $tomorrow['start'];
        $filters['booking_end_between'] = $tomorrow['end'];
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $tomorowAllBookingStatus = $futureBookingStatus;
        foreach ($tomorowAllBookingStatus as &$tomorowBookingStatus) {
            $filters['status'] = $tomorowBookingStatus['booking_status_id'];
            $tomorowBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $tomorowBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');
            $url = $this->router->assemble(array(), 'booking');
            $tomorowBookingStatus['url'] = FilterLink::generate_link('(' . $tomorowBookingStatus['count'] . ')', array('fltr' => $filters), false, true, $url);
        }
        $this->view->tomorowAllBookingStatus = $tomorowAllBookingStatus;


        //Future Week
        $this_week = getTimePeriodByName('this_week');

        $filters = array();
        $filters['booking_start_between'] = date('Y-m-d H:i:s', time());
        $filters['booking_end_between'] = $this_week['end'];
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $weekAllBookingStatus = $futureBookingStatus;
        foreach ($weekAllBookingStatus as &$weekBookingStatus) {
            $filters['status'] = $weekBookingStatus['booking_status_id'];
            $weekBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $weekBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');
            $url = $this->router->assemble(array(), 'booking');
            $weekBookingStatus['url'] = FilterLink::generate_link('(' . $weekBookingStatus['count'] . ')', array('fltr' => $filters), false, true, $url);
        }
        $this->view->weekAllBookingStatus = $weekAllBookingStatus;


        // Future Month
        $this_month = getTimePeriodByName('this_month');

        $filters = array();
        $filters['booking_start_between'] = date('Y-m-d H:i:s', time());
        $filters['booking_end_between'] = $this_month['end'];
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $monthAllBookingStatus = $futureBookingStatus;
        foreach ($monthAllBookingStatus as &$monthBookingStatus) {
            $filters['status'] = $monthBookingStatus['booking_status_id'];
            $monthBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $monthBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');
            $url = $this->router->assemble(array(), 'booking');
            $monthBookingStatus['url'] = FilterLink::generate_link('(' . $monthBookingStatus['count'] . ')', array('fltr' => $filters), false, true, $url);
        }
        $this->view->monthAllBookingStatus = $monthAllBookingStatus;


        // Future quarter
        $this_quarter = getTimePeriodByName('this_quarter');

        $filters = array();
        $filters['booking_start_between'] = date('Y-m-d H:i:s', time());
        $filters['booking_end_between'] = $this_quarter['end'];
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $quarterAllBookingStatus = $futureBookingStatus;
        foreach ($quarterAllBookingStatus as &$quarterBookingStatus) {
            $filters['status'] = $quarterBookingStatus['booking_status_id'];
            $quarterBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $quarterBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');
            $url = $this->router->assemble(array(), 'booking');
            $quarterBookingStatus['url'] = FilterLink::generate_link('(' . $quarterBookingStatus['count'] . ')', array('fltr' => $filters), false, true, $url);
        }
        $this->view->quarterAllBookingStatus = $quarterAllBookingStatus;


        // Future Next week
        $next_week = getTimePeriodByName('next_week');

        $filters = array();
        $filters['booking_start_between'] = $next_week['start'];
        $filters['booking_end_between'] = $next_week['end'];
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $nextWeekAllBookingStatus = $futureBookingStatus;
        foreach ($nextWeekAllBookingStatus as &$nextWeekBookingStatus) {
            $filters['status'] = $nextWeekBookingStatus['booking_status_id'];
            $nextWeekBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $nextWeekBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');
            $url = $this->router->assemble(array(), 'booking');
            $nextWeekBookingStatus['url'] = FilterLink::generate_link('(' . $nextWeekBookingStatus['count'] . ')', array('fltr' => $filters), false, true, $url);
        }
        $this->view->nextWeekAllBookingStatus = $nextWeekAllBookingStatus;


        // Future Next Month
        $next_month = getTimePeriodByName('next_month');

        $filters = array();
        $filters['booking_start_between'] = $next_month['start'];
        $filters['booking_end_between'] = $next_month['end'];
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $nextMonthAllBookingStatus = $futureBookingStatus;
        foreach ($nextMonthAllBookingStatus as &$nextMonthBookingStatus) {
            $filters['status'] = $nextMonthBookingStatus['booking_status_id'];
            $nextMonthBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $nextMonthBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');
            $url = $this->router->assemble(array(), 'booking');
            $nextMonthBookingStatus['url'] = FilterLink::generate_link('(' . $nextMonthBookingStatus['count'] . ')', array('fltr' => $filters), false, true, $url);
        }
        $this->view->nextMonthAllBookingStatus = $nextMonthAllBookingStatus;

        // Future Next Quarter
        $next_quarter = getTimePeriodByName('next_quarter');

        $filters = array();
        $filters['booking_start_between'] = $next_quarter['start'];
        $filters['booking_end_between'] = $next_quarter['end'];
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $nextQuarterAllBookingStatus = $futureBookingStatus;
        foreach ($nextQuarterAllBookingStatus as &$nextQuarterBookingStatus) {
            $filters['status'] = $nextQuarterBookingStatus['booking_status_id'];
            $nextQuarterBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $nextQuarterBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');
            $url = $this->router->assemble(array(), 'booking');
            $nextQuarterBookingStatus['url'] = FilterLink::generate_link('(' . $nextQuarterBookingStatus['count'] . ')', array('fltr' => $filters), false, true, $url);
        }
        $this->view->nextQuarterAllBookingStatus = $nextQuarterAllBookingStatus;


        //all future Sales
        $filters = array();
        $filters['booking_start'] = date('Y-m-d H:i:s', time());
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $allFutureBookingStatus = $futureBookingStatus;
        foreach ($allFutureBookingStatus as &$futureBookingStatus) {
            $filters['status'] = $futureBookingStatus['booking_status_id'];
            $futureBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $futureBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');
            $url = $this->router->assemble(array(), 'booking');
            $futureBookingStatus['url'] = FilterLink::generate_link('(' . $futureBookingStatus['count'] . ')', array('fltr' => $filters), false, true, $url);
        }
        $this->view->allFutureBookingStatus = $allFutureBookingStatus;
    }

    public function futureAction() {

        $this->view->sub_menu = 'future_bookings';
        
        $this->view->page_title = 'Future Bookings - '.$this->view->page_title;

        CheckAuth::checkPermission(array('dashboard'));

        //check login
        CheckAuth::checkLoggedIn();

        //
        // load model
        //
        $modelUser = new Model_User();
        $modelBooking = new Model_Booking();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelContractorInfo = new Model_ContractorInfo();
        $modelContractorEmployee = new Model_ContractorEmployee();

        //get logged user
        $loggedUser = CheckAuth::getLoggedUser();

        // role of logged user
        $role = CheckAuth::getRoleName();
        $this->view->role = $role;


        //logged user details
        $user = $modelUser->getById($loggedUser['user_id']);
        $this->view->user = $user;


        if (!$user) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }


        //#############################//
        //Contractor Info and employees//
        //#############################//
        $contractorInfo = $modelContractorInfo->getByContractorId($loggedUser['user_id']);
        $this->view->contractorInfo = $contractorInfo;

        $contractorEmployee = $modelContractorEmployee->getByContractorInfoId($contractorInfo['contractor_info_id']);
        $this->view->contractorEmployee = $contractorEmployee;

        //##########################//
        //        Statistics        //
        //##########################//
        //
        //Get Count Complete and inComplete
        //
        $myDashboard = false;

        if (!CheckAuth::getDashboardStatusSession()) {
            if (CheckAuth::checkCredential(array('canSeeAllBookingStatistics'))) {
                CheckAuth::setDashboardStatusSession('all_dashboard');
            } else {
                CheckAuth::setDashboardStatusSession('my_dashboard');
            }
        }

        $dasboardStatus = CheckAuth::getDashboardStatusSession();
        if ($dasboardStatus == 'my_dashboard') {
            $myDashboard = true;
        }

        $filters = array();
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $totalBooking = $modelBooking->getBookingCount($filters);
        $this->view->totalBooking = $totalBooking;

        $modelBookingStatus = new Model_BookingStatus();
        $allBookingStatus = $modelBookingStatus->getAll();

        foreach ($allBookingStatus as &$bookingStatus) {
            $filters['status'] = $bookingStatus['booking_status_id'];
            $bookingStatus['total'] = $modelBooking->getBookingCount($filters);
        }

        $this->view->allBookingStatus = $allBookingStatus;



        //get invoice information
        $modelBooking = new Model_Booking();
        $filters = array();
        $filters['invoice_type'] = 'unpaid';
        $filters['convert_status'] = 'invoice';
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }

        $totalQoute = $modelBooking->total($filters, 'qoute');
        $totalPaidAmount = $modelBooking->total($filters, 'paid_amount');

        $invoicesTotalAmount = $totalQoute - $totalPaidAmount;

        $this->view->invoicesTotalAmount = $invoicesTotalAmount;



        $modelBookingInvoice = new Model_BookingInvoice();
        $filters = array();
        $filters['invoice_type'] = 'unpaid';
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $invoices = $modelBookingInvoice->getAll($filters);
        $modelBookingInvoice->fills($invoices, array('number_of_due_days', 'paid_amount'));

        $due15 = 0;
        $due30 = 0;
        $due45 = 0;
        $dueAbove = 0;

        if ($invoices) {
            foreach ($invoices AS $invoice) {
                if ($invoice['due'] <= 15) {
                    $newdue15 = $invoice['amount'] - $invoice['paid_amount'];
                    $due15 += $newdue15;
                } elseif ($invoice['due'] <= 30) {
                    $newdue30 = $invoice['amount'] - $invoice['paid_amount'];
                    $due30 += $newdue30;
                } elseif ($invoice['due'] <= 45) {
                    $newdue45 = $invoice['amount'] - $invoice['paid_amount'];
                    $due45 += $newdue45;
                } else {
                    $newdueAbove = $invoice['amount'] - $invoice['paid_amount'];
                    $dueAbove += $newdueAbove;
                }
            }
        }

        $this->view->due15 = $due15;
        $this->view->due30 = $due30;
        $this->view->due45 = $due45;
        $this->view->dueAbove = $dueAbove;




        //
        //Future Bookings 
        //
        $futureBookingStatus = $modelBookingStatus->getAll(array('more_one_status_name' => array('IN PROGRESS', 'TO DO', 'ON HOLD', 'CANCELLED', 'TENTATIVE', 'TO VISIT')));

        $this->view->futureBookingStatus = $futureBookingStatus;

        // Future Today
        $today = getTimePeriodByName('today');

        $filters = array();
        $filters['booking_start_between'] = date('Y-m-d H:i:s', time());
        $filters['booking_end_between'] = $today['end'];
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $todayAllBookingStatus = $futureBookingStatus;
        foreach ($todayAllBookingStatus as &$todayBookingStatus) {
            $filters['status'] = $todayBookingStatus['booking_status_id'];
            $todayBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $todayBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');
        }
        $this->view->todayAllBookingStatus = $todayAllBookingStatus;


        // Future Tomorow
        $tomorrow = getTimePeriodByName('tomorrow');

        $filters = array();
        $filters['booking_start_between'] = $tomorrow['start'];
        $filters['booking_end_between'] = $tomorrow['end'];
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $tomorowAllBookingStatus = $futureBookingStatus;
        foreach ($tomorowAllBookingStatus as &$tomorowBookingStatus) {
            $filters['status'] = $tomorowBookingStatus['booking_status_id'];
            $tomorowBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $tomorowBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');
        }
        $this->view->tomorowAllBookingStatus = $tomorowAllBookingStatus;


        //Future Week
        $this_week = getTimePeriodByName('this_week');

        $filters = array();
        $filters['booking_start_between'] = date('Y-m-d H:i:s', time());
        $filters['booking_end_between'] = $this_week['end'];
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $weekAllBookingStatus = $futureBookingStatus;
        foreach ($weekAllBookingStatus as &$weekBookingStatus) {
            $filters['status'] = $weekBookingStatus['booking_status_id'];
            $weekBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $weekBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');
        }
        $this->view->weekAllBookingStatus = $weekAllBookingStatus;


        // Future Month
        $this_month = getTimePeriodByName('this_month');

        $filters = array();
        $filters['booking_start_between'] = date('Y-m-d H:i:s', time());
        $filters['booking_end_between'] = $this_month['end'];
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $monthAllBookingStatus = $futureBookingStatus;
        foreach ($monthAllBookingStatus as &$monthBookingStatus) {
            $filters['status'] = $monthBookingStatus['booking_status_id'];
            $monthBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $monthBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');
        }
        $this->view->monthAllBookingStatus = $monthAllBookingStatus;


        // Future quarter
        $this_quarter = getTimePeriodByName('this_quarter');

        $filters = array();
        $filters['booking_start_between'] = date('Y-m-d H:i:s', time());
        $filters['booking_end_between'] = $this_quarter['end'];
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $quarterAllBookingStatus = $futureBookingStatus;
        foreach ($quarterAllBookingStatus as &$quarterBookingStatus) {
            $filters['status'] = $quarterBookingStatus['booking_status_id'];
            $quarterBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $quarterBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');
        }
        $this->view->quarterAllBookingStatus = $quarterAllBookingStatus;


        // Future Next week
        $next_week = getTimePeriodByName('next_week');

        $filters = array();
        $filters['booking_start_between'] = $next_week['start'];
        $filters['booking_end_between'] = $next_week['end'];
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $nextWeekAllBookingStatus = $futureBookingStatus;
        foreach ($nextWeekAllBookingStatus as &$nextWeekBookingStatus) {
            $filters['status'] = $nextWeekBookingStatus['booking_status_id'];
            $nextWeekBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $nextWeekBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');
        }
        $this->view->nextWeekAllBookingStatus = $nextWeekAllBookingStatus;


        // Future Next Month
        $next_month = getTimePeriodByName('next_month');

        $filters = array();
        $filters['booking_start_between'] = $next_month['start'];
        $filters['booking_end_between'] = $next_month['end'];
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $nextMonthAllBookingStatus = $futureBookingStatus;
        foreach ($nextMonthAllBookingStatus as &$nextMonthBookingStatus) {
            $filters['status'] = $nextMonthBookingStatus['booking_status_id'];
            $nextMonthBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $nextMonthBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');
        }
        $this->view->nextMonthAllBookingStatus = $nextMonthAllBookingStatus;

        // Future Next Quarter
        $next_quarter = getTimePeriodByName('next_quarter');

        $filters = array();
        $filters['booking_start_between'] = $next_quarter['start'];
        $filters['booking_end_between'] = $next_quarter['end'];
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $nextQuarterAllBookingStatus = $futureBookingStatus;
        foreach ($nextQuarterAllBookingStatus as &$nextQuarterBookingStatus) {
            $filters['status'] = $nextQuarterBookingStatus['booking_status_id'];
            $nextQuarterBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $nextQuarterBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');
        }
        $this->view->nextQuarterAllBookingStatus = $nextQuarterAllBookingStatus;


        //all future Sales
        $filters = array();
        $filters['booking_start'] = date('Y-m-d H:i:s', time());
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $allFutureBookingStatus = $futureBookingStatus;
        foreach ($allFutureBookingStatus as &$futureBookingStatus) {
            $filters['status'] = $futureBookingStatus['booking_status_id'];
            $futureBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $futureBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');
        }
        $this->view->allFutureBookingStatus = $allFutureBookingStatus;
    }

    public function pastAction() {
        
        $this->view->page_title = 'Past Bookings - '.$this->view->page_title;
        $this->view->sub_menu = 'past_bookings';
        CheckAuth::checkPermission(array('dashboard'));

        //check login
        CheckAuth::checkLoggedIn();

        //
        // load model
        //
        $modelUser = new Model_User();
        $modelBooking = new Model_Booking();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelContractorInfo = new Model_ContractorInfo();
        $modelContractorEmployee = new Model_ContractorEmployee();

        //get logged user
        $loggedUser = CheckAuth::getLoggedUser();

        // role of logged user
        $role = CheckAuth::getRoleName();
        $this->view->role = $role;


        //logged user details
        $user = $modelUser->getById($loggedUser['user_id']);
        $this->view->user = $user;


        if (!$user) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }


        //#############################//
        //Contractor Info and employees//
        //#############################//
        $contractorInfo = $modelContractorInfo->getByContractorId($loggedUser['user_id']);
        $this->view->contractorInfo = $contractorInfo;

        $contractorEmployee = $modelContractorEmployee->getByContractorInfoId($contractorInfo['contractor_info_id']);
        $this->view->contractorEmployee = $contractorEmployee;

        //##########################//
        //        Statistics        //
        //##########################//
        //
        //Get Count Complete and inComplete
        //
        $myDashboard = false;

        if (!CheckAuth::getDashboardStatusSession()) {
            if (CheckAuth::checkCredential(array('canSeeAllBookingStatistics'))) {
                CheckAuth::setDashboardStatusSession('all_dashboard');
            } else {
                CheckAuth::setDashboardStatusSession('my_dashboard');
            }
        }

        $dasboardStatus = CheckAuth::getDashboardStatusSession();
        if ($dasboardStatus == 'my_dashboard') {
            $myDashboard = true;
        }

        $filters = array();
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $totalBooking = $modelBooking->getBookingCount($filters);
        $this->view->totalBooking = $totalBooking;

        $modelBookingStatus = new Model_BookingStatus();
        $allBookingStatus = $modelBookingStatus->getAll();

        foreach ($allBookingStatus as &$bookingStatus) {
            $filters['status'] = $bookingStatus['booking_status_id'];
            $bookingStatus['total'] = $modelBooking->getBookingCount($filters);
        }

        $this->view->allBookingStatus = $allBookingStatus;



        //get invoice information
        $modelBooking = new Model_Booking();
        $filters = array();
        $filters['invoice_type'] = 'unpaid';
        $filters['convert_status'] = 'invoice';
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }

        $totalQoute = $modelBooking->total($filters, 'qoute');
        $totalPaidAmount = $modelBooking->total($filters, 'paid_amount');

        $invoicesTotalAmount = $totalQoute - $totalPaidAmount;

        $this->view->invoicesTotalAmount = $invoicesTotalAmount;



        $modelBookingInvoice = new Model_BookingInvoice();
        $filters = array();
        $filters['invoice_type'] = 'unpaid';
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $invoices = $modelBookingInvoice->getAll($filters);
        $modelBookingInvoice->fills($invoices, array('number_of_due_days', 'paid_amount'));

        $due15 = 0;
        $due30 = 0;
        $due45 = 0;
        $dueAbove = 0;

        if ($invoices) {
            foreach ($invoices AS $invoice) {
                if ($invoice['due'] <= 15) {
                    $newdue15 = $invoice['amount'] - $invoice['paid_amount'];
                    $due15 += $newdue15;
                } elseif ($invoice['due'] <= 30) {
                    $newdue30 = $invoice['amount'] - $invoice['paid_amount'];
                    $due30 += $newdue30;
                } elseif ($invoice['due'] <= 45) {
                    $newdue45 = $invoice['amount'] - $invoice['paid_amount'];
                    $due45 += $newdue45;
                } else {
                    $newdueAbove = $invoice['amount'] - $invoice['paid_amount'];
                    $dueAbove += $newdueAbove;
                }
            }
        }

        $this->view->due15 = $due15;
        $this->view->due30 = $due30;
        $this->view->due45 = $due45;
        $this->view->dueAbove = $dueAbove;

        //
        //Past Bookings
        //
        //$pastBookingStatus = $modelBookingStatus->getAll(array('more_one_status_name' => array('IN PROGRESS', 'QUOTED ', 'ON HOLD', 'CANCELLED', 'TO DO', 'TENTATIVE', 'AWAITING UPDATE', 'FAILED')));
        $pastBookingStatus = $modelBookingStatus->getAll(array('more_one_status_name' => array('IN PROGRESS', 'QUOTED ', 'ON HOLD', 'CANCELLED', 'AWAITING UPDATE', 'FAILED')));

        $this->view->pastBookingStatus = $pastBookingStatus;

        //Last week(Past Bookings)
        $lastWeek = getTimePeriodByName('last_week');

        $filters = array();
        $filters['booking_start_between'] = $lastWeek['start'];
        $filters['booking_end_between'] = $lastWeek['end'];
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $lastWeekAllPastBookingStatus = $pastBookingStatus;
        foreach ($lastWeekAllPastBookingStatus as &$lastWeekPastBookingStatus) {
            $filters['status'] = $lastWeekPastBookingStatus['booking_status_id'];
            $lastWeekPastBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $lastWeekPastBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');
            $url = $this->router->assemble(array(), 'booking');
            $lastWeekPastBookingStatus['url'] = FilterLink::generate_link('(' . $lastWeekPastBookingStatus['count'] . ')', array('fltr' => $filters), false, true, $url);
        }
        $this->view->lastWeekAllPastBookingStatus = $lastWeekAllPastBookingStatus;


        // Last Month (Past Bookings)
        $lastMonth = getTimePeriodByName('last_month');

        $filters = array();
        $filters['booking_start_between'] = $lastMonth['start'];
        $filters['booking_end_between'] = $lastMonth['end'];
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $lastMonthAllPastBookingStatus = $pastBookingStatus;
        foreach ($lastMonthAllPastBookingStatus as &$lastMonthPastBookingStatus) {
            $filters['status'] = $lastMonthPastBookingStatus['booking_status_id'];
            $lastMonthPastBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $lastMonthPastBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');
            $url = $this->router->assemble(array(), 'booking');
            $lastMonthPastBookingStatus['url'] = FilterLink::generate_link('(' . $lastMonthPastBookingStatus['count'] . ')', array('fltr' => $filters), false, true, $url);
        }
        $this->view->lastMonthAllPastBookingStatus = $lastMonthAllPastBookingStatus;

        // Last quarter (Past Bookings)
        $lastQuarter = getTimePeriodByName('last_quarter');

        $filters = array();
        $filters['booking_start_between'] = $lastQuarter['start'];
        $filters['booking_end_between'] = $lastQuarter['end'];
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $lastQuarterAllPastBookingStatus = $pastBookingStatus;
        foreach ($lastQuarterAllPastBookingStatus as &$lastQuarterPastBookingStatus) {
            $filters['status'] = $lastQuarterPastBookingStatus['booking_status_id'];
            $lastQuarterPastBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $lastQuarterPastBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');
            $url = $this->router->assemble(array(), 'booking');
            $lastQuarterPastBookingStatus['url'] = FilterLink::generate_link('(' . $lastQuarterPastBookingStatus['count'] . ')', array('fltr' => $filters), false, true, $url);
        }
        $this->view->lastQuarterAllPastBookingStatus = $lastQuarterAllPastBookingStatus;


        // Last Year (Past Bookings)
        $lastYear = getTimePeriodByName('last_year');

        $filters = array();
        $filters['booking_start_between'] = $lastYear['start'];
        $filters['booking_end_between'] = $lastYear['end'];
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $lastYearAllPastBookingStatus = $pastBookingStatus;
        foreach ($lastYearAllPastBookingStatus as &$lastYearPastBookingStatus) {
            $filters['status'] = $lastYearPastBookingStatus['booking_status_id'];
            $lastYearPastBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $lastYearPastBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');
            $url = $this->router->assemble(array(), 'booking');
            $lastYearPastBookingStatus['url'] = FilterLink::generate_link('(' . $lastYearPastBookingStatus['count'] . ')', array('fltr' => $filters), false, true, $url);
        }
        $this->view->lastYearAllPastBookingStatus = $lastYearAllPastBookingStatus;

        //Today (Past Bookings)
        $today = getTimePeriodByName('today');

        $filters = array();
        $filters['booking_start_between'] = $today['start'];
        $filters['booking_end_between'] = $today['end'];
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $todayAllPastBookingStatus = $pastBookingStatus;
        foreach ($todayAllPastBookingStatus as &$todayPastBookingStatus) {
            $filters['status'] = $todayPastBookingStatus['booking_status_id'];
            $todayPastBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $todayPastBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');
            $url = $this->router->assemble(array(), 'booking');
            $todayPastBookingStatus['url'] = FilterLink::generate_link('(' . $todayPastBookingStatus['count'] . ')', array('fltr' => $filters), false, true, $url);
        }
        $this->view->todayAllPastBookingStatus = $todayAllPastBookingStatus;


        //This week (Past Bookings)
        $this_week = getTimePeriodByName('this_week');

        $filters = array();
        $filters['booking_start_between'] = $this_week['start'];
        $filters['booking_end_between'] = $this_week['end'];
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $thisWeekAllPastBookingStatus = $pastBookingStatus;
        foreach ($thisWeekAllPastBookingStatus as &$thisWeekPastBookingStatus) {
            $filters['status'] = $thisWeekPastBookingStatus['booking_status_id'];
            $thisWeekPastBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $thisWeekPastBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');
            $url = $this->router->assemble(array(), 'booking');
            $thisWeekPastBookingStatus['url'] = FilterLink::generate_link('(' . $thisWeekPastBookingStatus['count'] . ')', array('fltr' => $filters), false, true, $url);
        }
        $this->view->thisWeekAllPastBookingStatus = $thisWeekAllPastBookingStatus;


        //This month (Past Bookings)
        $this_month = getTimePeriodByName('this_month');

        $filters = array();
        $filters['booking_start_between'] = $this_month['start'];
        $filters['booking_end_between'] = $this_month['end'];
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $thisMonthAllPastBookingStatus = $pastBookingStatus;
        foreach ($thisMonthAllPastBookingStatus as &$thisMonthPastBookingStatus) {
            $filters['status'] = $thisMonthPastBookingStatus['booking_status_id'];
            $thisMonthPastBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $thisMonthPastBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');
            $url = $this->router->assemble(array(), 'booking');
            $thisMonthPastBookingStatus['url'] = FilterLink::generate_link('(' . $thisMonthPastBookingStatus['count'] . ')', array('fltr' => $filters), false, true, $url);
        }
        $this->view->thisMonthAllPastBookingStatus = $thisMonthAllPastBookingStatus;

        //This quarter (Past Bookings)
        $this_quarter = getTimePeriodByName('this_quarter');

        $filters = array();
        $filters['booking_start_between'] = $this_quarter['start'];
        $filters['booking_end_between'] = $this_quarter['end'];
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $thisQuarterAllPastBookingStatus = $pastBookingStatus;
        foreach ($thisQuarterAllPastBookingStatus as &$thisQuarterPastBookingStatus) {
            $filters['status'] = $thisQuarterPastBookingStatus['booking_status_id'];
            $thisQuarterPastBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $thisQuarterPastBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');
            $url = $this->router->assemble(array(), 'booking');
            $thisQuarterPastBookingStatus['url'] = FilterLink::generate_link('(' . $thisQuarterPastBookingStatus['count'] . ')', array('fltr' => $filters), false, true, $url);
        }
        $this->view->thisQuarterAllPastBookingStatus = $thisQuarterAllPastBookingStatus;


        //This year (Past Bookings)
        $this_year = getTimePeriodByName('this_year');

        $filters = array();
        $filters['booking_start_between'] = $this_year['start'];
        $filters['booking_end_between'] = $this_year['end'];
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $thisYearAllPastBookingStatus = $pastBookingStatus;
        foreach ($thisYearAllPastBookingStatus as &$thisYearPastBookingStatus) {
            $filters['status'] = $thisYearPastBookingStatus['booking_status_id'];
            $thisYearPastBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $thisYearPastBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');
            $url = $this->router->assemble(array(), 'booking');
            $thisYearPastBookingStatus['url'] = FilterLink::generate_link('(' . $thisYearPastBookingStatus['count'] . ')', array('fltr' => $filters), false, true, $url);
        }
        $this->view->thisYearAllPastBookingStatus = $thisYearAllPastBookingStatus;
    }

    public function completedAction() {
        $this->view->page_title = 'Completed Bookings - '.$this->view->page_title;
        $this->view->sub_menu = 'completed_bookings';

        CheckAuth::checkPermission(array('dashboard'));

        //check login
        CheckAuth::checkLoggedIn();

        //
        // load model
        //
        $modelUser = new Model_User();
        $modelBooking = new Model_Booking();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelContractorInfo = new Model_ContractorInfo();
        $modelContractorEmployee = new Model_ContractorEmployee();

        //get logged user
        $loggedUser = CheckAuth::getLoggedUser();

        // role of logged user
        $role = CheckAuth::getRoleName();
        $this->view->role = $role;


        //logged user details
        $user = $modelUser->getById($loggedUser['user_id']);
        $this->view->user = $user;


        if (!$user) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }


        //#############################//
        //Contractor Info and employees//
        //#############################//
        $contractorInfo = $modelContractorInfo->getByContractorId($loggedUser['user_id']);
        $this->view->contractorInfo = $contractorInfo;

        $contractorEmployee = $modelContractorEmployee->getByContractorInfoId($contractorInfo['contractor_info_id']);
        $this->view->contractorEmployee = $contractorEmployee;

        //##########################//
        //        Statistics        //
        //##########################//
        //
        //Get Count Complete and inComplete
        //
        $myDashboard = false;

        if (!CheckAuth::getDashboardStatusSession()) {
            if (CheckAuth::checkCredential(array('canSeeAllBookingStatistics'))) {
                CheckAuth::setDashboardStatusSession('all_dashboard');
            } else {
                CheckAuth::setDashboardStatusSession('my_dashboard');
            }
        }

        $dasboardStatus = CheckAuth::getDashboardStatusSession();
        if ($dasboardStatus == 'my_dashboard') {
            $myDashboard = true;
        }

        $filters = array();
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $totalBooking = $modelBooking->getBookingCount($filters);
        $this->view->totalBooking = $totalBooking;

        $modelBookingStatus = new Model_BookingStatus();
        $allBookingStatus = $modelBookingStatus->getAll();

        foreach ($allBookingStatus as &$bookingStatus) {
            $filters['status'] = $bookingStatus['booking_status_id'];
            $bookingStatus['total'] = $modelBooking->getBookingCount($filters);
        }

        $this->view->allBookingStatus = $allBookingStatus;


        //get invoice information
        $modelBooking = new Model_Booking();
        $filters = array();
        $filters['invoice_type'] = 'unpaid';
        $filters['convert_status'] = 'invoice';
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }

        $totalQoute = $modelBooking->total($filters, 'qoute');
        $totalPaidAmount = $modelBooking->total($filters, 'paid_amount');

        $invoicesTotalAmount = $totalQoute - $totalPaidAmount;

        $this->view->invoicesTotalAmount = $invoicesTotalAmount;



        $modelBookingInvoice = new Model_BookingInvoice();
        $filters = array();
        $filters['invoice_type'] = 'unpaid';
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $invoices = $modelBookingInvoice->getAll($filters);



        $modelBookingInvoice->fills($invoices, array('number_of_due_days', 'paid_amount'));



        $due15 = 0;
        $due30 = 0;
        $due45 = 0;
        $dueAbove = 0;



        if ($invoices) {
            foreach ($invoices AS $invoice) {
                if ($invoice['due'] <= 15) {
                    $newdue15 = $invoice['amount'] - $invoice['paid_amount'];
                    $due15 += $newdue15;
                } elseif ($invoice['due'] <= 30) {
                    $newdue30 = $invoice['amount'] - $invoice['paid_amount'];
                    $due30 += $newdue30;
                } elseif ($invoice['due'] <= 45) {
                    $newdue45 = $invoice['amount'] - $invoice['paid_amount'];
                    $due45 += $newdue45;
                } else {
                    $newdueAbove = $invoice['amount'] - $invoice['paid_amount'];
                    $dueAbove += $newdueAbove;
                }
            }
        }

        $this->view->due15 = $due15;
        $this->view->due30 = $due30;
        $this->view->due45 = $due45;
        $this->view->dueAbove = $dueAbove;


        //
        //Bookings Completed
        //
        
        // Last week
        $lastWeek = getTimePeriodByName('last_week');

        $filters = array();
        $filters['booking_start_between'] = $lastWeek['start'];
        $filters['booking_end_between'] = $lastWeek['end'];
        $filters['convert_status'] = 'invoice';
        $filters['invoice_type'] = 'all_active';
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $lastWeekSales = $modelBooking->total($filters, 'qoute');
        $lastWeekReceipts = $modelBooking->total($filters, 'paid_amount');
        $lastWeekDue = $lastWeekSales - $lastWeekReceipts;

        $this->view->lastWeekSales = $lastWeekSales;
        $this->view->lastWeekReceipts = $lastWeekReceipts;
        $this->view->lastWeekDue = $lastWeekDue;
        //$CompanyContractorShare = $this->getCompanyContractorShare($filters);
        //$this->view->lastWeekCompanyShare = $CompanyContractorShare['company_share'];
        //$this->view->lastWeekContractorShare = $CompanyContractorShare['contractor_share'];

        $this->view->lastWeekCompanyShare = 0;
        $this->view->lastWeekContractorShare = 0;


        // last Month
        $lastMonth = getTimePeriodByName('last_month');

        $filters = array();
        $filters['booking_start_between'] = $lastMonth['start'];
        $filters['booking_end_between'] = $lastMonth['end'];
        $filters['convert_status'] = 'invoice';
        $filters['invoice_type'] = 'all_active';
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $lastMonthSales = $modelBooking->total($filters, 'qoute');
        $lastMonthReceipts = $modelBooking->total($filters, 'paid_amount');
        $lastMonthDue = $lastMonthSales - $lastMonthReceipts;

        $this->view->lastMonthSales = $lastMonthSales;
        $this->view->lastMonthReceipts = $lastMonthReceipts;
        $this->view->lastMonthDue = $lastMonthDue;

        //$CompanyContractorShare = $this->getCompanyContractorShare($filters);
        //$this->view->lastMonthCompanyShare = $CompanyContractorShare['company_share'];
        //$this->view->lastMonthContractorShare = $CompanyContractorShare['contractor_share'];

        $this->view->lastMonthCompanyShare = 0;
        $this->view->lastMonthContractorShare = 0;


        // last quarter
        $lastQuarter = getTimePeriodByName('last_quarter');

        $filters = array();
        $filters['booking_start_between'] = $lastQuarter['start'];
        $filters['booking_end_between'] = $lastQuarter['end'];
        $filters['convert_status'] = 'invoice';
        $filters['invoice_type'] = 'all_active';
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $lastQuarterSales = $modelBooking->total($filters, 'qoute');
        $lastQuarterReceipts = $modelBooking->total($filters, 'paid_amount');
        $lastQuarterDue = $lastQuarterSales - $lastQuarterReceipts;

        $this->view->lastQuarterSales = $lastQuarterSales;
        $this->view->lastQuarterReceipts = $lastQuarterReceipts;
        $this->view->lastQuarterDue = $lastQuarterDue;

        //$CompanyContractorShare = $this->getCompanyContractorShare($filters);
        //$this->view->lastQuarterCompanyShare = $CompanyContractorShare['company_share'];
        //$this->view->lastQuarterContractorShare = $CompanyContractorShare['contractor_share'];

        $this->view->lastQuarterCompanyShare = 0;
        $this->view->lastQuarterContractorShare = 0;


        // last Year
        $lastYear = getTimePeriodByName('last_year');

        $filters = array();
        $filters['booking_start_between'] = $lastYear['start'];
        $filters['booking_end_between'] = $lastYear['end'];
        $filters['convert_status'] = 'invoice';
        $filters['invoice_type'] = 'all_active';
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $lastYearSales = $modelBooking->total($filters, 'qoute');
        $lastYearReceipts = $modelBooking->total($filters, 'paid_amount');
        $lastYearDue = $lastYearSales - $lastYearReceipts;

        $this->view->lastYearSales = $lastYearSales;
        //$CompanyContractorShare = $this->getCompanyContractorShare($filters);
        //$this->view->lastYearCompanyShare = $CompanyContractorShare['company_share'];
        //$this->view->lastYearContractorShare = $CompanyContractorShare['contractor_share'];

        $this->view->lastYearCompanyShare = 0;
        $this->view->lastYearContractorShare = 0;


        $this->view->lastYearReceipts = $lastYearReceipts;
        $this->view->lastYearDue = $lastYearDue;


        //Today
        $today = getTimePeriodByName('today');

        $filters = array();
        $filters['booking_start_between'] = $today['start'];
        $filters['convert_status'] = 'invoice';
        $filters['invoice_type'] = 'all_active';
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $todaySales = $modelBooking->total($filters, 'qoute');
        $todayReceipts = $modelBooking->total($filters, 'paid_amount');
        $todayDue = $todaySales - $todayReceipts;

        $this->view->todaySales = $todaySales;
        $this->view->todayReceipts = $todayReceipts;
        $this->view->todayDue = $todayDue;

        //$CompanyContractorShare = $this->getCompanyContractorShare($filters);
        //$this->view->todayCompanyShare = $CompanyContractorShare['company_share'];
        //$this->view->todayContractorShare = $CompanyContractorShare['contractor_share'];

        $this->view->todayCompanyShare = 0;
        $this->view->todayContractorShare = 0;

        //this week
        $this_week = getTimePeriodByName('this_week');

        $filters = array();
        $filters['booking_start_between'] = $this_week['start'];
        $filters['convert_status'] = 'invoice';
        $filters['invoice_type'] = 'all_active';
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $weekSales = $modelBooking->total($filters, 'qoute');
        $weekReceipts = $modelBooking->total($filters, 'paid_amount');
        $weekDue = $weekSales - $weekReceipts;

        $this->view->weekSales = $weekSales;
        $this->view->weekReceipts = $weekReceipts;
        $this->view->weekDue = $weekDue;
        //$CompanyContractorShare = $this->getCompanyContractorShare($filters);
        //$this->view->weekCompanyShare = $CompanyContractorShare['company_share'];
        //$this->view->weekContractorShare = $CompanyContractorShare['contractor_share'];

        $this->view->weekCompanyShare = 0;
        $this->view->weekContractorShare = 0;


        //this month
        $this_month = getTimePeriodByName('this_month');

        $filters = array();
        $filters['booking_start_between'] = $this_month['start'];
        $filters['convert_status'] = 'invoice';
        $filters['invoice_type'] = 'all_active';
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $monthSales = $modelBooking->total($filters, 'qoute');
        $monthReceipts = $modelBooking->total($filters, 'paid_amount');
        $monthDue = $monthSales - $monthReceipts;

        $this->view->monthSales = $monthSales;
        $this->view->monthReceipts = $monthReceipts;
        $this->view->monthDue = $monthDue;
        //$CompanyContractorShare = $this->getCompanyContractorShare($filters);
        //$this->view->monthCompanyShare = $CompanyContractorShare['company_share'];
        //$this->view->monthContractorShare = $CompanyContractorShare['contractor_share'];


        $this->view->monthCompanyShare = 0;
        $this->view->monthContractorShare = 0;

        //$this->view->monthCompanyShare = 0;
        //this quarter
        $this_quarter = getTimePeriodByName('this_quarter');

        $filters = array();
        $filters['booking_start_between'] = $this_quarter['start'];
        $filters['convert_status'] = 'invoice';
        $filters['invoice_type'] = 'all_active';
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $quarterSales = $modelBooking->total($filters, 'qoute');
        $quarterReceipts = $modelBooking->total($filters, 'paid_amount');
        $quarterDue = $quarterSales - $quarterReceipts;

        $this->view->quarterSales = $quarterSales;
        $this->view->quarterReceipts = $quarterReceipts;
        $this->view->quarterDue = $quarterDue;
        //$CompanyContractorShare = $this->getCompanyContractorShare($filters);
        //$this->view->quarterCompanyShare = $CompanyContractorShare['company_share'];
        //$this->view->quarterContractorShare = $CompanyContractorShare['contractor_share'];

        $this->view->quarterCompanyShare = 0;
        $this->view->quarterContractorShare = 0;


        //this year
        $this_year = getTimePeriodByName('this_year');

        $filters = array();
        $filters['booking_start_between'] = $this_year['start'];
        $filters['convert_status'] = 'invoice';
        $filters['invoice_type'] = 'all_active';
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }

        $yearSales = $modelBooking->total($filters, 'qoute');
        $yearReceipts = $modelBooking->total($filters, 'paid_amount');
        $yearDue = $yearSales - $yearReceipts;

        $this->view->yearSales = $yearSales;
        $this->view->yearReceipts = $yearReceipts;
        $this->view->yearDue = $yearDue;

        //$CompanyContractorShare = $this->getCompanyContractorShare($filters);
        //$this->view->yearCompanyShare = $CompanyContractorShare['company_share'];
        //$this->view->yearContractorShare = $CompanyContractorShare['contractor_share'];

        $this->view->yearCompanyShare = 0;
        $this->view->yearContractorShare = 0;
    }

    public function getCompanyContractorShareAction() {

        $results = array();
        $filters = array();
        $booking_start = $this->request->getParam('booking_start');
        $booking_end = $this->request->getParam('booking_end');

        if (isset($booking_start)) {

            $loggedUser = CheckAuth::getLoggedUser();
            $filters['company_id'] = $loggedUser['company_id'];

            $filters['convert_status'] = 'invoice';
            $filters['invoice_type'] = 'all_active';

            $myDashboard = false;
            $dasboardStatus = CheckAuth::getDashboardStatusSession();
            if ($dasboardStatus == 'my_dashboard') {
                $myDashboard = true;
            }

            if ($myDashboard) {
                $filters['created_by'] = $loggedUser['user_id'];
            }

            // last week company share & contractor share
            $lastWeek = getTimePeriodByName('last_week');
            $filters['booking_start_between'] = $booking_start;
            if (isset($booking_end)) {
                $filters['booking_end_between'] = $booking_end;
            } else {
                unset($filters['booking_end_between']);
            }
            $CompanyContractorShareResults = $this->getCompanyContractorShare($filters);
            $results['CompanyShare'] = number_format($CompanyContractorShareResults['company_share'], 2);
            $results['ContractorShare'] = number_format($CompanyContractorShareResults['contractor_share'], 2);

            echo json_encode($results);
            exit;
        }
    }

    public function getCompanyContractorShare($filters) {
        $company_share = 0;
        $contractor_share = 0;
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();

        $bookings = $modelContractorServiceBooking->getForCompanyShare($filters);


        foreach ($bookings as &$booking) {

            $contractorId = $booking['contractor_id'];
            $bookingId = $booking['booking_id'];

            $total_contractor_service_qoute_without_gst = $modelContractorServiceBooking->getTotalServiceBookingQoute($bookingId, $booking['service_id'], $booking['clone']);

            $totalDiscount = !empty($booking['total_discount']) ? $booking['total_discount'] : 0;
            $callOutFee = !empty($booking['call_out_fee']) ? $booking['call_out_fee'] : 0;
            $gst = ($total_contractor_service_qoute_without_gst + $callOutFee) * get_config('gst_tax');
            $booking['total_contractor_service_qoute'] = $total_contractor_service_qoute_without_gst + $callOutFee + $gst - $totalDiscount;


            if (isset($booking['contractor_share'])) {
                $company_share = $company_share + ($booking['total_contractor_service_qoute'] - $booking['contractor_share']);
                $contractor_share = $contractor_share + $booking['contractor_share'];
            } else {

                $calculated_contractor_share = $booking['total_contractor_service_qoute'] * $booking['commission'] / 100;
                /////not registerd gst
                if ($booking['gst'] == 0) {
                    $tax = $this->calculate_net_amount($calculated_contractor_share, 10);
                    $calculated_contractor_share = $calculated_contractor_share - $tax;
                }

                $calculated_company_share = $booking['total_contractor_service_qoute'] - $calculated_contractor_share;
                $company_share = $company_share + $calculated_company_share;
                $contractor_share = $contractor_share + $calculated_contractor_share;
            }
        }





        return array('company_share' => $company_share, 'contractor_share' => $contractor_share);
    }

    function calculate_net_amount($gross_amount, $tax_rate) {
        $divisor = ($tax_rate / 100) + 1;
        $net_price = $gross_amount / $divisor;
        $tax = $gross_amount - $net_price;
        //echo '$net_price is '.$net_price;
        //echo '$tax is '.$tax;
        return $tax;
    }

    public function inquiriesAction() {
        $this->view->page_title = 'Inquiries - '.$this->view->page_title;
        $this->view->sub_menu = 'inquiries';

        CheckAuth::checkPermission(array('dashboard'));

        //check login
        CheckAuth::checkLoggedIn();

        //
        // load model
        //
        $modelUser = new Model_User();
        $modelBooking = new Model_Booking();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelContractorInfo = new Model_ContractorInfo();
        $modelContractorEmployee = new Model_ContractorEmployee();

        //get logged user
        $loggedUser = CheckAuth::getLoggedUser();

        // role of logged user
        $role = CheckAuth::getRoleName();
        $this->view->role = $role;


        //logged user details
        $user = $modelUser->getById($loggedUser['user_id']);
        $this->view->user = $user;


        if (!$user) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }


        //#############################//
        //Contractor Info and employees//
        //#############################//
        $contractorInfo = $modelContractorInfo->getByContractorId($loggedUser['user_id']);
        $this->view->contractorInfo = $contractorInfo;

        $contractorEmployee = $modelContractorEmployee->getByContractorInfoId($contractorInfo['contractor_info_id']);
        $this->view->contractorEmployee = $contractorEmployee;



        $myDashboard = false;

        if (!CheckAuth::getDashboardStatusSession()) {
            if (CheckAuth::checkCredential(array('canSeeAllBookingStatistics'))) {
                CheckAuth::setDashboardStatusSession('all_dashboard');
            } else {
                CheckAuth::setDashboardStatusSession('my_dashboard');
            }
        }

        $dasboardStatus = CheckAuth::getDashboardStatusSession();
        if ($dasboardStatus == 'my_dashboard') {
            $myDashboard = true;
        }

        $filters = array();
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $totalBooking = $modelBooking->getBookingCount($filters);
        $this->view->totalBooking = $totalBooking;

        $modelBookingStatus = new Model_BookingStatus();
        $allBookingStatus = $modelBookingStatus->getAll();

        foreach ($allBookingStatus as &$bookingStatus) {
            $filters['status'] = $bookingStatus['booking_status_id'];
            $bookingStatus['total'] = $modelBooking->getBookingCount($filters);
        }

        $this->view->allBookingStatus = $allBookingStatus;





        //
        //Get Count Service Accepted and Rejected
        //
        
        // Total services
        $filters = array();
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $totalService = $modelContractorServiceBooking->getServicesCount($filters);
        $this->view->totalService = $totalService;


        // services is_accepted
        $filters = array();
        $filters['is_accepted'] = true;
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $totalServiceAccepted = $modelContractorServiceBooking->getServicesCount($filters);
        $this->view->totalServiceAccepted = $totalServiceAccepted;
        $this->view->totalServiceAcceptedPer = $totalService ? number_format((($totalServiceAccepted * 100) / $totalService), 2) : 0;

        // services is_rejected
        $filters = array();
        $filters['is_rejected'] = true;
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $totalServiceRejected = $modelContractorServiceBooking->getServicesCount($filters);
        $this->view->totalServiceRejected = $totalServiceRejected;
        $this->view->totalServiceRejectedPer = $totalService ? number_format((($totalServiceRejected * 100) / $totalService), 2) : 0;

        // services NotAcceptedAndRejected

        $filters = array();
        $filters['NotAcceptedAndRejected'] = true;
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $totalServiceNotAcceptedAndRejected = $modelContractorServiceBooking->getServicesCount($filters);
        $this->view->totalServiceNotAcceptedAndRejected = $totalServiceNotAcceptedAndRejected;
        $this->view->totalServiceNotAcceptedAndRejectedPer = $totalService ? number_format((($totalServiceNotAcceptedAndRejected * 100) / $totalService), 2) : 0;







        //
        // Numer of  inquires
        //
     
        $modelInquiry = new Model_Inquiry();
        $modelInquiryType = new Model_InquiryType();

        $inquiryTypes = $modelInquiryType->getAll();
        $this->view->inquiryTypes = $inquiryTypes;

        // inquires Last Year
        // last Year
        $lastYear = getTimePeriodByName('last_year');

        // last quarter
        $lastQuarter = getTimePeriodByName('last_quarter');

        $filters = array();
        $filters['startTimeRange'] = $lastYear['start'];
        $filters['endTimeRange'] = $lastYear['end'];
        if ($myDashboard) {
            $filters['user_id'] = $loggedUser['user_id'];
        }
        $allLastYearInquiries = $modelInquiry->getInquiryCount($filters);
        $this->view->allLastYearInquiries = $allLastYearInquiries;

        $lastYearInquiryTypes = $inquiryTypes;
        foreach ($lastYearInquiryTypes as &$lastYearInquiryType) {
            $filters['inquiry_type_id'] = $lastYearInquiryType['inquiry_type_id'];
            $lastYearInquiryType['count'] = $modelInquiry->getInquiryCount($filters);
        }
        $this->view->lastYearInquiryTypes = $lastYearInquiryTypes;


        // inquires Last Quarter

        $filters = array();
        $filters['startTimeRange'] = $lastQuarter['start'];
        $filters['endTimeRange'] = $lastQuarter['end'];
        if ($myDashboard) {
            $filters['user_id'] = $loggedUser['user_id'];
        }

        $allLastQuarterInquiries = $modelInquiry->getInquiryCount($filters);
        $this->view->allLastQuarterInquiries = $allLastQuarterInquiries;

        $lastQuarterInquiryTypes = $inquiryTypes;
        foreach ($lastQuarterInquiryTypes as &$lastQuarterInquiryType) {
            $filters['inquiry_type_id'] = $lastQuarterInquiryType['inquiry_type_id'];
            $lastQuarterInquiryType['count'] = $modelInquiry->getInquiryCount($filters);
        }
        $this->view->lastQuarterInquiryTypes = $lastQuarterInquiryTypes;

        // inquires Last Month
        // last Month
        $lastMonth = getTimePeriodByName('last_month');

        // last quarter
        $lastQuarter = getTimePeriodByName('last_quarter');

        $filters = array();
        $filters['startTimeRange'] = $lastMonth['start'];
        $filters['endTimeRange'] = $lastMonth['end'];
        if ($myDashboard) {
            $filters['user_id'] = $loggedUser['user_id'];
        }

        $allLastMonthInquiries = $modelInquiry->getInquiryCount($filters);
        $this->view->allLastMonthInquiries = $allLastMonthInquiries;

        $lastMonthInquiryTypes = $inquiryTypes;
        foreach ($lastMonthInquiryTypes as &$lastMonthInquiryType) {
            $filters['inquiry_type_id'] = $lastMonthInquiryType['inquiry_type_id'];
            $lastMonthInquiryType['count'] = $modelInquiry->getInquiryCount($filters);
        }
        $this->view->lastMonthInquiryTypes = $lastMonthInquiryTypes;

        // inquires Last Week
        // Last week
        $lastWeek = getTimePeriodByName('last_week');


        $filters = array();
        $filters['startTimeRange'] = $lastWeek['start'];
        $filters['endTimeRange'] = $lastWeek['end'];
        if ($myDashboard) {
            $filters['user_id'] = $loggedUser['user_id'];
        }

        $allLastWeekInquiries = $modelInquiry->getInquiryCount($filters);
        $this->view->allLastWeekInquiries = $allLastWeekInquiries;

        $lastWeekInquiryTypes = $inquiryTypes;
        foreach ($lastWeekInquiryTypes as &$lastWeekInquiryType) {
            $filters['inquiry_type_id'] = $lastWeekInquiryType['inquiry_type_id'];
            $lastWeekInquiryType['count'] = $modelInquiry->getInquiryCount($filters);
        }
        $this->view->lastWeekInquiryTypes = $lastWeekInquiryTypes;


        // inquires  Today
        //Today
        $today = getTimePeriodByName('today');

        $filters = array();
        $filters['startTimeRange'] = $today['start'];
        $filters['endTimeRange'] = date('Y-m-d H:i:s', time());
        if ($myDashboard) {
            $filters['user_id'] = $loggedUser['user_id'];
        }

        $allTodayInquiries = $modelInquiry->getInquiryCount($filters);
        $this->view->allTodayInquiries = $allTodayInquiries;

        $todayInquiryTypes = $inquiryTypes;
        foreach ($todayInquiryTypes as &$todayInquiryType) {
            $filters['inquiry_type_id'] = $todayInquiryType['inquiry_type_id'];
            $todayInquiryType['count'] = $modelInquiry->getInquiryCount($filters);
        }
        $this->view->todayInquiryTypes = $todayInquiryTypes;

        //inquires Week
        //this week
        $this_week = getTimePeriodByName('this_week');


        $filters = array();
        $filters['startTimeRange'] = $this_week['start'];
        $filters['endTimeRange'] = date('Y-m-d H:i:s', time());
        if ($myDashboard) {
            $filters['user_id'] = $loggedUser['user_id'];
        }

        $allThisWeekInquiries = $modelInquiry->getInquiryCount($filters);
        $this->view->allThisWeekInquiries = $allThisWeekInquiries;

        $thisWeekInquiryTypes = $inquiryTypes;
        foreach ($thisWeekInquiryTypes as &$thisWeekInquiryType) {
            $filters['inquiry_type_id'] = $thisWeekInquiryType['inquiry_type_id'];
            $thisWeekInquiryType['count'] = $modelInquiry->getInquiryCount($filters);
        }
        $this->view->thisWeekInquiryTypes = $thisWeekInquiryTypes;


        // inquires Month
        //This month 		
        $this_month = getTimePeriodByName('this_month');



        $filters = array();
        $filters['startTimeRange'] = $this_month['start'];
        $filters['endTimeRange'] = date('Y-m-d H:i:s', time());
        if ($myDashboard) {
            $filters['user_id'] = $loggedUser['user_id'];
        }

        $allThisMonthInquiries = $modelInquiry->getInquiryCount($filters);
        $this->view->allThisMonthInquiries = $allThisMonthInquiries;

        $thisMonthInquiryTypes = $inquiryTypes;
        foreach ($thisMonthInquiryTypes as &$thisMonthInquiryType) {
            $filters['inquiry_type_id'] = $thisMonthInquiryType['inquiry_type_id'];
            $thisMonthInquiryType['count'] = $modelInquiry->getInquiryCount($filters);
        }
        $this->view->thisMonthInquiryTypes = $thisMonthInquiryTypes;

        //  inquires quarter
        // Future quarter
        $this_quarter = getTimePeriodByName('this_quarter');



        $filters = array();
        $filters['startTimeRange'] = $this_quarter['start'];
        $filters['endTimeRange'] = date('Y-m-d H:i:s', time());
        if ($myDashboard) {
            $filters['user_id'] = $loggedUser['user_id'];
        }

        $allThisQuarterInquiries = $modelInquiry->getInquiryCount($filters);
        $this->view->allThisQuarterInquiries = $allThisQuarterInquiries;

        $thisQuarterInquiryTypes = $inquiryTypes;
        foreach ($thisQuarterInquiryTypes as &$thisQuarterInquiryType) {
            $filters['inquiry_type_id'] = $thisQuarterInquiryType['inquiry_type_id'];
            $thisQuarterInquiryType['count'] = $modelInquiry->getInquiryCount($filters);
        }
        $this->view->thisQuarterInquiryTypes = $thisQuarterInquiryTypes;

        //  inquires This Year
        //This year 
        $this_year = getTimePeriodByName('this_year');

        $filters = array();
        $filters['startTimeRange'] = $this_year['start'];
        $filters['endTimeRange'] = date('Y-m-d H:i:s', time());
        if ($myDashboard) {
            $filters['user_id'] = $loggedUser['user_id'];
        }

        $allThisYearInquiries = $modelInquiry->getInquiryCount($filters);
        $this->view->allThisYearInquiries = $allThisYearInquiries;

        $thisYearInquiryTypes = $inquiryTypes;
        foreach ($thisYearInquiryTypes as &$thisYearInquiryType) {
            $filters['inquiry_type_id'] = $thisYearInquiryType['inquiry_type_id'];
            $thisYearInquiryType['count'] = $modelInquiry->getInquiryCount($filters);
        }
        $this->view->thisYearInquiryTypes = $thisYearInquiryTypes;
    }

    public function recentByConvertStatusAction() {

        
        CheckAuth::checkPermission(array('dashboard'));

        $convert_status = $this->request->getParam('convert_status');
        $orderBy = $this->request->getParam('sort', 'bok.created');
        $sortingMethod = $this->request->getParam('method', 'desc');
        $currentPage = $this->request->getParam('page', 1);
        // Load Model
        $modelBooking = new Model_Booking();

        $loggedUser = CheckAuth::getLoggedUser();
        $dasboardStatus = CheckAuth::getDashboardStatusSession();

        $filters = array();
        $filters['convert_status'] = $convert_status;

        if ($convert_status == 'estimate') {
            $this->view->page_title = 'Recent Estimates - '.$this->view->page_title;
            $orderBy = 'est.created';
            $filters['inner_join_estimate'] = true;
        }
        else if ($convert_status == 'invoice') {
            $this->view->page_title = 'Recent Invoices - '.$this->view->page_title;
            $orderBy = 'bok.job_finish_time';
            // $filters['inner_join_invoice'] = true;
        }
        else{
            $this->view->page_title = 'Recent Bookings - '.$this->view->page_title;
        }

        $myDashboard = false;

        if ($dasboardStatus == 'my_dashboard') {
            $myDashboard = true;
        }
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }


        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        $recent = $modelBooking->getAll($filters, "{$orderBy} {$sortingMethod}", $pager, 100);
        $modelBooking->fills($recent, array('estimate', 'invoice', 'status_discussion', 'not_accepted_or_rejected', 'not_accepted',));

        $this->view->recent = $recent;
        $this->view->convert_status = $convert_status;

        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
    }

    public function recentByBookingStatusAction() {


        CheckAuth::checkPermission(array('dashboard'));
        $status_id = $this->request->getParam('status_id');
        $currentPage = $this->request->getParam('page', 1);
        
        $modelBookingStatus = new Model_BookingStatus();
        $bookingStatus = $modelBookingStatus->getById($status_id);
        $this->view->page_title = 'Recent '.$bookingStatus['name']. ' - '.$this->view->page_title;

        // Load Model
        $modelBooking = new Model_Booking();

        $loggedUser = CheckAuth::getLoggedUser();
        $dasboardStatus = CheckAuth::getDashboardStatusSession();

        $filters = array();
        //$filters['status'] = $status_id;


        $myDashboard = false;

        if ($dasboardStatus == 'my_dashboard') {
            $myDashboard = true;
        }
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }

        // because pager sending by refrance
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        // order by created status in bookink status history(bsh)
        $filters['order_by_status_history'] = $status_id;

        $recent = $modelBooking->getAll($filters, 'bsh.max_created DESC', $pager, 100);
        $modelBooking->fills($recent, array('estimate', 'invoice', 'status_discussion'));

        $this->view->recent = $recent;

        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        //$this->view->sortingMethod = $sortingMethod;
        //$this->view->orderBy = $orderBy;
    }

    public function recentDiscussionsAction() {
        
        $this->view->page_title = 'Recent Discussions - '.$this->view->page_title;

        CheckAuth::checkPermission(array('dashboard'));

        $modelBookingDiscussion = new Model_BookingDiscussion();

        $loggedUser = CheckAuth::getLoggedUser();
        $dasboardStatus = CheckAuth::getDashboardStatusSession();

        $myDashboard = false;
        $filters = array();

        if ($dasboardStatus == 'my_dashboard') {
            $myDashboard = true;
        }
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }

        $recentDiscussion = $modelBookingDiscussion->getRecentDiscussion($filters);
        $this->view->recentDiscussion = $recentDiscussion;
    }

    public function recentHistoryAction() {
        
        $this->view->page_title = 'Recent Bookings History - '.$this->view->page_title;

        CheckAuth::checkPermission(array('dashboard'));

        $modelBookingLog = new Model_BookingLog();

        $loggedUser = CheckAuth::getLoggedUser();
        $dasboardStatus = CheckAuth::getDashboardStatusSession();

        $myDashboard = false;
        $filters = array();

        if ($dasboardStatus == 'my_dashboard') {
            $myDashboard = true;
        }
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }

        //History

        $bookingLogs = $modelBookingLog->getRecentBookingLog($filters);
        $this->view->bookingLogs = $bookingLogs;
    }

    public function recentComplaintsAction() {
        
        $this->view->page_title = 'Recent Complaints - '.$this->view->page_title;

        CheckAuth::checkPermission(array('dashboard'));

        $modelComplaint = new Model_Complaint();

        $loggedUser = CheckAuth::getLoggedUser();
        $dasboardStatus = CheckAuth::getDashboardStatusSession();

        $myDashboard = false;
        $filters = array();

        if ($dasboardStatus == 'my_dashboard') {
            $myDashboard = true;
        }
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }

        $recentComplaints = $modelComplaint->getRecentComplaint($filters);
        $modelComplaint->fills($recentComplaints, array('booking'));

        $this->view->recentComplaints = $recentComplaints;
    }

    public function acceptServicesAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('acceptOrRejectServices'));


        $bookingId = $this->request->getParam('bookingId', 0);
        $allAccepted = $this->request->getParam('accepted', array());

        $modelBooking = new Model_Booking();
        if (!$modelBooking->checkIfCanSeeBookingWithOutTimePeriod($bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect($this->router->assemble(array(), 'booking'));
        }
        if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect($this->router->assemble(array(), 'booking'));
        }

        //
        //get logged user
        //
        $loggedUser = CheckAuth::getLoggedUser();

        //
        // load model
        //
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelServices = new Model_Services();

        $notAcceptedBookings = $modelContractorServiceBooking->getNotAcceptedBookings($bookingId, $loggedUser['user_id']);
        foreach ($notAcceptedBookings as &$notAcceptedBooking) {
            $notAcceptedBooking['service'] = $modelServices->getById($notAcceptedBooking['service_id']);
        }

        $this->view->notAcceptedBookings = $notAcceptedBookings;
        $this->view->bookingId = $bookingId;

        if ($this->request->isPost()) {

            if ($allAccepted) {

                $services = array();

                foreach ($allAccepted as $acceptedId) {
                    $modelContractorServiceBooking->updateById($acceptedId, array('is_accepted' => 1, 'is_rejected' => 0));

                    $contractorServiceBooking = $modelContractorServiceBooking->getById($acceptedId);

                    $services[] = array(
                        'service_id' => $contractorServiceBooking['service_id'],
                        'clone' => $contractorServiceBooking['clone']
                    );
                }
                $modelGoogleCalendarEvent = new Model_GoogleCalendarEvent();
                $modelGoogleCalendarEvent->sendBookingToGmailAcc($bookingId, $services);


                $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Services accepted"));

                echo 1;
                exit;
            } else {
                $this->view->errorMsg = 'You must select one service at least';
            }
        }

        echo $this->view->render('dashboard/accept-services.phtml');
        exit;
    }

    public function rejectServicesAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('acceptOrRejectServices'));

        $bookingId = $this->request->getParam('bookingId', 0);
        $allRejected = $this->request->getParam('rejected', array());

        $modelBooking = new Model_Booking();
        if (!$modelBooking->checkIfCanSeeBookingWithOutTimePeriod($bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect($this->router->assemble(array(), 'booking'));
        }
        if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect($this->router->assemble(array(), 'booking'));
        }

        //
        //get logged user
        //
        $loggedUser = CheckAuth::getLoggedUser();

        //
        // load model
        //
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelServices = new Model_Services();

        //
        //get all Not Rejected Bookings
        //
        $notRejectedBookings = $modelContractorServiceBooking->getNotRejectedOrAcceptedBookings($bookingId, $loggedUser['user_id']);
        foreach ($notRejectedBookings as &$notRejectedBooking) {
            $notRejectedBooking['service'] = $modelServices->getById($notRejectedBooking['service_id']);
        }

        $this->view->notRejectedBookings = $notRejectedBookings;
        $this->view->bookingId = $bookingId;

        if ($this->request->isPost()) {
            if ($allRejected) {

                foreach ($allRejected as $rejectedId) {
                    $modelContractorServiceBooking->updateById($rejectedId, array('is_rejected' => 1, 'is_accepted' => 0));
                }


                $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Services rejected"));

                echo 1;
                exit;
            } else {
                $this->view->errorMsg = 'You must select one service at least';
            }
        }

        echo $this->view->render('dashboard/reject-services.phtml');
        exit;
    }

    public function acceptAdminServicesAction() {

        CheckAuth::checkPermission(array('canAcceptAdminServices', 'canSeeAllBooking'));

        $bookingId = $this->request->getParam('bookingId', 0);
        $allAccepted = $this->request->getParam('accepted', array());

        if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect($this->router->assemble(array(), 'booking'));
        }

        //
        // load model
        //
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelServices = new Model_Services();

        //
        //get all Not Accepted Bookings
        //
        $notAcceptedBookings = $modelContractorServiceBooking->getAll(array('booking_id' => $bookingId, 'is_accepted' => 'not_accepted'));
        foreach ($notAcceptedBookings as &$notAcceptedBooking) {
            $notAcceptedBooking['service'] = $modelServices->getById($notAcceptedBooking['service_id']);
        }

        $this->view->notAcceptedBookings = $notAcceptedBookings;
        $this->view->bookingId = $bookingId;

        if ($this->request->isPost()) {

            if ($allAccepted) {

                $services = array();

                foreach ($allAccepted as $acceptedId) {
                    $modelContractorServiceBooking->updateById($acceptedId, array('is_accepted' => 1, 'is_rejected' => 0));
                    $contractorServiceBooking = $modelContractorServiceBooking->getById($acceptedId);

                    $services[] = array(
                        'service_id' => $contractorServiceBooking['service_id'],
                        'clone' => $contractorServiceBooking['clone']
                    );
                }
                //D.A 07/08/2015
                $ContractorServiceBookings = $modelContractorServiceBooking->getByBookingId($bookingId);
                //var_dump($ContractorServiceBooking);exit;
                foreach ($ContractorServiceBookings as $ContractorServiceBooking) {
                    if ($ContractorServiceBooking['is_rejected'] == 1) {
                        break;
                    }
                    $modelRejectBookingQuestionAnswers = new Model_RejectBookingQuestionAnswers();
                    $modelRejectBookingQuestionAnswers->deleteByBookingId($bookingId);
                }

                //if(my_ip('176.106.46.142')){
                $this->sendBookingToContracrtorGmailAcc($bookingId, $services);
                //}


                $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Services accepted"));

                echo 1;
                exit;
            } else {
                $this->view->errorMsg = 'You must select one service at least';
            }
        }

        echo $this->view->render('dashboard/accept-admin-services.phtml');
        exit;
    }

    // added by doaa
    /* public function rejectAdminServicesAction() {

      CheckAuth::checkPermission(array('canAcceptAdminServices', 'canSeeAllBooking'));

      $bookingId = $this->request->getParam('bookingId', 0);
      $allRejected = $this->request->getParam('rejected', array());

      //D.A 28/07/2015
      $question = $this->request->getParam('question', array());
      $option = $this->request->getParam('option', array());
      $comment = $this->request->getParam('comment', 0);
      $answer = $this->request->getParam('answer', array());

      if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
      $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
      $this->_redirect($this->router->assemble(array(), 'booking'));
      }

      //
      // load model
      //
      $modelContractorServiceBooking = new Model_ContractorServiceBooking();
      $modelServices = new Model_Services();

      //
      //get all Accepted Bookings
      //
      $acceptedBookings = $modelContractorServiceBooking->getAll(array('booking_id' => $bookingId, 'is_accepted' => 'accepted'));
      foreach ($acceptedBookings as &$acceptedBooking) {
      $acceptedBooking['service'] = $modelServices->getById($acceptedBooking['service_id']);
      }


      //D.A 28/07/2015
      $modelRejectBookingQuestion = new Model_RejectBookingQuestion();
      $this->view->data = $modelRejectBookingQuestion->getAll();

      $modelRejectBookingQuestionAnswers = new Model_RejectBookingQuestionAnswers();
      $rejectBookingQuestionAnswers = $modelRejectBookingQuestionAnswers->getCommentByBookingId($bookingId);
      $this->view->rejectBookingQuestions = $rejectBookingQuestionAnswers['comment'];

      $this->view->acceptedBookings = $acceptedBookings;
      $this->view->bookingId = $bookingId;

      if ($this->request->isPost()) {

      if ($allRejected) {

      $services = array();

      foreach ($allRejected as $rejectedId) {
      $modelContractorServiceBooking->updateById($rejectedId, array('is_accepted' => 0,'is_rejected' => 1));
      $contractorServiceBooking = $modelContractorServiceBooking->getById($rejectedId);

      $services[] = array(
      'service_id' => $contractorServiceBooking['service_id'],
      'clone' => $contractorServiceBooking['clone']
      );
      }


      //D.A 28/07/2015
      if($rejectBookingQuestionAnswers){
      $modelRejectBookingQuestionAnswers = new Model_RejectBookingQuestionAnswers();
      $modelRejectBookingQuestionAnswers->deleteByBookingId($bookingId);
      }

      foreach ($question as $ques) {
      $questiontype = $modelRejectBookingQuestion->getByQuestionId($ques);
      if($questiontype['question_type']== "multiple"){
      foreach ($option as $quesoption) {
      $ques_option = explode("_", $quesoption);
      $question_id=$ques_option[0];
      $option_id=$ques_option[1];
      if($question_id == $ques ){
      $data = array(
      'question_id'=>$question_id,
      'booking_id'=>$contractorServiceBooking['booking_id'],
      'answer'=>$option_id,
      'comment'=>$comment
      );

      $modelRejectBookingQuestionAnswers = new Model_RejectBookingQuestionAnswers();
      $modelRejectBookingQuestionAnswers->insert($data);

      }
      }

      }else{

      foreach ($answer as $key => $value) {
      if($ques == $key){

      $data = array(
      'question_id'=>$ques,
      'booking_id'=>$contractorServiceBooking['booking_id'],
      'answer'=>$value,
      'comment'=>$comment
      );

      $modelRejectBookingQuestionAnswers = new Model_RejectBookingQuestionAnswers();
      $modelRejectBookingQuestionAnswers->insert($data);

      }
      }

      }
      }


      $this->sendBookingToContracrtorGmailAcc($bookingId, $services);


      $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Services rejected."));

      echo 1;
      exit;
      } else {
      $this->view->errorMsg = 'You must select one service at least';
      }
      }

      echo $this->view->render('dashboard/reject-admin-services.phtml');
      exit;
      }
     */

    public function rejectAdminServicesAction() {

        CheckAuth::checkPermission(array('canAcceptAdminServices', 'canSeeAllBooking'));

        $bookingId = $this->request->getParam('bookingId', 0);
        $allRejected = $this->request->getParam('rejected', array());

        //D.A 28/07/2015
        $question = $this->request->getParam('question', array());
        $option = $this->request->getParam('option', array());
        $comment = $this->request->getParam('comment', 0);
        $answer = $this->request->getParam('answer', array());


        if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect($this->router->assemble(array(), 'booking'));
        }

        //
        // load model
        //
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelServices = new Model_Services();

        //
        //get all Accepted Bookings
        //
        $acceptedBookings = $modelContractorServiceBooking->getAll(array('booking_id' => $bookingId, 'is_accepted' => 'accepted'));
        foreach ($acceptedBookings as &$acceptedBooking) {
            $acceptedBooking['service'] = $modelServices->getById($acceptedBooking['service_id']);
        }

        //D.A 28/07/2015
        $modelRejectBookingQuestion = new Model_RejectBookingQuestion();
        $this->view->data = $modelRejectBookingQuestion->getAll();

        $modelRejectBookingQuestionAnswers = new Model_RejectBookingQuestionAnswers();
        $rejectBookingQuestionAnswers = $modelRejectBookingQuestionAnswers->getCommentByBookingId($bookingId);
        $this->view->rejectBookingQuestions = $rejectBookingQuestionAnswers['comment'];

        $this->view->acceptedBookings = $acceptedBookings;
        $this->view->bookingId = $bookingId;

        if ($this->request->isPost()) {

            if ($allRejected) {
                $insertID = '';
                $services = array();
                $contractorServiceBooking = array();
                foreach ($allRejected as $rejectedId) {
                    $modelContractorServiceBooking->updateById($rejectedId, array('is_accepted' => 0, 'is_rejected' => 1));
                    $contractorServiceBooking = $modelContractorServiceBooking->getById($rejectedId);

                    $services[] = array(
                        'service_id' => $contractorServiceBooking['service_id'],
                        'clone' => $contractorServiceBooking['clone']
                    );
                }

                //D.A 28/07/2015
                if ($rejectBookingQuestionAnswers) {
                    $modelRejectBookingQuestionAnswers = new Model_RejectBookingQuestionAnswers();
                    $modelRejectBookingQuestionAnswers->deleteByBookingId($bookingId);
                }

                foreach ($question as $ques) {
                    $questiontype = $modelRejectBookingQuestion->getByQuestionId($ques);
                    if ($questiontype['question_type'] == "multiple") {
                        foreach ($option as $quesoption) {
                            $ques_option = explode("_", $quesoption);
                            $question_id = $ques_option[0];
                            $option_id = $ques_option[1];
                            if ($question_id == $ques) {
                                $data = array(
                                    'question_id' => $question_id,
                                    'booking_id' => $contractorServiceBooking['booking_id'],
                                    'answer' => $option_id,
                                    'comment' => $comment
                                );

                                $modelRejectBookingQuestionAnswers = new Model_RejectBookingQuestionAnswers();
                                $insertID = $modelRejectBookingQuestionAnswers->insert($data);
                            }
                        }
                    } else {

                        foreach ($answer as $key => $value) {
                            if ($ques == $key) {

                                $data = array(
                                    'question_id' => $ques,
                                    'booking_id' => $contractorServiceBooking['booking_id'],
                                    'answer' => $value,
                                    'comment' => $comment
                                );

                                $modelRejectBookingQuestionAnswers = new Model_RejectBookingQuestionAnswers();
                                $insertID = $modelRejectBookingQuestionAnswers->insert($data);
                            }
                        }
                    }
                }
                //D.A 27/08/2015 Remove Reject Booking Questions Cache
                if ($insertID) {
                    require_once 'Zend/Cache.php';
                    $company_id = CheckAuth::getCompanySession();
                    $rejectBookingQuestionsCacheID = $bookingId . '_rejectBookingQuestions';
                    $bookingViewDir = get_config('cache') . '/' . 'bookingsView' . '/' . $company_id;
                    if (!is_dir($bookingViewDir)) {
                        mkdir($bookingViewDir, 0777, true);
                    }
                    $frontEndOption = array('lifetime' => 24 * 3600,
                        'automatic_serialization' => true);
                    $backendOptions = array('cache_dir' => $bookingViewDir);
                    $Cache = Zend_Cache::factory('Core', 'File', $frontEndOption, $backendOptions);
                    $Cache->remove($rejectBookingQuestionsCacheID);
                }

                $this->sendBookingToContracrtorGmailAcc($bookingId, $services);

                $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Services rejected"));

                echo 1;
                exit;
            } else {
                $this->view->errorMsg = 'You must select one service at least';
            }
        }

        echo $this->view->render('dashboard/reject-admin-services.phtml');
        exit;
    }

    /* public function rejectAdminServicesAction() {

      CheckAuth::checkPermission(array('canAcceptAdminServices', 'canSeeAllBooking'));

      $bookingId = $this->request->getParam('bookingId', 0);
      $allRejected = $this->request->getParam('rejected', array());

      if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
      $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
      $this->_redirect($this->router->assemble(array(), 'booking'));
      }

      //
      // load model
      //
      $modelContractorServiceBooking = new Model_ContractorServiceBooking();
      $modelServices = new Model_Services();

      //
      //get all Accepted Bookings
      //
      $acceptedBookings = $modelContractorServiceBooking->getAll(array('booking_id' => $bookingId, 'is_accepted' => 'accepted'));
      foreach ($acceptedBookings as &$acceptedBooking) {
      $acceptedBooking['service'] = $modelServices->getById($acceptedBooking['service_id']);
      }

      $this->view->acceptedBookings = $acceptedBookings;
      $this->view->bookingId = $bookingId;

      if ($this->request->isPost()) {

      if ($allRejected) {

      $services = array();

      foreach ($allRejected as $rejectedId) {
      $modelContractorServiceBooking->updateById($rejectedId, array('is_accepted' => 0,'is_rejected' => 1));
      $contractorServiceBooking = $modelContractorServiceBooking->getById($rejectedId);

      $services[] = array(
      'service_id' => $contractorServiceBooking['service_id'],
      'clone' => $contractorServiceBooking['clone']
      );
      }
      $this->sendBookingToContracrtorGmailAcc($bookingId, $services);


      $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Services rejected."));

      echo 1;
      exit;
      } else {
      $this->view->errorMsg = 'You must select one service at least';
      }
      }

      echo $this->view->render('dashboard/reject-admin-services.phtml');
      exit;
      } */

    public function resendAdminServicesAction() {

        CheckAuth::checkPermission(array('canAcceptAdminServices', 'canSeeAllBooking'));

        $bookingId = $this->request->getParam('bookingId', 0);
        $allAccepted = $this->request->getParam('accepted', array());

        if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect($this->router->assemble(array(), 'booking'));
        }

        //
        // load model
        //
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelServices = new Model_Services();

        //
        //get all Not Accepted Bookings
        //
        $filters = array('booking_id' => $bookingId);
        $notAcceptedBookings = $modelContractorServiceBooking->getAll($filters);
        foreach ($notAcceptedBookings as &$notAcceptedBooking) {
            $notAcceptedBooking['service'] = $modelServices->getById($notAcceptedBooking['service_id']);
        }

        $this->view->notAcceptedBookings = $notAcceptedBookings;
        $this->view->bookingId = $bookingId;

        if ($this->request->isPost()) {

            if ($allAccepted) {

                $services = array();

                foreach ($allAccepted as $acceptedId) {
                    $modelContractorServiceBooking->updateById($acceptedId, array('is_accepted' => 1, 'is_rejected' => 0));
                    $contractorServiceBooking = $modelContractorServiceBooking->getById($acceptedId);

                    $services[] = array(
                        'service_id' => $contractorServiceBooking['service_id'],
                        'clone' => $contractorServiceBooking['clone']
                    );
                }
                $this->sendBookingToContracrtorGmailAcc($bookingId, $services);
                $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Services accepted"));

                echo 1;
                exit;
            } else {
                $this->view->errorMsg = 'You must select one service at least';
            }
        }

        echo $this->view->render('dashboard/resend-admin-services.phtml');
        exit;
    }

    public function sendBookingToContracrtorGmailAcc($bookingId, $services) {

        //
        // load model
        //
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();

        //
        // get the gmail account contractor
        //
        $contractorServices = array();
        foreach ($services AS $service) {

            $ContractorServiceBooking = $modelContractorServiceBooking->getByBookingAndServiceAndClone($bookingId, $service['service_id'], $service['clone']);
            $contractor_id = isset($ContractorServiceBooking['contractor_id']) ? $ContractorServiceBooking['contractor_id'] : 0;

            $contractorServices[$contractor_id][] = $service;
        }

        $modelGoogleCalendarEvent = new Model_GoogleCalendarEvent();
        foreach ($contractorServices AS $contractorId => $contractorService) {
            $modelGoogleCalendarEvent->sendBookingToGmailAcc($bookingId, $contractorService, $contractorId);
        }
    }

    public function showStatusDiscussionAction() {

        $discussionId = $this->request->getParam('id');
        $statusId = $this->request->getParam('status_id');

        // Load Model
        $modelBookingDiscussion = new Model_BookingDiscussion();
        $modelBookingStatus = new Model_BookingStatus();

        $statusDiscussion = $modelBookingDiscussion->getById($discussionId);
        $bookingStatus = $modelBookingStatus->getById($statusId);

        $this->view->bookingStatus = $bookingStatus;
        $this->view->statusDiscussion = $statusDiscussion;
        echo $this->view->render('dashboard/show-status-discussion.phtml');
        exit;
    }

    public function logUserActivityAction() {
        
        $this->view->page_title = 'User Activity - '.$this->view->page_title;
        

        //get request parameters
        $orderBy = $this->request->getParam('sort', 'created');
        $sortingMethod = $this->request->getParam('method', 'desc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        // Load Model
        $modelLogUser = new Model_LogUser();

        $modelInquiry = new Model_Inquiry();
        $modelBooking = new Model_Booking();
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelBookingEstimate = new Model_BookingEstimate();
        $modelComplaint = new Model_Complaint();
        $modelCustomer = new Model_Customer();

        $modelInquiryDiscussion = new Model_InquiryDiscussion();
        $modelBookingDiscussion = new Model_BookingDiscussion();
        $modelInvoiceDiscussion = new Model_InvoiceDiscussion();
        $modelEstimateDiscussion = new Model_EstimateDiscussion();
        $modelComplaintDiscussion = new Model_ComplaintDiscussion();

        $modelInquiryLabel = new Model_InquiryLabel();
        $modelBookingLabel = new Model_BookingLabel();
        $modelInvoiceLabel = new Model_InvoiceLabel();
        $modelEstimateLabel = new Model_EstimateLabel();

        $modelBookingAttachment = new Model_BookingAttachment();
        $modelEmailLog = new Model_EmailLog();

        $modelInquiryReminder = new Model_InquiryReminder();
        $modelBookingReminder = new Model_BookingReminder();
        $modelBookingContactHistory = new Model_BookingContactHistory();

        $modelRefund = new Model_Refund();
        $modelPayment = new Model_Payment();

        $modelReport = new Model_Report();

        // by salim
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelContractorMobileInfo = new Model_ContractorMobileInfo();
        $modelUser = new Model_User();
        $modelPaymentToCont = new Model_PaymentToContractors();
        $modelPaymentToContAttachment = new Model_PaymentToContractors();

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        $loggedUser = CheckAuth::getLoggedUser();
        $dasboardStatus = CheckAuth::getDashboardStatusSession();

        $myDashboard = false;
        if ($dasboardStatus == 'my_dashboard') {
            $myDashboard = true;
        }

        if ($myDashboard && empty($filters['user_id'])) {
            $filters['user_id'] = $loggedUser['user_id'];
        }

        if (!CheckAuth::checkCredential(array('loggedUsersActivity')) && $myDashboard) {
            $filters['user_id'] = $loggedUser['user_id'];
        }

        if (empty($filters['from'])) {
            //this week
            $this_week = getTimePeriodByName('this_week');
            $filters['from'] = $this_week['start'];
        }


        //init pager and articles model object
        $pager = new Model_Pager();
        $pager->perPage = 50;
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        $logUserEvents = $modelLogUser->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);
        foreach ($logUserEvents as &$logUserEvent) {

            switch ($logUserEvent['item_type']) {
                // By Salim
                case 'booking_service_by_booking_id_and_service_and_clone':
                    $contServiceBooking = $modelContractorServiceBooking->getById($logUserEvent['item_id']);
                    if ($contServiceBooking) {
                        $booking = $modelBooking->getById($contServiceBooking['booking_id']);
                        if ($booking) {
                            $logUserEvent['event_on'] = $booking['booking_num'];

                            $logUserEvent['action'] = $this->router->assemble(array('id' => $logUserEvent['item_id']), 'bookingView');
                        }
                    } else {
                        $logUserEvent['action'] = 'javascript:;';
                    }
                    $logUserEvent['event_name'] = $logUserEvent['item_type'];
                    break;

                case 'contractor_service_booking_by_id':
                    $contServiceBooking = $modelContractorServiceBooking->getById($logUserEvent['item_id']);
                    if ($contServiceBooking) {
                        $booking = $modelBooking->getById($contServiceBooking['booking_id']);
                        if ($booking) {
                            $logUserEvent['event_on'] = $booking['booking_num'];

                            $logUserEvent['action'] = $this->router->assemble(array('id' => $logUserEvent['item_id']), 'bookingView');
                        }
                    } else {
                        $logUserEvent['action'] = 'javascript:;';
                    }
                    $logUserEvent['event_name'] = $logUserEvent['item_type'];
                    break;

                case 'booking_service_by_booking_id':
                    $contServiceBooking = $modelContractorServiceBooking->getById($logUserEvent['item_id']);
                    if ($contServiceBooking) {
                        $booking = $modelBooking->getById($contServiceBooking['booking_id']);
                        if ($booking) {
                            $logUserEvent['event_on'] = $booking['booking_num'];

                            $logUserEvent['action'] = $this->router->assemble(array('id' => $logUserEvent['item_id']), 'bookingView');
                        }
                    } else {
                        $logUserEvent['action'] = 'javascript:;';
                    }
                    $logUserEvent['event_name'] = $logUserEvent['item_type'];
                    break;

                case 'contractor_mobile_info':
                    $contMobileInfo = $modelContractorMobileInfo->getById($logUserEvent['item_id']);

                    if ($contMobileInfo) {
                        $userInfo = $modelUser->getById($contMobileInfo['contractor_id']);
                        if ($userInfo) {

                            $logUserEvent['event_on'] = $userInfo['username'];

                            $logUserEvent['action'] = $this->router->assemble(array('contractor_id' => $userInfo['user_id']), 'settingsContractorInfoList');
                        }
                    } else {
                        $logUserEvent['action'] = 'javascript:;';
                    }
                    $logUserEvent['event_name'] = $logUserEvent['item_type'];
                    break;
                case 'payment_to_contractor':
                    $contPaymentToCont = $modelPaymentToCont->getById($logUserEvent['item_id']);
                    if ($contPaymentToCont) {
                        $user_info = $modelUser->getById($contPaymentToCont['contractor_id']);
                        if ($user_info) {
                            $logUserEvent['event_on'] = $user_info['username'];

                            //$logUserEvent['action'] = $this->router->assemble(array('id' => $logUserEvent['item_id']), 'bookingView');
                            $logUserEvent['action'] = 'javascript:;';
                        }
                    } else {
                        $logUserEvent['action'] = 'javascript:;';
                    }
                    $logUserEvent['event_name'] = $logUserEvent['item_type'];
                    break;

                case 'payment_to_contractor_attachment':
                    $contPaymentToContAttach = $modelPaymentToContAttachment->getById($logUserEvent['item_id']);
                    if ($contPaymentToContAttach) {
                        $payment_to_cont = $modelPaymentToCont->getById($contPaymentToContAttach['payment_to_contractor_id']);
                        if ($payment_to_cont) {
                            $user_info = $modelUser->getById($payment_to_cont['contractor_id']);
                            if ($user_info) {
                                $logUserEvent['event_on'] = $user_info['username'];

                                //$logUserEvent['action'] = $this->router->assemble(array('id' => $logUserEvent['item_id']), 'bookingView');
                                $logUserEvent['action'] = 'javascript:;';
                            }
                        }
                    } else {
                        $logUserEvent['action'] = 'javascript:;';
                    }
                    $logUserEvent['event_name'] = $logUserEvent['item_type'];
                    break;
                case 'inquiry':
                    $inquiry = $modelInquiry->getById($logUserEvent['item_id']);
                    if ($inquiry) {
                        $logUserEvent['event_on'] = $inquiry['inquiry_num'];

                        $logUserEvent['action'] = $this->router->assemble(array('id' => $logUserEvent['item_id']), 'inquiryView');
                    } else {
                        $logUserEvent['action'] = 'javascript:;';
                    }
                    $logUserEvent['event_name'] = $logUserEvent['item_type'];
                    break;

                case 'booking':
                    $booking = $modelBooking->getById($logUserEvent['item_id']);
                    if ($booking) {
                        $logUserEvent['event_on'] = $booking['booking_num'];

                        $logUserEvent['action'] = $this->router->assemble(array('id' => $logUserEvent['item_id']), 'bookingView');
                    } else {
                        $logUserEvent['action'] = 'javascript:;';
                    }
                    $logUserEvent['event_name'] = $logUserEvent['item_type'];
                    break;

                case 'booking_estimate':
                    $estimate = $modelBookingEstimate->getById($logUserEvent['item_id']);
                    if ($estimate) {
                        $logUserEvent['event_on'] = $estimate['estimate_num'];

                        $logUserEvent['action'] = $this->router->assemble(array('id' => $logUserEvent['item_id']), 'estimateView');
                    } else {
                        $logUserEvent['action'] = 'javascript:;';
                    }
                    $logUserEvent['event_name'] = 'estimate';
                    break;

                case 'booking_invoice':
                    $invoice = $modelBookingInvoice->getById($logUserEvent['item_id']);
                    if ($invoice) {
                        $logUserEvent['event_on'] = $invoice['invoice_num'];

                        $logUserEvent['action'] = $this->router->assemble(array('id' => $logUserEvent['item_id']), 'invoiceView');
                    } else {
                        $logUserEvent['action'] = 'javascript:;';
                    }
                    $logUserEvent['event_name'] = 'invoice';
                    break;

                case 'customer':
                    $customer = $modelCustomer->getById($logUserEvent['item_id']);
                    if ($customer) {
                        $logUserEvent['event_on'] = get_customer_name($customer);

                        $logUserEvent['action'] = $this->router->assemble(array('id' => $logUserEvent['item_id']), 'customerView');
                    } else {
                        $logUserEvent['action'] = 'javascript:;';
                    }
                    $logUserEvent['event_name'] = $logUserEvent['item_type'];
                    break;

                case 'complaint':
                    $complaint = $modelComplaint->getById($logUserEvent['item_id']);
                    if ($complaint) {
                        $logUserEvent['event_on'] = $complaint['complaint_num'];

                        $logUserEvent['action'] = $this->router->assemble(array('id' => $logUserEvent['item_id']), 'complaintView');
                    } else {
                        $logUserEvent['action'] = 'javascript:;';
                    }
                    $logUserEvent['event_name'] = $logUserEvent['item_type'];
                    break;

                case 'inquiry_discussion':
                    $inquiryDiscussion = $modelInquiryDiscussion->getById($logUserEvent['item_id']);
                    if ($inquiryDiscussion) {
                        $inquiry = $modelInquiry->getById($inquiryDiscussion['inquiry_id']);
                        if ($inquiry) {
                            $logUserEvent['event_on'] = $inquiry['inquiry_num'];
                        }
                        $logUserEvent['action'] = 'javascript:;';
                        $logUserEvent['onclick'] = "showPopupWindow('{$this->router->assemble(array('id' => $inquiryDiscussion['inquiry_id'], 'process' => 'send', 'type' => 'inquiry'), 'itemDiscussion')}','470','700','Inquiry Discussion');";
                    } else {
                        $logUserEvent['action'] = 'javascript:;';
                    }
                    $logUserEvent['event_name'] = $logUserEvent['item_type'];
                    break;

                case 'booking_discussion':
                    $bookingDiscussion = $modelBookingDiscussion->getById($logUserEvent['item_id']);
                    if ($bookingDiscussion) {
                        $booking = $modelBooking->getById($bookingDiscussion['booking_id']);
                        if ($booking) {
                            $logUserEvent['event_on'] = $booking['booking_num'];
                        }
                        $logUserEvent['action'] = 'javascript:;';
                        $logUserEvent['onclick'] = "showPopupWindow('{$this->router->assemble(array('id' => $bookingDiscussion['booking_id'], 'process' => 'send', 'type' => 'booking'), 'itemDiscussion')}','470','700','Booking Discussion');";
                    } else {
                        $logUserEvent['action'] = 'javascript:;';
                    }
                    $logUserEvent['event_name'] = $logUserEvent['item_type'];
                    break;

                case 'estimate_discussion':
                    $estimateDiscussion = $modelEstimateDiscussion->getById($logUserEvent['item_id']);
                    if ($estimateDiscussion) {
                        $estimate = $modelBookingEstimate->getById($estimateDiscussion['estimate_id']);
                        if ($estimate) {
                            $logUserEvent['event_on'] = $estimate['estimate_num'];
                        }
                        $logUserEvent['action'] = 'javascript:;';
                        $logUserEvent['onclick'] = "showPopupWindow('{$this->router->assemble(array('id' => $estimateDiscussion['estimate_id'], 'process' => 'send', 'type' => 'estimate'), 'itemDiscussion')}','470','700','Estimate Discussion');";
                    } else {
                        $logUserEvent['action'] = 'javascript:;';
                    }
                    $logUserEvent['event_name'] = $logUserEvent['item_type'];
                    break;

                case 'invoice_discussion':
                    $invoiceDiscussion = $modelInvoiceDiscussion->getById($logUserEvent['item_id']);
                    if ($invoiceDiscussion) {
                        $invoice = $modelBookingInvoice->getById($invoiceDiscussion['invoice_id']);
                        if ($invoice) {
                            $logUserEvent['event_on'] = $invoice['invoice_num'];
                        }
                        $logUserEvent['action'] = 'javascript:;';
                        $logUserEvent['onclick'] = "showPopupWindow('{$this->router->assemble(array('id' => $invoiceDiscussion['invoice_id'], 'process' => 'send', 'type' => 'invoice'), 'itemDiscussion')}','470','700','Invoice Discussion');";
                    } else {
                        $logUserEvent['action'] = 'javascript:;';
                    }
                    $logUserEvent['event_name'] = $logUserEvent['item_type'];
                    break;

                case 'complaint_discussion':
                    $complaintDiscussion = $modelComplaintDiscussion->getById($logUserEvent['item_id']);
                    if ($complaintDiscussion) {
                        $complaint = $modelComplaint->getById($complaintDiscussion['complaint_id']);
                        if ($complaint) {
                            $logUserEvent['event_on'] = $complaint['complaint_num'];
                        }
                        $logUserEvent['action'] = 'javascript:;';
                        $logUserEvent['onclick'] = "showPopupWindow('{$this->router->assemble(array('id' => $complaintDiscussion['complaint_id'], 'process' => 'send', 'type' => 'complaint'), 'itemDiscussion')}','470','700','Complaint Discussion');";
                    } else {
                        $logUserEvent['action'] = 'javascript:;';
                    }
                    $logUserEvent['event_name'] = $logUserEvent['item_type'];
                    break;

                case 'inquiry_label':
                    $inquiryLabel = $modelInquiryLabel->getById($logUserEvent['item_id']);
                    if ($inquiryLabel) {
                        $inquiry = $modelInquiry->getById($inquiryLabel['inquiry_id']);
                        if ($inquiry) {
                            $logUserEvent['event_on'] = $inquiry['inquiry_num'];
                        }
                        $logUserEvent['action'] = $this->router->assemble(array('id' => $inquiryLabel['inquiry_id']), 'inquiryView');
                    } else {
                        $logUserEvent['action'] = 'javascript:;';
                    }
                    $logUserEvent['event_name'] = $logUserEvent['item_type'];
                    break;

                case 'booking_label':
                    $bookingLabel = $modelBookingLabel->getById($logUserEvent['item_id']);
                    if ($bookingLabel) {
                        $booking = $modelBooking->getById($bookingLabel['booking_id']);
                        if ($booking) {
                            $logUserEvent['event_on'] = $booking['booking_num'];
                        }
                        $logUserEvent['action'] = $this->router->assemble(array('id' => $bookingLabel['booking_id']), 'bookingView');
                    } else {
                        $logUserEvent['action'] = 'javascript:;';
                    }
                    $logUserEvent['event_name'] = $logUserEvent['item_type'];
                    break;

                case 'estimate_label':
                    $estimateLabel = $modelEstimateLabel->getById($logUserEvent['item_id']);
                    if ($estimateLabel) {
                        $estimate = $modelBookingEstimate->getById($estimateLabel['estimate_id']);
                        if ($estimate) {
                            $logUserEvent['event_on'] = $estimate['estimate_num'];
                        }
                        $logUserEvent['action'] = $this->router->assemble(array('id' => $estimateLabel['estimate_id']), 'estimateView');
                    } else {
                        $logUserEvent['action'] = 'javascript:;';
                    }
                    $logUserEvent['event_name'] = $logUserEvent['item_type'];
                    break;

                case 'invoice_label':
                    $invoiceLabel = $modelInvoiceLabel->getById($logUserEvent['item_id']);
                    if ($invoiceLabel) {
                        $invoice = $modelBookingInvoice->getById($invoiceLabel['invoice_id']);
                        if ($invoice) {
                            $logUserEvent['event_on'] = $invoice['invoice_num'];
                        }
                        $logUserEvent['action'] = $this->router->assemble(array('id' => $invoiceLabel['invoice_id']), 'invoiceView');
                    } else {
                        $logUserEvent['action'] = 'javascript:;';
                    }
                    $logUserEvent['event_name'] = $logUserEvent['item_type'];
                    break;

                case 'inquiry_reminder':
                    $inquiryReminder = $modelInquiryReminder->getById($logUserEvent['item_id']);
                    if ($inquiryReminder) {
                        $inquiry = $modelInquiry->getById($inquiryReminder['inquiry_id']);
                        if ($inquiry) {
                            $logUserEvent['event_on'] = $inquiry['inquiry_num'];
                        }
                        $logUserEvent['action'] = 'javascript:;';
                        $logUserEvent['onclick'] = "showPopupWindow('{$this->router->assemble(array('id' => $logUserEvent['item_id']), 'inquiryReminderView')}','180','400','Inquiry Contact History');";
                    } else {
                        $logUserEvent['action'] = 'javascript:;';
                    }
                    $logUserEvent['event_name'] = 'Inquiry Contact History';
                    break;

                case 'booking_reminder':
                    $bookingReminder = $modelBookingReminder->getById($logUserEvent['item_id']);
                    if ($bookingReminder) {
                        $booking = $modelBooking->getById($bookingReminder['booking_id']);
                        if ($booking) {
                            $logUserEvent['event_on'] = $booking['booking_num'];
                        }
                        $logUserEvent['action'] = 'javascript:;';
                        $logUserEvent['onclick'] = "showPopupWindow('{$this->router->assemble(array('id' => $logUserEvent['item_id']), 'bookingReminderView')}','180','400','Booking Confirmation');";
                    } else {
                        $logUserEvent['action'] = 'javascript:;';
                    }
                    $logUserEvent['event_name'] = 'Booking Confirmation';
                    break;

                case 'booking_contact_history':
                    $bookingContactHistory = $modelBookingContactHistory->getById($logUserEvent['item_id']);
                    if ($bookingContactHistory) {
                        $estimate = $modelBookingEstimate->getByBookingId($bookingContactHistory['booking_id']);
                        if ($estimate) {
                            $logUserEvent['event_on'] = $estimate['estimate_num'];
                        }
                        $logUserEvent['action'] = 'javascript:;';
                        $logUserEvent['onclick'] = "showPopupWindow('{$this->router->assemble(array('id' => $logUserEvent['item_id']), 'bookingContactHistoryView')}','180','400','Estimate Contact History');";
                    } else {
                        $logUserEvent['action'] = 'javascript:;';
                    }
                    $logUserEvent['event_name'] = 'Estimate Contact History';
                    break;

                case 'booking_attachment':
                    $bookingAttachment = $modelBookingAttachment->getById($logUserEvent['item_id']);

                    if (isset($bookingAttachment['booking_id']) && $bookingAttachment['booking_id']) {
                        $booking = $modelBooking->getById($bookingAttachment['booking_id']);
                        $logUserEvent['event_on'] = $booking['booking_num'];

                        $logUserEvent['action'] = $this->router->assemble(array('id' => $bookingAttachment['booking_id']), 'bookingAttachment');
                        $logUserEvent['event_name'] = 'Booking Attachment';
                    } elseif (isset($bookingAttachment['inquiry_id']) && $bookingAttachment['inquiry_id']) {
                        $inquiry = $modelInquiry->getById($bookingAttachment['inquiry_id']);
                        $logUserEvent['event_on'] = $inquiry['inquiry_num'];

                        $logUserEvent['action'] = $this->router->assemble(array('id' => $bookingAttachment['inquiry_id']), 'inquiryAttachment');
                        $logUserEvent['event_name'] = 'Inquiry Attachment';
                    } else {
                        $logUserEvent['action'] = $this->router->assemble(array('id' => $logUserEvent['item_id']), 'bookingFileDownload');
                        $logUserEvent['event_name'] = 'attachment';
                    }
                    break;

                case 'refund':
                    $refund = $modelRefund->getById($logUserEvent['item_id']);
                    if ($refund) {
                        $invoice = $modelBookingInvoice->getByBookingId($refund['booking_id']);
                        if ($invoice) {
                            $logUserEvent['event_on'] = $invoice['invoice_num'];
                        }
                        $logUserEvent['action'] = $this->router->assemble(array('booking_id' => $refund['booking_id']), 'bookingRefundList');
                    } else {
                        $logUserEvent['action'] = 'javascript:;';
                    }
                    $logUserEvent['event_name'] = $logUserEvent['item_type'];
                    break;

                case 'payment':
                    $payment = $modelPayment->getById($logUserEvent['item_id']);
                    if ($payment) {
                        $invoice = $modelBookingInvoice->getByBookingId($payment['booking_id']);
                        if ($invoice) {
                            $logUserEvent['event_on'] = $invoice['invoice_num'];
                        }
                        $logUserEvent['action'] = $this->router->assemble(array('booking_id' => $payment['booking_id']), 'bookingPaymentList');
                    } else {
                        $logUserEvent['action'] = 'javascript:;';
                    }
                    $logUserEvent['event_name'] = $logUserEvent['item_type'];
                    break;

                case 'email':

                    $emailLog = $modelEmailLog->getById($logUserEvent['item_id']);

                    switch ($emailLog['type']) {
                        case 'common':
                            $logUserEvent['event_on'] = $emailLog['to'];
                            break;
                        case 'invoice':
                            $invoice = $modelBookingInvoice->getById($emailLog['reference_id']);
                            if ($invoice) {
                                $logUserEvent['event_on'] = $invoice['invoice_num'];
                            } else {
                                $logUserEvent['event_on'] = $emailLog['to'];
                            }
                            break;
                        case 'estimate':
                            $estimate = $modelBookingEstimate->getById($emailLog['reference_id']);
                            if ($estimate) {
                                $logUserEvent['event_on'] = $estimate['estimate_num'];
                            } else {
                                $logUserEvent['event_on'] = $emailLog['to'];
                            }
                            break;
                        case 'complaint':
                            $complaint = $modelComplaint->getById($emailLog['reference_id']);
                            if ($complaint) {
                                $logUserEvent['event_on'] = $complaint['complaint_num'];
                            } else {
                                $logUserEvent['event_on'] = $emailLog['to'];
                            }
                            break;
                        case 'booking':
                            $booking = $modelBooking->getById($emailLog['reference_id']);
                            if ($booking) {
                                $logUserEvent['event_on'] = $booking['booking_num'];
                            } else {
                                $logUserEvent['event_on'] = $emailLog['to'];
                            }
                            break;
                        case 'inquiry':
                            $inquiry = $modelInquiry->getById($emailLog['reference_id']);
                            if ($inquiry) {
                                $logUserEvent['event_on'] = $inquiry['inquiry_num'];
                            } else {
                                $logUserEvent['event_on'] = $emailLog['to'];
                            }
                            break;
                        case 'customer':
                            $customer = $modelCustomer->getById($emailLog['reference_id']);
                            if ($customer) {
                                $logUserEvent['event_on'] = get_customer_name($customer);
                            } else {
                                $logUserEvent['event_on'] = $emailLog['to'];
                            }
                            break;
                    }

                    $logUserEvent['action'] = 'javascript:;';
                    $logUserEvent['onclick'] = "showPopupWindow('{$this->router->assemble(array('id' => $logUserEvent['item_id']), 'emailDetails')}','470','800','Email');";
                    $logUserEvent['event_name'] = $logUserEvent['item_type'];
                    break;

                case 'report':
                    $report = $modelReport->getById($logUserEvent['item_id']);
                    if ($report) {
                        $logUserEvent['event_on'] = ucwords(str_replace('_', ' ', $report['report_type']));

                        $logUserEvent['action'] = $this->router->assemble(array('id' => $logUserEvent['item_id']), 'savedReportDownload');
                    } else {
                        $logUserEvent['action'] = 'javascript:;';
                    }
                    $logUserEvent['event_name'] = $logUserEvent['item_type'];
                    break;
                case 'login':
                    $logUserEvent['event_on'] = 'login';
                    $logUserEvent['action'] = 'javascript:;';
                    $logUserEvent['event_name'] = 'login';
                    break;
				//Rand	
				case 'contractor':
				$userInfo = $modelUser->getById($logUserEvent['item_id']);
						if($userInfo){
							
                        $logUserEvent['event_on'] = $userInfo['username'];
						
                        $logUserEvent['action'] = $this->router->assemble(array('contractor_id' => $userInfo['user_id']), 'settingsContractorInfoList');
						} 
						else {
                        $logUserEvent['action'] = 'javascript:;';
						}
						$logUserEvent['event_name'] = $logUserEvent['item_type'];
                    break;
				//End Rand
            }
        }

        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->logUserEvents = $logUserEvents;
        $this->view->myDashboard = $myDashboard;
        $this->view->from = !empty($filters['from']) ? date('Y-m-d', strtotime($filters['from'])) : '';
        $this->view->to = !empty($filters['to']) ? date('Y-m-d', strtotime($filters['to'])) : '';
        $this->view->user_id = !empty($filters['user_id']) ? $filters['user_id'] : '';
    }

    public function recentEmailsAction() {
        
        $this->view->page_title = 'Recent Emails - '.$this->view->page_title;

        CheckAuth::checkPermission(array('dashboard'));
        CheckAuth::checkPermission(array('emailDetails'));


        // load model
        $modelEmailLog = new Model_EmailLog();

        $loggedUser = CheckAuth::getLoggedUser();
        $dasboardStatus = CheckAuth::getDashboardStatusSession();

        $orderBy = $this->request->getParam('sort', 'created');
        $sortingMethod = $this->request->getParam('method', 'desc');
        $currentPage = $this->request->getParam('page', 1);

        $myDashboard = false;
        $filters = array();

        if ($dasboardStatus == 'my_dashboard') {
            $myDashboard = true;
        }
        if ($myDashboard) {
            $filters['user_id'] = $loggedUser['user_id'];
        }


        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = 30;
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        $bookingEmails = $modelEmailLog->getAll($filters, "{$orderBy} {$sortingMethod}", $pager, 100);
        $modelEmailLog->fills($bookingEmails, array('booking'));

        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->bookingEmails = $bookingEmails;
    }

    //Added by Walaa
    public function recentCallsAction() {
        
        $this->view->page_title = 'Recent Calls - '.$this->view->page_title;

        CheckAuth::checkPermission(array('dashboard'));
//  CheckAuth::checkPermission(array('callDetails'));
        // load model
        $modelCallLog = new Model_CallLog();

        $loggedUser = CheckAuth::getLoggedUser();
        $dasboardStatus = CheckAuth::getDashboardStatusSession();

        $orderBy = $this->request->getParam('sort', 'created');
        $sortingMethod = $this->request->getParam('method', 'desc');
        $is_first_time = $this->request->getParam('is_first_time');
        $page_number = $this->request->getParam('page_number');

        $myDashboard = false;
        $filters = array();


        $result = array();
        $sid = "ACd698f0b22aa408a0865abb86cc97b35d";
        $token = "13359760efe3d9ff0057ebf0df7213dc";


//        foreach ($client->recordings->read() as $recording) {
//            echo $recording->duration;
//        }


        if ($dasboardStatus == 'my_dashboard') {
            $myDashboard = true;
        }
        if ($myDashboard) {
            $filters['user_id'] = $loggedUser['user_id'];
        }

        if ($this->request->isPost()) {
            if (isset($page_number)) {
                $perPage = 20;
                $currentPage = $page_number + 1;
            }


            $callLogs = $modelCallLog->getAll($filters, "{$orderBy} {$sortingMethod}", $pager, 0, $perPage, $currentPage);
            //  $callLogs = $modelCallLog->getAll($filters, "{$orderBy} {$sortingMethod}", $perPage, 100);
//            $modelCallLog->fills($callLogs, array('contractor', 'customer'));
            // set view params
            //
             $client = new Services_Twilio($sid, $token);
            foreach ($client->account->recordings->getIterator(0, 50, array()) as $recording) {
                foreach ($callLogs as $key => $callLog) {
                   if($recording->call_sid == $callLog['call_sid']){
                       $callLogs[$key]['url']= 'http://api.twilio.com'.$recording->uri;
                   }
                }
                //echo $recording->sid .' - '.$recording->uri. " -- " . $recording->date_created . "\n";
            }
           
            $this->view->data = $callLogs;
            $this->view->is_first_time = $is_first_time;
            $result['data'] = $this->view->render('dashboard/draw-node-recent-calls.phtml');
            if ($callLogs) {
                $result['is_last_request'] = 0;
            } else {
                $result['is_last_request'] = 1;
            }

            echo json_encode($result);
            exit;
//            $this->view->currentPage = $currentPage;
//            $this->view->callLogs = $callLogs;
        }
//        $this->view->isDeleted = $isDeleted;
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
    }

    public function customRecentEmailsAction() {

        CheckAuth::checkPermission(array('dashboard'));
        CheckAuth::checkPermission(array('emailDetails'));


        // load model
        $modelEmailLog = new Model_EmailLog();

        $loggedUser = CheckAuth::getLoggedUser();
        $dasboardStatus = CheckAuth::getDashboardStatusSession();

        $orderBy = $this->request->getParam('sort', 'created');
        $sortingMethod = $this->request->getParam('method', 'desc');
        $currentPage = $this->request->getParam('page', 1);

        $myDashboard = false;
        $filters = array();

        if ($dasboardStatus == 'my_dashboard') {
            $myDashboard = true;
        }
        if ($myDashboard) {
            $filters['user_id'] = $loggedUser['user_id'];
        }


        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = 30;
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        //$bookingEmails = $modelEmailLog->getAll($filters, "{$orderBy} {$sortingMethod}", $pager, 100);

        $all = $modelEmailLog->getEmailInvForSaid();

        $modelEmailLog->fills($all, array('booking'));

        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->bookingEmails = $all;
    }

    public function cronjobHistoryAction() {
        
        $this->view->page_title = 'Cronjob History - '.$this->view->page_title;


        CheckAuth::checkPermission(array('dashboard'));
        //CheckAuth::checkPermission(array('emailDetails'));
        // load model
        $model_cronjobHistory = new Model_CronjobHistory();

        $loggedUser = CheckAuth::getLoggedUser();
        $dasboardStatus = CheckAuth::getDashboardStatusSession();

        $orderBy = $this->request->getParam('sort', 'run_time');
        $sortingMethod = $this->request->getParam('method', 'desc');
        $currentPage = $this->request->getParam('page', 1);

        $myDashboard = false;
        $filters = array();
        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = 30;
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        $cronjobHistory = $model_cronjobHistory->getAll($filters, "{$orderBy} {$sortingMethod}", $pager, 100);
        // $modelEmailLog->fills($bookingEmails, array('booking'));
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->cronjobHistory = $cronjobHistory;
    }

    public function replayRecentEmailsAction() {

        CheckAuth::checkPermission(array('dashboard'));
        CheckAuth::checkPermission(array('emailDetails'));

        $pdf_attachment = $this->request->getParam('pdf_attachment', 0);
        $emailLogId = $this->request->getParam('id', 0);

        $modelEmailLog = new Model_EmailLog();
        $emailLog = $modelEmailLog->getById($emailLogId);

        $pdf_num = '';
        switch ($emailLog['type']) {
            case 'invoice':
                $modelBookingInvoice = new Model_BookingInvoice();
                $invoice = $modelBookingInvoice->getById($emailLog['reference_id']);
                if ($invoice) {
                    $pdf_num = $invoice['invoice_num'];
                }
                break;
            case 'estimate':
                $modelBookingEstimate = new Model_BookingEstimate();
                $estimate = $modelBookingEstimate->getById($emailLog['reference_id']);
                if ($estimate) {
                    $pdf_num = $estimate['estimate_num'];
                }
                break;
            case 'booking':
                $modelBooking = new Model_Booking();
                $booking = $modelBooking->getById($emailLog['reference_id']);
                if ($booking) {
                    $pdf_num = $booking['booking_num'];
                }
                break;
        }

        $body = $emailLog['body'];
        $subject = $emailLog['subject'];
        $to = $emailLog['to'];

        if ($this->request->isPost()) {
            $to = $this->request->getParam('to');
            $cc = $this->request->getParam('cc');
            $subject = $this->request->getParam('subject');
            $body = $this->request->getParam('body');

            $params = array(
                'to' => $to,
                'cc' => $cc,
                'body' => $body,
                'subject' => $subject,
                'layout' => 'empty_email_template'
            );

            if (!empty($pdf_attachment)) {
                switch ($emailLog['type']) {
                    case 'invoice':
                        $modelBookingInvoice = new Model_BookingInvoice();
                        $viewParam = $modelBookingInvoice->getInvoiceViewParam($emailLog['reference_id']);
                        $view = new Zend_View();
                        $view->setScriptPath(APPLICATION_PATH . '/modules/invoices/views/scripts/index');
                        $view->bookingServices = $viewParam['bookingServices'];
                        $view->thisBookingServices = $viewParam['thisBookingServices'];
                        $view->priceArray = $viewParam['priceArray'];
                        $view->invoice = $viewParam['invoice'];
                        $view->customer = $viewParam['customer'];
                        $view->isAccepted = $viewParam['isAccepted'];
                        $view->booking = $viewParam['invoice']['booking'];
                        $bodyInvoice = $view->render('invoice.phtml');
                        // Create pdf
                        $pdfPath = createPdfPath();
                        $destination = $pdfPath['fullDir'] . $viewParam['invoice']['invoice_num'] . '.pdf';
                        wkhtmltopdf($bodyInvoice, $destination);
                        $params['attachment'] = $destination;
                        break;
                    case 'estimate':
                        $modelBookingEstimate = new Model_BookingEstimate();
                        $viewParam = $modelBookingEstimate->getEstimateViewParam($emailLog['reference_id']);
                        $view = new Zend_View();
                        $view->setScriptPath(APPLICATION_PATH . '/modules/estimates/views/scripts/index');
                        $view->bookingServices = $viewParam['bookingServices'];
                        $view->thisBookingServices = $viewParam['thisBookingServices'];
                        $view->priceArray = $viewParam['priceArray'];
                        $view->estimate = $viewParam['estimate'];
                        $bodyEstimate = $view->render('estimate.phtml');
                        // Create pdf
                        $pdfPath = createPdfPath();
                        $destination = $pdfPath['fullDir'] . $viewParam['estimate']['estimate_num'] . '.pdf';
                        wkhtmltopdf($bodyEstimate, $destination);
                        $params['attachment'] = $destination;
                        break;
                    case 'booking':
                        $modelBooking = new Model_Booking();
                        $viewParam = $modelBooking->getBookingViewParam($emailLog['reference_id']);
                        $view = new Zend_View();
                        $view->setScriptPath(APPLICATION_PATH . '/modules/booking/views/scripts/index');
                        $view->booking = $viewParam['booking'];
                        $view->bookingServices = $viewParam['bookingServices'];
                        $view->thisBookingServices = $viewParam['thisBookingServices'];
                        $view->priceArray = $viewParam['priceArray'];
                        $bodyBooking = $view->render('booking-customer.phtml');
                        // Create pdf
                        $pdfPath = createPdfPath();
                        $destination = $pdfPath['fullDir'] . $viewParam['booking']['booking_num'] . '.pdf';
                        wkhtmltopdf($bodyBooking, $destination);
                        $params['attachment'] = $destination;
                        break;
                }
            }

            $error_mesages = array();
            if (EmailNotification::validation($params, $error_mesages)) {

                // Send Email
                $success = EmailNotification::sendEmail($params, '', array(), array('reference_id' => $emailLog['reference_id'], 'type' => $emailLog['type']));

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Email sent"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Failed to send email"));
                }

                echo 1;
                exit;
            }
            $this->view->error_mesages = $error_mesages;
        }

        $this->view->to = $to;
        $this->view->subject = $subject;
        $this->view->body = $body;
        $this->view->email_log_id = $emailLogId;
        $this->view->pdf_num = $pdf_num;

        echo $this->view->render('dashboard/replay-recent-emails.phtml');
        exit;
    }

    public function downloadEmailPdfFileAction() {

        //
        // get params 
        //
        $logId = $this->request->getParam('id', 0);

        //
        // load models
        //
        $modelEmailLog = new Model_EmailLog();

        //
        // geting data
        //
        $log = $modelEmailLog->getById($logId);

        //
        // validation
        //
        if (!$log) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Email not found"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }


        $filename = $log['attachment_path'];

        $attachment = explode(',', $log['attachment_path']);
        foreach ($attachment as $key => $filename) {
            if (file_exists($filename)) {
                /* header("Pragma: public");
                  header("Expires: 0");
                  header("Pragma: no-cache");
                  header("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
                  header("Content-Type: application/force-download");
                  header("Content-Type: application/octet-stream");
                  header("Content-Type: application/download");
                  header('Content-disposition: attachment; filename=' . basename($filename));
                  header("Content-Type: application/pdf");
                  header("Content-Transfer-Encoding: binary");
                  header('Content-Length: ' . filesize($filename));
                  @readfile($filename); */

                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="' . basename($filename) . '"');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($filename));
                readfile($filename);
            }
        }
        exit(0);
    }

    //18/05/2015 D.A
    // calculate the Amount of  how much was booked in the selected day
    /* public function dailyBookingAmountAction() {
      // load model
      $modelBooking = new Model_Booking();
      // initialize variables

      $totalAmountDetails['total_amount']=0.0;
      $companyId = CheckAuth::getCompanySession();
      $startDate = $this->_request->getParam('sd');
      $endDate = $this->_request->getParam('ed');
      $date=$startDate;
      $dateArray = array();
      $amountArray = array();
      $bookingIdArray = array();
      $bookingCountArray = array();

      while ($date <= $endDate) {
      $totalAmount =0.0;
      $bookingCount =0;
      $dateBookings = $modelBooking->getBookingByDateAndStatus($date ,'TO DO',$companyId);
      if ($dateBookings) {
      foreach ($dateBookings as $dateBooking) {
      // Call a function that calculate the total amount of booking according to date
      $totalAmountDetails = $modelBooking->getTotalAmountBookingDetails($dateBooking);
      // Add the amount of each booking created at specific date
      $totalAmount+= $totalAmountDetails['total_amount'];
      $bookingCount++;
      $bookingIdArray[]=$dateBooking;

      }
      }

      $dateArray[] = $date;
      $amountArray[] = $totalAmount;
      $bookingCountArray[]=$bookingCount;
      $date=date('Y-m-d', strtotime('+1 day', strtotime($date)));

      }

      $results = array(
      'date' =>$dateArray,
      'bookingId' =>$bookingIdArray,
      'bookingCount' =>$bookingCountArray,
      'amount' => $amountArray
      );

      echo $this->_helper->json($results);
      exit;
      }
     */
    public function dailyBookingAmountAction() {
        CheckAuth::checkPermission(array('dashboard'));
        //check login
        CheckAuth::checkLoggedIn();
        // load model
        $modelBooking = new Model_Booking();
        // initialize variables
        $loggedUser = CheckAuth::getLoggedUser();
        $totalAmountDetails['total_amount'] = 0.0;
        $companyId = CheckAuth::getCompanySession();
        $startDate = $this->_request->getParam('sd');
        $endDate = $this->_request->getParam('ed');
        $date = $startDate;
        $dateArray = array();
        $amountArray = array();
        $bookingIdArrays = array();
        $bookingCountArray = array();
        $myDashboard = false;

        if (!CheckAuth::getDashboardStatusSession()) {
            if (CheckAuth::checkCredential(array('canSeeAllBookingStatistics'))) {
                CheckAuth::setDashboardStatusSession('all_dashboard');
            } else {
                CheckAuth::setDashboardStatusSession('my_dashboard');
            }
        }

        $dasboardStatus = CheckAuth::getDashboardStatusSession();
        if ($dasboardStatus == 'my_dashboard') {
            $myDashboard = true;
        }

        while ($date <= $endDate) {
            $totalAmount = 0.0;
            $bookingCount = 0;
            $bookingIdArray = array();
            $dateBookings = $modelBooking->getBookingByDateAndStatus($date, 'TO DO', $companyId, $loggedUser['user_id'], $myDashboard);
            if ($dateBookings) {
                foreach ($dateBookings as $dateBooking) {
                    // Call a function that calculate the total amount of booking according to date
                    $totalAmountDetails = $modelBooking->getTotalAmountBookingDetails($dateBooking);
                    // Add the amount of each booking created at specific date
                    $totalAmount+= $totalAmountDetails['total_amount'];
                    $bookingCount++;
                    $bookingIdArray[] = $dateBooking;
                }
            }
            $part = 'date';
            if(getNewDateFormat($date)){
                    $dateArray[] =  getNewDateFormat($date,'date'); //display anly date
            }else{
                    $dateArray[] = $date;
            }
            $amountArray[] = $totalAmount;
            $bookingCountArray[] = $bookingCount;
            $bookingIdArrays[] = $bookingIdArray;
            $date = date('Y-m-d', strtotime('+1 day', strtotime($date)));
        }

        $results = array(
            'date' => $dateArray,
            'bookingId' => $bookingIdArrays,
            'bookingCount' => $bookingCountArray,
            'amount' => $amountArray
        );

        echo $this->_helper->json($results);
        exit;
    }

    public function recentSmsAction() {
        
        $this->view->page_title = 'Recent SMS - '.$this->view->page_title;
        //for review
        //CheckAuth::checkPermission(array('dashboard'));
        //CheckAuth::checkPermission(array('emailDetails'));
        $modelSmsHistory = new Model_SmsHistorty();
        $loggedUser = CheckAuth::getLoggedUser();
        $dasboardStatus = CheckAuth::getDashboardStatusSession();
        $orderBy = $this->request->getParam('sort', 'el.id');
        $smsType = $this->request->getParam('sms_type');
        $sms_id = $this->request->getParam('sms_id');
        $mobileNum = $this->request->getParam('mobile_no');
        $sortingMethod = $this->request->getParam('method', 'desc');
        $currentPage = $this->request->getParam('page', 1);

        $modelBooking = new Model_Booking();
        $sms_count = $modelSmsHistory->getCountMessage();
        if (count($sms_count) > 0)
            $modelBooking->cronJobGetSmsInfo();

        $myDashboard = false;
        $filters = array();
        if ($smsType != "") {
            $filters['sms_type'] = $smsType;
            $this->view->smsType = $smsType;
        }
        if ($sms_id != "") {
            $filters['sms_id'] = $sms_id;
            $this->view->sms_id = $sms_id;
        }

        if ($mobileNum != "") {
            $mobileWithoutSpace = "+" . str_replace(' ', '', $mobileNum);
            $filters['mobileNum'] = $mobileWithoutSpace;
            $this->view->mobileNum = $mobileWithoutSpace;
        }

        if ($dasboardStatus == 'my_dashboard') {
            $myDashboard = true;
        }
        if ($myDashboard) {
            $filters['user_id'] = $loggedUser['user_id'];
        }
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        $twilioSms = $modelSmsHistory->getAll($filters, "{$orderBy} {$sortingMethod}", $pager, 100);
        $mobiles = $this->getAllMobileNumber();
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->twilioSms = $twilioSms;
        $this->view->mobiles = $mobiles;
    }

    public function getAllMobileNumber() {
        $modelSmsHistorty = new Model_SmsHistorty();
        $data = $modelSmsHistorty->getAllMobileNumber();
        return $data;
    }

}
