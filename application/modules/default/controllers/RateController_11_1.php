<?php

class RateController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
    }

    /**
     * Items list action
     */
    public function indexAction() {
//        echo "salim";
//        exit;
    }

    public function customerRateAction() {

        $model_ratingTag = new Model_RatingTag();
        $model_contractorRate = new Model_ContractorRate();
        
         $item_id = $this->request->getParam('item_id');
        $item_type = $this->request->getParam('type');

//        $item_id = $this->request->getParam("booking_id");
//        var_dump($bookingId);
//        exit
//        $item_type = $this->request->getParam("type");
//        if ($item_type == 'booking') {

        $model_contractorServiceBooking = new Model_ContractorServiceBooking();
        $contractors = $model_contractorServiceBooking->getContractorsDataByBookingId($item_id);
         foreach ($contractors as $key => $contractor) {
            $ratingTag[$contractor['contractor_id']] = $model_ratingTag->getAll($contractor['contractor_id'], 1, $item_id);
        }
        
        $this->view->item_id = $item_id;
        $this->view->contractors = $contractors;
        $this->view->ratingTag = $ratingTag;
        if ($this->request->isPost()) {
            $rate = $this->request->getParam('rate');
            $tag_id = $this->request->getParam('tag_id');
            $contractor_id = $this->request->getParam('contractor_id');
            $item_id = $this->request->getParam('item_id');
            $item_type = $this->request->getParam('type');
            $loggedUser = CheckAuth::getLoggedUser();
            $rated_by = $loggedUser['user_id'];
            $role_id = $loggedUser['role_id'];

            $rate_data = array(
                'contractor_id' => $contractor_id,
                'rated_by' => $rated_by,
                'rate' => $rate,
                'user_role' => $role_id,
                'created' => time(),
                'rating_tag_id' => $tag_id,
                'item_id' => $item_id,
                'item_type' => $item_type
            );

            $previousRate = $model_contractorRate->getByContractorIdAndRatedByAndItemIdAndTagId($contractor_id, $rated_by, $item_id, $tag_id);
            if (!empty($previousRate)) {

                $success = $model_contractorRate->updateById($previousRate[0]['contractor_rate_id'], $rate_data);
            } else {
                $success = $model_contractorRate->insert($rate_data);
            }
            if ($success) {
                $result = $model_contractorRate->getByContractorIdAndRatedByAndItemIdAndTagId($contractor_id, $rated_by, $item_id);

                foreach ($result as $key => $data) {
                    $rate_db[] = $data;
                    $sum_rates[] = $data['rate'];
                }
                $rate_times = count($rate_db);
                $sum_rates = array_sum($sum_rates);
                $total = $sum_rates / $rate_times;
//                $rate_bg = (($rate_value) / 5) * 100;
                echo json_encode(array("rate" => $rate, 'total' => floor($total)));
            }
            exit;
        }
//        $contractor_id = $this->request->getParm('contractor_id');
//        $rate = $this->request->getParm('rate');
//        $rate = $this->request->getParm('rate');
// echo $this->view->render('rate/customer-rate.phtml');
//exit;
    }

    public function customerRateEmailAction() {
        $model_booking = new Model_Booking();
        $id = $this->request->getParam('item_id', 0);
//        $type = $this->request->getParam('type');
     
        $model_tradingName = new Model_TradingName();

        $booking_data = $model_booking->getById($id);
        $customer_id = $booking_data['customer_id'];
        $default_page = $this->router->assemble(array('id' => $id), 'bookingView').'?type=rate';
        $customer_model = new Model_Customer();
        $customer = $customer_model->getById($customer_id);
        $authRole_model = new Model_AuthRole();
        $auth = Zend_Auth::getInstance();
        $authStorge = $auth->getStorage();
        $role = $authRole_model->getRoleIdByName('Customer');
        $customer['role_id'] = $role;
        $user_id = $customer['customer_id'];
        $customer['user_id'] = $user_id;
        $customer['username'] = $customer['first_name'];
        $customer['default_page'] = $default_page;
        $customer_obj = (object) $customer;
        $authStorge->write($customer_obj);
        CheckAuth::redirectCustomer($default_page);
    }

}
