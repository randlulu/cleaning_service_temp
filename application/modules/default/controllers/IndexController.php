<?php

class IndexController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        /* Initialize action controller here */
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
    }

    public function testSystemAction() {
        $modelBooking = new Model_Booking();
        $modelBooking->cronJobReminderCustomerBookingConfirmationOneDay();
        exit;
    }

    public function receiveIncomingMsgTwilioAction() {
        $messageSid = $this->request->getParam('MessageSid');
        $smsSid = $this->request->getParam('SmsSid');
        $from = $this->request->getParam('From');
        $to = $this->request->getParam('To');
        $body = $this->request->getParam('Body');
        $modelUser = new Model_User();
        $modelCustomer = new Model_Customer();
        $modelSmsHistory = new Model_SmsHistorty();
        $mobileFormat = $modelSmsHistory->getMobileFormat($from);
        $userInfo = $modelUser->getByMobile($mobileFormat);
        $modelBooking = new Model_Booking();
        $sms_count = $modelSmsHistory->getCountMessage();
        if (count($sms_count) > 0)
            $modelBooking->cronJobGetSmsInfo();
        if ($userInfo) {
            $userId = $userInfo['user_id'];
            $referenceId = -1;
            $customerInfo = $modelCustomer->getByMobile($mobileFormat);
            if ($customerInfo)
                $both = "both";
            else
                $both = "";
        }
        else {
            $customerInfo = $modelCustomer->getByMobile($mobileFormat);
            $userId = $customerInfo['customer_id'];
            $referenceId = -2;
            $both = "";
        }
        $smsInfo = $modelSmsHistory->getLastMessageId($from);
        $type_id = $smsInfo['reason_id'];
        $reason = $smsInfo['sms_reason'];

        $filters = array();
        if ($reason == "booking" && isset($customerInfo['customer_id'])) {
            $for_booking = 1;
            $filters['for_booking'] = $for_booking;
        }

        if ($from != "") {
            $params = array(
                'reference_id' => $referenceId,
                'from' => $from,
                'to' => '+61447075733',
                'message' => $body,
                'receiver_id' => $userId,
                'message_sid' => $messageSid,
                'sms_type' => 'received',
                'sender_type' => $both,
                'sms_reason' => $reason,
                'reason_id' => $type_id,
                'template_type' => 'standard',
            );

            $id = $modelSmsHistory->insert($params);
            MobileNotification::notify(0, 'new sms', $filters, $id);
        }
        /* header("content-type: application/xml");
          $file = file_get_contents('http://temp.tilecleaners.com.au/response.php');
          echo $file; */
        exit;
    }

    public function indexAction() {

        // $this->_helper->layout->setLayout('layout_subscription');

        if (Zend_Auth::getInstance()->hasIdentity()) {
            $loggedUser = Zend_Auth::getInstance()->getIdentity();
            /*             * ****Check if seesion is Octopus Employee*****IBM */
            if (isset($loggedUser->is_employee) && $loggedUser->is_employee == 'octopus') {
                CheckAuth::checkLoggedIn();
            } else {
                /*                 * **End**** */
                $modelAuthRole = new Model_AuthRole();
                $authRole = $modelAuthRole->getById($loggedUser->role_id);
                $defaultPage = $authRole['default_page'];

                $messages = $this->_helper->flashMessenger->getMessages();
                if ($_SERVER['REMOTE_ADDR'] == '176.106.46.142') {
                    print_r($messages);
                }
                if (count($messages)) {
                    foreach ($messages as $message) {
                        $this->_helper->flashMessenger->addMessage(array('type' => $message['type'], 'message' => $message['message']));
                        if (CheckAuth::getRoleName() == 'customer') {
                            $this->_redirect("/logout");
                        }
                    }
                }

                $this->_redirect($defaultPage);
            }
        }

//
// get request parameters
//
        $keepSign = $this->request->getParam('keep_sign', 0);
        $email = $this->request->getParam('email');
        $password = $this->request->getParam('password');
//
// init action form
//
        $form = new Form_Login();

//
// handling the insertion process
//
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $authrezed = $this->getAuthrezed();

                $authrezed->setIdentity($email);
                $authrezed->setCredential(sha1($password));

                $auth = Zend_Auth::getInstance();
                $result = $auth->authenticate($authrezed);

                if ($result->isValid()) {

                    $identity = $authrezed->getResultRowObject();


                    $authStorge = $auth->getStorage();
                    $authStorge->write($identity);

                    if ($keepSign) {

                        $session = new Zend_Session_Namespace();
                        $session->setExpirationSeconds(strtotime('30 day', 0));
                        Zend_Session::rememberMe();
                    } else {
                        Zend_Session::forgetMe();
                    }
                    CheckAuth::afterlogin();
                } else {

                    $form->getElement('email')->setErrors(array('invalid email or password'));
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => 'invalid email or password'));
                    $this->_redirect($this->router->assemble(array(), 'Login'));
                }
            }
        }

        /// from mohammed  check to domain when get to login page  and we want to display  propriate company name and logo 
        $domain = $_SERVER['HTTP_HOST'];
        $start = '.';
        $end = '.';
        $string = ' ' . $domain;
        $ini = strpos($string, $start);
        if ($ini == 0)
            return '';
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        $returnString = substr($string, $ini, $len);

        if ($returnString == 'tilecleaners') {
            $this->view->tilecleaner = 'tilecleaner';
        } else {
            $this->view->pathLogo = '/pic/octlogo.png';
            $this->view->logo_name = 'OctopusPro';
        }


        //  $this->view->pathLogo = '/uploads/company_logo/2012/03/07/logo_1.png' ;    
        // $this->view->logo_name = 'The Tile Cleaners';
        //////

        $this->view->form = $form;
    }

    public function getAuthrezed() {
        $authrezed = new Zend_Auth_Adapter_DbTable(Zend_Db_Table::getDefaultAdapter());
        $authrezed->setTableName('user')
                ->setIdentityColumn('email1')
                ->setCredentialColumn('password')
                ->setCredentialTreatment('? AND active = "TRUE"')
                ->setCredentialTreatment('? AND blocked = 0');

        return $authrezed;
    }

    public function logoutAction() {
        CheckAuth::logout();
        $this->_redirect($this->router->assemble(array(), 'Login'));
    }

    public function myAccountAction() {
//
//check login
//
        CheckAuth::checkLoggedIn();

        $loggedUser = CheckAuth::getLoggedUser();

        $modelUser = new Model_User();
        $user = $modelUser->getById($loggedUser['user_id']);

        $pageName = $this->request->getParam('page_name', '');
        $this->view->page_title = (!empty($pageName)) ? $pageName . " - My Account" : "My Account";

        $accountModel = new Model_Account();
        $account = $accountModel->getByCreatedBy($loggedUser['user_id']);
        if ($account) {
            $companiesObj = new Model_Companies();
            $comp = $companiesObj->getById($account['company_id']);

            if (isset($comp) and $comp['company_logo'] != '')
                $this->view->company_logo = $comp['company_logo'];
        }

        if ($user) {
            $this->view->user = $user;
            $this->view->contractor_id = $user['user_id'];
            $modelAuthRole = new Model_AuthRole();
            $role = $modelAuthRole->getById($user['role_id']);
            $this->view->role = $role;
            $line_address = get_line_address($user);
            $is_address = false;

            if (empty($line_address)) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "There are no Address for this Booking"));
                $this->_redirect($this->router->assemble(array(), 'Login'));
            } else {
                $MAP_OBJECT_Booking = new GoogleMapAPI();
                $MAP_OBJECT_Booking->setHeight(200);
                $MAP_OBJECT_Booking->setWidth('100%');
                $MAP_OBJECT_Booking->setMapType('map');
                $MAP_OBJECT_Booking->addMarkerByAddress($line_address);
                $this->view->MAP_OBJECT_Booking = $MAP_OBJECT_Booking;
                $is_address = true;
            }
            $this->view->is_address = $is_address;
            $this->view->line_address = $line_address;
            if ('contractor' == $role['role_name']) {
// booking 

                $contractorInfoObj = new Model_ContractorInfo();
                $contractorInfo = $contractorInfoObj->getByContractorId($loggedUser['user_id']);


                $modelContractorRate = new Model_ContractorRate();
                $contractorRate = $modelContractorRate->getRateByContractor($loggedUser['user_id']);
//                $this->view->contractorInfo = $contractorInfo;
//                $this->view->bookingStatusCounts = $counts;

                if ($contractorInfo) {

                    //get contractorOwner
                    $contractorOwnerObj = new Model_ContractorOwner();
                    $contractorOwner = $contractorOwnerObj->getAll(array('contractor_info_id' => $contractorInfo['contractor_info_id']));

                    $this->view->contractorOwner = $contractorOwner;

                    //get contractorEmployee
                    $contractorEmployeeObj = new Model_ContractorEmployee();
                    $contractorEmployee = $contractorEmployeeObj->getAll(array('contractor_info_id' => $contractorInfo['contractor_info_id']));

                    $this->view->contractorEmployee = $contractorEmployee;

                    //get DeclarationOfChemicals
                    $declarationOfChemicalsObj = new Model_DeclarationOfChemicals();
                    $declarationOfChemicals = $declarationOfChemicalsObj->getByContractorInfoId($contractorInfo['contractor_info_id']);

                    $this->view->declarationOfChemicals = $declarationOfChemicals;

                    $contractorInsuranceObj = new Model_ContractorInsurance();
                    $contractorInsurances = $contractorInsuranceObj->getAll(array('contractor_info_id' => $contractorInfo['contractor_info_id']));

                    $this->view->contractorInsurance = $contractorInsurances;

                    //get DeclarationOfEquipment
                    $declarationOfEquipmentObj = new Model_DeclarationOfEquipment();
                    $declarationOfEquipment = $declarationOfEquipmentObj->getByContractorInfoId($contractorInfo['contractor_info_id']);

                    $this->view->declarationOfEquipment = $declarationOfEquipment;

                    //get DeclarationOfOtherApparatus
                    $declarationOfOtherApparatusObj = new Model_DeclarationOfOtherApparatus();
                    $declarationOfOtherApparatus = $declarationOfOtherApparatusObj->getByContractorInfoId($contractorInfo['contractor_info_id']);

                    $this->view->declarationOfOtherApparatus = $declarationOfOtherApparatus;

                    //get Vehicle
                    $contractorVehicleObj = new Model_ContractorVehicle();
                    $contractorVehicle = $contractorVehicleObj->getByContractorInfoId($contractorInfo['contractor_info_id']);

                    $this->view->contractorVehicle = $contractorVehicle;

                    $model_workingHours = new Model_WorkingHours();
                    $filters = array(
                        'contractor_id' => $contractorInfo['contractor_id']
                    );
                    $working_hours = $model_workingHours->getAll($filters);
//                    var_dump($working_hours);
//                    exit;
                    $this->view->working_hours = $working_hours;

                    // get insurance attachment
                    $modelAttachment = new Model_Attachment();
                    $filter = array('type' => 'insurance', 'itemid' => $contractorInfo['contractor_info_id']);
                    $pager = null;
                    $insuranceAttachments = $modelAttachment->getAll('a.created desc', $pager, $filter);
                    if ($insuranceAttachments) {
                        $this->view->insuranceAttachments = $insuranceAttachments;
                    }

                    // get licence attachment
                    $filter = array('type' => 'licence', 'itemid' => $contractorInfo['contractor_info_id']);
                    $pager = null;
                    $licenceAttachments = $modelAttachment->getAll('a.created desc', $pager, $filter);
                    if ($licenceAttachments) {
                        $this->view->licenceAttachments = $licenceAttachments;
                    }
                    $this->view->contractorInfo = $contractorInfo;
                    $this->view->contractorRate = $contractorRate;
                }
            } else {
                $userInfoObj = new Model_UserInfo();
                $userInfo = $userInfoObj->getByUserId($loggedUser['user_id']);

                $this->view->userInfo = $userInfo;
            }
            $modelBookingStatus = new Model_BookingStatus();
            $BookingStatus = $modelBookingStatus->getAllWithoutPermission();
            $modelClaimOwner = new Model_ClaimOwner();
            $modelMissedCalls = new Model_MissedCalls();
            $modelComplaint = new Model_Complaint();
            $modelPayment = new Model_Payment();
//            $modelBookingContractorPayment = new Model_BookingContractorPayment();
            $modelBooking = new Model_Booking();
            $counts = array();
            foreach ($BookingStatus as $status) {
                $results = array();
                $total = $modelBooking->getcountBookingsByStatus($status['name'], $user['user_id']);
                $results['name'] = $status['name'];
                $results['booking_status_id'] = $status['booking_status_id'];
                $results['color'] = $status['color'];
                $results['total'] = $total;
                $counts[] = $results;
            }
            $modelContractorGmailAccounts = new Model_ContractorGmailAccounts();
            $contractorAccounts = $modelContractorGmailAccounts->getByContractorId($user['user_id']);

            $countAwaitingaccept = $modelBooking->getCountAwaitingAcceptBooking($user['user_id']);
            $countAwaitingUpdate = $modelBooking->getCountAwaitingupdateBooking($user['user_id']);
            $countUnapprovedBookingContractor = $modelBooking->getCountUnapprovedBooking($user['user_id']);
            $countRejectBookingContractor = $modelBooking->getCountRejectedBookings($user['user_id']);

            $countOpenComplaintForContractors = $modelComplaint->getComplaintCount(array('complaintStatus' => 'open', 'contractor_id' => $user['user_id']));
            $countUnapprovedComplaintForContractors = $modelComplaint->countUapprovedCompalints($user['user_id']);
            $countClaimOwnerContractor = $modelClaimOwner->getCountClaimOwner($user['user_id']);
            $countMissedCallsContractor = $modelMissedCalls->getCountMissedCalls($user['user_id']);

//            $countUnapprovedPaymentsForContractor = $modelPayment->getCountUnapprovedPayments($loggedUser['user_id']);




            $this->view->countAwaitingUpdate = $countAwaitingUpdate;
            $this->view->countAwaitingaccept = $countAwaitingaccept;
            $this->view->countUnapprovedBookingContractor = $countUnapprovedBookingContractor;
            $this->view->countRejectBookingContractor = $countRejectBookingContractor;
            //$this->view->countUnapprovedPaymentsForContractors = $countUnapprovedPaymentsForContractor;
            $this->view->countClaimOwnerContractor = $countClaimOwnerContractor;
            $this->view->countMissedCallsContaractor = $countMissedCallsContractor;
            $this->view->countOpenComplaintForContractor = $countOpenComplaintForContractors;
            $this->view->countUnapprovedComplaintForContractors = $countUnapprovedComplaintForContractors;
            $this->view->bookingStatusCounts = $counts;
            $this->view->contractorGmailAccount = $contractorAccounts['email'];
        } else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }
    }

    public function editWorkingHoursAction() {
        $model_workingHours = new Model_WorkingHours();
        $contractor_id = $this->request->getParam('contractor_id');
        $working_hours_id = $this->request->getParam('working_hours_id');

        $start_time = $this->request->getParam('start_time');
        $end_time = $this->request->getParam('end_time');
        $day = $this->request->getParam('day');
        if (!empty($working_hours_id)) {
            $filters = array(
                'contractor_id' => $contractor_id,
                'working_hours_id' => $working_hours_id
            );
            $working_hours = $model_workingHours->getAll($filters);
            $this->view->working_hours = $working_hours;
        }

        if ($this->request->isPost()) {
//            $model_workingHours->deleteByContractorId($contractor_id);
//            foreach ($days as $key => $day) {
            $data = array(
                'contractor_id' => $contractor_id,
                'day' => $day,
                'start_time' => $start_time,
                'end_time' => $end_time,
            );

            $workingHoursForContractor = $model_workingHours->getDayByContractorsId($data);

            if (!empty($workingHoursForContractor)) {
                foreach ($workingHoursForContractor as $workingHourForContractor) {
                    if ($workingHourForContractor == $data['day']) {
                        
                    }
                }
            }

            if (!empty($working_hours_id)) {
                $status = $model_workingHours->updateById($working_hours_id, $data);
            } else {
                $status = $model_workingHours->insert($data);
            }
//            }
            if ($status) {
                echo 1;
            }
            exit;
        }
        $this->view->contractor_id = $contractor_id;
        echo $this->view->render('my-account/edit-working-hours.phtml');
        exit;
    }

    public function deleteWorkingHoursAction() {
        $model_workingHours = new Model_WorkingHours();
        $working_hours_id = $this->request->getParam('working_hour_id');
        $model_workingHours->deleteById($working_hours_id);
        echo "Deleted successfuly";
        exit;
    }

// by abdallah
    public function userPicAction() {

        CheckAuth::checkLoggedIn();

        $loggedUser = CheckAuth::getLoggedUser();
        if ($this->request->getParam('id')) {

            $id = $this->request->getParam('id');
        } else {
            $id = $loggedUser['user_id'];
        }


        if ($this->request->isPost()) {

            $upload = new Zend_File_Transfer_Adapter_Http();

            $files = $upload->getFileInfo();
            $countFiles = count($files);

            //  var_dump('mm' .  $countFiles); exit; 


            foreach ($files as $file => $fileInfo) {
                if ($upload->isUploaded($file)) {
                    if ($upload->receive($file)) {
                        $info = $upload->getFileInfo($file);
                        $source = $info[$file]['tmp_name'];
                        $imageInfo = pathinfo($source);
                        $ext = $imageInfo['extension'];
                        $dir = get_config('user_picture');
                        $dir2 = get_config('user_picture_medium');
                        $dir3 = get_config('user_picture_small');

                        if (!is_dir($dir)) {
                            mkdir($dir, 0777, true);
                        }
                        if (!is_dir($dir2)) {
                            mkdir($dir2, 0777, true);
                        }
                        if (!is_dir($dir3)) {
                            mkdir($dir3, 0777, true);
                        }

                        $original_path = time() . "_" . $id . '.' . $ext;
                        $image_saved = copy($source, $dir . $original_path);

                        ImageMagick::create_thumbnail($source, $dir2 . $original_path, 210, 210);
                        ImageMagick::create_thumbnail($source, $dir3 . $original_path, 50, 50);

                        $modeluser = new Model_User();
                        if ($image_saved) {
                            $data = array('avatar' => $original_path, 'avatar_date' => date("Y-m-d H:i:s"),);
//print_r($data);
//echo '$this->contractor_id   '.$this->contractor_id;
                            if ($modeluser->updateById($id, $data)) {


                                $result['profile_image'] = '<img width="96" height="96" class="img-circle circle-border m-b-md" alt="profile" src="' . $this->getRequest()->getBaseUrl() . '/uploads/user_pic/thumb_medium/' . $original_path . '" /> ';
                                $result['profile_image_left_menu'] = '<img class="img-responsive" alt="profile" src="' . $this->getRequest()->getBaseUrl() . '/uploads/user_pic/thumb_medium/' . $original_path . '" /> ';
                                echo json_encode($result);
                            }
                        }
                    }
                }
            }
        }

        exit;
    }

    public function companyPicAction() {

        CheckAuth::checkLoggedIn();

        $loggedUser = CheckAuth::getLoggedUser();

        $id = $loggedUser['user_id'];

        // $accountModel = new Model_Account();
        //$account = $accountModel->getByCreatedBy($id);

        $modelUserCompanies = new Model_UserCompanies();
        $userCompanies = $modelUserCompanies->getCompaniesByUserId($id);
        $companyId = isset($userCompanies['company_id']) ? $userCompanies['company_id'] : 0;

        $companiesObj = new Model_Companies();
        // $id =$account['company_id'] ;
        //  $comp = $companiesObj->getById();



        if ($this->request->isPost()) {

            $upload = new Zend_File_Transfer_Adapter_Http();

            $files = $upload->getFileInfo();
            $countFiles = count($files);

            //  var_dump('mm' .  $countFiles); exit; 


            foreach ($files as $file => $fileInfo) {
                if ($upload->isUploaded($file)) {
                    if ($upload->receive($file)) {
                        $info = $upload->getFileInfo($file);
                        $source = $info[$file]['tmp_name'];
                        $imageInfo = pathinfo($source);
                        $ext = $imageInfo['extension'];


                        $dir = get_config('company_logo');
                        // var_dump($source  ,$imageInfo ,$dir) ;exit ;
                        // $dir2 = get_config('user_picture_medium');
                        // $dir3 = get_config('user_picture_small');

                        if (!is_dir($dir)) {
                            mkdir($dir, 0777, true);
                        }


                        $original_path = time() . "_" . $companyId . '.' . $ext;
                        //var_dump($original_path) ;exit ;

                        $image_saved = copy($source, $dir . $original_path);

                        //  ImageMagick::create_thumbnail($source, $dir2 . $original_path, 210, 210);
                        // ImageMagick::create_thumbnail($source, $dir3 . $original_path, 50, 50);

                        $modeluser = new Model_User();
                        if ($image_saved) {
                            $data = array('company_logo' => $original_path);
//print_r($data);
//echo '$this->contractor_id   '.$this->contractor_id;
                            if ($companiesObj->updateById($companyId, $data)) {


                                echo '<img border="0" height="108" width="100" id="complo" class="img-responsive" alt="company_logo" src="' . $this->getRequest()->getBaseUrl() . '/uploads/company_logo/' . $original_path . '" /> ';
                            }
                        }
                    }
                }
            }
        }

        exit;
    }

    public function changeEmailAction() {
//
//check login
//

        CheckAuth::checkLoggedIn();

//
// get request parameters
//
        $old_email = $this->request->getParam('old_email');
        $new_email = $this->request->getParam('new_email');
        $confirm_new_email = $this->request->getParam('confirm_new_email');

        $pageName = $this->request->getParam('page_name', '');
        $this->view->page_title = (!empty($pageName)) ? $pageName . " - Change Email" : "Change Email";

        $loggedUser = CheckAuth::getLoggedUser();

        $modelUser = new Model_User();
        $user = $modelUser->getById($loggedUser['user_id']);

//
// init action form
//
        $form = new Form_ChangeEmail(array('user' => $user));
        $this->view->form = $form;

//
// handling the insertion process
//
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $old_user_email = $modelUser->getByEmail($old_email);
                if ($loggedUser['user_id'] == $old_user_email['user_id']) {
                    if ($new_email == $confirm_new_email) {
                        $modelUser = new Model_User();

                        $data = array(
                            'temp_email' => $new_email
                        );
                        $modelUser->updateById($user['user_id'], $data);

//create code
                        $activation_code = sha1(uniqid());

//save code
                        $params = array(
                            'code' => $activation_code,
                            'created' => time(),
                            'type' => 'change_email',
                            'ip_address' => $_SERVER['REMOTE_ADDR'],
                            'user_id' => $user['user_id']
                        );
                        $modelCode = new Model_Code();
                        $modelCode->insert($params);

                        $activation_link = $this->router->assemble(array('code' => $activation_code, 'id' => $user['user_id']), 'changeEmailStep2');

                        $template_params = array(
                            '{username}' => ucwords($user['username']),
                            '{new_email}' => $new_email,
                            '{activation_link}' => '<a href="' . $activation_link . '">' . $activation_link . '</a>'
                        );

//send email
                        $sucess = EmailNotification::sendEmail(array('to' => $user['email1']), 'change_email', $template_params);

                        if ($sucess) {
                            $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Please Check Your Email address and Complete the Instruction"));
                        } else {
                            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Could Not Send change Email"));
                        }

                        $this->_redirect($this->router->assemble(array(), 'myAccount'));
                    } else {
                        $form->getElement('confirm_new_email')->setErrors(array('new email and confirm new email not match'));
                    }
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
                    $this->_redirect($this->router->assemble(array(), 'settingsUserList'));
                }
            }
        }
    }

    public function changeEmailStep2Action() {
//
// get request parameters
//
        $code = $this->request->getParam('code');
        $id = $this->request->getParam('id');

        $modelCode = new Model_Code();
        $modelUser = new Model_User();

        $userCode = $modelCode->getByCode($code);
        $user = $modelUser->getById($id);

        if (empty($user) || empty($userCode)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Can't Change Email!"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }

        $expirationTime = $userCode['created'] + (24 * 60 * 60);
        if (time() > $expirationTime) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Can't Change Email!, Your Change Email Link has been expired ."));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }

        if ($user['user_id'] != $userCode['user_id']) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Can't Change Email!"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }

        if ('change_email' != $userCode['type']) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Can't Change Email!"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }

        $data = array(
            'email1' => $user['temp_email'],
            'temp_email' => ''
        );
        $sucess = $modelUser->updateById($user['user_id'], $data);

//delete code
        $modelCode->deleteById($userCode['id']);

        if ($sucess) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Your Email is Changed successfully"));
        } else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Could Not Change Your Email"));
        }


        if (Zend_Auth::getInstance()->hasIdentity()) {
            $this->_redirect($this->router->assemble(array(), 'myAccount'));
        } else {
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }
    }

    public function forgetPasswordAction() {

//
// get request parameters
//
        $email = $this->request->getParam('email');

//
// init action form
//
        $form = new Form_ForgetPassword();

//
// handling the insertion process
//
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $modelUser = new Model_User();
                $user = $modelUser->getByEmail($email);
                if ($user) {

                    $iduser = $user['user_id'];
                    $userCompaniesModel = new Model_UserCompanies();
                    $company = $userCompaniesModel->getCompaniesByUserId($iduser);

                    $company_id = (isset($company) ? $company['company_id'] : 1 );
                    // $account            =  $accountModel->getByCompanyId($company['company_id'] );
//create code
                    $activation_code = sha1(uniqid());

//save code          
                    $params = array(
                        'code' => $activation_code,
                        'created' => time(),
                        'type' => 'forgot_password',
                        'ip_address' => $_SERVER['REMOTE_ADDR'],
                        'user_id' => $user['user_id']
                    );
                    $modelCode = new Model_Code();
                    $modelCode->insert($params);

                    $activation_link = $this->router->assemble(array('code' => $activation_code, 'id' => $user['user_id']), 'forgetPasswordStep2');

                    $template_params = array(
                        '{username}' => ucwords($user['username']),
                        '{activation_link}' => '<a href="' . $activation_link . '">' . $activation_link . '</a>'
                    );

//send email
                    $sucess = EmailNotification::sendEmail(array('to' => $email), 'forget_password', $template_params, array(), $company_id);

                    if ($sucess) {
                        $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Please Check Your Email address and Complete the Instruction"));
                    } else {
                        $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Could Not Send Reset Passward Email"));
                    }

                    $this->_redirect($this->router->assemble(array(), 'Login'));
                }
            }
        }
        $this->view->form = $form;
    }

    public function forgetPasswordStep2Action() {
//
// get request parameters
//
        $code = $this->request->getParam('code');
        $id = $this->request->getParam('id');

        $modelCode = new Model_Code();
        $modelUser = new Model_User();

        $userCode = $modelCode->getByCode($code);
        $user = $modelUser->getById($id);

        if (empty($user) || empty($userCode)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Can't Reset Passward!"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }

        $expirationTime = $userCode['created'] + (24 * 60 * 60);
        if (time() > $expirationTime) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Can't Reset Passward!, Your Reset Passward Link has been expired ."));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }

        if ($user['user_id'] != $userCode['user_id']) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Can't Reset Passward!"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }

        if ('forgot_password' != $userCode['type']) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Can't Reset Passward!"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }

//
// init action form
//
        $form = new Form_ForgetPasswordStep2();

        $password = $this->request->getParam('password');
        $password_confirm = $this->request->getParam('password_confirm');

//
// handling the insertion process
//
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                if ($password == $password_confirm) {

                    $data = array(
                        'password' => sha1($password)
                    );
                    $modelUser->updateById($user['user_id'], $data);

//delete code
                    $modelCode->deleteById($userCode['id']);

                    $this->_redirect($this->router->assemble(array(), 'Login'));
                } else {
                    $form->getElement('password_confirm')->setErrors(array('password and confirm password not match'));
                }
            }
        }
        $this->view->form = $form;
    }

    public function changeAccountInfoAction() {

//
//chech if the user loged in or not
//
        CheckAuth::checkLoggedIn();

//
// get request parameters
//
        $username = $this->request->getParam('username');
        $display_name = $this->request->getParam('display_name');
        $first_name = $this->request->getParam('first_name');
        $last_name = $this->request->getParam('last_name');
        $email2 = $this->request->getParam('email2');
        $email3 = $this->request->getParam('email3');
        $mobile1 = $this->request->getParam('mobile1');
        $mobile2 = $this->request->getParam('mobile2');
        $mobile3 = $this->request->getParam('mobile3');
        $phone1 = $this->request->getParam('phone1');
        $phone2 = $this->request->getParam('phone2');
        $phone3 = $this->request->getParam('phone3');
        $fax = $this->request->getParam('fax');
        $emergencyPhone = $this->request->getParam('emergency_phone');
        $unitLotNumber = $this->request->getParam('unit_lot_number');
        $streetNumber = $this->request->getParam('street_number');
        $streetAddress = $this->request->getParam('street_address');
        $suburb = $this->request->getParam('suburb');
        $state = $this->request->getParam('state');
        $postcode = $this->request->getParam('postcode');
        $po_box = $this->request->getParam('po_box');
        $cityId = $this->request->getParam('city_id');
        $countryId = $this->request->getParam('country_id');
        $companyId = $this->request->getParam('company_id');

        if (get_config('remove_white_spacing')) {
            $mobile1 = preparer_number($mobile1);
            $mobile2 = preparer_number($mobile2);
            $mobile3 = preparer_number($mobile3);
            $phone1 = preparer_number($phone1);
            $phone2 = preparer_number($phone2);
            $phone3 = preparer_number($phone3);
        }

        $loggedUser = CheckAuth::getLoggedUser();
//
// validation
//
        $modelUser = new Model_User();
        $user = $modelUser->getById($loggedUser['user_id']);
        if (!$user) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
            return;
        }

//
//get user company
//
        $userCompaniesModel = new Model_UserCompanies();
        $userCompanies = $userCompaniesModel->getByUserId($user['user_id']);


//
// init action form
//
        $form = new Form_ChangeAccountInfo(array('user' => $user, 'country_id' => $countryId, 'company_id' => $userCompanies['company_id']));
        $this->view->form = $form;

//
// handling the updating process
//
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    //save user information
                    'username' => $username,
                    'display_name' => $display_name,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'city_id' => $cityId,
                    'email2' => $email2,
                    'email3' => $email3,
                    'mobile1' => $mobile1,
                    'mobile2' => $mobile2,
                    'mobile3' => $mobile3,
                    'phone1' => $phone1,
                    'phone2' => $phone2,
                    'phone3' => $phone3,
                    'fax' => $fax,
                    'emergency_phone' => $emergencyPhone,
                    'unit_lot_number' => $unitLotNumber,
                    'street_number' => $streetNumber,
                    'street_address' => $streetAddress,
                    'suburb' => $suburb,
                    'state' => $state,
                    'postcode' => $postcode,
                    'po_box' => $po_box
                );


                $success = $modelUser->updateById($user['user_id'], $data);


                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in User"));
                }


//$this->_redirect($this->router->assemble(array(), 'myAccount'));
                echo 1;
                exit;
            }
        }

        echo $this->view->render('my-account/change-account-info.phtml');
        exit;
    }

//
//change password
//
    public function changePasswordAction() {

        CheckAuth::checkLoggedIn();

        $loggedUser = CheckAuth::getLoggedUser();

        $pageName = $this->request->getParam('page_name', '');
        $this->view->page_title = (!empty($pageName)) ? $pageName . " - Change Password" : "Change Password";

        $modelUser = new Model_User();
        $user = $modelUser->getById($loggedUser['user_id']);
        if (!$user) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
            return;
        }

        $old_password = $this->request->getParam('old_password');
        $new_password = $this->request->getParam('new_password');
        $confirm_new_password = $this->request->getParam('confirm_new_password');

        $form = new Form_ChangePassword(array('user' => $user));
        $this->view->form = $form;

        if ($this->request->isPost()) {
            if ($form->isValid($this->request->getPost())) {

                $data = array(
                    'password' => sha1($new_password)
                );
                $modelUser->updateById($user['user_id'], $data);

                $this->_redirect($this->router->assemble(array(), 'Login'));
            }
        }
    }

    public function modfyUserInfoAction() {

//
//check login
//
        CheckAuth::checkLoggedIn();

        if (!CheckAuth::checkCredential(array('canModifyUserInfo'))) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }

        $loggedUser = CheckAuth::getLoggedUser();

        $userInfoObj = new Model_UserInfo();
        $userInfo = $userInfoObj->getByUserId($loggedUser['user_id']);

//
// get request parameters
//
        $firstName = $this->request->getParam('first_name');
        $lastName = $this->request->getParam('last_name');


//
// init action form
//
        if (!$userInfo) {
            $form = new Form_UserInfo(array('user_id' => $loggedUser['user_id']));
        } else {
            $form = new Form_UserInfo(array('user_id' => $loggedUser['user_id'], 'userInfo' => $userInfo));
        }

//
// handling the insertion process
//
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $userInfoObj = new Model_UserInfo();
                $data = array(
                    'first_name' => $firstName,
                    'last_name' => $lastName
                );

                if (!$userInfo) {
                    $data['user_id'] = $loggedUser['user_id'];
                    $success = $userInfoObj->insert($data);
                } else {
                    $success = $userInfoObj->updateById($userInfo['user_info_id'], $data);
                }

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in User Info"));
                }
//$this->_redirect($this->router->assemble(array(), 'settingsUserEmailList'));

                echo 1;
                exit;
            }
        }

        $this->view->form = $form;
//
// render views
//

        echo $this->view->render('my-account/user_info.phtml');
        exit;
    }

    public function modfyContractorInfoAction() {

//
//check login
//
        CheckAuth::checkLoggedIn();

        if ('contractor' != CheckAuth::getRoleName()) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }

        $loggedUser = CheckAuth::getLoggedUser();

        $contractorInfoObj = new Model_ContractorInfo();
        $contractorInfo = $contractorInfoObj->getByContractorId($loggedUser['user_id']);

        $modelUser = new Model_User();
        $user = $modelUser->getById($loggedUser['user_id']);

//
// get request parameters
//
        $business_name = $this->request->getParam('business_name');
        $acn = $this->request->getParam('acn');
        $abn = $this->request->getParam('abn');
        $tfn = $this->request->getParam('tfn');
        $gst = $this->request->getParam('gst');
        $gst_date_registered = $this->request->getParam('gst_date_registered');

		$db_format = 0;
		$dateTimeObj= get_settings_date_format();
		if($dateTimeObj)		 {
			$db_format = 1;
		}
//
// init action form
//
        if (!$contractorInfo) {
            $form = new Form_ContractorInfo(array('contractor_id' => $loggedUser['user_id'], 'user' => $user,));
        } else {
            $form = new Form_ContractorInfo(array('contractor_id' => $loggedUser['user_id'], 'contractorInfo' => $contractorInfo, 'user' => $user,));
        }

//
// handling the insertion process
//
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
				if($gst_date_registered && $db_format) 
					$gst_date_registered = dateFormat_zend_to_purePhp('d-m-Y', $gst_date_registered);
                $data = array(
                    'business_name' => $business_name,
                    'acn' => $acn,
                    'abn' => $abn,
                    'tfn' => $tfn,
                    'gst' => $gst,
                    'gst_date_registered' => strtotime($gst_date_registered),
                );



                if (!$contractorInfo) {
                    $data['contractor_id'] = $loggedUser['user_id'];
                    $success = $contractorInfoObj->insert($data);
                } else {
                    $success = $contractorInfoObj->updateById($contractorInfo['contractor_info_id'], $data);
                }

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Contractor Info"));
                }

                echo 1;
                exit;
            }
        }

        $this->view->form = $form;
//
// render views
//

        echo $this->view->render('my-account/contractor_info.phtml');
        exit;
    }

    public function editBankAction() {

        // get request parameters
        $loggedUser = CheckAuth::getLoggedUser();



        $bank_name = $this->request->getParam('bank_name');
        $account_name = $this->request->getParam('account_name');
        $account_number = $this->request->getParam('account_number');
        $bsb = $this->request->getParam('bsb');

        $modelContractorInfo = new Model_ContractorInfo();
        $contractorInfo = $modelContractorInfo->getByContractorId($loggedUser['user_id']);



        $form = new Form_BankInfo(array('contractorInfo' => $contractorInfo, 'contractor_id' => $loggedUser['user_id']));


        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data			  
                $data = array(
                    'bank_name' => $bank_name,
                    'account_name' => $account_name,
                    'account_number' => $account_number,
                    'bsb' => $bsb
                );




                $success = $modelContractorInfo->updateByContractorId($loggedUser['user_id'], $data);
                if ($success) {

                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Contractor Info"));
                }
                echo 1;
                exit;
            }
        }


        $this->view->form = $form;
        $this->view->id = $contractorInfo['contractor_info_id'];

        //
        // render views
        //
        echo $this->view->render('my-account/edit_bank.phtml');
        exit;
    }

    public function editInsuranceAction() {


        // check Auth for logged user
        // get request parameters
        $loggedUser = CheckAuth::getLoggedUser();

        $insurance_policy_number = $this->request->getParam('insurance_policy_number');
        $insurance_policy_start = $this->request->getParam('insurance_policy_start');
        $insurance_policy_expiry = $this->request->getParam('insurance_policy_expiry');
        $insurance_listed_services_covered = $this->request->getParam('insurance_listed_services_covered');
        $insurance_type = $this->request->getParam('insurance_type');
        // validation
        //
        $modelContractorInfo = new Model_ContractorInfo();
        $modelContractorInfoAttachment = new Model_ContractorInfoAttachment();
        $attachmentObj = new Model_Attachment();
        $contractorInfo = $modelContractorInfo->getByContractorId($loggedUser['user_id']);


        $form = new Form_InsuranceInfo(array('contractorInfo' => $contractorInfo));


        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data			  
                $data = array(
                    'insurance_policy_number' => $insurance_policy_number,
                    'insurance_policy_start' => strtotime($insurance_policy_start),
                    'insurance_policy_expiry' => strtotime($insurance_policy_expiry),
                    'insurance_listed_services_covered' => $insurance_listed_services_covered,
                    'insurance_type' => $insurance_type,
                );


                $deleted_ids = $this->request->getParam('deleted_ids', '');
                $successDeleted = 0;
                if (!empty($deleted_ids)) {
                    $deleted_ids = explode(",", $deleted_ids);
                    $modelAttachment = new Model_Attachment();
                    foreach ($deleted_ids as $deleted_id) {
                        $successDeleted = $modelAttachment->updateById($deleted_id, array('is_deleted' => '1'));
                    }
                }
                $success = $modelContractorInfo->updateByContractorId($loggedUser['user_id'], $data);

                $updateAttachment = 0;
                $upload = new Zend_File_Transfer_Adapter_Http();
                $files = $upload->getFileInfo();
                $Attachments = $modelContractorInfo->getAllAttachmentByTypeAndId($contractorInfo['contractor_info_id'], 'insurance');
                $counter = count($Attachments);
                foreach ($files as $file => $fileInfo) {
                    if ($upload->isUploaded($file)) {
                        if ($upload->receive($file)) {
                            $counter = $counter + 1;
                            $info = $upload->getFileInfo($file);
                            $source = $info[$file]['tmp_name'];
                            $imageInfo = pathinfo($source);
                            $ext = $imageInfo['extension'];
                            $size = $info[$file]['size'];
                            $type = $info[$file]['type'];
                            $dir = get_config('attachment') . '/';
                            $subdir = date('Y/m/d/');
                            $fullDir = $dir . $subdir;
                            if (!is_dir($fullDir)) {
                                mkdir($fullDir, 0777, true);
                            }
                            $loggedUser = CheckAuth::getLoggedUser();
                            $data = array(
                                'created_by' => $loggedUser['user_id']
                                , 'created' => time()
                                , 'size' => $size
                                , 'type' => $type
                            );



                            $Attachid = $attachmentObj->insert($data);
                            $contractorInfoAttachmentId = $modelContractorInfoAttachment->insert(array('contractor_info_id' => $contractorInfo['contractor_info_id'], 'attachment_id' => $Attachid, 'type' => 'insurance'));
                            $fileName = $contractorInfo['contractor_info_id'] . '_' . $counter . '_' . 'insurance' . '.' . $ext;
                            $image_saved = copy($source, $fullDir . $fileName);
                            $typeParts = explode("/", $type);
                            if ($typeParts[0] == 'image') {
                                $thumbName = $contractorInfo['contractor_info_id'] . '_' . $counter . '_' . 'insurance_thumbnail' . '.' . $ext;
                                ImageMagick::create_thumbnail($source, $fullDir . $thumbName, 78, 64);
                            } else {
                                $thumbName = $contractorInfo['contractor_info_id'] . '_' . $counter . '_' . 'insurance' . '.jpg';
                                ImageMagick::convertFile($source . '[0]', $fullDir . $thumbName);
                            }



                            $Updatedata = array(
                                'path' => $fullDir . $fileName,
                                'file_name' => $fileName,
                                'thumbnail_file' => $fullDir . $thumbName
                            );


                            $updateAttachment = $attachmentObj->updateById($Attachid, $Updatedata);
                            if ($image_saved) {
                                if (file_exists($source)) {
                                    unlink($source);
                                }
                            }
                        }
                    }
                }
                if ($success || $updateAttachment || $successDeleted) {

                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Contractor Info"));
                }
                //$this->view->successMessage = 'Updated Successfully';
                echo 1;
                exit;
                //$this->_redirect($this->router->assemble(array(), 'settingsContractorInfoList'));
                //return;
            }
        }

        // init action form
        //contractor_id
        //
		//
        // handling the updating process
        //

		
	   $this->view->form = $form;
        $this->view->id = $contractorInfo['contractor_info_id'];
        //
        // render views
        //
        echo $this->view->render('my-account/edit_insurance.phtml');
        exit;
    }

    public function getAttacmentsAction() {


        $contractor_info_id = $this->request->getParam('contractor_info_id', 0);
        $type = $this->request->getParam('type', 0);
        $modelContractorInfo = new Model_ContractorInfo();
        $Attachemnts = $modelContractorInfo->getAllAttachmentByTypeAndId($contractor_info_id, $type);

        header('Content-type: text/json');
        header('Content-type: application/json');
        echo json_encode($Attachemnts);
        exit;
    }

    public function editLicenceAction() {

        // get request parameters

        $loggedUser = CheckAuth::getLoggedUser();

        $drivers_licence_number = $this->request->getParam('drivers_licence_number');
        $drivers_licence_expiry = $this->request->getParam('drivers_licence_expiry');
        $driver_licence_type = $this->request->getParam('driver_licence_type');
        // validation

        $modelContractorInfo = new Model_ContractorInfo();
        $modelContractorInfoAttachment = new Model_ContractorInfoAttachment();
        $attachmentObj = new Model_Attachment();
        $contractorInfo = $modelContractorInfo->getByContractorId($loggedUser['user_id']);




        $form = new Form_LicenceInfo(array('contractorInfo' => $contractorInfo));

        // init action form
        //contractor_id

        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data	
                $data = array(
                    'drivers_licence_number' => $drivers_licence_number,
                    'driver_licence_type' => $driver_licence_type,
                    'drivers_licence_expiry' => strtotime($drivers_licence_expiry),
                );


                $deleted_ids = $this->request->getParam('deleted_ids', '');
                $successDeleted = 0;
                if (!empty($deleted_ids)) {
                    $deleted_ids = explode(",", $deleted_ids);
                    $modelAttachment = new Model_Attachment();
                    foreach ($deleted_ids as $deleted_id) {
                        $successDeleted = $modelAttachment->updateById($deleted_id, array('is_deleted' => '1'));
                    }
                }
                $success = $modelContractorInfo->updateByContractorId($loggedUser['user_id'], $data);

                $updateAttachment = 0;
                $upload = new Zend_File_Transfer_Adapter_Http();
                $files = $upload->getFileInfo();
                $Attachments = $modelContractorInfo->getAllAttachmentByTypeAndId($contractorInfo['contractor_info_id'], 'licence');
                $counter = count($Attachments);
                foreach ($files as $file => $fileInfo) {
                    if ($upload->isUploaded($file)) {
                        if ($upload->receive($file)) {
                            $counter = $counter + 1;
                            $info = $upload->getFileInfo($file);
                            $source = $info[$file]['tmp_name'];
                            $imageInfo = pathinfo($source);
                            $ext = $imageInfo['extension'];
                            $size = $info[$file]['size'];
                            $type = $info[$file]['type'];
                            $dir = get_config('attachment') . '/';
                            $subdir = date('Y/m/d/');
                            $fullDir = $dir . $subdir;
                            if (!is_dir($fullDir)) {
                                mkdir($fullDir, 0777, true);
                            }
                            $loggedUser = CheckAuth::getLoggedUser();
                            $data = array(
                                'created_by' => $loggedUser['user_id']
                                , 'created' => time()
                                , 'size' => $size
                                , 'type' => $type
                            );



                            $Attachid = $attachmentObj->insert($data);
                            $contractorInfoAttachmentId = $modelContractorInfoAttachment->insert(array('contractor_info_id' => $contractorInfo['contractor_info_id'], 'attachment_id' => $Attachid, 'type' => 'licence'));
                            $fileName = $contractorInfo['contractor_info_id'] . '_' . $counter . '_' . 'licence' . '.' . $ext;
                            $image_saved = copy($source, $fullDir . $fileName);

                            $typeParts = explode("/", $type);
                            if ($typeParts[0] == 'image') {
                                $thumbName = $contractorInfo['contractor_info_id'] . '_' . $counter . '_' . 'licence_thumbnail' . '.' . $ext;
                                ImageMagick::create_thumbnail($source, $fullDir . $thumbName, 78, 64);
                            } else {
                                $thumbName = $contractorInfo['contractor_info_id'] . '_' . $counter . '_' . 'licence' . '.jpg';
                                ImageMagick::convertFile($source . '[0]', $fullDir . $thumbName);
                            }

                            $Updatedata = array(
                                'path' => $fullDir . $fileName,
                                'file_name' => $fileName,
                                'thumbnail_file' => $fullDir . $thumbName
                            );




                            $updateAttachment = $attachmentObj->updateById($Attachid, $Updatedata);
                            if ($image_saved) {
                                if (file_exists($source)) {
                                    unlink($source);
                                }
                            }
                        }
                    }
                }

                if ($success || $updateAttachment || $successDeleted) {

                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Contractor Info"));
                }
                //$this->view->successMessage = 'Updated Successfully';
                echo 1;
                exit;
                //$this->_redirect($this->router->assemble(array(), 'settingsContractorInfoList'));
                //return;
            }
        }


        // handling the updating process



        $this->view->form = $form;
        $this->view->id = $contractorInfo['contractor_info_id'];

        //
        // render views
        //
        echo $this->view->render('my-account/edit_licence.phtml');
        exit;
    }

    // old modifyContractorInfo
    /* public function modfyContractorInfoAction() {

      //
      //check login
      //
      CheckAuth::checkLoggedIn();

      if ('contractor' != CheckAuth::getRoleName()) {
      $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
      $this->_redirect($this->router->assemble(array(), 'Login'));
      }

      $loggedUser = CheckAuth::getLoggedUser();

      $contractorInfoObj = new Model_ContractorInfo();
      $contractorInfo = $contractorInfoObj->getByContractorId($loggedUser['user_id']);

      //
      // get request parameters
      //
      $business_name = $this->request->getParam('business_name');
      $acn = $this->request->getParam('acn');
      $abn = $this->request->getParam('abn');
      $tfn = $this->request->getParam('tfn');
      $gst = $this->request->getParam('gst');
      $gst_date_registered = $this->request->getParam('gst_date_registered');
      $insurance_policy_number = $this->request->getParam('insurance_policy_number');
      $insurance_policy_start = $this->request->getParam('insurance_policy_start');
      $insurance_policy_expiry = $this->request->getParam('insurance_policy_expiry');
      $insurance_listed_services_covered = $this->request->getParam('insurance_listed_services_covered');
      $drivers_licence_number = $this->request->getParam('drivers_licence_number');
      $drivers_licence_expiry = $this->request->getParam('drivers_licence_expiry');


      //
      // init action form
      //
      if (!$contractorInfo) {
      $form = new Form_ContractorInfo(array('contractor_id' => $loggedUser['user_id']));
      } else {
      $form = new Form_ContractorInfo(array('contractor_id' => $loggedUser['user_id'], 'contractorInfo' => $contractorInfo));
      }

      //
      // handling the insertion process
      //
      if ($this->request->isPost()) { // check if POST request method
      if ($form->isValid($this->request->getPost())) { // validate form data
      $data = array(
      'business_name' => $business_name,
      'acn' => $acn,
      'abn' => $abn,
      'tfn' => $tfn,
      'gst' => $gst,
      'gst_date_registered' => strtotime($gst_date_registered),
      'insurance_policy_number' => $insurance_policy_number,
      'insurance_policy_start' => strtotime($insurance_policy_start),
      'insurance_policy_expiry' => strtotime($insurance_policy_expiry),
      'insurance_listed_services_covered' => $insurance_listed_services_covered,
      'drivers_licence_number' => $drivers_licence_number,
      'drivers_licence_expiry' => strtotime($drivers_licence_expiry),
      );

      if (!$contractorInfo) {
      $data['contractor_id'] = $loggedUser['user_id'];
      $success = $contractorInfoObj->insert($data);
      } else {
      $success = $contractorInfoObj->updateById($contractorInfo['contractor_info_id'], $data);
      }

      if ($success) {
      $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
      } else {
      $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Contractor Info"));
      }

      echo 1;
      exit;
      }
      }

      $this->view->form = $form;
      //
      // render views
      //

      echo $this->view->render('my-account/contractor_info.phtml');
      exit;
      } */

    public function pageNotFoundAction() {
        
    }

    public function cancelBookingAction() {

//get request parameters
        $bookingId = $this->request->getParam('booking_id');
        $cancelHashcode = $this->request->getParam('cancel_hashcode');
        $statusId = $this->request->getParam('status_id');

        $modelBooking = new Model_Booking();
        $booking = $modelBooking->getById($bookingId);

        $modelBookingStatus = new Model_BookingStatus();
        $bookingStatus = $modelBookingStatus->getById($statusId);

        if (!$booking) {
            $this->_redirect($this->router->assemble(array(), 'pageNotFound'));
        }
        if (!$bookingStatus) {
            $this->_redirect($this->router->assemble(array(), 'pageNotFound'));
        }
        if ($booking['status_id'] != $statusId) {
            $this->_redirect($this->router->assemble(array(), 'pageNotFound'));
        }
        if ($cancelHashcode != $booking['cancel_hashcode']) {
            $this->_redirect($this->router->assemble(array(), 'pageNotFound'));
        }

//CANCELL QUOTED
        $quoted = $modelBookingStatus->getByStatusName('QUOTED');
        if ($quoted['booking_status_id'] == $statusId) {
            $modelBookingEstimate = new Model_BookingEstimate();
            $estimate = $modelBookingEstimate->getByBookingId($bookingId);
            if ($estimate) {
                if ($estimate['estimate_type'] == 'draft') {
                    $modelBookingEstimate->updateById($estimate['id'], array('is_deleted' => 1));
                }
            }
        }

//CANCELL ON HOLD
        $onHold = $modelBookingStatus->getByStatusName('ON HOLD');
        if ($onHold['booking_status_id'] == $statusId) {
            $cancelled = $modelBookingStatus->getByStatusName('CANCELLED');
            $modelBooking->updateById($bookingId, array('status_id' => $cancelled['booking_status_id']));
        }

//CANCELL TENTATIVE
        $tentative = $modelBookingStatus->getByStatusName('TENTATIVE');
        if ($tentative['booking_status_id'] == $statusId) {
            $cancelled = $modelBookingStatus->getByStatusName('CANCELLED');
            $modelBooking->updateById($bookingId, array('status_id' => $cancelled['booking_status_id']));
        }

        $modelCompanies = new Model_Companies();
        $company = $modelCompanies->getById($booking['company_id']);
        $this->view->company = $company;

        $modelBooking->updateById($bookingId, array('cancel_hashcode' => sha1(uniqid())));
    }

    public function feedbackBookingAction() {

//get request parameters
        $bookingId = $this->request->getParam('booking_id');
        $feedbackHashcode = $this->request->getParam('feedback_hashcode');
        $statusId = $this->request->getParam('status_id');
        $feedback = $this->request->getParam('feedback', '');

        $modelBooking = new Model_Booking();
        $booking = $modelBooking->getById($bookingId);

        $modelCustomerFeedback = new Model_CustomerFeedback();
        $customerFeedback = $modelCustomerFeedback->getBybookingAndstatusId($bookingId, $statusId);

        if (!$booking) {
            $this->_redirect($this->router->assemble(array(), 'pageNotFound'));
        }

        if ($customerFeedback) {
            if ($feedbackHashcode != $customerFeedback['hashcode']) {
                $this->_redirect($this->router->assemble(array(), 'pageNotFound'));
            }
        } else {
            $this->_redirect($this->router->assemble(array(), 'pageNotFound'));
        }

        $modelCompanies = new Model_Companies();
        $company = $modelCompanies->getById($booking['company_id']);
        $this->view->company = $company;
        $this->view->feedback_hashcode = $feedbackHashcode;
        $this->view->booking_id = $bookingId;
        $this->view->status_id = $statusId;

        if ($this->request->isPost()) {
            $feedback_hashcode = sha1(uniqid());
            $this->view->suceess = true;
            $modelCustomerFeedback->updateById($customerFeedback['id'], array('feedback' => $feedback, 'hashcode' => $feedback_hashcode));
        }
    }

    public function confirmAction() {
        $booking_id = $this->request->getParam('id');
        $booking_model = new Model_Booking();
        $customer_model = new Model_Customer();
        $booking = $booking_model->getById($booking_id);
        $booking_num = $booking['booking_num'];
        $customer_id = $booking['customer_id'];
        $customer = $customer_model->getById($customer_id);
        $default_page = "/booking/view/$booking_id";
        $authRole_model = new Model_AuthRole();
        $user_model = new Model_User();
        $auth = Zend_Auth::getInstance();
        $authStorge = $auth->getStorage();
        $role = $authRole_model->getRoleIdByName('Customer');
        $customer['role_id'] = $role;
        $user_id = $customer['customer_id'];

        $customer['user_id'] = $user_id;

        $customer['username'] = $customer['first_name'];
        $customer['default_page'] = $default_page;

        $customer_obj = (object) $customer;

        $authStorge->write($customer_obj);
// echo $estimate_id;

        CheckAuth::redirectCustomer($default_page);
    }

    public function customerBookingLoginAction() {
        $booking_id = $this->request->getParam('booking_id');
        $type = $this->request->getParam('type');
        $booking_model = new Model_Booking();
        $customer_model = new Model_Customer();
        $booking = $booking_model->getById($booking_id);
        $booking_num = $booking['booking_num'];
        $customer_id = $booking['customer_id'];
        $customer = $customer_model->getById($customer_id);
        $default_page = "/booking/view/$booking_id?type=" . $type;
        $authRole_model = new Model_AuthRole();
        $user_model = new Model_User();
        $auth = Zend_Auth::getInstance();
        $authStorge = $auth->getStorage();
        $role = $authRole_model->getRoleIdByName('Customer');
        $customer['role_id'] = $role;
        $user_id = $customer['customer_id'];
        $customer['user_id'] = $user_id;
        $customer['username'] = $customer['first_name'];
        $customer['default_page'] = $default_page;
        $customer_obj = (object) $customer;
        $authStorge->write($customer_obj);
        CheckAuth::redirectCustomer($default_page);
    }

    /*

      //    if(isset($booking_id)){
      //       $this->view->confirm='confirm';
      //       $this->view->form=$form;
      //   }
      //    else{



      if ($this->request->isPost()) {

      print_r($booking);
      $booking_num=$booking['booking_num'];
      $form=new Settings_Form_Confirm(array('booking_id'=>$booking_id));

      $email=$this->request->getParam('email');
      $booking_number=$this->request->getParam('booking_num');
      echo $booking_number;
      echo $booking_num;
      if($booking_num==$booking_number){
      if($email==$customer['email1']||$email==$customer['email2']||$email==$customer['email3']){
      if(($booking['booking_end']-date('Y-m-d h:i:s')/60*60*24))<=30){
      $model2=new Model_EmailLog();
      if ($customer['email1']&& filter_var($customer['email1'], FILTER_VALIDATE_EMAIL)) {
      $to[] = $customer['email1'];
      }
      if ($customer['email2']&& filter_var($customer['email2'], FILTER_VALIDATE_EMAIL)) {
      $to[] = $customer['email2'];
      }
      if ($customer['email3']&& filter_var($customer['email3'], FILTER_VALIDATE_EMAIL)) {
      $to[] = $customer['email3'];
      }
      $to=implode($to,',');
      $subject='Your booking reminder with Tile Cleaners Pty Ltd - ('.$booking['booking_start'].')';
      $filters=array('subject'=>$subject,'to'=>$to,'type'=>'booking');
      $bookings=$model2->getAll($filters);
      $model2->fills($bookings, array('booking'));
      foreach($bookings as $row){
      if($row['booking']['booking_num']==$booking_num){
      echo $row['body'];
      break;

      }
      }



      }
      else
      $this->view->error=">30";
      }
      }
      else {
      $this->view->confirm='confirm';
      $this->view->authError='Try Again';
      $this->view->form=$form;
      }


      //}
      // }
      } */

//    public function addPhotoAction() {
//        $inquiry_id = $this->request->getParam('id', 0);
//        $model_inquiry = new Model_Inquiry();
//        $inquiry_data = $model_inquiry->getById($inquiry_id);
//        $model_originalInquiry = new Model_OriginalInquiry();
//        $OriginalInquiryData = $model_originalInquiry->getById($inquiry_data['original_inquiry_id']);
//        $model_tradingName = new Model_TradingName();
//        $tradingNames = $model_tradingName->getTradingNameByWebsite($OriginalInquiryData['website']);
//        $tradingName = str_replace(' ', '-', $tradingNames['trading_name']);
//        $default_page = $this->router->assemble(array('id' => $inquiry_id, 'trading_name' => $tradingName), 'inquiryaddPhotoTradingName'); // "inquiry/view/$inquiry_id/$tradingName";
//
//        $customer_model = new Model_Customer();
//        $customer_id = $inquiry_data['customer_id'];
//        $customer = $customer_model->getById($customer_id);
//        $authRole_model = new Model_AuthRole();
//        $auth = Zend_Auth::getInstance();
//        $authStorge = $auth->getStorage();
//        $role = $authRole_model->getRoleIdByName('Customer');
//        $customer['role_id'] = $role;
//        $user_id = $customer['customer_id'];
//        $customer['user_id'] = $user_id;
//        $customer['username'] = $customer['first_name'];
//        $customer['default_page'] = $default_page;
//
//        $customer_obj = (object) $customer;
//        $authStorge->write($customer_obj);
//        CheckAuth::redirectCustomer($default_page);
//    }
    public function addPhotoAction() {
        $model_booking = new Model_Booking();
        $id = $this->request->getParam('id', 0);
        $type = $this->request->getParam('type');

        $model_tradingName = new Model_TradingName();
        if (!empty($type)) {
            if ($type == 'inquiry') {
                $model_inquiry = new Model_Inquiry();
                $inquiry_data = $model_inquiry->getById($id);
                $model_originalInquiry = new Model_OriginalInquiry();
                $OriginalInquiryData = $model_originalInquiry->getById($inquiry_data['original_inquiry_id']);
                $customer_id = $inquiry_data['customer_id'];
                $tradingNames = $model_tradingName->getTradingNameByWebsite($OriginalInquiryData['website']);
                $tradingName = str_replace(' ', '-', $tradingNames['trading_name']);
                $default_page = $this->router->assemble(array('id' => $id, 'trading_name' => $tradingName), 'inquiryAddPhotoTradingName'); // "inquiry/view/$inquiry_id/$tradingName";
            } else if ($type == 'booking') {

                $booking_data = $model_booking->getById($id);
                $customer_id = $booking_data['customer_id'];
                $tradingNames = $model_tradingName->getById($booking_data['trading_name_id']);
                $tradingName = str_replace(' ', '-', $tradingNames['trading_name']);
                $default_page = $this->router->assemble(array('id' => $id, 'trading_name' => $tradingName), 'bookingAddPhotoTradingName');
            } else if ($type == 'estimate') {

                $model_estimate = new Model_BookingEstimate();
                $estimate_data = $model_estimate->getById($id);
                $booking_estimate_data = $model_booking->getById($estimate_data['booking_id']);
                $customer_id = $booking_estimate_data['customer_id'];
                $tradingNames = $model_tradingName->getById($booking_estimate_data['trading_name_id']);
                $tradingName = str_replace(' ', '-', $tradingNames['trading_name']);
                $default_page = $this->router->assemble(array('id' => $id, 'trading_name' => $tradingName), 'estimateAddPhotoTradingName');
            }
            $customer_model = new Model_Customer();
            $customer = $customer_model->getById($customer_id);
            $authRole_model = new Model_AuthRole();
            $auth = Zend_Auth::getInstance();
            $authStorge = $auth->getStorage();
            $role = $authRole_model->getRoleIdByName('Customer');
            $customer['role_id'] = $role;
            $user_id = $customer['customer_id'];
            //$customer['customer_id'] = $user_id;
            $customer['user_id'] = $user_id;
            $customer['username'] = $customer['first_name'];
            $customer['default_page'] = $default_page;
            $customer_obj = (object) $customer;
            $authStorge->write($customer_obj);
            CheckAuth::redirectCustomer($default_page);
        }
    }

    public function getItemCountsAction() {

        $type = $this->request->getParam('type');
        $type = trim($type);


        $data = array();
        if (strpos($type, 'estimate') !== false) {
            $model_BookingEstimate = new Model_BookingEstimate();
            //$countAllBookingEstimate = $model_BookingEstimate->getCount(array('estimate_type' => 'draft'));
            //$countmyBookingEstimate = $model_BookingEstimate->getCount(array('to_follow' => true, 'to_follow_date' => 'today'));
            $countToFollow = $model_BookingEstimate->getCount(array('to_follow' => true, 'estimate_type' => 'draft'));
            $data = array('countToFollow' => array('count' => $countToFollow[0]['count'], 'color' => 'primary'));
        } else if ($type == 'inquiry') {
            $modelInquiry = new Model_Inquiry();
            $countToFollowInquiry = $modelInquiry->getCount(array('status' => 'inquiry', 'to_follow' => true, 'to_follow_date' => 'past'));
            $countInquiry = $modelInquiry->getCountInquiry(false);
            $countmyInquiry = $modelInquiry->getCountInquiry(true);
            $data = array('countToFollowInquiry' => array('count' => $countToFollowInquiry[0]['count'], 'color' => 'primary')
                , 'countInquiries' => array('count' => $countmyInquiry . '/' . $countInquiry, 'color' => 'primary')
                , 'countmyInquiry' => array('count' => $countmyInquiry, 'color' => 'primary')
                , 'countInquiry' => array('count' => $countInquiry, 'color' => 'primary'));
        } else if ($type == 'invoices') {

            $model_BookingInvoice = new Model_BookingInvoice();
            $countOfUnpaid = $model_BookingInvoice->getCount(array('invoice_type' => 'unpaid'));
            $countOfOpen = $model_BookingInvoice->getCount(array('invoice_type' => 'open'));
            $countOfOverdue = $model_BookingInvoice->getCount(array('invoice_type' => 'overdue'));
            $modelRefund = new Model_Refund();
            $newRefundsFilters = array('is_approved' => 'no');
            $newRefunds = $modelRefund->getAll($newRefundsFilters);
            $modelPayment = new Model_Payment();
            $countUnapprovedPayments = $modelPayment->getCountUnapprovedPayments();
            $countDuplicates = $model_BookingInvoice->getDuplicatedInvoiceNumbers(1);
            $countUnkownPayments = $modelPayment->getNotMatchedPayments(array('is_count' => 1, 'is_approved' => 'no'));

            $data = array('countOfUnpaid' => array('count' => $countOfUnpaid, 'color' => 'primary')
                , 'countOfOpen' => array('count' => $countOfOpen, 'color' => 'primary')
                , 'countOfOverdue' => array('count' => $countOfOverdue, 'color' => 'danger'),
                'countNewRefunds' => array('count' => count($newRefunds), 'color' => 'primary')
                , 'countUnapprovedPayments' => array('count' => $countUnapprovedPayments, 'color' => 'primary')
                , 'countDuplicates' => array('count' => $countDuplicates, 'color' => 'primary')
                , 'countUnkownPayments' => array('count' => $countUnkownPayments, 'color' => 'primary')
            );
        } else if ($type == 'booking') {
            $modelBooking = new Model_Booking();
            $modelBookingStatus = new Model_BookingStatus();
            $countAwaitingupdateBooking = $modelBooking->getCountAwaitingupdateBooking();
            $filters = array('convert_status' => 'booking', 'to_follow' => true);
            $countToFollowBooking = $modelBooking->getCountToFollowBooking($filters);
            $countAwaitingacceptBooking = $modelBooking->getCountAwaitingAcceptBooking();
            $countUnapprovedBooking = $modelBooking->getCountUnapprovedBooking();
            $newRequestsFilters = array(
                'acceptance' => 'notAcceptNorReject',
                'withoutEstimateStatus' => 1,
                'booking_not_started_yet' => true
            );
            $toConfirmFilters = array(
                'to_confirm' => true,
                'next_working_day' => true,
                'withoutEstimateStatus' => 1
            );
            $countNewRequests = $modelBooking->getAll($newRequestsFilters, null, $pager, 0, 0, 0, 1);
            $countToConfirm = $modelBooking->getAll($toConfirmFilters, null, $pager, 0, 0, 0, 1);
            $data = array(
                'countAwaitingupdateBooking' => array('count' => $countAwaitingupdateBooking, 'color' => 'danger')
                , 'countAwaitingacceptBooking' => array('count' => $countAwaitingacceptBooking, 'color' => 'primary')
                , 'countToFollowBooking' => array('count' => $countToFollowBooking['count'], 'color' => 'warning')
                , 'countUnapprovedBooking' => array('count' => $countUnapprovedBooking, 'color' => 'primary')
                , 'countNewRequests' => array('count' => $countNewRequests[0]['count'], 'color' => 'warning')
                , 'countToConfirm' => array('count' => $countToConfirm[0]['count'], 'color' => 'danger'));
        } else if ($type == 'booking1') {
            $modelBooking = new Model_Booking();
            $modelBookingStatus = new Model_BookingStatus();

            $today = getTimePeriodByName('today');
            $tomorrow = getTimePeriodByName('tomorrow');
            $cancelledStatus = $modelBookingStatus->getByStatusName('CANCELLED');
            $onHoldStatus = $modelBookingStatus->getByStatusName('ON HOLD');
            $primaryTodayFilters = array(
                'booking_start_between' => $today['start'],
                'booking_end_between' => $today['end'],
                'withoutEstimateStatus' => 1,
                'exclude_more_one_status' => $cancelledStatus['booking_status_id'] . ',' . $onHoldStatus['booking_status_id']
            );
            $multipleTodayFilter = array(
                'multiple_start_between' => $today['start'],
                'multiple_end_between' => $today['end'],
                'withoutEstimateStatus' => 1,
                'exclude_more_one_status' => $cancelledStatus['booking_status_id'] . ',' . $onHoldStatus['booking_status_id']
            );

            $sameMultipleTodayFilter = array(
                'multiple_start_between' => $today['start'],
                'multiple_end_between' => $today['end'],
                'same_dates' => 1,
                'withoutEstimateStatus' => 1,
                'exclude_more_one_status' => $cancelledStatus['booking_status_id'] . ',' . $onHoldStatus['booking_status_id']
            );

            $primaryTomorrowFilters = array(
                'booking_start_between' => $tomorrow['start'],
                'booking_end_between' => $tomorrow['end'],
                'withoutEstimateStatus' => 1,
                'exclude_more_one_status' => $cancelledStatus['booking_status_id'] . ',' . $onHoldStatus['booking_status_id']
            );
            $multipleTomorrowFilter = array(
                'multiple_start_between' => $tomorrow['start'],
                'multiple_end_between' => $tomorrow['end'],
                'withoutEstimateStatus' => 1,
                'exclude_more_one_status' => $cancelledStatus['booking_status_id'] . ',' . $onHoldStatus['booking_status_id']
            );

            $sameMultipleTomorrowFilter = array(
                'multiple_start_between' => $tomorrow['start'],
                'multiple_end_between' => $tomorrow['end'],
                'same_dates' => 1,
                'withoutEstimateStatus' => 1,
                'exclude_more_one_status' => $cancelledStatus['booking_status_id'] . ',' . $onHoldStatus['booking_status_id']
            );


            $futureFilters = array(
                'booking_not_started_yet' => true,
                'multiple_not_started_yet' => true,
                'withoutEstimateStatus' => 1,
                'exclude_more_one_status' => $cancelledStatus['booking_status_id'] . ',' . $onHoldStatus['booking_status_id']
            );
            $pager = null;
            $countFuture = $modelBooking->getAll($futureFilters, null, $pager, 0, 0, 0, 1);
            $inProcessStatus = $modelBookingStatus->getByStatusName('IN PROGRESS');
            $toDo = $modelBookingStatus->getByStatusName('TO DO');
            $toVisit = $modelBookingStatus->getByStatusName('TO VISIT');
            $notAssignedFilters = array(
                'withoutEstimateStatus' => 1,
                'more_one_status' => $inProcessStatus['booking_status_id'] . ',' . $toDo['booking_status_id'] . ',' . $toVisit['booking_status_id'],
                'notAssigned' => 1
            );

            $notAssignedCount = $modelBooking->getAll($notAssignedFilters, null, $pager, 0, 0, 0, 1);
            $countRejectBooking = $modelBooking->getCountRejectBooking();
            $countNotAssigned = $notAssignedCount[0]['count'] + $countRejectBooking;
            $countRejectBooking = $countRejectBooking;



            $AllNotMultipleBookingToday = $modelBooking->getAll($primaryTodayFilters, null, $pager, 0, 0, 0, 1);
            $AllMultipleBookingToday = $modelBooking->getAll($multipleTodayFilter, null, $pager, 0, 0, 0, 1);
            $sameMultipleBookingToday = $modelBooking->getAll($sameMultipleTodayFilter, null, $pager, 0, 0, 0, 1);
            $AllNotMultipleBookingTomorrow = $modelBooking->getAll($primaryTomorrowFilters, null, $pager, 0, 0, 0, 1);
            $AllMultipleBookingTomorrow = $modelBooking->getAll($multipleTomorrowFilter, null, $pager, 0, 0, 0, 1);
            $sameMultipleBookingTomorrow = $modelBooking->getAll($sameMultipleTomorrowFilter, null, $pager, 0, 0, 0, 1);
            if ((!isset($sameMultipleBookingTomorrow[0]['count']))) {
                $sameData = 0;
            } else {
                $sameData = $sameMultipleBookingTomorrow[0]['count'];
            }

            if ((!isset($sameMultipleBookingToday[0]['count']))) {
                $sameDataToday = 0;
            } else {
                $sameDataToday = $sameMultipleBookingToday[0]['count'];
            }

            $countToday = abs(($AllNotMultipleBookingToday[0]['count'] + $AllMultipleBookingToday[0]['count']) - $sameDataToday);
            $countTomorrow = abs(($AllNotMultipleBookingTomorrow[0]['count'] + $AllMultipleBookingTomorrow[0]['count']) - $sameData);
            $countFuture = $countFuture[0]['count'];
            $countInProcessBooking = $modelBooking->getCountInProcessBooking();
            $data = array(
                'countToday' => array('count' => $countToday, 'color' => 'primary')
                , 'countTomorrow' => array('count' => $countTomorrow, 'color' => 'primary')
                , 'countFuture' => array('count' => $countFuture, 'color' => 'primary')
                , 'countNotAssigned' => array('count' => $countNotAssigned, 'color' => 'danger')
                , 'countRejectBooking' => array('count' => $countRejectBooking, 'color' => 'danger')
                , 'countInProcessBooking' => array('count' => $countInProcessBooking, 'color' => 'warning'));
        } else if ($type == 'complaint') {
            $modelComplaint = new Model_Complaint();
            $countUapprovedCompalints = $modelComplaint->countUapprovedCompalints();
            $countOpenComplaint = $modelComplaint->getCount(array('complaintStatus' => 'open', 'is_approved' => '1'));
            $data = array('countUapprovedCompalints' => array('count' => $countUapprovedCompalints, 'color' => 'primary')
                , 'countOpenComplaint' => array('count' => $countOpenComplaint, 'color' => 'danger'));
        }

        echo json_encode($data);
        exit;
    }

    public function completeProfileVideoAction() {
        
    }

    public function completeProfileAction() {
        $loggedUser = CheckAuth::getLoggedUser();
        $modelUser = new Model_User();
        $user = $modelUser->getById($loggedUser['user_id']);

        $contractorInfoObj = new Model_ContractorInfo();
        $contractorInfo = $contractorInfoObj->getByContractorId($user['user_id']);

        //if ($contractorInfo) {
        //get Insurance
        $contractorInsuranceObj = new Model_ContractorInsurance();
        $contractorInsurances = $contractorInsuranceObj->getAll(array('contractor_info_id' => $contractorInfo['contractor_info_id']));

        //get contractorOwner
        $contractorOwnerObj = new Model_ContractorOwner();
        $contractorOwner = $contractorOwnerObj->getAll(array('contractor_info_id' => $contractorInfo['contractor_info_id']));

        //get Vehicle
        $contractorVehicleObj = new Model_ContractorVehicle();
        $contractorVehicle = $contractorVehicleObj->getByContractorInfoId($contractorInfo['contractor_info_id']);

        //get DeclarationOfChemicals
        $declarationOfChemicalsObj = new Model_DeclarationOfChemicals();
        $declarationOfChemicals = $declarationOfChemicalsObj->getByContractorInfoId($contractorInfo['contractor_info_id']);

        //get DeclarationOfEquipment
        $declarationOfEquipmentObj = new Model_DeclarationOfEquipment();
        $declarationOfEquipment = $declarationOfEquipmentObj->getByContractorInfoId($contractorInfo['contractor_info_id']);

        if ($user['username'] && $user['mobile1'] && $user['street_number'] && $user['street_address'] && $user['postcode'] && $user['country_name'] && $user['state']) {
            $personalInfoIcon = 'completed';
        } else {
            $personalInfoIcon = 'not completed';
        }
        $this->view->personalInfoIcon = $personalInfoIcon;

        if ($contractorInfo['business_name'] && ($contractorInfo['acn'] || $contractorInfo['abn'] || $contractorInfo['tfn'])) {
            $businessInfoIcon = 'completed';
        } else {
            $businessInfoIcon = 'not completed';
        }
        $this->view->businessInfoIcon = $businessInfoIcon;

        if ($contractorInfo['account_name'] && $contractorInfo['account_number'] && $contractorInfo['bsb']) {
            $bankAccountIcon = 'completed';
        } else {
            $bankAccountIcon = 'not completed';
        }
        $this->view->bankAccountIcon = $bankAccountIcon;

        if ($contractorInfo['drivers_licence_number']) {
            $driversLicenceIcon = 'completed';
        } else {
            $driversLicenceIcon = 'not completed';
        }
        $this->view->driversLicenceIcon = $driversLicenceIcon;

        if (!empty($contractorInsurances)) {
            $insuranceInfoIcon = 'completed';
        } else {
            $insuranceInfoIcon = 'not completed';
        }
        $this->view->insuranceInfoIcon = $insuranceInfoIcon;

        if (!empty($contractorOwner)) {
            $contractorOwnerIcon = 'completed';
        } else {
            $contractorOwnerIcon = 'not completed';
        }
        $this->view->contractorOwnerIcon = $contractorOwnerIcon;

        if (!empty($contractorVehicle)) {
            $contractorVehicleIcon = 'completed';
        } else {
            $contractorVehicleIcon = 'not completed';
        }
        $this->view->contractorVehicleIcon = $contractorVehicleIcon;

        if (!empty($declarationOfChemicals)) {
            $declarationOfChemicalsIcon = 'completed';
        } else {
            $declarationOfChemicalsIcon = 'not completed';
        }
        $this->view->declarationOfChemicalsIcon = $declarationOfChemicalsIcon;

        if (!empty($declarationOfEquipment)) {
            $declarationOfEquipmentIcon = 'completed';
        } else {
            $declarationOfEquipmentIcon = 'not completed';
        }
        $this->view->declarationOfEquipmentIcon = $declarationOfEquipmentIcon;
    }

    public function sliderAction() {

        $processID = $this->request->getParam('processID');
        $currentImageID = $this->request->getParam('imageID');
        $type = $this->request->getParam('type');
        $modelImage = new Model_Image();
        $photos = $modelImage->getAll($processID, $type);
        $this->view->photos = $photos;
        $this->view->currentImageID = $currentImageID;
        $this->view->processID = $processID;
        $this->view->type = $type;
        echo $this->view->render('index/slider.phtml');
        exit();
    }

    /* public function sliderTabsAction() {

      $processID = $this->request->getParam('processID');
      $currentImageID = $this->request->getParam('imageID');
      $type = $this->request->getParam('type');
      $modelImage = new Model_Image();
      $photo = $modelImage->getById($currentImageID, null);
      $this->view->photo = $photo;
      $this->view->currentImageID = $currentImageID;
      $this->view->processID = $processID;
      $this->view->type = $type;
      echo $this->view->render('index/slider_tabs.phtml');
      exit();
      } */

    public function sliderTabsAction() {

        $processID = $this->request->getParam('processID');
        $currentImageID = $this->request->getParam('imageID');
        $groupID = $this->request->getParam('group_id');
        $type = $this->request->getParam('type');
        $modelImage = new Model_Image();
        if ($groupID == 0) {
            $photo = $modelImage->getById($currentImageID, null);
        } else {
            $photo = $modelImage->getByGroupId($groupID, null, null);
            //$result = array();
            if ($type == 'booking') {
                $modelBookingDiscussion = new Model_BookingDiscussion();
                $bookingDiscussions = $modelBookingDiscussion->getByBookingId($processID, 'DESC');
                foreach ($bookingDiscussions as $key => $row) {
                    
                }
                foreach ($row as $key => $row1) {
                    if ($key == 'user_message') {
                        $result[0] = $row1;
                    }
                    if ($key == 'created') {
                        $result[1] = $row1;
                    }
                    if ($key == 'user_name') {
                        $result[2] = $row1;
                    }
                }
            }
            if ($type == 'inquiry') {
                $modelInquiryDiscussion = new Model_InquiryDiscussion();
                $inquiryDiscussions = $modelInquiryDiscussion->getByInquiryId($processID, 'DESC');
                foreach ($inquiryDiscussions as $key => $row) {
                    
                }
                foreach ($row as $key => $row1) {
                    if ($key == 'user_message') {
                        $result[0] = $row1;
                    }
                    if ($key == 'created') {
                        $result[1] = time_ago($row1);
                    }
                    if ($key == 'user_name') {
                        $result[2] = $row1;
                    }
                }
            }
            if ($type == 'estimate') {
                $modelEstimateDiscussion = new Model_EstimateDiscussion();
                $estimateDiscussions = $modelEstimateDiscussion->getByEstimateId($processID, 'DESC');
                foreach ($estimateDiscussions as $key => $row) {
                    
                }
                foreach ($row as $key => $row1) {
                    if ($key == 'user_message') {
                        $result[0] = $row1;
                    }
                    if ($key == 'created') {
                        $result[1] = time_ago($row1);
                    }
                    if ($key == 'user_name') {
                        $result[2] = $row1;
                    }
                }
            }
            if ($type == 'invoice') {
                $modelInvoiceDiscussion = new Model_InvoiceDiscussion();
                $invoiceDiscussions = $modelInvoiceDiscussion->getByInvoiceId($processID, 'DESC');
                foreach ($invoiceDiscussions as $key => $row) {
                    
                }
                foreach ($row as $key => $row1) {
                    if ($key == 'user_message') {
                        $result[0] = $row1;
                    }
                    if ($key == 'created') {
                        $result[1] = time_ago($row1);
                    }
                    if ($key == 'user_name') {
                        $result[2] = $row1;
                    }
                }
            }
            if ($type == 'complaint') {
                $modelComplaintDiscussion = new Model_ComplaintDiscussion();
                $complaintDiscussions = $modelComplaintDiscussion->getByComplaintId($processID, 'DESC');
                foreach ($complaintDiscussions as $key => $row) {
                    
                }
                foreach ($row as $key => $row1) {
                    if ($key == 'user_message') {
                        $result[0] = $row1;
                    }
                    if ($key == 'created') {
                        $result[1] = time_ago($row1);
                    }
                    if ($key == 'user_name') {
                        $result[2] = $row1;
                    }
                }
            }
        }
        if (!empty($result)) {
            //print_r($result);
            $this->view->resultArray = $result;
            //exit;
        }

        $this->view->photo = $photo;
        $this->view->currentImageID = $currentImageID;
        $this->view->processID = $processID;
        $this->view->type = $type;
        echo $this->view->render('index/slider_tabs.phtml');
        exit();
    }

    public function sliderImageAction() {

        $idVal = $this->request->getParam('idVal');

        $modelContractorDiscussionMongo = new Model_ContractorDiscussionMongo();

        $object = $modelContractorDiscussionMongo->getById($idVal);
        foreach ($object as $key => $row) {
            
        }

        foreach ($row['original_images'] as $key => $row1) {
            $photo[] = $row1;
        }

        $data = array(
            'user_message' => $row['user_message'],
            'user_name' => $row['user_name'],
            'created' => time_ago($row['created'])
        );
        $this->view->data = $data;
        $this->view->photos = $photo;




        echo $this->view->render('index/slider-image.phtml');
        exit();
    }

}
