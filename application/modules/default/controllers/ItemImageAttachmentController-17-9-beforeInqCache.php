<?php

class ItemImageAttachmentController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        /* Initialize action controller here */
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
    }

    public function indexAction() {
	
	  $itemId =  $this->request->getParam('itemid',0);
	  $type =  $this->request->getParam('type',0);
	  $orderBy = $this->request->getParam('sort', 'iu.created');
      $sortingMethod = $this->request->getParam('method', 'desc');
      $currentPage = $this->request->getParam('page', 1);
      $image_type = $this->request->getParam('image_type', '');
	  $filter = array('image_type'=>$image_type);
	  
	  $pager = new Model_Pager();
      $pager->perPage = 30;
      $pager->currentPage = $currentPage;
      $pager->url = $_SERVER['REQUEST_URI'];
	  
	  $modelImageTypes = new Model_ImageTypes();	   
	  $imagetypes = $modelImageTypes->getAllTypesAsArray();
	  
	  
	  
	    $select = new Zend_Form_Element_Select('image_type');
        $select->setDecorators(array('ViewHelper'));
        $select->setValue(isset($image_type) ? $image_type : '');
        $select->setAttrib('onchange', "fltr('" . $this->router->assemble(array('type'=>$type,'itemid'=>$itemId), 'ItemImageAttachmentList') . "?image_type=','image_type')");
        $select->setAttrib('class', "form-control");
        $select->addMultiOption('all', 'All');
        $select->addMultiOptions($imagetypes);
        $this->view->allImagesTypes = $select;
	  

	  //$ItemImage = new Model_ItemImage();
      //$this->view->data = $modelImageAttachment->getAll($itemId , $type, "{$orderBy} {$sortingMethod}", $pager,$filter);
	  
	  $this->view->itemid = $itemId;
	  $this->view->type = $type;
	  $this->view->image_type = $image_type;
	  $this->view->currentPage = $currentPage;
      $this->view->perPage = $pager->perPage;
      $this->view->pageLinks = $pager->getPager();
      $this->view->sortingMethod = $sortingMethod;
      $this->view->orderBy = $orderBy;
	
    }
	
	
	
	
	public function getImagesByScrollAction(){
	
	/*$modelImageAttachment = new Model_Image();
	$maxGroup =  max($modelImageAttachment->getMaxGroup());
	$newGroup = $maxGroup['group_id'] + 1;
	echo $newGroup;
	exit;*/
	
	  $itemid = $this->request->getParam('itemid');
	  $type = $this->request->getParam('type');
	  $pager = null;
	  $group_number = $this->request->getParam('group_no');
	  $modelImageAttachment = new Model_Image();
	  $modelServices = new Model_Services();
	  $modelBookingDiscussion = new Model_BookingDiscussion();
	  $modelUser = new Model_User();
	  $modelAuthRole = new Model_AuthRole();
	  $modelContractorInfo = new Model_ContractorInfo();
	  $image_type = $this->request->getParam('image_type', '');
	  $filter = array('image_type'=>$image_type);
      $Images =  $modelImageAttachment->getAll($itemid,$type,'iu.created desc',$pager, $filter ,0, 30, ($group_number + 1));
	  $total = count($modelImageAttachment->getAll($itemid,$type,'iu.created desc'));
	  $AllImages = array();
	  if($group_number){
	    $index = $total - 30;
	  }else{
	    $index = $total;
	  }
	  
	  foreach ($Images as $key=>&$image) {
	    
	    $AllImages[$key] = $image;
		if($itemid){
		 $editUrl = $this->router->assemble(array('id' => $image['image_id'],'type'=>$type,'itemid'=>$itemid), 'ItemImageAttachmentEdit');
		 $deleteUrl = $this->router->assemble(array('id' => $image['image_id'],'type'=>$type,'itemId'=>$itemid), 'ItemImageAttachmentDelete');
		}else{
		 $editUrl = $this->router->assemble(array('id' => $image['image_id'],'type'=>$type), 'ServiceImageAttachmentEdit');
		 $deleteUrl = $this->router->assemble(array('id' => $image['image_id'],'type'=>$type), 'ServiceImageAttachmentDelete');
		}
		
		
		
		$AllImages[$key]['edit_url'] = $editUrl;
		$AllImages[$key]['total'] = $total;
		$AllImages[$key]['num'] = $index--;

		$original_url = $this->getRequest()->getBaseUrl().'/uploads/image_attachment/'.$image['original_path'];
		list($width, $height) = getimagesize($original_url);
		$AllImages[$key]['width'] = $width;
		$AllImages[$key]['height'] = $height;

      


		//$AllImages[$key]['width'] = $editUrl;
		$AllImages[$key]['delete_url'] = $deleteUrl;
		
		// get service name of image
		$service_name = '';
		if($image['service_id']){
		 $service = $modelServices->getById($image['service_id']);
		 $service_name = $service['service_name'];
		}
	    $AllImages[$key]['service_name'] = $service_name;
		
		// get image discussions 
		
		if($type == 'booking'){
		  $imageDiscussions = $modelBookingDiscussion->getByImageId($image['image_id'], 'DESC'); 
		}elseif($type == 'inquiry'){
		  $modelInquiryDiscussion = new Model_InquiryDiscussion();
		  $imageDiscussions = $modelInquiryDiscussion->getByImageId($image['image_id'], 'DESC'); 
		}elseif($type == 'estimate'){
		  $modelEstimateDiscussion = new Model_EstimateDiscussion();
		  $imageDiscussions = $modelEstimateDiscussion->getByImageId($image['image_id'], 'DESC'); 
		
		}
		 
		 foreach ($imageDiscussions as &$imageDiscussion) {
            $imageDiscussion['user'] = $modelUser->getById($imageDiscussion['user_id']);
            $imageDiscussion['discussion_date'] = getDateFormating( $imageDiscussion['created']);
            $imageDiscussion['user_message'] = nl2br(htmlentities($imageDiscussion['user_message'], ENT_NOQUOTES, 'UTF-8'));
            if ($imageDiscussion['user']['role_name'] == 'contractor') {
                $contractorInfo = $modelContractorInfo->getByContractorId($imageDiscussion['user']['user_id']);
                $imageDiscussion['user']['username'] = ucwords($imageDiscussion['user']['username']) . ' - ' . ucwords($contractorInfo['business_name']);
            } else {
                $imageDiscussion['user']['username'] = ucwords($imageDiscussion['user']['username']);
            }
        }
		 $AllImages[$key]['discussions'] = $imageDiscussions;
	  }
	  
	  
	  echo json_encode($AllImages);
	  exit;
	}
	
	
	public function imageAttachAction(){
	
      
	   $itemId =  $this->request->getParam('itemid',0);
	   $type =  $this->request->getParam('type',0);	
	   $discussion = $this->request->getParam('discussion','');
	   $before_after_job = $this->request->getParam('before_after_job','');
       $service_id = $this->request->getParam('service_id', 0);
       $service = $this->request->getParam('serviceid');
	   $modelBookingDiscussion = new Model_BookingDiscussion();
	   $modelImageAttachment = new Model_Image();
	   $modelItemImageDiscussion = new Model_ItemImageDiscussion();
	   

	   
	   if(isset($service)){
	    $form = new Form_ImageAttachmentItem(array('itemId' => $itemId,'type'=>$type,'service'=>$service));
	   }else{
	    $form = new Form_ImageAttachmentItem(array('itemId' => $itemId,'type'=>$type));
	   }
        $this->view->form = $form;
	   
	   if ($this->request->isPost()) {
	    if ($form->isValid($this->getRequest()->getParams())) {
	    $upload = new Zend_File_Transfer_Adapter_Http();
        $files  = $upload->getFileInfo();
		
		$countFiles = count($files);
		$image_group = 0;
		if($countFiles > 1){
			 $maxGroup =  max($modelItemImageDiscussion->getMaxGroup());
	         $image_group = $maxGroup['group_id'] + 1;			 
			}
		$counter = 0;
		foreach($files as $file => $fileInfo) {
			  if ($upload->isUploaded($file)) {
                if ($upload->receive($file)) {
				
				 $info = $upload->getFileInfo($file);
                 $source  = $info[$file]['tmp_name'];
                 $imageInfo = pathinfo($source);
                 $ext = $imageInfo['extension'];
				 $dir = get_config('image_attachment') . '/';
                 $subdir = date('Y/m/d/');
                 $fullDir = $dir . $subdir;
                  if (!is_dir($fullDir)) {
                        mkdir($fullDir, 0777, true);
                    }
					
				 $ip = $_SERVER['REMOTE_ADDR'];
                 $loggedUser = CheckAuth::getLoggedUser();
				 
				
                 			 
				 $data = array(
                      'user_ip'=> $ip
				     ,'created_by'=>$loggedUser['user_id'] 
					 ,'image_types_id'=>$before_after_job
					 ,'group_id'=>$image_group
                 );
				

				 
                 $id = $modelImageAttachment->insert($data);
				 
				$image_id = $id;
				if($countFiles > 1){
				  $image_id = 0;
				}
				 
				$success = 0;
                 if ($discussion) {				 
				 if($type == 'booking'){
				   $data = array(
                    'booking_id' => $itemId,
                    'user_id' => $loggedUser['user_id'],
                    'user_message' => $discussion,
                    'created' => time(),
                 );				 
				   $success = $modelBookingDiscussion->insert($data);
				 }elseif($type == 'estimate'){
				   $data = array(
                    'estimate_id' => $itemId,
                    'user_id' => $loggedUser['user_id'],
                    'user_message' => $discussion,
                    'created' => time(),
                 );		
                   $modelEstimateDiscussion = new Model_EstimateDiscussion();				 
				   $success = $modelEstimateDiscussion->insert($data);
				 
				 }elseif($type == 'inquiry'){
				   $data = array(
                    'inquiry_id' => $itemId,
                    'user_id' => $loggedUser['user_id'],
                    'user_message' => $discussion,
                    'created' => time(),
                 );		
                   $modelInquiryDiscussion = new Model_InquiryDiscussion();				 
				   $success = $modelInquiryDiscussion->insert($data);				 
				 }
				 
				 
				 $modelItemImageDiscussion =new Model_ItemImageDiscussion();
				 
				 $dataDiscssion = array(
				 'image_id'=>$image_id,
				 'group_id' =>$image_group,
				 'item_id'=>$success,
				 'type'=>$type
				 );
				 $modelItemImageDiscussion->insert($dataDiscssion);
				 
				 
				 
                } 
				 
				
				
				
				 
				if(!($service_id == 0)){
				 $type_new = $type;
				  if($success){
					$type_new = $type_new.'_discussion' ;
					//$itemId = $itemId.'_'.$success;
				  }
				  $type_new = $type_new.'_service';				    
					 if($type == 'service'){
					   $itemId = $service_id;
					}					 
				}else{
				  $type_new = $type;
				  if($success){
					$type_new = $type_new.'_discussion' ;
					//$itemId = $itemId.'_'.$success;
				  }
				}
					
			    $Itemdata = array(
				'item_id'=>$itemId,
				'service_id'=>$service_id,
				'discussion_id'=>$success,
				'image_id' =>$id,
				'type'=>$type_new
				);
				 
				 
				$modelItemImage = new Model_ItemImage();
                $Item_image_id = $modelItemImage->insert($Itemdata);
				
				$original_path = "original_{$id}.{$ext}";
                $large_path = "large_{$id}.{$ext}";
                $small_path = "small_{$id}.{$ext}";
                $thumbnail_path = "thumbnail_{$id}.{$ext}";
                $compressed_path = "compressed_{$id}.jpg";

                $data = array(
                        'original_path' => $subdir . $original_path,
                        'large_path' => $subdir . $large_path,
                        'small_path' => $subdir . $small_path,
                        'thumbnail_path' => $subdir . $thumbnail_path,
                        'compressed_path' => $subdir . $compressed_path
                    );

                $modelImageAttachment->updateById($id, $data);
                $counter+=1;
                    //save image to database and filesystem here
                $image_saved = copy($source, $fullDir . $original_path);	
                $compressed_saved = copy($source, $fullDir . $compressed_path);				
                ImageMagick::scale_image($source, $fullDir . $large_path, 510);
                ImageMagick::scale_image($source, $fullDir . $small_path, 250);
                ImageMagick::create_thumbnail($source, $fullDir . $thumbnail_path, 78, 64);
				ImageMagick::convert($source, $fullDir . $compressed_path);
                ImageMagick::compress_image($source, $fullDir . $compressed_path);
				
				if ($image_saved) {
                        if (file_exists($source)) {
                            unlink($source);
                        }               
                    }
				
				 
				}else{
                    $this->view->message = '<div class="errors">Errors Receiving File.</div>';
                    echo $this->view->render('item-image-attachment/image-attach.phtml');
                    exit;               
			    }
				
			     
				}
				
			}
			
			 if($type == 'booking'){
			 	//echo $id;
				MobileNotification::notify($itemId , 'booking has new photos' , 0 , $counter);
			}
			
			//D.A 30/08/2015 Remove Booking Photos Cache				
			require_once 'Zend/Cache.php';
			$company_id = CheckAuth::getCompanySession();
			$bookingPhotosCacheID= $itemId.'_bookingPhotos';
			$bookingViewDir=get_config('cache').'/'.'bookingsView'.'/'.$company_id;								
			$frontEndOption= array('lifetime'=> 24 * 3600,
			'automatic_serialization'=> true);
			$backendOptions = array('cache_dir'=>$bookingViewDir );
			$Cache = Zend_Cache::factory('Core','File',$frontEndOption,$backendOptions);			
			$Cache->remove($bookingPhotosCacheID);
			
            $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
			echo 1;
            exit;			
		 }
	    }
	   
	   $this->view->itemid = $itemId;
	   $this->view->type = $type;
	   echo $this->view->render('item-image-attachment/image-attach.phtml');
       exit;
	   
	  
	}
	
	public function imageUploadAction(){
	
	   //CheckAuth::checkPermission(array('settingsImageAttachmentAdd'));

        //
        // get request parameters
        //
        $description = $this->request->getParam('description','');
        $service_id = $this->request->getParam('service_id', 0);
		$itemId =  $this->request->getParam('itemid',0);
	    $type =  $this->request->getParam('type',0);
        $company_id = CheckAuth::getCompanySession();
        $this->view->type= $type;
        //
        // init action form
        //
        //

		$form = new Form_ImageAttachmentItem(array('itemId' => $itemId,'type'=>$type));
        $this->view->form = $form;
		
		if ($this->request->isPost()) {
		 if ($form->isValid($this->request->getPost())) {
		    $upload = new Zend_File_Transfer_Adapter_Http();
            $files  = $upload->getFileInfo();
			$countFiles = count($files);
			$imageIds = array();
			foreach($files as $file => $fileInfo) {
			  if ($upload->isUploaded($file)) {
                if ($upload->receive($file)) {
				 $info = $upload->getFileInfo($file);
                 $source  = $info[$file]['tmp_name'];
                 $imageInfo = pathinfo($source);
                 $ext = $imageInfo['extension'];
				 $dir = get_config('image_attachment') . '/';
                 $subdir = date('Y/m/d/');
                 $fullDir = $dir . $subdir;
                  if (!is_dir($fullDir)) {
                        mkdir($fullDir, 0777, true);
                    }					
				$ip = $_SERVER['REMOTE_ADDR'];
                $loggedUser = CheckAuth::getLoggedUser();				 
				$data = array(
                     'description' => $description
				     ,'user_ip'=> $ip
				     ,'created_by'=>$countFiles
                );
				
				$modelImageAttachment = new Model_Image();
                $id = $modelImageAttachment->insert($data);
				array_push($imageIds,$id);
								
				if(!($service_id == 0)){
				  $type_new = $type.'_service';
					 if($type_new == 'service_service'){
					   $itemId = $service_id;
					}					 
				}else{
				  $type_new = $type;
				}
					
			    $Itemdata = array(
				'item_id'=>$itemId,
				'service_id'=>$service_id,
				'image_id' =>$id,
				'type'=>$type_new
				);
					
				
				$modelItemImage = new Model_ItemImage();
                $Item_image_id = $modelItemImage->insert($Itemdata);
				
				$original_path = "original_{$id}.{$ext}";
                $large_path = "large_{$id}.{$ext}";
                $small_path = "small_{$id}.{$ext}";
                $thumbnail_path = "thumbnail_{$id}.{$ext}";

                $data = array(
                        'original_path' => $subdir . $original_path,
                        'large_path' => $subdir . $large_path,
                        'small_path' => $subdir . $small_path,
                        'thumbnail_path' => $subdir . $thumbnail_path
                    );

                $modelImageAttachment->updateById($id, $data);

                    //save image to database and filesystem here
                $image_saved = copy($source, $fullDir . $original_path);
                ImageMagick::scale_image($source, $fullDir . $large_path, 510);
                ImageMagick::scale_image($source, $fullDir . $small_path, 250);
                ImageMagick::create_thumbnail($source, $fullDir . $thumbnail_path, 78, 64);
				
				if ($image_saved) {
                        if (file_exists($source)) {
                            unlink($source);
                        }               
                    }
				
				 
				}else{
                    $this->view->message = '<div class="errors">Errors Receiving File.</div>';
                    echo $this->view->render('item-image-attachment/image-upload.phtml');
                    exit;               
			    }
			 }	
			}
			
			if($countFiles > 1){
			 $maxGroup =  max($modelImageAttachment->getMaxGroup());
			 $newGroup = $maxGroup + 1;
			 
			 foreach($imageIds as $imageId){
			    $modelImageAttachment->updateById($imageId,array('group_id' => $newGroup));
			 }
			 
			}
			
			$this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
			echo 1;
            exit;
		 
		 }
		
		}
		
		
		
		echo $this->view->render('item-image-attachment/image-upload.phtml');
        exit;
	
	}
	
	
	public function deleteAction(){
	   //
        $id = $this->request->getParam('id', 0);
        $type = $this->request->getParam('type', 0);
        $itemId = $this->request->getParam('itemId', 0);
        $deleteComments = $this->request->getParam('deleteComments', 0);
		
        $ids = $this->request->getParam('ids', array());
		
		if ($id) {
            $ids[] = $id;
        }

		$modelImageAttachment = new Model_Image();
        $ModelItemImage = new Model_ItemImage();
        
		
		foreach ($ids as $id) {
            $imageAttachment = $modelImageAttachment->getById($id,$type);
            if ($imageAttachment) {

               if (file_exists(get_config('image_attachment') . '/' . $imageAttachment['original_path'])) {
                    unlink(get_config('image_attachment') . '/' . $imageAttachment['original_path']);
                }
                if (file_exists(get_config('image_attachment') . '/' . $imageAttachment['large_path'])) {
                    unlink(get_config('image_attachment') . '/' . $imageAttachment['large_path']);
                }
                if (file_exists(get_config('image_attachment') . '/' . $imageAttachment['small_path'])) {
                    unlink(get_config('image_attachment') . '/' . $imageAttachment['small_path']);
                }
                if (file_exists(get_config('image_attachment') . '/' . $imageAttachment['thumbnail_path'])) {
                    unlink(get_config('image_attachment') . '/' . $imageAttachment['thumbnail_path']);
                }

                $modelImageAttachment->deleteById($id);
                $ModelItemImage->deleteById($id);
				if($deleteComments == 'true'){
				if($type == 'booking'){
				 $ModelBookingDiscussion = new Model_BookingDiscussion();
				 $imageDiscussions = $ModelBookingDiscussion->getByImageId($id);	
                 foreach ($imageDiscussions as &$imageDiscussion) {
				   $ModelBookingDiscussion->deleteById($imageDiscussion['discussion_id']);	
				 }
				}elseif($type == 'inquiry'){
				 $ModelInquiryDiscussion = new Model_InquiryDiscussion();
				 $imageDiscussions = $ModelInquiryDiscussion->getByImageId($id);	
                 foreach ($imageDiscussions as &$imageDiscussion) {
				   $ModelInquiryDiscussion->deleteById($imageDiscussion['discussion_id']);	
				 }
				
				}elseif($type == 'estimate'){
				 $ModelEstimateDiscussion = new Model_EstimateDiscussion();
				 $imageDiscussions = $ModelEstimateDiscussion->getByImageId($id);	
                 foreach ($imageDiscussions as &$imageDiscussion) {
				   $ModelEstimateDiscussion->deleteById($imageDiscussion['discussion_id']);	
				 }
				}
                 				 
				}
				
            }
		}
		
		//D.A 30/08/2015 Remove Booking Photos Cache				
		require_once 'Zend/Cache.php';
		$company_id = CheckAuth::getCompanySession();
		$bookingPhotosCacheID= $itemId.'_bookingPhotos';
		$bookingViewDir=get_config('cache').'/'.'bookingsView'.'/'.$company_id;								
		$frontEndOption= array('lifetime'=> 24 * 3600,
		'automatic_serialization'=> true);
		$backendOptions = array('cache_dir'=>$bookingViewDir );
		$Cache = Zend_Cache::factory('Core','File',$frontEndOption,$backendOptions);			
		$Cache->remove($bookingPhotosCacheID);
		
		$this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
		exit;
		//$this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
		
		/*if($itemId){
		$this->_redirect($this->router->assemble(array('type'=>$type,'itemid'=>$itemId), 'ItemImageAttachmentList'));
		}else{
		$this->_redirect($this->router->assemble(array('type'=>$type), 'ServiceImageAttachmentList'));
		}*/
	 
	}
	
	
	/*public function imageEditAction(){
	 
	
	 
	 $id = $this->request->getParam('id');
     $itemid = $this->request->getParam('itemid',0);
     $description = $this->request->getParam('description');
     $type = $this->request->getParam('type');
     $service_id = $this->request->getParam('service_id', 0);
     $change_image = $this->request->getParam('change_image', 0);
	 
	 
	
	 
	 $modelImageAttachment = new Model_Image();
     $imageAttachment = $modelImageAttachment->getById($id,$type);
	 
	
		if (!$imageAttachment) {
			if($itemid){
            $this->_redirect($this->router->assemble(array('type'=>$type,'itemid'=>$itemid), 'ItemImageAttachmentList'));
			
			}else{
            $this->_redirect($this->router->assemble(array('type'=>$type), 'ServiceImageAttachmentList'));
			}
            return;
        }
		
	  if (file_exists(get_config('image_attachment') . '/' . $imageAttachment['thumbnail_path'])) {
            $this->view->image = $imageAttachment['thumbnail_path'];
        }	
		
	   $form = new Form_ImageAttachmentItem(array('mode' => 'update', 'imageAttachment' => $imageAttachment,'itemId' => $itemid,'type'=>$type));
       $this->view->form = $form; 	
	
	if ($this->request->isPost()) {
        if ($form->isValid($this->request->getPost())) {
		if ($change_image) {
			$upload = new Zend_File_Transfer_Adapter_Http();
            $files  = $upload->getFileInfo();
            foreach($files as $file => $fileInfo) {
               if ($upload->isUploaded($file)) {
                if ($upload->receive($file)) {
                    $info = $upload->getFileInfo($file);
                    $source  = $info[$file]['tmp_name'];
                    $imageInfo = pathinfo($source);
                    $ext = $imageInfo['extension'];
                    $dir = get_config('image_attachment') . '/';
                    $subdir = date('Y/m/d/');
                    $fullDir = $dir . $subdir;
                    if (!is_dir($fullDir)) {
                        mkdir($fullDir, 0777, true);
                    }					
					$ip = $_SERVER['REMOTE_ADDR'];
                    $data = array(
                        'description' => $description
						,'user_ip'=> $ip
                    );

                    $modelImageAttachment = new Model_Image();
                    $modelImageAttachment->updateById($id,$data);
					
					if(!($service_id == 0) || !($service_id == null)){
				      $type_new = $type.'_service';
					if($type_new == 'service_service'){
					   $itemid = $service_id;
					}					 
				}else{
				  $service_id = 0;
				  $type_new = $type;
				}
					
			    $Itemdata = array(
				'item_id'=>$itemid,
				'service_id'=>$service_id,
				'image_id' =>$id,
				'type'=>$type_new
				);
					
					
					
					$modelItemImage = new Model_ItemImage();
                    $Item_image_id = $modelItemImage->updateById($id,$Itemdata);

                    $original_path = "original_{$id}.{$ext}";
                    $large_path = "large_{$id}.{$ext}";
                    $small_path = "small_{$id}.{$ext}";
                    $thumbnail_path = "thumbnail_{$id}.{$ext}";

                    $data = array(
                        'original_path' => $subdir . $original_path,
                        'large_path' => $subdir . $large_path,
                        'small_path' => $subdir . $small_path,
                        'thumbnail_path' => $subdir . $thumbnail_path
                    );

                    $modelImageAttachment->updateById($id, $data);

                    //save image to database and filesystem here
                    $image_saved = copy($source, $fullDir . $original_path);
                    ImageMagick::scale_image($source, $fullDir . $large_path, 510);
                    ImageMagick::scale_image($source, $fullDir . $small_path, 250);
                    ImageMagick::create_thumbnail($source, $fullDir . $thumbnail_path, 78, 64);
                    
                    if ($image_saved) {
                        if (file_exists($source)) {
                            unlink($source);
                        } 
                    }
            }
         
     }
}

  $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
  echo 1;
  exit;


  
}else {

        $ip = $_SERVER['REMOTE_ADDR'];
        $data = array(
            'description' => $description
			,'user_ip'=> $ip
         );

        $modelImageAttachment = new Model_Image();
        $modelImageAttachment->updateById($id,$data);
		if(!($service_id == 0) || !($service_id == null)){
				  $type_new = $type.'_service';
					 if($type_new == 'service_service'){
					   $itemid = $service_id;
					}					 
				}else{
				  $service_id = 0;
				  $type_new = $type;
				}
					
			    $Itemdata = array(
				'item_id'=>$itemid,
				'service_id'=>$service_id,
				'image_id' =>$id,
				'type'=>$type_new
				);
				
				
		$modelItemImage = new Model_ItemImage();
       $Item_image_id = $modelItemImage->updateById($id,$Itemdata);

        $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));

        echo 1;
         exit;
    }

     
            }
			
	    
		
        }
		
		$this->view->type = $type;		
		echo $this->view->render('item-image-attachment/image-upload.phtml');
        exit;		
	}*/
	
	
	
	public function imageEditAction(){
	 
	 $id = $this->request->getParam('id');
     $itemid = $this->request->getParam('itemid',0);
     $description = '';
     $type = $this->request->getParam('type');
     $service_id = $this->request->getParam('service_id', 0);
 
	 $modelImageAttachment = new Model_Image();
     $imageAttachment = $modelImageAttachment->getById($id,$type);
	 
	
		if (!$imageAttachment) {
			if($itemid){
            $this->_redirect($this->router->assemble(array('type'=>$type,'itemid'=>$itemid), 'ItemImageAttachmentList'));
			
			}else{
            $this->_redirect($this->router->assemble(array('type'=>$type), 'ServiceImageAttachmentList'));
			}
            return;
        }

		
	   $form = new Form_EditServiceImage(array('imageAttachment' => $imageAttachment,'itemId' => $itemid,'type'=>$type));
       $this->view->form = $form; 	
	
	if ($this->request->isPost()) {
       if ($form->isValid($this->request->getPost())) {
			$ip = $_SERVER['REMOTE_ADDR'];
			$data = array(
				'user_ip'=> $ip
			 );

			$modelImageAttachment = new Model_Image();
			$modelImageAttachment->updateById($id,$data);
			if(!($service_id == 0) || !($service_id == null)){
					  $type_new = $type.'_service';
						 if($type_new == 'service_service'){
						   $itemid = $service_id;
						}					 
					}else{
					  $service_id = 0;
					  $type_new = $type;
					}
						
					$Itemdata = array(
					'item_id'=>$itemid,
					'service_id'=>$service_id,
					'image_id' =>$id,
					'type'=>$type_new
					);
					
					
				$modelItemImage = new Model_ItemImage();
				$Item_image_id = $modelItemImage->updateById($id,$Itemdata);

			    //D.A 30/08/2015 Remove Booking Photos Cache				
				require_once 'Zend/Cache.php';
				$company_id = CheckAuth::getCompanySession();
				$bookingPhotosCacheID=  $itemid.'_bookingPhotos';
				$bookingViewDir=get_config('cache').'/'.'bookingsView'.'/'.$company_id;								
				$frontEndOption= array('lifetime'=> 24 * 3600,
				'automatic_serialization'=> true);
				$backendOptions = array('cache_dir'=>$bookingViewDir );
				$Cache = Zend_Cache::factory('Core','File',$frontEndOption,$backendOptions);			
				$Cache->remove($bookingPhotosCacheID);
			   
				$this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));

				echo 1;
				exit;        			
			 
			}
        }
		
		$this->view->type = $type;		
		echo $this->view->render('item-image-attachment/image-upload.phtml');
        exit;		
	}
}

