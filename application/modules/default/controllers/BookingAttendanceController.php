<?php

class BookingAttendanceController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
		
        $pageName = $this->request->getParam('page_name', '');
        $this->view->page_title = (!empty($pageName))?$pageName." - Bookings Attendance":"Bookings Attendance";
	}
	
	
	public function indexAction(){
	  
	  
	  CheckAuth::checkLoggedIn();
	  CheckAuth::checkCredential(array('canSeeBookingChecks'));
	  
	  $orderBy = $this->request->getParam('sort', 'booking_attendance_id');
      $sortingMethod = $this->request->getParam('method', 'desc');
      //$currentPage = $this->request->getParam('page', 1);
      $filters = $this->request->getParam('fltr', array());
	    
      $is_first_time = $this->request->getParam('is_first_time');
	  $page_number = $this->request->getParam('page_number');
	  $modelUser = new Model_User();
	  
        if($filters){
            foreach ($filters as &$filter){
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }
	  
	  /*$pager = new Model_Pager();
      $pager->perPage = get_config('perPage');
      $pager->currentPage = $currentPage;
      $pager->url = $_SERVER['REQUEST_URI'];*/
		
	   if ($this->request->isPost()) {
		  if(isset($page_number)){
		   $perPage = 15;
		   $currentPage = $page_number +1;
	    }
		
		
		$loggedUser = CheckAuth::getLoggedUser();
		//$filters['company_id'] = $loggedUser['company_id'];
		$filters['company_id'] = CheckAuth::getCompanySession();

		
		 $modelBookingAttendance = new Model_BookingAttendance();
         $data = $modelBookingAttendance->getAll($filters, "{$orderBy} {$sortingMethod}", $pager , 0 ,$perPage,$currentPage);	  
	     $result = array();
		 
	  foreach($data as $key=>$row){
///////////Added By Rand //////////////////////////
	  //Get booking Info
	  $modelBooking = new Model_Booking();
	  $booking = $modelBooking->getById($row['booking_id']);
	  $result[$key]['qoute'] = $booking['qoute'];
	  $result[$key]['booking_start'] = $booking['booking_start'];
	  $result[$key]['booking_end'] = $booking['booking_end'];
	  $result[$key]['booking']=$booking;
	  
	  //Get booking status
	  $modelBookingStatus = new Model_BookingStatus();
	  $BookingStatus = $modelBookingStatus->getById($booking['status_id']);
	  $result[$key]['status_id'] = $booking['status_id'];
      $result[$key]['BookingStatus'] = $BookingStatus['name'];
	  $result[$key]['color'] = $BookingStatus['color'];
	  
	  //get booking customer
	  $modelCustomer= new Model_Customer();
	  $customer= $modelCustomer->getById($booking['customer_id']);
	  $result[$key]['customer_name'] = get_customer_name($customer);
	  $result[$key]['customer'] = $customer;
	  
	  //get booking address
	  $modelBookingAddress = new Model_BookingAddress();
      $bookingAddress = $modelBookingAddress->getByBookingId($row['booking_id']);
	  $result[$key]['bookingAddress']=get_line_address($bookingAddress);
	  
	  //Get booking services
	  $modelContractorServiceBooking = new Model_ContractorServiceBooking();
	  $AllServices= $modelContractorServiceBooking->getContractorServicesByBookingId($row['booking_id']);
	  $modelService = new Model_Services();
	  foreach($AllServices as &$service){
		  $serviceData = $modelService->getById($service['service_id']);
		  $service['service_details']=$serviceData;
	  }
	  $result[$key]['AllServices']=$AllServices;
	  
	  //Get booking contractors
	  $allContractors= $modelContractorServiceBooking->getContractorsDataByBookingId($row['booking_id']);
	  $modelUser = new Model_User();
	  foreach($allContractors as &$contractor){
		  $userData = $modelUser->getById($contractor['contractor_id']);
		  $contractor['contractor_details']=$userData;
		  
		  //get contractor services
		  $contractorServices = $modelContractorServiceBooking->getByBookingIdAndContractorId($row['booking_id'], $contractor['contractor_id']);
		  $contractor['contractor_services']=$contractorServices ;
		  
		  //get contractor checklist
		  $last_check = $modelBookingAttendance->getLastCheckByBookingIdAndContractorId($row['booking_id'],$contractor['contractor_id']);
		  if($last_check){
		  $lastRecordData = $modelBookingAttendance->getByBookingAttendanceID($last_check);
		  if($lastRecordData){
		  $contractor['checkList']=$lastRecordData ;
		  }
		  else {$contractor['checkList']=array();}
		  }
		  else {$contractor['checkList']=array();}
		  
		  $LastCheckin = $modelBookingAttendance->getLastCheckInByBookingIdAndContractorId($row['booking_id'],$contractor['contractor_id']);
		  $lastCheckinData=[];
		  if($LastCheckin!= null || $LastCheckin!=0){
		  $lastCheckinData = $modelBookingAttendance->getByBookingAttendanceID($LastCheckin);}
		  $contractor['LastCheckIn']=$lastCheckinData ;
	  }
	  $result[$key]['AllBookingContractors']=$allContractors;
	  
	  ////// END /////////////////////////////////
		
		$result[$key]['booking_id'] = $row['booking_id'];
	    $result[$key]['booking_num'] = $row['booking_num'];
	    $result[$key]['username'] = $row['username'];
	    $result[$key]['contractor_id'] = $row['contractor_id'];
	  }
	  //var_dump($result);
	  
	    $this->view->is_first_time = $is_first_time;  
		$this->view->data = $result;
        $this->view->filters = $filters; 
        //$this->view->filters = $filters; 	
		echo $this->view->render('booking-attendance/draw-node.phtml');
		exit;  
		
		}  
	 
	  
	  /*$Contractordata=[];
	  $modelUser = new Model_User();
	  $activeContractor = $modelUser->getContractorsByStatus('TRUE');	
	    foreach ($activeContractor as $ContractorResult) {
			$modelUser->fill($ContractorResult, array('contractor_info'));
            $Contractordata[$ContractorResult['user_id']] = $ContractorResult['contractor_name'];
        }*/

            /**
             * Create contractor Element
             */
			 
		$select_contractor = new Zend_Form_Element_Select('fltr');
        $select_contractor->setBelongsTo('contractor');
        $select_contractor->setDecorators(array('ViewHelper'));
        $select_contractor->setValue(isset($filters['contractor']) ? $filters['contractor'] : '');
		
		$url = $this->router->assemble(array(), 'bookingCheckInCheckOutList');
		if(isset($filters['time'])){
	      $url = $url.'?fltr[time]='.$filters['time'].'&fltr[contractor]=';
	     }else{
		  $url = $url.'?fltr[contractor]=';
		 }
		
		$select_contractor->setAttrib('onchange', "fltr('".$url."','contractor-fltr')");
		
        
		$select_contractor->setAttrib('class', "form-control");
        $select_contractor->addMultiOption('', 'ALL TECHNICIANS');
		$states = $modelUser->getStates();
		foreach($states as $key => $state){
			$displayedState = $state['state']?$state['state']:'General';
			$activeContractor = $modelUser->getContractorsByStatus('TRUE', $state['state']);
			//var_dump($activeContractor);echo " seperator ";
			if(!empty($activeContractor)){
				foreach ($activeContractor as $result) {
					$modelUser->fill($result, array('contractor_info'));
					$data[$result['user_id']] = $result['contractor_name'];
				}
			}else{
			 $data = array();
			}
			//echo $displayedState. " ";var_dump($data);echo " sep ";
			if($data){
				$select_contractor->addMultiOption($displayedState, $displayedState);
				$select_contractor->setAttrib('disable', array('NSW','VIC','WA','QLD','SA','ACT','TAS','NT','unknown','General'));
				$select_contractor->addMultiOptions($data);
			}
			
			
		}
        //$select_contractor->addMultiOptions($Contractordata);
		
		
	$select_time = new Zend_Form_Element_Select('fltr');
        $select_time->setBelongsTo('time');
        $select_time->setDecorators(array('ViewHelper'));
		
		$url = $this->router->assemble(array(), 'bookingCheckInCheckOutList');
		if(isset($filters['contractor'])){
	      $url = $url.'?fltr[contractor]='.$filters['contractor'].'&fltr[time]=';
	     }else{
		  $url = $url.'?fltr[time]=';
		 }
		
        $select_time->setAttrib('onchange', "fltr('".$url."','time-fltr')");
        $select_time->setValue(isset($filters['time']) ? $filters['time'] : '');
	$select_time->setAttrib('class', "form-control");
        $select_time->addMultiOption('', 'ALL');
        $select_time->addMultiOption('current', 'CURRENT');
        $select_time->addMultiOption('previous', 'HISTORY');
		
		$select_active = new Zend_Form_Element_Select('active');
        $select_active->setDecorators(array('ViewHelper'));
		$select_active->setAttrib('onchange', "getContracotrsByStatus()");
        $select_active->setValue(isset($filters['active']) ? $filters['active'] : 'TRUE');
        
        $select_active->setAttrib('class', "form-control");

        $select_active->addMultiOption('TRUE', 'ACTIVE');
        $select_active->addMultiOption('FALSE', 'INACTIVE');
        $select_active->addMultiOption('ALL', 'ALL');
        $this->view->select_active = $select_active;
		
		
		
        $this->view->select_contractor = $select_contractor;
        $this->view->select_time = $select_time;

		
		
		
	  
	  //$this->view->data = $result;
	  //$this->view->currentPage = $currentPage;
      //$this->view->perPage = $pager->perPage;
      //$this->view->pageLinks = $pager->getPager();
      $this->view->sortingMethod = $sortingMethod;
      $this->view->orderBy = $orderBy;
      $this->view->filters = $filters;
	 
	  
	}
	
		public function indexContractorAction(){
	  
	  
	  CheckAuth::checkLoggedIn();
	  CheckAuth::checkCredential(array('canSeeBookingChecks'));
	  
	  $orderBy = $this->request->getParam('sort', 'booking_attendance_id');
      $sortingMethod = $this->request->getParam('method', 'desc');
      //$currentPage = $this->request->getParam('page', 1);
      $filters = $this->request->getParam('fltr', array());
	  
        if($filters){
            foreach ($filters as &$filter){
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }
	  
	   if ($this->request->isPost()) {
		  
		
		$loggedUser = CheckAuth::getLoggedUser();
		$filters['company_id'] = $loggedUser['company_id'];
		
		
		$contractor_id = $this->request->getParam('contractor_id', '');
		
		if(isset ($contractor_id) && $contractor_id != null)
		{
			
		
		   $filters['contractor'] = $contractor_id;
		}
		else {
			$filters['group'] = 1;
			$filters['group_by_contractor'] = 'contractor_id';
		}
		
		 $modelBookingAttendance = new Model_BookingAttendance();
         //$data = $modelBookingAttendance->getAll($filters, "{$orderBy} {$sortingMethod}", $pager , 0 ,$perPage,$currentPage);
         $data = $modelBookingAttendance->getAll($filters, "{$orderBy} {$sortingMethod}", $pager , 0 ,0,0);		 
	     $result = array();
		 
		 
		  $modelBooking = new Model_Booking();
		  $modelBookingStatus = new Model_BookingStatus();
		  $modelUser = new Model_User();
		  $modelBookingReminder= new Model_BookingReminder();
		  $modelBookingMultipleDays= new Model_BookingMultipleDays();
		  $modelContractorServiceBooking = new Model_ContractorServiceBooking();
		  $modelCustomer= new Model_Customer();
		 $modelBookingEstimate = new Model_BookingEstimate();
		 $modelBookingInvoice = new Model_BookingInvoice();
		 $customerContact = new Model_CustomerContact();
		 $modelBookingAddress = new Model_BookingAddress();
		 
		  if(isset ($contractor_id) && $contractor_id != null)
		{
	     foreach($data as $key=>&$booking_attendance)
	       {
				  
				  //get contractor info 
				  $contractor_info = $modelUser->getById($contractor_id);
				  
				  $booking_attendance['contractor_info']=$contractor_info;
			  //Main booking Info
		        $booking_info = $modelBooking->getById($booking_attendance['booking_id']);
				$booking_attendance['booking_info']=$booking_info;
				
			  //Booking status
		       $booking_status = $modelBookingStatus->getById($booking_info['status_id']);
		       $booking_attendance['booking_status']=$booking_status;
		      
			  //Get bookingReminders
			  $ReminderArray= $modelBookingReminder->getByBookingId($booking_attendance['booking_id']);
			  $lastReminderDate = $modelBookingReminder->getLastConfirmationByBookingId($booking_attendance['booking_id']);
			
				// is the booking confirmed
				 $isconfirmed = 0;
				  if(count($ReminderArray)>=1)
				  {
					 $isconfirmed = 1; 
				  }
				  else $isconfirmed = 0;
				  
			   $booking_attendance['is_confirmed']=$isconfirmed;
			   $booking_attendance['last_reminder_date']=$lastReminderDate;
			   
				//get last check(in or out) and last check in 
				  $last_check = $modelBookingAttendance->getLastCheckByBookingIdAndContractorId($booking_attendance['booking_id'],$contractor_id);
				  if($last_check){
				     $lastRecordData = $modelBookingAttendance->getByBookingAttendanceID($last_check);
						if($lastRecordData){
						$booking_attendance['last_check']=$lastRecordData ;
						}
				        else {$booking_attendance['last_check']=array();}
				    }
				  else {$booking_attendance['last_check']=array();}
				  
				  $LastCheckin = $modelBookingAttendance->getLastCheckInByBookingIdAndContractorId($booking_attendance['booking_id'],$contractor_id);
				  $lastCheckinData=array();
				  if($LastCheckin!= null || $LastCheckin!=0)
				  {
				  $lastCheckinData = $modelBookingAttendance->getByBookingAttendanceID($LastCheckin);
				  }
				$booking_attendance['LastCheckIn']=$lastCheckinData ;
				
				//get booking multiple days
				$booking_multiple_days = $modelBookingMultipleDays->getByBookingId($booking_attendance['booking_id']);
				$booking_attendance['multiple_days'] = $booking_multiple_days;
				
				
				//GET contractor services for this booking
				  $contractorServices = $modelContractorServiceBooking->getByBookingIdAndContractorId($booking_attendance['booking_id'], $contractor_id);
				  $booking_attendance['contractor_services']=$contractorServices ;
				  
				  
				 //get booking customer
				  $customer= $modelCustomer->getById($booking_info['customer_id']);
				  $booking_attendance['customer_name'] = get_customer_name($customer);
				  $booking_attendance['customer'] = $customer;
				  
				  //get booking estimate
				  $estimate = $modelBookingEstimate->getByBookingId($booking_attendance['booking_id']);
				  $booking_attendance['estimate'] = $estimate;
				  
				  //get booking invoice
	              $invoice = $modelBookingInvoice->getByBookingId($booking_attendance['booking_id']);
				  $booking_attendance['invoice'] = $invoice;
				  
				  //secondary contact list
                  $customerContactList = $customerContact->getByCustomerId($booking_info['customer_id']);
				  $booking_attendance['customer_contact_list'] = $customerContactList;
				  
				  //get booking address
				  $bookingAddress = $modelBookingAddress->getByBookingId($booking_attendance['booking_id']);
	              $booking_attendance['booking_line_address']=get_line_address($bookingAddress);
				  $booking_attendance['bookingAddress']=$bookingAddress;
				  
		  
				//$result[$key]['all_booking_attendance']=$data;
			  }
			  //var_dump($data);
			    $this->view->contractor_id = $contractor_id;
				$this->view->data = $data;
				$this->view->filters = $filters; 
				
				echo $this->view->render('booking-attendance/booking-attendance-for-contractor.phtml');
				exit; 
		}
		else 
		{
		  foreach($data as $key=>$row)
	     {
		  $contractor_id = $row['contractor_id'];
		  $result[$key]['contractor_id']=$contractor_id;
		  //get contractor info 
		  $contractor_info = $modelUser->getById($contractor_id);
		  $result[$key]['contractor_info']=$contractor_info; 
	    }
		$this->view->data = $result;
        $this->view->filters = $filters; 
		
		echo $this->view->render('booking-attendance/draw-node-contractor.phtml');
		exit;  
		
		}
	  //var_dump($result);
	  
	   
		}  
	 
	  
	  $Contractordata=[];
	  $modelUser = new Model_User();
	  $activeContractor = $modelUser->getContractorsByStatus('TRUE');	
	    foreach ($activeContractor as $ContractorResult) {
			$modelUser->fill($ContractorResult, array('contractor_info'));
            $Contractordata[$ContractorResult['user_id']] = $ContractorResult['contractor_name'];
        }

            /**
             * Create contractor Element
             */
			 
		$select_contractor = new Zend_Form_Element_Select('fltr');
        $select_contractor->setBelongsTo('contractor');
        $select_contractor->setDecorators(array('ViewHelper'));
        $select_contractor->setValue(isset($filters['contractor']) ? $filters['contractor'] : '');
		
		$url = $this->router->assemble(array(), 'bookingAttendancePerContractor');
		if(isset($filters['time'])){
	      $url = $url.'?fltr[time]='.$filters['time'].'&fltr[contractor]=';
	     }else{
		  $url = $url.'?fltr[contractor]=';
		 }
		
		$select_contractor->setAttrib('onchange', "fltr('".$url."','contractor-fltr')");
		
        
		$select_contractor->setAttrib('class', "form-control");
        $select_contractor->addMultiOption('', 'ALL TECHNICIANS');
        $select_contractor->addMultiOptions($Contractordata);
		
		
	$select_time = new Zend_Form_Element_Select('fltr');
        $select_time->setBelongsTo('time');
        $select_time->setDecorators(array('ViewHelper'));
		
		$url = $this->router->assemble(array(), 'bookingAttendancePerContractor');
		if(isset($filters['contractor'])){
	      $url = $url.'?fltr[contractor]='.$filters['contractor'].'&fltr[time]=';
	     }else{
		  $url = $url.'?fltr[time]=';
		 }
		
        $select_time->setAttrib('onchange', "fltr('".$url."','time-fltr')");
        $select_time->setValue(isset($filters['time']) ? $filters['time'] : '');
	$select_time->setAttrib('class', "form-control");
        $select_time->addMultiOption('', 'ALL');
        $select_time->addMultiOption('current', 'CURRENT');
        $select_time->addMultiOption('previous', 'HISTORY');
		
		
		
        $this->view->select_contractor = $select_contractor;
        $this->view->select_time = $select_time;

      $this->view->sortingMethod = $sortingMethod;
      $this->view->orderBy = $orderBy;
      $this->view->filters = $filters;
	 
	  
	}
	
}
?>