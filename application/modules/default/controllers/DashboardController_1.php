<?php

class DashboardController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        /* Initialize action controller here */
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
		
    }

    public function indexAction() {
	$this->view->sub_menu = 'future_bookings';
	
        CheckAuth::checkPermission(array('dashboard'));

        //check login
        CheckAuth::checkLoggedIn();

        //
        // load model
        //
        $modelUser = new Model_User();
        $modelBooking = new Model_Booking();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelContractorInfo = new Model_ContractorInfo();
        $modelContractorEmployee = new Model_ContractorEmployee();

        //get logged user
        $loggedUser = CheckAuth::getLoggedUser();

        // role of logged user
        $role = CheckAuth::getRoleName();
        $this->view->role = $role;


        //logged user details
        $user = $modelUser->getById($loggedUser['user_id']);
        $this->view->user = $user;


        if (!$user) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }


        //#############################//
        //Contractor Info and employees//
        //#############################//
        $contractorInfo = $modelContractorInfo->getByContractorId($loggedUser['user_id']);
        $this->view->contractorInfo = $contractorInfo;

        $contractorEmployee = $modelContractorEmployee->getByContractorInfoId($contractorInfo['contractor_info_id']);
        $this->view->contractorEmployee = $contractorEmployee;

        //##########################//
        //        Statistics        //
        //##########################//
        //
        //Get Count Complete and inComplete
        //
        $myDashboard = false;

        if (!CheckAuth::getDashboardStatusSession()) {
            if (CheckAuth::checkCredential(array('canSeeAllBookingStatistics'))) {
                CheckAuth::setDashboardStatusSession('all_dashboard');
            } else {
                CheckAuth::setDashboardStatusSession('my_dashboard');
            }
        }

        $dasboardStatus = CheckAuth::getDashboardStatusSession();
        if ($dasboardStatus == 'my_dashboard') {
            $myDashboard = true;
        }

        $filters = array();
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $totalBooking = $modelBooking->getBookingCount($filters);
        $this->view->totalBooking = $totalBooking;

        $modelBookingStatus = new Model_BookingStatus();
        $allBookingStatus = $modelBookingStatus->getAll();

        foreach ($allBookingStatus as &$bookingStatus) {
            $filters['status'] = $bookingStatus['booking_status_id'];
            $bookingStatus['total'] = $modelBooking->getBookingCount($filters);
        }

        $this->view->allBookingStatus = $allBookingStatus;


        
        //get invoice information
        $modelBooking = new Model_Booking();
        $filters = array();
        $filters['invoice_type'] = 'unpaid';
        $filters['convert_status'] = 'invoice';
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }

        $totalQoute = $modelBooking->total($filters, 'qoute');
        $totalPaidAmount = $modelBooking->total($filters, 'paid_amount');

        $invoicesTotalAmount = $totalQoute - $totalPaidAmount;

        $this->view->invoicesTotalAmount = $invoicesTotalAmount;



        $modelBookingInvoice = new Model_BookingInvoice();
        $filters = array();
        $filters['invoice_type'] = 'unpaid';
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $invoices = $modelBookingInvoice->getAll($filters);
        $modelBookingInvoice->fills($invoices, array('number_of_due_days', 'paid_amount'));

        $due15 = 0;
        $due30 = 0;
        $due45 = 0;
        $dueAbove = 0;

        if ($invoices) {
            foreach ($invoices AS $invoice) {
                if ($invoice['due'] <= 15) {
                    $newdue15 = $invoice['amount'] - $invoice['paid_amount'];
                    $due15 += $newdue15;
                } elseif ($invoice['due'] <= 30) {
                    $newdue30 = $invoice['amount'] - $invoice['paid_amount'];
                    $due30 += $newdue30;
                } elseif ($invoice['due'] <= 45) {
                    $newdue45 = $invoice['amount'] - $invoice['paid_amount'];
                    $due45 += $newdue45;
                } else {
                    $newdueAbove = $invoice['amount'] - $invoice['paid_amount'];
                    $dueAbove += $newdueAbove;
                }
            }
        }

        $this->view->due15 = $due15;
        $this->view->due30 = $due30;
        $this->view->due45 = $due45;
        $this->view->dueAbove = $dueAbove;


             

        //
        //Future Bookings 
        //
        $futureBookingStatus = $modelBookingStatus->getAll(array('more_one_status_name' => array('IN PROGRESS', 'TO DO', 'ON HOLD', 'CANCELLED', 'TENTATIVE', 'TO VISIT')));

        $this->view->futureBookingStatus = $futureBookingStatus;

        // Future Today
        $today = getTimePeriodByName('today');

        $filters = array();
        $filters['booking_start_between'] = date('Y-m-d H:i:s', time());
        $filters['booking_end_between'] = $today['end'];
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $todayAllBookingStatus = $futureBookingStatus;
        foreach ($todayAllBookingStatus as &$todayBookingStatus) {
            $filters['status'] = $todayBookingStatus['booking_status_id'];
            $todayBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $todayBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');
        }
        $this->view->todayAllBookingStatus = $todayAllBookingStatus;


        // Future Tomorow
        $tomorrow = getTimePeriodByName('tomorrow');

        $filters = array();
        $filters['booking_start_between'] = $tomorrow['start'];
        $filters['booking_end_between'] = $tomorrow['end'];
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $tomorowAllBookingStatus = $futureBookingStatus;
        foreach ($tomorowAllBookingStatus as &$tomorowBookingStatus) {
            $filters['status'] = $tomorowBookingStatus['booking_status_id'];
            $tomorowBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $tomorowBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');
        }
        $this->view->tomorowAllBookingStatus = $tomorowAllBookingStatus;


        //Future Week
        $this_week = getTimePeriodByName('this_week');

        $filters = array();
        $filters['booking_start_between'] = date('Y-m-d H:i:s', time());
        $filters['booking_end_between'] = $this_week['end'];
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $weekAllBookingStatus = $futureBookingStatus;
        foreach ($weekAllBookingStatus as &$weekBookingStatus) {
            $filters['status'] = $weekBookingStatus['booking_status_id'];
            $weekBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $weekBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');
        }
        $this->view->weekAllBookingStatus = $weekAllBookingStatus;


        // Future Month
        $this_month = getTimePeriodByName('this_month');

        $filters = array();
        $filters['booking_start_between'] = date('Y-m-d H:i:s', time());
        $filters['booking_end_between'] = $this_month['end'];
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $monthAllBookingStatus = $futureBookingStatus;
        foreach ($monthAllBookingStatus as &$monthBookingStatus) {
            $filters['status'] = $monthBookingStatus['booking_status_id'];
            $monthBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $monthBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');
        }
        $this->view->monthAllBookingStatus = $monthAllBookingStatus;


        // Future quarter
        $this_quarter = getTimePeriodByName('this_quarter');

        $filters = array();
        $filters['booking_start_between'] = date('Y-m-d H:i:s', time());
        $filters['booking_end_between'] = $this_quarter['end'];
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $quarterAllBookingStatus = $futureBookingStatus;
        foreach ($quarterAllBookingStatus as &$quarterBookingStatus) {
            $filters['status'] = $quarterBookingStatus['booking_status_id'];
            $quarterBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $quarterBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');
        }
        $this->view->quarterAllBookingStatus = $quarterAllBookingStatus;


        // Future Next week
        $next_week = getTimePeriodByName('next_week');

        $filters = array();
        $filters['booking_start_between'] = $next_week['start'];
        $filters['booking_end_between'] = $next_week['end'];
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $nextWeekAllBookingStatus = $futureBookingStatus;
        foreach ($nextWeekAllBookingStatus as &$nextWeekBookingStatus) {
            $filters['status'] = $nextWeekBookingStatus['booking_status_id'];
            $nextWeekBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $nextWeekBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');
        }
        $this->view->nextWeekAllBookingStatus = $nextWeekAllBookingStatus;


        // Future Next Month
        $next_month = getTimePeriodByName('next_month');

        $filters = array();
        $filters['booking_start_between'] = $next_month['start'];
        $filters['booking_end_between'] = $next_month['end'];
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $nextMonthAllBookingStatus = $futureBookingStatus;
        foreach ($nextMonthAllBookingStatus as &$nextMonthBookingStatus) {
            $filters['status'] = $nextMonthBookingStatus['booking_status_id'];
            $nextMonthBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $nextMonthBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');
        }
        $this->view->nextMonthAllBookingStatus = $nextMonthAllBookingStatus;

        // Future Next Quarter
        $next_quarter = getTimePeriodByName('next_quarter');

        $filters = array();
        $filters['booking_start_between'] = $next_quarter['start'];
        $filters['booking_end_between'] = $next_quarter['end'];
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $nextQuarterAllBookingStatus = $futureBookingStatus;
        foreach ($nextQuarterAllBookingStatus as &$nextQuarterBookingStatus) {
            $filters['status'] = $nextQuarterBookingStatus['booking_status_id'];
            $nextQuarterBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $nextQuarterBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');
        }
        $this->view->nextQuarterAllBookingStatus = $nextQuarterAllBookingStatus;


        //all future Sales
        $filters = array();
        $filters['booking_start'] = date('Y-m-d H:i:s', time());
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $allFutureBookingStatus = $futureBookingStatus;
        foreach ($allFutureBookingStatus as &$futureBookingStatus) {
            $filters['status'] = $futureBookingStatus['booking_status_id'];
            $futureBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $futureBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');
        }
        $this->view->allFutureBookingStatus = $allFutureBookingStatus;


    }

    
	
	
	public function futureAction(){
	
		$this->view->sub_menu = 'future_bookings';
	
        CheckAuth::checkPermission(array('dashboard'));

        //check login
        CheckAuth::checkLoggedIn();

        //
        // load model
        //
        $modelUser = new Model_User();
        $modelBooking = new Model_Booking();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelContractorInfo = new Model_ContractorInfo();
        $modelContractorEmployee = new Model_ContractorEmployee();

        //get logged user
        $loggedUser = CheckAuth::getLoggedUser();

        // role of logged user
        $role = CheckAuth::getRoleName();
        $this->view->role = $role;


        //logged user details
        $user = $modelUser->getById($loggedUser['user_id']);
        $this->view->user = $user;


        if (!$user) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }


        //#############################//
        //Contractor Info and employees//
        //#############################//
        $contractorInfo = $modelContractorInfo->getByContractorId($loggedUser['user_id']);
        $this->view->contractorInfo = $contractorInfo;

        $contractorEmployee = $modelContractorEmployee->getByContractorInfoId($contractorInfo['contractor_info_id']);
        $this->view->contractorEmployee = $contractorEmployee;

        //##########################//
        //        Statistics        //
        //##########################//
        //
        //Get Count Complete and inComplete
        //
        $myDashboard = false;

        if (!CheckAuth::getDashboardStatusSession()) {
            if (CheckAuth::checkCredential(array('canSeeAllBookingStatistics'))) {
                CheckAuth::setDashboardStatusSession('all_dashboard');
            } else {
                CheckAuth::setDashboardStatusSession('my_dashboard');
            }
        }

        $dasboardStatus = CheckAuth::getDashboardStatusSession();
        if ($dasboardStatus == 'my_dashboard') {
            $myDashboard = true;
        }

        $filters = array();
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $totalBooking = $modelBooking->getBookingCount($filters);
        $this->view->totalBooking = $totalBooking;

        $modelBookingStatus = new Model_BookingStatus();
        $allBookingStatus = $modelBookingStatus->getAll();

        foreach ($allBookingStatus as &$bookingStatus) {
            $filters['status'] = $bookingStatus['booking_status_id'];
            $bookingStatus['total'] = $modelBooking->getBookingCount($filters);
        }

        $this->view->allBookingStatus = $allBookingStatus;


        
        //get invoice information
        $modelBooking = new Model_Booking();
        $filters = array();
        $filters['invoice_type'] = 'unpaid';
        $filters['convert_status'] = 'invoice';
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }

        $totalQoute = $modelBooking->total($filters, 'qoute');
        $totalPaidAmount = $modelBooking->total($filters, 'paid_amount');

        $invoicesTotalAmount = $totalQoute - $totalPaidAmount;

        $this->view->invoicesTotalAmount = $invoicesTotalAmount;



        $modelBookingInvoice = new Model_BookingInvoice();
        $filters = array();
        $filters['invoice_type'] = 'unpaid';
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $invoices = $modelBookingInvoice->getAll($filters);
        $modelBookingInvoice->fills($invoices, array('number_of_due_days', 'paid_amount'));

        $due15 = 0;
        $due30 = 0;
        $due45 = 0;
        $dueAbove = 0;

        if ($invoices) {
            foreach ($invoices AS $invoice) {
                if ($invoice['due'] <= 15) {
                    $newdue15 = $invoice['amount'] - $invoice['paid_amount'];
                    $due15 += $newdue15;
                } elseif ($invoice['due'] <= 30) {
                    $newdue30 = $invoice['amount'] - $invoice['paid_amount'];
                    $due30 += $newdue30;
                } elseif ($invoice['due'] <= 45) {
                    $newdue45 = $invoice['amount'] - $invoice['paid_amount'];
                    $due45 += $newdue45;
                } else {
                    $newdueAbove = $invoice['amount'] - $invoice['paid_amount'];
                    $dueAbove += $newdueAbove;
                }
            }
        }

        $this->view->due15 = $due15;
        $this->view->due30 = $due30;
        $this->view->due45 = $due45;
        $this->view->dueAbove = $dueAbove;


             

        //
        //Future Bookings 
        //
        $futureBookingStatus = $modelBookingStatus->getAll(array('more_one_status_name' => array('IN PROGRESS', 'TO DO', 'ON HOLD', 'CANCELLED', 'TENTATIVE', 'TO VISIT')));

        $this->view->futureBookingStatus = $futureBookingStatus;

        // Future Today
        $today = getTimePeriodByName('today');

        $filters = array();
        $filters['booking_start_between'] = date('Y-m-d H:i:s', time());
        $filters['booking_end_between'] = $today['end'];
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $todayAllBookingStatus = $futureBookingStatus;
        foreach ($todayAllBookingStatus as &$todayBookingStatus) {
            $filters['status'] = $todayBookingStatus['booking_status_id'];
            $todayBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $todayBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');
        }
        $this->view->todayAllBookingStatus = $todayAllBookingStatus;


        // Future Tomorow
        $tomorrow = getTimePeriodByName('tomorrow');

        $filters = array();
        $filters['booking_start_between'] = $tomorrow['start'];
        $filters['booking_end_between'] = $tomorrow['end'];
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $tomorowAllBookingStatus = $futureBookingStatus;
        foreach ($tomorowAllBookingStatus as &$tomorowBookingStatus) {
            $filters['status'] = $tomorowBookingStatus['booking_status_id'];
            $tomorowBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $tomorowBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');
        }
        $this->view->tomorowAllBookingStatus = $tomorowAllBookingStatus;


        //Future Week
        $this_week = getTimePeriodByName('this_week');

        $filters = array();
        $filters['booking_start_between'] = date('Y-m-d H:i:s', time());
        $filters['booking_end_between'] = $this_week['end'];
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $weekAllBookingStatus = $futureBookingStatus;
        foreach ($weekAllBookingStatus as &$weekBookingStatus) {
            $filters['status'] = $weekBookingStatus['booking_status_id'];
            $weekBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $weekBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');
        }
        $this->view->weekAllBookingStatus = $weekAllBookingStatus;


        // Future Month
        $this_month = getTimePeriodByName('this_month');

        $filters = array();
        $filters['booking_start_between'] = date('Y-m-d H:i:s', time());
        $filters['booking_end_between'] = $this_month['end'];
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $monthAllBookingStatus = $futureBookingStatus;
        foreach ($monthAllBookingStatus as &$monthBookingStatus) {
            $filters['status'] = $monthBookingStatus['booking_status_id'];
            $monthBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $monthBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');
        }
        $this->view->monthAllBookingStatus = $monthAllBookingStatus;


        // Future quarter
        $this_quarter = getTimePeriodByName('this_quarter');

        $filters = array();
        $filters['booking_start_between'] = date('Y-m-d H:i:s', time());
        $filters['booking_end_between'] = $this_quarter['end'];
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $quarterAllBookingStatus = $futureBookingStatus;
        foreach ($quarterAllBookingStatus as &$quarterBookingStatus) {
            $filters['status'] = $quarterBookingStatus['booking_status_id'];
            $quarterBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $quarterBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');
        }
        $this->view->quarterAllBookingStatus = $quarterAllBookingStatus;


        // Future Next week
        $next_week = getTimePeriodByName('next_week');

        $filters = array();
        $filters['booking_start_between'] = $next_week['start'];
        $filters['booking_end_between'] = $next_week['end'];
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $nextWeekAllBookingStatus = $futureBookingStatus;
        foreach ($nextWeekAllBookingStatus as &$nextWeekBookingStatus) {
            $filters['status'] = $nextWeekBookingStatus['booking_status_id'];
            $nextWeekBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $nextWeekBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');
        }
        $this->view->nextWeekAllBookingStatus = $nextWeekAllBookingStatus;


        // Future Next Month
        $next_month = getTimePeriodByName('next_month');

        $filters = array();
        $filters['booking_start_between'] = $next_month['start'];
        $filters['booking_end_between'] = $next_month['end'];
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $nextMonthAllBookingStatus = $futureBookingStatus;
        foreach ($nextMonthAllBookingStatus as &$nextMonthBookingStatus) {
            $filters['status'] = $nextMonthBookingStatus['booking_status_id'];
            $nextMonthBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $nextMonthBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');
        }
        $this->view->nextMonthAllBookingStatus = $nextMonthAllBookingStatus;

        // Future Next Quarter
        $next_quarter = getTimePeriodByName('next_quarter');

        $filters = array();
        $filters['booking_start_between'] = $next_quarter['start'];
        $filters['booking_end_between'] = $next_quarter['end'];
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $nextQuarterAllBookingStatus = $futureBookingStatus;
        foreach ($nextQuarterAllBookingStatus as &$nextQuarterBookingStatus) {
            $filters['status'] = $nextQuarterBookingStatus['booking_status_id'];
            $nextQuarterBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $nextQuarterBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');
        }
        $this->view->nextQuarterAllBookingStatus = $nextQuarterAllBookingStatus;


        //all future Sales
        $filters = array();
        $filters['booking_start'] = date('Y-m-d H:i:s', time());
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $allFutureBookingStatus = $futureBookingStatus;
        foreach ($allFutureBookingStatus as &$futureBookingStatus) {
            $filters['status'] = $futureBookingStatus['booking_status_id'];
            $futureBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $futureBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');
        }
        $this->view->allFutureBookingStatus = $allFutureBookingStatus;


       
	}
	
	public function pastAction() {
	$this->view->sub_menu = 'past_bookings';
	 CheckAuth::checkPermission(array('dashboard'));

        //check login
        CheckAuth::checkLoggedIn();

        //
        // load model
        //
        $modelUser = new Model_User();
        $modelBooking = new Model_Booking();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelContractorInfo = new Model_ContractorInfo();
        $modelContractorEmployee = new Model_ContractorEmployee();

        //get logged user
        $loggedUser = CheckAuth::getLoggedUser();

        // role of logged user
        $role = CheckAuth::getRoleName();
        $this->view->role = $role;


        //logged user details
        $user = $modelUser->getById($loggedUser['user_id']);
        $this->view->user = $user;


        if (!$user) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }


        //#############################//
        //Contractor Info and employees//
        //#############################//
        $contractorInfo = $modelContractorInfo->getByContractorId($loggedUser['user_id']);
        $this->view->contractorInfo = $contractorInfo;

        $contractorEmployee = $modelContractorEmployee->getByContractorInfoId($contractorInfo['contractor_info_id']);
        $this->view->contractorEmployee = $contractorEmployee;

        //##########################//
        //        Statistics        //
        //##########################//
        //
        //Get Count Complete and inComplete
        //
        $myDashboard = false;

        if (!CheckAuth::getDashboardStatusSession()) {
            if (CheckAuth::checkCredential(array('canSeeAllBookingStatistics'))) {
                CheckAuth::setDashboardStatusSession('all_dashboard');
            } else {
                CheckAuth::setDashboardStatusSession('my_dashboard');
            }
        }

        $dasboardStatus = CheckAuth::getDashboardStatusSession();
        if ($dasboardStatus == 'my_dashboard') {
            $myDashboard = true;
        }

        $filters = array();
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $totalBooking = $modelBooking->getBookingCount($filters);
        $this->view->totalBooking = $totalBooking;

        $modelBookingStatus = new Model_BookingStatus();
        $allBookingStatus = $modelBookingStatus->getAll();

        foreach ($allBookingStatus as &$bookingStatus) {
            $filters['status'] = $bookingStatus['booking_status_id'];
            $bookingStatus['total'] = $modelBooking->getBookingCount($filters);
        }

        $this->view->allBookingStatus = $allBookingStatus;


       
        //get invoice information
        $modelBooking = new Model_Booking();
        $filters = array();
        $filters['invoice_type'] = 'unpaid';
        $filters['convert_status'] = 'invoice';
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }

        $totalQoute = $modelBooking->total($filters, 'qoute');
        $totalPaidAmount = $modelBooking->total($filters, 'paid_amount');

        $invoicesTotalAmount = $totalQoute - $totalPaidAmount;

        $this->view->invoicesTotalAmount = $invoicesTotalAmount;



        $modelBookingInvoice = new Model_BookingInvoice();
        $filters = array();
        $filters['invoice_type'] = 'unpaid';
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $invoices = $modelBookingInvoice->getAll($filters);
        $modelBookingInvoice->fills($invoices, array('number_of_due_days', 'paid_amount'));

        $due15 = 0;
        $due30 = 0;
        $due45 = 0;
        $dueAbove = 0;

        if ($invoices) {
            foreach ($invoices AS $invoice) {
                if ($invoice['due'] <= 15) {
                    $newdue15 = $invoice['amount'] - $invoice['paid_amount'];
                    $due15 += $newdue15;
                } elseif ($invoice['due'] <= 30) {
                    $newdue30 = $invoice['amount'] - $invoice['paid_amount'];
                    $due30 += $newdue30;
                } elseif ($invoice['due'] <= 45) {
                    $newdue45 = $invoice['amount'] - $invoice['paid_amount'];
                    $due45 += $newdue45;
                } else {
                    $newdueAbove = $invoice['amount'] - $invoice['paid_amount'];
                    $dueAbove += $newdueAbove;
                }
            }
        }

        $this->view->due15 = $due15;
        $this->view->due30 = $due30;
        $this->view->due45 = $due45;
        $this->view->dueAbove = $dueAbove;
	
	 //
        //Past Bookings
        //
        $pastBookingStatus = $modelBookingStatus->getAll(array('more_one_status_name' => array('IN PROGRESS', 'QUOTED ', 'ON HOLD', 'CANCELLED', 'TO DO', 'TENTATIVE', 'AWAITING UPDATE', 'FAILED')));

        $this->view->pastBookingStatus = $pastBookingStatus;

        //Last week(Past Bookings)
        $lastWeek = getTimePeriodByName('last_week');

        $filters = array();
        $filters['booking_start_between'] = $lastWeek['start'];
        $filters['booking_end_between'] = $lastWeek['end'];
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $lastWeekAllPastBookingStatus = $pastBookingStatus;
        foreach ($lastWeekAllPastBookingStatus as &$lastWeekPastBookingStatus) {
            $filters['status'] = $lastWeekPastBookingStatus['booking_status_id'];
            $lastWeekPastBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $lastWeekPastBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');
        }
        $this->view->lastWeekAllPastBookingStatus = $lastWeekAllPastBookingStatus;


        // Last Month (Past Bookings)
        $lastMonth = getTimePeriodByName('last_month');

        $filters = array();
        $filters['booking_start_between'] = $lastMonth['start'];
        $filters['booking_end_between'] = $lastMonth['end'];
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $lastMonthAllPastBookingStatus = $pastBookingStatus;
        foreach ($lastMonthAllPastBookingStatus as &$lastMonthPastBookingStatus) {
            $filters['status'] = $lastMonthPastBookingStatus['booking_status_id'];
            $lastMonthPastBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $lastMonthPastBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');
        }
        $this->view->lastMonthAllPastBookingStatus = $lastMonthAllPastBookingStatus;

        // Last quarter (Past Bookings)
        $lastQuarter = getTimePeriodByName('last_quarter');

        $filters = array();
        $filters['booking_start_between'] = $lastQuarter['start'];
        $filters['booking_end_between'] = $lastQuarter['end'];
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $lastQuarterAllPastBookingStatus = $pastBookingStatus;
        foreach ($lastQuarterAllPastBookingStatus as &$lastQuarterPastBookingStatus) {
            $filters['status'] = $lastQuarterPastBookingStatus['booking_status_id'];
            $lastQuarterPastBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $lastQuarterPastBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');
        }
        $this->view->lastQuarterAllPastBookingStatus = $lastQuarterAllPastBookingStatus;


        // Last Year (Past Bookings)
        $lastYear = getTimePeriodByName('last_year');

        $filters = array();
        $filters['booking_start_between'] = $lastYear['start'];
        $filters['booking_end_between'] = $lastYear['end'];
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $lastYearAllPastBookingStatus = $pastBookingStatus;
        foreach ($lastYearAllPastBookingStatus as &$lastYearPastBookingStatus) {
            $filters['status'] = $lastYearPastBookingStatus['booking_status_id'];
            $lastYearPastBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $lastYearPastBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');
        }
        $this->view->lastYearAllPastBookingStatus = $lastYearAllPastBookingStatus;

        //Today (Past Bookings)
        $today = getTimePeriodByName('today');

        $filters = array();
        $filters['booking_start_between'] = $today['start'];
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $todayAllPastBookingStatus = $pastBookingStatus;
        foreach ($todayAllPastBookingStatus as &$todayPastBookingStatus) {
            $filters['status'] = $todayPastBookingStatus['booking_status_id'];
            $todayPastBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $todayPastBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');
        }
        $this->view->todayAllPastBookingStatus = $todayAllPastBookingStatus;


        //This week (Past Bookings)
        $this_week = getTimePeriodByName('this_week');

        $filters = array();
        $filters['booking_start_between'] = $this_week['start'];
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $thisWeekAllPastBookingStatus = $pastBookingStatus;
        foreach ($thisWeekAllPastBookingStatus as &$thisWeekPastBookingStatus) {
            $filters['status'] = $thisWeekPastBookingStatus['booking_status_id'];
            $thisWeekPastBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $thisWeekPastBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');
        }
        $this->view->thisWeekAllPastBookingStatus = $thisWeekAllPastBookingStatus;


        //This month (Past Bookings)
        $this_month = getTimePeriodByName('this_month');

        $filters = array();
        $filters['booking_start_between'] = $this_month['start'];
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $thisMonthAllPastBookingStatus = $pastBookingStatus;
        foreach ($thisMonthAllPastBookingStatus as &$thisMonthPastBookingStatus) {
            $filters['status'] = $thisMonthPastBookingStatus['booking_status_id'];
            $thisMonthPastBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $thisMonthPastBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');
        }
        $this->view->thisMonthAllPastBookingStatus = $thisMonthAllPastBookingStatus;

        //This quarter (Past Bookings)
        $this_quarter = getTimePeriodByName('this_quarter');

        $filters = array();
        $filters['booking_start_between'] = $this_quarter['start'];
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $thisQuarterAllPastBookingStatus = $pastBookingStatus;
        foreach ($thisQuarterAllPastBookingStatus as &$thisQuarterPastBookingStatus) {
            $filters['status'] = $thisQuarterPastBookingStatus['booking_status_id'];
            $thisQuarterPastBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $thisQuarterPastBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');
        }
        $this->view->thisQuarterAllPastBookingStatus = $thisQuarterAllPastBookingStatus;


        //This year (Past Bookings)
        $this_year = getTimePeriodByName('this_year');

        $filters = array();
        $filters['booking_start_between'] = $this_year['start'];
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $thisYearAllPastBookingStatus = $pastBookingStatus;
        foreach ($thisYearAllPastBookingStatus as &$thisYearPastBookingStatus) {
            $filters['status'] = $thisYearPastBookingStatus['booking_status_id'];
            $thisYearPastBookingStatus['count'] = $modelBooking->getBookingCount($filters);
            $thisYearPastBookingStatus['total_qoute'] = $modelBooking->total($filters, 'qoute');
        }
        $this->view->thisYearAllPastBookingStatus = $thisYearAllPastBookingStatus;



	}
	
	
	public function completedAction(){
		$this->view->sub_menu = 'completed_bookings';
	
        CheckAuth::checkPermission(array('dashboard'));

        //check login
        CheckAuth::checkLoggedIn();

        //
        // load model
        //
        $modelUser = new Model_User();
        $modelBooking = new Model_Booking();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelContractorInfo = new Model_ContractorInfo();
        $modelContractorEmployee = new Model_ContractorEmployee();

        //get logged user
        $loggedUser = CheckAuth::getLoggedUser();

        // role of logged user
        $role = CheckAuth::getRoleName();
        $this->view->role = $role;


        //logged user details
        $user = $modelUser->getById($loggedUser['user_id']);
        $this->view->user = $user;


        if (!$user) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }


        //#############################//
        //Contractor Info and employees//
        //#############################//
        $contractorInfo = $modelContractorInfo->getByContractorId($loggedUser['user_id']);
        $this->view->contractorInfo = $contractorInfo;

        $contractorEmployee = $modelContractorEmployee->getByContractorInfoId($contractorInfo['contractor_info_id']);
        $this->view->contractorEmployee = $contractorEmployee;

        //##########################//
        //        Statistics        //
        //##########################//
        //
        //Get Count Complete and inComplete
        //
        $myDashboard = false;

        if (!CheckAuth::getDashboardStatusSession()) {
            if (CheckAuth::checkCredential(array('canSeeAllBookingStatistics'))) {
                CheckAuth::setDashboardStatusSession('all_dashboard');
            } else {
                CheckAuth::setDashboardStatusSession('my_dashboard');
            }
        }

        $dasboardStatus = CheckAuth::getDashboardStatusSession();
        if ($dasboardStatus == 'my_dashboard') {
            $myDashboard = true;
        }

        $filters = array();
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $totalBooking = $modelBooking->getBookingCount($filters);
        $this->view->totalBooking = $totalBooking;

        $modelBookingStatus = new Model_BookingStatus();
        $allBookingStatus = $modelBookingStatus->getAll();

        foreach ($allBookingStatus as &$bookingStatus) {
            $filters['status'] = $bookingStatus['booking_status_id'];
            $bookingStatus['total'] = $modelBooking->getBookingCount($filters);
        }

        $this->view->allBookingStatus = $allBookingStatus;


        //get invoice information
        $modelBooking = new Model_Booking();
        $filters = array();
        $filters['invoice_type'] = 'unpaid';
        $filters['convert_status'] = 'invoice';
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }

        $totalQoute = $modelBooking->total($filters, 'qoute');
        $totalPaidAmount = $modelBooking->total($filters, 'paid_amount');

        $invoicesTotalAmount = $totalQoute - $totalPaidAmount;

        $this->view->invoicesTotalAmount = $invoicesTotalAmount;



        $modelBookingInvoice = new Model_BookingInvoice();
        $filters = array();
        $filters['invoice_type'] = 'unpaid';
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $invoices = $modelBookingInvoice->getAll($filters);
        $modelBookingInvoice->fills($invoices, array('number_of_due_days', 'paid_amount'));

        $due15 = 0;
        $due30 = 0;
        $due45 = 0;
        $dueAbove = 0;

        if ($invoices) {
            foreach ($invoices AS $invoice) {
                if ($invoice['due'] <= 15) {
                    $newdue15 = $invoice['amount'] - $invoice['paid_amount'];
                    $due15 += $newdue15;
                } elseif ($invoice['due'] <= 30) {
                    $newdue30 = $invoice['amount'] - $invoice['paid_amount'];
                    $due30 += $newdue30;
                } elseif ($invoice['due'] <= 45) {
                    $newdue45 = $invoice['amount'] - $invoice['paid_amount'];
                    $due45 += $newdue45;
                } else {
                    $newdueAbove = $invoice['amount'] - $invoice['paid_amount'];
                    $dueAbove += $newdueAbove;
                }
            }
        }

        $this->view->due15 = $due15;
        $this->view->due30 = $due30;
        $this->view->due45 = $due45;
        $this->view->dueAbove = $dueAbove;


        //
        //Bookings Completed
        //
        
        // Last week
        $lastWeek = getTimePeriodByName('last_week');

        $filters = array();
        $filters['booking_start_between'] = $lastWeek['start'];
        $filters['booking_end_between'] = $lastWeek['end'];
        $filters['convert_status'] = 'invoice';
        $filters['invoice_type'] = 'all_active';
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $lastWeekSales = $modelBooking->total($filters, 'qoute');
        $lastWeekReceipts = $modelBooking->total($filters, 'paid_amount');
        $lastWeekDue = $lastWeekSales - $lastWeekReceipts;

        $this->view->lastWeekSales = $lastWeekSales;
        $this->view->lastWeekReceipts = $lastWeekReceipts;
        $this->view->lastWeekDue = $lastWeekDue;


        // last Month
        $lastMonth = getTimePeriodByName('last_month');

        $filters = array();
        $filters['booking_start_between'] = $lastMonth['start'];
        $filters['booking_end_between'] = $lastMonth['end'];
        $filters['convert_status'] = 'invoice';
        $filters['invoice_type'] = 'all_active';
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $lastMonthSales = $modelBooking->total($filters, 'qoute');
        $lastMonthReceipts = $modelBooking->total($filters, 'paid_amount');
        $lastMonthDue = $lastMonthSales - $lastMonthReceipts;

        $this->view->lastMonthSales = $lastMonthSales;
        $this->view->lastMonthReceipts = $lastMonthReceipts;
        $this->view->lastMonthDue = $lastMonthDue;


        // last quarter
        $lastQuarter = getTimePeriodByName('last_quarter');

        $filters = array();
        $filters['booking_start_between'] = $lastQuarter['start'];
        $filters['booking_end_between'] = $lastQuarter['end'];
        $filters['convert_status'] = 'invoice';
        $filters['invoice_type'] = 'all_active';
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $lastQuarterSales = $modelBooking->total($filters, 'qoute');
        $lastQuarterReceipts = $modelBooking->total($filters, 'paid_amount');
        $lastQuarterDue = $lastQuarterSales - $lastQuarterReceipts;

        $this->view->lastQuarterSales = $lastQuarterSales;
        $this->view->lastQuarterReceipts = $lastQuarterReceipts;
        $this->view->lastQuarterDue = $lastQuarterDue;


        // last Year
        $lastYear = getTimePeriodByName('last_year');

        $filters = array();
        $filters['booking_start_between'] = $lastYear['start'];
        $filters['booking_end_between'] = $lastYear['end'];
        $filters['convert_status'] = 'invoice';
        $filters['invoice_type'] = 'all_active';
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $lastYearSales = $modelBooking->total($filters, 'qoute');
        $lastYearReceipts = $modelBooking->total($filters, 'paid_amount');
        $lastYearDue = $lastYearSales - $lastYearReceipts;

        $this->view->lastYearSales = $lastYearSales;
        $this->view->lastYearReceipts = $lastYearReceipts;
        $this->view->lastYearDue = $lastYearDue;


        //Today
        $today = getTimePeriodByName('today');

        $filters = array();
        $filters['booking_start_between'] = $today['start'];
        $filters['convert_status'] = 'invoice';
        $filters['invoice_type'] = 'all_active';
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $todaySales = $modelBooking->total($filters, 'qoute');
        $todayReceipts = $modelBooking->total($filters, 'paid_amount');
        $todayDue = $todaySales - $todayReceipts;

        $this->view->todaySales = $todaySales;
        $this->view->todayReceipts = $todayReceipts;
        $this->view->todayDue = $todayDue;


        //this week
        $this_week = getTimePeriodByName('this_week');

        $filters = array();
        $filters['booking_start_between'] = $this_week['start'];
        $filters['convert_status'] = 'invoice';
        $filters['invoice_type'] = 'all_active';
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $weekSales = $modelBooking->total($filters, 'qoute');
        $weekReceipts = $modelBooking->total($filters, 'paid_amount');
        $weekDue = $weekSales - $weekReceipts;

        $this->view->weekSales = $weekSales;
        $this->view->weekReceipts = $weekReceipts;
        $this->view->weekDue = $weekDue;


        //this month
        $this_month = getTimePeriodByName('this_month');

        $filters = array();
        $filters['booking_start_between'] = $this_month['start'];
        $filters['convert_status'] = 'invoice';
        $filters['invoice_type'] = 'all_active';
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $monthSales = $modelBooking->total($filters, 'qoute');
        $monthReceipts = $modelBooking->total($filters, 'paid_amount');
        $monthDue = $monthSales - $monthReceipts;

        $this->view->monthSales = $monthSales;
        $this->view->monthReceipts = $monthReceipts;
        $this->view->monthDue = $monthDue;


        //this quarter
        $this_quarter = getTimePeriodByName('this_quarter');

        $filters = array();
        $filters['booking_start_between'] = $this_quarter['start'];
        $filters['convert_status'] = 'invoice';
        $filters['invoice_type'] = 'all_active';
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $quarterSales = $modelBooking->total($filters, 'qoute');
        $quarterReceipts = $modelBooking->total($filters, 'paid_amount');
        $quarterDue = $quarterSales - $quarterReceipts;

        $this->view->quarterSales = $quarterSales;
        $this->view->quarterReceipts = $quarterReceipts;
        $this->view->quarterDue = $quarterDue;


        //this year
        $this_year = getTimePeriodByName('this_year');

        $filters = array();
        $filters['booking_start_between'] = $this_year['start'];
        $filters['convert_status'] = 'invoice';
        $filters['invoice_type'] = 'all_active';
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }

        $yearSales = $modelBooking->total($filters, 'qoute');
        $yearReceipts = $modelBooking->total($filters, 'paid_amount');
        $yearDue = $yearSales - $yearReceipts;

        $this->view->yearSales = $yearSales;
        $this->view->yearReceipts = $yearReceipts;
        $this->view->yearDue = $yearDue;


	}
	
	
	public function inquiriesAction(){
		$this->view->sub_menu = 'inquiries';
	
        CheckAuth::checkPermission(array('dashboard'));

        //check login
        CheckAuth::checkLoggedIn();

        //
        // load model
        //
        $modelUser = new Model_User();
        $modelBooking = new Model_Booking();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelContractorInfo = new Model_ContractorInfo();
        $modelContractorEmployee = new Model_ContractorEmployee();

        //get logged user
        $loggedUser = CheckAuth::getLoggedUser();

        // role of logged user
        $role = CheckAuth::getRoleName();
        $this->view->role = $role;


        //logged user details
        $user = $modelUser->getById($loggedUser['user_id']);
        $this->view->user = $user;


        if (!$user) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }


        //#############################//
        //Contractor Info and employees//
        //#############################//
        $contractorInfo = $modelContractorInfo->getByContractorId($loggedUser['user_id']);
        $this->view->contractorInfo = $contractorInfo;

        $contractorEmployee = $modelContractorEmployee->getByContractorInfoId($contractorInfo['contractor_info_id']);
        $this->view->contractorEmployee = $contractorEmployee;
		
		
		
		$myDashboard = false;

        if (!CheckAuth::getDashboardStatusSession()) {
            if (CheckAuth::checkCredential(array('canSeeAllBookingStatistics'))) {
                CheckAuth::setDashboardStatusSession('all_dashboard');
            } else {
                CheckAuth::setDashboardStatusSession('my_dashboard');
            }
        }

        $dasboardStatus = CheckAuth::getDashboardStatusSession();
        if ($dasboardStatus == 'my_dashboard') {
            $myDashboard = true;
        }

        $filters = array();
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $totalBooking = $modelBooking->getBookingCount($filters);
        $this->view->totalBooking = $totalBooking;

        $modelBookingStatus = new Model_BookingStatus();
        $allBookingStatus = $modelBookingStatus->getAll();

        foreach ($allBookingStatus as &$bookingStatus) {
            $filters['status'] = $bookingStatus['booking_status_id'];
            $bookingStatus['total'] = $modelBooking->getBookingCount($filters);
        }

        $this->view->allBookingStatus = $allBookingStatus;

		
		
		
		
		 //
        //Get Count Service Accepted and Rejected
        //
        
        // Total services
        $filters = array();
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $totalService = $modelContractorServiceBooking->getServicesCount($filters);
        $this->view->totalService = $totalService;


        // services is_accepted
        $filters = array();
        $filters['is_accepted'] = true;
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $totalServiceAccepted = $modelContractorServiceBooking->getServicesCount($filters);
        $this->view->totalServiceAccepted = $totalServiceAccepted;
        $this->view->totalServiceAcceptedPer = $totalService ? number_format((($totalServiceAccepted * 100) / $totalService), 2) : 0;

        // services is_rejected
        $filters = array();
        $filters['is_rejected'] = true;
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $totalServiceRejected = $modelContractorServiceBooking->getServicesCount($filters);
        $this->view->totalServiceRejected = $totalServiceRejected;
        $this->view->totalServiceRejectedPer = $totalService ? number_format((($totalServiceRejected * 100) / $totalService), 2) : 0;

        // services NotAcceptedAndRejected

        $filters = array();
        $filters['NotAcceptedAndRejected'] = true;
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }
        $totalServiceNotAcceptedAndRejected = $modelContractorServiceBooking->getServicesCount($filters);
        $this->view->totalServiceNotAcceptedAndRejected = $totalServiceNotAcceptedAndRejected;
        $this->view->totalServiceNotAcceptedAndRejectedPer = $totalService ? number_format((($totalServiceNotAcceptedAndRejected * 100) / $totalService), 2) : 0;

		
		
		
		
		
		
		//
        // Numer of  inquires
        //
     
        $modelInquiry = new Model_Inquiry();
        $modelInquiryType = new Model_InquiryType();

        $inquiryTypes = $modelInquiryType->getAll();
        $this->view->inquiryTypes = $inquiryTypes;

        // inquires Last Year
		// last Year
        $lastYear = getTimePeriodByName('last_year');
				
		// last quarter
        $lastQuarter = getTimePeriodByName('last_quarter');
		
        $filters = array();
        $filters['startTimeRange'] = $lastYear['start'];
        $filters['endTimeRange'] = $lastYear['end'];
        if ($myDashboard) {
            $filters['user_id'] = $loggedUser['user_id'];
        }
        $allLastYearInquiries = $modelInquiry->getInquiryCount($filters);
        $this->view->allLastYearInquiries = $allLastYearInquiries;

        $lastYearInquiryTypes = $inquiryTypes;
        foreach ($lastYearInquiryTypes as &$lastYearInquiryType) {
            $filters['inquiry_type_id'] = $lastYearInquiryType['inquiry_type_id'];
            $lastYearInquiryType['count'] = $modelInquiry->getInquiryCount($filters);
        }
        $this->view->lastYearInquiryTypes = $lastYearInquiryTypes;


        // inquires Last Quarter

        $filters = array();
        $filters['startTimeRange'] = $lastQuarter['start'];
        $filters['endTimeRange'] = $lastQuarter['end'];
        if ($myDashboard) {
            $filters['user_id'] = $loggedUser['user_id'];
        }

        $allLastQuarterInquiries = $modelInquiry->getInquiryCount($filters);
        $this->view->allLastQuarterInquiries = $allLastQuarterInquiries;

        $lastQuarterInquiryTypes = $inquiryTypes;
        foreach ($lastQuarterInquiryTypes as &$lastQuarterInquiryType) {
            $filters['inquiry_type_id'] = $lastQuarterInquiryType['inquiry_type_id'];
            $lastQuarterInquiryType['count'] = $modelInquiry->getInquiryCount($filters);
        }
        $this->view->lastQuarterInquiryTypes = $lastQuarterInquiryTypes;

        // inquires Last Month
		// last Month
        $lastMonth = getTimePeriodByName('last_month');
		
		 // last quarter
        $lastQuarter = getTimePeriodByName('last_quarter');
		
        $filters = array();
        $filters['startTimeRange'] = $lastMonth['start'];
        $filters['endTimeRange'] = $lastMonth['end'];
        if ($myDashboard) {
            $filters['user_id'] = $loggedUser['user_id'];
        }

        $allLastMonthInquiries = $modelInquiry->getInquiryCount($filters);
        $this->view->allLastMonthInquiries = $allLastMonthInquiries;

        $lastMonthInquiryTypes = $inquiryTypes;
        foreach ($lastMonthInquiryTypes as &$lastMonthInquiryType) {
            $filters['inquiry_type_id'] = $lastMonthInquiryType['inquiry_type_id'];
            $lastMonthInquiryType['count'] = $modelInquiry->getInquiryCount($filters);
        }
        $this->view->lastMonthInquiryTypes = $lastMonthInquiryTypes;

        // inquires Last Week
		// Last week
        $lastWeek = getTimePeriodByName('last_week');
		
		
        $filters = array();
        $filters['startTimeRange'] = $lastWeek['start'];
        $filters['endTimeRange'] = $lastWeek['end'];
        if ($myDashboard) {
            $filters['user_id'] = $loggedUser['user_id'];
        }

        $allLastWeekInquiries = $modelInquiry->getInquiryCount($filters);
        $this->view->allLastWeekInquiries = $allLastWeekInquiries;

        $lastWeekInquiryTypes = $inquiryTypes;
        foreach ($lastWeekInquiryTypes as &$lastWeekInquiryType) {
            $filters['inquiry_type_id'] = $lastWeekInquiryType['inquiry_type_id'];
            $lastWeekInquiryType['count'] = $modelInquiry->getInquiryCount($filters);
        }
        $this->view->lastWeekInquiryTypes = $lastWeekInquiryTypes;


        // inquires  Today
		//Today
        $today = getTimePeriodByName('today');
		
        $filters = array();
        $filters['startTimeRange'] = $today['start'];
        $filters['endTimeRange'] = date('Y-m-d H:i:s', time());
        if ($myDashboard) {
            $filters['user_id'] = $loggedUser['user_id'];
        }

        $allTodayInquiries = $modelInquiry->getInquiryCount($filters);
        $this->view->allTodayInquiries = $allTodayInquiries;

        $todayInquiryTypes = $inquiryTypes;
        foreach ($todayInquiryTypes as &$todayInquiryType) {
            $filters['inquiry_type_id'] = $todayInquiryType['inquiry_type_id'];
            $todayInquiryType['count'] = $modelInquiry->getInquiryCount($filters);
        }
        $this->view->todayInquiryTypes = $todayInquiryTypes;

        //inquires Week
		//this week
        $this_week = getTimePeriodByName('this_week');
		
		
        $filters = array();
        $filters['startTimeRange'] = $this_week['start'];
        $filters['endTimeRange'] = date('Y-m-d H:i:s', time());
        if ($myDashboard) {
            $filters['user_id'] = $loggedUser['user_id'];
        }

        $allThisWeekInquiries = $modelInquiry->getInquiryCount($filters);
        $this->view->allThisWeekInquiries = $allThisWeekInquiries;

        $thisWeekInquiryTypes = $inquiryTypes;
        foreach ($thisWeekInquiryTypes as &$thisWeekInquiryType) {
            $filters['inquiry_type_id'] = $thisWeekInquiryType['inquiry_type_id'];
            $thisWeekInquiryType['count'] = $modelInquiry->getInquiryCount($filters);
        }
        $this->view->thisWeekInquiryTypes = $thisWeekInquiryTypes;


        // inquires Month
	//This month 		
		$this_month = getTimePeriodByName('this_month');
		
		

        $filters = array();
        $filters['startTimeRange'] = $this_month['start'];
        $filters['endTimeRange'] = date('Y-m-d H:i:s', time());
        if ($myDashboard) {
            $filters['user_id'] = $loggedUser['user_id'];
        }

        $allThisMonthInquiries = $modelInquiry->getInquiryCount($filters);
        $this->view->allThisMonthInquiries = $allThisMonthInquiries;

        $thisMonthInquiryTypes = $inquiryTypes;
        foreach ($thisMonthInquiryTypes as &$thisMonthInquiryType) {
            $filters['inquiry_type_id'] = $thisMonthInquiryType['inquiry_type_id'];
            $thisMonthInquiryType['count'] = $modelInquiry->getInquiryCount($filters);
        }
        $this->view->thisMonthInquiryTypes = $thisMonthInquiryTypes;

        //  inquires quarter
		// Future quarter
        $this_quarter = getTimePeriodByName('this_quarter');
		
		

        $filters = array();
        $filters['startTimeRange'] = $this_quarter['start'];
        $filters['endTimeRange'] = date('Y-m-d H:i:s', time());
        if ($myDashboard) {
            $filters['user_id'] = $loggedUser['user_id'];
        }

        $allThisQuarterInquiries = $modelInquiry->getInquiryCount($filters);
        $this->view->allThisQuarterInquiries = $allThisQuarterInquiries;

        $thisQuarterInquiryTypes = $inquiryTypes;
        foreach ($thisQuarterInquiryTypes as &$thisQuarterInquiryType) {
            $filters['inquiry_type_id'] = $thisQuarterInquiryType['inquiry_type_id'];
            $thisQuarterInquiryType['count'] = $modelInquiry->getInquiryCount($filters);
        }
        $this->view->thisQuarterInquiryTypes = $thisQuarterInquiryTypes;

        //  inquires This Year
		//This year 
        $this_year = getTimePeriodByName('this_year');

        $filters = array();
        $filters['startTimeRange'] = $this_year['start'];
        $filters['endTimeRange'] = date('Y-m-d H:i:s', time());
        if ($myDashboard) {
            $filters['user_id'] = $loggedUser['user_id'];
        }

        $allThisYearInquiries = $modelInquiry->getInquiryCount($filters);
        $this->view->allThisYearInquiries = $allThisYearInquiries;

        $thisYearInquiryTypes = $inquiryTypes;
        foreach ($thisYearInquiryTypes as &$thisYearInquiryType) {
            $filters['inquiry_type_id'] = $thisYearInquiryType['inquiry_type_id'];
            $thisYearInquiryType['count'] = $modelInquiry->getInquiryCount($filters);
        }
        $this->view->thisYearInquiryTypes = $thisYearInquiryTypes;
	
	}
	
	public function recentByConvertStatusAction() {


        CheckAuth::checkPermission(array('dashboard'));

        $convert_status = $this->request->getParam('convert_status');
        $orderBy = $this->request->getParam('sort', 'bok.created');
        $sortingMethod = $this->request->getParam('method', 'desc');

        // Load Model
        $modelBooking = new Model_Booking();

        $loggedUser = CheckAuth::getLoggedUser();
        $dasboardStatus = CheckAuth::getDashboardStatusSession();

        $filters = array();
        $filters['convert_status'] = $convert_status;

        if ($convert_status == 'estimate') {
            $orderBy = 'est.created';
            $filters['inner_join_estimate'] = true;
        }
        if ($convert_status == 'invoice') {
            $orderBy = 'bok.job_finish_time';
            // $filters['inner_join_invoice'] = true;
        }

        $myDashboard = false;

        if ($dasboardStatus == 'my_dashboard') {
            $myDashboard = true;
        }
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }

        // because pager sending by refrance
        $pager = null;

        $recent = $modelBooking->getAll($filters, "{$orderBy} {$sortingMethod}", $pager, 100);
        $modelBooking->fills($recent, array('estimate', 'invoice', 'status_discussion', 'not_accepted_or_rejected', 'not_accepted',));

        $this->view->recent = $recent;
        $this->view->convert_status = $convert_status;
    }

    public function recentByBookingStatusAction() {


        CheckAuth::checkPermission(array('dashboard'));
        $status_id = $this->request->getParam('status_id');

        // Load Model
        $modelBooking = new Model_Booking();

        $loggedUser = CheckAuth::getLoggedUser();
        $dasboardStatus = CheckAuth::getDashboardStatusSession();

        $filters = array();
        //$filters['status'] = $status_id;

        $myDashboard = false;

        if ($dasboardStatus == 'my_dashboard') {
            $myDashboard = true;
        }
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }

        // because pager sending by refrance
        $pager = null;

        // order by created status in bookink status history(bsh)
        $filters['order_by_status_history'] = $status_id;

        $recent = $modelBooking->getAll($filters, 'bsh.max_created DESC', $pager, 100);
        $modelBooking->fills($recent, array('estimate', 'invoice', 'status_discussion'));

        $this->view->recent = $recent;
    }

    public function recentDiscussionsAction() {

        CheckAuth::checkPermission(array('dashboard'));

        $modelBookingDiscussion = new Model_BookingDiscussion();

        $loggedUser = CheckAuth::getLoggedUser();
        $dasboardStatus = CheckAuth::getDashboardStatusSession();

        $myDashboard = false;
        $filters = array();

        if ($dasboardStatus == 'my_dashboard') {
            $myDashboard = true;
        }
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }

        $recentDiscussion = $modelBookingDiscussion->getRecentDiscussion($filters);
        $this->view->recentDiscussion = $recentDiscussion;
    }

    public function recentHistoryAction() {

        CheckAuth::checkPermission(array('dashboard'));

        $modelBookingLog = new Model_BookingLog();

        $loggedUser = CheckAuth::getLoggedUser();
        $dasboardStatus = CheckAuth::getDashboardStatusSession();

        $myDashboard = false;
        $filters = array();

        if ($dasboardStatus == 'my_dashboard') {
            $myDashboard = true;
        }
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }

        //History
       
        $bookingLogs = $modelBookingLog->getRecentBookingLog($filters);
        $this->view->bookingLogs = $bookingLogs;
    }

    public function recentComplaintsAction() {

        CheckAuth::checkPermission(array('dashboard'));

        $modelComplaint = new Model_Complaint();

        $loggedUser = CheckAuth::getLoggedUser();
        $dasboardStatus = CheckAuth::getDashboardStatusSession();

        $myDashboard = false;
        $filters = array();

        if ($dasboardStatus == 'my_dashboard') {
            $myDashboard = true;
        }
        if ($myDashboard) {
            $filters['created_by'] = $loggedUser['user_id'];
        }

        $recentComplaints = $modelComplaint->getRecentComplaint($filters);
        $modelComplaint->fills($recentComplaints, array('booking'));

        $this->view->recentComplaints = $recentComplaints;
    }

    public function acceptServicesAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('acceptOrRejectServices'));


        $bookingId = $this->request->getParam('bookingId', 0);
        $allAccepted = $this->request->getParam('accepted', array());

        $modelBooking = new Model_Booking();
        if (!$modelBooking->checkIfCanSeeBookingWithOutTimePeriod($bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect($this->router->assemble(array(), 'booking'));
        }
        if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect($this->router->assemble(array(), 'booking'));
        }

        //
        //get logged user
        //
        $loggedUser = CheckAuth::getLoggedUser();

        //
        // load model
        //
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelServices = new Model_Services();

        $notAcceptedBookings = $modelContractorServiceBooking->getNotAcceptedBookings($bookingId, $loggedUser['user_id']);
        foreach ($notAcceptedBookings as &$notAcceptedBooking) {
            $notAcceptedBooking['service'] = $modelServices->getById($notAcceptedBooking['service_id']);
        }

        $this->view->notAcceptedBookings = $notAcceptedBookings;
        $this->view->bookingId = $bookingId;

        if ($this->request->isPost()) {

            if ($allAccepted) {

                $services = array();

                foreach ($allAccepted as $acceptedId) {
                    $modelContractorServiceBooking->updateById($acceptedId, array('is_accepted' => 1, 'is_rejected' => 0));

                    $contractorServiceBooking = $modelContractorServiceBooking->getById($acceptedId);

                    $services[] = array(
                        'service_id' => $contractorServiceBooking['service_id'],
                        'clone' => $contractorServiceBooking['clone']
                    );
                }
                $modelGoogleCalendarEvent = new Model_GoogleCalendarEvent();
                $modelGoogleCalendarEvent->sendBookingToGmailAcc($bookingId, $services);


                $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Accept Services Successfully."));

                echo 1;
                exit;
            } else {
                $this->view->errorMsg = 'you have to choose at least one service';
            }
        }

        echo $this->view->render('dashboard/accept-services.phtml');
        exit;
    }

    public function rejectServicesAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('acceptOrRejectServices'));

        $bookingId = $this->request->getParam('bookingId', 0);
        $allRejected = $this->request->getParam('rejected', array());

        $modelBooking = new Model_Booking();
        if (!$modelBooking->checkIfCanSeeBookingWithOutTimePeriod($bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect($this->router->assemble(array(), 'booking'));
        }
        if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect($this->router->assemble(array(), 'booking'));
        }

        //
        //get logged user
        //
        $loggedUser = CheckAuth::getLoggedUser();

        //
        // load model
        //
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelServices = new Model_Services();

        //
        //get all Not Rejected Bookings
        //
        $notRejectedBookings = $modelContractorServiceBooking->getNotRejectedOrAcceptedBookings($bookingId, $loggedUser['user_id']);
        foreach ($notRejectedBookings as &$notRejectedBooking) {
            $notRejectedBooking['service'] = $modelServices->getById($notRejectedBooking['service_id']);
        }

        $this->view->notRejectedBookings = $notRejectedBookings;
        $this->view->bookingId = $bookingId;

        if ($this->request->isPost()) {
            if ($allRejected) {

                foreach ($allRejected as $rejectedId) {
                    $modelContractorServiceBooking->updateById($rejectedId, array('is_rejected' => 1, 'is_accepted' => 0));
                }


                $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Reject Services Successfully."));

                echo 1;
                exit;
            } else {
                $this->view->errorMsg = 'you have to choose at least one service';
            }
        }

        echo $this->view->render('dashboard/reject-services.phtml');
        exit;
    }

    public function acceptAdminServicesAction() {

        CheckAuth::checkPermission(array('canAcceptAdminServices', 'canSeeAllBooking'));

        $bookingId = $this->request->getParam('bookingId', 0);
        $allAccepted = $this->request->getParam('accepted', array());

        if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect($this->router->assemble(array(), 'booking'));
        }

        //
        // load model
        //
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelServices = new Model_Services();

        //
        //get all Not Accepted Bookings
        //
        $notAcceptedBookings = $modelContractorServiceBooking->getAll(array('booking_id' => $bookingId, 'is_accepted' => 'not_accepted'));
        foreach ($notAcceptedBookings as &$notAcceptedBooking) {
            $notAcceptedBooking['service'] = $modelServices->getById($notAcceptedBooking['service_id']);
        }

        $this->view->notAcceptedBookings = $notAcceptedBookings;
        $this->view->bookingId = $bookingId;

        if ($this->request->isPost()) {

            if ($allAccepted) {

                $services = array();

                foreach ($allAccepted as $acceptedId) {
                    $modelContractorServiceBooking->updateById($acceptedId, array('is_accepted' => 1, 'is_rejected' => 0));
                    $contractorServiceBooking = $modelContractorServiceBooking->getById($acceptedId);

                    $services[] = array(
                        'service_id' => $contractorServiceBooking['service_id'],
                        'clone' => $contractorServiceBooking['clone']
                    );
                }
                $this->sendBookingToContracrtorGmailAcc($bookingId, $services);


                $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Accept Services Successfully."));

                echo 1;
                exit;
            } else {
                $this->view->errorMsg = 'you have to choose at least one service';
            }
        }

        echo $this->view->render('dashboard/accept-admin-services.phtml');
        exit;
    }
	
	
	
	public function rejectAdminServicesAction() {

        CheckAuth::checkPermission(array('canAcceptAdminServices', 'canSeeAllBooking'));

        $bookingId = $this->request->getParam('bookingId', 0);
        $allRejected = $this->request->getParam('rejected', array());

        if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect($this->router->assemble(array(), 'booking'));
        }

        //
        // load model
        //
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelServices = new Model_Services();

        //
        //get all Accepted Bookings
        //
        $acceptedBookings = $modelContractorServiceBooking->getAll(array('booking_id' => $bookingId, 'is_accepted' => 'accepted'));
        foreach ($acceptedBookings as &$acceptedBooking) {
            $acceptedBooking['service'] = $modelServices->getById($acceptedBooking['service_id']);
        }

        $this->view->acceptedBookings = $acceptedBookings;
        $this->view->bookingId = $bookingId;

        if ($this->request->isPost()) {

            if ($allRejected) {

                $services = array();

                foreach ($allRejected as $rejectedId) {
                    $modelContractorServiceBooking->updateById($rejectedId, array('is_accepted' => 0,'is_rejected' => 1));
                    $contractorServiceBooking = $modelContractorServiceBooking->getById($rejectedId);

                    $services[] = array(
                        'service_id' => $contractorServiceBooking['service_id'],
                        'clone' => $contractorServiceBooking['clone']
                    );
                }
                $this->sendBookingToContracrtorGmailAcc($bookingId, $services);


                $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Reject Services Successfully."));

                echo 1;
                exit;
            } else {
                $this->view->errorMsg = 'you have to choose at least one service';
            }
        }

        echo $this->view->render('dashboard/reject-admin-services.phtml');
        exit;
    }

    public function resendAdminServicesAction() {

        CheckAuth::checkPermission(array('canAcceptAdminServices', 'canSeeAllBooking'));

        $bookingId = $this->request->getParam('bookingId', 0);
        $allAccepted = $this->request->getParam('accepted', array());

        if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect($this->router->assemble(array(), 'booking'));
        }

        //
        // load model
        //
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelServices = new Model_Services();

        //
        //get all Not Accepted Bookings
        //
        $filters = array('booking_id' => $bookingId);
        $notAcceptedBookings = $modelContractorServiceBooking->getAll($filters);
        foreach ($notAcceptedBookings as &$notAcceptedBooking) {
            $notAcceptedBooking['service'] = $modelServices->getById($notAcceptedBooking['service_id']);
        }

        $this->view->notAcceptedBookings = $notAcceptedBookings;
        $this->view->bookingId = $bookingId;

        if ($this->request->isPost()) {

            if ($allAccepted) {

                $services = array();

                foreach ($allAccepted as $acceptedId) {
                    $modelContractorServiceBooking->updateById($acceptedId, array('is_accepted' => 1, 'is_rejected' => 0));
                    $contractorServiceBooking = $modelContractorServiceBooking->getById($acceptedId);

                    $services[] = array(
                        'service_id' => $contractorServiceBooking['service_id'],
                        'clone' => $contractorServiceBooking['clone']
                    );
                }
                $this->sendBookingToContracrtorGmailAcc($bookingId, $services);
                $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Accept Services Successfully."));

                echo 1;
                exit;
            } else {
                $this->view->errorMsg = 'you have to choose at least one service';
            }
        }

        echo $this->view->render('dashboard/resend-admin-services.phtml');
        exit;
    }

    public function sendBookingToContracrtorGmailAcc($bookingId, $services) {

        //
        // load model
        //
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();

        //
        // get the gmail account contractor
        //
        $contractorServices = array();
        foreach ($services AS $service) {

            $ContractorServiceBooking = $modelContractorServiceBooking->getByBookingAndServiceAndClone($bookingId, $service['service_id'], $service['clone']);
            $contractor_id = isset($ContractorServiceBooking['contractor_id']) ? $ContractorServiceBooking['contractor_id'] : 0;

            $contractorServices[$contractor_id][] = $service;
        }

        $modelGoogleCalendarEvent = new Model_GoogleCalendarEvent();
        foreach ($contractorServices AS $contractorId => $contractorService) {
            $modelGoogleCalendarEvent->sendBookingToGmailAcc($bookingId, $contractorService, $contractorId);
        }
    }

    public function showStatusDiscussionAction() {

        $discussionId = $this->request->getParam('id');
        $statusId = $this->request->getParam('status_id');

        // Load Model
        $modelBookingDiscussion = new Model_BookingDiscussion();
        $modelBookingStatus = new Model_BookingStatus();

        $statusDiscussion = $modelBookingDiscussion->getById($discussionId);
        $bookingStatus = $modelBookingStatus->getById($statusId);

        $this->view->bookingStatus = $bookingStatus;
        $this->view->statusDiscussion = $statusDiscussion;
        echo $this->view->render('dashboard/show-status-discussion.phtml');
        exit;
    }

    public function logUserActivityAction() {

        //get request parameters
        $orderBy = $this->request->getParam('sort', 'created');
        $sortingMethod = $this->request->getParam('method', 'desc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        // Load Model
        $modelLogUser = new Model_LogUser();

        $modelInquiry = new Model_Inquiry();
        $modelBooking = new Model_Booking();
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelBookingEstimate = new Model_BookingEstimate();
        $modelComplaint = new Model_Complaint();
        $modelCustomer = new Model_Customer();

        $modelInquiryDiscussion = new Model_InquiryDiscussion();
        $modelBookingDiscussion = new Model_BookingDiscussion();
        $modelInvoiceDiscussion = new Model_InvoiceDiscussion();
        $modelEstimateDiscussion = new Model_EstimateDiscussion();
        $modelComplaintDiscussion = new Model_ComplaintDiscussion();

        $modelInquiryLabel = new Model_InquiryLabel();
        $modelBookingLabel = new Model_BookingLabel();
        $modelInvoiceLabel = new Model_InvoiceLabel();
        $modelEstimateLabel = new Model_EstimateLabel();

        $modelBookingAttachment = new Model_BookingAttachment();
        $modelEmailLog = new Model_EmailLog();

        $modelInquiryReminder = new Model_InquiryReminder();
        $modelBookingReminder = new Model_BookingReminder();
        $modelBookingContactHistory = new Model_BookingContactHistory();

        $modelRefund = new Model_Refund();
        $modelPayment = new Model_Payment();

        $modelReport = new Model_Report();

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        $loggedUser = CheckAuth::getLoggedUser();
        $dasboardStatus = CheckAuth::getDashboardStatusSession();

        $myDashboard = false;
        if ($dasboardStatus == 'my_dashboard') {
            $myDashboard = true;
        }

        if ($myDashboard && empty($filters['user_id'])) {
            $filters['user_id'] = $loggedUser['user_id'];
        }

        if (!CheckAuth::checkCredential(array('loggedUsersActivity')) && $myDashboard) {
            $filters['user_id'] = $loggedUser['user_id'];
        }

        if (empty($filters['from'])) {
            //this week
            $this_week = getTimePeriodByName('this_week');
            $filters['from'] = $this_week['start'];
        }


        //init pager and articles model object
        $pager = new Model_Pager();
        $pager->perPage = 50;
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        $logUserEvents = $modelLogUser->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);
        foreach ($logUserEvents as &$logUserEvent) {

            switch ($logUserEvent['item_type']) {
                case 'inquiry':
                    $inquiry = $modelInquiry->getById($logUserEvent['item_id']);
                    if ($inquiry) {
                        $logUserEvent['event_on'] = $inquiry['inquiry_num'];

                        $logUserEvent['action'] = $this->router->assemble(array('id' => $logUserEvent['item_id']), 'inquiryView');
                    } else {
                        $logUserEvent['action'] = 'javascript:;';
                    }
                    $logUserEvent['event_name'] = $logUserEvent['item_type'];
                    break;

                case 'booking':
                    $booking = $modelBooking->getById($logUserEvent['item_id']);
                    if ($booking) {
                        $logUserEvent['event_on'] = $booking['booking_num'];

                        $logUserEvent['action'] = $this->router->assemble(array('id' => $logUserEvent['item_id']), 'bookingView');
                    } else {
                        $logUserEvent['action'] = 'javascript:;';
                    }
                    $logUserEvent['event_name'] = $logUserEvent['item_type'];
                    break;

                case 'booking_estimate':
                    $estimate = $modelBookingEstimate->getById($logUserEvent['item_id']);
                    if ($estimate) {
                        $logUserEvent['event_on'] = $estimate['estimate_num'];

                        $logUserEvent['action'] = $this->router->assemble(array('id' => $logUserEvent['item_id']), 'estimateView');
                    } else {
                        $logUserEvent['action'] = 'javascript:;';
                    }
                    $logUserEvent['event_name'] = 'estimate';
                    break;

                case 'booking_invoice':
                    $invoice = $modelBookingInvoice->getById($logUserEvent['item_id']);
                    if ($invoice) {
                        $logUserEvent['event_on'] = $invoice['invoice_num'];

                        $logUserEvent['action'] = $this->router->assemble(array('id' => $logUserEvent['item_id']), 'invoiceView');
                    } else {
                        $logUserEvent['action'] = 'javascript:;';
                    }
                    $logUserEvent['event_name'] = 'invoice';
                    break;

                case 'customer':
                    $customer = $modelCustomer->getById($logUserEvent['item_id']);
                    if ($customer) {
                        $logUserEvent['event_on'] = get_customer_name($customer);

                        $logUserEvent['action'] = $this->router->assemble(array('id' => $logUserEvent['item_id']), 'customerView');
                    } else {
                        $logUserEvent['action'] = 'javascript:;';
                    }
                    $logUserEvent['event_name'] = $logUserEvent['item_type'];
                    break;

                case 'complaint':
                    $complaint = $modelComplaint->getById($logUserEvent['item_id']);
                    if ($complaint) {
                        $logUserEvent['event_on'] = $complaint['complaint_num'];

                        $logUserEvent['action'] = $this->router->assemble(array('id' => $logUserEvent['item_id']), 'complaintView');
                    } else {
                        $logUserEvent['action'] = 'javascript:;';
                    }
                    $logUserEvent['event_name'] = $logUserEvent['item_type'];
                    break;

                case 'inquiry_discussion':
                    $inquiryDiscussion = $modelInquiryDiscussion->getById($logUserEvent['item_id']);
                    if ($inquiryDiscussion) {
                        $inquiry = $modelInquiry->getById($inquiryDiscussion['inquiry_id']);
                        if ($inquiry) {
                            $logUserEvent['event_on'] = $inquiry['inquiry_num'];
                        }
                        $logUserEvent['action'] = 'javascript:;';
                        $logUserEvent['onclick'] = "showPopupWindow('{$this->router->assemble(array('id' => $inquiryDiscussion['inquiry_id']), 'inquiryDisscussion')}','470','700','Inquiry Discussion');";
                    } else {
                        $logUserEvent['action'] = 'javascript:;';
                    }
                    $logUserEvent['event_name'] = $logUserEvent['item_type'];
                    break;

                case 'booking_discussion':
                    $bookingDiscussion = $modelBookingDiscussion->getById($logUserEvent['item_id']);
                    if ($bookingDiscussion) {
                        $booking = $modelBooking->getById($bookingDiscussion['booking_id']);
                        if ($booking) {
                            $logUserEvent['event_on'] = $booking['booking_num'];
                        }
                        $logUserEvent['action'] = 'javascript:;';
                        $logUserEvent['onclick'] = "showPopupWindow('{$this->router->assemble(array('id' => $bookingDiscussion['booking_id']), 'bookingDiscussion')}','470','700','Booking Discussion');";
                    } else {
                        $logUserEvent['action'] = 'javascript:;';
                    }
                    $logUserEvent['event_name'] = $logUserEvent['item_type'];
                    break;

                case 'estimate_discussion':
                    $estimateDiscussion = $modelEstimateDiscussion->getById($logUserEvent['item_id']);
                    if ($estimateDiscussion) {
                        $estimate = $modelBookingEstimate->getById($estimateDiscussion['estimate_id']);
                        if ($estimate) {
                            $logUserEvent['event_on'] = $estimate['estimate_num'];
                        }
                        $logUserEvent['action'] = 'javascript:;';
                        $logUserEvent['onclick'] = "showPopupWindow('{$this->router->assemble(array('id' => $estimateDiscussion['estimate_id']), 'estimateDisscussion')}','470','700','Estimate Discussion');";
                    } else {
                        $logUserEvent['action'] = 'javascript:;';
                    }
                    $logUserEvent['event_name'] = $logUserEvent['item_type'];
                    break;

                case 'invoice_discussion':
                    $invoiceDiscussion = $modelInvoiceDiscussion->getById($logUserEvent['item_id']);
                    if ($invoiceDiscussion) {
                        $invoice = $modelBookingInvoice->getById($invoiceDiscussion['invoice_id']);
                        if ($invoice) {
                            $logUserEvent['event_on'] = $invoice['invoice_num'];
                        }
                        $logUserEvent['action'] = 'javascript:;';
                        $logUserEvent['onclick'] = "showPopupWindow('{$this->router->assemble(array('id' => $invoiceDiscussion['invoice_id']), 'invoiceDisscussion')}','470','700','Invoice Discussion');";
                    } else {
                        $logUserEvent['action'] = 'javascript:;';
                    }
                    $logUserEvent['event_name'] = $logUserEvent['item_type'];
                    break;

                case 'complaint_discussion':
                    $complaintDiscussion = $modelComplaintDiscussion->getById($logUserEvent['item_id']);
                    if ($complaintDiscussion) {
                        $complaint = $modelComplaint->getById($complaintDiscussion['complaint_id']);
                        if ($complaint) {
                            $logUserEvent['event_on'] = $complaint['complaint_num'];
                        }
                        $logUserEvent['action'] = 'javascript:;';
                        $logUserEvent['onclick'] = "showPopupWindow('{$this->router->assemble(array('id' => $complaintDiscussion['complaint_id'],'process' =>'send'), 'complaintDisscussion')}','470','700','Complaint Discussion');";
                    } else {
                        $logUserEvent['action'] = 'javascript:;';
                    }
                    $logUserEvent['event_name'] = $logUserEvent['item_type'];
                    break;

                case 'inquiry_label':
                    $inquiryLabel = $modelInquiryLabel->getById($logUserEvent['item_id']);
                    if ($inquiryLabel) {
                        $inquiry = $modelInquiry->getById($inquiryLabel['inquiry_id']);
                        if ($inquiry) {
                            $logUserEvent['event_on'] = $inquiry['inquiry_num'];
                        }
                        $logUserEvent['action'] = $this->router->assemble(array('id' => $inquiryLabel['inquiry_id']), 'inquiryView');
                    } else {
                        $logUserEvent['action'] = 'javascript:;';
                    }
                    $logUserEvent['event_name'] = $logUserEvent['item_type'];
                    break;

                case 'booking_label':
                    $bookingLabel = $modelBookingLabel->getById($logUserEvent['item_id']);
                    if ($bookingLabel) {
                        $booking = $modelBooking->getById($bookingLabel['booking_id']);
                        if ($booking) {
                            $logUserEvent['event_on'] = $booking['booking_num'];
                        }
                        $logUserEvent['action'] = $this->router->assemble(array('id' => $bookingLabel['booking_id']), 'bookingView');
                    } else {
                        $logUserEvent['action'] = 'javascript:;';
                    }
                    $logUserEvent['event_name'] = $logUserEvent['item_type'];
                    break;

                case 'estimate_label':
                    $estimateLabel = $modelEstimateLabel->getById($logUserEvent['item_id']);
                    if ($estimateLabel) {
                        $estimate = $modelBookingEstimate->getById($estimateLabel['estimate_id']);
                        if ($estimate) {
                            $logUserEvent['event_on'] = $estimate['estimate_num'];
                        }
                        $logUserEvent['action'] = $this->router->assemble(array('id' => $estimateLabel['estimate_id']), 'estimateView');
                    } else {
                        $logUserEvent['action'] = 'javascript:;';
                    }
                    $logUserEvent['event_name'] = $logUserEvent['item_type'];
                    break;

                case 'invoice_label':
                    $invoiceLabel = $modelInvoiceLabel->getById($logUserEvent['item_id']);
                    if ($invoiceLabel) {
                        $invoice = $modelBookingInvoice->getById($invoiceLabel['invoice_id']);
                        if ($invoice) {
                            $logUserEvent['event_on'] = $invoice['invoice_num'];
                        }
                        $logUserEvent['action'] = $this->router->assemble(array('id' => $invoiceLabel['invoice_id']), 'invoiceView');
                    } else {
                        $logUserEvent['action'] = 'javascript:;';
                    }
                    $logUserEvent['event_name'] = $logUserEvent['item_type'];
                    break;

                case 'inquiry_reminder':
                    $inquiryReminder = $modelInquiryReminder->getById($logUserEvent['item_id']);
                    if ($inquiryReminder) {
                        $inquiry = $modelInquiry->getById($inquiryReminder['inquiry_id']);
                        if ($inquiry) {
                            $logUserEvent['event_on'] = $inquiry['inquiry_num'];
                        }
                        $logUserEvent['action'] = 'javascript:;';
                        $logUserEvent['onclick'] = "showPopupWindow('{$this->router->assemble(array('id' => $logUserEvent['item_id']), 'inquiryReminderView')}','180','400','Inquiry Contact History');";
                    } else {
                        $logUserEvent['action'] = 'javascript:;';
                    }
                    $logUserEvent['event_name'] = 'Inquiry Contact History';
                    break;

                case 'booking_reminder':
                    $bookingReminder = $modelBookingReminder->getById($logUserEvent['item_id']);
                    if ($bookingReminder) {
                        $booking = $modelBooking->getById($bookingReminder['booking_id']);
                        if ($booking) {
                            $logUserEvent['event_on'] = $booking['booking_num'];
                        }
                        $logUserEvent['action'] = 'javascript:;';
                        $logUserEvent['onclick'] = "showPopupWindow('{$this->router->assemble(array('id' => $logUserEvent['item_id']), 'bookingReminderView')}','180','400','Booking Confirmation');";
                    } else {
                        $logUserEvent['action'] = 'javascript:;';
                    }
                    $logUserEvent['event_name'] = 'Booking Confirmation';
                    break;

                case 'booking_contact_history':
                    $bookingContactHistory = $modelBookingContactHistory->getById($logUserEvent['item_id']);
                    if ($bookingContactHistory) {
                        $estimate = $modelBookingEstimate->getByBookingId($bookingContactHistory['booking_id']);
                        if ($estimate) {
                            $logUserEvent['event_on'] = $estimate['estimate_num'];
                        }
                        $logUserEvent['action'] = 'javascript:;';
                        $logUserEvent['onclick'] = "showPopupWindow('{$this->router->assemble(array('id' => $logUserEvent['item_id']), 'bookingContactHistoryView')}','180','400','Estimate Contact History');";
                    } else {
                        $logUserEvent['action'] = 'javascript:;';
                    }
                    $logUserEvent['event_name'] = 'Estimate Contact History';
                    break;

                case 'booking_attachment':
                    $bookingAttachment = $modelBookingAttachment->getById($logUserEvent['item_id']);

                    if (isset($bookingAttachment['booking_id']) && $bookingAttachment['booking_id']) {
                        $booking = $modelBooking->getById($bookingAttachment['booking_id']);
                        $logUserEvent['event_on'] = $booking['booking_num'];

                        $logUserEvent['action'] = $this->router->assemble(array('id' => $bookingAttachment['booking_id']), 'bookingAttachment');
                        $logUserEvent['event_name'] = 'Booking Attachment';
                    } elseif (isset($bookingAttachment['inquiry_id']) && $bookingAttachment['inquiry_id']) {
                        $inquiry = $modelInquiry->getById($bookingAttachment['inquiry_id']);
                        $logUserEvent['event_on'] = $inquiry['inquiry_num'];

                        $logUserEvent['action'] = $this->router->assemble(array('id' => $bookingAttachment['inquiry_id']), 'inquiryAttachment');
                        $logUserEvent['event_name'] = 'Inquiry Attachment';
                    } else {
                        $logUserEvent['action'] = $this->router->assemble(array('id' => $logUserEvent['item_id']), 'bookingFileDownload');
                        $logUserEvent['event_name'] = 'attachment';
                    }
                    break;

                case 'refund':
                    $refund = $modelRefund->getById($logUserEvent['item_id']);
                    if ($refund) {
                        $invoice = $modelBookingInvoice->getByBookingId($refund['booking_id']);
                        if ($invoice) {
                            $logUserEvent['event_on'] = $invoice['invoice_num'];
                        }
                        $logUserEvent['action'] = $this->router->assemble(array('booking_id' => $refund['booking_id']), 'bookingRefundList');
                    } else {
                        $logUserEvent['action'] = 'javascript:;';
                    }
                    $logUserEvent['event_name'] = $logUserEvent['item_type'];
                    break;

                case 'payment':
                    $payment = $modelPayment->getById($logUserEvent['item_id']);
                    if ($payment) {
                        $invoice = $modelBookingInvoice->getByBookingId($payment['booking_id']);
                        if ($invoice) {
                            $logUserEvent['event_on'] = $invoice['invoice_num'];
                        }
                        $logUserEvent['action'] = $this->router->assemble(array('booking_id' => $payment['booking_id']), 'bookingPaymentList');
                    } else {
                        $logUserEvent['action'] = 'javascript:;';
                    }
                    $logUserEvent['event_name'] = $logUserEvent['item_type'];
                    break;

                case 'email':

                    $emailLog = $modelEmailLog->getById($logUserEvent['item_id']);

                    switch ($emailLog['type']) {
                        case 'common':
                            $logUserEvent['event_on'] = $emailLog['to'];
                            break;
                        case 'invoice':
                            $invoice = $modelBookingInvoice->getById($emailLog['reference_id']);
                            if ($invoice) {
                                $logUserEvent['event_on'] = $invoice['invoice_num'];
                            } else {
                                $logUserEvent['event_on'] = $emailLog['to'];
                            }
                            break;
                        case 'estimate':
                            $estimate = $modelBookingEstimate->getById($emailLog['reference_id']);
                            if ($estimate) {
                                $logUserEvent['event_on'] = $estimate['estimate_num'];
                            } else {
                                $logUserEvent['event_on'] = $emailLog['to'];
                            }
                            break;
                        case 'complaint':
                            $complaint = $modelComplaint->getById($emailLog['reference_id']);
                            if ($complaint) {
                                $logUserEvent['event_on'] = $complaint['complaint_num'];
                            } else {
                                $logUserEvent['event_on'] = $emailLog['to'];
                            }
                            break;
                        case 'booking':
                            $booking = $modelBooking->getById($emailLog['reference_id']);
                            if ($booking) {
                                $logUserEvent['event_on'] = $booking['booking_num'];
                            } else {
                                $logUserEvent['event_on'] = $emailLog['to'];
                            }
                            break;
                        case 'inquiry':
                            $inquiry = $modelInquiry->getById($emailLog['reference_id']);
                            if ($inquiry) {
                                $logUserEvent['event_on'] = $inquiry['inquiry_num'];
                            } else {
                                $logUserEvent['event_on'] = $emailLog['to'];
                            }
                            break;
                        case 'customer':
                            $customer = $modelCustomer->getById($emailLog['reference_id']);
                            if ($customer) {
                                $logUserEvent['event_on'] = get_customer_name($customer);
                            } else {
                                $logUserEvent['event_on'] = $emailLog['to'];
                            }
                            break;
                    }

                    $logUserEvent['action'] = 'javascript:;';
                    $logUserEvent['onclick'] = "showPopupWindow('{$this->router->assemble(array('id' => $logUserEvent['item_id']), 'emailDetails')}','470','800','Email');";
                    $logUserEvent['event_name'] = $logUserEvent['item_type'];
                    break;

                case 'report':
                    $report = $modelReport->getById($logUserEvent['item_id']);
                    if ($report) {
                        $logUserEvent['event_on'] = ucwords(str_replace('_', ' ', $report['report_type']));

                        $logUserEvent['action'] = $this->router->assemble(array('id' => $logUserEvent['item_id']), 'savedReportDownload');
                    } else {
                        $logUserEvent['action'] = 'javascript:;';
                    }
                    $logUserEvent['event_name'] = $logUserEvent['item_type'];
                    break;
				case 'login':
                    $logUserEvent['event_on'] = 'login';
                    $logUserEvent['action'] = 'javascript:;';
					$logUserEvent['event_name'] = 'login';
                    break;
            }
        }

        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->logUserEvents = $logUserEvents;
        $this->view->myDashboard = $myDashboard;
        $this->view->from = !empty($filters['from']) ? date('Y-m-d', strtotime($filters['from'])) : '';
        $this->view->to = !empty($filters['to']) ? date('Y-m-d', strtotime($filters['to'])) : '';
        $this->view->user_id = !empty($filters['user_id']) ? $filters['user_id'] : '';
    }

    public function recentEmailsAction() {

        CheckAuth::checkPermission(array('dashboard'));
        CheckAuth::checkPermission(array('emailDetails'));


        // load model
        $modelEmailLog = new Model_EmailLog();

        $loggedUser = CheckAuth::getLoggedUser();
        $dasboardStatus = CheckAuth::getDashboardStatusSession();

        $orderBy = $this->request->getParam('sort', 'created');
        $sortingMethod = $this->request->getParam('method', 'desc');
        $currentPage = $this->request->getParam('page', 1);

        $myDashboard = false;
        $filters = array();

        if ($dasboardStatus == 'my_dashboard') {
            $myDashboard = true;
        }
        if ($myDashboard) {
            $filters['user_id'] = $loggedUser['user_id'];
        }


        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = 30;
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        $bookingEmails = $modelEmailLog->getAll($filters, "{$orderBy} {$sortingMethod}", $pager, 100);
        $modelEmailLog->fills($bookingEmails, array('booking'));

        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->bookingEmails = $bookingEmails;
    }

	
	public function cronjobHistoryAction() {

        CheckAuth::checkPermission(array('dashboard'));
        //CheckAuth::checkPermission(array('emailDetails'));


        // load model
        $model_cronjobHistory = new Model_CronjobHistory();

        $loggedUser = CheckAuth::getLoggedUser();
        $dasboardStatus = CheckAuth::getDashboardStatusSession();

        $orderBy = $this->request->getParam('sort', 'run_time');
        $sortingMethod = $this->request->getParam('method', 'desc');
        $currentPage = $this->request->getParam('page', 1);

        $myDashboard = false;
        $filters = array();

        


        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = 30;
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        $cronjobHistory = $model_cronjobHistory->getAll($filters, "{$orderBy} {$sortingMethod}", $pager, 100);
       // $modelEmailLog->fills($bookingEmails, array('booking'));

        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->cronjobHistory = $cronjobHistory;
    }
	
	
    public function replayRecentEmailsAction() {

        CheckAuth::checkPermission(array('dashboard'));
        CheckAuth::checkPermission(array('emailDetails'));

        $pdf_attachment = $this->request->getParam('pdf_attachment', 0);
        $emailLogId = $this->request->getParam('id', 0);

        $modelEmailLog = new Model_EmailLog();
        $emailLog = $modelEmailLog->getById($emailLogId);

        $pdf_num = '';
        switch ($emailLog['type']) {
            case 'invoice':
                $modelBookingInvoice = new Model_BookingInvoice();
                $invoice = $modelBookingInvoice->getById($emailLog['reference_id']);
                if ($invoice) {
                    $pdf_num = $invoice['invoice_num'];
                }
                break;
            case 'estimate':
                $modelBookingEstimate = new Model_BookingEstimate();
                $estimate = $modelBookingEstimate->getById($emailLog['reference_id']);
                if ($estimate) {
                    $pdf_num = $estimate['estimate_num'];
                }
                break;
            case 'booking':
                $modelBooking = new Model_Booking();
                $booking = $modelBooking->getById($emailLog['reference_id']);
                if ($booking) {
                    $pdf_num = $booking['booking_num'];
                }
                break;
        }

        $body = $emailLog['body'];
        $subject = $emailLog['subject'];
        $to = $emailLog['to'];

        if ($this->request->isPost()) {
            $to = $this->request->getParam('to');
            $cc = $this->request->getParam('cc');
            $subject = $this->request->getParam('subject');
            $body = $this->request->getParam('body');

            $params = array(
                'to' => $to,
                'cc' => $cc,
                'body' => $body,
                'subject' => $subject,
                'layout' => 'empty_email_template'
            );

            if (!empty($pdf_attachment)) {
                switch ($emailLog['type']) {
                    case 'invoice':
                        $modelBookingInvoice = new Model_BookingInvoice();
                        $viewParam = $modelBookingInvoice->getInvoiceViewParam($emailLog['reference_id']);
                        $view = new Zend_View();
                        $view->setScriptPath(APPLICATION_PATH . '/modules/invoices/views/scripts/index');
                        $view->bookingServices = $viewParam['bookingServices'];
                        $view->thisBookingServices = $viewParam['thisBookingServices'];
                        $view->priceArray = $viewParam['priceArray'];
                        $view->invoice = $viewParam['invoice'];
                        $view->customer = $viewParam['customer'];
                        $view->isAccepted = $viewParam['isAccepted'];
                        $view->booking = $viewParam['invoice']['booking'];
                        $bodyInvoice = $view->render('invoice.phtml');
                        // Create pdf
                        $pdfPath = createPdfPath();
                        $destination = $pdfPath['fullDir'] . $viewParam['invoice']['invoice_num'] . '.pdf';
                        wkhtmltopdf($bodyInvoice, $destination);
                        $params['attachment'] = $destination;
                        break;
                    case 'estimate':
                        $modelBookingEstimate = new Model_BookingEstimate();
                        $viewParam = $modelBookingEstimate->getEstimateViewParam($emailLog['reference_id']);
                        $view = new Zend_View();
                        $view->setScriptPath(APPLICATION_PATH . '/modules/estimates/views/scripts/index');
                        $view->bookingServices = $viewParam['bookingServices'];
                        $view->thisBookingServices = $viewParam['thisBookingServices'];
                        $view->priceArray = $viewParam['priceArray'];
                        $view->estimate = $viewParam['estimate'];
                        $bodyEstimate = $view->render('estimate.phtml');
                        // Create pdf
                        $pdfPath = createPdfPath();
                        $destination = $pdfPath['fullDir'] . $viewParam['estimate']['estimate_num'] . '.pdf';
                        wkhtmltopdf($bodyEstimate, $destination);
                        $params['attachment'] = $destination;
                        break;
                    case 'booking':
                        $modelBooking = new Model_Booking();
                        $viewParam = $modelBooking->getBookingViewParam($emailLog['reference_id']);
                        $view = new Zend_View();
                        $view->setScriptPath(APPLICATION_PATH . '/modules/booking/views/scripts/index');
                        $view->booking = $viewParam['booking'];
                        $view->bookingServices = $viewParam['bookingServices'];
                        $view->thisBookingServices = $viewParam['thisBookingServices'];
                        $view->priceArray = $viewParam['priceArray'];
                        $bodyBooking = $view->render('booking-customer.phtml');
                        // Create pdf
                        $pdfPath = createPdfPath();
                        $destination = $pdfPath['fullDir'] . $viewParam['booking']['booking_num'] . '.pdf';
                        wkhtmltopdf($bodyBooking, $destination);
                        $params['attachment'] = $destination;
                        break;
                }
            }

            $error_mesages = array();
            if (EmailNotification::validation($params, $error_mesages)) {

                // Send Email
                $success = EmailNotification::sendEmail($params, '', array(), array('reference_id' => $emailLog['reference_id'], 'type' => $emailLog['type']));

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Email sent successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Email not sent"));
                }

                echo 1;
                exit;
            }
            $this->view->error_mesages = $error_mesages;
        }

        $this->view->to = $to;
        $this->view->subject = $subject;
        $this->view->body = $body;
        $this->view->email_log_id = $emailLogId;
        $this->view->pdf_num = $pdf_num;

        echo $this->view->render('dashboard/replay-recent-emails.phtml');
        exit;
    }

    public function downloadEmailPdfFileAction() {

        //
        // get params 
        //
        $logId = $this->request->getParam('id', 0);

        //
        // load models
        //
        $modelEmailLog = new Model_EmailLog();

        //
        // geting data
        //
        $log = $modelEmailLog->getById($logId);

        //
        // validation
        //
        if (!$log) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Email not exist"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }


        $filename = $log['attachment_path'];


        header("Pragma: public");
        header("Expires: 0");
        header("Pragma: no-cache");
        header("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header('Content-disposition: attachment; filename=' . basename($filename));
        header("Content-Type: application/pdf");
        header("Content-Transfer-Encoding: binary");
        header('Content-Length: ' . filesize($filename));
        @readfile($filename);
        exit(0);
    }

}

