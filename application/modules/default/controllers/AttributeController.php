<?php

class AttributeController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
    }

	
    public function drowAttributeAction() {
        $service_id = $this->request->getParam('service_id');
        $clone = $this->request->getParam('clone', 0);
        $without_pricing = $this->request->getParam('without_pricing', 0);
		$type = $this->request->getParam('type');
		
        $serviceAttribute_obj = new Model_ServiceAttribute();

        $serviceAttribute = $serviceAttribute_obj->getAttributeByServiceId($service_id, $without_pricing);
		//var_dump($serviceAttribute);
		//exit;
        $this->view->serviceAttribute = $serviceAttribute;
        $this->view->clone = $clone;
		$this->view->type = $type;

        echo $this->view->render('attribute/drowAttribute.phtml');
        exit;
    }

    public function countServicePriceAction() {
        $serviceId = $this->request->getParam('service_id');
        $clone = $this->request->getParam('clone', 0);
        $from_session = $this->request->getParam('from_session', 0);
        $view_confirm = $this->request->getParam('view_confirm');
        $reset = $this->request->getParam('reset');
		
        $serviceObj = new Model_Services();
        $attributesObj = new Model_Attributes();
		$serviceAttribute_obj = new Model_ServiceAttribute();
		$serviceDiscount_obj = new Model_ServiceDiscount();
        $attributeListValueObj = new Model_AttributeListValue();
		
		$with_draw = $this->request->getParam('with_draw');
        $just_price = $this->request->getParam('just_price');
		
		if((isset($view_confirm) && $view_confirm)){
		    $priceAttribute = $serviceAttribute_obj->getByAttributeName('price',$serviceId);
			$base_price = $priceAttribute['default_value'];
			$attribute = $attributesObj->getByVariableName('price');
            $attributeId = isset($attribute['attribute_id']) && $attribute['attribute_id'] ? $attribute['attribute_id'] : 0;              
            $price_value = $this->request->getParam("attribute_{$serviceId}{$attributeId}" . ($clone ? '_' . $clone : ''), 0);
			$result = array('base_price'=>$base_price,'current_price'=>$price_value);
			echo json_encode($result);
			exit;
		}
		
		if((isset($from_session) && $from_session) && (isset($with_draw) && $with_draw)){
		    
			 if($clone){
			    $index = 'services_'.$serviceId.'_'.$clone;
			 }else{
			   $index = 'services_'.$serviceId;
			 }
		     $current_service = $_SESSION['new_booking'][$index];
			 $pice_with_gst = $current_service['price'] * (1 + get_config('gst_tax')); 
			 $this->view->attributesValues = $current_service['attributes'];
			 $this->view->totalPrice =  $pice_with_gst;
			 $this->view->service =  $current_service['service'];
			 $this->view->unit_price =  $current_service['unit_price'];
			 $this->view->clone = $current_service['clone'];
			 
			
			 $result['data'] = $this->view->render('attribute/drawService.phtml');
			 $result['price'] = $current_service['price'];
			 $result['min_price'] = $current_service['service']['min_price'];
			 echo json_encode($result);
             exit;
			 
		}

        //get service by id to get price_equasion
        $service = $serviceObj->getById($serviceId);
		
        $Quantity = 0;
        if (preg_match_all('#\[(.*?)\]#', $service['price_equasion'], $matches)) {
            $param = array();
            foreach ($matches[1] as $attributeVariableName) {
                $attribute = $attributesObj->getByVariableName($attributeVariableName);
                $attributeId = isset($attribute['attribute_id']) && $attribute['attribute_id'] ? $attribute['attribute_id'] : 0;
                
                $attribute_value = $this->request->getParam("attribute_{$serviceId}{$attributeId}" . ($clone ? '_' . $clone : ''), 0);
								
                if ($attribute['is_list']) {
                    $attributeListValue = $attributeListValueObj->getById($attribute_value);
                    $param[$attributeVariableName] = isset($attributeListValue['unit_price']) && $attributeListValue['unit_price'] ? $attributeListValue['unit_price'] : 0;
                } else {
                    $param[$attributeVariableName] = $attribute_value;
                }
				
				if($attribute['attribute_name'] == 'Quantity'){
				 $Quantity = $attribute_value;
				}
            }
			
			
            $equasion = $service['price_equasion'];
            //build getPrice function
			createPriceEquasionFunction('getPrice', $equasion);
			if($reset && isset($reset)){
			 $priceAttribute = $serviceAttribute_obj->getByAttributeName('price',$serviceId);
			 $param['price'] = $priceAttribute['default_value'];
			}
			
            $service_price = getPrice($param);
            $new_unit_price = $param['price'];
			$priceAttribute = $attributesObj->getByVariableName('price');
			//$serviceDiscounts = $serviceDiscount_obj->getAll(array('service_id'=>$serviceId),'min_size_range asc');
			$ServiceDiscountRange = $serviceDiscount_obj->getByServiceIdAndQunatity($serviceId,$Quantity);
			$maxServiceDiscount = $serviceDiscount_obj->getMaxDiscount($serviceId);
			if(isset($ServiceDiscountRange) && !empty($ServiceDiscountRange)){
			   $qntyRange = $ServiceDiscountRange['max_size_range'] - $ServiceDiscountRange['min_size_range'];
			   $discountRange = $ServiceDiscountRange['max_discount_range'] - $ServiceDiscountRange['min_discount_range'];
			   $unit_discount = $discountRange / $qntyRange ;
			   $units = $Quantity - $ServiceDiscountRange['min_size_range'];
			   $discount_percent = ($units * $unit_discount) + $ServiceDiscountRange['min_discount_range'];
			   $discount = $Quantity * ($discount_percent / 100);
			   $new_unit_price = $param['price'] - ($discount_percent / 100);
			   $service_price = getPrice($param) - $discount;
			   
			}else if($Quantity > $maxServiceDiscount['max_size']){
			  $new_unit_price = $param['price'] - ($maxServiceDiscount['max_discount'] / 100);
			  $discount = $Quantity * ($maxServiceDiscount['max_discount'] / 100);
			  $service_price = getPrice($param) - $discount;			  
			}
			
			
			
			$extra_charge = $this->countAttributesPrice($Quantity,$serviceId);
			
			
			if(isset($with_draw) && $with_draw){
			   
			    $serviceAttributes = $serviceAttribute_obj->getAttributeByServiceId($serviceId, 0);
				
				foreach($serviceAttributes as $key=>$serviceAttribute){
				 $attributeId = isset($serviceAttribute['attribute_id']) && $serviceAttribute['attribute_id'] ? $serviceAttribute['attribute_id'] : 0;
				 $serviceAttributeValue = $this->request->getParam("attribute_{$serviceId}{$attributeId}" . ($clone ? '_' . $clone : ''), 0);
				 $serviceAttributes[$key]['value'] = $serviceAttributeValue; 
				}
			  $customer_comment = $this->request->getParam("customer_comment",'');
			  $price = $service_price;			  
              
			  
			  
			    $_SESSION['new_booking']['current_tab'] = 'Quote-details';
			    $_SESSION['new_booking']['prev_tab'] = 'all-services'; 
			  
			 /* if ($price < $service['min_price']) {
                    $price = $service['min_price'];
                }*/
             $price_with_extra = $price + $extra_charge;			
			 $pice_with_gst = $price_with_extra * (1 + get_config('gst_tax')); 
			 $this->view->attributesValues = $serviceAttributes;
			 $this->view->totalPrice =  $pice_with_gst;
			 $this->view->extra_charge =  $extra_charge;
			 $this->view->unit_price =  $new_unit_price;
			 $this->view->service =  $service;
			 $this->view->clone = $clone;
			 $this->view->customer_comment = $customer_comment;
			 if($clone){
			  $service_index = 'services_'.$serviceId.'_'.$clone;
			 }else{
			  $service_index = 'services_'.$serviceId;
			 }
			 $service_object = array('service'=>$service ,
 			 'attributes'=>$serviceAttributes ,
			 'clone'=>$clone,
			 'price'=>$price,
			 'totalPrice'=>$pice_with_gst,
			 'unit_price' => $new_unit_price,
			 'price_attribute_id' => $priceAttribute['attribute_id'],
			 'extra_charge'=>$extra_charge);
			 $_SESSION['new_booking'][$service_index] = $service_object;
			 
			 
			 $result['data'] = $this->view->render('attribute/drawService.phtml');
			 $result['price'] = $price;
			 $result['extra_charge'] = $extra_charge;
			 $result['min_price'] = $service['min_price'];
			 $result['unit_price'] = $new_unit_price;
			 $result['price_attribute_id'] = $priceAttribute['attribute_id'];
			 echo json_encode($result);
             exit;
			}else{
             $result['price'] = $service_price + $extra_charge;		
             $result['unit_price'] = $new_unit_price;		
             $result['price_attribute_id'] = $priceAttribute['attribute_id'];		
             echo json_encode($result);
             exit;
			}
			
			
        } else {
            echo 0;
            exit;
        }
    }
	
	public function countAttributesPrice($quantity,$serviceId){
	  
	  //$serviceId = $this->request->getParam('service_id');
      //$clone = $this->request->getParam('clone', 0);
	  //$attributesObj = new Model_Attributes();
	  $serviceAttribute_obj = new Model_ServiceAttribute();
	  //$attribute = $attributesObj->getByVariableName('quantity');
	  $total_extraCharge = 0;
	  if(isset($quantity) && $quantity){
	   $serviceAttributes = $serviceAttribute_obj->getAttributeByServiceId($serviceId, 0);
	   foreach($serviceAttributes as $serviceAttribute){
	    // get unit price and discount for attribute
        
		$modelServiceAttributeDiscount = new Model_ServiceAttributeDiscount();
        $serviceAttributeDiscount = $modelServiceAttributeDiscount->getByAttributeIdAndServiceIdAndQunatity($serviceAttribute['attribute_id'],$serviceId,$quantity);
		$maxServiceAttributeDiscount = $modelServiceAttributeDiscount->getMaxDiscount($serviceAttribute['attribute_id'],$serviceId);
		$unit_price = $serviceAttribute['unit_price'];
         if(!empty($serviceAttributeDiscount) && $serviceAttributeDiscount){
			$qntyRange = $serviceAttributeDiscount['max_size_range'] - $serviceAttributeDiscount['min_size_range'];
			$discountRange = $serviceAttributeDiscount['max_discount_range'] - $serviceAttributeDiscount['min_discount_range'];
			$unit_discount = $discountRange / $qntyRange ;
			$units = $quantity - $serviceAttributeDiscount['min_size_range'];
			$discount_percent = ($units * $unit_discount) + $serviceAttributeDiscount['min_discount_range'];							
			$discount = $quantity * ($discount_percent / 100);
			$price = ($quantity * $unit_price) - $discount;
			$total_extraCharge = $total_extraCharge + $price;
		}else if($quantity > $maxServiceAttributeDiscount['max_size']){
			  $discount = $quantity * ($maxServiceAttributeDiscount['max_discount'] / 100);
			  $price = ($quantity * $unit_price) - $discount;
              $total_extraCharge = $total_extraCharge + $price;			  
		 }
	   }
	  }
	  return $total_extraCharge;
	  
	 
	}
	
	
	public function getAttributeValuesAction(){
	  
	  
	 $attributeId = $this->request->getParam('attribute_id');
	 $attributeValueId = $this->request->getParam('attribute_value_id');
	 $modelAttributes = new Model_Attributes();
	 $modelAttributeType = new Model_AttributeType();
	 $attributeListValueAttachmentObj = new Model_AttributeListValueAttachment();
	 
	 if(isset($attributeValueId) && !empty($attributeValueId)){
	    
		$attribute_value_images = $attributeListValueAttachmentObj->getByAttributeValueId($attributeValueId,false,true);
        $attributeValueImages = array();
         if($attribute_value_images){
			$parts = explode("public",$attribute_value_images[0]['path']);
            $defualt_image = $parts['1'];                               
          }else{
            $defualt_image = '/pic/attribute_pics/default_image.png';
           }
		 
		 $result = array('default_image'=>$defualt_image);
		 echo json_encode($result);
		 exit;
	 }
	 
	 $attribute = $modelAttributes->getById($attributeId);
     $attributeType = $modelAttributeType->getById($attribute['attribute_type_id']);
	 
	 $is_list = $attributeType['is_list'];
	 $listOptions = array();
    if ($attributeType['is_list']) {
        $modelAttributeListValue = new Model_AttributeListValue();
        $attributeListValue = $modelAttributeListValue->getByAttributeId($attribute['attribute_id']);

        if ($attributeListValue) {
            foreach ($attributeListValue as $attributeValue) {
			    $attribute_value_images = $attributeListValueAttachmentObj->getByAttributeValueId($attributeValue['attribute_value_id'],false,true);
                            $attributeValueImages = array();
                            if($attribute_value_images){
							 $parts = explode("public",$attribute_value_images[0]['path']);
                             $defualt_image = $parts['1'];                               
                           }else{
                             $defualt_image = '/pic/attribute_pics/default_image.png';
                           }
                $listOptions[$attributeValue['attribute_value_id']] = array('value'=>$attributeValue['attribute_value'] , 'default_image'=>$defualt_image);
            }
        }
	
		$result['listOptions'] = $listOptions;
    }
	
	$result['is_list'] = $is_list;
	$result['attribute'] = $attribute;
	
	echo json_encode($result);
	exit;
	
	}

}

?>
