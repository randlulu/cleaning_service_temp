<?php

class Model_RejectBookingQuestionOptions extends Zend_Db_Table_Abstract {

    protected $_name = 'reject_booking_question_options';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from(array('rbqo' => $this->_name));
        $select->joinInner(array('rbq' => 'reject_booking_question'), 'rbq.id =rbqo.question_id', array());
        $select->order($order);

        $filters['company_id'] = CheckAuth::getCompanySession();

        if ($filters) {
            if (!empty($filters['company_id'])) {
                $select->where("rbq.company_id = {$filters['company_id']}");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }


	public function updateById($id, array $data){
		$id = (int) $id;
        return parent::update($data, "id = {$id}");
	
	}

    /**
     * delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean
     */
    public function deleteById($id){
        $id = (int) $id;
        return parent::delete("id= {$id}");

    }
   
	public function insert(array $data) {  
	
        return parent::insert($data);
    }

    public function getByQuestionId($reject_booking_question_id) {
        $reject_booking_question_id = (int) $reject_booking_question_id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("question_id= '{$reject_booking_question_id}'");

        return $this->getAdapter()->fetchAll($select);
    }
	
	 public function getById($option_id) {
        $option_id = (int) $option_id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id= '{$option_id}'");
		$select->order('id ASC');
		/*if($_SERVER['REMOTE_ADDR'] == '176.106.46.142'){
			echo 'By Islam '.$select->__toString();
		}*/
        return $this->getAdapter()->fetchRow($select);
    }

    public function deleteByQuestionId($reject_booking_question_id) {
        $reject_booking_question_id = (int) $reject_booking_question_id;
        return parent::delete("question_id= {$reject_booking_question_id}");
    }


}