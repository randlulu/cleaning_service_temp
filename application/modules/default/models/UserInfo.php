<?php

class Model_UserInfo extends Zend_Db_Table_Abstract {

    protected $_name = 'user_info';
    
     /**
     *update table row to the sent id
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "user_info_id = '{$id}'");
    }

     /**
     *delete table row according to the assigned $id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("user_info_id = '{$id}'");
    }
    
    /**
     *get table rows according to the assigned $id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("user_info_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }
    /**
     *get table rows according to the assigned user $id
     * 
     * @param int $id
     * @return array 
     */
    
    public function getByUserId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("user_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

}