<?php

class Model_BookingAddress extends Zend_Db_Table_Abstract {

    protected $_name = 'booking_address';

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateById($id, $data,$log_id = 0) {
        $id = (int) $id;
        $success = parent::update($data, "booking_address_id = '{$id}'");

        /**
         * get all record and insert it in log
         */
        $modelBookingAddressLog = new Model_BookingAddressLog();
        $modelBookingAddressLog->addBookingAddressLog($id,0,$log_id);

        return $success;
    }

    public function insert(array $data) {
        $id = parent::insert($data);

        /**
         * get all record and insert it in log
         */
        $modelBookingAddressLog = new Model_BookingAddressLog();
        $modelBookingAddressLog->addBookingAddressLog($id);

        return $id;
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_address_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the assigned BookingId
     * 
     * @param int $id
     * @return array
     */
    public function getByBookingId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function getByBookingIdWithOutLatAndLon($id) {

        $bookingAddress = $this->getByBookingId($id);

        $address = array();
        if ($bookingAddress) {
            $address['unit_lot_number'] = $bookingAddress['unit_lot_number'];
            $address['street_number'] = $bookingAddress['street_number'];
            $address['street_address'] = $bookingAddress['street_address'];
            $address['suburb'] = $bookingAddress['suburb'];
            $address['state'] = $bookingAddress['state'];
            $address['postcode'] = $bookingAddress['postcode'];
            $address['po_box'] = $bookingAddress['po_box'];
            $address['booking_id'] = $bookingAddress['booking_id'];
        }

        return $address;
    }

    /**
     * update table row according to the assigned  bookingId
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateByBookingId($bookingId, $data) {
        $bookingId = (int) $bookingId;
        $bookingAddress = $this->getByBookingId($bookingId);
        if ($bookingAddress) {
            $this->updateById($bookingAddress['booking_address_id'], $data);
        }
    }

    /**
     * delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("booking_address_id= '{$id}'");
    }

    public function getDistanceByTwoAddress($address1, $address2, $unit = 'k') {
		
        $geocode1 = $this->getLatAndLon($address1);
        $lat1 = $geocode1['lat'];
        $lon1 = $geocode1['lon'];

        $geocode2 = $this->getLatAndLon($address2);
        $lat2 = $geocode2['lat'];
        $lon2 = $geocode2['lon'];

        $MAP_OBJECT = new GoogleMapAPI();
        return $MAP_OBJECT->geoGetDistance($lat1, $lon1, $lat2, $lon2, $unit);
    }

    public function getLatAndLon($address) {
        if (is_array($address)) {
            $line_address = get_line_address($address);
        } else {
            $line_address = $address;
        }

				
        $MAP_OBJECT = new GoogleMapAPI();
        $geocode = $MAP_OBJECT->getGeocode($line_address);

        $lat = (float) $geocode['lat'] ? $geocode['lat'] : 0;
        $lon = (float) $geocode['lon'] ? $geocode['lon'] : 0;

        return array('lat' => $lat, 'lon' => $lon);
    }

    public function getAllRelatedAddress($address, $filters = array(), $order = "distance ASC", $distance = 0, $unit = 'k') {

        $geocode = $this->getLatAndLon($address);

        $lat = $geocode['lat'];
        $lon = $geocode['lon'];

        switch ($unit) {
            case 'k':
                $distance_qry = "((((acos(sin(({$lat}*pi()/180)) * sin((`lat`*pi()/180))+cos(({$lat}*pi()/180)) * cos((`lat`*pi()/180)) * cos((({$lon}- `lon`)*pi()/180))))*180/pi())*60*1.1515*1.609344)) AS distance";
                break;
            case 'm':
                $distance_qry = "((((acos(sin(({$lat}*pi()/180)) * sin((`lat`*pi()/180))+cos(({$lat}*pi()/180)) * cos((`lat`*pi()/180)) * cos((({$lon}- `lon`)*pi()/180))))*180/pi())*60*1.1515)) AS distance";
                break;
            default :
                $distance_qry = "((((acos(sin(({$lat}*pi()/180)) * sin((`lat`*pi()/180))+cos(({$lat}*pi()/180)) * cos((`lat`*pi()/180)) * cos((({$lon}- `lon`)*pi()/180))))*180/pi())*60*1.1515*1.609344)) AS distance";
                break;
        }

        $select = $this->getAdapter()->select();
        $select->from(array('rba' => $this->_name), array('*', $distance_qry));
        $select->distinct();
		
        $joinInner = array();
        $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'bok.booking_id = rba.booking_id', 'cols' => array('status_id'));
        $joinInner['contractor_service_booking'] = array('name' => array('csb' => 'contractor_service_booking'), 'cond' => 'bok.booking_id = csb.booking_id', 'cols' => '');
        $joinInner['user'] = array('name' => array('u' => 'user'), 'cond' => 'csb.contractor_id = u.user_id', 'cols' => '');
		$joinInner['status'] = array('name' => array('bok_stat' => 'booking_status'), 'cond' => 'bok_stat.booking_status_id = bok.status_id', 'cols' => array('status_name'=>'name'));

        $loggedUser = CheckAuth::getLoggedUser();
        $filters['company_id'] = CheckAuth::getCompanySession();

        $select->where('bok.is_deleted = 0');
        //$select->where("bok.status_id != {$cancelledID}");
        $select->where('u.active = "TRUE"');
		
		
        if (!CheckAuth::checkCredential(array('canSeeAllBooking'))) {
            if (CheckAuth::checkCredential(array('canSeeOnlyHisBooking'))) {
                if (CheckAuth::checkCredential(array('canSeeAssignedBooking'))) {
                    $select->where("bok.created_by = {$loggedUser['user_id']} OR csb.contractor_id = {$loggedUser['user_id']}");
                } else {
                    $select->where("bok.created_by = {$loggedUser['user_id']}");
                }
            } else {
                $select->where("bok.created_by = {$loggedUser['user_id']}");
            }
        }

        if ($filters) {
            if (!empty($filters['company_id'])) {
                $company_id = (int) $filters['company_id'];
                $select->where("bok.company_id = {$company_id}");
            }
            if (!empty($filters['booking_start'])) {
                $bookingStart = strtotime(trim($filters['booking_start']));
                $bookingStart = date('Y-m-d H:i:s', $bookingStart);
                $bookingStart = $this->getAdapter()->quote(trim($bookingStart));

                $select->where("bok.booking_start >= {$bookingStart}");
            }
            if (!empty($filters['booking_end'])) {
                $bookingEnd = strtotime(trim($filters['booking_end']));
                $bookingEnd = date('Y-m-d H:i:s', $bookingEnd);
                $bookingEnd = $this->getAdapter()->quote(trim($bookingEnd));

                $select->where("bok.booking_end <= {$bookingEnd}");
            }
            if (!empty($filters['city_id'])) {
                $city_id = (int) $filters['city_id'];
                $select->where("bok.city_id = {$city_id}");
            }
            if (!empty($filters['not_booking_id'])) {
                $not_booking_id = (int) $filters['not_booking_id'];
                $select->where("bok.booking_id != {$not_booking_id}");
            }
            if (!empty($filters['booking_id'])) {
                $booking_id = (int) $filters['booking_id'];
                $select->where("bok.booking_id = {$booking_id}");
            }
        }

        if ($joinInner) {
            foreach ($joinInner as $inner) {
                $select->joinInner($inner['name'], $inner['cond'], $inner['cols']);
            }
        }

        if ($order) {
            $select->order($order);
        }

        $sql = $this->getAdapter()->select();
        $sql->from(array('bad' => $select));
		$modelBookingStatus = new Model_BookingStatus();
		$statusIds = $modelBookingStatus->getstatusIdByNearestBookingFeatureAndCompanyId();
		//$select->where("bok.status_id in(?)",$statusIds);
		$sql->where('bad.status_id IN(' . implode(', ', $statusIds) . ')');
		
        if ($distance) {
            $sql->where("bad.distance <= {$distance}");
        }
		
		//echo $sql->__toString();
		
        return $this->getAdapter()->fetchAll($sql);
    }

}