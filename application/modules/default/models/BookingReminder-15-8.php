<?php

class Model_BookingReminder extends Zend_Db_Table_Abstract {

    protected $_name = 'booking_reminder';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from(array('el' => $this->_name));
        $select->order($order);


        if ($filters) {
            if (!empty($filters['booking_id'])) {
                $bookingId = (int) $filters['booking_id'];
                $select->where("el.booking_id = '{$bookingId}'");
            }
			
			if(!empty($filters['user_id'])){
			 $userId = (int) $filters['user_id'];
             $select->where("el.user_id = '{$userId}'");
			}
			
			if(!empty($filters['role_id'])){
			 $roleId = (int) $filters['role_id'];
             $select->where("el.user_role = '{$roleId}'");
			}
        }


        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }
        return $this->getAdapter()->fetchAll($select);
    }

    /*public function getContactHistory($bookingId) {
        $bookingId = (int) $bookingId;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_id = '{$bookingId}'");
        $select->order('created DESC');
        $select->limit(4);

        return $this->getAdapter()->fetchAll($select);
    }*/

    public function getContactHistoryWithCustomer($bookingId,$is_count = 0) {
        $bookingId = (int) $bookingId;
        $select = $this->getAdapter()->select();
		if($is_count){
		 $select->from($this->_name ,array('count'=>'count(DISTINCT(id))'));
		}else{
		 $select->from($this->_name);
		}
        $select->where("booking_id = '{$bookingId}'");
        $select->where("type = 'Customer'");
        $select->order('created DESC');

        if($is_count){
		 return $this->getAdapter()->fetchOne($select);
		}else{
		  return $this->getAdapter()->fetchAll($select);
		}
    }

    public function getContactHistoryWithTechnician($bookingId,$is_count = 0) {
        $bookingId = (int) $bookingId;
        $select = $this->getAdapter()->select();
        if($is_count){
		 $select->from($this->_name ,array('count'=>'count(DISTINCT(id))'));
		}else{
		 $select->from($this->_name);
		}  
        $select->where("booking_id = '{$bookingId}'");
        $select->where("type = 'Technician'");
        $select->order('created DESC');

         if($is_count){
		  return $this->getAdapter()->fetchOne($select);
		}else{
		  return $this->getAdapter()->fetchAll($select);
		}
    }

    /**
     * update table rows according to the assigned id and data
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;

        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'changed');

        return parent::update($data, "id = '{$id}'");
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getByBookingId($bookingId) {
        $bookingId = (int) $bookingId;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_id = '{$bookingId}'");

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;

        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'deleted');

        return parent::delete("id = '{$id}'");
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function getReminderType() {
        $query = "SHOW COLUMNS FROM {$this->_name} LIKE 'type'";
        $stmt = $this->getAdapter()->query($query);
        $row = $stmt->fetch();
        $row = $row['Type'];
        $regex = "/'(.*?)'/";
        preg_match_all($regex, $row, $enum_array);
        $enum_fields = $enum_array[1];
        foreach ($enum_fields as $key => $value) {
            $enums[$value] = $value;
        }
        return $enums;
    }

    public function insert(array $data) {

        $id = parent::insert($data);

        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'added');

        return $id;
    }
    
     public function getLastConfirmationByBookingId($booking_id){

        $booking_id = (int) $booking_id;
        $select = $this->getAdapter()->select();
        $select->from(array('br'=>$this->_name));
        $select->where("booking_id = '{$booking_id}'");
        $select->joinInner(array('u'=>'user') , 'u.user_id = br.user_id' , 'username');
        $select->order('id DESC');
        $select->limit(1);

        return $this->getAdapter()->fetchRow($select);
    
    }
	
	public function getLastConfirmationByBookingIdForAllUsers($booking_id){

        $booking_id = (int) $booking_id;
        $select = $this->getAdapter()->select();
        $select->from(array('br'=>$this->_name) , array('br.user_id','br.user_role','br.booking_id','br.created'));
        $select->where("booking_id = '{$booking_id}'"); 
        $select->order('id DESC');
        $select->limit(1);

        return $this->getAdapter()->fetchRow($select);
    
    } 
	
	 public function getContactHistory($bookingId,$limit = '4') {
        $bookingId = (int) $bookingId;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_id = '{$bookingId}'");
        $select->order('created DESC');
		if($limit != 'All'){
        $select->limit(4);
        }
        return $this->getAdapter()->fetchAll($select);
    }

}
