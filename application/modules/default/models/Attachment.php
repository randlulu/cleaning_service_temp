<?php

class Model_Attachment extends Zend_Db_Table_Abstract {

    protected $_name = 'attachment';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($order = null, &$pager = null, $filters = array(), $limit = 0, $perPage = 0, $currentPage = 0) {

        $select = $this->getAdapter()->select();
        $select->from(array('a' => $this->_name));
        $select->where("a.is_deleted = 0");
        $select->order($order);

        if ($filters) {
            if (!empty($filters['type'])) {
                $typeFilter = $this->getAdapter()->quote('%' . $filters['type'] . '%');
                $type = $filters['type'];
                $item_id = (int) $filters['itemid'];
                if ($type == 'licence') {
                    $select->joinInner(array('cia' => 'contractor_info_attachment'), 'a.attachment_id=cia.attachment_id');
                    $select->where("cia.type LIKE {$typeFilter}");
                    $select->where("cia.contractor_info_id = {$item_id}");
                } else if ($type == 'vehicle') {
                    $select->joinInner(array('va' => 'vehicle_attachment'), 'a.attachment_id=va.attachment_id');
                    $select->where("va.vehicle_id = {$item_id}");
                } else if ($type == 'equipment') {
                    $select->joinInner(array('ea' => 'declaration_of_equipment_attachment'), 'a.attachment_id=ea.attachment_id');
                    $select->where("ea.equipment_id = {$item_id}");
                } else if ($type == 'insurance') {
                    $select->joinInner(array('cia' => 'contractor_insurance_attachment'), 'a.attachment_id=cia.attachment_id');
                    $select->where("cia.contractor_insurance_id = {$item_id}");
                } else if ($type == 'payment'){
                     $select->joinInner(array('cia' => 'contractor_info_attachment'), 'a.attachment_id=cia.attachment_id');
                    $select->where("cia.type LIKE {$typeFilter}");
                    $select->where("cia.contractor_info_id = {$item_id}");
                }
            }

            if (!empty($filters['item_type'])) {
                $item_type = $this->getAdapter()->quote('image%');
                $select->where("a.type LIKE {$item_type}");
            }
        }




        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        } elseif ($limit) {
            $select->limit($limit);
        } elseif ($perPage && $currentPage) {
            $select->limit($perPage, ($currentPage - 1 ) * $perPage);
        }


        //echo $select->__toString();

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "attachment_id = '{$id}'");
    }

    /**
     * delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("attachment_id = '{$id}'");
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("attachment_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

}
