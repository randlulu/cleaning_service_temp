<?php

class Model_BookingEstimate extends Zend_Db_Table_Abstract {

    protected $_name = 'booking_estimate';
    //
    //model for fill function
    //
    private $loggedUser = array();
    private $modelBooking;
    private $modelContractorInfo;
    private $modelContractorServiceBooking;
    private $modelCustomer;
    private $modelBookingLog;
    private $modelComplaint;
    private $modelEstimateDiscussion;
    private $modelEmailLog;
    private $modelEstimateLabel;
    private $modelLabel;
    private $modelCities;
    private $modelBookingAddress;
    private $modelUser;
    private $modelCustomerCommercialInfo;
    private $modelBookingAttachment;
    private $modelCustomerContact;
    private $modelCustomerContactLabel;
    private $modelBookingMultipleDays;
    private $modelServices;
    private $modelBookingContactHistory;
    private $modelInquiryReminder;

    public function init() {
        parent::init();
        $this->loggedUser = CheckAuth::getLoggedUser();
    }

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null , $limit = 0, $perPage = 0, $currentPage = 0) {
         
		$db_format = 0;
		$dateTimeObj= get_settings_date_format();
		if($dateTimeObj)
		{
			$db_format = 1;
		}
		
		
		
		
		$select = $this->getAdapter()->select();
        $select->from(array('est' => $this->_name));
        $select->distinct();
        $select->order($order);

        $joinInner = array();
        $loggedUser = CheckAuth::getLoggedUser();

        $filters['company_id'] = CheckAuth::getCompanySession();

        if (CheckAuth::checkCredential(array('canSeeDeletedEstimate'))) {
            if (!empty($filters['is_deleted'])) {
                $select->where('est.is_deleted = 1');
            } else {
                $select->where('est.is_deleted = 0');
            }
        } else {
            $select->where('est.is_deleted = 0');
        }

        if (!CheckAuth::checkCredential(array('canSeeAllEstimate'))) {
            if (CheckAuth::checkCredential(array('canSeeOnlyHisEstimate'))) {
                if (CheckAuth::checkCredential(array('canSeeAssignedEstimate'))) {

                    $joinInner['contractor_service_booking'] = array('name' => array('csb' => 'contractor_service_booking'), 'cond' => 'est.booking_id = csb.booking_id', 'cols' => '');
                    $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');

                    $select->where("bok.created_by = " . $loggedUser['user_id'] . " OR csb.contractor_id = {$loggedUser['user_id']}");
                } else {
                    $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');
                    $select->where("bok.created_by = " . $loggedUser['user_id']);
                }
            } else {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');
                $select->where("bok.created_by = " . $loggedUser['user_id']);
            }
        }

        if ($filters) {
            if (!empty($filters['keywords'])) {
                $keywords = $this->getAdapter()->quote('%' . trim($filters['keywords']) . '%');
                
				$joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');
				
				
				/*if(stripos($filters['keywords'] , 'EST')  === 0 ){
				   $rows = $this->getByNum($filters['keywords']);
				   if(!empty($rows)){
				    $select->where("est.estimate_num = '{$filters['keywords']}' ");
				   }  
				}*/
				
                $select->where("est.full_text_search LIKE {$keywords} OR bok.full_text_search LIKE {$keywords} ");
				
                //$keywords = '+' . preg_replace('#[\s]+#', ' +', trim($filters['keywords']));
                //$keywords = $this->getAdapter()->quote($keywords);
                //$select->where("MATCH(est.full_text_search) AGAINST({$keywords} IN BOOLEAN MODE)");
            }
			
			

            if (!empty($filters['estimate_num'])) {
                $estimate_num = $this->getAdapter()->quote('%' . trim($filters['estimate_num']) . '%');
                $select->where("est.estimate_num LIKE {$estimate_num}");
            }

            if (!empty($filters['booking_num'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');
                $booking_num = $this->getAdapter()->quote('%' . trim($filters['booking_num']) . '%');
                $select->where("bok.booking_num LIKE {$booking_num}");
            }

            if (!empty($filters['estimate_type'])) {
                if ($filters['estimate_type'] != 'all') {
                    $estimate_type = $this->getAdapter()->quote(trim($filters['estimate_type']));
                    $select->where("est.estimate_type = {$estimate_type}");
                }
            }

            
            if (!empty($filters['customer_id'])) {
                $filters['customer_id'] = (int) $filters['customer_id'];
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');
                $select->where("bok.customer_id = {$filters['customer_id']} ");
            }
            if (!empty($filters['customer_name'])) {
                if (!$this->modelCustomer) {
                    $this->modelCustomer = new Model_Customer();
                }
                $allCustomer = $this->modelCustomer->getCustomerIdByIdFullNameSearch(trim($filters['customer_name']));
                
                $customerIds = implode(', ', $allCustomer);
                if ($customerIds) {
                    $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');
                    $select->where("bok.customer_id in ( $customerIds )");
                }
            }
            
            
            if (!empty($filters['customer_type_id'])) {
                $filters['customer_type_id'] = (int) $filters['customer_type_id'];
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');
                $joinInner['customer'] = array('name' => array('cus' => 'customer'), 'cond' => 'cus.customer_id = bok.customer_id', 'cols' => '');
                $select->where("cus.customer_type_id = {$filters['customer_type_id']} ");
            }

			if (!empty($filters['floor_id'])) {
                $filters['floor_id'] = (int) $filters['floor_id'];
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');
                $joinInner['service_attribute_value'] = array('name' => array('sav' => 'service_attribute_value'), 'cond' => 'sav.booking_id = bok.booking_id', 'cols' => '');
                $select->where("sav.value = {$filters['floor_id']} ");
            }

            if (!empty($filters['created_by'])) {
                $user_id = (int) $filters['created_by'];
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');
                $select->where("bok.created_by = '{$user_id}'");
            }

            if (!empty($filters['contractor_id'])) {
                $contractor_id = (int) $filters['contractor_id'];
                $joinInner['contractor_service_booking'] = array('name' => array('csb' => 'contractor_service_booking'), 'cond' => 'est.booking_id = csb.booking_id', 'cols' => '');
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');
                $select->where("bok.created_by = {$contractor_id} OR csb.contractor_id = {$contractor_id}");
            }

            if (!empty($filters['created_from'])) {
                $created_from = $this->getAdapter()->quote(strtotime(trim($filters['created_from'])));
                $select->where("est.created >= {$created_from}");
            }

            if (!empty($filters['created_to'])) {
                $created_to = $this->getAdapter()->quote(strtotime(trim($filters['created_to'])));
                $select->where("est.created <= {$created_to}");
            }

            if (!empty($filters['estimate_label_ids']) && $filters['estimate_label_ids']) {
                $joinInner['estimate_label'] = array('name' => array('el' => 'estimate_label'), 'cond' => 'est.id = el.estimate_id', 'cols' => '');
                $select->where('el.label_id IN (' . implode(', ', $filters['estimate_label_ids']) . ')');
            }

            if (!empty($filters['service_id'])) {
                $joinInner['contractor_service_booking'] = array('name' => array('csb' => 'contractor_service_booking'), 'cond' => 'est.booking_id = csb.booking_id', 'cols' => '');
                $service_id = (int) $filters['service_id'];
                $select->where("csb.service_id = {$service_id}");
            }

            if (!empty($filters['status'])) {
                $status = $this->getAdapter()->quote(trim($filters['status']));
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');
                $select->where("bok.status_id = {$status}");
            }

            if (!empty($filters['city_id'])) {
                $city_id = (int) $filters['city_id'];
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');
                $select->where("bok.city_id = {$city_id}");
            }
			
			if (!empty($filters['state'])) {
                $state = $filters['state'];
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');
                $joinInner['city'] = array('name' => array('c' => 'city'), 'cond' => 'c.city_id = bok.city_id', 'cols' => '');
                $select->where("c.state = '{$state}'");
            }
            
            if (!empty($filters['booking_start'])) {
                $bookingStart = $this->getAdapter()->quote(trim($filters['booking_start']));
				if($db_format)
				{
					$bookingStart = $this->getAdapter()->quote(timeFormat_zend_to_purePhp('Y-m-d H:i',trim($filters['booking_start'])));
				}
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');
                $select->where("bok.booking_start >= {$bookingStart}");
            }

            if (!empty($filters['booking_end'])) {
                $bookingEnd = $this->getAdapter()->quote(trim($filters['booking_end']));
				if($db_format)
				{
					$bookingEnd = $this->getAdapter()->quote(timeFormat_zend_to_purePhp('Y-m-d H:i', trim($filters['booking_end'])));
				}
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');
                $select->where("bok.booking_end <= {$bookingEnd}");
            }

            if (!empty($filters['qoute_from'])) {
                $qoute_from = $this->getAdapter()->quote(trim($filters['qoute_from']));
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');
                $select->where("bok.qoute >= {$qoute_from}");
            }

            if (!empty($filters['qoute_to'])) {
                $qoute_to = $this->getAdapter()->quote(trim($filters['qoute_to']));
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');
                $select->where("bok.qoute <= {$qoute_to}");
            }

            if (!empty($filters['company_id'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');

                $company_id = (int) $filters['company_id'];
                $select->where("bok.company_id = {$company_id}");
            }

            if (!empty($filters['address'])) {
                $address = trim($filters['address']);
                $joinInner['booking_address'] = array('name' => array('ba' => 'booking_address'), 'cond' => 'est.booking_id = ba.booking_id', 'cols' => '');
                $select->where("CONCAT_WS(' ',ba.street_number ,ba.street_address ,ba.suburb,ba.state,ba.postcode) LIKE '%{$address}%'");
            }

            if (!empty($filters['email'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');
                $joinInner['customer'] = array('name' => array('c' => 'customer'), 'cond' => 'bok.customer_id = c.customer_id', 'cols' => '');
                $email = trim($filters['email']);
                $select->where("CONCAT_WS(' ',email1 ,email2 ,email3) LIKE '%{$email}%'");
            }

            if (!empty($filters['mobile/phone'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');
                $joinInner['customer'] = array('name' => array('c' => 'customer'), 'cond' => 'bok.customer_id = c.customer_id', 'cols' => '');
                $mobile_phone = trim($filters['mobile/phone']);
                $select->where("CONCAT_WS(' ',phone1 ,phone2 ,phone3,mobile1 ,mobile2 ,mobile3) LIKE '%{$mobile_phone}%'");
            }

            if (!empty($filters['bussiness_name'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');
                $joinInner['customer_commercial_info'] = array('name' => array('cci' => 'customer_commercial_info'), 'cond' => 'bok.customer_id = cci.customer_id', 'cols' => '');
                $select->where("cci.business_name LIKE '%{$filters['bussiness_name']}%'");
            }

            if (!empty($filters['my_estimates'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');

                $user_id = (int) $filters['my_estimates'];

                $my_booking_sql = $this->getAdapter()->select();
                $my_booking_sql->from('booking_log', 'booking_id');
                $my_booking_sql->where("log_user_id = {$user_id}");
                $my_booking_sql->distinct();
                $my_booking_results = $this->getAdapter()->fetchAll($my_booking_sql);

                $not_my_booking_sql = $this->getAdapter()->select();
                $not_my_booking_sql->from('booking_log', 'booking_id');
                $not_my_booking_sql->distinct();

                if ($my_booking_results) {
                    $my_booking_ids = array();

                    foreach ($my_booking_results as $my_result) {
                        $my_booking_ids[$my_result['booking_id']] = $my_result['booking_id'];
                    }

                    $not_my_booking_sql->where('booking_id NOT IN (' . implode(', ', $my_booking_ids) . ')');
                }

                $not_my_results = $this->getAdapter()->fetchAll($not_my_booking_sql);

                $not_my_booking_ids = array();

                foreach ($not_my_results as $not_my_result) {
                    $not_my_booking_ids[$not_my_result['booking_id']] = $not_my_result['booking_id'];
                }

                if ($not_my_booking_ids) {
                    $select->where('bok.booking_id NOT IN (' . implode(', ', $not_my_booking_ids) . ')');
                }
            }
			//////////// get all visited estimates
			if (!empty($filters['visited']) && $filters['visited'] == true) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');
                $select->where("bok.visited = 1");
                
            }
			///////////By Islam get visited or not visited estimates from droplist filter
			if(!empty($filters['is_visited'])){
			if ($filters['is_visited']==1) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');
                $select->where("bok.visited =0");
            }
			if ($filters['is_visited']==2) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');
                $select->where("bok.visited =1");
            }
			}
			///////////
			
			
			

            if (!empty($filters['to_follow']) && $filters['to_follow'] == true) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');
                $select->where("bok.is_to_follow = 1");
                $select->where("bok.convert_status = 'estimate'");
            }

            if (!empty($filters['to_follow_date'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');
                switch ($filters['to_follow_date']) {
                    case 'yesterday':
                        $yesterday = getTimePeriodByName('yesterday');
                        $select->where("bok.to_follow between '" . strtotime($yesterday['start']) . "' and '" . strtotime($yesterday['end']) . "'");
                        break;
                    case 'today':
                        $today = getTimePeriodByName('today');
                        $select->where("bok.to_follow between '" . strtotime($today['start']) . "' and '" . strtotime($today['end']) . "'");
                        break;
                    case 'tomorrow':
                        $tomorrow = getTimePeriodByName('tomorrow');
                        $select->where("bok.to_follow between '" . strtotime($tomorrow['start']) . "' and '" . strtotime($tomorrow['end']) . "'");
                        break;
                    case 'past':
                        $today = getTimePeriodByName('today');
                        $select->where("bok.to_follow < '" . strtotime($today['start']) . "'");
                        break;
                    case 'future':
                        $today = getTimePeriodByName('today');
                        $select->where("bok.to_follow > '" . strtotime($today['end']) . "'");
                        break;
                }
            }
			
			if(!empty($filters['to_follow_date_start_range']) && !empty($filters['to_follow_date_end_range'])){
			$joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');
			$select->where("bok.to_follow between '" . strtotime($filters['to_follow_date_start_range']) . "' and '" . strtotime($filters['to_follow_date_end_range']) . "'");  
			
			}

            if (!empty($filters['inquiry_num'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');
                $joinInner['inquiry'] = array('name' => array('i' => 'inquiry'), 'cond' => 'bok.original_inquiry_id = i.inquiry_id', 'cols' => '');
                $inquiry_num = $this->getAdapter()->quote('%' . trim($filters['inquiry_num']) . '%');
                $select->where("i.inquiry_num LIKE {$inquiry_num}");
            }
			
			/***Filter between (from - to) Date ***IBM*/
            if (isset($filters['to_follow_between_dates']) && $filters['to_follow_between_dates'] == true) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');
                $toFollowStartBetween = $filters['to_follow_start_between'];
                $toFollowEndBetween = !empty($filters['to_follow_end_between']) ? $filters['to_follow_end_between'] : date('Y-m-d H:i:s', time());                 
                    $select->where("bok.to_follow between '" . strtotime($toFollowStartBetween) . "' and '" . strtotime($toFollowEndBetween) . "'");         
            }
        }
		

        if ($joinInner) {
            foreach ($joinInner as $inner) {
                $select->joinInner($inner['name'], $inner['cond'], $inner['cols']);
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }elseif ($limit) {
            $select->limit($limit);
        } elseif ($perPage && $currentPage) {
            $select->limit($perPage, ($currentPage - 1) * $perPage);
        }
		
		/*echo $select->__toString();
		exit();*/
		
		
        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateById($id, $data, $addLog = true) {
        $id = (int) $id;

        /**
         * update full text search table
         */
        $done = false;
        if (!empty($data['full_text_search'])) {
            $done = true;
        }
        $modelUpdateFullTextSearch = new Model_UpdateFullTextSearch();
        $modelUpdateFullTextSearch->updateFullTextSearchFlag($this->_name, $id, $done);

        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'changed');

        $success = parent::update($data, "id = '{$id}'");

        /**
         * get all record and insert it in log
         */
        $modelBookingEstimateLog = new Model_BookingEstimateLog();
        if ($addLog) {
            $modelBookingEstimateLog->addBookingEstimateLog($id);
        } else {
            $last = $modelBookingEstimateLog->getLastLogByEstimateId($id);
            $lastLogEstimateId = !empty($last) ? $last['log_id'] : 0;
            $data = $this->getById($id);
			unset($data['pause_emails']);
            $modelBookingEstimateLog->updateById($lastLogEstimateId, $data);
        }

        return $success;
    }

    public function insert(array $data) {

        $company_id = CheckAuth::getCompanySession();

        $select = $this->getAdapter()->select();
        $select->from(array('est' => $this->_name), 'MAX(est.count)');
        $select->joinInner(array('bok' => 'booking'), 'est.booking_id = bok.booking_id', '');
        $select->where("bok.company_id = {$company_id}");
        $result = $this->getAdapter()->fetchOne($select);

        $data['count'] = $result + 1;
        $data['estimate_num'] = 'EST-' . ($result + 1);

        $id = parent::insert($data);

        /**
         * update full text search table
         */
        $modelUpdateFullTextSearch = new Model_UpdateFullTextSearch();
        $modelUpdateFullTextSearch->insertFullTextSearch($this->_name, $id);

        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'added');

        /**
         * get all record and insert it in log
         */
        $modelBookingEstimateLog = new Model_BookingEstimateLog();
        $modelBookingEstimateLog->addBookingEstimateLog($id);

        return $id;
    }

    /**
     * delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;

        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'deleted');

        return parent::delete("id = '{$id}'");
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from(array('est'=>$this->_name));
		$select->joinInner(array('bok' => 'booking'),'bok.booking_id = est.booking_id',array('pause_emails', 'booking_id'));
        $select->where("id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }
	
	
	public function getByNum($num) {
        
		$company_id = CheckAuth::getCompanySession();
		$company_id = (int) $company_id;
        $select = $this->getAdapter()->select();
        $select->from(array('est'=>$this->_name));
        $select->joinInner(array('bok' => 'booking'),'bok.booking_id = est.booking_id',array('pause_emails'));
        $select->where("est.estimate_num = '{$num}'");
		$select->where('est.is_deleted = 0');
		$select->where("bok.company_id = {$company_id}");
        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * getEstimateByCustomerId
     *  
     * @param int $customerId
     * @return array 
     */
    public function getEstimateByCustomerId($customerId) {

        $filters = array();
        $filters['customer_id'] = $customerId;

        return $this->getAll($filters);
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getByBookingId($bookingId) {
        $bookingId = (int) $bookingId;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_id = '{$bookingId}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getNotDeletedByBookingId($bookingId) {
        $bookingId = (int) $bookingId;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_id = '{$bookingId}' AND is_deleted = 0");

        return $this->getAdapter()->fetchRow($select);
    }

    public function getDeletedByBookingId($bookingId) {
        $bookingId = (int) $bookingId;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_id = '{$bookingId}' AND is_deleted = 1");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateByBookingId($bookingId, $data) {
        $bookingId = (int) $bookingId;
        $bookingEstimate = $this->getByBookingId($bookingId);
        if ($bookingEstimate) {
            $this->updateById($bookingEstimate['id'], $data);
        }
    }

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateNotDeletedByBookingId($bookingId, $data) {
        $bookingId = (int) $bookingId;
        $bookingEstimate = $this->getNotDeletedByBookingId($bookingId);
        if ($bookingEstimate) {
            $this->updateById($bookingEstimate['id'], $data);
        }
    }

    /**
     * delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean
     */
    public function deleteForeverByBookingId($bookingId) {
        $bookingId = (int) $bookingId;

        return parent::delete("booking_id = '{$bookingId}'");
    }

    public function getEnamEstimateType() {
        $query = "SHOW COLUMNS FROM {$this->_name} LIKE 'estimate_type'";
        $stmt = $this->getAdapter()->query($query);
        $row = $stmt->fetch();
        $row = $row['Type'];
        $regex = "/'(.*?)'/";
        preg_match_all($regex, $row, $enum_array);
        $enum_fields = $enum_array[1];
        foreach ($enum_fields as $value) {
            $enums[$value] = $value;
        }
        return $enums;
    }

    /**
     * get extra info for all the rows
     * @param array $row
     * @param array $types
     * @return array
     */
    public function fills(&$rows, $types = array()) {
        foreach ($rows as &$row) {
            $this->fill($row, $types);
        }
    }

    /**
     * get extra info for each row (Ex: getting the category row by c_id, author row by author id ... etc)
     * 
     * @param array $row
     * @param array $types
     * @return array
     */
    public function fill(&$row, $types = array()) {

        if (in_array('booking', $types)) {
            /**
             * load model
             */
            if (!$this->modelBooking) {
                $this->modelBooking = new Model_Booking();
            }

            $booking = $this->modelBooking->getById($row['booking_id']);
            $row['booking'] = $booking;
        }

        if (in_array('contractors', $types)) {
            /**
             * load model
             */
            if (!$this->modelContractorInfo) {
                $this->modelContractorInfo = new Model_ContractorInfo();
            }
            if (!$this->modelContractorServiceBooking) {
                $this->modelContractorServiceBooking = new Model_ContractorServiceBooking();
            }
            if (!$this->modelUser) {
                $this->modelUser = new Model_User();
            }

            $allContractorServiceBooking = $this->modelContractorServiceBooking->getByBookingId($row['booking_id']);

            $allContractors = array();
            foreach ($allContractorServiceBooking as $contractorServiceBooking) {
                $contractorInfo = $this->modelContractorInfo->getByContractorId($contractorServiceBooking['contractor_id']);
                $userInfo = $this->modelUser->getById($contractorServiceBooking['contractor_id']);
                if ('contractor' == CheckAuth::getRoleName()) {
                    if ($contractorServiceBooking['contractor_id'] == $this->loggedUser['user_id']) {
                        $allContractors[$contractorServiceBooking['contractor_id']] = ucwords($userInfo['username']) . " - " . ucwords($contractorInfo['business_name'] ? $contractorInfo['business_name'] : '');
                    }
                } else {
                    $allContractors[$contractorServiceBooking['contractor_id']] = ucwords($userInfo['username']) . " - " . ucwords($contractorInfo['business_name']);
                }
            }
            $row['contractors'] = $allContractors;
        }

        if (in_array('name_contractors', $types)) {
            /**
             * load model
             */
            if (!$this->modelUser) {
                $this->modelUser = new Model_User();
            }
            if (!$this->modelContractorServiceBooking) {
                $this->modelContractorServiceBooking = new Model_ContractorServiceBooking();
            }

            $allContractorServiceBooking = $this->modelContractorServiceBooking->getByBookingId($row['booking_id']);

            $contractors = array();
            if ($allContractorServiceBooking) {
                foreach ($allContractorServiceBooking as $bookingService) {
                    $user = $this->modelUser->getById($bookingService['contractor_id']);

                    $contractorName = isset($user['display_name']) ? $user['display_name'] : '';
                    $contractors[$bookingService['contractor_id']] = ucwords($contractorName);
                }
            }
            $row['name_contractors'] = $contractors;
        }

        if (in_array('customer', $types)) {
            /**
             * load model
             */
            if (!$this->modelBooking) {
                $this->modelBooking = new Model_Booking();
            }
            if (!$this->modelCustomer) {
                $this->modelCustomer = new Model_Customer();
            }
            if (!$this->modelCustomerCommercialInfo) {
                $this->modelCustomerCommercialInfo = new Model_CustomerCommercialInfo();
            }

            $booking = $this->modelBooking->getById($row['booking_id']);
            $row['customer'] = $this->modelCustomer->getById($booking['customer_id']);
            $row['customer_name'] = get_customer_name($row['customer']);
        }
        if (in_array('booking_contact_history', $types)) {
            /**
             * load model
             */
            if (!$this->modelBookingContactHistory) {
                $this->modelBookingContactHistory = new Model_BookingContactHistory();
            }

            $row['booking_contact_history'] = $this->modelBookingContactHistory->getContactHistory($row['booking_id']);
        }
        if (in_array('contact_history', $types)) {
            /**
             * load model
             */
            if (!$this->modelBookingContactHistory) {
                $this->modelBookingContactHistory = new Model_BookingContactHistory();
            }
            if (!$this->modelInquiryReminder) {
                $this->modelInquiryReminder = new Model_InquiryReminder();
            }
            if (!$this->modelBooking) {
                $this->modelBooking = new Model_Booking();
            }

            $inquiryContactHistory = array();
            $bookingContactHistory = array();

            $booking = $this->modelBooking->getById($row['booking_id']);

            if (!empty($booking['original_inquiry_id'])) {
                $inquiryContactHistory = $this->modelInquiryReminder->getContactHistory($booking['original_inquiry_id']);
            }

            $bookingContactHistory = $this->modelBookingContactHistory->getContactHistory($row['booking_id']);
            $row['contact_history'] = array_merge($inquiryContactHistory, $bookingContactHistory);
        }
        if (in_array('customer_contacts', $types)) {
            /**
             * load model
             */
            if (!$this->modelBooking) {
                $this->modelBooking = new Model_Booking();
            }
            if (!$this->modelCustomer) {
                $this->modelCustomer = new Model_Customer();
            }
            if (!$this->modelCustomerContact) {
                $this->modelCustomerContact = new Model_CustomerContact();
            }
            if (!$this->modelCustomerContactLabel) {
                $this->modelCustomerContactLabel = new Model_CustomerContactLabel();
            }

            $booking = $this->modelBooking->getById($row['booking_id']);
            $row['customer'] = $this->modelCustomer->getById($booking['customer_id']);
            $customerContacts = '';
            if ($row['customer']) {
                $customerContacts = $this->modelCustomerContact->getByCustomerId($row['customer']['customer_id']);
                foreach ($customerContacts as &$customerContact) {
                    $customerContactLabel = $this->modelCustomerContactLabel->getById($customerContact['customer_contact_label_id']);
                    $customerContact['contact_label'] = $customerContactLabel['contact_label'];
                }
            }

            $row['customer_contacts'] = $customerContacts;
        }
        if (in_array('customer_commercial_info', $types)) {
            /**
             * load model
             */
            if (!$this->modelBooking) {
                $this->modelBooking = new Model_Booking();
            }
            if (!$this->modelCustomerCommercialInfo) {
                $this->modelCustomerCommercialInfo = new Model_CustomerCommercialInfo();
            }
            $booking = $this->modelBooking->getById($row['booking_id']);
            $row['customer_commercial_info'] = $this->modelCustomerCommercialInfo->getByCustomerId($booking['customer_id']);
        }

        if (in_array('booking_address', $types)) {
            /**
             * load model
             */
            if (!$this->modelBookingAddress) {
                $this->modelBookingAddress = new Model_BookingAddress();
            }

            $row['booking_address'] = $this->modelBookingAddress->getByBookingId($row['booking_id']);
        }

        if (in_array('complaint', $types)) {
            /**
             * load model
             */
            if (!$this->modelComplaint) {
                $this->modelComplaint = new Model_Complaint();
            }

            $row['complaint'] = $this->modelComplaint->getByBookingId($row['booking_id']);
        }

        if (in_array('discussion', $types)) {
            /**
             * load model
             */
            if (!$this->modelEstimateDiscussion) {
                $this->modelEstimateDiscussion = new Model_EstimateDiscussion();
            }

            $row['discussion'] = $this->modelEstimateDiscussion->getByEstimateId($row['id']);
        }

        if (in_array('email_history', $types)) {
            /**
             * load model
             */
            if (!$this->modelEmailLog) {
                $this->modelEmailLog = new Model_EmailLog();
            }

            $type = 'estimate';
            $row['email_history'] = $this->modelEmailLog->getByReferenceIdAndType($row['id'], $type);
        }



        if (in_array('labels', $types)) {
            /**
             * load model
             */
            if (!$this->modelEstimateLabel) {
                $this->modelEstimateLabel = new Model_EstimateLabel();
            }
            if (!$this->modelLabel) {
                $this->modelLabel = new Model_Label();
            }

            $LabelIds = $this->modelEstimateLabel->getByEstimateId($row['id']);

            $labels = array();
            foreach ($LabelIds as $LabelId) {
                $label = $this->modelLabel->getById($LabelId['label_id']);
                $labels[$label['id']] = $label['label_name'];
            }
            $row['labels'] = $labels;
        }

        if (in_array('city', $types)) {
            /**
             * load model
             */
            if (!$this->modelBooking) {
                $this->modelBooking = new Model_Booking();
            }
            if (!$this->modelCities) {
                $this->modelCities = new Model_Cities();
            }

            $booking = $this->modelBooking->getById($row['booking_id']);
            $row['city'] = $this->modelCities->getById($booking['city_id']);
            $row['city_name'] = $row['city']['city_name'];
        }
        if (in_array('isAccepted', $types)) {
            /**
             * load model
             */
            if (!$this->modelBooking) {
                $this->modelBooking = new Model_Booking();
            }

            $row['isAccepted'] = $this->modelBooking->checkBookingIfAccepted($row['booking_id']);
        }
        if (in_array('not_accepted', $types)) {
            /**
             * load model
             */
            if (!$this->modelContractorServiceBooking) {
                $this->modelContractorServiceBooking = new Model_ContractorServiceBooking();
            }
            $allServices = $this->modelContractorServiceBooking->getContractorServicesByBookingId($row['booking_id']);

            $notAccepted = 0;
            if ($allServices) {
                foreach ($allServices as $serviceCB) {
                    if ((isset($serviceCB['is_accepted']) && $serviceCB['is_accepted'] == 0)) {
                        $notAccepted = 1;
                    }
                }
            }
            $row['not_accepted'] = $notAccepted;
        }

        if (in_array('not_accepted_or_rejected', $types)) {
            /**
             * load model
             */
            if (!$this->modelContractorServiceBooking) {
                $this->modelContractorServiceBooking = new Model_ContractorServiceBooking();
            }

            $allServices = $this->modelContractorServiceBooking->getContractorServicesByBookingId($row['booking_id']);

            $notAcceptedOrRejected = 0;
            if ($allServices) {
                foreach ($allServices as $serviceCB) {
                    if ((isset($serviceCB['is_accepted']) && $serviceCB['is_accepted'] == 0) && (isset($serviceCB['is_rejected']) && $serviceCB['is_rejected'] == 0)) {
                        $notAcceptedOrRejected = 1;
                    }
                }
            }
            $row['not_accepted_or_rejected'] = $notAcceptedOrRejected;
        }
        if (in_array('booking_users', $types)) {
            /**
             * load model
             */
            if (!$this->modelBookingLog) {
                $this->modelBookingLog = new Model_BookingLog();
            }
            if (!$this->modelUser) {
                $this->modelUser = new Model_User();
            }

            $bookingUsers = $this->modelBookingLog->getUsersByBookingId($row['booking_id']);
            $this->modelUser->fills($bookingUsers, array('user'));

            $row['booking_users'] = $bookingUsers;
        }
        if (in_array('booking_attachment', $types)) {
            /**
             * load model
             */
            if (!$this->modelBooking) {
                $this->modelBooking = new Model_Booking();
            }
            if (!$this->modelBookingAttachment) {
                $this->modelBookingAttachment = new Model_BookingAttachment();
            }
            $booking = $this->modelBooking->getById($row['booking_id']);
            $row['booking_attachment'] = $this->modelBookingAttachment->getAll(array('booking_id' => $row['booking_id'], 'inquiry_id' => $booking['original_inquiry_id']));
        }
        if (in_array('have_attachment', $types)) {
            if (!$this->modelBookingAttachment) {
                $this->modelBookingAttachment = new Model_BookingAttachment();
            }
            $booking = $this->modelBooking->getById($row['booking_id']);

            $have_attachment = $this->modelBookingAttachment->getByBookingIdOrInquiryId($row['booking_id'], $booking['original_inquiry_id']);
            $row['have_attachment'] = !empty($have_attachment) ? 1 : 0;
        }
        if (in_array('multiple_days', $types)) {
            /**
             * load model
             */
            if (!$this->modelBookingMultipleDays) {
                $this->modelBookingMultipleDays = new Model_BookingMultipleDays();
            }
            $row['multiple_days'] = $this->modelBookingMultipleDays->getByBookingId($row['booking_id']);
        }
        if (in_array('services', $types)) {
            /**
             * load model
             */
            if (!$this->modelContractorServiceBooking) {
                $this->modelContractorServiceBooking = new Model_ContractorServiceBooking();
            }
            if (!$this->modelServices) {
                $this->modelServices = new Model_Services();
            }
            $filters = array();
            $filters['booking_id'] = $row['booking_id'];
            $services = $this->modelContractorServiceBooking->getAll($filters);

            foreach ($services as &$service) {
                $service['info'] = $this->modelServices->getById($service['service_id']);
            }

            $row['services'] = $services;
        }

        return $row;
    }


    public function convertToEstimate($bookingId, $toFollow = false, $addLog = true, $isToFollow = false,$log_id=0,$os='') {

        $modelBookingInvoice = new Model_BookingInvoice();
        $bookingInvoice = $modelBookingInvoice->getByBookingId($bookingId);

        $estimateId = 0;

        if (!$bookingInvoice) {

            $db_params = array();
            $db_params['booking_id'] = $bookingId;
            $db_params['estimate_type'] = 'draft';
            $db_params['is_deleted'] = 0;

            $bookingEstimate = $this->getByBookingId($bookingId);

            if ($bookingEstimate) {
                if ($bookingEstimate['estimate_type'] == 'booking') {
                    $db_params['created'] = time();
                }
                $this->updateById($bookingEstimate['id'], $db_params);
                $estimateId = $bookingEstimate['id'];
            } else {
                $db_params['created'] = time();
                $estimateId = $this->insert($db_params);
            }

            $modelBookingStatus = new Model_BookingStatus();
            $quoted = $modelBookingStatus->getByStatusName('QUOTED');

            $params = array();
            $params['convert_status'] = 'estimate';
            $params['status_id'] = $quoted['booking_status_id'];
            if ($toFollow) {
                $params['to_follow'] = strtotime($toFollow);
            }
            if ($isToFollow) {
                $params['is_to_follow'] = 1;
            }

            $modelBooking = new Model_Booking();

            $modelBooking->updateById($bookingId, $params, $addLog,$log_id, $os);
        }

        return $estimateId;
    }

    public function deleteEstimate($bookingId, $statusId = 0, $addLog = true,$os='') {

        $bookingEstimate = $this->getNotDeletedByBookingId($bookingId);

        $success = false;
        if ($bookingEstimate) {
            //add User Log
            $modelLogUser = new Model_LogUser();
            $modelLogUser->addUserLogEvent($bookingId, $this->_name, 'deleted');
            $success = $this->updateById($bookingEstimate['id'], array('is_deleted' => 1));


            $modelBookingStatus = new Model_BookingStatus();
            $quoted = $modelBookingStatus->getByStatusName('QUOTED');

            if ($statusId && $statusId != $quoted['booking_status_id']) {
                $modelBooking = new Model_Booking();
                $modelBooking->updateById($bookingId, array('convert_status' => 'booking', 'status_id' => $statusId, 'created' => time()), $addLog);
            }
        }

        return $success;
    }

    public function changedEstimateToBooking($bookingId, $statusId = 0, $addLog = true,$log_id=0,$os='') {
	
	    $modelBooking = new Model_Booking();
        $bookingEstimate = $this->getNotDeletedByBookingId($bookingId);
		$booking = $modelBooking->getById($bookingId);
        $success = false;
        if ($bookingEstimate && $booking['convert_status'] == 'estimate') {
            //add User Log
            $modelLogUser = new Model_LogUser();
            $modelLogUser->addUserLogEvent($bookingId, $this->_name, 'changed');
            $success = $this->updateById($bookingEstimate['id'], array('estimate_type' => 'booking'));

            $modelBookingStatus = new Model_BookingStatus();
            $quoted = $modelBookingStatus->getByStatusName('QUOTED');

            if ($statusId && $statusId != $quoted['booking_status_id']) {
                /////by islam to calcuate number of hours between estimate creation and convert date
				$estimateCreateDate = $bookingEstimate['created'];
				$convertDurationInSeconds = time() - $estimateCreateDate;
				$convertDurationInHrs = $convertDurationInSeconds/3600;
				 $modelBooking->updateById($bookingId, array('convert_status' => 'booking', 'status_id' => $statusId, 'created' => time(),'convert_to_booking_duration' => $convertDurationInHrs), $addLog,$log_id,$os);
            }
        }

        return $success;
    }

    public function unDeleteEstimate($bookingId) {

        $bookingEstimate = $this->getByBookingId($bookingId);

        $success = false;
        if ($bookingEstimate) {
            //add User Log
            $modelLogUser = new Model_LogUser();
            $modelLogUser->addUserLogEvent($bookingId, $this->_name, 'added');
            $success = $this->updateById($bookingEstimate['id'], array('is_deleted' => 0));

            $modelBookingInvoice = new Model_BookingInvoice();
            $bookingInvoice = $modelBookingInvoice->getByBookingId($bookingId);

            if (!$bookingInvoice) {
                $modelBookingStatus = new Model_BookingStatus();
                $quoted = $modelBookingStatus->getByStatusName('QUOTED');

                $modelBooking = new Model_Booking();
                $modelBooking->updateById($bookingId, array('convert_status' => 'estimate', 'status_id' => $quoted['booking_status_id']));
            }
        }

        return $success;
    }

    public function deleteEstimateForEver($bookingId) {
        $bookingEstimate = $this->getDeletedByBookingId($bookingId);

        $success = false;
        if ($bookingEstimate) {
            //add User Log
            $modelLogUser = new Model_LogUser();
            $modelLogUser->addUserLogEvent($bookingId, $this->_name, 'deleted');
            $success = $this->deleteById($bookingEstimate['id']);
            $this->deleteRelatedEstimate($bookingEstimate['id']);

            $modelBooking = new Model_Booking();
            $modelBooking->deleteById($bookingId);
            $modelBooking->deleteRelatedBooking($bookingId);
        }

        return $success;
    }

    public function cronJobReminderQuotedBooking() {
	
		$modelCronjobHistory = new Model_CronjobHistory();
		$modelCronJob = new Model_CronJob();
		$modelMailingListUnsubscribed = new Model_MailingListUnsubscribed();
		
		//
		// save this cron in cronjob_history table
		//
		$cronjob = $modelCronJob->getIdByName('Reminder_Estimates');
		$cronjonID = $cronjob['id'];
		
		$cronjobHistoryData = array(
		'cron_job_id' => $cronjonID,
		'run_time' => time()
		);
		$cronjobHistoryId = $modelCronjobHistory->insert($cronjobHistoryData);
		
		//***********

        //
        // load models
        //
        $modelUser = new Model_User();
        $modelBooking = new Model_Booking();
        $modelCustomer = new Model_Customer();
        $modelBookingStatus = new Model_BookingStatus();
        $modelBookingAddress = new Model_BookingAddress();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
		$modelAuthRole = new Model_AuthRole();
		
        $router = Zend_Controller_Front::getInstance()->getRouter();

        $today = time();

        $quoted = $modelBookingStatus->getByStatusName('QUOTED');

        $select = $this->getAdapter()->select();

        $select->from(array('est' => $this->_name));
        $select->joinInner(array('bok' => 'booking'), 'est.booking_id = bok.booking_id', '');
        $select->where("bok.convert_status = 'estimate'");
        $select->where("bok.is_deleted = 0");
        $select->where("bok.pause_emails = 0");
        $select->where("bok.status_id = {$quoted['booking_status_id']}");
        $select->where("bok.to_follow < '{$today}'");
        $select->where("bok.is_to_follow = 1");
        $select->where("est.estimate_reminded != 'done'");
        $select->where("est.is_deleted = 0");
        $select->where("est.estimate_type = 'draft'");
		$select->where("bok.is_change = 0");//not awaiting approval
		
        $estimates = $this->getAdapter()->fetchAll($select);
        $this->fills($estimates, array('booking'));
		
        foreach ($estimates as $estimate) {
			
            //create code
            $cancel_hashcode = sha1(uniqid());
            $modelBooking->updateById($estimate['booking_id'], array('cancel_hashcode' => $cancel_hashcode));

            $cancel_link = $router->assemble(array('cancel_hashcode' => $cancel_hashcode, 'booking_id' => $estimate['booking_id'], 'status_id' => $quoted['booking_status_id']), 'cancelBooking');

            $customer = $modelCustomer->getById($estimate['booking']['customer_id']);
            $user = $modelUser->getById($estimate['booking']['created_by']);

            // Create Attachment
            $viewParam = $this->getViewParam($estimate['id']);
            $view = new Zend_View();
            $view->setScriptPath(APPLICATION_PATH . '/modules/estimates/views/scripts/index');
            $view->bookingServices = $viewParam['bookingServices'];
            $view->thisBookingServices = $viewParam['thisBookingServices'];
            $view->priceArray = $viewParam['priceArray'];
            $view->estimate = $viewParam['estimate'];

            $bodyEstimate = $view->render('estimate.phtml');
            $pdfPath = createPdfPath();
            $destination = $pdfPath['fullDir'] . $viewParam['estimate']['estimate_num'] . '.pdf';
            wkhtmltopdf($bodyEstimate, $destination);
			$pdfAttachment = $destination;
			
			/// get faq for booking

        $mdoel_contractorServiceBooking = new Model_ContractorServiceBooking();
		$trading_namesObj = new Model_TradingName();
        $allItemService = $mdoel_contractorServiceBooking->getByBookingId($estimate['booking']['booking_id']);
        $trading_names = $trading_namesObj->getById($estimate['booking']['trading_name_id']);
        if(!isset($estimate['booking']['trading_name_id']) || empty($estimate['booking']['trading_name_id']) || $estimate['booking']['trading_name_id'] =='' ) {
                    $estimate_trading_name_id =0;
                }else{
                    $estimate_trading_name_id =$estimate['booking']['trading_name_id'];
                }

        $faq_html = "";
        $wheres = array();
		
		

        foreach ($allItemService as $key => $itemService) {
            $model_attributes = new Model_Attributes();
			$modelServiceAttribute = new Model_ServiceAttribute();
            $attribute = $model_attributes->getByAttributeName("Floor", $estimate['booking']['company_id']);
            $allServiceAttribute = $modelServiceAttribute->getByAttributeIdAndServiceId($attribute['attribute_id'], $itemService['service_id']);
            $modelServiceAtrributeValue = new Model_ServiceAttributeValue();
            $serviceAttributeValue = $modelServiceAtrributeValue->getByBookingIdAndServiceAttributeIdAndClone($estimate['booking']['booking_id'], $allServiceAttribute['service_attribute_id'], $itemService['clone']);

            if (!empty($serviceAttributeValue['value'])) {
                $wheres[] = 'service_id =' . $itemService['service_id'] . ' and floor_id = ' . $serviceAttributeValue['value'];
            }
        }

        if (!empty($wheres)) {
            $model_faq = new Model_Faq();
            $faq = $model_faq->getFaqByServiceIdAndFloorId($wheres);
            if (empty($faq)) {
                $faq = $model_faq->getFaqByServiceIdAndFloorId($wheres, 1);
            }

            foreach ($faq as $key => $faq_value) {
                $question = str_replace('$custom_city', $customer['city_name'], $faq_value['question']);
                $answer = str_replace('$custom_city', $customer['city_name'], $faq_value['answer']);
                $faq_html .= '<ul><li><p><a style="color:#0966c2;font-weight:bold;" href="' . $trading_names['website_url'] . '">' . $question . '</a></p></li><li style="list-style:none;"><p>' . $answer . '</p></li></ul>';
            }
        }
		
		
			

            $template_params = array(
                //link
                '{cancel_link}' => '<a href="' . $cancel_link . '">' . $cancel_link . '</a>',
                //estimate
                '{estimate_num}' => $estimate['estimate_num'],
                '{estimate_created}' => date('d/m/Y', $estimate['created']),
				'{faq}' => $faq_html,
                //booking
                '{booking_num}' => $estimate['booking']['booking_num'],
                '{total_without_tax}' => number_format($estimate['booking']['sub_total'], 2),
                '{gst_tax}' => number_format($estimate['booking']['gst'], 2),
                '{total_with_tax}' => number_format($estimate['booking']['qoute'], 2),
                '{description}' => nl2br($estimate['booking']['description'] ? $estimate['booking']['description'] : ''),
                '{booking_created}' => date('d/m/Y', $estimate['booking']['created']),
                '{booking_created_by}' => ucwords($user['username']),
                '{booking_start}' => date("F j, Y, g:i a", strtotime($estimate['booking']['booking_start'])),
                '{booking_address}' => get_line_address($modelBookingAddress->getByBookingId($estimate['booking']['booking_id'])),
                '{service}' => nl2br($modelContractorServiceBooking->getBookingAsText($estimate['booking']['booking_id'], true)),
                //customer
                '{customer_name}' => get_customer_name($customer),
                '{customer_first_name}' => isset($customer['first_name']) && $customer['first_name'] ? ucwords($customer['first_name']) : '',
                '{customer_last_name}' => isset($customer['last_name']) && $customer['last_name'] ? ' ' . ucwords($customer['last_name']) : '',
                '{customer_contacts}' => nl2br($modelCustomer->getCustomerContacts($estimate['booking']['customer_id'])),
                '{attachment}' => $bodyEstimate,
                '{sender_name}' => ucwords($user['username'])
            );

            $to = array();
			$modelMailingListUnsubscribed = new Model_MailingListUnsubscribed();
			$unsubscribeCustomerEmails = $modelMailingListUnsubscribed->getByCronJobIdAndCustomerId($cronjonID , $estimate['booking']['customer_id']);
			
           /* if ($customer['email1']&& filter_var($customer['email1'], FILTER_VALIDATE_EMAIL)) {
                $to[] = $customer['email1'];
            }
            if ($customer['email2']&& filter_var($customer['email2'], FILTER_VALIDATE_EMAIL)) {
                $to[] = $customer['email2'];
            }
            if ($customer['email3']&& filter_var($customer['email3'], FILTER_VALIDATE_EMAIL)) {
                $to[] = $customer['email3'];
            }
            $to = implode(',', $to);*/
			
			
			if ($customer['email1']&& filter_var($customer['email1'], FILTER_VALIDATE_EMAIL)) {
				$to[0] = array('email'=>$customer['email1'] ,'number'=>'1');
            }
            if ($customer['email2']&& filter_var($customer['email2'], FILTER_VALIDATE_EMAIL)) {
                $to[1] = array('email'=>$customer['email2'] ,'number'=>'2');
            }
            if ($customer['email3']&& filter_var($customer['email3'], FILTER_VALIDATE_EMAIL)) {
                $to[2] = array('email'=>$customer['email3'] ,'number'=>'3');
            }
			
			
			foreach($unsubscribeCustomerEmails as $customerEmail){
			  if($customer['email1'] == $customerEmail['customer_email']){
					unset($to[0]);
				}else if($customer['email2'] == $customerEmail['customer_email']){
					unset($to[1]);
				}else if($customer['email3'] == $customerEmail['customer_email']){
					unset($to[2]);
				}			  
			}
			
			

		
			
            $email_log = array('reference_id' => $estimate['id'], 'cronjob_history_id' => $cronjobHistoryId, 'type' => 'estimate');
			
            $companyId = $estimate['booking']['company_id'];

			$trading_namesObj = new Model_TradingName();
        $trading_names = $trading_namesObj->getById($estimate['booking']['trading_name_id']);
		

            $success = false;
			if ($to) {
            $data = array(
                'reply' => array('name' => $user['display_name'], 'email' => $user['email1'],),
                'attachment' => $pdfAttachment,
				'companyId' => $companyId,
				'trading_name' => $trading_names['trading_name'],
				'from' => $trading_names['email'],	
            );
			
			///if the creator of this booking is super_admin don't send to his personal email send to system email instead
			$superAdminRoleId = $modelAuthRole->getRoleIdByName('super_admin');
			if($user['role_id'] == $superAdminRoleId){
				$data['reply']= array('email' => $user['system_email']);
			}
			///check if the contractor is disabled, if yes we should replace his email with enquiries email
				if($user['active']=='FALSE'){
					//get company of the contractor
					$modelUserCompanies = new Model_UserCompanies();
					$userCompany = $modelUserCompanies->getCompaniesByUserId($user['user_id']);
					$data['reply']= array('email' => $userCompany['company_enquiries_email']);
				}
                
				foreach($to as $toEmail){

			     $data['to'] = $toEmail['email'];
				 //$template_params['{unsubscribe_link}'] = $router->assemble(array('cronjob_id' => $cronjonID , 'customer_id'=> $estimate['booking']['customer_id'] , 'email'=> $toEmail['number'],'trading_name_id'=>$estimate['booking']['trading_name_id']), 'UnsubscribeEmails');
				 
				 $template_params['{unsubscribe_link}'] = 'http://cm.tilecleaners.com.au/unsubscribe-link/'.$cronjonID.'/'.$estimate['booking']['customer_id'].'/'.$toEmail['number'].'/'.$estimate_trading_name_id;
            
				try {
					EmailNotification::sendEmail($data, 'reminder_estimate', $template_params, $email_log, $companyId);
					$estimateReminded = 'done';
					} 
				catch( Zend_Mail_Transport_Exception $e ) {
					$estimateReminded = 'error';
					}
				 }	
				$this->updateById($estimate['id'], array('estimate_reminded' => $estimateReminded));
                
            }
			
          
			
        }
		
    }


    public function cronJobReminderCreatedQuotedBooking() {
		
		$modelCronjobHistory = new Model_CronjobHistory();
		$modelCronJob = new Model_CronJob();
		
		//
		// save this cron in cronjob_history table
		//
		$cronjob = $modelCronJob->getIdByName('Reminder_Created_Quoted_Booking');
		$cronjonID = $cronjob['id'];
		
		$cronjobHistoryData = array(
		'cron_job_id' => $cronjonID,
		'run_time' => time()
		);
		$cronjobHistoryId = $modelCronjobHistory->insert($cronjobHistoryData);
		//***********

        //
        // load models
        //
        $modelUser = new Model_User();
        $modelBooking = new Model_Booking();
        $modelCustomer = new Model_Customer();
        $modelBookingStatus = new Model_BookingStatus();
        $modelBookingAddress = new Model_BookingAddress();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
		$modelAuthRole = new Model_AuthRole();
		$modelMailingListUnsubscribed = new Model_MailingListUnsubscribed();
		
        $router = Zend_Controller_Front::getInstance()->getRouter();


        $quoted = $modelBookingStatus->getByStatusName('QUOTED');

        $select = $this->getAdapter()->select();

        $select->from(array('est' => $this->_name));
        $select->joinInner(array('bok' => 'booking'), 'est.booking_id = bok.booking_id', '');
        $select->where("bok.convert_status = 'estimate'");
        $select->where("bok.status_id = {$quoted['booking_status_id']}");
        $select->where("est.created_estimate_reminded != 1 ");
        $select->where("est.is_deleted = 0");
        $select->where("bok.is_deleted = 0");
        $select->where("bok.pause_emails = 0");
        $select->where("est.estimate_type = 'draft'");
		$select->where("DATEDIFF(Date(NOW()), Date(FROM_UNIXTIME(est.created))) >= 2");
		$select->where("bok.is_change = 0");//not awaiting approval
		
        $estimates = $this->getAdapter()->fetchAll($select);
		
		
		
        $this->fills($estimates, array('booking'));
		
        foreach ($estimates as $estimate) {

            $time = time();
            $twoDaysAfter = $estimate['created'] + (2 * 24 * 60 * 60);

            if ($time > $twoDaysAfter) {

                //create code
                $cancel_hashcode = sha1(uniqid());
                $modelBooking->updateById($estimate['booking_id'], array('cancel_hashcode' => $cancel_hashcode));

                $cancel_link = $router->assemble(array('cancel_hashcode' => $cancel_hashcode, 'booking_id' => $estimate['booking_id'], 'status_id' => $quoted['booking_status_id']), 'cancelBooking');

                $customer = $modelCustomer->getById($estimate['booking']['customer_id']);
                $user = $modelUser->getById($estimate['booking']['created_by']);

                // Create Attachment
                $viewParam = $this->getViewParam($estimate['id']);
                $view = new Zend_View();
                $view->setScriptPath(APPLICATION_PATH . '/modules/estimates/views/scripts/index');
                $view->bookingServices = $viewParam['bookingServices'];
                $view->thisBookingServices = $viewParam['thisBookingServices'];
                $view->priceArray = $viewParam['priceArray'];
                $view->estimate = $viewParam['estimate'];

                $bodyEstimate = $view->render('estimate.phtml');
                $pdfPath = createPdfPath();
                $destination = $pdfPath['fullDir'] . $viewParam['estimate']['estimate_num'] . '.pdf';
                wkhtmltopdf($bodyEstimate, $destination);
				$pdfAttachment = $destination;


                $template_params = array(
                    //link
                    '{cancel_link}' => '<a href="' . $cancel_link . '">' . $cancel_link . '</a>',
                    //estimate
                    '{estimate_num}' => $estimate['estimate_num'],
                    '{estimate_created}' => date('d/m/Y', $estimate['created']),
                    //booking
                    '{booking_num}' => $estimate['booking']['booking_num'],
                    '{total_without_tax}' => number_format($estimate['booking']['sub_total'], 2),
                    '{gst_tax}' => number_format($estimate['booking']['gst'], 2),
                    '{total_with_tax}' => number_format($estimate['booking']['qoute'], 2),
                    '{description}' => nl2br($estimate['booking']['description'] ? $estimate['booking']['description'] : ''),
                    '{booking_created}' => date('d/m/Y', $estimate['booking']['created']),
                    '{booking_created_by}' => ucwords($user['username']),
                    '{booking_start}' => date("F j, Y, g:i a", strtotime($estimate['booking']['booking_start'])),
                    '{booking_address}' => get_line_address($modelBookingAddress->getByBookingId($estimate['booking']['booking_id'])),
                    '{service}' => nl2br($modelContractorServiceBooking->getBookingAsText($estimate['booking']['booking_id'], true)),
                    //customer
                    '{customer_name}' => get_customer_name($customer),
                    '{customer_first_name}' => isset($customer['first_name']) && $customer['first_name'] ? ucwords($customer['first_name']) : '',
                    '{customer_last_name}' => isset($customer['last_name']) && $customer['last_name'] ? ' ' . ucwords($customer['last_name']) : '',
                    '{customer_contacts}' => nl2br($modelCustomer->getCustomerContacts($estimate['booking']['customer_id'])),
                    '{attachment}' => $bodyEstimate,
                    '{sender_name}' => ucwords($user['username'])
                );

                $to = array();
				$unsubscribeCustomerEmails = $modelMailingListUnsubscribed->getByCronJobIdAndCustomerId($cronjonID,$estimate['booking']['customer_id']);

                if ($customer['email1']&& filter_var($customer['email1'], FILTER_VALIDATE_EMAIL)) {
                    //$to[] = $customer['email1'];
					$to[0] = array('email' => $customer['email1'], 'number' => '1');

                }
                if ($customer['email2']&& filter_var($customer['email2'], FILTER_VALIDATE_EMAIL)) {
                    //$to[] = $customer['email2'];
					$to[1] = array('email' => $customer['email2'], 'number' => '2');
                }
                if ($customer['email3']&& filter_var($customer['email3'], FILTER_VALIDATE_EMAIL)) {
                    //$to[] = $customer['email3'];
					$to[2] = array('email' => $customer['email3'], 'number' => '3');
                }
				
				foreach ($unsubscribeCustomerEmails as $customerEmail) {
                if ($customer['email1'] == $customerEmail['customer_email']) {
                    unset($to[0]);
                } else if ($customer['email2'] == $customerEmail['customer_email']) {
                    unset($to[1]);
                } else if ($customer['email3'] == $customerEmail['customer_email']) {
                    unset($to[2]);
                }
             }
				
				
				
				
                //$to = implode(',', $to);

                $email_log = array('reference_id' => $estimate['id'], 'cronjob_history_id' => $cronjobHistoryId, 'type' => 'estimate');

                $companyId = $estimate['booking']['company_id'];
				
				$trading_namesObj = new Model_TradingName();
				$trading_names = $trading_namesObj->getById($estimate['booking']['trading_name_id']);
		       // By IBM
                if(!isset($estimate['booking']['trading_name_id']) || empty($estimate['booking']['trading_name_id']) || $estimate['booking']['trading_name_id'] =='' ) {
                    $estimate_trading_name_id =0;
                }else{
                    $estimate_trading_name_id =$estimate['booking']['trading_name_id'];
                }

                
                //

                $success = false;
                $data = array(
                    //'to' => $to,
                    'reply' => array('name' => $user['display_name'], 'email' => $user['email1'],),
                    'attachment' => $pdfAttachment,
					'companyId' => $companyId,
					'trading_name' => $trading_names['trading_name'],
					'from' => $trading_names['email'],	

                );
				
				///if the creator of this booking is super_admin don't send to his personal email, but send to system email
				$superAdminRoleId = $modelAuthRole->getRoleIdByName('super_admin');
				if($user['role_id'] == $superAdminRoleId){
					$data['reply']= array('email' => $user['system_email']);
				}
				///check if the contractor is disabled, if yes we should replace his email with enquiries email
				if($user['active']=='FALSE'){
					//get company of the contractor
					$modelUserCompanies = new Model_UserCompanies();
					$userCompany = $modelUserCompanies->getCompaniesByUserId($user['user_id']);
					$data['reply']= array('email' => $userCompany['company_enquiries_email']);
				}

                if ($to) {
				   
				   foreach ($to as $toEmail) {

                    $data['to'] = $toEmail['email'];
                    //$template_params['{unsubscribe_link}'] = 'http://cm.tilecleaners.com.au/unsubscribe-link/'.$cronjonID.'/'.$estimate['booking']['customer_id'].'/'.$toEmail['number'].'/'.$estimate['booking']['trading_name_id'];
                    $template_params['{unsubscribe_link}'] = 'http://temp.tilecleaners.com.au/unsubscribe-link/'.$cronjonID.'/'.$estimate['booking']['customer_id'].'/'.$toEmail['number'].'/'.$estimate_trading_name_id;

                    try {
					EmailNotification::sendEmail($data, 'reminder_estimate', $template_params, $email_log, $companyId);
					$estimateReminded = 1;
					} 
					catch( Zend_Mail_Transport_Exception $e ) {
						$estimateReminded = 0;
						}
                }
				
				
              		
                } 
				
                $this->updateById($estimate['id'], array('created_estimate_reminded' => $estimateReminded));
				
				
            }
			
        }
		
    }

    public function cronJobResetQuotedBookingReminder() {

        //
        // load models
        //
        $modelBookingStatus = new Model_BookingStatus();

        $today = time();

        $quoted = $modelBookingStatus->getByStatusName('QUOTED');

        $select = $this->getAdapter()->select();

        $select->from(array('est' => $this->_name));
        $select->joinInner(array('bok' => 'booking'), 'est.booking_id = bok.booking_id', '');
        $select->where("bok.convert_status = 'estimate'");
        $select->where("bok.is_deleted = 0");
        $select->where("bok.status_id = {$quoted['booking_status_id']}");
        $select->where("bok.to_follow < '{$today}'");
        $select->where("bok.is_to_follow = 1");
        $select->where("est.is_deleted = 0");
        $select->where("est.estimate_type = 'draft'");

        $estimates = $this->getAdapter()->fetchAll($select);

        foreach ($estimates as $estimate) {
            $this->updateById($estimate['id'], array('estimate_reminded' => 'none'));
        }
    }

    public function updateFullTextSearch($estimateId) {

        $estimate = $this->getById($estimateId);

        $fullTextSearch = array();

        // estimate        
        $fullTextSearch[] = trim($estimate['estimate_num']);

        // booking
        $model_Booking = new Model_Booking();
        $modelBookingStatus = new Model_BookingStatus();
        $bookingId = $estimate['booking_id'];
        $booking = $model_Booking->getById($bookingId);
        $fullTextSearch[] = trim($booking['booking_num']);
        $fullTextSearch[] = trim($booking['title']);
        $fullTextSearch[] = trim($booking['description']);
        $status = $modelBookingStatus->getById($booking['status_id']);
        $fullTextSearch[] = trim($status['name']);


        // address
        $modelBookingAddress = new Model_BookingAddress();
        $bookingAddress = $modelBookingAddress->getByBookingId($bookingId);
        $fullTextSearch[] = trim(get_line_address($bookingAddress));

        //customer
        $modelCustomer = new Model_Customer();
        $fullTextCustomer = $modelCustomer->getFullTextCustomerContacts($booking['customer_id']);
        $fullTextSearch[] = trim($fullTextCustomer);

        // services
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $fullTextContractorServiceBooking = $modelContractorServiceBooking->getFullTextContractorServiceBooking($bookingId);
        if ($fullTextContractorServiceBooking) {
            $fullTextSearch[] = trim($fullTextContractorServiceBooking);
        }

        //inquiry
        if (isset($booking['original_inquiry_id']) && $booking['original_inquiry_id']) {
            $modelInquiry = new Model_Inquiry();
            $Inquiry = $modelInquiry->getById($booking['original_inquiry_id']);
            if ($Inquiry['inquiry_num']) {
                $fullTextSearch[] = trim($Inquiry['inquiry_num']);
            }
        }

        //invoice
        $modelBookingInvoice = new Model_BookingInvoice();
        $invoice = $modelBookingInvoice->getByBookingId($bookingId);
        if (isset($invoice['invoice_num']) && $invoice['invoice_num']) {
            $fullTextSearch[] = trim($invoice['invoice_num']);
        }
        if (isset($invoice['invoice_type']) && $invoice['invoice_type']) {
            $fullTextSearch[] = trim($invoice['invoice_type']);
        }

        //complaint
        $model_Complaint = new Model_Complaint();
        $complaints = $model_Complaint->getFullTextComplaintByBookingId($bookingId);
        if ($complaints) {
            $fullTextSearch[] = trim($complaints);
        }

        $data = array();
        $data['full_text_search'] = implode(' ', $fullTextSearch);

        $this->updateById($estimateId, $data, false);
    }

    public function deleteRelatedEstimate($id) {

        //delete data from estimate_label
        $this->getAdapter()->delete('estimate_label', "estimate_id = '{$id}'");

        //delete data from estimate_discussion
        $this->getAdapter()->delete('estimate_discussion', "estimate_id = '{$id}'");
    }

    public function getViewParam($estimateId) {
        //
        // load model
        //
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();

        //
        // get estimate data
        //
        $estimate = $this->getById($estimateId);
        $this->fill($estimate, array('booking', 'customer_commercial_info', 'customer_contacts'));
        $bookingServices = $modelContractorServiceBooking->getByBookingId($estimate['booking_id']);

        $thisBookingServices = array();
        $priceArray = array();

        foreach ($bookingServices as $bookingService) {

            $serviceId = $bookingService['service_id'];
            $clone = $bookingService['clone'];
            $bookingId = $bookingService['booking_id'];

            $service_and_clone = $serviceId . ($clone ? '_' . $clone : '');

            $thisBookingServices[] = $service_and_clone;

            $priceArray[$service_and_clone] = $modelContractorServiceBooking->getTotalServiceBookingQoute($bookingId, $serviceId, $clone);
        }

        $viewParam = array();

        $viewParam['estimate'] = $estimate;
        $viewParam['bookingServices'] = $bookingServices;
        $viewParam['thisBookingServices'] = $thisBookingServices;
        $viewParam['priceArray'] = $priceArray;

        return $viewParam;
    }

    public function getEstimateViewParam($estimateId, $isTemp = false) {

        $viewParam = array();

        $estimate = $this->getById($estimateId);
        if ($estimate) {

            // fill estimate
            $this->fill($estimate, array('booking', 'customer_commercial_info', 'customer_contacts'));

            //
            // load model
            //
            $modelBooking = new Model_Booking();
            $modelCustomerType = new Model_CustomerType();
            $modelBookingAttachment = new Model_BookingAttachment();
            $modelContractorServiceBooking = new Model_ContractorServiceBooking();
            $modelContractorServiceBookingTemp = new Model_ContractorServiceBookingTemp();

            $contractorServiceBookings = $modelContractorServiceBooking->getByBookingId($estimate['booking_id']);
            $contractorServiceBookingsTemp = $modelContractorServiceBookingTemp->getByBookingId($estimate['booking_id']);

            $thisBookingServices = array();
            $priceArray = array();
            $bookingServices = array();
            if ($contractorServiceBookingsTemp && !$modelBooking->checkCanEditBookingDetails($estimate['booking_id']) && !$isTemp) {
                $bookingServices = $contractorServiceBookingsTemp;
                foreach ($bookingServices as $bookingService) {

                    $serviceId = $bookingService['service_id'];
                    $clone = $bookingService['clone'];
                    $bookingId = $bookingService['booking_id'];

                    $service_and_clone = $serviceId . ($clone ? '_' . $clone : '');

                    $thisBookingServices[] = $service_and_clone;

                    $priceArray[$service_and_clone] = $modelContractorServiceBookingTemp->getTotalServiceBookingQoute($bookingId, $serviceId, $clone);
                }
            } elseif ($contractorServiceBookings) {
                $bookingServices = $contractorServiceBookings;
                foreach ($bookingServices as $bookingService) {

                    $serviceId = $bookingService['service_id'];
                    $clone = $bookingService['clone'];
                    $bookingId = $bookingService['booking_id'];

                    $service_and_clone = $serviceId . ($clone ? '_' . $clone : '');

                    $thisBookingServices[] = $service_and_clone;

                    $priceArray[$service_and_clone] = $modelContractorServiceBooking->getTotalServiceBookingQoute($bookingId, $serviceId, $clone);
                }
            }

            // customer type work order is_required Message
            $isWorkOrder = false;
            $workOrder = $modelCustomerType->getCustomerTypeIsWorkOrder();

            if (in_array($estimate['customer']['customer_type_id'], $workOrder)) {
                $bookingAttachments = $modelBookingAttachment->getByBookingIdOrInquiryId($bookingId, $estimate['booking']['original_inquiry_id']);
                $isWorkOrder = true;
                if (!empty($bookingAttachments)) {
                    foreach ($bookingAttachments as $attachment) {
                        if ($attachment['work_order'] == 1) {
                            $isWorkOrder = false;
                        }
                    }
                }
            }

            //return view param
            $viewParam['estimate'] = $estimate;
            $viewParam['bookingServices'] = $bookingServices;
            $viewParam['thisBookingServices'] = $thisBookingServices;
            $viewParam['priceArray'] = $priceArray;
            $viewParam['isTemp'] = $isTemp;
            $viewParam['isWorkOrder'] = $isWorkOrder;
            $viewParam['customer'] = $estimate['customer'];
            $viewParam['customer_commercial_info'] = $estimate['customer_commercial_info'];
        }

        return $viewParam;
    }
	////By Nour
	
public function	getWheresAndJoinsByFilters($filters, $isCronJob = false){
	  $wheres = array();
        $joinInner = array();
		
		 if (!$isCronJob) {
		 $filters['company_id'] = CheckAuth::getCompanySession();
		 
		  if (CheckAuth::checkCredential(array('canSeeDeletedEstimate'))) {
            if (!empty($filters['is_deleted'])) {
				 $wheres['is_deleted'] = 'est.is_deleted = 1';
               
            } else {
				 $wheres['is_deleted'] = 'est.is_deleted = 0';
               
            }
        } else {
			 $wheres['is_deleted'] = 'est.is_deleted = 0';
          
        }

        if (!CheckAuth::checkCredential(array('canSeeAllEstimate'))) {
            if (CheckAuth::checkCredential(array('canSeeOnlyHisEstimate'))) {
                if (CheckAuth::checkCredential(array('canSeeAssignedEstimate'))) {

                    $joinInner['contractor_service_booking'] = array('name' => array('csb' => 'contractor_service_booking'), 'cond' => 'est.booking_id = csb.booking_id', 'cols' => '');
                    $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');

			      $wheres['can_see'] = ("bok.created_by = {$this->loggedUser['user_id']} OR csb.contractor_id = {$this->loggedUser['user_id']}");

                   
                } else {
                    $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');
					   $wheres['can_see'] = ("bok.created_by = {$this->loggedUser['user_id']}");

                }
            } else {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');

					   $wheres['can_see'] = ("bok.created_by = {$this->loggedUser['user_id']}");


				}
        }
		
		 }
		 

        if ($filters) {
            if (!empty($filters['keywords'])) {
                $keywords = $this->getAdapter()->quote('%' . trim($filters['keywords']) . '%');
                
				$joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');
				
				                $wheres['keywords'] = ("est.full_text_search LIKE {$keywords} OR bok.full_text_search LIKE {$keywords} ");

				
				
            }
			
			

            if (!empty($filters['estimate_num'])) {
                $estimate_num = $this->getAdapter()->quote('%' . trim($filters['estimate_num']) . '%');
				 $wheres['estimate_num'] = ("est.estimate_num LIKE {$estimate_num}");

            }

            if (!empty($filters['booking_num'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');
                $booking_num = $this->getAdapter()->quote('%' . trim($filters['booking_num']) . '%');
              
			 $wheres['booking_num'] = ("bok.booking_num LIKE {$booking_num}");

            }

            if (!empty($filters['estimate_type'])) {
                if ($filters['estimate_type'] != 'all') {
                    $estimate_type = $this->getAdapter()->quote(trim($filters['estimate_type']));
					 $wheres['estimate_type'] = ("est.estimate_type = {$estimate_type}");

                }
            }

            if (!empty($filters['customer_id'])) {
                $filters['customer_id'] = (int) $filters['customer_id'];
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');
			   $wheres['customer_id'] = ("bok.customer_id = {$filters['customer_id']} ");

            }

            if (!empty($filters['created_by'])) {
                $user_id = (int) $filters['created_by'];
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');
			    $wheres['created_by'] = ("bok.created_by = '{$user_id}'");

            }

            if (!empty($filters['contractor_id'])) {
                $contractor_id = (int) $filters['contractor_id'];
                $joinInner['contractor_service_booking'] = array('name' => array('csb' => 'contractor_service_booking'), 'cond' => 'est.booking_id = csb.booking_id', 'cols' => '');
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');
				$wheres['contractor_id'] = ("bok.created_by = {$contractor_id} OR csb.contractor_id = {$contractor_id}");

				
            }

            if (!empty($filters['created_from'])) {
                $created_from = $this->getAdapter()->quote(strtotime(trim($filters['created_from'])));
				 $wheres['created_from'] = ("est.created >= {$created_from}");

            }

            if (!empty($filters['created_to'])) {
                $created_to = $this->getAdapter()->quote(strtotime(trim($filters['created_to'])));
		    	 $wheres['created_to'] = ("est.created <= {$created_to}");

            }

            if (!empty($filters['estimate_label_ids']) && $filters['estimate_label_ids']) {
                $joinInner['estimate_label'] = array('name' => array('el' => 'estimate_label'), 'cond' => 'est.id = el.estimate_id', 'cols' => '');
                $select->where('el.label_id IN (' . implode(', ', $filters['estimate_label_ids']) . ')');
						    	 $wheres['estimate_label_ids'] = ('el.label_id IN (' . implode(', ', $filters['estimate_label_ids']) . ')');

            }

            if (!empty($filters['service_id'])) {
                $joinInner['contractor_service_booking'] = array('name' => array('csb' => 'contractor_service_booking'), 'cond' => 'est.booking_id = csb.booking_id', 'cols' => '');
                $service_id = (int) $filters['service_id'];
			    $wheres['service_id'] = ("csb.service_id = {$service_id}");

            }

            if (!empty($filters['status'])) {
                $status = $this->getAdapter()->quote(trim($filters['status']));
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');
							    $wheres['status'] = ("bok.status_id = {$status}");

            }

            if (!empty($filters['city_id'])) {
                $city_id = (int) $filters['city_id'];
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');
											    $wheres['city_id'] = ("bok.city_id = {$city_id}");

            }

            if (!empty($filters['booking_start'])) {
                $bookingStart = $this->getAdapter()->quote(trim($filters['booking_start']));
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');
				 $wheres['booking_start'] = ("bok.booking_start >= {$bookingStart}");

            }

            if (!empty($filters['booking_end'])) {
                $bookingEnd = $this->getAdapter()->quote(trim($filters['booking_end']));
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');
				 $wheres['booking_end'] = ("bok.booking_end <= {$bookingEnd}");

            }
		

            if (!empty($filters['qoute_from'])) {
                $qoute_from = $this->getAdapter()->quote(trim($filters['qoute_from']));
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');
		    	 $wheres['qoute_from'] = ("bok.qoute >= {$qoute_from}");


				
            }

            if (!empty($filters['qoute_to'])) {
                $qoute_to = $this->getAdapter()->quote(trim($filters['qoute_to']));
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');
						    	 $wheres['qoute_to'] = ("bok.qoute <= {$qoute_to}");

            }

            if (!empty($filters['company_id'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');

                $company_id = (int) $filters['company_id'];
										    	 $wheres['company_id'] = ("bok.company_id = {$company_id}");

            }

            if (!empty($filters['address'])) {
                $address = trim($filters['address']);
                $joinInner['booking_address'] = array('name' => array('ba' => 'booking_address'), 'cond' => 'est.booking_id = ba.booking_id', 'cols' => '');
	    	 $wheres['address'] = ("CONCAT_WS(' ',ba.street_number ,ba.street_address ,ba.suburb,ba.state,ba.postcode) LIKE '%{$address}%'");

            }

            if (!empty($filters['email'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');
                $joinInner['customer'] = array('name' => array('c' => 'customer'), 'cond' => 'bok.customer_id = c.customer_id', 'cols' => '');
                $email = trim($filters['email']);
		    	 $wheres['email'] = ("CONCAT_WS(' ',email1 ,email2 ,email3) LIKE '%{$email}%'");

            }

            if (!empty($filters['mobile/phone'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');
                $joinInner['customer'] = array('name' => array('c' => 'customer'), 'cond' => 'bok.customer_id = c.customer_id', 'cols' => '');
                $mobile_phone = trim($filters['mobile/phone']);
										 $wheres['mobile/phone'] = ("CONCAT_WS(' ',phone1 ,phone2 ,phone3,mobile1 ,mobile2 ,mobile3) LIKE '%{$mobile_phone}%'");

            }

            if (!empty($filters['bussiness_name'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');
                $joinInner['customer_commercial_info'] = array('name' => array('cci' => 'customer_commercial_info'), 'cond' => 'bok.customer_id = cci.customer_id', 'cols' => '');
			 	 $wheres['bussiness_name'] = ("cci.business_name LIKE '%{$filters['bussiness_name']}%'");

            }

            if (!empty($filters['my_estimates'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');

                $user_id = (int) $filters['my_estimates'];

                $my_booking_sql = $this->getAdapter()->select();
                $my_booking_sql->from('booking_log', 'booking_id');
				                $joinInner['booking_log'] = array('name' => array('log' => 'booking_log'), 'cond' => 'bok.booking_id = log.booking_id', 'cols' => '');

				            $wheres['log_user_id']=("log.log_user_id = {$user_id}");

			  

               
                $my_booking_sql->distinct();
                $my_booking_results = $this->getAdapter()->fetchAll($my_booking_sql);

                $not_my_booking_sql = $this->getAdapter()->select();
                $not_my_booking_sql->from('booking_log', 'booking_id');
                $not_my_booking_sql->distinct();

                if ($my_booking_results) {
                    $my_booking_ids = array();

                    foreach ($my_booking_results as $my_result) {
                        $my_booking_ids[$my_result['booking_id']] = $my_result['booking_id'];
                    }

                    //$not_my_booking_sql->where('booking_id NOT IN (' . implode(', ', $my_booking_ids) . ')');
								   $wheres['booking_id'] = ('bok.booking_id NOT IN (' . implode(', ', $my_booking_ids) . ')');

                }

                $not_my_results = $this->getAdapter()->fetchAll($not_my_booking_sql);

                $not_my_booking_ids = array();

                foreach ($not_my_results as $not_my_result) {
                    $not_my_booking_ids[$not_my_result['booking_id']] = $not_my_result['booking_id'];
                }

                if ($not_my_booking_ids) {
                   // $select->where('bok.booking_id NOT IN (' . implode(', ', $not_my_booking_ids) . ')');
						  $wheres['my_booking_id'] = ('bok.booking_id NOT IN (' . implode(', ', $my_booking_ids) . ')');

					
                }
            }
			//////////// get all visited estimates
			if (!empty($filters['visited']) && $filters['visited'] == true) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');
                //$select->where("bok.visited = 1");
				 $wheres['visited'] =("bok.visited = 1");
                
            }
			///////////By Islam get visited or not visited estimates from droplist filter
			if(!empty($filters['is_visited'])){
			if ($filters['is_visited']==1) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');
                //$select->where("bok.visited =0");
								 $wheres['is_visited'] =("bok.visited =0");

            }
			if ($filters['is_visited']==2) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');
               // $select->where("bok.visited =1");
												 $wheres['is_visited_booking'] =("bok.visited =1");

            }
			}
			///////////
			
			
			

            if (!empty($filters['to_follow']) && $filters['to_follow'] == true) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');
                //$select->where("bok.is_to_follow = 1");
				 $wheres['is_to_follow'] =("bok.is_to_follow = 1");

               // $select->where("bok.convert_status = 'estimate'");
				 $wheres['convert_status'] =("bok.convert_status = 'estimate'");

            }

            if (!empty($filters['to_follow_date'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');
                switch ($filters['to_follow_date']) {
                    case 'yesterday':
                        $yesterday = getTimePeriodByName('yesterday');
                       // $select->where("bok.to_follow between '" . strtotime($yesterday['start']) . "' and '" . strtotime($yesterday['end']) . "'");
					 $wheres['to_follow between_yesterday'] =("bok.to_follow between '" . strtotime($yesterday['start']) . "' and '" . strtotime($yesterday['end']) . "'");

                        break;
                    case 'today':
                        $today = getTimePeriodByName('today');
                      //  $select->where("bok.to_follow between '" . strtotime($today['start']) . "' and '" . strtotime($today['end']) . "'");
						                 $wheres['to_follow between_today']=("bok.to_follow between '" . strtotime($today['start']) . "' and '" . strtotime($today['end']) . "'");

                        break;
                    case 'tomorrow':
                        $tomorrow = getTimePeriodByName('tomorrow');
                       
					  // $select->where("bok.to_follow between '" . strtotime($tomorrow['start']) . "' and '" . strtotime($tomorrow['end']) . "'");
                        $wheres['to_follow between_tomorrow']=("bok.to_follow between '" . strtotime($tomorrow['start']) . "' and '" . strtotime($tomorrow['end']) . "'");

                        break;
                    case 'past':
                        $today = getTimePeriodByName('today');
                        $wheres['past']=("bok.to_follow < '" . strtotime($today['start']) . "'");
                        break;
                    case 'future':
                        $today = getTimePeriodByName('today');
                       // $select->where("bok.to_follow > '" . strtotime($today['end']) . "'");
					   $wheres['future']= ("bok.to_follow > '" . strtotime($today['end']) . "'");
                        break;
                }
            }

            if (!empty($filters['inquiry_num'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');
                $joinInner['inquiry'] = array('name' => array('i' => 'inquiry'), 'cond' => 'bok.original_inquiry_id = i.inquiry_id', 'cols' => '');
                $inquiry_num = $this->getAdapter()->quote('%' . trim($filters['inquiry_num']) . '%');
               // $select->where("i.inquiry_num LIKE {$inquiry_num}");
				  $wheres['inquiry_num']= ("i.inquiry_num LIKE {$inquiry_num}");
            }
        // filter
		

    }
	

		
	
 return array('wheres' => $wheres, 'joinInner' => $joinInner);	
	
}









public function getCount($filters = array()){
		
		
		$select = $this->getAdapter()->select();
        $select->from(array('est' => $this->_name),array('count'=>'count(DISTINCT(est.booking_id))'));
		


        $wheresAndJoins = $this->getWheresAndJoinsByFilters($filters);
		
        $joinInner = $wheresAndJoins['joinInner'];
        $wheres = $wheresAndJoins['wheres'];
		

        if ($wheres) {
            foreach ($wheres as $where) {
				$select->where($where);
            }
        }
		

        if ($joinInner) {
            foreach ($joinInner as $inner) {
			    $select->joinInner($inner['name'], $inner['cond'], $inner['cols']);
            }
        }
		
		 
		return $this->getAdapter()->fetchAll($select);
		
	}


	///End

}