<?php

class Model_BookingContractorPayment extends Zend_Db_Table_Abstract {

    protected $_name = 'booking_contractor_payment';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
	public function test22(){
	$select = $this->getAdapter()->select();
        $select->from(array('bcp' => $this->_name),array('sum'=>'sum(payment_to_contractor)','INV#'=>'contractor_invoice_num','contractor_id'));
		$select->where('payment_to_contractor_id= 0 and contractor_invoice_num is not null ');
		$select->group('contractor_invoice_num');
		$sql = $select->__toString();
		echo "$sql\n";
		return $this->getAdapter()->fetchAll($select);
		
	}
	
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from(array('bcp' => $this->_name));
        $select->order($order);

        $joinInner = array();

        if ($filters) {
            if (!empty($filters['booking_id'])) {
                $select->where("bcp.booking_id = {$filters['booking_id']}");
            }
            if (!empty($filters['contractor_id'])) {
                $select->where("bcp.contractor_id = {$filters['contractor_id']}");
            }
            if (!empty($filters['booking_start_between'])) {
                $bookingStartBetween = $filters['booking_start_between'];
                $bookingEndBetween = !empty($filters['booking_end_between']) ? $filters['booking_end_between'] : date('Y-m-d H:i:s', time());
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'bcp.booking_id = bok.booking_id', 'cols' => '');
                $select->where("bok.booking_start between '" . $bookingStartBetween . "' and '" . $bookingEndBetween . "'");
            }
        }

        if ($joinInner) {
            foreach ($joinInner as $inner) {
                $select->joinInner($inner['name'], $inner['cond'], $inner['cols']);
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table rows according to the assigned id and data
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    
	public function getBookingIdsByInvNumber($invNum){
		$select = $this->getAdapter()->select();
        $select->from(array('bcp' => $this->_name),array('booking_id'));
		$select->where("bcp.contractor_invoice_num = '{$invNum}'");
		return $this->getAdapter()->fetchAll($select);
	}
	public function getBookingIdsByPaymentToContractorId($payment_to_contractor_id){
		$payment_to_contractor_id = (int) $payment_to_contractor_id;
		$select = $this->getAdapter()->select();
        $select->from(array('bcp' => $this->_name),array('booking_id'));
		$select->where("bcp.payment_to_contractor_id = {$payment_to_contractor_id}");
		return $this->getAdapter()->fetchAll($select);
	}
	
	public function updateById($id, $data) {
        $id = (int) $id;
        $updated_id= parent::update($data, "id = '{$id}'");
		
		//D.A 31/09/2015 Remove Invoice Cache
		if($updated_id){
				$bookingContractorPayment = $this->getById($id);	
				require_once 'Zend/Cache.php';
				$company_id = CheckAuth::getCompanySession();
				$modelBookingInvoice = new Model_BookingInvoice();
				$invoice = $modelBookingInvoice->getByBookingId($bookingContractorPayment['booking_id']);
				$invoicePaymentToTechnicianCacheID= $invoice['id'].'_invoicePaymentToTechnician';
				$invoiceViewDir=get_config('cache').'/'.'invoicesView'.'/'.$company_id;	
				if (!is_dir($invoiceViewDir)) {
					mkdir($invoiceViewDir, 0777, true);
				}			
				$invoiceFrontEndOption= array('lifetime'=> NULL,
				'automatic_serialization'=> true);
				$invoiceBackendOptions = array('cache_dir'=>$invoiceViewDir );
				$invoiceCache = Zend_Cache::factory('Core','File',$invoiceFrontEndOption,$invoiceBackendOptions);			
				$invoiceCache->remove($invoicePaymentToTechnicianCacheID);			
		}
	return $updated_id;			
    }
	
	public function updateBybookingIdAndContractorId($bookingId, $contractorId, $data) {
        $bookingId = (int) $bookingId;
        $contractorId = (int) $contractorId;
        		
        return parent::update($data, "booking_id = '{$bookingId}' and contractor_id = '{$contractorId}'");
    }
	
	public function updateByInvoiceNumber($invoiceNumber, $data) {
        
        		
        return parent::update($data, "contractor_invoice_num = '{$invoiceNumber}' ");
    }

    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
   public function deleteById($id) {
        $id = (int) $id;
		$bookingContractorPayment = $this->getById($id);
		$modelLogUser = new Model_LogUser();
		$deleted = parent::delete("id = '{$id}'");
		if($deleted){
			$booking_id = $bookingContractorPayment['booking_id'];
			$item_type = 'payment_to_contractor_by_id';
			$event = 'deleted';				
			$modelLogUser->addUserLogEvent($booking_id, $item_type, $event);
			
			//D.A 29/09/2015 Remove Invoice Services Cache
			require_once 'Zend/Cache.php';
			$company_id = CheckAuth::getCompanySession();
			$modelBookingInvoice = new Model_BookingInvoice();
			$invoice = $modelBookingInvoice->getByBookingId($booking_id);
			$invoicePaymentToTechnicianCacheID= $invoice['id'].'_invoicePaymentToTechnician';
			$invoiceViewDir=get_config('cache').'/'.'invoicesView'.'/'.$company_id;	
			if (!is_dir($invoiceViewDir)) {
				mkdir($invoiceViewDir, 0777, true);
			}			
			$invoiceFrontEndOption= array('lifetime'=> NULL,
			'automatic_serialization'=> true);
			$invoiceBackendOptions = array('cache_dir'=>$invoiceViewDir );
			$invoiceCache = Zend_Cache::factory('Core','File',$invoiceFrontEndOption,$invoiceBackendOptions);			
			$invoiceCache->remove($invoicePaymentToTechnicianCacheID);		
		}
        return $deleted;
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getBybookingIdAndContractorId($bookingId, $contractorId) {
        $bookingId = (int) $bookingId;
        $contractorId = (int) $contractorId;
        $select = $this->getAdapter()->select();
        $select->from(array('bcp' => $this->_name));
		$select->joinLeft(array('ptoc' => 'payment_to_contractor'),'ptoc.payment_to_contractor_id= bcp.payment_to_contractor_id and ptoc.contractor_id = bcp.contractor_id',array('contractor_invoice_num' ,'amount_paid'));		
        $select->where("bcp.booking_id = '{$bookingId}'");
        $select->where("bcp.contractor_id = '{$contractorId}'");
        
        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getTotalPaymentByContractorId($contractorId, $filters=array()) {
        $contractorId = (int) $contractorId;

        $filters['contractor_id'] = $contractorId;

        $results = $this->getAll($filters);
       
        $totalPayment = 0;
        foreach ($results as $result) {
            $totalPayment = $totalPayment + $result['payment_to_contractor'];
        }

        return $totalPayment;
    }

	
	public function getTotalPaymentByPaymentToContractorId($paymentToContractorId){
		$paymentToContractorId = (int) $paymentToContractorId;
        $select = $this->getAdapter()->select();
        $select->from($this->_name,array('calculated_amount'=>"sum(payment_to_contractor)"));
        $select->where("payment_to_contractor_id = '{$paymentToContractorId}'");

        return $this->getAdapter()->fetchRow($select);
	
	
	}
}
