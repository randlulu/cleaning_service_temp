<?php

class Model_ItemImageDiscussion extends Zend_Db_Table_Abstract {

    protected $_name = 'item_image_discussion';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    
	/*public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "image_id = '{$id}'");
    }

	public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("image_id = '{$id}'");
    }*/

	
	
	public function getMaxGroup(){
	   
	    $select = $this->getAdapter()->select();
        $select->from($this->_name,array('group_id'));
		return $this->getAdapter()->fetchAll($select);	
	}
	
	public function getAll($filters = array()){

	    $select = $this->getAdapter()->select();
        //$select->from($this->_name);
		$select->from(array('iid'=>$this->_name));
		if(isset($filters)){		  
			if(isset($filters['group_id'])){
				$group_id = (int) $filters['group_id'];
				$select->where("iid.group_id = '{$group_id}'");		  
			}
			if(isset($filters['type'])){
				$typeFilter = $this->getAdapter()->quote($filters['type'] . '%');
				$select->where("iid.type LIKE {$typeFilter}");
			}
			if(isset($filters['discussion_group_id'])){
				$group_id = (int) $filters['discussion_group_id'];
				$select->where("iid.group_id = '{$group_id}'");
				$select->group('iid.group_id');
			}

			if(isset($filters['image_id'])){
				$id = (int) $filters['image_id'];
				$select->where(" iid.image_id = '{$id}' ");
				//$select->group('iid.group_id');
			}
		}
		return $this->getAdapter()->fetchAll($select);
	}
	
	public function getDistinctGroups(){
		$typeFilter = $this->getAdapter()->quote('booking' . '%');	
		$select = $this->getAdapter()->select();
		$select->distinct();
		$select->from(array('iid'=>$this->_name), 'group_id');
		$select->where("iid.group_id > 0");	
		$select->where("iid.type LIKE {$typeFilter}");
		//echo $select->__toString();
		return $this->getAdapter()->fetchAll($select);
	}
	
	public function getGroupDiscussions($group_id){
		$id = (int) $group_id;
		$select = $this->getAdapter()->select();
        $select->from(array('bd' => 'booking_discussion'));
		$typeFilter = $this->getAdapter()->quote('booking' . '%');	
		$select->joinLeft(array('iid' => $this->_name), " iid.item_id = bd.discussion_id AND iid.type LIKE {$typeFilter}",array('group_id'));
		$select->where("iid.group_id = {$id}");
		$select->order(array('bd.booking_id','iid.group_id'));
		//echo $select->__toString();
		return $this->getAdapter()->fetchAll($select);
	}
	
	public function getByDiscussionIdAndType($id, $type){
		$id = (int) $id;
		$typeFilter = $this->getAdapter()->quote($type . '%');
		$select = $this->getAdapter()->select();
		$select->from(array('iid'=>$this->_name));
		$select->where("iid.item_id = '{$id}'");
		$select->where("iid.type LIKE {$typeFilter}");		
		return $this->getAdapter()->fetchAll($select);
	}
	
	public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "item_image_discussion_id = '{$id}'");
    }
    
    public function test(){
        $bookingModelObj = new Model_Booking();
        $modelBookingDiscussion = new Model_BookingDiscussion();
        $modelComplaintDiscussion = new Model_ComplaintDiscussion();

            $filters = array('contractor_id' => 8,'get_complaint'=>'yes');
            $allBookingsOfContractor = $bookingModelObj->getAll($filters);
            $allBookingIdsOfContractor = array();
            $allComplaintIdsOfContractor = array();
            foreach ($allBookingsOfContractor as $booking){
                 $allBookingIdsOfContractor[] = (int)$booking['booking_id'];
                 if(isset($booking['complaint_id']) && $booking['complaint_id'] != Null && !empty($booking['complaint_id'])){
                     $allComplaintIdsOfContractor[] = (int)$booking['complaint_id'];
                 }

            }
        $BookingDiscussions = $modelBookingDiscussion->getByBookingIds($allBookingIdsOfContractor,'desc');	
        $ComplaintDiscussions = $modelComplaintDiscussion->getByComplaintIds($allComplaintIdsOfContractor,'desc');	


        foreach ($BookingDiscussions as $row) {
            $BookingIds[]  = $row['booking_id'];
        }
        foreach ($ComplaintDiscussions as $row) {
            $ComplaintIds[]  = $row['complaint_id'];
        }
        
        $select1 = $this->getAdapter()->select();
        $select1->from($this->_name, array('item_id'));
		$select1->where('item_id IN (?)', $BookingIds);  
        $select1->where("type = 'booking'");
        
        $BookingDiscussionImages = $this->getAdapter()->fetchAll($select1);
        
        $BookingDiscussionImagesId = array();
        foreach ($BookingDiscussionImages as $row) {
                $BookingDiscussionImagesId[]  = $row['item_id'];
            }
        
        $select2 = $this->getAdapter()->select();
        $select2->from($this->_name, array('item_id'));
		$select2->where('item_id IN (?)', $ComplaintIds);  
        $select2->where("type = 'complaint'");
        
		$ComplaintDiscussionImages = $this->getAdapter()->fetchAll($select2);
        
        $ComplaintDiscussionImagesId = array();
        foreach ($ComplaintDiscussionImages as $row) {
                $ComplaintDiscussionImagesId[]  = $row['item_id'];
            }
        
        foreach ($BookingDiscussions as &$row) {
            if (in_array($row['booking_id'], $BookingDiscussionImagesId)){
                $row['images'] = 'true';
            }
            else{
                $row['images'] = 'false';
            }
        }
        
        foreach ($ComplaintDiscussions as &$row) {
            if (in_array($row['complaint_id'], $ComplaintDiscussionImagesId)){
                $row['images'] = 'true';
            }
            else{
                $row['images'] = 'false';
            }
        }
        
        
        
        $mergedArray = array_merge($BookingDiscussions, $ComplaintDiscussions);
        
        foreach ($mergedArray as $key => $row) {
            $created[$key]  = $row['created'];
        }
        
        array_multisort($created, SORT_DESC, $mergedArray);

       return $mergedArray;
        
    }
    
  public function getAllDiscussionByContractor($logged_user_id){

        $BookingDetails = array();
        $ComplaintDetails = array();
        $bookingModelObj = new Model_Booking();
        $modelBookingDiscussion = new Model_BookingDiscussion();
        $modelComplaintDiscussion = new Model_ComplaintDiscussion();
      
      $status_colors = array(
            '0' => '#888888',
            '1' => '#cc3333',
            '2' => '#dd4477',
            '3' => '#994499',
            '4' => '#6633cc',
            '5' => '#336699',
            '6' => '#3366cc',
            '7' => '#22aa99',
            '8' => '#329262',
            '9' => '#109618',
            '10' => '#66aa00',
            '11' => '#aaaa11',
            '12' => '#d6ae00',
            '13' => '#ee8800',
            '14' => '#dd5511',
            '15' => '#a87070',
            '16' => '#8c6d8c',
            '17' => '#627487',
            '18' => '#7083a8',
            '19' => '#5c8d87',
            '20' => '#898951',
            '21' => '#b08b59',
            '-1' => '#7a367a'
        );
      

            $modelContractorDiscussion = new Model_ContractorDiscussionMongo();
            $lastContractorDiscussion =  $modelContractorDiscussion->getByContractorIdthelast($logged_user_id);
      
      $lastContractorDiscussion[0]['is_audio'] = isset($lastContractorDiscussion['audio_path']) && !empty($lastContractorDiscussion['audio_path']) ? 'true' : 'false';
      $lastContractorDiscussion[0]['images'] = isset($lastContractorDiscussion['thumbnail_images']) && !empty($lastContractorDiscussion['thumbnail_images']) ? 'true' : 'false';
      
            $filters = array('contractor_id' => $logged_user_id,'get_complaint'=>'yes', 'get_status'=>'yes','get_address'=>'yes');
            $allBookingsOfContractor = $bookingModelObj->getAll($filters);
            $allBookingIdsOfContractor = array();
            $allComplaintIdsOfContractor = array();
            foreach ($allBookingsOfContractor as $booking){
                 $allBookingIdsOfContractor[] = (int)$booking['booking_id'];
                 $bookingId = $booking['booking_id'];
                 $BookingDetails[$bookingId]['booking_num'] = $booking['booking_num'];
                 $BookingDetails[$bookingId]['color'] = $status_colors[$booking['color']];
                 $BookingDetails[$bookingId]['address'] = get_line_address($booking);
                 if(isset($booking['complaint_id']) && $booking['complaint_id'] != Null && !empty($booking['complaint_id'])){
                     $allComplaintIdsOfContractor[] = (int)$booking['complaint_id'];
                     $complaintId = $booking['complaint_id'];
                     $ComplaintDetails[$complaintId]['booking_num'] = $booking['booking_num'];
                     $ComplaintDetails[$complaintId]['color'] = $status_colors[$booking['color']];
                     $ComplaintDetails[$complaintId]['address'] = get_line_address($booking);
                     $ComplaintDetails[$complaintId]['complaint_num'] = $booking['complaint_num'];
                 }

            }
        $BookingDiscussions = $modelBookingDiscussion->getByBookingIds($allBookingIdsOfContractor,'desc');	
        $ComplaintDiscussions = $modelComplaintDiscussion->getByComplaintIds($allComplaintIdsOfContractor,'desc');	


        foreach ($BookingDiscussions as $row) {
            $BookingIds[]  = $row['booking_id'];
            
            
        }
        foreach ($ComplaintDiscussions as $row) {
            $ComplaintIds[]  = $row['complaint_id'];
            
        }
        
        $select1 = $this->getAdapter()->select();
        $select1->from($this->_name, array('item_id'));
		$select1->where('item_id IN (?)', $BookingIds);  
        $select1->where("type = 'booking'");
        
        $BookingDiscussionImages = $this->getAdapter()->fetchAll($select1);
        
        $BookingDiscussionImagesId = array();
        foreach ($BookingDiscussionImages as $row) {
                $BookingDiscussionImagesId[]  = $row['item_id'];
            }
        
        $select2 = $this->getAdapter()->select();
        $select2->from($this->_name, array('item_id'));
		$select2->where('item_id IN (?)', $ComplaintIds);  
        $select2->where("type = 'complaint'");
        
		$ComplaintDiscussionImages = $this->getAdapter()->fetchAll($select2);
        
        $ComplaintDiscussionImagesId = array();
        foreach ($ComplaintDiscussionImages as $row) {
                $ComplaintDiscussionImagesId[]  = $row['item_id'];
            }
        
        foreach ($BookingDiscussions as &$row) {
            $row['type'] = 'booking';
            if (in_array($row['booking_id'], $BookingDiscussionImagesId)){
                $row['images'] = 'true';
            }
            else{
                $row['images'] = 'false';
            }
        }
        foreach ($ComplaintDiscussions as &$row) {
            $row['type'] = 'complaint';
            if (in_array($row['complaint_id'], $ComplaintDiscussionImagesId)){
                $row['images'] = 'true';
            }
            else{
                $row['images'] = 'false';
            }
        }
        
        
        
        $mergedArray = array_merge($BookingDiscussions, $ComplaintDiscussions);
      
        foreach ($mergedArray as $key => $row) {
            $created[$key]  = $row['created'];
            
            $result[$key]['user_name'] = $row['user_name'];
            $result[$key]['user_message'] = $row['user_message'];
            $result[$key]['created'] = $row['created'];
            $result[$key]['type'] = $row['type'];
            $result[$key]['images'] = $row['images'];
            
            if(isset($row['complaint_id'])){
                $complaintId = $row['complaint_id'];
                $result[$key]['complaint_id'] = $row['complaint_id'];
                $result[$key]['booking_num'] = $ComplaintDetails[$complaintId]['booking_num'];
                $result[$key]['color'] = $ComplaintDetails[$complaintId]['color'];
                $result[$key]['address'] = $ComplaintDetails[$complaintId]['address'];
                $result[$key]['complaint_num'] = $ComplaintDetails[$complaintId]['complaint_num'];
            }
            else{
                $bookingId = $row['booking_id'];
                $result[$key]['booking_id'] = $row['booking_id'];
                $result[$key]['booking_num'] = $BookingDetails[$bookingId]['booking_num'];
                $result[$key]['color'] = $BookingDetails[$bookingId]['color'];
                $result[$key]['address'] = $BookingDetails[$bookingId]['address'];
            }
        }
        array_multisort($created, SORT_DESC, $result);
      
      $result = array_merge($lastContractorDiscussion, $result);

       return $result;
        
    }
	
	
   


}