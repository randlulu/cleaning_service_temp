<?php

class Model_EmailLog extends Zend_Db_Table_Abstract { 

    protected $_name = 'email_log';
    private $modelBooking;

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null, $limit = 0,$is_count = 0) {
        $select = $this->getAdapter()->select();
        if($is_count){
		 $select->from(array('el' => $this->_name),array('count' => 'count(DISTINCT(el.email_log_id))'));
		}else{
		 $select->from(array('el' => $this->_name));
		}
        $select->joinLeft(array('ch' => 'cronjob_history'), 'el.cronjob_history_id = ch.id', '');
        $select->joinLeft(array('cj' => 'cron_job'), 'ch.cron_job_id = cj.id', 'cj.cronjob_name');
        $select->order($order);

        if ($filters) {
            if (!empty($filters['subject'])) {
                $subject = $this->getAdapter()->quote($filters['subject']);
                $select->where("el.subject = {$subject}");
            }

            if (!empty($filters['user_id'])) {
                $user_id = $this->getAdapter()->quote($filters['user_id']);
                $select->where("el.user_id = {$user_id}");
            }

            if (!empty($filters['reference_id'])) {
                $reference_id = $this->getAdapter()->quote($filters['reference_id']);
                $select->where("el.reference_id = {$reference_id}");
            }

            if (!empty($filters['type'])) {
                $type = $this->getAdapter()->quote($filters['type']);
                $select->where("el.type = {$type}");
            }
            if (!empty($filters['to'])) {
                $to = $this->getAdapter()->quote(trim($filters['to']));
                $select->where("el.to = {$to}");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        } else if ($limit) {
            $select->limit($limit);
        }

        if($is_count){
		 return $this->getAdapter()->fetchOne($select);
		}else{
		  return $this->getAdapter()->fetchAll($select);
		}
    }

    /**
     * update table rows according to the assigned id and data
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "email_log_id = '{$id}'");
    }

    public function updateByReferenceIdAndCronjobHistoryId($ref_id, $history_id, $data) {
        $ref_id = (int) $ref_id;
        $history_id = (int) $history_id;

        return parent::update($data, "reference_id = {$ref_id} and cronjob_history_id = {$history_id}");
    }

    public function insert($data) {
        $id = parent::insert($data);

        //add User Log 
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, 'email', 'sent');

        return $id;
    }

    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("email_log_id = '{$id}'");
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("email_log_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function getByReferenceIdAndType($id, $type) {

        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("reference_id = '{$id}'");
        $select->where("type = '{$type}'");
        $select->order('email_log_id  desc');

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * get extra info for all the rows
     * @param array $row
     * @param array $types
     * @return array
     */
    public function fills(&$rows, $types = array()) {
        foreach ($rows as &$row) {
            $this->fill($row, $types);
        }
    }

    /**
     * get extra info for each row (Ex: getting the category row by c_id, author row by author id ... etc)
     * 
     * @param array $row
     * @param array $types
     * @return array
     */
    public function fill(&$row, $types = array()) {
        if (in_array('booking', $types)) {
            /**
             * load model
             */
            if (!$this->modelBooking) {
                $this->modelBooking = new Model_Booking();
            }

            $booking = $this->modelBooking->getById($row['reference_id']);
            $row['booking'] = $booking;
        }
    }

    /*     * 20/05/2015 D.A
     * get table rows according to the id
     *
     * @param int $id
     * @return array
     */

    public function getByContractorId($id, &$pager = null, $limit = 0, $perPage = 0, $currentPage = 0) {
        $id = (int) $id;
        
		/*$selectEmails = $this->getAdapter()->select();
        $selectEmails->from('user', array('email1','email2', 'email3','user_id'));
        $selectEmails->where("user_id = '{$id}'");
		$emails=$this->getAdapter()->fetchRow($selectEmails);
		$email1=$emails['email1'];
		$email2=$emails['email2'];
		$email3=$emails['email3'];*/
		$select = $this->getAdapter()->select();
        $select->from($this->_name);
       // $select->where("user_id = '{$id}' or email_log.to in ('{$email1}' ,'{$email2}','{$email3}')");
        $select->where("user_id = '{$id}' or email_log.to in (select email1 from user where user_id = '{$id}')");

		
		
        if ($pager) {
			echo"1";
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        } elseif ($limit) {
						echo"11";


            $select->limit($limit);
        } elseif ($perPage && $currentPage) {
						echo"11";

            $select->limit($perPage, ($currentPage - 1) * $perPage);
			
        }
        $select->order("email_log_id desc");
		//echo $select;
		
        return $this->getAdapter()->fetchAll($select);
    }

    //D.A 28/08/2015
    public function getLocalEmailsCount($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("user_id = '{$id}'");
        $select->where("is_read = '0'");
        $localEmailsCount = $this->getAdapter()->fetchAll($select);
        return count($localEmailsCount);
    }
	
	public function getEmailInvForSaid() {
        
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("email_log.to like '%saidk%' or email_log.to like '%saidel%'");
        $select->where("email_log.subject like '%Inv%'");
       
        return $this->getAdapter()->fetchAll($select);
    }

}
