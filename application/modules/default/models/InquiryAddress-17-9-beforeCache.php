<?php

class Model_InquiryAddress extends Zend_Db_Table_Abstract {

    protected $_name = 'inquiry_address';

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateByInquiryId($id, $data) {
        $id = (int) $id;
        return parent::update($data, "inquiry_id = '{$id}'");
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("inquiry_address_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * to update or insert data into table
     *
     * @param int $inquiry_id
     * @param array $data 
     */
    public function setInquiryAddress($inquiry_id, $data) {
        $inquiry_id = (int) $inquiry_id;
        $success = $this->getById($inquiry_id);
        if($success){
            $this->updateByInquiryId($inquiry_id, $data);
        }else{
            $this->insert($data);
        }
    }

    /**
     * get table row according to the assigned BookingId
     * 
     * @param int $id
     * @return array
     */
    public function getByInquiryId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("inquiry_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

}