<?php

class Model_Mongo {

    protected $_dbhost = 'localhost';
    protected $_dbName = 'cleaning_service';
    protected $_collectionName = 'notification';
    protected $_con;
    protected $_collection;
    protected $_db;

    //protected static $db;

    /**
     * __construct
     * 
     * @param array $data as data
     * @param array $config as array of config
     * 
     * @return null
     */
    public function __construct($data = array(), $config = array()) {
        $dbhost = 'localhost';
        // Connect to test database  
        $this->_con = new Mongo("mongodb://127.0.0.1");

        //$db = $con->cleaning_service;
        $this->_db = $this->_con->selectDB($this->_dbName);

        $this->_collection = $this->_db->selectCollection($this->_collectionName);
    }

    public function closeConnection() {
        $isClosed = $this->_con->close();
        return $isClosed;
    }

    public function getNotificationsByContractorId($lastMonth = false, $contractor_id = 0, $cases = array(), $page_number = 0) {

        $contractor_id = (int) $contractor_id;
        $last_month_value = strtotime(date('d-m-Y', strtotime(" -1 month")));
        $notifications = 0;

        $modelUser = new Model_User();
        $modelAuthRole = new Model_AuthRole();
        $modelAuthRoleCredential = new Model_AuthRoleCredential();
//        $model_notificationSetting = new Model_NotificationSetting();


        $contractroRoleId = $modelAuthRole->getRoleIdByName('contractor');
        //$customerRoleId = $modelAuthRole->getRoleIdByName('customer');
        $company_id = (int) CheckAuth::getCompanySession();
        $loggedUser = CheckAuth::getLoggedUser();
        if ($contractor_id) {
            $userId = $contractor_id;
        } else {
            $userId = $loggedUser['user_id'];
        }
        //$userReg = "/$userId/";
        $userReg = "/\b$userId\b/";
        $regex = new MongoRegex($userReg);
        $user = $modelUser->getById($userId);

        if (isset($cases) && !empty($cases)) {
            $credential = $cases;
        } else {
            $credential = $modelAuthRoleCredential->getCredentialsByParentName('notifications', $user['role_id'], $user['active']);
        }

        if ($user['role_id'] == $contractroRoleId) {
            if ($credential) {
                $where = array();
                foreach ($credential as $key => $value) {
                    $condition = array('title' => strtolower(spaceBeforeCapital($value['credential_name'])));
                    $where[] = $condition;
                }

                if ($lastMonth) {

                    if ($this->_collection->find(array('$and' => array(array('company_id' => $company_id), array('$or' => array(array('contractor_id' => $userId), array('notify_others' => $regex))), array('$or' => $where)), 'date_sent' => array('$gt' => $last_month_value)))->sort(array('_id' => -1))->count() > 0) {
                        $notifications = $this->_collection->find(array('$and' => array(array('company_id' => $company_id), array('contractor_id' => $userId), array('$or' => $where)), 'date_sent' => array('$gt' => $last_month_value)))->sort(array('_id' => -1))->limit(30);
                    } else {
                        $notifications = 0;
                    }
                } else {
                    if ($this->_collection->find(array('$and' => array(array('company_id' => $company_id), array('$or' => array(array('contractor_id' => $userId), array('notify_others' => $regex))), array('$or' => $where))))
                                    ->sort(array('_id' => -1))->count() > 0) {
                        if (isset($page_number) && $page_number) {
                            $notifications = $this->_collection->find(array('$and' => array(array('company_id' => $company_id), array('$or' => array(array('contractor_id' => $userId), array('notify_others' => $regex))), array('$or' => $where))))
                                            ->sort(array('_id' => -1))->skip(15 * ($page_number - 1))->limit(15);
                        }
                    } else {
                        $notifications = 0;
                    }
                }
            } else {
                $notifications = 0;
            }
        } else {
            $sess = new Zend_Session_Namespace();
            if ($credential) {
                $where = array();
                foreach ($credential as $key => $value) {
//                    $notification_settings = $model_notificationSetting->getByUserIdAndCredentialId($loggedUser['user_id'], $value['credential_id']);
//                    if ($notification_settings['is_show'] == 1) {
                        $condition = array('title' => strtolower(spaceBeforeCapital($value['credential_name'])));
                        $where[] = $condition;
//                    }
                }

                if ($this->_collection->find(array('$and' => array(array('created_by' => array('$ne' => (int) $loggedUser['user_id'])), array('company_id' => $company_id), array('$or' => $where))))->sort(array('_id' => -1))->count() > 0) {

                    if (isset($page_number) && $page_number) {
                        $notifications = $this->_collection->find(array('$and' => array(array('created_by' => array('$ne' => (int) $loggedUser['user_id'])), array('company_id' => $company_id), array('$or' => $where))))->sort(array('_id' => -1))->skip(15 * ($page_number - 1))->limit(15);
                    } else {
                        $disRegex = "/\bdiscussion\b/";
                        $disRegex = new MongoRegex($disRegex);
                        $notifications = $this->_collection->find(array('$and' => array(array('created_by' => array('$ne' => (int) $loggedUser['user_id'])), array('company_id' => $company_id), array('$or' => array(array('notify_others' => $regex), array('title' => $disRegex))), array('$or' => $where))))->sort(array('_id' => -1));
//                                                $notifications = $this->_collection->find(array('$and' => array(array('created_by' => array('$ne' => (int) $loggedUser['user_id'])), array('company_id' => $company_id), array('$or' => array(array('show' => $regex), array('notify_others' => $regex) , )),array('title' => 'new contractor discussion'))))->sort(array('_id' => -1));
                    }
                } else {
                    $notifications = 0;
                }
            } else if (isset($sess->is_employee)) {
                if ($this->_collection->find(array('title' => 'subscriber phone request'))->sort(array('_id' => -1))->count() > 0) {

                    if (isset($page_number) && $page_number) {
                        $notifications = $this->_collection->find(array('title' => 'subscriber phone request'))->sort(array('_id' => -1))->skip(15 * ($page_number - 1))->limit(15);
                    } else {
                        $disRegex = "/\bdiscussion\b/";
                        $disRegex = new MongoRegex($disRegex);
                        $notifications = $this->_collection->find(array('title' => 'subscriber phone request'))->sort(array('_id' => -1));
//                                                $notifications = $this->_collection->find(array('$and' => array(array('created_by' => array('$ne' => (int) $loggedUser['user_id'])), array('company_id' => $company_id), array('$or' => array(array('show' => $regex), array('notify_others' => $regex) , )),array('title' => 'new contractor discussion'))))->sort(array('_id' => -1));
                    }
                } else {
                    $notifications = 0;
                }
            } else {
                $notifications = 0;
            }
        }



        return $notifications;
    }

    public function insertNotification($doc = array()) {
        return $this->_collection->insert($doc);
    }

    //public function updateNotification($id) {
    public function updateNotification($id, $type = 0) {

        $modelAuthRole = new Model_AuthRole();
        $modelAuthRoleCredential = new Model_AuthRoleCredential();
        $contractroRoleId = $modelAuthRole->getRoleIdByName('contractor');
        $loggedUser = CheckAuth::getLoggedUser();
        $userId = $loggedUser['user_id'];

        if ($type) {
            $credential = $modelAuthRoleCredential->getCredentialsByParentName('smsNotification', $loggedUser['role_id'], $loggedUser['active']);
        } else {
            $credential = $modelAuthRoleCredential->getCredentialsByParentName('notifications', $loggedUser['role_id'], $loggedUser['active']);
        }

        //$credential = $modelAuthRoleCredential->getCredentialsByParentName('notifications', $loggedUser['role_id']);
        $user_ids = $this->getSeenByIds($credential);

        $new = "";
        $read = array("");

        foreach ($user_ids as $key => $value2) {

            $read = explode(',', $value2['read']);


            if (!in_array($userId, $read)) {
                if (!empty($value2['read'])) {
                    $new = $value2['read'] . "," . $userId;
                } else {
                    $new = (string) $userId;
                }
                $this->_collection->update(
                        array('_id' => new MongoId($id)), array('$set' => array('read' => $new))
                );
            }
        }
        return 1;
    }

    /* public function clearCountstatusNotification($cases = array(), $loggedUser = null, $last_month = false, $sms = 0) {
      $modelUser = new Model_User();
      $modelAuthRole = new Model_AuthRole();
      $modelAuthRoleCredential = new Model_AuthRoleCredential();
      $contractroRoleId = $modelAuthRole->getRoleIdByName('contractor');

      if (!(isset($loggedUser))) {
      $loggedUser = CheckAuth::getLoggedUser();
      }
      $userId = $loggedUser['user_id'];

      $userReg = "/\b$userId\b/";
      $regex = new MongoRegex($userReg);
      if (isset($cases) && !empty($cases)) {

      $credential = $cases;
      $user_ids = $this->getSeenByIds($credential);
      } else {
      if ($sms) {
      $credential = $modelAuthRoleCredential->getCredentialsByParentName('smsNotification', $loggedUser['role_id']);
      } else {
      $credential = $modelAuthRoleCredential->getCredentialsByParentName('notifications', $loggedUser['role_id']);
      }
      $user_ids = $this->getSeenByIds($credential);
      }
      $new = "";
      $seen = array("");
      foreach ($user_ids as $key_id => $value2) {
      $seen = explode(',', $value2['seen']);
      if (!in_array($userId, $seen)) {
      if (!empty($value2['seen'])) {
      $new = $value2['seen'] . "," . $userId;
      } else {
      $new = (string) $userId;
      }
      if ($contractroRoleId == $loggedUser['role_id']) {
      foreach ($credential as $key => $value) {
      $this->_collection->update(
      array('$and' => array(array('contractor_id' => $userId), array('title' => strtolower(spaceBeforeCapital($value['credential_name']))))), array('$set' => array('seen' => $new)), array(
      'multiple' => true,
      )
      );
      }
      } else {
      if ($sms) {
      foreach ($credential as $key => $value) {
      $this->_collection->update(
      array('$and' => array(array('_id' => new MongoId($key_id)))), array('$set' => array('seen' => $new)), array(
      'multiple' => true,
      )
      );
      }
      } else {
      foreach ($credential as $key => $value) {
      $this->_collection->update(
      array('$and' => array(array('show' => $regex), array('_id' => new MongoId($key_id)))), array('$set' => array('seen' => $new)), array(
      'multiple' => true,
      )
      );
      }
      }
      }
      }
      }

      return 1;
      }
     */

    public function clearCountstatusNotification($cases = array(), $loggedUser = null, $last_month = false, $sms = 0) {
        exit;
        $modelUser = new Model_User();
        $modelAuthRole = new Model_AuthRole();
        $modelAuthRoleCredential = new Model_AuthRoleCredential();
        $contractroRoleId = $modelAuthRole->getRoleIdByName('contractor');

        if (!(isset($loggedUser))) {
            $loggedUser = CheckAuth::getLoggedUser();
        }
        $userId = $loggedUser['user_id'];

        $userReg = "/\b$userId\b/";
        $regex = new MongoRegex($userReg);
        if (isset($cases) && !empty($cases)) {

            $credential = $cases;
            $user_ids = $this->getSeenByIds($credential);
        } else {
            if ($sms) {
                $credential = $modelAuthRoleCredential->getCredentialsByParentName('smsNotification', $loggedUser['role_id'], $loggedUser['active']);
            } else {
                $credential = $modelAuthRoleCredential->getCredentialsByParentName('notifications', $loggedUser['role_id'], $loggedUser['active']);
            }
            $user_ids = $this->getSeenByIds($credential);
        }
        $new = "";
        $seen = array("");
        foreach ($user_ids as $key_id => $value2) {
            $seen = explode(',', $value2['seen']);
            if (!in_array($userId, $seen)) {
                if (!empty($value2['seen'])) {
                    $new = $value2['seen'] . "," . $userId;
                } else {
                    $new = (string) $userId;
                }
                if ($contractroRoleId == $loggedUser['role_id']) {
                    foreach ($credential as $key => $value) {
                        $this->_collection->update(
                                array('$and' => array(array('contractor_id' => $userId), array('title' => strtolower(spaceBeforeCapital($value['credential_name']))))), array('$set' => array('seen' => $new)), array(
                            'multiple' => true,
                                )
                        );
                    }
                } else {
                    if ($sms) {
                        foreach ($credential as $key => $value) {
                            $this->_collection->update(
                                    array('$and' => array(array('_id' => new MongoId($key_id)))), array('$set' => array('seen' => $new)), array(
                                'multiple' => true,
                                    )
                            );
                        }
                    } else {
                        foreach ($credential as $key => $value) {
                            $this->_collection->update(
                                    array('$and' => array(array('show' => $regex), array('_id' => new MongoId($key_id)))), array('$set' => array('seen' => $new)), array(
                                'multiple' => true,
                                    )
                            );
                        }
                    }
                }
            }
        }

        return 1;
    }

    public function getSeenByIds($credential) {
//        var_dump($credential);
//        exit;
        $modelAuthRole = new Model_AuthRole();
        $where = array();
        $modelUser = new Model_User();
        $loggedUser = CheckAuth::getLoggedUser();
        $userId = $loggedUser['user_id'];
        $user = $modelUser->getById($userId);
        $userReg = "/$userId/";
        $regex = new MongoRegex($userReg);
        $contractroRoleId = $modelAuthRole->getRoleIdByName('contractor');
        $company_id = (int) CheckAuth::getCompanySession();
        if ($user['role_id'] == $contractroRoleId) {
            foreach ($credential as $key => $value) {
                $condition = array('$and' => array(array("title" => trim(strtolower(spaceBeforeCapital($value['credential_name'])))), array('contractor_id' => (int) $userId)));
                $where[] = $condition;
            }
        } else {
//            $model_notificationSetting = new Model_NotificationSetting();
            foreach ($credential as $key => $value) {
//                $notification_settings = $model_notificationSetting->getByUserIdAndCredentialId($loggedUser['user_id'], $value['credential_id']);
//                if ($notification_settings['is_show'] == 1) {
                    $condition = array('$and' => array(array('company_id' => $company_id), array("title" => trim(strtolower(spaceBeforeCapital($value['credential_name'])))), array('created_by' => array('$ne' => (int) $userId))));
                    $where[] = $condition;
//                }
            }
        }

        $notifications = $this->_collection->find(array('$or' => $where))->sort(array('_id' => -1));

//        if(my_ip("83.244.48.39")){
//            echo "salim";
////            var_dump($where);
//            foreach ($notifications as $key => $value) {
//                var_dump($value);
//            }
//        }

        return $notifications;
    }

//    public function countNotifications($cases = array()) {
    public function countNotifications($cases = array(), $sms = 0) {
        $company_id = (int) CheckAuth::getCompanySession();
        $modelAuthRole = new Model_AuthRole();
        $modelAuthRoleCredential = new Model_AuthRoleCredential();
        $contractroRoleId = $modelAuthRole->getRoleIdByName('contractor');
        //$customerRoleId = $modelAuthRole->getRoleIdByName('customer');
        $loggedUser = CheckAuth::getLoggedUser();
        $userId = $loggedUser['user_id'];
        if (isset($cases) && !empty($cases)) {
            $credential = $cases;
        } else {
            if ($sms) {
                $credential = $modelAuthRoleCredential->getCredentialsByParentName('smsNotification', $loggedUser['role_id'], $loggedUser['active']);
            } else {
                $credential = $modelAuthRoleCredential->getCredentialsByParentName('notifications', $loggedUser['role_id'], $loggedUser['active']);
            }
        }

        if ($credential) {
            $where = array();
            foreach ($credential as $key => $value) {
////                if (my_ip("176.106.46.142")) {
////                    var_dump($value);
////                    echo "---------- <br/>";
//                    $model_notificationSetting = new Model_NotificationSetting();
//                    $notification_settings = $model_notificationSetting->getByUserIdAndCredentialId($loggedUser['user_id'], $value['credential_id']);
//                    if ($notification_settings['is_show'] == 1 && $loggedUser['role_id'] != $contractroRoleId) {
//                        $condition = array('title' => strtolower(spaceBeforeCapital($value['credential_name'])));
//                        $where[] = $condition;
//                    }
//                } else {
//                } else if ($loggedUser['role_id'] == $contractroRoleId) {
                
                    $condition = array('title' => strtolower(spaceBeforeCapital($value['credential_name'])));
                    $where[] = $condition;
                
//                }
            }
            // $userReg = "/$userId/";
            $userReg = "/\b$userId\b/";
            $regexs = new MongoRegex($userReg);
            //
            /* if ($loggedUser['role_id'] == $contractroRoleId) {
              $count = $this->_collection->find(array('$and' => array(array('contractor_id' => $userId), array('seen' => array('$not' => $regexs)), array('$or' => $where))))->count();
              } else {
              $count = $this->_collection->find(array('$and' => array(array('created_by' => array('$ne' => (int) $loggedUser['user_id'])), array('show' => $regexs),array('notification_user' => $loggedUser['username']), array('contractor_id' => array('$ne' => $userId)), array('seen' => array('$not' => $regexs)), array('$or' => $where))))->count();
              } */

            if ($loggedUser['role_id'] == $contractroRoleId) {
                //$count = $this->_collection->find(array('$and' => array(array('company_id' => $company_id), array('$or' => array(array('contractor_id' => $userId), array('notify_others' => $regexs))), array('seen' => array('$not' => $regexs)), array('$or' => $where))))->count();
                if ($sms) {
                    $count = $this->_collection->find(array('$and' => array(array('company_id' => $company_id), array('contractor_id' => $userId), array('seen' => array('$not' => $regexs)), array('title' => 'new sms'))))->count();
                } else
                    $count = $this->_collection->find(array('$and' => array(array('company_id' => $company_id), array('$or' => array(array('contractor_id' => $userId), array('notify_others' => $regexs))), array('seen' => array('$not' => $regexs)), array('$or' => $where))))->count();
            } else {
                if ($sms) {
                    $count = $this->_collection->find(array('$and' => array(array('company_id' => $company_id), array('seen' => array('$not' => $regexs)), array('title' => 'new sms'))))->count();
                } else {
                    $count = $this->_collection->find(array('$and' => array(array('company_id' => $company_id), array('created_by' => array('$ne' => (int) $loggedUser['user_id'])), array('$or' => array(array('notify_others' => $regexs), array('$and' => array(array('notification_user' => $loggedUser['username']))))), array('contractor_id' => array('$ne' => $userId)), array('seen' => array('$not' => $regexs)), array('$or' => $where))))->count();
                }
            }
        }
        $sess = new Zend_Session_Namespace();
        if (isset($sess->is_employee)) {
            $count = $this->_collection->find(array('title' => 'subscriber phone request'))->count();
        }
        if (isset($count)) {
            return $count;
        } else {
            return 0;
        }
    }

    public function getUnreadNotifiedMessages() {
        $modelAuthRole = new Model_AuthRole();
        $modelAuthRoleCredential = new Model_AuthRoleCredential();
        $contractroRoleId = $modelAuthRole->getRoleIdByName('contractor');
        $loggedUser = CheckAuth::getLoggedUser();
        $userId = $loggedUser['user_id'];

        $credential = $modelAuthRoleCredential->getCredentialsByParentName('notifications', $loggedUser['role_id'], $loggedUser['active']);
        if ($credential) {
            $where = array();
            foreach ($credential as $key => $value) {
                $condition = array('title' => strtolower(spaceBeforeCapital($value['credential_name'])));
                $where[] = $condition;
            }
            $userReg = "/$userId/";

            $regexs = new MongoRegex($userReg);

            //if ($loggedUser['role_id'] == $contractroRoleId) {
            //    $count = $this->_collection->find(array('$and' => array(array('contractor_id' => $userId), array('seen' => array('$not' => $regexs)), array('$or' => $where))))->count();
            //} else {
            $count = $this->_collection->find();
        }
        //}

        return $count;
    }

    public function markAllAsRead($cases = array(), $sms = 0) {
        //public function markAllAsRead($cases = array()) {
        $modelAuthRole = new Model_AuthRole();
        $contractroRoleId = $modelAuthRole->getRoleIdByName('contractor');
        $modelAuthRoleCredential = new Model_AuthRoleCredential();

        $loggedUser = CheckAuth::getLoggedUser();
        $userId = $loggedUser['user_id'];
        $new = "";
        $read = array("");

        if (isset($cases) && !empty($cases)) {
            $credential = $cases;
        } else {
            if ($sms) {
                $credential = $modelAuthRoleCredential->getCredentialsByParentName('smsNotification', $loggedUser['role_id'], $loggedUser['active']);
            } else {
                $credential = $modelAuthRoleCredential->getCredentialsByParentName('notifications', $loggedUser['role_id'], $loggedUser['active']);
            }
            //$credential = $modelAuthRoleCredential->getCredentialsByParentName('notifications', $loggedUser['role_id']);
        }
        $user_ids = $this->getSeenByIds($credential);

        foreach ($user_ids as $key => $value2) {

            $read = explode(',', $value2['read']);



            if (!in_array($userId, $read)) {

                if (!empty($value2['read'])) {
                    $new = $value2['read'] . "," . $userId;
                } else {
                    $new = (string) $userId;
                }
                foreach ($credential as $key => $value) {
                    $this->_collection->update(
                            array('title' => strtolower(spaceBeforeCapital($value['credential_name']))), array('$set' => array('read' => $new)), array('multiple' => true)
                    );
                }
            }
        }


        return 1;
    }

    public function getShowByTitle($title) {
        $notification = $this->_collection->findOne(array('title' => $title));
        return $notification['show'];
    }

    public function getMultiShowByTitle($title) {
        $notification = $this->_collection->find(array('title' => $title));
        return $notification;
    }

    public function updateShowByTitle($title) {
        $modelUser = new Model_User();
        $modelAuthRole = new Model_AuthRole();
        $modelAuthRoleCredential = new Model_AuthRoleCredential();
        $contractroRoleId = $modelAuthRole->getRoleIdByName('contractor');
        $loggedUser = CheckAuth::getLoggedUser();
        $userId = $loggedUser['user_id'];


        $user_ids = $this->getMultiShowByTitle($title);

        $new = "";
        $seen = array("");

        foreach ($user_ids as $key_id => $value2) {

            $seen = explode(',', $value2['show']);

            if (!in_array($userId, $seen)) {
                if (!empty($value2['show'])) {
                    $new = $value2['show'] . "," . $userId;
                } else {
                    $new = (string) $userId;
                }

                if ($contractroRoleId == $loggedUser['role_id']) {


                    $this->_collection->update(
                            array('$and' => array(array('contractor_id' => $userId), array('title' => $title))), array('$set' => array('show' => $new)), array(
                        'multiple' => true,
                            )
                    );
                } else {
                    $this->_collection->update(
                            array('_id' => new MongoId($key_id)), array('$set' => array('show' => $new)), array('multiple' => true)
                    );
                }
            }
        }
    }

    public function deleteByUserIdByCredentialNameAndUserID($title, $user_id) {
        $modelUser = new Model_User();
        $modelAuthRole = new Model_AuthRole();
        $modelAuthRoleCredential = new Model_AuthRoleCredential();
        $contractroRoleId = $modelAuthRole->getRoleIdByName('contractor');
        $loggedUser = CheckAuth::getLoggedUser();
        $userId = $loggedUser['user_id'];


        $user_ids = $this->getMultiShowByTitle($title);

        $new = "";
        $seen = array("");

        foreach ($user_ids as $key_id => $value2) {

            $seen = explode(',', $value2['show']);

            if (($key = array_search($userId, $seen)) !== false) {
                unset($seen[$key]);

                $new = implode(',', $seen);

                if ($contractroRoleId == $loggedUser['role_id']) {

                    $this->_collection->update(
                            array('$and' => array(array('contractor_id' => $userId), array('title' => $title))), array('$set' => array('show' => $new)), array(
                        'multiple' => true,
                            )
                    );
                } else {
                    $this->_collection->update(
                            array('_id' => new MongoId($key_id)), array('$set' => array('show' => $new)), array('multiple' => true)
                    );
                }
            }
        }
    }

    public function getByTitleAndUserId($title) {
        $loggedUser = CheckAuth::getLoggedUser();
        $user_id = $loggedUser['user_id'];
        $userReg = "/$user_id/";
        $regex = new MongoRegex($userReg);
        $notification = $this->_collection->findOne(array('$and' => array(array('title' => $title), array('show' => $regex))));
        return $notification;
    }

    public function getByContractorId($id) {
        $id = (int) $id;
        $notifications = $this->_collection->find(array('contractor_id' => $id));

        return $notifications;
    }

    public function getByTitle($title) {
        $modelUser = new Model_User();
        $modelAuthRole = new Model_AuthRole();
        $loggedUser = CheckAuth::getLoggedUser();
        $user_id = $loggedUser['user_id'];
        $userReg = "/$user_id/";
        $regex = new MongoRegex($userReg);
        $user = $modelUser->getById($user_id);
        $contractroRoleId = $modelAuthRole->getRoleIdByName('contractor');

        if ($user['role_id'] == $contractroRoleId) {

            $notification = $this->_collection->find(array('$and' => array(array('contractor_id' => $user_id), array('title' => $title))))->sort(array('_id' => -1));
        } else {
            $notification = $this->_collection->find(array('$and' => array(array('title' => $title), array('show' => $regex))))->sort(array('_id' => -1));
        }
        return $notification;
    }

    /*     * **function for sms*********** */

    public function getNotificationsByTitle($lastMonth = false, $cases = array(), $page_number = 0) {
        $last_month_value = strtotime(date('d-m-Y', strtotime(" -1 month")));
        $modelUser = new Model_User();
        $modelAuthRole = new Model_AuthRole();
        $modelAuthRoleCredential = new Model_AuthRoleCredential();
        $loggedUser = CheckAuth::getLoggedUser();
        $userId = $loggedUser['user_id'];
        $userReg = "/\b$userId\b/";
        $regex = new MongoRegex($userReg);
        $user = $modelUser->getById($userId);
        $credential = $modelAuthRoleCredential->getCredentialsByParentName('smsNotification', $user['role_id'], $user['active']);

        if ($credential) {
            if ($this->_collection->find(array('title' => 'new sms'))->sort(array('_id' => -1))->count() > 0) {
                if (isset($page_number) && $page_number) {
                    $notifications = $this->_collection->find(array('title' => 'new sms'))->skip(15 * ($page_number - 1))->sort(array('_id' => -1))->limit(15);
                } else {
                    $notifications = $this->_collection->find(array('title' => 'new sms'))->sort(array('_id' => -1));
                }
            }
        } else {
            $notifications = 0;
        }
        return $notifications;
    }

    /*     * *************getByContractorId************* */

    public function getNotificationsBySmsContractorId($lastMonth = false, $cases = array(), $page_number = 0) {
        $last_month_value = strtotime(date('d-m-Y', strtotime(" -1 month")));
        $modelUser = new Model_User();
        $modelAuthRole = new Model_AuthRole();
        $modelAuthRoleCredential = new Model_AuthRoleCredential();
        $loggedUser = CheckAuth::getLoggedUser();
        $userId = $loggedUser['user_id'];
        $userReg = "/\b$userId\b/";
        $regex = new MongoRegex($userReg);
        $user = $modelUser->getById($userId);
        $credential = $modelAuthRoleCredential->getCredentialsByParentName('smsNotification', $user['role_id'], $user['active']);

        if ($credential) {
            if ($this->_collection->find(array('title' => 'new sms', 'contractor_id' => $loggedUser['user_id']))->sort(array('_id' => -1))->count() > 0) {
                if (isset($page_number) && $page_number) {
                    $notifications = $this->_collection->find(array('title' => 'new sms', 'contractor_id' => $loggedUser['user_id']))->skip(15 * ($page_number - 1))->sort(array('_id' => -1))->limit(15);
                } else {
                    $notifications = $this->_collection->find(array('title' => 'new sms', 'contractor_id' => $loggedUser['user_id']))->sort(array('_id' => -1));
                }
            }
        } else {
            $notifications = 0;
        }
        return $notifications;
    }

}
