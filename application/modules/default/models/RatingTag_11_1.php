<?php

class Model_RatingTag extends Zend_Db_Table_Abstract {

    protected $_name = 'rating_tag';

    public function getAll($contractor_id = 0, $with_join = 0, $item_id) {
        $contractor_id = (int) $contractor_id;
        $select = $this->getAdapter()->select();
        $select->from(array('rt' => $this->_name));
        if ($with_join) {
            $select2 = $this->getAdapter()->select();
            $select2->from('contractor_rate');
            if ($contractor_id) {
                $select2->where("contractor_id = {$contractor_id} ");
                $select2->where("item_id = {$item_id}");
            }
            $select->joinLeft(array('cr' => $select2), 'cr.rating_tag_id = rt.rating_tag_id', array('cr.rate', 'cr.contractor_id'));
        }
        $select->order("rt.rating_tag_id DESC");
//        $select->group('rt.rating_tag_id');
        $rates = $this->getAdapter()->fetchAll($select);


        return $rates;
    }

    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from(array('rt' => $this->_name));
        $select->where("rating_tag_id = {$id}");
        return $this->getAdapter()->fetchRow($select);
    }

    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("rating_tag_id = '{$id}'");
    }

}
