<?php

class Model_InquiryServiceAttributeValue extends Zend_Db_Table_Abstract {

    protected $_name = 'inquiry_service_attribute_value';

    /**
     *update table rows according to the assigned id and data
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "service_attribute_value_id= '{$id}'");
    }

    /**
     *delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("service_attribute_value_id= '{$id}'");
    }

    /**
     *get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("service_attribute_value_id= '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     *get table row according to the inquiry id
     * 
     * @param int $id
     * @return array 
     */
    public function getByInquiryId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from(array('sav' => $this->_name));
        $select->joinInner(array('sa' => 'service_attribute'), 'sav.service_attribute_id=sa.service_attribute_id', array('sa.service_id,sa.attribute_id'));
        $select->where("inquiry_id= '{$id}'");

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     *get table row according to the inquiry id and service attribute id and clone
     * 
     * @param int $inquiryId
     * @param int $serviceAttributeId
     * @param int $clone
     * @return array 
     */
    public function getByInquiryIdAndServiceAttributeIdAndClone($inquiryId, $serviceAttributeId, $clone) {
        $inquiryId = (int) $inquiryId;
        $serviceAttributeId = (int) $serviceAttributeId;
        $clone = (int) $clone;

        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("inquiry_id= '{$inquiryId}' AND service_attribute_id= '{$serviceAttributeId}' AND clone = '{$clone}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     *add attribute by the inquiry id and service id and clone
     * 
     * @param int $inquiryId
     * @param int $serviceAttributeId
     * @param int $clone
     */
    public function addAttributeByInquiryIdAndServiceIdAndClone($inquiryId, $serviceId, $clone) {

        $inquiryId = (int) $inquiryId;
        $serviceId = (int) $serviceId;
        $clone = (int) $clone;

        $request = Zend_Controller_Front::getInstance()->getRequest();

        $modelServices = new Model_Services();
        $modelAttributes = new Model_Attributes();
        $modelServiceAttribute = new Model_ServiceAttribute();

        $service = $modelServices->getById($serviceId);

        $serviceAttribute = $modelServiceAttribute->getAttributeByServiceId($serviceId, true);

        if ($serviceAttribute) {
            foreach ($serviceAttribute as $attribute) {

                $value = $request->getParam('attribute_' . $serviceId . $attribute['attribute_id'] . ($clone ? '_' . $clone : ''));
                $value = !empty($value) ? $value : '';

                $serviceAttributeValue = $this->getByInquiryIdAndServiceAttributeIdAndClone($inquiryId, $attribute['service_attribute_id'], $clone);

                $is_serialize = 0;
                if (is_array($value)) {
                    $value = serialize($value);
                    $is_serialize = 1;
                }

                $data = array(
                    'service_attribute_id' => $attribute['service_attribute_id'],
                    'inquiry_id' => $inquiryId,
                    'value' => $value,
                    'is_serialized_array' => $is_serialize,
                    'clone' => $clone
                );

                if ($serviceAttributeValue) {
                    $this->updateById($serviceAttributeValue['service_attribute_value_id'], $data);
                } else {
                    $this->insert($data);
                }
            }
        }
    }

    /**
     *delete table row accourding to the inquiry id and service id and clone
     * 
     * @param int $inquiryId
     * @param int $serviceAttributeId
     * @param int $clone
     */
    public function deleteByInquiryIdServiceIdAndClone($inquiryId, $serviceId, $clone) {
        $inquiryId = (int) $inquiryId;
        $clone = (int) $clone;

        $modelServiceAttribute = new Model_ServiceAttribute();
        $serviceAttribute = $modelServiceAttribute->getAttributeByServiceId($serviceId);

        if ($serviceAttribute) {
            foreach ($serviceAttribute as $attribute) {
                $this->delete("service_attribute_id= '{$attribute['service_attribute_id']}' AND inquiry_id= '{$bookingId}' AND clone= '{$clone}'");
            }
        }
    }

}