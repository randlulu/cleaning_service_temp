<?php

class Model_AttributeListValue extends Zend_Db_Table_Abstract {

    protected $_name = 'attribute_list_value';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from(array('alv' => $this->_name));
        $select->joinInner(array('a' => 'attribute'), 'alv.attribute_id=a.attribute_id', array('a.attribute_name'));
        $select->order($order);

        if ($filters) {
            if (!empty($filters['keywords'])) {
                $keywords = $this->getAdapter()->quote('%' . $filters['keywords'] . '%');
                $select->where("attribute_value LIKE {$keywords}");
            }
            if (!empty($filters['attribute_id'])) {
                $select->where("alv.attribute_id = {$filters['attribute_id']}");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "attribute_value_id= '{$id}'");
    }

    /**
     * delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("attribute_value_id= '{$id}'");
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("attribute_value_id= '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the assigned attributeValue
     * 
     * @param string $attributeValue
     * @return array
     */
    public function getByAttributeValue($attributeValue) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("attribute_value = '{$attributeValue}'");
        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table rows as array
     * 
     * @return array
     */
    public function getAttributeListValueAsArray() {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order('attribute_value asc');

        $attributeListValue = $this->getAdapter()->fetchAll($select);

        $data = array();
        foreach ($attributeListValue as $attributeValue) {
            $data[] = array(
                'id' => $attributeValue['attribute_value_id'],
                'name' => $attributeValue['attribute_value']
            );
        }
        return $data;
    }

    /**
     * get table row according to the assigned attributeId
     * 
     * @param int $id
     * @return array
     */
    public function getByAttributeId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("attribute_id = '{$id}'");

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * get table row according to the assigned attributeId
     * 
     * @param int $id
     * @return array
     */
    public function getByAttributeIdAsArray($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("attribute_id = '{$id}'");

        $attributeListvalue = $this->getAdapter()->fetchAll($select);

        $data = array();
        foreach ($attributeListvalue as $attributevalue) {
            $data[$attributevalue['attribute_value_id']] = $attributevalue['attribute_value'];
        }
        return $data;
    }

    public function getByAttributeIdAsIosArray($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("attribute_id = '{$id}'");

        $attributeListvalue = $this->getAdapter()->fetchAll($select);

        $data = array();
        foreach ($attributeListvalue as $attributevalue) {
            $data[] = array('name' => $attributevalue['attribute_value'], 'value' => $attributevalue['attribute_value_id']);
        }
        return $data;
    }

    public function checkIfFloorTypeAndHaveAttachment($attributeListValueId, &$tables = array()) {


        $modelAttributes = new Model_Attributes();

        $attributeListValue = $this->getById($attributeListValueId);
        $attribute = $modelAttributes->getById($attributeListValue['attribute_id']);
        $sucsess = false;

        if ($attribute['attribute_name'] == 'Floor') {
            $select_image_attachment = $this->getAdapter()->select();
            $select_image_attachment->from('image_attachment', 'COUNT(*)');
            $select_image_attachment->where("attribute_list_value_id = {$attributeListValueId}");
            $count_image_attachment = $this->getAdapter()->fetchOne($select_image_attachment);

            if ($count_image_attachment) {
                $tables[] = 'image_attachment';
                $sucsess = true;
            }
        }
        return $sucsess;
    }

    public function checkBeforeDeleteServiceAttributeValue($attributeListValueId, &$tables = array()) {

        $sucsess = true;

        $select_service_attribute_value = $this->getAdapter()->select();
        $select_service_attribute_value->from(array('sav' => 'service_attribute_value'), 'COUNT(*)');
        $select_service_attribute_value->joinInner(array('sa' => 'service_attribute'), 'sav.service_attribute_id = sa.service_attribute_id');
        $select_service_attribute_value->joinInner(array('a' => 'attribute'), 'a.attribute_id = sa.attribute_id');
        $select_service_attribute_value->joinInner(array('at' => 'attribute_type'), 'a.attribute_type_id = at.attribute_type_id');

        $select_service_attribute_value->where("sav.value = {$attributeListValueId}");
        $select_service_attribute_value->where("at.is_list = 1");
        $count_service_attribute_value = $this->getAdapter()->fetchOne($select_service_attribute_value);
        if ($count_service_attribute_value) {
            $sucsess = false;
        }


        //serliaze
        $select_service_attribute_value = $this->getAdapter()->select();
        $select_service_attribute_value->from(array('sav' => 'service_attribute_value'), 'COUNT(*)');
        $select_service_attribute_value->joinInner(array('sa' => 'service_attribute'), 'sav.service_attribute_id = sa.service_attribute_id');
        $select_service_attribute_value->joinInner(array('a' => 'attribute'), 'a.attribute_id = sa.attribute_id');
        $select_service_attribute_value->joinInner(array('at' => 'attribute_type'), 'a.attribute_type_id = at.attribute_type_id');

        $select_service_attribute_value->where("sav.value like '%{$attributeListValueId}%'");
        $select_service_attribute_value->where("at.is_list = 1");
        $select_service_attribute_value->where("sav.is_serialized_array = 1");

        $count_service_attribute_value = $this->getAdapter()->fetchOne($select_service_attribute_value);
        if ($count_service_attribute_value) {
            $sucsess = false;
        }
        return $sucsess;
    }
	
	public function getAllFloor(){
 
		 $select_attribute_list_value = $this->getAdapter()->select();
		 $select_attribute_list_value->from(array('a' => 'attribute'),array());
		 $select_attribute_list_value->joinInner(array('alv' => 'attribute_list_value'), 'a.attribute_id = alv.attribute_id',array('attribute_value','attribute_value_id'));
		 $select_attribute_list_value->where("a.attribute_name = 'floor' ");
		// echo $select_attribute_list_value->__toString();
		 return $this->getAdapter()->fetchAll($select_attribute_list_value);
		 
	 }
	 
	 public function getAllSealers(){
	 
	   $select_attribute_list_value = $this->getAdapter()->select();
	   $select_attribute_list_value->from(array('a' => 'attribute'),array());
	   $select_attribute_list_value->joinInner(array('alv' => 'attribute_list_value'), 'a.attribute_id = alv.attribute_id',array('attribute_value','attribute_value_id'));
	   $select_attribute_list_value->where("a.attribute_name = 'Sealer' ");
	  // echo $select_attribute_list_value->__toString();
	   return $this->getAdapter()->fetchAll($select_attribute_list_value);
	   
	  }


}