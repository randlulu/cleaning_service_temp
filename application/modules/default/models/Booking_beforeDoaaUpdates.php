<?php

class Model_Booking extends Zend_Db_Table_Abstract {

    protected $_name = 'booking';
    //
    //model for fill function
    //
    private $loggedUser = array();
    private $modelBookingInvoice;
    private $modelContractorInfo;
    private $modelContractorServiceBooking;
    private $modelContractorServiceBookingTemp;
    private $modelCustomer;
    private $modelBookingLabel;
    private $modelLabel;
    private $modelCities;
    private $modelCountries;
    private $modelBookingStatus;
    private $modelServices;
    private $modelBookingAddress;
    private $modelUser;
    private $modelBookingDiscussion;
    private $modelComplaint;
    private $modelEmailLog;
    private $modelPropertyType;
    private $modelBookingReminder;
    private $modelBookingEstimate;
    private $modelBookingLog;
    private $modelCustomerCommercialInfo;
    private $modelCustomerContact;
    private $modelCustomerContactLabel;
    private $modelBookingAttachment;
    private $modelBookingStatusDiscussion;
    private $modelPayment;
    private $modelAttributeListValue;
    private $modelServiceAttributeValue;
    private $modelServiceAttribute;
    private $modelAttributes;
    private $modelPaymentType;
    private $modelRefund;
    private $modelBookingContactHistory;
    private $modelBookingMultipleDays;

    public function init() {
        parent::init();
        $this->loggedUser = CheckAuth::getLoggedUser();
    }
	public function updateGST(){
        $select = $this->getAdapter()->select();
        $select->from(array('bok' => $this->_name));
        $select->where("bok.total_discount != 0");
        $select->order('bok.booking_id ASC');
        $bookings = $this->getAdapter()->fetchAll($select);
        if ($bookings) {
            foreach ($bookings as $booking) {
              $new_gst=$booking['gst']+($booking['total_discount']*.10);
              $new_qoute=$booking['qoute']+($booking['total_discount']* 1.1);
              $data = array(
              'gst_new' => $new_gst,
              'qoute_new' => $new_qoute
              );
              $this->updateById($booking['booking_id'], $data);
            }
        }
    }
	
	public function updateGST2(){
        $select = $this->getAdapter()->select();
        $select->from(array('bok' => $this->_name));
        $select->where("bok.total_discount = 0");
        $select->order('bok.booking_id Desc');
        $bookings = $this->getAdapter()->fetchAll($select);
        if ($bookings) {
            foreach ($bookings as $booking) {
              $new_gst=$booking['gst_old'];
              $new_qoute=$booking['qoute_old'];
              $data = array(
              'gst' => $new_gst,
              'qoute' => $new_qoute
              );
              $this->updateById($booking['booking_id'], $data);
            }
        }
    }


    private function getWheresAndJoinsByFilters($filters, $isCronJob = false) {
        $wheres = array();
        $joinInner = array();

        if (!$isCronJob) {
            $filters['company_id'] = CheckAuth::getCompanySession();

            if (CheckAuth::checkCredential(array('canSeeDeletedBooking'))) {
				//echo 'Islaaaaam';
				//echo CheckAuth::checkCredential(array('canSeeDeletedBooking'));
                if (!empty($filters['is_deleted'])) {
                    $wheres['is_deleted'] = 'bok.is_deleted = 1';
                } else {
                    $wheres['is_deleted'] = 'bok.is_deleted = 0';
                }
            } else {
                $wheres['is_deleted'] = 'bok.is_deleted = 0';
            }
			
			
            if (!CheckAuth::checkCredential(array('canSeeAllBooking'))) {
                if (CheckAuth::checkCredential(array('canSeeOnlyHisBooking'))) {
                    if (CheckAuth::checkCredential(array('canSeeAssignedBooking'))) {

                        $joinInner['contractor_service_booking'] = array('name' => array('csb' => 'contractor_service_booking'), 'cond' => 'bok.booking_id = csb.booking_id', 'cols' => '');
						if(empty($this->loggedUser)){
						  $this->loggedUser = CheckAuth::getLoggedUser();
						   //echo 'loggedUser is Empty';
						 }

                        $wheres['can_see'] = ("bok.created_by = {$this->loggedUser['user_id']} OR csb.contractor_id = {$this->loggedUser['user_id']}");
                    } else {
                        $wheres['can_see'] = ("bok.created_by = {$this->loggedUser['user_id']}");
                    }
                } else {
                    $wheres['can_see'] = ("bok.created_by = {$this->loggedUser['user_id']}");
                }
            }
        } else {
            $wheres['is_deleted'] = 'bok.is_deleted = 0';
        }

        if ($filters) {
			if (!empty($filters['distance'])) {
                $joinInner['contractor_service_booking2'] = array('name' => array('csb2' => 'contractor_service_booking'), 'cond' => 'bok.booking_id = csb2.booking_id', 'cols' => '');

                
                $wheres['distance'] = ("csb2.distance = 0");
            }

            if (!empty($filters['keywords'])) {
                $keywords = $this->getAdapter()->quote('%' . trim($filters['keywords']) . '%');
                $wheres['keywords'] = ("bok.full_text_search LIKE {$keywords}");

                //$keywords = '+' . preg_replace('#[\s]+#', ' +', trim($filters['keywords']));
                //$keywords = $this->getAdapter()->quote($keywords);
                //$wheres['keywords'] = ("MATCH(bok.full_text_search) AGAINST({$keywords} IN BOOLEAN MODE)");
            }
			
			//////get google event id 
			if (!empty($filters['google_event_ids']) && $filters['google_event_ids']) {
                $joinInner['google_calendar_event'] = array('name' => array('gce' => 'google_calendar_event'), 'cond' => 'gce.booking_id = bok.booking_id', 'cols' => 'google_calendar_event_id');
                
				$wheres['google_event_ids'] = ('gce.google_calendar_event_id NOt IN (' . implode(', ', $filters['google_event_ids']) . ')');
            }
			//////

            if (!empty($filters['status']) && $filters['status'] != 'all') {
                if ($filters['status'] == 'current') {

                    // current include(TO VISIT,TENTATIVE,TO DO,AWAITING UPDATE,IN PROGRESS)
                    $modelBookingStatus = new Model_BookingStatus();
                    $toVisit = $modelBookingStatus->getByStatusName('TO VISIT');
                    $tentative = $modelBookingStatus->getByStatusName('TENTATIVE');
                    $toDo = $modelBookingStatus->getByStatusName('TO DO');
                    $awaitingUpdate = $modelBookingStatus->getByStatusName('AWAITING UPDATE');
                    $inProcess = $modelBookingStatus->getByStatusName('IN PROGRESS');

                    $wheres['status'] = ("bok.status_id IN ({$toVisit['booking_status_id']},{$tentative['booking_status_id']},{$toDo['booking_status_id']},{$awaitingUpdate['booking_status_id']},{$inProcess['booking_status_id']})");
                } else {
                    $status = $this->getAdapter()->quote(trim($filters['status']));
                    $wheres['status'] = ("bok.status_id = {$status}");
                }
            }

            if (!empty($filters['order_by_status_history'])) {
                $status = $this->getAdapter()->quote(trim($filters['order_by_status_history']));
                $select_max_bsh = $this->getAdapter()->select();
                $select_max_bsh->from(array('max_bsh' => 'booking_status_history'), array('MAX(created) AS max_created', 'booking_id', 'status_id'));
                $select_max_bsh->group(array('booking_id', 'status_id'));
                $joinInner['booking_status_history'] = array('name' => array('bsh' => $select_max_bsh), 'cond' => 'bsh.booking_id = bok.booking_id AND bsh.status_id = bok.status_id', 'cols' => 'max_created');
                $wheres['order_by_status_history'] = "bsh.status_id = {$status}";
            }
			
			if (!empty($filters['service_history_contractor_id'])) {
                //$service_history_contractor_id = $this->getAdapter()->quote(trim($filters['service_history_contractor_id']));
                $select_last_service_log = $this->getAdapter()->select();
                $select_last_service_log->from(array('csbl' => 'contractor_service_booking_log'), array('MAX(log_created)', 'booking_id'));
				$select_last_service_log->where("csbl.log_user_id = {$filters['service_history_contractor_id']}");
                $select_last_service_log->group(array('booking_id'));
                $joinInner['service_history_contractor_id'] = array('name' => array('lsl' => $select_last_service_log), 'cond' => 'lsl.booking_id = bok.booking_id');
                
            }
			

            if (!empty($filters['more_one_status'])) {
                if (is_array($filters['more_one_status'])) {
                    $wheres['status'] = ('bok.status_id IN (' . implode(', ', $filters['more_one_status']) . ')');
                }
            }

			 //02/06/2015 D.A
            if (!empty($filters['my_booking_ids']) && $filters['my_booking_ids']) {
                $my_amount_booking_ids=array();
                $my_amount_booking_ids = $filters['my_booking_ids'];
                $wheres['my_booking_ids'] =("bok.booking_id IN ({$my_amount_booking_ids})");
                }
			
            if (!empty($filters['city_id'])) {
                $city_id = (int) $filters['city_id'];
                $wheres['city_id'] = ("bok.city_id = {$city_id}");
            }
            if (!empty($filters['booking_ids']) && $filters['booking_ids']) {
                $wheres['booking_ids'] = ('bok.booking_id IN (' . implode(', ', $filters['booking_ids']) . ')');
            }


            if (!empty($filters['customer_id'])) {
                $customer_id = (int) $filters['customer_id'];
                $wheres['customer_id'] = ("bok.customer_id = {$customer_id} ");
            }
			
			////by islam get all data of some customer ids which have the same business name
			if (!empty($filters['customer_ids']) && $filters['customer_ids']) {
                $wheres['customer_ids'] = ('bok.customer_id IN (' . implode(', ', $filters['customer_ids']) . ')');
            }
			/////
            if (!empty($filters['contractor_id'])) {
                $contractor_id = (int) $filters['contractor_id'];
                $joinInner['contractor_service_booking'] = array('name' => array('csb' => 'contractor_service_booking'), 'cond' => 'bok.booking_id = csb.booking_id', 'cols' => '');

                $wheres['created_by'] = ("bok.created_by = {$contractor_id} OR csb.contractor_id = {$contractor_id}");
            }

            if (!empty($filters['created_by'])) {
                $userId = (int) $filters['created_by'];
                $wheres['created_by'] = ("bok.created_by = '{$userId}'");
            }

            if (!empty($filters['booking_num'])) {
                $booking_num = $this->getAdapter()->quote('%' . trim($filters['booking_num']) . '%');
                $wheres['booking_num'] = ("bok.booking_num LIKE {$booking_num}");
            }

            if (!empty($filters['estimate_num'])) {
                $joinInner['booking_estimate'] = array('name' => array('est' => 'booking_estimate'), 'cond' => 'bok.booking_id = est.booking_id', 'cols' => '');

                $estimate_num = $this->getAdapter()->quote('%' . trim($filters['estimate_num']) . '%');
                $wheres['estimate_num'] = ("est.estimate_num LIKE {$estimate_num}");
            }
            if (!empty($filters['inner_join_estimate'])) {
                $joinInner['booking_estimate'] = array('name' => array('est' => 'booking_estimate'), 'cond' => 'bok.booking_id = est.booking_id', 'cols' => '');
            }
            if (!empty($filters['inner_join_invoice'])) {
                $joinInner['booking_invoice'] = array('name' => array('inv' => 'booking_invoice'), 'cond' => 'bok.booking_id = inv.booking_id', 'cols' => '');
            }
            if (!empty($filters['invoice_num'])) {
                $joinInner['booking_invoice'] = array('name' => array('inv' => 'booking_invoice'), 'cond' => 'bok.booking_id = inv.booking_id', 'cols' => '');

                $invoice_num = $this->getAdapter()->quote('%' . trim($filters['invoice_num']) . '%');
                $wheres['invoice_num'] = ("inv.invoice_num LIKE {$invoice_num}");
            }

            if (!empty($filters['is_change'])) {
                $is_change = (int) $filters['is_change'];
                $wheres['is_change'] = ("bok.is_change = {$is_change}");
				
				if(!empty($filters['contractors_id'])){
					
					$contractors_id = (int) $filters['contractors_id'];
					$joinInner['contractor_service_booking'] = array('name' => array('csb' => 'contractor_service_booking'), 'cond' => 'bok.booking_id = csb.booking_id', 'cols' => '');

					$wheres['created_bys'] = ("bok.created_by = {$contractors_id} OR csb.contractor_id = {$contractors_id}");
				}
            }



            if (!empty($filters['title'])) {
                $title = $this->getAdapter()->quote('%' . trim($filters['title']) . '%');
                $wheres['title'] = ("bok.title LIKE {$title}");
            }

            if (!empty($filters['created_from'])) {
                $created_from = $this->getAdapter()->quote(strtotime(trim($filters['created_from'])));
                $wheres['created_from'] = ("bok.created >= {$created_from}");
            }

            if (!empty($filters['created_to'])) {
                $created_to = $this->getAdapter()->quote(strtotime(trim($filters['created_to'])));
                $wheres['created_to'] = ("bok.created <= {$created_to}");
            }

            if (!empty($filters['calendar_booking_start']) && !empty($filters['calendar_booking_end'])) {
                $calendarBookingStart = $filters['calendar_booking_start'];
                $calendarBookingEnd = $filters['calendar_booking_end'];
                $wheres['booking_start'] = ("bok.booking_start between '" . php2MySqlTime($calendarBookingStart) . "' and '" . php2MySqlTime($calendarBookingEnd) . "'");
            }
            if (!empty($filters['booking_start_between'])) {
                $bookingStartBetween = $filters['booking_start_between'];
                $bookingEndBetween = !empty($filters['booking_end_between']) ? $filters['booking_end_between'] : date('Y-m-d H:i:s', time());
                $wheres['booking_start'] = ("bok.booking_start between '" . $bookingStartBetween . "' and '" . $bookingEndBetween . "'");
            }
			if (!empty($filters['booking_not_started_yet'])) {
				$today = date('Y-m-d H:i:s', time());
                $wheres['booking_start'] = ("bok.booking_start > '{$today}'");
            }

            if (!empty($filters['time_between'])) {
                $time_between = isset($filters['time_between']) ? $filters['time_between'] : 0;
                if (is_int($time_between)) {
                    $time_between = date('Y-m-d H:i:s', $time_between);
                } else {
                    $time_between = date('Y-m-d H:i:s', strtotime($time_between));
                }
                $wheres['time_between'] = ("'{$time_between}' between bok.booking_start and bok.booking_end");
            }
			/// by islam to search by date only in the start and end time
            if (!empty($filters['date'])) {
                $date= date('Y-m-d', strtotime(trim($filters['date'])));
                $date = $this->getAdapter()->quote('%'.trim($date).'%');
                $wheres['date'] = ("bok.booking_start like {$date} OR bok.booking_end like {$date}");
            }

            /*if (!empty($filters['booking_end'])) {
                $bookingEnd = date('Y-m-d H:i:s', strtotime(trim($filters['booking_end'])));
                $bookingEnd = $this->getAdapter()->quote(trim($bookingEnd));
				
                $wheres['booking_end'] = ("bok.booking_end <= {$bookingEnd}");
            }*/

            if (!empty($filters['booking_in_this_day'])) {
                $bookingStart = strtotime(trim($filters['booking_in_this_day']));
                $bookingStartBetween = date("Y-m-d", $bookingStart) . " 00:00:00";
                $bookingEndBetween = date("Y-m-d", $bookingStart) . " 23:59:59";
                $wheres['booking_in_this_day'] = ("(bok.booking_start between '" . $bookingStartBetween . "' AND '" . $bookingEndBetween . "') OR (bok.booking_end between '" . $bookingStartBetween . "' AND '" . $bookingEndBetween . "')");
            }

            if (!empty($filters['is_all_day_event'])) {
                $isAllDayEvent = $this->getAdapter()->quote(trim($filters['is_all_day_event']));
                $wheres['is_all_day_event'] = ("bok.is_all_day_event = {$isAllDayEvent}");
            }

            if (!empty($filters['convert_status']) && $filters['convert_status'] != 'all') {
                $convert_status = $this->getAdapter()->quote(trim($filters['convert_status']));
                $wheres['convert_status'] = ("bok.convert_status = {$convert_status}");
            }

            if (!empty($filters['withoutEstimateStatus'])) {
                $wheres['withoutEstimateStatus'] = ("bok.convert_status NOT LIKE 'estimate'");
            }

            if (!empty($filters['estimate_type'])) {
                $joinInner['booking_estimate'] = array('name' => array('est' => 'booking_estimate'), 'cond' => 'bok.booking_id = est.booking_id', 'cols' => '');

                $estimate_type = $this->getAdapter()->quote(trim($filters['estimate_type']));
                $wheres['estimate_type'] = ("est.estimate_type = {$estimate_type}");
            }

            if (!empty($filters['invoice_type'])) {
                $joinInner['booking_invoice'] = array('name' => array('inv' => 'booking_invoice'), 'cond' => 'bok.booking_id = inv.booking_id', 'cols' => '');

                $invoice_type = $this->getAdapter()->quote(trim($filters['invoice_type']));
                if ('unpaid' == $filters['invoice_type']) {
                    $wheres['invoice_type'] = ("inv.invoice_type = 'overdue' OR inv.invoice_type = 'open'");
                } elseif ('all_active' == $filters['invoice_type']) {
                    $wheres['invoice_type'] = ("inv.invoice_type = 'overdue' OR inv.invoice_type = 'open' OR inv.invoice_type = 'closed'");
                } else {
                    $wheres['invoice_type'] = ("inv.invoice_type = {$invoice_type}");
                }
            }

            if (!empty($filters['service_id'])) {
                $joinInner['contractor_service_booking'] = array('name' => array('csb' => 'contractor_service_booking'), 'cond' => 'bok.booking_id = csb.booking_id', 'cols' => '');

                $service_id = (int) trim($filters['service_id']);
                $wheres['service_id'] = ("csb.service_id = {$service_id}");
            }
            if (!empty($filters['attribute_value_id'])) {
                $joinInner['service_attribute_value'] = array('name' => array('sav' => 'service_attribute_value'), 'cond' => 'sav.booking_id = bok.booking_id', 'cols' => '');
                $joinInner['service_attribute'] = array('name' => array('sa' => 'service_attribute'), 'cond' => 'sa.service_attribute_id = sav.service_attribute_id', 'cols' => '');
                $joinInner['attribute'] = array('name' => array('a' => 'attribute'), 'cond' => 'sa.attribute_id = a.attribute_id', 'cols' => '');
                $joinInner['attribute_type'] = array('name' => array('at' => 'attribute_type'), 'cond' => 'at.attribute_type_id = a.attribute_type_id', 'cols' => '');

                $attribute_value_id = (int) trim($filters['attribute_value_id']);
                $wheres['attribute_value_id'] = ("sav.value = {$attribute_value_id}");
                $wheres['attribute_value_id_is_list'] = ("at.is_list = 1");
            }

            if (!empty($filters['label_ids']) && $filters['label_ids']) {
                $joinInner['booking_label'] = array('name' => array('bl' => 'booking_label'), 'cond' => 'bok.booking_id = bl.booking_id', 'cols' => '');
                $wheres['label_id'] = ('bl.label_id IN (' . implode(', ', $filters['label_ids']) . ')');
            }

            if (!empty($filters['invoice_label_ids']) && $filters['invoice_label_ids']) {
                $joinInner['invoice_label'] = array('name' => array('il' => 'invoice_label'), 'cond' => 'bok.booking_id = il.invoice_id', 'cols' => '');
                $wheres['invoice_label_ids'] = ('il.label_id IN (' . implode(', ', $filters['invoice_label_ids']) . ')');
            }

            if (!empty($filters['estimate_label_ids']) && $filters['estimate_label_ids']) {
                $joinInner['estimate_label'] = array('name' => array('el' => 'estimate_label'), 'cond' => 'bok.booking_id = el.estimate_id', 'cols' => '');
                $wheres['estimate_label_ids'] = ('el.label_id IN (' . implode(', ', $filters['estimate_label_ids']) . ')');
            }

            if (!empty($filters['customer_name'])) {
                if (!$this->modelCustomer) {
                    $this->modelCustomer = new Model_Customer();
                }
                $allCustomer = $this->modelCustomer->getCustomerIdByIdFullNameSearch(trim($filters['customer_name']));

                $customerIds = implode(', ', $allCustomer);
                if ($customerIds) {
                    $wheres['customer_name'] = ("bok.customer_id in ( $customerIds )");
                }
            }

            if (!empty($filters['qoute_from'])) {
                $qoute_from = $this->getAdapter()->quote(trim($filters['qoute_from']));
                $wheres['qoute_from'] = ("bok.qoute >= {$qoute_from}");
            }

            if (!empty($filters['qoute_to'])) {
                $qoute_to = $this->getAdapter()->quote(trim($filters['qoute_to']));
                $wheres['qoute_to'] = ("bok.qoute <= {$qoute_to}");
            }

            if (!empty($filters['company_id'])) {
                $company_id = (int) $filters['company_id'];
                $wheres['company_id'] = ("bok.company_id = {$company_id}");
            }

            if (!empty($filters['address'])) {
                $joinInner['booking_address'] = array('name' => array('ba' => 'booking_address'), 'cond' => 'bok.booking_id = ba.booking_id', 'cols' => '');

                $address = $this->getAdapter()->quote('%' . trim($filters['address']) . '%');
                $wheres['address'] = ("CONCAT_WS(' ',street_number,street_address,suburb,state,postcode) LIKE {$address}");
            }

            if (!empty($filters['email'])) {
                $joinInner['customer'] = array('name' => array('c' => 'customer'), 'cond' => 'bok.customer_id = c.customer_id', 'cols' => '');

                $email = $this->getAdapter()->quote('%' . trim($filters['email']) . '%');
                $wheres['email'] = ("CONCAT_WS(' ',email1,email2,email3) LIKE {$email}");
            }

            if (!empty($filters['mobile/phone'])) {
                $joinInner['customer'] = array('name' => array('c' => 'customer'), 'cond' => 'bok.customer_id = c.customer_id', 'cols' => '');

                $mobile_phone = $this->getAdapter()->quote('%' . trim($filters['mobile/phone']) . '%');
                $wheres['mobile/phone'] = ("CONCAT_WS(' ',phone1,phone2,phone3,mobile1,mobile2,mobile3) LIKE {$mobile_phone}");
            }

            if (!empty($filters['bussiness_name'])) {
                $joinInner['customer_commercial_info'] = array('name' => array('cci' => 'customer_commercial_info'), 'cond' => 'bok.customer_id = cci.customer_id', 'cols' => '');

                $bussiness_name = $this->getAdapter()->quote('%' . trim($filters['bussiness_name']) . '%');
                $wheres['bussiness_name'] = ("cci.business_name LIKE {$bussiness_name}");
            }

            if (!empty($filters['my_bookings'])) {

                $user_id = (int) $filters['my_bookings'];

                $my_booking_sql = $this->getAdapter()->select();
                $my_booking_sql->from('booking_log', 'booking_id');
                $my_booking_sql->where("log_user_id = {$user_id}");
                $my_booking_sql->distinct();
                $my_booking_results = $this->getAdapter()->fetchAll($my_booking_sql);

                $not_my_booking_sql = $this->getAdapter()->select();
                $not_my_booking_sql->from('booking_log', 'booking_id');
                $not_my_booking_sql->distinct();

                if ($my_booking_results) {
                    $my_booking_ids = array();
                    foreach ($my_booking_results as $my_result) {
                        $my_booking_ids[$my_result['booking_id']] = $my_result['booking_id'];
                    }
                    $not_my_booking_sql->where('booking_id NOT IN (' . implode(', ', $my_booking_ids) . ')');
                }

                $not_my_results = $this->getAdapter()->fetchAll($not_my_booking_sql);

                $not_my_booking_ids = array();

                foreach ($not_my_results as $not_my_result) {
                    $not_my_booking_ids[$not_my_result['booking_id']] = $not_my_result['booking_id'];
                }

                if ($not_my_booking_ids) {
                    $wheres['my_bookings'] = ('bok.booking_id NOT IN (' . implode(', ', $not_my_booking_ids) . ')');
                }
            }

            if (!empty($filters['acceptance'])) {
                $joinInner['contractor_service_booking'] = array('name' => array('csb' => 'contractor_service_booking'), 'cond' => 'bok.booking_id = csb.booking_id', 'cols' => '');

                if ($filters['acceptance'] == 'accepted') {
                    $wheres['acceptance'] = ("csb.is_accepted = 1");
                } elseif ($filters['acceptance'] == 'rejected') {
                    $wheres['acceptance'] = ("csb.is_rejected = 1");
                } else {
					if(!empty($filters['contractor_id'])){
						 $wheres['contractor_id'] = ("contractor_id = {$filters['contractor_id']} ");
						 $wheres['acceptances'] = ("csb.is_accepted = 0 AND csb.is_rejected = 0");
					}else{
                    $wheres['acceptance'] = ("csb.is_accepted = 0 AND csb.is_rejected = 0");
					}
					
                }
            }
			
			if (!empty($filters['area'])) {
				
               /* $joinInner['contractor_service_booking'] = array('name' => array('csb' => 'contractor_service_booking'), 'cond' => 'bok.booking_id = csb.booking_id', 'cols' => '');
				$attributes_model = new Model_Attributes();
				$area_attribute = $attributes_model->getByVariableName('area');
				$area_attribute_id =$area_attribute['attribute_id'];
				$joinInner['service_attribute'] = array('name' => array('sa' => 'service_attribute'), 'cond' => 'csb.service_id = sa.service_id', 'cols' => '');
				$wheres['area'] = ("sa.attribute_id = {$area_attribute_id}");
                $joinInner['service_attribute_value'] = array('name' => array('sav' => 'service_attribute_value'), 'cond' => 'sav.service_attribute_id = sa.service_attribute_id', 'cols' => '');
				$wheres['area_value'] = ("sav.value = {$area_attribute_id}");
                $joinInner['attribute_list_value'] = array('name' => array('alv' => 'attribute_list_value'), 'cond' => 'alv.attribute_value_id = sav.value', 'cols' => '');
				$wheres['area_value'] = ("alv.attribute_value = '{$filters['Area']}'");	*/

				$joinInner['service_attribute_value'] = array('name' => array('sav' => 'service_attribute_value'), 'cond' => 'bok.booking_id = sav.booking_id', 'cols' => '');
				$joinInner['attribute_list_value'] = array('name' => array('alv' => 'attribute_list_value'), 'cond' => 'alv.attribute_value_id = sav.value', 'cols' => '');
				
				
				$wheres['area_value'] = ("alv.attribute_value_id = '{$filters['area']}'");	
				
            }

            if (!empty($filters['state'])) {
                $joinInner['booking_address'] = array('name' => array('ba' => 'booking_address'), 'cond' => 'bok.booking_id = ba.booking_id', 'cols' => '');
                $state = $this->getAdapter()->quote('%' . trim($filters['state']) . '%');
                $wheres['state'] = ("ba.state LIKE {$state}");
            }
			

            if (!empty($filters['to_follow']) && $filters['to_follow'] = true) {
                $time = time();
                $wheres['to_follow'] = ("bok.to_follow <= {$time} AND bok.to_follow > 0");
            }

            if (!empty($filters['not_booking_ids']) && $filters['not_booking_ids']) {
                if (is_array($filters['not_booking_ids'])) {
                    $wheres['booking_id'] = ('bok.booking_id NOT IN (' . implode(', ', $filters['not_booking_ids']) . ')');
                }
            }

            if (!empty($filters['not_booking_id'])) {
                $not_booking_id = (int) $filters['not_booking_id'];
                $wheres['booking_id'] = ("bok.booking_id != {$not_booking_id}");
            }

            if (!empty($filters['booking_id'])) {
                $booking_id = (int) $filters['booking_id'];
                $wheres['booking_id'] = ("bok.booking_id = {$booking_id}");
            }

            if (!empty($filters['inquiry_num'])) {
                $joinInner['inquiry'] = array('name' => array('i' => 'inquiry'), 'cond' => 'bok.original_inquiry_id = i.inquiry_id', 'cols' => '');
                $inquiry_num = $this->getAdapter()->quote('%' . trim($filters['inquiry_num']) . '%');
                $wheres['inquiry_num'] = ("i.inquiry_num LIKE {$inquiry_num}");
            }

            if (!empty($filters['original_inquiry_id'])) {
                $original_inquiry_id = (int) $filters['original_inquiry_id'];
                $wheres['original_inquiry_id'] = ("bok.original_inquiry_id = {$original_inquiry_id}");
            }
        }

        return array('wheres' => $wheres, 'joinInner' => $joinInner);
    }
	
	public function getBookingNumById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name,array('booking_num'));
        $select->where("booking_id = '{$id}'");
		$row = $this->getAdapter()->fetchRow($select);
        return $row['booking_num'];
    }
    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null, $limit = 0, $perPage = 0, $currentPage = 0) {
		//print_r($filters);
		
        $select = $this->getAdapter()->select();
        $select->from(array('bok' => $this->_name));
        $select->distinct();
		

        if ($order) {
            $select->order($order);
        }

        $wheresAndJoins = $this->getWheresAndJoinsByFilters($filters);
		
        $joinInner = $wheresAndJoins['joinInner'];
        $wheres = $wheresAndJoins['wheres'];
		

        if ($wheres) {
            foreach ($wheres as $where) {
				$select->where($where);
            }
        }
		

        if ($joinInner) {
            foreach ($joinInner as $inner) {
			    $select->joinInner($inner['name'], $inner['cond'], $inner['cols']);
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        } elseif ($limit) {
            $select->limit($limit);
        } elseif ($perPage && $currentPage) {
            $select->limit($perPage, ($currentPage - 1) * $perPage);
        }
		
        //echo '<pre>' . $select . '</pre>';
		$str = $select->__toString();
		//echo $str;
		$select->group('bok.booking_id');
		
		
		
		
		
        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * getRecentBooking
     * 
     * @param array $filters
     * @param string $order
     * @return array
     */
    public function getRecentBooking() {

        $filters = array();

        $filters['convert_status'] = 'booking';

        // because pager sending by refrance
        $pager = null;

        return $this->getAll($filters, 'bok.created DESC', $pager, 100);
    }

    /**
     * getRecentBooking
     * 
     * @param array $filters
     * @param string $order
     * @rjoin inner in zend framework with more columnseturn array
     */
    public function getRecentQuoted() {

        $filters = array();

        $filters['convert_status'] = 'estimate';

        // because pager sending by refrance
        $pager = null;

        return $this->getAll($filters, 'bok.created DESC', $pager, 100);
    }

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
		public function notVisitedBooking($id) {
		$id = (int) $id;
			$success = parent::update(array('visited' => 0), "booking_id = '{$id}'");
			return $success;
		}
		public function visitedBooking($id) {
		$id = (int) $id;
			$success = parent::update(array('visited' => 1), "booking_id = '{$id}'");
			return $success;
		}
		
		public function updateById($id, $data, $addLog = true,$log_id = 0) {


        if (isset($data['status_id']) && $data['status_id']) {
            /**
             * save  booking status history
             */
            $modelBookingStatusHistory = new Model_BookingStatusHistory();
            $modelBookingStatusHistory->modifyStatusHistory($id, $data['status_id']);
        }
        /**
         * change ios Sync Booking
         */
        $modelIosSync = new Model_IosSync();
        $modelIosSync->changeSyncBooking($id);
		
		 
        /**
         * update full text search table
         */
        $done = false;
        if (!empty($data['full_text_search'])) {
            $id = (int) $id;
            $done = true;
        }
        $modelUpdateFullTextSearch = new Model_UpdateFullTextSearch();
        $modelUpdateFullTextSearch->updateFullTextSearchFlag($this->_name, $id, $done);


        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'changed');

        /**
         * update Booking 
         */
        $success = parent::update($data, "booking_id = '{$id}'");

        /**
         * get all record and insert it in log
         */
        $modelBookingLog = new Model_BookingLog();
        //$last = $modelBookingLog->getLastLogByBookingId($id);
        //$lastLogBookingId = !empty($last) ? $last['log_id'] : 0;
        if ($addLog) {
            $log_id = $modelBookingLog->addBookingLog($id);
        } else {
            $data = $this->getById($id);
            $modelBookingLog->updateById($log_id, $data);
        }
		
		
		/*if($_SERVER['REMOTE_ADDR'] == '176.106.46.142'){
			foreach ($this->getAdapter()->getProfiler()->getQueryProfiles() as $query) {
				 echo $query->getQuery();
				 echo ';';
			   }
			//exit;   
		   }*/
		
		
		return array('success'=>$success , 'log_id'=>$log_id);
        //return $success;
    }

    public function updateFullTextSearch($bookingId) {

        // load Model
        $modelBookingStatus = new Model_BookingStatus();

        $booking = $this->getById($bookingId);

        $fullTextSearch = array();
        $fullTextSearch[] = trim($booking['booking_num']);
        $fullTextSearch[] = trim($booking['title']);
        $fullTextSearch[] = trim($booking['description']);
        $status = $modelBookingStatus->getById($booking['status_id']);
        $fullTextSearch[] = trim($status['name']);


        // address
        $modelBookingAddress = new Model_BookingAddress();
        $bookingAddress = $modelBookingAddress->getByBookingId($bookingId);
        $fullTextSearch[] = trim(get_line_address($bookingAddress));

        //customer
        $modelCustomer = new Model_Customer();
        $fullTextCustomer = $modelCustomer->getFullTextCustomerContacts($booking['customer_id']);
        $fullTextSearch[] = trim($fullTextCustomer);

        // services
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $fullTextContractorServiceBooking = $modelContractorServiceBooking->getFullTextContractorServiceBooking($bookingId);
        if ($fullTextContractorServiceBooking) {
            $fullTextSearch[] = trim($fullTextContractorServiceBooking);
        }

        //inquiry
        if (isset($booking['original_inquiry_id']) && $booking['original_inquiry_id']) {
            $modelInquiry = new Model_Inquiry();
            $Inquiry = $modelInquiry->getById($booking['original_inquiry_id']);
            if ($Inquiry['inquiry_num']) {
                $fullTextSearch[] = trim($Inquiry['inquiry_num']);
            }
        }

        //estimate
        $modelBookingEstimate = new Model_BookingEstimate();
        $estimate = $modelBookingEstimate->getByBookingId($bookingId);
        if (isset($estimate['estimate_num']) && $estimate['estimate_num']) {
            $fullTextSearch[] = trim($estimate['estimate_num']);
        }

        //invoice
        $modelBookingInvoice = new Model_BookingInvoice();
        $invoice = $modelBookingInvoice->getByBookingId($bookingId);
        if (isset($invoice['invoice_num']) && $invoice['invoice_num']) {
            $fullTextSearch[] = trim($invoice['invoice_num']);
        }
        if (isset($invoice['invoice_type']) && $invoice['invoice_type']) {
            $fullTextSearch[] = trim($invoice['invoice_type']);
        }

        //complaint
        $model_Complaint = new Model_Complaint();
        $complaints = $model_Complaint->getFullTextComplaintByBookingId($bookingId);
        if ($complaints) {
            $fullTextSearch[] = trim($complaints);
        }

        $data = array();
        $data['full_text_search'] = implode(' ', $fullTextSearch);

        $this->updateById($bookingId, $data, false);
    }

    /**
     * delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;

        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'deleted');
		
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'deleted');

		
        return parent::delete("booking_id = '{$id}'");
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
	 
	public function addContractorsLog($id){
	  
	  if($_SERVER['REMOTE_ADDR'] == '176.106.46.142'){
            $stmt =  $this->getAdapter()->query("select * from contractor_service_booking_log where booking_id = '{$id}' ");
            $rows = $stmt->fetchAll();	
            print_r($rows);			          
		}
	
	} 
	 
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_id = '{$id}'");
		
        return $this->getAdapter()->fetchRow($select);
    }
	
	public function getCustomerByBookingId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name,array());
        $select->join('customer','booking.customer_id = customer.customer_id',array('email1','email2','email3'));
        $select->where("booking_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }
	
	public function getCustomerByBookingNum($bok_num){
		
        $select = $this->getAdapter()->select();
        $select->from($this->_name,array());
        $select->join('customer','booking.customer_id = customer.customer_id',array('email1','email2','email3'));
        $select->where("booking_num = '{$bok_num}'");

        return $this->getAdapter()->fetchRow($select);
	}

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getByInquiryId($inquiryId) {
        $inquiryId = (int) $inquiryId;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("original_inquiry_id = '{$inquiryId}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the assigned ustomer Id
     * 
     * @param int $customerId
     * @return array
     */
    public function getByCustomerId($customerId) {
        $customerId = (int) $customerId;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("customer_id = '{$customerId}'");
        $select->where("is_deleted = 0");
        $result = $this->getAdapter()->fetchAll($select);

        return $result;
    }

    public function getbyContractorAndAwaitingUpdateInProcessStatus($contractorId) {
        $contractorId = (int) $contractorId;

        $modelBookingStatus = new Model_BookingStatus();
        $inProcess = $modelBookingStatus->getByStatusName('IN PROGRESS');
        $awaitingUpdate = $modelBookingStatus->getByStatusName('AWAITING UPDATE');

        $select = $this->getAdapter()->select();
        $select->from(array('bok' => $this->_name));
        $select->where("bok.is_deleted = 0");
        $select->where("bok.status_id = '{$inProcess['booking_status_id']}' OR bok.status_id = '{$awaitingUpdate['booking_status_id']}'");
        $select->joinInner(array('csb' => 'contractor_service_booking'), 'bok.booking_id = csb.booking_id');
        $select->where("csb.contractor_id = '{$contractorId}'");
        $result = $this->getAdapter()->fetchAll($select);
        return $result;
    }

    /**
     * getBookingByCustomerId
     * 
     * @param int $customerId
     * @return array 
     */
    public function getBookingByCustomerId($customerId) {

        $filters = array();
        $filters['customer_id'] = $customerId;

        return $this->getAll($filters);
    }

    public function insert(array $data) {

        $company_id = CheckAuth::getCompanySession();

        $select = $this->getAdapter()->select();
        $select->from($this->_name, 'MAX(count)');
        $select->where("company_id = {$company_id}");
        $result = $this->getAdapter()->fetchOne($select);

        $data['count'] = $result + 1;
        $data['booking_num'] = 'BOK-' . ($result + 1);

        $id = parent::insert($data);

        /**
         * update full text search table
         */
        $modelUpdateFullTextSearch = new Model_UpdateFullTextSearch();
        $modelUpdateFullTextSearch->insertFullTextSearch($this->_name, $id);

        /**
         * get all record and insert it in log
         */
		 
		$log_id  = 0 ;
        $modelBookingLog = new Model_BookingLog();
        $log_id = $modelBookingLog->addBookingLog($id);

        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'added');
		
        return array('id'=>$id , 'log_id'=>$log_id);
        
    }

    public function addCalendar($st, $et, $sub, $ade, $user) {
        $ret = array();
        try {
            $data = array(
                'title' => $sub,
                'booking_start' => php2MySqlTime(js2PhpTime($st)),
                'booking_end' => php2MySqlTime(js2PhpTime($et)),
                'is_all_day_event' => $ade,
                'created_by' => $user,
                'created' => time()
            );
			$insertArr = $this->insert($data);
            $insert_id = $insertArr['id'];
            if ($insert_id) {
                $ret['IsSuccess'] = true;
                $ret['Msg'] = 'add success';
                $ret['Data'] = $insert_id;
            } else {
                $ret['IsSuccess'] = false;
                $ret['Msg'] = mysql_error();
            }
        } catch (Exception $e) {
            $ret['IsSuccess'] = false;
            $ret['Msg'] = $e->getMessage();
        }
        return $ret;
    }

    public function addDetailedCalendar($data = array()) {
        $ret = array();
        try {
		    $insertArr = $this->insert($data);
            $insert_id = $insertArr['id'];
			
            if ($insert_id) {
                $ret['IsSuccess'] = true;
                $ret['Msg'] = 'add success';
                $ret['Data'] = $insert_id;
                $ret['log'] = $insertArr['log_id'];
				/**
				 * send notification
				 */
				//MobileNotification::notify($insert_id,'new booking');		
				//D.A 09/08/2015 				
				    $booking_start=date('Y-m-d', strtotime($data['booking_start']));
					$firstDay = date('d', strtotime($booking_start));
					if($firstDay >= 1 && $firstDay <= 7){
					$localEventsCacheId= date("Y_m",strtotime($booking_start)).'_'.'1';
					}else if($firstDay >= 8 && $firstDay <= 14){
					$localEventsCacheId= date("Y_m",strtotime($booking_start)).'_'.'2';	
					}else if($firstDay >= 15 && $firstDay <= 21){
					$localEventsCacheId= date("Y_m",strtotime($booking_start)).'_'.'3';	
					}else{
					$localEventsCacheId= date("Y_m",strtotime($booking_start)).'_'.'4';		
					}
					$localEventsDir=get_config('cache').'/'.'localEvents';			
					$frontEndOption= array('lifetime'=> 24 * 3600,
					'automatic_serialization'=> true);
					$backendOptions = array('cache_dir'=>$localEventsDir );
					$cache = Zend_Cache::factory('Core','File',$frontEndOption,$backendOptions);
					$cache->remove($localEventsCacheId);
					
            } else {
                $ret['IsSuccess'] = false;
                $ret['Msg'] = mysql_error();
                $ret['log'] = '';
            }
        } catch (Exception $e) {
            $ret['IsSuccess'] = false;
            $ret['Msg'] = $e->getMessage();
        }
        return $ret;
    }
/*
     public function listCalendarByRange($sd, $ed, $filters = array()) {
        $ret = array();
        $ret['events'] = array();
        //$ret["issort"] = true;
        //$ret["start"] = php2JsTime($sd);
        //$ret["end"] = php2JsTime($ed);
        //$ret['error'] = null;
		//print_r($ret);
		//print_r($filters);
		if ($filters['contractor_id']) {
			//////////get live events By Islam
			
			$modelContractorGmailAccounts = new Model_ContractorGmailAccounts();

			// get the gmail account contractor
			$contractorAccount = $modelContractorGmailAccounts->getByContractorId($filters['contractor_id']);
			$user = isset($contractorAccount['email']) ? $contractorAccount['email'] : '';
			$pass = isset($contractorAccount['password']) ? $contractorAccount['password'] : '';
			//print_r($contractorAccount);
			if($user && $pass){		
				$minCheck = date(DATE_ATOM, $sd);
				$maxCheck = date(DATE_ATOM, $ed);
			
				$optParam = array("timeMin"=>$minCheck , 
                                 "timeMax"=>$maxCheck); 
								 
				$modelCalendarServiceAccount = new Model_CalendarServiceAccount($filters['contractor_id']);
				$event_list = $modelCalendarServiceAccount->getAllEvents($optParam);
				//print_r($event_list);
				foreach ($event_list as $event)
				{
					$slash_tokns = explode("/",$event['id']);
					//echo 'last element  '.$slash_tokns[sizeof($slash_tokns) - 1];
					$event_id = $slash_tokns[sizeof($slash_tokns) - 1];
					$qouted_event_id = "'".$event_id."'";
					//$qouted_event_id = $this->getAdapter()->quote($event_id);
					//$qouted_event_id = $conn->quote($event_id);
					
					//$event_ids[] = $slash_tokns[sizeof($slash_tokns) - 1];	
					$event_ids[] = $qouted_event_id;	
					
					
				}
				
				//$filters['google_event_ids'] = $event_ids;
				//print_r($event_ids);

			
				foreach ($event_list as $event)
				{	
					//print_r($event);
					//////formate start date
					$pure_start_date = '';
					$pure_end_date = '';
					if(!empty($event['start'])){					
						$start = $event['start'];
						$start_date = $start->getDateTime();
						$start_date = str_replace("T", " ", $start_date);
						
						$start_date = str_replace("Starts:", "", $start_date);
						$remove_millisecond = explode(".",$start_date);
						$pure_start_date = strtotime($remove_millisecond[0]);
						$pure_start_date = date('m/d/Y H:i', $pure_start_date); 
						
					}
					if(!empty($event['end'])){
						//////the same with end date 
						$end = $event['end'];
						$end_date = $end->getDateTime();
						$end_date = str_replace("T", " ", $end_date);
						
						$end_date = str_replace("Starts:", "", $end_date);
						$remove_millisecond = explode(".",$end_date);
						$pure_end_date = strtotime($remove_millisecond[0]);
						$pure_end_date = date('m/d/Y H:i', $pure_end_date); 
						
					}
					
					if($pure_start_date){
					
						$ret['events'][] = array(
							0,
							$event->getSummary(),
							$pure_start_date,
							$pure_end_date,
							0,
							0, //more than one day event
							   //$row->InstanceType,
							0, //Recurring event,
							15,
							0, //editable
							$event->getDescription(),
							//$row['address'],
							''//$attends
						);
					}
				}
				
			} //end of if user and pass
		}

        try {
            //
            //select all booking to calendar
            //
            $filters['calendar_booking_start'] = $sd;
            $filters['calendar_booking_end'] = $ed;

            $Model_BookingMultipleDays = new Model_BookingMultipleDays();
            $bookingMultipleDays = $Model_BookingMultipleDays->getAll($filters, 'booking_start ASC');

            $bookings = $this->getAll($filters, 'booking_start ASC');

            $handles = array_merge($bookings, $bookingMultipleDays);

            if (!$this->modelBookingStatus) {
                $this->modelBookingStatus = new Model_BookingStatus();
            }

            foreach ($handles as &$handle) {
                $bookingStatus = $this->modelBookingStatus->getById($handle['status_id']);
                $handle['color'] = $bookingStatus['color'];
                $handle['booking_status'] = $bookingStatus['name'];
            }

            //echo $sql;
            foreach ($handles as $row) {
                //$ret['events'][] = $row;
                //$attends = $row->AttendeeNames;
                //if($row->OtherAttendee){
                //  $attends .= $row->OtherAttendee;
                //}
                //echo $row->startTime;
                // $row['color']=1;
                $ret['events'][] = array(
                    isset($row['booking_id']) ? $row['booking_id'] : $row['inquiry_id'],
                    $row['booking_status'] . ' - ' . $row['title'],
                    php2JsTime(mySql2PhpTime($row['booking_start'])),
                    php2JsTime(mySql2PhpTime($row['booking_end'])),
                    $row['is_all_day_event'],
                    0, //more than one day event
                    //$row->InstanceType,
                    0, //Recurring event,
                    $row['color'],
                    1, //editable
                    isset($row['booking_id']) ? 'booking' : 'inquiry',
                    //$row['address'],
                    ''//$attends
                );
            }
			
			
        } catch (Exception $e) {
            $ret['error'] = $e->getMessage();
        }
		
        return $ret;
    }*/
	
	public function listCalendarByRange($sd, $ed, $filters = array()) {
        $ret = array();
		$colors = array (
                            		'0' => '#888888',
                            		'1' => '#cc3333',
                            		'2' => '#dd4477',
                            		'3' => '#994499',
                            		'4' => '#6633cc',
                            		'5' => '#336699',
                            		'6' => '#3366cc',
                            		'7' => '#22aa99',
                            		'8' => '#329262',
                            		'9' => '#109618',
                            		'10' => '#66aa00',
                            		'11' => '#aaaa11',
                            		'12' => '#d6ae00',
                            		'13' => '#ee8800',
                            		'14' => '#dd5511',
                            		'15' => '#a87070',
                            		'16' => '#8c6d8c',
                            		'17' => '#627487',
                            		'18' => '#7083a8',
                            		'19' => '#5c8d87',
                            		'20' => '#898951',
                            		'21' => '#b08b59',
                            		'-1' => '#7a367a' 
                            );
       
		
        //$ret['events'] = array();
        //$ret["issort"] = true;
        //$ret["start"] = php2JsTime($sd);
        //$ret["end"] = php2JsTime($ed);
        //$ret['error'] = null;
		if ($filters['contractor_id']) {
			//////////get live events By Islam
			$loggedUser = CheckAuth::getLoggedUser();
			$loggedUserId = $loggedUser['user_id'];
			$modelContractorGmailAccounts = new Model_ContractorGmailAccounts();

			// get the gmail account contractor
			$contractorAccount = $modelContractorGmailAccounts->getByContractorId($filters['contractor_id']);
			$user = isset($contractorAccount['email']) ? $contractorAccount['email'] : '';
			$pass = isset($contractorAccount['password']) ? $contractorAccount['password'] : '';
			
			if($user && $pass){	
			
				/*$service = Zend_Gdata_Calendar::AUTH_SERVICE_NAME; // predefined service name for calendar

				$client = Zend_Gdata_ClientLogin::getHttpClient($user,$pass,$service);
				$gdataCal = new Zend_Gdata_Calendar($client);
						
				$query = $gdataCal->newEventQuery();
				$query->setUser($user);
				
				$query->setVisibility('private');
				$query->setSingleEvents(true);
				$query->setProjection('full');
				$query->setOrderby('starttime');
				$query->setSortOrder('descending');
				$query->setStartMin($sd);
				$query->setStartMax($ed);
				//$query->setMaxResults(300);

				$event_list = $gdataCal->getCalendarEventFeed($query);*/
				
				$minCheck = date(DATE_ATOM, $sd);
				$maxCheck = date(DATE_ATOM, $ed);
				
				/*$optParam = array("timeMin"=>$minCheck , 
                                 "timeMax"=>$maxCheck); */
								 
				
				$timeMin = date(DateTime::ATOM,$sd);
                $timeMax = date(DateTime::ATOM, $ed);	
				

				$optParam = array("timeMin"=>$timeMin , 
                                 "timeMax"=>$timeMax);
				
				
				////get All event ids then check them in event table to delete dublicated
				$event_ids = array();
				$event_list = array();
				
				$modelCalendarServiceAccount = new Model_CalendarServiceAccount($filters['contractor_id']);
				$event_list = $modelCalendarServiceAccount->getAllEvents($optParam);
				if($_SERVER['REMOTE_ADDR'] == '176.106.46.142'){
					//echo 'count1  '.count($event_list);
					//echo 'next....';
			   }
		   
				
				//$conn = new PDO('sqlite:/home/lynn/music.sql3');
				foreach ($event_list as $event)
				{
					$slash_tokns = explode("/",$event->id);
					//echo 'last element  '.$slash_tokns[sizeof($slash_tokns) - 1];
					$event_id = $slash_tokns[sizeof($slash_tokns) - 1];
					$qouted_event_id = "'".$event_id."'";
					//$qouted_event_id = $this->getAdapter()->quote($event_id);
					//$qouted_event_id = $conn->quote($event_id);
					
					//$event_ids[] = $slash_tokns[sizeof($slash_tokns) - 1];	
					$event_ids[] = $qouted_event_id;	
					
					
				}
				$filters['google_event_ids'] = $event_ids;
				//print_r($event_ids);

			
				foreach ($event_list as $event)
				{
					//////formate start date
					
					$event_arr = explode('-' ,$event->getSummary());
					$final_title = (isset($event_arr[3])? $event_arr[3] : '').' - '.(isset($event_arr[0])? $event_arr[0] : '').' - '.(isset($event_arr[2])? $event_arr[2] : '');
					
					
					/*$tok = explode("Ends:",$event->when[0]);
					$start_date = $tok[0];
					$start_date = str_replace("T", " ", $tok[0]);
					$start_date = str_replace("Starts:", "", $start_date);
					$remove_millisecond = explode(".",$start_date);
					$pure_start_date = strtotime($remove_millisecond[0]);
					$pure_start_date = date('d-m-Y H:i', $pure_start_date); 
					//echo $pure_start_date;
					
					//////the same with end date 
					$end_date = $tok[1];
					$end_date = str_replace("T", " ", $tok[1]);
					$remove_millisecond = explode(".",$end_date);
					$pure_end_date = strtotime($remove_millisecond[0]);
					$pure_end_date = date('d-m-Y H:i', $pure_end_date); */
					
					
					$pure_start_date = '';
					$pure_end_date = '';
					
					if(!empty($event['start'])){					
						$start = $event['start'];
						$start_date = $start->getDateTime();
						$start_date = str_replace("T", " ", $start_date);
						
						$start_date = str_replace("Starts:", "", $start_date);
						$remove_millisecond = explode(".",$start_date);
						$pure_start_date = strtotime($remove_millisecond[0]);
						$pure_start_date = date('Y-m-d H:i', $pure_start_date); 
						
					}
					if(!empty($event['end'])){
						//////the same with end date 
						$end = $event['end'];
						$end_date = $end->getDateTime();
						$end_date = str_replace("T", " ", $end_date);
						
						$end_date = str_replace("Starts:", "", $end_date);
						$remove_millisecond = explode(".",$end_date);
						$pure_end_date = strtotime($remove_millisecond[0]);
						$pure_end_date = date('Y-m-d H:i', $pure_end_date); 
						
					}
					
					$ret[] = array(
					'id'=>0,
                    'start'=>$pure_start_date,
                    'end'=>$pure_end_date,
					'title'=>$final_title,
                    'body'=>"",///body
                    'multi'=>0, //more than one day event
					'allDay'=>0,
					'desc'=>$final_title .'',
                    //$row->InstanceType,
                    'extension_id'=>0,
					'backgroundColor'=> '#be9494',
					'borderColor' => '#be9494'
						/*0,
						$event->title->text.' '.$event->content->text,
						$pure_start_date,
						$pure_end_date,
						0,
						0, //more than one day event
						   //$row->InstanceType,
						0, //Recurring event,
						15,
						0, //editable
						$event->content->text,
						//$row['address'],
						''//$attends*
						*/
					);
				}
			} //end of if user and pass
		}

        try {
            //
            //select all booking to calendar
            //
            $filters['calendar_booking_start'] = $sd;
            $filters['calendar_booking_end'] = $ed;

            $Model_BookingMultipleDays = new Model_BookingMultipleDays();
            $bookingMultipleDays = $Model_BookingMultipleDays->getAll($filters, 'booking_start ASC');
			
			

            $bookings = $this->getAll($filters, 'booking_start ASC');
			if($_SERVER['REMOTE_ADDR'] == '176.106.46.142'){
					//echo 'count2  '.count($bookings);
			   }
			if($_SERVER['REMOTE_ADDR'] == '176.106.46.142'){
					//echo 'count3  '.count($bookingMultipleDays);
			   }
            
            $handles = array_merge($bookings, $bookingMultipleDays);

            if (!$this->modelBookingStatus) {
                $this->modelBookingStatus = new Model_BookingStatus();
            }
			
            foreach ($handles as &$handle) {
                $bookingStatus = $this->modelBookingStatus->getById($handle['status_id']);
                $handle['color'] = $bookingStatus['color'];
                $handle['booking_status'] = $bookingStatus['name'];
				
            }
			$modelContractorServiceBooking = new Model_ContractorServiceBooking();
            //echo $sql;
            foreach ($handles as $row) {
				$contractors = $modelContractorServiceBooking->getContractorNamesByBookingId($row['booking_id']);
				$title_row = explode( '-' , $row['title']);
				$cont_name = (isset($title_row[2]) ? $title_row[2] : '');
                //$ret['events'][] = $row;
                //$attends = $row->AttendeeNames;
                //if($row->OtherAttendee){
                //  $attends .= $row->OtherAttendee;
                //}
                //echo $row->startTime;
                // $row['color']=1;
                
                $ret[] = array(
                    'id'=>isset($row['booking_id']) ? $row['booking_id'] : $row['inquiry_id'],
                    'start'=>date('Y-m-d H:i', strtotime($row['booking_start'])),//strtotime($row['booking_start']),
                    'end'=>date('Y-m-d H:i', strtotime($row['booking_end'])),//strtotime($row['booking_end']),
					'title'=>  $cont_name .' - '. $row['booking_status'] . ' - $' . $row['qoute'],
                    'body'=>"",///body
                    'multi'=>0, //more than one day event
					'allDay'=>$row['is_all_day_event'],
					'desc'=> implode(" ",$contractors) .' - '. $row['booking_status'] . ' - $' . $row['qoute'] ,
                    //$row->InstanceType,
                    'extension_id'=>0,
                    'backgroundColor'=> $colors[$row['color']],
					'borderColor' => $colors[$row['color']]
                    //$row->InstanceType,
                    //'color'=>$row['color'],
                  //  'editable'=>1 //editable
                    //'id'=>isset($row['booking_id']) ? 'booking' : 'inquiry',
                    //$row['address'],
                   // 'id'=>''//$attends
                );
            }
			
			
        } catch (Exception $e) {
            $ret['error'] = $e->getMessage();
        }

        return $ret;
		
		/*try {
			
            //D.A 07/08/2015			
            $result=array();
            $localBookingEvents=array();			
			$startdate=date('Y-m-d', $sd);
			$enddate=date('Y-m-d', $ed);
			
			$firstDay = date('d', strtotime($startdate));
			
			if($firstDay != "01"){			
			$curMonth = date('n', strtotime($startdate));
			$curYear  = date('Y', strtotime($startdate));
						
			if ($curMonth == 12)
            $firstDayNextMonth = mktime(0, 0, 0, 0, 0, $curYear+1);
            else
            $firstDayNextMonth = mktime(0, 0, 0, $curMonth+1, 1);
		
			$firstDayMonth=date('Y-m-d', $firstDayNextMonth);
			$lastDayMonth = date('Y-m-t', strtotime($firstDayMonth));
			
			
			   $lastMonthStart=date("Y-m",strtotime($startdate)).'-'.'22';			
			$lastMonthEndDate = date('Y-m-t', strtotime($lastMonthStart));
			$date1 = date_create($lastMonthEndDate);
			date_time_set($date1, 23, 59, 59);
            $lastMonthEnd=date_format($date1, 'Y-m-d H:i:s');
				
			$part1Start=$firstDayMonth;
			$part1EndDate=date('Y-m-d', strtotime($part1Start. ' + 6 days'));
			$date2 = date_create($part1EndDate);
			date_time_set($date2, 23, 59, 59);
            $part1End=date_format($date2, 'Y-m-d H:i:s');
			$part2Start=date('Y-m-d', strtotime($part1End. ' + 1 days'));
			$part2EndDate=date('Y-m-d', strtotime($part2Start. ' + 6 days'));
			$date3 = date_create($part2EndDate);
			date_time_set($date3, 23, 59, 59);
            $part2End=date_format($date3, 'Y-m-d H:i:s');
			$part3Start=date('Y-m-d', strtotime($part2End. ' + 1 days'));
			$part3EndDate=date('Y-m-d', strtotime($part3Start. ' + 6 days'));
			$date4 = date_create($part3EndDate);
			date_time_set($date4, 23, 59, 59);
            $part3End=date_format($date4, 'Y-m-d H:i:s');
			$part4Start=date('Y-m-d', strtotime($part3End. ' + 1 days'));
			$part4EndDate=$lastDayMonth;
			$date5 = date_create($part4EndDate);
			date_time_set($date5, 23, 59, 59);
            $part4End=date_format($date5, 'Y-m-d H:i:s');
			
			$month = date('n', strtotime($part1Start));
			$year  = date('Y', strtotime($part1Start));
						
			if ($month == 12)
            $firstNextMonth = mktime(0, 0, 0, 0, 0, $year+1);
            else
            $firstNextMonth = mktime(0, 0, 0, $month+1, 1);
		
			$firstDayOfNextMonth=date('Y-m-d', $firstNextMonth);
			
            $nextMonthStart= $firstDayOfNextMonth;
			$nextMonthEnd = date("Y-m",strtotime($enddate)).'-'.'08';	
			
						
			$startDays=array($lastMonthStart,$part1Start,$part2Start,$part3Start,$part4Start,$nextMonthStart);
			$endDays=array($lastMonthEnd,$part1End,$part2End,$part3End,$part4End,$nextMonthEnd);						
			$cacheIds=array(date("Y_m",strtotime($lastMonthStart)).'_'.'4',date("Y_m",strtotime($part1Start)).'_'.'1',date("Y_m",strtotime($part1Start)).'_'.'2',date("Y_m",strtotime($part1Start)).'_'.'3',date("Y_m",strtotime($part1Start)).'_'.'4',date("Y_m",strtotime($nextMonthStart)).'_'.'1');
		
					
			}else{
				
			$part1Start=$startdate;
			$part1EndDate=date('Y-m-d', strtotime($part1Start. ' + 6 days'));
			$date1 = date_create($part1EndDate);
			date_time_set($date1, 23, 59, 59);
            $part1End=date_format($date1, 'Y-m-d H:i:s');
			$part2Start=date('Y-m-d', strtotime($part1End. ' + 1 days'));
			$part2EndDate=date('Y-m-d', strtotime($part2Start. ' + 6 days'));
			$date2 = date_create($part2EndDate);
			date_time_set($date2, 23, 59, 59);
            $part2End=date_format($date2, 'Y-m-d H:i:s');
			$part3Start=date('Y-m-d', strtotime($part2End. ' + 1 days'));
			$part3EndDate=date('Y-m-d', strtotime($part3Start. ' + 6 days'));
			$date3 = date_create($part3EndDate);
			date_time_set($date3, 23, 59, 59);
            $part3End=date_format($date3, 'Y-m-d H:i:s');
			$part4Start=date('Y-m-d', strtotime($part3End. ' + 1 days'));
			$lastDayMonth = date('Y-m-t', strtotime($part1Start));
			$part4EndDate=$lastDayMonth;
			$date4 = date_create($part4EndDate);
			date_time_set($date4, 23, 59, 59);
            $part4End=date_format($date4, 'Y-m-d H:i:s');
			
			$nextMonthPart1Start= date('Y-m-d', strtotime($part4End. ' + 1 days'));
			$nextMonthPart1EndDate=date('Y-m-d', strtotime($nextMonthPart1Start. ' + 6 days'));
			$date5 = date_create($nextMonthPart1EndDate);
			date_time_set($date5, 23, 59, 59);
            $nextMonthPart1End=date_format($date5, 'Y-m-d H:i:s');
			$nextMonthPart2Start=date('Y-m-d', strtotime($nextMonthPart1End. ' + 1 days'));
			$nextMonthPart2EndDate = $nextMonthEnd = date("Y-m",strtotime($enddate)).'-'.'14';
			$date6 = date_create($nextMonthPart2EndDate);
			date_time_set($date6, 23, 59, 59);
            $nextMonthPart2End=date_format($date6, 'Y-m-d H:i:s');
			
									
			$startDays=array($part1Start,$part2Start,$part3Start,$part4Start,$nextMonthPart1Start,$nextMonthPart2Start);
			$endDays=array($part1End,$part2End,$part3End,$part4End,$nextMonthPart1End,$nextMonthPart2End);						
			$cacheIds=array(date("Y_m",strtotime($part1Start)).'_'.'1',date("Y_m",strtotime($part1Start)).'_'.'2',date("Y_m",strtotime($part1Start)).'_'.'3',date("Y_m",strtotime($part1Start)).'_'.'4',date("Y_m",strtotime($nextMonthPart1Start)).'_'.'1',date("Y_m",strtotime($nextMonthPart1Start)).'_'.'2');
		
			}
			
					
			$days_between = ceil(abs(strtotime($enddate) - strtotime($startdate)) / 86400);
			if($days_between > 7){	
            require_once 'Zend/Cache.php';			
			$localEventsDir=get_config('cache').'/'.'localEvents';          		
			$frontEndOption= array('lifetime'=> 24 * 3600,
			'automatic_serialization'=> true);
			$backendOptions = array('cache_dir'=>$localEventsDir );
			$cache = Zend_Cache::factory('Core','File',$frontEndOption,$backendOptions);
			
            for($i=0;$i<count($cacheIds);$i++){    
			if ( ($result = $cache->load($cacheIds[$i])) === false ) {
						
            //cache miss              
            $localret = array();		     
			$filters['calendar_booking_start'] = strtotime($startDays[$i]);
            $filters['calendar_booking_end'] = strtotime($endDays[$i]);	
			
            $Model_BookingMultipleDays = new Model_BookingMultipleDays();
            $bookingMultipleDays = $Model_BookingMultipleDays->getAll($filters, 'booking_start ASC'); 
            $bookings = $this->getAll($filters, 'booking_start ASC');
            $handles = array_merge($bookings, $bookingMultipleDays);
					
            if (!$this->modelBookingStatus) {
                $this->modelBookingStatus = new Model_BookingStatus();
            }

            foreach ($handles as &$handle) {
                $bookingStatus = $this->modelBookingStatus->getById($handle['status_id']);
                $handle['color'] = $bookingStatus['color'];
                $handle['booking_status'] = $bookingStatus['name'];
				
            }
	
            foreach ($handles as $row) {				
				$title_row = explode( '-' , $row['title']);
				$cont_name = (isset($title_row[2]) ? $title_row[2] : '');
				
                $localret[] = array(
                    'id'=>isset($row['booking_id']) ? $row['booking_id'] : $row['inquiry_id'],
                    'start'=>date('Y-m-d H:i', strtotime($row['booking_start'])),//strtotime($row['booking_start']),
                    'end'=>date('Y-m-d H:i', strtotime($row['booking_end'])),//strtotime($row['booking_end']),
					'title'=>  $cont_name .' - '. $row['booking_status'] . ' - $' . $row['qoute'],
                    'body'=>"",///body
                    'multi'=>0, //more than one day event
					'allDay'=>$row['is_all_day_event'],
					'desc'=> $cont_name .' - '. $row['booking_status'] . ' - $' . $row['qoute'] ,
                    'extension_id'=>0,
                    'backgroundColor'=> $colors[$row['color']],
					'borderColor' => $colors[$row['color']]
                );
            }	
			
		$result=$localret;
		if($result)	{
			$cache->save($result,$cacheIds[$i]);
			$localBookingEvents = array_merge($localBookingEvents, $result);
		}				
			}			
			else{				
                $localBookingEvents = array_merge($localBookingEvents, $result);				
			}		
        }
			
			}else{
			$localBookingEvents = array();	
			$filters['calendar_booking_start'] = $sd;
            $filters['calendar_booking_end'] = $ed;

            $Model_BookingMultipleDays = new Model_BookingMultipleDays();
            $bookingMultipleDays = $Model_BookingMultipleDays->getAll($filters, 'booking_start ASC');
			
            $bookings = $this->getAll($filters, 'booking_start ASC');
			           
            $handles = array_merge($bookings, $bookingMultipleDays);

            if (!$this->modelBookingStatus) {
                $this->modelBookingStatus = new Model_BookingStatus();
            }
			
            foreach ($handles as &$handle) {
                $bookingStatus = $this->modelBookingStatus->getById($handle['status_id']);
                $handle['color'] = $bookingStatus['color'];
                $handle['booking_status'] = $bookingStatus['name'];
				
            }
			$modelContractorServiceBooking = new Model_ContractorServiceBooking();

            foreach ($handles as $row) {
				$contractors = $modelContractorServiceBooking->getContractorNamesByBookingId($row['booking_id']);
				$title_row = explode( '-' , $row['title']);
				$cont_name = (isset($title_row[2]) ? $title_row[2] : '');
                
                $localBookingEvents[] = array(
                    'id'=>isset($row['booking_id']) ? $row['booking_id'] : $row['inquiry_id'],
                    'start'=>date('Y-m-d H:i', strtotime($row['booking_start'])),//strtotime($row['booking_start']),
                    'end'=>date('Y-m-d H:i', strtotime($row['booking_end'])),//strtotime($row['booking_end']),
					'title'=>  $cont_name .' - '. $row['booking_status'] . ' - $' . $row['qoute'],
                    'body'=>"",///body
                    'multi'=>0, //more than one day event
					'allDay'=>$row['is_all_day_event'],
					'desc'=> implode(" ",$contractors) .' - '. $row['booking_status'] . ' - $' . $row['qoute'] ,
                    'extension_id'=>0,
                    'backgroundColor'=> $colors[$row['color']],
					'borderColor' => $colors[$row['color']]
                );
            }	
				
			}
			
			
        } catch (Exception $e) {
            $localret['error'] = $e->getMessage();
        }		

        $bookingEvents = array_merge($ret, $localBookingEvents);
        return $bookingEvents;
		*/
    }
/*
    public function listCalendar($day, $type, $filters = array()) {
        $phpTime = js2PhpTime($day);
        //echo $phpTime . "+" . $type;
        $st = 0;
        $et = 0;
        switch ($type) {
            case "month":
                $st = mktime(0, 0, 0, date("m", $phpTime), 1, date("Y", $phpTime));
                $et = mktime(0, 0, -1, date("m", $phpTime) + 1, 1, date("Y", $phpTime));
                break;
            case "week":
                //suppose first day of a week is monday
                $monday = date("d", $phpTime) - date('N', $phpTime) + 1;
                //echo date('N', $phpTime);
                $st = mktime(0, 0, 0, date("m", $phpTime), $monday, date("Y", $phpTime));
                $et = mktime(0, 0, -1, date("m", $phpTime), $monday + 7, date("Y", $phpTime));
                break;
            case "day":
                $st = mktime(0, 0, 0, date("m", $phpTime), date("d", $phpTime), date("Y", $phpTime));
                $et = mktime(0, 0, -1, date("m", $phpTime), date("d", $phpTime) + 1, date("Y", $phpTime));
                break;
        }
        //echo $st . "--" . $et;
        return $this->listCalendarByRange($st, $et, $filters);
    }*/
	
	 public function listCalendar($st, $et, $filters = array()) {
        /*$phpTime = js2PhpTime($day);
        echo $phpTime . "+" . $type;
		exit;
        $st = 0;
        $et = 0;
        switch ($type) {
            case "month":
                $st = date(DATE_ATOM, mktime(0, 0, 0, date("m"), 1, date("Y")));
                $et = date(DATE_ATOM, mktime(0, 0, 0, date("m"), 30, date("Y")));
                break;
            case "week":
                //suppose first day of a week is monday
                $monday = date("d") - date('N') + 1;
                //echo date('N', $phpTime);
                $st = date(DATE_ATOM, mktime(0, 0, 0, date("m"), 1, date("Y")));
                $et = date(DATE_ATOM, mktime(0, 0, 0, date("m"), 7, date("Y")));
                break;
            case "day":
                $st = date(DATE_ATOM, mktime(0, 0, 0, date("m"), date("d"), date("Y")));
                $et = date(DATE_ATOM, mktime(0, 0, 0, date("m"), date("d") + 1, date("Y")));
                break;
        }*/
        //echo $st . "--" . $et;
		
		
        return $this->listCalendarByRange($st, $et, $filters);
    }

    public function updateCalendar($eventId, $data, $eventType) {
        $ret = array();
        try {
            if ($eventType == 'booking') {
                $this->updateById($eventId, $data);
            } else {
                $inquiryModel = new Model_Inquiry();
                $inquiryModel->updateById($eventId, $data);
            }
            $ret['IsSuccess'] = true;
            $ret['Msg'] = 'Succefully';
        } catch (Exception $e) {
            $ret['IsSuccess'] = false;
            $ret['Msg'] = $e->getMessage();
        }
        return $ret;
    }

    public function updateDetailedCalendar($bookingId, $data = array()) {
        $ret = array();
        try {
            $updated = $this->updateById($bookingId, $data);
			/**
			 * send notification
			 */
			//MobileNotification::notify($bookingId,'updated booking');

            $ret['IsSuccess'] = true;
            $ret['Msg'] = 'Succefully';
            $ret['Data'] = $bookingId;
			$ret['log'] = $updated['log_id'];
        } catch (Exception $e) {
            $ret['IsSuccess'] = false;
            $ret['Msg'] = $e->getMessage();
        }
        return $ret;
    }

    public function removeCalendar($bookingId) {
        $ret = array();
        try {
            $data = array('is_deleted' => 1);
            $this->updateById($bookingId, $data);
            $ret['IsSuccess'] = true;
            $ret['Msg'] = 'Succefully';
        } catch (Exception $e) {
            $ret['IsSuccess'] = false;
            $ret['Msg'] = $e->getMessage();
        }
        return $ret;
    }

    public function total($filters = array(), $type = 'qoute') {
  
        
        $select = $this->getAdapter()->select();
        $select->from(array('bok' => $this->_name));
        $select->distinct();

        $wheresAndJoins = $this->getWheresAndJoinsByFilters($filters);

        $joinInner = $wheresAndJoins['joinInner'];
        $wheres = $wheresAndJoins['wheres'];

        if ($wheres) {
            foreach ($wheres as $where) {
                $select->where($where);
            }
        }

        if ($joinInner) {
            foreach ($joinInner as $inner) {
                $select->joinInner($inner['name'], $inner['cond'], $inner['cols']);
            }
        }

        if (!in_array($type, array('qoute', 'paid_amount', 'refund'))) {
            $type = 'qoute';
        }

        $selectTotal = $this->getAdapter()->select();
        $selectTotal->from($select, "SUM({$type})");
		
		      
        return $this->getAdapter()->fetchOne($selectTotal);
    }

    public function checkIfCanSeeLocation($bookingId, $userId = 0) {

        if (!$userId) {
            $userId = $this->loggedUser['user_id'];
        }

        if (CheckAuth::checkCredential(array('canSeeAllBookingLocation'))) {
            return true;
        }

        $booking = $this->getById($bookingId);
        if ($booking['created_by'] == $userId) {
            return true;
        } else {
            if ($this->checkBookingIfAccepted($bookingId)) {
                return true;
            }
        }
        return false;
    }

    public function checkBookingIfAccepted($bookingId, $userId = 0, $checkPeriod = true) {

        if (!$userId) {
            $userId = $this->loggedUser['user_id'];
        }

        if (CheckAuth::checkCredential(array('canSeeAllBooking'))) {
            return true;
        }

        $checkTimePeriod = true;
        if ($checkPeriod) {
            $checkTimePeriod = $this->checkBookingTimePeriod($bookingId, $userId);
        }

        if ($checkTimePeriod) {
            $booking = $this->getById($bookingId);
            if ($booking['created_by'] == $userId) {
                return true;
            } else {
                if (!$this->modelContractorServiceBooking) {
                    $this->modelContractorServiceBooking = new Model_ContractorServiceBooking();
                }
                $services = $this->modelContractorServiceBooking->getAll(array('booking_id' => $bookingId, 'contractor_id' => $userId));

                foreach ($services AS $service) {
                    if ($service['is_accepted']) {
                        return true;
                    }
                }
                return false;
            }
        } else {
            return false;
        }
    }

	///////////By Islam
	public function checkBookingIfRejected($bookingId, $userId = 0, $checkPeriod = true) {

        if (!$userId) {
            $userId = $this->loggedUser['user_id'];
        }
				if (!$this->modelContractorServiceBooking) {
                    $this->modelContractorServiceBooking = new Model_ContractorServiceBooking();
                }
                $services = $this->modelContractorServiceBooking->getAll(array('booking_id' => $bookingId, 'contractor_id' => $userId));

                foreach ($services AS $service) {
                    if ($service['is_rejected']) {
                        return true;
                    }
                }
                return false;
				
    }
    public function checkIfGeneralContractor($bookingId, $userId) {

        $booking = $this->getById($bookingId);

        $modelCompanies = new Model_Companies();
        $company = $modelCompanies->getById($booking['company_id']);
        $companyName = trim($company['company_name']);

        $modelUser = new Model_User();
        $generalContractor = $modelUser->getByUserCode(sha1("General {$companyName}"));

        if ($generalContractor['user_id'] == $userId) {
            return true;
        }
        return false;
    }

    public function checkIfContractorAcceptBooking($bookingId, $userId, $allowGeneralContractor = false) {

        if ($allowGeneralContractor) {
            if ($this->checkIfGeneralContractor($bookingId, $userId)) {
                return true;
            }
        }

        $checkTimePeriod = $this->checkContractorTimePeriod($bookingId, $userId);
        if ($checkTimePeriod) {
            $booking = $this->getById($bookingId);
            if ($booking['created_by'] == $userId) {
                return true;
            } else {
                if (!$this->modelContractorServiceBooking) {
                    $this->modelContractorServiceBooking = new Model_ContractorServiceBooking();
                }
                $services = $this->modelContractorServiceBooking->getAll(array('booking_id' => $bookingId, 'contractor_id' => $userId));

                foreach ($services AS $service) {
                    if ($service['is_accepted']) {
                        return true;
                    }
                }
                return false;
            }
        } else {
            return false;
        }
    }

    public function checkContractorTimePeriod($bookingId, $userId) {

        $booking = $this->getById($bookingId);

        if ($booking['created_by'] != $userId) {
            $startTime = strtotime($booking['booking_start']);
            $before = time() + get_config('before_period_time');
            if ($before < $startTime) {
                return false;
            }

            $endTime = strtotime($booking['booking_end']) + get_config('after_period_time');
            $after = time();
            if ($after > $endTime) {
                return false;
            }
        }
        return true;
    }

    public function checkIfCanSeeBooking($bookingId, $userId = 0) {

        if (!$userId) {
            $userId = $this->loggedUser['user_id'];
        }

        if (CheckAuth::checkCredential(array('canSeeAllBooking'))) {
            return true;
        }

        $checkTimePeriod = $this->checkBookingTimePeriod($bookingId, $userId);
        if ($checkTimePeriod) {
            $booking = $this->getById($bookingId);
            if ($booking['created_by'] == $userId) {
                return true;
            } else {
                if (!$this->modelContractorServiceBooking) {
                    $this->modelContractorServiceBooking = new Model_ContractorServiceBooking();
                }
                $services = $this->modelContractorServiceBooking->getAll(array('booking_id' => $bookingId, 'contractor_id' => $userId));

                if (!empty($services)) {
                    return true;
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }
    }

    public function checkIfCanSeeBookingWithOutTimePeriod($bookingId, $userId = 0) {

        if (!$userId) {
            $userId = $this->loggedUser['user_id'];
        }

        if (CheckAuth::checkCredential(array('canSeeAllBooking'))) {
            return true;
        }

        $booking = $this->getById($bookingId);
        if ($booking['created_by'] == $userId) {
            return true;
        } else {
            if (!$this->modelContractorServiceBooking) {
                $this->modelContractorServiceBooking = new Model_ContractorServiceBooking();
            }
            $services = $this->modelContractorServiceBooking->getAll(array('booking_id' => $bookingId, 'contractor_id' => $userId));

            if (!empty($services)) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function checkIfCanDeleteBooking($bookingId, $userId = 0) {

        if (!$userId) {
            $userId = $this->loggedUser['user_id'];
        }

        if (CheckAuth::checkCredential(array('canDeleteAllBooking'))) {
            return true;
        }

        $booking = $this->getById($bookingId);
        if ($booking['created_by'] == $userId) {
            return true;
        } else {
            return false;
        }
    }

    public function checkIfCanEditBooking($bookingId, $userId = 0) {

        if (!$userId) {
            $userId = $this->loggedUser['user_id'];
        }

        if (CheckAuth::checkCredential(array('canEditAllBooking'))) {
            return true;
        }

        $checkAccepted = $this->checkBookingIfAccepted($bookingId, $userId);
        if ($checkAccepted) {
            $booking = $this->getBookingByOwnerOrAssignedUser($bookingId, $userId);
            if ($booking) {
                return true;
            }
            return false;
        } else {
            return false;
        }
    }

    public function checkBookingTimePeriod($bookingId, $userId = 0) {

        if (!$userId) {
            $userId = $this->loggedUser['user_id'];
        }

        if (CheckAuth::checkCredentialOr(array('canSeeAllBooking', 'canEditAllBooking'))) {
            return true;
        }

        $booking = $this->getById($bookingId);

        $modelBookingStatus = new Model_BookingStatus();
        $awaitingUpdateStatus = $modelBookingStatus->getByStatusName('AWAITING UPDATE');
        $inProcess = $modelBookingStatus->getByStatusName('IN PROGRESS');
		$completed = $modelBookingStatus->getByStatusName('COMPLETED');
		$failed = $modelBookingStatus->getByStatusName('FAILED');
        if ($awaitingUpdateStatus['booking_status_id'] == $booking['status_id']) {
            return true;
        }
        if ($inProcess['booking_status_id'] == $booking['status_id']) {
            return true;
        }
		///////////By Islam  
		if ($completed['booking_status_id'] == $booking['status_id']) {
            return true;
        }
		if ($failed['booking_status_id'] == $booking['status_id']) {
            return true;
        }
		//////////

        if ($booking['created_by'] != $userId) {
            if (CheckAuth::checkCredential(array('canEditOrViewBookingOnlyInTimePeriod'))) {
                $startTime = strtotime($booking['booking_start']);
                $before = time() + get_config('before_period_time');
                if ($before < $startTime) {
                    return false;
                }

                $endTime = strtotime($booking['booking_end']) + get_config('after_period_time');
                $after = time();
                if ($after > $endTime) {
                    return false;
                }
            }
        }
        return true;
    }

    public function checkCanEditBookingDetails($bookingId, $userId = 0) {

        if (!$userId) {
            $userId = $this->loggedUser['user_id'];
        }

        if (CheckAuth::checkCredential(array('canEditBookingDetails'))) {
            return true;
        }

        $booking = $this->getById($bookingId);
        if ($booking['created_by'] == $userId) {
            return true;
        }

        return false;
    }

    public function getBookingByOwnerOrAssignedUser($bookingId, $userId = 0) {

        if (!$userId) {
            $userId = $this->loggedUser['user_id'];
        }

        $select = $this->getAdapter()->select();
        $select->from(array('bok' => $this->_name));
        $select->where("bok.booking_id = {$bookingId}");
        $select->where('bok.is_deleted = 0');

        if (!CheckAuth::checkCredential(array('canSeeAllBooking'))) {
            if (CheckAuth::checkCredential(array('canSeeOnlyHisBooking'))) {
                if (CheckAuth::checkCredential(array('canSeeAssignedBooking'))) {
                    $select->joinInner(array('csb' => 'contractor_service_booking'), 'bok.booking_id = csb.booking_id', '');
                    $select->where("bok.created_by = {$userId} OR csb.contractor_id = {$userId}");
                } else {
                    $select->where("bok.created_by = {$userId}");
                }
            } else {
                $select->where("bok.created_by = {$userId}");
            }
        }

        return $this->getAdapter()->fetchRow($select);
    }

    public function getBookingCount($filters = array()) {

        $select = $this->getAdapter()->select();
        $select->from(array('bok' => $this->_name));
        $select->distinct();

        $wheresAndJoins = $this->getWheresAndJoinsByFilters($filters);

        $joinInner = $wheresAndJoins['joinInner'];
        $wheres = $wheresAndJoins['wheres'];

        if ($wheres) {
            foreach ($wheres as $wh=>$where) {
                $select->where($where);
            }
        }

        if ($joinInner) {
            foreach ($joinInner as $inner) {
                $select->joinInner($inner['name'], $inner['cond'], $inner['cols']);
            }
        }

        $sql = $this->getAdapter()->select();
        $sql->from($select, 'COUNT(*)');
        return $this->getAdapter()->fetchOne($sql);
    }

    public function getCronJobCountAndTotalBooking($filters = array()) {

        $select = $this->getAdapter()->select();
        $select->from(array('bok' => $this->_name));
        $select->distinct();

        $wheresAndJoins = $this->getWheresAndJoinsByFilters($filters, true);

        $joinInner = $wheresAndJoins['joinInner'];
        $wheres = $wheresAndJoins['wheres'];

        if ($wheres) {
            foreach ($wheres as $where) {
                $select->where($where);
            }
        }

        if ($joinInner) {
            foreach ($joinInner as $inner) {
                $select->joinInner($inner['name'], $inner['cond'], $inner['cols']);
            }
        }

        $result = array();

        $countSql = $this->getAdapter()->select();
        $countSql->from($select, 'COUNT(*)');
        $result['count'] = $this->getAdapter()->fetchOne($countSql);

        $totalSql = $this->getAdapter()->select();
        $totalSql->from($select, 'SUM(qoute)');
        $result['total'] = $this->getAdapter()->fetchOne($totalSql);

        return $result;
    }

    /**
     * get extra info for all the rows
     * @param array $row
     * @param array $types
     * @return array
     */
    public function fills(&$rows, $types = array()) {
        foreach ($rows as &$row) {
            $this->fill($row, $types);
        }
    }

    /**
     * get extra info for each row (Ex: getting the category row by c_id, author row by author id ... etc)
     * 
     * @param array $row
     * @param array $types
     * @return array
     */
    public function fill(&$row, $types = array()) {

        if (in_array('booking', $types)) {
            $row['booking'] = $this->getById($row['booking_id']);
        }

        if (in_array('contractors', $types)) {
            /**
             * load model
             */
            if (!$this->modelContractorInfo) {
                $this->modelContractorInfo = new Model_ContractorInfo();
            }
            if (!$this->modelUser) {
                $this->modelUser = new Model_User();
            }
            if (!$this->modelContractorServiceBooking) {
                $this->modelContractorServiceBooking = new Model_ContractorServiceBooking();
            }

            $allContractorServiceBooking = $this->modelContractorServiceBooking->getByBookingId($row['booking_id']);

            $allContractors = array();
            foreach ($allContractorServiceBooking as $contractorServiceBooking) {
                $contractorInfo = $this->modelContractorInfo->getByContractorId($contractorServiceBooking['contractor_id']);
                $userInfo = $this->modelUser->getById($contractorServiceBooking['contractor_id']);
                if ('contractor' == CheckAuth::getRoleName()) {
                    if ($contractorServiceBooking['contractor_id'] == $this->loggedUser['user_id']) {
                        $allContractors[$contractorServiceBooking['contractor_id']] = ucwords($userInfo['username']) . " - " . ucwords($contractorInfo['business_name'] ? $contractorInfo['business_name'] : '');
                    }
                } else {
                    $allContractors[$contractorServiceBooking['contractor_id']] = ucwords($userInfo['username']) . " - " . ucwords($contractorInfo['business_name']);
                }
            }
            $row['contractors'] = $allContractors;
        }

        if (in_array('name_contractors', $types)) {
            /**
             * load model
             */
            if (!$this->modelUser) {
                $this->modelUser = new Model_User();
            }
            if (!$this->modelContractorServiceBooking) {
                $this->modelContractorServiceBooking = new Model_ContractorServiceBooking();
            }

            $allContractorServiceBooking = $this->modelContractorServiceBooking->getByBookingId($row['booking_id']);

            $contractors = array();
            if ($allContractorServiceBooking) {
                foreach ($allContractorServiceBooking as $bookingService) {
                    $user = $this->modelUser->getById($bookingService['contractor_id']);

                    $contractorName = isset($user['display_name']) ? $user['display_name'] : '';
                    $contractors[$bookingService['contractor_id']] = ucwords($contractorName);
                }
            }
            $row['name_contractors'] = $contractors;
        }

        if (in_array('email_contractors', $types)) {
            /**
             * load model
             */
            if (!$this->modelUser) {
                $this->modelUser = new Model_User();
            }
            if (!$this->modelContractorServiceBooking) {
                $this->modelContractorServiceBooking = new Model_ContractorServiceBooking();
            }

            $allContractorServiceBooking = $this->modelContractorServiceBooking->getByBookingId($row['booking_id']);

            $contractors = array();
            if ($allContractorServiceBooking) {
                foreach ($allContractorServiceBooking as $bookingService) {
                    $user = $this->modelUser->getById($bookingService['contractor_id']);
                    $contractors[$bookingService['contractor_id']] = $user['email1'];
                }
            }
            $row['email_contractors'] = $contractors;
        }

        if (in_array('contact_history', $types)) {
            /**
             * load model
             */
            if (!$this->modelBookingContactHistory) {
                $this->modelBookingContactHistory = new Model_BookingContactHistory();
            }

            $row['contact_history'] = $this->modelBookingContactHistory->getContactHistory($row['booking_id']);
        }

        if (in_array('customer', $types)) {
            /**
             * load model
             */
            if (!$this->modelCustomer) {
                $this->modelCustomer = new Model_Customer();
            }

            $row['customer'] = $this->modelCustomer->getById($row['customer_id']);
            $row['customer_name'] = get_customer_name($row['customer']);
        }
        if (in_array('full_customer_info', $types)) {
            /**
             * load model
             */
            if (!$this->modelCustomer) {
                $this->modelCustomer = new Model_Customer();
            }

            $customer = $this->modelCustomer->getById($row['customer_id']);
            $this->modelCustomer->fill($customer, array('customer_commercial_info', 'customer_contacts'));

            $row['customer'] = $customer;
            $row['customer_name'] = get_customer_name($customer);
        }
        if (in_array('customer_contacts', $types)) {
            /**
             * load model
             */
            if (!$this->modelCustomer) {
                $this->modelCustomer = new Model_Customer();
            }
            if (!$this->modelCustomerContact) {
                $this->modelCustomerContact = new Model_CustomerContact();
            }
            if (!$this->modelCustomerContactLabel) {
                $this->modelCustomerContactLabel = new Model_CustomerContactLabel();
            }

            $row['customer'] = $this->modelCustomer->getById($row['customer_id']);
            $customerContacts = '';
            if ($row['customer']) {
                $customerContacts = $this->modelCustomerContact->getByCustomerId($row['customer']['customer_id']);
                foreach ($customerContacts as &$customerContact) {
                    $customerContactLabel = $this->modelCustomerContactLabel->getById($customerContact['customer_contact_label_id']);
                    $customerContact['contact_label'] = $customerContactLabel['contact_label'];
                }
            }

            $row['customer_contacts'] = $customerContacts;
        }
        if (in_array('customer_commercial_info', $types)) {
            /**
             * load model
             */
            if (!$this->modelCustomerCommercialInfo) {
                $this->modelCustomerCommercialInfo = new Model_CustomerCommercialInfo();
            }
            $row['customer_commercial_info'] = $this->modelCustomerCommercialInfo->getByCustomerId($row['customer_id']);
        }

        if (in_array('city', $types)) {
            /**
             * load model
             */
            if (!$this->modelCities) {
                $this->modelCities = new Model_Cities();
            }
            if (!$this->modelCountries) {
                $this->modelCountries = new Model_Countries();
            }

            $row['city'] = $this->modelCities->getById($row['city_id']);
            $row['city_name'] = $row['city']['city_name'];

            $countryName = $this->modelCountries->getById($row['city']['country_id']);
            $row['country_name'] = $countryName['country_name'];
        }

        if (in_array('labels', $types)) {
            /**
             * load model
             */
            if (!$this->modelLabel) {
                $this->modelLabel = new Model_Label();
            }
            if (!$this->modelBookingLabel) {
                $this->modelBookingLabel = new Model_BookingLabel();
            }

            $LabelIds = $this->modelBookingLabel->getByBookingId($row['booking_id']);
            $labels = array();

            foreach ($LabelIds as $LabelId) {
                $label = $this->modelLabel->getById($LabelId['label_id']);
                $labels[$label['id']] = $label['label_name'];
            }
            $row['labels'] = $labels;
        }
        if (in_array('services_with_floor_type', $types)) {
            /**
             * load model
             */
            if (!$this->modelContractorServiceBooking) {
                $this->modelContractorServiceBooking = new Model_ContractorServiceBooking();
            }
            if (!$this->modelServices) {
                $this->modelServices = new Model_Services();
            }
            if (!$this->modelServiceAttribute) {
                $this->modelServiceAttribute = new Model_ServiceAttribute();
            }
            if (!$this->modelServiceAttributeValue) {
                $this->modelServiceAttributeValue = new Model_ServiceAttributeValue();
            }
            if (!$this->modelAttributeListValue) {
                $this->modelAttributeListValue = new Model_AttributeListValue();
            }
            if (!$this->modelAttributes) {
                $this->modelAttributes = new Model_Attributes();
            }

            $filters = array();
            $filters['booking_id'] = $row['booking_id'];
            $services = $this->modelContractorServiceBooking->getAll($filters);

            $attribute = $this->modelAttributes->getByVariableName('Floor');

            foreach ($services as &$service) {
                $service['info'] = $this->modelServices->getById($service['service_id']);
                $ServiceAttribute = $this->modelServiceAttribute->getByAttributeIdAndServiceId($attribute['attribute_id'], $service['service_id']);
                $ServiceAttributeValue = $this->modelServiceAttributeValue->getByBookingIdAndServiceAttributeIdAndClone($row['booking_id'], $ServiceAttribute['service_attribute_id'], $service['clone']);
                $AttributeListValue = $this->modelAttributeListValue->getById($ServiceAttributeValue['value']);
                $service['floor'] = $AttributeListValue;
            }

            $row['services_with_floor_type'] = $services;
        }

        if (in_array('status', $types)) {
            /**
             * load model
             */
            if (!$this->modelBookingStatus) {
                $this->modelBookingStatus = new Model_BookingStatus();
            }

            $row['status'] = $this->modelBookingStatus->getById($row['status_id']);
        }

        if (in_array('created_by', $types)) {
            /**
             * load model
             */
            if (!$this->modelUser) {
                $this->modelUser = new Model_User();
            }

            $row['created_by'] = $this->modelUser->getById($row['created_by']);
        }

        if (in_array('address', $types)) {
            /**
             * load model
             */
            if (!$this->modelBookingAddress) {
                $this->modelBookingAddress = new Model_BookingAddress();
            }

            $bookingAddress = $this->modelBookingAddress->getByBookingId($row['booking_id']);
            $bookingAddress['line_address'] = get_line_address($bookingAddress);

            $row['address'] = $bookingAddress;
        }
        if (in_array('discussion', $types)) {
            /**
             * load model
             */
            if (!$this->modelBookingDiscussion) {
                $this->modelBookingDiscussion = new Model_BookingDiscussion();
            }

            $bookingDiscussion = $this->modelBookingDiscussion->getByBookingId($row['booking_id']);
            $row['discussion'] = $bookingDiscussion;
        }
        if (in_array('status_discussion', $types)) {
            /**
             * load model
             */
            if (!$this->modelBookingStatusDiscussion) {
                $this->modelBookingStatusDiscussion = new Model_BookingStatusDiscussion();
            }
            $status_discussion = $this->modelBookingStatusDiscussion->getLastDiscussionByBookingIdAndStatusId($row['booking_id'], $row['status_id']);

            $row['status_discussion'] = $status_discussion;
        }
        if (in_array('complaint', $types)) {
            /**
             * load model
             */
            if (!$this->modelComplaint) {
                $this->modelComplaint = new Model_Complaint();
            }
            $bookingComplaints = $this->modelComplaint->getByBookingId($row['booking_id']);
            $row['complaint'] = $bookingComplaints;
        }
        if (in_array('email_log', $types)) {
            /**
             * load model
             */
            if (!$this->modelEmailLog) {
                $this->modelEmailLog = new Model_EmailLog();
            }
            $filter_email = array();
            $filter_email['reference_id'] = $row['booking_id'];
            $filter_email['type'] = 'booking';
            $emailLog = $this->modelEmailLog->getAll($filter_email);
            $row['email_log'] = $emailLog;
        }
        if (in_array('invoice', $types)) {
            /**
             * load model
             */
            if (!$this->modelBookingInvoice) {
                $this->modelBookingInvoice = new Model_BookingInvoice();
            }

            $row['invoice'] = $this->modelBookingInvoice->getByBookingId($row['booking_id']);
        }
        if (in_array('estimate', $types)) {
            /**
             * load model
             */
            if (!$this->modelBookingEstimate) {
                $this->modelBookingEstimate = new Model_BookingEstimate();
            }

            $row['estimate'] = $this->modelBookingEstimate->getNotDeletedByBookingId($row['booking_id']);
        }

        if (in_array('is_accepted', $types)) {

            $row['is_accepted'] = $this->checkBookingIfAccepted($row['booking_id']);
        }
        if (in_array('have_attachment', $types)) {
            if (!$this->modelBookingAttachment) {
                $this->modelBookingAttachment = new Model_BookingAttachment();
            }

            $have_attachment = $this->modelBookingAttachment->getByBookingIdOrInquiryId($row['booking_id'], $row['original_inquiry_id']);
            $row['have_attachment'] = !empty($have_attachment) ? 1 : 0;
        }

        if (in_array('can_delete', $types)) {
            $row['can_delete'] = $this->checkIfCanDeleteBooking($row['booking_id']);
        }
        if (in_array('reminder', $types)) {
            /**
             * load model
             */
            if (!$this->modelBookingReminder) {
                $this->modelBookingReminder = new Model_BookingReminder();
            }

            $row['reminder'] = $this->modelBookingReminder->getContactHistory($row['booking_id']);
        }
        if (in_array('services', $types)) {
            /**
             * load model
             */
            if (!$this->modelContractorServiceBooking) {
                $this->modelContractorServiceBooking = new Model_ContractorServiceBooking();
            }
            if (!$this->modelServices) {
                $this->modelServices = new Model_Services();
            }
            $filters = array();
            $filters['booking_id'] = $row['booking_id'];
            $services = $this->modelContractorServiceBooking->getAll($filters);

            foreach ($services as &$service) {
                $service['info'] = $this->modelServices->getById($service['service_id']);
            }

            $row['services'] = $services;
        }
        if (in_array('services_temp', $types)) {
            /**
             * load model
             */
            if (!$this->modelContractorServiceBookingTemp) {
                $this->modelContractorServiceBookingTemp = new Model_ContractorServiceBookingTemp();
            }
            if (!$this->modelServices) {
                $this->modelServices = new Model_Services();
            }
            $filters = array();
            $filters['booking_id'] = $row['booking_id'];
            $services = $this->modelContractorServiceBookingTemp->getAll($filters);

            foreach ($services as &$service) {
                $service['info'] = $this->modelServices->getById($service['service_id']);
            }

            $row['services_temp'] = $services;
        }

        if (in_array('not_accepted_or_rejected', $types)) {
            /**
             * load model
             */
            if (!$this->modelContractorServiceBooking) {
                $this->modelContractorServiceBooking = new Model_ContractorServiceBooking();
            }

            $allServices = $this->modelContractorServiceBooking->getContractorServicesByBookingId($row['booking_id']);

            $notAcceptedOrRejected = 0;
            if ($allServices) {
                foreach ($allServices as $serviceCB) {
                    if ((isset($serviceCB['is_accepted']) && $serviceCB['is_accepted'] == 0) && (isset($serviceCB['is_rejected']) && $serviceCB['is_rejected'] == 0)) {
                        $notAcceptedOrRejected = 1;
                    }
                }
            }
            $row['not_accepted_or_rejected'] = $notAcceptedOrRejected;
        }

        if (in_array('not_accepted', $types)) {
            /**
             * load model
             */
            if (!$this->modelContractorServiceBooking) {
                $this->modelContractorServiceBooking = new Model_ContractorServiceBooking();
            }
            $allServices = $this->modelContractorServiceBooking->getContractorServicesByBookingId($row['booking_id']);

            $notAccepted = 0;
            if ($allServices) {
                foreach ($allServices as $serviceCB) {
                    if ((isset($serviceCB['is_accepted']) && $serviceCB['is_accepted'] == 0)) {
                        $notAccepted = 1;
                    }
                }
            }
            $row['not_accepted'] = $notAccepted;
        }

        if (in_array('property_type', $types)) {
            /**
             * load model
             */
            if (!$this->modelPropertyType) {
                $this->modelPropertyType = new Model_PropertyType();
            }

            $propertyType = $this->modelPropertyType->getById($row['property_type_id']);

            $row['property_type'] = $propertyType['property_type'];
        }
        if (in_array('booking_users', $types)) {
            /**
             * load model
             */
            if (!$this->modelBookingLog) {
                $this->modelBookingLog = new Model_BookingLog();
            }
            if (!$this->modelUser) {
                $this->modelUser = new Model_User();
            }

            $bookingUsers = $this->modelBookingLog->getUsersByBookingId($row['booking_id']);
            $this->modelUser->fills($bookingUsers, array('user'));

            $row['booking_users'] = $bookingUsers;
        }
        if (in_array('number_of_due_days', $types)) {
            if (!$this->modelBookingInvoice) {
                $this->modelBookingInvoice = new Model_BookingInvoice();
            }
            $invoice = $this->modelBookingInvoice->getByBookingId($row['booking_id']);
            $cdate = $invoice['created'];
            $today = time();
            $difference = $today - $cdate;
            if ($difference < 0) {
                $difference = 0;
            }
            $ago = (int) floor($difference / 60 / 60 / 24);
            $due = $this->modelBookingInvoice->getDueDate($row['booking_id']);

            if ($ago > $due) {
                $row['due'] = $ago - $due;
            } else {
                $row['due'] = $due - $ago;
            }
        }
        if (in_array('approved_ammount', $types)) {
            /**
             * load model
             */
            if (!$this->modelPayment) {
                $this->modelPayment = new Model_Payment();
            }

            $row['approved_ammount'] = $this->modelPayment->getTotalAmount(array('booking_id' => $row['booking_id'], 'is_approved' => 'yes'));
        }
        if (in_array('unapproved_ammount', $types)) {
            /**
             * load model
             */
            if (!$this->modelPayment) {
                $this->modelPayment = new Model_Payment();
            }

            $row['unapproved_ammount'] = $this->modelPayment->getTotalAmount(array('booking_id' => $row['booking_id'], 'is_approved' => 'no'));
        }
        if (in_array('approved_payment', $types)) {
            /**
             * load model
             */
            if (!$this->modelPayment) {
                $this->modelPayment = new Model_Payment();
            }

            $row['approved_payment'] = $this->modelPayment->getTotalPayment(array('booking_id' => $row['booking_id'], 'is_approved' => 'yes'));
        }
        if (in_array('unapproved_payment', $types)) {
            /**
             * load model
             */
            if (!$this->modelPayment) {
                $this->modelPayment = new Model_Payment();
            }

            $row['unapproved_payment'] = $this->modelPayment->getTotalPayment(array('booking_id' => $row['booking_id'], 'is_approved' => 'no'));
        }
        if (in_array('approved_refund', $types)) {
            /**
             * load model
             */
            if (!$this->modelRefund) {
                $this->modelRefund = new Model_Refund();
            }

            $row['approved_refund'] = $this->modelRefund->getTotalRefund(array('booking_id' => $row['booking_id'], 'is_approved' => 'yes'));
        }
        if (in_array('unapproved_refund', $types)) {
            /**
             * load model
             */
            if (!$this->modelRefund) {
                $this->modelRefund = new Model_Refund();
            }

            $row['unapproved_refund'] = $this->modelRefund->getTotalRefund(array('booking_id' => $row['booking_id'], 'is_approved' => 'no'));
        }

        if (in_array('total_without_cash', $types)) {
            /**
             * load model
             */
            if (!$this->modelPayment) {
                $this->modelPayment = new Model_Payment();
            }

            $row['total_without_cash'] = $this->modelPayment->getTotalAmount(array('booking_id' => $row['booking_id'], 'without_cash' => true));
        }
        if (in_array('cash_payment', $types)) {
            /**
             * load model
             */
            if (!$this->modelPayment) {
                $this->modelPayment = new Model_Payment();
            }
            if (!$this->modelPaymentType) {
                $this->modelPaymentType = new Model_PaymentType();
            }

            $row['cash_payment'] = $this->modelPayment->getTotalAmount(array('booking_id' => $row['booking_id'], 'payment_type' => $this->modelPaymentType->getPaymentIdBySlug('cash')));
        }

        
		if (in_array('multiple_days', $types)) {
            /**
             * load model
             */
            if (!$this->modelBookingMultipleDays) {
                $this->modelBookingStatusDiscussion = new Model_BookingMultipleDays();
            }
            $multiple_days = $this->modelBookingStatusDiscussion->getByBookingId($row['booking_id']);

            $row['multiple_days'] = $multiple_days;
        }
		
		return $row;
    }

    public function cronJobChangeBookingToAwatingUpdate() {

        //Load Model
        $modelBookingStatus = new Model_BookingStatus();

        $toDoStatus = $modelBookingStatus->getByStatusName('TO DO');
        $toDoStatusId = $this->getAdapter()->quote(trim($toDoStatus['booking_status_id']));

        $toVisitStatus = $modelBookingStatus->getByStatusName('TO VISIT');
        $toVisitStatusId = $this->getAdapter()->quote(trim($toVisitStatus['booking_status_id']));

        $currnetDate = date('Y-m-d H:i:s', time());
        $currnetDate = $this->getAdapter()->quote(trim($currnetDate));

        $select = $this->getAdapter()->select();
        $select->from(array('bok' => 'booking'));
        $select->where("bok.status_id = {$toDoStatusId} OR bok.status_id = {$toVisitStatusId}");
        $select->where("bok.booking_end < {$currnetDate}");
        $select->where("bok.is_change = 0 ");
        $results = $this->getAdapter()->fetchAll($select);

        $awaitingUpdateStatus = $modelBookingStatus->getByStatusName('AWAITING UPDATE');
        $awaitingUpdateStatusId = $awaitingUpdateStatus['booking_status_id'];

        foreach ($results as $result) {

            $db_params = array(
                'status_id' => $awaitingUpdateStatusId
            );

            $this->updateById($result['booking_id'], $db_params);
        }
    }
	public function getcountBookingsByStatus($statusName){
	
	    $modelBookingStatus = new Model_BookingStatus();
        $Status = $modelBookingStatus->getByStatusName($statusName);
        $BookingStaus = $this->getAll(array('status' => $Status['booking_status_id']));
        return count($BookingStaus);	 
	}
	
	public function getCountNewRequest($contractor_id){
	 

     date_default_timezone_set('Australia/Sydney'); 
     $date = date('Y-m-d H:i');	 
	 $modelBookingStatus = new Model_BookingStatus();
     $toVisit = $modelBookingStatus->getByStatusName('TO VISIT');
     $tentative = $modelBookingStatus->getByStatusName('TENTATIVE');
     $toDo = $modelBookingStatus->getByStatusName('TO DO');
     $inProcess = $modelBookingStatus->getByStatusName('IN PROGRESS');
     
	 $company_id = CheckAuth::getCompanySession();
	 $contractor_id = (int) $contractor_id;
	 $select = $this->getAdapter()->select();
	 $select->from(array('bok' => $this->_name),array('created'));
	 $select->distinct();
	 $select->joinInner(array('csb' => 'contractor_service_booking'),'bok.booking_id = csb.booking_id',array());
	 $select->where("bok.created_by = {$contractor_id} OR csb.contractor_id = {$contractor_id}");   
	 $select->where("bok.booking_start >= '{$date}'");   
	 $select->where('bok.is_deleted = 0');   
	 $select->where('csb.is_rejected = 0');   
	 $select->where('csb.is_accepted = 0');   
	 $select->where("bok.status_id IN ({$toVisit['booking_status_id']},{$tentative['booking_status_id']},{$toDo['booking_status_id']},{$inProcess['booking_status_id']})");
	 $select->where("bok.company_id = {$company_id}");   
	 
	 
	 return count($this->getAdapter()->fetchAll($select));	  
	}
	public function getCountRejectedBookings($contractor_id){
	  
     $company_id = CheckAuth::getCompanySession();
	 $contractor_id = (int) $contractor_id;
	 $select = $this->getAdapter()->select();
	 $select->from(array('bok' => $this->_name),array('created'));
	 $select->distinct();
	 $select->joinInner(array('csb' => 'contractor_service_booking'),'bok.booking_id = csb.booking_id',array());
	 $select->where("bok.created_by = {$contractor_id} OR csb.contractor_id = {$contractor_id}");   
	 $select->where('bok.is_deleted = 0');   
	 $select->where('csb.is_rejected = 1');    
	 $select->where("bok.company_id = {$company_id}");   
	 
	 return count($this->getAdapter()->fetchAll($select));	  
	}

    public function getCountAwaitingupdateBooking() {

        $modelBookingStatus = new Model_BookingStatus();
        $awaitingUpdateStatus = $modelBookingStatus->getByStatusName('AWAITING UPDATE');

        $awaitingupdateBooking = $this->getAll(array('status' => $awaitingUpdateStatus['booking_status_id']));

        return count($awaitingupdateBooking);
    }
	
	
	public function getCountAwaitingAcceptBooking() {

		$modelAuthRole = new Model_AuthRole();
		
		//get logged user 
		$loggedUser = CheckAuth::getLoggedUser();
		$loggedUserId = $loggedUser['user_id'];
		$contractorRoleId = $modelAuthRole->getRoleIdByName('contractor');
		
        $filters = array('acceptance' => 'not_accepted_or_rejected');
		$filters['booking_not_started_yet'] ='yes';
		$filters['convert_status'] ='booking';
		if($contractorRoleId == $loggedUser['role_id']){
			$filters['contractor_id']= $loggedUser['user_id'];
		}
		
       
        $awaitingacceptBooking = $this->getAll($filters);

        return count($awaitingacceptBooking);
    }
	

    public function getCountUnapprovedBooking() {

        $unapprovedBooking = $this->getAll(array('is_change' => 1));

        return count($unapprovedBooking);
    }

    public function getCountInProcessBooking() {
        $modelBookingStatus = new Model_BookingStatus();

        $inProcessStatus = $modelBookingStatus->getByStatusName('IN PROGRESS');
        $inProcessBooking = $this->getAll(array('status' => $inProcessStatus['booking_status_id']));

        return count($inProcessBooking);
    }

	
	public function cronJobReminderOnHoldBooking() {
		$modelCronjobHistory = new Model_CronjobHistory();
		$modelCronJob = new Model_CronJob();
		
		//
		// save this cron in cronjob_history table
		//
		$cronjob = $modelCronJob->getIdByName('Reminder_On_Hold_Booking');
		$cronjonID = $cronjob['id'];
		
		$cronjobHistoryData = array(
		'cron_job_id' => $cronjonID,
		'run_time' => time()
		);
		$cronjobHistoryId = $modelCronjobHistory->insert($cronjobHistoryData);
		//************
        //
        // load models
        //
        $modelUser = new Model_User();
        $modelCustomer = new Model_Customer();
        $modelBookingStatus = new Model_BookingStatus();
        $modelBookingAddress = new Model_BookingAddress();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
		$modelAuthRole = new Model_AuthRole();
		
        $router = Zend_Controller_Front::getInstance()->getRouter();

        $today = time();

        $onHold = $modelBookingStatus->getByStatusName('ON HOLD');

        $select = $this->getAdapter()->select();

        $select->from(array('bok' => $this->_name));
        $select->where("bok.convert_status = 'booking'");
        $select->where("bok.is_deleted = 0");
        $select->where("bok.pause_emails = 0");
        $select->where("bok.status_id = {$onHold['booking_status_id']}");
        $select->where("bok.to_follow < '{$today}'");
        $select->where("bok.is_to_follow = 1");
        $select->where("bok.on_hold_reminded != 'done'");
		$select->where("bok.is_change = 0");//not awaiting approval
		
        $bookings = $this->getAdapter()->fetchAll($select);
		$post_arr = array();
		
        foreach ($bookings as $booking) {

            $this->fill($booking, array('property_type', 'contractors', 'email_contractors', 'name_contractors', 'address', 'customer', 'customer_commercial_info', 'customer_contacts'));

            $bookingServices = $modelContractorServiceBooking->getServicesAndTotalServiceBookingQouteAsViewParam($booking['booking_id']);

            //create code
            $cancel_hashcode = sha1(uniqid());
            $this->updateById($booking['booking_id'], array('cancel_hashcode' => $cancel_hashcode));

            $cancel_link = $router->assemble(array('cancel_hashcode' => $cancel_hashcode, 'booking_id' => $booking['booking_id'], 'status_id' => $onHold['booking_status_id']), 'cancelBooking');

            $customer = $modelCustomer->getById($booking['customer_id']);
            $user = $modelUser->getById($booking['created_by']);

            // Create pdf
            $view = new Zend_View();
            $view->setScriptPath(APPLICATION_PATH . '/modules/booking/views/scripts/index');
            $view->booking = $booking;
            $view->bookingServices = $bookingServices['bookingServices'];
            $view->thisBookingServices = $bookingServices['thisBookingServices'];
            $view->priceArray = $bookingServices['priceArray'];

            $bodyBooking = $view->render('booking-customer.phtml');

            $pdfPath = createPdfPath();
            $destination = $pdfPath['fullDir'] . $booking['booking_num'] . '.pdf';
            wkhtmltopdf($bodyBooking, $destination);
			
            $template_params = array(
                //link
                '{cancel_link}' => '<a href="' . $cancel_link . '">' . $cancel_link . '</a>',
                //booking
                '{booking_num}' => $booking['booking_num'],
                '{to_follow}' => getDateFormating($booking['to_follow']),
                '{total_without_tax}' => number_format($booking['sub_total'], 2),
                '{gst_tax}' => number_format($booking['gst'], 2),
                '{total_with_tax}' => number_format($booking['qoute'], 2),
                '{description}' => nl2br($booking['description'] ? $booking['description'] : ''),
                '{booking_created}' => date('d/m/Y', $booking['created']),
                '{booking_created_by}' => ucwords($user['username']),
                '{booking_start}' => date("F j, Y, g:i a", strtotime($booking['booking_start'])),
                '{booking_address}' => get_line_address($modelBookingAddress->getByBookingId($booking['booking_id'])),
                '{service}' => nl2br($modelContractorServiceBooking->getBookingAsText($booking['booking_id'], true)),
                '{property}' => $booking['property_type'],
                '{booking_view}' => $bodyBooking,
                //customer
                '{customer_name}' => get_customer_name($customer),
                '{customer_first_name}' => isset($customer['first_name']) && $customer['first_name'] ? ucwords($customer['first_name']) : '',
                '{customer_last_name}' => isset($customer['last_name']) && $customer['last_name'] ? ' ' . ucwords($customer['last_name']) : '',
                '{customer_contacts}' => nl2br($modelCustomer->getCustomerContacts($booking['customer_id'])),
                '{sender_name}' => ucwords($user['username'])
            );

            $to = array();
            if ($customer['email1']&& filter_var($customer['email1'], FILTER_VALIDATE_EMAIL)) {
                $to[] = $customer['email1'];
            }
            if ($customer['email2']&& filter_var($customer['email2'], FILTER_VALIDATE_EMAIL)) {
                $to[] = $customer['email2'];
            }
            if ($customer['email3']&& filter_var($customer['email3'], FILTER_VALIDATE_EMAIL)) {
                $to[] = $customer['email3'];
            }
            $to = implode(',', $to);

            $email_log = array('reference_id' => $booking['booking_id'],'cronjob_history_id' => $cronjobHistoryId, 'type' => 'booking');

            $companyId = $booking['company_id'];

            $success = false;
			
			$trading_namesObj = new Model_TradingName();
			$trading_names = $trading_namesObj->getById($booking['trading_name_id']);
		

			
            $params = array(
                'to' => $to,
                'reply' => array('name' => $user['display_name'], 'email' => $user['email1']),
                'attachment' => $destination,
				'companyId' => $companyId,
				'trading_name' => $trading_names['trading_name'],
				'from' => $trading_names['email'],	
            );
			
			///if the creator of this booking is super_admin don't send to his personal email, but send to system email
				$superAdminRoleId = $modelAuthRole->getRoleIdByName('super_admin');
				if($user['role_id'] == $superAdminRoleId){
					$params['reply']= array('email' => $user['system_email']);
				}
			///check if the contractor is disabled, if yes we should replace his email with enquiries email
				if($user['active']=='FALSE'){
					//get company of the contractor
					$modelUserCompanies = new Model_UserCompanies();
					$userCompany = $modelUserCompanies->getCompaniesByUserId($user['user_id']);
					$params['reply']= array('email' => $userCompany['company_enquiries_email']);
				}

			 if ($to) {
              		try {
					EmailNotification::sendEmail($params, 'reminder_on_hold_booking', $template_params, $email_log, $companyId);
					$onHoldReminded = 'done';
					} 
				catch( Zend_Mail_Transport_Exception $e ) {
					$onHoldReminded = 'error';
					}
                }
               
            $this->updateById($booking['booking_id'], array('on_hold_reminded' => $onHoldReminded));
			
			
        }
		
    }

    public function cronJobResetOnHoldBookingReminder() {
        //
        // load models
        //
        $modelBookingStatus = new Model_BookingStatus();

        $today = time();

        $onHold = $modelBookingStatus->getByStatusName('ON HOLD');

        $select = $this->getAdapter()->select();

        $select->from(array('bok' => $this->_name));
        $select->where("bok.convert_status = 'booking'");
        $select->where("bok.is_deleted = 0");
        $select->where("bok.status_id = {$onHold['booking_status_id']}");
        $select->where("bok.to_follow < '{$today}'");
		$select->where("bok.is_to_follow = 1");

        $bookings = $this->getAdapter()->fetchAll($select);

        foreach ($bookings as $booking) {
            $this->updateById($booking['booking_id'], array('on_hold_reminded' => 'none'));
        }
    }

    public function cronJobReminderOnAwaitingUpdateAndInProcessBooking() {
		$modelCronjobHistory = new Model_CronjobHistory();
		$modelCronJob = new Model_CronJob();
		
		//
		// save this cron in cronjob_history table
		//
		$cronjob = $modelCronJob->getIdByName('Reminder_Contractor_to_Update_Booking');
		$cronjonID = $cronjob['id'];
		
		$cronjobHistoryData = array(
		'cron_job_id' => $cronjonID,
		'run_time' => time()
		);
		$cronjobHistoryId = $modelCronjobHistory->insert($cronjobHistoryData);
		//************

        $modelBookingStatus = new Model_BookingStatus();
        $modelUser = new Model_User();

        $inProcess = $modelBookingStatus->getByStatusName('IN PROGRESS');
        $awaitingUpdate = $modelBookingStatus->getByStatusName('AWAITING UPDATE');

        $select_bookings = $this->getAdapter()->select();
        $select_bookings->from(array('bok' => $this->_name));
        $select_bookings->where("bok.is_deleted = 0");
        $select_bookings->where("bok.pause_emails = 0");
        $select_bookings->where("bok.status_id = '{$inProcess['booking_status_id']}' OR bok.status_id = '{$awaitingUpdate['booking_status_id']}'");
        $select_bookings->joinInner(array('csb' => 'contractor_service_booking'), 'bok.booking_id = csb.booking_id');
        $bookings = $this->getAdapter()->fetchAll($select_bookings);

        $contractors = array();
        foreach ($bookings as $booking) {
            $contractors[$booking['contractor_id']] = array('user' => $modelUser->getById($booking['contractor_id']), 'company_id' => $booking['company_id']);
        }

		
        foreach ($contractors as $contractor_id => $contractor) {

            $to = array();
            if ($contractor['user']['email1']&& filter_var($contractor['user']['email1'], FILTER_VALIDATE_EMAIL)) {
                $to[] = $contractor['user']['email1'];
            }
            if ($contractor['user']['email2']&& filter_var($contractor['user']['email2'], FILTER_VALIDATE_EMAIL)) {
                $to[] = $contractor['user']['email2'];
            }
            if ($contractor['user']['email3']&& filter_var($contractor['user']['email3'], FILTER_VALIDATE_EMAIL)) {
                $to[] = $contractor['user']['email3'];
            }

            $to = implode(',', $to);

            $emailTemlate = $this->getAwaitingUpdateAndInProcessEmailTemplateByContractor($contractor_id);

            $params = array(
                'to' => $to,
                'body' => $emailTemlate['body'],
                'subject' => $emailTemlate['subject'],
				'companyId' => $contractor['company_id']
            );

            $email_log = array('reference_id' => $contractor_id, 'cronjob_history_id' => $cronjobHistoryId, 'type' => 'common');
            EmailNotification::sendEmail($params, '', array(), $email_log, $contractor['company_id']);
			
        }
		
    }


   public function cronJobReminderTentativeBooking() {
		$modelCronjobHistory = new Model_CronjobHistory();
		$modelCronJob = new Model_CronJob();
		
		//
		// save this cron in cronjob_history table
		//
		$cronjob = $modelCronJob->getIdByName('Reminder_Tentative_Booking');
		$cronjonID = $cronjob['id'];
		
		$cronjobHistoryData = array(
		'cron_job_id' => $cronjonID,
		'run_time' => time()
		);
		$cronjobHistoryId = $modelCronjobHistory->insert($cronjobHistoryData);
		//************
        //
        // load models
        //
        $modelUser = new Model_User();
        $modelCustomer = new Model_Customer();
        $modelBookingStatus = new Model_BookingStatus();
        $modelBookingAddress = new Model_BookingAddress();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
		$modelAuthRole = new Model_AuthRole();
		
        $router = Zend_Controller_Front::getInstance()->getRouter();

        $today = date('Y-m-d H:i:s', time());

        $tentative = $modelBookingStatus->getByStatusName('TENTATIVE');

        $select = $this->getAdapter()->select();

        $select->from(array('bok' => $this->_name));
        $select->where("bok.convert_status = 'booking'");
        $select->where("bok.is_deleted = 0");
        $select->where("bok.pause_emails = 0");
        $select->where("bok.status_id = {$tentative['booking_status_id']}");
        $select->where("bok.booking_start < '{$today}'");
        $select->where("bok.tentative_reminded != 'done'");
        $select->where("bok.is_change = 0");//not awaiting approval

        $bookings = $this->getAdapter()->fetchAll($select);
		
        foreach ($bookings as $booking) {


            $this->fill($booking, array('property_type', 'contractors', 'email_contractors', 'name_contractors', 'address', 'customer', 'customer_commercial_info', 'customer_contacts'));
            $bookingServices = $modelContractorServiceBooking->getServicesAndTotalServiceBookingQouteAsViewParam($booking['booking_id']);

            //create code
            $cancel_hashcode = sha1(uniqid());
            $this->updateById($booking['booking_id'], array('cancel_hashcode' => $cancel_hashcode));

            $cancel_link = $router->assemble(array('cancel_hashcode' => $cancel_hashcode, 'booking_id' => $booking['booking_id'], 'status_id' => $tentative['booking_status_id']), 'cancelBooking');

            $customer = $modelCustomer->getById($booking['customer_id']);
            $user = $modelUser->getById($booking['created_by']);

            // Create pdf
            $view = new Zend_View();
            $view->setScriptPath(APPLICATION_PATH . '/modules/booking/views/scripts/index');
            $view->booking = $booking;
            $view->bookingServices = $bookingServices['bookingServices'];
            $view->thisBookingServices = $bookingServices['thisBookingServices'];
            $view->priceArray = $bookingServices['priceArray'];

            $bodyBooking = $view->render('booking-customer.phtml');

            $pdfPath = createPdfPath();
            $destination = $pdfPath['fullDir'] . $booking['booking_num'] . '.pdf';
            wkhtmltopdf($bodyBooking, $destination);
			
            $template_params = array(
                //link
                '{cancel_link}' => '<a href="' . $cancel_link . '">' . $cancel_link . '</a>',
                //booking
                '{booking_num}' => $booking['booking_num'],
                '{total_without_tax}' => number_format($booking['sub_total'], 2),
                '{gst_tax}' => number_format($booking['gst'], 2),
                '{total_with_tax}' => number_format($booking['qoute'], 2),
                '{description}' => nl2br($booking['description'] ? $booking['description'] : ''),
                '{booking_created}' => date('d/m/Y', $booking['created']),
                '{booking_created_by}' => ucwords($user['username']),
                '{booking_start}' => date("F j, Y, g:i a", strtotime($booking['booking_start'])),
                '{booking_address}' => get_line_address($modelBookingAddress->getByBookingId($booking['booking_id'])),
                '{service}' => nl2br($modelContractorServiceBooking->getBookingAsText($booking['booking_id'], true)),
                '{property}' => $booking['property_type'],
                '{booking_view}' => $bodyBooking,
                //customer
                '{customer_name}' => get_customer_name($customer),
                '{customer_first_name}' => isset($customer['first_name']) && $customer['first_name'] ? ucwords($customer['first_name']) : '',
                '{customer_last_name}' => isset($customer['last_name']) && $customer['last_name'] ? ' ' . ucwords($customer['last_name']) : '',
                '{customer_contacts}' => nl2br($modelCustomer->getCustomerContacts($booking['customer_id'])),
                '{sender_name}' => ucwords($user['username'])
            );

            $to = array();
            if ($customer['email1']&& filter_var($customer['email1'], FILTER_VALIDATE_EMAIL)) {
                $to[] = $customer['email1'];
            }
            if ($customer['email2']&& filter_var($customer['email2'], FILTER_VALIDATE_EMAIL)) {
                $to[] = $customer['email2'];
            }
            if ($customer['email2']&& filter_var($customer['email3'], FILTER_VALIDATE_EMAIL)) {
                $to[] = $customer['email3'];
            }
            $to = implode(',', $to);

            $email_log = array('reference_id' => $booking['booking_id'], 'cronjob_history_id' => $cronjobHistoryId, 'type' => 'booking');

            $companyId = $booking['company_id'];

            $success = false;
			
			$trading_namesObj = new Model_TradingName();
        $trading_names = $trading_namesObj->getById($booking['trading_name_id']);
		

			
            $data = array(
                'to' => $to,
                'reply' => array('name' => $user['display_name'], 'email' => $user['email1']),
                'attachment' => $destination,
				'companyId' => $companyId,
				'trading_name' => $trading_names['trading_name'],
				'from' => $trading_names['email'],	
            );
			
			///if the creator of this booking is super_admin don't send to his personal email, but send to system email
			$superAdminRoleId = $modelAuthRole->getRoleIdByName('super_admin');
			if($user['role_id'] == $superAdminRoleId){
				$data['reply']= array('email' => $user['system_email']);
			}
			///check if the contractor is disabled, if yes we should replace his email with enquiries email
				if($user['active']=='FALSE'){
					//get company of the contractor
					$modelUserCompanies = new Model_UserCompanies();
					$userCompany = $modelUserCompanies->getCompaniesByUserId($user['user_id']);
					$data['reply']= array('email' => $userCompany['company_enquiries_email']);
				}

				
			if ($to) {
              	try {
					EmailNotification::sendEmail($data, 'reminder_tentative_booking', $template_params, $email_log, $companyId);
					$tentativeReminded = 'done';
					} 
				catch( Zend_Mail_Transport_Exception $e ) {
					$tentativeReminded = 'error';
					}
                }
            $this->updateById($booking['booking_id'], array('tentative_reminded' => $tentativeReminded));
			
			
        }		
    }

    public function cronJobResetTentativeBookingReminder() {
        //
        // load models
        //
        $modelBookingStatus = new Model_BookingStatus();

        $today = date('Y-m-d H:i:s', time());

        $tentative = $modelBookingStatus->getByStatusName('TENTATIVE');

        $select = $this->getAdapter()->select();

        $select->from(array('bok' => $this->_name));
        $select->where("bok.convert_status = 'booking'");
        $select->where("bok.is_deleted = 0");
        $select->where("bok.status_id = {$tentative['booking_status_id']}");
        $select->where("bok.booking_start < '{$today}'");

        $bookings = $this->getAdapter()->fetchAll($select);

        foreach ($bookings as $booking) {
            $this->updateById($booking['booking_id'], array('tentative_reminded' => 'none'));
        }
    }
	
	
	      /*
           * 10/05/2015 D.A
           * 24/05/2015 D.A updated
           * get table row of booking Ids according to the assigned date and status name
           * @param: Date Date , statusName String , companyId int
           * @return: array of booking that have to do status at that date
           */

    public function getBookingByDateAndStatus($date , $statusName , $companyId , $loggedUser , $myDashboard ) {
        //
        // load models
        //
        $modelBookingStatus = new Model_BookingStatus();
        $status = $modelBookingStatus->getByStatusName($statusName);
        // This query return Booking created at a specific date
        $select = $this->getAdapter()->select();
        $select->from(array('bok' => $this->_name));
        $select->where("bok.is_deleted = 0");
        $select->where("bok.company_id ='{$companyId}'");
        $select->where("bok.status_id = {$status['booking_status_id']}");
        if ($myDashboard) {
            $select->joinInner(array('csb' => 'contractor_service_booking'), 'bok.booking_id = csb.booking_id');
            $select->where("bok.created_by = {$loggedUser} OR csb.contractor_id = {$loggedUser}");
        }

        $theDayBookings = $this->getAdapter()->fetchAll($select);
        $booking=array();
        if ($theDayBookings) {
            foreach ($theDayBookings as $theDayBooking) {
                $theDayBookings['created']= date('Y-m-d', $theDayBooking['created']);
                if($theDayBookings['created']==$date){
                    $booking[]=$theDayBooking['booking_id'];
                }
            }
        }

        // This query return the converted to booking with TO DO status at specific date
        $select_bookings = $this->getAdapter()->select();
        $select_bookings->from(array('bok' => $this->_name));
        $select_bookings->where("bok.is_deleted = 0");
        $select->where("bok.company_id ='{$companyId}'");
        $select_bookings->where("bok.convert_status = 'booking'");
        $select_bookings->joinInner(array('bh' => 'booking_history'), 'bok.booking_id = bh.booking_id', array('history_created'=> 'created'));
        $select_bookings->where("bh.description = 'status'");
        $select_bookings->where("bh.booking_after = 'TO DO'");
        $select_bookings->where("bh.type = 'changed'");

        $theDayBookingsToDo = $this->getAdapter()->fetchAll($select_bookings);
        if ($theDayBookingsToDo) {
            foreach ($theDayBookingsToDo as $TheDayBookingToDo) {
                $theDayBookingsToDo['history_created']= date('Y-m-d', $TheDayBookingToDo['history_created']);
                if($theDayBookingsToDo['history_created']==$date){
                    //check that the booking not duplicated in the array
                    if(!in_array($TheDayBookingToDo['booking_id'], $booking)){
                        $booking[]=$TheDayBookingToDo['booking_id'];
                    }
                }
            }
        }


        // This query return booking with TO DO status at a date before booking date
        $select_history_bookings = $this->getAdapter()->select();
        $select_history_bookings->from(array('bok' => $this->_name));
        $select_history_bookings->where("bok.company_id ='{$companyId}'");
        $select_history_bookings->where("bok.convert_status = 'booking'");
        $select_history_bookings->joinInner(array('bh' => 'booking_history'), 'bok.booking_id = bh.booking_id', array('history_created'=> 'created'));
        $select_history_bookings->where("bh.created > bok.created");
        $select_history_bookings->where("bh.description = 'status'");
        $select_history_bookings->where("bh.booking_before = 'TO DO'");
        if ($myDashboard) {
            $select_history_bookings->joinInner(array('csb' => 'contractor_service_booking'), 'bok.booking_id = csb.booking_id');
            $select_history_bookings->where("bok.created_by = {$loggedUser} OR csb.contractor_id = {$loggedUser}");
        }
        $theLastBookingsToDo = $this->getAdapter()->fetchAll($select_history_bookings);

        if ($theLastBookingsToDo) {
            foreach ($theLastBookingsToDo as $TheLastBookingToDo) {
                $theLastBookingsToDo['created']= date('Y-m-d', $TheLastBookingToDo['created']);
                if($theLastBookingsToDo['created']==$date){
                    //check that the booking not duplicated in the array
                    if(!in_array($TheLastBookingToDo['booking_id'], $booking)){
                        $booking[]=$TheLastBookingToDo['booking_id'];
                    }
                }
            }
        }


        // This query return deleted booking that was with TO DO status
        $select_deleted_bookings = $this->getAdapter()->select();
        $select_deleted_bookings->from(array('bok' => $this->_name));
        $select_deleted_bookings->where("bok.company_id ='{$companyId}'");
        $select_deleted_bookings->where("bok.status_id = {$status['booking_status_id']}");
        $select_deleted_bookings->joinInner(array('lu' => 'log_user'), 'bok.booking_id = lu.item_id', array('logUser_created'=> 'created'));
        $select_deleted_bookings->where("lu.created > bok.created");
        $select_deleted_bookings->where("lu.item_type = 'booking'");
        $select_deleted_bookings->where("lu.event = 'deleted'");
        if ($myDashboard) {
            $select_deleted_bookings->joinInner(array('csb' => 'contractor_service_booking'), 'bok.booking_id = csb.booking_id');
            $select_deleted_bookings->where("bok.created_by = {$loggedUser} OR csb.contractor_id = {$loggedUser}");
        }
        $theDeletedBookingsToDo = $this->getAdapter()->fetchAll($select_deleted_bookings);
        if ($theDeletedBookingsToDo) {
            foreach ($theDeletedBookingsToDo as $TheDeletedBookingToDo) {
                $theDeletedBookingsToDo['created']= date('Y-m-d', $TheDeletedBookingToDo['created']);
                if($theDeletedBookingsToDo['created']==$date){
                    //check that the booking not duplicated in the array
                    if(!in_array($TheDeletedBookingToDo['booking_id'], $booking)){
                        $booking[]=$TheDeletedBookingToDo['booking_id'];
                    }
                }
            }
        }

        return $booking;
    }

    public function getTotalAmountBookingDetails($bookingId) {

        $booking = $this->getById($bookingId);

        // Load Model

        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelContractorServiceBookingTemp = new Model_ContractorServiceBookingTemp();

        $serviceBookingTemp = $modelContractorServiceBookingTemp->getByBookingId($bookingId);
        if (!empty($serviceBookingTemp) && !$this->checkCanEditBookingDetails($bookingId)) {

            $subTotal = $modelContractorServiceBookingTemp->getTotalBookingQoute($bookingId);
            $totalDiscount = !empty($booking['total_discount_temp']) ? $booking['total_discount_temp'] : 0;
            $callOutFee = !empty($booking['call_out_fee_temp']) ? $booking['call_out_fee_temp'] : 0;
        } else {
            $subTotal = $modelContractorServiceBooking->getTotalBookingQoute($bookingId);
            $totalDiscount = !empty($booking['total_discount']) ? $booking['total_discount'] : 0;
            $callOutFee = !empty($booking['call_out_fee']) ? $booking['call_out_fee'] : 0;
        }

        //$gst = ($subTotal + $callOutFee - $totalDiscount) * get_config('gst_tax');
		$gst = ($subTotal + $callOutFee) * get_config('gst_tax');
        $paidAmount = !empty($booking['paid_amount']) ? $booking['paid_amount'] : 0;
        $refund = !empty($booking['refund']) ? $booking['refund'] : 0;
        $total = $subTotal + $callOutFee + $gst - $totalDiscount;
        $totalAmount = $total - $refund;
        $balanceDue = $totalAmount - $paidAmount;
		
		
		


        $totalAmountDetails = array();
        $totalAmountDetails['sub_total'] = $subTotal;
        $totalAmountDetails['total_discount'] = $totalDiscount;
        $totalAmountDetails['call_out_fee'] = $callOutFee;
        $totalAmountDetails['gst'] = $gst;
        $totalAmountDetails['paid_amount'] = $paidAmount;
        $totalAmountDetails['refund'] = $refund;
        $totalAmountDetails['total'] = $total;
        $totalAmountDetails['paid_amount'] = $paidAmount;
        $totalAmountDetails['total_amount'] = $totalAmount;
        $totalAmountDetails['balance_due'] = $balanceDue;

        return $totalAmountDetails;
    }

    public function checkBeforeDeleteBooking($id, &$tables = array()) {

        $sucsess = true;

        $select_booking_invoice = $this->getAdapter()->select();
        $select_booking_invoice->from('booking_invoice', 'COUNT(*)');
        $select_booking_invoice->where("booking_id = {$id}");
        $count_booking_invoice = $this->getAdapter()->fetchOne($select_booking_invoice);

        if ($count_booking_invoice) {
            $tables[] = 'booking_invoice';
            $sucsess = false;
        }

        $select_booking_estimate = $this->getAdapter()->select();
        $select_booking_estimate->from('booking_estimate', 'COUNT(*)');
        $select_booking_estimate->where("booking_id = {$id}");
        $count_booking_estimate = $this->getAdapter()->fetchOne($select_booking_estimate);

        if ($count_booking_estimate) {
            $tables[] = 'booking_estimate';
            $sucsess = false;
        }

        $select_payment = $this->getAdapter()->select();
        $select_payment->from('payment', 'COUNT(*)');
        $select_payment->where("booking_id = {$id}");
        $count_payment = $this->getAdapter()->fetchOne($select_payment);

        if ($count_payment) {
            $tables[] = 'payment';
            $sucsess = false;
        }

        $select_refund = $this->getAdapter()->select();
        $select_refund->from('refund', 'COUNT(*)');
        $select_refund->where("booking_id = {$id}");
        $count_refund = $this->getAdapter()->fetchOne($select_refund);

        if ($count_refund) {
            $tables[] = 'refund';
            $sucsess = false;
        }

        $select_booking_contractor_payment = $this->getAdapter()->select();
        $select_booking_contractor_payment->from('booking_contractor_payment', 'COUNT(*)');
        $select_booking_contractor_payment->where("booking_id = {$id}");
        $count_booking_contractor_payment = $this->getAdapter()->fetchOne($select_booking_contractor_payment);

        if ($count_booking_contractor_payment) {
            $tables[] = 'booking_contractor_payment';
            $sucsess = false;
        }

        $select_contractor_share_booking = $this->getAdapter()->select();
        $select_contractor_share_booking->from('contractor_share_booking', 'COUNT(*)');
        $select_contractor_share_booking->where("booking_id = {$id}");
        $count_contractor_share_booking = $this->getAdapter()->fetchOne($select_contractor_share_booking);

        if ($count_contractor_share_booking) {
            $tables[] = 'contractor_share_booking';
            $sucsess = false;
        }

        $select_complaint = $this->getAdapter()->select();
        $select_complaint->from('complaint', 'COUNT(*)');
        $select_complaint->where("booking_id = {$id}");
        $count_complaint = $this->getAdapter()->fetchOne($select_complaint);

        if ($count_complaint) {
            $tables[] = 'complaint';
            $sucsess = false;
        }

        return $sucsess;
    }

    public function deleteRelatedBooking($id) {

        //delete data from booking_attachment
        $this->getAdapter()->delete('booking_attachment', "booking_id = '{$id}'");

        //delete data from booking_label
        $this->getAdapter()->delete('booking_label', "booking_id = '{$id}'");

        //delete data from booking_product
        $this->getAdapter()->delete('booking_product', "booking_id = '{$id}'");

        //delete data from booking_discussion
        $this->getAdapter()->delete('booking_discussion', "booking_id = '{$id}'");

        //delete data from booking_status_discussion
        $this->getAdapter()->delete('booking_status_discussion', "booking_id = '{$id}'");

        //delete data from google_calendar_event
        $this->getAdapter()->delete('google_calendar_event', "booking_id = '{$id}'");

        //delete data from booking_status_history
        $this->getAdapter()->delete('booking_status_history', "booking_id = '{$id}'");

        //delete data from booking_multiple_days
        $this->getAdapter()->delete('booking_multiple_days', "booking_id = '{$id}'");

        //delete data from booking_contact_history
        $this->getAdapter()->delete('booking_contact_history', "booking_id = '{$id}'");

        //delete data from booking_reminder
        $this->getAdapter()->delete('booking_reminder', "booking_id = '{$id}'");

        //delete data from booking_address
        $this->getAdapter()->delete('booking_address', "booking_id = '{$id}'");

        //delete data from booking_address_temp
        $this->getAdapter()->delete('booking_address_temp', "booking_id = '{$id}'");

        //delete data from booking_due_date
        $this->getAdapter()->delete('booking_due_date', "booking_id = '{$id}'");

        //delete data from ios_sync
        $this->getAdapter()->delete('ios_sync', "booking_id = '{$id}'");

        //delete data from contractor_service_booking
        $this->getAdapter()->delete('contractor_service_booking', "booking_id = '{$id}'");

        //delete data from contractor_service_booking_temp
        $this->getAdapter()->delete('contractor_service_booking_temp', "booking_id = '{$id}'");

        //delete data from service_attribute_value
        $this->getAdapter()->delete('service_attribute_value', "booking_id = '{$id}'");

        //delete data from service_attribute_value_temp
        $this->getAdapter()->delete('service_attribute_value_temp', "booking_id = '{$id}'");
    }

    public function getMultipleTimeAsString($multipleDays = array()) {

        $string = "";

        if (!empty($multipleDays)) {
            $string .= "This Booking linked also to these Days" . "<br>";
            foreach ($multipleDays as $day) {
                $date = date("F j, Y, g:i a", strtotime($day['booking_start']));
                $string .="Date: {$date}" . "<br>";
            }
        }

        return $string;
    }

    public function checkIfCanChangeBookingStatus($bookingId, $newStatusId, &$message) {

        //load model
        $modelPayment = new Model_Payment();
        $modelBookingStatus = new Model_BookingStatus();

        // get status by name
        $completed = $modelBookingStatus->getByStatusName('COMPLETED');
        $failed = $modelBookingStatus->getByStatusName('FAILED');
        $inProcess = $modelBookingStatus->getByStatusName('IN PROGRESS');
		 $cancelled = $modelBookingStatus->getByStatusName('CANCELLED');
        $isSuccess = true;
		
        $booking = $this->getById($bookingId);
		if ($booking) {
		if ($booking['convert_status'] == 'invoice') {
			$modelBookingInvoice = new Model_BookingInvoice();
				$booking_invoice = $modelBookingInvoice->getByBookingId($booking['booking_id']);
				//var_dump($booking_invoice);
				//exit;
                if ($newStatusId != $failed['booking_status_id'] && $newStatusId != $inProcess['booking_status_id'] && $newStatusId != $completed['booking_status_id'] && $newStatusId != $cancelled['booking_status_id'] ) {
                    $isSuccess = false;
                    $message = 'You can currently only convert this booking status to (IN PROGRESS, Completed, Failed), because it has a valid invoice. 
To be able to convert this booking status to "Cancelled", you must first convert the invoice to Void.';
                } else {
     if($newStatusId == $cancelled['booking_status_id']){
      if($booking_invoice['invoice_type'] != 'void'){
       $isSuccess = false;
       $message = 'You can currently only convert this booking status to (IN PROGRESS, Completed, Failed), because it has a valid invoice. 
To be able to convert this booking status to "Cancelled", you must first convert the invoice to Void.';
      }
	  
      
     }
     else{
      $payments = $modelPayment->getByBookingId($bookingId);

      if ($payments) {

       //Completed Status  and InProcess Status
       if ($booking['status_id'] == $completed['booking_status_id'] || $booking['status_id'] == $inProcess['booking_status_id']) {
        if ($newStatusId == $failed['booking_status_id']) {
         $isSuccess = false;
         $message = 'You could not change to this Status , Because it has a Payments';
        }
       }

       //Faild Status
       if ($booking['status_id'] == $failed['booking_status_id']) {
        $isSuccess = false;
        $message = 'You could not change to this Status , Because it has a Payments';
       }
      }
      
     }
                    
                }
		}
		}
        /*if ($booking) {
            if ($booking['convert_status'] == 'invoice') {
                if ($newStatusId != $faild['booking_status_id'] && $newStatusId != $inProcess['booking_status_id'] && $newStatusId != $completed['booking_status_id']) {
                    $isSuccess = false;
                    $message = 'You could not change to this Status , Because it is Invoice , You can change to this Status only (IN PROGRESS , Completed , Failed)';
                } else {
                    $payments = $modelPayment->getByBookingId($bookingId);

                    if ($payments) {

                        //Completed Status  and InProcess Status
                        if ($booking['status_id'] == $completed['booking_status_id'] || $booking['status_id'] == $inProcess['booking_status_id']) {
                            if ($newStatusId == $faild['booking_status_id']) {
                                $isSuccess = false;
                                $message = 'You could not change to this Status , Because it has a Payments';
                            }
                        }

                        //Faild Status
                        if ($booking['status_id'] == $faild['booking_status_id']) {
                            $isSuccess = false;
                            $message = 'You could not change to this Status , Because it has a Payments';
                        }
                    }
                }
            }
        }
		*/
        return $isSuccess;
    }

    public function getAwaitingUpdateAndInProcessEmailTemplateByContractor($contractorId) {

        // load model
        $modelBookingStatus = new Model_BookingStatus();
        $modelEmailTemplate = new Model_EmailTemplate();
        $modelUserCompanies = new Model_UserCompanies();

        $inProcess = $modelBookingStatus->getByStatusName('IN PROGRESS');
        $awaitingUpdate = $modelBookingStatus->getByStatusName('AWAITING UPDATE');

        $inProcessBookings = $this->cronJopGetBookingByStatusAndContractor($inProcess['booking_status_id'], $contractorId);
        $awaitingUpdateBookings = $this->cronJopGetBookingByStatusAndContractor($awaitingUpdate['booking_status_id'], $contractorId);

        $contractorCompany = $modelUserCompanies->getByUserId($contractorId);

        $inProcessView = '';
        if ($inProcessBookings) {
            $this->fills($inProcessBookings, array('address', 'customer', 'status'));

            $view = new Zend_View();
            $view->setScriptPath(APPLICATION_PATH . '/modules/default/views/scripts/common-views');
            $view->bookings = $inProcessBookings;

            $inProcessView = $view->render('reminder_update_booking.phtml');
        }

        $awaitingUpdateView = '';
        if ($awaitingUpdateBookings) {
            $this->fills($awaitingUpdateBookings, array('address', 'customer', 'status'));

            $view = new Zend_View();
            $view->setScriptPath(APPLICATION_PATH . '/modules/default/views/scripts/common-views');
            $view->bookings = $awaitingUpdateBookings;

            $awaitingUpdateView = $view->render('reminder_update_booking.phtml');
        }

        $template_params = array(
            //count for Awaiting Update And InProcess
            '{in_process_count}' => count($inProcessBookings),
            '{awaiting_update_count}' => count($awaitingUpdateBookings),
            //list for Awaiting Update And IN PROGRESS
            '{in_process_list}' => $inProcessView,
            '{awaiting_update_list}' => $awaitingUpdateView,
        );

        return $modelEmailTemplate->getEmailTemplate('send_reminder_update_booking_to_contractor', $template_params, isset($contractorCompany) ? $contractorCompany['company_id'] : 0);
    }

    public function cronJopGetBookingByStatusAndContractor($statusId, $contractorId) {

        $select_bookings = $this->getAdapter()->select();
        $select_bookings->from(array('bok' => $this->_name));
        $select_bookings->where("bok.is_deleted = 0");
        $select_bookings->where("bok.status_id = '{$statusId}'");
        $select_bookings->joinInner(array('csb' => 'contractor_service_booking'), 'bok.booking_id = csb.booking_id');
        $select_bookings->where("bok.created_by = '{$contractorId}' OR csb.contractor_id = '{$contractorId}'");

        return $this->getAdapter()->fetchAll($select_bookings);
    }

    public function cronJobSendRequestFeedback() {

        //
        // load models
        //
        $modelUser = new Model_User();
        $modelCustomer = new Model_Customer();
        $modelBookingAddress = new Model_BookingAddress();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelCustomerFeedback = new Model_CustomerFeedback();
		$modelAuthRole = new Model_AuthRole();
		
        $router = Zend_Controller_Front::getInstance()->getRouter();


        //select statment
        $selectFeedBack = $this->getAdapter()->select();
        $selectFeedBack->from(array('cfb' => 'customer_feedback'), 'status_id');
        $selectFeedBack->where('bok.booking_id = cfb.booking_id');
        $selectFeedBack->where('bok.status_id = cfb.status_id');

        $select = $this->getAdapter()->select();
        $select->from(array('bok' => $this->_name));
        $select->joinInner(array('bs' => 'booking_status'), 'bs.booking_status_id = bok.status_id', 'name');
        $select->where('NOT EXISTS (?)', $selectFeedBack);
        $select->where('bs.request_feedback = 1');
        $bookings = $this->getAdapter()->fetchAll($select);

        foreach ($bookings as $booking) {


            $this->fill($booking, array('property_type', 'contractors', 'email_contractors', 'name_contractors', 'address', 'customer', 'customer_commercial_info', 'customer_contacts'));
            $bookingServices = $modelContractorServiceBooking->getServicesAndTotalServiceBookingQouteAsViewParam($booking['booking_id']);

            //create code
            $feedback_hashcode = sha1(uniqid());

            $data = array(
                'booking_id' => $booking['booking_id'],
                'status_id' => $booking['status_id'],
                'hashcode' => $feedback_hashcode,
                'created' => time()
            );
            $modelCustomerFeedback->insert($data);

            $feedback_link = $router->assemble(array('feedback_hashcode' => $feedback_hashcode, 'booking_id' => $booking['booking_id'], 'status_id' => $booking['status_id']), 'feedbackBooking');

            $customer = $modelCustomer->getById($booking['customer_id']);
            $user = $modelUser->getById($booking['created_by']);

            // Create pdf

            $view = new Zend_View();
            $view->setScriptPath(APPLICATION_PATH . '/modules/booking/views/scripts/index');
            $view->booking = $booking;
            $view->bookingServices = $bookingServices['bookingServices'];
            $view->thisBookingServices = $bookingServices['thisBookingServices'];
            $view->priceArray = $bookingServices['priceArray'];

            $bodyBooking = $view->render('booking-customer.phtml');

            $pdfPath = createPdfPath();
            $destination = $pdfPath['fullDir'] . $booking['booking_num'] . '.pdf';
            wkhtmltopdf($bodyBooking, $destination);

            $template_params = array(
                //link
                '{feedback_link}' => '<a href="' . $feedback_link . '">' . $feedback_link . '</a>',
                //booking
                '{booking_num}' => $booking['booking_num'],
                '{total_without_tax}' => number_format($booking['sub_total'], 2),
                '{gst_tax}' => number_format($booking['gst'], 2),
                '{total_with_tax}' => number_format($booking['qoute'], 2),
                '{description}' => nl2br($booking['description'] ? $booking['description'] : ''),
                '{booking_created}' => date('d/m/Y', $booking['created']),
                '{booking_created_by}' => ucwords($user['username']),
                '{booking_start}' => date("F j, Y, g:i a", strtotime($booking['booking_start'])),
                '{booking_address}' => get_line_address($modelBookingAddress->getByBookingId($booking['booking_id'])),
                '{service}' => nl2br($modelContractorServiceBooking->getBookingAsText($booking['booking_id'], true)),
                '{property}' => $booking['property_type'],
                '{booking_view}' => $bodyBooking,
                //status name
                '{status_name}' => $booking['name'],
                //
                //customer
                '{customer_name}' => get_customer_name($customer),
                '{customer_first_name}' => isset($customer['first_name']) && $customer['first_name'] ? ucwords($customer['first_name']) : '',
                '{customer_last_name}' => isset($customer['last_name']) && $customer['last_name'] ? ' ' . ucwords($customer['last_name']) : '',
                '{customer_contacts}' => nl2br($modelCustomer->getCustomerContacts($booking['customer_id'])),
                '{sender_name}' => ucwords($user['username'])
            );

            $to = array();
            if ($customer['email1']&& filter_var($customer['email1'], FILTER_VALIDATE_EMAIL)) {
                $to[] = $customer['email1'];
            }
            if ($customer['email2']&& filter_var($customer['email2'], FILTER_VALIDATE_EMAIL)) {
                $to[] = $customer['email2'];
            }
            if ($customer['email3']&& filter_var($customer['email3'], FILTER_VALIDATE_EMAIL)) {
                $to[] = $customer['email3'];
            }
            $to = implode(',', $to);

            $email_log = array('reference_id' => $booking['booking_id'], 'type' => 'booking');

            $companyId = $booking['company_id'];

            $success = false;
			$trading_namesObj = new Model_TradingName();
        $trading_names = $trading_namesObj->getById($booking['trading_name_id']);
		

            $data = array(
                'to' => $to,
                'reply' => array('name' => $user['display_name'], 'email' => $user['email1'],),
                'layout' => 'designed_email_template',
                'attachment' => $destination,
				'trading_name' => $trading_names['trading_name'],
				'from' => $trading_names['email'],	
            );
			
			///if the creator of this booking is super_admin don't send to his personal email, but send to system email
			$superAdminRoleId = $modelAuthRole->getRoleIdByName('super_admin');
			if($user['role_id'] == $superAdminRoleId){
				$data['reply']= array('email' => $user['system_email']);
			}
			///check if the contractor is disabled, if yes we should replace his email with enquiries email
				if($user['active']=='FALSE'){
					//get company of the contractor
					$modelUserCompanies = new Model_UserCompanies();
					$userCompany = $modelUserCompanies->getCompaniesByUserId($user['user_id']);
					$data['reply']= array('email' => $userCompany['company_enquiries_email']);
				}
			
            if ($to) {
                $success = EmailNotification::sendEmail($data, 'feedback_booking', $template_params, $email_log, $companyId);
            }
        }
    }

    public function getBookingViewParam($bookingId) {
        $viewParam = array();

        $booking = $this->getById($bookingId);
        if ($booking) {
            // fill data
            $this->fill($booking, array('contractors', 'name_contractors', 'address', 'customer', 'city', 'labels', 'status', 'is_accepted', 'can_delete', 'not_accepted_or_rejected', 'discussion', 'complaint', 'email_log', 'customer_commercial_info', 'customer_contacts'));

            // load models
            $modelContractorServiceBooking = new Model_ContractorServiceBooking();

            $bookingServices = $modelContractorServiceBooking->getByBookingId($bookingId);
            // $thisBookingServices : to put all service for this booking 
            $thisBookingServices = array();
            // $priceArray : to put all price and service for this booking 
            $priceArray = array();

            if ($bookingServices) {
                foreach ($bookingServices as $bookingService) {

                    $serviceId = $bookingService['service_id'];
                    $clone = $bookingService['clone'];
                    $bookingId = $bookingService['booking_id'];

                    $service_and_clone = $serviceId . ($clone ? '_' . $clone : '');

                    $thisBookingServices[] = $service_and_clone;

                    $priceArray[$service_and_clone] = $modelContractorServiceBooking->getTotalServiceBookingQoute($bookingId, $serviceId, $clone);
                }
            }

            $viewParam['booking'] = $booking;
            $viewParam['bookingServices'] = $bookingServices;
            $viewParam['thisBookingServices'] = $thisBookingServices;
            $viewParam['priceArray'] = $priceArray;
        }
        return $viewParam;
    }

    public function fillBookingAndServicesFieldTemp($bookingId) {

        $booking = $this->getById($bookingId);

        // check befor insert data , that this field not change (add to the temp)from contracor and not approved untill now 
        if ($booking['is_change'] == 0) {

            /*
             *  Booking Field Temp
             */
            $bookingData = array(
                'total_discount_temp' => $booking['total_discount'],
                'call_out_fee_temp' => $booking['call_out_fee']
            );
            $this->updateById($bookingId, $bookingData);

            /*
             * service Temp
             */
            $modelContractorServiceBooking = new Model_ContractorServiceBooking();
            $modelContractorServiceBookingTemp = new Model_ContractorServiceBookingTemp();
            $modelServiceAttributeValue = new Model_ServiceAttributeValue();
            $modelServiceAttributeValueTemp = new Model_ServiceAttributeValueTemp();
            $modelServiceAttribute = new Model_ServiceAttribute();

            $bookingServices = $modelContractorServiceBooking->getByBookingId($bookingId);

            foreach ($bookingServices as $bookingService) {

                $clone = $bookingService['clone'];
                $contractorId = $bookingService['contractor_id'];
                $serviceId = $bookingService['service_id'];
                $minPrice = $bookingService['consider_min_price'];

                $serviceDataTemp = array(
                    'booking_id' => $bookingId,
                    'contractor_id' => $contractorId,
                    'service_id' => $serviceId,
                    'clone' => $clone,
                    'consider_min_price' => $minPrice
                );

                $modelContractorServiceBookingTemp->insert($serviceDataTemp);

                /*
                 * Attribute Temp
                 */
                $serviceAttributes = $modelServiceAttribute->getAttributeByServiceId($serviceId);

                foreach ($serviceAttributes as $attribute) {
                    $serviceAttributeValue = $modelServiceAttributeValue->getByBookingIdAndServiceAttributeIdAndClone($bookingId, $attribute['service_attribute_id'], $clone);

                    $attributeDataTemp = array(
                        'service_attribute_id' => $attribute['service_attribute_id'],
                        'booking_id' => $bookingId,
                        'value' => $serviceAttributeValue['value'],
                        'is_serialized_array' => $serviceAttributeValue['is_serialized_array'],
                        'clone' => $clone
                    );

                    $modelServiceAttributeValueTemp->insert($attributeDataTemp);
                }
            }
        }
    }
	
	public function cronJobReminderCustomerBookingConfirmationOneDay() {
		
		$modelCronjobHistory = new Model_CronjobHistory();
		$modelCronJob = new Model_CronJob();
		$modelAuthRole = new Model_AuthRole();
		//
		// save this cron in cronjob_history table
		//
		$cronjob = $modelCronJob->getIdByName('Reminder_Customer_Booking_Confirmation_One_Day');
		$cronjonID = $cronjob['id'];
		
		$cronjobHistoryData = array(
		'cron_job_id' => $cronjonID,
		'run_time' => time()
		);
		$cronjobHistoryId = $modelCronjobHistory->insert($cronjobHistoryData);
		//************
	    $modelBookingStatus = new Model_BookingStatus();
        

        $inProcess = $modelBookingStatus->getByStatusName('IN PROGRESS');
        $toDo = $modelBookingStatus->getByStatusName('TO DO');
		$toVisit = $modelBookingStatus->getByStatusName('TO VISIT');
		
        $select_bookings = $this->getAdapter()->select();
        $select_bookings->from(array('bok' => $this->_name));
        $select_bookings->where("bok.is_deleted = 0");
        $select_bookings->where("bok.pause_emails = 0");
        $select_bookings->where("bok.status_id = '{$inProcess['booking_status_id']}' OR bok.status_id = '{$toDo['booking_status_id']}' OR bok.status_id = '{$toVisit['booking_status_id']}'");
        $select_bookings->where("DATEDIFF(Date(bok.booking_start), Date(NOW()) ) = 1");
		$select_bookings->joinInner(array('cst' => 'customer'), 'bok.customer_id = cst.customer_id',array('customer_email1'=> 'email1', 'customer_email2'=> 'email2', 'customer_email3'=> 'email3'));
        //$sql = $select_bookings->__toString();
		//echo "$sql\n";
		$customer_bookings = $this->getAdapter()->fetchAll($select_bookings);
		//$this->prepareAndSendReminderBookingEmailToCustomer($customer_bookings);
		$modelUser = new Model_User();
		$modelCustomer = new Model_Customer();
		$modelBookingAddress = new Model_BookingAddress();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
		
		$post_arr = array();
		foreach ($customer_bookings as $booking) {
			
            $to = array();
			
			$bookingServices = $modelContractorServiceBooking->getServicesAndTotalServiceBookingQouteAsViewParam($booking['booking_id']);

			
           if ($booking['customer_email1']&& filter_var($booking['customer_email1'], FILTER_VALIDATE_EMAIL)) {
                $to[] = $booking['customer_email1'];
            }
            if ($booking['customer_email2']&& filter_var($booking['customer_email2'], FILTER_VALIDATE_EMAIL)) {
                $to[] = $booking['customer_email2'];
            }
            if ($booking['customer_email3']&& filter_var($booking['customer_email3'], FILTER_VALIDATE_EMAIL)) {
                $to[] = $booking['customer_email3'];
            }
		
										
			$template_params = array(
			'{customer_first_name}' => isset($booking['first_name']) && $booking['first_name'] ? ucwords($booking['first_name']) : '',
			'{booking_num}' => $booking['booking_num'],
			'{booking_start}' => date("F j, Y, g:i a", strtotime($booking['booking_start'])),
			'{booking_address}' => get_line_address($modelBookingAddress->getByBookingId($booking['booking_id'])),
			'{property}' => $booking['property_type'],
			'{customer_contacts}' => nl2br($modelCustomer->getCustomerContacts($booking['customer_id'])),
			'{booking_created}' => date('d/m/Y', $booking['created']),
			'{sender_name}' => '--'
            
			);
			
			//By Islam
			$customer = $modelCustomer->getById($booking['customer_id']);
			
			//End By Islam
            $to = implode(',', $to);

            $modelEmailTemplate = new Model_EmailTemplate();
			$emailTemplate = $modelEmailTemplate->getEmailTemplate('send_customer_booking_reminder_email_1_day', $template_params,$customer['company_id']);
			
			///get the creator of this booking
			$user = $modelUser->getById($booking['created_by']);
			
			$trading_namesObj = new Model_TradingName();
			$trading_names = $trading_namesObj->getById($booking['trading_name_id']);


            $params = array(
                'to' => $to,
				'reply' => array('name' => $user['display_name'], 'email' => $user['email1']),
                'body' => $emailTemplate['body'],
                'subject' => $emailTemplate['subject'],
				'companyId' => $booking['company_id'],
				'trading_name' => $trading_names['trading_name'],
				'from' => $trading_names['email'],	
            );
			///if the creator of this booking is super_admin don't send to his personal email, but send to system email
				$superAdminRoleId = $modelAuthRole->getRoleIdByName('super_admin');
				if($user['role_id'] == $superAdminRoleId){
					$params['reply']= array('email' => $user['system_email']);
				}
			///check if the contractor is disabled, if yes we should replace his email with enquiries email
				if($user['active']=='FALSE'){
					//get company of the contractor
					$modelUserCompanies = new Model_UserCompanies();
					$userCompany = $modelUserCompanies->getCompaniesByUserId($user['user_id']);
					$params['reply']= array('email' => $userCompany['company_enquiries_email']);
				}	
			////end
            $email_log = array('reference_id' => $booking['booking_id'],'cronjob_history_id'=> $cronjobHistoryId, 'type' => 'booking', 'cronjob_history_id' => $cronjobHistoryId);
              
			if ($to) {
				try {
					EmailNotification::sendEmail($params, 'send_customer_booking_reminder_email_1_day', $template_params, $email_log, $booking['company_id']);
					
					} 
				catch( Zend_Mail_Transport_Exception $e ) {
					
					}
				
			
			}
        }
		
        
    }
	
	public function cronJobReminderCustomerBookingConfirmationTwoDay() {

        $modelBookingStatus = new Model_BookingStatus();
        

        $inProcess = $modelBookingStatus->getByStatusName('IN PROGRESS');
        $toDo = $modelBookingStatus->getByStatusName('TO DO');
		$toVisit = $modelBookingStatus->getByStatusName('TO VISIT');
		
        $select_bookings = $this->getAdapter()->select();
        $select_bookings->from(array('bok' => $this->_name));
        $select_bookings->where("bok.is_deleted = 0");
        $select_bookings->where("bok.pause_emails = 0");
        $select_bookings->where("bok.status_id = '{$inProcess['booking_status_id']}' OR bok.status_id = '{$toDo['booking_status_id']}' OR bok.status_id = '{$toVisit['booking_status_id']}'");
        $select_bookings->where("DATEDIFF(Date(bok.booking_start), Date(NOW()) ) = 2");
		$select_bookings->joinInner(array('cst' => 'customer'), 'bok.customer_id = cst.customer_id',array('customer_email1'=> 'email1', 'customer_email2'=> 'email2', 'customer_email3'=> 'email3'));
        
		$customer_bookings = $this->getAdapter()->fetchAll($select_bookings);
		//$this->prepareAndSendReminderBookingEmailToCustomer($customer_bookings);
		
		$modelCustomer = new Model_Customer();
		$modelBookingAddress = new Model_BookingAddress();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();

		foreach ($customer_bookings as $booking) {

            $to = array();
			
            if ($booking['customer_email1']&& filter_var($booking['customer_email1'], FILTER_VALIDATE_EMAIL)) {
                $to[] = $booking['customer_email1'];
            }
            if ($booking['customer_email2']&& filter_var($booking['customer_email2'], FILTER_VALIDATE_EMAIL)) {
                $to[] = $booking['customer_email2'];
            }
            if ($booking['customer_email3']&& filter_var($booking['customer_email3'], FILTER_VALIDATE_EMAIL)) {
                $to[] = $booking['customer_email3'];
            }
			
			// booking view Params
			$viewParam = $this->getBookingViewParam($booking['booking_id'], true);
			$view = new Zend_View();
			$view->setScriptPath(APPLICATION_PATH . '/modules/booking/views/scripts/index');
			$view->booking = $viewParam['booking'];
			$view->bookingServices = $viewParam['bookingServices'];
			$view->thisBookingServices = $viewParam['thisBookingServices'];
			$view->priceArray = $viewParam['priceArray'];

			$bodyBooking = $view->render('booking-customer.phtml');
							
			$template_params = array(
			'{customer_first_name}' => isset($booking['first_name']) && $booking['first_name'] ? ucwords($booking['first_name']) : '',
			'{booking_num}' => $booking['booking_num'],
			'{booking_start}' => date("F j, Y, g:i a", strtotime($booking['booking_start'])),
			'{booking_address}' => get_line_address($modelBookingAddress->getByBookingId($booking['booking_id'])),
			'{property}' => $booking['property_type'],
			'{customer_contacts}' => nl2br($modelCustomer->getCustomerContacts($booking['customer_id'])),
			'{booking_created}' => date('d/m/Y', $booking['created']),
            '{sender_name}' => '--'
			);
			
			//By Islam
			$customer = $modelCustomer->getById($booking['customer_id']);
			
            $to = implode(',', $to);

            $modelEmailTemplate = new Model_EmailTemplate();
				$emailTemplate = $modelEmailTemplate->getEmailTemplate('send_customer_booking_reminder_email_2_days', $template_params,$customer['company_id']);
				
			$trading_namesObj = new Model_TradingName();
			$trading_names = $trading_namesObj->getById($booking['trading_name_id']);
	


            $params = array(
                'to' => $to,
                'body' => $emailTemplate['body'],
                'subject' => $emailTemplate['subject'],
				'trading_name' => $trading_names['trading_name'],
				'from' => $trading_names['email'],	
            );
             // Create pdf
            $pdfPath = createPdfPath();
            $destination = $pdfPath['fullDir'] . $viewParam['booking']['booking_num'] . '.pdf';
            wkhtmltopdf($bodyBooking, $destination);
            $params['attachment'] = $destination;
          
                // Send Email
                 
                try {
					EmailNotification::sendEmail($params, '', array(), array('reference_id' => $booking['booking_id'], 'type' => 'booking'));
					$sent = 'done';
					} 
				catch( Zend_Mail_Transport_Exception $e ) {
					$$sent = 'error';
					}
				echo $sent;
        }

        
    }
	
	public function cronJobReminderContractorBookingConfirmationOneDay() {
		$modelCronjobHistory = new Model_CronjobHistory();
		$modelCronJob = new Model_CronJob();
		
		//
		// save this cron in cronjob_history table
		//
		$cronjob = $modelCronJob->getIdByName('Reminder_Contractor_Booking_Confirmation_One_Day');
		$cronjonID = $cronjob['id'];
		
		$cronjobHistoryData = array(
		'cron_job_id' => $cronjonID,
		'run_time' => time()
		);
		$cronjobHistoryId = $modelCronjobHistory->insert($cronjobHistoryData);
		//************
        $modelBookingStatus = new Model_BookingStatus();
        

        $inProcess = $modelBookingStatus->getByStatusName('IN PROGRESS');
        $toDo = $modelBookingStatus->getByStatusName('TO DO');
		$toVisit = $modelBookingStatus->getByStatusName('TO VISIT');
		
        $select_bookings = $this->getAdapter()->select();
        $select_bookings->from(array('bok' => $this->_name));
        $select_bookings->where("bok.is_deleted = 0");
        $select_bookings->where("bok.pause_emails = 0");
        $select_bookings->where("bok.status_id = '{$inProcess['booking_status_id']}' OR bok.status_id = '{$toDo['booking_status_id']}' OR bok.status_id = '{$toVisit['booking_status_id']}'");
        $select_bookings->where("DATEDIFF(Date(bok.booking_start), Date(NOW()) ) = 1");
		$select_bookings->joinInner(array('csb' => 'contractor_service_booking'), 'bok.booking_id = csb.booking_id',array('contractor_id'));
        $select_bookings->joinInner(array('usr' => 'user'), 'csb.contractor_id = usr.user_id',array('contractor_display_name'=> 'display_name','contractor_email1'=> 'email1', 'contractor_email2'=> 'email2', 'contractor_email3'=> 'email3'));
        $select_bookings->joinInner(array('cret_by_usr' => 'user'), 'bok.created_by = cret_by_usr.user_id',array('created_by'=> 'username'));
        $select_bookings->joinInner(array('cst' => 'customer'), 'bok.customer_id = cst.customer_id');
		$select_bookings->joinInner(array('bok_sts' => 'booking_status'), 'bok.status_id = bok_sts.booking_status_id',array('status_name'=>'name'));
       
		$select_bookings->group(array('booking_id','contractor_id'));
		//$sql = $select_bookings->__toString();
		//echo "$sql\n";
		$contractor_bookings = $this->getAdapter()->fetchAll($select_bookings);
		//$this->prepareAndSendReminderBookingEmailToContractor($contractor_bookings);
		
		 $modelUser = new Model_User();
       // $modelBooking = new Model_Booking();
        $modelCustomer = new Model_Customer();
        $modelBookingAddress = new Model_BookingAddress();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();

		$post_arr = array();
		
		foreach ($contractor_bookings as $booking) {
		
        $template_params = array(
			'{technician_display_name}' => implode(',', $booking['contractor_display_name']),
			'{booking_num}' => $booking['booking_num'],
			'{booking_status}' => $booking['status_name'],
			'{booking_start}' => date("F j, Y, g:i a", strtotime($booking['booking_start'])),
			'{booking_address}' => get_line_address($modelBookingAddress->getByBookingId($booking['booking_id'])),
			'{customer_name}' => (isset($booking['first_name']) && $booking['first_name'] ? ucwords($booking['first_name']) : '').(isset($booking['last_name']) && $booking['last_name'] ? ' ' . ucwords($booking['last_name']) : ''),
			'{property}' => $booking['property_type'],
			'{total_with_tax}' => number_format($booking['qoute'], 2),
			'{customer_contacts}' => nl2br($modelCustomer->getCustomerContacts($booking['customer_id'])),
			'{description}' => $booking['description'] ? $booking['description'] : '',
			'{service}' => nl2br($modelContractorServiceBooking->getBookingAsText($booking['booking_id'], true)),
			'{booking_created}' => date('d/m/Y', $booking['created']),
            '{sender_name}' => '--'
        );

        $modelEmailTemplate = new Model_EmailTemplate();
        $emailTemplate = $modelEmailTemplate->getEmailTemplate('send_contractor_booking_reminder_email_1_day', $template_params);
		
		
		$to = array();
		
			if ($booking['contractor_email1']&& filter_var($booking['contractor_email1'], FILTER_VALIDATE_EMAIL)) {
                $to[] = $booking['contractor_email1'];
            }
            if ($booking['contractor_email2']&& filter_var($booking['contractor_email2'], FILTER_VALIDATE_EMAIL)) {
                $to[] = $booking['contractor_email2'];
            }
            if ($booking['contractor_email3']&& filter_var($booking['contractor_email3'], FILTER_VALIDATE_EMAIL)) {
                $to[] = $booking['contractor_email3'];
            }
		
		
		
        $body = $emailTemplate['body'];
        //$subject = $emailTemplate['subject'];
        $subject = 'Your Booking Reminder ('.$booking['booking_num'].') on '. date("F j, Y, g:i a", strtotime($booking['booking_start'])).' )';
        $to = implode(',', $to);

            $params = array(
                'to' => $to,
                'body' => $body,
                'subject' => $subject,
				'companyId' => $booking['company_id']
            );

			
		$email_log = array('reference_id' => $booking['booking_id'],'cronjob_history_id' => $cronjobHistoryId, 'type' => 'booking');	
			if ($to) {
				
				try {
					EmailNotification::sendEmail($params, 'send_contractor_booking_reminder_email_1_day', $template_params, $email_log, $booking['company_id']);
					} 
				catch( Zend_Mail_Transport_Exception $e ) {
					
					}
				
			
			/*$post_arr[] = array('data' => $params, 'template_params' => $template_params, 'email_log'=>$email_log, 'cronjob_name'=>'Reminder_Contractor_Booking_Confirmation_One_Day');*/
			}
        }
		
		//MailingServer::sendEmailsDataToMailingServer($post_arr);
		//return;
		
    }
	
	
		
	public function cronJobReminderContractorBookingConfirmationTwoDay() {

        $modelBookingStatus = new Model_BookingStatus();
        

        $inProcess = $modelBookingStatus->getByStatusName('IN PROGRESS');
        $toDo = $modelBookingStatus->getByStatusName('TO DO');
		$toVisit = $modelBookingStatus->getByStatusName('TO VISIT');
		
        $select_bookings = $this->getAdapter()->select();
        $select_bookings->from(array('bok' => $this->_name));
        $select_bookings->where("bok.is_deleted = 0");
        $select_bookings->where("bok.pause_emails = 0");
        $select_bookings->where("bok.status_id = '{$inProcess['booking_status_id']}' OR bok.status_id = '{$toDo['booking_status_id']}' OR bok.status_id = '{$toVisit['booking_status_id']}'");
        $select_bookings->where("DATEDIFF(Date(bok.booking_start), Date(NOW()) ) = 2");
		$select_bookings->joinInner(array('csb' => 'contractor_service_booking'), 'bok.booking_id = csb.booking_id',array('contractor_id'));
        $select_bookings->joinInner(array('usr' => 'user'), 'csb.contractor_id = usr.user_id',array('contractor_display_name'=> 'display_name','contractor_email1'=> 'email1', 'contractor_email2'=> 'email2', 'contractor_email3'=> 'email3'));
        $select_bookings->joinInner(array('cret_by_usr' => 'user'), 'bok.created_by = cret_by_usr.user_id',array('created_by'=> 'username'));
        $select_bookings->joinInner(array('cst' => 'customer'), 'bok.customer_id = cst.customer_id');
		$select_bookings->joinInner(array('bok_sts' => 'booking_status'), 'bok.status_id = bok_sts.booking_status_id',array('status_name'=>'name'));
       
		$select_bookings->group(array('booking_id','contractor_id'));
		
		$contractor_bookings = $this->getAdapter()->fetchAll($select_bookings);
		//$this->prepareAndSendReminderBookingEmailToContractor($contractor_bookings);
		
		$modelUser = new Model_User();
       // $modelBooking = new Model_Booking();
        $modelCustomer = new Model_Customer();
        $modelBookingAddress = new Model_BookingAddress();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();

		foreach ($contractor_bookings as $booking) {
		 // Create pdf
        $viewParam = $this->getBookingViewParam($booking['booking_id'], true);
        $view = new Zend_View();
        $view->setScriptPath(APPLICATION_PATH . '/modules/booking/views/scripts/index');
        $view->booking = $viewParam['booking'];
        $view->bookingServices = $viewParam['bookingServices'];
        $view->thisBookingServices = $viewParam['thisBookingServices'];
        $view->priceArray = $viewParam['priceArray'];
        $bodyBooking = $view->render('booking.phtml');

        $template_params = array(
			'{technician_display_name}' => implode(',', $booking['contractor_display_name']),
			'{booking_num}' => $booking['booking_num'],
			'{booking_status}' => $booking['status_name'],
			'{booking_start}' => date("F j, Y, g:i a", strtotime($booking['booking_start'])),
			'{booking_address}' => get_line_address($modelBookingAddress->getByBookingId($booking['booking_id'])),
			'{customer_name}' => (isset($booking['first_name']) && $booking['first_name'] ? ucwords($booking['first_name']) : '').(isset($booking['last_name']) && $booking['last_name'] ? ' ' . ucwords($booking['last_name']) : ''),
			'{property}' => $booking['property_type'],
			'{total_with_tax}' => number_format($booking['qoute'], 2),
			'{customer_contacts}' => nl2br($modelCustomer->getCustomerContacts($booking['customer_id'])),
			'{description}' => $booking['description'] ? $booking['description'] : '',
			'{service}' => nl2br($modelContractorServiceBooking->getBookingAsText($booking['booking_id'], true)),
			'{booking_created}' => date('d/m/Y', $booking['created']),
			'{sender_name}' => '--'
            
        );

        $modelEmailTemplate = new Model_EmailTemplate();
        $emailTemplate = $modelEmailTemplate->getEmailTemplate('send_contractor_booking_reminder_email_2_days', $template_params);
		
		
		$to = array();
		
            if ($booking['contractor_email1']&& filter_var($booking['contractor_email1'], FILTER_VALIDATE_EMAIL)) {
                $to[] = $booking['contractor_email1'];
            }
            if ($booking['contractor_email2']&& filter_var($booking['contractor_email2'], FILTER_VALIDATE_EMAIL)) {
                $to[] = $booking['contractor_email2'];
            }
            if ($booking['contractor_email3']&& filter_var($booking['contractor_email3'], FILTER_VALIDATE_EMAIL)) {
                $to[] = $booking['contractor_email3'];
            }
		
		
		
        $body = $emailTemplate['body'];
        //$subject = $emailTemplate['subject'];
        $subject = 'Your Booking Reminder ('.$booking['booking_num'].') on '. date("F j, Y, g:i a", strtotime($booking['booking_start'])).' )';
        $to = implode(',', $to);

            $params = array(
                'to' => $to,
                'body' => $body,
                'subject' => $subject,
            );

            $error_mesages = array();
			
                    $pdfPath = createPdfPath();
                    $destination = $pdfPath['fullDir'] . $viewParam['booking']['booking_num'] . '.pdf';
                    wkhtmltopdf($bodyBooking, $destination);
                    $params['attachment'] = $destination;
            

                
				 // Send Email
                 
                try {
					EmailNotification::sendEmail($params, '', array(), array('reference_id' => $booking['booking_id'], 'type' => 'booking'));
					$sent = 'done';
					} 
				catch( Zend_Mail_Transport_Exception $e ) {
					$sent = 'error';
					}
		}
		
		
    }
	
	public function prepareAndSendReminderBookingEmailToContractor($bookings){
		
		 $modelUser = new Model_User();
       // $modelBooking = new Model_Booking();
        $modelCustomer = new Model_Customer();
        $modelBookingAddress = new Model_BookingAddress();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();

		foreach ($bookings as $booking) {
		 // Create pdf
        $viewParam = $this->getBookingViewParam($booking['booking_id'], true);
        $view = new Zend_View();
        $view->setScriptPath(APPLICATION_PATH . '/modules/booking/views/scripts/index');
        $view->booking = $viewParam['booking'];
        $view->bookingServices = $viewParam['bookingServices'];
        $view->thisBookingServices = $viewParam['thisBookingServices'];
        $view->priceArray = $viewParam['priceArray'];
        $bodyBooking = $view->render('booking.phtml');

        $template_params = array(
            //booking
			//'[subject]' => 'New Booking ('.$booking['booking_num'].') on'. Date($booking['booking_start']).' )', 
            '{booking_num}' => $booking['booking_num'],
            '{total_without_tax}' => number_format($booking['sub_total'], 2),
            '{gst_tax}' => number_format($booking['gst'], 2),
            '{total_with_tax}' => number_format($booking['qoute'], 2),
            '{description}' => $booking['description'] ? $booking['description'] : '',
            '{booking_created}' => date('d/m/Y', $booking['created']),
            '{booking_created_by}' => ucwords($booking['created_by']),
            '{booking_start}' => date("F j, Y, g:i a", strtotime($booking['booking_start'])),
            '{booking_address}' => get_line_address($modelBookingAddress->getByBookingId($booking['booking_id'])),
            '{service}' => nl2br($modelContractorServiceBooking->getBookingAsText($booking['booking_id'], true)),
            '{property}' => $booking['property_type'],
            '{booking_view}' => $bodyBooking,
            //customer
            '{customer_name}' => (isset($booking['first_name']) && $booking['first_name'] ? ucwords($booking['first_name']) : '').(isset($booking['last_name']) && $booking['last_name'] ? ' ' . ucwords($booking['last_name']) : ''),
            '{customer_first_name}' => isset($booking['first_name']) && $booking['first_name'] ? ucwords($booking['first_name']) : '',
            '{customer_last_name}' => isset($booking['last_name']) && $booking['last_name'] ? ' ' . ucwords($booking['last_name']) : '',
            '{customer_contacts}' => nl2br($modelCustomer->getCustomerContacts($booking['customer_id'])),
            '{booking_status}' => $booking['status_name'],
            '{technician_display_name}' => implode(',', $booking['contractor_display_name'])
        );

        $modelEmailTemplate = new Model_EmailTemplate();
        $emailTemplate = $modelEmailTemplate->getEmailTemplate('send_booking_as_email_to_contractor', $template_params);
		
		
		$to = array();
		
            if ($booking['contractor_email1']&& filter_var($booking['contractor_email1'], FILTER_VALIDATE_EMAIL)) {
                $to[] = $booking['contractor_email1'];
            }
            if ($booking['contractor_email2']&& filter_var($booking['contractor_email2'], FILTER_VALIDATE_EMAIL)) {
                $to[] = $booking['contractor_email2'];
            }
            if ($booking['contractor_email3']&& filter_var($booking['contractor_email3'], FILTER_VALIDATE_EMAIL)) {
                $to[] = $booking['contractor_email3'];
            }
		
		
		
        $body = $emailTemplate['body'];
        //$subject = $emailTemplate['subject'];
        $subject = 'Your Booking Reminder ('.$booking['booking_num'].') on '. date("F j, Y, g:i a", strtotime($booking['booking_start'])).' )';
        $to = implode(',', $to);

            $params = array(
                'to' => $to,
                'body' => $body,
                'subject' => $subject,
            );

            $error_mesages = array();
			
                    $pdfPath = createPdfPath();
                    $destination = $pdfPath['fullDir'] . $viewParam['booking']['booking_num'] . '.pdf';
                    wkhtmltopdf($bodyBooking, $destination);
                    $params['attachment'] = $destination;
            

                
				 // Send Email
                 
                try {
					EmailNotification::sendEmail($params, '', array(), array('reference_id' => $booking['booking_id'], 'type' => 'booking'));
					$sent = 'done';
					} 
				catch( Zend_Mail_Transport_Exception $e ) {
					$sent = 'error';
					}
		}
        
	
	}
	
	
	public function prepareAndSendReminderBookingEmailToCustomer($bookings){
		$modelUser = new Model_User();
		$modelCustomer = new Model_Customer();
		$modelBookingAddress = new Model_BookingAddress();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
		
		
		foreach ($bookings as $booking) {

            $to = array();
			
            if ($booking['customer_email1']&& filter_var($booking['customer_email1'], FILTER_VALIDATE_EMAIL)) {
                $to[] = $booking['customer_email1'];
            }
            if ($booking['customer_email2']&& filter_var($booking['customer_email2'], FILTER_VALIDATE_EMAIL)) {
                $to[] = $booking['customer_email2'];
            }
            if ($booking['customer_email3']&& filter_var($booking['customer_email3'], FILTER_VALIDATE_EMAIL)) {
                $to[] = $booking['customer_email3'];
            }
			
			// booking view Params
			$viewParam = $this->getBookingViewParam($booking['booking_id'], true);
			$view = new Zend_View();
			$view->setScriptPath(APPLICATION_PATH . '/modules/booking/views/scripts/index');
			$view->booking = $viewParam['booking'];
			$view->bookingServices = $viewParam['bookingServices'];
			$view->thisBookingServices = $viewParam['thisBookingServices'];
			$view->priceArray = $viewParam['priceArray'];

			$bodyBooking = $view->render('booking-customer.phtml');
			
				
			$template_params = array(
            //booking
            '{booking_num}' => $booking['booking_num'],
            '{total_without_tax}' => number_format($booking['sub_total'], 2),
            '{gst_tax}' => number_format($booking['gst'], 2),
            '{total_with_tax}' => number_format($booking['qoute'], 2),
            '{description}' => $booking['description'] ? $booking['description'] : '',
            '{booking_created}' => date('d/m/Y', $booking['created']),
            '{booking_created_by}' => ucwords($booking['first_name']),
            '{booking_start}' => date("F j, Y, g:i a", strtotime($booking['booking_start'])),
            '{booking_address}' => get_line_address($modelBookingAddress->getByBookingId($booking['booking_id'])),
            '{service}' => nl2br($modelContractorServiceBooking->getBookingAsText($booking['booking_id'], true)),
            '{property}' => $booking['property_type'],
            '{booking_view}' => $bodyBooking,
            //customer
            '{customer_name}' => $booking['first_name'],
            '{customer_first_name}' => isset($booking['first_name']) && $booking['first_name'] ? ucwords($booking['first_name']) : '',
            '{customer_last_name}' => isset($booking['last_name']) && $booking['last_name'] ? ' ' . ucwords($booking['last_name']) : '',
            '{customer_contacts}' => nl2br($modelCustomer->getCustomerContacts($booking['customer_id'])),
        );
			
			//we got customer info to use company_id in preparing email template
			$customer = $modelCustomer->getById($booking['customer_id']);
		
			
            $to = implode(',', $to);

            $modelEmailTemplate = new Model_EmailTemplate();
				$emailTemplate = $modelEmailTemplate->getEmailTemplate('send_booking_as_email', $template_params, $customer['company_id']);
				

            $params = array(
                'to' => $to,
                'body' => $emailTemplate['body'],
                'subject' => $emailTemplate['subject'],
            );
             // Create pdf
            $pdfPath = createPdfPath();
            $destination = $pdfPath['fullDir'] . $viewParam['booking']['booking_num'] . '.pdf';
            wkhtmltopdf($bodyBooking, $destination);
            $params['attachment'] = $destination;
          
                // Send Email
                 
                try {
					EmailNotification::sendEmail($params, '', array(), array('reference_id' => $booking['booking_id'], 'type' => 'booking'));
					$sent = 'done';
					} 
				catch( Zend_Mail_Transport_Exception $e ) {
					$sent = 'error';
					}
				echo $sent;
        }
	}
	
	
	// urgent booking === inprocess, awaiting_update, awaiting_approval, has complaint
	public function getUrgentBookingByContractorId($contractorId) {
        $contractorId = (int) $contractorId;
		
		////load models
		$modelBookingStatus = new Model_BookingStatus();
		
		/// get statuses info
		$inProcess = $modelBookingStatus->getByStatusName('IN PROGRESS');
		$completed = $modelBookingStatus->getByStatusName('COMPLETED');
		$failed = $modelBookingStatus->getByStatusName('FAILED');
		$quoted = $modelBookingStatus->getByStatusName('QUOTED');
		$toDo = $modelBookingStatus->getByStatusName('TO DO');
		$onHold = $modelBookingStatus->getByStatusName('ON HOLD');
		$cancelled = $modelBookingStatus->getByStatusName('CANCELLED');
		$awaitingUpdate = $modelBookingStatus->getByStatusName('AWAITING UPDATE');
		$tentative = $modelBookingStatus->getByStatusName('TENTATIVE');
		$toVisit = $modelBookingStatus->getByStatusName('TO VISIT');
		
		/// get id of each status
		$inProcessId = $inProcess['booking_status_id'];
		$completedId = $completed['booking_status_id'];
		$failedId = $failed['booking_status_id'];
		$quotedId = $quoted['booking_status_id'];
		$toDoId = $toDo['booking_status_id'];
		$onHoldId = $onHold['booking_status_id'];
		$cancelledId = $cancelled['booking_status_id'];
		$awaitingUpdateId = $awaitingUpdate['booking_status_id'];
		$tentativeId = $tentative['booking_status_id'];
		$toVisitId = $toVisit['booking_status_id'];
        
        
        $select = $this->getAdapter()->select();
        $select->from(array('bok' => $this->_name));
		$select->joinInner(array('csb' => 'contractor_service_booking'), 'bok.booking_id = csb.booking_id');
        $select->joinLeft(array('com' => 'complaint'),'com.booking_id= bok.booking_id',array('complaint_id','complaint_num','complaint_status'=>'com.complaint_status'));
		$select->joinInner(array('ct' => 'complaint_type'),'com.complaint_type_id= ct.complaint_type_id',array('name'));
		$select->where("bok.is_deleted = 0");
		$select->where("bok.status_id = '{$inProcessId}' OR bok.status_id = '{$awaitingUpdateId}' OR bok.is_change = 1 OR com.complaint_status = 'open'");
       
		
		
		
		if($contractorId != 0){
			$select->where("csb.contractor_id = '{$contractorId}'");
			$select->group('bok.booking_id');
			$result = $this->getAdapter()->fetchAll($select);
		}
		else{
			$result = array();
		}
		
        return $result;
    }
	
	public function getIdByBookingNum($booking_num){
		$select = $this->getAdapter()->select();
        $select->from($this->_name);
		$select->where("booking_num = '{$booking_num}'");
		$result = $this->getAdapter()->fetchRow($select);
		return $result;
	}
	/**
     * 23/06/2015 D.A
     * get table row according to the assigned num
     *
     * @param int $num
     * @return array
     */
    public function getByNum($num) {
		
       
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_num = '{$num}'");

        return $this->getAdapter()->fetchRow($select);
		
    }
	/**
     * 23/06/2015 D.A
     * get table row according to the assigned num and company id
     *
     * @param int $num
     * @return array
     */
    public function getByNumgetByNumAndCompanyId($num,$companyId) {
		$companyId = (int) $companyId;      
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_num = '{$num}'");
		$select->where("company_id = '{$companyId}'");

        return $this->getAdapter()->fetchRow($select);
    }
	
	public function getCountRejectBooking() {
        $bookingIdArray=array();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $rejectBookingIds = $modelContractorServiceBooking->getRejectBookingsIds();

        foreach($rejectBookingIds as $rejectBookingId){
            $bookingIdArray[]=$rejectBookingId['booking_id'];
        }

        $filters['booking_ids']=$bookingIdArray;
        $rejectBooking = $this->getAll($filters);
        return count($rejectBooking);
    }
	
	
	public function getCountAwaitingAcceptBookingForContractor($id) {

        $filters = array('acceptance' => 'not_accepted_or_rejected');
		$filters['booking_not_started_yet'] ='yes';
		$filters['convert_status'] ='booking';
		$filters['contractor_id']= $id;
        $awaitingacceptBooking = $this->getAll($filters); 

        return count($awaitingacceptBooking);
    }
	
	public function getCountAwaitingupdateBookingForContractor($id) {

        $modelBookingStatus = new Model_BookingStatus();
        $awaitingUpdateStatus = $modelBookingStatus->getByStatusName('AWAITING UPDATE');

        $awaitingupdateBooking = $this->getAll(array('status' => $awaitingUpdateStatus['booking_status_id'],'contractor_id'=>$id));

        return count($awaitingupdateBooking);
    }
	
	public function getCountUnapprovedBookingForContractor($id) {

        $unapprovedBooking = $this->getAll(array('is_change' => 1,'contractor_id'=>$id));

        return count($unapprovedBooking);
    }
	
	public function getCountRejectBookingForContractor($id) {
        $bookingIdArray=array();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $rejectBookingIds = $modelContractorServiceBooking->getRejectBookingsIds();

        foreach($rejectBookingIds as $rejectBookingId){
            $bookingIdArray[]=$rejectBookingId['booking_id'];
        }

        $filters['booking_ids']=$bookingIdArray;
        $filters['contractor_id']=$id;
        $rejectBooking = $this->getAll($filters);
        return count($rejectBooking);
    }
	
	public function getCountInProcessBookingForContractor($dd) {
        $modelBookingStatus = new Model_BookingStatus();

        $inProcessBooking = $this->getAll(array('status' => 1,'contractor_id'=>$dd));

        return count($inProcessBooking);
    }
	
	
	public function getCountCompletedBookingForContractor($dd) {
        $modelBookingStatus = new Model_BookingStatus();
		$Completedstatus = $modelBookingStatus->getByStatusName('COMPLETED');
        $CompletedBooking = $this->getAll(array('status' => $Completedstatus['booking_status_id'],'contractor_id'=>$dd));

        return count($CompletedBooking);
    }
	public function getCountFailedBookingForContractor($dd) {
        $modelBookingStatus = new Model_BookingStatus();
		$Faildstatus = $modelBookingStatus->getByStatusName('FAILED');
        $FailedBooking = $this->getAll(array('status' => $Faildstatus['booking_status_id'],'contractor_id'=>$dd));

        return count($FailedBooking);
    }
	
	public function getCountOnHoledBookingForContractor($dd) {
        $modelBookingStatus = new Model_BookingStatus();
		$OnHoledstatus = $modelBookingStatus->getByStatusName('ON HOLD');
        $OnHoldBooking = $this->getAll(array('status' =>$OnHoledstatus['booking_status_id'],'contractor_id'=>$dd));

        return count($OnHoldBooking);
    }
	public function getCounTentitiveBookingForContractor($dd) {
        $modelBookingStatus = new Model_BookingStatus();
		$tentativestatus = $modelBookingStatus->getByStatusName('TENTATIVE');
        $tentativeBooking = $this->getAll(array('status' =>$tentativestatus['booking_status_id'],'contractor_id'=>$dd));

        return count($tentativeBooking);
    } 
	
	public function getCounToDoBookingForContractor($dd) {
        $modelBookingStatus = new Model_BookingStatus();
		$todostatus = $modelBookingStatus->getByStatusName('TO DO');
        $todoBooking = $this->getAll(array('status' =>$todostatus['booking_status_id'],'contractor_id'=>$dd));

        return count($todoBooking);
    }
	public function getCounToVisitBookingForContractor($dd) {
        $modelBookingStatus = new Model_BookingStatus();
		$toVisitstatus = $modelBookingStatus->getByStatusName('TO VISIT');
        $tovisitBooking = $this->getAll(array('status' =>$toVisitstatus['booking_status_id'],'contractor_id'=>$dd));

        return count($tovisitBooking);
    }
	
	
	
	
}