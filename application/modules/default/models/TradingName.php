<?php

class Model_TradingName extends Zend_Db_Table_Abstract {

    protected $_name = 'trading_name';

    public function insert(array $data) {

        $id = parent::insert($data);

        return $id;
    }

    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from(array('tn' => $this->_name));

        $select->order($order);


        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("trading_name_id = '{$id}'");
        return $this->getAdapter()->fetchRow($select);
    }

    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "trading_name_id = '{$id}'");
    }


     public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("trading_name_id = '{$id}'");
    }
	
	public function getByCompanyId($company_id) {
        $company_id = (int) $company_id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("company_id = '{$company_id}'");

        return $this->getAdapter()->fetchAll($select);
    }
	
	/**
     * 23/06/2015 D.A
     * get deafualt trading namesaccording to the assigned company Id
     *
     * @param int $company_Id
     * @return array
     */
	public function getDefualtTradingName($company_Id) {
        $company_Id = (int) $company_Id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("is_default = '1'");
		$select->where("company_id = '{$company_Id}'");
		
        return $this->getAdapter()->fetchRow($select);
    }
	
	public function getTradingNameByWebsite($website_url) {
        
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("website_url LIKE ?" , $website_url."%");
		
        return $this->getAdapter()->fetchRow($select);
    }
	

}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

