<?php

class Model_UserLogin extends Zend_Db_Table_Abstract {

    protected $_name = 'user_login';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order($order);

        $where = array();

       

        if ($filters) {
            if (!empty($filters['keywords'])) {
                $keywords = $this->getAdapter()->quote('%' . $filters['keywords'] . '%');
                $where[] = ("bsb LIKE {$keywords}");
            }
           
            if ($where) {
                $select->where(implode(' AND ', $where));
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "user_login_id = '{$id}'");
    }
    
    

    /**
     * delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("user_login_id = '{$id}'");
    }
    public function deleteByUserId($id) {
        $id = (int) $id;
        return parent::delete("user_id = '{$id}'");
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("user_login_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function getByUserId($user_id) {
        $user_id = (int) $user_id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("user_id = '{$user_id}'");

        return $this->getAdapter()->fetchRow($select);
    }
    public function getByUserIdAndIP($user_id , $user_ip) {
        $user_id = (int) $user_id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("user_id = '{$user_id}'");
        $select->where("user_ip = '{$user_ip}'");

        return $this->getAdapter()->fetchRow($select);
    }
    
    

}