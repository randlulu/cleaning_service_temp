<?php

class Model_BookingAttendance extends Zend_Db_Table_Abstract {

    protected $_name = 'booking_attendance';
    private $modelContractorInfo;

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array() , $order = null, &$pager = null, $limit = 0, $perPage = 0, $currentPage = 0) {
        $select = $this->getAdapter()->select();
        $select->from(array('ba' => $this->_name) ,array('ba.booking_id','ba.contractor_id'));
        $select->joinInner(array('bok' => 'booking'), 'bok.booking_id = ba.booking_id' , 'bok.booking_num');
        $select->joinInner(array('u' => 'user'), 'u.user_id = ba.contractor_id' , 'username');
		
		if ($filters) {
		    $today = date("Y-m-d");
            if (!empty($filters['contractor'])) {
                $select->where("ba.contractor_id = {$filters['contractor']}");
            }
			
			if (!empty($filters['company_id'])) {
                $select->where("bok.company_id = {$filters['company_id']}");
            }
			
			if (!empty($filters['time'])) {
			    if($filters['time'] == 'current'){
				 $start = $this->getAdapter()->quote(trim($today) . '%');
				 $select->where("bok.booking_start LIKE {$start} "); 
				}else if($filters['time'] == 'previous'){			
				 $select->where("bok.booking_start < '{$today}' "); 
				}                
            }
			
			
        }
		
		
		if ($order) {
            $select->order('ba.'.$order);
        }
		
		
		if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        } elseif ($limit) {
            
			$select->limit($limit);
        } elseif ($perPage && $currentPage) {
            $select->limit($perPage, ($currentPage - 1) * $perPage);
        }

		$select->group('ba.booking_id'); 
		
		//echo $select->__toString();
		//exit;
        return $this->getAdapter()->fetchAll($select);
    }

	
	public function getByBookingId($booking_id){
	  
	    $booking_id = (int) $booking_id;
	    $select = $this->getAdapter()->select();
        $select->from(array('ba' => $this->_name));

		$select->where("ba.booking_id = {$booking_id}"); 
        return $this->getAdapter()->fetchAll($select);
	
	}

    /**
     * update table row according to the assigned $id and submited $data
     * 
     * @param $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "booking_attendance_id = {$id}");
    }

    /**
     * delete table row according to the assigned $id
     * 
     * @param  int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("booking_attendance_id = '{$id}'");
    }

    /**
     * get table row according to the assigned $id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from(array('u' => $this->_name));
        $select->joinInner(array('ci' => 'city'), 'ci.city_id = u.city_id', array('ci.city_name'));
        $select->joinInner(array('co' => 'country'), 'ci.country_id = co.country_id', array('co.country_name'));
        $select->joinInner(array('ar' => 'auth_role'), 'ar.role_id = u.role_id', array('ar.role_name'));
        $select->where("user_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }
	
	
	public function getLastCheckInByBookingIdAndContractorId($bookingId,$contractorId){
	  
	    $select = $this->getAdapter()->select();
        $select->from($this->_name, array('MAX(booking_attendance_id) as last_booking_attendance_id'));
        $select->where("booking_id  = {$bookingId}");
        $select->where("contractor_id  = {$contractorId}");
        $select->where("check_out_time  IS NULL ");

	    $result  = $this->getAdapter()->fetchRow($select);
		return $result['last_booking_attendance_id'];
	
	}
	
	
    public function insert(array $data) {

        $id = parent::insert($data);

        return $id;
    }
	//Rand start : this function is to get attendance based on booking and contractor
        public function getByBookingIdAndContractorId($booking_id,$contractor_id){
	  
	    $booking_id = (int) $booking_id;
	    $select = $this->getAdapter()->select();
        $select->from(array('ba' => $this->_name));

		$select->where("ba.booking_id = {$booking_id}"); 
		$select->where("ba.contractor_id = {$contractor_id}"); 
        return $this->getAdapter()->fetchAll($select);
	
	}
    // Rand : get last check record by booking and contractor
	
public function getLastCheckByBookingIdAndContractorId($bookingId,$contractorId){
	  
	    $select = $this->getAdapter()->select();
        $select->from($this->_name, array('MAX(booking_attendance_id) as last_booking_attendance_id'));
        $select->where("booking_id  = {$bookingId}");
        $select->where("contractor_id  = {$contractorId}");

	    $result  = $this->getAdapter()->fetchRow($select);
		return $result['last_booking_attendance_id'];
	
	}
	public function getByBookingAttendanceID($bookingAttendanceID){
	  
	    $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_attendance_id  = {$bookingAttendanceID}");
	    $result  = $this->getAdapter()->fetchRow($select);
		return $result;
	
	}

}