<?php

class Model_UnavailableEvent extends Zend_Db_Table_Abstract {

    protected $_name = 'unavailable_event';
	
	public function getAll($filters = array()){
      
	  
	    $select = $this->getAdapter()->select();
        $select->from($this->_name);
        if($filters){
		  if($filters['contractor_id']){
		    $contractor_id = (int) $filters['contractor_id'];
		    $select->where("contractor_id = '{$contractor_id}'");
		  }
		}

        return $this->getAdapter()->fetchAll($select);
    	
	}
	
	
	public function getByEventId($id){

	 $select = $this->getAdapter()->select();
     $select->from($this->_name);
	 $select->where("event_id = '{$id}'");
	 return $this->getAdapter()->fetchRow($select);
	}
	
	
	public function getById($id){
	  
	 $select = $this->getAdapter()->select();
     $select->from($this->_name);
	 $select->where("unavailable_event_id = '{$id}'");
	 return $this->getAdapter()->fetchRow($select);
	}
	
	
	public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("unavailable_event_id= '{$id}'");
    }


}

