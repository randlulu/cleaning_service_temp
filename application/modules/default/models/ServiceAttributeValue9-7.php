<?php

class Model_ServiceAttributeValue extends Zend_Db_Table_Abstract {

    protected $_name = 'service_attribute_value';
    
    private $modelAttribute;
    private $modelAttributeType;
    private $modelAttributeListValue;

    /**
     * update table row according to the assigned $id on the submited $data
     * 
     * @param int $id
     * @param array $order
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        $success = parent::update($data, "service_attribute_value_id= '{$id}'");

        /**
         * get all record and insert it in log
         */
        $modelServiceAttributeValueLog = new Model_ServiceAttributeValueLog();
        $modelServiceAttributeValueLog->addServiceAttributeValueLog($id);

        return $success;
    }

    public function insert(array $data) {

        $id = parent::insert($data);

        /**
         * get all record and insert it in log
         */
        $modelServiceAttributeValueLog = new Model_ServiceAttributeValueLog();
        $modelServiceAttributeValueLog->addServiceAttributeValueLog($id);

        return $id;
    }

    /**
     * delete table row according to the assigned $id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("service_attribute_value_id= '{$id}'");
    }

    /**
     * get table row according to the assigned $id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("service_attribute_value_id= '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table rows according to the assigned $id
     * 
     * @param int $id
     * @return array 
     */
    public function getByBookingId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from(array('sav' => $this->_name));
        $select->joinInner(array('sa' => 'service_attribute'), 'sav.service_attribute_id=sa.service_attribute_id', array('sa.service_id,sa.attribute_id'));
        $select->where("booking_id= '{$id}'");

        return $this->getAdapter()->fetchAll($select);
    }

    public function getByBookingIdWithoutExtraField($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from(array('sav' => $this->_name));
        $select->joinInner(array('sa' => 'service_attribute'), 'sav.service_attribute_id=sa.service_attribute_id', '');
        $select->where("booking_id = '{$id}'");

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * get table rows according to the assigned bookingId ,serviceAttributeId and clone 
     * 
     * @param int $bookingId
     * @param int $serviceAttributeId
     * @param  int $clone
     * @return array 
     */
    public function getByBookingIdAndServiceAttributeIdAndClone($bookingId, $serviceAttributeId, $clone) {
        $bookingId = (int) $bookingId;
        $serviceAttributeId = (int) $serviceAttributeId;
        $clone = (int) $clone;

        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_id = '{$bookingId}' AND service_attribute_id = '{$serviceAttributeId}' AND clone = '{$clone}'");

        return $this->getAdapter()->fetchRow($select);
    }
	////////////By Islam
	public function getByBookingIdAndServiceAttributeId($bookingId, $serviceAttributeId) {
        $bookingId = (int) $bookingId;
        $serviceAttributeId = (int) $serviceAttributeId;

        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_id = '{$bookingId}' AND service_attribute_id = '{$serviceAttributeId}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * add attribute row by the assigned bookingId ,serviceAttributeId and clone 
     * 
     * @param int $bookingId
     * @param int $serviceId
     * @param  int $clone 
     */
    public function addAttributeByBookingIdAndServiceIdAndClone($bookingId, $serviceId, $clone) {
        $bookingId = (int) $bookingId;
        $serviceId = (int) $serviceId;
        $clone = (int) $clone;

        $request = Zend_Controller_Front::getInstance()->getRequest();

        $serviceAttributeObj = new Model_ServiceAttribute();

        $serviceAttribute = $serviceAttributeObj->getAttributeByServiceId($serviceId);

        if ($serviceAttribute) {
            if (!$this->modelAttribute) {
                $this->modelAttribute = new Model_Attributes();
            }
            if (!$this->modelAttributeType) {
                $this->modelAttributeType = new Model_AttributeType();
            }
            if (!$this->modelAttributeListValue) {
                $this->modelAttributeListValue = new Model_AttributeListValue();
            }

            foreach ($serviceAttribute as $attribute) {

                $value = $request->getParam('attribute_' . $serviceId . $attribute['attribute_id'] . ($clone ? '_' . $clone : ''));
                $value = !empty($value) ? $value : '';

                $oldServiceAttributeValue = $this->getByBookingIdAndServiceAttributeIdAndClone($bookingId, $attribute['service_attribute_id'], $clone);
                $is_serialize = 0;
                $delete_add_values = 0;

                if (!empty($value) && is_array($value)) {
                    $value = serialize($value);
                    $is_serialize = 1;
                } elseif ($oldServiceAttributeValue['is_serialized_array'] == 1) {

                    $deletevalues = unserialize($oldServiceAttributeValue['value']);
                    foreach ($deletevalues as &$delete_value) {
                        $attributeListValue = $this->modelAttributeListValue->getById($delete_value);
                        $delete_value = $attributeListValue['attribute_value'];
                    }
                    $delete_add_values = 1;
                }


                $data = array(
                    'service_attribute_id' => $attribute['service_attribute_id'],
                    'booking_id' => $bookingId,
                    'value' => $value,
                    'is_serialized_array' => $is_serialize,
                    'clone' => $clone
                );
                $attribute = $this->modelAttribute->getById($attribute['attribute_id']);
                $attributeType = $this->modelAttributeType->getById($attribute['attribute_type_id']);
                $newAttributeValue = $value;
                if ($oldServiceAttributeValue) {
                    $oldAttributeValue = $oldServiceAttributeValue['value'];
                }

                if ($attributeType['is_list'] == 1) {
                    if ($is_serialize == 1) {
                        $serializeValues = unserialize($value);
                        if ($serializeValues) {
                            foreach ($serializeValues as &$serializeValue) {

                                $attributeListValue = $this->modelAttributeListValue->getById($serializeValue);
                                $serializeValue = $attributeListValue['attribute_value'];
                            }
                        }
                        if ($oldServiceAttributeValue && $oldServiceAttributeValue['is_serialized_array'] == 1) {

                            $oldSerializeValues = unserialize($oldServiceAttributeValue['value']);
                            if ($oldSerializeValues) {
                                foreach ($oldSerializeValues as &$oldSerializeValue) {

                                    $oldAttributeListValue = $this->modelAttributeListValue->getById($oldSerializeValue);
                                    $oldSerializeValue = $oldAttributeListValue['attribute_value'];
                                }
                            }
                            $addvalues = array_diff($serializeValues, $oldSerializeValues);
                            $deletevalues = array_diff($oldSerializeValues, $serializeValues);
                        }
                        ////check if old value empty in serialize status
                        if (empty($oldServiceAttributeValue['value'])) {
                            $addvalues = $serializeValues;

                            $delete_add_values = 1;
                        }
                    } else {

                        $attributeListValue = $this->modelAttributeListValue->getById($value);
                        $newAttributeValue = $attributeListValue['attribute_value'];

                        if ($oldServiceAttributeValue) {
                            $attributeListValue = $this->modelAttributeListValue->getById($oldServiceAttributeValue['value']);
                            $oldAttributeValue = $attributeListValue['attribute_value'];
                        }
                    }
                }
                if ($oldServiceAttributeValue) {
                    // update attribute
                    $this->updateById($oldServiceAttributeValue['service_attribute_value_id'], $data);
                } else {
                    //add attribute
                    $this->insert($data);
                }
            }
        }
    }

    /**
     * delete table row according to the assigned bookingId ,serviceAttributeId and clone 
     * 
     * @param int $bookingId
     * @param int $serviceId
     * @param  int $clone
     */
    public function deleteByBookingIdServiceIdAndClone($bookingId, $serviceId, $clone) {
        $bookingId = (int) $bookingId;
        $clone = (int) $clone;
        $serviceId = (int) $serviceId;

        $serviceAttributeObj = new Model_ServiceAttribute();
        $serviceAttribute = $serviceAttributeObj->getAttributeByServiceId($serviceId);

        if ($serviceAttribute) {
            foreach ($serviceAttribute as $attribute) {
                $this->delete("service_attribute_id= '{$attribute['service_attribute_id']}' AND booking_id= '{$bookingId}' AND clone= '{$clone}'");
            }
        }
    }

}