<?php

class Model_IosSync extends Zend_Db_Table_Abstract {

    protected $_name = 'ios_sync';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
	 
	 
	
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order($order);

        if ($filters) {
            if (!empty($filters['booking_id'])) {
                $select->where("booking_id = {$filters['booking_id']}");
            }
            if (!empty($filters['ios_user_id'])) {
                $select->where("ios_user_id = {$filters['ios_user_id']}");
            }
            if (!empty($filters['is_sync'])) {
                if ('yes' == $filters['is_sync']) {
                    $select->where('is_sync = 1');
                } elseif ('no' == $filters['is_sync']) {
                    $select->where('is_sync = 0');
                }
            } 
			
			if (!empty($filters['mobile_changes_is_sync'])) {
                if ('yes' == $filters['mobile_changes_is_sync']) {
                    $select->where('mobile_changes_is_sync = 1');
                } elseif ('no' == $filters['mobile_changes_is_sync']) {
                    $select->where('mobile_changes_is_sync = 0');
                }
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table rows according to the assigned id and data
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "id = '{$id}'");
    }

    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("id = '{$id}'");
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function getByBookingIdAndIosUser($bookingId, $iosUserId) {
        $bookingId = (int) $bookingId;
        $iosUserId = (int) $iosUserId;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_id = '{$bookingId}' AND ios_user_id = '{$iosUserId}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function getSyncedBookingIds($iosUserId) {
        
        $filters = array();
        $filters['ios_user_id'] = $iosUserId;
        $filters['is_sync'] = 'yes';
        $iosSyncs = $this->getAll($filters);

        $syncedBookingId = array();
        foreach ($iosSyncs as $iosSync) {
            $syncedBookingId[$iosSync['booking_id']] = $iosSync['booking_id'];
        }

        return $syncedBookingId;
    }
	
	public function getMobileSyncedBookingIds($iosUserId) {
        
        $filters = array();
        $filters['ios_user_id'] = $iosUserId;
        $filters['mobile_changes_is_sync'] = 'yes';
        $iosSyncs = $this->getAll($filters);

        $syncedBookingId = array();
        foreach ($iosSyncs as $iosSync) {
            $syncedBookingId[$iosSync['booking_id']] = $iosSync['booking_id'];
        }

        return $syncedBookingId;
    }

    public function changeSyncBooking($bookingId) {
		
        $modelIosUser = new Model_IosUser();
		$filters['booking_id'] = $bookingId;
        $iosUsers = $modelIosUser->getAll($filters);
		
        $db_params = array();
        $db_params['is_sync'] = false;
        $db_params['mobile_changes_is_sync'] = false;
        $db_params['booking_id'] = $bookingId;

        if ($iosUsers) {
            foreach ($iosUsers as $iosUser) {

                $db_params['ios_user_id'] = $iosUser['id'];

                $syncedBooking = $this->getByBookingIdAndIosUser($bookingId, $iosUser['id']);
                if ($syncedBooking) {
                    $this->updateById($syncedBooking['id'], $db_params);
                } else {
                    $this->insert($db_params);
                }
            }
        }
    }

}
