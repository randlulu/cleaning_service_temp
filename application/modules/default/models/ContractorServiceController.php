<?php

class Settings_ContractorServiceController extends Zend_Controller_Action {

    private $request;
    private $router;
    private $contractor_id;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
        $this->contractor_id = $this->request->getParam('contractor_id');

        $this->view->main_menu = 'settings';
        $this->view->sub_menu = 'settingsUser';

        //
        // get data list
        //
        $contractorInfoObj = new Model_ContractorInfo();
        $contractorInfo = $contractorInfoObj->getByContractorId($this->contractor_id);

        BreadCrumbs::setLevel(4, 'Contractor Services');
    }

    /**
     * Items list action
     */
    public function indexAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsContractorServiceList'));


        if (!CheckAuth::checkIfCanHandelAllCompany('user', $this->contractor_id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'contractor_service_id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());
        
        if($filters){
            foreach ($filters as &$filter){
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];


        //
        //get user email by id
        //
        $filters['contractor_id'] = $this->contractor_id;


        //
        // get data list
        //
        $contractorServiceObj = new Model_ContractorService();
        $this->view->data = $contractorServiceObj->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);

        //
        // set view params
        //
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
        $this->view->contractor_id = $this->contractor_id;
    }

    /**
     * Add new item action
     */
    public function addAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsContractorServiceAdd'));

        if (!CheckAuth::checkIfCanHandelAllCompany('user', $this->contractor_id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // get request parameters
        //
        $serviceIds = $this->request->getParam('service_id');
        $router = Zend_Controller_Front::getInstance()->getRouter();

        //
        // init action form
        //
		$filters['contractor_id'] = $this->contractor_id;
        $contractorServiceObj = new Model_ContractorService();
        $old_services = $contractorServiceObj->getAll($filters);
        $form = new Settings_Form_ContractorService(array('contractor_id' => $this->contractor_id , 'old_services'=>$old_services));

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $contractorServiceObj = new Model_ContractorService();
				 /********** Default city (form user info)*************IBM*/
                    $userObj = new Model_User();
                    $user = $userObj->getById($this->contractor_id);
                    $default_city_availability = $user['city_id'];            
                 /***********End***********/

                $success = false;
                foreach ($serviceIds as $serviceId) {
                    $data = array(
                        'service_id' => $serviceId,
                        'contractor_id' => $this->contractor_id
                    );

                    if (!$contractorServiceObj->getByContractorIdAndServiceId($this->contractor_id, $serviceId)) {
                        $success = $contractorServiceObj->insert($data);
						
					/****insert  city as default city to contractor service *IBM*/
                        $city_availability_data = array(
                            'city_id' => $default_city_availability,
                            'contractor_service_id' => $success
                        );
                        $contractorServiceAvailabilityObj = new Model_ContractorServiceAvailability();
                        $contractorServiceAvailabilityObj->insert($city_availability_data);
                     /***********End***********/
						
                    }
                }

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                }
                //$this->_redirect($this->router->assemble(array(), 'settingsUserEmailList'));
                echo 1;
                exit;
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('contractor-service/add_edit.phtml');
        exit;
    }

    public function deleteAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsContractorServiceDelete'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());
        if ($id) {
            $ids[] = $id;
        }

        $contractorServiceObj = new Model_ContractorService();
        foreach ($ids as $id) {
            if (CheckAuth::checkIfCanHandelAllCompany('contractor_service', $id)) {
                $contractorServiceObj->deleteById($id);
            }
        }
        $this->_redirect($this->router->assemble(array('contractor_id' => $this->contractor_id), 'settingsContractorServiceList'));
    }

	
	public function contractorServicesAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('settingsContractorServiceList'));


        if (!CheckAuth::checkIfCanHandelAllCompany('user', $this->contractor_id)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        //
        // get request parameters
        //
        $orderBy = $this->request->getParam('sort', 'contractor_service_id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        //$currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());
        
        if($filters){
            foreach ($filters as &$filter){
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // init pager and articles model object
      /*
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

*/
        //
        //get user email by id
        //
        $filters['contractor_id'] = $this->contractor_id;


        //
        // get data list
        //
        $contractorServiceObj = new Model_ContractorService();
        $this->view->data = $contractorServiceObj->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);

        //
        // set view params
        //
       // $this->view->currentPage = $currentPage;
       // $this->view->perPage = $pager->perPage;
       // $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
        $this->view->contractor_id = $this->contractor_id;
		  echo $this->view->render('contractor-service/contractorservices.phtml');
        exit;
    }
	
	
		public function editServiceCommissionAction(){
	  
	    $id = $this->request->getParam('contractor_id');
        $commission = $this->request->getParam('commission');
        $contractor_service_id = $this->request->getParam('contractor_service_id');
		
		$modelContractorService = new Model_ContractorService();
        $contractorService = $modelContractorService->getById($contractor_service_id);
		
		
		$modelContractorInfo = new Model_ContractorInfo();
        $contractorInfo = $modelContractorInfo->getByContractorId($id);
		
		 if (!$contractorService) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't Have Permission"));
            $this->_redirect($this->router->assemble(array('contractor_id' => $this->contractor_id), 'settingsContractorInfoList'));
            return;
        }
		

		if ($this->request->isPost()) { // check if POST request method		  
                $data = array(
                    'commission' => $commission
                );


                $success = $modelContractorService->updateById($contractor_service_id, $data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Contractor Service"));
                }               
                echo 1;
                exit;
              
            
        }
		
		$this->view->contractorService = $contractorService;
		$this->view->contractor_id = $this->contractor_id;
		$this->view->contractor_service_id = $contractor_service_id;
		$this->view->contractorInfo = $contractorInfo;
		
		
		 echo $this->view->render('contractor-service/edit-service-commission.phtml');
        exit;
	
	
	}
}

