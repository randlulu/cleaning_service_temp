<?php

class Model_InquiryDiscussion {

    protected $_dbhost = '127.0.0.1';
    protected $_dbName = 'cleaning_service';
    protected $_collectionName = 'inquiry_discussion';
    protected $_con;
    protected $_collection;
    protected $_db;

    //protected static $db;

    /**
     * __construct
     * 
     * @param array $data as data
     * @param array $config as array of config
     * 
     * @return null
     */
    public function __construct($data = array(), $config = array()) {
        $dbhost = 'localhost';
        // Connect to test database  
        $this->_con = new Mongo("mongodb://127.0.0.1");

        //$db = $con->cleaning_service;
        $this->_db = $this->_con->selectDB($this->_dbName);

        $this->_collection = $this->_db->selectCollection($this->_collectionName);
    }

    public function closeConnection() {
        $isClosed = $this->_con->close();
        return $isClosed;
    }
	
	public function insert($doc = array()) {
		//$loggedUser = CheckAuth::getLoggedUser();
		if(isset($doc['user_id']) && (!isset($doc['user_name']) || !isset($doc['avatar']) || !isset($doc['user_role_name'] ))){
			$modelUser = new Model_User();
			$modelContractorInfo = new Model_ContractorInfo();
			$modelAuthRole = new Model_AuthRole();
			//$contractorRoleId = $modelAuthRole->getRoleIdByName('contractor');
			$adminRoleId = $modelAuthRole->getRoleIdByName('super_admin');
			$manggerRoleId = $modelAuthRole->getRoleIdByName('manager');
			$salesRoleId = $modelAuthRole->getRoleIdByName('sales');
		
			$roles = array($adminRoleId, $manggerRoleId, $salesRoleId);
			$user = $modelUser->getById($doc['user_id']);
			if(!$user){
				$modelCustomer = new Model_Customer();
				$user = $modelCustomer->getById($doc['user_id']);
				$user['role_name'] = 'customer';
				$user['role_id'] = $modelAuthRole->getRoleIdByName('customer');
				$user['username'] = $user['first_name'] . ' ' . $user['last_name'];
			}
			if ($user['role_name'] == 'contractor') {
				$contractorInfo = $modelContractorInfo->getByContractorId($user['user_id']);
				$user['username'] = ucwords($user['username']) . ' - ' . ucwords($contractorInfo['business_name']);
			} else {
				$user['username'] = ucwords($user['username']);
			}
			
			if(!isset($doc['user_name'])){
				$doc['user_name'] = $user['username'];	
			}
			if(!isset($doc['avatar'])){
				$doc['avatar'] = isset($user['avatar'])?$user['avatar']:"";	
			}
			if(!isset($doc['user_role_name'])){
				$doc['user_role_name'] = $user['role_name'];	
			}
			if(!isset($doc['visibility'])){
				if(!in_array($user['role_id'], $roles)){
					$doc['visibility'] = 2;
				}else{
					$doc['visibility'] = 1;
				}
			}
			
		}
		if(!isset($doc['user_role'])){
			$doc['user_role'] = $user['role_id'];
		}
		if(!isset($doc['seen_by_ids'])){
			$doc['seen_by_ids'] = "";
		}
		if(!isset($doc['seen_by_names'])){
			$doc['seen_by_names'] = "";
		}
		if(!isset($doc['discussion_id'])){
			$modelDiscussionSeq = new Model_DiscussionSeq();
			$newDiscussionId = $modelDiscussionSeq->updateInquiryDiscussionSeq();
			$doc['discussion_id'] = $newDiscussionId;
		}else{
			$newDiscussionId = 1;
		}
		if(isset($doc['inquiry_id']) && getType($doc['inquiry_id']) == 'string'){
			$doc['inquiry_id'] = (int) $doc['inquiry_id'];
		}
		if(isset($doc['user_role']) && getType($doc['user_role']) == 'string'){
			$doc['user_role'] = (int) $doc['user_role'];
		}
		if(isset($doc['user_id']) && getType($doc['user_id']) == 'string'){
			$doc['user_id'] = (int) $doc['user_id'];
		}
		
		$success = $this->_collection->insert($doc);
		$modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($newDiscussionId, $this->_collectionName, 'added');
		/*$newDocID = $doc['_id'];
		
		$data = array(
			'discussion_id' => $newDocID->{'$id'}
		);
		
		
		$successUpdate = $this->updateById($newDocID, $data);
		/*$modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'added');*/
		return $newDiscussionId; 
    }
	
	public function getByInquiryId($id, $order = 'asc', $filter = array()) {
	    $id = (int) $id;
		$modelInquiry = new Model_Inquiry();
		$inquiry = $modelInquiry->getById($id);
		$modelAuthRole = new Model_AuthRole();
		$loggedUser = CheckAuth::getLoggedUser();
		$contractorRoleId = $modelAuthRole->getRoleIdByName('contractor');
		$isContractor = false;
		if($loggedUser['role_id'] == $contractorRoleId){
			$isContractor = true;
		}
		
		if($order == 'asc'){
			$newOrder = 1;
		}else{
			$newOrder = -1;
		}
		
		$conditions = array();
		$conditions[] = array('inquiry_id' => $id);
		if($isContractor){
			$conditions[] =	array('visibility' => array('$ne' => 1));
		}
		
		if($this->_collection->find(array('$and' => $conditions))->sort(array('created' => $newOrder))->count() > 0){
			$discussions = $this->_collection->find(array('$and' => $conditions))->limit(10)->sort(array('created' => $newOrder));
		}else{
			$discussions = 0;
		}
		
		
		if($discussions){
			foreach($discussions as $discussion){
				$discussionsAsArray[] = $discussion;
			}
			//$discussionsAsArray = iterator_to_array($discussions);
		}else{
			$discussionsAsArray = array();
		}
		
		if($discussionsAsArray){
			$modelItemImageDiscussion = new Model_ItemImageDiscussion();
			$modelImage = new Model_Image();
			foreach($discussionsAsArray as &$discussion){
			    
				$discussion['created'] = isset($discussion['created'])? $discussion['created'] : $inquiry['created']; 
				$itemImageDiscussion = $modelItemImageDiscussion->getByDiscussionIdAndType($discussion['discussion_id'], 'inquiry');
				if($itemImageDiscussion){
					$discussion['group_id'] = $itemImageDiscussion[0]['group_id'];
					if($itemImageDiscussion[0]['image_id'] != 0){
						$image = $modelImage->getByImageId($itemImageDiscussion[0]['image_id']);
						$discussion['image_id'] = $itemImageDiscussion[0]['image_id'];
						$discussion['thumbnail_path'] = $image['thumbnail_path'];
						$discussion['compressed_path'] = $image['compressed_path'];
						$discussion['role_id'] = $image['user_role'];
					}else{
						$discussion['image_id'] = NULL;
						$discussion['thumbnail_path'] = NULL;
						$discussion['compressed_path'] = NULL;
						$discussion['role_id'] = $discussion['user_role'];
					}
				}else{
					$discussion['group_id'] = NULL;
					$discussion['image_id'] = NULL;
					$discussion['thumbnail_path'] = NULL;
					$discussion['compressed_path'] = NULL;
					$discussion['role_id'] = $discussion['user_role'];
				}
			}
		}
		
		return $discussionsAsArray;
		
	}
	
	public function countDiscussions($id, $isContractor){
		$id = (int) $id;
		if($isContractor){
			$countDiscussions = $this->_collection->find(array('$and' => array(array('inquiry_id' => $id),array('visibility' => array('$ne' => 1)))))->count();
		}else{
			$countDiscussions = $this->_collection->find(array('$and' => array(array('inquiry_id' => $id))))->count();
		}
		return $countDiscussions;
	}
	
	
	public function getSpecificTypeDisByOtherUsers($item_id, $loggedUserId, $isContractor){
		$item_id = (int) $item_id;
		$loggedUserId = (int) $loggedUserId;
		
		if($isContractor){
			if($this->_collection->find(array('$and' => array(array('user_id' => array('$ne'=> $loggedUserId)),array('inquiry_id' => $item_id),array('visibility' => array('$ne' => 1)))))->sort(array('created' => -1))->count() > 0){
				$discussions = $this->_collection->find(array('$and' => array(array('user_id' => array('$ne'=> $loggedUserId)),array('inquiry_id' => $item_id),array('visibility' => array('$ne' => 1)))))->sort(array('created' => -1));
				
			}else{
				$discussions = 0;
			}
		}else{
			if($this->_collection->find(array('$and' => array(array('user_id' => array('$ne'=> $loggedUserId)),array('inquiry_id' => $item_id))))->sort(array('created' => -1))->count() > 0){
				$discussions = $this->_collection->find(array('$and' => array(array('user_id' => array('$ne'=> $loggedUserId)),array('inquiry_id' => $item_id))))->sort(array('created' => -1));
				
			}else{
				$discussions = 0;
			}
			
		}
		return $discussions;
	}
	
	public function changeCommentSeenFlagById($id, $loggedUserId, $loggedUserName) {
        //$id = (int) $id;
		$loggedUserId = (int) $loggedUserId;
		$discussion = $this->getByMongoId($id);
		
		$newId = "";
		$newName = "";
        $seenId = array("");
		$seenNames = array("");
		$result1 = 0;
		$result2 = 0;
		
		
		foreach ($discussion as $key => $value) {
            $seenId = explode(',', $value['seen_by_ids']);
			$seenNames = explode(',', $value['seen_by_names']);
			
            if (!in_array($loggedUserId, $seenId)) {
                if (!empty($value['seen_by_ids'])) {
					$newId = $value['seen_by_ids'] . "," . $loggedUserId;
					$newName = $value['seen_by_names'] . "," . $loggedUserName;
					//echo "by walaa f";print_r($newId);exit;
                } else {
                    $newId = (string) $loggedUserId;
					
					$newName = $loggedUserName;
					//echo "by walaa n";print_r($newId);exit;
                }
               
				
				$result1 = $this->_collection->update(
					array('_id' => new MongoId($key)), array('$set' =>array('seen_by_ids' => $newId)), array('multiple' => true));
				$result2 = $this->_collection->update(
					array('_id' => new MongoId($key)), array('$set' =>array('seen_by_names' => $newName)), array('multiple' => true));
				
            }
        }
			
		if($result1 && $result2)
			return 1;
		return 0;
    }
	
	public function updateDiscImages($id, $images){
		
		$this->_collection->update(
					array('_id' => new MongoId($id)), array('$set' =>array('images' => $images)));
					return 1;
	}
	
	public function getByMongoId($id) {
        //$id = (int) $id;
		$discussion = $this->_collection->find(array('_id' => new MongoId($id)));
		if($discussion){
			$discussionsAsArray = iterator_to_array($discussion);
		}else{
			$discussionsAsArray = 0;
		}
		
		return $discussionsAsArray;
    }
	
	public function getById($id) {
        $id = (int) $id;
		$discussion = $this->_collection->find(array('discussion_id' => $id));
		
		if($discussion){
			$discussionsAsArray = iterator_to_array($discussion);
		}else{
			$discussionsAsArray = 0;
		}
		
		if($discussionsAsArray){
			$discussionsAfterProcessing = array();
			foreach($discussionsAsArray as $k => $v){
				$discussionsAfterProcessing['discussion_id'] = $v['discussion_id'];
				$discussionsAfterProcessing['inquiry_id'] = $v['inquiry_id'];
				$discussionsAfterProcessing['user_id'] = $v['user_id'];
				$discussionsAfterProcessing['user_name'] = $v['user_name'];
				$discussionsAfterProcessing['avatar'] = $v['avatar'];
				$discussionsAfterProcessing['user_message'] = $v['user_message'];
				$discussionsAfterProcessing['created'] = $v['created'];
				$discussionsAfterProcessing['user_role_name'] = $v['user_role_name'];
				$discussionsAfterProcessing['user_role'] = $v['user_role'];
				$discussionsAfterProcessing['visibility'] = $v['visibility'];
				
			}
		}else{
			$discussionsAfterProcessing = 0;
		}
		
		
		
		return $discussionsAfterProcessing;
    }
	
	public function markAllAsSeen($item_id, $loggedUserName, $loggedUserId, $isContractor){
		$loggedUserId = (int) $loggedUserId;
		$item_id = (int) $item_id;
		
		$discussions = $this->getSpecificTypeDisByOtherUsers($item_id, $loggedUserId, $isContractor);
		
		$newId = "";
		$newName = "";
        $seenIds = array("");
		$seenNames = array("");
		$result1 = array();
		$result2 = array();
		if($discussions){
		foreach ($discussions as $key => $value) {
            $seenIds = explode(',', $value['seen_by_ids']);
		
			$seenNames = explode(',', $value['seen_by_names']);
			
            if (!in_array($loggedUserId, $seenIds)) {
                if (!empty($value['seen_by_ids'])) {
					$newId = $value['seen_by_ids'] . "," . $loggedUserId;
					$newName = $value['seen_by_names'] . "," . $loggedUserName;
					
                } else {
                    $newId = (string) $loggedUserId;
					
					$newName = $loggedUserName;
				
                }
				
				$result1[$key] = $this->_collection->update(
					array('_id' => new MongoId($key)), array('$set' =>array('seen_by_ids' => $newId)), array('multiple' => true));
				$result2[$key] = $this->_collection->update(
					array('_id' => new MongoId($key)), array('$set' =>array('seen_by_names' => $newName)), array('multiple' => true));
					
				
            }
        }
		}
		if(!empty($result1) && !in_array(0,$result1) && !empty($result2) && !in_array(0,$result2)){
			return 1;
		}
        return 0;
	}
	
	public function deleteByMongoId($id) {
		//$id = (int) $id;
        return $this->_collection->remove(array('_id' => new MongoId($id)));
    }
	
	public function deleteById($id) {
		$id = (int) $id;
        return $this->_collection->remove(array('discussion_id' => $id));
    }

	public function updateById($id, $data){
		$id = (int) $id;
		if(isset($data['user_id']) && (!isset($data['user_name']) || !isset($data['avatar']) || !isset($data['user_role_name'] ) || !isset($data['user_role'] ))){
			$modelUser = new Model_User();
			$modelContractorInfo = new Model_ContractorInfo();
			$modelAuthRole = new Model_AuthRole();
			$contractorRoleId = $modelAuthRole->getRoleIdByName('contractor');
			$user = $modelUser->getById($data['user_id']);
			if ($user['role_name'] == 'contractor') {
				$contractorInfo = $modelContractorInfo->getByContractorId($user['user_id']);
				$user['username'] = ucwords($user['username']) . ' - ' . ucwords($contractorInfo['business_name']);
			} else {
				$user['username'] = ucwords($user['username']);
			}
			
			if(!isset($data['user_name'])){
				$data['user_name'] = $user['username'];	
			}
			if(!isset($data['avatar'])){
				$data['avatar'] = $user['avatar'];	
			}
			if(!isset($data['user_role_name'])){
				$data['user_role_name'] = $user['role_name'];	
			}
			if(!isset($data['user_role'])){
				$data['user_role'] = $user['role_id'];	
			}
		}
		$modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_collectionName, 'changed');
		return $this->_collection->update(
						array('discussion_id' => $id), array('$set' => $data));	
	}
	
	public function getByImageId($id, $order = 'asc') {
		$modelItemImageDiscussion = new Model_ItemImageDiscussion();
		$filters['type'] = 'inquiry';
		
		$filters['image_id'] = $id;
		$itemImageDiscussions = $modelItemImageDiscussion->getAll($filters);
		
		$discussion_ids = array();
		foreach($itemImageDiscussions as $itemImageDiscussion){
			$discussion_ids[] = $itemImageDiscussion['item_id'];
		}
		
		$conditions = array();
		
		$conditions[] =	array('discussion_id' => array('$in' => $discussion_ids));
		if(isset($filters)){		  
		    if(isset($filters['user_id'])){
			    $user_id = (int) $filters['user_id'];
			   	$conditions[] = array('user_id' => $user_id);  
		    }
		}
		
		if($order == 'asc'){
			$newOrder = 1;
		}else{
			$newOrder = -1;
		}
		if($this->_collection->find(array('$and' => $conditions))->sort(array('created' => $newOrder))->count() > 0){
			$discussions = $this->_collection->find(array('$and' => $conditions))->limit(10)->sort(array('created' => $newOrder));
		}else{
			$discussions = 0;
		}
		if($discussions){
			foreach($discussions as $discussion){
				$discussionsAsArray[] = $discussion;
			}
			//$discussionsAsArray = iterator_to_array($discussions);
		}else{
			$discussionsAsArray = 0;
		}
		if($discussionsAsArray){
			$modelItemImageDiscussion = new Model_ItemImageDiscussion();
			$modelImage = new Model_Image();
			foreach($discussionsAsArray as &$discussion){
				$itemImageDiscussion = $modelItemImageDiscussion->getByDiscussionIdAndType($discussion['discussion_id'], 'inquiry');
				if($itemImageDiscussion){
					$discussion['group_id'] = $itemImageDiscussion[0]['group_id'];
				}else{
					$discussion['group_id'] = 0;
				}
			}
		}
		return $discussionsAsArray;

	}
}
