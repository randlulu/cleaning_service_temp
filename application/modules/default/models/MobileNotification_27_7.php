<?php

class Model_MobileNotification extends Zend_Db_Table_Abstract {
    
	public function notify($booking_id,$case){
		$model_booking = new Model_Booking();
		$model_contractorServiceBooking = new Model_ContractorServiceBooking();
		
		$booking = $model_booking->getById($booking_id);
		if($case == 'new booking'){
			$notification_text = 'You have new booking '.$booking['booking_num'].' On '.$booking['booking_start'].' please check it.';
		}
		else if($case == 'updated booking'){
			$notification_text = 'Your booking '.$booking['booking_num'].' has been updated, please check it.';
		}
		else if($case == 'delete booking'){
			$notification_text = 'Your booking '.$booking['booking_num'].' has been deleted.';
		}
		else if($case == 'new complaint'){
			$notification_text = 'you have new complaint on booking number '.$booking['booking_num'].', please check it.';
		}
		else if($case == 'new comment'){
			$notification_text = 'you have new comment on booking number '.$booking['booking_num'].', please check it.';
		}
		
		
		$contractors = $model_contractorServiceBooking->getContractorIdsByBookingId($booking_id);
	
		foreach($contractors as $contractor){
		
		//// get contractor info
		//$loggedUser = CheckAuth::getLoggedUser();
        //$userId = $loggedUser['user_id'];
        //$userId = 8;
		
		$model_ContractorMobileInfo = new Model_ContractorMobileInfo();
		$mobileInfo = $model_ContractorMobileInfo->getByContractorId($contractor['contractor_id']);
		
		print_r($mobileInfo);
		
		$message = new Zend_Mobile_Push_Message_Apns();
		$message->setAlert($notification_text);
		$message->setBadge(1);
		$message->setSound('default');
		$message->setId(time());
		$message->setToken($mobileInfo['devicetoken']);
		 
		$apns = new Zend_Mobile_Push_Apns();
		$apns_cert = APPLICATION_PATH.'/PushExportDevelopment.pem';
		//$apns_cert = APPLICATION_PATH.'/PushExportProduction.pem';
		echo $apns_cert;
		$apns->setCertificate($apns_cert);
		// if you have a passphrase on your certificate:
		//$apns->setCertificatePassphrase('tile@cleaners');
		 
		try {
			$apns->connect(Zend_Mobile_Push_Apns::SERVER_SANDBOX_URI);
		} catch (Zend_Mobile_Push_Exception_ServerUnavailable $e) {
			// you can either attempt to reconnect here or try again later
			echo $e->getMessage();
			exit(1);
		} catch (Zend_Mobile_Push_Exception $e) {
			echo 'APNS Connection Error:' . $e->getMessage();
			exit(1);
		}
		 
		try {
			$apns->send($message);
			///////save this notification on mongo
			$model_Mongo = new Model_Mongo();
			$doc = array(
			  'contractor_mobile_info_id'=> $mobileInfo['id'],
			  'contractor_id'=> $contractor['contractor_id'],
			  'contractor_name'=> 'markk',
			  'notification_text' => $notification_text,
			  'send_date' => 'October 31, 2014, 5:30 pm',
			  'seen'=> 0,
			  'booking_id'=> $booking_id
			  );
			$model_Mongo->insertNotification($doc);
			
			
			
		} catch (Zend_Mobile_Push_Exception_InvalidToken $e) {
			// you would likely want to remove the token from being sent to again
			echo $e->getMessage();
		} catch (Zend_Mobile_Push_Exception $e) {
			// all other exceptions only require action to be sent
			echo $e->getMessage();
		}
		$apns->close();
		

		
		}
	}
    
}
