<?php

class Model_Complaint extends Zend_Db_Table_Abstract {

    protected $_name = 'complaint';
    //fill 
    private $modelBooking;
    private $modelUser;
    private $modelContractorServiceBooking;
    private $modelCustomer;
    private $modelComplaintType;
    private $modelComplaintDiscussion;

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
	 
	public function getByNum($num){
	    
        $company_id = CheckAuth::getCompanySession();
		$company_id = (int) $company_id;		
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("complaint_num = '{$num}'");
		$select->where("company_id = {$company_id}");
		return $this->getAdapter()->fetchRow($select);
	 }
	 
	 
	 public function getAll($filters = array(), $order = null, &$pager = null, $limit = 0, $perPage = 0, $currentPage = 0) {
	
	    $select = $this->getAdapter()->select();
        $select->from(array('c' => $this->_name));
        $select->order($order);
        $select->distinct();
		
	    if ($order) {
            $select->order($order);
        }

        $wheresAndJoins = $this->getWheresAndJoinsByFilters($filters);
        $joinInner = $wheresAndJoins['joinInner'];
        $wheres = $wheresAndJoins['wheres'];
	
	    if ($wheres) {
            foreach ($wheres as $where) {
				$select->where($where);
            }
        }
        if ($pager) {
            if (!empty($filters['perPage'])) {
                $select->limit($filters['perPage'], ($pager->currentPage - 1) * $filters['perPage']);
            } else {
                $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            }

            $pager->dbSelect = $select;
        } elseif ($perPage && $currentPage) {
            $select->limit($perPage, ($currentPage - 1) * $perPage);
        } else {
            if (!empty($filters['perPage'])) {
                $select->limit($filters['perPage']);
            }
        }
		
		if ($joinInner) {
            foreach ($joinInner as $inner) {
                $select->joinInner($inner['name'], $inner['cond'], $inner['cols']);
            }
        }

		//echo $select->__toString();
        return $this->getAdapter()->fetchAll($select);
	}
	 
    /*public function getAll($filters = array(), $order = null, &$pager = null) {
        $joinInner = array();
        $select = $this->getAdapter()->select();
        $select->from(array('c' => $this->_name));
        $select->distinct();
        $select->order($order);

        $loggedUser = CheckAuth::getLoggedUser();
        $filters['company_id'] = CheckAuth::getCompanySession();

        $select->joinInner(array('csb' => 'contractor_service_booking'), 'c.booking_id = csb.booking_id', '');

        if (!CheckAuth::checkCredential(array('canSeeAllComplaint'))) {
            if (CheckAuth::checkCredential(array('canSeeOnlyHisComplaint'))) {
                if (CheckAuth::checkCredential(array('canSeeAssignedComplaint'))) {
                    $select->where("c.user_id = " . $loggedUser['user_id'] . " OR csb.contractor_id = {$loggedUser['user_id']}");
                } else {
                    $select->where("c.user_id = " . $loggedUser['user_id']);
                }
            }
        }

        if ($filters) {
            if (!empty($filters['keywords'])) {
                $keywords = $this->getAdapter()->quote('%' . trim($filters['keywords']) . '%');
                
				$joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.booking_id = bok.booking_id', 'cols' => '');
				
				
				
                $select->where("c.full_text_search LIKE {$keywords} OR bok.full_text_search LIKE {$keywords}");

              
            }
            if (!empty($filters['complaintType'])) {
                $select->where("c.complaint_type_id = {$filters['complaintType']}");
            }

            if (!empty($filters['complaintStatus']) && $filters['complaintStatus'] != 'all') {
                $select->where("c.complaint_status = '{$filters['complaintStatus']}'");
            }

            if (!empty($filters['company_id'])) {
                $company_id = (int) $filters['company_id'];
                $select->where("c.company_id = {$company_id}");
            }
            if (!empty($filters['complaint_num'])) {
                $complaint_num = $this->getAdapter()->quote('%' . trim($filters['complaint_num']) . '%');
                $select->where("c.complaint_num LIKE {$complaint_num}");
            }
			if (!empty($filters['is_approved']) || key_exists("is_approved",$filters)) {
                $is_approved = (int) $filters['is_approved'];
                $select->where("c.is_approved = {$is_approved}");
            }
            if (!empty($filters['comment'])) {
                $comment = $this->getAdapter()->quote('%' . trim($filters['comment']) . '%');
                $select->where("c.comment LIKE {$comment}");
            }
            if (!empty($filters['booking_num'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.booking_id = bok.booking_id', 'cols' => '');
                $booking_num = $this->getAdapter()->quote('%' . trim($filters['booking_num']) . '%');
                $select->where("bok.booking_num LIKE {$booking_num}");
            }
            if (!empty($filters['customer_id'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.booking_id = bok.booking_id', 'cols' => '');
                $filters['customer_id'] = (int) $filters['customer_id'];
                $select->where("bok.customer_id = {$filters['customer_id']} ");
            }
            if (!empty($filters['contractor_id'])) {
                $contractor_id = (int) $filters['contractor_id'];
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.booking_id = bok.booking_id', 'cols' => '');
                $select->where("bok.created_by = {$contractor_id} OR csb.contractor_id = {$contractor_id}");
            }
            if (!empty($filters['user_id'])) {
                $user_id = (int) $filters['user_id'];
                $select->where("c.user_id = {$user_id}");
            }
            if (!empty($filters['bussiness_name'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.booking_id = bok.booking_id', 'cols' => '');
                $joinInner['customer_commercial_info'] = array('name' => array('cci' => 'customer_commercial_info'), 'cond' => 'bok.customer_id = cci.customer_id', 'cols' => '');
                $select->where("cci.business_name LIKE '%{$filters['bussiness_name']}%'");
            }
            if (!empty($filters['inquiry_num'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.booking_id = bok.booking_id', 'cols' => '');
                $joinInner['inquiry'] = array('name' => array('i' => 'inquiry'), 'cond' => 'bok.original_inquiry_id = i.inquiry_id', 'cols' => '');
                $inquiry_num = $this->getAdapter()->quote('%' . trim($filters['inquiry_num']) . '%');
                $select->where("i.inquiry_num LIKE {$inquiry_num}");
            }
            if (!empty($filters['estimate_num'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.booking_id = bok.booking_id', 'cols' => '');
                $joinInner['booking_estimate'] = array('name' => array('est' => 'booking_estimate'), 'cond' => 'bok.booking_id = est.booking_id', 'cols' => '');
                $estimate_num = $this->getAdapter()->quote('%' . trim($filters['estimate_num']) . '%');
                $select->where("est.estimate_num LIKE {$estimate_num}");
            }
            if (!empty($filters['mobile/phone'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.booking_id = bok.booking_id', 'cols' => '');
                $joinInner['customer'] = array('name' => array('cu' => 'customer'), 'cond' => 'bok.customer_id = cu.customer_id', 'cols' => '');
                $mobile_phone = trim($filters['mobile/phone']);
                $select->where("CONCAT_WS(' ',phone1 ,phone2 ,phone3,mobile1 ,mobile2 ,mobile3) LIKE '%{$mobile_phone}%'");
            }
            if (!empty($filters['email'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.booking_id = bok.booking_id', 'cols' => '');
                $joinInner['customer'] = array('name' => array('cu' => 'customer'), 'cond' => 'bok.customer_id = cu.customer_id', 'cols' => '');
                $email = trim($filters['email']);
                $select->where("CONCAT_WS(' ',email1 ,email2 ,email3) LIKE '%{$email}%'");
            }
            if (!empty($filters['service_id'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.booking_id = bok.booking_id', 'cols' => '');
                $service_id = (int) $filters['service_id'];
                $select->where("csb.service_id = {$service_id}");
            }
        }

        if ($joinInner) {
            foreach ($joinInner as $inner) {
                $select->joinInner($inner['name'], $inner['cond'], $inner['cols']);
            }
        }
        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

		
		

		
        return $this->getAdapter()->fetchAll($select);
    }*/

    public function insert(array $data) {

        $company_id = CheckAuth::getCompanySession();

        $select = $this->getAdapter()->select();
        $select->from($this->_name, 'MAX(count)');
        $select->where("company_id = {$company_id}");
        $result = $this->getAdapter()->fetchOne($select);

        $data['count'] = $result + 1;
        $data['complaint_num'] = 'CMP-' . ($result + 1);

        $id = parent::insert($data);
		//$this->updateFullTextSearch($id);
        /**
         * update full text search table
         */
        $modelUpdateFullTextSearch = new Model_UpdateFullTextSearch();
        $modelUpdateFullTextSearch->insertFullTextSearch($this->_name, $id);

		
		$complaint = $this->getById($id);
		
		if($complaint['booking_id'] && isset($complaint['booking_id'])){
		  $modelIosSync = new Model_IosSync();
          $modelIosSync->changeSyncBooking($complaint['booking_id']);
        }
		
        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'added');
		
		/**
		* send notification
		*/
		//MobileNotification::notify($data['booking_id'],'new complaint');			

		
        return $id;
    }

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateById($id, $data) {
        $id = (int) $id;

        /**
         * update full text search table
         */
        $done = false;
        if (!empty($data['full_text_search'])) {
            $done = true;
        }
        $modelUpdateFullTextSearch = new Model_UpdateFullTextSearch();
        $modelUpdateFullTextSearch->updateFullTextSearchFlag($this->_name, $id, $done);

        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'changed');
		$success = parent::update($data, "complaint_id = '{$id}'");
		$modelComplaintLog = new Model_ComplaintLog();
		$modelComplaintLog->addComplaintLog($id);
		
		  $complaint = $this->getById($id);		
		  $modelIosSync = new Model_IosSync();
          $modelIosSync->changeSyncBooking($complaint['booking_id']);
		
		//$this->updateFullTextSearch($id);
        return $success;
    }

    /**
     * delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;

        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'deleted');
		
		$complaint = $this->getById($id);
		
		if($complaint['booking_id'] && isset($complaint['booking_id'])){
		  $modelIosSync = new Model_IosSync();
          $modelIosSync->changeSyncBooking($complaint['booking_id']);
        }

        return parent::delete("complaint_id = '{$id}'");
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from(array('cmp'=>$this->_name));
        $select->joinInner(array('bok'=>'booking') , 'bok.booking_id = cmp.booking_id' , array('pause_emails'));
        $select->where("complaint_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the assigned BookingId
     * 
     * @param int $id
     * @return array
     */
    public function getByBookingId($id, $is_count = 0) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
		if($is_count){
		 $select->from($this->_name, array('count' => 'count(DISTINCT(complaint_id))'));
		}else{
		 $select->from($this->_name);
		}
        
        $select->where("booking_id = '{$id}'");

        if($is_count){
         return $this->getAdapter()->fetchOne($select);
		}else{
		 return $this->getAdapter()->fetchAll($select);
		}
    }
	public function getByBookingIdAndIsApproved($id){
	    $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_id = '{$id}'");
        $select->where("is_approved = '1'");

        return $this->getAdapter()->fetchAll($select);
	  
	
	}

    /**
     * get all the recent complaints that the user are able to see
     * 
     * @return array  
     */
    public function getRecentComplaint($filters = array()) {

        $loggedUser = CheckAuth::getLoggedUser();
        $userId = $loggedUser['user_id'];
        $company_id = CheckAuth::getCompanySession();

        $select = $this->getAdapter()->select();

        $select->distinct();

        $select->from(array('bc' => $this->_name));

        $select->joinInner(array('csb' => 'contractor_service_booking'), 'bc.booking_id = csb.booking_id', 'csb.contractor_id');

        if (!CheckAuth::checkCredential(array('canSeeAllComplaint'))) {
            if (CheckAuth::checkCredential(array('canSeeOnlyHisComplaint'))) {
                if (CheckAuth::checkCredential(array('canSeeAssignedComplaint'))) {
                    $select->where("bc.user_id = {$userId} OR csb.contractor_id = {$userId}");
                } else {
                    $select->where("bc.user_id = {$userId}");
                }
            }
        }

        if ($company_id) {
            $select->where("bc.company_id = {$company_id}");
        }
        if ($filters) {
            $select->joinInner(array('bok' => 'booking'), 'bc.booking_id = bok.booking_id', '');
            if (!empty($filters['created_by'])) {
                $userId = (int) $filters['created_by'];
                $select->where("bok.created_by = '{$userId}'");
            }
        }

        $select->order("created DESC");

        $select->limit(20);

        return $this->getAdapter()->fetchall($select);
    }
	
	
		public function ReminderComplaintToContractor() {

        $today = strtotime(date('Y-m-d'));
		$router = Zend_Controller_Front::getInstance()->getRouter();
	    $modelBookingStatus = new Model_BookingStatus();
	    $modelCustomer = new Model_Customer();
	    $modelBookingAddress = new Model_BookingAddress();
	    $modelVisitedExtraInfo = new Model_VisitedExtraInfo();
		$trading_namesObj = new Model_TradingName();
		
		$colors = array(
		'0' => '#888888',
		'1' => '#cc3333',
		'2' => '#dd4477',
		'3' => '#994499',
		'4' => '#6633cc',
		'5' => '#336699',
		'6' => '#3366cc',
		'7' => '#22aa99',
		'8' => '#329262',
		'9' => '#109618',
		'10' => '#66aa00',
		'11' => '#aaaa11',
		'12' => '#d6ae00',
		'13' => '#ee8800',
		'14' => '#dd5511',
		'15' => '#a87070',
		'16' => '#8c6d8c',
		'17' => '#627487',
		'18' => '#7083a8',
		'19' => '#5c8d87',
		'20' => '#898951',
		'21' => '#b08b59',
		'-1' => '#7a367a'
	);
		$select = $this->getAdapter()->select();
        $select->from(array('cmp'=>$this->_name));
        $select->joinInner(array('bok'=>'booking') , 'bok.booking_id = cmp.booking_id' , array('pause_emails'));
		$select->where("bok.is_deleted = 0");
        $select->where("bok.pause_emails = 0");
		$select->where("cmp.to_follow <= '{$today}' and cmp.to_follow > 0");
        $select->where("cmp.is_to_follow = 1");
        $select->where("cmp.complaint_status = 'open'");

	   
      
			
		$complaints = $this->getAdapter()->fetchAll($select);
		
	

		if($complaints){
		$this->fills($complaints, array('booking', 'name_email_contractors', 'complaint_type' , 'customer'));
		
       foreach($complaints as $complaint)
        {		
		$complaintId = $complaint['complaint_id'];
		$status = $modelBookingStatus->getById($complaint['booking']['status_id']);
		$trading_names = $trading_namesObj->getById($complaint['booking']['trading_name_id']);
		$extra_info = $modelVisitedExtraInfo->getCompletedBookingExtraInfo($complaint['booking']['booking_id']);
		$booking_link = $router->assemble(array('id'=>$complaint['booking']['booking_id']), 'bookingView');
		$complaint_link = $router->assemble(array('id'=>$complaint['complaint_id']), 'complaintView');
		$convert_status = $router->assemble(array(), 'complaint');
		$customer_contacts = nl2br($modelCustomer->getCustomerContacts($complaint['customer']['customer_id'] , 'email'));
		$booking_address = get_line_address($modelBookingAddress->getByBookingId($complaint['booking']['booking_id']));
		$template_params = array(
            //booking
            '{booking_num}' => $complaint['booking']['booking_num'],
            '{status_color}' => $colors[$status['color']],
            '{status_name}' => $status['name'],
            '{booking_link}' => $booking_link,
			'{booking_address}' => $booking_address,
			'{total_with_tax}' => number_format($complaint['booking']['qoute'], 2),
			'{booking_Scheduled_visits}' =>$this->getBookingViewScheduleAsPlaceHolder($complaint['booking']['booking_id']),
			'{trading_name}' => $trading_names['trading_name'],
            //complaint
            '{comment}' => $complaint['comment'],
            '{created}' => getDateFormating($complaint['created']),
			'{to_follow}' => getDateFormating($complaint['to_follow']),
            '{complaint_num}' => $complaint['complaint_num'],
            '{complaint_status}' => $complaint['complaint_status'],
            '{complaint_link}' => $complaint_link,
            '{complaint_type}' => $complaint['complaint_type']['name'],
			'{complaint_discussion_view}' => $this->getComplaintViewDiscussionAsPlaceHolder($complaintId , 'new'),
			'{complaint_photo_view}' => $this->getComplaintViewPhotoAsPlaceHolder($complaintId),
			'{convert_status}' =>$convert_status.'?fltr[complaintStatus]='.$complaint['complaint_status'],
			//customer 
			'{customer_name}' => get_customer_name($complaint['customer']),
            '{customer_contacts}' => $customer_contacts
			
        );

	
        foreach ($complaint['name_email_contractors'] as $contractor) {
		    $template_params['{technician_display_name}'] =  $contractor['display_name'];
		    $email_log = array('reference_id' => $complaint['complaint_id'], 'type' => 'complaint');
            EmailNotification::sendEmail(array('to' => $contractor['email']), 'reminder_complaint', $template_params, $email_log, $complaint['booking']['company_id']);
        }
	   }	
	  } 
		
    }
	
	public function getBookingViewScheduleAsPlaceHolder($bookingId) {
	    
		$modelBooking = new Model_Booking();
		$modelBookingMultipleDays = new Model_BookingMultipleDays();
        $MainExtraInfo = $modelBooking->getExtraInfoBookingPrimaryDatesByBooking($bookingId);
		$multipleDaysWithVisitedExtraInfo = $modelBookingMultipleDays->getMultipleDaysWithVisitedByBookingId($bookingId);
        $view = new Zend_View();
        $view->setScriptPath(APPLICATION_PATH . '/modules/booking/views/scripts/index');
        $view->MainExtraInfo = $MainExtraInfo;
        $view->multipleDaysWithVisitedExtraInfo = $multipleDaysWithVisitedExtraInfo;
        $bodyComplaint = $view->render('scheduled_visits.phtml');
        return $bodyComplaint;
    }
	
	public function getComplaintViewPhotoAsPlaceHolder($complaintId) {
        $bodyComplaintPhoto = "";
        $modelImageAttachment = new Model_Image();
		$router = Zend_Controller_Front::getInstance()->getRouter();
		$pager = null ;
		$photo = $modelImageAttachment->getAll( $complaintId , 'complaint', 'iu.created desc' , $pager, $filter = array() , $limit = 10);  
        if (!empty($photo)) {
            $view = new Zend_View();
            $view->setScriptPath(APPLICATION_PATH . '/modules/complaint/views/scripts/index');
            $view->photos = $photo;
			$view->complaintLink = $router->assemble(array('id'=>$complaintId), 'complaintView');
			 $bodyComplaintPhoto = $view->render('complaint_photo.phtml');			
        }

        return $bodyComplaintPhoto;
    }
    
    public function countUapprovedCompalints($contractor_id = null){
	
	
	if(isset($contractor_id)){
		  $unapprovedComplaint = $this->getAll(array('is_approved' => 0 , 'contractor_id'=>$contractor_id));
		}else{
	     $unapprovedComplaint = $this->getAll(array('is_approved' => 0));
		}
        return count($unapprovedComplaint);
	}

    public function getComplaintCount($filters = array()) {

		
		$complaints = $this->getAll($filters);
		if($complaints && !empty($complaints)){
			return count($complaints);
		}
		else{
			return 0;
		}
		
    }
	
	 public function getWheresAndJoinsByFilters($filters, $isCronJob = false){

        $wheres = array();
        $joinInner = array();
		        $loggedUser = CheckAuth::getLoggedUser();

		        $joinInner['contractor_service_booking'] = array('name' => array('csb' => 'contractor_service_booking'), 'cond' => 'c.booking_id = csb.booking_id', 'cols' => '');

	if (!$isCronJob) {
		
        $filters['company_id'] = CheckAuth::getCompanySession();

        if (!CheckAuth::checkCredential(array('canSeeAllComplaint'))) {
            if (CheckAuth::checkCredential(array('canSeeOnlyHisComplaint'))) {
                if (CheckAuth::checkCredential(array('canSeeAssignedComplaint'))) {
					$wheres['canSeeAssignedComplaint'] = ("c.user_id = " . $loggedUser['user_id'] . " OR csb.contractor_id = {$loggedUser['user_id']}");
                } else {
					$wheres['canSeeAssignedComplaint'] =("c.user_id = " . $loggedUser['user_id']);
                }
            }
	 }
	 
	 	 	

	 
	 }
	 
	 
	 if ($filters) {
            if (!empty($filters['keywords'])) {
                $keywords = $this->getAdapter()->quote('%' . trim($filters['keywords']) . '%');
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.booking_id = bok.booking_id', 'cols' => '');
				$wheres['keywords'] =("c.full_text_search LIKE {$keywords} OR bok.full_text_search LIKE {$keywords}");

            }
            if (!empty($filters['complaintType'])) {
				$wheres['complaintType'] = ("c.complaint_type_id = {$filters['complaintType']}");
            }

            if (!empty($filters['complaintStatus']) && $filters['complaintStatus'] != 'all') {
				$wheres['complaintStatus'] =("c.complaint_status = '{$filters['complaintStatus']}'");
            }

            if (!empty($filters['company_id'])) {
                $company_id = (int) $filters['company_id'];
				$wheres['company_id'] =("c.company_id = {$company_id}");
            }
            if (!empty($filters['complaint_num'])) {
                $complaint_num = $this->getAdapter()->quote('%' . trim($filters['complaint_num']) . '%');
				$wheres['complaint_num'] =("c.complaint_num LIKE {$complaint_num}");
            }
			if (!empty($filters['is_approved']) || key_exists("is_approved",$filters)) {
                $is_approved = (int) $filters['is_approved'];
				$wheres['is_approved'] = ("c.is_approved = {$is_approved}");
            }
            if (!empty($filters['comment'])) {
                $comment = $this->getAdapter()->quote('%' . trim($filters['comment']) . '%');
				$wheres['comment'] =("c.comment LIKE {$comment}");
            }
            if (!empty($filters['booking_num'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.booking_id = bok.booking_id', 'cols' => '');
                $booking_num = $this->getAdapter()->quote('%' . trim($filters['booking_num']) . '%');
				$wheres['booking_num'] =("bok.booking_num LIKE {$booking_num}");
            }
            if (!empty($filters['customer_id'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.booking_id = bok.booking_id', 'cols' => '');
                $filters['customer_id'] = (int) $filters['customer_id'];
				$wheres['customer_id'] = ("bok.customer_id = {$filters['customer_id']} ");
            }
         if (!empty($filters['customer_name'])) {
             $modelCustomer = new Model_Customer();
             $allCustomer = $modelCustomer->getCustomerIdByIdFullNameSearch(trim($filters['customer_name']));
             
             $customerIds = implode(', ', $allCustomer);
             if ($customerIds) {
                 $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.booking_id = bok.booking_id', 'cols' => '');
                 $wheres['customer_name'] = ("bok.customer_id in ( $customerIds ) ");
             }
         }
         
         if (!empty($filters['contractor_id'])) {
                $contractor_id = (int) $filters['contractor_id'];
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.booking_id = bok.booking_id', 'cols' => '');
				$wheres['contractor_id'] = ("bok.created_by = {$contractor_id} OR csb.contractor_id = {$contractor_id}");
            }
            if (!empty($filters['user_id'])) {
                $user_id = (int) $filters['user_id'];
				$wheres['user_id'] = ("c.user_id = {$user_id}");
            }
            if (!empty($filters['bussiness_name'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.booking_id = bok.booking_id', 'cols' => '');
                $joinInner['customer_commercial_info'] = array('name' => array('cci' => 'customer_commercial_info'), 'cond' => 'bok.customer_id = cci.customer_id', 'cols' => '');
                $wheres['bussiness_name'] = ("cci.business_name LIKE '%{$filters['bussiness_name']}%'");
            }
            if (!empty($filters['inquiry_num'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.booking_id = bok.booking_id', 'cols' => '');
                $joinInner['inquiry'] = array('name' => array('i' => 'inquiry'), 'cond' => 'bok.original_inquiry_id = i.inquiry_id', 'cols' => '');
                $inquiry_num = $this->getAdapter()->quote('%' . trim($filters['inquiry_num']) . '%');
                $wheres['inquiry_num'] =("i.inquiry_num LIKE {$inquiry_num}");
            }
            if (!empty($filters['estimate_num'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.booking_id = bok.booking_id', 'cols' => '');
                $joinInner['booking_estimate'] = array('name' => array('est' => 'booking_estimate'), 'cond' => 'bok.booking_id = est.booking_id', 'cols' => '');
                $estimate_num = $this->getAdapter()->quote('%' . trim($filters['estimate_num']) . '%');
                 $wheres['estimate_num'] = ("est.estimate_num LIKE {$estimate_num}");
            }
            if (!empty($filters['mobile/phone'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.booking_id = bok.booking_id', 'cols' => '');
                $joinInner['customer'] = array('name' => array('cu' => 'customer'), 'cond' => 'bok.customer_id = cu.customer_id', 'cols' => '');
                $mobile_phone = trim($filters['mobile/phone']);
                $wheres['mobile/phone'] = ("CONCAT_WS(' ',phone1 ,phone2 ,phone3,mobile1 ,mobile2 ,mobile3) LIKE '%{$mobile_phone}%'");
            }
            if (!empty($filters['email'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.booking_id = bok.booking_id', 'cols' => '');
                $joinInner['customer'] = array('name' => array('cu' => 'customer'), 'cond' => 'bok.customer_id = cu.customer_id', 'cols' => '');
                $email = trim($filters['email']);
                $wheres['email'] = ("CONCAT_WS(' ',email1 ,email2 ,email3) LIKE '%{$email}%'");
				
            }
            if (!empty($filters['service_id'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.booking_id = bok.booking_id', 'cols' => '');
                $service_id = (int) $filters['service_id'];
				 $wheres['service_id'] =("csb.service_id = {$service_id}");
            }
			
			 //added by Mohamed
            if (!empty($filters['to_follow']) && $filters['to_follow'] == true) {
                $wheres['to_follow'] = ("c.is_to_follow = 1");              
            }

            if (!empty($filters['to_follow_date'])) {
                switch ($filters['to_follow_date']) {

                    case 'today':
                         $today = getTimePeriodByName('today');
                         $wheres['to_follow_date'] = ("c.to_follow between '" . strtotime($today['start']) . "' and '" . strtotime($today['end']) . "'");
                        break;

                    case 'past':
                        $today = getTimePeriodByName('today');
                         $wheres['to_follow_date'] =("c.to_follow < '" . strtotime($today['start']) . "'");
                        break;
                    case 'future':
                        $today = getTimePeriodByName('today');
                        $wheres['to_follow_date'] = ("c.to_follow > '" . strtotime($today['end']) . "'");
                        break;
                }
            }
            //added by Mohamed
			
			///By islam
			if (!empty($filters['has_approved_payment'])) {
                $joinInner['payment'] = array('name' => array('p' => 'payment'), 'cond' => 'c.booking_id = p.booking_id', 'cols' => 'p.is_approved');
				$wheres['is_approved'] = ("p.is_approved = 1");
            }
			if (!empty($filters['created_by'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.booking_id = bok.booking_id', 'cols' => '');
               // $service_id = (int) $filters['service_id'];
				 $wheres['created_by'] =("bok.created_by = {$filters['created_by']}");
            }
			
			if (!empty($filters['booking_start_between'])) {
				$joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.booking_id = bok.booking_id', 'cols' => '');
				
                $bookingStartBetween = $filters['booking_start_between'];
                $bookingEndBetween = !empty($filters['booking_end_between']) ? $filters['booking_end_between'] : date('Y-m-d H:i:s', time());
                
                    $wheres['booking_start'] = ("bok.booking_start between '" . $bookingStartBetween . "' and '" . $bookingEndBetween . "'");
               
            }
			
			///End
			
			
        }
	 
	 
	 return array('wheres' => $wheres, 'joinInner' => $joinInner);
	 

	 
	 }
	 
	 
	  public function getCount($filters = array()){
		 $count = $this->getAll($filters);
		 return count($count);
	 }
	 
	 

    /**
     * get extra info for all the rows
     * @param array $row
     * @param array $types
     * @return array
     */
    public function fills(&$rows, $types = array()) {
        foreach ($rows as &$row) {
            $this->fill($row, $types);
        }
    }

    /**
     * get extra info for each row (Ex: getting the category row by c_id, author row by author id ... etc)
     * 
     * @param array $row
     * @param array $types
     * @return array
     */
    public function fill(&$row, $types = array()) {
        if (in_array('booking', $types)) {
            /**
             * load model
             */
            if (!$this->modelBooking) {
                $this->modelBooking = new Model_Booking();
            }

            $booking = $this->modelBooking->getById($row['booking_id']);
            $row['booking'] = $booking;
        }

        if (in_array('complaint_type', $types)) {
            /**
             * load model
             */
            if (!$this->modelComplaintType) {
                $this->modelComplaintType = new Model_ComplaintType();
            }

            $booking = $this->modelComplaintType->getById($row['complaint_type_id']);
            $row['complaint_type'] = $booking;
        }

        if (in_array('complaint_discussions', $types)) {
            /**
             * load model
             */
            if (!$this->modelComplaintDiscussion) {
                $this->modelComplaintDiscussion = new Model_ComplaintDiscussion();
            }
            if (!$this->modelUser) {
                $this->modelUser = new Model_User();
            }

            $complaintDiscussions = $this->modelComplaintDiscussion->getByComplaintId($row['complaint_id'], 'created DESC');
            foreach ($complaintDiscussions as &$complaintDiscussion) {
                $complaintDiscussion['user'] = $this->modelUser->getById($complaintDiscussion['user_id']);
            }

            $row['complaint_discussions'] = $complaintDiscussions;
        }
		
		if (in_array('complaint_photo_discussions', $types)) {
            /**
             * load model
             */
            if (!$this->modelComplaintDiscussion) {
                $this->modelComplaintDiscussion = new Model_ComplaintDiscussion();
            }
            if (!$this->modelUser) {
                $this->modelUser = new Model_User();
            }
			

            $complaintDiscussions = $this->modelComplaintDiscussion->getByComplaintId($row['complaint_id'], 'created DESC');
			$GroupscomplaintImageDiscussions = $this->modelComplaintDiscussion->getByComplaintId($row['complaint_id'], 'created DESC', array('groups' => 'groups'));

			$complaintDiscussions = array_merge($complaintDiscussions, $GroupscomplaintImageDiscussions);
			array_multisort($complaintDiscussions, SORT_DESC);
            
			foreach ($complaintDiscussions as &$complaintDiscussion) {
                $complaintDiscussion['user'] = $this->modelUser->getById($complaintDiscussion['user_id']);
            }

            $row['complaint_photo_discussions'] = $complaintDiscussions;
        }
		
		if (in_array('name_email_contractors', $types)) {
            /**
             * load model
             */
            if (!$this->modelUser) {
                $this->modelUser = new Model_User();
            }
            if (!$this->modelContractorServiceBooking) {
                $this->modelContractorServiceBooking = new Model_ContractorServiceBooking();
            }

            $allContractorServiceBooking = $this->modelContractorServiceBooking->getByBookingId($row['booking_id']);

            $contractors = array();
            if ($allContractorServiceBooking) {
                foreach ($allContractorServiceBooking as $bookingService) {
                    $user = $this->modelUser->getById($bookingService['contractor_id']);
                    $contractors[$bookingService['contractor_id']]['email'] = $user['email1'];
                    $contractors[$bookingService['contractor_id']]['display_name'] = $user['display_name'];
                }
            }
            $row['name_email_contractors'] = $contractors;
        }

		
        if (in_array('email_contractors', $types)) {
            /**
             * load model
             */
            if (!$this->modelUser) {
                $this->modelUser = new Model_User();
            }
            if (!$this->modelContractorServiceBooking) {
                $this->modelContractorServiceBooking = new Model_ContractorServiceBooking();
            }

            $allContractorServiceBooking = $this->modelContractorServiceBooking->getByBookingId($row['booking_id']);

            $contractors = array();
            if ($allContractorServiceBooking) {
                foreach ($allContractorServiceBooking as $bookingService) {
                    $user = $this->modelUser->getById($bookingService['contractor_id']);
                    $contractors[$bookingService['contractor_id']] = $user['email1'];
                }
            }
            $row['email_contractors'] = $contractors;
        }

        if (in_array('customer', $types)) {
            /**
             * load model
             */
            if (!$this->modelCustomer) {
                $this->modelCustomer = new Model_Customer();
            }
            if (!$this->modelBooking) {
                $this->modelBooking = new Model_Booking();
            }

            $booking = $this->modelBooking->getById($row['booking_id']);

            $row['customer'] = $this->modelCustomer->getById($booking['customer_id']);
            $row['customer_name'] = get_customer_name($row['customer']);
        }
    }

    public function getFullTextComplaintByBookingId($bookingId) {

        $complaints = $this->getByBookingId($bookingId);

        $complaint_array = array();
        foreach ($complaints as $complaint) {
            if ($complaint['complaint_num']) {
                $complaint_array[] = trim($complaint['complaint_num']);
            }
            if ($complaint['comment']) {
                $complaint_array[] = trim($complaint['comment']);
            }
        }

        if ($complaint_array) {
            $complaint_array = implode(' ', $complaint_array);
        }

        return $complaint_array;
    }

    public function updateFullTextSearch($complaintId) {

        $complaint = $this->getById($complaintId);

        $fullTextSearch = array();

        // complaint     
        $fullTextSearch[] = trim($complaint['complaint_num']);
        $fullTextSearch[] = trim($complaint['comment']);

        // booking
        $model_Booking = new Model_Booking();
        $modelBookingStatus = new Model_BookingStatus();
        $bookingId = $complaint['booking_id'];
        $booking = $model_Booking->getById($bookingId);
        $fullTextSearch[] = trim($booking['booking_num']);
        $fullTextSearch[] = trim($booking['title']);
        $fullTextSearch[] = trim($booking['description']);
        $status = $modelBookingStatus->getById($booking['status_id']);
        $fullTextSearch[] = trim($status['name']);


        // address
        $modelBookingAddress = new Model_BookingAddress();
        $bookingAddress = $modelBookingAddress->getByBookingId($bookingId);
        $fullTextSearch[] = trim(get_line_address($bookingAddress));

        //customer
        $modelCustomer = new Model_Customer();
        $fullTextCustomer = $modelCustomer->getFullTextCustomerContacts($booking['customer_id']);
        $fullTextSearch[] = trim($fullTextCustomer);

        // services
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $fullTextContractorServiceBooking = $modelContractorServiceBooking->getFullTextContractorServiceBooking($bookingId);
        if ($fullTextContractorServiceBooking) {
            $fullTextSearch[] = trim($fullTextContractorServiceBooking);
        }

        //inquiry
        if (isset($booking['original_inquiry_id']) && $booking['original_inquiry_id']) {
            $modelInquiry = new Model_Inquiry();
            $Inquiry = $modelInquiry->getById($booking['original_inquiry_id']);
            if ($Inquiry['inquiry_num']) {
                $fullTextSearch[] = trim($Inquiry['inquiry_num']);
            }
        }

        //estimate
        $modelBookingEstimate = new Model_BookingEstimate();
        $estimate = $modelBookingEstimate->getByBookingId($bookingId);
        if (isset($estimate['estimate_num']) && $estimate['estimate_num']) {
            $fullTextSearch[] = trim($estimate['estimate_num']);
        }

        //invoice
        $modelBookingInvoice = new Model_BookingInvoice();
        $invoice = $modelBookingInvoice->getByBookingId($bookingId);
        if (isset($invoice['invoice_num']) && $invoice['invoice_num']) {
            $fullTextSearch[] = trim($invoice['invoice_num']);
        }
        if (isset($invoice['invoice_type']) && $invoice['invoice_type']) {
            $fullTextSearch[] = trim($invoice['invoice_type']);
        }


        $data = array();
        $data['full_text_search'] = implode(' ', $fullTextSearch);

        $this->updateById($complaintId, $data);
    }

    public function deleteRelatedComplaint($id) {

        //delete data from complaint_discussion
        $this->getAdapter()->delete('complaint_discussion', "complaint_id = '{$id}'");
    }

    public function sendComplaintAsEmailToContractor($complaintId) {

        $complaint = $this->getById($complaintId);

        $this->fill($complaint, array('booking', 'name_email_contractors', 'customer', 'complaint_type'));

        $modelUser = new Model_User();
        $user = $modelUser->getById($complaint['booking']['created_by']);

        $template_params = array(
		    
            //booking
            '{booking_num}' => $complaint['booking']['booking_num'],
            '{booking_created}' => date('d/m/Y', $complaint['booking']['created']),
            '{booking_created_by}' => ucwords($user['username']),
            '{booking_start}' => date("F j, Y, g:i a", strtotime($complaint['booking']['booking_start'])),
			'{booking_end_date}' => date("F j, Y, g:i a", strtotime($complaint['booking']['booking_end'])),
            //complaint
            '{complaint}' => $complaint['comment'],
            '{complaint_num}' => $complaint['complaint_num'],
            '{complaint_type}' => $complaint['complaint_type']['name'],
            '{complaint_view}' => $this->getComplaintViewAsPlaceHolder($complaintId),
            '{complaint_discussion_view}' => $this->getComplaintViewDiscussionAsPlaceHolder($complaintId),
            //customer
            '{customer_name}' => get_customer_name($complaint['customer']),
            '{customer_first_name}' => isset($complaint['customer']['first_name']) && $complaint['customer']['first_name'] ? ucwords($complaint['customer']['first_name']) : '',
            '{customer_last_name}' => isset($complaint['customer']['last_name']) && $complaint['customer']['last_name'] ? ' ' . ucwords($complaint['customer']['last_name']) : ''
        );

        foreach ($complaint['name_email_contractors'] as $contractor) {
            $email_log = array('reference_id' => $complaint['complaint_id'], 'type' => 'complaint');
			$template_params['{contractor_first_name}'] = $contractor['display_name'];
            EmailNotification::sendEmail(array('to' => $contractor['email']), 'send_complaint_as_email_to_contractor ', $template_params, $email_log, $complaint['booking']['company_id']);
        }
    }

    public function getComplaintViewAsPlaceHolder($complaintId) {
        $complaint = $this->getById($complaintId);
        $this->fill($complaint, array('complaint_type'));
        $view = new Zend_View();
        $view->setScriptPath(APPLICATION_PATH . '/modules/complaint/views/scripts/index');
        $view->complaint = $complaint;
        $bodyComplaint = $view->render('complaint.phtml');
        return $bodyComplaint;
    }

    /*public function getComplaintViewDiscussionAsPlaceHolder($complaintId) {
        $bodyComplaintDiscussion = "";
        $complaint = $this->getById($complaintId);
        $this->fill($complaint, array('complaint_discussions'));
        if (!empty($complaint['complaint_discussions'])) {
            $view = new Zend_View();
            $view->setScriptPath(APPLICATION_PATH . '/modules/complaint/views/scripts/index');
            $view->complaint = $complaint;
            $bodyComplaintDiscussion = $view->render('complaint_discussion.phtml');
        }

        return $bodyComplaintDiscussion;
    }*/
	
	public function getComplaintViewDiscussionAsPlaceHolder($complaintId , $type = 'old') {
        $bodyComplaintDiscussion = "";
		$router = Zend_Controller_Front::getInstance()->getRouter();
        $complaint = $this->getById($complaintId);
		if($type == 'new'){		 
		  $this->fill($complaint, array('complaint_photo_discussions'));
		  $complaint_discussion = $complaint['complaint_photo_discussions'];
		}else{
		 $this->fill($complaint, array('complaint_discussions'));
		 $complaint_discussion = $complaint['complaint_discussions'];
		}
        if (!empty($complaint_discussion)){
            $view = new Zend_View();
            $view->setScriptPath(APPLICATION_PATH . '/modules/complaint/views/scripts/index');
            $view->complaint = $complaint;
			if($type == 'new'){
			 $view->complaintLink = $router->assemble(array('id'=>$complaintId), 'complaintView');
			 $bodyComplaintDiscussion = $view->render('complaint_discussion_new.phtml');
			}else{
            $bodyComplaintDiscussion = $view->render('complaint_discussion.phtml');
			}
        }
		
        return $bodyComplaintDiscussion;
    }
	

    public function getLastComplaintByBookingId($booking_id){
        $select = $this->getAdapter()->select();
        $select->from(array('com' => $this->_name));
        $select->join(array('u'=> 'user') , 'u.user_id = com.user_id' , 'username');
        $select->where("booking_id = {$booking_id} ");
        $select->order("complaint_id DESC");
        $select->limit(1);
        //echo $select->__toString();

        return $this->getAdapter()->fetchRow($select);
    }
	
	
	public function countUapprovedCompalintsForContractor($ee){
		
			$unapprovedComplaint = $this->getAll(array('is_approved' => 0,'contractor_id'=>$ee));
			return count($unapprovedComplaint);
		}
		
	 public function getComplaintCountForContractor($dd){
  
		$select = $this->getAdapter()->select();
        $select->from(array('com' => $this->_name));
        $select->distinct();
     
        

	  $select->join(array('csb' => 'contractor_service_booking'), 'com.booking_id = csb.booking_id', '');
	  $select->where("csb.contractor_id = {$dd}");
		$select->where("com.complaint_status = 'open'");
		 
	  //echo $select->__toString();
        return count($this->getAdapter()->fetchAll($select));
  
	}

}