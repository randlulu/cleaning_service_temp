<?php

class Model_BookingDueDate extends Zend_Db_Table_Abstract {

    protected $_name = 'booking_due_date';

   /**
    * update table row according to the assigned id
    * 
    * @param int $id
    * @param array $data
    * @return boolean
    */ 
   public function updateById($id, $data) {
        $id = (int) $id;
        $updateID= parent::update($data, "id = '{$id}'");
		//D.A 30/09/2015 Remove Invoice Cache
		if($updateID){	
			require_once 'Zend/Cache.php';
			$company_id = CheckAuth::getCompanySession();		
			$bookingDueDate = $this->getById($id);		
			$modelBookingInvoice = new Model_BookingInvoice();
			$invoice = $modelBookingInvoice->getByBookingId($bookingDueDate['booking_id']);
			$invoiceParamsCacheID= $invoice['id'].'_invoiceParams';
			$invoiceViewDir=get_config('cache').'/'.'invoicesView'.'/'.$company_id;	
			if (!is_dir($invoiceViewDir)) {
				mkdir($invoiceViewDir, 0777, true);
			}			
			$invoiceFrontEndOption= array('lifetime'=> NULL,
			'automatic_serialization'=> true);
			$invoiceBackendOptions = array('cache_dir'=>$invoiceViewDir );
			$invoiceCache = Zend_Cache::factory('Core','File',$invoiceFrontEndOption,$invoiceBackendOptions);			
			$invoiceCache->remove($invoiceParamsCacheID);	
		}	
		return $updateID;
    }

    /**
     *get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     *get table row according to the assigned BookingId
     * 
     * @param int $id
     * @return array
     */
    public function getByBookingId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

}
