<?php

class Model_ContractorServiceBooking extends Zend_Db_Table_Abstract {

    protected $_name = 'contractor_service_booking';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order($order);

        if ($filters) {
            if (!empty($filters['booking_id'])) {
                $select->where("booking_id = {$filters['booking_id']}");
            }
            if (!empty($filters['contractor_id'])) {
                $select->where("contractor_id = {$filters['contractor_id']}");
            }
            if (!empty($filters['service_id'])) {
                $select->where("service_id = {$filters['service_id']}");
            }

            if (isset($filters['is_accepted'])) {
                if ($filters['is_accepted'] == "all") {
                    $select->where("is_accepted = 0 OR is_accepted = 1");
                } elseif ($filters['is_accepted'] == "accepted") {
                    $select->where("is_accepted = 1");
                } elseif ($filters['is_accepted'] == "not_accepted") {
                    $select->where("is_accepted = 0");
                }
            }

            if (isset($filters['not_accepted_or_rejected'])) {
                $select->where("is_accepted = 0 AND is_rejected = 0");
            }

            if (isset($filters['is_rejected'])) {
                if ($filters['is_rejected'] == "all") {
                    $select->where("is_rejected = 0 OR is_rejected = 1");
                } elseif ($filters['is_rejected'] == "rejected") {
                    $select->where("is_rejected = 1");
                } elseif ($filters['is_rejected'] == "not_rejected") {
                    $select->where("is_rejected = 0");
                }
            }
        }
        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }
	
	
	public function updateDistanceByBookingId($booking_id){
		///get booking address by booking id
		
		$modelBookingAddress = new Model_BookingAddress();
		$modelUser = new Model_User();
		
		$booking_address = $modelBookingAddress->getByBookingId($booking_id);
		//print_r($booking_address);
		$booking_services = $this->getByBookingId($booking_id);
		$success = 0;
		foreach($booking_services as $booking_service){
			
			$id = $booking_service['id'];
			$contractor_id = $booking_service['contractor_id'];
			$contractor = $modelUser->getById($contractor_id);
			$distance = $modelBookingAddress->getDistanceByTwoAddress($booking_address,$contractor);
			
			$success = parent::update(array('distance'=> $distance), "id = {$id}");
		}
		return $success;
	}
	
	public function updateDistanceForAllBooking(){
		///get booking address by booking id
		echo 'in update all';
		$modelBooking = new Model_Booking();
		$filters = array();
		$filters['distance'] = 1 ;
		
		$bookings = $modelBooking->getAll($filters);
		foreach($bookings as $booking){
			$this->updateDistanceByBookingId($booking['booking_id']);
		}
		
	}
	
	
    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateById($id, $data,$log_id = 0) {
        $id = (int) $id;

        if (!empty($data['contractor_id']) || !empty($data['service_id'])) {

            $oldData = $this->getById($id);

            if (!empty($data['contractor_id']) && $oldData['contractor_id'] != $data['contractor_id']) {
                $data['is_accepted'] = 0;
                $data['is_rejected'] = 0;
            }

            if (!empty($data['service_id']) && $oldData['service_id'] != $data['service_id']) {
                $data['is_accepted'] = 0;
                $data['is_rejected'] = 0;
            }
        }

        $success = parent::update($data, "id = '{$id}'");

        /**
         * get all record and insert it in log
         */
        $modelContractorServiceBookingLog = new Model_ContractorServiceBookingLog();
        $modelContractorServiceBookingLog->addContractorServiceBookingLog($id,0,$log_id);

        return $success;
    }

    public function insert(array $data) {
        $id = parent::insert($data);
        
        /**
         * get all record and insert it in log
         */
        $modelContractorServiceBookingLog = new Model_ContractorServiceBookingLog();
        $modelContractorServiceBookingLog->addContractorServiceBookingLog($id);
        
        return $id;
    }

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateByServiceId($id, $data) {
        $id = (int) $id;
        return parent::update($data, "service_id = '{$id}'");
    }

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateByBookingId($id, $data) {
        $id = (int) $id;
        return parent::update($data, "booking_id = '{$id}'");
    }

    /**
     * delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $deleted = 0;
        $id = (int) $id;
		/**
         * get service record and insert it in log
         */
		$service = $this->getById($id);
		if($service){
			$modelContractorServiceBookingLog = new Model_ContractorServiceBookingLog();
			$logId = $modelContractorServiceBookingLog->addContractorServiceBookingLog($id);
			////delete this record
			$deleted = parent::delete("id= '{$id}'");
			//// add record in user log table
			if($deleted){
				$item_type = 'contractor_service_booking_by_id';
				$event = 'deleted';
				$modelLogUser = new Model_LogUser();
				$modelLogUser->addUserLogEvent($logId, $item_type, $event);
			}
		}
        return $deleted;
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id= '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }
	
	/*
	 try two get all data by select object
	*/
	
	 public function getAll2($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order($order);

        if ($filters) {
            if (!empty($filters['booking_id'])) {
                $select->where("booking_id = {$filters['booking_id']}");
            }
            if (!empty($filters['contractor_id'])) {
                $select->where("contractor_id = {$filters['contractor_id']}");
            }
            if (!empty($filters['service_id'])) {
                $select->where("service_id = {$filters['service_id']}");
            }

            if (isset($filters['is_accepted'])) {
                if ($filters['is_accepted'] == "all") {
                    $select->where("is_accepted = 0 OR is_accepted = 1");
                } elseif ($filters['is_accepted'] == "accepted") {
                    $select->where("is_accepted = 1");
                } elseif ($filters['is_accepted'] == "not_accepted") {
                    $select->where("is_accepted = 0");
                }
            }

            if (isset($filters['not_accepted_or_rejected'])) {
                $select->where("is_accepted = 0 AND is_rejected = 0");
            }

            if (isset($filters['is_rejected'])) {
                if ($filters['is_rejected'] == "all") {
                    $select->where("is_rejected = 0 OR is_rejected = 1");
                } elseif ($filters['is_rejected'] == "rejected") {
                    $select->where("is_rejected = 1");
                } elseif ($filters['is_rejected'] == "not_rejected") {
                    $select->where("is_rejected = 0");
                }
            }
        }
        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

	
	////****by islam test function 
	public function test($filters = array()) {
        		
		$select = $this->getAdapter()->select();
        $select->from(array('csb' => $this->_name),array('contractor_id'));
		$select->joinLeft(array('r' => 'refund'),'r.booking_id= csb.booking_id and (r.contractor_id = csb.contractor_id or r.contractor_id = 0)',array('unapproved_refund'=> new Zend_Db_Expr("IF(r.is_approved=0,(SUM(r.amount) / COUNT(r.refund_id) * COUNT(DISTINCT r.refund_id)),0)"),'approved_refund'=> new Zend_Db_Expr("IF(r.is_approved=1,(SUM(r.amount) / COUNT(r.refund_id) * COUNT(DISTINCT r.refund_id)),0)")));
		$select->joinLeft(array('p' => 'payment'),'p.booking_id= csb.booking_id and p.contractor_id= csb.contractor_id',array('payment_amount'=>'p.amount','approved_payment'=> new Zend_Db_Expr("IF(p.is_approved=1,(SUM(`p`.`amount`) / COUNT(`p`.`payment_id`) * COUNT(DISTINCT `p`.`payment_id`)),0)"),'unapproved_payment'=> new Zend_Db_Expr("IF(p.is_approved=0,(SUM(`p`.`amount`) / COUNT(`p`.`payment_id`) * COUNT(DISTINCT `p`.`payment_id`)),0)")));
		$select->joinLeft(array('pt' => 'payment_type'),'pt.id = p.payment_type_id',array('payment_type'));
		$select->joinLeft(array('bcp' => 'booking_contractor_payment'),'bcp.booking_id= csb.booking_id and bcp.contractor_id= csb.contractor_id',array('booking_contractor_payment_id'=>'id','booking_id','payment_to_contractor','paid_status'=> new Zend_Db_Expr("IF(bcp.id!='NULL','paid','unpaid')"),'paid_to_contractor'=> 'bcp.payment_to_contractor'));
		$select->joinLeft(array('bi' => 'booking_invoice'),'bi.booking_id= csb.booking_id',array('booking_invoice_id'=>'id','INV#'=>'bi.invoice_num'));
		$select->joinLeft(array('b' => 'booking'),'b.booking_id= csb.booking_id and b.is_deleted != 1',array('booking_id','qoute','convert_status','status_id','start_date'=>'b.booking_start','job#'=>'b.booking_num','total_discount','call_out_fee'));
		$select->joinLeft(array('s' => 'service'),'s.service_id= csb.service_id',array('job_nature'=>'s.service_name'));
		$select->joinLeft(array('u' => 'user'),'u.user_id = csb.contractor_id',array('contractor'=>'u.username','contractor_email1'=> 'u.email1'));
		$select->joinLeft(array('badd' => 'booking_address'),'badd.booking_id= csb.booking_id',array('address'=>new Zend_Db_Expr("concat(badd.street_number,' - ',badd.street_address, ' - ',badd.state,' - ', badd.postcode)"),'state'=>'badd.state'));
		$select->joinLeft(array('bs' => 'booking_status'),'bs.booking_status_id = b.status_id',array('job_status'=>'bs.name'));
		$select->joinLeft(array('bookedByu' => 'user'),'bookedByu.user_id = b.created_by',array('booked_by'=>'bookedByu.username'));
		$select->joinLeft(array('com' => 'complaint'),'com.booking_id= csb.booking_id',array('complaint_id','complaint_num','complaint_status'=>'com.complaint_status'));
		$select->joinLeft(array('bdis' => 'booking_discussion'),'bdis.booking_id= csb.booking_id',array('comments'=>'bdis.user_message'));
		$select->joinLeft(array('cshb' => 'contractor_share_booking'),'cshb.booking_id= csb.booking_id and cshb.contractor_id = csb.contractor_id',array('contractor_share'));
		$select->joinLeft(array('ptoc' => 'payment_to_contractor'),'ptoc.payment_to_contractor_id= bcp.payment_to_contractor_id and ptoc.contractor_id = bcp.contractor_id',array('payment_to_contractor_id','invoice_total_amount','cash_paid','contractor_INV'=>'contractor_invoice_num','amount_calculated'=>'ptoc.amount_calculated','amount_paid'=>'ptoc.amount_paid'));
		$select->joinLeft(array('poffice' => 'payment'),'poffice.booking_id= csb.booking_id and poffice.contractor_id= csb.contractor_id and poffice.payment_type_id != 1',array('office_payment'=>("SUM(`poffice`.`amount`) / COUNT(`poffice`.`payment_id`) * COUNT(DISTINCT `poffice`.`payment_id`)")));
		$select->joinLeft(array('pcash' => 'payment'),'pcash.booking_id= csb.booking_id and pcash.contractor_id= csb.contractor_id and pcash.payment_type_id = 1',array('cash_payment'=>("SUM(`pcash`.`amount`) / COUNT(`pcash`.`payment_id`) * COUNT(DISTINCT `pcash`.`payment_id`)")));
		
		//$select->where('ptoc.is_deleted= 0'); 
		
		 if(isset($filters['contractor_id']) && !empty($filters['contractor_id'])){
		    $contractor_id=(int)$filters['contractor_id'];
			$select->where('csb.contractor_id= '.$contractor_id);  
		 }
		 if(isset($filters['booking_start_between']) && !empty($filters['booking_start_between'])){
			$select->where("b.booking_start >= '".$filters['booking_start_between']."'");
		 }
		 
		 if(isset($filters['booking_end_between']) && !empty($filters['booking_end_between'])){
			$select->where("b.booking_end <= '".$filters['booking_end_between']."'");
		 }
		 
		 /*if(isset($filters['booking_start_between']) && !empty($filters['booking_start_between']) && isset($filters['booking_end_between']) && !empty($filters['booking_end_between']) ){
			$select->where("b.booking_start between '".$filters['booking_start_between']."' and '".$filters['booking_end_between']."'");
		 }*/
		 
		 if(!empty($filters['Inv'])){
			$select->where("ptoc.contractor_invoice_num='".$filters['Inv']."'");
		 }
		 
		  if(!empty($filters['customer_inv'])){
			$select->where("bi.invoice_num ='".$filters['customer_inv']."'");
		 }
		 //////payment to contractor status ==1 (paid), ==2 (unpaid)
		 if($filters['paid_status']==1){
			$select->where("bcp.id IS NOT NULL");
		 }
		 if($filters['paid_status']==2){
			$select->where("bcp.id is NULL");
		 }
		 /////// customer_payment_status
		 if(isset($filters['customer_payment_status']) && $filters['customer_payment_status']==1){
			$select->where("p.payment_id IS NOT NULL");
		 }
		 if(isset($filters['customer_payment_status']) && $filters['customer_payment_status']==2){
			$select->where("p.payment_id IS NULL");
		 }
		 ////// booking status
		 if(!empty($filters['status_id'])){
		 $status_id= (int)$filters['status_id'];
		 /////status_id =18 means completed and failed booking together 
		 /////
			if($status_id==18){
			$model_bookingStatus = new Model_BookingStatus();
			$completed_stats_data=$model_bookingStatus->getByStatusName('COMPLETED');
			$completed_stats_id=(int)$completed_stats_data['booking_status_id'];
			
			$failed_stats_data=$model_bookingStatus->getByStatusName('FAILED');
			$failed_stats_id=(int)$failed_stats_data['booking_status_id'];
			
			 $select->where('b.status_id ='.$completed_stats_id .' or b.status_id ='.$failed_stats_id); 
				
			}
			else{
			$select->where('b.status_id ='.$status_id); 
			}
		   
		 }
		 
		 //////$booking_ids_of_invoice_number
		 if(isset($filters['booking_ids_of_invoice_number']) && !empty($filters['contractor_id'])){
			$select->where('csb.booking_id IN (?)', $filters['booking_ids_of_invoice_number']);  
		 }
		///// payment_to_contractor_id
		if(!empty($filters['payment_to_contractor_id'])){
			$select->where("ptoc.payment_to_contractor_id ='".$filters['payment_to_contractor_id']."'");
		 }
		 
		 
		//$select->group('p.payment_id');
		//$select->group('p.payment_type_id');
		$select->group('csb.booking_id');
		$select->order('b.booking_start');
		//$sql = $select->__toString();
		//echo "$sql\n";
		$bookings = $this->getAdapter()->fetchAll($select);
		foreach($bookings as &$booking){
				$contractorId = $booking['contractor_id'];
                $bookingId = $booking['booking_id'];

                $total_contractor_service_qoute_without_gst = $this->getTotalContractorServicesQoute($bookingId, $contractorId);
                
				$totalDiscount = !empty($booking['total_discount']) ? $booking['total_discount'] : 0;
				$callOutFee = !empty($booking['call_out_fee']) ? $booking['call_out_fee'] : 0;
				
				/*$gst = $total_contractor_service_qoute_without_gst * get_config('gst_tax');;
                $booking['total_contractor_service_qoute'] = $total_contractor_service_qoute_without_gst + $gst;
				*/
				$gst = ($total_contractor_service_qoute_without_gst + $callOutFee - $totalDiscount) * get_config('gst_tax');
				$booking['total_contractor_service_qoute'] = $total_contractor_service_qoute_without_gst + $callOutFee + $gst - $totalDiscount;
            
		}
		//print_r($bookings);
		return $bookings;
		//return $this->getAdapter()->fetchAll($select);
      
    }
	

    /**
     * delete table row according to the assigned bookingId
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteBybookingId($id) {
		$deleted = 0;
        $booking_id = (int) $id;
		$booking_services = $this->getByBookingId($booking_id);
		if($booking_services){
			$modelContractorServiceBookingLog = new Model_ContractorServiceBookingLog();
			$modelLogUser = new Model_LogUser();
			foreach($booking_services as $booking_service){
				print_r($booking_service);
				/**
				 * get service record and insert it in log
				 */
				$logId = $modelContractorServiceBookingLog->addContractorServiceBookingLog($booking_service['id']);
				////delete this record
				$deleted = parent::delete("id= '{$booking_service['id']}'");
				//// add record in user log table
				if($deleted){
					$item_type = 'booking_service_by_booking_id';
					$event = 'deleted';
					
					$modelLogUser->addUserLogEvent($logId, $item_type, $event);
				}
			}
		}
		return $deleted;
    }

    /**
     * get table row according to the assigned BookingId
     * 
     * @param int $bookingId
     * @return array
     */
    public function getByBookingId($bookingId) {
        $bookingId = (int) $bookingId;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_id= '{$bookingId}'");

        return $this->getAdapter()->fetchAll($select);
    }
	
	public function getContractorIdsByBookingId($bookingId) {
        $bookingId = (int) $bookingId;
        $select = $this->getAdapter()->select();
        $select->from(array('csb'=>$this->_name), 'contractor_id');
        $select->join(array('u'=>'user'), 'csb.contractor_id= u.user_id',array('u.username'));
        $select->where("csb.booking_id= '{$bookingId}'");

        return $this->getAdapter()->fetchAll($select);
    }
	
	public function getContractorNamesByBookingId($bookingId) {
        $bookingId = (int) $bookingId;
        $select = $this->getAdapter()->select();
        $select->from(array('csb'=>$this->_name),array())
				->distinct();
        $select->join(array('u'=>'user'), 'csb.contractor_id= u.user_id','u.username');
        $select->where("csb.booking_id= '{$bookingId}'");
		$rows = $this->getAdapter()->fetchAll($select);
		$result = array();
		foreach($rows as $row){
			$result[] = $row['username'];
		}
        return $result;
    }

    /**
     * get table row according to the assigned contractor Id
     * 
     * @param int $contractorId
     * @return array
     */
    public function getByContractorId($contractorId) {
        $contractorId = (int) $contractorId;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("contractor_id= '{$contractorId}'");

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * get the acceptrd services in the booking of the logged user
     *
     * @param int $bookingId
     * @return array 
     */
    public function getByBookingIdAndContractorId($bookingId, $contractorId) {
        $bookingId = (int) $bookingId;
        $contractorId = (int) $contractorId;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_id= '{$bookingId}'");
        $select->where("contractor_id= '{$contractorId}'");

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * get By Booking And Contractor Not Rejected Bookings
     * 
     * @param int $bookingId
     * @param int $contractorId
     * @return array 
     */
    public function getNotRejectedOrAcceptedBookings($bookingId, $contractorId) {
        $bookingId = (int) $bookingId;
        $contractorId = (int) $contractorId;

        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("contractor_id= '{$contractorId}' AND booking_id='{$bookingId}' AND is_accepted = 0 AND is_rejected = 0");

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * get By Booking And Contractor Not Rejected Bookings
     * 
     * @param int $bookingId
     * @param int $contractorId
     * @return array 
     */
    public function getNotAcceptedBookings($bookingId, $contractorId) {
        $bookingId = (int) $bookingId;
        $contractorId = (int) $contractorId;

        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("contractor_id= '{$contractorId}' AND booking_id='{$bookingId}' AND is_accepted = 0");

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * get table row according to the assigned BookingId Service and Clone
     * 
     * @param int $bookingId
     * @param int $service_id
     * @param int $clone
     * @return array
     */
    public function getByBookingAndServiceAndClone($bookingId, $service_id, $clone) {
        $bookingId = (int) $bookingId;
        $service_id = (int) $service_id;
        $clone = (int) $clone;

        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("service_id='{$service_id}' AND booking_id='{$bookingId}' AND clone='{$clone}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the assigned BookingId Service and Contractor
     * 
     * @param int $bookingId
     * @param int $service_id
     * @param int $clone
     * @return array
     */
    public function getByBookingAndServiceAndCloneAndContractor($bookingId, $service_id, $clone, $contractorId) {
        $bookingId = (int) $bookingId;
        $service_id = (int) $service_id;
        $contractorId = (int) $contractorId;
        $clone = (int) $clone;

        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("service_id='{$service_id}' AND booking_id='{$bookingId}' AND clone='{$clone}' AND contractor_id='{$contractorId}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * delete table row according to the assigned BookingId Service and Clone
     * 
     * @param int $bookingId
     * @param int $service_id
     * @param int $clone
     * @return boolean 
     */
    public function deleteByBookingAndServiceAndClone($bookingId, $service_id, $clone) {
        $deleted = 0;
        $bookingId = (int) $bookingId;
        $service_id = (int) $service_id;
        $clone = (int) $clone;
		$booking_service = $this->getByBookingAndServiceAndClone($bookingId, $service_id, $clone);
		
		/**
		 * get service record and insert it in log
		 */
		if($booking_service ){
			$modelContractorServiceBookingLog = new Model_ContractorServiceBookingLog();
			$logId = $modelContractorServiceBookingLog->addContractorServiceBookingLog($booking_service['id']);
			////delete this record
			$deleted = parent::delete("id= '{$booking_service['id']}'");
			//// add record in user log table
			if($deleted){
				$item_type = 'booking_service_by_booking_id_and_service_and_clone';
				$event = 'deleted';
				$modelLogUser = new Model_LogUser();
				$modelLogUser->addUserLogEvent($logId, $item_type, $event);
			}
		}
        return $deleted;
		
	}

    /**
     * delete table row according to the assigned BookingId Services 
     * 
     * @param int $bookingId
     * @param array $services
     * @return boolean 
     */
    public function deleteByBookingIdAndServices($bookingId, $services = array()) {
        $bookingId = (int) $bookingId;
        $oldServices = $this->getByBookingId($bookingId);
        $modelServiceAttributeValue = new Model_ServiceAttributeValue();

        if (!empty($oldServices)) {
            foreach ($oldServices as &$oldService) {
                $serviceId = $oldService['service_id'];
                $clone = $oldService['clone'];

                $oldService = $serviceId . '_' . $clone;
            }
        }

        if (!empty($services)) {
            foreach ($services as &$service) {
                $serviceId = $service['service_id'];
                $clone = $service['clone'];

                $service = $serviceId . '_' . $clone;
            }
        }

        //
        //delete
        //
        $toDeleteServices = array_diff($oldServices, $services);
        if ($toDeleteServices) {

            foreach ($toDeleteServices as $toDeleteService) {

                $serviceAndClone = explode('_', $toDeleteService);
                $serviceId = (int) (isset($serviceAndClone[0]) ? $serviceAndClone[0] : 0);
                $clone = (int) (isset($serviceAndClone[1]) && $serviceAndClone[1] ? $serviceAndClone[1] : 0);

                //delete By Service And Booking And Clone
                $this->deleteByBookingAndServiceAndClone($bookingId, $serviceId, $clone);

                // Delete Attribute Value
                $modelServiceAttributeValue->deleteByBookingIdServiceIdAndClone($bookingId, $serviceId, $clone);
            }
        }
    }

    /*
     * setServicesToBooking
     */

    public function setServicesToBooking($bookingId, $services = array(), $log_id = 0) {

        //Get Zend Request
        $request = Zend_Controller_Front::getInstance()->getRequest();

        //
        // delete Services not in the list
        //
        $bookingId = (int) $bookingId;
        $allServices = array();
        if (!empty($services)) {
            foreach ($services AS $service) {
                $serviceAndClone = explode('_', $service);
                $serviceId = (int) (isset($serviceAndClone[0]) ? $serviceAndClone[0] : 0);
                $clone = (int) (isset($serviceAndClone[1]) && $serviceAndClone[1] ? $serviceAndClone[1] : 0);
                $contractorId = (int) $request->getParam('contractor_' . $serviceId . ($clone ? '_' . $clone : ''));
                $considerMinPrice = (int) $request->getParam('consider_min_price_' . $serviceId . ($clone ? '_' . $clone : ''));

                $allServices[] = array(
                    'service_id' => $serviceId,
                    'clone' => $clone,
                    'contractor_id' => $contractorId,
                    'consider_min_price' => $considerMinPrice,
                    'booking_id' => $bookingId
                );
            }
        }

        $this->deleteByBookingIdAndServices($bookingId, $allServices);

        //
        // assign the Services to Booking
        //
        foreach ($allServices as $allService) {
            $this->assignServicesToBooking($allService,$log_id);
        }
    }

	
	/////OFFLINE
		public function setServicesToBookingOffline($bookingId, $services = array()) {

        //Get Zend Request
        $request = Zend_Controller_Front::getInstance()->getRequest();
		$loggedUser = CheckAuth::getLoggedUser();
        //
        // delete Services not in the list
        //
        $bookingId = (int) $bookingId;
        $allServices = array();
        if (!empty($services)) {
			//print_r($services);
            foreach ($services AS $service) {
				//print_r($service);
               // $serviceAndClone = explode('_', $service);
                $serviceId = (int) (isset($service['service_id']) ? $service['id'] : 0);
                $clone = (int) (isset($service['clone']) && $service['clone'] ? $service['clone'] : 0);
                $contractorId = $loggedUser['user_id'];
                $considerMinPrice = 0;
				//echo 'serviceId   '.$serviceId;
				//echo 'clone  '.$clone;
				
				
                $allServices[] = array(
                    'service_id' => $serviceId,
                    'clone' => $clone,
                    'contractor_id' => $contractorId,
                    'consider_min_price' => $considerMinPrice,
                    'booking_id' => $bookingId
                );
            }
        }
		//print_r($allServices);
		//echo 'doneeeeeeeee';
        $this->deleteByBookingIdAndServices($bookingId, $allServices);

        //
        // assign the Services to Booking
        //
        foreach ($allServices as $allService) {
            $this->assignServicesToBooking($allService);
        }
    }
	
	
	public function changeAttributIfFaildOffline($bookingId, $services) {

        // Load Model

        $modelAttributes = new Model_Attributes();
        $modelServiceAttribute = new Model_ServiceAttribute();
        $modelServiceAttributeValue = new Model_ServiceAttributeValue();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();

        foreach ($services as $service) {

            
			$serviceId = (int) (isset($service['service_id']) ? $service['service_id'] : 0);
            $clone = (int) (isset($service['clone']) && $service['clone'] ? $service['clone'] : 0);
               
            // Quantity
            $quantityAttribute = $modelAttributes->getByVariableName('Quantity');
            $serviceQuantityAttribute = $modelServiceAttribute->getByAttributeIdAndServiceId($quantityAttribute['attribute_id'], $serviceId);
            $oldServiceQuantityValue = $modelServiceAttributeValue->getByBookingIdAndServiceAttributeIdAndClone($bookingId, $serviceQuantityAttribute['service_attribute_id'], $clone);
            $modelServiceAttributeValue->updateById($oldServiceQuantityValue['service_attribute_value_id'], array('value' => ''));

            // Discount

            $discountAttribute = $modelAttributes->getByVariableName('Discount');
            $serviceDiscountAttribute = $modelServiceAttribute->getByAttributeIdAndServiceId($discountAttribute['attribute_id'], $serviceId);
            $oldServiceDiscountValue = $modelServiceAttributeValue->getByBookingIdAndServiceAttributeIdAndClone($bookingId, $serviceDiscountAttribute['service_attribute_id'], $clone);
            $modelServiceAttributeValue->updateById($oldServiceDiscountValue['service_attribute_value_id'], array('value' => ''));

            // Change Min Price to zero

            $booking_service = $modelContractorServiceBooking->getByBookingAndServiceAndClone($bookingId, $serviceId, $clone);
            $modelContractorServiceBooking->updateById($booking_service['id'], $data = array('consider_min_price' => 0));
        }
    }
	
	
    /*
     * assignServicesToBooking
     */

    public function assignServicesToBooking($params,$log_id = 0) {
        
        $bookingServicelink = $this->getByBookingAndServiceAndClone($params['booking_id'], $params['service_id'], $params['clone']);

        $loggedUser = CheckAuth::getLoggedUser();
        if ('contractor' == CheckAuth::getRoleName()) {
            $params['contractor_id'] = $loggedUser['user_id'];
        }
        if ($loggedUser['user_id'] == $params['contractor_id']) {
            $params['is_accepted'] = 1;
        }

        if (!$bookingServicelink) {
            $returnId = $this->insert($params);
        } else {
            $this->updateById($bookingServicelink['id'], $params, $log_id);
            $returnId = $bookingServicelink['id'];
        }

        $modelServiceAttributeValue = new Model_ServiceAttributeValue();
        $modelServiceAttributeValue->addAttributeByBookingIdAndServiceIdAndClone($params['booking_id'], $params['service_id'], $params['clone'], $log_id);
        return $returnId;
    }

    /**
     * get the acceptrd services in the booking of the logged user
     *
     * @param int $bookingId
     * @return array 
     */
    public function getContractorServicesByBookingId($bookingId) {
        $bookingId = (int) $bookingId;

        $loggedUser = CheckAuth::getLoggedUser();
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_id= '{$bookingId}'");

        if (!CheckAuth::checkCredential(array('canSeeAllServices'))) {
            if (CheckAuth::checkCredential(array('canSeeOnlyHisServices'))) {
                $select->where("contractor_id = '{$loggedUser['user_id']}'");
            }
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * get count of tables rows to the assigned filters
     * 
     * @param array $filters
     * @return array
     */
    public function getServicesCount($filters = array()) {


        $loggedUser = CheckAuth::getLoggedUser();
        $companyId = CheckAuth::getCompanySession();

        $select = $this->getAdapter()->select();
        $select->from(array('csb' => $this->_name));

        $select->joinInner(array('b' => 'booking'), 'b.booking_id = csb.booking_id', '');
        $select->where("b.company_id = {$companyId}");
        $select->where('b.is_deleted = 0');

        if (!CheckAuth::checkCredential(array('canSeeAllServices'))) {
            if (CheckAuth::checkCredential(array('canSeeOnlyHisServices'))) {
                $select->where("contractor_id = '{$loggedUser['user_id']}'");
            }
        }

        if ($filters) {

            if (!empty($filters['is_accepted'])) {
                $select->where("csb.is_accepted = '{$filters['is_accepted']}'");
            }

            if (!empty($filters['is_rejected'])) {
                $select->where("csb.is_rejected = '{$filters['is_rejected']}'");
            }

            if (!empty($filters['NotAcceptedAndRejected'])) {
                $select->where("csb.is_accepted = 0 AND csb.is_rejected = 0");
            }
            if (!empty($filters['created_by'])) {
                $userId = (int) $filters['created_by'];
                $select->where("b.created_by = '{$userId}'");
            }
        }

        $sql = $this->getAdapter()->select();
        $sql->from($select, 'COUNT(*)');

        return $this->getAdapter()->fetchOne($sql);
    }

    public function getTotalBookingQoute($bookingId) {
        //
        // load models
        //
        $modelServices = new Model_Services();

        //
        //get all service for this booking 
        //
        $bookingServices = $this->getByBookingId($bookingId);

        $totalQoute = 0;

        if ($bookingServices) {
            foreach ($bookingServices as $bookingService) {
                $serviceId = $bookingService['service_id'];
                $clone = $bookingService['clone'];
                $considerMinPrice = $bookingService['consider_min_price'];

                $service = $modelServices->getById($serviceId);
                $min_price = $service['min_price'];

                $serviceQoute = $this->getServiceBookingQoute($bookingId, $serviceId, $clone);

                if (($serviceQoute < $min_price) && $considerMinPrice === 1) {
                    $totalQoute = $totalQoute + $min_price;
                } else {
                    $totalQoute = $totalQoute + $serviceQoute;
                }
            }
        }

        return $totalQoute;
    }

    public function getServiceBookingQoute($bookingId, $serviceId, $clone = 0) {

        //
        // load models
        //
        $modelAttributeListValue = new Model_AttributeListValue();
        $modelServiceAttribute = new Model_ServiceAttribute();
        $modelServiceAttributeValue = new Model_ServiceAttributeValue();
        $modelServices = new Model_Services();
        $modelAttributes = new Model_Attributes();

        $uniqid = uniqid();
        $serviceQoute = 0;

        $service = $modelServices->getById($serviceId);
        if (preg_match_all('#\[(.*?)\]#', $service['price_equasion'], $matches)) {
            $param = array();
            foreach ($matches[1] as $attributeVariableName) {
                $attribute = $modelAttributes->getByVariableName($attributeVariableName);

                $serviceAttribute = $modelServiceAttribute->getByAttributeIdAndServiceId($attribute['attribute_id'], $serviceId);

                $value = $modelServiceAttributeValue->getByBookingIdAndServiceAttributeIdAndClone($bookingId, $serviceAttribute['service_attribute_id'], $clone);

                if ($attribute['is_list']) {
                    $attributeListValue = $modelAttributeListValue->getById($value['value']);
                    $param[$attributeVariableName] = $attributeListValue['unit_price'];
                } else {
                    $param[$attributeVariableName] = $value['value'];
                }
            }

            $equasion = $service['price_equasion'];

            //build getPrice function
            createPriceEquasionFunction("getPrice_{$uniqid}", $equasion);

            $serviceQoute = call_user_func("getPrice_{$uniqid}", $param);
        }

        return $serviceQoute;
    }

    public function getPriceEquasionToIOS($serviceId, $clone = 0) {

        //
        // load models
        //
        $modelServices = new Model_Services();
        $modelAttributes = new Model_Attributes();

        $service = $modelServices->getById($serviceId);
        $priceEquasion = $service['price_equasion'];
        if (preg_match_all('#\[(.*?)\]#', $service['price_equasion'], $matches)) {
            foreach ($matches[1] as $attributeVariableName) {
                $attribute = $modelAttributes->getByVariableName($attributeVariableName);

                $name = 'attribute_' . $serviceId . $attribute['attribute_id'] . ($clone ? '_' . $clone : '');

                $priceEquasion = str_replace($attributeVariableName, $name, $priceEquasion);
            }
        }

        $priceEquasion .= '=[total_' . $serviceId . ($clone ? '_' . $clone : '') . ']';

        return $priceEquasion;
    }

    public function getTotalServiceBookingQoute($bookingId, $serviceId, $clone = 0) {

        //
        // load models
        //
        $modelServices = new Model_Services();

        $bookingService = $this->getByBookingAndServiceAndClone($bookingId, $serviceId, $clone);
        $considerMinPrice = $bookingService['consider_min_price'];

        $service = $modelServices->getById($serviceId);
        $min_price = $service['min_price'];

        $serviceQoute = $this->getServiceBookingQoute($bookingId, $serviceId, $clone);

        if (($serviceQoute < $min_price) && $considerMinPrice === 1) {
            $serviceQoute = $min_price;
        }

        return $serviceQoute;
    }

    public function getTotalContractorServicesQoute($bookingId, $contractorId) {

        $servicesQoute = 0;

        $contractorServices = $this->getByBookingIdAndContractorId($bookingId, $contractorId);

        foreach ($contractorServices as $contractorService) {
            $serviceQoute = $this->getTotalServiceBookingQoute($bookingId, $contractorService['service_id'], $contractorService['clone']);
            $servicesQoute = $servicesQoute + $serviceQoute;
        }

        return $servicesQoute;
    }

    public function getContractorsBybookingId($bookingId) {
        $contractors = array();
        $contractorServiceBookings = $this->getByBookingId($bookingId);
        if ($contractorServiceBookings) {
            foreach ($contractorServiceBookings as $contractorServiceBooking) {
                $contractors[$contractorServiceBooking['contractor_id']] = $contractorServiceBooking['contractor_id'];
            }
        }

        return $contractors;
    }

    public function getTotalContractorShare($contractorId, $filters = array()) {

        // Load Model
        $modelRefund = new Model_Refund();
        $modelBooking = new Model_Booking();
        $modelContractorInfo = new Model_ContractorInfo();
        $modelContractorShareBooking = new Model_ContractorShareBooking();

        //Get Contractor Info
        $contractorInfo = $modelContractorInfo->getByContractorId($contractorId);

        // get all booking for contractor
        $filters['contractor_id'] = $contractorId;
        $filters['convert_status'] = 'invoice';
        $bookings = $modelBooking->getAll($filters);

        $totalContractorShare = 0;

        if ($bookings) {
            foreach ($bookings as $booking) {
                $bookingId = $booking['booking_id'];
                //check if the contractor have fixed contractor share
                $contractorShareBooking = $modelContractorShareBooking->getByBookingIdAndContractorId($bookingId, $contractorId);

                if (!empty($contractorShareBooking['contractor_share'])) {
                    $contractorShare = $contractorShareBooking['contractor_share'];
                } else {
                    //get total refund
                    $totalRefund = $modelRefund->getTotalRefund(array('booking_id' => $bookingId, 'is_approved' => 'yes'));
                    //booking Qoute without tax
                    $bookingQouteWithoutTax = $this->getTotalBookingQoute($bookingId);
                    //Contractor Services Qoute
                    $contractorServicesQoute = $this->getTotalContractorServicesQoute($bookingId, $contractorId);

                    if ($booking['call_out_fee']) {
                        $contractorServicesQoute = $this->getCallOutFeeByBookingIdAndContractorId($bookingId);
                    }


                    // if the contractor not pay taxes
                    $contractorShare = $contractorServicesQoute * $contractorInfo['commission'] / 100;

                    // percentage calculator of discount
                    $discount = 0;
                    if ($bookingQouteWithoutTax) {
                        $discount = ($contractorServicesQoute / $bookingQouteWithoutTax) * $booking['total_discount'];
                    } else if ($booking['call_out_fee']) {
                        $discount = ($contractorServicesQoute / $booking['call_out_fee']) * $booking['total_discount'];
                    }

                    // percentage calculator of refund
                    $refund = 0;
                    if ($totalRefund) {
                        if ($bookingQouteWithoutTax) {
                            $refund = ($contractorShare / $bookingQouteWithoutTax) * $totalRefund;
                        } else {
                            $contractors = $this->getContractorsBybookingId($bookingId);
                            $subRefund = 0;
                            if (count($contractors)) {
                                $subRefund = $totalRefund / count($contractors);
                            }
                            $refund = $subRefund * $contractorInfo['commission'] / 100;
                        }
                    }

                    // if the contractor pay taxes
                    if ($contractorInfo['gst']) {
                        $servicesQouteWithTax = ($contractorServicesQoute - $discount) * (1 + get_config('gst_tax'));
                        $contractorShare = $servicesQouteWithTax * $contractorInfo['commission'] / 100;
                    } else {
                        $contractorShare = $contractorShare - $discount;
                    }
                    $contractorShare = $contractorShare - $refund;
                }
                $totalContractorShare += $contractorShare;
            }
        }

        return $totalContractorShare;
    }

    public function getTotalContractorRefund($contractorId, $filters = array(), $isApproved = 'yes') {

        // Load Model
        $modelRefund = new Model_Refund();
        $modelBooking = new Model_Booking();
        $modelContractorInfo = new Model_ContractorInfo();

        //Get Contractor Info
        $contractorInfo = $modelContractorInfo->getByContractorId($contractorId);




        // get all booking for contractor
        $filters['contractor_id'] = $contractorId;
        $filters['convert_status'] = 'invoice';
        $bookings = $modelBooking->getAll($filters);


        $totalContractorRefund = 0;

        if ($bookings) {
            foreach ($bookings as $booking) {
                $bookingId = $booking['booking_id'];

                //get total refund
                $totalRefund = $modelRefund->getTotalRefund(array('booking_id' => $bookingId, 'is_approved' => $isApproved));

                //booking Qoute without tax
                $bookingQouteWithoutTax = $this->getTotalBookingQoute($bookingId);

                // total Contractor Services Qoute
                $contractorServicesQoute = $this->getTotalContractorServicesQoute($bookingId, $contractorId);

                $contractorShare = $contractorServicesQoute * $contractorInfo['commission'] / 100;


                // percentage calculator of refund(must be without tax)
                $refund = 0;
                if ($totalRefund) {
                    if ($bookingQouteWithoutTax) {
                        $refund = ($contractorShare / $bookingQouteWithoutTax) * $totalRefund;
                    } else {
                        $contractors = $this->getContractorsBybookingId($bookingId);
                        $subRefund = 0;
                        if (count($contractors)) {
                            $subRefund = $totalRefund / count($contractors);
                        }
                        $refund = $subRefund * $contractorInfo['commission'] / 100;
                    }
                }

                $totalContractorRefund += $refund;
            }
        }

        return $totalContractorRefund;
    }

    public function getTotalServiceRefund($contractorId, $filters = array()) {

        // Load Model
        $modelRefund = new Model_Refund();
        $modelBooking = new Model_Booking();
        $modelContractorInfo = new Model_ContractorInfo();

        //Get Contractor Info
        $contractorInfo = $modelContractorInfo->getByContractorId($contractorId);

        // get all booking for contractor
        $filters['contractor_id'] = $contractorId;
        $filters['convert_status'] = 'invoice';
        $bookings = $modelBooking->getAll($filters);

        $totalServiceRefund = 0;

        if ($bookings) {
            foreach ($bookings as $booking) {
                $bookingId = $booking['booking_id'];

                //get total refund
                $totalRefund = $modelRefund->getTotalRefund(array('booking_id' => $bookingId, 'is_approved' => 'yes'));

                //booking Qoute without tax
                $bookingQouteWithoutTax = $this->getTotalBookingQoute($bookingId);

                // total Contractor Services Qoute
                $contractorServicesQoute = $this->getTotalContractorServicesQoute($bookingId, $contractorId);


                // percentage calculator of refund(must be without tax)
                $refund = 0;
                if ($totalRefund) {
                    if ($bookingQouteWithoutTax) {
                        $refund = ($contractorServicesQoute / $bookingQouteWithoutTax) * $totalRefund;
                    } else {
                        $contractors = $this->getContractorsBybookingId($bookingId);
                        if (count($contractors)) {
                            $refund = $totalRefund / count($contractors);
                        }
                    }
                }
                $totalServiceRefund += $refund;
            }
        }

        return $totalServiceRefund;
    }

    public function getTotalServiceDiscount($contractorId, $filters = array()) {

        // Load Model
        $modelBooking = new Model_Booking();


        // get all booking for contractor
        $filters['contractor_id'] = $contractorId;
        $filters['convert_status'] = 'invoice';
        $bookings = $modelBooking->getAll($filters);

        $totalServiceDiscount = 0;

        if ($bookings) {
            foreach ($bookings as $booking) {

                //booking Qoute without tax
                $bookingQouteWithoutTax = $this->getTotalBookingQoute($booking['booking_id']);

                // total Contractor Services Qoute
                $contractorServicesQoute = $this->getTotalContractorServicesQoute($booking['booking_id'], $contractorId);

                if ($booking['call_out_fee']) {
                    $contractorServicesQoute = $this->getCallOutFeeByBookingIdAndContractorId($booking['booking_id']);
                }

                // percentage calculator of discount
                $discount = 0;
                if ($bookingQouteWithoutTax) {
                    $discount = ($contractorServicesQoute / $bookingQouteWithoutTax) * $booking['total_discount'];
                } else if ($booking['call_out_fee']) {
                    $discount = ($contractorServicesQoute / $booking['call_out_fee']) * $booking['total_discount'];
                }

                $totalServiceDiscount += $discount;
            }
        }

        return $totalServiceDiscount;
    }

    public function getCallOutFeeByBookingIdAndContractorId($bookingId) {
        $modelBooking = new Model_Booking();

        $booking = $modelBooking->getById($bookingId);

        $call_out_fee = 0;
        if ($booking['call_out_fee']) {
            $contractors = $this->getContractorsBybookingId($bookingId);
            if (count($contractors)) {
                $call_out_fee = ($booking['call_out_fee'] / count($contractors));
            }
        }
        return $call_out_fee;
    }

    public function getTotalAmountServicesInvoicedByContractorId($contractorId, $filters = array()) {
        $filters['convert_status'] = 'invoice';
        return $this->getTotalAmountServicesByContractorIdAndStatusId($contractorId, 0, $filters);
    }

    public function getTotalAmountServicesByContractorIdAndStatusId($contractorId, $statusId = 0, $filters = array()) {

        // Load Model
        $modelBooking = new Model_Booking();
        $modelRefund = new Model_Refund();
        $modelContractorInfo = new Model_ContractorInfo();

        //Get Contractor Info
        $contractorInfo = $modelContractorInfo->getByContractorId($contractorId);

        // get all booking for contractor
        $filters['contractor_id'] = $contractorId;
        if ($statusId) {
            $filters['status'] = $statusId;
        }

        $bookings = $modelBooking->getAll($filters);

        $totalAmountServices = 0;

        if ($bookings) {
            foreach ($bookings as $booking) {

                $bookingId = $booking['booking_id'];

                //booking Qoute without tax
                $bookingQouteWithoutTax = $this->getTotalBookingQoute($bookingId);

                //Contractor Services Qoute
                $contractorServicesQoute = $this->getTotalContractorServicesQoute($bookingId, $contractorId);

                if ($booking['call_out_fee']) {
                    $contractorServicesQoute = $this->getCallOutFeeByBookingIdAndContractorId($bookingId);
                }

                // percentage calculator of discount
                $discount = 0;
                if ($bookingQouteWithoutTax) {
                    $discount = ($contractorServicesQoute / $bookingQouteWithoutTax) * $booking['total_discount'];
                } else if ($booking['call_out_fee']) {
                    $discount = ($contractorServicesQoute / $booking['call_out_fee']) * $booking['total_discount'];
                }

                //calculate percentage  total refund when status is completed or faild
                $totalRefund = $modelRefund->getTotalRefund(array('booking_id' => $bookingId, 'is_approved' => 'yes'));

                $refund = 0;
                if ($totalRefund) {
                    if ($bookingQouteWithoutTax) {
                        $refund = ($contractorServicesQoute / $bookingQouteWithoutTax) * $totalRefund;
                    } else {
                        $contractors = $this->getContractorsBybookingId($bookingId);
                        if (count($contractors)) {
                            $refund = $totalRefund / count($contractors);
                        }
                    }
                }

                $totalAmountServices += ($contractorServicesQoute - $discount) * (1 + get_config('gst_tax')) - $refund;
            }
        }

        return $totalAmountServices;
    }

    public function getUnapprovedBooking() {
        $select = $this->getAdapter()->select();
        $select->from($this->_name, 'booking_id');
        $select->distinct();
        $select->where("is_change = 1");

        return $this->getAdapter()->fetchAll($select);
    }

    public function changeAttributIfFaild($bookingId, $services,$log_id = 0) {

        // Load Model

        $modelAttributes = new Model_Attributes();
        $modelServiceAttribute = new Model_ServiceAttribute();
        $modelServiceAttributeValue = new Model_ServiceAttributeValue();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();

        foreach ($services as $service) {

            $serviceAndClone = explode('_', $service);
            $serviceId = (int) (isset($serviceAndClone[0]) ? $serviceAndClone[0] : 0);
            $clone = (int) (isset($serviceAndClone[1]) && $serviceAndClone[1] ? $serviceAndClone[1] : 0);

            // Quantity
            $quantityAttribute = $modelAttributes->getByVariableName('Quantity');
            $serviceQuantityAttribute = $modelServiceAttribute->getByAttributeIdAndServiceId($quantityAttribute['attribute_id'], $serviceId);
            $oldServiceQuantityValue = $modelServiceAttributeValue->getByBookingIdAndServiceAttributeIdAndClone($bookingId, $serviceQuantityAttribute['service_attribute_id'], $clone);
            $modelServiceAttributeValue->updateById($oldServiceQuantityValue['service_attribute_value_id'], array('value' => ''));

            // Discount

            $discountAttribute = $modelAttributes->getByVariableName('Discount');
            $serviceDiscountAttribute = $modelServiceAttribute->getByAttributeIdAndServiceId($discountAttribute['attribute_id'], $serviceId);
            $oldServiceDiscountValue = $modelServiceAttributeValue->getByBookingIdAndServiceAttributeIdAndClone($bookingId, $serviceDiscountAttribute['service_attribute_id'], $clone);
            $modelServiceAttributeValue->updateById($oldServiceDiscountValue['service_attribute_value_id'], array('value' => ''));

            // Change Min Price to zero

            $booking_service = $modelContractorServiceBooking->getByBookingAndServiceAndClone($bookingId, $serviceId, $clone);
            $modelContractorServiceBooking->updateById($booking_service['id'], $data = array('consider_min_price' => 0),$log_id);
        }
    }

    public function getBookingAsText($bookingId, $full = false, $services = array()) {

        //
        // load models
        //
        $modelServices = new Model_Services();
        $modelServiceAttribute = new Model_ServiceAttribute();
        $modelServiceAttributeValue = new Model_ServiceAttributeValue();
        $modelAttributes = new Model_Attributes();
        $modelAttributeType = new Model_AttributeType();
        $modelAttributeListValue = new Model_AttributeListValue();
        $modelUser = new Model_User();
        $modelContractorinfo = new Model_ContractorInfo();

        if (!$services) {
            $services = $this->getByBookingId($bookingId);
        }

        $serviecsAsText = "";

        foreach ($services as $service) {

//            if ($full) {
//                $contractorServiceBooking = $this->getByBookingAndServiceAndClone($bookingId, $service['service_id'], $service['clone']);
//                $contractorInfo = $modelContractorinfo->getByContractorId($contractorServiceBooking['contractor_id']);
//                $userInfo = $modelUser->getById($contractorServiceBooking['contractor_id']);
//
//                $contractorName = ucwords($userInfo['username']) . ucwords($contractorInfo['business_name'] ? " - " . $contractorInfo['business_name'] : '');
//                $serviecsAsText .= 'Technician : ' . $contractorName . "<br>";
//            }

            $serviceId = isset($service['service_id']) ? $service['service_id'] : 0;
            $clone = isset($service['clone']) ? $service['clone'] : 0;

            //get the service
            $serviceById = $modelServices->getById($serviceId);
            $serviecsAsText .= 'Service : ' . $serviceById['service_name'] . "<br>";

            $unit_price = 0;
            $qty = 0;
            //get the service attribute id
            $service_attributes = $modelServiceAttribute->getByServiceId($serviceId);
            foreach ($service_attributes as $service_attribute) {

                $attribute = $modelAttributes->getById($service_attribute['attribute_id']);

                //get the service attribute value
                $attributeValue = $modelServiceAttributeValue->getByBookingIdAndServiceAttributeIdAndClone($bookingId, $service_attribute['service_attribute_id'], $clone);

                switch ($attribute['attribute_name']) {
                    case 'Quantity':
                        $qty = $attributeValue['value'];
                        break;
                    case 'Price':
                        $unit_price = $attributeValue['value'];
                        break;
                    default:
                        if ($full) {
                            $atributetype = $modelAttributeType->getById($attribute['attribute_type_id']);

                            // check if the attribute type is list
                            if ($atributetype['is_list']) {
                                // check if the attribute value is serialized
                                if ($attributeValue['is_serialized_array']) {

                                    $unserializeValues = unserialize($attributeValue['value']);

                                    $values = array();
                                    foreach ($unserializeValues as $unserializeValue) {
                                        $attributeListValue = $modelAttributeListValue->getById($unserializeValue);
                                        $values[] = $attributeListValue['attribute_value'];
                                    }

                                    $value = implode(', ', $values);
                                } else {
                                    $attributeListValue = $modelAttributeListValue->getById($attributeValue['value']);
                                    $value = $attributeListValue['attribute_value'];
                                }
                            } else {
                                $value = $attributeValue['value'];
                            }

                            if (!empty($value)) {

                                //$numberOfSpaces = strlen($attribute['attribute_name']) + 3;
                                //
                                //  $spaces = '';
                                //for ($i = 0; $i < $numberOfSpaces; $i++) {
                                //  $spaces .= ' ';
                                // }
                                //
                                //$value = str_replace("\n", "<br>" . $spaces, $value);

                                $serviecsAsText .= empty($attribute['attribute_name']) ? '' : $attribute['attribute_name'] . " : " . $value . "<br>";
                            }
                        }
                        break;
                }
            }

            $serviecsAsText .= 'Service minimum price : $' . $serviceById['min_price'] . "<br>";

            $unit_price = empty($unit_price) ? 0 : $unit_price;
            $qty = empty($qty) ? 0 : $qty;

            $serviecsAsText .= "Quote : $" . $unit_price . " * (Around) " . $qty . " m2 <br>";

            $total = $this->getTotalServiceBookingQoute($bookingId, $serviceId, $clone);
            $serviecsAsText .= "Total Quote : Around $" . number_format($total, 2) . " + GST<br><br>";
        }

        return $serviecsAsText;
    }

    public function getFullTextContractorServiceBooking($bookingId) {

        //
        // load models
        //
        $modelServices = new Model_Services();
        $modelServiceAttribute = new Model_ServiceAttribute();
        $modelServiceAttributeValue = new Model_ServiceAttributeValue();
        $modelAttributes = new Model_Attributes();
        $modelAttributeType = new Model_AttributeType();
        $modelAttributeListValue = new Model_AttributeListValue();
        $modelUser = new Model_User();
        $modelContractorinfo = new Model_ContractorInfo();

        $services = $this->getByBookingId($bookingId);
        $attribute = $modelAttributes->getByVariableName('Floor');

        $contractorServices = array();
        if ($services) {
            foreach ($services as $service) {


                $contractorServiceBooking = $this->getByBookingAndServiceAndClone($bookingId, $service['service_id'], $service['clone']);
                $contractorInfo = $modelContractorinfo->getByContractorId($contractorServiceBooking['contractor_id']);
                $userInfo = $modelUser->getById($contractorServiceBooking['contractor_id']);


                // get Contractors Name
                if (!empty($userInfo['username'])) {
                    $contractorServices[] = $userInfo['username'];
                }
                if (!empty($contractorInfo['business_name'])) {
                    $contractorServices[] = $contractorInfo['business_name'];
                }

                //get Service Name
                $serviceById = $modelServices->getById($service['service_id']);

                if (!empty($serviceById['service_name'])) {
                    $contractorServices[] = $serviceById['service_name'];
                }

                // get floor type
                $ServiceAttribute = $modelServiceAttribute->getByAttributeIdAndServiceId($attribute['attribute_id'], $service['service_id']);
                $ServiceAttributeValue = $modelServiceAttributeValue->getByBookingIdAndServiceAttributeIdAndClone($bookingId, $ServiceAttribute['service_attribute_id'], $service['clone']);
                $attributeListValue = $modelAttributeListValue->getById($ServiceAttributeValue['value']);
                $floorType = $attributeListValue['attribute_value'];
                if (!empty($floorType)) {
                    $contractorServices[] = $floorType;
                }
            }
        }

        if ($contractorServices) {
            $contractorServices = implode(' ', $contractorServices);
        }

        return $contractorServices;
    }

    public function getBookingForEachContractor($filters = array(), $order = null) {

        // get booking for each contractor, so the booking will duplicate if they have more one contractor
						
        $select = $this->getAdapter()->select();
        $select->from(array('csb' => $this->_name), array('csb.booking_id', 'csb.contractor_id'));
        
		$select->joinLeft(array('com' => 'complaint'), 'com.booking_id = csb.booking_id', array('com.complaint_status','com.complaint_id'));
		
		$select->order($order);
        $select->distinct();

        if ($filters) {
            if (!empty($filters['order_by_booking_start'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'csb.booking_id = bok.booking_id', 'cols' => '');
                $select->order('bok.booking_start ASC');
            }
            if (!empty($filters['booking_id'])) {
                $select->where("csb.booking_id = {$filters['booking_id']}");
            }
            if (!empty($filters['contractor_id'])) {
                $select->where("csb.contractor_id = {$filters['contractor_id']}");
            }
            if (!empty($filters['service_id'])) {
                $select->where("csb.service_id = {$filters['service_id']}");
            }

            if (isset($filters['is_accepted'])) {
                if ($filters['is_accepted'] == "all") {
                    $select->where("csb.is_accepted = 0 OR csb.is_accepted = 1");
                } elseif ($filters['is_accepted'] == "accepted") {
                    $select->where("csb.is_accepted = 1");
                } elseif ($filters['is_accepted'] == "not_accepted") {
                    $select->where("csb.is_accepted = 0");
                }
            }

            if (isset($filters['not_accepted_or_rejected'])) {
                $select->where("csb.is_accepted = 0 AND csb.is_rejected = 0");
            }

            if (isset($filters['is_rejected'])) {
                if ($filters['is_rejected'] == "all") {
                    $select->where("csb.is_rejected = 0 OR csb.is_rejected = 1");
                } elseif ($filters['is_rejected'] == "rejected") {
                    $select->where("csb.is_rejected = 1");
                } elseif ($filters['is_rejected'] == "not_rejected") {
                    $select->where("csb.is_rejected = 0");
                }
            }

            if (!empty($filters['booking_start_between'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'csb.booking_id = bok.booking_id', 'cols' => '');
                $bookingStartBetween = $filters['booking_start_between'];
                $bookingEndBetween = !empty($filters['booking_end_between']) ? $filters['booking_end_between'] : date('Y-m-d H:i:s', time());
                $select->where("bok.booking_start between '" . $bookingStartBetween . "' and '" . $bookingEndBetween . "'");
            }
			///////////By Islam join with booking_contractor_payment
			if (!empty($filters['Inv'])) {
                $joinInner['booking_contractor_payment'] = array('name' => array('bcp' => 'booking_contractor_payment'), 'cond' => 'csb.booking_id = bcp.booking_id', 'cols' => '');
                 //$select->where("bcb.contractor_id = bcp.contractor_id '");
                $select->where("bcp.contractor_invoice_num = '" . $filters['Inv'] . "'");
            }
			
			if (!empty($filters['status_id'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'csb.booking_id = bok.booking_id', 'cols' => '');
                if($filters['status_id']==18){
					//$modelBookingStatus = new Model_BookingStatus();
					//$completed = array_values($modelBookingStatus->getByStatusName('COMPLETED'))[0];
					//$failed = array_values($modelBookingStatus->getByStatusName('FAILED'))[0];
					$select->where("bok.status_id = '2' or bok.status_id = '5'");
						
					}
				else{
					$select->where("bok.status_id = {$filters['status_id']}");
				}
				$select->order('bok.booking_start ASC');
            }
			
			///////////////////
			
			
        }

        if ($joinInner) {
            foreach ($joinInner as $inner) {
                $select->joinInner($inner['name'], $inner['cond'], $inner['cols']);
            }
        }

        return $this->getAdapter()->fetchAll($select);
    }

    public function getBookingsbyServiceIdandAttributeValueId($filters = array(), $order = null) {

        // get booking for each contractor, so the booking will duplicate if they have more one contractor

		
        $select = $this->getAdapter()->select();
        $select->from(array('csb' => $this->_name), array('csb.booking_id', 'csb.contractor_id'));
        $select->order($order);
        $select->distinct();
		$this->getAdapter()->fetchAll($select);
        if ($filters) {
            if (!empty($filters['booking_id'])) {
                $select->where("csb.booking_id = {$filters['booking_id']}");
            }
            if (!empty($filters['contractor_id'])) {
                $select->where("csb.contractor_id = {$filters['contractor_id']}");
            }
            if (!empty($filters['service_id'])) {
                $select->where("csb.service_id = {$filters['service_id']}");
            }

            if (isset($filters['is_accepted'])) {
                if ($filters['is_accepted'] == "all") {
                    $select->where("csb.is_accepted = 0 OR csb.is_accepted = 1");
                } elseif ($filters['is_accepted'] == "accepted") {
                    $select->where("csb.is_accepted = 1");
                } elseif ($filters['is_accepted'] == "not_accepted") {
                    $select->where("csb.is_accepted = 0");
                }
            }

            if (isset($filters['not_accepted_or_rejected'])) {
                $select->where("csb.is_accepted = 0 AND csb.is_rejected = 0");
            }

            if (isset($filters['is_rejected'])) {
                if ($filters['is_rejected'] == "all") {
                    $select->where("csb.is_rejected = 0 OR csb.is_rejected = 1");
                } elseif ($filters['is_rejected'] == "rejected") {
                    $select->where("csb.is_rejected = 1");
                } elseif ($filters['is_rejected'] == "not_rejected") {
                    $select->where("csb.is_rejected = 0");
                }
            }

            if (!empty($filters['booking_start_between'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'csb.booking_id = bok.booking_id', 'cols' => '');
                $bookingStartBetween = $filters['booking_start_between'];
                $bookingEndBetween = !empty($filters['booking_end_between']) ? $filters['booking_end_between'] : date('Y-m-d H:i:s', time());
                $select->where("bok.booking_start between '" . $bookingStartBetween . "' and '" . $bookingEndBetween . "'");
            }
            if (!empty($filters['attribute_value_id'])) {
                $joinInner['service_attribute_value'] = array('name' => array('sav' => 'service_attribute_value'), 'cond' => 'sav.booking_id = csb.booking_id', 'cols' => '');
                $joinInner['service_attribute'] = array('name' => array('sa' => 'service_attribute'), 'cond' => 'sa.service_attribute_id = sav.service_attribute_id', 'cols' => '');
                $joinInner['attribute'] = array('name' => array('a' => 'attribute'), 'cond' => 'sa.attribute_id = a.attribute_id', 'cols' => '');
                $joinInner['attribute_type'] = array('name' => array('at' => 'attribute_type'), 'cond' => 'at.attribute_type_id = a.attribute_type_id', 'cols' => '');

                $attribute_value_id = (int) trim($filters['attribute_value_id']);
                $select->where("sav.value = {$attribute_value_id}");
                $select->where("at.is_list = 1");
            }
        }

        if ($joinInner) {
            foreach ($joinInner as $inner) {
                $select->joinInner($inner['name'], $inner['cond'], $inner['cols']);
            }
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * get the acceptrd services in the booking of the logged user
     *
     * @param int $bookingId
     * @return array 
     */
    public function getServicesAndTotalServiceBookingQouteAsViewParam($bookingId) {
        $bookingServices = $this->getByBookingId($bookingId);
        // $thisBookingServices : to put all service for this booking 
        $thisBookingServices = array();
        // $priceArray : to put all price and service for this booking 
        $priceArray = array();

        if ($bookingServices) {
            foreach ($bookingServices as $bookingService) {

                $serviceId = $bookingService['service_id'];
                $clone = $bookingService['clone'];
                $bookingId = $bookingService['booking_id'];

                $service_and_clone = $serviceId . ($clone ? '_' . $clone : '');

                $thisBookingServices[] = $service_and_clone;

                $priceArray[$service_and_clone] = $this->getTotalServiceBookingQoute($bookingId, $serviceId, $clone);
            }
        }
        $viewParam = array();

        $viewParam['bookingServices'] = $bookingServices;
        $viewParam['thisBookingServices'] = $thisBookingServices;
        $viewParam['priceArray'] = $priceArray;

        return $viewParam;
    }

}

