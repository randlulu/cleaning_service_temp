<?php

class Model_Faq extends Zend_Db_Table_Abstract {

    protected $_name = 'faq';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->distinct();
        $select->from(array('f' => $this->_name));
        $select->joinLeft(array('u' => 'user'), 'u.user_id = f.user_id', array('username'));
        $select->joinInner(array('uc' => 'user_company'), 'uc.user_id = u.user_id');
       

         $loggedUser = CheckAuth::getLoggedUser();
          if($loggedUser ){
              $companyId = CheckAuth::getCompanySession();
          }else{
             $companyId = 1 ;
          }

        $select->where("uc.company_id = '{$companyId}'");
        //$select->joinInner(array('fs' => 'faq_service'), 'fs.faq_id = f.faq_id');
        //$select->joinInner(array('s' => 'service'), 'fs.service_id = s.service_id' , array('service_name'));
        //$select->joinInner(array('alv' => 'attribute_list_value'), 'alv.attribute_value_id = fs.floor_id' , array('attribute_value'));
 

        /*******************************IBM*/
        $wheresAndJoins = $this->getWheresAndJoinsByFilters($filters);

        $joinInner = $wheresAndJoins['joinInner'];
        $wheres = $wheresAndJoins['wheres'];

        if ($wheres) {
            foreach ($wheres as $where) {
                $select->where($where);
            }
        }

        if ($joinInner) {
            foreach ($joinInner as $inner) {
                $select->joinInner($inner['name'], $inner['cond'], $inner['cols']);
            }
        }
        
        $select->order($order);

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        /********************************/

       

        return $this->getAdapter()->fetchAll($select);
    }


    private function getWheresAndJoinsByFilters($filters) {
        $wheres = array();
        $joinInner = array();

        if ($filters) {

            /********Filter By Service *****IBM*/
            if (!empty($filters['service'])) {

                $faqservice = $this->getAdapter()->quote(trim($filters['service']));
                $joinInner['faq_service'] = array('name' => array('fs' => 'faq_service'), 'cond' => 'f.faq_id = fs.faq_id', 'cols' => '');
                 $wheres['service'] = ("fs.service_id = {$faqservice}");
            }
            /******Filter By Floor *****IBM*/
            if (!empty($filters['floor'])) {

                $faqByFloor = $this->getAdapter()->quote(trim($filters['floor']));
                $joinInner['faq_service'] = array('name' => array('fs' => 'faq_service'), 'cond' => 'f.faq_id = fs.faq_id', 'cols' => '');
                 $wheres['floor'] = ("fs.floor_id = {$faqByFloor}");
            }
            /*********End Filter By Floor********/

        }
        return array('wheres' => $wheres, 'joinInner' => $joinInner);
    }
   


    public function insert(array $data) {
        $id = parent::insert($data);
        return $id;
    }

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "faq_id = '{$id}'");
    }

    /**
     * delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("faq_id = '{$id}'");
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from(array('f' => $this->_name));
        //$select->joinInner(array('fs' => 'faq_service'), 'f.faq_id = fs.faq_id');
        $select->where("f.faq_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function getFaqByServiceIdAndFloorId($wheres, $general = 0) {

        $select = $this->getAdapter()->select();
        $select->from(array('f' => $this->_name));
        $select->joinLeft(array('fs' => 'faq_service'), 'f.faq_id = fs.faq_id');
        if ($general) {
            $select->where('service_id IS NULL and floor_id Is NULL');
           
        } else {
            $select->where($wheres[0]);
            array_shift($wheres);
            foreach ($wheres as $where) {
                $select->orWhere($where);
            }
        }
      
	  
        $select->group("f.question");
		
        return $this->getAdapter()->fetchAll($select);
    }

//    public function getByCompanyId($companyId) {
//        $companyId = (int) $companyId;
//        $select = $this->getAdapter()->select();
//        $select->from($this->_name);
//        $select->where("company_id = '{$companyId}'");
//
//        return $this->getAdapter()->fetchRow($select);
//    }
}
