
<?php

class Model_Inquiry extends Zend_Db_Table_Abstract {

    protected $_name = 'inquiry';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getByNum($num) {

        $company_id = CheckAuth::getCompanySession();
        $company_id = (int) $company_id;
        $select = $this->getAdapter()->select();
        $select->from(array('i' => $this->_name));
        $select->joinInner(array('tr' => 'trading_name'), 'i.trading_name_id = tr.trading_name_id', array('trading_name', 'website_url', 'logo'));
        $select->where("inquiry_num = '{$num}'");
        $select->where('i.is_deleted = 0');
        $select->where("i.is_spam = 0 ");
        $select->where("i.company_id = {$company_id}");

        return $this->getAdapter()->fetchRow($select);
    }

    public function getAll($filters = array(), $order = null, &$pager = null, $limit = 0, $perPage = 0, $currentPage = 0) {
        $select = $this->getAdapter()->select();
        $select->from(array('i' => $this->_name));
        $select->order($order);
        $select->distinct();

        $loggedUser = CheckAuth::getLoggedUser();
        $filters['company_id'] = CheckAuth::getCompanySession();

        $joinInner = array();
        $joinLeft = array();
        $joinInner['trading_name'] = array('name' => array('tr' => 'trading_name'), 'cond' => 'i.trading_name_id = tr.trading_name_id', 'cols' => array('trading_name', 'website_url', 'logo'));
        $loggedUser = CheckAuth::getLoggedUser();

        if (!CheckAuth::checkCredential(array('canSeeAllInquiry'))) {
            if (CheckAuth::checkCredential(array('canSeeOnlyHisInquiry'))) {
                $select->where("i.user_id = " . $loggedUser['user_id']);
            }
        }

        if (CheckAuth::checkCredential(array('canSeeDeletedInquiry'))) {
            if (!empty($filters['is_deleted'])) {
                $select->where('i.is_deleted = 1');
            } else {
                $select->where('i.is_deleted = 0');
            }
        } else {
            $select->where('i.is_deleted = 0');
        }



        if ($filters) {
            if (!empty($filters['keywords'])) {
                $keywords = $this->getAdapter()->quote('%' . trim($filters['keywords']) . '%');
                $select->joinInner(array('cust' => 'customer'), 'i.customer_id = cust.customer_id', array('cust_full_search' => 'cust.full_text_search'));


                /* if(stripos($filters['keywords'] , 'INQ')  === 0 ){
                  $rows = $this->getByNum($filters['keywords']);
                  if(!empty($rows)){
                  $select->where("i.inquiry_num = '{$filters['keywords']}' ");
                  }
                  } */

                $select->where("i.full_text_search LIKE {$keywords} or cust.full_text_search LIKE {$keywords} ");


                //$select->where("is.service_id = {$service_id}");
                //$joinInner['inquiry_service'] = array('name' => array('is' => 'inquiry_service'), 'cond' => 'i.inquiry_id = is.inquiry_id', 'cols' => array('is.service_id'));
                //$keywords = '+' . preg_replace('#[\s]+#', ' +', trim($filters['keywords']));
                //$keywords = $this->getAdapter()->quote($keywords);
                //$select->where("MATCH(i.full_text_search) AGAINST({$keywords} IN BOOLEAN MODE)");
            }
            if (!empty($filters['is_spam'])) {
                $select->where("i.is_spam = 1 ");
            } else {
                $select->where("i.is_spam = 0 ");
            }

            if (!empty($filters['created_from'])) {
                $created_from = $this->getAdapter()->quote(strtotime(trim($filters['created_from'])));
                $select->where("i.created >= {$created_from}");
            }


            if (!empty($filters['created_to'])) {
                $created_to = $this->getAdapter()->quote(strtotime(trim($filters['created_to'])));
                $select->where("i.created <= {$created_to}");
            }

            if (!empty($filters['customer_id'])) {
                $customer_id = $this->getAdapter()->quote(trim($filters['customer_id']));
                $select->where("i.customer_id = {$customer_id}");
            }

            if (!empty($filters['customer_name'])) {
                $modelCustomer = new Model_Customer();
                $allCustomer = $modelCustomer->getCustomerIdByIdFullNameSearch(trim($filters['customer_name']));

                $customerIds = implode(', ', $allCustomer);
                if ($customerIds) {
                    $select->where("i.customer_id in ( $customerIds )");
                }
            }

            if (!empty($filters['service_id'])) {

                $service_id = (int) $filters['service_id'];

                $select->where("is.service_id = {$service_id}");
                $joinInner['inquiry_service'] = array('name' => array('is' => 'inquiry_service'), 'cond' => 'i.inquiry_id = is.inquiry_id', 'cols' => array('is.service_id'));
            }

            if (!empty($filters['inquiry_num'])) {
                $inquiry_num = $this->getAdapter()->quote('%' . trim($filters['inquiry_num']) . '%');
                $select->where("i.inquiry_num LIKE {$inquiry_num}");
            }

            if (!empty($filters['city_id'])) {
                $city_id = (int) $filters['city_id'];
                $select->where("i.city_id = {$city_id}");
            }

            if (!empty($filters['inquiry_type_id'])) {
                $inquiry_type_id = (int) trim($filters['inquiry_type_id']);
                $select->where("i.inquiry_type_id = {$inquiry_type_id}");
            }

            if (!empty($filters['status'])) {
                if ($filters['status'] != 'all') {
                    $status = $this->getAdapter()->quote(trim($filters['status']));
                    $select->where("i.status = {$status}");
                }
            }

            if (!empty($filters['label_ids'])) {
                if ($filters['label_ids']) {
                    $joinInner['inquiry_label'] = array('name' => array('il' => 'inquiry_label'), 'cond' => 'i.inquiry_id = il.inquiry_id', 'cols' => '');
                    $select->where('il.label_id IN (' . implode(', ', $filters['label_ids']) . ')');
                }
            }

            if (!empty($filters['unlabeled']) && $filters['unlabeled'] == true) {

                $sql = $this->getAdapter()->select();
                $sql->from('inquiry_label', 'inquiry_id');
                $sql->distinct();
                $results = $this->getAdapter()->fetchAll($sql);

                $inquiry_ids = array();
                foreach ($results as $result) {
                    $inquiry_ids[] = $result['inquiry_id'];
                }

                if ($inquiry_ids) {
                    $select->where('i.inquiry_id NOT IN (' . implode(', ', $inquiry_ids) . ')');
                }
            }

            if (!empty($filters['address'])) {
                $address = trim($filters['address']);
                $joinInner['inquiry_address'] = array('name' => array('ia' => 'inquiry_address'), 'cond' => 'i.inquiry_id = ia.inquiry_id', 'cols' => '');
                $select->where("CONCAT_WS(' ',ia.street_number ,ia.street_address ,ia.suburb,ia.state,ia.postcode) LIKE '%{$address}%'");
            }

            if (!empty($filters['email'])) {
                $email = trim($filters['email']);
                $joinInner['customer'] = array('name' => array('c' => 'customer'), 'cond' => 'i.customer_id = c.customer_id', 'cols' => '');
                $select->where("CONCAT_WS(' ',c.email1 ,c.email2 ,c.email3) LIKE '%{$email}%'");
            }

            if (!empty($filters['mobile/phone'])) {
                $mobile_phone = trim($filters['mobile/phone']);
                $joinInner['customer'] = array('name' => array('c' => 'customer'), 'cond' => 'i.customer_id = c.customer_id', 'cols' => '');
                $select->where("CONCAT_WS(' ',c.phone1 ,c.phone2 ,c.phone3,c.mobile1 ,c.mobile2 ,c.mobile3) LIKE '%{$mobile_phone}%'");
            }

            if (!empty($filters['bussiness_name'])) {
                $joinInner['customer_commercial_info'] = array('name' => array('cci' => 'customer_commercial_info'), 'cond' => 'i.customer_id = cci.customer_id', 'cols' => '');
                $select->where("cci.business_name LIKE '%{$filters['bussiness_name']}%'");
            }

            if (!empty($filters['deferred_to'])) {
                $deferred_to = $this->getAdapter()->quote(strtotime(trim($filters['deferred_to'])));
                $select->where("i.deferred_date = {$deferred_to}");
            }

            if (!empty($filters['to_follow_date'])) {
                switch ($filters['to_follow_date']) {
                    case 'yesterday':
                        $yesterday = getTimePeriodByName('yesterday');
                        $select->where("i.deferred_date between '" . strtotime($yesterday['start']) . "' and '" . strtotime($yesterday['end']) . "'");
                        break;
                    case 'today':
                        $today = getTimePeriodByName('today');
                        $select->where("i.deferred_date between '" . strtotime($today['start']) . "' and '" . strtotime($today['end']) . "'");
                        break;
                    case 'tomorrow':
                        $tomorrow = getTimePeriodByName('tomorrow');
                        $select->where("i.deferred_date between '" . strtotime($tomorrow['start']) . "' and '" . strtotime($tomorrow['end']) . "'");
                        break;
                    case 'past':
                        $today = getTimePeriodByName('today');
                        $select->where("i.deferred_date < '" . strtotime($today['start']) . "'");
                        break;
                    case 'future':
                        $today = getTimePeriodByName('today');
                        $select->where("i.deferred_date > '" . strtotime($today['end']) . "'");
                        break;
                }
            }

//            if (!empty($filters['to_follow']) && $filters['to_follow'] == true) {
//                $time = time();
//                $select->where("i.deferred_date <= {$time} ");
//                $select->where("i.deferred_date > 0 ");
//            }

            if (!empty($filters['to_follow']) && $filters['to_follow'] == true) {
                $select->where("i.is_to_follow = 1");
            }

            if (!empty($filters['company_id'])) {
                $company_id = (int) $filters['company_id'];
                $select->where("i.company_id = {$company_id}");
            }

            if (!empty($filters['un_reminded']) && $filters['un_reminded'] == true) {

                $sql = $this->getAdapter()->select();
                $sql->from('inquiry_reminder', 'inquiry_id');
                $sql->distinct();
                $results = $this->getAdapter()->fetchAll($sql);

                $inquiry_ids = array();
                foreach ($results as $result) {
                    $inquiry_ids[] = $result['inquiry_id'];
                }

                if ($inquiry_ids) {
                    $select->where('i.inquiry_id NOT IN (' . implode(', ', $inquiry_ids) . ')');
                }
            }

            if (!empty($filters['my_inquiries'])) {

                $user_id = (int) $filters['my_inquiries'];

                $my_inquiry_sql = $this->getAdapter()->select();
                $my_inquiry_sql->from('inquiry_reminder', 'inquiry_id');
                $my_inquiry_sql->where("user_id = {$user_id}");
                $my_inquiry_sql->distinct();

                $my_inquiry_results = $this->getAdapter()->fetchAll($my_inquiry_sql);

                $not_my_inquiry_sql = $this->getAdapter()->select();
                $not_my_inquiry_sql->from('inquiry_reminder', 'inquiry_id');
                $not_my_inquiry_sql->distinct();

                if ($my_inquiry_results) {
                    $my_inquiry_ids = array();
                    foreach ($my_inquiry_results as $my_result) {
                        $my_inquiry_ids[$my_result['inquiry_id']] = $my_result['inquiry_id'];
                    }
                    $not_my_inquiry_sql->where('inquiry_id NOT IN (' . implode(', ', $my_inquiry_ids) . ')');
                }

                $not_my_inquiry_results = $this->getAdapter()->fetchAll($not_my_inquiry_sql);

                $not_my_inquiry_ids = array();
                foreach ($not_my_inquiry_results as $not_my_result) {
                    $not_my_inquiry_ids[$not_my_result['inquiry_id']] = $not_my_result['inquiry_id'];
                }

                if ($not_my_inquiry_ids) {
                    $select->where('i.inquiry_id NOT IN (' . implode(', ', $not_my_inquiry_ids) . ')');
                }
            }

            if (!empty($filters['booking_num'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'i.inquiry_id = bok.original_inquiry_id', 'cols' => '');
                $booking_num = $this->getAdapter()->quote('%' . trim($filters['booking_num']) . '%');
                $select->where("bok.booking_num LIKE {$booking_num}");
            }

            if (!empty($filters['estimate_num'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'i.inquiry_id = bok.original_inquiry_id', 'cols' => '');
                $joinInner['booking_estimate'] = array('name' => array('est' => 'booking_estimate'), 'cond' => 'bok.booking_id = est.booking_id', 'cols' => '');
                $estimate_num = $this->getAdapter()->quote('%' . trim($filters['estimate_num']) . '%');
                $select->where("est.estimate_num LIKE {$estimate_num}");
            }

            if (!empty($filters['contractor_id'])) {
                $contractor_id = (int) $filters['contractor_id'];
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'i.inquiry_id = bok.original_inquiry_id', 'cols' => '');
                $joinInner['contractor_service_booking'] = array('name' => array('csb' => 'contractor_service_booking'), 'cond' => 'bok.booking_id = csb.booking_id', 'cols' => '');
                $select->where("bok.created_by = {$contractor_id} OR csb.contractor_id = {$contractor_id}");
            }

            if (!empty($filters['startDateRange']) && !empty($filters['endDateRange'])) {
                $startDate = $filters['startDateRange'];
                $endDate = $filters['endDateRange'];
                $select->where("i.deferred_date between '" . strtotime($startDate) . "' and '" . strtotime($endDate) . "'");
            }
        }

        if ($pager) {
            if (!empty($filters['perPage'])) {
                $select->limit($filters['perPage'], ($pager->currentPage - 1) * $filters['perPage']);
            } else {
                $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            }

            $pager->dbSelect = $select;
        } elseif ($limit) {
            $select->limit($limit);
        } elseif ($perPage && $currentPage) {
            $select->limit($perPage, ($currentPage - 1) * $perPage);
        } else {
            if (!empty($filters['perPage'])) {
                $select->limit($filters['perPage']);
            }
        }

        if ($joinInner) {
            foreach ($joinInner as $inner) {
                $select->joinInner($inner['name'], $inner['cond'], $inner['cols']);
            }
        }

        if ($joinLeft) {
            foreach ($joinLeft as $left) {
                $select->joinLeft($left['name'], $left['cond'], $left['cols']);
            }
        }
        $sql = $select->__toString();


        return $this->getAdapter()->fetchAll($select);
    }

    public function getCustomerByInquiryId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name, array());
        $select->join('customer', 'inquiry.customer_id = customer.customer_id', array('first_name', 'last_name', 'customer_id'));
        $select->where("inquiry_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function getAllIds() {
        $select = $this->getAdapter()->select();
        $select->from($this->_name, array('inquiry_id'));
        $select->order('inquiry_id DESC');
        $select->limit(100, 0);
        $select->distinct();
        //$sql= $select->__toString();
        //echo $sql;
        return $this->getAdapter()->fetchAll($select);
    }

    public function insert(array $data, $company_id = 0) {

        if (!$company_id) {
            $company_id = CheckAuth::getCompanySession();
        }

        $select = $this->getAdapter()->select();
        $select->from($this->_name, 'MAX(count)');
        $select->where("company_id = {$company_id}");
        $result = $this->getAdapter()->fetchOne($select);

        $data['count'] = $result + 1;
        $data['inquiry_num'] = 'INQ-' . ($result + 1);

        $id = parent::insert($data);
        /**
         * update full_text_search field in the same table (inquiries)
         */
        //$this->updateFullTextSearch($id);

        /**
         * update full text search table
         */
        $modelUpdateFullTextSearch = new Model_UpdateFullTextSearch();
        $modelUpdateFullTextSearch->insertFullTextSearch($this->_name, $id);

        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'added');

        return $id;
    }

    /**
     * update table rows according to the assigned id and data
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;

        /**
         * update full text search table
         */
        $done = false;
        if (!empty($data['full_text_search'])) {
            $done = true;
        }
        $modelUpdateFullTextSearch = new Model_UpdateFullTextSearch();
        $modelUpdateFullTextSearch->updateFullTextSearchFlag($this->_name, $id, $done);


        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'changed');
        $success = parent::update($data, "inquiry_id = '{$id}'");
        //$this->updateFullTextSearch($id);
        //D.A 27/08/2015 Remove inquiry Cache
        if ($success) {
            require_once 'Zend/Cache.php';
            $inquiryDetailsCacheID = $id . '_inquiryDetails';
            $company_id = CheckAuth::getCompanySession();
            $inquiryViewDir = get_config('cache') . '/' . 'inquiriesView' . '/' . $company_id;
            if (!is_dir($inquiryViewDir)) {
                mkdir($inquiryViewDir, 0777, true);
            }
            $frontEndOption = array('lifetime' => NULL,
                'automatic_serialization' => true);
            $backendOptions = array('cache_dir' => $inquiryViewDir);
            $cache = Zend_Cache::factory('Core', 'File', $frontEndOption, $backendOptions);
            $cache->remove($inquiryDetailsCacheID);
        }


        return $success;
    }

    public function update($data, $id) {
        if (!empty($data['customer_id'])) {
            $select = $this->getAdapter()->select();
            $select->from(array('inq' => $this->_name));
            $select->where("inq.customer_id = {$id}");
            $results = $this->getAdapter()->fetchAll($select);
            if ($results) {
                foreach ($results as $result) {
                    require_once 'Zend/Cache.php';
                    $inquiryDetailsCacheID = $result['inquiry_id'] . '_inquiryDetails';
                    $company_id = CheckAuth::getCompanySession();
                    $inquiryViewDir = get_config('cache') . '/' . 'inquiriesView' . '/' . $company_id;
                    if (!is_dir($inquiryViewDir)) {
                        mkdir($inquiryViewDir, 0777, true);
                    }
                    $frontEndOption = array('lifetime' => NULL,
                        'automatic_serialization' => true);
                    $backendOptions = array('cache_dir' => $inquiryViewDir);
                    $cache = Zend_Cache::factory('Core', 'File', $frontEndOption, $backendOptions);
                    $cache->remove($inquiryDetailsCacheID);
                }
            }
        }

        $success = parent::update($data, $id);
        return $success;
    }

    public function updateByIp($ip, $data) {
        $select = $this->getAdapter()->select();
        $select->from(array('oi' => 'original_inquiry'), array('id'));
        $select->where("oi.ip_address = '{$ip}'");
        $originalBannedInquiriesIds = $this->getAdapter()->fetchAll($select);

        //// update inquiry where original_inquiry_id has this banned_ip
        $where = $this->getAdapter()->quoteInto('original_inquiry_id in (?)', $originalBannedInquiriesIds);
        return parent::update($data, $where);
    }

    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;

        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'deleted');

        return parent::delete("inquiry_id = '{$id}'");
    }

    public function deleteForeverById($id) {
        $id = (int) $id;

        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'deleted');

        return parent::delete("inquiry_id = '{$id}'");
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from(array('i' => $this->_name));
        $select->joinInner(array('tr' => 'trading_name'), 'i.trading_name_id = tr.trading_name_id', array('trading_name', 'website_url', 'logo'));
        $select->where("inquiry_id = '{$id}'");

        $sql = $select->__toString();

        return $this->getAdapter()->fetchRow($select);
    }

    public function getInquiryCount($filters = array()) {

        $select = $this->getAdapter()->select();
        $select->from(array('i' => $this->_name));
        $select->where('i.is_deleted = 0');
		$select->where("i.is_spam = 0 ");

        if (!isset($filters['company_id'])) {
            $filters['company_id'] = CheckAuth::getCompanySession();
        }

        if ($filters) {

            if (!empty($filters['customer_id'])) {
                $customer_id = (int) $filters['customer_id'];
                $select->where("i.customer_id = '{$customer_id}'");
            }
            //////////by islam select * inquiries of business name
            if (!empty($filters['customer_ids']) && $filters['customer_ids']) {
                $select->where('i.customer_id IN(?)', $filters['customer_ids']);
            }

            if (!empty($filters['startTimeRange']) && !empty($filters['endTimeRange'])) {
                $startTime = $filters['startTimeRange'];
                $endTime = $filters['endTimeRange'];
                $select->where("i.created between '" . mySql2PhpTime($startTime) . "' and '" . mySql2PhpTime($endTime) . "'");
            }

            if (!empty($filters['inquiry_status'])) {
                $inquiry_status = $this->getAdapter()->quote(trim($filters['inquiry_status']));
                $select->where("i.status = {$inquiry_status}");
            }
            if (!empty($filters['company_id'])) {
                $company_id = (int) $filters['company_id'];
                $select->where("i.company_id = {$company_id}");
            }
            if (!empty($filters['user_id'])) {
                $user_id = (int) $filters['user_id'];
                $select->where("i.user_id = {$user_id}");
            }
            if (!empty($filters['inquiry_type_id'])) {
                $inquiry_type_id = (int) $filters['inquiry_type_id'];
                $select->where("i.inquiry_type_id = {$inquiry_type_id}");
            }
        }

        $sql = $this->getAdapter()->select();
        $sql->from($select, 'COUNT(*)');
        return $this->getAdapter()->fetchOne($sql);
    }

    public function getEnamStatus() {
        $query = "SHOW COLUMNS FROM {$this->_name} LIKE 'status'";
        $stmt = $this->getAdapter()->query($query);
        $row = $stmt->fetch();
        $row = $row['Type'];
        $regex = "/'(.*?)'/";
        preg_match_all($regex, $row, $enum_array);
        $enum_fields = $enum_array[1];
        foreach ($enum_fields as $key => $value) {
            $enums[$value] = $value;
        }
        return $enums;
    }

    public function checkIfCanSeeInquiry($inquiryId, $userId = 0) {

        if (!$userId) {
            $loggedUser = CheckAuth::getLoggedUser();
            $userId = $loggedUser['user_id'];
        }

        if (CheckAuth::checkCredential(array('canSeeAllInquiry'))) {
            return true;
        }

        $inquiry = $this->getById($inquiryId);
        if ($inquiry['user_id'] == $userId) {
            return true;
        }
        return false;
    }

    public function checkIfCanSeeLocation($inquiryId, $userId = 0) {

        if (!$userId) {
            $loggedUser = CheckAuth::getLoggedUser();
            $userId = $loggedUser['user_id'];
        }

        if (CheckAuth::checkCredential(array('canSeeAllInquiryLocation'))) {
            return true;
        }

        $inquiry = $this->getById($inquiryId);
        if ($inquiry['user_id'] == $userId) {
            return true;
        }
        return false;
    }

    /**
     * get extra info for all the rows
     * @param array $row
     * @param array $types
     * @return array
     */
    public function fills(&$rows, $types = array()) {
        foreach ($rows as &$row) {
            $this->fill($row, $types);
        }
    }

    private $modelInquiryService;
    private $modelService;
    private $modelCustomer;
    private $modelCities;
    private $modelCountries;
    private $modelInquiryLabel;
    private $modelLabel;
    private $modelInquiryReminder;
    private $modelServiceAttribute;
    private $modelAttributeListValue;
    private $modelInquiryServiceAttributeValue;
    private $modelAttributeType;
    private $modelBookingAttachment;

    /**
     * get extra info for each row (Ex: getting the category row by c_id, author row by author id ... etc)
     * 
     * @param array $row
     * @param array $types
     * @return array
     */
    public function fill(&$row, $types = array()) {

        if (in_array('allService', $types)) {
            /**
             * load model
             */
            if (!$this->modelInquiryService) {
                $this->modelInquiryService = new Model_InquiryService();
            }
            if (!$this->modelService) {
                $this->modelService = new Model_Services();
            }

            $inquiryServices = $this->modelInquiryService->getByInquiryId($row['inquiry_id']);
            $allService = array();
            foreach ($inquiryServices as $inquiryService) {
                $service = $this->modelService->getById($inquiryService['service_id']);

                $allService[$service['service_id']] = $service['service_name'];
            }

            $row['allService'] = $allService;
        }
        if (in_array('service_attribute', $types)) {

            /**
             * load model
             */
            if (!$this->modelInquiryService) {
                $this->modelInquiryService = new Model_InquiryService();
            }
            if (!$this->modelService) {
                $this->modelService = new Model_Services();
            }
            if (!$this->modelServiceAttribute) {
                $this->modelServiceAttribute = new Model_ServiceAttribute();
            }
            if (!$this->modelAttributeListValue) {
                $this->modelAttributeListValue = new Model_AttributeListValue();
            }
            if (!$this->modelInquiryServiceAttributeValue) {
                $this->modelInquiryServiceAttributeValue = new Model_InquiryServiceAttributeValue();
            }
            if (!$this->modelAttributeType) {
                $this->modelAttributeType = new Model_AttributeType();
            }

            $allInquiryService = $this->modelInquiryService->getByInquiryId($row['inquiry_id']);
            foreach ($allInquiryService as &$inquiryService) {

                $service = $this->modelService->getById($inquiryService['service_id']);

                $inquiryService['service_name'] = $service['service_name'];

                //get service attribute
                $allServiceAttribute = $this->modelServiceAttribute->getAttributeByServiceId($inquiryService['service_id'], true);

                $attributes = array();
                foreach ($allServiceAttribute as &$serviceAttribute) {
                    $inquiryServiceAttributeValue = $this->modelInquiryServiceAttributeValue->getByInquiryIdAndServiceAttributeIdAndClone($row['inquiry_id'], $serviceAttribute['service_attribute_id'], $inquiryService['clone']);
                    $attributeType = $this->modelAttributeType->getById($serviceAttribute['attribute_type_id']);

                    $value = '';
                    if ($attributeType['is_list']) {
                        if ($inquiryServiceAttributeValue['is_serialized_array']) {
                            $unserializeValues = unserialize($inquiryServiceAttributeValue['value']);
                            if (is_array($unserializeValues)) {
                                $values = array();
                                foreach ($unserializeValues as $unserializeValue) {
                                    $attributeListValue = $this->modelAttributeListValue->getById($unserializeValue);
                                    if ($attributeListValue['unit_price']) {
                                        $values[] = number_format($attributeListValue['unit_price'], 2);
                                    } else {
                                        $values[] = $attributeListValue['attribute_value'];
                                    }
                                }
                                $value = $values;
                            }
                        } else {
                            $attributeListValue = $this->modelAttributeListValue->getById($inquiryServiceAttributeValue['value']);
                            if ($attributeListValue['unit_price']) {
                                $value = number_format($attributeListValue['unit_price'], 2);
                            } else {
                                $value = $attributeListValue['attribute_value'];
                            }
                        }
                    } else {
                        $value = $inquiryServiceAttributeValue['value'];
                    }

                    $serviceAttribute['value'] = $value;
                    $inquiryService['attributes'] = $allServiceAttribute;
                }
            }
            $row['service_attribute'] = $allInquiryService;
        }

        if (in_array('customer', $types)) {
            /**
             * load model
             */
            if (!$this->modelCustomer) {
                $this->modelCustomer = new Model_Customer();
            }

            $row['customer'] = $this->modelCustomer->getById($row['customer_id']);
            $row['customer_name'] = get_customer_name($row['customer']);
        }
        if (in_array('full_customer_info', $types)) {
            /**
             * load model
             */
            if (!$this->modelCustomer) {
                $this->modelCustomer = new Model_Customer();
            }

            $customer = $this->modelCustomer->getById($row['customer_id']);
            $this->modelCustomer->fill($customer, array('customer_commercial_info', 'customer_contacts'));

            $row['customer'] = $customer;
            $row['customer_name'] = get_customer_name($customer);
        }
        if (in_array('reminder', $types)) {
            /**
             * load model
             */
            if (!$this->modelInquiryReminder) {
                $this->modelInquiryReminder = new Model_InquiryReminder();
            }

            $row['reminder'] = $this->modelInquiryReminder->getContactHistory($row['inquiry_id']);
        }
        if (in_array('city', $types)) {
            /**
             * load model
             */
            if (!$this->modelCities) {
                $this->modelCities = new Model_Cities();
            }
            if (!$this->modelCountries) {
                $this->modelCountries = new Model_Countries();
            }

            $row['city'] = $this->modelCities->getById($row['city_id']);
            $row['city_name'] = $row['city']['city_name'];
            $countryName = $this->modelCountries->getById($row['city']['country_id']);
            $row['country_name'] = $countryName['country_name'];
        }

        if (in_array('labels', $types)) {
            /**
             * load model
             */
            if (!$this->modelInquiryLabel) {
                $this->modelInquiryLabel = new Model_InquiryLabel();
            }
            if (!$this->modelLabel) {
                $this->modelLabel = new Model_Label();
            }

            $LabelIds = $this->modelInquiryLabel->getByInquiryId($row['inquiry_id']);
            $labels = array();

            foreach ($LabelIds as $LabelId) {
                $label = $this->modelLabel->getById($LabelId['label_id']);
                $labels[$label['id']] = $label['label_name'];
            }
            $row['labels'] = $labels;
        }
        if (in_array('have_attachment', $types)) {
            if (!$this->modelBookingAttachment) {
                $this->modelBookingAttachment = new Model_BookingAttachment();
            }
            $have_attachment = $this->modelBookingAttachment->getByInquiryId($row['inquiry_id']);
            $row['have_attachment'] = !empty($have_attachment) ? 1 : 0;
        }
        return $row;
    }

    public function updateFullTextSearch($inquiryId) {


        $inquiry = $this->getById($inquiryId);


        $fullTextSearch = array();


        // inquiry       
        $fullTextSearch[] = trim($inquiry['inquiry_num']);
        $fullTextSearch[] = trim($inquiry['comment']);

        //customer
        $modelCustomer = new Model_Customer();
        $fullTextCustomerInquiry = $modelCustomer->getFullTextCustomerContacts($inquiry['customer_id']);
        $fullTextSearch[] = trim($fullTextCustomerInquiry);

        // booking
        $modelBooking = new Model_Booking();
        $modelBookingStatus = new Model_BookingStatus();
        $booking = $modelBooking->getByInquiryId($inquiryId);

        if ($booking) {
            $bookingId = $booking['booking_id'];
            $fullTextSearch[] = trim($booking['booking_num']);
            $fullTextSearch[] = trim($booking['title']);
            $fullTextSearch[] = trim($booking['description']);
            $status = $modelBookingStatus->getById($booking['status_id']);
            $fullTextSearch[] = trim($status['name']);


            // address
            $modelBookingAddress = new Model_BookingAddress();
            $bookingAddress = $modelBookingAddress->getByBookingId($bookingId);
            $fullTextSearch[] = trim(get_line_address($bookingAddress));

            //customer
            $fullTextCustomer = $modelCustomer->getFullTextCustomerContacts($booking['customer_id']);
            $fullTextSearch[] = trim($fullTextCustomer);

            // services
            $modelContractorServiceBooking = new Model_ContractorServiceBooking();
            $fullTextContractorServiceBooking = $modelContractorServiceBooking->getFullTextContractorServiceBooking($bookingId);
            if ($fullTextContractorServiceBooking) {
                $fullTextSearch[] = trim($fullTextContractorServiceBooking);
            }

            //estimate
            $modelBookingEstimate = new Model_BookingEstimate();
            $estimate = $modelBookingEstimate->getByBookingId($bookingId);
            if (isset($estimate['estimate_num']) && $estimate['estimate_num']) {
                $fullTextSearch[] = trim($estimate['estimate_num']);
            }

            //invoice
            $modelBookingInvoice = new Model_BookingInvoice();
            $invoice = $modelBookingInvoice->getByBookingId($bookingId);
            if (isset($invoice['invoice_num']) && $invoice['invoice_num']) {
                $fullTextSearch[] = trim($invoice['invoice_num']);
            }
            if (isset($invoice['invoice_type']) && $invoice['invoice_type']) {
                $fullTextSearch[] = trim($invoice['invoice_type']);
            }

            //complaint
            $model_Complaint = new Model_Complaint();
            $complaints = $model_Complaint->getFullTextComplaintByBookingId($bookingId);
            if ($complaints) {
                $fullTextSearch[] = trim($complaints);
            }
        }

        $data = array();
        $data['full_text_search'] = implode(' ', $fullTextSearch);

        $this->updateById($inquiryId, $data);
    }

    public function deleteRelatedInquiry($id) {

        //delete data from inquiry_label
        $this->getAdapter()->delete('inquiry_label', "inquiry_id = '{$id}'");

        //delete data from inquiry_reminder
        $this->getAdapter()->delete('inquiry_reminder', "inquiry_id = '{$id}'");

        //delete data from inquiry_discussion
        $this->getAdapter()->delete('inquiry_discussion', "inquiry_id = '{$id}'");

        //delete data from customer_reminder
        $this->getAdapter()->delete('customer_reminder', "inquiry_id = '{$id}'");

        //delete data from inquiry_address
        $this->getAdapter()->delete('inquiry_address', "inquiry_id = '{$id}'");

        //delete data from booking_attachment
        $this->getAdapter()->delete('booking_attachment', "inquiry_id = '{$id}'");

        //delete data from inquiry_service
        $this->getAdapter()->delete('inquiry_service', "inquiry_id = '{$id}'");

        //delete data from inquiry_service_attribute_value
        $this->getAdapter()->delete('inquiry_service_attribute_value', "inquiry_id = '{$id}'");

        //delete data from inquiry_type_attribute_value
        $this->getAdapter()->delete('inquiry_type_attribute_value', "inquiry_id = '{$id}'");
    }

    public function cronJobSendInquiryContactAttempt() {


        $modelCronjobHistory = new Model_CronjobHistory();
        $modelCronJob = new Model_CronJob();
        $modelMailingListUnsubscribed = new Model_MailingListUnsubscribed();


//        //
//        // save this cron in cronjob_history table
//        //
        $cronjob = $modelCronJob->getIdByName('Send_Inquiry_Contact_Attempt');
        $cronjonID = $cronjob['id'];

        $cronjobHistoryData = array(
            'cron_job_id' => $cronjonID,
            'run_time' => time()
        );
        $cronjobHistoryId = $modelCronjobHistory->insert($cronjobHistoryData);
        //*************************
        //
        // load models
        //
        $modelCustomer = new Model_Customer();
        $modelInquiryAddress = new Model_InquiryAddress();
        $modelInquiryService = new Model_InquiryService();
        $modelUser = new Model_User();



        // sub query
        //$selectInquiryLabel = $this->getAdapter()->select();
        //$selectInquiryLabel->from('inquiry_label', array('inquiry_id'));

        $select = $this->getAdapter()->select();
        $select->from(array('i' => $this->_name));
        $select->distinct();
        $select->where("i.status = 'inquiry'");
        $select->where("i.is_deleted = 0");
        $select->where("i.send_inquiry_contact_attempt != 'done'");
        //$select->where("i.inquiry_id NOT IN ?", $selectInquiryLabel);
        $select->joinleft(array('il' => 'inquiry_label'), 'i.inquiry_id = il.inquiry_id', array('inquiry_id'));
        $select->where("il.inquiry_id is NULL ");

        $select->joinInner(array('ir' => 'inquiry_reminder'), 'i.inquiry_id = ir.inquiry_id', '');
//        $select->limit(4);
        $inquiries = $this->getAdapter()->fetchAll($select);
        $model_image = new Model_Image();
        foreach ($inquiries as $inquiry) {
            $photos = $model_image->getAll($inquiry['inquiry_id'], 'inquiry');
            $customer = $modelCustomer->getById($inquiry['customer_id']);
            $user = $modelUser->getById($inquiry['user_id']);

            $template_params = array(
                //inquiry
                '{inquiry_num}' => $inquiry['inquiry_num'],
                '{comment}' => $inquiry['comment'] ? $inquiry['comment'] : '',
                '{inquiry_created}' => date('d/m/Y', $inquiry['created']),
                '{inquiry_address}' => get_line_address($modelInquiryAddress->getByInquiryId($inquiry['inquiry_id'])),
                '{service}' => nl2br($modelInquiryService->getByInquiryIdAsText($inquiry['inquiry_id'])),
                //customer
                '{customer_name}' => get_customer_name($customer),
                '{customer_first_name}' => isset($customer['first_name']) && $customer['first_name'] ? ucwords($customer['first_name']) : '',
                '{customer_last_name}' => isset($customer['last_name']) && $customer['last_name'] ? ' ' . ucwords($customer['last_name']) : '',
                '{customer_contacts}' => nl2br($modelCustomer->getCustomerContacts($inquiry['customer_id'])),
                '{sender_name}' => ucwords($user['username']),
            );

            if (count($photos) == 0) {
                $template_params['{add_photo_link}'] = ''
                        . '<div style="display: block; float: left; border: 1px solid #0966C2; padding: 10px; margin-left: 10px; width: 80px; height: 60px; text-align: center;"><a href="http://cm.tilecleaners.com.au/addPhoto/inquiry/' . $inquiry['inquiry_id'] . '"><img src="http://cm.tilecleaners.com.au/pic/camera_image.png" alt="" width="50" height="50" /><br />Add Photos</a></div>
<p style="font-size: 20px; font-style: italic; display: block; padding: 10px; float: left; width: 70%;">To help us best service your enquiry, please upload photos that highlight the areas of concern.</p>';
            } else {
                $template_params['{add_photo_link}'] = ' ';
            }

            $to = array();
            $unsubscribeCustomerEmails = $modelMailingListUnsubscribed->getByCronJobIdAndCustomerId($cronjonID, $inquiry['customer_id']);


            if ($customer['email1'] && filter_var($customer['email1'], FILTER_VALIDATE_EMAIL)) {
                $to[0] = array('email' => $customer['email1'], 'number' => '1');
            }
            if ($customer['email2'] && filter_var($customer['email2'], FILTER_VALIDATE_EMAIL)) {
                $to[1] = array('email' => $customer['email2'], 'number' => '2');
            }
            if ($customer['email3'] && filter_var($customer['email3'], FILTER_VALIDATE_EMAIL)) {
                $to[2] = array('email' => $customer['email3'], 'number' => '3');
            }





            foreach ($unsubscribeCustomerEmails as $customerEmail) {
                if ($customer['email1'] == $customerEmail['customer_email']) {
                    unset($to[0]);
                } else if ($customer['email2'] == $customerEmail['customer_email']) {
                    unset($to[1]);
                } else if ($customer['email3'] == $customerEmail['customer_email']) {
                    unset($to[2]);
                }
            }


            //$to = implode(',', $to);

            $email_log = array('reference_id' => $inquiry['inquiry_id'], 'type' => 'inquiry', 'cronjob_history_id' => $cronjobHistoryId);

            $companyId = $inquiry['company_id'];

            $success = false;
            if ($to) {


                foreach ($to as $toEmail) {

                    //$params['to'] = $toEmail['email'];
                    $template_params['{unsubscribe_link}'] = 'http://cm.tilecleaners.com.au/unsubscribe-link/' . $cronjonID . '/' . $inquiry['customer_id'] . '/' . $toEmail['number'] . '/' . $inquiry['trading_name_id'];
                    try {
                        EmailNotification::sendEmail(array('to' => $toEmail['email']), 'send_inquiry_contact_attempt', $template_params, $email_log, $companyId);
                        $sendInquiryContactAttempt = 'done';
                    } catch (Zend_Mail_Transport_Exception $e) {
                        $sendInquiryContactAttempt = 'error';
                    }
                }
            }
            $this->updateById($inquiry['inquiry_id'], array('send_inquiry_contact_attempt' => $sendInquiryContactAttempt));
        }
    }

    public function cronJobResetSendInquiryContactAttempt() {

        // sub query
        //$selectInquiryLabel = $this->getAdapter()->select();
        //$selectInquiryLabel->from('inquiry_label', array('inquiry_id'));

        $select = $this->getAdapter()->select();
        $select->from(array('i' => $this->_name));
        $select->distinct();
        $select->where("i.status = 'inquiry'");
        $select->where("i.is_deleted = 0");
        //$select->where("i.inquiry_id NOT IN ?", $selectInquiryLabel);
        $select->joinleft(array('il' => 'inquiry_label'), 'i.inquiry_id = il.inquiry_id', array('inquiry_id'));
        $select->where("il.inquiry_id is NULL ");
        $select->joinInner(array('ir' => 'inquiry_reminder'), 'i.inquiry_id = ir.inquiry_id', '');



        $inquiries = $this->getAdapter()->fetchAll($select);

        foreach ($inquiries as $inquiry) {
            $this->updateById($inquiry['inquiry_id'], array('send_inquiry_contact_attempt' => 'none'));
        }
    }

    public function getCountInquiry($myinquiries) {

        //$currentPage = $this->request->getParam('page', 1);
        //$perPage = $this->request->getParam('perPage', 15);
        $filters = array('status' => 'inquiry', 'unlabeled' => '1');

        //$filters['perPage'] = 15;
        $loggedUser = CheckAuth::getLoggedUser();

        if ($myinquiries) {

            $filters['my_inquiries'] = $loggedUser['user_id'];
            //echo 'myinquiries ';
        }

        $inquiries = $this->getAll($filters);
        if ($inquiries && !empty($inquiries)) {
            return count($inquiries);
        } else {
            return 0;
        }
    }

    //////By Nour

    public function getWheresAndJoinsByFilters($filters, $isCronJob = false) {

        $wheres = array();
        $joinInner = array();
        //     $loggedUser = CheckAuth::getLoggedUser();
        if (!$isCronJob) {
            $filters['company_id'] = CheckAuth::getCompanySession();

            // $joinInner = array();
            $joinLeft = array();
            $joinInner['trading_name'] = array('name' => array('tr' => 'trading_name'), 'cond' => 'i.trading_name_id = tr.trading_name_id', 'cols' => array('trading_name', 'website_url'));
            $loggedUser = CheckAuth::getLoggedUser();

            if (!CheckAuth::checkCredential(array('canSeeAllInquiry'))) {
                if (CheckAuth::checkCredential(array('canSeeOnlyHisInquiry'))) {
                    // $select->where("i.user_id = " . $loggedUser['user_id']);
                    $wheres['canSeeOnlyHisInquiry'] = ("i.user_id = " . $loggedUser['user_id']);
                }
            }
            if (CheckAuth::checkCredential(array('canSeeHisCompanyInquiry'))) {

                $wheres['canSeeHisCompanyInquiry'] = ("i.company_id = " . $loggedUser['company_id']);
            }

            if (CheckAuth::checkCredential(array('canSeeDeletedInquiry'))) {
                if (!empty($filters['is_deleted'])) {
                    //$select->where('i.is_deleted = 1');
                    $wheres['is_deleted'] = ('i.is_deleted = 1');
                } else {
                    // $select->where('i.is_deleted = 0');
                    $wheres['is_deleted'] = ('i.is_deleted = 0');
                }
            } else {
                //  $select->where('i.is_deleted = 0');
                $wheres['is_deleted'] = ('i.is_deleted = 0');
            }
        }   //CronJob

        if ($filters) {

            if (!empty($filters['keywords'])) {
                $keywords = $this->getAdapter()->quote('%' . trim($filters['keywords']) . '%');
                $joinInner['customer'] = array('name' => array('cust' => 'customer'), 'cond' => 'i.customer_id = cust.customer_id', 'cols' => array('cust_full_search' => 'cust.full_text_search'));



                /* if(stripos($filters['keywords'] , 'INQ')  === 0 ){
                  $rows = $this->getByNum($filters['keywords']);
                  if(!empty($rows)){
                  $select->where("i.inquiry_num = '{$filters['keywords']}' ");
                  }
                  } */
                $wheres['full_text_search'] = ("i.full_text_search LIKE {$keywords} or cust.full_text_search LIKE {$keywords} ");

                //$select->where("i.full_text_search LIKE {$keywords} or cust.full_text_search LIKE {$keywords} ");
                //$select->where("is.service_id = {$service_id}");
                //$joinInner['inquiry_service'] = array('name' => array('is' => 'inquiry_service'), 'cond' => 'i.inquiry_id = is.inquiry_id', 'cols' => array('is.service_id'));
                //$keywords = '+' . preg_replace('#[\s]+#', ' +', trim($filters['keywords']));
                //$keywords = $this->getAdapter()->quote($keywords);
                //$select->where("MATCH(i.full_text_search) AGAINST({$keywords} IN BOOLEAN MODE)");
            }
            if (!empty($filters['is_spam'])) {
                $wheres['is_spam'] = ("i.is_spam = 1 ");
                //$select->where("i.is_spam = 1 ");
            } else {
                $wheres['is_spam'] = ("i.is_spam = 0 ");

                //$select->where("i.is_spam = 0 ");
            }

            if (!empty($filters['created_from'])) {
                $created_from = $this->getAdapter()->quote(strtotime(trim($filters['created_from'])));
                // $select->where("i.created >= {$created_from}");
                $wheres['created_from'] = ("i.created >= {$created_from}");
            }


            if (!empty($filters['created_to'])) {
                $created_to = $this->getAdapter()->quote(strtotime(trim($filters['created_to'])));
                // $select->where("i.created >= {$created_from}");
                $wheres['created_to'] = ("i.created <= {$created_to}");
                // $select->where("i.created <= {$created_to}");
            }

            if (!empty($filters['customer_id'])) {
                $customer_id = $this->getAdapter()->quote(trim($filters['customer_id']));
                $wheres['customer_id'] = ("i.customer_id = {$customer_id}");
                //$select->where("i.customer_id = {$customer_id}");
            }

            if (!empty($filters['customer_name'])) {
                $modelCustomer = new Model_Customer();
                $allCustomer = $modelCustomer->getCustomerIdByIdFullNameSearch(trim($filters['customer_name']));

                $customerIds = implode(', ', $allCustomer);
                if ($customerIds) {
                    $wheres['customer_name'] = ("i.customer_id in ( $customerIds )");
                    // $select->where("i.customer_id in ( $customerIds )");
                }
            }

            if (!empty($filters['service_id'])) {

                $service_id = (int) $filters['service_id'];
                $wheres['service_id'] = ("is.service_id = {$service_id}");
                //  $select->where("is.service_id = {$service_id}");
                $joinInner['inquiry_service'] = array('name' => array('is' => 'inquiry_service'), 'cond' => 'i.inquiry_id = is.inquiry_id', 'cols' => array('is.service_id'));
            }

            if (!empty($filters['inquiry_num'])) {
                $inquiry_num = $this->getAdapter()->quote('%' . trim($filters['inquiry_num']) . '%');
                $wheres['inquiry_num'] = ("i.inquiry_num LIKE {$inquiry_num}");
                //$select->where("i.inquiry_num LIKE {$inquiry_num}");
            }

            if (!empty($filters['city_id'])) {
                $city_id = (int) $filters['city_id'];
                $wheres['inquiry_num'] = ("i.city_id = {$city_id}");
                //$select->where("i.city_id = {$city_id}");
            }

            if (!empty($filters['inquiry_type_id'])) {
                $inquiry_type_id = (int) trim($filters['inquiry_type_id']);
                // $select->where("i.inquiry_type_id = {$inquiry_type_id}");
                $wheres['inquiry_type_id'] = ("i.inquiry_type_id = {$inquiry_type_id}");
            }

            if (!empty($filters['status'])) {
                if ($filters['status'] != 'all') {
                    $status = $this->getAdapter()->quote(trim($filters['status']));
                    //$select->where("i.status = {$status}");
                    $wheres['status'] = ("i.status = {$status}");
                }
            }

            if (!empty($filters['label_ids'])) {
                if ($filters['label_ids']) {
                    $joinInner['inquiry_label'] = array('name' => array('il' => 'inquiry_label'), 'cond' => 'i.inquiry_id = il.inquiry_id', 'cols' => '');
                    //     $select->where('il.label_id IN (' . implode(', ', $filters['label_ids']) . ')');
                    $wheres['label_ids'] = ('il.label_id IN (' . implode(', ', $filters['label_ids']) . ')');
                }
            }

            if (!empty($filters['unlabeled']) && $filters['unlabeled'] == true) {

                $sql = $this->getAdapter()->select();
                $sql->from('inquiry_label', 'inquiry_id');
                $sql->distinct();
                $results = $this->getAdapter()->fetchAll($sql);

                $inquiry_ids = array();
                foreach ($results as $result) {
                    $inquiry_ids[] = $result['inquiry_id'];
                }

                if ($inquiry_ids) {
                    $wheres['unlabeled'] = ('i.inquiry_id NOT IN (' . implode(', ', $inquiry_ids) . ')');
                    // $select->where('i.inquiry_id NOT IN (' . implode(', ', $inquiry_ids) . ')');
                }
            }

            if (!empty($filters['address'])) {
                $address = trim($filters['address']);
                $joinInner['inquiry_address'] = array('name' => array('ia' => 'inquiry_address'), 'cond' => 'i.inquiry_id = ia.inquiry_id', 'cols' => '');
                //   $select->where("CONCAT_WS(' ',ia.street_number ,ia.street_address ,ia.suburb,ia.state,ia.postcode) LIKE '%{$address}%'");
                $wheres['address'] = ("CONCAT_WS(' ',ia.street_number ,ia.street_address ,ia.suburb,ia.state,ia.postcode) LIKE '%{$address}%'");
            }

            if (!empty($filters['email'])) {
                $email = trim($filters['email']);
                $joinInner['customer'] = array('name' => array('c' => 'customer'), 'cond' => 'i.customer_id = c.customer_id', 'cols' => '');
                $wheres['email'] = ("CONCAT_WS(' ',c.email1 ,c.email2 ,c.email3) LIKE '%{$email}%'");
                //$select->where("CONCAT_WS(' ',c.email1 ,c.email2 ,c.email3) LIKE '%{$email}%'");
            }

            if (!empty($filters['mobile/phone'])) {
                $mobile_phone = trim($filters['mobile/phone']);
                $joinInner['customer'] = array('name' => array('c' => 'customer'), 'cond' => 'i.customer_id = c.customer_id', 'cols' => '');
                $wheres['email'] = ("CONCAT_WS(' ',c.phone1 ,c.phone2 ,c.phone3,c.mobile1 ,c.mobile2 ,c.mobile3) LIKE '%{$mobile_phone}%'");
                //$select->where("CONCAT_WS(' ',c.phone1 ,c.phone2 ,c.phone3,c.mobile1 ,c.mobile2 ,c.mobile3) LIKE '%{$mobile_phone}%'");
            }

            if (!empty($filters['bussiness_name'])) {
                $joinInner['customer_commercial_info'] = array('name' => array('cci' => 'customer_commercial_info'), 'cond' => 'i.customer_id = cci.customer_id', 'cols' => '');
                $wheres['bussiness_name'] = ("cci.business_name LIKE '%{$filters['bussiness_name']}%'");
                //$select->where("cci.business_name LIKE '%{$filters['bussiness_name']}%'");
            }

            if (!empty($filters['deferred_to'])) {
                $deferred_to = $this->getAdapter()->quote(strtotime(trim($filters['deferred_to'])));
                $wheres['deferred_to'] = ("i.deferred_date = {$deferred_to}");
                //$select->where("i.deferred_date = {$deferred_to}");
            }

            if (!empty($filters['to_follow_date'])) {
                switch ($filters['to_follow_date']) {
                    case 'yesterday':
                        $yesterday = getTimePeriodByName('yesterday');
                        //  $select->where("i.deferred_date between '" . strtotime($yesterday['start']) . "' and '" . strtotime($yesterday['end']) . "'");
                        $wheres['to_follow_date_yesterday'] = ("i.deferred_date between '" . strtotime($yesterday['start']) . "' and '" . strtotime($yesterday['end']) . "'");
                        break;
                    case 'today':
                        $today = getTimePeriodByName('today');

                        $wheres['to_follow_date_today'] = ("i.deferred_date between '" . strtotime($today['start']) . "' and '" . strtotime($today['end']) . "'");

                        //$select->where("i.deferred_date between '" . strtotime($today['start']) . "' and '" . strtotime($today['end']) . "'");
                        break;
                    case 'tomorrow':
                        $tomorrow = getTimePeriodByName('tomorrow');
                        $wheres['to_follow_date_tomorrow'] = ("i.deferred_date between '" . strtotime($tomorrow['start']) . "' and '" . strtotime($tomorrow['end']) . "'");
                        //$select->where("i.deferred_date between '" . strtotime($tomorrow['start']) . "' and '" . strtotime($tomorrow['end']) . "'");
                        break;
                    case 'past':
                        $today = getTimePeriodByName('today');
                        $wheres['to_follow_date_past'] = ("i.deferred_date < '" . strtotime($today['start']) . "'");
                        // $select->where("i.deferred_date < '" . strtotime($today['start']) . "'");
                        break;
                    case 'future':
                        $today = getTimePeriodByName('today');
                        $wheres['to_follow_date_future'] = ("i.deferred_date > '" . strtotime($today['end']) . "'");
                        //$select->where("i.deferred_date > '" . strtotime($today['end']) . "'");
                        break;
                }
            }

//            if (!empty($filters['to_follow']) && $filters['to_follow'] == true) {
//                $time = time();
//                $select->where("i.deferred_date <= {$time} ");
//                $select->where("i.deferred_date > 0 ");
//            }

            if (!empty($filters['to_follow']) && $filters['to_follow'] == true) {
                $timeNow = time();
                $wheres['to_follow'] = ("i.is_to_follow = 1");

                // $select->where("i.is_to_follow = 1");
            }
//            if(!empty($filters['past_to_follow']) && $filters['past_to_follow'] == true){
//                $wheres['past_to_follow'] = ("i.deferred_date <= $timeNow");
//            }

            if (!empty($filters['company_id'])) {
                $company_id = (int) $filters['company_id'];
                $wheres['company_id'] = ("i.company_id = {$company_id}");
                // $select->where("i.company_id = {$company_id}");
            }

            if (!empty($filters['un_reminded']) && $filters['un_reminded'] == true) {

                $sql = $this->getAdapter()->select();
                $sql->from('inquiry_reminder', 'inquiry_id');
                $sql->distinct();
                $results = $this->getAdapter()->fetchAll($sql);

                $inquiry_ids = array();
                foreach ($results as $result) {
                    $inquiry_ids[] = $result['inquiry_id'];
                }

                if ($inquiry_ids) {
                    $wheres['un_reminded'] = ('i.inquiry_id NOT IN (' . implode(', ', $inquiry_ids) . ')');
                    // $select->where('i.inquiry_id NOT IN (' . implode(', ', $inquiry_ids) . ')');
                }
            }

            if (!empty($filters['my_inquiries'])) {

                $user_id = (int) $filters['my_inquiries'];

                $my_inquiry_sql = $this->getAdapter()->select();
                $my_inquiry_sql->from('inquiry_reminder', 'inquiry_id');
                $my_inquiry_sql->where("user_id = {$user_id}");
                $wheres['user_id'] = ("user_id = {$user_id}");
                $my_inquiry_sql->distinct();

                $my_inquiry_results = $this->getAdapter()->fetchAll($my_inquiry_sql);

                $not_my_inquiry_sql = $this->getAdapter()->select();
                $not_my_inquiry_sql->from('inquiry_reminder', 'inquiry_id');
                $not_my_inquiry_sql->distinct();

                if ($my_inquiry_results) {
                    $my_inquiry_ids = array();
                    foreach ($my_inquiry_results as $my_result) {
                        $my_inquiry_ids[$my_result['inquiry_id']] = $my_result['inquiry_id'];
                    }
                    $not_my_inquiry_sql->where('inquiry_id NOT IN (' . implode(', ', $my_inquiry_ids) . ')');
                    $wheres['my_inquiry_id'] = ('inquiry_id NOT IN (' . implode(', ', $my_inquiry_ids) . ')');
                }

                $not_my_inquiry_results = $this->getAdapter()->fetchAll($not_my_inquiry_sql);

                $not_my_inquiry_ids = array();
                foreach ($not_my_inquiry_results as $not_my_result) {
                    $not_my_inquiry_ids[$not_my_result['inquiry_id']] = $not_my_result['inquiry_id'];
                }

                if ($not_my_inquiry_ids) {
                    // $select->where('i.inquiry_id NOT IN (' . implode(', ', $not_my_inquiry_ids) . ')');
                    $wheres['not_my_inquiry_id'] = ('i.inquiry_id NOT IN (' . implode(', ', $not_my_inquiry_ids) . ')');
                }
            }

            if (!empty($filters['booking_num'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'i.inquiry_id = bok.original_inquiry_id', 'cols' => '');
                $booking_num = $this->getAdapter()->quote('%' . trim($filters['booking_num']) . '%');
                //$select->where("bok.booking_num LIKE {$booking_num}");
                $wheres['booking_num'] = ("bok.booking_num LIKE {$booking_num}");
            }

            if (!empty($filters['estimate_num'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'i.inquiry_id = bok.original_inquiry_id', 'cols' => '');
                $joinInner['booking_estimate'] = array('name' => array('est' => 'booking_estimate'), 'cond' => 'bok.booking_id = est.booking_id', 'cols' => '');
                $estimate_num = $this->getAdapter()->quote('%' . trim($filters['estimate_num']) . '%');
                $wheres['estimate_num'] = ("est.estimate_num LIKE {$estimate_num}");
                //  $select->where("est.estimate_num LIKE {$estimate_num}");
            }

            if (!empty($filters['contractor_id'])) {
                $contractor_id = (int) $filters['contractor_id'];
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'i.inquiry_id = bok.original_inquiry_id', 'cols' => '');
                $joinInner['contractor_service_booking'] = array('name' => array('csb' => 'contractor_service_booking'), 'cond' => 'bok.booking_id = csb.booking_id', 'cols' => '');
                // $select->where("bok.created_by = {$contractor_id} OR csb.contractor_id = {$contractor_id}");
                $wheres['contractor_id'] = ("bok.created_by = {$contractor_id} OR csb.contractor_id = {$contractor_id}");
            }
        }
        return array('wheres' => $wheres, 'joinInner' => $joinInner);
    }

    public function getCount($filters = array()) {


        $select = $this->getAdapter()->select();
        $select->from(array('i' => $this->_name), array('count' => 'COUNT(*)'));

        $wheresAndJoins = $this->getWheresAndJoinsByFilters($filters);

        $joinInner = $wheresAndJoins['joinInner'];
        $wheres = $wheresAndJoins['wheres'];


        if ($wheres) {
            foreach ($wheres as $where) {
                $select->where($where);
            }
        }


        if ($joinInner) {
            foreach ($joinInner as $inner) {
                $select->joinInner($inner['name'], $inner['cond'], $inner['cols']);
            }
        }

        $sql = $select->__toString();

        return $this->getAdapter()->fetchAll($select);
    }

    public function cronJobFaqCustomerEveryDay() {

        $modelCronjobHistory = new Model_CronjobHistory();
        $modelCronJob = new Model_CronJob();
        //
        // save this cron in cronjob_history table
        //
	$cronjob = $modelCronJob->getIdByName('Faq_Customer_Every_Day');
        $cronjonID = $cronjob['id'];
////
        $cronjobHistoryData = array(
            'cron_job_id' => $cronjonID,
            'run_time' => time()
        );
        $cronjobHistoryId = $modelCronjobHistory->insert($cronjobHistoryData);
//        
        //************

        $date1 = strtotime('-1 day', time());
        $date2 = time();

//        echo date('Y-m-d', $date1);

        $select_inqueries = $this->getAdapter()->select();
        $select_inqueries->from(array('inq' => $this->_name));
        $select_inqueries->where("inq.is_deleted = 0");
        $select_inqueries->where("inq.status = 'inquiry'");
        $select_inqueries->joinInner(array('cst' => 'customer'), 'inq.customer_id = cst.customer_id', array('customer_email1' => 'email1', 'customer_email2' => 'email2', 'customer_email3' => 'email3', 'first_name' => 'first_name'));
        $select_inqueries->where("inq.created BETWEEN '" . $date1 . "' AND '" . $date2 . "'");
//        $select_inqueries->where('inq.inquiry_id = 27328');
        $customer_inqueries = $this->getAdapter()->fetchAll($select_inqueries);
//        echo $select_inqueries->__toString();
        $select_booking = $this->getAdapter()->select();
        $select_booking->from(array('bok' => 'booking'));
        $select_booking->where("bok.is_deleted = 0");
        $select_booking->joinInner(array('cst' => 'customer'), 'bok.customer_id = cst.customer_id', array('customer_email1' => 'email1', 'customer_email2' => 'email2', 'customer_email3' => 'email3', 'first_name' => 'first_name'));
        $select_booking->where("bok.created BETWEEN '" . $date1 . "' AND '" . $date2 . "'");
        $customer_bookings = $this->getAdapter()->fetchAll($select_booking);

        /* if(my_ip('176.106.46.142')){
          echo $select_booking->__toString();
          exit;
          } */
//        $customer_bookings = array();
        $items = array_merge($customer_bookings, $customer_inqueries);
        $modelCustomer = new Model_Customer();
        $modelInquiryServiceAttributeValue = new Model_InquiryServiceAttributeValue();
        $model_itemImage = new Model_ItemImage();
        $trading_namesObj = new Model_TradingName();

        foreach ($items as $item) {
            if (isset($item['inquiry_id'])) {
                $item_id = $item['inquiry_id'];
                $type = 'inquiry';
            } else {
                $item_id = $item['booking_id'];
                $type = 'booking';
            }
            $modelServiceAttribute = new Model_ServiceAttribute();
            if (isset($item['inquiry_id'])) {
                $model_originalInquiry = new Model_OriginalInquiry();
                if ($item['original_inquiry_id'] == 0) {
                    $trading_names = $trading_namesObj->getById($item['trading_name_id']);
                } else {
                    $OriginalInquiryData = $model_originalInquiry->getById($item['original_inquiry_id']);
                    $trading_names = $trading_namesObj->getTradingNameByWebsite($OriginalInquiryData['website']);
                }
                $modelInquiryService = new Model_InquiryService();
                $allItemService = $modelInquiryService->getByInquiryId($item_id);
            } else {
                $mdoel_contractorServiceBooking = new Model_ContractorServiceBooking();
                $allItemService = $mdoel_contractorServiceBooking->getByBookingId($item_id);
                $trading_names = $trading_namesObj->getById($item['trading_name_id']);
            }

            $faq_html = "";
            $photos_html = "";
            $wheres = array();

            foreach ($allItemService as $key => $itemService) {
                $model_attributes = new Model_Attributes();
                $attribute = $model_attributes->getByAttributeName("Floor", $item['company_id']);
//                var_dump($attribute);
                $allServiceAttribute = $modelServiceAttribute->getByAttributeIdAndServiceId($attribute['attribute_id'], $itemService['service_id']);
                if (isset($item['inquiry_id'])) {
                    $serviceAttributeValue = $modelInquiryServiceAttributeValue->getByInquiryIdAndServiceAttributeIdAndClone($item_id, $allServiceAttribute['service_attribute_id'], $itemService['clone']);
                } else {
                    $modelServiceAtrributeValue = new Model_ServiceAttributeValue();
                    $serviceAttributeValue = $modelServiceAtrributeValue->getByBookingIdAndServiceAttributeIdAndClone($item_id, $allServiceAttribute['service_attribute_id'], $itemService['clone']);
                }
                if (!empty($serviceAttributeValue['value'])) {
                    $wheres[] = 'service_id =' . $itemService['service_id'] . ' and floor_id = ' . $serviceAttributeValue['value'];
                }
            }



            if (!empty($wheres)) {
                $model_faq = new Model_Faq();
                $faq = $model_faq->getFaqByServiceIdAndFloorId($wheres);
                $photos = $model_itemImage->getByServiceIdAndFloorId($wheres);
                $customer = $modelCustomer->getById($item['customer_id']);
//                  var_dump($faq);
                if (empty($faq)) {
                    $faq = $model_faq->getFaqByServiceIdAndFloorId($wheres, 1);
                }

                foreach ($faq as $key => $faq_value) {
                    $question = str_replace('$custom_city', $customer['city_name'], $faq_value['question']);
                    $answer = str_replace('$custom_city', $customer['city_name'], $faq_value['answer']);
                    $faq_html .= '<ul><li><p><a style="color:#0966c2;font-weight:bold;" href="' . $trading_names['website_url'] . '">' . $question . '</a></p></li><li style="list-style:none;"><p>' . $answer . '</p></li></ul>';
                }
                if (!empty($photos)) {
                    $photos_html = ' <table width="100%" cellspacing="0" cellpadding="0" align="center" style="position: relative;">
						<tbody><tr>
							<td valign="top">
								<table width="600" cellspacing="0" cellpadding="0" align="center">
									<tbody>
									<tr>
										<td width="5" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
										<td width="590" valign="top">';
                    foreach ($photos as $key => $photo) {

                        if ($key % 2 == 0) {
                            $photos_html .="<tr>";
                        }
                        if ($photo['name'] == 'before') {
                            $photos_html .= '<table width="600" cellspacing="0" cellpadding="0" align="center">
									<tbody><tr>
										<td colspan="3" height="40" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
									</tr>
									<tr>
										<td width="5" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
										<td width="590" valign="top">											
											<table class="columnMargin" cellspacing="0" cellpadding="0" width="295" align="left" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
												<tbody><tr>
													<td valign="top" style="font-family: Helvetica, Arial, sans-serif; font-size: 13px; color: #363d45; font-weight: bold; line-height: 22px; text-align: center;" class="text">
														<img src="http://cm.tilecleaners.com.au/uploads/image_attachment/' . $photo['small_path'] . '" width="100%"  alt="TEXT HERE." style="border: 0; font-family: Helvetica, Arial, sans-serif; font-size: 30px; color: #363d45; text-align: center;">
														<br>
														Before
													</td>
												</tr>
											</tbody></table>';
                        } else if ($photo['name'] == 'after') {
                            $photos_html .= '										
											<table class="columnMargin" cellspacing="0" cellpadding="0" width="295" align="left" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
												<tbody><tr>
													<td valign="top" style="font-family: Helvetica, Arial, sans-serif; font-size: 13px; color: #363d45; font-weight: bold; line-height: 22px; text-align: center;" class="text">
														<img src="http://cm.tilecleaners.com.au/uploads/image_attachment/' . $photo['small_path'] . '" width="100%"  alt="TEXT HERE." style="border: 0; font-family: Helvetica, Arial, sans-serif; font-size: 30px; color: #363d45; text-align: center;">
														<br>
														After
													</td>
												</tr>
											</tbody></table>';
                        }
                    }
                    $photos_html .='</td>
										<td width="5" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
									</tr>
									<tr>
										<td colspan="3" height="40" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
									</tr>
								</tbody></table>
							</td>
						</tr>
					</tbody><div id="bin" style="left: 994.5px; top: 111.5px; display: block;"></div></table>';
                }

                $to = array();

                if ($customer['email1'] && filter_var($customer['email1'], FILTER_VALIDATE_EMAIL)) {
                    $to[] = $customer['email1'];
                }
                if ($customer['email2'] && filter_var($customer['email2'], FILTER_VALIDATE_EMAIL)) {
                    $to[] = $customer['email2'];
                }
                if ($customer['email3'] && filter_var($customer['email3'], FILTER_VALIDATE_EMAIL)) {
                    $to[] = $customer['email3'];
                }


                $router = Zend_Controller_Front::getInstance()->getRouter();
                $template_params = array(
                    '{faq}' => $faq_html,
                    '{photos}' => $photos_html,
                    "{phone}" => '<a style="color:white;" href="tel:' . $trading_names['phone'] . '"/>' . $trading_names['phone'] . '</a>',
                    "{trading_name}" => $trading_names['trading_name'],
                    "{trading_name_id}" => $trading_names['trading_name_id'],
                );
                $to = implode(',', $to);
                $modelEmailTemplate = new Model_EmailTemplate();
                $emailTemplate = $modelEmailTemplate->getEmailTemplate('Faq_Customer_Every_Day', $template_params, $customer['company_id']);
                ///get the creator of this booking
                $modelUser = new Model_User();
                $user = $modelUser->getById($item_id);
//  
//                var_dump($emailTemplate);
                $params = array(
                    'to' => $to,
                    'reply' => array('name' => $user['display_name'], 'email' => $user['email1']),
                    'body' => $emailTemplate['body'],
                    'subject' => $emailTemplate['subject'],
                    'companyId' => $item['company_id'],
                    'trading_name' => $trading_names['trading_name'],
                    'from' => $trading_names['email'],
                );


//            var_dump($trading_names);
//            exit;
                //  $email_log = array('reference_id' => $item_id, 'cronjob_history_id' => $cronjobHistoryId, 'type' => $type);
                $email_log = array('reference_id' => $item_id, 'type' => $type, 'cronjob_history_id' => $cronjobHistoryId);
//                $email_log = array();

                if ($to) {
                    try {
                        EmailNotification::sendEmail($params, 'Faq_Customer_Every_Day', $template_params, $email_log, $item['company_id']);
                    } catch (Zend_Mail_Transport_Exception $e) {
                        
                    }
                }
            }
        }
    }

    public function cronJobPhotoRequest() {

        $modelCustomer = new Model_Customer();
        $model_originalInquiry = new Model_OriginalInquiry();
        $trading_namesObj = new Model_TradingName();
        $modelCronjobHistory = new Model_CronjobHistory();
        $modelCronJob = new Model_CronJob();
        //
        // save this cron in cronjob_history table
        //
	$cronjob = $modelCronJob->getIdByName('photo_request');
        $cronjonID = $cronjob['id'];

        $cronjobHistoryData = array(
            'cron_job_id' => $cronjonID,
            'run_time' => time()
        );
        $cronjobHistoryId = $modelCronjobHistory->insert($cronjobHistoryData);

        $select_inqueries = $this->getAdapter()->select();
        $select_inqueries->from(array('inq' => $this->_name));
        $select_inqueries->distinct();
        $select_inqueries->joinleft(array('il' => 'inquiry_label'), 'inq.inquiry_id = il.inquiry_id', array('inquiry_id as label_inquiry_id'));
        $select_inqueries->where("inq.is_deleted = 0");
        $select_inqueries->where("inq.status = 'inquiry'");
        $select_inqueries->where("inq.is_spam = 0 ");
        $select_inqueries->where('inq.company_id = 1');
        $select_inqueries->where("il.inquiry_id is NULL ");
        $customer_inqueries = $this->getAdapter()->fetchAll($select_inqueries);
        $model_image = new Model_Image();
        foreach ($customer_inqueries as $key => $inquiry) {
            $photos = $model_image->getAll($inquiry['inquiry_id'], 'inquiry');
            $is_send_photo = $inquiry['is_send_photo'];
            if (count($photos) == 0) {
                $customer = $modelCustomer->getById($inquiry['customer_id']);
                $to = array();
                if ($customer['email1'] && filter_var($customer['email1'], FILTER_VALIDATE_EMAIL)) {
                    $to[] = $customer['email1'];
                }
                if ($customer['email2'] && filter_var($customer['email2'], FILTER_VALIDATE_EMAIL)) {
                    $to[] = $customer['email2'];
                }
                if ($customer['email3'] && filter_var($customer['email3'], FILTER_VALIDATE_EMAIL)) {
                    $to[] = $customer['email3'];
                }


                if ($inquiry['original_inquiry_id'] == 0) {
                    $trading_names = $trading_namesObj->getById($inquiry['trading_name_id']);
                } else {
                    $OriginalInquiryData = $model_originalInquiry->getById($inquiry['original_inquiry_id']);
                    $trading_names = $trading_namesObj->getTradingNameByWebsite($OriginalInquiryData['website']);
                }

                $template_params = array(
                    "{customer_name}" => $customer['first_name'],
                    "{trading_name}" => $trading_names['trading_name'],
                    "{trading_name_id}" => $trading_names['trading_name_id'],
                    '{add_photo_link}' => 'http://cm.tilecleaners.com.au/addPhoto/inquiry/' . $inquiry['inquiry_id'],
                    '{type}' => 'inquiry'
                );

                $modelEmailTemplate = new Model_EmailTemplate();
                $emailTemplate = $modelEmailTemplate->getEmailTemplate('photo_request', $template_params, $customer['company_id']);
//                var_dump($emailTemplate);
//                exit;
                ///get the creator of this booking
                $modelUser = new Model_User();
                $user = $modelUser->getById($inquiry['inquiry_id']);
//
                $params = array(
                    'to' => $to,
                    'reply' => array('name' => $user['display_name'], 'email' => $user['email1']),
                    'body' => $emailTemplate['body'],
                    'subject' => $emailTemplate['subject'],
                    'companyId' => $inquiry['company_id'],
                    'trading_name' => $trading_names['trading_name'],
                    'from' => $trading_names['email'],
                );

                $email_log = array('reference_id' => $inquiry['inquiry_id'], 'type' => 'inquiry', 'cronjob_history_id' => $cronjobHistoryId);
//                $email_log = array();

                if ($to) {
                    try {
                        if ($is_send_photo == 0) {
                            EmailNotification::sendEmail($params, 'photo_request', $template_params, $email_log, $inquiry['company_id']);
                        }
                        $is_send_photo +=1;
                        if ($is_send_photo == 3) {
                            $is_send_photo = 0;
                        }
                        $this->updateById($inquiry['inquiry_id'], array('is_send_photo' => $is_send_photo));
                    } catch (Zend_Mail_Transport_Exception $e) {
                        
                    }
                }
            }
        }

        exit;
    }

//    

    /**
     * get table row according to the customer id
     * 
     * @param int $id
     * @return array 
     */
    public function getByCustomerId($id, $isLastBooking = 0) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("customer_id = '{$id}'");
        $select->where("is_deleted = 0");
        if ($isLastBooking = 0) {
            $select->order("inquiry_id desc");
            $select->limit(1);
        }
        $result = $this->getAdapter()->fetchAll($select);

        return $result;
    }

     public function cronJobSendSmsInquiryContactAttempt() {
        $modelCronjobHistory = new Model_CronjobHistory();
        $modelSmsTemplate = new Model_SmsTemplate();
        $modelSmsHistory = new Model_SmsHistorty();
        $modelCronJob = new Model_CronJob();
		$modelBooking = new Model_Booking();
		$modelBooking->cronJobGetSmsInfo();
   
         $cronjob = $modelCronJob->getIdByName('Send_Sms_Inquiry_Contact_Attempt');
          $cronjonID = $cronjob['id'];

          $cronjobHistoryData = array(
          'cron_job_id' => $cronjonID,
          'run_time' => time()
          );
         
        $cronjobHistoryId = $modelCronjobHistory->insert($cronjobHistoryData);

        $modelCustomer = new Model_Customer();
        $modelUser = new Model_User();
        $select = $this->getAdapter()->select();
        $select->from(array('i' => $this->_name));
        $select->distinct();
        $select->where("i.status = 'inquiry'");
        $select->where("i.is_deleted = 0"); //error
        $select->where("i.send_sms_inquiry_contact_attempt = 'none'");
        //$select->where("i.inquiry_id NOT IN ?", $selectInquiryLabel);
        $select->joinleft(array('il' => 'inquiry_label'), 'i.inquiry_id = il.inquiry_id', array('il_inquiry_id' => 'inquiry_id'));
        $select->where("il.inquiry_id is NULL ");
        $select->joinInner(array('ir' => 'inquiry_reminder'), 'i.inquiry_id = ir.inquiry_id', '');
//      $select->limit(4);
        $inquiries = $this->getAdapter()->fetchAll($select);
        $modelTradingName = new Model_TradingName();
        foreach ($inquiries as $inquiry) {
            $customer = $modelCustomer->getById($inquiry['customer_id'], 1);
            $user = $modelUser->getById($inquiry['user_id']);
			if($customer['pause_customer_sms'] or  $inquiry['pause_inquiry_sms'] )
            continue;
            $trading_info = $modelTradingName->getById($inquiry['trading_name_id']);
            $trading_name = $trading_info['trading_name'];
            $phone = $trading_info['phone'];

            //Hi {customerFirstName}, We tried calling you! Please call us back on {phone}. Regards, {trading_name}
            $customer_first_names = explode(" ", $customer['first_name']);


			//Hi {customerFirstName}, We tried calling you! Please call us back on {phone}. Regards, {trading_name}
            $template_params = array(
                '{customerFirstName}' => isset($customer_first_names[0]) && $customer_first_names[0] ? ucwords($customer_first_names[0]) : '',
				'{phone}'             => $phone,
				'{trading_name}'      => $trading_name,
            );
            
            
            $templateInfo = $modelSmsTemplate->getByName('send message to customer for inquiry');
            $template_id = $templateInfo['id'];
            $smsTemplate = $modelSmsTemplate->getsmsTemplate($template_id, $template_params);
            $message = $smsTemplate['message'];
            $to_customer = array();
            array_push($to_customer, $modelSmsHistory->getMobileFormat($customer['mobile1']), $modelSmsHistory->getMobileFormat($customer['mobile2']), $modelSmsHistory->getMobileFormat($customer['mobile3']));

            for ($i = 0; $i < count($to_customer); $i++) {
                $company_id = CheckAuth::getCompanySession();
                $api_name = 'twilio';

                $model_api_parameters = new Model_ApiParameters();

                $parameter_name = 'from number';
                $parameter = $model_api_parameters->getParameterValue($company_id, $api_name, $parameter_name);
                $fromNumber = $parameter['parameter_value']; 
                
                //$fromNumber = "+61447075733";
                if ($to_customer[$i] != '')
                    $toNumber = "$to_customer[$i]";
                else
                continue;
				$date=date("Y-m-d H:i:s");
				$is_send_today=$modelSmsHistory->is_send_today($date,'inquiry',$inquiry['inquiry_id'],$cronjonID);
				if(count($is_send_today)==1)
				{
					$sendInquiryContactAttempt= 'done';
					$this->updateById($inquiry['inquiry_id'], array('send_inquiry_contact_attempt' => $sendInquiryContactAttempt));
					continue;
				}
				
		
                $sms_id = $modelSmsHistory->sendSmsTwilio($fromNumber, $toNumber, $message);
                if ($sms_id) {
                    $params = array(
                        'reference_id'      => $template_id,
                        'from'              => $fromNumber,
                        'to'                => $toNumber,
                        'message_sid'       => $sms_id,
                        'message'           => $message,
                        'receiver_id'       => $inquiry['customer_id'],
                        'status'            => '',
                        'sms_type'          => 'sent',
                        'sms_reason'        => 'inquiry',
                        'reason_id'         => $inquiry['inquiry_id'],
                        'template_type'     => 'standard',
						'cron_job_history'  => $cronjobHistoryId,
	
                    );
                    $modelSmsHistory->insert($params);
                    $sendInquiryContactAttempt = 'done';
                    $this->updateById($inquiry['inquiry_id'], array('send_sms_inquiry_contact_attempt' => $sendInquiryContactAttempt));
                    sleep(500);
                } else {
                    $sendInquiryContactAttempt = 'error';
                    $this->updateById($inquiry['inquiry_id'], array('send_sms_inquiry_contact_attempt' => $sendInquiryContactAttempt));
                }
            }
        }
    }

}
