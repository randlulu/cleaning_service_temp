<?php

class Model_FaqService extends Zend_Db_Table_Abstract {

    protected $_name = 'faq_service';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order($order);

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }
    
     public function insert(array $data) {

    
        $id = parent::insert($data);

     
        return $id;
        
    }
	
	

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "faq_service_id = '{$id}'");
    }
	
	public function getServicesAndFloorByFaqId($id){
	  
	    $select = $this->getAdapter()->select();
        $select->from(array('fs' => $this->_name));
        $select->joinInner(array('s' => 'service'), 'fs.service_id = s.service_id' , array('service_name'));
        $select->joinInner(array('alv' => 'attribute_list_value'), 'alv.attribute_value_id = fs.floor_id' , array('attribute_value'));
		$select->where("fs.faq_id = {$id}");
		
		return $this->getAdapter()->fetchAll($select);
	
	}

    /**
     * delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("faq_service_id = '{$id}'");
    }

	public function deleteByFaqId($id) {
        $id = (int) $id;
        return parent::delete("faq_id = '{$id}'");
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("faq_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }
	
	public function getByServiceId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("service_id  = '{$id}'");
        $select->where("floor_id = 121");

        return $this->getAdapter()->fetchAll($select);
    }

//    public function getByCompanyId($companyId) {
//        $companyId = (int) $companyId;
//        $select = $this->getAdapter()->select();
//        $select->from($this->_name);
//        $select->where("company_id = '{$companyId}'");
//
//        return $this->getAdapter()->fetchRow($select);
//    }

}