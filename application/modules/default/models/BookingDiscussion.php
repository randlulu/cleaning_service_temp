<?php

class Model_BookingDiscussion {

    protected $_dbhost = '127.0.0.1';
    protected $_dbName = 'cleaning_service';
    protected $_collectionName = 'booking_discussion';
    protected $_con;
    protected $_collection;
    protected $_db;

    //protected static $db;

    /**
     * __construct
     * 
     * @param array $data as data
     * @param array $config as array of config
     * 
     * @return null
     */
    public function __construct($data = array(), $config = array()) {
        $dbhost = 'localhost';
        // Connect to test database  
        $this->_con = new Mongo("mongodb://127.0.0.1");

        //$db = $con->cleaning_service;
        $this->_db = $this->_con->selectDB($this->_dbName);

        $this->_collection = $this->_db->selectCollection($this->_collectionName);
    }

    public function closeConnection() {
        $isClosed = $this->_con->close();
        return $isClosed;
    }
	
	public function insert($doc = array()) {
		//
		//$loggedUser = CheckAuth::getLoggedUser();
		if(isset($doc['user_id']) && (!isset($doc['user_name']) || !isset($doc['avatar']) || !isset($doc['user_role_name'] ))){
			$modelUser = new Model_User();
			$modelContractorInfo = new Model_ContractorInfo();
			$modelAuthRole = new Model_AuthRole();
			//$contractorRoleId = $modelAuthRole->getRoleIdByName('contractor');
			$adminRoleId = $modelAuthRole->getRoleIdByName('super_admin');
			$manggerRoleId = $modelAuthRole->getRoleIdByName('manager');
			$salesRoleId = $modelAuthRole->getRoleIdByName('sales');
                        $contractorRoleId = $modelAuthRole->getRoleIdByName('contractor');
                        $customerRoleId = $modelAuthRole->getRoleIdByName('customer');

		
			$roles = array($adminRoleId, $manggerRoleId, $salesRoleId);
			$user = $modelUser->getById($doc['user_id']);
			if(!$user){
				$modelCustomer = new Model_Customer();
				$user = $modelCustomer->getById($doc['user_id']);
				$user['role_name'] = 'customer';
				$user['role_id'] = $modelAuthRole->getRoleIdByName('customer');
				$user['username'] = $user['first_name'] . ' ' . $user['last_name'];
			}
			
			if ($user['role_name'] == 'contractor') {
				$contractorInfo = $modelContractorInfo->getByContractorId($user['user_id']);
				$user['username'] = ucwords($user['username']) . ' - ' . ucwords($contractorInfo['business_name']);
			} else {
				$user['username'] = ucwords($user['username']);
			}
			
			if(!isset($doc['user_name'])){
				$doc['user_name'] = $user['username'];	
			}
			if(!isset($doc['avatar'])){
				$doc['avatar'] = isset($user['avatar'])?$user['avatar']:"";	
			}
			if(!isset($doc['user_role_name'])){
				$doc['user_role_name'] = $user['role_name'];	
			}
			if(!isset($doc['visibility'])){
                            if(in_array($user['role_id'], $roles)){
                                $doc['visibility'] = 1;
                            }else if($user['role_id'] == $contractorRoleId){
                                $doc['visibility'] = 2;
                            }else if($user['role_id'] == $customerRoleId){
                                $doc['visibility'] = 3;
                            }
			}
		}
		if(!isset($doc['user_role'])){
			$doc['user_role'] = $user['role_id'];
		}
		
		if(!isset($doc['seen_by_ids'])){
			$doc['seen_by_ids'] = "";
		}
		if(!isset($doc['seen_by_names'])){
			$doc['seen_by_names'] = "";
		}
		if(!isset($doc['visited_extra_info_id'])){
			$doc['visited_extra_info_id'] = 0;
		}
		if(!isset($doc['discussion_id'])){
			$modelDiscussionSeq = new Model_DiscussionSeq();
			$newDiscussionId = $modelDiscussionSeq->updateBookingDiscussionSeq();
			$doc['discussion_id'] = $newDiscussionId;
		}else{
			$newDiscussionId = 1;
		}
		if(isset($doc['booking_id']) && getType($doc['booking_id']) == 'string'){
			$doc['booking_id'] = (int) $doc['booking_id'];
		}
		if(isset($doc['user_role']) && getType($doc['user_role']) == 'string'){
			$doc['user_role'] = (int) $doc['user_role'];
		}
		if(isset($doc['user_id']) && getType($doc['user_id']) == 'string'){
			$doc['user_id'] = (int) $doc['user_id'];
		}
		if(isset($doc['visited_extra_info_id']) && getType($doc['visited_extra_info_id']) == 'string'){
			$doc['visited_extra_info_id'] = (int) $doc['visited_extra_info_id'];
		}
		
		
		$success = $this->_collection->insert($doc);
		$modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($newDiscussionId, $this->_collectionName, 'added');
		//$newDocID = $doc['_id'];
		
		/*$data = array(
			'discussion_id' => $newDocID->{'$id'}
		);
		
		
		$successUpdate = $this->updateById($newDocID, $data);*/
		
		/*$modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'added');*/
		
		return $newDiscussionId; 
    }
	//by mohamed
    public function getByContractorId1($order = 'asc' , $filter = array()){

		$modelAuthRole = new Model_AuthRole();
		$loggedUser = CheckAuth::getLoggedUser();
		$contractorRoleId = $modelAuthRole->getRoleIdByName('contractor');
		$isContractor = false;
		if($loggedUser['role_id'] == $contractorRoleId){
			$isContractor = true;
		}
		if($order == 'asc'){
			$newOrder = 1;
		}else{
			$newOrder = -1;
		}
                
                $bookingModelObj = new Model_Booking();
                $filters = array('contractor_id' => $loggedUser['user_id']);
                $allBookingsOfContractor = $bookingModelObj->getAll($filters);
                $allBookingsIdsOfContractor = array();
                foreach ($allBookingsOfContractor as $allBookingOfContractor){
                    array_push($allBookingsIdsOfContractor, $allBookingOfContractor['booking_id']);
                }
				
                //return $allBookingsIdsOfContractor; 
		
               $ss = array();
               foreach($allBookingsIdsOfContractor as $allBookingsIdOfContractor){
                        $testValue = $this->getByBookingId($allBookingsIdOfContractor);
                        if(!empty($testValue)){
                            array_push($ss, $this->getByBookingId($allBookingsIdOfContractor));  
                        }
               }
               
               return $ss;
		

		
                                
    }
	public function getByContractorId($order = 'asc' , $filter = array()){

		
		$loggedUser = CheckAuth::getLoggedUser();
		
		
		if($order == 'asc'){
			$newOrder = 1;
		}else{
			$newOrder = -1;
		}
                
                $bookingModelObj = new Model_Booking();
                $filters = array('contractor_id' => 8);
                $allBookingsOfContractor = $bookingModelObj->getAll($filters);
                $allBookingIdsOfContractor = array();
                foreach ($allBookingsOfContractor as $booking){
                     $allBookingIdsOfContractor[] = (int)$booking['booking_id'];
                }
				//print_r($allBookingIdsOfContractor);
                //return $allBookingsIdsOfContractor; 
		
               
			   $recentTenCommentsForContractorBookings = $this->getByBookingIds($allBookingIdsOfContractor,'desc');
			   
              /* foreach($allBookingsIdsOfContractor as $allBookingsIdOfContractor){
                        $testValue = $this->getByBookingId($allBookingsIdOfContractor);
                        if(!empty($testValue)){
                            array_push($ss, $this->getByBookingId($allBookingsIdOfContractor));  
                        }
               }*/
               
               return $recentTenCommentsForContractorBookings;
		

		
                                
    }
	public function getByBookingIds($ids, $order = 'asc' , $filter = array()) {
	   // $id = (int) $id;
		
		$modelAuthRole = new Model_AuthRole();
		$loggedUser = CheckAuth::getLoggedUser();
		$contractorRoleId = $modelAuthRole->getRoleIdByName('contractor');
		$isContractor = false;
		/*if($loggedUser['role_id'] == $contractorRoleId){
			$isContractor = true;
		}*/
		if($order == 'asc'){
			$newOrder = 1;
		}else{
			$newOrder = -1;
		}
		
		$conditions = array();
		$orConditions = array();
		$discussionsAsArray = array();

		
		$conditions[] = array('booking_id' => array('$in' => $ids));
		 

		if($isContractor){
			$conditions[] =	array('visibility' => array('$ne' => 1));
		}
		
		if(isset($filter['user_id']) && $filter['user_id']){
		   $user_id = (int) $filter['user_id'];
		   $orConditions[] = array('user_id' => $user_id);	  
		}
		if(isset($filter['user_role']) && $filter['user_role'] == 'customer'){  
		   $orConditions[] = array('visibility' => 3);		
			$conditions[] = array('$or' => $orConditions);  
		}

		//print_r($conditions);
		/*if($this->_collection->find(array('$and' => $conditions))->sort(array('created' => $newOrder))->count() > 0){*/
			$discussions = $this->_collection->find(array('$and' => $conditions))->sort(array('created' => $newOrder))->limit(20);
			
			if($discussions){
                    foreach($discussions as $discussion){
                            $discussionsAsArray[] = $discussion;
                    }
                    //

            }else{
                    $discussionsAsArray = array();
            }
			
	
		return $discussionsAsArray;
	}
	
	
	public function getByBookingId($id, $order = 'asc' , $filter = array()) {
	    $id = (int) $id;
		$modelBooking = new Model_Booking();
		$booking = $modelBooking->getById($id);
		$modelAuthRole = new Model_AuthRole();
		$loggedUser = CheckAuth::getLoggedUser();
		$contractorRoleId = $modelAuthRole->getRoleIdByName('contractor');
		$isContractor = false;
		if($loggedUser['role_id'] == $contractorRoleId){
			$isContractor = true;
		}
		if($order == 'asc'){
			$newOrder = 1;
		}else{
			$newOrder = -1;
		}
		
		$conditions = array();
		$orConditions = array();

		$conditions[] = array('booking_id' => $id);
		if($isContractor){
			$conditions[] =	array('visibility' => array('$ne' => 1));
		}
		
		if(isset($filter['user_id']) && $filter['user_id']){
		   $user_id = (int) $filter['user_id'];
		   $orConditions[] = array('user_id' => $user_id);	  
		}
		if(isset($filter['user_role']) && $filter['user_role'] == 'customer'){  
		   $orConditions[] = array('visibility' => 3);		
			$conditions[] = array('$or' => $orConditions);  
		}
		
		if($this->_collection->find(array('$and' => $conditions))->sort(array('created' => $newOrder))->count() > 0){
			$discussions = $this->_collection->find(array('$and' => $conditions))->limit(10)->sort(array('created' => $newOrder));
		}else{
			$discussions = 0;  
		}
		
		/*if($isContractor){
			if($this->_collection->find(array('$and' => array(array('booking_id' => $id),array('visibility' => array('$ne' => 1)))))->sort(array('created' => $newOrder))->count() > 0){
				$discussions = $this->_collection->find(array('$and' => array(array('booking_id' => $id),array('visibility' => array('$ne' => 1)))))->limit(10)->sort(array('created' => $newOrder));
			}else{
				$discussions = 0;
			}
			
		}else{
			if($this->_collection->find(array('$and' => array(array('booking_id' => $id))))->sort(array('created' => $newOrder))->count() > 0){
				$discussions = $this->_collection->find(array('$and' => array(array('booking_id' => $id))))->limit(10)->sort(array('created' => $newOrder));
			}else{
				$discussions = 0;
			}
		}*/
		if($discussions){
			foreach($discussions as $discussion){
				$discussionsAsArray[] = $discussion;
			}
			//$discussionsAsArray = iterator_to_array($discussions);
		}else{
			$discussionsAsArray = array();
		}
		
		if($discussionsAsArray){
			$modelItemImageDiscussion = new Model_ItemImageDiscussion();
			$modelImage = new Model_Image();
			foreach($discussionsAsArray as &$discussion){
			    $discussion['created'] = isset($discussion['created'])? $discussion['created'] : $booking['created']; 
				$discussion['user_message'] = urldecode($discussion['user_message']);
				$itemImageDiscussion = $modelItemImageDiscussion->getByDiscussionIdAndType($discussion['discussion_id'], 'booking');
				if($itemImageDiscussion){
					$discussion['group_id'] = $itemImageDiscussion[0]['group_id'];
					if($itemImageDiscussion[0]['image_id'] != 0){
						$image = $modelImage->getByImageId($itemImageDiscussion[0]['image_id']);
						$discussion['image_id'] = $itemImageDiscussion[0]['image_id'];
						$discussion['thumbnail_path'] = $image['thumbnail_path'];
						$discussion['compressed_path'] = $image['compressed_path'];
                        $discussion['large_path'] = $image['large_path'];
						$discussion['role_id'] = $image['user_role'];
					}else{
						$discussion['image_id'] = NULL;
						$discussion['thumbnail_path'] = NULL;
						$discussion['compressed_path'] = NULL;
                        $discussion['large_path'] = NULL;
						$discussion['role_id'] = $discussion['user_role'];
					}
				}else{
					$discussion['group_id'] = NULL;
					$discussion['image_id'] = NULL;
					$discussion['thumbnail_path'] = NULL;
					$discussion['compressed_path'] = NULL;
                    $discussion['large_path'] = NULL;
					$discussion['role_id'] = $discussion['user_role'];
				}
			}
		}
		
		return $discussionsAsArray;
		
	}
	
	public function countDiscussions($id, $isContractor){
		$id = (int) $id;
		if($isContractor){
			$countDiscussions = $this->_collection->find(array('$and' => array(array('booking_id' => $id),array('visibility' => array('$ne' => 1)))))->count();
		}else{
			$countDiscussions = $this->_collection->find(array('$and' => array(array('booking_id' => $id))))->count();
		}
		return $countDiscussions;
	}
	
	/*public function countDiscussions($bookingInfo = array(), $inquiry_id, $complaintId, $isContractor){
		$countBookingDiscussions = $countEstimateDiscussions = $countInquiryDiscussions = $countinvoiceDiscussions = $countComplaintDiscussion = 0;
		$inquiry_id = (int) $inquiry_id;
		$complaintId = (int) $complaintId;
		$bookingId = (int) $bookingInfo['bookingId'];
		$estimateId = (int) $bookingInfo['estimateId'];
		$invoiceId = (int) $bookingInfo['invoiceId'];
		$inquiryId = (int) $bookingInfo['inquiryId'];
		if($isContractor){
			if($bookingId){
				$countBookingDiscussions = $this->_collection->find(array('$and' => array(array('type' => 'booking'),array('item_id' => $bookingId),array('visibility' => array('$ne' => 1)))))->count();
				$countEstimateDiscussions = $this->_collection->find(array('$and' => array(array('type' => 'estimate'),array('item_id' => $estimateId),array('visibility' => array('$ne' => 1)))))->count();
				$countInvoiceDiscussions = $this->_collection->find(array('$and' => array(array('type' => 'invoice'),array('item_id' => $invoiceId),array('visibility' => array('$ne' => 1)))))->count();
				$countInquiryDiscussions = $this->_collection->find(array('$and' => array(array('type' => 'inquiry'),array('item_id' => $inquiryId),array('visibility' => array('$ne' => 1)))))->count();
			}else if($inquiry_id){
				$countInquiryDiscussions = $this->_collection->find(array('$and' => array(array('type' => 'inquiry'),array('item_id' => $inquiry_id),array('visibility' => array('$ne' => 1)))))->count();
			}
			if($complaintId){
				$countComplaintDiscussion = $this->_collection->find(array('$and' => array(array('type' => 'complaint'),array('item_id' => $complaintId),array('visibility' => array('$ne' => 1)))))->count();
			}
			$countAll = $countBookingDiscussions + $countEstimateDiscussions + $countInvoiceDiscussions + $countInquiryDiscussions + $countComplaintDiscussion; 
		}else{
			if($bookingId){
				$countBookingDiscussions = $this->_collection->find(array('$and' => array(array('type' => 'booking'),array('item_id' => $bookingId))))->count();
				$countEstimateDiscussions = $this->_collection->find(array('$and' => array(array('type' => 'estimate'),array('item_id' => $estimateId))))->count();
				$countInvoiceDiscussions = $this->_collection->find(array('$and' => array(array('type' => 'invoice'),array('item_id' => $invoiceId))))->count();
				$countInquiryDiscussions = $this->_collection->find(array('$and' => array(array('type' => 'inquiry'),array('item_id' => $inquiryId))))->count();
			}else if($inquiry_id){
				$countInquiryDiscussions = $this->_collection->find(array('$and' => array(array('type' => 'inquiry'),array('item_id' => $inquiry_id))))->count();
			}
			if($complaintId){
				$countComplaintDiscussion = $this->_collection->find(array('$and' => array(array('type' => 'complaint'),array('item_id' => $complaintId))))->count();
			}
			$countAll = $countBookingDiscussions + $countEstimateDiscussions + $countInvoiceDiscussions + $countInquiryDiscussions + $countComplaintDiscussion; 
		}
		
		return $countAll;
		
	}*/
	
	
	
	public function getSpecificTypeDisByOtherUsers($item_id, $loggedUserId, $isContractor){
		$item_id = (int) $item_id;
		$loggedUserId = (int) $loggedUserId;
		
		if($isContractor){
			if($this->_collection->find(array('$and' => array(array('user_id' => array('$ne'=> $loggedUserId)),array('booking_id' => $item_id),array('visibility' => array('$ne' => 1)))))->sort(array('created' => -1))->count() > 0){
				$discussions = $this->_collection->find(array('$and' => array(array('user_id' => array('$ne'=> $loggedUserId)),array('booking_id' => $item_id),array('visibility' => array('$ne' => 1)))))->sort(array('created' => -1));
				
			}else{
				$discussions = 0;
			}
		}else{
			if($this->_collection->find(array('$and' => array(array('user_id' => array('$ne'=> $loggedUserId)),array('booking_id' => $item_id))))->sort(array('created' => -1))->count() > 0){
				$discussions = $this->_collection->find(array('$and' => array(array('user_id' => array('$ne'=> $loggedUserId)),array('booking_id' => $item_id))))->sort(array('created' => -1));
				
			}else{
				$discussions = 0;
			}
			
		}
		return $discussions;
	}
	
	
	/*public function getTabsDiscussionsByOtherUsers($bookingInfo = array(), $inquiry_id, $complaintId, $loggedUserId, $isContractor){
		$inquiry_id = (int) $inquiry_id;
		$complaintId = (int) $complaintId;
		$bookingId = (int) $bookingInfo['bookingId'];
		$estimateId = (int) $bookingInfo['estimateId'];
		$invoiceId = (int) $bookingInfo['invoiceId'];
		$inquiryId = (int) $bookingInfo['inquiryId'];
		$loggedUserId = (int) $loggedUserId;
		
		/*$bookingDiscussions = array();
        $invoiceDiscussions = array();
        $estimateDiscussions = array();
        $inquiryDiscussions = array();
        $complaintDiscussions = array();
		
		$bookingDiscussionsAsArray = array();
        $invoiceDiscussionsAsArray = array();
        $estimateDiscussionsAsArray = array();
        $inquiryDiscussionsAsArray = array();
        $complaintDiscussionsAsArray = array();
		
		if($isContractor){
			if($bookingId){
				if($this->_collection->find(array('$and' => array(array('type' => 'booking'),array('item_id' => $bookingId),array('visibility' => array('$ne' => 1)),array('user_id' => array('$ne'=> $loggedUserId)))))->count() > 0){
					$bookingDiscussions = $this->_collection->find(array('$and' => array(array('type' => 'booking'),array('item_id' => $bookingId),array('visibility' => array('$ne' => 1)),array('user_id' => array('$ne'=> $loggedUserId)))))->sort(array('created' => -1));
					foreach($bookingDiscussions as $dis)
						$bookingDiscussionsAsArray[] = $dis;
				}
				if($this->_collection->find(array('$and' => array(array('type' => 'estimate'),array('item_id' => $estimateId),array('visibility' => array('$ne' => 1)), array('user_id' => array('$ne'=> $loggedUserId)))))->count() > 0){
					$estimateDiscussions = $this->_collection->find(array('$and' => array(array('type' => 'estimate'),array('item_id' => $estimateId),array('visibility' => array('$ne' => 1)), array('user_id' => array('$ne'=> $loggedUserId)))))->sort(array('created' => -1));
					foreach($estimateDiscussions as $dis)
						$estimateDiscussionsAsArray[] = $dis;
				}
				if($this->_collection->find(array('$and' => array(array('type' => 'invoice'),array('item_id' => $invoiceId),array('visibility' => array('$ne' => 1)), array('user_id' => array('$ne'=> $loggedUserId)))))->count() > 0){
					$invoiceDiscussions = $this->_collection->find(array('$and' => array(array('type' => 'invoice'),array('item_id' => $invoiceId),array('visibility' => array('$ne' => 1)), array('user_id' => array('$ne'=> $loggedUserId)))))->sort(array('created' => -1));
					foreach($invoiceDiscussions as $dis)
						$invoiceDiscussionsAsArray[] = $dis;
				}
				if($this->_collection->find(array('$and' => array(array('type' => 'inquiry'),array('item_id' => $inquiryId),array('visibility' => array('$ne' => 1)), array('user_id' => array('$ne'=> $loggedUserId)))))->count() > 0){
					$inquiryDiscussions = $this->_collection->find(array('$and' => array(array('type' => 'inquiry'),array('item_id' => $inquiryId),array('visibility' => array('$ne' => 1)), array('user_id' => array('$ne'=> $loggedUserId)))))->sort(array('created' => -1));
					foreach($inquiryDiscussions as $dis)
						$inquiryDiscussionsAsArray[] = $dis;
				}
			}else if($inquiry_id){
				if($this->_collection->find(array('$and' => array(array('type' => 'inquiry'),array('item_id' => $inquiryId),array('visibility' => array('$ne' => 1)), array('user_id' => array('$ne'=> $loggedUserId)))))->count() > 0){
					$inquiryDiscussions = $this->_collection->find(array('$and' => array(array('type' => 'inquiry'),array('item_id' => $inquiryId),array('visibility' => array('$ne' => 1)), array('user_id' => array('$ne'=> $loggedUserId)))))->sort(array('created' => -1));
					foreach($inquiryDiscussions as $dis)
						$inquiryDiscussionsAsArray[] = $dis;
				}
			}
			if($complaintId){
				if($this->_collection->find(array('$and' => array(array('type' => 'complaint'),array('item_id' => $complaintId),array('visibility' => array('$ne' => 1)), array('user_id' => array('$ne'=> $loggedUserId)))))->count() > 0){
					$complaintDiscussions = $this->_collection->find(array('$and' => array(array('type' => 'complaint'),array('item_id' => $complaintId),array('visibility' => array('$ne' => 1)), array('user_id' => array('$ne'=> $loggedUserId)))))->sort(array('created' => -1));
					foreach($complaintDiscussions as $dis)
						$complaintDiscussionsAsArray[] = $dis;
				}
				
			}
			$discussions = array_merge($bookingDiscussionsAsArray, $invoiceDiscussionsAsArray, $estimateDiscussionsAsArray, $inquiryDiscussionsAsArray, $complaintDiscussionsAsArray);
		}else{
			if($bookingId){
				if($this->_collection->find(array('$and' => array(array('type' => 'booking'),array('item_id' => $bookingId), array('user_id' => array('$ne'=> $loggedUserId)))))->count() > 0){
					$bookingDiscussions = $this->_collection->find(array('$and' => array(array('type' => 'booking'),array('item_id' => $bookingId), array('user_id' => array('$ne'=> $loggedUserId)))))->sort(array('created' => -1));
					foreach($bookingDiscussions as $dis)
						$bookingDiscussionsAsArray[] = $dis;
				}
				if($this->_collection->find(array('$and' => array(array('type' => 'estimate'),array('item_id' => $estimateId), array('user_id' => array('$ne'=> $loggedUserId)))))->count() > 0){
					$estimateDiscussions = $this->_collection->find(array('$and' => array(array('type' => 'estimate'),array('item_id' => $estimateId), array('user_id' => array('$ne'=> $loggedUserId)))))->sort(array('created' => -1));
					foreach($estimateDiscussions as $dis)
						$estimateDiscussionsAsArray[] = $dis;
				}
				if($this->_collection->find(array('$and' => array(array('type' => 'invoice'),array('item_id' => $invoiceId), array('user_id' => array('$ne'=> $loggedUserId)))))->count() > 0){
					$invoiceDiscussions = $this->_collection->find(array('$and' => array(array('type' => 'invoice'),array('item_id' => $invoiceId), array('user_id' => array('$ne'=> $loggedUserId)))))->sort(array('created' => -1));
					foreach($invoiceDiscussions as $dis)
						$invoiceDiscussionsAsArray[] = $dis;
				}
				if($this->_collection->find(array('$and' => array(array('type' => 'inquiry'),array('item_id' => $inquiryId), array('user_id' => array('$ne'=> $loggedUserId)))))->count() > 0){
					$inquiryDiscussions = $this->_collection->find(array('$and' => array(array('type' => 'inquiry'),array('item_id' => $inquiryId), array('user_id' => array('$ne'=> $loggedUserId)))))->sort(array('created' => -1));
					foreach($inquiryDiscussions as $dis)
						$inquiryDiscussionsAsArray[] = $dis;
				}
			}else if($inquiry_id){
				if($this->_collection->find(array('$and' => array(array('type' => 'inquiry'),array('item_id' => $inquiryId), array('user_id' => array('$ne'=> $loggedUserId)))))->count() > 0){
					$inquiryDiscussions = $this->_collection->find(array('$and' => array(array('type' => 'inquiry'),array('item_id' => $inquiryId), array('user_id' => array('$ne'=> $loggedUserId)))))->sort(array('created' => -1));
					foreach($inquiryDiscussions as $dis)
						$inquiryDiscussionsAsArray[] = $dis;
				}
			}
			if($complaintId){
				if($this->_collection->find(array('$and' => array(array('type' => 'complaint'),array('item_id' => $complaintId), array('user_id' => array('$ne'=> $loggedUserId)))))->count() > 0){
					$complaintDiscussions = $this->_collection->find(array('$and' => array(array('type' => 'complaint'),array('item_id' => $complaintId), array('user_id' => array('$ne'=> $loggedUserId)))))->sort(array('created' => -1));
					foreach($complaintDiscussions as $dis)
						$complaintDiscussionsAsArray[] = $dis;
				}
				
			}
			$discussions = array_merge($bookingDiscussionsAsArray, $invoiceDiscussionsAsArray, $estimateDiscussionsAsArray, $inquiryDiscussionsAsArray, $complaintDiscussionsAsArray);
		}
		
		return $discussions;
		
	}*/
	
	public function changeCommentSeenFlagById($id, $loggedUserId, $loggedUserName) {
        //$id = (int) $id;
		$loggedUserId = (int) $loggedUserId;
		$discussion = $this->getByMongoId($id);
		
		$newId = "";
		$newName = "";
        $seenId = array("");
		$seenNames = array("");
		$result1 = 0;
		$result2 = 0;
		
		
		foreach ($discussion as $key => $value) {
            $seenId = explode(',', $value['seen_by_ids']);
			$seenNames = explode(',', $value['seen_by_names']);
			
            if (!in_array($loggedUserId, $seenId)) {
                if (!empty($value['seen_by_ids'])) {
					$newId = $value['seen_by_ids'] . "," . $loggedUserId;
					$newName = $value['seen_by_names'] . "," . $loggedUserName;
					//echo "by walaa f";print_r($newId);exit;
                } else {
                    $newId = (string) $loggedUserId;
					
					$newName = $loggedUserName;
					//echo "by walaa n";print_r($newId);exit;
                }
               
				
				$result1 = $this->_collection->update(
					array('_id' => new MongoId($key)), array('$set' =>array('seen_by_ids' => $newId)), array('multiple' => true));
				$result2 = $this->_collection->update(
					array('_id' => new MongoId($key)), array('$set' =>array('seen_by_names' => $newName)), array('multiple' => true));
				
            }
        }
			
		if($result1 && $result2)
			return 1;
		return 0;
    }
	
	public function updateDiscImages($id, $images){
		$id = (int) $id;
		
		$this->_collection->update(
					array('discussion_id' => $id), array('$set' =>array('images' => $images)));
		//$this->_collection->update(
					//array('_id' => new MongoId($id)), array('$set' =>array('images' => $images)));
					return 1;
	}
	
	public function getByMongoId($id) {
        //$id = (int) $id;
		$discussion = $this->_collection->find(array('_id' => new MongoId($id)));
		return $discussion;
    }
	
	public function getById($id) {
        $id = (int) $id;
		//$id = new MongoInt64($id);
		$discussion = $this->_collection->find(array('discussion_id' => $id));
		
		if($discussion){
			$discussionsAsArray = iterator_to_array($discussion);
		}else{
			$discussionsAsArray = 0;
		}
		if($discussionsAsArray){
			$discussionsAfterProcessing = array();
			foreach($discussionsAsArray as $k => $v){
				$discussionsAfterProcessing['discussion_id'] = $v['discussion_id'];
				$discussionsAfterProcessing['booking_id'] = $v['booking_id'];
				$discussionsAfterProcessing['user_id'] = $v['user_id'];
				$discussionsAfterProcessing['user_name'] = $v['user_name'];
				$discussionsAfterProcessing['avatar'] = $v['avatar'];
				$discussionsAfterProcessing['user_message'] = urldecode($v['user_message']);
				$discussionsAfterProcessing['created'] = $v['created'];
				$discussionsAfterProcessing['user_role_name'] = $v['user_role_name'];
				$discussionsAfterProcessing['user_role'] = $v['user_role'];
				$discussionsAfterProcessing['visited_extra_info_id'] = $v['visited_extra_info_id'];
				$discussionsAfterProcessing['visibility'] = $v['visibility'];
				
			}
		}else{
			$discussionsAfterProcessing = 0;
		}
		
		
		
		return $discussionsAfterProcessing;
    }
	
	public function markAllAsSeen($item_id , $loggedUserName, $loggedUserId, $isContractor){
		$loggedUserId = (int) $loggedUserId;
		$item_id = (int) $item_id;
		
		
		$discussions = $this->getSpecificTypeDisByOtherUsers($item_id, $loggedUserId, $isContractor);
		
		/*if($discussions){
		foreach($discussions as $k => $v){
			foreach($v['_id'] as $x)
				$v['_id'] = $x;
					print_r($v);
					echo " break ";
					print_r($k);
				}
		}echo "test";exit;*/
		
		$newId = "";
		$newName = "";
        $seenIds = array("");
		$seenNames = array("");
		$result1 = array();
		$result2 = array();
		if($discussions){
		foreach ($discussions as $key => $value) {
            $seenIds = explode(',', $value['seen_by_ids']);
		
			$seenNames = explode(',', $value['seen_by_names']);
			
            if (!in_array($loggedUserId, $seenIds)) {
                if (!empty($value['seen_by_ids'])) {
					$newId = $value['seen_by_ids'] . "," . $loggedUserId;
					$newName = $value['seen_by_names'] . "," . $loggedUserName;
					
                } else {
                    $newId = (string) $loggedUserId;
					
					$newName = $loggedUserName;
				
                }
				
				$result1[$key] = $this->_collection->update(
					array('_id' => new MongoId($key)), array('$set' =>array('seen_by_ids' => $newId)), array('multiple' => true));
				$result2[$key] = $this->_collection->update(
					array('_id' => new MongoId($key)), array('$set' =>array('seen_by_names' => $newName)), array('multiple' => true));
					
				
            }
        }
		}
		if(!empty($result1) && !in_array(0,$result1) && !empty($result2) && !in_array(0,$result2)){
			return 1;
		}
        return 0;
	}
	
	public function deleteById($id) {
		$id = (int) $id;
		$result = $this->_collection->remove(array('discussion_id' => $id), array('w' => true));
		
		return $result['n'];
		
        //return $this->_collection->remove(array('discussion_id' => $id));
    }
	
	public function deleteByMongoId($id) {
		//$id = (int) $id;
        $result = $this->_collection->remove(array('_id' => new MongoId($id)), array('w' => true));
		return $result['n'];
    }

	public function updateById($id, $data){
		$id = (int) $id;
		if(isset($data['user_id']) && (!isset($data['user_name']) || !isset($data['avatar']) || !isset($data['user_role_name'] ) || !isset($data['user_role'] ))){
			//echo "yes by wlaa"; 
			$modelUser = new Model_User();
			$modelContractorInfo = new Model_ContractorInfo();
			$modelAuthRole = new Model_AuthRole();
			$contractorRoleId = $modelAuthRole->getRoleIdByName('contractor');
			$user = $modelUser->getById($data['user_id']);
			if ($user['role_name'] == 'contractor') {
				$contractorInfo = $modelContractorInfo->getByContractorId($user['user_id']);
				$user['username'] = ucwords($user['username']) . ' - ' . ucwords($contractorInfo['business_name']);
			} else {
				$user['username'] = ucwords($user['username']);
			}
			
			if(!isset($data['user_name'])){
				$data['user_name'] = $user['username'];	
			}
			if(!isset($data['avatar'])){
				$data['avatar'] = $user['avatar'];	
			}
			if(!isset($data['user_role_name'])){
				$data['user_role_name'] = $user['role_name'];	
			}
			if(!isset($data['user_role'])){
				$data['user_role'] = $user['role_id'];	
			}
		}
		
		$modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_collectionName, 'changed');
		return $this->_collection->update(
						array('discussion_id' => $id), array('$set' => $data));
			
	}
	
	public function updateByExtraInfoId($id , $data){
		$id = (int) $id;
		$this->_collection->update(
						array('visited_extra_info_id' => $id), array('$set' => $data));
		return 1;
	}
	
	public function getByExtraInfoId($id) {
        $id = (int) $id;
		if($id){
			$discussion = $this->_collection->find(array('visited_extra_info_id' => $id));
		}else{
			$discussion = 0;
		}
		if($discussion){
			$discussionsAsArray = iterator_to_array($discussion);
		}else{
			$discussionsAsArray = 0;
		}
		
		if($discussionsAsArray){
			$discussionsAfterProcessing = array();
			foreach($discussionsAsArray as $k => $v){
				$discussionsAfterProcessing['discussion_id'] = $v['discussion_id'];
				$discussionsAfterProcessing['booking_id'] = $v['booking_id'];
				$discussionsAfterProcessing['user_id'] = $v['user_id'];
				$discussionsAfterProcessing['user_name'] = $v['user_name'];
				$discussionsAfterProcessing['avatar'] = $v['avatar'];
				$discussionsAfterProcessing['user_message'] = urldecode($v['user_message']);
				$discussionsAfterProcessing['created'] = isset($v['created']) ? $v['created'] : 0 ;
				$discussionsAfterProcessing['user_role_name'] = $v['user_role_name'];
				$discussionsAfterProcessing['user_role'] = $v['user_role'];
				$discussionsAfterProcessing['visited_extra_info_id'] = $v['visited_extra_info_id'];
				$discussionsAfterProcessing['visibility'] = $v['visibility'];
				
			}
		}else{
			$discussionsAfterProcessing = 0;
		}
		
		
		
		return $discussionsAfterProcessing;
	}
	
	public function getLastBookingDiscussionByBookingId($bookingId){
	    $bookingId = (int) $bookingId;
	    if($this->_collection->find(array('booking_id' => $bookingId))->sort(array('created' => -1))->count() > 0 ){
			$discussions = $this->_collection->find(array('booking_id' => $bookingId))->sort(array('created' => -1))->limit(1);
	    }else{
			$discussions = 0;
		}
	    if($discussions){
			$discussionsAsArray = iterator_to_array($discussions);
		}else{
			$discussionsAsArray = 0;
		}
		
		if($discussionsAsArray){
			$discussionsAfterProcessing = array();
			foreach($discussionsAsArray as $k => $v){
				$discussionsAfterProcessing['discussion_id'] = $v['discussion_id'];
				$discussionsAfterProcessing['booking_id'] = $v['booking_id'];
				$discussionsAfterProcessing['user_id'] = $v['user_id'];
				$discussionsAfterProcessing['user_name'] = $v['user_name'];
				$discussionsAfterProcessing['avatar'] = $v['avatar'];
				$discussionsAfterProcessing['user_message'] = urldecode($v['user_message']);
				$discussionsAfterProcessing['created'] = $v['created'];
				$discussionsAfterProcessing['user_role_name'] = $v['user_role_name'];
				$discussionsAfterProcessing['user_role'] = $v['user_role'];
				$discussionsAfterProcessing['visited_extra_info_id'] = $v['visited_extra_info_id'];
				$discussionsAfterProcessing['visibility'] = $v['visibility'];
				
			}
		}else{
			$discussionsAfterProcessing = 0;
		}
		
		
		
		return $discussionsAfterProcessing;
	}
	
	public function deleteByExtraInfoId($id){
	    $id = (int) $id;
		 $result = $this->_collection->remove(array('visited_extra_info_id' => $id), array('w' => true)); 
		 return $result['n'];
	}
	
	public function getNotesByVisitExtraInfoId($id){
        $id = (int) $id;
		$discussion = $this->_collection->find(array('visited_extra_info_id' => $id));
		
		if($discussion){
			$discussionsAsArray = iterator_to_array($discussion);
		}else{
			$discussionsAsArray = 0;
		}
		
		if($discussionsAsArray){
			$discussionsAfterProcessing = array();
			foreach($discussionsAsArray as $k => $v){
				$discussionsAfterProcessing['discussion_id'] = $v['discussion_id'];
				$discussionsAfterProcessing['booking_id'] = $v['booking_id'];
				$discussionsAfterProcessing['user_id'] = $v['user_id'];
				$discussionsAfterProcessing['user_name'] = $v['user_name'];
				$discussionsAfterProcessing['avatar'] = $v['avatar'];
				$discussionsAfterProcessing['user_message'] = urldecode($v['user_message']);
				$discussionsAfterProcessing['created'] = isset($v['created']) ? $v['created'] : '';
				//$discussionsAfterProcessing['created'] = $v['created']; 
				$discussionsAfterProcessing['user_role_name'] = $v['user_role_name'];
				$discussionsAfterProcessing['user_role'] = $v['user_role'];
				$discussionsAfterProcessing['visited_extra_info_id'] = $v['visited_extra_info_id'];
				$discussionsAfterProcessing['visibility'] = $v['visibility'];
				
			}
		}else{
			$discussionsAfterProcessing = 0;
		}
		
		
		
		return $discussionsAfterProcessing;
		//return $discussionsAsArray;
	}
	
	public function getByImageId($id,$order = 'asc', $group_id = 0 , $filters = array()) {
            $modelAuthRole = new Model_AuthRole();
            $loggedUser = CheckAuth::getLoggedUser();
            $contractorRoleId = $modelAuthRole->getRoleIdByName('contractor');
            $isContractor = false;
            if($loggedUser['role_id'] == $contractorRoleId){
                    $isContractor = true;
            }

            $modelItemImageDiscussion = new Model_ItemImageDiscussion();
            $filters['type'] = 'booking';

            if($group_id){
                    $filters['discussion_group_id'] = $group_id;
            }else{
                    $filters['image_id'] = $id;
            }
            $itemImageDiscussions = $modelItemImageDiscussion->getAll($filters);



            $discussion_ids = array();
            foreach($itemImageDiscussions as $itemImageDiscussion){
                    $discussion_ids[] = $itemImageDiscussion['item_id'];
            }


            $conditions = array();
            $orConditions = array();
		
            $conditions[] =	array('discussion_id' => array('$in' => $discussion_ids));
            if($isContractor){
		$conditions[] =	array('visibility' => array('$ne' => 1));
            }

            if(isset($filters)){		  
                if(isset($filter['user_id']) && $filter['user_id']){
                    $user_id = (int) $filter['user_id'];
                    $orConditions[] = array('user_id' => $user_id);	  
                }
                if(isset($filter['user_role']) && $filter['user_role'] == 'customer'){  
                    $orConditions[] = array('visibility' => 3);		
                    $conditions[] = array('$or' => $orConditions);  
                }

            }

            if($order == 'asc'){
                    $newOrder = 1;
            }else{
                    $newOrder = -1;
            }
            if($this->_collection->find(array('$and' => $conditions))->sort(array('created' => $newOrder))->count() > 0){
                    $discussions = $this->_collection->find(array('$and' => $conditions))->limit(10)->sort(array('created' => $newOrder));
            }else{
                    $discussions = 0;
            }
            if($discussions){
                    foreach($discussions as $discussion){
                            $discussionsAsArray[] = $discussion;
                    }
                    //$discussionsAsArray = iterator_to_array($discussions);

            }else{
                    $discussionsAsArray = array();
            }
            if($discussionsAsArray){
                    $modelItemImageDiscussion = new Model_ItemImageDiscussion();
                    $modelImage = new Model_Image();
                    foreach($discussionsAsArray as &$discussion){
                            $discussion['user_message'] = urldecode($discussion['user_message']);
                            $itemImageDiscussion = $modelItemImageDiscussion->getByDiscussionIdAndType($discussion['discussion_id'], 'booking');
                            if($itemImageDiscussion){
                                    $discussion['group_id'] = $itemImageDiscussion[0]['group_id'];
                            }else{
                                    $discussion['group_id'] = 0;
                            }
                    }
            }
            return $discussionsAsArray;
		/*$id = (int) $id;
		$findImageQuery = array('images.image_id' => $id);
		if(isset($filters)){		  
			if(isset($filters['user_id'])){
			    $user_id = (int) $filters['user_id'];
				$findImageQuery = array('images.image_id' => $id, 'user_id' => $user_id);		
			}else{
				$findImageQuery = array('images.image_id' => $id);
			}
		}
		
		if($order == 'asc'){
			$newOrder = 1;
		}else{
			$newOrder = -1;
		}
		
		$discussion = $this->_collection->find($findImageQuery)->sort(array('created' => $newOrder));
		if($discussion){
			$discussionsAsArray = iterator_to_array($discussion);
		}else{
			$discussionsAsArray = 0;
		}
		
		return $discussionsAsArray;*/
	}
	
	public function getRecentDiscussion($filters = array()) {
		$loggedUser = CheckAuth::getLoggedUser();
        $companyId = CheckAuth::getCompanySession();
        $userId = $loggedUser['user_id'];
		$requestNumber = 0;
		$recordsPerRequest = 20;
		$count = 0;
		$isLastRequest = 0;
		$result = array();
		$modelBooking = new Model_Booking();
		$modelContractorServiceBooking = new Model_ContractorServiceBooking();
		
		while($count != 20 && $isLastRequest == 0){
			$skip = $requestNumber * $recordsPerRequest;
			$discussions = $this->_collection->find()->skip($skip)->limit($recordsPerRequest)->sort(array('created' => -1));
			$requestNumber++;
			
			if($discussions){
				$discussionsAsArray = iterator_to_array($discussions);
				
			}else{
				$discussionsAsArray = 0;
				$isLastRequest = 1;
				return $result;
			}
			
			if($discussionsAsArray){
				foreach($discussionsAsArray as &$discussion){
					$discussion['user_message'] = urldecode($discussion['user_message']);
					$booking = $modelBooking->getById($discussion['booking_id']);
					$discussion['company_id'] = $booking['company_id'];
					
					$filters['contractor_id'] = $userId;
					$filters['booking_id'] = $discussion['booking_id'];
					$csb = $modelContractorServiceBooking->getAll($filters);
					$contractor_id = array();
					foreach($csb as $value){
						$contractor_id[] = $value['contractor_id'];
					}
					$discussion['contractor_id'] = $contractor_id;
					
					
				}
				
				
				foreach($discussionsAsArray as $discussion){
					if($discussion['company_id'] == $companyId){
						if (!CheckAuth::checkCredential(array('canSeeAllBookingDiscussion'))) {
							if (CheckAuth::checkCredential(array('canSeeOnlyHisBookingDiscussion'))) {
								if (CheckAuth::checkCredential(array('canSeeAssignedBookingDiscussion'))) {
									//$select->where("bd.user_id = {$userId} OR csb.contractor_id = {$userId}");
									if($discussion['user_id'] == $userId || in_array($userId, $discussion['contractor_id'])){
										$result[] = $discussion;
										$count++;
									}
								} else {
									//$select->where("bd.user_id = {$userId}");
									if($discussion['user_id'] == $userId){
										$result[] = $discussion;
										$count++;
									}	
								}
							}
						}else{
							$result[] = $discussion;
							$count++;
						}
					}
					if($count == 20){
						return $result;
					}
				}				
			}
		} 
		return $result;
		
		/*var_dump($discussionsAsArray);exit;
		
		$modelBooking = new Model_Booking();
		$myFilters = array();
		$myFilters['company_id'] = $companyId;
		$conditions = array();
		if ($filters) {
            if (!empty($filters['created_by'])) {
                $myFilters['created_by'] = (int) $filters['created_by'];
            }
        }

		$bookings = $modelBooking->getAll($myFilters);
		$booking_ids_from_booking = array();
		foreach($bookings as $booking){
			$booking_ids_from_booking[] = $booking['booking_id'];
		}
		
		$conditions[] =	array('booking_id' => array('$in' => $booking_ids_from_booking));
		
		if (!CheckAuth::checkCredential(array('canSeeAllBookingDiscussion'))) {
            if (CheckAuth::checkCredential(array('canSeeOnlyHisBookingDiscussion'))) {
                if (CheckAuth::checkCredential(array('canSeeAssignedBookingDiscussion'))) {
                    //$select->where("bd.user_id = {$userId} OR csb.contractor_id = {$userId}");
					$CSBfilters = array();
					$modelContractorServiceBooking = new Model_ContractorServiceBooking();
					$CSBfilters['contractor_id'] = $userId;
					$contractorServiceBooking = $modelContractorServiceBooking->getAll($CSBfilters);
					$booking_ids = array();
					foreach($contractorServiceBooking as $csb){
						$booking_ids[] = $csb['booking_id'];
					}
					
					$conditions[] = array('$or' => array('user_id' => $userId, array('booking_id' => array('$in' => $booking_ids))));
                } else {
                    //$select->where("bd.user_id = {$userId}");
					$conditions[] = array('user_id' => $userId);
                }
            }
        }
		
		
		if($this->_collection->find(array('$and' => $conditions))->sort(array('created' => -1))->count() > 0){
			$discussions = $this->_collection->find(array('$and' => $conditions))->limit(20)->sort(array('created' => -1));
		}else{
			$discussions = 0;
		}
		if($discussions){
			foreach($discussions as $discussion){
				$discussionsAsArray[] = $discussion;
			}
			//$discussionsAsArray = iterator_to_array($discussions);
		}else{
			$discussionsAsArray = 0;
		}
		
		if($discussionsAsArray){
			foreach($discussionsAsArray as &$discussion){
				$discussion['contractor_id'] = $userId;
			}
		}
		return $discussionsAsArray;*/
	}
	
	public function deleteByImageId($id) {
        $id = (int) $id;
		//not used
    }
}
