<?php

class Model_CalendarClientAccount {
		protected $_client;
		protected $_cal;
		protected $_clientId;
        protected $_redirectUri;
		public function __construct($userId , $userEmail ='') {
            
            $company_id = CheckAuth::getCompanySession();
            $api_name = 'google-api-php-client';

            $model_api_parameters = new Model_ApiParameters();

            $parameter_name = 'application name';
            $parameter = $model_api_parameters->getParameterValue($company_id, $api_name, $parameter_name);
            $application_name = $parameter['parameter_value'];
        
            $parameter_name = 'client secret path';
            $parameter = $model_api_parameters->getParameterValue($company_id, $api_name, $parameter_name);
            $client_secret_path = $parameter['parameter_value'];

          if (!defined('APPLICATION_NAME')){	
             define('APPLICATION_NAME', $application_name);
          }
          if (!defined('CLIENT_SECRET_PATH')){	
            define('CLIENT_SECRET_PATH',APPLICATION_PATH .'/'.$client_secret_path);
          }
            $router = Zend_Controller_Front::getInstance()->getRouter();
            $this->_redirectUri = $router->assemble(array(), 'getAuthKey');
            
			$modelUserGmailAccounts = new Model_UserGmailAccounts();

			// get the gmail account contractor
			if(isset($userId) ){
			   $userAccount = $modelUserGmailAccounts->getFirstEmailByContractorId($userId);
			   $email = isset($userAccount['user_gmail_account']) ? $userAccount['user_gmail_account'] : '';
			}
            if(isset($userEmail) && !empty($userEmail)){
			    $email = $userEmail;
			}
			///to stop process if the user has no gmail account
			if(empty($userAccount)){
				$this->_cal = null;
				$this->_client = null;
				$this->_clientId =null;
				return null;
				
			}
             $client = $this->getClient($userAccount);
             $cal = new Google_Service_Calendar($client);
			 
			////put the client and cal in static variable
			$this->_client = $client;
			$this->_cal = $cal;
			$this->_clientId = $email;
		}
		
		public function __get($property) {
			if (property_exists($this, $property)) {
			  return $this->$property;
			}
		}

		public function __set($property, $value) {
			if (property_exists($this, $property)) {
			  $this->$property = $value;
			}
			return $this;
		}
		
		public function getAllEvents($calendarId,$optParams = array()){
            /*
			$optParams = array(
				  'maxResults' => 300,
				  'orderBy' => 'startTime',
				  'singleEvents' => TRUE,
				  'timeMin' => date('c'),
				);
			*/
			$result= $this->_cal->events->listEvents($this->_clientId, $optParams);		
			
			return $result;		
			
		}

		public function insertEvent($event){
		
		  
		  if(!is_null($this->_cal)){
			$createdEvent = $this->_cal->events->insert($this->_clientId, $event);
			return $createdEvent->getId();
		  }
			
		  return false;	
		}
		
		public function instances($eventId,$optParam = array()){
			$result= $this->_cal->events->instances($this->_clientId, $eventId,$optParam);					
			return $result;		
		}
		
		
		public function getEvent($event_id){
			try {
				$eventOld = $this->_cal->events->get($this->_clientId, $event_id);
				
				return $eventOld;
				
				}
 			catch (Exception $e) {
				return array();
			}
			
		}
		
		public function updateEvent($event_id, $new_event){
			
		$eventOld = $this->_cal->events->get($this->_clientId, $event_id);
			
			$seq = $eventOld['sequence'];
			$new_event->setSequence($seq);
			if ($eventOld) {				
				$updatedEvent = $this->_cal->events->update($this->_clientId, $event_id, $new_event);
				return $updatedEvent;
			} else {
				return null;
			}	
		}
		
		public function deleteEvent($event_id){
			
			
			$eventOld = $this->getEvent($event_id);
			
			
			if (!empty($eventOld)) {
				if($eventOld['status'] !='cancelled'){
					$deletedEvent = $this->_cal->events->delete($this->_clientId, $event_id);
					return $deletedEvent;
				}
				
			} else {
				return null;
			}	
				
		}
	
		public function getClient($userAccount) {
			  $client = new Google_Client();
			  $client->setApplicationName(APPLICATION_NAME);
			  $client->setScopes(Google_Service_Calendar::CALENDAR);
			  $client->setAuthConfigFile(CLIENT_SECRET_PATH);
			  $client->setAccessType('offline');
			  $client->setApprovalPrompt('force');
               
			  //$guzzleClient = new \GuzzleHttp\Client(array( 'curl' => array( CURLOPT_SSL_VERIFYPEER => false, ), ));
			  //$client->setHttpClient($guzzleClient);
            
            $request = new Google_Http_Request('https://www.googleapis.com/auth/calendar');
            $client->getAuth()->sign($request);
               
			   // get the stored token
			  if ($userAccount['gmail_token']) {
				//$accessToken = json_decode($userAccount['gmail_token'], true);
                  $accessToken = $userAccount['gmail_token'];
                  
			  } else {
				  return ;
			  }
			  $client->setAccessToken($accessToken);

			  // Refresh the token if it's expired.
			  if ($client->isAccessTokenExpired()) {
				$refreshToken = $client->getRefreshToken();
				$client->refreshToken($refreshToken);
				$newAccessToken = $client->getAccessToken();
                  
				//$newAccessToken['refresh_token'] = $refreshToken;

				$modelUserGmailAccounts = new Model_UserGmailAccounts();
                  
                //$newAccessToken =  json_decode($newAccessToken);
                  
				$data = array('gmail_token'=>$newAccessToken);
                  //print_r($data);
				
				$modelUserGmailAccounts->updateById($userAccount['account_id'],$data);
	
			  }
			  return $client;
			}
			
		public function getAuthUrl($account_id,$email)
			{
				//return CLIENT_SECRET_PATH;
				  $client = new Google_Client();
				  $client->setApplicationName(APPLICATION_NAME);
				  $client->setScopes(Google_Service_Calendar::CALENDAR);
				  $client->setAuthConfigFile(CLIENT_SECRET_PATH);
				  $client->setRedirectUri($this->_redirectUri);
				  $client->setAccessType('offline');
				  $client->setApprovalPrompt('force');
				  
				  $data = array('account_id' => $account_id, 'email' => $email);
				  $data = json_encode($data);
				  $data = base64UrlEncode($data);
				  
				  $client->setState($data);

				  //$guzzleClient = new \GuzzleHttp\Client(array( 'curl' => array( CURLOPT_SSL_VERIFYPEER => false, ), ));
				  //$client->setHttpClient($guzzleClient);
            
            $request = new Google_Http_Request('https://www.googleapis.com/auth/calendar');
            $client->getAuth()->sign($request);
				  
				  $authUrl = $client->createAuthUrl();
				 
				  
				  return $authUrl;
			}
			
			public function createNewAccessToken($gmail_auth_key)
			{
				
				  $client = new Google_Client();
				  $client->setApplicationName(APPLICATION_NAME);
				  $client->setScopes(Google_Service_Calendar::CALENDAR);
				  $client->setAuthConfigFile(CLIENT_SECRET_PATH);
				  $client->setRedirectUri($this->_redirectUri);
				  $client->setAccessType('offline');
				  $client->setApprovalPrompt('force');

				  //$guzzleClient = new \GuzzleHttp\Client(array( 'curl' => array( CURLOPT_SSL_VERIFYPEER => false, ), ));
				  //$client->setHttpClient($guzzleClient);
                
                $request = new Google_Http_Request('https://www.googleapis.com/auth/calendar');
                $client->getAuth()->sign($request);
				  
				  //$accessToken = $client->fetchAccessTokenWithAuthCode($gmail_auth_key);
                $accessToken = $client->authenticate($gmail_auth_key);

				return $accessToken;

			}
	 
}

?>