<?php

class Model_ContractorVehicle extends Zend_Db_Table_Abstract {

    protected $_name = 'vehicle';

    /**
     *get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from(array('cv' => $this->_name));
        $select->joinInner(array('ci' => 'contractor_info'), 'ci.contractor_info_id=cv.contractor_info_id', array('ci.contractor_id'));
        $select->joinInner(array('u' => 'user'), 'ci.contractor_id=u.user_id', array('u.username'));
        $select->order($order);

        if ($filters) {
            if (!empty($filters['contractor_info_id'])) {
                $select->where("cv.contractor_info_id = {$filters['contractor_info_id']}");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

   /**
    * update table row according to the assigned id
    * 
    * @param int $id
    * @param array $data
    * @return boolean
    */ 
	
    public function updateById($id, $data) {
        $id = (int) $id;
        //return parent::update($data, "vehicle_id = '{$id}'");
        
        $modelUser = new Model_User();
        $contractorVehicle = $this->getById($id);

        $modelContractorInfo = new Model_ContractorInfo();
        $contractorInfo = $modelContractorInfo->getById($contractorVehicle['contractor_info_id']);
        $ret_val = parent::update($data, "vehicle_id = '{$id}'");
        if ($ret_val) {
            $modelUser->calculateProfileCompleteness($contractorInfo['contractor_id']);
        }
        return $ret_val;
    }

    /**
     *delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean 
     */
	 
    public function deleteById($id) {
        $id = (int) $id;
        //return parent::delete("vehicle_id = '{$id}'");
          $modelUser = new Model_User();
        $contractorVehicle = $this->getById($id);

        $modelContractorInfo = new Model_ContractorInfo();
        $contractorInfo = $modelContractorInfo->getById($contractorVehicle['contractor_info_id']);
        $ret_val = parent::delete("vehicle_id = '{$id}'");
        if ($ret_val) {
            $modelUser->calculateProfileCompleteness($contractorInfo['contractor_id']);
        }
        return $ret_val;
    }
	
	public function deleteByContractorInfoId($contractor_info_id){
	 
	 return parent::delete("contractor_info_id = '{$contractor_info_id}'");
	 
	}
	

    /**
     *get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("vehicle_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     *get table row according to the assigned ContractorInfoId
     * 
     * @param int $id
     * @return array 
     */
    public function getByContractorInfoId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("contractor_info_id = '{$id}'");

        return $this->getAdapter()->fetchAll($select);
    }
    
}