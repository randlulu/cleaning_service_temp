<?php

class Model_InquiryLabel extends Zend_Db_Table_Abstract {

    protected $_name = 'inquiry_label';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order($order);




        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }


        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table rows according to the assigned id and data
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        
        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'changed');
        
        return parent::update($data, "id = '{$id}'");
    }

    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        
        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'deleted');
        
        return parent::delete("id = '{$id}'");
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function deleteByInquiryIdAndLabelIds($inquiry_id, $label_ids) {
        
        $inquiry_id = (int) $inquiry_id;
        $sql = "inquiry_id = '{$inquiry_id}'";
        if ($label_ids) {
            $sql .= ' AND label_id NOT IN (' . implode(', ', $label_ids) . ')';
        }
        return parent::delete($sql);
    }

    public function setLabelsToInquiry($inquiry_id, $label_ids) {
      
      
        //
        // delete products not in the list
        //
        $inquiry_id = (int) $inquiry_id;         
        if (!empty($label_ids)) {
            foreach ($label_ids AS &$label_id) {              
                $label_id = (int) $label_id;
            }
        }
            
        $this->deleteByInquiryIdAndLabelIds($inquiry_id, $label_ids);

        //
        // add the new products
        //
        foreach ($label_ids AS $id) {
            $params = array(
                'inquiry_id' => $inquiry_id,
                'label_id' => $id
            );
           
            $this->assignLabelToInquiry($params);
        }
    }

    public function assignLabelToInquiry($params) {
        $inquiryLabelLink = $this->getByInquiryAndLabelId($params['inquiry_id'], $params['label_id']);

        if (!$inquiryLabelLink) {
            return $this->insert($params);
        } else {
            $this->updateById($inquiryLabelLink['id'], $params);
            return $inquiryLabelLink['id'];
        }
    }

    public function getByInquiryAndLabelId($inquiry_id, $label_id) {
        $inquiry_id = (int) $inquiry_id;
        $label_id = (int) $label_id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("inquiry_id = '{$inquiry_id}' AND label_id = '{$label_id}'");

        return $this->getAdapter()->fetchRow($select);
    }
    
    public function getByInquiryId($inquiry_id) {
        $inquiry_id = (int) $inquiry_id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("inquiry_id = '{$inquiry_id}'");

        return $this->getAdapter()->fetchAll($select);
    }

    public function insert(array $data) {

        $id = parent::insert($data);
        
        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'added');
        
        return $id;
    }
    
}
