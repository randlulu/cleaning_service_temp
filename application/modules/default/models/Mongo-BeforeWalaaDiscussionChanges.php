<?php

class Model_Mongo {

    protected $_dbhost = 'localhost';
    protected $_dbName = 'cleaning_service';
    protected $_collectionName = 'notification';
    protected $_con;
    protected $_collection;
    protected $_db;

    //protected static $db;

    /**
     * __construct
     * 
     * @param array $data as data
     * @param array $config as array of config
     * 
     * @return null
     */
    public function __construct($data = array(), $config = array()) {
        $dbhost = 'localhost';
        // Connect to test database  
        $this->_con = new Mongo("mongodb://$this->_dbhost");

        //$db = $con->cleaning_service;
        $this->_db = $this->_con->selectDB($this->_dbName);

        $this->_collection = $this->_db->selectCollection($this->_collectionName);
    }

    public function closeConnection() {
        $isClosed = $this->_con->close();
        return $isClosed;
    }

    public function getNotificationsByContractorId($lastMonth = false, $contractor_id = 0, $cases = array()) {
        $contractor_id = (int) $contractor_id;
        $last_month_value = strtotime(date('d-m-Y', strtotime(" -1 month")));

        $modelUser = new Model_User();
        $modelAuthRole = new Model_AuthRole();
        $modelAuthRoleCredential = new Model_AuthRoleCredential();

        $contractroRoleId = $modelAuthRole->getRoleIdByName('contractor');

        $loggedUser = CheckAuth::getLoggedUser();
        if ($contractor_id) {
            $userId = $contractor_id;
        } else {
            $userId = $loggedUser['user_id'];
        }
        $userReg = "/$userId/";
        $regex = new MongoRegex($userReg);
        $user = $modelUser->getById($userId);
        if (isset($cases) && !empty($cases)) {
            $credential = $cases;
        } else {
            $credential = $modelAuthRoleCredential->getCredentialsByParentName('notifications', $user['role_id']);
        }

        if ($user['role_id'] == $contractroRoleId) {
            if ($credential) {
                $where = array();
                foreach ($credential as $key => $value) {
                    $condition = array('title' => strtolower(spaceBeforeCapital($value['credential_name'])));
                    $where[] = $condition;
                }

                if ($lastMonth) {
                    if ($this->_collection->find(array('$and' => array(array('contractor_id' => $userId), array('$or' => $where)), 'date_sent' => array('$gt' => $last_month_value)))->sort(array('_id' => -1))->count() > 0) {
                        $notifications = $this->_collection->find(array('$and' => array(array('contractor_id' => $userId), array('$or' => $where)), 'date_sent' => array('$gt' => $last_month_value)))->sort(array('_id' => -1));
                    } else {
                        $notifications = 0;
                    }
                } else {
                    if ($this->_collection->find(array('$and' => array(array('contractor_id' => $userId), array('$or' => $where))))->sort(array('_id' => -1))->count() > 0) {
                        $notifications = $this->_collection->find(array('$and' => array(array('contractor_id' => $userId), array('$or' => $where))))->sort(array('_id' => -1));
                    } else {
                        $notifications = 0;
                    }
                }
            } else {
                $notifications = 0;
            }
        } else {

            if ($credential) {
                $where = array();
                foreach ($credential as $key => $value) {
                    $condition = array('title' => strtolower(spaceBeforeCapital($value['credential_name'])));
                    $where[] = $condition;
                }
                //array('notification_user' => $user['username']),
//                var_dump($where);
                if ($this->_collection->find(array('$and' => array(array('created_by' => array('$ne' => (int) $loggedUser['user_id'])), array('show' => $regex), array('$or' => $where))))->sort(array('_id' => -1))->count() > 0) {
                    $notifications = $this->_collection->find(array('$and' => array(array('created_by' => array('$ne' => (int) $loggedUser['user_id'])),array('notification_user' => $user['username']), array('show' => $regex), array('$or' => $where))))->sort(array('_id' => -1));
                } else {
                    $notifications = 0;
                }
            } else {
                $notifications = 0;
            }
        }


        return $notifications;
    }

    public function insertNotification($doc = array()) {
        return $this->_collection->insert($doc);
    }

    public function updateNotification($id) {

        $modelAuthRole = new Model_AuthRole();
        $modelAuthRoleCredential = new Model_AuthRoleCredential();
        $contractroRoleId = $modelAuthRole->getRoleIdByName('contractor');
        $loggedUser = CheckAuth::getLoggedUser();
        $userId = $loggedUser['user_id'];
        $credential = $modelAuthRoleCredential->getCredentialsByParentName('notifications', $loggedUser['role_id']);
        $user_ids = $this->getSeenByIds($credential);
        $new = "";
        $read = array("");

        foreach ($user_ids as $key => $value2) {

            $read = explode(',', $value2['read']);


            if (!in_array($userId, $read)) {
                if (!empty($value2['read'])) {
                    $new = $value2['read'] . "," . $userId;
                } else {
                    $new = (string) $userId;
                }
                $this->_collection->update(
                        array('_id' => new MongoId($id)), array('$set' => array('read' => $new))
                );
            }
        }
        return 1;
    }

    public function clearCountstatusNotification($cases = array()) {

        $modelUser = new Model_User();
        $modelAuthRole = new Model_AuthRole();
        $modelAuthRoleCredential = new Model_AuthRoleCredential();
        $contractroRoleId = $modelAuthRole->getRoleIdByName('contractor');
        $loggedUser = CheckAuth::getLoggedUser();
        $userId = $loggedUser['user_id'];
        $userReg = "/$userId/";
        $regex = new MongoRegex($userReg);
        if (isset($cases) && !empty($cases)) {
            $credential = $cases;
            $user_ids = $this->getSeenByIds($credential);
        } else {
            $credential = $modelAuthRoleCredential->getCredentialsByParentName('notifications', $loggedUser['role_id']);
            $user_ids = $this->getSeenByIds($credential);
        }


        $user_ids2 = array();
        $new = "";
        $seen = array("");

        foreach ($user_ids as $key_id => $value2) {
            $seen = explode(',', $value2['seen']);
            if (!in_array($userId, $seen)) {
                if (!empty($value2['seen'])) {
                    $new = $value2['seen'] . "," . $userId;
                } else {
                    $new = (string) $userId;
                }
                if ($contractroRoleId == $loggedUser['role_id']) {
                    foreach ($credential as $key => $value) {
                        $this->_collection->update(
                                array('$and' => array(array('contractor_id' => $userId), array('title' => strtolower(spaceBeforeCapital($value['credential_name']))))), array('$set' => array('seen' => $new)), array(
                            'multiple' => true,
                                )
                        );
                    }
                } else {
                    foreach ($credential as $key => $value) {
                        $this->_collection->update(
                                array('$and' => array(array('show' => $regex), array('_id' => new MongoId($key_id)))), array('$set' => array('seen' => $new)), array(
                            'multiple' => true,
                                )
                        );
                    }
                }
            }
        }

        return 1;
    }

    public function getSeenByIds($credential) {
//        var_dump($credential);
//        exit;
        $where = array();
        foreach ($credential as $key => $value) {
            $condition = array('title' => strtolower(spaceBeforeCapital($value['credential_name'])));
            $where[] = $condition;
        }
        $notifications = $this->_collection->find(array('$or' => $where))->sort(array('_id' => -1));

        return $notifications;
    }

    public function countNotifications($cases = array()) {
        $modelAuthRole = new Model_AuthRole();
        $modelAuthRoleCredential = new Model_AuthRoleCredential();
        $contractroRoleId = $modelAuthRole->getRoleIdByName('contractor');
        $loggedUser = CheckAuth::getLoggedUser();
        $userId = $loggedUser['user_id'];
        if (isset($cases) && !empty($cases)) {
            $credential = $cases;
        } else {
            $credential = $modelAuthRoleCredential->getCredentialsByParentName('notifications', $loggedUser['role_id']);
        }

        if ($credential) {
            $where = array();
            foreach ($credential as $key => $value) {
                $condition = array('title' => strtolower(spaceBeforeCapital($value['credential_name'])));
                $where[] = $condition;
            }
            $userReg = "/$userId/";

            $regexs = new MongoRegex($userReg);

            if ($loggedUser['role_id'] == $contractroRoleId) {
                $count = $this->_collection->find(array('$and' => array(array('contractor_id' => $userId), array('seen' => array('$not' => $regexs)), array('$or' => $where))))->count();
            } else {
                $count = $this->_collection->find(array('$and' => array(array('created_by' => array('$ne' => (int) $loggedUser['user_id'])),array('notification_user' => $loggedUser['username']), array('show' => $regexs), array('contractor_id' => array('$ne' => $userId)), array('seen' => array('$not' => $regexs)), array('$or' => $where))))->count();
            }
        }

        if (isset($count)) {
            return $count;
        } else {
            return 0;
        }
    }

    public function getUnreadNotifiedMessages() {
        $modelAuthRole = new Model_AuthRole();
        $modelAuthRoleCredential = new Model_AuthRoleCredential();
        $contractroRoleId = $modelAuthRole->getRoleIdByName('contractor');
        $loggedUser = CheckAuth::getLoggedUser();
        $userId = $loggedUser['user_id'];

        $credential = $modelAuthRoleCredential->getCredentialsByParentName('notifications', $loggedUser['role_id']);
        if ($credential) {
            $where = array();
            foreach ($credential as $key => $value) {
                $condition = array('title' => strtolower(spaceBeforeCapital($value['credential_name'])));
                $where[] = $condition;
            }
            $userReg = "/$userId/";

            $regexs = new MongoRegex($userReg);

            //if ($loggedUser['role_id'] == $contractroRoleId) {
            //    $count = $this->_collection->find(array('$and' => array(array('contractor_id' => $userId), array('seen' => array('$not' => $regexs)), array('$or' => $where))))->count();
            //} else {
            $count = $this->_collection->find();
        }
        //}

        return $count;
    }

    public function markAllAsRead($cases = array()) {
        $modelAuthRole = new Model_AuthRole();
        $contractroRoleId = $modelAuthRole->getRoleIdByName('contractor');
        $modelAuthRoleCredential = new Model_AuthRoleCredential();

        $loggedUser = CheckAuth::getLoggedUser();
        $userId = $loggedUser['user_id'];
        $new = "";
        $read = array("");

        if (isset($cases) && !empty($cases)) {
           $credential = $cases;
        } else {
            $credential = $modelAuthRoleCredential->getCredentialsByParentName('notifications', $loggedUser['role_id']);
        }
        $user_ids = $this->getSeenByIds($credential);


        foreach ($user_ids as $key => $value2) {

            $read = explode(',', $value2['read']);



            if (!in_array($userId, $read)) {

                if (!empty($value2['read'])) {
                    $new = $value2['read'] . "," . $userId;
                } else {
                    $new = (string) $userId;
                }
                foreach ($credential as $key => $value) {
                    $this->_collection->update(
                            array('title' => strtolower(spaceBeforeCapital($value['credential_name']))), array('$set' => array('read' => $new)), array('multiple' => true)
                    );
                }
            }
        }


        return 1;
    }

    public function getShowByTitle($title) {
        $notification = $this->_collection->findOne(array('title' => $title));
        return $notification['show'];
    }

    public function getMultiShowByTitle($title) {
        $notification = $this->_collection->find(array('title' => $title));
        return $notification;
    }

    public function updateShowByTitle($title) {
        $modelUser = new Model_User();
        $modelAuthRole = new Model_AuthRole();
        $modelAuthRoleCredential = new Model_AuthRoleCredential();
        $contractroRoleId = $modelAuthRole->getRoleIdByName('contractor');
        $loggedUser = CheckAuth::getLoggedUser();
        $userId = $loggedUser['user_id'];


        $user_ids = $this->getMultiShowByTitle($title);

        $new = "";
        $seen = array("");

        foreach ($user_ids as $key_id => $value2) {

            $seen = explode(',', $value2['show']);

            if (!in_array($userId, $seen)) {
                if (!empty($value2['show'])) {
                    $new = $value2['show'] . "," . $userId;
                } else {
                    $new = (string) $userId;
                }

                if ($contractroRoleId == $loggedUser['role_id']) {


                    $this->_collection->update(
                            array('$and' => array(array('contractor_id' => $userId), array('title' => $title))), array('$set' => array('show' => $new)), array(
                        'multiple' => true,
                            )
                    );
                } else {
                    $this->_collection->update(
                            array('_id' => new MongoId($key_id)), array('$set' => array('show' => $new)), array('multiple' => true)
                    );
                }
            }
        }
    }

    public function deleteByUserIdByCredentialNameAndUserID($title, $user_id) {
        $modelUser = new Model_User();
        $modelAuthRole = new Model_AuthRole();
        $modelAuthRoleCredential = new Model_AuthRoleCredential();
        $contractroRoleId = $modelAuthRole->getRoleIdByName('contractor');
        $loggedUser = CheckAuth::getLoggedUser();
        $userId = $loggedUser['user_id'];


        $user_ids = $this->getMultiShowByTitle($title);

        $new = "";
        $seen = array("");

        foreach ($user_ids as $key_id => $value2) {

            $seen = explode(',', $value2['show']);

            if (($key = array_search($userId, $seen)) !== false) {
                unset($seen[$key]);

                $new = implode(',', $seen);

                if ($contractroRoleId == $loggedUser['role_id']) {

                    $this->_collection->update(
                            array('$and' => array(array('contractor_id' => $userId), array('title' => $title))), array('$set' => array('show' => $new)), array(
                        'multiple' => true,
                            )
                    );
                } else {
                    $this->_collection->update(
                            array('_id' => new MongoId($key_id)), array('$set' => array('show' => $new)), array('multiple' => true)
                    );
                }
            }
        }
    }

    public function getByTitleAndUserId($title) {
        $loggedUser = CheckAuth::getLoggedUser();
        $user_id = $loggedUser['user_id'];
        $userReg = "/$user_id/";
        $regex = new MongoRegex($userReg);
        $notification = $this->_collection->findOne(array('$and' => array(array('title' => $title), array('show' => $regex))));
        return $notification;
    }

    public function getByContractorId($id) {
        $id = (int) $id;
        $notifications = $this->_collection->find(array('contractor_id' => $id));

        return $notifications;
    }

    public function getByTitle($title) {
        $modelUser = new Model_User();
        $modelAuthRole = new Model_AuthRole();
        $loggedUser = CheckAuth::getLoggedUser();
        $user_id = $loggedUser['user_id'];
        $userReg = "/$user_id/";
        $regex = new MongoRegex($userReg);
        $user = $modelUser->getById($user_id);
        $contractroRoleId = $modelAuthRole->getRoleIdByName('contractor');

        if ($user['role_id'] == $contractroRoleId) {

            $notification = $this->_collection->find(array('$and' => array(array('contractor_id' => $user_id), array('title' => $title))))->sort(array('_id' => -1));
        } else {
            $notification = $this->_collection->find(array('$and' => array(array('title' => $title), array('show' => $regex))))->sort(array('_id' => -1));
        }
        return $notification;
    }

}
