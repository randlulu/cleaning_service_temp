<?php

class Model_UpdateBookingQuestionStatus extends Zend_Db_Table_Abstract {

    protected $_name = 'update_booking_question_status';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from(array('ubqs' => $this->_name));
        $select->order($order);

        /*$filters['company_id'] = CheckAuth::getCompanySession();

        if ($filters) {
            if (!empty($filters['company_id'])) {
                $select->where("ubq.company_id = {$filters['company_id']}");
            }
        }*/

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }
		//echo $select->__toString();
        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function getByQuestionId($update_booking_question_id) {
		$update_booking_question_id = (int) $update_booking_question_id;
		$select1 = $this->getAdapter()->select();
        $select1->from(array('ubqs' => $this->_name));
		$select1->joinRight(array('bs' => 'booking_status'), 'ubqs.booking_status_id = bs.booking_status_id',array('x'=>'ubqs.update_booking_question_status_id','bs.name','status_id1'=>'bs.booking_status_id'));
        $select1->where("ubqs.update_booking_question_id = {$update_booking_question_id}");
		
		$select = $this->getAdapter()->select();
        $select->from(array('bss' => 'booking_status'));
		$select->joinLeft(array('rs' => $select1), 'rs.booking_status_id = bss.booking_status_id', array('status_id'=>'bss.booking_status_id', 'bss.name', 'rs.update_booking_question_status_id'));
        
		//echo $select->__toString();
        return $this->getAdapter()->fetchAll($select);
        
    }

    /**
     * delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteByQuestionId($update_booking_question_id) {
        $update_booking_question_id = (int) $update_booking_question_id;
        return parent::delete("update_booking_question_id= '{$update_booking_question_id}'");
    }
	
	public function insert(array $data){
	
        return parent::insert($data);
	
	}
	
	
}