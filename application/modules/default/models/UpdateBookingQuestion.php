<?php

class Model_UpdateBookingQuestion extends Zend_Db_Table_Abstract {

    protected $_name = 'update_booking_question';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from(array('ubq' => $this->_name));
        $select->order($order);

        $filters['company_id'] = CheckAuth::getCompanySession();

        if ($filters) {
            if (!empty($filters['company_id'])) {
                $select->where("ubq.company_id = {$filters['company_id']}");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }
		//echo $select->__toString();
        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function getByBookingStatusId($status_id) {
		$status_id = (int) $status_id;
		$company_id = CheckAuth::getCompanySession();
		$select = $this->getAdapter()->select();
        $select->from(array('ubq' => $this->_name));
        $select->joinInner(array('ubqs' => 'update_booking_question_status'), 'ubq.update_booking_question_id = ubqs.update_booking_question_id');
        $select->where("ubq.company_id = {$company_id}");
        $select->where("ubqs.booking_status_id = {$status_id}");

        return $this->getAdapter()->fetchAll($select);
        
    }
	
	public function insert(array $data){
	
        return parent::insert($data);
	
	}

	public function getByQuestionId($update_booking_question_id) {
        $update_booking_question_id = (int) $update_booking_question_id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("update_booking_question_id= '{$update_booking_question_id}'");

        return $this->getAdapter()->fetchRow($select);
    }
	
	
	public function updateByQuestionId($update_booking_question_id, array $data){
		$update_booking_question_id = (int) $update_booking_question_id;
        return parent::update($data, "update_booking_question_id = {$update_booking_question_id}");
	
	}
	
	public function deleteByQuestionId($update_booking_question_id) {
        $update_booking_question_id = (int) $update_booking_question_id;
        return parent::delete("update_booking_question_id= {$update_booking_question_id}");
    }
    
}