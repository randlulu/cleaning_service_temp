<?php
class Model_DiscussionSeq {

    protected $_dbhost = '127.0.0.1';
    protected $_dbName = 'cleaning_service';
    protected $_collectionName = 'discussion_seq';
    protected $_con;
    protected $_collection;
    protected $_db;

    //protected static $db;

    /**
     * __construct
     * 
     * @param array $data as data
     * @param array $config as array of config
     * 
     * @return null
     */
    public function __construct($data = array(), $config = array()) {
        $dbhost = 'localhost';
        // Connect to test database  
        $this->_con = new Mongo("mongodb://$this->_dbhost");

        //$db = $con->cleaning_service;
        $this->_db = $this->_con->selectDB($this->_dbName);

        $this->_collection = $this->_db->selectCollection($this->_collectionName);
    }
	
	public function updateBookingDiscussionSeq(){
		$success = $this->_collection->update(array("collection" => "booking_discussion"), array('$inc' => array("seq" => 1)));
		if($success){
			$newSeq = $this->_collection->find(array("collection" => "booking_discussion"), array("seq" => 1, "_id" => 0));
			$newSeqAsArray = iterator_to_array($newSeq);
		}
		
		//var_dump($newSeqAsArray[0]['seq']);exit;
		return $newSeqAsArray[0]['seq'];
	}
	
	public function updateComplaintDiscussionSeq(){
		$success = $this->_collection->update(array("collection" => "complaint_discussion"), array('$inc' => array("seq" => 1)));
		if($success){
			$newSeq = $this->_collection->find(array("collection" => "complaint_discussion"), array("seq" => 1, "_id" => 0));
			$newSeqAsArray = iterator_to_array($newSeq);
		}
		return $newSeqAsArray[0]['seq'];
	}
	
	public function updateEstimateDiscussionSeq(){
		$success = $this->_collection->update(array("collection" => "estimate_discussion"), array('$inc' => array("seq" => 1)));
		if($success){
			$newSeq = $this->_collection->find(array("collection" => "estimate_discussion"), array("seq" => 1, "_id" => 0));
			$newSeqAsArray = iterator_to_array($newSeq);
		}
		return $newSeqAsArray[0]['seq'];
	}
	
	public function updateInquiryDiscussionSeq(){
		$success = $this->_collection->update(array("collection" => "inquiry_discussion"), array('$inc' => array("seq" => 1)));
		if($success){
			$newSeq = $this->_collection->find(array("collection" => "inquiry_discussion"), array("seq" => 1, "_id" => 0));
			$newSeqAsArray = iterator_to_array($newSeq);
		}
		return $newSeqAsArray[0]['seq'];
	}
	
	public function updateInvoiceDiscussionSeq(){
		$success = $this->_collection->update(array("collection" => "invoice_discussion"), array('$inc' => array("seq" => 1)));
		if($success){
			$newSeq = $this->_collection->find(array("collection" => "invoice_discussion"), array("seq" => 1, "_id" => 0));
			$newSeqAsArray = iterator_to_array($newSeq);
		}
		return $newSeqAsArray[0]['seq'];
	}
}