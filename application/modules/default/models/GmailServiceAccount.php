<?php

class Model_GmailServiceAccount {

    protected $_client;
    protected $_gmail;

    public function __construct($id = null, $type = null) {
        $modelContractorGmailAccounts = new Model_ContractorGmailAccounts();

        // get the gmail account contractor
        if ($type == 'contractor') {
            $contractorAccount = $modelContractorGmailAccounts->getByContractorId($id);
            $email = isset($contractorAccount['email']) ? $contractorAccount['email'] : '';
        } else {
            $company = new Model_Companies();
            $email = $company->getEnquiryEmailByCompanyId($id);
        }
        
      
        $client_id = '233716746954-nsgmg6pn2dajp1flkrbsuj598jbvhck0.apps.googleusercontent.com'; // Service Account CLIENT ID
        $service_account_name = '233716746954-nsgmg6pn2dajp1flkrbsuj598jbvhck0@developer.gserviceaccount.com'; // Service Account EMAIL ADDRESS
        $key_file_location = APPLICATION_PATH . '/Tile Cleaners-83d152f9fa40.p12'; // *.p12 file location


        $client = new Google_Client();
        $client->setApplicationName("My Project");


        //$client->setDeveloperKey(123); // API KEY (Key for server applications) 

        $gmail = new Google_Service_Gmail($client);

        $key = file_get_contents(APPLICATION_PATH . '/Tile Cleaners-83d152f9fa40.p12');

        $scopes = array('https://mail.google.com/', 'https://www.googleapis.com/auth/gmail.readonly', 'https://www.googleapis.com/auth/gmail.modify', 'https://www.googleapis.com/auth/gmail.compose',);

        $cred = new Google_Auth_AssertionCredentials(
                $service_account_name, $scopes, $key
        );
        //$cred->sub = "arielqld@tilecleaners.com.au";
        $cred->sub = $email;
        $client->setAssertionCredentials($cred);
        /* if ($client->getAuth()->isAccessTokenExpired()){
          $client->getAuth()->refreshTokenWithAssertion($cred);
          } */

        $_SESSION['service_token'] = $client->getAccessToken();
        ////put the client and cal in static variable
        $this->_client = $client;
        $this->_gmail = $gmail;
    }

    public function getAllEmails() {
        $optParams = array();
        $messages = $this->_gmail->users_messages->listUsersMessages('me', $optParams);
        $list = $messages->getMessages();
        return $list;
    }
	
	//D.A 28/08/2015
	public function getAllEmailsCount() {
        $optParams = array();
        $optParams['labelIds'] = 'UNREAD';
        $messages = $this->_gmail->users_messages->listUsersMessages('me', $optParams);
        $list = $messages->getMessages();
        return count($list);
    }

    public function getService() {

        return $this->_gmail;
    }

}

?>