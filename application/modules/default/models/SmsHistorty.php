<?php
class Model_SmsHistorty extends Zend_Db_Table_Abstract {
    protected $_name = 'sms_history';
	public function getAll($filters = array(), $order = null, &$pager = null, $limit = 0) {
        $select = $this->getAdapter()->select();
        $select->from(array('el' => $this->_name));//cron_job_history
		$select->joinLeft(array('ch' => 'cronjob_history'), 'el.cron_job_history = ch.id', '');
        $select->joinLeft(array('cj' => 'cron_job'), 'ch.cron_job_id = cj.id', 'cj.cronjob_name');

		if ($order) {
            $select->order($order);
        }
		  if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        } elseif ($limit) {
            $select->limit($limit);
        } elseif ($perPage && $currentPage) {
            $select->limit($perPage, ($currentPage - 1) * $perPage);
        }

        if ($filters) {
            if (!empty($filters['reference_id'])) {
                $reference_id = $this->getAdapter()->quote($filters['reference_id']);
                $select->where("el.reference_id = {$reference_id}");
            }
			
			 if (!empty($filters['sms_type'])) {
                $sms_type = $this->getAdapter()->quote($filters['sms_type']);
                $select->where("el.sms_type = {$sms_type}");
            }
			
			 if (!empty($filters['sms_id'])) {
                $sms_id = $this->getAdapter()->quote($filters['sms_id']);
                $select->where("el.id = {$sms_id}");
            }
		
			
			if (!empty($filters['mobileNum'])) {
				
                $mobile_no = $this->getAdapter()->quote($filters['mobileNum']);
				
                $select->where("el.from = {$mobile_no} or el.to = {$mobile_no} ");
            }
			
			
			
        }

        return $this->getAdapter()->fetchAll($select);
    }
    /**
     * update table rows according to the assigned id and data
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
		
        return parent::update($data, "id = '{$id}'");
    }

    public function insert($data) {
        return parent::insert($data);
    }
    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("id = '{$id}'");
    }
    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id = '{$id}'");
        return $this->getAdapter()->fetchRow($select);
    }

    public function getByReferenceIdAndCronjobHistoryId($ref_id, $history_id) {

        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("reference_id = {$ref_id}");
        $select->where("cronjob_history_id = {$history_id}");
        
        return $this->getAdapter()->fetchAll($select);
    }
	
	public function IsParent($id) {
			$id = (int) $id;
			$select = $this->getAdapter()->select();
			$select->from($this->_name);
			$select->where("parent_id = '{$id}'");
			$rows=$this->getAdapter()->fetchAll($select);
			if(count($rows)>0)
				return true;
			else
				return false;
				
			
		}
		
		public function getAllChild($id) {
			$id = (int) $id;
			$select = $this->getAdapter()->select();
			$select->from($this->_name);
			$select->where("parent_id = '{$id}'");
			return $this->getAdapter()->fetchAll($select);
		
		}
		
		public function getMobileFormat($to)
		{
			if (strpos($to, '+') !== false)
				{
				 $mobile_no = ltrim($to, '+');
				 $finalFormat="00".$mobile_no;
				  return $finalFormat;
				}
				else
				{
				  $mobile_no = substr($to, 2);
				  $finalFormat="+".$mobile_no;
                  if(strlen($to)<13 ){
					 $finalFormat="";
					 return 0;
					 }
					else   
				  return $finalFormat;
				}
	
		}
		
		public function getLastMessageId($to)
		{
			$select = $this->getAdapter()->select();
			$select->from($this->_name);
		    $select->where("sms_history.to = '{$to}' and status!='undelivered' and status!='Failed' and parent_id=0 and sms_type='sent'");
            $select->order("id desc");
		    $select->limit(1);
            return $this->getAdapter()->fetchRow($select);

		}
		
		public function getLastMessageBtReceiverId($reference_id,$id,$sms_reason,$date='')
		{
			$id = (int) $id;
	      if($reference_id==-1)
				$type="contractor";
			else
				$type="customer";
			$select = $this->getAdapter()->select();
			$select->from($this->_name);
			if($date!='')
		    $select->where("(select type from sms_template where id=sms_history.reference_id )='{$type}' and sms_history.send_time <'{$date}' and receiver_id = '{$id}' and sms_reason = '{$sms_reason}' and sms_history.status!='undelivered' and sms_history.status!='Failed' and sms_type='sent' ");
			else
			$select->where("(select type from sms_template where id=sms_history.reference_id )='{$type}' and receiver_id = '{$id}' and  sms_reason = '{$sms_reason}' and status!='undelivered' and status!='Failed' and sms_type='sent' ");
			$select->order("id desc");
		    $select->limit(1);
			return $this->getAdapter()->fetchRow($select);

		}
		
     		public function getLastMessageesAtSameDay($to,$date)
		{
			$date=date('Y-m-d',strtotime($date));
			$select = $this->getAdapter()->select();
			$select->from($this->_name);
			$select->where("sms_history.to = '{$to}' and  sms_type='sent' and parent_id=0 and send_time like '{$date}%' and status!='undelivered' and status!='Failed'");
            $select->order("id desc");
			return $this->getAdapter()->fetchAll($select);

		}
		
		public function getAllMobileNumber()
		{
		    $stmt = $this->getAdapter()->query("SELECT sms_history.to AS mobile_no FROM cleaning_service.sms_history
            UNION SELECT sms_history.from AS mobile_no FROM cleaning_service.sms_history");
            $rows = $stmt->fetchAll();
			return $rows;

		}

		public function sendSmsTwilio($fromNumber,$toNumber,$smsMessage)
		{
            $company_id = CheckAuth::getCompanySession();
            $api_name = 'twilio';

            $model_api_parameters = new Model_ApiParameters();

            $parameter_name = 'sid';
            $parameter = $model_api_parameters->getParameterValue($company_id, $api_name, $parameter_name);
            $sid = $parameter['parameter_value'];  
            
            $parameter_name = 'token';
            $parameter = $model_api_parameters->getParameterValue($company_id, $api_name, $parameter_name);
            $token = $parameter['parameter_value'];  
            
			//$sid = 'ACd698f0b22aa408a0865abb86cc97b35d';
			//$token = '13359760efe3d9ff0057ebf0df7213dc';
			$client = new Services_Twilio($sid, $token);
				 try{ 	
						if(is_null($sid) || is_null($token) || empty($sid) || empty($token))
						{
							echo "<p>Failed to retrieve Twilio authentication parameters.</p>";
						}
						else 
						{
							$message =$client->account->messages->sendMessage(
							$fromNumber,
							$toNumber,
							$smsMessage
							);
						$sid_msg=$message->sid;
						return $sid_msg; 
					   }
					 }
					  catch(Exception $e) 
					  {
					  echo '<p>There was an error sending an SMS using Twilio!!!</p>';
					  echo $e->getMessage();
					   }

			    	}
					
		public function getByReasonTypeId($sms_reason,$reason_id) 
		{
        $id = (int) $reason_id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("reason_id = '{$id}' and sms_reason='{$sms_reason}'");
        return $this->getAdapter()->fetchAll($select);
        

        }
			
        public function getOutgoingSmsSameDateType($reference_id,$user_id,$reason,$date_received)
        {
			if($reference_id==-1)
				$type="contractor";
			else
				$type="customer";
			
			$date=date('Y-m-d',strtotime($date_received));
			
			$select = $this->getAdapter()->select();
			$select->from($this->_name);
			$select->where("(select type from sms_template where id=sms_history.reference_id )='{$type}' and receiver_id= '{$user_id}' and sms_reason= '{$reason}'  and sms_type='sent' and parent_id=0 and send_time <'{$date_received}' and status!='undelivered' and status!='Failed' and template_type='standard'");
            $select->order("id desc");
			$select->limit(3);
			return $this->getAdapter()->fetchAll($select);

		}
		
			public function getLastMessageBtReceiverId1($to,$date='')
		{
			$id = (int) $id;
			$select = $this->getAdapter()->select();
			$select->from($this->_name);
			if($date!='')
		    $select->where("sms_history.send_time <'{$date}' and sms_history.to = '{$to}' and sms_reason = '{$sms_reason}' and sms_history.status!='undelivered' and sms_history.status!='Failed' and sms_type='sent' ");
			else
			$select->where("sms_history.to = '{$to}' and status!='undelivered' and status!='Failed' and sms_type='sent' ");
			$select->order("id desc");
		    $select->limit(1);
			return $this->getAdapter()->fetchRow($select);

		}
		
		
		public function getChildOfParent($incomingsid)
		{
			$incomingsid = (int) $incomingsid;
			$select = $this->getAdapter()->select();
			$select->from($this->_name);
			$select->where("parent_id = '{$incomingsid}' and status!='undelivered' and status!='Failed' and sms_type='sent' ");
			$select->order("id desc");
			return $this->getAdapter()->fetchAll($select);
		}
		
		
		public function getCountMessage()
		{
			$select = $this->getAdapter()->select();
			$select->from($this->_name);
		    $select->where("status=''");
            return $this->getAdapter()->fetchAll($select);
            
		}

		public function is_send_today($date,$type,$id,$cronjonID)
		{
			$date=date('Y-m-d',strtotime($date));
            $select = $this->getAdapter()->select();
			$select->from($this->_name);
		    $select->where("sms_reason='{$type}' and reason_id='{$id}' and cron_job_history='{$cronjonID}' and send_time like '{$date}%'");
            return $this->getAdapter()->fetchAll($select);
            
		}
		
		public function getSmsByContractorId($contractor_id) 
		{
            $id = (int) $contractor_id;
		    $select1 = $this->getAdapter()->select();
			$select1->from($this->_name);
			$select1->joinInner(array('sms_temp' => 'sms_template'), 'sms_temp.id = sms_history.reference_id', array('sms_temp.name'));
			$select1->where("sms_temp.type='contractor'");
			$select1->where("receiver_id = '{$id}'");
            $select1->where("template_type = 'standard'");
			$select2 = $this->getAdapter()->select();
			$select2->from($this->_name);
			$select2->joinInner(array('canned_res' => 'canned_responses'), 'canned_res.id = sms_history.reference_id' , array('canned_res.receriver_type'));
			$select2->where("canned_res.receriver_type='contractor'");
			$select2->where("receiver_id = '{$id}'");
			$select2->where("template_type = 'free' or template_type='canned'");
			$select = $this->getAdapter()->select();
			$select->union(array($select1, $select2));
			$select->order("id desc");
			return $this->getAdapter()->fetchAll($select);

        }
		
		public function getSmsIncomingReplyByContractorId($contractor_id) 
		{
            $id = (int) $contractor_id;
			$un_standard= -1;
			$un_canned= -6;
			$select = $this->getAdapter()->select();
			$select->from($this->_name);
			$select->where("reference_id='{$un_standard}' or reference_id='{$un_canned}' ");
			$select->where("receiver_id = '{$id}'");
            $select->order("id desc");
			return $this->getAdapter()->fetchAll($select);
        }
		
		/*************************/
		public function getSmsInbox($reference_id,$user_id)
		{
			
	       $loggedUser = CheckAuth::getLoggedUser();
           $modelAuthRole = new Model_AuthRole ();
           $contractorRoleId = $modelAuthRole->getRoleIdByName('contractor');
           $contractor_id=$loggedUser['user_id'];
		    if($reference_id==-1)
			{
				$type="contractor";
			}
			else
			{
			$type="customer";
			}
			$select = $this->getAdapter()->select();
			$select->from($this->_name);
			$select->where(" receiver_id ='{$user_id}' and (reference_id in (select id from sms_template where type='{$type}')  or reference_id='{$reference_id}')");
			
			if ($loggedUser['role_id'] == $contractorRoleId)
			{ 
            $select->where("sms_reason='booking'");
			$select->join(array('csb' => 'contractor_service_booking'), 'sms_history.reason_id= csb.booking_id', array('csb.contractor_id'));
            $select->where("contractor_id='{$contractor_id}'");
			}
			$select->order("id desc");
			return $this->getAdapter()->fetchAll($select);

		}
		
		
		
		
		}