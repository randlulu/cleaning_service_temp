<?php

class Model_ContractorService extends Zend_Db_Table_Abstract {

    protected $_name = 'contractor_service';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from(array('cs' => $this->_name));
        $select->joinInner(array('u' => 'user'), 'cs.contractor_id=u.user_id', array('u.username'));
        $select->joinInner(array('s' => 'service'), 'cs.service_id=s.service_id', array('s.service_name'));
        $select->order($order);
        $select->distinct();

        if ($filters) {
            if (!empty($filters['contractor_id'])) {
                $select->where("cs.contractor_id = {$filters['contractor_id']}");
            }
            if (!empty($filters['service_id'])) {
                $select->where("cs.service_id = {$filters['service_id']}");
            }
            if (!empty($filters['city_id'])) {
                $select->joinInner(array('c' => 'contractor_service_availability'), 'cs.contractor_service_id=c.contractor_service_id', '');
                $select->where("c.city_id = {$filters['city_id']}");
            }
            if (!empty($filters['active'])) {
                $select->where("u.active = '{$filters['active']}'");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "contractor_service_id = '{$id}'");
    }

    /**
     * delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("contractor_service_id = '{$id}'");
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("contractor_service_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the assigned Contractor Id
     * 
     * @param int $id
     * @return array
     */
    public function getByContractorId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("contractor_id = '{$id}'");

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * get table row according to the assigned Contractor Id and Service Id
     * 
     * @param int $contractor_id
     * @param int $service_id
     * @return array
     */
    public function getByContractorIdAndServiceId($contractor_id, $service_id) {
        $contractor_id = (int) $contractor_id;
        $service_id = (int) $service_id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("contractor_id = '{$contractor_id}'");
        $select->where("service_id = '{$service_id}'");

		//echo 'sql.....'.$select->__toString();
        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the assigned Service Id
     * 
     * @param int $id
     * @return array
     */
    public function getByServiceId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("service_id = '{$id}'");

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * get table row according to the assigned Service Id
     * 
     * @param int $id
     * @param boolean $asArray
     * @return array
     */
    public function getContractorByServiceId($serviceid, $asArray = false) {
        $serviceid = (int) $serviceid;
        $modelContractorInfo = new Model_ContractorInfo();

        $contractors = $this->getAll(array('service_id' => $serviceid, 'active' => 'TRUE'));
        $modelContractorInfo->fills($contractors, array('contractor_info_by_contractor_id'));

        if ($asArray) {
            $data = array();
            foreach ($contractors as $contractor) {
                $data[$contractor['contractor_id']] = ucwords($contractor['username']) . (!empty($contractor['contractor_info']['business_name']) ? ' - ' . ucwords($contractor['contractor_info']['business_name']) : '');
            }
            return $data;
        } else {
            return $contractors;
        }
    }

    /**
     * get table row according to the assigned Service Id
     * 
     * @param int $id
     * @param boolean $asArray
     * @return array
     */
    public function getContractorByServiceIdAndCityId($serviceid, $cityId, $asArray = false) {
        $serviceid = (int) $serviceid;
        $modelContractorInfo = new Model_ContractorInfo();

        $contractors = $this->getAll(array('service_id' => $serviceid, 'city_id' => $cityId, 'active' => 'TRUE'));
        $modelContractorInfo->fills($contractors, array('contractor_info_by_contractor_id'));

        if ($asArray) {
            $data = array();
            foreach ($contractors as $contractor) {
                $data[$contractor['contractor_id']] = ucwords($contractor['username']) . (!empty($contractor['contractor_info']['business_name']) ? ' - ' . ucwords($contractor['contractor_info']['business_name']) : '');
            }
            return $data;
        } else {
            return $contractors;
        }
    }

    public function deleteRelatedContractorService($id) {

        //delete data from contractor_service_availability
        $this->getAdapter()->delete('contractor_service_availability', "contractor_service_id = '{$id}'");
    }

    public function checkIfThisServiceJustBelongsToGeneralContractor($serviceId) {

        $modelCompanies = new Model_Companies();
        $modelUser = new Model_User();

        $companyId = CheckAuth::getCompanySession();
        $company = $modelCompanies->getById($companyId);
        $user_code = "General {$company['company_name']}";
        $generalContractor = $modelUser->getByUserCode(sha1($user_code));

        $contractors = $this->getContractorByServiceId($serviceId, true);
        $justBelong = true;
        foreach ($contractors as $contractor_id => $contractor) {
            if ($generalContractor[user_id] != $contractor_id) {
                $justBelong = false;
            }
        }
        return $justBelong;
    }

}