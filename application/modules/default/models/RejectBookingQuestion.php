<?php

class Model_RejectBookingQuestion extends Zend_Db_Table_Abstract {

    protected $_name = 'reject_booking_question';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order($order);

        $filters['company_id'] = CheckAuth::getCompanySession();

        if ($filters) {
            if (!empty($filters['company_id'])) {
                $select->where("company_id = {$filters['company_id']}");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }
        return $this->getAdapter()->fetchAll($select);
    }


	public function insert(array $data){
	
        return parent::insert($data);
	
	}

	public function getByQuestionId($reject_booking_question_id) {
        $reject_booking_question_id = (int) $reject_booking_question_id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id= '{$reject_booking_question_id}'");

        return $this->getAdapter()->fetchRow($select);
    }
	
	
	public function updateByQuestionId($reject_booking_question_id, array $data){
        $reject_booking_question_id = (int) $reject_booking_question_id;
        return parent::update($data, "id = {$reject_booking_question_id}");
	
	}
	
	public function deleteByQuestionId($reject_booking_question_id) {
        $reject_booking_question_id = (int) $reject_booking_question_id;
        return parent::delete("id= {$reject_booking_question_id}");
    }
    
}