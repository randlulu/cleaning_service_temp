<?php

class Model_ServiceAttributeValueTemp extends Zend_Db_Table_Abstract {

    protected $_name = 'service_attribute_value_temp';

    /**
     * update table row according to the assigned $id on the submited $data
     * 
     * @param int $id
     * @param array $order
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "service_attribute_value_id= '{$id}'");
    }

    /**
     * delete table row according to the assigned $id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("service_attribute_value_id= '{$id}'");
    }

    /**
     * get table row according to the assigned $id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("service_attribute_value_id= '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table rows according to the assigned $id
     * 
     * @param int $id
     * @return array 
     */
    public function getByBookingId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from(array('sav' => $this->_name));
        $select->joinInner(array('sa' => 'service_attribute'), 'sav.service_attribute_id=sa.service_attribute_id', array('sa.service_id,sa.attribute_id'));
        $select->where("booking_id= '{$id}'");

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * get table rows according to the assigned bookingId ,serviceAttributeId and clone 
     * 
     * @param int $bookingId
     * @param int $serviceAttributeId
     * @param  int $clone
     * @return array 
     */
    public function getByBookingIdAndServiceAttributeIdAndClone($bookingId, $serviceAttributeId, $clone) {
        $bookingId = (int) $bookingId;
        $serviceAttributeId = (int) $serviceAttributeId;
        $clone = (int) $clone;

        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_id = '{$bookingId}' AND service_attribute_id = '{$serviceAttributeId}' AND clone = '{$clone}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * add attribute row by the assigned bookingId ,serviceAttributeId and clone 
     * 
     * @param int $bookingId
     * @param int $serviceId
     * @param  int $clone 
     */
    public function addAttributeByBookingIdAndServiceIdAndClone($bookingId, $serviceId, $clone) {
        $bookingId = (int) $bookingId;
        $serviceId = (int) $serviceId;
        $clone = (int) $clone;

        $request = Zend_Controller_Front::getInstance()->getRequest();

        $serviceObj = new Model_Services();
        $attributesObj = new Model_Attributes();
        $serviceAttributeObj = new Model_ServiceAttribute();
        $modelServiceAttributeValue = new Model_ServiceAttributeValue();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelContractorServiceBookingTemp = new Model_ContractorServiceBookingTemp();
        $modelBooking = new Model_Booking();

        $service = $serviceObj->getById($serviceId);

        $serviceAttribute = $serviceAttributeObj->getAttributeByServiceId($serviceId);

        $isChange = false;
        if ($serviceAttribute) {
            foreach ($serviceAttribute as $attribute) {

                $value = $request->getParam('attribute_' . $serviceId . $attribute['attribute_id'] . ($clone ? '_' . $clone : ''));
                $value = !empty($value) ? $value : '';

                $serviceAttributeValue = $this->getByBookingIdAndServiceAttributeIdAndClone($bookingId, $attribute['service_attribute_id'], $clone);

                $is_serialize = 0;
                if (is_array($value)) {
                    $value = serialize($value);
                    $is_serialize = 1;
                }

                $data = array(
                    'service_attribute_id' => $attribute['service_attribute_id'],
                    'booking_id' => $bookingId,
                    'value' => $value,
                    'is_serialized_array' => $is_serialize,
                    'clone' => $clone
                );

                if ($serviceAttributeValue) {
                    $this->updateById($serviceAttributeValue['service_attribute_value_id'], $data);
                } else {
                    $this->insert($data);
                }

                $serviceAttributeValueOriginal = $modelServiceAttributeValue->getByBookingIdAndServiceAttributeIdAndClone($bookingId, $attribute['service_attribute_id'], $clone);
                $dataOriginal = array(
                    'service_attribute_id' => $serviceAttributeValueOriginal['service_attribute_id'],
                    'booking_id' => $serviceAttributeValueOriginal['booking_id'],
                    'value' => $serviceAttributeValueOriginal['value'],
                    'is_serialized_array' => $serviceAttributeValueOriginal['is_serialized_array'],
                    'clone' => $serviceAttributeValueOriginal['clone']
                );

                if ($data != $dataOriginal) {
                    $isChange = true;
                }
            }
        }

        $db_params = array();
        $db_params['is_change'] = $isChange;
        $bookingServicelink = $modelContractorServiceBooking->getByBookingAndServiceAndClone($bookingId, $serviceId, $clone);
        if ($bookingServicelink) {
            $modelContractorServiceBooking->updateById($bookingServicelink['id'], $db_params);
        }
        $bookingServiceTemplink = $modelContractorServiceBookingTemp->getByBookingAndServiceAndClone($bookingId, $serviceId, $clone);
        $modelContractorServiceBookingTemp->updateById($bookingServiceTemplink['id'], $db_params);

        if ($isChange) {
            $db_params = array();
            $db_params['is_change'] = 1;
            $modelBooking->updateById($bookingId, $db_params);
        }
    }

    /**
     * delete table row according to the assigned bookingId ,serviceAttributeId and clone 
     * 
     * @param int $bookingId
     * @param int $serviceId
     * @param  int $clone
     */
    public function deleteByBookingIdServiceIdAndClone($bookingId, $serviceId, $clone) {
        $bookingId = (int) $bookingId;
        $clone = (int) $clone;
        $serviceId = (int) $serviceId;

        $serviceAttributeObj = new Model_ServiceAttribute();
        $serviceAttribute = $serviceAttributeObj->getAttributeByServiceId($serviceId);

        if ($serviceAttribute) {
            foreach ($serviceAttribute as $attribute) {
                $this->delete("service_attribute_id= '{$attribute['service_attribute_id']}' AND booking_id= '{$bookingId}' AND clone= '{$clone}'");
            }
        }
    }

    /**
     * delete table row according to the assigned bookingId 
     * 
     * @param int $bookingId
     */
    public function deleteByBookingId($bookingId) {
        $bookingId = (int) $bookingId;

        return parent::delete("booking_id= '{$bookingId}'");
    }

}