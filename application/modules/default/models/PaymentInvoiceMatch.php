<?php

class Model_PaymentInvoiceMatch extends Zend_Db_Table_Abstract {

    protected $_name = 'payment_invoice_match';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from(array('pim' => $this->_name));
//        $select->joinLeft(array('u' => 'user'), 'u.user_id = f.user_id', array('username'));

        /*         * *****************************IBM */
        $wheresAndJoins = $this->getWheresAndJoinsByFilters($filters);

        $joinInner = $wheresAndJoins['joinInner'];
        $wheres = $wheresAndJoins['wheres'];

        if ($wheres) {
            foreach ($wheres as $where) {
                $select->where($where);
            }
        }

        if ($joinInner) {
            foreach ($joinInner as $inner) {
                $select->joinInner($inner['name'], $inner['cond'], $inner['cols']);
            }
        }

        $select->order($order);

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        /*         * ***************************** */



        return $this->getAdapter()->fetchAll($select);
    }

    private function getWheresAndJoinsByFilters($filters) {
        $wheres = array();
        $joinInner = array();

        if ($filters) {

            /*             * ******Filter By Service *****IBM */
            if (!empty($filters['payment_id'])) {

//                $faqservice = $this->getAdapter()->quote(trim($filters['service']));
//                $joinInner['faq_service'] = array('name' => array('fs' => 'faq_service'), 'cond' => 'f.faq_id = fs.faq_id', 'cols' => '');
                $wheres['payment'] = ("pim.payment_id = {$filters['payment_id']}");
            }
            /*             * ****Filter By Floor *****IBM */
//            if (!empty($filters['floor'])) {
//
//                $faqByFloor = $this->getAdapter()->quote(trim($filters['floor']));
//                $joinInner['faq_service'] = array('name' => array('fs' => 'faq_service'), 'cond' => 'f.faq_id = fs.faq_id', 'cols' => '');
//                $wheres['floor'] = ("fs.floor_id = {$faqByFloor}");
//            }
            /*             * *******End Filter By Floor******* */
        }
        return array('wheres' => $wheres, 'joinInner' => $joinInner);
    }

    public function insert(array $data) {
        $id = parent::insert($data);
        return $id;
    }

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "payment_invoice_match_id = '{$id}'");
    }

    /**
     * delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("payment_invoice_match_id = '{$id}'");
    }
    
    public function deleteByPaymentId($id) {
        $id = (int) $id;
        return parent::delete("payment_id = '{$id}'");
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from(array('pim' => $this->_name));
        //$select->joinInner(array('fs' => 'faq_service'), 'f.faq_id = fs.faq_id');
        $select->where("pim.payment_invoice_match_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function getCountByPaymentId($payment_id) {
        $payment_id = (int) $payment_id;
        $select = $this->getAdapter()->select();
        $select->from(array('pim' => $this->_name), array('count' => 'count(pim.payment_id)'));
        //$select->joinInner(array('fs' => 'faq_service'), 'f.faq_id = fs.faq_id');
        $select->where("pim.payment_id = '{$payment_id}'");
        return $this->getAdapter()->fetchRow($select);
    }

    public function getMaxScoreOfPayments($payment_id) {
        $payment_id = (int) $payment_id;
        $select = $this->getAdapter()->select();
        $select->from(array('pim' => $this->_name));
        $select->where("pim.payment_id = '{$payment_id}'");
        $select->order("pim.score_count DESC");
        $select->limit(1);
        return $this->getAdapter()->fetchRow($select);
    }

}
