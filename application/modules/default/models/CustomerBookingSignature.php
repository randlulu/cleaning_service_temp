<?php

class Model_CustomerBookingSignature extends Zend_Db_Table_Abstract {

    protected $_name = 'customer_booking_signature';
	
	public function insert(array $data) {
        $id = parent::insert($data);
        return $id;
    }
	
	public function getByBookingIdAndCustomerContactId($bookingId, $customerContactId){
		$bookingId = (int) $bookingId;
        $customerContactId = (int) $customerContactId;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_id = '{$bookingId}' AND customer_contact_id = '{$customerContactId}'");

        return $this->getAdapter()->fetchRow($select);
	}
	
	/**
     * update table row according to the assigned wheres
     * 
     * @param array $wheres
     * @param array $data
     * @return boolean
     */
	
	public function updateByDifferentCriterias($wheres, $data) {
        return parent::update($data, $wheres);
    }
}