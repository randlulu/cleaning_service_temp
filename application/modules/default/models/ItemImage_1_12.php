<?php

class Model_ItemImage extends Zend_Db_Table_Abstract {

    protected $_name = 'item_image';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    
	public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "image_id = '{$id}'");
    }

	public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("image_id = '{$id}'");
    }

	
	public function getAll(){

	    $select = $this->getAdapter()->select();
        $select->from(array('iu' => $this->_name));				
		return $this->getAdapter()->fetchAll($select);
	
	}
   


}