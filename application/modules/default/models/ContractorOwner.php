<?php

class Model_ContractorOwner extends Zend_Db_Table_Abstract {

    protected $_name = 'contractor_owner';

    
    /**
     *get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from(array('co' => $this->_name));
        $select->joinInner(array('ci' => 'contractor_info'), 'ci.contractor_info_id=co.contractor_info_id', array('ci.contractor_id'));
        $select->joinInner(array('u' => 'user'), 'ci.contractor_id=u.user_id', array('u.username'));
        $select->joinInner(array('c' => 'city'), 'c.city_id=co.city_id', array('c.country_id' , 'c.city_name'));
        $select->joinInner(array('cu' => 'country'), 'cu.country_id= c.country_id', array('cu.country_name'));
        $select->order($order);

        if (!empty($filters['is_deleted'])) {
              $select->where("co.is_deleted = 1");
            } else {
               $select->where("co.is_deleted = 0");
            }
            
        if ($filters) {
            if (!empty($filters['contractor_info_id'])) {
                $select->where("co.contractor_info_id = {$filters['contractor_info_id']}");
            }
             if (!empty($filters['keywords'])) {
                $keywords = $this->getAdapter()->quote('%' . $filters['keywords'] . '%');
                $select->where("co.name LIKE {$keywords}");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

   /**
    * update table row according to the assigned id
    * 
    * @param int $id
    * @param array $data
    * @return boolean
    */ 
	
   public function updateById($id, $data) {
        $id = (int) $id;
        //return parent::update($data, "contractor_owner_id = '{$id}'");

        $modelUser = new Model_User();
        $contractorOwner = $this->getById($id);

        $modelContractorInfo = new Model_ContractorInfo();
        $contractorInfo = $modelContractorInfo->getById($contractorOwner['contractor_info_id']);
        $ret_val = parent::update($data, "contractor_owner_id = '{$id}'");
        if ($ret_val) {
            $modelUser->calculateProfileCompleteness($contractorInfo['contractor_id']);
        }
        return $ret_val;
    }

    /**
     *delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean 
     */
     public function deleteById($id) {
        $id = (int) $id;
        $modelUser = new Model_User();
        $contractorOwner = $this->getById($id);

        $modelContractorInfo = new Model_ContractorInfo();
        $contractorInfo = $modelContractorInfo->getById($contractorOwner['contractor_info_id']);
        $ret_val = parent::delete("contractor_owner_id = '{$id}'");
        if ($ret_val) {
            $modelUser->calculateProfileCompleteness($contractorInfo['contractor_id']);
        }
        return $ret_val;
    }

    /**
     *get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from(array('co'=>$this->_name));
        $select->where("co.contractor_owner_id = '{$id}'");
		 $select->joinInner(array('c' => 'city'), 'c.city_id=co.city_id', array('c.country_id' , 'c.city_name'));
        $select->joinInner(array('cu' => 'country'), 'cu.country_id= c.country_id', array('cu.country_name'));

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     *get table row according to the assigned Contractor Info Id
     * 
     * @param int $id
     * @return array
     */
    public function getByContractorInfoId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("contractor_info_id = '{$id}'");
        $select->where("is_deleted = 0");

        return $this->getAdapter()->fetchAll($select);
    }
	
	public function deleteByContractorInfoId($contractor_info_id){
	 
	 return parent::delete("contractor_info_id = '{$contractor_info_id}'");
	 
	}
    
}