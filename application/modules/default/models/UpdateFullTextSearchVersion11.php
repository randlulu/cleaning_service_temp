<?php

class Model_UpdateFullTextSearch extends Zend_Db_Table_Abstract {

    protected $_name = 'update_full_text_search';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order($order);


        if ($filters) {
            if (!empty($filters['type'])) {
                $type = $this->getAdapter()->quote(trim($filters['type']));
                $select->where("type = {$type}");
            }
            if (!empty($filters['type_id'])) {
                $type_id = (int) $filters['type_id'];
                $select->where("type_id = {$type_id}");
            }
            if (!empty($filters['is_update']) && in_array($filters['is_update'], array('yes', 'no'))) {
                if ($filters['is_update'] == 'yes') {
                    $select->where("is_update = 1");
                } else if ($filters['is_update'] == 'no') {
                    $select->where("is_update = 0");
                }
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table rows according to the assigned id and data
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "id = '{$id}'");
    }

    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("id = '{$id}'");
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the type and type_id
     * 
     * @param int $id
     * @return array 
     */
    public function getByTypeAndTypeId($type, $type_id) {
        $type_id = (int) $type_id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("type_id = '{$type_id}'");
        $select->where("type = '{$type}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function getDataToUpdate() {

        $data = array();

        $types = $this->getTypes();

        if ($types) {
            foreach ($types as $type) {
                $data[$type['type']] = $this->getAll(array('type' => $type['type'], 'is_update' => 'no'));
            }
        }

        return $data;
    }

    public function getTypes() {
        $select = $this->getAdapter()->select();
        $select->from($this->_name, 'type');
        $select->distinct();
        return $this->getAdapter()->fetchALL($select);
    }

    public function updateFullTextSearchFlag($type, $id, $done) {

        $data = $this->getByTypeAndTypeId($type, $id);
        if ($data) {
            $this->updateById($data['id'], array('is_update' => $done));
        } else {
            $this->insertFullTextSearch($type, $id, $done);
        }
    }

    public function insertFullTextSearch($type, $id, $done = 0) {

        $db_params = array(
            'type' => $type,
            'type_id' => $id,
            'is_update' => $done
        );

        $this->insert($db_params);
    }

    public function cronJobUpdateFullTextSearch() {

        $dataToUpdate = $this->getDataToUpdate();

        if ($dataToUpdate) {

            $modelBooking = new Model_Booking();
            $modelBookingEstimate = new Model_BookingEstimate();
            $modelBookingInvoice = new Model_BookingInvoice();
            $modelComplaint = new Model_Complaint();
            $modelInquiry = new Model_Inquiry();
            $modelCustomer = new Model_Customer();

            if (isset($dataToUpdate['booking']) && $dataToUpdate['booking']) {
                foreach ($dataToUpdate['booking'] as $bookingToUpdate) {

                    $booking = $modelBooking->getById($bookingToUpdate['type_id']);
                    if ($booking) {
                        $modelBooking->fill($booking, array('invoice', 'estimate', 'complaint'));

                        $modelBooking->updateFullTextSearch($booking['booking_id']);
                        $modelCustomer->updateFullTextSearch($booking['customer_id']);

                        if ($booking['original_inquiry_id']) {
                            $modelInquiry->updateFullTextSearch($booking['original_inquiry_id']);
                        }

                        if (isset($booking['invoice']['id']) && $booking['invoice']['id']) {
                            $modelBookingInvoice->updateFullTextSearch($booking['invoice']['id']);
                        }

                        if (isset($booking['estimate']['id']) && $booking['estimate']['id']) {
                            $modelBookingEstimate->updateFullTextSearch($booking['estimate']['customer_id']);
                        }

                        if (isset($booking['complaint']) && $booking['complaint']) {
                            foreach ($booking['complaint'] as $complaint) {
                                $modelComplaint->updateFullTextSearch($complaint['complaint_id']);
                            }
                        }
                    }
                }
            }

            if (isset($dataToUpdate['booking_estimate']) && $dataToUpdate['booking_estimate']) {
                foreach ($dataToUpdate['booking_estimate'] as $estimateToUpdate) {

                    $bookingEstimate = $modelBookingEstimate->getById($estimateToUpdate['type_id']);
                    if ($bookingEstimate) {
                        $modelBookingEstimate->updateFullTextSearch($estimateToUpdate['type_id']);

                        $booking = $modelBooking->getById($bookingEstimate['booking_id']);
                        if ($booking) {
                            $modelBooking->fill($booking, array('invoice', 'estimate', 'complaint'));

                            $modelBooking->updateFullTextSearch($booking['booking_id']);
                            $modelCustomer->updateFullTextSearch($booking['customer_id']);

                            if ($booking['original_inquiry_id']) {
                                $modelInquiry->updateFullTextSearch($booking['original_inquiry_id']);
                            }

                            if (isset($booking['invoice']['id']) && $booking['invoice']['id']) {
                                $modelBookingInvoice->updateFullTextSearch($booking['invoice']['id']);
                            }

                            if (isset($booking['complaint']) && $booking['complaint']) {
                                foreach ($booking['complaint'] as $complaint) {
                                    $modelComplaint->updateFullTextSearch($complaint['complaint_id']);
                                }
                            }
                        }
                    }
                }
            }

            if (isset($dataToUpdate['booking_invoice']) && $dataToUpdate['booking_invoice']) {
                foreach ($dataToUpdate['booking_invoice'] as $invoiceToUpdate) {

                    $bookingInvoice = $modelBookingInvoice->getById($invoiceToUpdate['type_id']);
                    if ($bookingInvoice) {
                        $modelBookingInvoice->updateFullTextSearch($invoiceToUpdate['type_id']);

                        $booking = $modelBooking->getById($bookingInvoice['booking_id']);
                        if ($booking) {
                            $modelBooking->fill($booking, array('invoice', 'estimate', 'complaint'));

                            $modelBooking->updateFullTextSearch($booking['booking_id']);
                            $modelCustomer->updateFullTextSearch($booking['customer_id']);

                            if ($booking['original_inquiry_id']) {
                                $modelInquiry->updateFullTextSearch($booking['original_inquiry_id']);
                            }

                            if (isset($booking['estimate']['id']) && $booking['estimate']['id']) {
                                $modelBookingEstimate->updateFullTextSearch($booking['estimate']['customer_id']);
                            }

                            if (isset($booking['complaint']) && $booking['complaint']) {
                                foreach ($booking['complaint'] as $complaint) {
                                    $modelComplaint->updateFullTextSearch($complaint['complaint_id']);
                                }
                            }
                        }
                    }
                }
            }

            if (isset($dataToUpdate['complaint']) && $dataToUpdate['complaint']) {
                foreach ($dataToUpdate['complaint'] as $complaintToUpdate) {

                    $complaint = $modelComplaint->getById($complaintToUpdate['type_id']);
                    if ($complaint) {
                        $modelComplaint->updateFullTextSearch($complaintToUpdate['type_id']);

                        $booking = $modelBooking->getById($complaint['booking_id']);
                        if ($booking) {
                            $modelBooking->fill($booking, array('invoice', 'estimate'));

                            $modelBooking->updateFullTextSearch($booking['booking_id']);
                            $modelCustomer->updateFullTextSearch($booking['customer_id']);

                            if ($booking['original_inquiry_id']) {
                                $modelInquiry->updateFullTextSearch($booking['original_inquiry_id']);
                            }

                            if (isset($booking['invoice']['id']) && $booking['invoice']['id']) {
                                $modelBookingInvoice->updateFullTextSearch($booking['invoice']['id']);
                            }

                            if (isset($booking['estimate']['id']) && $booking['estimate']['id']) {
                                $modelBookingEstimate->updateFullTextSearch($booking['estimate']['customer_id']);
                            }
                        }
                    }
                }
            }

            if (isset($dataToUpdate['inquiry']) && $dataToUpdate['inquiry']) {
                foreach ($dataToUpdate['inquiry'] as $inquiryToUpdate) {
                    $inquiry = $modelInquiry->getById($inquiryToUpdate['type_id']);
                    if ($inquiry) {
                        $modelInquiry->updateFullTextSearch($inquiryToUpdate['type_id']);

                        $selectBooking = $this->getAdapter()->select();
                        $selectBooking->from('booking');
                        $selectBooking->where("original_inquiry_id = {$inquiry['inquiry_id']}");
                        $selectBooking->where('is_deleted = 0');
                        $bookings = $this->getAdapter()->fetchAll($selectBooking);

                        if ($bookings) {
                            $modelBooking->fills($bookings, array('invoice', 'estimate', 'complaint'));

                            foreach ($bookings as $booking) {
                                $modelBooking->updateFullTextSearch($booking['booking_id']);
                                $modelCustomer->updateFullTextSearch($booking['customer_id']);

                                if (isset($booking['invoice']['id']) && $booking['invoice']['id']) {
                                    $modelBookingInvoice->updateFullTextSearch($booking['invoice']['id']);
                                }

                                if (isset($booking['estimate']['id']) && $booking['estimate']['id']) {
                                    $modelBookingEstimate->updateFullTextSearch($booking['estimate']['customer_id']);
                                }

                                if (isset($booking['complaint']) && $booking['complaint']) {
                                    foreach ($booking['complaint'] as $complaint) {
                                        $modelComplaint->updateFullTextSearch($complaint['complaint_id']);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (isset($dataToUpdate['customer']) && $dataToUpdate['customer']) {
                foreach ($dataToUpdate['customer'] as $customerToUpdate) {
                    $customer = $modelCustomer->getById($customerToUpdate['type_id']);
                    if ($customer) {
                        $modelCustomer->updateFullTextSearch($customerToUpdate['type_id']);

                        $selectBooking = $this->getAdapter()->select();
                        $selectBooking->from('booking');
                        $selectBooking->where("customer_id = {$customer['customer_id']}");
                        $selectBooking->where('is_deleted = 0');
                        $bookings = $this->getAdapter()->fetchAll($selectBooking);
                        if ($bookings) {
                            $modelBooking->fills($bookings, array('invoice', 'estimate', 'complaint'));

                            foreach ($bookings as $booking) {
                                $modelBooking->updateFullTextSearch($booking['booking_id']);

                                if ($booking['original_inquiry_id']) {
                                    $modelInquiry->updateFullTextSearch($booking['original_inquiry_id']);
                                }

                                if (isset($booking['invoice']['id']) && $booking['invoice']['id']) {
                                    $modelBookingInvoice->updateFullTextSearch($booking['invoice']['id']);
                                }

                                if (isset($booking['estimate']['id']) && $booking['estimate']['id']) {
                                    $modelBookingEstimate->updateFullTextSearch($booking['estimate']['customer_id']);
                                }

                                if (isset($booking['complaint']) && $booking['complaint']) {
                                    foreach ($booking['complaint'] as $complaint) {
                                        $modelComplaint->updateFullTextSearch($complaint['complaint_id']);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

}
