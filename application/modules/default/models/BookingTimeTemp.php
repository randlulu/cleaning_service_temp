<?php

class Model_BookingTimeTemp extends Zend_Db_Table_Abstract {

    protected $_name = 'booking_time_temp';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
	 
  public function getAll($order = null, &$pager = null , $filters = array() , $limit = 0, $perPage = 0, $currentPage = 0) {

		$select = $this->getAdapter()->select();
        $select->from(array('btt' => $this->_name));
        $select->order($order);

	
        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }elseif ($limit) {
		    $select->limit($limit);
        } elseif ($perPage && $currentPage) {
            $select->limit($perPage, ($currentPage -1 ) * $perPage);
        }


        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "booking_time_id = '{$id}'");
    }

    /**
     * delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean 
     */
	 
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("booking_time_id = '{$id}'");
    }
	


    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_time_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }
	
	
	public function getByBookingId($bookingId){
	    $bookingId = (int) $bookingId;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_id = '{$bookingId}'");

        return $this->getAdapter()->fetchRow($select);
	}

}