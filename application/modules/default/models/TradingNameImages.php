<?php

class Model_TradingNameImages extends Zend_Db_Table_Abstract {

    protected $_name = 'tradingName_EmailTemplate_images';

    public function insert(array $data) {

        $id = parent::insert($data);

        return $id;
    }

    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from(array('tn' => $this->_name));

        $select->order($order);

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id = '{$id}'");
        return $this->getAdapter()->fetchRow($select);
    }

    public function getByTradingnameIdandTemplateId($trading_name_id,$template_name_id){
        $trading_name_id = (int) $trading_name_id;
        $template_name_id = (int) $template_name_id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("trading_name_id = '{$trading_name_id}'");
        $select->where("template_id = '{$template_name_id}'");
        return $this->getAdapter()->fetchAll($select);
    }

    public function updateById($id, $data) {
		
        $id = (int) $id;
        return parent::update($data, "id = '{$id}'");
    }
	


     public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("id = '{$id}'");
    }

}


