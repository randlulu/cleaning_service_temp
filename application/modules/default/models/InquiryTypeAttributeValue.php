<?php

class Model_InquiryTypeAttributeValue extends Zend_Db_Table_Abstract {

    protected $_name = 'inquiry_type_attribute_value';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from(array('ita' => $this->_name));
        $select->joinInner(array('it' => 'inquiry_type'), 'ita.inquiry_type_id=it.inquiry_type_id', array('it.inquiry_name'));
        $select->joinInner(array('a' => 'attribute'), 'ita.attribute_id=a.attribute_id', array('a.attribute_name'));
        $select->order($order);

        if ($filters) {
            if (!empty($filters['inquiry_type_id'])) {
                $select->where("it.inquiry_type_id = {$filters['inquiry_type_id']}");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table rows according to the assigned id and date
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "inquiry_attribute_value_id = '{$id}'");
    }

    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("inquiry_attribute_value_id = '{$id}'");
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("inquiry_attribute_value_id  = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the Inquiry id
     * 
     * @param int $id
     * @return array 
     */
    public function getByInquiryId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("inquiry_id = '{$id}'");

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * get By InquiryId And InquiryTypeAttributeId
     * 
     * @param type $inquiryId
     * @param type $inquiryTypeAttributeId
     * @return type row
     */
    public function getByInquiryIdAndInquiryTypeAttributeId($inquiryId, $inquiryTypeAttributeId) {
        $inquiryId = (int) $inquiryId;
        $inquiryTypeAttributeId = (int) $inquiryTypeAttributeId;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("inquiry_id = '{$inquiryId}'");
        $select->where("inquiry_type_attribute_id = '{$inquiryTypeAttributeId}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * setInquiryTypeAttributeValueByInquiryIdAndInquiryTypeId
     * 
     * @param type $inquiryId
     * @param type $inquiryTypeId 
     */
    public function setInquiryTypeAttributeValueByInquiryIdAndInquiryTypeId($inquiryId, $inquiryTypeId) {

        //Get Zend Request
        $request = Zend_Controller_Front::getInstance()->getRequest();

        //
        //getting the inquiry type id to get the atttribute of that type id from the request params
        //
        $modelInquiryTypeAttribute = new Model_InquiryTypeAttribute();
        $inquiryTypeAttributes = $modelInquiryTypeAttribute->getAttributeByInquiryTypeId($inquiryTypeId);
        
        if ($inquiryTypeAttributes) {
            foreach ($inquiryTypeAttributes as $inquiryTypeAttribute) {

                $data = array();
                $value = $request->getParam('attribute_' . $inquiryTypeAttribute['attribute_id'], '');
                //check if it an array to serialize the value
                if (is_array($value)) {
                    $serialized = serialize($value);
                    $data['value'] = $serialized;
                    $data['is_serialized'] = 1;
                } else {
                    $data['value'] = $value;
                }
                $data['inquiry_type_attribute_id'] = $inquiryTypeAttribute['inquiry_type_attribute_id'];
                $data['inquiry_id'] = $inquiryId;

                $InquiryValue = $this->getByInquiryIdAndInquiryTypeAttributeId($inquiryId, $inquiryTypeAttribute['inquiry_type_attribute_id']);
                if ($InquiryValue) {
                    $this->updateById($InquiryValue['inquiry_attribute_value_id'], $data);
                } else {
                    $this->insert($data);
                }
            }
        }
    }

    /**
     * delete table row according to the inquiry_id
     * 
     * @param int $inquiryId
     * @return boolean 
     */
    public function deleteByInquiryId($inquiryId) {
        $inquiryId = (int) $inquiryId;
        return parent::delete("inquiry_id = '{$inquiryId}'");
    }

}