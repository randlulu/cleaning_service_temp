<?php

class Model_ComplaintTemp extends Zend_Db_Table_Abstract {

   protected $_name = 'complaint_temp';
   
    public function getAll($filters = array(), $order = null, &$pager = null , $limit = 0, $perPage = 0, $currentPage = 0) {
        $joinInner = array();
        $select = $this->getAdapter()->select();
        $select->from(array('c' => $this->_name));
        $select->distinct();
        $select->order($order);

        $loggedUser = CheckAuth::getLoggedUser();
        $filters['company_id'] = CheckAuth::getCompanySession();

        $select->joinInner(array('csb' => 'contractor_service_booking'), 'c.booking_id = csb.booking_id', '');

        if (!CheckAuth::checkCredential(array('canSeeAllComplaint'))) {
            if (CheckAuth::checkCredential(array('canSeeOnlyHisComplaint'))) {
                if (CheckAuth::checkCredential(array('canSeeAssignedComplaint'))) {
                    $select->where("c.user_id = " . $loggedUser['user_id'] . " OR csb.contractor_id = {$loggedUser['user_id']}");
                } else {
                    $select->where("c.user_id = " . $loggedUser['user_id']);
                }
            }
        }

        if ($filters) {
            if (!empty($filters['keywords'])) {
                $keywords = $this->getAdapter()->quote('%' . trim($filters['keywords']) . '%');
                
				$joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.booking_id = bok.booking_id', 'cols' => '');
                $select->where("c.full_text_search LIKE {$keywords} OR bok.full_text_search LIKE {$keywords}");

                //$keywords = '+' . preg_replace('#[\s]+#', ' +', trim($filters['keywords']));
                //$keywords = $this->getAdapter()->quote($keywords);
                //$select->where("MATCH(c.full_text_search) AGAINST({$keywords} IN BOOLEAN MODE)");
            }
            if (!empty($filters['complaintType'])) {
                $select->where("c.complaint_type_id = {$filters['complaintType']}");
            }

            if (!empty($filters['complaintStatus']) && $filters['complaintStatus'] != 'all') {
                $select->where("c.complaint_status = '{$filters['complaintStatus']}'");
            }

            if (!empty($filters['company_id'])) {
                $company_id = (int) $filters['company_id'];
                $select->where("c.company_id = {$company_id}");
            }
            if (!empty($filters['complaint_num'])) {
                $complaint_num = $this->getAdapter()->quote('%' . trim($filters['complaint_num']) . '%');
                $select->where("c.complaint_num LIKE {$complaint_num}");
            }
			if (!empty($filters['is_approved'])) {
                $is_approved = (int) $filters['is_approved'];
                $select->where("c.is_approved = {$is_approved}");
            }
            if (!empty($filters['comment'])) {
                $comment = $this->getAdapter()->quote('%' . trim($filters['comment']) . '%');
                $select->where("c.comment LIKE {$comment}");
            }
            if (!empty($filters['booking_num'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.booking_id = bok.booking_id', 'cols' => '');
                $booking_num = $this->getAdapter()->quote('%' . trim($filters['booking_num']) . '%');
                $select->where("bok.booking_num LIKE {$booking_num}");
            }
            if (!empty($filters['customer_id'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.booking_id = bok.booking_id', 'cols' => '');
                $filters['customer_id'] = (int) $filters['customer_id'];
                $select->where("bok.customer_id = {$filters['customer_id']} ");
            }
            if (!empty($filters['customer_name'])) {
                $modelCustomer = new Model_Customer();
                
                $allCustomer = $modelCustomer->getCustomerIdByIdFullNameSearch(trim($filters['customer_name']));
                
                $customerIds = implode(', ', $allCustomer);
                if ($customerIds) {
                    $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.booking_id = bok.booking_id', 'cols' => '');
                    $select->where("bok.customer_id in ( $customerIds )");
                }
            }
            if (!empty($filters['contractor_id'])) {
                $contractor_id = (int) $filters['contractor_id'];
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.booking_id = bok.booking_id', 'cols' => '');
                $select->where("bok.created_by = {$contractor_id} OR csb.contractor_id = {$contractor_id}");
            }
            if (!empty($filters['user_id'])) {
                $user_id = (int) $filters['user_id'];
                $select->where("c.user_id = {$user_id}");
            }
            if (!empty($filters['bussiness_name'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.booking_id = bok.booking_id', 'cols' => '');
                $joinInner['customer_commercial_info'] = array('name' => array('cci' => 'customer_commercial_info'), 'cond' => 'bok.customer_id = cci.customer_id', 'cols' => '');
                $select->where("cci.business_name LIKE '%{$filters['bussiness_name']}%'");
            }
            if (!empty($filters['inquiry_num'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.booking_id = bok.booking_id', 'cols' => '');
                $joinInner['inquiry'] = array('name' => array('i' => 'inquiry'), 'cond' => 'bok.original_inquiry_id = i.inquiry_id', 'cols' => '');
                $inquiry_num = $this->getAdapter()->quote('%' . trim($filters['inquiry_num']) . '%');
                $select->where("i.inquiry_num LIKE {$inquiry_num}");
            }
            if (!empty($filters['estimate_num'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.booking_id = bok.booking_id', 'cols' => '');
                $joinInner['booking_estimate'] = array('name' => array('est' => 'booking_estimate'), 'cond' => 'bok.booking_id = est.booking_id', 'cols' => '');
                $estimate_num = $this->getAdapter()->quote('%' . trim($filters['estimate_num']) . '%');
                $select->where("est.estimate_num LIKE {$estimate_num}");
            }
            if (!empty($filters['mobile/phone'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.booking_id = bok.booking_id', 'cols' => '');
                $joinInner['customer'] = array('name' => array('cu' => 'customer'), 'cond' => 'bok.customer_id = cu.customer_id', 'cols' => '');
                $mobile_phone = trim($filters['mobile/phone']);
                $select->where("CONCAT_WS(' ',phone1 ,phone2 ,phone3,mobile1 ,mobile2 ,mobile3) LIKE '%{$mobile_phone}%'");
            }
            if (!empty($filters['email'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.booking_id = bok.booking_id', 'cols' => '');
                $joinInner['customer'] = array('name' => array('cu' => 'customer'), 'cond' => 'bok.customer_id = cu.customer_id', 'cols' => '');
                $email = trim($filters['email']);
                $select->where("CONCAT_WS(' ',email1 ,email2 ,email3) LIKE '%{$email}%'");
            }
            if (!empty($filters['service_id'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.booking_id = bok.booking_id', 'cols' => '');
                $service_id = (int) $filters['service_id'];
                $select->where("csb.service_id = {$service_id}");
            }
			
			    //added by Mohamed
          if (!empty($filters['to_follow']) && $filters['to_follow'] == true) {
                $select->where("c.is_to_follow = 1");
               
            }

            if (!empty($filters['to_follow_date'])) {
                switch ($filters['to_follow_date']) {
                   
                    case 'today':
                        $today = getTimePeriodByName('today');
                        $select->where("c.to_follow between '" . strtotime($today['start']) . "' and '" . strtotime($today['end']) . "'");
                        break;
                    
                    case 'past':
                        $today = getTimePeriodByName('today');
                        $select->where("c.to_follow < '" . strtotime($today['start']) . "'");
                        break;
                    case 'future':
                        $today = getTimePeriodByName('today');
                        $select->where("c.to_follow > '" . strtotime($today['end']) . "'");
                        break;
                }
            }
	 //added by Mohamed
        }

        if ($joinInner) {
            foreach ($joinInner as $inner) {
                $select->joinInner($inner['name'], $inner['cond'], $inner['cols']);
            }
        }
        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        } elseif ($perPage && $currentPage) {
            $select->limit($perPage, ($currentPage - 1) * $perPage);
        }
		
		//echo $select->__toString();

        return $this->getAdapter()->fetchAll($select);
    }
	
	
	public function getByBookingId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_id = '{$id}'");
        $select->where("is_approved = '0'");
		
        return $this->getAdapter()->fetchAll($select);
    }
	
	
	public function addComplaintTemp($id,$tempdata){
	   
	    $modelComplaint = new Model_Complaint();
        $data = $modelComplaint->getById($id);

        $dbParams = array();
        $dbParams['complaint_id'] = $data['complaint_id'];
        $dbParams['user_id'] = $tempdata['user_id'];
        $dbParams['complaint_type_id'] = $data['complaint_type_id'];
        $dbParams['comment'] = $data['comment'];
        $dbParams['booking_id'] = $data['booking_id'];
        $dbParams['created'] = $data['created'];
        $dbParams['complaint_num'] = $data['complaint_num'];
        $dbParams['complaint_status'] = $tempdata['complaint_status'];
        $dbParams['to_follow'] = $data['to_follow'];
        $dbParams['is_to_follow'] = $data['is_to_follow'];
        $dbParams['company_id'] = $data['company_id'];
        $dbParams['count'] = $data['count'];
        $dbParams['full_text_search'] = $data['full_text_search'];
		$dbParams['is_approved'] = $data['is_approved'];;
		
		return parent::insert($dbParams);
	   
	}
	
	public function getByComplaintId($id, $filters = array()){
	  
	    $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from(array('comp'=>$this->_name));
		$select->joinInner(array('bok' => 'booking'),'bok.booking_id = comp.booking_id',array('pause_emails'));
        if($filters){
			if(!empty($filters['complaintStatus'])){
				$select->where("comp.complaint_status = '{$filters['complaintStatus']}'");
			}
		}
        $select->where("complaint_id = '{$id}'");
        
        return $this->getAdapter()->fetchRow($select);
	
	}
	
	
	 public function updateById($id, $data) {
        $id = (int) $id;
        return  parent::update($data, "id= '{$id}'");	
    }
	
	public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("id= '{$id}'");
    }
	
	public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from(array('comp'=>$this->_name));
		$select->joinInner(array('bok' => 'booking'),'bok.booking_id = comp.booking_id',array('pause_emails'));
        $select->where("comp.id= '{$id}'");
		/*if($filters){
			if(!empty($filters['complaintStatus'])){
				$select->where("comp.complaint_status = '{$filters['complaintStatus']}'");
			}
		}*/

        return $this->getAdapter()->fetchRow($select);
    }
	

	
	
	
	
	
 
}

?>