<?php

class MobileNotification {
    
	public static function notify($booking_id,$case,$settingName="test"){


		
		$model_booking = new Model_Booking();
		$model_Mongo = new Model_Mongo();
		$model_user = new Model_User();
		$modelIosUserNotificationSetting = new Model_IosUserNotificationSetting();		
		$model_contractorServiceBooking = new Model_ContractorServiceBooking();
		$model_bookingAddress = new Model_BookingAddress();
		$model_bookingHistory = new Model_BookingHistory();
		$model_bookingStatus = new Model_BookingStatus();
		$model_complaint = new Model_Complaint();
		$booking = $model_booking->getById($booking_id);
		$booking_address = $model_bookingAddress->getByBookingId($booking_id);
		$booking_user = $model_user->getById($booking['created_by']);
		
		$timeStamp = strtotime($booking['booking_start']);
		$day = date("l" , $timeStamp);
		$time = date("H:i a" , $timeStamp);
		$month_day = date("M d" , $timeStamp);
		
		$created_date = date("M d", $booking['created']);
		$created_time = date("H:i a" , $booking['created']);
		
		$bookingAddress = $booking_address['unit_lot_number'] .' '.$booking_address['street_number'].' , '.$booking_address['street_address'].' '.$booking_address['suburb'].' '.$booking_address['state'];
		//echo "after getting booking";
		if($case == 'new booking'){
			
			$notification_text = 'New Booking has been added to your calendar. 
			 '.$booking['booking_num'].' '.$bookingAddress.' for '.$day.', '.$month_day.' '.$time;
			 
		}
		else if($case == 'booking moved'){
			$booking_history = $model_bookingHistory->getBookingHistoryByBookingIdAndDescription($booking_id , "booking_start");

			$notification_text = $booking['booking_num'].' '.$bookingAddress.' has been moved from '.$booking_history['booking_before'].' to '. $booking_history['booking_after'];
		}
		else if($case == 'delete booking'){
			$notification_text = 'Your booking '.$booking['booking_num'].' has been deleted.';
		}
		//else if($case == 'new complaint'){
		//	$notification_text = 'you have new complaint on booking number '.$booking['booking_num'].', please check it.';
		//}
		else if($case == 'new comment'){
			$notification_text = 'you have new comment on booking number '.$booking['booking_num'].', please check it.';
		}
		else if($case == 'new discussion'){
			$bookingDiscussionObj = new Model_BookingDiscussion();
			$booking_discussion = $bookingDiscussionObj->getLastBookingDiscussionByBookingId($booking_id);
			$notification_text = $booking_user['username'] .' has posted on the discussion board for '.$booking['booking_num'] .' '.$bookingAddress .' for '.$day.', '.$month_day.' '.$time.' " '.$booking_discussion['user_message'].' "' ;
		}else if($case == 'new complaint'){
			//echo 'te '.$status_id;
			$booking_status = $model_bookingStatus->getById($booking['status_id']);
			$complaint = $model_complaint->getLastComplaintByBookingId($booking_id);
			$comp_date_time = date("l" , $complaint['created']) .' '.date("H:i a" , $complaint['created']).' '.date("M d" , $complaint['created']);
			
			
			$notification_text = $booking['booking_num'] .' '.$bookingAddress .' '.$booking_status['name'] .' '.$day.', '.$month_day.' has a complaint placed by '.$complaint['username'] .' on '.$comp_date_time.' " '.$complaint['comment'].' " ';
		}
		
		
		$contractors = $model_contractorServiceBooking->getContractorIdsByBookingId($booking_id);
		
		foreach($contractors as $contractor){			
			///check if the contractor needs to receive notification in this case
			/*$sendNotificationToContractors = $modelIosUserNotificationSetting->getBySettingNameAndUserId($contractor['contractor_id'],$settingName);
			*/
			
			$sendNotificationToContractors = 1;
			if($sendNotificationToContractors){
				
					$doc = array(
					  'contractor_mobile_info_id'=> $contractor['contractor_id'],
					  'contractor_id'=> $contractor['contractor_id'],
					  'contractor_name'=> $contractor['username'],
					  'notification_text' => $notification_text,
					  'send_date' => time(),
					  'notification_user'=> $booking_user['username'],
					  'created' => $created_date .' at '.$created_time,
					  'seen'=> 0,
					  'booking_id'=> $booking_id,
					  'count_status'=> 0 , 
					  'title' => $case
					  );
					
					$model_Mongo->insertNotification($doc);	
				
			}
		}
		
		$model_Mongo->closeConnection();
		return;
	}
    
}
