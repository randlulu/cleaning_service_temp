<?php

class MobileNotification {
    
	public static function notify($booking_id,$case,$settingName="test"){
		echo 'islam test';
		$model_booking = new Model_Booking();
		$model_Mongo = new Model_Mongo();
		$modelIosUserNotificationSetting = new Model_IosUserNotificationSetting();		
		$model_contractorServiceBooking = new Model_ContractorServiceBooking();
		
		$booking = $model_booking->getById($booking_id);
		//echo "after getting booking";
		if($case == 'new booking'){
			$notification_text = 'You have new booking '.$booking['booking_num'].' On '.$booking['booking_start'].' please check it.';
		}
		else if($case == 'updated booking'){
			$notification_text = 'Your booking '.$booking['booking_num'].' has been updated, please check it.';
		}
		else if($case == 'delete booking'){
			$notification_text = 'Your booking '.$booking['booking_num'].' has been deleted.';
		}
		else if($case == 'new complaint'){
			$notification_text = 'you have new complaint on booking number '.$booking['booking_num'].', please check it.';
		}
		else if($case == 'new comment'){
			$notification_text = 'you have new comment on booking number '.$booking['booking_num'].', please check it.';
		}
		
		
		$contractors = $model_contractorServiceBooking->getContractorIdsByBookingId($booking_id);
		
		foreach($contractors as $contractor){
			print_r($contractor);
			///check if the contractor needs to receive notification in this case
			/*$sendNotificationToContractors = $modelIosUserNotificationSetting->getBySettingNameAndUserId($contractor['contractor_id'],$settingName);
			*/
			$sendNotificationToContractors = 1;
			if($sendNotificationToContractors){
				
					$doc = array(
					  'contractor_mobile_info_id'=> $contractor['contractor_id'],
					  'contractor_id'=> $contractor['contractor_id'],
					  'contractor_name'=> 'contractor_name',
					  'notification_text' => $notification_text,
					  'send_date' => time(),
					  'seen'=> 0,
					  'booking_id'=> $booking_id
					  );
					$model_Mongo->insertNotification($doc);	
				
			}
		}
		
		$model_Mongo->closeConnection();
		return;
	}
    
}
