<?php

class Model_PaymentType extends Zend_Db_Table_Abstract {

    protected $_name = 'payment_type';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order($order);

        if (!isset($filters['company_id'])) {
            $filters['company_id'] = CheckAuth::getCompanySession();
        }
        
        if ($filters) {
            if (!empty($filters['keywords'])) {
                $keywords = $this->getAdapter()->quote('%' . $filters['keywords'] . '%');
                $select->where("payment_type LIKE {$keywords}");
            }
            if (!empty($filters['without_cash'])) {
                $select->where("slug != 'cash'");
            }
            if (!empty($filters['company_id'])) {
                $company_id = (int) $filters['company_id'];
                $select->where("company_id = {$company_id} OR is_core = 1");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table rows according to the assigned id and date
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "id = '{$id}'");
    }

    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("id = '{$id}'");
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the slug value
     * 
     * @param int $slyg_value
     * @return one 
     */
    public function getPaymentIdBySlug($slug_value) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name, array('id'));
        $select->where("slug = '{$slug_value}'");

        return $this->getAdapter()->fetchOne($select);
    }

    /**
     * get table row according to the slug value
     * 
     * @param int $slyg_value
     * @return array 
     */
    public function getBySlug($slug_value) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("slug = '{$slug_value}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * create slug according to the slug value = string
     * 
     * @param string $strnig
     * @return array 
     */
    public function createSlug($string) {
        if (strlen($string) > 40) {
            $string = substr($string, 0, 40);
        }
        $slug = preg_replace('#[\:\;\"\'\|\<\,\>\.\?\`\~\!\@\#\$\%\^\&\*\(\)\_\-\+\=\{\[\}\]\/\t\n]#', ' ', $string);
        $slug = preg_replace('#[\s]+#', '-', trim($slug));
        $slug = strtolower($slug);

        $tmp_slug = $slug;
        $counter = 0;
        while ($this->getBySlug($tmp_slug)) {
            $tmp_slug = $slug . '-' . rand(1, 1000000);
            $counter++;
            if ($counter >= 10) {
                return uniqid('pay');
            }
        }
        return $tmp_slug;
    }

    public function getByPaymentTypeAndCompany($paymentType, $companyId = 0) {

        if (!$companyId) {
            $companyId = CheckAuth::getCompanySession();
        }

        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("payment_type = '{$paymentType}'");
        $select->where("company_id = '{$companyId}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table rows as array
     * 
     * @return array
     */
    public function getPaymentTypeAsArray() {
        $select = $this->getAdapter()->select();
        $select->from(array('pt' => $this->_name));
        $select->order('payment_type asc');
        $company_id = CheckAuth::getCompanySession();
        $company_id = isset($company_id)? $company_id : 1;
       
        $select->where("company_id = {$company_id} OR is_core = 1");
     
        
        $paymentTypes = $this->getAdapter()->fetchAll($select);

        $data = array();
        foreach ($paymentTypes as $paymentType) {
            $data[] = array(
                'payment_type_id' => $paymentType['id'],
                'payment_type' => $paymentType['payment_type']
            );
        }
        return $data;
    }

    public function checkBeforeDeletePaymentType($paymentTypeId, &$tables = array()) {

        $sucsess = true;

        $select_payment = $this->getAdapter()->select();
        $select_payment->from('payment', 'COUNT(*)');
        $select_payment->where("payment_type_id = {$paymentTypeId}");
        $count_payment = $this->getAdapter()->fetchOne($select_payment);

        if ($count_payment) {
            $tables[] = 'payment';
            $sucsess = false;
        }

        $select_refund = $this->getAdapter()->select();
        $select_refund->from('refund', 'COUNT(*)');
        $select_refund->where("payment_type_id = {$paymentTypeId}");
        $count_refund = $this->getAdapter()->fetchOne($select_refund);

        if ($count_refund) {
            $tables[] = 'refund';
            $sucsess = false;
        }

        return $sucsess;
    }

}