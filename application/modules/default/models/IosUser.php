<?php

class Model_IosUser extends Zend_Db_Table_Abstract {

    protected $_name = 'ios_user';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order($order);

        $filters['company_id'] = CheckAuth::getCompanySession();

        if ($filters) {
            if (!empty($filters['user_id'])) {
                $select->where("user_id = {$filters['user_id']}");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }


        return $this->getAdapter()->fetchAll($select);
    }
	
	/*public function getAll($filters = array(), $order = null, &$pager = null){
		
		if(isset($filters['booking_id']) && !empty($filters['booking_id']) && $filters['booking_id'] != null ){
			$bookingId = $filters['booking_id'];
		}
		else{
			$bookingId = 0;
		}
		$select2 = $this->getAdapter()->select();
        $select2->from('contractor_service_booking',array('contractor_id'));
		$select2->where("booking_id = {$bookingId}");
		$users = $this->getAdapter()->fetchAll($select2);
		
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
		$select->join('user','user.user_id = ios_user.user_id',array());
		$select->join('auth_role','user.role_id = auth_role.role_id',array());
		
        
		$select->where('user.role_id != 1 or user.user_id in (?)', $users); 
		$select->order($order);
		
        $filters['company_id'] = CheckAuth::getCompanySession();

        if ($filters) {
            if (!empty($filters['user_id'])) {
                $select->where("user_id = {$filters['user_id']}");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }*/

    /**
     * update table rows according to the assigned id and data
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "id = '{$id}'");
    }

    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("id = '{$id}'");
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function getByUserId($userId) {
        $userId = (int) $userId;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("user_id = '{$userId}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function getByUserIdAndUUID($userId, $uuid) {
        $userId = (int) $userId;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("user_id = '{$userId}'");
        $select->where("uuid = '{$uuid}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function afterIosLogin($uuid) {

        $loggedUser = Zend_Auth::getInstance()->getIdentity();
		
        $params = array(
            'last_login' => time()
        );

        $iosLoggedUser = $this->getByUserIdAndUUID($loggedUser->user_id, $uuid);

        if ($iosLoggedUser) {
            $this->updateById($iosLoggedUser['id'], $params);
            return $iosLoggedUser['access_token'];
        } else {
            $params['user_id'] = $loggedUser->user_id;
            $params['uuid'] = $uuid;
			$access_token = $this->getGUID();
            $params['access_token'] = $access_token;
			$this->insert($params);
            return $access_token;
			
        }
    }
	
	public function getGUID(){
		if (function_exists('com_create_guid')){
			return com_create_guid();
		}else{
			mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
			$charid = strtoupper(md5(uniqid(rand(), true)));
			$hyphen = chr(45);// "-"
			$uuid = substr($charid, 0, 8).$hyphen
				.substr($charid, 8, 4).$hyphen
				.substr($charid,12, 4).$hyphen
				.substr($charid,16, 4).$hyphen
				.substr($charid,20,12);
			return $uuid;
		}
	}
	public function getByUserInfoById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from(array('iu' => $this->_name));
		$select->join(array('u' => 'user'), 'iu.user_id = u.user_id');
        
        $select->where("id = {$id}");
        
        return $this->getAdapter()->fetchRow($select);
    }
	public function getByUserInfoByAccessToken($accessToken) {
        
        $select = $this->getAdapter()->select();
        $select->from(array('iu' => $this->_name));
		$select->join(array('u' => 'user'), 'iu.user_id = u.user_id');
        
        $select->where("access_token = '{$accessToken}'");
        //echo 'testttt '.$select->__toString();
        return $this->getAdapter()->fetchRow($select);
    }

	
}
