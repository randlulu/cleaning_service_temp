<?php

class Model_Account extends Zend_Db_Table_Abstract {

    protected $_name = 'account';

    /**  get table row according to the assigned id * */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function getByCompanyId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("company_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**  get table row according to the assigned $created_by id * */
    public function getByCreatedBy($created_by) {
        $id = (int) $created_by;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("created_by = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**  get user row according to the assigned Account id * */
    public function getUserById($created_by) {
        $id = (int) $created_by;
        $select = $this->getAdapter()->select();
        $select->from(array('usr' => 'user'));
        $select->where("usr.user_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function getUsersByCompanyId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from(array('usr' => 'user'));
        $select->joinInner(array('uc' => 'user_company'), 'usr.user_id = uc.user_id', '');
        $select->where("uc.company_id = '{$id}'");

        return $this->getAdapter()->fetchAll($select);
    }

    /**  get company row according to the assigned account id * */
    public function getCompanyById($compny_id) {
        $id = (int) $compny_id;
        $select = $this->getAdapter()->select();
        $select->from(array('cmp' => 'company'));

        $select->where("cmp.company_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**  get plan row according to the assigned account id * */
    public function getPlanById($plan_id) {
        $id = (int) $plan_id;
        $select = $this->getAdapter()->select();
        $select->from(array('pln' => 'plan'));
        $select->where("pln.plan_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /** get all plan information                          * */
//    public function getAllPlan() {
//        $select = $this->getAdapter()->select();
//        $select->from('plan');
//        return $this->getAdapter()->fetchAll($select);
//    }

    //** get all row from account                         */
    public function getAll($filters = array(), $order = null, &$pager = null, $limit = 0, $perPage = 0, $currentPage = 0) {
        $select = $this->getAdapter()->select();
        $select->from(array('acc' => $this->_name));
        $select->order($order);
        $select->distinct();

        if ($order) {
            $select->order($order);
        }

        $wheresAndJoins = $this->getWheresAndJoinsByFilters($filters);
        $joinInner = $wheresAndJoins['joinInner'];
        $wheres = $wheresAndJoins['wheres'];
        $groupBy = $wheresAndJoins['groupBy'];

        if ($wheres) {
            foreach ($wheres as $where) {
                $select->where($where);
            }
        }

        if ($joinInner) {
            foreach ($joinInner as $inner) {
                $select->joinInner($inner['name'], $inner['cond'], $inner['cols']);
            }
        }
        /*         * *Select Group BY **IBM */
        if ($groupBy) {
            foreach ($groupBy as $group) {
                $select->group($group);
            }
        }
        /*         * End Select Group By */

        if ($pager) {
            if (!empty($filters['perPage'])) {
                $select->limit($filters['perPage'], ($pager->currentPage - 1) * $filters['perPage']);
            } else {
                $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            }

            $pager->dbSelect = $select;
        } elseif ($perPage && $currentPage) {
            $select->limit($perPage, ($currentPage - 1) * $perPage);
        } else {
            if (!empty($filters['perPage'])) {
                $select->limit($filters['perPage']);
            }
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /** delete table row according to the assigned id */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("id = '{$id}'");
    }

    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "id = '{$id}'");
    }

    public function getWheresAndJoinsByFilters($filters) {

        $select = $this->getAdapter()->select();
        $select->from(array('acc' => $this->_name));

        $wheres = array();
        $joinInner = array();
        $groupBy = array();

        if ($filters) {

            if (!empty($filters['subscription_status'])) {
                //$joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.booking_id = bok.booking_id', 'cols' => '');
                switch ($filters['subscription_status']) {

                    case 'non-subscriber':
                        $wheres['subscription_status'] = (" acc.account_status = 'Non-subscriber'");
                        break;
                    case 'subscriber-trial':
                        $wheres['subscription_status'] = (" acc.account_status = 'Subscriber-trial'");
                        break;
                    case 'subscriber-payed':
                        $wheres['subscription_status'] = (" acc.account_status = 'Subscriber-paied'");
                        break;
                    case 'canceled':
                        $wheres['subscription_status'] = (" acc.account_status = 'Canceled'");
                        break;
                    case 'suspended':
                        $wheres['subscription_status'] = (" acc.account_status = 'Suspended'");
                        break;
                }
            }
            if (!empty($filters['plan_id'])) {
                $wheres['plan_id'] = (" acc.plan_id = '" . $filters['plan_id'] . "'");
            }
        }

        return array('wheres' => $wheres, 'joinInner' => $joinInner, 'groupBy' => $groupBy);
    }

    public function getCount($filters = array()) {
        $count = $this->getAll($filters);
        return count($count);
    }

    //THIS FUNCTION TO CHECK IF A CUSTOME SUBSCRIPION PAYED OR NOT OR IT IS IN TRIAL MODE........
    public function checkSubscriptionPaymentStatus($account_id) {
        $subscriptionPaymentObj = new Model_SubscriptionPayment();
        $subscriptionPaymentInfo = $subscriptionPaymentObj->getPaymentByAccountID($account_id);
        if (empty($subscriptionPaymentInfo)) {
            return 'Trial';
        } elseif ($subscriptionPaymentInfo['end_date'] > time()) {
            return 'Payed';
        } else {
            return 'Not Payed';
        }
    }

    //THIS FUNCTION CALCULATE THE PROFILE COMPLETNESS AND HELP SALES TO MAKE DECISION
    public function calculateProfileCompleteness($account_id) {

        $accountInfo = $this->getById($account_id);
        $userInfo = $this->getUserById($accountInfo['created_by']);
        $CompanyInfo = $this->getCompanyById($accountInfo['company_id']);
        $usersInfo = $this->getUsersByCompanyId($accountInfo['company_id']);

        $completeness = 0;
        if ($userInfo['username'] && $CompanyInfo['company_name']) {
            $completeness +=25;
        }
        if ($CompanyInfo['company_name'] && $CompanyInfo['company_abn'] && $CompanyInfo['company_postcode'] && $CompanyInfo['company_invoices_email'] && $CompanyInfo['company_accounts_email'] && $CompanyInfo['company_complaints_email'] && $CompanyInfo['company_phone1'] && $CompanyInfo['company_mobile1']) {
            $completeness +=25;
        }
        if ($userInfo['first_name'] && $userInfo['last_name'] && $userInfo['street_number'] && $userInfo['street_address'] && $userInfo['email1'] && $userInfo['mobile1']) {
            $completeness +=25;
        }

        if (count($usersInfo) > 1) {
            $completeness +=25;
        }
        return $completeness;
    }


    public function remindDays($to) {
        //return 15; 
        // it deffirence Now from To  to remind positive days 
        $now = time(); // or your date as well

        $to = strtotime($to);

        //return $to;
        $datediff = $to - $now;
        return floor($datediff / (60 * 60 * 24));
    }

     public function sendEmailNewAccount($username_email, $hash_code) {

        // this for send email account activation 

         $image1 = 'http://'.$_SERVER['SERVER_NAME'] . '/emaillogo.png';
         $image2 = 'http://'.$_SERVER['SERVER_NAME'] . "/bkemail.png";

        $template_params = array(
            '{link_activate}' => 'http://'.$_SERVER['SERVER_NAME'] . '/checkvalidateaccount/' . $username_email . '/' . $hash_code ,
            '{image_1}' => $image1 ,
            '{image_2}' => $image2
        );

        $to= $username_email;
        $modelEmailTemplate = new Model_EmailTemplate();
       
        $emailTemplate = $modelEmailTemplate->getEmailTemplate('Activate_Your_Account', $template_params , 1 ,1);

        $params = array(
            'to' => $to,
            'body' => $emailTemplate['body'],
            'subject' => 'Activate Your Account',
                // 'email' => 'admin@octupospro.com', 
                // 'name' => 'octupospro', 
        );

        try {
            EmailNotification::sendEmail($params, '', array(), array() , 2);
            $sent = 'done';
        } catch (Zend_Mail_Transport_Exception $e) {
            $sent = 'error';
        }
    }


    public function sendEmailInvitation($email, $hash_code , $username) {
             // this for esend email invitation with activation link 
                $to = $email ;
                $image1 = 'http://'.$_SERVER['SERVER_NAME'] . '/emaillogo.png';
                $image2 = 'http://'.$_SERVER['SERVER_NAME'] . "/bkemail.png";
                $href = 'http://'.$_SERVER['SERVER_NAME'] .'/getinvitation/' . $email . '/' . $hash_code;

             

        $template_params = array(
            '{link_activate}' => $href ,
            '{image_1}' => $image1 ,
            '{image_2}' => $image2 ,
            '{username}'  => $username
        );

      //  $to = $email;
        $modelEmailTemplate = new Model_EmailTemplate();
        $emailTemplate = $modelEmailTemplate->getEmailTemplate('send_invitation', $template_params , 1 ,1);//1 is core email template for octopos

        $params = array(
            'to' => $to,
            'body' => $emailTemplate['body'],
            'subject' => 'Activate Your Account',
                // 'email' => 'admin@octupospro.com', 
                // 'name' => 'octupospro', 
        );


     //   var_dump($params) ; exit;

        try {
            EmailNotification::sendEmail($params, '', array(), array() , 2);
            $sent = 'done';
        } catch (Zend_Mail_Transport_Exception $e) {
            $sent = 'error';
        }
    }
    
    public function findAccountThatNeedRenew(){
       
        $select = $this->getAdapter()->select();
        $select->from(array('acc' => $this->_name));
        $select->where("(acc.to >= now() - INTERVAL 1 DAY AND acc.to < now() AND acc.customer_token_id IS NOT NULL) OR (acc.trial_end_date >= now() - INTERVAL 1 DAY AND acc.trial_end_date < now() AND acc.customer_token_id IS NOT NULL)");        
        
        return $this->getAdapter()->fetchAll($select);
    }


       public function checkAccountIfhaveRequestBefor($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from(array('acc' => $this->_name));
        $select->where("acc.id = '{$id}'");
        $select->where("acc.phone_requested = '1'");
        $retval =  $this->getAdapter()->fetchRow($select);
        if(empty($retval)){
           return false; 
        }else{
           return true;
        }
    }



}
