<?php

class Model_InvoiceLabel extends Zend_Db_Table_Abstract {

    protected $_name = 'invoice_label';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order($order);




        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }


        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table rows according to the assigned id and data
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        
        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'changed');
        
        return parent::update($data, "id = '{$id}'");
    }

    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        
        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'deleted');
        
        return parent::delete("id = '{$id}'");
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function deleteByInvoicIdAndLabelIds($id, $label_ids) {

        $id = (int) $id;
        $sql = "invoice_id = '{$id}'";
        if ($label_ids) {
            $sql .= ' AND label_id NOT IN (' . implode(', ', $label_ids) . ')';
        }
        return parent::delete($sql);
    }

    public function setLabelsToInvoice($invoice_id, $label_ids) {
        //
        // delete products not in the list
        //
        $invoice_id = (int) $invoice_id;
        if (!empty($label_ids)) {
            foreach ($label_ids AS &$label_id) {
                $label_id = (int) $label_id;
            }
        }

        $this->deleteByInvoicIdAndLabelIds($invoice_id, $label_ids);
        //
        // add the new products
        //
        foreach ($label_ids AS $id) {
            $params = array(
                'invoice_id' => $invoice_id,
                'label_id' => $id
            );

            $this->assignLabelToInvoice($params);
        }
    }

    public function assignLabelToInvoice($params) {

        $invoiceLabelLink = $this->getByInvoiceAndLabelId($params['invoice_id'], $params['label_id']);
		$add_edit_id='';
		$invoiceLabelid='';
        if (!$invoiceLabelLink) {
			 $add_edit_id=$this->insert($params);
			$invoiceLabelid=$add_edit_id;
        } else {
			$add_edit_id=$this->updateById($invoiceLabelLink['id'], $params);
            $invoiceLabelid= $invoiceLabelLink['id'];
        }
		//D.A 30/09/2015 Remove Invoice Cache
		if($add_edit_id){										
					require_once 'Zend/Cache.php';
					$company_id = CheckAuth::getCompanySession();
					$invoiceParamsCacheID= $params['invoice_id'].'_invoiceParams';			
					$invoiceViewDir=get_config('cache').'/'.'invoicesView'.'/'.$company_id;	
					if (!is_dir($invoiceViewDir)) {
						mkdir($invoiceViewDir, 0777, true);
					}			
					$invoiceFrontEndOption= array('lifetime'=> NULL,
					'automatic_serialization'=> true);
					$invoiceBackendOptions = array('cache_dir'=>$invoiceViewDir );
					$invoiceCache = Zend_Cache::factory('Core','File',$invoiceFrontEndOption,$invoiceBackendOptions);			
					$invoiceCache->remove($invoiceParamsCacheID);			
			}
						
		 return $invoiceLabelid;
    }

    public function getByInvoiceAndLabelId($id, $label_id) {
        $id = (int) $id;
        $label_id = (int) $label_id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("invoice_id = '{$id}' AND label_id = '{$label_id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function getByInvoiceId($id) {

        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("invoice_id = '{$id}'");

        return $this->getAdapter()->fetchAll($select);
    }

    public function insert(array $data) {

        $id = parent::insert($data);
        
        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'added');
        
        return $id;
    }
}
