<?php

class Model_Pager {

    public $dbSelect;
    public $currentPage;
    public $perPage;
    public $url;
    public $pageLabel;
    public $pageRange = 10;
    public $pageFirst = '&laquo; First';
    public $pageLast = 'Last &raquo;';

    /**
     * create a pager for the requested data
     * 
     * @param from DB state.
     * @return array 
     */
    public function getPager() {
        try {
            if (!$this->dbSelect) {
                return;
            }
            $pager = new Zend_Paginator(new Model_PagerDB($this->dbSelect));
            // set page number from request
            $pager->setCurrentPageNumber($this->currentPage);

            // set number of items per page from request
            $pager->setItemCountPerPage($this->perPage);

            // set number of pages in page range
            $pager->setPageRange($this->pageRange);

            // get page data
            $pages = $pager->getPages();

            // build first page link
            $pageLinks = array();
            $pageLinks[] = self::getLink($pages->first, $this->url, $this->pageFirst);

            // build previous page link
            if (!empty($pages->previous)) {
                $pageLinks[] = self::getLink($pages->previous, $this->url, '&lt;');
            }

            // build page number links
            foreach ($pages->pagesInRange as $x) {
                if ($x == $pages->current) {
                    //$pageLinks[] = '<span class="current">' . $x . '</span>';
					$pageLinks[]='<span style="background-color:#CCC; color:#FFF; >' . self::getLink($x, $this->url, $x) . '</span>';
                } else {
                    $pageLinks[] = self::getLink($x, $this->url, $x);
                }
            }

            // build next page link
            if (!empty($pages->next)) {
                $pageLinks[] = self::getLink($pages->next, $this->url, '&gt;');
            }

            // build last page link
            $pageLinks[] = self::getLink($pages->last, $this->url, $this->pageLast);
        } catch (Exception $e) {
            die('ERROR: ' . $e->getMessage());
        }

        return $pageLinks;
    }

    /**
     * get link according to the page and url and label
     * 
     * @param int $page
     * @param string $url
     * @param string $label
     * @return string 
     */
    private function getLink($page, $url, $label) {

        $pageLabel = $this->pageLabel ? $this->pageLabel : 'page';

        $newUrl = $this->handleQueryStringParams($url, array($pageLabel => $page));
        return "<a href=\"{$newUrl}\">{$label}</a>";
    }

    /**
     * create link according to the url and page number (new param)
     * 
     * @param string $url
     * @param array $newParams
     * @return string 
     */
    public function handleQueryStringParams($url, $newParams = array()) {
        $urlParts = explode('?', $url);
        $path = $urlParts[0];
        $queryString = '';
        if (count($urlParts) > 1) {
            unset($urlParts[0]);
            $queryString = implode('?', $urlParts);
        }
        foreach ($newParams as $paramName => $paramValue) {
            if ($queryString) {

                if (strpos($queryString, $paramName) === 0 || strpos($queryString, '&' . $paramName) === 0 || strpos($queryString, '&' . $paramName)) {
                    $queryString = preg_replace(array('#' . $paramName . '=[^&]*#', '#' . $paramName . '[\s]*$#'), "{$paramName}={$paramValue}", $queryString);
                } else {
                    $queryString .= "&{$paramName}={$paramValue}";
                }
            } else {
                $queryString .= "{$paramName}={$paramValue}";
            }
        }
        $newUrl = $path;
        if ($queryString) {
            $newUrl .= '?' . $queryString;
        }
        return $newUrl;
    }

}

