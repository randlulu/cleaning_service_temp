<?php

class Model_WorkingHours extends Zend_Db_Table_Abstract {

    protected $_name = 'working_hours';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order($order);


//        $filters['company_id'] = CheckAuth::getCompanySession();

        if ($filters) {
            if (!empty($filters['contractor_id'])) {
                $select->where("contractor_id = {$filters['contractor_id']}");
            }
			if (!empty($filters['day'])) {
                $select->where("day = '{$filters['day']}'");
            }
            if (!empty($filters['working_hours_id'])) {
                $select->where("working_hours_id = {$filters['working_hours_id']}");
            }
            if (!empty($filters['keywords'])) {
                $keywords = $this->getAdapter()->quote('%' . $filters['keywords'] . '%');
                $select->where("label_name like {$keywords}");
            }
            if (!empty($filters['company_id'])) {
                $company_id = (int) $filters['company_id'];
                $select->where("company_id = {$company_id}");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }
		$select->group("day");
        $select->order("working_hours_id ASC");

        return $this->getAdapter()->fetchAll($select);
    }

    public function getAllLabelAsArray($filters = array()) {

        $all_label = $this->getAll($filters, 'label_name asc');

        $data = array();
        foreach ($all_label as $label) {
            $data[$label['id']] = $label['label_name'];
        }
        return $data;
    }

    /**
     * update table rows according to the assigned id and data
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "working_hours_id = '{$id}'");
    }

    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteByContractorId($contractor_id) {
        $contractor_id = (int) $contractor_id;
        return parent::delete("contractor_id = '{$contractor_id}'");
    }

    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("working_hours_id = '{$id}'");
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("working_hours_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function getDayByContractorsId($contractor_id) {
        $contractor_id = (int) $contractor_id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name, 'day');
        $select->where("contractor_id = {$contractor_id}");
        $days = $this->getAdapter()->fetchAll($select);
        $newDays = array();
        foreach ($days as $key => $day) {
            $newDays[] = $day['day'];
        }
        return $newDays;
    }
	

}
