<?php
class Model_TwilioAccountInfo extends Zend_Db_Table_Abstract {
	
	protected $_name = 'twilio_account_info';
	public function getAll($filters = array(), $order = null, &$pager = null, $limit = 0) {
        $select = $this->getAdapter()->select();
        $select->from(array('twl' => $this->_name));
		if ($order) {
            $select->order($order);
        }
		  if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        } elseif ($limit) {
            $select->limit($limit);
        } elseif ($perPage && $currentPage) {
            $select->limit($perPage, ($currentPage - 1) * $perPage);
        }

       

        return $this->getAdapter()->fetchAll($select);
    }
    /**
     * update table rows according to the assigned id and data
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
		
        return parent::update($data, "id = '{$id}'");
    }

    public function insert($data) {
        return parent::insert($data);
    }
    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("id = '{$id}'");
    }
    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id = '{$id}'");
        return $this->getAdapter()->fetchRow($select);
    }

    public function getBycompanyId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("company_id = '{$id}'");
        return $this->getAdapter()->fetchRow($select);
    }



	
		
		}