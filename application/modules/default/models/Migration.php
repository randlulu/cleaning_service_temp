<?php

class Model_Migration extends Zend_Db {

    private $db;

    public function __construct() {

        $config = new Zend_Config(
                        array(
                            'database' => array(
                                'adapter' => 'Mysqli',
                                'params' => array(
                                    'host' => '127.0.0.1',
                                    'username' => 'root',
                                    'password' => 'root',
                                    'dbname' => 'tileclea_tilecleaners'
                                )
                            )
                        )
        );

        $this->db = $this->factory($config->database);
    }

    public function getAll() {
        
        $sql = 'SELECT * FROM enquiries';
        
        return $this->db->fetchAll($sql);
        
    }

}
