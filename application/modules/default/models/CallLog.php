<?php

class Model_CallLog extends Zend_Db_Table_Abstract {

    protected $_name = 'call_log';
    private $modelUser = array();

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null, $limit = 0, $perPage = 0, $currentPage = 0) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order($order);
      
        if ($filters) {
            if (!empty($filters['user_id'])) {
                $user_id = $this->getAdapter()->quote($filters['user_id']);
                $select->where("user_id = {$user_id}");
            }
            if (!empty($filters['contractor_id'])) {
                $contractor_id = $filters['contractor_id'];
                $select->where("caller_id = {$contractor_id} or callee_id = {$contractor_id}");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        } else if ($limit) {
            $select->limit($limit);
        } else if ($perPage && $currentPage) {
            $select->limit($perPage, ($currentPage - 1) * $perPage);
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "call_log_id = '{$id}'");
    }

    /**
     * delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("call_log_id = '{$id}'");
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("call_log_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get extra info for all the rows
     * @param array $row
     * @param array $types
     * @return array
     */
//    public function fills(&$rows, $types = array()) {
//        foreach ($rows as &$row) {
//            $this->fill($row, $types);
//        }
//    }

    /**
     * get extra info for each row (Ex: getting the category row by c_id, author row by author id ... etc)
     * 
     * @param array $row
     * @param array $types
     * @return array
     */
//    public function fill(&$row, $types = array()) {
//        $modelAuthRole = new Model_AuthRole();
//
//        $contractorRoleId = $modelAuthRole->getRoleIdByName('contractor');
//
//        if (in_array('contractor', $types)) {
//            /**
//             * load model
//             */
//            if (!$this->modelUser) {
//                $this->modelUser = new Model_User();
//            }
//            if ($row['callee_user_role'] == $contractorRoleId['role_id']) {
//                $user = $this->modelUser->getById($row['callee_id']);
//            } else {
//                // callee customer 
//                $modelCustomer = new Model_Customer();
//                $user = $modelCustomer->getById($row['callee_id']);
//            }
//            $row['callee'] = $user;
//        }
//        if (in_array('customer', $types)) {
//            /**
//             * load model
//             */
//            if (!$this->modelCustomer) {
//                $this->modelCustomer = new Model_Customer();
//            }
//
//            $user = $this->Model_Customer->getById($row['contractor_id']);
//            $row['contractor'] = $user;
//        }
//    }
    // BY RAND
    public function getByCallSid($id) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("call_sid = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }
	function deleteAllCallRecordesInTwilio()
	{
		$sid = "ACd698f0b22aa408a0865abb86cc97b35d";
        $token = "13359760efe3d9ff0057ebf0df7213dc";
		$client = new Services_Twilio($sid, $token);
		//$today = date('Y-m-d H:i:s -6 months');
		$today = date('Y-m-d H:i:s');
        $dayBefore6Months = strtotime($today.'-6 months');
		$dateBefore6Months = date('Y-m-d H:i:s',$dayBefore6Months);
		//$client->recordings 
		foreach ($client->account->recordings->getIterator(0, 50, array('DateCreated<' => $dateBefore6Months)) as $recording) {
		$row = $this->getByCallSid($recording->sid);
		$data = array('call_sid'=>'');
		$this->updateById($row['call_log_id'],$data);
		$client->account->recordings->delete($recording->sid);
		
		}
	}
	//RAND
}
