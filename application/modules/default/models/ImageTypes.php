<?php

class Model_ImageTypes extends Zend_Db_Table_Abstract {

    protected $_name = 'image_types';
   
    public function init() {
        parent::init();
        $this->loggedUser = CheckAuth::getLoggedUser();
    }
	
	public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order($order);
		


        if (!isset($filters['company_id'])) {
            $filters['company_id'] = CheckAuth::getCompanySession();
        }

        if ($filters) {
            if (!empty($filters['keywords'])) {
                $keywords = $this->getAdapter()->quote('%' . $filters['keywords'] . '%');
                $select->where("name LIKE {$keywords}");
            }

            if (!empty($filters['company_id'])) {
                $select->where("company_id = {$filters['company_id']} OR company_id = 0");
            }
			
			if(!empty($filters['by_name'])){
                 $select->where("name = '{$filters['by_name']}'");
            }

        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }
		
		//$sql = $select->__toString();
		//echo $sql;
        return $this->getAdapter()->fetchAll($select);
    }
	
	
	 public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("image_types_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }
	
	 public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("image_types_id = '{$id}'");
    }
	
	
	public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "image_types_id = '{$id}'");
    }
	
	
	 public function checkBeforeDeleteImageTypes($image_type_id, &$tables = array()) {

        $sucsess = true;

        $select_image = $this->getAdapter()->select();
        $select_image->from('image', 'COUNT(*)');
        $select_image->where("image_types_id = {$image_type_id}");
        $count_image = $this->getAdapter()->fetchOne($select_image);

        if ($count_image) {
            $tables[] = 'image';
            $sucsess = false;
        }

        return $sucsess;
    }
	
	public function getByStatusNameAndCompany($name, $companyId = 0) {

        if (!$companyId) {
            $companyId = CheckAuth::getCompanySession();
        }

        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("name = '{$name}'");
        $select->where("company_id = '{$companyId}'");



        return $this->getAdapter()->fetchRow($select);
    }

	
	public function getAllTypesAsArray($filters = array()) {

        $all_types = $this->getAll($filters, 'name asc');

        $data = array();
        foreach ($all_types as $type) {
            $data[$type['image_types_id']] = $type['name'];
        }
        return $data;
    }

  
}