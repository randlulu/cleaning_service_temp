<?php

class Model_Countries extends Zend_Db_Table_Abstract {

    protected $_name = 'country';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order($order);

        if ($filters) {
            if (!empty($filters['keywords'])) {
                $keywords = $this->getAdapter()->quote('%' . $filters['keywords'] . '%');
                $select->where("country_name LIKE {$keywords}");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table rows according to the assigned id and data
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "country_id= '{$id}'");
    }

    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("country_id= '{$id}'");
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("country_id= '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get the countries as array
     *
     * @return array 
     */
    public function getCountriesAsArray() {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order('country_name asc');

        $countries = $this->getAdapter()->fetchAll($select);

        $data = array();
        foreach ($countries as $country) {
            $data[] = array(
                'id' => $country['country_id'],
                'name' => $country['country_name']
            );
        }
        return $data;
    }

    /**
     * get all countries
     *
     * @return array
     */
    public function getAllCountries() {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order('country_name asc');

        $countries = $this->getAdapter()->fetchAll($select);

        $data = array();
        foreach ($countries as $country) {
            $data[$country['country_id']] = $country['country_name'];
        }
        return $data;
    }

    public function getByname($name) {

        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("country_name = '{$name}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function checkBeforeDeleteCountry($countryId, &$tables = array()) {

        $sucsess = true;

        $select_city = $this->getAdapter()->select();
        $select_city->from('city', 'COUNT(*)');
        $select_city->where("country_id = {$countryId}");
        $count_city = $this->getAdapter()->fetchOne($select_city);

        if ($count_city) {
            $tables[] = 'city';
            $sucsess = false;
        }

        return $sucsess;
    }

}