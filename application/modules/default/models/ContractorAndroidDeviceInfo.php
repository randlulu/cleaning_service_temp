<?php

class Model_ContractorAndroidDeviceInfo extends Zend_Db_Table_Abstract {

    protected $_name = 'Contractor_android_device_info';
    
  
	public function insert(array $data) {
		$id = 0;
        $id = parent::insert($data);
		
        
        return $id;
    }

	
	public function getByContractorId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from(array('cadi' => $this->_name));
        $select->where("cadi.contractor_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }
	

}