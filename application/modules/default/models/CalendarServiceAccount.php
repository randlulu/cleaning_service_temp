<?php

class Model_CalendarServiceAccount {
		protected $_client;
		protected $_cal;
		
		public function __construct($contractorId , $contractorEmail = '') {
            
            $company_id = CheckAuth::getCompanySession();
            $api_name = 'google-api-php-client';

            $model_api_parameters = new Model_ApiParameters();

            $parameter_name = 'application name';
            $parameter = $model_api_parameters->getParameterValue($company_id, $api_name, $parameter_name);
            $application_name = $parameter['parameter_value'];
        
            $parameter_name = 'client id';
            $parameter = $model_api_parameters->getParameterValue($company_id, $api_name, $parameter_name);
            $client_id = $parameter['parameter_value'];
            
            $parameter_name = 'service account name';
            $parameter = $model_api_parameters->getParameterValue($company_id, $api_name, $parameter_name);
            $service_account_name = $parameter['parameter_value'];
            
            $parameter_name = 'key file';
            $parameter = $model_api_parameters->getParameterValue($company_id, $api_name, $parameter_name);
            $key_file = $parameter['parameter_value'];
            
		//set_include_path(get_include_path() . PATH_SEPARATOR . '/vol01/var/www/cleaning_service_new/library/google-api-php-client-master/src');
		
		//require_once '/vol01/var/www/cleaning_service_new/library/google-api-php-client-master/src/Google/Client.php';
		//require_once '/vol01/var/www/cleaning_service_new/library/google-api-php-client-master/src/Google/Service/Calendar.php';
			$modelContractorGmailAccounts = new Model_ContractorGmailAccounts();

			// get the gmail account contractor
			if(isset($contractorId) && $contractorId != 0){
			   $contractorAccount = $modelContractorGmailAccounts->getByContractorId($contractorId);
			   $email = isset($contractorAccount['email']) ? $contractorAccount['email'] : '';
			}else if(isset($contractorEmail) && !empty($contractorEmail)){
			    $email = $contractorEmail;
			}
			///by islam to stop process if the user has no gmail account
			if(empty($contractorAccount)){
				$this->_cal = null;
				$this->_client = null;
				return null;
				
			}
            
			
			if (!defined('CLIENT_ID')){						
			//define('CLIENT_ID','233716746954-nsgmg6pn2dajp1flkrbsuj598jbvhck0.apps.googleusercontent.com');
                define('CLIENT_ID',$client_id);
			}
			if (!defined('SERVICE_ACCOUNT_NAME')){
			//define('SERVICE_ACCOUNT_NAME','233716746954-nsgmg6pn2dajp1flkrbsuj598jbvhck0@developer.gserviceaccount.com');
                define('SERVICE_ACCOUNT_NAME', $service_account_name);
			}
			if (!defined('KEY_FILE')){
			//define('KEY_FILE',APPLICATION_PATH.'/Tile Cleaners-83d152f9fa40.p12');
                define('KEY_FILE',APPLICATION_PATH.'/'.$key_file);
			}
            
            if (!defined('APPLICATION_NAME')){
            //define('APPLICATION_NAME', 'Tile Cleaners App');
                define('APPLICATION_NAME', $application_name);
            }
			

			$client = new Google_Client();
			$client->setApplicationName(APPLICATION_NAME);
			//$client->setUseObjects(true); //IF USING SERVICE ACCOUNT (YES)

			 //old

            
			$key = file_get_contents(KEY_FILE);
			//print_r($key);
			$client->setClientId(CLIENT_ID);

           
            
            
			$cred = new Google_Auth_AssertionCredentials(SERVICE_ACCOUNT_NAME,
			array('https://www.googleapis.com/auth/calendar'),
			$key);
			
			$cred->sub = $email;
			//$cred->sub = "ahmets@tilecleaners.com.au";
			//$cred->sub = "ayman@tilecleaners.com.au";
			//$cred->sub = "bernief@tilecleaners.com.au";
			$client->setAssertionCredentials($cred);
            //old*/
            
            if (isset($_SESSION['token'])) {
				 $client->setAccessToken($_SESSION['token']);
			}
            
            /*new
            $client->setAuthConfig(SERVICE_ACCOUNT_PATH);

            // OR use environment variables (recommended)

            //putenv('GOOGLE_APPLICATION_CREDENTIALS=SERVICE_ACCOUNT_PATH');
            //$client->useApplicationDefaultCredentials();
            
            //$client->setScopes(Google_Service_Calendar::CALENDAR);
            $client->setScopes(['https://www.googleapis.com/auth/calendar']);
            $client->setSubject($email);
            //new*/
            

			//$client->setClientId(CLIENT_ID);
			$cal = new Google_Service_Calendar($client);
			//print_r($cal);
			
			
			/*$calList = $cal->calendarList->listCalendarList();
			print_r($cal->calendarList, true);
			print "<h1>Calendar List</h1><pre>" . print_r($calList, true) . "</pre>";*/
			
			////put the client and cal in static variable
			$this->_client = $client;
			$this->_cal = $cal;
			
		
		}
		
		public function __get($property) {
			if (property_exists($this, $property)) {
			  return $this->$property;
			}
		}

		public function __set($property, $value) {
			if (property_exists($this, $property)) {
			  $this->$property = $value;
			}
			return $this;
		}
		
		public function getAllEvents($optParam = array()){
			/*$allEvents = $this->_cal->events->listEvents('primary');
			return $allEvents;*/
			if($this->_cal != null){		
				$result= $this->_cal->events->listEvents('primary', $optParam);				
				return $result;		
			}else{
				return array();		
			}
			
		}

		public function insertEvent($event){
		
		  if($this->_cal != null){
			$createdEvent = $this->_cal->events->insert('primary', $event);					
			//echo $createdEvent->getId();
			return $createdEvent->getId();
		  }else{
			return 0;
		  }
			
			
			
		}
		
		public function instances($eventId,$optParam = array()){
			$result= $this->_cal->events->instances('primary', $eventId,$optParam);					
			return $result;		
		}
		
		
		public function getEvent($event_id){
			try {
				$eventOld = $this->_cal->events->get('primary', $event_id);
				
				return $eventOld;
				
				}
 			catch (Exception $e) {
				return array();
			}
			
		}
		
		public function updateEvent($event_id, $new_event){
				if($_SERVER['REMOTE_ADDR'] == '176.67.121.59'){
					var_dump($this->_cal->events->get("primary", $event_id));
					exit;
					
					}
		$eventOld = $this->_cal->events->get("primary", $event_id);
			
			//print_r($eventOld); // bhjdh2u7rtrtvi4aohfc189its
			$seq = $eventOld['sequence'];
			$new_event->setSequence($seq);
			if ($eventOld) {				
				$updatedEvent = $this->_cal->events->update('primary', $event_id, $new_event);
				return $updatedEvent;
			} else {
				return null;
			}	
		}
		
		public function deleteEvent($event_id){
			
			
			$eventOld = $this->getEvent($event_id);
			
			
			if (!empty($eventOld)) {
				if($eventOld['status'] !='cancelled'){
					$deletedEvent = $this->_cal->events->delete('primary', $event_id);
					return $deletedEvent;
				}
				
			} else {
				return null;
			}	
				
		}
	
		public function connect() {
	 
			//$CLIENT_ID ='233716746954-nsgmg6pn2dajp1flkrbsuj598jbvhck0.apps.googleusercontent.com';
			//$SERVICE_ACCOUNT_NAME = '233716746954-nsgmg6pn2dajp1flkrbsuj598jbvhck0@developer.gserviceaccount.com';
			//$KEY_FILE = 'Tile Cleaners-83d152f9fa40.p12';

			$client = new Google_Client();
			$client->setApplicationName("SV Temple APP");
			//$client->setUseObjects(true); //IF USING SERVICE ACCOUNT (YES)

			if (isset($_SESSION['token'])) {
				 $client->setAccessToken($_SESSION['token']);
			}

			$key = file_get_contents(KEY_FILE);
			$client->setClientId(CLIENT_ID);

			$cred = new Google_Auth_AssertionCredentials(SERVICE_ACCOUNT_NAME,
			array('https://www.googleapis.com/auth/calendar'),
			$key);
			
			$cred->sub = "ahmets@tilecleaners.com.au";
			//$cred->sub = "ayman@tilecleaners.com.au";
			//$cred->sub = "bernief@tilecleaners.com.au";
			$client->setAssertionCredentials($cred);

			$client->setClientId(CLIENT_ID);
			$cal = new Google_Service_Calendar($client);
			//print_r($cal);
			
			$calList = $cal->calendarList->listCalendarList();
			//print_r($cal->calendarList, true);
			//print "<h1>Calendar List</h1><pre>" . print_r($calList, true) . "</pre>";


		}
	 
}

?>