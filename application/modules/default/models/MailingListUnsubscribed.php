<?php

class Model_MailingListUnsubscribed extends Zend_Db_Table_Abstract {

    protected $_name = 'mailing_list_unsubscribed';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
	 
	 
	 public function getByMailingListId($mailing_list_id){

        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("mailing_list_id = '{$mailing_list_id}'");
 
        return $this->getAdapter()->fetchAll($select);
	 
	 } 
	 

	  public function getByCronJobIdAndCustomerId($cronjob_id , $customer_id){

	    $cronjob_id = (int) $cronjob_id;
		$customer_id = (int) $customer_id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("cronjob_id = '{$cronjob_id}'");
		$select->where("customer_id = '{$customer_id}'");
 
        return $this->getAdapter()->fetchAll($select);
	 }

	 public function getByEmailTemplateIdAndCustomerId($email_template_id , $customer_id){

	    $cronjob_id = (int) $cronjob_id;
		$customer_id = (int) $customer_id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("email_template_id = '{$email_template_id}'");
		$select->where("customer_id = '{$customer_id}'");
 
        return $this->getAdapter()->fetchAll($select);
	 } 
	 
	 public function getByCustomerIdAndEmailAndMailingList($mailing_list_id , $customer_id , $email){
	    
		$mailing_list_id = (int) $mailing_list_id;
		$customer_id = (int) $customer_id;
		
		$select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("customer_id = '{$customer_id}'");
        $select->where("mailing_list_id = '{$mailing_list_id}'");
        $select->where("customer_email = '{$email}'");
 
        return $this->getAdapter()->fetchAll($select);
	 
	 }

	 public function getByCustomerIdAndEmailAndCronJob($cronjob_id , $customer_id , $email){
	    
		$cronjob_id = (int) $cronjob_id;
		$customer_id = (int) $customer_id;
		
		$select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("customer_id = '{$customer_id}'");
        $select->where("cronjob_id = '{$cronjob_id}'");
        $select->where("customer_email = '{$email}'");
 
        return $this->getAdapter()->fetchAll($select);
	 
	 }

	 public function getByCustomerIdAndEmailAndEmailTemplate($email_template_id , $customer_id , $email){
	    
		$email_template_id = (int) $email_template_id;
		$customer_id = (int) $customer_id;
		
		$select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("customer_id = '{$customer_id}'");
        $select->where("email_template_id = '{$email_template_id}'");
        $select->where("customer_email = '{$email}'");
 
        return $this->getAdapter()->fetchAll($select);
	 
	 }
	 
	 public function getByCustomerId($customer_id){

        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("customer_id = '{$customer_id}'");
 
        return $this->getAdapter()->fetchAll($select);
	 
	 }
    
	 public function getAll($filters = array(), $order = null, &$pager = null) {
        
		$companyId = CheckAuth::getCompanySession();
		$select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order($order);

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }
	
	 public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "mailing_list_unsubscribed_id = '{$id}'");
    }
	


    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("mailing_list_unsubscribed_id = '{$id}'");
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("mailing_list_unsubscribed_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }
	
	
	

}
