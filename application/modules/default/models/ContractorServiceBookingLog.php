
<?php

class Model_ContractorServiceBookingLog extends Zend_Db_Table_Abstract {

    protected $_name = 'contractor_service_booking_log';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
	 
	 public function getByBookingLogId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("log_booking_id = '{$id}'");
        $select->group(array("service_id", "contractor_id"));
        return $this->getAdapter()->fetchAll($select);
    }
    public function getAll($filters = array(), $order = null, &$pager = null) {

        $joinInner = array();
        $select = $this->getAdapter()->select();
        $select->from(array('cb_log' => $this->_name));
        $select->order($order);


        if ($joinInner) {
            foreach ($joinInner as $inner) {
                $select->joinInner($inner['name'], $inner['cond'], $inner['cols']);
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table rows according to the assigned id and data
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "log_id= '{$id}'");
    }

    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("log_id= '{$id}'");
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("log_id= '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function getByBookingId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_id = '{$id}'");
        return $this->getAdapter()->fetchAll($select);
    }

    public function getLastLogByBookingId($bookingId) {

        $selectB = $this->getAdapter()->select();
        $selectB->from($this->_name, array('MAX(log_created)'));
        $selectB->where("booking_id = {$bookingId}");

        $selectA = $this->getAdapter()->select();
        $selectA->from($this->_name);
        $selectA->where("log_created = ({$selectB})");
        $selectA->where("booking_id = {$bookingId}");

        return $this->getAdapter()->fetchRow($selectA);
    }
	
	public function getLastLogByContractorAndBookingId($bookingId) {

        $selectB = $this->getAdapter()->select();
        $selectB->from(array('csbl' => $this->_name), array('MAX(log_created)','contractor_id'));
        $selectB->joinInner(array('u' => 'user'), 'u.user_id = csbl.contractor_id', array('username'));
        $selectB->where("csbl.log_user_id = csbl.contractor_id");
        $selectB->where("csbl.booking_id = {$bookingId}");
		
        return $this->getAdapter()->fetchRow($selectB);
    }

 

    public function addContractorServiceBookingLog($id, $log_user_id = 0,$log_id = 0) {

        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $data = $modelContractorServiceBooking->getById($id);

        $modelBookingLog = new Model_BookingLog();
        
		if($log_id == 0){
		$last = $modelBookingLog->getLastLogByBookingId($data['booking_id']);
        $lastLogBookingId = !empty($last) ? $last['log_id'] : 0;
        $log_id = $lastLogBookingId ; 
		}
		
		
        if (!$log_user_id) {
            $logedUser = CheckAuth::getLoggedUser();
            $log_user_id = !empty($logedUser) ? $logedUser['user_id'] : 0;
        }

        $dbParams = array();
        $dbParams['log_created'] = time();
        $dbParams['log_user_id'] = $log_user_id;
        $dbParams['log_booking_id'] = $log_id;
        $dbParams['id'] = $data['id'];
        $dbParams['booking_id'] = $data['booking_id'];
        $dbParams['contractor_id'] = $data['contractor_id'];
        $dbParams['service_id'] = $data['service_id'];
        $dbParams['clone'] = $data['clone'];
        $dbParams['is_accepted'] = $data['is_accepted'];
        $dbParams['is_rejected'] = $data['is_rejected'];
        $dbParams['is_change'] = $data['is_change'];
        $dbParams['consider_min_price'] = $data['consider_min_price'];

        $idReturn = parent::insert($dbParams);

        return $idReturn;
    }

    public function getLastLogbyBookingIdAndLogBookingId($bookingId, $LogBookingId) {

        $selectB = $this->getAdapter()->select();
        $selectB->from($this->_name, array('MAX(log_Booking_id)'));
        $selectB->where("booking_id = {$bookingId}");
        $selectB->where("log_booking_id <= {$LogBookingId}");

        $selectA = $this->getAdapter()->select();
        $selectA->from($this->_name);
        $selectA->where("log_booking_id = ({$selectB})");

        return $this->getAdapter()->fetchAll($selectA);
    }

    public function getTotalServiceBookingQoute($logServiceId) {


        $bookingService = $this->getById($logServiceId);
        $serviceId = $bookingService['service_id'];
        $considerMinPrice = $bookingService['consider_min_price'];

        $modelServices = new Model_Services();
        $service = $modelServices->getById($serviceId);
        $min_price = $service['min_price'];

        $serviceQoute = $this->getServiceBookingQoute($logServiceId);

        if (($serviceQoute < $min_price) && $considerMinPrice === 1) {
            $serviceQoute = $min_price;
        }

        return $serviceQoute;
    }

    public function getServiceBookingQoute($logServiceId) {

        $bookingService = $this->getById($logServiceId);

        $serviceId = $bookingService['service_id'];
        $clone = $bookingService['clone'];
        $bookingId = $bookingService['booking_id'];
        $logBookingId = $bookingService['log_booking_id'];


        //
        // load models
        //
        $modelAttributeListValue = new Model_AttributeListValue();
        $modelServiceAttribute = new Model_ServiceAttribute();
        $modelServiceAttributeValueLog = new Model_ServiceAttributeValueLog();
        $modelServices = new Model_Services();
        $modelAttributes = new Model_Attributes();

        $uniqid = uniqid();
        $serviceQoute = 0;

        $service = $modelServices->getById($serviceId);
        if (preg_match_all('#\[(.*?)\]#', $service['price_equasion'], $matches)) {
            $param = array();
            foreach ($matches[1] as $attributeVariableName) {
                $attribute = $modelAttributes->getByVariableName($attributeVariableName);

                $serviceAttribute = $modelServiceAttribute->getByAttributeIdAndServiceId($attribute['attribute_id'], $serviceId);

                //$value = $modelServiceAttributeValue->getByBookingIdAndServiceAttributeIdAndClone($bookingId, $serviceAttribute['service_attribute_id'], $clone);
                $value = $modelServiceAttributeValueLog->getServiceAttributeValueLog($bookingId, $serviceAttribute['service_attribute_id'], $clone, $logBookingId);
                if ($attribute['is_list']) {
                    $attributeListValue = $modelAttributeListValue->getById($value['value']);
                    $param[$attributeVariableName] = $attributeListValue['unit_price'];
                } else {
                    $param[$attributeVariableName] = $value['value'];
                }
            }

            $equasion = $service['price_equasion'];

            //build getPrice function
            createPriceEquasionFunction("getPrice_{$uniqid}", $equasion);

            $serviceQoute = call_user_func("getPrice_{$uniqid}", $param);
        }

        return $serviceQoute;
    }

    
    
    public function getTotalBookingQoute($bookingId,$logBookingId) {
        //
        // load models
        //
        $modelServices = new Model_Services();

        //
        //get all service for this booking 
        //
        $bookingServices = $this->getLastLogbyBookingIdAndLogBookingId($bookingId, $logBookingId);

        $totalQoute = 0;

        if ($bookingServices) {
            foreach ($bookingServices as $bookingService) {
                $serviceId = $bookingService['service_id'];
                $considerMinPrice = $bookingService['consider_min_price'];

                $service = $modelServices->getById($serviceId);
                $min_price = $service['min_price'];

                $serviceQoute = $this->getServiceBookingQoute($bookingService['log_id']);

                if (($serviceQoute < $min_price) && $considerMinPrice === 1) {
                    $totalQoute = $totalQoute + $min_price;
                } else {
                    $totalQoute = $totalQoute + $serviceQoute;
                }
            }
        }

        return $totalQoute;
    }
}