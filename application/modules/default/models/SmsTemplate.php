<?php
class Model_SmsTemplate extends Zend_Db_Table_Abstract {

    protected $_name = 'sms_template';
	public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order($order);
        if ($filters) {
            if (!empty($filters['keywords'])) {
                $keywords = $this->getAdapter()->quote('%' . $filters['keywords'] . '%');
                $select->where("name LIKE {$keywords}");
            }
        }
        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }
        return $this->getAdapter()->fetchAll($select);
    }
	
	
	 public function getByName($name) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("name = '{$name}'");
        return $this->getAdapter()->fetchRow($select);
    }

	
    /**
     * update table rows according to the assigned id and data
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "id = '{$id}'");
    }
	public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("id = '{$id}'");
    }
    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
      public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id = '{$id}'");
        return $this->getAdapter()->fetchRow($select);
    }
	
	/////////////////////get by type
	/**
     * get table row according to the id
     * 
     * @param int $type
     * @return array 
     */
      public function getByType($type) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("type = '{$type}'");
		return $this->getAdapter()->fetchAll($select);
    }
	
	
	
		public function getAllByType($type) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        //$select->order($order);
		$select->where("type = '{$type}'");
		return $this->getAdapter()->fetchAll($select);
    }
	
	////////////////////////////////////////////get template/////////////////
	    public function getsmsTemplate($template_id =0, $template_params = array(), $companyId = 0) {
         /*$dateTimeObj= get_settings_date_format();
		$db_format = 0;
		if($dateTimeObj){
		$db_format = 1;
		}else{
		$db_format = 0;
	    }*/
	   
        $placeholder = array();
        $template_images = array();
        foreach ($template_params as $key => $value) {
			/*if($db_format)
			{
				if($key == '{booking_start}')
				{
					$value = getNewDateFormat($value,'all');
				}
				if($key == '{booking_created}')
				{
					$value = getNewDateFormat($value);
				}
				if($key == '{payment_received_date}')
				{
					$value = getNewDateFormat($value);
				}
				if($key == '{refund_received_date}')
				{
					$value = getNewDateFormat($value);
				}
				if($key == '{invoice_created}')
				{
					$value = getNewDateFormat($value);
				}
				if($key == '{booking_end_date}')
				{
					$value = getNewDateFormat($value,'all');
				}
				if($key == '{tomorrow_date}')
				{
					$value = getNewDateFormat($value,'all');
				}
				if($key == '{created}')
				{
					$value = getNewDateFormat($value,'all');
				}
				if($key == '{to_follow}')
				{
					$value = getNewDateFormat($value,'all');
				}
				if($key == '{booking_end}')
				{
					$value = getNewDateFormat($value,'all');
				}
				if($key == '{booking_start_Date}')
				{
					$value = getNewDateFormat($value);
				}
				if($key == '{booking_start_Time}')
				{
					$value = getNewDateFormat($value,'time');
				}
				
				
				
			}*/
            $placeholder[] = $key;
			
        }
        $message = '';
        if ($template_id) {
            $template = $this->getById($template_id);
		     if($template) {
                $message = $template['message'];
            foreach($template_params as $key => $value) {
            $message = html_entity_decode(strip_tags(str_replace($key, $value, $message)));
        }
            }
        }

       return array('message' => $message);
    }
}