<?php

class Model_BookingLabel extends Zend_Db_Table_Abstract {

    protected $_name = 'booking_label';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order($order);




        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }


        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table rows according to the assigned id and data
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        
        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'changed');
        
        return parent::update($data, "id = '{$id}'");
    }

    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        
        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'deleted');
        
        return parent::delete("id = '{$id}'");
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function deleteByBookingIdAndLabelIds($id, $label_ids) {

        $id = (int) $id;
        $sql = "booking_id = '{$id}'";
        if ($label_ids) {
            $sql .= ' AND label_id NOT IN (' . implode(', ', $label_ids) . ')';
        }
        return parent::delete($sql);
    }

    public function setLabelsToBooking($booking_id, $label_ids) {
        //
        // delete products not in the list
        //
        $booking_id = (int) $booking_id;
        if (!empty($label_ids)) {
            foreach ($label_ids AS &$label_id) {
                $label_id = (int) $label_id;
            }
        }

        $this->deleteByBookingIdAndLabelIds($booking_id, $label_ids);
        //
        // add the new products
        //
        foreach ($label_ids AS $id) {
            $params = array(
                'booking_id' => $booking_id,
                'label_id' => $id
            );

            $this->assignLabelToBooking($params);
        }
    }

   /* public function assignLabelToBooking($params) {

        $bookingLabelLink = $this->getByBookingAndLabelId($params['booking_id'], $params['label_id']);

        if (!$bookingLabelLink) {
            return $this->insert($params);
        } else {
            $this->updateById($bookingLabelLink['id'], $params);
            return $bookingLabelLink['id'];
        }
    }
*/

	public function assignLabelToBooking($params) {

        $bookingLabelLink = $this->getByBookingAndLabelId($params['booking_id'], $params['label_id']);
        $add_edit_id='';
		$bookingLabelid='';
        if (!$bookingLabelLink) {
            $add_edit_id=$this->insert($params);
			$bookingLabelid=$add_edit_id;
        } else {
            $add_edit_id=$this->updateById($bookingLabelLink['id'], $params);
            $bookingLabelid= $bookingLabelLink['id'];
        }
		
		//D.A 27/08/2015 Remove Booking Labels Cache
		if($add_edit_id){							
			require_once 'Zend/Cache.php';
			$company_id = CheckAuth::getCompanySession();
			$bookingLabelsCacheID= $params['booking_id'].'_bookingLabels';
			$bookingViewDir=get_config('cache').'/'.'bookingsView'.'/'.$company_id;								
			if (!is_dir($bookingViewDir)) {
				mkdir($bookingViewDir, 0777, true);
			}
			$frontEndOption= array('lifetime'=> NULL,
			'automatic_serialization'=> true);
			$backendOptions = array('cache_dir'=>$bookingViewDir );
			$Cache = Zend_Cache::factory('Core','File',$frontEndOption,$backendOptions);			
			$Cache->remove($bookingLabelsCacheID);			
			}
						
		 return $bookingLabelid;
    }
    public function getByBookingAndLabelId($id, $label_id) {
        $id = (int) $id;
        $label_id = (int) $label_id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_id = '{$id}' AND label_id = '{$label_id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function getByBookingId($id) {

        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_id = '{$id}'");

        return $this->getAdapter()->fetchAll($select);
    }
    
    public function insert(array $data) {

        $id = parent::insert($data);
        
        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'added');
        
        return $id;
    }

}
