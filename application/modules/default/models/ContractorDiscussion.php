<?php

class Model_ContractorDiscussion extends Zend_Db_Table_Abstract {

    protected $_name = 'contractor_discussion';

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateById($id, $data) {
        $id = (int) $id;

        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'changed');

        return parent::update($data, "discussion_id = '{$id}'");
    }
	
	
	public function updateByDifferentCriterias($wheres, $data) {
        return parent::update($data, $wheres);
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("discussion_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get all the recent discussions that the user are able to see
     *  
     * @return array  
     */
    public function getRecentDiscussion($filters = array()) {

        $loggedUser = CheckAuth::getLoggedUser();
        $companyId = CheckAuth::getCompanySession();
        $userId = $loggedUser['user_id'];

        $select = $this->getAdapter()->select();

        $select->distinct();

        $select->from(array('cd' => $this->_name));
        $select->where("bok.company_id = {$companyId}");

        
        $select->order("created DESC");

        $select->limit(20);

        return $this->getAdapter()->fetchall($select);
    }

    /**
     * get table row according to the assigned BookingId
     * 
     * @param int $id
     * @return array
     */
    
	public function getByContractorId($id, $order = 'asc') {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order("created {$order}");
        $select->where("contractor_id = {$id}");

        return $this->getAdapter()->fetchAll($select);
    }
	
	
	
	public function getLastDiscussionByContractorId1($contractor_id){
	    
		$contractor_id = (int) $contractor_id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order("created asc");
        $select->where("contractor_id = {$contractor_id}");
        $select->limit(1);
		
        return $this->getAdapter()->fetchRow($select);
        }
	
	
	public function getLastDiscussionByContractorId($contractor_id){
	    
		$contractor_id = (int) $contractor_id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order("created desc");
        $select->where("contractor_id = {$contractor_id}");
        $select->limit(1);
		
        return $this->getAdapter()->fetchRow($select);
	
	}

    public function insert(array $data) {

        $id = parent::insert($data);

        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'added');

        return $id;
    }

}