<?php

class Model_SubscriptionPayment extends Zend_Db_Table_Abstract {

    protected $_name = 'subscription_payment';

    public function getAll($filters = array(), $order = null, &$pager = null, $limit = 0, $perPage = 0, $currentPage = 0) {
        $select = $this->getAdapter()->select();
        $select->from(array('py' => $this->_name));
        $select->order($order);
        $select->distinct();

        if ($order) {
            $select->order($order);
        }

        $wheresAndJoins = $this->getWheresAndJoinsByFilters($filters);
        $joinInner = $wheresAndJoins['joinInner'];
        $wheres = $wheresAndJoins['wheres'];
        $groupBy = $wheresAndJoins['groupBy'];

        if ($wheres) {
            foreach ($wheres as $where) {
                $select->where($where);
            }
        }

        if ($joinInner) {
            foreach ($joinInner as $inner) {
                $select->joinInner($inner['name'], $inner['cond'], $inner['cols']);
            }
        }

        if ($groupBy) {
            foreach ($groupBy as $group) {
                $select->group($group);
            }
        }


        if ($pager) {
            if (!empty($filters['perPage'])) {
                $select->limit($filters['perPage'], ($pager->currentPage - 1) * $filters['perPage']);
            } else {
                $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            }

            $pager->dbSelect = $select;
        } elseif ($perPage && $currentPage) {
            $select->limit($perPage, ($currentPage - 1) * $perPage);
        } else {
            if (!empty($filters['perPage'])) {
                $select->limit($filters['perPage']);
            }
        }

        return $this->getAdapter()->fetchAll($select);
    }

    public function getWheresAndJoinsByFilters($filters) {

        $select = $this->getAdapter()->select();
        $select->from(array('py' => $this->_name));

        $wheres = array();
        $joinInner = array();
        $groupBy = array();

        if ($filters) {
            
        }

        return array('wheres' => $wheres, 'joinInner' => $joinInner, 'groupBy' => $groupBy);
    }

    // get table row according to the assigned id  
    public function getPaymentByAccountID($account_id) {
        $id = (int) $account_id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order('subscription_payment_id DESC');
        $select->where("account_id = '{$id}'");
        return $this->getAdapter()->fetchRow($select);
    }

    //get all payment accourding to period for specific account_id..
    public function getPaymentByAccountIDAndPeriod($account_id, $from, $to) {
        $id = (int) $account_id;
        $select = $this->getAdapter()->select();
        $select->from(array('py' => $this->_name));
        $select->order('id DESC');
        $select->joinInner(array('acc' => 'account'), 'py.account_id = acc.id', '');
        $select->where("py.account_id = '{$id}'");
        $select->where("py.created between '" . strtotime($from) . "' and '" . strtotime($to) . "'");
        return $this->getAdapter()->fetchRow($select);
    }

    //get payment by ID
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("subscription_payment_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    //this function calculate the total amount that should be paid by user when he want to change his plan.......
    public function calculateTotalAmountChangePlan($newPlanID = 0, $accountEndTrialDate = 0, $toDate = 0, $oldPlanCharge = 0, $nbrOfRegisteredUser = 0, $actualPlanID = 0, $account_positive_amount = 0) {
        //select new plan record.......
        $newPlanObj = new Model_Plan();
        $newPlan = $newPlanObj->getById($newPlanID);
        $total_amount = '';


        //if subscriber paid mode and want to pay for registered to change plan......
        if (((strtotime($accountEndTrialDate) < time())) && (strtotime($toDate) >= time())) {

            $stop_date1 = new DateTime($toDate);
            $to = $stop_date1->format('Y-m-d');

            $difference = strtotime($to) - time();
            if ($difference < 0) {
                $difference = 0;
            }
            $nbrOfDay = floor($difference / 60 / 60 / 24);

            $positive_total_amount = ($nbrOfRegisteredUser * ($nbrOfDay) * $oldPlanCharge) / 30;
            $sub_total_amount_new = $nbrOfRegisteredUser * $newPlan['charge_amount'];
            //$sub_total_amount_old = $nbrOfRegisteredUser * $oldPlanCharge;
            $total_amount = $sub_total_amount_new - $positive_total_amount - $account_positive_amount;

            if ($actualPlanID == $newPlanID) {
                $data = array(
                    'new_plan_id' => $newPlan['plan_id'],
                    'max_users' => $newPlan['max_users'],
                    'name' => $newPlan['name'],
                    'charge_amount' => $newPlan['charge_amount'],
                    'positive_amount' => 0.00,
                    'account_positive_amount' => 0.00,
                    'gst_tax_amount' => 0.00,
                    'amount_without_tax' => 0.00,
                    'amount' => 0.00,
                    'period' => 30 - $nbrOfDay
                );
            } elseif (($actualPlanID != $newPlanID) && ($total_amount > 0)) {
                $data = array(
                    'new_plan_id' => $newPlanID,
                    'max_users' => $newPlan['max_users'],
                    'name' => $newPlan['name'],
                    'charge_amount' => $newPlan['charge_amount'],
                    'positive_amount' => $positive_total_amount,
                    'account_positive_amount' => 0.00,
                    'gst_tax_amount' => 0.1 * $total_amount,
                    'amount_without_tax' => $total_amount,
                    'amount' => $total_amount + (0.1 * $total_amount),
                    'period' => 30 - $nbrOfDay
                );
            } elseif (($actualPlanID != $newPlanID) && ($total_amount < 0)) {
                $data = array(
                    'new_plan_id' => $newPlanID,
                    'max_users' => $newPlan['max_users'],
                    'name' => $newPlan['name'],
                    'charge_amount' => $newPlan['charge_amount'],
                    'positive_amount' => 0.00,
                    'account_positive_amount' => $total_amount * -1,
                    'gst_tax_amount' => (0.1 * $total_amount),
                    'amount_without_tax' => 0.00,
                    'amount' => (0.1 * $total_amount),
                    'period' => 30 - $nbrOfDay
                );
            } elseif (($actualPlanID != $newPlanID) && ($total_amount == 0)) {
                $data = array(
                    'new_plan_id' => $newPlanID,
                    'max_users' => $newPlan['max_users'],
                    'name' => $newPlan['name'],
                    'charge_amount' => $newPlan['charge_amount'],
                    'positive_amount' => 0.00,
                    'account_positive_amount' => 0.00,
                    'gst_tax_amount' => 0.00,
                    'amount_without_tax' => 0.00,
                    'amount' => 2,
                    'period' => 30 - $nbrOfDay
                );
            }

            return $data;
        }
    }

    //this function calculate the total amount for adding new user..........
    public function calculateTotalAmountAddUser($accountToDate, $planCharge, $account_positive_amount = 0) {

        $stop_date1 = new DateTime($accountToDate);
        $to = $stop_date1->format('Y-m-d');

        $difference = strtotime($to) - time();
        if ($difference < 0) {
            $difference = 0;
        }
        $nbrOfDay = floor($difference / 60 / 60 / 24);
        $sub_total = ($nbrOfDay * $planCharge) / 30;
        $total_amount = (($nbrOfDay * $planCharge) / 30) - $account_positive_amount;


        if ($total_amount > 0) {
            $data = array(
                'account_positive_amount' => 0.00,
                'gst_tax_amount' => 0.1 * $total_amount,
                'amount_without_tax' => $total_amount,
                'amount_with_tax' => $total_amount + (0.1 * $total_amount),
                'period' => $nbrOfDay
            );
        } elseif ($total_amount < 0) {
            $data = array(
                'account_positive_amount' => $total_amount * -1,
                'gst_tax_amount' => (-0.1 * $total_amount),
                'amount_without_tax' => $sub_total,
                'amount_with_tax' => (-0.1 * $total_amount),
                'period' => $nbrOfDay
            );
        } elseif ($total_amount == 0) {
            $data = array(
                'account_positive_amount' => 0.00,
                'gst_tax_amount' => 0.2,
                'amount_without_tax' => $sub_total,
                'amount_with_tax' => 2.2,
                'period' => $nbrOfDay
            );
        }


//        $data = array(
//            'amount' => $total_amount,
//            'period' => $nbrOfDay
//        );
        return $data;
    }

    public function makeTransaction($tokenCustomerID, $totalAmount, $invoiceNumber) {

       // CheckAuth::checkLoggedIn();
        $userr = CheckAuth::getLoggedUser();
        $accountModel = new Model_Account();
        $userModel = new Model_User();
        $user = $userModel->getPureDataById($userr['user_id']);

        $Payment = array(
            'TotalAmount' => $totalAmount * 100,
            'InvoiceNumber' => $invoiceNumber
        );

        $Customer = array(
            'TokenCustomerID' => $tokenCustomerID
        );


        $transaction = array(
            'Customer' => $Customer,
            'Payment' => $Payment
        );



        Zend_Loader::loadFile('include_eway.php', APPLICATION_PATH . '/../library/eway-rapid-php-master');

        if (0) {
            $apiKey = 'A1001CbqrpVrgR/OJWzGztNull6mdN+cGg/+Pe6aJQtj4dpaByyXOtfhAEuoyj8G2TUDSM';
            $apiPassword = 'KlsiXk6R';
            $apiEndpoint = \Eway\Rapid\Client::MODE_SANDBOX;
        }

        $apiKey = 'A1001CbqrpVrgR/OJWzGztNull6mdN+cGg/+Pe6aJQtj4dpaByyXOtfhAEuoyj8G2TUDSM';
        $apiPassword = 'KlsiXk6R';
        $apiEndpoint = \Eway\Rapid\Client::MODE_SANDBOX;
        $client = \Eway\Rapid::createClient($apiKey, $apiPassword, $apiEndpoint);

        $transaction['TransactionType'] = \Eway\Rapid\Enum\TransactionType::RECURRING;
        $response = $client->createTransaction(\Eway\Rapid\Enum\ApiMethod::DIRECT, $transaction);
        return $response;
    }

    public function sendPaymentInvoiceChangePlanAsEmail($emailData) {

        //CheckAuth::checkLoggedIn();

        $view = new Zend_View();
        $view->setScriptPath(APPLICATION_PATH . '/modules/subscription/views/scripts/payment');
        $view->invoice_num = $emailData['invoice_num'];
        $view->subscription_payment_id = $emailData['subscription_payment_id'];
        $view->issued_date = $emailData['issued_date'];
        $view->first_name = $emailData['first_name'];
        $view->last_name = $emailData['last_name'];
        $view->company_name = $emailData['company_name'];
        $view->company_suburb = $emailData['company_suburb'];
        $view->company_street_address = $emailData['company_street_address'];
        $view->description = $emailData['description'];
        $view->number_of_user = $emailData['number_of_user'];
        $view->new_price_per_user = $emailData['new_price_per_user'];
        $view->old_price_per_user = $emailData['old_price_per_user'];
        $view->from_old = $emailData['from_old'];
        $view->to_old = $emailData['to_old'];
        $view->account_positive_amount = $emailData['account_positive_amount'];
        $view->positive_amount = $emailData['positive_amount'];
        $view->from_new = $emailData['from_new'];
        $view->to_new = $emailData['to_new'];
        $view->gst_tax_amount = $emailData['gst_tax_amount'];
        $view->total_without_tax = $emailData['amount_without_tax'];
        $view->total_with_tax = $emailData['total'];
        $bodyPaymentInvoice = $view->render('changePlanPaymentInvoice.phtml');


        $inc = str_pad($emailData['subscription_payment_id'], 3, '0', STR_PAD_LEFT);
        $template_params = array(
//invoice
            '{invoice_num}' => $emailData['invoice_num'],
            '{billing_id}' => 'OCTO-PAY' . date('y') . $inc,
//payment
            '{payment_invoice_view}' => $bodyPaymentInvoice,
//customer
            '{customer_first_name}' => $emailData['first_name'],
        );

        $modelEmailTemplate = new Model_EmailTemplate();
        $emailTemplate = $modelEmailTemplate->getEmailTemplate('send_subscription_invoice_as_email', $template_params,1);


        $params = array(
            'to' => $emailData['email1'],
            'body' => $emailTemplate['body'],
            'subject' => 'Change Plan Payment Invoice(Invoice#: ' . $emailData['invoice_num'] . ')',
        );


        $pdfPath = createPdfPath();
        $destination = $pdfPath['fullDir'] . $emailData['invoice_num'] . '.pdf';
        
        wkhtmltopdf($bodyPaymentInvoice, $destination);
       
        $params['attachment'] = $destination;

        $success = EmailNotification::sendEmail($params, '', array(), array());


        return $success;
    }

    public function sendPaymentInvoiceRenewAccountAsEmail($emailData) {

       // CheckAuth::checkLoggedIn();

        $view = new Zend_View();
        $view->setScriptPath(APPLICATION_PATH . '/modules/subscription/views/scripts/payment');
        $view->invoice_num = $emailData['invoice_num'];
        $view->subscription_payment_id = $emailData['subscription_payment_id'];
        $view->issued_date = $emailData['issued_date'];
        $view->first_name = $emailData['first_name'];
        $view->last_name = $emailData['last_name'];
        $view->mobile1 = $emailData['mobile1'];
        $view->email1 = $emailData['email1'];
        $view->company_name = $emailData['company_name'];
        $view->company_suburb = $emailData['company_suburb'];
        $view->company_street_address = $emailData['company_street_address'];
        $view->description = $emailData['description'];
        $view->number_of_user = $emailData['number_of_user'];
        $view->price_per_user = $emailData['price_per_user'];
        $view->from = $emailData['from'];
        $view->to = $emailData['to'];

        $view->account_positive_amount = $emailData['account_positive_amount'];
        $view->amount_without_tax = $emailData['amount_without_tax'];
        $view->amount_with_tax = $emailData['amount_with_tax'];
        $bodyPaymentInvoice = $view->render('renewAccountPaymentInvoice.phtml');


        $inc = str_pad($emailData['subscription_payment_id'], 3, '0', STR_PAD_LEFT);
        $template_params = array(
//invoice
            '{invoice_num}' => $emailData['invoice_num'],
            '{billing_id}' => 'OCTO-PAY' . date('y') . $inc,
//payment
            '{payment_invoice_view}' => $bodyPaymentInvoice,
//customer
            '{customer_first_name}' => $emailData['first_name'],
        );

        $modelEmailTemplate = new Model_EmailTemplate();
        $emailTemplate = $modelEmailTemplate->getEmailTemplate('send_subscription_invoice_as_email', $template_params, 1);

        $params = array(
            'to' => $emailData['email1'],
            'body' => $emailTemplate['body'],
            'subject' => 'Renew Account Payment Invoice(Invoice#: ' . $emailData['invoice_num'] . ')',
        );

        $pdfPath = createPdfPath();
        $destination = $pdfPath['fullDir'] . $emailData['invoice_num'] . '.pdf';
        wkhtmltopdf($bodyPaymentInvoice, $destination);
        $params['attachment'] = $destination;



        $success = EmailNotification::sendEmail($params, '', array(), array());

        return $success;
    }

    public function sendPaymentInvoiceAddUserAsEmail($emailData) {

       // CheckAuth::checkLoggedIn();

        $view = new Zend_View();
        $view->setScriptPath(APPLICATION_PATH . '/modules/subscription/views/scripts/payment');
        $view->invoice_num = $emailData['invoice_num'];
        $view->subscription_payment_id = $emailData['subscription_payment_id'];
        $view->issued_date = $emailData['issued_date'];
        $view->first_name = $emailData['first_name'];
        $view->last_name = $emailData['last_name'];
        $view->mobile1 = $emailData['mobile1'];
        $view->email1 = $emailData['email1'];
        $view->company_name = $emailData['company_name'];
        $view->company_suburb = $emailData['company_suburb'];
        $view->company_street_address = $emailData['company_street_address'];
        $view->description = $emailData['description'];
        $view->price_per_user = $emailData['price_per_user'];
        $view->from = $emailData['from'];
        $view->to = $emailData['to'];
        $view->number_of_user = 1;
        $view->account_positive_amount = $emailData['account_positive_amount'];
        $view->gst_tax_amount = $emailData['gst_tax_amount'];
        $view->amount_without_tax = $emailData['amount_without_tax'];
        $view->amount_with_tax = $emailData['amount_with_tax'];
        $bodyPaymentInvoice = $view->render('addUserPaymentInvoice.phtml');

        $inc = str_pad($emailData['subscription_payment_id'], 3, '0', STR_PAD_LEFT);
        $template_params = array(
//invoice
            '{invoice_num}' => $emailData['invoice_num'],
            '{billing_id}' => 'OCTO-PAY' . date('y') . $inc,
//payment
            '{payment_invoice_view}' => $bodyPaymentInvoice,
//customer
            '{customer_first_name}' => $emailData['first_name'],
        );

        $modelEmailTemplate = new Model_EmailTemplate();
        $emailTemplate = $modelEmailTemplate->getEmailTemplate('send_subscription_invoice_as_email', $template_params, 1);

        $params = array(
            'to' => $emailData['email1'],
            'body' => $emailTemplate['body'],
            'subject' => 'Add User Payment Invoice',
        );

        $pdfPath = createPdfPath();
        $destination = $pdfPath['fullDir'] . $emailData['invoice_num'] . '.pdf';
        wkhtmltopdf($bodyPaymentInvoice, $destination);
        $params['attachment'] = $destination;

        $success = EmailNotification::sendEmail($params, '', array(), array());

        return $success;
    }

    public function cronJobMakeAccountRenew() {



//        $email_log = array('reference_id' => $contractor_id, 'cronjob_history_id' => $cronjobHistoryId, 'type' => 'common');
//        EmailNotification::sendEmail($params, '', array(), $email_log, $contractor['company_id']);
//        $modelCronjobHistory = new Model_CronjobHistory();
//        $modelCronJob = new Model_CronJob();
//        
//         //save this cron in cronjob_history table
//        
//		$cronjob = $modelCronJob->getIdByName('Reminder_Contractor_to_Update_Booking');
//        $cronjonID = $cronjob['id'];
//
//        $cronjobHistoryData = array(
//            'cron_job_id' => $cronjonID,
//            'run_time' => time()
//        );
//        $cronjobHistoryId = $modelCronjobHistory->insert($cronjobHistoryData);
        //load Related Models-------------------------------------------------------------
        $modelAccountObj = new Model_Account();
        $invoicemodelObj = new Model_InvoiceSubscription();
        $modelUser = new Model_User();


        $accountsNeedRenew = $modelAccountObj->findAccountThatNeedRenew();
        if (!empty($accountsNeedRenew)) {
            foreach ($accountsNeedRenew as $accountNeedRenew) {

                $select = $this->getAdapter()->select();
                $select->from(array('inv' => 'invoice_subscription'), 'MAX(inv.invoice_id)');
                $select->where("inv.account_id = '{$accountNeedRenew['id']}'");
                $result = $this->getAdapter()->fetchOne($select);
                $inc = str_pad($result, 5, '0', STR_PAD_LEFT);
                $invoice_num = 'OCTO' . date('y') . $inc;


                //1- make transcation-------------------------------------------------------  
                $company = $modelAccountObj->getCompanyById($accountNeedRenew['company_id']);
                $users = $modelAccountObj->getUsersByCompanyId($company['company_id']);
                $plan = $modelAccountObj->getPlanById($accountNeedRenew['plan_id']);
                $nbrOfuser = count($users);
                $amountWithoutGST = $nbrOfuser * $plan['charge_amount'];
                $totalAmountIncludeGST = $amountWithoutGST + ($amountWithoutGST * 0.1) - $accountNeedRenew['positive_amount'];


                $response = $this->makeTransaction($accountNeedRenew['customer_token_id'], abs($totalAmountIncludeGST), $invoice_num);
                if ($response->getErrors()) {
                    //insert new record in payment with status fail
                    $paymentData = array(
                        'account_id' => $accountNeedRenew['id'],
                        'created_by' => $accountNeedRenew['created_by'],
                        'positive_amount' => 0.00,
                        'amount' => abs($totalAmountIncludeGST),
                        'from' => date("Y-m-d", time()),
                        'to' => date('Y-m-d', strtotime('+1 month')),
                        'transaction_id' => $response->TransactionID,
                        'description' => 'Plan Renew',
                        'period' => 30,
                        'new_plan_id' => $accountNeedRenew['plan_id'],
                        'invoice_num' => $invoice_num,
                        'status' => 'fail'
                    );
                    $paymentInsert = $this->insert($paymentData);

                    //update status in account
                    $accountData = array(
                        'account_status' => 'Overdue'
                    );
                    $updateSuccess = $modelAccountObj->updateById($accountNeedRenew['id'], $accountData);
                } else {
                    //2- make new invoice-------------------------------------------------------
                    $invoiceData = array(
                        'invoice_num' => $invoice_num,
                        'account_id' => $accountNeedRenew['id'],
                        'created_by' => $accountNeedRenew['created_by'],
                        'status' => 'paid',
                        'amount' => $totalAmountIncludeGST
                    );
                    $insertInvoiceSuccess = $invoicemodelObj->insert($invoiceData);


                    //3- update account record--------------------------------------------------   
                    $accountData = array(
                        'from' => date("Y-m-d", time()),
                        'to' => date('Y-m-d', strtotime('+1 month')),
                        'account_status' => 'Subscriber-paid'
                    );
                    $updateSuccess = $modelAccountObj->updateById($accountNeedRenew['id'], $accountData);


                    //4- insert new record to subscription payment Table--------------------------------- 
                    $paymentData = array(
                        'account_id' => $accountNeedRenew['id'],
                        'created_by' => $accountNeedRenew['created_by'],
                        'positive_amount' => 0.00,
                        'amount' => $totalAmountIncludeGST,
                        'from' => date("Y-m-d", time()),
                        'to' => date('Y-m-d', strtotime('+1 month')),
                        'transaction_id' => $response->TransactionID,
                        'description' => 'Plan Renew',
                        'period' => 30,
                        'new_plan_id' => $accountNeedRenew['plan_id'],
                        'invoice_num' => $invoice_num,
                        'status' => 'paid'
                    );
                    $paymentInsert = $this->insert($paymentData);


                    //5- send Email to the Owner -------------------------------------------------------
                    $user = $modelUser->getPureDataById($accountNeedRenew['created_by']);
                    $emailData = array(
                        'invoice_num' => $invoice_num,
                        'subscription_payment_id' => $paymentInsert,
                        'issued_date' => date("d/m/Y", time()),
                        'first_name' => $user['first_name'],
                        'last_name' => $user['last_name'],
                        'mobile1' => $user['mobile1'],
                        'email1' => $user['email1'],
                        'company_name' => $company['company_name'],
                        'company_suburb' => $company['company_suburb'],
                        'company_street_address' => $company['company_street_address'],
                        'description' => 'This invoice for renew your account<br> under plan ' . $plan['name'] . '<br> which number of user is ' . $nbrOfuser,
                        'number_of_user' => $nbrOfuser,
                        'price_per_user' => $plan['charge_amount'],
                        'from' => date("d/m/Y", time()),
                        'to' => date('d/m/Y', strtotime('+1 month')),
                        'account_positive_amount' => $accountNeedRenew['positive_amount'],
                        'amount_without_tax' => $amountWithoutGST,
                        'amount_with_tax' => $totalAmountIncludeGST
                    );
                    $emailSuccess = $this->sendPaymentInvoiceRenewAccountAsEmail($emailData);
                }//end transaction if*********
            }//end foreach**************
        }//end if******************
    }

    public function getnewInvoiceNum() {
        $select = $this->getAdapter()->select();
        $select->from($this->_name, 'MAX(subscription_payment_id)');
        //$select->where("account_id = '{$accountID}'");
        $result = $this->getAdapter()->fetchOne($select);
        $inc = str_pad($result, 5, '0', STR_PAD_LEFT);
        $invoice_num = 'OCTO' . date('y') . $inc;
        return $invoice_num;
    }
    
    public function sendPaymentInvoiceRenewTwilioAccountAsEmail($emailData) {

       // CheckAuth::checkLoggedIn();

        $view = new Zend_View();
        $view->setScriptPath(APPLICATION_PATH . '/modules/subscription/views/scripts/payment');
        $view->invoice_num = $emailData['invoice_num'];
        $view->subscription_payment_id = $emailData['subscription_payment_id'];
        $view->first_name = $emailData['first_name'];
        $view->last_name = $emailData['last_name'];
        $view->mobile1 = $emailData['mobile1'];
        $view->email1 = $emailData['email1'];
        
        $view->company_name = $emailData['company_name'];
        $view->company_suburb = $emailData['company_suburb'];
        $view->company_street_address = $emailData['company_street_address'];
        $view->description = $emailData['description'];
        
        $view->from = $emailData['from'];
        $view->to = $emailData['to'];

        $view->amount_without_tax = $emailData['amount_without_tax'];
        $view->amount_gst = $emailData['amount_gst'];
        $view->total = $emailData['total'];
        
        //3ayel teeeeeeeeeeeeeeeeeeet
        $bodyPaymentInvoice = $view->render('purchaseTwilioNumberInvoice.phtml');


        $inc = str_pad($emailData['subscription_payment_id'], 3, '0', STR_PAD_LEFT);
        $template_params = array(
//invoice
            '{invoice_num}' => $emailData['invoice_num'],
            '{billing_id}' => 'OCTO-PAY' . date('y') . $inc,
//payment
            '{payment_invoice_view}' => $bodyPaymentInvoice,
//customer
            '{customer_first_name}' => $emailData['first_name'],
        );

        $modelEmailTemplate = new Model_EmailTemplate();
        $emailTemplate = $modelEmailTemplate->getEmailTemplate('send_subscription_invoice_as_email', $template_params,1,0);
        
        $pdfPath = createPdfPath();
        $destination = $pdfPath['fullDir'] . $emailData['invoice_num'] . '.pdf';
        wkhtmltopdf($bodyPaymentInvoice, $destination);
        $params['attachment'] = $destination;

        $params = array(
            'to' => $emailData['email1'],
            'body' => $emailTemplate['body'],
            'subject' => 'Purchasing Twilio number Invoice(Invoice#: ' . $emailData['invoice_num'] . ')',
            'attachment'=>$destination
        );
 

        $success = EmailNotification::sendEmail($params, '', array(), array());

        return $success;
    }

//end CronJob**********
}
