<?php

class Model_Image extends Zend_Db_Table_Abstract {

    protected $_name = 'image';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    
public function getImagesAndAttachments($perPage = 0, $currentPage = 0) {

        $select1 = $this->getAdapter()->select();
        $select1->from(array('a' => 'attachment'), array('original_path' => 'path', 'large_path' => 'large_file', 'flag'=> new Zend_Db_Expr('1'), 'created'));
  $select1->where('a.type like "image%"');
  
  $select2 = $this->getAdapter()->select();
        $select2->from($this->_name, array('original_path', 'large_path', 'flag' => new Zend_Db_Expr('2'), 'created' => 'UNIX_TIMESTAMP(created)'));
   
  $select = $this->getAdapter()->select()
      ->union(array($select1, $select2))
      ->order('created desc');
  
  if ($perPage && $currentPage) {
            $select->limit($perPage, ($currentPage - 1 ) * $perPage);
        }
  
        return $this->getAdapter()->fetchAll($select);
    }
    
    
    public function getImages() {

        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        return $this->getAdapter()->fetchAll($select);
    }

    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "image_id = '{$id}'");
    }

    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("image_id = '{$id}'");
    }

    public function getTotalGroups($id = null, $type = null, $per_group = null) {

        $total_records = count($this->getAll($id, $type));
        $total_groups = ceil($total_records / $per_group);
        return $total_groups;
    }

    public function getMaxGroup() {

        $select = $this->getAdapter()->select();
        $select->from($this->_name, array('group_id'));
        return $this->getAdapter()->fetchAll($select);
    }
	
	public function getContractorDiscussionImages($contractorId,$discussionId , $grouped_images = 0){
	  
	  $discussionId = (int) $discussionId;
	  $contractorId = (int) $contractorId;
	  $select = $this->getAdapter()->select();
      $select->from(array('i' => $this->_name),array('image_id','thumbnail_path','large_path','small_path','original_path', 'compressed_path'));
	  $select->joinInner(array('ii' => 'item_image'), "i.image_id = ii.image_id", array('ii.item_id'));
	  $select->where("ii.item_id = '{$contractorId}' ");
	  $select->where("ii.discussion_id = '{$discussionId}' ");
	  if(!$grouped_images){
	     $select->where('i.group_id = 0');
	  }else{
	    $select->where('i.group_id != 0');
		$select->limit(4);
	  }
	  
	  return $this->getAdapter()->fetchAll($select);
      
	  
	}

    public function getAll($id = null, $type = null, $order = null, &$pager = null, $filter = array(), $limit = 0, $perPage = 0, $currentPage = 0) {
        //$id = (int) $id;
        $modelBooking = new Model_Booking();
        $modelBookingEstimate = new Model_BookingEstimate();
        $inquiryBookingId = 0;
        $select = $this->getAdapter()->select();
        $select->from(array('i' => $this->_name));
        $select->order($order);
        $typeFilter = $this->getAdapter()->quote($type . '%');
        $select->joinInner(array('ii' => 'item_image'), "i.image_id = ii.image_id", array('ii.item_image_id', 'ii.item_id', 'ii.type', 'ii.service_id', 'ii.discussion_id'));
        $select->joinLeft(array('s' => 'service'), "s.service_id = ii.service_id", array('s.service_name'));
        $select->where("ii.type LIKE {$typeFilter}");
        if (!($id == 0))
            $select->where("ii.item_id = '{$id}' ");

        if ($type == 'booking') {
            $booking = $modelBooking->getById($id);
            $estimate = $modelBookingEstimate->getByBookingId($id);
            if ($estimate) {
                $newtypeFilter = $this->getAdapter()->quote('estimate' . '%');
                $select->orWhere(" ( ii.type LIKE {$newtypeFilter} AND ii.item_id = '{$estimate['id']}' )");
            }
            if ($booking['original_inquiry_id']) {
                $newtypeFilter = $this->getAdapter()->quote('inquiry' . '%');
                $select->orWhere(" ( ii.type LIKE {$newtypeFilter} AND ii.item_id = '{$booking['original_inquiry_id']}' )");
            }
        }

        if ($type == 'estimate') {
            $estimate = $modelBookingEstimate->getById($id);
            if ($estimate['booking_id']) {
                $booking = $modelBooking->getById($estimate['booking_id']);
                if ($booking['original_inquiry_id']) {
                    $newtypeFilter = $this->getAdapter()->quote('inquiry' . '%');
                    $select->orWhere(" ( ii.type LIKE {$newtypeFilter} AND ii.item_id = '{$booking['original_inquiry_id']}' )");
                }
            }
        }


        if ($filter) {
            if (!empty($filter['image_type']) && !($filter['image_type'] == 'all')) {
                $select->where("i.image_types_id = '{$filter['image_type']}' ");
            }
        }



        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        } elseif ($limit) {

            $select->limit($limit);
        } elseif ($perPage && $currentPage) {
            $select->limit($perPage, ($currentPage - 1 ) * $perPage);
        }



//
//if ($_SERVER['REMOTE_ADDR'] == '176.106.46.142') {
//            echo $select->__toString();
//        }
        /* echo $select->__toString();
          exit; */
        return $this->getAdapter()->fetchAll($select);
    }

    public function getByGroupId($group_id, $item_id = null, $type = null) {

        $group_id = (int) $group_id;
        $select = $this->getAdapter()->select();
        $select->from(array('i' => $this->_name));
        $select->where("i.group_id = {$group_id}");
        //by mohamed
        $select->joinInner(array('ii' => 'item_image'), "i.image_id = ii.image_id", array('ii.item_id', 'ii.item_id', 'ii.service_id'));
        $select->joinLeft(array('s' => 'service'), "s.service_id = ii.service_id", array('s.service_name'));
        //by mohamed
        if (isset($item_id)) {
            $select->joinInner(array('iid' => 'item_image_discussion'), 'i.group_id = iid.group_id');
            $select->where("iid.item_id = '{$item_id}'");
            $select->where("iid.type = '{$type}'");
        }
        $select->limit(4);
        return $this->getAdapter()->fetchAll($select);
    }

    public function getByGroupIdAndType($group_id, $item_id = null, $type = null) {

        $group_id = (int) $group_id;
        $select = $this->getAdapter()->select();
        $select->from(array('iid' => 'item_image_discussion'));
        $select->where("iid.group_id = {$group_id}");
        $select->where("iid.type = '{$type}'");
        if (isset($item_id)) {
            $select->joinInner(array('ii' => 'item_image'), 'ii.discussion_id = iid.item_id');
			$select->where("ii.item_id = '{$item_id}'");
            $select->joinInner(array('im' => 'image'), 'im.image_id = ii.image_id');
        }

        $select->limit(4);
        
        return $this->getAdapter()->fetchAll($select);
    }

    public function getById($id, $type) {

        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from(array('i' => $this->_name));
        $select->joinInner(array('ii' => 'item_image'), "i.image_id = ii.image_id", array('ii.item_id', 'ii.item_id', 'ii.type', 'ii.service_id', 'ii.floor_id'));
        $select->joinLeft(array('s' => 'service'), "s.service_id = ii.service_id", array('s.service_name'));
        $select->where("i.image_id = {$id}");


        return $this->getAdapter()->fetchRow($select);
    }
	
	public function getByImageId($id) {
		$id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from(array('i' => $this->_name));
		$select->where("i.image_id = {$id}");
        return $this->getAdapter()->fetchRow($select);
	}

}
