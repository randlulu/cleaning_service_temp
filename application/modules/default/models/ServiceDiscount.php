<?php

class Model_ServiceDiscount extends Zend_Db_Table_Abstract {

    protected $_name = 'service_discount';

	
    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from(array('sd' => $this->_name));
        //$select->joinInner(array('s' => 'service'), 'sa.service_id=s.service_id', array('s.service_name'));
        $select->order($order);

        if ($filters) {
            if (!empty($filters['service_id'])) {
                $select->where("sd.service_id = {$filters['service_id']}");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }
    
     /**
     * update table rows according to the assigned id and data
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */

    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "id = '{$id}'");
    }

    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("id = '{$id}'");
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
   public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
		$select->where("id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }
    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getByServiceId($id){
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("service_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }
	
	public function insert(array $data) {
		
		return parent::insert($data);	
	}
	
	public function getByServiceIdAndQunatity($id,$qunatity){
	  
	    $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("service_id = '{$id}'");
		
        $select->where(" '{$qunatity}' > min_size_range and '{$qunatity}' <= max_size_range ");
		return $this->getAdapter()->fetchRow($select);
	
	}
	
	public function getMaxDiscount($id){
	  
	    $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name,array('max_discount'=>'MAX(max_discount_range)','max_size'=>'MAX(max_size_range)'));
        $select->where("service_id = '{$id}'");
		return $this->getAdapter()->fetchRow($select);
	
	}


}