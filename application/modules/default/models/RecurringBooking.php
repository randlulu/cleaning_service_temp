<?php

class Model_RecurringBooking extends Zend_Db_Table_Abstract {

		protected $_name = 'recurring_booking';

		public function insert(array $data) 
		{
		  $id = parent::insert($data);
			return $id;
		}

		public function getById($id) {
			$id = (int) $id;
			$select = $this->getAdapter()->select();
			$select->from($this->_name);
			$select->where("booking_id = '{$id}'");
			return $this->getAdapter()->fetchRow($select);
		}
	
}