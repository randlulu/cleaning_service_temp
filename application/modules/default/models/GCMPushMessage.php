<?php
/*	Class to send push notifications using Google Cloud Messaging for Android

	Example usage
	-----------------------
	$an = new GCMPushMessage($apiKey);
	$an->setDevices($devices);
	$response = $an->send($message);
	-----------------------
	
	$apiKey Your GCM api key
	$devices An array or string of registered device tokens
	$message The mesasge you want to push out

	@author Matt Grundy

	Adapted from the code available at:
	http://stackoverflow.com/questions/11242743/gcm-with-php-google-cloud-messaging

*/
class Model_GCMPushMessage { 

	var $url = 'https://android.googleapis.com/gcm/send';
	var $serverApiKey = "AIzaSyDlZZ6U8GlPfVpXDGeam3mNYMk1hd-kfIk";
	//var $devices = array('APA91bGOr-4RZu6-YjrlooZXFDVXdDHYtsANTPg_hxg2w46wey4H0uwcCpUOhBMdQpngZBy9dP8vQ6tqEzzn_LakvOvwVQJJxNcu8t50PMGLleg61zDIYhk3HquFiWku6eEAhC8PPYidDOf0PerCAG5QqbBmDeTGeA');
	var $devices = array('APA91bHj46t9EOFTFduQE13IqdlPvRR0kJgmwO8Jixh3c0JSIa3zl5m690okAun73Lwf-M3hPw6XUnQF1ZGtB3JMCkAiZrorpwOnux4KibQypgjIJCMjokGGIRZl5N8KAfCUWbcTkdp-');
	/*
		Constructor
		@param $apiKeyIn the server API key
	*/
	public function __construct($apiKeyIn = ''){
        
        if(!$apiKeyIn){
            $company_id = CheckAuth::getCompanySession();
            $api_name = 'GCM-push-notification';

            $model_api_parameters = new Model_ApiParameters();

            $parameter_name = 'api key in';
            $parameter = $model_api_parameters->getParameterValue($company_id, $api_name, $parameter_name);
            $apiKeyIn = $parameter['parameter_value'];  
        }  
        //$apiKeyIn = 'AIzaSyCOZw75Spbsi0flk0S7IoyM73Hcv8dd-4o';
		$this->serverApiKey = $apiKeyIn;
	}

	/*
		Set the devices to send to
		@param $deviceIds array of device tokens to send to
	*/
	function setDevices($deviceIds){
	
		if(is_array($deviceIds)){
			$this->devices = $deviceIds;
		} else {
			$this->devices = array($deviceIds);
		}
	
	}

	/*
		Send the message to the device
		@param $message The message to send
		@param $data Array of data to accompany the message
	*/
	function send($message, $data = false){
		
		if(!is_array($this->devices) || count($this->devices) == 0){
			$this->error("No devices set");
		}
		
		if(strlen($this->serverApiKey) < 8){
			$this->error("Server API Key not set");
		}
		
		$fields = array(
			'registration_ids'  => $this->devices,
			'data'              => array( "msg" => $message ),
		);
		
		if(is_array($data)){
			foreach ($data as $key => $value) {
				$fields['data'][$key] = $value;
			}
		}

		$headers = array( 
			'Authorization: key=' . $this->serverApiKey,
			'Content-Type: application/json'
		);

		// Open connection
		$ch = curl_init();
		
		// Set the url, number of POST vars, POST data
		curl_setopt( $ch, CURLOPT_URL, $this->url );
		
		curl_setopt( $ch, CURLOPT_POST, true );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		
		curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
		
		// Avoids problem with https certificate
		curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false);
		
		// Execute post
		$result = curl_exec($ch);
		
		// Close connection
		curl_close($ch);
		
		return $result;
	}
	
	function error($msg){
		echo "Android send notification failed with error:";
		echo "\t" . $msg;
		exit(1);
	}
}
//AIzaSyDlZZ6U8GlPfVpXDGeam3mNYMk1hd-kfIk
//AIzaSyDA1t7waAeZxmjhWwyuCCrjrI5rvDzpCCE
//AIzaSyDlZZ6U8GlPfVpXDGeam3mNYMk1hd-kfIk
/*$test = new GCMPushMessage('AIzaSyDlZZ6U8GlPfVpXDGeam3mNYMk1hd-kfIk');
$test->send("<b style=\"color:#69bdaa;\">BOK-13285</b> <i class=\"fa fa-map-marker\"></i> <b><i>123 12 , 6 / 166 Ramsgate Ave 12 VIC</i></b> ON HOLD <span style=\"color:#69bdaa;\">Monday, Aug 10 01:07 am</span> has a complaint placed by <b>ahmets</b> on Monday 22:19 pm Aug 10 \" test \" ");
echo 'send done ';*/
?>

