<?php

class Reports_PaymentReportController extends Zend_Controller_Action {

    private $request;
    private $router;
    private $LoggedUser;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'reports';
        $this->view->sub_menu = 'reports';

        $this->LoggedUser = CheckAuth::getLoggedUser();
    }

    public function paymentReceivedAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('reportPaymentReceived'));
        $modelPayment = new Model_Payment();
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        if (!isset($filters['payment_created_between']) && !isset($filters['payment_end_between'])) {
            $filters['payment_created_between'] = date('Y-m-1', time());
            $filters['payment_end_between'] = date('Y-m-d', strtotime('-1 second', strtotime('+1 month', strtotime(date('m') . '/01/' . date('Y') . ' 00:00:00'))));
        }

        $options = array(
            'action_url' => $this->router->assemble(array(), 'reportPaymentReceived'),
            'created_date' => $filters['payment_created_between'],
            'end_date' => $filters['payment_end_between'],
            'type' => 'payment_received'
        );
        $form = new Reports_Form_PaymentFilters($options);

        $all_payment_filters = array();
        $all_payment_filters['payment_created_between'] = $filters['payment_created_between'] ? $filters['payment_created_between'] . " 00:00:00" : "";
        $all_payment_filters['payment_end_between'] = $filters['payment_end_between'] ? $filters['payment_end_between'] . " 23:59:59" : "";

        $payments = $modelPayment->getAll($all_payment_filters, 'received_date ASC');
        $modelPayment->fills($payments, array('customer', 'payment_type', 'total_amount', 'invoice'));

        $this->view->payments = $payments;
        $this->view->fltr = $filters;
        $this->view->form = $form;
    }
	
	
	////////By Islam action of payment to contractor report
	public function paymentToContractorsAction() {

        //
        // check Auth for logged user
        //
        //CheckAuth::checkPermission(array('reportPaymentReceived'));
        $modelPayment = new Model_PaymentToContractors();
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }
		if (!isset($filters['contractor_id']) && !isset($filters['contractor_id'])) {
			$LoggedUser=CheckAuth::getLoggedUser();
			$modelAuthRole = new Model_AuthRole();
			if ($LoggedUser['role_id'] == $modelAuthRole->getRoleIdByName('contractor') && !CheckAuth::checkCredential(array('canSeePaymentsToAllContractors'))){
				$filters['contractor_id'] = $LoggedUser['user_id'];
				}
			else{
				$filters['contractor_id'] = "";
			}
                        
            
        }

        if (!isset($filters['payment_created_between']) && !isset($filters['payment_end_between'])) {
            $filters['payment_created_between'] = date('Y-m-1', time());
            $filters['payment_end_between'] = date('Y-m-d', strtotime('-1 second', strtotime('+1 month', strtotime(date('m') . '/01/' . date('Y') . ' 00:00:00'))));
        }
		
		if (!isset($filters['payment_created_between']) && !isset($filters['payment_end_between'])) {
            $filters['payment_created_between'] = date('Y-m-1', time());
            $filters['payment_end_between'] = date('Y-m-d', strtotime('-1 second', strtotime('+1 month', strtotime(date('m') . '/01/' . date('Y') . ' 00:00:00'))));
        }

        $options = array(
            'action_url' => $this->router->assemble(array(), 'reportPaymentToContractors'),
            'created_date' => $filters['payment_created_between'],
            'end_date' => $filters['payment_end_between'],
            'type' => 'payment_to_contractors'
        );
        $form = new Reports_Form_PaymentToContractorsFilters($options);

        $all_payment_filters = array();
        $all_payment_filters['payment_created_between'] = $filters['payment_created_between'] ? $filters['payment_created_between'] . " 00:00:00" : "";
        $all_payment_filters['payment_end_between'] = $filters['payment_end_between'] ? $filters['payment_end_between'] . " 23:59:59" : "";
		$all_payment_filters['contractor_id']= $filters['contractor_id'] ? $filters['contractor_id']: "";
        $payments = $modelPayment->getAll($all_payment_filters, 'date_of_payment ASC');
        
        $this->view->payments = $payments;
        $this->view->fltr = $filters;
        $this->view->form = $form;
    }

    public function refundHistoryAction() {


        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('reportRefundHistory'));

        // Load Model
        $modelRefund = new Model_Refund();


        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        if (!isset($filters['payment_created_between']) && !isset($filters['payment_end_between'])) {
            $filters['payment_created_between'] = date('Y-m-1', time());
            $filters['payment_end_between'] = date('Y-m-d', strtotime('-1 second', strtotime('+1 month', strtotime(date('m') . '/01/' . date('Y') . ' 00:00:00'))));
        }


        $options = array(
            'action_url' => $this->router->assemble(array(), 'reportRefundHistory'),
            'created_date' => $filters['payment_created_between'],
            'end_date' => $filters['payment_end_between'],
            'type' => 'refund_history'
        );

        $form = new Reports_Form_PaymentFilters($options);

        $all_refund_filters = array();
        $all_refund_filters['payment_created_between'] = $filters['payment_created_between'] ? $filters['payment_created_between'] . " 00:00:00" : "";
        $all_refund_filters['payment_end_between'] = $filters['payment_end_between'] ? $filters['payment_end_between'] . " 23:59:59" : "";

        $refunds = $modelRefund->getAll($all_refund_filters, 'received_date ASC');
        $modelRefund->fills($refunds, array('customer', 'payment_type', 'invoice'));

        $this->view->refunds = $refunds;
        $this->view->fltr = $filters;
        $this->view->form = $form;
    }

    public function overdueInvoicesAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('reportOverdueInvoices'));

        //
        //declaring the moduels
        //
        $modelBooking = new Model_Booking();
        $modelUser = new Model_User();
        $modelPaymentType = new Model_PaymentType();
        $modelPayment = new Model_Payment();

        //
        //get the requested param
        //
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        $form = new Reports_Form_ContractorFilters(array('action_url' => $this->router->assemble(array(), 'reportOverdueInvoices'), 'type' => 'overdue_invoices'));
        $this->view->form = $form;

        $paymentTypes = $modelPaymentType->getAll(array('without_cash' => true));
        $this->view->paymentTypes = $paymentTypes;

        $data = array();
        if ($filters) {
            //
            //the requested contractor info
            //
            $user = $modelUser->getById($filters['contractor_id']);
            $this->view->user = $user;

            //
            //get all the booking
            //
            $all_booking_filters = array();
            $all_booking_filters['contractor_id'] = $filters['contractor_id'];
            $all_booking_filters['booking_start_between'] = $filters['booking_start_between'];
            $all_booking_filters['booking_end_between'] = !empty($filters['booking_end_between']) ? $filters['booking_end_between'] . ' 23:59:59' : '';
            $all_booking_filters['convert_status'] = 'invoice';
            $all_booking_filters['invoice_type'] = 'unpaid';

            $data = $modelBooking->getAll($all_booking_filters, 'booking_start ASC');
            $modelBooking->fills($data, array('contractors', 'address', 'created_by', 'status', 'services', 'invoice', 'full_customer_info', 'number_of_due_days','status_discussion','approved_ammount','unapproved_ammount','total_without_cash','cash_payment'));

        }

        $this->view->data = $data;
        $this->view->filters = $filters;
    }

	
	public function uploadDocumentToGoogleDriveAction(){
		$modleGoogleDrive = new Model_GoogleDrive();
		$modlePaymentAttachment = new Model_PaymentAttachment();
		$contractorId = $this->request->getParam('contractor_id');
		$attachmentId = $this->request->getParam('attachment_id');
		$fileName = $this->request->getParam('file_name');
		$pathArr = $modlePaymentAttachment->getPathById($attachmentId);
		$path = $pathArr['path'];
		$originalFileName = get_config('payment_attachment').'/'.$path;
		
		$retVal = $modleGoogleDrive->uploadDocumentToGoogleDrive($contractorId, false, $originalFileName,$fileName);
		
		echo json_encode(array('retVal' => $retVal, 'attachmentId' => $attachmentId));
		  
		exit;		  
	
	}
	
	
	public function deletePaymentToContractorAttachmentAction(){
			$modlePaymentAttachment = new Model_PaymentAttachment();
			
			$attachmentId = $this->request->getParam('attachment_id');
			
			$isDeleted = $modlePaymentAttachment->deleteById($attachmentId);
			echo json_encode(array('isDeleted' => $isDeleted,'attachmentId' => $attachmentId ));
			exit;
	
	}
	
	public function deletePaymentToContractorsAction(){ 
	
		$payment_to_contractors_id=$this->request->getParam('id');
		////////load models
		$modelPaymentAttachment=new Model_PaymentAttachment();
		$modelPaymentToContractors = new Model_PaymentToContractors();
		
		
		$isDeletedPayment=$modelPaymentToContractors->deleteById($payment_to_contractors_id);
		$areAttachmentsDeleted= 0;
		if($isDeletedPayment){
			$areAttachmentsDeleted = $modelPaymentAttachment->deleteByPaymentToContractorId($payment_to_contractors_id);
		}
		
		echo json_encode(array('isDeletedPayment' => $isDeletedPayment,'payment_to_contractors_id' => $payment_to_contractors_id,'areAttachmentsDeleted' => $areAttachmentsDeleted ));
		exit;
	}
	
	
	public function sendPaymentAsEmailAction(){ 
		//load models
		$modelPaymentToContractors = new Model_PaymentToContractors();
		$modelContractorServiceBooking = new Model_ContractorServiceBooking();
		
		$paymentToContractorId = $this->request->getParam('id', 0);
		$contractorId = $this->request->getParam('contractor_id', 0);
		
		$payment = $modelPaymentToContractors->getById($paymentToContractorId);
		$this->view->payment = $payment;
		
		$filters = array();
		$filters['payment_to_contractor_id'] = $paymentToContractorId;
		$filters['contractor_id'] = $contractorId;
		$filters['paid_status'] =1;
		//$filters['customer_payment_status'] =1;
		$bookings = $modelContractorServiceBooking->test($filters);
		$this->view->bookings = $bookings;
		//$this->view->to = 'iabosalem88@hotmail.com';
		//$this->view->attachment = $paymentToContractorId;
		//$this->view->subject = 'invoice datails';
		
			
            if ($this->request->isPost()) {
                $to = $this->request->getParam('to');
                $cc = $this->request->getParam('cc');
                $subject = $this->request->getParam('subject');
                $body = $this->request->getParam('body');
                $pdf_attachment = $this->request->getParam('pdf_attachment', 0);

                $params = array(
                    'to' => $to,
                    'cc' => $cc,
                    'body' => $body,
                    'subject' => $subject
                );

				$view = new Zend_View();
				$error_mesages = array();
				$view->setScriptPath(APPLICATION_PATH . '/modules/reports/views/scripts/payment-report');
				$view->payment = $payment;
				$view->bookings = $bookings;
			
			
				$bodyInvoice = $view->render('payment-as-email.phtml');
				
                //if (EmailNotification::validation($params, $error_mesages)) {
				try{
                    if (!empty($pdf_attachment)) {
                        // Create pdf
                        $pdfPath = createPdfPath();
                        $destination = $pdfPath['fullDir'] .$payment['contractor_invoice_num'].'.pdf';
                        wkhtmltopdf($bodyInvoice, $destination);
                        $params['attachment'] = $destination;
                        $params['body'] = $bodyInvoice;
                    }

                    // Send Email
                    $success = EmailNotification::sendEmail($params, '', array(), array('reference_id' => $paymentToContractorId, 'type' => 'payment_to_contractor'));
				}
				
				catch (Exception $e) {
					echo 'Caught exception: ',  $e->getMessage(), "\n";
				}
                    if ($success) {
                        $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Email sent successfully"));
                    } else {
                        $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Email not sent"));
                    }
                    echo 1;
                    exit;
                }
                //$this->view->error_mesages = $error_mesages;
            

            /*$this->view->estimate = $estimate;
            $this->view->to = $to;
            $this->view->subject = $subject;
            $this->view->body = $body;
            $this->view->cc = isset($cc) ? $cc : '';*/

            echo $this->view->render('payment-report/send-payment-as-email.phtml');
            exit;
        /*} else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Estimate not exist"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }*/
	}
	
	
}
	
	
	
	
	
	
	/*public function sendPaymentAsEmailAction(){ 
		//load models
		$modelPaymentToContractors = new Model_PaymentToContractors();
		$modelContractorServiceBooking = new Model_ContractorServiceBooking();
		
		$paymentToContractorId = $this->request->getParam('id', 0);
		
		$payment = $modelPaymentToContractors->getById($paymentToContractorId);
		$this->view->payment = $payment;
		
		$filters = array();
		$filters['payment_to_contractor_id'] = $paymentToContractorId;
		$filters['paid_status'] =1;
		$filters['customer_payment_status'] =1;
		$bookings = $modelContractorServiceBooking->test($filters);
		$this->view->bookings = $bookings;
		//print_r($contractorInvoice);
		/*
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('sendEstimateAsEmail'));

        


        if (!CheckAuth::checkIfCanHandelAllCompany('estimate', $estimateId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Estimate not belongs to your Company"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        //
        // load models
        //
        $modelUser = new Model_User();
        $modelBooking = new Model_Booking();
        $modelCustomer = new Model_Customer();
        $modelBookingAddress = new Model_BookingAddress();
        $modelBookingEstimate = new Model_BookingEstimate();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();

        //
        // geting data
        //
        $estimate = $modelBookingEstimate->getById($estimateId);
        $modelBookingEstimate->fill($estimate, array('booking'));

        //
        // validation
        //
        if (!$estimate) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Estimate not exist"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        if ($estimate['estimate_type'] == 'booking') {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Estimate has been converted to booking"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        if (!$modelBooking->checkBookingIfAccepted($estimate['booking_id'])) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Booking not accepted"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }


        if ($estimate) {

            //
            // filling extra data
            //
            $customer = $modelCustomer->getById($estimate['booking']['customer_id']);
            $user = $modelUser->getById($estimate['booking']['created_by']);
$view = new Zend_View();
            /*$viewParam = $this->getEstimateViewParam($estimateId, true);
            
            $view->setScriptPath(APPLICATION_PATH . '/modules/estimates/views/scripts/index');
            $view->bookingServices = $viewParam['bookingServices'];
            $view->thisBookingServices = $viewParam['thisBookingServices'];
            $view->priceArray = $viewParam['priceArray'];
            $view->estimate = $viewParam['estimate'];
            $bodyEstimate = $view->render('estimate.phtml');

            $template_params = array(
                //estimate
                '{estimate_num}' => $estimate['estimate_num'],
                '{estimate_created}' => date('d/m/Y', $estimate['created']),
                //booking
                '{booking_num}' => $estimate['booking']['booking_num'],
                '{total_without_tax}' => number_format($estimate['booking']['sub_total'], 2),
                '{gst_tax}' => number_format($estimate['booking']['gst'], 2),
                '{total_with_tax}' => number_format($estimate['booking']['qoute'], 2),
                '{description}' => $estimate['booking']['description'] ? $estimate['booking']['description'] : '',
                '{booking_created}' => date('d/m/Y', $estimate['booking']['created']),
                '{booking_created_by}' => ucwords($user['username']),
                '{booking_start}' => date("F j, Y, g:i a", strtotime($estimate['booking']['booking_start'])),
                '{booking_address}' => get_line_address($modelBookingAddress->getByBookingId($estimate['booking']['booking_id'])),
                '{service}' => nl2br($modelContractorServiceBooking->getBookingAsText($estimate['booking']['booking_id'], true)),
                '{estimate_view}' => $bodyEstimate,
                //customer
                '{customer_name}' => get_customer_name($customer),
                '{customer_first_name}' => isset($customer['first_name']) && $customer['first_name'] ? ucwords($customer['first_name']) : '',
                '{customer_last_name}' => isset($customer['last_name']) && $customer['last_name'] ? ' ' . ucwords($customer['last_name']) : '',
                '{customer_contacts}' => nl2br($modelCustomer->getCustomerContacts($estimate['booking']['customer_id'])),
            );


            $modelEmailTemplate = new Model_EmailTemplate();
            $emailTemplate = $modelEmailTemplate->getEmailTemplate('send_estimate_as_email', $template_params);

            $body = $emailTemplate['body'];
            $subject = $emailTemplate['subject'];
            $to = array();
            if ($customer['email1']) {
                $to[] = $customer['email1'];
            }
            if ($customer['email2']) {
                $to[] = $customer['email2'];
            }
            if ($customer['email2']) {
                $to[] = $customer['email3'];
            }
            $to = implode(',', $to);

            if ($this->request->isPost()) {
                $to = $this->request->getParam('to');
                $cc = $this->request->getParam('cc');
                $subject = $this->request->getParam('subject');
                $body = $this->request->getParam('body');
                $pdf_attachment = $this->request->getParam('pdf_attachment', 0);

                $params = array(
                    'to' => $to,
                    'cc' => $cc,
                    'body' => $body,
                    'subject' => $subject
                );

                $error_mesages = array();
                if (EmailNotification::validation($params, $error_mesages)) {
				try{
                    if (!empty($pdf_attachment)) {
                        // Create pdf
                        $pdfPath = createPdfPath();
                        $destination = $pdfPath['fullDir'] . $viewParam['estimate']['estimate_num'] . '.pdf';
                        wkhtmltopdf($bodyEstimate, $destination);
                        $params['attachment'] = $destination;
                    }

                    // Send Email
                    $success = EmailNotification::sendEmail($params, '', array(), array('reference_id' => $estimate['id'], 'type' => 'estimate'));
				}
				
				catch (Exception $e) {
					echo 'Caught exception: ',  $e->getMessage(), "\n";
				}
                    if ($success) {
                        $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Email sent successfully"));
                    } else {
                        $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Email not sent"));
                    }
                    echo 1;
                    exit;
                }
                $this->view->error_mesages = $error_mesages;
            }

            $this->view->estimate = $estimate;
            $this->view->to = $to;
            $this->view->subject = $subject;
            $this->view->body = $body;
            $this->view->cc = isset($cc) ? $cc : '';/

            echo $this->view->render('payment-report/send-payment-as-email.phtml');
            exit;
        /*} else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Estimate not exist"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }/
	}
	
	
}