<?php

class Model_MongoTest {

    protected $_dbhost = '127.0.0.1';
    protected $_dbName = 'test';
    protected $_collectionName = 'salim';
    protected $_con;
    protected $_collection;
    protected $_db;

    //protected static $db;

    /**
     * __construct
     * 
     * @param array $data as data
     * @param array $config as array of config
     * 
     * @return null
     */
    public function __construct($data = array(), $config = array()) {
        $dbhost = 'localhost';
        // Connect to test database  
        $this->_con = new Mongo("mongodb://127.0.0.1");

        //$db = $con->cleaning_service;
        $this->_db = $this->_con->selectDB($this->_dbName);

        $this->_collection = $this->_db->selectCollection($this->_collectionName);
    }

    public function getAll($filters = null) {

        $model_user = new Model_User();
        $modelAuthRoleCredential = new Model_AuthRoleCredential();

        $user = $model_user->getById($filters['user_id']);
        $user_id = (int) $user['user_id'];
        $company_id = (int) CheckAuth::getCompanySession();
        $credential = $modelAuthRoleCredential->getCredentialsByParentName('notifications', $user['role_id'], $user['active']);
        $notifications = 0;
        $notification_type = $filters['notification_type'];
//        var_dump($notification_type);
        // var_dump($credential);

        $count = 0;
        $where = array();
        if ($credential) {

            foreach ($credential as $key => $value) {
                $condition = array('case' => strtolower(spaceBeforeCapital($value['credential_name'])));
                $where[] = $condition;
            }
            $notification_count = $this->_collection->find(array('$and' => array(
                            array('created_by' => array('$ne' => $user_id)),
                            array('notify_user_id' => $user_id),
                            array('notification_type' => $notification_type),
                            array('show' => (int) 1),
                            array('$or' => $where))))
                    ->count();
//            var_dump($notification_count);
            if ($notification_count) {
                $notifications = $this->_collection->find(array('$and' => array(
                                array('created_by' => array('$ne' => $user_id)),
                                array('notify_user_id' => $user_id),
                                array('notification_type' => $notification_type),
                                array('show' => (int) 1),
//                                array('company_id' => $company_id),
                                array('$or' => $where))))
                        ->sort(array('_id' => -1));
            }

            if (isset($filters['count'])) {
                $count = $this->_collection->find(array('$and' => array(
                                array('created_by' => array('$ne' => $user_id)),
                                array('notify_user_id' => $user_id),
                                array('notification_type' => $notification_type),
                                array('show' => 1),
                                array('seen' => 0),
                                array('$or' => $where))))
                        ->count();

                return $count;
            } else {
                return $notifications;
            }
        } else {
            return 0;
        }
    }

    public function resetCount($filters = null) {

        $notifications = $this->getAll($filters);
        if (!empty($notifications)) {
            foreach ($notifications as $key => $value) {
                $this->_collection->update(
                        array('$and' => array(array('_id' => new MongoId($key)))), array('$set' => array('seen' => 1)), array('multiple' => true)
                );
            }
        }
        $filters['count'] = 1;
        $newCount = $this->getAll($filters);
        return $newCount;
    }

    // type  
    // all : read all notification 
    // one : read one notification 
    public function read($filters = null) {
        if (isset($filters['notification_id'])) {
            $notification_id = $filters['notification_id'];
            $this->_collection->update(
                    array('$and' => array(array('_id' => new MongoId($notification_id)))), array('$set' => array('read' => 1)), array('multiple' => true)
            );
        } else {
            $notifications = $this->getAll($filters);
            if (!empty($notifications)) {
                foreach ($notifications as $key => $value) {
                    $this->_collection->update(
                            array('$and' => array(array('_id' => new MongoId($key)))), array('$set' => array('read' => 1)), array('multiple' => true)
                    );
                }
            }
        }
        return 1;
    }

    public function changeShowByCreadentailIdAndNotifyUserId($credential_id, $notify_user_id, $status) {
        $this->_collection->update(
                array('$and' => array(array('credential_id' => (int) $credential_id),
                array('notify_user_id' => (int) $notify_user_id))), array('$set' => array('show' => (int) $status)), array('multiple' => true)
        );
    }

    public function insertNotification($doc = array()) {
        return $this->_collection->insert($doc);
    }

}
