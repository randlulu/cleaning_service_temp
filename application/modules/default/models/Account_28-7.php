<?php

class Model_Account extends Zend_Db_Table_Abstract {

    protected $_name = 'account';

    /**  get table row according to the assigned id * */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function insertInHistoryAccountById($id, $userID, $reason) {
        $account = $this->getById($id);

        $data = array(
            'account_id' => $account['id'],
            'company_id' => $account['company_id'],
            'plan_id' => $account['plan_id'],
            'update_by' => $userID,
            'reason' => $reason,
            'account_status' => $account['account_status'],
            'trial_end_date' => $account['trial_end_date'],
            'created' => $account['created'],
            'from' => $account['from'],
            'to' => $account['to'],
            'paid_user_count' => $account['paid_user_count'],
            'total_amount' => $account['total_amount']
        );
        $account_history = new Model_AccountHistory();
        $account_history->insert($data);
    }

    public function getByCompanyId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("company_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**  get table row according to the assigned $created_by id * */
    public function getByCreatedBy($created_by) {
        $id = (int) $created_by;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("created_by = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**  get user row according to the assigned Account id * */
    public function getUserById($created_by) {
        $id = (int) $created_by;
        $select = $this->getAdapter()->select();
        $select->from(array('usr' => 'user'));
        $select->where("usr.user_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function getUsersByCompanyId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from(array('usr' => 'user'));
        $select->joinInner(array('uc' => 'user_company'), 'usr.user_id = uc.user_id', '');
        $select->where("uc.company_id = '{$id}'");

        return $this->getAdapter()->fetchAll($select);
    }

    /**  get company row according to the assigned account id * */
    public function getCompanyById($compny_id) {
        $id = (int) $compny_id;
        $select = $this->getAdapter()->select();
        $select->from(array('cmp' => 'company'));

        $select->where("cmp.company_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**  get plan row according to the assigned account id * */
    public function getPlanById($plan_id) {
        $id = (int) $plan_id;
        $select = $this->getAdapter()->select();
        $select->from(array('pln' => 'plan'));
        $select->where("pln.plan_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /** get all plan information                          * */
    public function getAllPlan() {
        $select = $this->getAdapter()->select();
        $select->from('plan');
        return $this->getAdapter()->fetchAll($select);
    }

    //** get all row from account                         */
    public function getAll($filters = array(), $order = null, &$pager = null, $limit = 0, $perPage = 0, $currentPage = 0) {
        $select = $this->getAdapter()->select();
        $select->from(array('acc' => $this->_name));
        $select->order($order);
        $select->distinct();

        if ($order) {
            $select->order($order);
        }

        $wheresAndJoins = $this->getWheresAndJoinsByFilters($filters);
        $joinInner = $wheresAndJoins['joinInner'];
        $wheres = $wheresAndJoins['wheres'];
        $groupBy = $wheresAndJoins['groupBy'];

        if ($wheres) {
            foreach ($wheres as $where) {
                $select->where($where);
            }
        }

        if ($joinInner) {
            foreach ($joinInner as $inner) {
                $select->joinInner($inner['name'], $inner['cond'], $inner['cols']);
            }
        }
        /*         * *Select Group BY **IBM */
        if ($groupBy) {
            foreach ($groupBy as $group) {
                $select->group($group);
            }
        }
        /*         * End Select Group By */

        if ($pager) {
            if (!empty($filters['perPage'])) {
                $select->limit($filters['perPage'], ($pager->currentPage - 1) * $filters['perPage']);
            } else {
                $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            }

            $pager->dbSelect = $select;
        } elseif ($perPage && $currentPage) {
            $select->limit($perPage, ($currentPage - 1) * $perPage);
        } else {
            if (!empty($filters['perPage'])) {
                $select->limit($filters['perPage']);
            }
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /** delete table row according to the assigned id */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("id = '{$id}'");
    }

    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "id = '{$id}'");
    }

    public function getWheresAndJoinsByFilters($filters) {

        $select = $this->getAdapter()->select();
        $select->from(array('acc' => $this->_name));

        $wheres = array();
        $joinInner = array();
        $groupBy = array();

        if ($filters) {

            if (!empty($filters['subscription_status'])) {
                //$joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.booking_id = bok.booking_id', 'cols' => '');
                switch ($filters['subscription_status']) {

                    case 'non-subscriber':
                        $wheres['subscription_status'] = (" acc.account_status = 'Non-subscriber'");
                        break;
                    case 'subscriber-trial':
                        $wheres['subscription_status'] = (" acc.account_status = 'Subscriber-trial'");
                        break;
                    case 'subscriber-payed':
                        $wheres['subscription_status'] = (" acc.account_status = 'Subscriber-paied'");
                        break;
                    case 'canceled':
                        $wheres['subscription_status'] = (" acc.account_status = 'Canceled'");
                        break;
                    case 'suspended':
                        $wheres['subscription_status'] = (" acc.account_status = 'Suspended'");
                        break;
                }
            }
            if (!empty($filters['plan_id'])) {
                $wheres['plan_id'] = (" acc.plan_id = '" . $filters['plan_id'] . "'");
            }
        }

        return array('wheres' => $wheres, 'joinInner' => $joinInner, 'groupBy' => $groupBy);
    }

    public function getCount($filters = array()) {
        $count = $this->getAll($filters);
        return count($count);
    }

    //THIS FUNCTION TO CHECK IF A CUSTOME SUBSCRIPION PAYED OR NOT OR IT IS IN TRIAL MODE........
    public function checkSubscriptionPaymentStatus($account_id) {
        $subscriptionPaymentObj = new Model_SubscriptionPayment();
        $subscriptionPaymentInfo = $subscriptionPaymentObj->getPaymentByAccountID($account_id);
        if (empty($subscriptionPaymentInfo)) {
            return 'Trial';
        } elseif ($subscriptionPaymentInfo['end_date'] > time()) {
            return 'Payed';
        } else {
            return 'Not Payed';
        }
    }

    //THIS FUNCTION CALCULATE THE PROFILE COMPLETNESS AND HELP SALES TO MAKE DECISION
    public function calculateProfileCompleteness($account_id) {

        $accountInfo = $this->getById($account_id);
        $userInfo = $this->getUserById($accountInfo['created_by']);
        $CompanyInfo = $this->getCompanyById($accountInfo['company_id']);
        $usersInfo = $this->getUsersByCompanyId($accountInfo['company_id']);

        $completeness = 0;
        if ($userInfo['username'] && $CompanyInfo['company_name']) {
            $completeness +=25;
        }
        if ($CompanyInfo['company_name'] && $CompanyInfo['company_abn'] && $CompanyInfo['company_postcode'] && $CompanyInfo['company_invoices_email'] && $CompanyInfo['company_accounts_email'] && $CompanyInfo['company_complaints_email'] && $CompanyInfo['company_phone1'] && $CompanyInfo['company_mobile1']) {
            $completeness +=25;
        }
        if ($userInfo['first_name'] && $userInfo['last_name'] && $userInfo['street_number'] && $userInfo['street_address'] && $userInfo['email1'] && $userInfo['mobile1']) {
            $completeness +=25;
        }

        if (count($usersInfo) > 1) {
            $completeness +=25;
        }
        return $completeness;
    }

    public function sendActivationEmail($email, $hash_code, $firstName, $user) {
        /////////////////////////////send email invitation to new user //////////////////////////////////////////

        $template_params = array(
            '{admin_name}' => $user['first_name'],
            '{user_activecode}' => 'http://temp.tilecleaners.com.au/getinvitation/' . $email . '/' . $hash_code
        );

        //we got customer info to use company_id in preparing email template

        $to[] = $email;

        $to = implode(',', $to);

        $modelEmailTemplate = new Model_EmailTemplate();
        $emailTemplate = $modelEmailTemplate->getEmailTemplate('octopuspro_invite', $template_params);

        //$username = $userAdmin ['first_name'] ;
        $image1 = $_SERVER['SERVER_NAME'] . '/emaillogo.png';
        $image2 = $_SERVER['SERVER_NAME'] . "/bkemail.png";

        //$username = $userAdmin['username'];

        $href = 'http://temp.tilecleaners.com.au/getinvitation/' . $email . '/' . $hash_code;

        $bodyhtml = '
			<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" media="screen">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<script src="http://code.jquery.com/jquery.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Open+Sans|Montserrat" rel="stylesheet" type="text/css">
</head>
<body>
 <table border="0" cellpadding="0" align="center"  width="600" style="border-collapse: collapse;">
  <tr>
    <td align="center" bgcolor="#fff" >
      <img src="' . $image1 . '" alt="Creating Email Magic" width="300" height="56" style="display: block;" />
    </td>
 </tr>
 <tr style="background:url(' . $image2 . '); height:600px;">

  <td style="padding:260px 50px 0px 50px; text-align:center; color:#fff; font-size:18px;"> 
      ' . $username . '
	  <br></br>
 Invited you to join their team

      </br>
	  <a href="' . $href . '" > 
	  <div style="padding:6px 30px 6px 30px; text-align:center; color:#fff; font-size:18px; margin:40px auto ;
	        width:260px; background:#0091d7; border-radius:10px; ">
             Accept Invite
	  </div>
	  </a>
  </td>
 </tr>
 </tr>
 </table>
	
	<style>
	  a, a:hover{
	   color:#FFF;
	   text-decoration:none;
	  }
	</style>
</body>
</html>';


        $params = array(
            'to' => $to,
            'body' => $bodyhtml,
            'subject' => '(octopuspro)�?',
            'from' => 'admin@octupospro.com',
            'name' => 'octupospro'
        );

        try {
            EmailNotification::sendEmail($params, '', array(), array(), 1);
            $sent = 'done';
        } catch (Zend_Mail_Transport_Exception $e) {
            $sent = 'error';
            var_dump('error');
            exit;
        }
    }

    public function remindDays($to) {
        //return 15; 
        // it deffirence Now from To  to remind positive days 
        $now = time(); // or your date as well

        $to = strtotime($to);

        //return $to;
        $datediff = $to - $now;
        return floor($datediff / (60 * 60 * 24));
    }

    public function accountExpireReminderBeforeTwoDayCronJob() {

        $modelAuthRole = new Model_AuthRole();
        $contractorRole = $modelAuthRole->getRoleIdByName('account_admin');

        $select = $this->getAdapter()->select();
        $select->from(array('u' => 'user'));
        $select->joinInner(array('acc' => 'account'), 'acc.created_by = u.user_id', array('id'));
        $select->where('u.active = true');
        $select->where("u.role_id = '{$contractorRole}'");

        $subscribers = $this->getAdapter()->fetchRow($select);

        foreach ($subscribers as $subscriber) {

            $expired_accounts = $this->getExpiredAccount($subscriber['id']);
            $expired_accounts_text = '';
            if (isset($expired_accounts) && !empty($expired_accounts)) {

//                var_dump($expired_accounts);
//
                foreach ($expired_accounts as $expired_account) {
                    $accountDetails = '<p>start date : ' . $expired_account['from'] .
                            '<br/> expire date : ' . date(get_config('date'), $expired_account['to']) . '<br/></p>';
                    $expired_accounts_text .= $accountDetails;
                }
                
                $modelUser = new Model_User();
                $user = $modelUser->getById($subscriber['created_by']);

                $modelUserCompanies = new Model_UserCompanies();
                $userCompany = $modelUserCompanies->getByUserId($subscriber['created_by']);

                $modelTradingName = new Model_TradingName();
                $trading_name = $modelTradingName->getDefualtTradingName($subscriber['company_id']);
                $template_params = array(
                    //booking
                    '{display_name}' => $user['display_name'],
                    '{sender_name}' => isset($user['first_name']) && $user['first_name'] ? ucwords($user['first_name']) : '',
                    '{trading_name}' => $trading_name['trading_name'],
                    '{website_url}' => $trading_name['website_url'],
                    '{phone}' => $trading_name['phone'],
                    '{expired_insurances}' => $expired_insurances_text
                );


                $to = array();
                if ($user['email1']) {
                    $to[] = $user['email1'];
                }
                if ($user['email2']) {
                    $to[] = $user['email2'];
                }
                if ($user['email2']) {
                    $to[] = $user['email3'];
                }

                $to = array('arabidmohamed@gmail.com');
                $to = implode(',', $to);
                if ($to) {
                    echo 'here';
                    $email_log = array();
                    EmailNotification::sendEmail(array('to' => $to), 'account_expire_reminder ', $template_params, $email_log, $userCompany['company_id']);
                }
            }
        }
    }

    public function getExpiredAccount($accountID) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        //$select->where("id = '{$accountID}'");
        $select->where("Date(to)");
       	//$select->where("Date(from_unixtime(to)) between  Date(date_sub(now(),INTERVAL 2 WEEK)) and Date(now()) OR Date(from_unixtime(to)) < Date(now())");
        return $this->getAdapter()->fetchAll($select);
    }

}
