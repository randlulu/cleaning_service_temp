<?php

class Model_InquiryReminder extends Zend_Db_Table_Abstract {

    protected $_name = 'inquiry_reminder';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from(array('el' => $this->_name));
        $select->order($order);


        if ($filters) {
            if (!empty($filters['inquiry_id'])) {
                $inquiry_id = (int) $filters['inquiry_id'];
                $select->where("el.inquiry_id = '{$inquiry_id}'");
            }
        }


        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }
        return $this->getAdapter()->fetchAll($select);
    }

    public function getContactHistory($inquiry_id) {
        $inquiry_id = (int) $inquiry_id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("inquiry_id = '{$inquiry_id}'");
        $select->order('created DESC');
        $select->limit(4);

        return $this->getAdapter()->fetchAll($select);
    }
    
    public function getContactHistoryWithoutLimit($inquiry_id,$is_count = 0) {
        $inquiry_id = (int) $inquiry_id;
        $select = $this->getAdapter()->select();
        if($is_count){
		 $select->from($this->_name,array('count'=>'count(DISTINCT(id))'));
		}else{
		  $select->from($this->_name);
		}
        $select->where("inquiry_id = '{$inquiry_id}'");
        $select->order('created DESC');

        if($is_count){
		 return $this->getAdapter()->fetchOne($select);
		}else{
         return $this->getAdapter()->fetchAll($select);
		}
    }

    /**
     * update table rows according to the assigned id and data
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        
        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'changed');
        
        return parent::update($data, "id = '{$id}'");
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getByInquiryId($inquiry_id) {
        $inquiry_id = (int) $inquiry_id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("inquiry_id = '{$inquiry_id}'");

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        
        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'deleted');
        
        return parent::delete("id = '{$id}'");
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function insert(array $data) {

        $id = parent::insert($data);
        
        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'added');
        
        return $id;
    }
}
