<?php

class Model_InquiryTypeAttribute extends Zend_Db_Table_Abstract {

    protected $_name = 'inquiry_type_attribute';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from(array('ita' => $this->_name));
        $select->joinInner(array('it' => 'inquiry_type'), 'ita.inquiry_type_id=it.inquiry_type_id', array('it.inquiry_name'));
        $select->joinInner(array('a' => 'attribute'), 'ita.attribute_id=a.attribute_id', array('a.attribute_name'));
        $select->order($order);

        if ($filters) {
            if (!empty($filters['inquiry_type_id'])) {
                $select->where("it.inquiry_type_id = {$filters['inquiry_type_id']}");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table rows according to the assigned id and date
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "inquiry_type_attribute_id = '{$id}'");
    }

    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("inquiry_type_attribute_id = '{$id}'");
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("inquiry_type_attribute_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the service id
     * 
     * @param int $id
     * @return array 
     */
    public function getByServiceId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("inquiry_type_id = '{$id}'");

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * get table row according to the attribute id
     * 
     * @param int $id
     * @return array 
     */
    public function getByAttributeId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("attribute_id= '{$id}'");

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * get table row according to the attribute id and inquiry type id
     * 
     * @param int $id
     * @param int $inquiryTypeId
     * @return array 
     */
    public function getByAttributeIdAndInquiryTypeId($id, $inquiryTypeId) {
        $id = (int) $id;
        $inquiryTypeId = (int) $inquiryTypeId;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("attribute_id= '{$id}'");
        $select->where("inquiry_type_id= '{$inquiryTypeId}'");

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * get attribute according to the inquiry type id
     * 
     * @param int $inquiryTypeId
     * @return array 
     */
    public function getAttributeByInquiryTypeId($inquiryTypeId) {
        $inquiryTypeId = (int) $inquiryTypeId;

        $select = $this->getAdapter()->select();
        $select->from(array('ia' => $this->_name));
        $select->joinInner(array('a' => 'attribute'), 'ia.attribute_id=a.attribute_id', array('a.attribute_name', 'a.is_price_attribute', 'a.attribute_type_id', 'a.extra_info'));
        $select->where("inquiry_type_id = '{$inquiryTypeId}'");

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * get table row according to the attribute id inquiry_type_id
     * 
     * @param type $attributeId
     * @param type $inquiryTypeId
     * @return type 
     */
    public function getByAttributeIdInquiryTypeId($attributeId, $inquiryTypeId) {
        $attributeId = (int) $attributeId;
        $inquiryTypeId = (int) $inquiryTypeId;

        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("attribute_id= '{$attributeId}'");
        $select->where("inquiry_type_id= '{$inquiryTypeId}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function checkBeforeDeleteIinquiryTypeAttribute($inquiryTypeAttributeId, &$tables = array()) {

        $sucsess = true;

        $select_inquiry_type_attribute_value = $this->getAdapter()->select();
        $select_inquiry_type_attribute_value->from('inquiry_type_attribute_value', 'COUNT(*)');
        $select_inquiry_type_attribute_value->where("inquiry_type_attribute_id = {$inquiryTypeAttributeId}");
        $count_inquiry_type_attribute_value = $this->getAdapter()->fetchOne($select_inquiry_type_attribute_value);

        if ($count_inquiry_type_attribute_value) {
            $tables[] = 'inquiry_type_attribute_value';
            $sucsess = false;
        }

        return $sucsess;
    }

}