<?php

class Model_AttributeListValueAttachment extends Zend_Db_Table_Abstract {

    protected $_name = 'attribute_list_value_attachment';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from(array('a' => $this->_name));
        $select->order($order);

        if ($filters) {
            if (!empty($filters['attribute_list_value_id'])) {
                $select->where("a.attribute_list_value_id = {$filters['attribute_value_id']}");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "attribute_list_value_attachment_id = '{$id}'");
    }

    /**
     * delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean 
     */
	 
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("attribute_list_value_attachment_id = '{$id}'");
    }
	
	public function deleteByAttributeValueId($id) {
        $id = (int) $id;
        return parent::delete(" attribute_list_value_id = '{$id}'");
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where(" attribute_list_value_attachment_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the assigned attributeValue
     * 
     * @param string $attributeValue
     * @return array
     */
	 
    public function getByAttributeValueId($attributeValueId , $all_attachment = false , $just_images = false) {
	    $attributeValueId = (int) $attributeValueId ; 
        $select = $this->getAdapter()->select();
        $select->from(array('a'=>$this->_name));
        $select->joinInner(array('attach'=>'attachment') , 'a.attachment_id = attach.attachment_id');
        $select->where("attribute_list_value_id = '{$attributeValueId}'");
        $select->where("attach.is_deleted = '0'");
		if(!($all_attachment)){
		  $select->where("is_default = '0'");
		}		
		if($just_images){
		  $select->where("attach.type like '%image%'");
		}
        return $this->getAdapter()->fetchAll($select);
    }
	
	public function getByAttributeValueIdAndIsDefault($attributeValueId) {
        $attributeValueId = (int) $attributeValueId ; 
        $select = $this->getAdapter()->select();
        $select->from(array('a'=>$this->_name));
        $select->joinInner(array('attach'=>'attachment') , 'a.attachment_id = attach.attachment_id');
        $select->where("is_default = '1'");
        $select->where("attribute_list_value_id = '{$attributeValueId}' ");

        return $this->getAdapter()->fetchRow($select);
    }
    
    
    public function getAttachmentsForUpdate() { 
        $select = $this->getAdapter()->select();
        $select->from(array('sa'=>$this->_name));
        $select->joinInner(array('attach'=>'attachment') , 'sa.attachment_id = attach.attachment_id');
		$select->where("attach.is_deleted = '0'");
		
		return $this->getAdapter()->fetchAll($select);
	}

}