<?php

class Model_UnsubscribeReason extends Zend_Db_Table_Abstract {

    protected $_name = 'unsubscribe_reason';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from(array('a' => $this->_name));
        $select->order($order);

        $filters['company_id'] = CheckAuth::getCompanySession();

        if ($filters) {
            if (!empty($filters['keywords'])) {
                $keywords = $this->getAdapter()->quote('%' . $filters['keywords'] . '%');
                $select->where("a.reason_text LIKE {$keywords}");
            }

            if (!empty($filters['company_id'])) {
                $select->where("a.company_id = {$filters['company_id']} OR a.company_id = 0");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

	
	 public function checkBeforeDeleteUnsubscribeReason($unsubscribe_reason_id, &$tables = array()) {

        $sucsess = true;

        $select = $this->getAdapter()->select();
        $select->from('mailing_list_unsubscribed', 'COUNT(*)');
        $select->where("unsubscribe_reason_id = {$unsubscribe_reason_id}");
        $count_rows = $this->getAdapter()->fetchOne($select);

        if ($count_rows) {
            $tables[] = 'mailing list unsubscribed';
            $sucsess = false;
        }

        return $sucsess;
    }
	
    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "unsubscribe_reason_id = '{$id}'");
    }

    /**
     * delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("unsubscribe_reason_id = '{$id}'");
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("unsubscribe_reason_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the assigned variable name
     * 
     * @param string $variable_name
     * @return array
     */


    /**
     * get table rows as array
     * 
     * @return array
     */
	 
    public function getReasonsAsArray(){

        $reasons = $this->getAll();

        $data = array();
        foreach ($reasons as $reason) {
            $data[] = array(
                'id' => $attribute['unsubscribe_reason_id'],
                'name' => $attribute['reason_text']
            );
        }
        return $data;
    }


}