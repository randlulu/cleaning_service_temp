<?php

class Model_ImageAttachment extends Zend_Db_Table_Abstract {

    protected $_name = 'image_attachment';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from(array('ia' => $this->_name));
        $select->order($order);

        if ($filters) {

            if (!empty($filters['keywords'])) {
                $keywords = $this->getAdapter()->quote('%' . $filters['keywords'] . '%');
                $select->where("ia.description LIKE {$keywords}");
            }

            if (!empty($filters['service_id'])) {
                $service_id = $this->getAdapter()->quote($filters['service_id']);
                $select->where("ia.service_id = {$service_id}");
            }

            if (!empty($filters['attribute_list_value_id'])) {
                $attribute_list_value_id = $this->getAdapter()->quote($filters['attribute_list_value_id']);
                $select->where("ia.attribute_list_value_id = {$attribute_list_value_id}");
            }
        }


        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }
        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table rows according to the assigned id and data
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "id = '{$id}'");
    }

    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("id = '{$id}'");
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getByAttributListValueId($attributListValueId, $limit=0) {
        $attributListValueId = (int) $attributListValueId;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("attribute_list_value_id = '{$attributListValueId}'");
        $select->order('RAND()');
        if ($limit) {
            $select->limit($limit);
        }
     
        return $this->getAdapter()->fetchAll($select);
    }

}