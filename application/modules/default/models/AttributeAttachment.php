<?php

class Model_AttributeAttachment extends Zend_Db_Table_Abstract {

    protected $_name = 'attribute_attachment';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from(array('aa' => $this->_name));
        $select->order($order);

        if ($filters) {
            if (!empty($filters['attribute_id'])) {
                $select->where("aa.attribute_id = {$filters['attribute_id']}");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "attribute_attachment_id = '{$id}'");
    }

    /**
     * delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean 
     */
	 
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("attribute_attachment_id = '{$id}'");
    }
	
	public function deleteByAttributeId($id) {
        $id = (int) $id;
        return parent::delete("attribute_id = '{$id}'");
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where(" attribute_attachment_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the assigned attributeValue
     * 
     * @param string $attributeValue
     * @return array
     */
	 
    public function getByAttributeId($attributeId , $check_default = null) {
	    $attributeId = (int) $attributeId ; 
        $select = $this->getAdapter()->select();
        $select->from(array('aa'=>$this->_name));
        $select->joinInner(array('attach'=>'attachment') , 'aa.attachment_id = attach.attachment_id');
        $select->where("attribute_id = '{$attributeId}'");
        $select->where("attach.is_deleted = '0'");
		if(isset($check_default)){
		 $select->where("is_default = '{$check_default}'");
		}
		if($check_default == 1){
		  return $this->getAdapter()->fetchRow($select);
		}
		
		//echo $select->__toString();
        //$select->where("is_default = '0'");
        return $this->getAdapter()->fetchAll($select);
    }
	
	
    
    
    


}