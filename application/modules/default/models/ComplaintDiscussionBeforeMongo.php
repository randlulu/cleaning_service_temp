<?php

class Model_ComplaintDiscussionBeforeMongo extends Zend_Db_Table_Abstract {

    protected $_name = 'complaint_discussion';

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateById($id, $data) {
        $id = (int) $id;

        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'changed');

        return parent::update($data, "discussion_id = '{$id}'");
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("discussion_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get all the recent discussions that the user are able to see
     *  
     * @return array  
     */
    public function getRecentDiscussion() {

        $loggedUser = CheckAuth::getLoggedUser();
        $userId = $loggedUser['user_id'];

        $select = $this->getAdapter()->select();

        $select->distinct();

        $select->from(array('id' => $this->_name));

        $select->joinInner(array('csb' => 'contractor_service_booking'), 'id.estimate_id = csb.booking_id', 'csb.contractor_id');

        if (!CheckAuth::checkCredential(array('canSeeAllComplaintDiscussion'))) {
            if (CheckAuth::checkCredential(array('canSeeOnlyHisComplaintDiscussion'))) {
                if (CheckAuth::checkCredential(array('canSeeAssignedComplaintDiscussion'))) {
                    $select->where("id.user_id = {$userId} OR csb.contractor_id = {$userId}");
                } else {
                    $select->where("id.user_id = {$userId}");
                }
            }
        }

        $select->order("created DESC");

        $select->limit(20);

        return $this->getAdapter()->fetchall($select);
    }

    /**
     * get table row according to the assigned estimateId
     * 
     * @param int $id
     * @return array
     */
	 
    /*public function getByComplaintId($id, $order ='created DESC') {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order($order);
        $select->where("complaint_id = '{$id}'");

        return $this->getAdapter()->fetchAll($select);
    }*/
	
	public function getByComplaintId($id, $order ='created DESC',$filter = array()) {
        $id = (int) $id;
		$select = $this->getAdapter()->select();
        $select->from(array('cd'=>$this->_name));
		$typeFilter = $this->getAdapter()->quote('complaint' . '%');	
		$select->joinLeft(array('iid' => 'item_image_discussion'), " iid.item_id = cd.discussion_id AND iid.type LIKE {$typeFilter}",array('group_id'));
		$select->joinLeft(array('im' => 'image'), "(iid.image_id = im.image_id )",array('im.compressed_path','im.thumbnail_path','im.image_id','role_id'=>'im.user_role'));
		if(isset($filter['groups'])){
		 $select->where("iid.group_id > 0");
		 $select->group("iid.group_id");
		}else{		
         $select->where("iid.group_id = 0 OR iid.group_id IS NULL");		 
		}
		$select->order("cd.{$order}");
        //$select->where("cd.complaint_id = '{$id}'");
		$select->where("is_deleted = 0");

        return $this->getAdapter()->fetchAll($select);
    }
	
	
		public function getDiscussionByImageId($id,$order = 'asc') {
		$id = (int) $id;
		$typeFilter = $this->getAdapter()->quote('complaint' . '%');	
        $select = $this->getAdapter()->select();
        $select->from(array('id'=>$this->_name));
		$select->joinInner(array('iid' => 'item_image_discussion'), " iid.item_id = id.discussion_id AND iid.type LIKE {$typeFilter}",array('group_id'));
		$select->order("id.created {$order}");
        $select->where("iid.image_id = '{$id}'");
  		
        return $this->getAdapter()->fetchAll($select);
    }
	

    public function insert(array $data) {

        $id = parent::insert($data);

        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'added');

		/**
         * send notification
         */
		//at first get booking_id by complaint id
		$modelComplaint = new Model_Complaint();
		$complaint = $modelComplaint->getById($data['complaint_id']);
	
     //   MobileNotification::notify($complaint['booking_id'],'new comment');

        return $id;
    }
	
	
	public function getLastDiscussionByComplaintId($complaintId){
	  
	    $complaintId = (int) $complaintId;
        $select = $this->getAdapter()->select();
        $select->from(array('d' => $this->_name) , array('MAX(discussion_id) as discussion'));
        $select->joinInner(array('c' => 'complaint_temp'), 'c.complaint_id = d.complaint_id', 'c.complaint_status');
        $select->where("d.complaint_id = '{$complaintId}'");
        $select->where("c.complaint_status = 'closed'");
	
        return $this->getAdapter()->fetchRow($select);
	
	}

}