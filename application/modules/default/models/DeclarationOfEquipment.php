<?php
  
class Model_DeclarationOfEquipment extends Zend_Db_Table_Abstract {

    protected $_name = 'declaration_of_equipment';

    /**
     *get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from(array('dof' => $this->_name));
        $select->joinInner(array('ci' => 'contractor_info'), 'ci.contractor_info_id=dof.contractor_info_id', array('ci.contractor_id'));
        $select->joinInner(array('u' => 'user'), 'ci.contractor_id=u.user_id', array('u.username'));
        $select->order($order);

        if ($filters) {
            if (!empty($filters['contractor_info_id'])) {
                $select->where("dof.contractor_info_id = {$filters['contractor_info_id']}");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     *update table rows according to the assigned id and data
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        //return parent::update($data, "id = '{$id}'");
        $modelUser = new Model_User();
        $contractorDeclaretionOfEquipment = $this->getById($id);

        $modelContractorInfo = new Model_ContractorInfo();
        $contractorInfo = $modelContractorInfo->getById($contractorDeclaretionOfEquipment['contractor_info_id']);
        $ret_val = parent::update($data, "id = '{$id}'");
        if ($ret_val) {
            $modelUser->calculateProfileCompleteness($contractorInfo['contractor_id']);
        }
        return $ret_val;
    }

    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        //return parent::delete("id = '{$id}'");
        $modelUser = new Model_User();
        $contractorDeclaretionOfEquipment = $this->getById($id);

        $modelContractorInfo = new Model_ContractorInfo();
        $contractorInfo = $modelContractorInfo->getById($contractorDeclaretionOfEquipment['contractor_info_id']);
        $ret_val = parent::delete("id = '{$id}'");
        if ($ret_val) {
            $modelUser->calculateProfileCompleteness($contractorInfo['contractor_id']);
        }
        return $ret_val;
    }
	
	public function deleteByContractorInfoId($contractor_info_id){
	 
	 return parent::delete("contractor_info_id = '{$contractor_info_id}'");
	 
	}

    /**
     *get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     *get table row according to the contractor info id
     * 
     * @param int $id
     * @return array 
     */
    public function getByContractorInfoId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("contractor_info_id = '{$id}'");

        return $this->getAdapter()->fetchAll($select);
    }
    
    
     public function getAllAttachmentById($equipment_id){
         
        
	    
        $equipment_id = (int) $equipment_id;
        $select = $this->getAdapter()->select();
        $select->from(array('de' => $this->_name));
		$select->joinInner(array('dea' => 'declaration_of_equipment_attachment'), 'dea.equipment_id = de.id');
		$select->joinInner(array('a' => 'attachment'), 'a.attachment_id = dea.attachment_id');
        $select->where("de.id = '{$equipment_id}'");
        $select->where("a.is_deleted = '0'");
		
        return $this->getAdapter()->fetchAll($select);		
	 
	  
	  }
    
}