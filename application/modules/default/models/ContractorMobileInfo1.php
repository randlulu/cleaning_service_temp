<?php

class Model_ContractorMobileInfo extends Zend_Db_Table_Abstract {

    protected $_name = 'contractor_mobile_info';
    
  
	public function insert(array $data) {
		$id = 0;
        $id = parent::insert($data);
		
        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'added');

        return $id;
    }

	
	public function getByContractorId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from(array('cmi' => $this->_name));
        $select->where("cmi.contractor_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }
	

}