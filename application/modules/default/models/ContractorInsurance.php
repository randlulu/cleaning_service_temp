<?php

class Model_ContractorInsurance extends Zend_Db_Table_Abstract {

    protected $_name = 'contractor_insurance';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
	 
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from(array('ci' => $this->_name));
		$select->joinInner(array('cif' => 'contractor_info'), 'ci.contractor_info_id=cif.contractor_info_id', array('cif.contractor_id'));
		$select->joinInner(array('u' => 'user'), 'cif.contractor_id=u.user_id', array('u.username'));

        $select->order($order);

		if ($filters) {
            if (!empty($filters['contractor_info_id'])) {
                $select->where("ci.contractor_info_id = {$filters['contractor_info_id']}");
            }
			
			if (!empty($filters['insurance_type'])) {
                $select->where("ci.insurance_type = '{$filters['insurance_type']}' ");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
     public function updateById($id, $data) {
        $id = (int) $id;
        //return parent::update($data, "contractor_insurance_id = '{$id}'");
        $modelUser = new Model_User();
        $contractorInsurance = $this->getById($id);

        $modelContractorInfo = new Model_ContractorInfo();
        $contractorInfo = $modelContractorInfo->getById($contractorInsurance['contractor_info_id']);
        $ret_val = parent::update($data, "contractor_insurance_id = '{$id}'");
        if ($ret_val) {
            $modelUser->calculateProfileCompleteness($contractorInfo['contractor_id']);
        }
        return $ret_val;
    }

    /**
     * delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean 
     */
	 
     public function deleteById($id) {
        $id = (int) $id;
        $modelUser = new Model_User();
        $contractorInsurance = $this->getById($id);

        $modelContractorInfo = new Model_ContractorInfo();
        $contractorInfo = $modelContractorInfo->getById($contractorInsurance['contractor_info_id']);
        $ret_val = parent::delete("contractor_insurance_id = '{$id}'");
        if ($ret_val) {
            $modelUser->calculateProfileCompleteness($contractorInfo['contractor_id']);
        }
        return $ret_val;
    }
	
	public function deleteByContractorInfoId($contractor_info_id){
	 
	 return parent::delete("contractor_info_id = '{$contractor_info_id}'");
	 
	}
	
	
	 public function getAllAttachmentById($contractor_insurance_id){
	    
        $contractor_insurance_id = (int) $contractor_insurance_id;
        $select = $this->getAdapter()->select();
        $select->from(array('ci' => $this->_name));
		$select->joinInner(array('cia' => 'contractor_insurance_attachment'), 'cia.contractor_insurance_id = ci.contractor_insurance_id');
		$select->joinInner(array('a' => 'attachment'), 'a.attachment_id = cia.attachment_id');
        $select->where("ci.contractor_insurance_id = '{$contractor_insurance_id}'");
        $select->where("a.is_deleted = '0'");
		
        return $this->getAdapter()->fetchAll($select);		
		 
	  
	  }


    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("contractor_insurance_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }
	
	
 public function getExpiredInsurances($contractorInfoId){
    $select = $this->getAdapter()->select();
	$select->from($this->_name);
	$select->where("contractor_info_id = '{$contractorInfoId}'");
	$select->where("Date(from_unixtime(insurance_policy_expiry)) between  Date(date_sub(now(),INTERVAL 2 WEEK)) and Date(now()) OR Date(from_unixtime(insurance_policy_expiry)) < Date(now())");	
	$select->where("insurance_type = 'Public Liability'");	
	return $this->getAdapter()->fetchAll($select);
 }

 public function insuranceUpdateReminderBeforeTwoWeekCronJob(){
	  
	    $modelAuthRole = new Model_AuthRole();
        $contractorRole = $modelAuthRole->getRoleIdByName('contractor');
		$router = Zend_Controller_Front::getInstance()->getRouter();
		$modelCronjobHistory = new Model_CronjobHistory();
        $modelCronJob = new Model_CronJob();
		
		$cronjob = $modelCronJob->getIdByName('Insurance_Update_Reminder');
        $cronjonID = $cronjob['id'];

        $cronjobHistoryData = array(
            'cron_job_id' => $cronjonID,
            'run_time' => time()
        );
        $cronjobHistoryId = $modelCronjobHistory->insert($cronjobHistoryData);
		
		$select = $this->getAdapter()->select();
		$select->from(array('u'=>'user'));
		$select->joinInner(array('ci'=>'contractor_info'),'ci.contractor_id = u.user_id',array('contractor_info_id'));
		$select->joinInner(array('uc'=>'user_company'),'uc.user_id = u.user_id',array('company_id'));
		$select->where('u.active = true');
		$select->where("u.role_id = '{$contractorRole}'");
		$select->where("uc.company_id = 1 ");
	
		$contractors = $this->getAdapter()->fetchAll($select);
		
		
		foreach($contractors as $contractor){
		   
		   
		   $have_insurance = $this->getAll(array('contractor_info_id'=>$contractor['contractor_info_id'],'insurance_type'=>'Public Liability'));
		   $expired_insurances =  $this->getExpiredInsurances($contractor['contractor_info_id']);
		    $expired_insurances_text = '';
		  if((isset($expired_insurances) && !empty($expired_insurances)) || empty($have_insurance)){ 
		    $expired_link = $router->assemble(array(), 'ContractorInsuranceList');
                  
			$template_params = array(
				//booking
				'{display_name}' => isset($contractor['display_name']) && $contractor['display_name'] ? ucwords($contractor['display_name']) : $contractor['username'],
				'{sender_name}' => '',
				'{expired_insurances}' =>$expired_link
			);
		
		
		$to = array();
        if ($contractor['email1']) {
            $to[] = $contractor['email1'];
        }
        if ($contractor['email2']) {
            $to[] = $contractor['email2'];
        }
        if ($contractor['email2']) {
            $to[] = $contractor['email3'];
        }
        

		
		
		//$to = array('monahussein69@gmail.com','ayman@tilecleaners.com.au');
        $to = implode(',', $to);
		
		if($to){
		 
         try{
			 //type
			 //reference_id
		//cronjob_history_id
            /*   $email_log = array(
			'cronjob_history_id'=>$cronjobHistoryId
                'type' => $type,
                'booking_id' => $bookingId
            );*/


		$email_log = array(
		'cronjob_history_id'=> $cronjobHistoryId
		);
		  EmailNotification::sendEmail(array('to' => $to), 'Insurance Update Reminder ', $template_params, $email_log,$contractor['company_id']);
		  } catch (Zend_Mail_Transport_Exception $e) {
                           
            }
		}
		
		
		   }  
		}
	}

}