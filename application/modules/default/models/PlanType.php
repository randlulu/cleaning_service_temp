<?php

class Model_PlanType extends Zend_Db_Table_Abstract {

    protected $_name = 'plan_type';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order($order);

        if ($filters) {
            if (!empty($filters['keywords'])) {
                $keywords = $this->getAdapter()->quote('%' . $filters['keywords'] . '%');
                $select->where("plan_type_name LIKE {$keywords}");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "plan_type_id = '{$id}'");
    }

    /**
     * delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("plan_type_id = '{$id}'");
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("plan_type_id = {$id}");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the assigned plan Type Id
     * 
     * @param int $id
     * @return array
     */
    public function getPlanTypeName($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name, array('plan_type_name'));
        $select->where("plan_type_id = {$id}");

        return $this->getAdapter()->fetchOne($select);
    }

        /**
     * get table row according to the assigned plan Type Id
     * 
     * @param int $id
     * @return array
     */
    public function getViewPlanTypeName($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name, array('view_plan_type_name'));
        $select->where("plan_type_id = {$id}");

        return $this->getAdapter()->fetchOne($select);
    }


    /**
     * get table row according to the assigned plan Type Name
     * 
     * @param string $name
     * @return array
     */
    public function getPlanTypeIdByName($name) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name, array('plan_type_id'));
        $select->where("plan_type_name = '{$name}'");
        return $this->getAdapter()->fetchOne($select);
    }

    /**
     * get table rows as array
     * 
     * @return array
     */
    public function getPlanTypeAsArray() {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order('plan_type_id asc');

        $planTypes = $this->getAdapter()->fetchAll($select);

        $data = array();
        foreach ($planTypes as $planType) {
            $data[$planType['plan_type_id']] = $planType['view_plan_type_name'];
            // $data[] = array(
            //     'id' => $planType['plan_type_id'],
            //     'name' => $planType['view_plan_type_name']
            // );
        }
        return $data;
    }
    /**
     * create plan Name with slug filters 
     * 
     * @param string $string
     * @return string 
     */
    public function createPlanTypeName($string) {

        $slug = preg_replace('#[\:\;\"\'\|\<\,\>\.\?\`\~\!\@\#\$\%\^\&\*\(\)\_\-\+\=\{\[\}\]\/\t\n]#', ' ', $string);
        $slug = preg_replace('#[\s]+#', '_', trim($slug));
        $slug = strtolower($slug);

        $tmp_slug = $slug;
        $counter = 0;
        while ($this->getPlanTypeIdByName($tmp_slug)) {
            $tmp_slug = $slug . '-' . rand(1, 1000000);
            $counter++;
            if ($counter >= 10) {
                return uniqid('planType');
            }
        }
        return $tmp_slug;
    }

    public function checkBeforeDeletePlanType($planTypeId, &$tables = array()) {

        $sucsess = true;

        $select_plan = $this->getAdapter()->select();
        $select_plan->from('plan', 'COUNT(*)');
        $select_plan->where("plan_type_id = {$planTypeId}");
        $count_plan = $this->getAdapter()->fetchOne($select_plan);

        if ($count_plan) {
            $tables[] = 'plan';
            $sucsess = false;
        }

        $select_plan_type_credential = $this->getAdapter()->select();
        $select_plan_type_credential->from('plan_type_credential', 'COUNT(*)');
        $select_plan_type_credential->where("plan_type_id = {$planTypeId}");
        $count_plan_type_credential = $this->getAdapter()->fetchOne($select_plan_type_credential);

        if ($count_plan_type_credential) {
            $tables[] = 'plan_type_credential';
            $sucsess = false;
        }

        return $sucsess;
    }

}