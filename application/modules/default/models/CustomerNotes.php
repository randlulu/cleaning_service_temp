<?php

class Model_CustomerNotes extends Zend_Db_Table_Abstract {

    protected $_name = 'customer_notes';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from(array('cn' => $this->_name));
        $select->order($order);

        if ($filters) {
            if (!empty($filters['customer_id'])) {
                $filters['customer_id'] = (int) $filters['customer_id'];
                $select->where("cn.customer_id = {$filters['customer_id']} ");
            }

            if (!empty($filters['company_id'])) {
                $company_id = (int) $filters['company_id'];
                $select->where("c.company_id = {$company_id}");
            }
        }
        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table rows according to the assigned id and data
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "id = '{$id}'");
    }

    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("id = '{$id}'");
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getByCustomerId($customerId) {

        $customerId = (int) $customerId;
        $select = $this->getAdapter()->select();
        $select->from(array('cn' => $this->_name));
        $select->where("cn.customer_id = {$customerId} ");

        return $this->getAdapter()->fetchRow($select);
    }

    public function save($data, $customerId) {
        $customerId = (int) $customerId;
        $customerNote = $this->getByCustomerId($customerId);

        if (!$customerNote) {
            return $this->insert($data);
        } else {
            return $this->updateById($customerNote['id'], $data);
        }
    }

}