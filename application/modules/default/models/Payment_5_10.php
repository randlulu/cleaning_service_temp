<?php

class Model_Payment extends Zend_Db_Table_Abstract {

    protected $_name = 'payment';
    private $modelCustomer;
    private $modelPaymentType;
    private $modelBookingInvoice;

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from(array('pay' => $this->_name));
		$select->join(array('book' => 'booking'),'book.booking_id = pay.booking_id',array('is_change','booking_num'));
        $select->order($order);
        $select->distinct();

        if (!empty($filters['is_deleted'])) {
            $select->where('pay.is_deleted = 1');
        } else {
            $select->where('pay.is_deleted = 0');
        }

        $joinInner = array();
        $filters['company_id'] = CheckAuth::getCompanySession();


        $loggedUser = CheckAuth::getLoggedUser();

        if ($loggedUser) {
            if (!CheckAuth::checkCredential(array('canSeeAllPayment'))) {
                if (CheckAuth::checkCredential(array('canSeeOnlyHisPayment'))) {
                    if (CheckAuth::checkCredential(array('canSeeAssignedPayment'))) {
                        $joinInner['contractor_service_booking'] = array('name' => array('csb' => 'contractor_service_booking'), 'cond' => 'pay.booking_id = csb.booking_id', 'cols' => '');
                        $joinInner['user'] = array('name' => array('usr' => 'user'), 'cond' => 'usr.user_id = pay.user_id', 'cols' => '');

                        $select->where("pay.user_id = {$loggedUser['user_id']} OR csb.contractor_id = {$loggedUser['user_id']}");
                        $select->where('usr.is_deleted = 0');
                    } else {
                        $joinInner['user'] = array('name' => array('usr' => 'user'), 'cond' => 'usr.user_id = pay.user_id', 'cols' => '');

                        $select->where("pay.user_id = " . $loggedUser['user_id']);
                        $select->where('usr.is_deleted = 0');
                    }
                } else {
                    $joinInner['user'] = array('name' => array('usr' => 'user'), 'cond' => 'usr.user_id = pay.user_id', 'cols' => '');

                    $select->where("pay.user_id = " . $loggedUser['user_id']);
                    $select->where('usr.is_deleted = 0');
                }
            }
        }

        if ($filters) {
            if (!empty($filters['keywords'])) {
                $keywords = $this->getAdapter()->quote('%' . $filters['keywords'] . '%');
				$joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'bok.booking_id = pay.booking_id', 'cols' => '');
                $select->where("bok.full_text_search LIKE {$keywords}");
            }

            if (!empty($filters['booking_id'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'bok.booking_id = pay.booking_id', 'cols' => '');

                $select->where("pay.booking_id = {$filters['booking_id']}");
                //$select->where('bok.is_deleted = 0');
            }

            if (!empty($filters['customer_id'])) {
                $joinInner['customer'] = array('name' => array('cus' => 'customer'), 'cond' => 'cus.customer_id = pay.customer_id', 'cols' => '');

                $select->where("pay.customer_id = {$filters['customer_id']}");
                $select->where('cus.is_deleted = 0');
            }

            if (!empty($filters['payment_type_id'])) {
                $select->where("pay.payment_type_id = {$filters['payment_type_id']}");
            }

            if (!empty($filters['is_approved'])) {
                if ('yes' == $filters['is_approved']) {
                    $select->where('pay.is_approved = 1');
                } elseif ('no' == $filters['is_approved']) {
					if(!empty($filters['contractor_id'])){
						$joinInner['contractor_service_bookings'] = array('name' => array('csb' => 'contractor_service_booking'), 'cond' => 'pay.booking_id = csb.booking_id', 'cols' => '');
						$select->where("csb.contractor_id = {$filters['contractor_id']}");
						$select->where('pay.is_approved = 0');
					}else{
                    $select->where('pay.is_approved = 0');
					}
                }
            }
            if (!empty($filters['payment_created_between'])) {
                $paymentCreatedBetween = $filters['payment_created_between'];
                $paymentEndBetween = !empty($filters['payment_end_between']) ? $filters['payment_end_between'] : date('Y-m-d H:i:s', time());
                $select->where("pay.received_date between '" . strtotime($paymentCreatedBetween) . "' and '" . strtotime($paymentEndBetween) . "'");
            }
            if (!empty($filters['company_id'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'bok.booking_id = pay.booking_id', 'cols' => '');

                $company_id = (int) $filters['company_id'];
                $select->where("bok.company_id = {$company_id}");
            }
            if (!empty($filters['order_by']) && $filters['order_by'] == 'payment_type') {
                $joinInner['payment_type'] = array('name' => array('pyt' => 'payment_type'), 'cond' => 'pyt.id = pay.payment_type_id', 'cols' => '');
            }
            if (!empty($filters['is_mark'])) {
                $select->where('pay.is_mark = 1');
            }
        }

        if ($joinInner) {
            foreach ($joinInner as $inner) {
                $select->joinInner($inner['name'], $inner['cond'], $inner['cols']);
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }
        //echo $select;die;
		
	
		
		
        return $this->getAdapter()->fetchAll($select);
    }
	
	
	//Added by Salim
	function getByRefernce($refernce ,$order = null, &$pager = null){
		$select = $this->getAdapter()->select();
        $select->from(array('pay' => $this->_name));
		$select->join(array('book' => 'booking'),'book.booking_id = pay.booking_id',array('is_change','booking_num'));
        $select->order($order);
        $select->distinct();

        if (!empty($filters['is_deleted'])) {
            $select->where('pay.is_deleted = 1');
        } else {
            $select->where('pay.is_deleted = 0');
        }

        $joinInner = array();
        $filters['company_id'] = CheckAuth::getCompanySession();


        $loggedUser = CheckAuth::getLoggedUser();

        if ($loggedUser) {
            if (!CheckAuth::checkCredential(array('canSeeAllPayment'))) {
                if (CheckAuth::checkCredential(array('canSeeOnlyHisPayment'))) {
                    if (CheckAuth::checkCredential(array('canSeeAssignedPayment'))) {
                        $joinInner['contractor_service_booking'] = array('name' => array('csb' => 'contractor_service_booking'), 'cond' => 'pay.booking_id = csb.booking_id', 'cols' => '');
                        $joinInner['user'] = array('name' => array('usr' => 'user'), 'cond' => 'usr.user_id = pay.user_id', 'cols' => '');

                        $select->where("pay.user_id = {$loggedUser['user_id']} OR csb.contractor_id = {$loggedUser['user_id']}");
                        $select->where('usr.is_deleted = 0');
                    } else {
                        $joinInner['user'] = array('name' => array('usr' => 'user'), 'cond' => 'usr.user_id = pay.user_id', 'cols' => '');

                        $select->where("pay.user_id = " . $loggedUser['user_id']);
                        $select->where('usr.is_deleted = 0');
                    }
                } else {
                    $joinInner['user'] = array('name' => array('usr' => 'user'), 'cond' => 'usr.user_id = pay.user_id', 'cols' => '');

                    $select->where("pay.user_id = " . $loggedUser['user_id']);
                    $select->where('usr.is_deleted = 0');
                }
            }
        }
		  if (!empty($refernce)) {
                $keywords = $this->getAdapter()->quote('%' . $refernce . '%');
				
                $select->where("pay.reference LIKE {$keywords}");
		  }

		    if ($joinInner) {
            foreach ($joinInner as $inner) {
                $select->joinInner($inner['name'], $inner['cond'], $inner['cols']);
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }
        //echo $select;die;
        return $this->getAdapter()->fetchAll($select);
	}

    /**
     * update table rows according to the assigned id and date
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;

        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'changed');

        return parent::update($data, "payment_id = '{$id}'");
    }

    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;

        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'deleted');

        return parent::delete("payment_id = '{$id}'");
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("payment_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the id and booking id
     * 
     * @param int $id
     * @param int $booking_id
     * @return array 
     */
    public function getByIdAndBookingId($id, $booking_id) {
        $id = (int) $id;
        $booking_id = (int) $booking_id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("payment_id = '{$id}'");
        $select->where("booking_id = '{$booking_id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the booking id
     * 
     * @param int $booking_id
     * @return array 
     */
   /* public function getByBookingId($booking_id) {
        $booking_id = (int) $booking_id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_id = '{$booking_id}'");

        return $this->getAdapter()->fetchAll($select);
    }*/
	public function getByBookingId($booking_id) {
        $booking_id = (int) $booking_id;
        $select = $this->getAdapter()->select();
        $select->from(array('p'=>$this->_name));
		$select->joinInner(array('pt'=>'payment_type'),'p.payment_type_id = pt.id',array('payment_type'));
		$select->joinLeft(array('c'=>'customer_commercial_info'),'p.customer_id = c.customer_id',array('business_name'));
        $select->where("p.booking_id = '{$booking_id}'");

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * get total amount according to the assigned filters
     * 
     * @param array $filters
     * @return array 
     */
    public function getTotalAmount($filters = array()) {
        $totalPayment = $this->getTotalPayment($filters);

        $modelRefund = new Model_Refund();
        $totalRefund = $modelRefund->getTotalRefund($filters);

        return $totalPayment - $totalRefund;
    }

    public function getTotalPayment($filters = array()) {
        $select = $this->getAdapter()->select();
        $select->from(array('pay' => $this->_name), array('pay.payment_id', 'pay.amount'));

        $joinInner = array();

        $select->distinct();

        if (!empty($filters['is_approved'])) {
            if ('all' == $filters['is_approved']) {
                $select->where("pay.is_approved = 1 OR pay.is_approved = 0");
            } elseif ('yes' == $filters['is_approved']) {
                $select->where('pay.is_approved = 1');
            } elseif ('no' == $filters['is_approved']) {
                $select->where('pay.is_approved = 0');
            }
        } else {
            $select->where('pay.is_approved = 1');
        }

        $select->where('pay.is_deleted = 0');

        $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'bok.booking_id = pay.booking_id', 'cols' => '');
        $select->where('bok.is_deleted = 0');

        $filters['company_id'] = CheckAuth::getCompanySession();

        if ($filters) {
            if (!empty($filters['contractor_id'])) {
                $joinInner['contractor_service_booking'] = array('name' => array('csb' => 'contractor_service_booking'), 'cond' => 'bok.booking_id = csb.booking_id', 'cols' => '');
                $select->where("bok.created_by = " . $filters['contractor_id'] . " OR csb.contractor_id = {$filters['contractor_id']}");
            }

            if (!empty($filters['booking_id'])) {
                $booking_id = $this->getAdapter()->quote(trim($filters['booking_id']));
                $select->where("bok.booking_id = {$booking_id}");
            }

            if (!empty($filters['booking_start_between'])) {
                $bookingStartBetween = $filters['booking_start_between'];
                $bookingEndBetween = !empty($filters['booking_end_between']) ? $filters['booking_end_between'] : date('Y-m-d H:i:s', time());
                $select->where("bok.booking_start between '" . $bookingStartBetween . "' and '" . $bookingEndBetween . "'");
            }

            if (!empty($filters['invoice_type'])) {
                $joinInner['booking_invoice'] = array('name' => array('inv' => 'booking_invoice'), 'cond' => 'bok.booking_id = inv.booking_id', 'cols' => '');

                $invoice_type = $this->getAdapter()->quote(trim($filters['invoice_type']));
                if ('unpaid' == $filters['invoice_type']) {
                    $select->where("inv.invoice_type = 'overdue' OR inv.invoice_type = 'open'");
                } elseif ('all_active' == $filters['invoice_type']) {
                    $select->where("inv.invoice_type = 'overdue' OR inv.invoice_type = 'open' OR inv.invoice_type = 'closed'");
                } else {
                    $select->where("inv.invoice_type = {$invoice_type}");
                }
            }

            if (!empty($filters['booking_start'])) {
                $bookingStart = $this->getAdapter()->quote(trim($filters['booking_start']));
                $select->where("bok.booking_start >= {$bookingStart}");
            }

            if (!empty($filters['booking_end'])) {
                $bookingEnd = $this->getAdapter()->quote(trim($filters['booking_end']));
                $select->where("bok.booking_end <= {$bookingEnd}");
            }

            if (!empty($filters['payment_type'])) {
                $paymentType = $this->getAdapter()->quote(trim($filters['payment_type']));
                $select->where("pay.payment_type_id = {$paymentType}");
            }
            if (!empty($filters['payment_contractor_id'])) {
                $select->where("pay.contractor_id = {$filters['payment_contractor_id']}");
            }

            if (!empty($filters['without_cash'])) {
                $modelPaymentType = new Model_PaymentType();
                $cash = $modelPaymentType->getPaymentIdBySlug('cash');

                $select->where("pay.payment_type_id != {$cash}");
            }

            if (!empty($filters['convert_status'])) {
                $convert_status = $this->getAdapter()->quote(trim($filters['convert_status']));
                $select->where("bok.convert_status = {$convert_status}");
            }

            if (!empty($filters['status'])) {
                $status = $this->getAdapter()->quote(trim($filters['status']));
                $select->where("bok.status_id = {$status}");
            }

            if (!empty($filters['more_one_status'])) {

                $select->where('bok.status_id IN (' . implode(', ', $filters['more_one_status']) . ')');
            }

            if (!empty($filters['created_by'])) {
                $user_id = (int) $filters['created_by'];
                $select->where("bok.created_by = '{$user_id}'");
            }
            if (!empty($filters['company_id'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'bok.booking_id = pay.booking_id', 'cols' => '');

                $company_id = (int) $filters['company_id'];
                $select->where("bok.company_id = {$company_id}");
            }
        }

        if ($joinInner) {
            foreach ($joinInner as $inner) {
                $select->joinInner($inner['name'], $inner['cond'], $inner['cols']);
            }
        }

        $selectAmount = $this->getAdapter()->select();
        $selectAmount->from($select, 'SUM(amount)');

        return $this->getAdapter()->fetchOne($selectAmount);
    }

    public function fills(&$rows, $types = array()) {
        foreach ($rows as &$row) {
            $this->fill($row, $types);
        }
    }

    /**
     * get extra info for each row (Ex: getting the category row by c_id, author row by author id ... etc)
     * 
     * @param array $row
     * @param array $types
     * @return array
     */
    public function fill(&$row, $types = array()) {

        if (in_array('customer', $types)) {
            if (!$this->modelCustomer) {
                $this->modelCustomer = new Model_Customer();
            }
            $customer = $this->modelCustomer->getById($row['customer_id']);
            $row['customer'] = get_customer_name($customer);
        }

        if (in_array('invoice', $types)) {
            if (!$this->modelBookingInvoice) {
                $this->modelBookingInvoice = new Model_BookingInvoice();
            }
            $row['invoice'] = $this->modelBookingInvoice->getByBookingId($row['booking_id']);
        }

        if (in_array('payment_type', $types)) {
            if (!$this->modelPaymentType) {
                $this->modelPaymentType = new Model_PaymentType();
            }
            $paymentType = $this->modelPaymentType->getById($row['payment_type_id']);
            $row['payment_type'] = $paymentType['payment_type'];
        }
        if (in_array('total_amount', $types)) {
            $total = 0;
            $total +=$row['amount'];
            $row['total_amount'] = $total;
        }
    }

    public function insert(array $data) {

        $id = parent::insert($data);

        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'added');

        return $id;
    }

    public function getCountUnapprovedPayments($contractor_id = null) {
      
	  if(isset($contractor_id)){
		   $unapprovedPayments = $this->getAll(array('is_approved' => 'no' , 'contractor_id'=>$contractor_id));
		}else{
         $unapprovedPayments = $this->getAll(array('is_approved' => 'no'));
        }
        return count($unapprovedPayments);
    }

     public function getLastBookingPaymentByBookingId($booking_id){
        $booking_id = (int) $booking_id;
        $select = $this->getAdapter()->select();
        $select->from(array('p'=>$this->_name));
        $select->joinInner(array('pt'=>'payment_type'),'p.payment_type_id = pt.id',array('payment_type'));
        $select->joinLeft(array('c'=>'customer_commercial_info'),'p.customer_id = c.customer_id',array('business_name'));
        $select->where("p.booking_id = '{$booking_id}'");
        $select->where('p.is_approved = 1');
        $select->order("payment_id DESC");
        $select->limit(1);

        return $this->getAdapter()->fetchRow($select);
    }
	
	
	
	public function getCountUnapprovedPaymentsForContractor($dd) {
		$filters['contractor_id']=$dd;
		$filters['is_approved']='no';
        $unapprovedPayments = $this->getAll($filters);

        return count($unapprovedPayments);
    }
	
		public function getCashPayments($bookingId){
	  
	    $booking_id = (int) $bookingId;
        $select = $this->getAdapter()->select();
        $select->from(array('p'=>$this->_name),array('SUM(`p`.`amount`) as cash_amount'));
        $select->joinInner(array('pt'=>'payment_type'),'p.payment_type_id = pt.id',array());
	    $select->where("p.booking_id = '{$booking_id}'");
	    $select->where("pt.payment_type = 'Cash'");
		//echo $select->__toString();
		
		return $this->getAdapter()->fetchRow($select);
	
	}
}