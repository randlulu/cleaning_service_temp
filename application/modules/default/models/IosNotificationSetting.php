<?php

class Model_IosNotificationSetting extends Zend_Db_Table_Abstract {

    protected $_name = 'ios_notification_setting';

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "setting_id = '{$id}'");
    }

    /**
     * delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("setting_id = '{$id}'");
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("setting_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the assigned settingName
     * 
     * @param string $settingName
     * @return array
     */
    public function getBySettingName($settingName) {

        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("setting_name = '{$settingName}'");

        return $this->getAdapter()->fetchRow($select);
    }

   
    public function getAllIosNotificationSetting($asArray = false) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);

        $results = $this->getAdapter()->fetchAll($select);

        if ($asArray) {
            $data = array();
            if ($results) {
                foreach ($results as $result) {
                    $data[$result['setting_id']] = spaceBeforeCapital(ucfirst($result['setting_name']));
                }
            }
            return $data;
        } else {
            return $results;
        }
    }

    

}