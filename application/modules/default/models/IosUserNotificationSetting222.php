<?php

class Model_IosUserNotificationSetting extends Zend_Db_Table_Abstract {

    protected $_name = 'ios_user_notification_setting';

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "id = '{$id}'");
    }

	public function updateBySettingIdAndUserId($userId,$settingId, $data) {
        $userId = (int) $userId;
        $settingId = (int) $settingId;
        return parent::update($data, "user_id = '{$userId}' and setting_id= '{$settingId}' ");
    }
	
	

    /**
     * delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("id = '{$id}'");
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }
	
	public function getByUserId($userId) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("user_id = '{$userId}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the assigned settingName
     * 
     * @param string $settingName
     * @return array
     */
    public function getBySettingId($settingId) {

        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("setting_id = '{$settingId}'");

        return $this->getAdapter()->fetchRow($select);
    }

   
    public function getAllIosSettingNamesByUserId($userId) {
        $select = $this->getAdapter()->select();
        $select->from(array('iuns'=>$this->_name), array('is_active'=>'IFNULL(is_active,1)'));
		$select->joinRight(array('ins'=>'ios_notification_setting'), 'iuns.setting_id = ins.setting_id', array('setting_id','setting_name','type'=>'IF(setting_name IS NOT NULL, "boolean", "")') );
        //$sql = $select->__toString();
		//echo $sql;
		$results = $this->getAdapter()->fetchAll($select);

       
            return $results;
       
    }

    

}