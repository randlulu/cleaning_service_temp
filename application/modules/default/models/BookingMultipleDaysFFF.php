<?php

class Model_BookingMultipleDays extends Zend_Db_Table_Abstract {

    protected $_name = 'booking_multiple_days';
    //
    //model for fill function
    //
    private $loggedUser = array();

    /**
     * init
     */
    public function init() {
        parent::init();
        $this->loggedUser = CheckAuth::getLoggedUser();
    }

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null, $limit = 0) {

        $columns = $this->getAdapter()->fetchAll("SHOW COLUMNS FROM `booking` where field not in ('booking_start','booking_end','is_all_day_event')");

        $bookingColumns = array();
        foreach ($columns as $column) {
            $bookingColumns[] = "bok.{$column['Field']}";
        }
        $bookingColumns[] = 'bmd.booking_start';
        $bookingColumns[] = 'bmd.booking_end';
        $bookingColumns[] = 'bmd.is_all_day_event';

        $select = $this->getAdapter()->select();
        $select->from(array('bmd' => $this->_name), $bookingColumns);
        $select->distinct();

        $joinInner = array();
        $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'bok.booking_id = bmd.booking_id', 'cols' => '');


        if ($order) {
            $select->order($order);
        }

        $filters['company_id'] = CheckAuth::getCompanySession();

        if (CheckAuth::checkCredential(array('canSeeDeletedBooking'))) {
            if (!empty($filters['is_deleted'])) {
                $select->where('bok.is_deleted = 1');
            } else {
                $select->where('bok.is_deleted = 0');
            }
        } else {
            $select->where('bok.is_deleted = 0');
        }

        if (!CheckAuth::checkCredential(array('canSeeAllBooking'))) {
            if (CheckAuth::checkCredential(array('canSeeOnlyHisBooking'))) {
                if (CheckAuth::checkCredential(array('canSeeAssignedBooking'))) {

                    $joinInner['contractor_service_booking'] = array('name' => array('csb' => 'contractor_service_booking'), 'cond' => 'bok.booking_id = csb.booking_id', 'cols' => '');

                    $select->where("bok.created_by = {$this->loggedUser['user_id']} OR csb.contractor_id = {$this->loggedUser['user_id']}");
                } else {
                    $select->where("bok.created_by = {$this->loggedUser['user_id']}");
                }
            } else {
                $select->where("bok.created_by = {$this->loggedUser['user_id']}");
            }
        }

        if ($filters) {
            if (!empty($filters['status']) && $filters['status'] != 'all') {
                if ($filters['status'] == 'current') {

                    // current include(TO VISIT,TENTATIVE,TO DO,AWAITING UPDATE,IN PROGRESS)
                    $modelBookingStatus = new Model_BookingStatus();
                    $toVisit = $modelBookingStatus->getByStatusName('TO VISIT');
                    $tentative = $modelBookingStatus->getByStatusName('TENTATIVE');
                    $toDo = $modelBookingStatus->getByStatusName('TO DO');
                    $awaitingUpdate = $modelBookingStatus->getByStatusName('AWAITING UPDATE');
                    $inProcess = $modelBookingStatus->getByStatusName('IN PROGRESS');

                    $wheres['status'] = ("bok.status_id IN ({$toVisit['booking_status_id']},{$tentative['booking_status_id']},{$toDo['booking_status_id']},{$awaitingUpdate['booking_status_id']},{$inProcess['booking_status_id']})");
                } else {
                    $status = $this->getAdapter()->quote(trim($filters['status']));
                    $wheres['status'] = ("bok.status_id = {$status}");
                }
            }
            if (!empty($filters['contractor_id'])) {
                $contractor_id = (int) $filters['contractor_id'];
                $joinInner['contractor_service_booking'] = array('name' => array('csb' => 'contractor_service_booking'), 'cond' => 'bok.booking_id = csb.booking_id', 'cols' => '');

                $select->where("bok.created_by = {$contractor_id} OR csb.contractor_id = {$contractor_id}");
            }
            if (!empty($filters['company_id'])) {
                $company_id = (int) $filters['company_id'];
                $select->where("bok.company_id = {$company_id}");
            }
            if (!empty($filters['city_id'])) {
                $city_id = (int) $filters['city_id'];
                $select->where("bok.city_id = {$city_id}");
            }

            if (!empty($filters['calendar_booking_start']) && !empty($filters['calendar_booking_end'])) {
                $calendarBookingStart = $filters['calendar_booking_start'];
                $calendarBookingEnd = $filters['calendar_booking_end'];
                $select->where("bmd.booking_start between '" . php2MySqlTime($calendarBookingStart) . "' AND '" . php2MySqlTime($calendarBookingEnd) . "'");
            }
            if (!empty($filters['booking_in_this_day'])) {
                $bookingStart = strtotime(trim($filters['booking_in_this_day']));
                $bookingStartBetween = date("Y-m-d", $bookingStart) . " 00:00:00";
                $bookingEndBetween = date("Y-m-d", $bookingStart) . " 23:59:59";
                $select->where("(bmd.booking_start between '" . $bookingStartBetween . "' AND '" . $bookingEndBetween . "') OR (bmd.booking_end between '" . $bookingStartBetween . "' AND '" . $bookingEndBetween . "')");
            }
        }

        if ($joinInner) {
            foreach ($joinInner as $inner) {
                $select->joinInner($inner['name'], $inner['cond'], $inner['cols']);
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        } elseif ($limit) {
            $select->limit($limit);
        }

        return $this->getAdapter()->fetchAll($select);
    }

    public function updateById($id, $data) {
        $id = (int) $id;
        $suceess = parent::update($data, "id = '{$id}'");

        /**
         * get all record and insert it in log
         */
        $modelBookingMultipleDaysLog = new Model_BookingMultipleDaysLog();
        $modelBookingMultipleDaysLog->addBookingMultipleDaysLog($id);

        return $suceess;
    }

    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("id = '{$id}'");
    }

    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function getByBookingId($bookingId) {
        $bookingId = (int) $bookingId;

        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_id = '{$bookingId}'");

        return $this->getAdapter()->fetchAll($select);
    }

    public function deleteByBookingId($bookingId) {
        $bookingId = (int) $bookingId;
        return parent::delete("booking_id = '{$bookingId}'");
    }

    public function saveMultipleDays($bookingId) {

        //Get Zend Request
        $request = Zend_Controller_Front::getInstance()->getRequest();

        $multi_stpartdate = $request->getParam('multi_stpartdate');
        $multi_stparttime = $request->getParam('multi_stparttime');
        $multi_etpartdate = $request->getParam('multi_etpartdate');
        $multi_etparttime = $request->getParam('multi_etparttime');
        $multi_isAllDayEvent = $request->getParam('multi_isAllDayEvent');

        // delete all multiple days from database
        $this->deleteByBookingId($bookingId);

        if ($multi_stpartdate) {

            $bookinMultipleDays = array();
            foreach ($multi_stpartdate as $key => $stpartdate) {
                $stpartdate = $stpartdate ? $stpartdate : '0000-00-00';
                $stparttime = !empty($multi_stparttime[$key]) ? $multi_stparttime[$key] : '00:00:00';
                $etpartdate = !empty($multi_etpartdate[$key]) ? $multi_etpartdate[$key] : '0000-00-00';
                $etparttime = !empty($multi_etparttime[$key]) ? $multi_etparttime[$key] : '00:00:00';
                $isAllDayEvent = !empty($multi_isAllDayEvent[$key]) ? $multi_isAllDayEvent[$key] : 0;

                $st = $stpartdate . " " . $stparttime;
                $et = $etpartdate . " " . $etparttime;

                $booking_start = php2MySqlTime(js2PhpTime($st));
                $booking_end = php2MySqlTime(js2PhpTime($et));
                $is_all_day_event = $isAllDayEvent ? 1 : 0;

                $db_params = array();
                $db_params['booking_start'] = $booking_start;
                $db_params['booking_end'] = $booking_end;
                $db_params['booking_id'] = $bookingId;
                $db_params['is_all_day_event'] = $is_all_day_event;

                $booking_start = strtotime($booking_start);
                $booking_end = strtotime($booking_end);

                $bookinMultipleDays["{$booking_start}_{$booking_end}_{$bookingId}_{$is_all_day_event}"] = $db_params;
            }

            foreach ($bookinMultipleDays as $bookinMultipleDay) {
                $id = $this->insert($bookinMultipleDay);

                /**
                 * get all record and insert it in log
                 */
                $modelBookingMultipleDaysLog = new Model_BookingMultipleDaysLog();
                $modelBookingMultipleDaysLog->addBookingMultipleDaysLog($id);
            }
        }
    }

}