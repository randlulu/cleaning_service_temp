<?php
class Booking_Form_sendSms extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('send_sms');

        $router = Zend_Controller_Front::getInstance()->getRouter();
        $message = new Zend_Form_Element_Textarea('message');
        $message->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'textarea_field form-control','rows'=>'6'))
                ->setErrorMessages(array('Required' => 'Please enter the message'));
		$button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('send');
        $button->setAttribs(array('class' => 'button btn btn-primary'));
        $this->addElements(array($message, $button));
		$this->setMethod('post');

       
    }

}

