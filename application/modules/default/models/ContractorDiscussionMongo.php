<?php

class Model_ContractorDiscussionMongo {

    protected $_dbhost = '127.0.0.1';
    protected $_dbName = 'cleaning_service';
    protected $_collectionName = 'discussion';
    protected $_con;
    protected $_collection;
    protected $_db;

    //protected static $db;

    /**
     * __construct
     * 
     * @param array $data as data
     * @param array $config as array of config
     * 
     * @return null
     */
    public function __construct($data = array(), $config = array()) {
        $dbhost = 'localhost';
        // Connect to test database  
        $this->_con = new Mongo("mongodb://$this->_dbhost");

        //$db = $con->cleaning_service;
        $this->_db = $this->_con->selectDB($this->_dbName);

        $this->_collection = $this->_db->selectCollection($this->_collectionName);
    }

    public function closeConnection() {
        $isClosed = $this->_con->close();
        return $isClosed;
    }
	
	public function insertDiscussion($doc = array()) {
		//$this->_collection->insert($doc);
		//$newDocID = $doc['_id'];
        return $this->_collection->insert($doc);
    }
	
	public function getByContractorId($id, $isContractor){
	    $id = (int) $id;
		if($isContractor){
			if($this->_collection->find(array('$and' => array(array('type' => 'contractor_discussion'),array('contractor_id' => $id),array('visibility' => 2))))->sort(array('created' => -1))->count() > 0){
				$contractorDiscussions = $this->_collection->find(array('$and' => array(array('type' => 'contractor_discussion'),array('contractor_id' => $id),array('visibility' => 2))))->limit(10)->sort(array('created' => -1));
			}else{
				$contractorDiscussions = 0;
			}
			
		}else{
			if($this->_collection->find(array('$and' => array(array('type' => 'contractor_discussion'), array('contractor_id' => $id))))->sort(array('created' => -1))->count() > 0){
				$contractorDiscussions = $this->_collection->find(array('$and' => array(array('type' => 'contractor_discussion'), array('contractor_id' => $id))))->limit(10)->sort(array('created' => -1));
			}else{
				$contractorDiscussions = 0;
			}
		}
		return $contractorDiscussions;
		
	}
        
    public function getByContractorIdthelast($id){
	   
			if($this->_collection->find(array('$and' => array(array('type' => 'contractor_discussion'),array('contractor_id' => $id),array('visibility' => 2))))->sort(array('created' => -1))->count() > 0){
				$contractorDiscussions = $this->_collection->find(array('$and' => array(array('type' => 'contractor_discussion'),array('contractor_id' => $id),array('visibility' => 2))))->limit(1)->sort(array('created' => -1));
			}else{
				$contractorDiscussions = 0;
			}
            if ($contractorDiscussions) {
            foreach ($contractorDiscussions as $key => $row)
                $contractorDiscussions2[] = $row;

			}
				
		return $contractorDiscussions2;
		
	}
    
	
	
	
	public function getDisByOtherUsers($contractorId, $loggedUserId, $isContractor){
		$contractorId = (int) $contractorId;
		$loggedUserId = (int) $loggedUserId;
		
		if($isContractor){
			if($this->_collection->find(array('$and' => array(array('type' => 'contractor_discussion'),array('user_id' => array('$ne'=> $loggedUserId)),array('contractor_id' => $contractorId),array('visibility' => 2))))->sort(array('created' => -1))->count() > 0){
				$contractorDiscussions = $this->_collection->find(array('$and' => array(array('type' => 'contractor_discussion'),array('user_id' => array('$ne'=> $loggedUserId)),array('contractor_id' => $contractorId),array('visibility' => 2))))->sort(array('created' => -1));
			}else{
				$contractorDiscussions = 0;
			}
		}else{
			if($this->_collection->find(array('$and' => array(array('type' => 'contractor_discussion'), array('contractor_id' => $contractorId),array('user_id' => array('$ne'=> $loggedUserId)))))->sort(array('created' => -1))->count() > 0){
				$contractorDiscussions = $this->_collection->find(array('$and' => array(array('type' => 'contractor_discussion'), array('contractor_id' => $contractorId),array('user_id' => array('$ne'=> $loggedUserId)))))->sort(array('created' => -1));
				
			}else{
				$contractorDiscussions = 0;
			}
			
		}
		return $contractorDiscussions;
	}
	
	public function changeCommentSeenFlagById($id, $loggedUserId, $loggedUserName) {
        //$id = (int) $id;
		$loggedUserId = (int) $loggedUserId;
		$discussion = $this->getById($id);
		
		$newId = "";
		$newName = "";
        $seenId = array("");
		$seenNames = array("");
		$result1 = 0;
		$result2 = 0;
		
		
		foreach ($discussion as $key => $value) {
            $seenId = explode(',', $value['seen_by_ids']);
			$seenNames = explode(',', $value['seen_by_names']);
			
            if (!in_array($loggedUserId, $seenId)) {
                if (!empty($value['seen_by_ids'])) {
					$newId = $value['seen_by_ids'] . "," . $loggedUserId;
					$newName = $value['seen_by_names'] . "," . $loggedUserName;
					//echo "by walaa f";print_r($newId);exit;
                } else {
                    $newId = (string) $loggedUserId;
					
					$newName = $loggedUserName;
					//echo "by walaa n";print_r($newId);exit;
                }
               
				
				$result1 = $this->_collection->update(
					array('_id' => new MongoId($key)), array('$set' =>array('seen_by_ids' => $newId)), array('multiple' => true));
				$result2 = $this->_collection->update(
					array('_id' => new MongoId($key)), array('$set' =>array('seen_by_names' => $newName)), array('multiple' => true));
				
            }
        }
			
		if($result1 && $result2)
			return 1;
		return 0;
    }
	
	public function updateDiscImages($id, $thumnails, $orignal, $large, $small, $compressed){
		$this->_collection->update(
					array('_id' => new MongoId($id)), array('$set' =>array('thumbnail_images' => $thumnails)));
		$this->_collection->update(
					array('_id' => new MongoId($id)), array('$set' =>array('original_images' => $orignal)));
					$this->_collection->update(
					array('_id' => new MongoId($id)), array('$set' =>array('large_images' => $large)));
					$this->_collection->update(
					array('_id' => new MongoId($id)), array('$set' =>array('small_images' => $small)));
					$this->_collection->update(
					array('_id' => new MongoId($id)), array('$set' =>array('compressed_images' => $compressed)));
					return 1;
	}
	
	public function getById($id) {
        //$id = (int) $id;
		$discussion = $this->_collection->find(array('_id' => new MongoId($id)));
		return $discussion;
    }
	
	public function markAllAsSeen($contractor_id , $loggedUserName, $loggedUserId, $isContractor){
		$contractor_id = (int) $contractor_id;
		$loggedUserId = (int) $loggedUserId;
		//echo "contId: " . $contractor_id . " ietm: " . $item_id . " type: " . $type;
		
		$discussions = $this->getDisByOtherUsers($contractor_id, $loggedUserId, $isContractor);
		
		$newId = "";
		$newName = "";
        $seenIds = array("");
		$seenNames = array("");
		$result1 = array();
		$result2 = array();
		if($discussions){
		foreach ($discussions as $key => $value) {
            $seenIds = explode(',', $value['seen_by_ids']);
		
			$seenNames = explode(',', $value['seen_by_names']);
			
            if (!in_array($loggedUserId, $seenIds)) {
                if (!empty($value['seen_by_ids'])) {
					$newId = $value['seen_by_ids'] . "," . $loggedUserId;
					$newName = $value['seen_by_names'] . "," . $loggedUserName;
					
                } else {
                    $newId = (string) $loggedUserId;
					
					$newName = $loggedUserName;
				
                }
				
				
				$result1[$key] = $this->_collection->update(
					array('_id' => new MongoId($key)), array('$set' =>array('seen_by_ids' => $newId)), array('multiple' => true));
				$result2[$key] = $this->_collection->update(
					array('_id' => new MongoId($key)), array('$set' =>array('seen_by_names' => $newName)), array('multiple' => true));
            }
        }
		}
		if(!empty($result1) && !in_array(0,$result1) && !empty($result2) && !in_array(0,$result2)){
			return 1;
		}
        return 0;
	}
	
	public function deleteById($id) {
        $result = $this->_collection->remove(array('_id' => new MongoId($id)), array('w' => true));
		return $result['n'];
    }

	public function updatebyId($id, $data){
		$this->_collection->update(
						array('_id' => new MongoId($id)), array('$set' => $data));
		return 1;
		
	}
	
}
