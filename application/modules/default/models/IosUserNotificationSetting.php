<?php

class Model_IosUserNotificationSetting extends Zend_Db_Table_Abstract {

    protected $_name = 'ios_user_notification_setting';

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "id = '{$id}'");
    }

	public function updateBySettingIdAndUserId($userId,$settingId, $data) {
        $userId = (int) $userId;
        $settingId = (int) $settingId;
		$ret = $this->getBySettingIdAndUserId($userId,$settingId);
		if(!empty($ret)){
			return parent::update($data, "user_id = '{$userId}' and setting_id= '{$settingId}' ");
		}
		else{
			$data['user_id'] = $userId;
			$data['setting_id'] = $settingId;
			return parent::insert($data);
		}
        
    }
	
	 public function getBySettingIdAndUserId($userId,$settingId) {
        //$id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("user_id = '{$userId}' and setting_id= '{$settingId}' ");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("id = '{$id}'");
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }
	
	public function getByUserId($userId) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("user_id = '{$userId}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the assigned settingName
     * 
     * @param string $settingName
     * @return array
     */
    public function getBySettingId($settingId) {

        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("setting_id = '{$settingId}'");

        return $this->getAdapter()->fetchRow($select);
    }

   
   public function getAllIosSettingNamesByUserId($userId) {
		
        /*$select = $this->getAdapter()->select();
        $select->from(array('iuns'=>$this->_name), array('is_active'=>'IFNULL(is_active,default_value)'));
		$select->joinRight(array('ins'=>'ios_notification_setting'), 'iuns.setting_id = ins.setting_id', array('setting_id','setting_name','type'=>'IF(setting_name IS NOT NULL, "boolean", "")') );
        $select->where("iuns.user_id = {$userId}");
		$sql = $select->__toString();
		echo $sql;
		$results = $this->getAdapter()->fetchAll($select);*/
		$select1 = $this->getAdapter()->select();
        $select1->from(array('iuns'=>'ios_user_notification_setting'));
		$select1->where("iuns.user_id = {$userId}");
		
		$select = $this->getAdapter()->select();
        $select->from(array('ins'=>'ios_notification_setting'), array('setting_id','setting_name','type'=>'IF(setting_name IS NOT NULL, "boolean", "")') );
		$select->joinLeft(array('iuns'=>$select1), 'iuns.setting_id = ins.setting_id',array('is_active'=>'IFNULL(is_active,default_value)'));
        
		$sql = $select->__toString();
		//echo $sql;
		$results = $this->getAdapter()->fetchAll($select);

       
        return $results;
       
    }
    

}