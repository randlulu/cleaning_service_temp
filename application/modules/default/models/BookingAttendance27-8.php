<?php

class Model_BookingAttendance extends Zend_Db_Table_Abstract {

    protected $_name = 'booking_attendance';
    private $modelContractorInfo;

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array()) {
        /*$select = $this->getAdapter()->select();
        $select->from(array('ba' => $this->_name));
        

        return $this->getAdapter()->fetchAll($select);*/
    }

    /**
     * update table row according to the assigned $id and submited $data
     * 
     * @param $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "booking_attendance_id = {$id}");
    }

    /**
     * delete table row according to the assigned $id
     * 
     * @param  int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("booking_attendance_id = '{$id}'");
    }

    /**
     * get table row according to the assigned $id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from(array('u' => $this->_name));
        $select->joinInner(array('ci' => 'city'), 'ci.city_id = u.city_id', array('ci.city_name'));
        $select->joinInner(array('co' => 'country'), 'ci.country_id = co.country_id', array('co.country_name'));
        $select->joinInner(array('ar' => 'auth_role'), 'ar.role_id = u.role_id', array('ar.role_name'));
        $select->where("user_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }
	
	
	public function getLastCheckInByBookingIdAndContractorId($bookingId,$contractorId){
	  
	    $select = $this->getAdapter()->select();
        $select->from($this->_name, array('MAX(booking_attendance_id) as last_booking_attendance_id'));
        $select->where("booking_id  = {$bookingId}");
        $select->where("contractor_id  = {$contractorId}");
        $select->where("check_out_time  IS NULL ");
		
	    $result  = $this->getAdapter()->fetchRow($select);
		return $result['last_booking_attendance_id'];
	
	}
	
	
    public function insert(array $data) {

        $id = parent::insert($data);

        return $id;
    }

    
}