<?php

class Model_EstimateDiscussionBeforeMongo extends Zend_Db_Table_Abstract {

    protected $_name = 'estimate_discussion';

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
	 
	
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("discussion_id = '{$id}'");
    }
	
    public function updateById($id, $data) {
        $id = (int) $id;

        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'changed');

        return parent::update($data, "discussion_id = '{$id}'");
    }
	
	
	public function getByImageId($id, $order = 'asc') {
        
		$id = (int) $id;
		$typeFilter1 = $this->getAdapter()->quote('estimate' . '%');	
        $select = $this->getAdapter()->select();
        $select->from(array('ed'=>$this->_name));
		$select->joinInner(array('iid' => 'item_image_discussion'), " iid.item_id = ed.discussion_id AND iid.type LIKE {$typeFilter1}",array('group_id'));		
		$select->order("ed.created {$order}");
        $select->where("iid.image_id = '{$id}'");
  		
        return $this->getAdapter()->fetchAll($select);
    }
	

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("discussion_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get all the recent discussions that the user are able to see
     *  
     * @return array  
     */
    public function getRecentDiscussion() {

        $loggedUser = CheckAuth::getLoggedUser();
        $userId = $loggedUser['user_id'];

        $select = $this->getAdapter()->select();

        $select->distinct();

        $select->from(array('id' => $this->_name));

        $select->joinInner(array('csb' => 'contractor_service_booking'), 'id.estimate_id = csb.booking_id', 'csb.contractor_id');

        if (!CheckAuth::checkCredential(array('canSeeAllEstimateDiscussion'))) {
            if (CheckAuth::checkCredential(array('canSeeOnlyHisEstimateDiscussion'))) {
                if (CheckAuth::checkCredential(array('canSeeAssignedEstimateDiscussion'))) {
                    $select->where("id.user_id = {$userId} OR csb.contractor_id = {$userId}");
                } else {
                    $select->where("id.user_id = {$userId}");
                }
            }
        }

        $select->order("created DESC");

        $select->limit(20);

        return $this->getAdapter()->fetchall($select);
    }

    /**
     * get table row according to the assigned estimateId
     * 
     * @param int $id
     * @return array
     */
   
   public function getByEstimateId($id, $order = 'asc',$filter = array()) {
	
	    $id = (int) $id;

        $select = $this->getAdapter()->select();
        $select->from(array('ed'=>$this->_name));
		$typeFilter = $this->getAdapter()->quote('estimate' . '%');	
		$select->joinLeft(array('iid' => 'item_image_discussion'), " iid.item_id = ed.discussion_id AND iid.type LIKE {$typeFilter}",array('group_id'));
		$select->joinLeft(array('im' => 'image'), "(iid.image_id = im.image_id )",array('im.thumbnail_path','im.image_id','im.compressed_path'));
		if(isset($filter['groups'])){
		 $select->where("iid.group_id > 0");
		 $select->group("iid.group_id");
		}else{		
         $select->where("iid.group_id = 0");		 
		}
		
		
		
		$select->order("ed.created {$order}");
        //$select->where("estimate_id = '{$id}'");
		$select->where("is_deleted = 0");
		
        return $this->getAdapter()->fetchAll($select);
    }
   
    public function insert(array $data) {

        $id = parent::insert($data);

        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'added');

        return $id;
    }

}