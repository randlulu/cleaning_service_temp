<?php

class Model_CustomerAccessToken extends Zend_Db_Table_Abstract {

    protected $_name = 'customer_access_token';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order($order);


        if ($filters) {
            if (!empty($filters['customer_id'])) {
                $select->where("customer_id = {$filters['customer_id']}");
            }
        }

	
        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }


        return $this->getAdapter()->fetchAll($select);
    }
	

    /**
     * update table rows according to the assigned id and data
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "id = '{$id}'");
    }

    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("id = '{$id}'");
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function getByCustomerId($customerId) {
        $customerId = (int) $customerId;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("customer_id = '{$customerId}'");

        return $this->getAdapter()->fetchRow($select);
    }


	
	public function getGUID(){
		if (function_exists('com_create_guid')){
			return com_create_guid();
		}else{
			mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
			$charid = strtoupper(md5(uniqid(rand(), true)));
			$hyphen = chr(45);// "-"
			$uuid = substr($charid, 0, 8).$hyphen
				.substr($charid, 8, 4).$hyphen
				.substr($charid,12, 4).$hyphen
				.substr($charid,16, 4).$hyphen
				.substr($charid,20,12);
			return $uuid;
		}
	}
	
	
	public function getAccessToken($customer_id){
	  
	  
	  $data = array('customer_id' => $customer_id);
	  $accessTokenData = $this->getByCustomerId($customer_id);
	  if(!empty($accessTokenData) && isset($accessTokenData)){
	    $access_token = $accessTokenData['access_token'];
	  }else{
	    $access_token = $this->getGUID();
		 $data['access_token'] = $access_token;
	    $this->insert($data);
	  }
	  
      return $access_token;
	  
	}
	
	
	public function getByCustomerInfoById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from(array('cat' => $this->_name));
		$select->join(array('c' => 'customer'), 'cat.cutomer_id = c.customer_id');
        
        $select->where("id = {$id}");
        
        return $this->getAdapter()->fetchRow($select);
    }
	public function getByUserInfoByAccessToken($accessToken) {
        
        $select = $this->getAdapter()->select();
        $select->from(array('cat' => $this->_name));
		$select->join(array('c' => 'customer'), 'cat.customer_id = c.customer_id');
        
        $select->where("access_token = '{$accessToken}'");
 
	
        return $this->getAdapter()->fetchRow($select);
    }

	
}
