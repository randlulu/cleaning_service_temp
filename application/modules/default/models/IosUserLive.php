<?php

class Model_IosUser extends Zend_Db_Table_Abstract {

    protected $_name = 'ios_user';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order($order);

        $filters['company_id'] = CheckAuth::getCompanySession();

        if ($filters) {
            if (!empty($filters['user_id'])) {
                $select->where("user_id = {$filters['user_id']}");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }


        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table rows according to the assigned id and data
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "id = '{$id}'");
    }

    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("id = '{$id}'");
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function getByUserId($userId) {
        $userId = (int) $userId;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("user_id = '{$userId}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function getByUserIdAndUUID($userId, $uuid) {
        $userId = (int) $userId;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("user_id = '{$userId}'");
        $select->where("uuid = '{$uuid}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function afterIosLogin($uuid) {

        $loggedUser = Zend_Auth::getInstance()->getIdentity();

        $params = array(
            'last_login' => time()
        );

        $iosLoggedUser = $this->getByUserIdAndUUID($loggedUser->user_id, $uuid);

        if ($iosLoggedUser) {
            $this->updateById($iosLoggedUser['id'], $params);
            return $iosLoggedUser['id'];
        } else {
            $params['user_id'] = $loggedUser->user_id;
            $params['uuid'] = $uuid;
            return $this->insert($params);
        }
    }

}
