<?php

class Model_UserGmailAccounts extends Zend_Db_Table_Abstract {

    protected $_name = 'user_gmail_accounts';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order($order);

        if ($filters) {
            if (!empty($filters['user_gmail_account'])) {
                $select->where("user_gmail_account = {$filters['user_gmail_account']}");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }


        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table rows according to the assigned id and data
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "account_id = '{$id}'");
    }
	
	 public function insert(array $data) {
        
       $id = parent::insert($data);
	   return $id;
    }

    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("account_id = '{$id}'");
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("account_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function getByContractorId($contractor_id) {

        $contractor_id = (int) $contractor_id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("user_id = '{$contractor_id}'");

        return $this->getAdapter()->fetchAll($select);
    }
	
	public function getGmailToken($email)
	{
		$select = $this->getAdapter()->select();
        $select->from($this->_name, array('gmail_token'));
        $select->where("user_gmail_account = '{$email}'");

        return $this->getAdapter()->fetchRow($select);
		
	}
	public function getByEmail($email) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("user_gmail_account = '{$email}'");

        return $this->getAdapter()->fetchRow($select);
    }
	
	public function getAuthEmails($contractor_id) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("user_id = '{$contractor_id}'");
		$select->where("gmail_token is not null");

        return $this->getAdapter()->fetchAll($select);
    }
	
	public function getFirstEmailByContractorId($contractor_id) {

        $contractor_id = (int) $contractor_id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("user_id = '{$contractor_id}'");
		$select->where("gmail_token is not null");
		$select->order("account_id");
		$select->limit(1);

        return $this->getAdapter()->fetchRow($select);
    }
	

}
