<?php

class Model_ClaimOwner extends Zend_Db_Table_Abstract {

    protected $_name = 'claim_owner';
    //fill 
    private $modelBooking;
    private $modelCustomer;
    private $modelCities;
    private $modelCountries;
    private $modelBookingStatus;
    private $modelBookingAddress;
    private $modelBookingLog;
    private $modelUser;
    private $modelBookingEstimate;
    private $modelBookingInvoice;

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {

        $joinInner = array();
        $select = $this->getAdapter()->select();
        $select->from(array('ro' => $this->_name));
        $select->order($order);
        $select->group('booking_id');

        if (empty($filters['request_submit'])) {
            $filters['request_submit'] = 'none';
        }
        if (empty($filters['created_by'])) {
            $loggedUser = CheckAuth::getLoggedUser();
            $filters['created_by'] = $loggedUser['user_id'];
        }

        if ($filters) {
            if (!empty($filters['created_by'])) {
                $created_by = $this->getAdapter()->quote($filters['created_by']);
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'ro.booking_id = bok.booking_id', 'cols' => '');
                $select->where("bok.created_by = {$created_by}");
            }
            if (!empty($filters['request_submit'])) {
                $request_submit = $this->getAdapter()->quote($filters['request_submit']);
                $select->where("request_submit = {$request_submit}");
            }
        }

        if ($joinInner) {
            foreach ($joinInner as $inner) {
                $select->joinInner($inner['name'], $inner['cond'], $inner['cols']);
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table rows according to the assigned id and data
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "request_id= '{$id}'");
    }

    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("request_id= '{$id}'");
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("request_id= '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function getByBookingId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_id = '{$id}'");
        return $this->getAdapter()->fetchAll($select);
    }

    public function getByBookingAndUserId($booking_id, $user_id, $status = 'none') {
        $booking_id = (int) $booking_id;
        $user_id = (int) $user_id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_id = '{$booking_id}'");
        $select->where("user_id = '{$user_id}'");
        $select->where("request_submit = '{$status}'");
        return $this->getAdapter()->fetchRow($select);
    }

    public function getCountClaimOwner($contractor_id = null) {

        $loggedUser = CheckAuth::getLoggedUser();

        $select = $this->getAdapter()->select();
        $select->from(array('ro' => $this->_name));
        $select->where("`ro`.`request_submit` = 'none'");
        $select->joinInner(array('bok' => 'booking'), '`ro`.`booking_id` = `bok`.`booking_id`', '');
        if (isset($contractor_id)) {
            $select->where("`bok`.`created_by` = {$contractor_id}");
        } else {
            $select->where("`bok`.`created_by` = {$loggedUser['user_id']}");
        }
        $requestOwners = $this->getAdapter()->fetchAll($select);

        return count($requestOwners);
    }

    public function getCountClaimOwnerByContractor($dd) {



        $select = $this->getAdapter()->select();
        $select->from(array('ro' => $this->_name));
        $select->where("`ro`.`request_submit` = 'none'");
        $select->joinInner(array('bok' => 'booking'), '`ro`.`booking_id` = `bok`.`booking_id`', '');
        $select->where("`bok`.`created_by` = {$dd}");
        $requestOwners = $this->getAdapter()->fetchAll($select);

        return count($requestOwners);
    }

    /**
     * get extra info for all the rows
     * @param array $row
     * @param array $types
     * @return array
     */
    public function fills(&$rows, $types = array()) {
        foreach ($rows as &$row) {
            $this->fill($row, $types);
        }
    }

    /**
     * get extra info for each row (Ex: getting the category row by c_id, author row by author id ... etc)
     * 
     * @param array $row
     * @param array $types
     * @return array
     */
    public function fill(&$row, $types = array()) {
        if (in_array('booking', $types)) {
            /**
             * load model
             */
            if (!$this->modelBooking) {
                $this->modelBooking = new Model_Booking();
            }

            $booking = $this->modelBooking->getById($row['booking_id']);
            $row['booking'] = $booking;
        }


        if (in_array('customer', $types)) {
            /**
             * load model
             */
            if (!$this->modelBooking) {
                $this->modelBooking = new Model_Booking();
            }
            if (!$this->modelCustomer) {
                $this->modelCustomer = new Model_Customer();
            }
            $booking = $this->modelBooking->getById($row['booking_id']);
            $row['customer'] = $this->modelCustomer->getById($booking['customer_id']);
            $row['customer_name'] = get_customer_name($row['customer']);
        }

        if (in_array('city', $types)) {
            /**
             * load model
             */
            if (!$this->modelBooking) {
                $this->modelBooking = new Model_Booking();
            }
            if (!$this->modelCities) {
                $this->modelCities = new Model_Cities();
            }
            if (!$this->modelCountries) {
                $this->modelCountries = new Model_Countries();
            }
            $booking = $this->modelBooking->getById($row['booking_id']);
            $row['city'] = $this->modelCities->getById($booking['city_id']);
            $row['city_name'] = $row['city']['city_name'];

            $countryName = $this->modelCountries->getById($row['city']['country_id']);
            $row['country_name'] = $countryName['country_name'];
        }

        if (in_array('address', $types)) {
            /**
             * load model
             */
            if (!$this->modelBookingAddress) {
                $this->modelBookingAddress = new Model_BookingAddress();
            }

            $bookingAddress = $this->modelBookingAddress->getByBookingId($row['booking_id']);
            $bookingAddress['line_address'] = get_line_address($bookingAddress);

            $row['address'] = $bookingAddress;
        }

        if (in_array('status', $types)) {
            /**
             * load model
             */
            if (!$this->modelBooking) {
                $this->modelBooking = new Model_Booking();
            }
            if (!$this->modelBookingStatus) {
                $this->modelBookingStatus = new Model_BookingStatus();
            }
            $booking = $this->modelBooking->getById($row['booking_id']);
            $row['status'] = $this->modelBookingStatus->getById($booking['status_id']);
        }


        if (in_array('booking_users', $types)) {
            /**
             * load model
             */
            if (!$this->modelBookingLog) {
                $this->modelBookingLog = new Model_BookingLog();
            }
            if (!$this->modelUser) {
                $this->modelUser = new Model_User();
            }

            $bookingUsers = $this->modelBookingLog->getUsersByBookingId($row['booking_id']);
            $this->modelUser->fills($bookingUsers, array('user'));

            $row['booking_users'] = $bookingUsers;
        }

        if (in_array('estimate', $types)) {
            /**
             * load model
             */
            if (!$this->modelBookingEstimate) {
                $this->modelBookingEstimate = new Model_BookingEstimate();
            }

            $row['estimate'] = $this->modelBookingEstimate->getNotDeletedByBookingId($row['booking_id']);
        }

        if (in_array('invoice', $types)) {
            /**
             * load model
             */
            if (!$this->modelBookingInvoice) {
                $this->modelBookingInvoice = new Model_BookingInvoice();
            }

            $row['invoice'] = $this->modelBookingInvoice->getByBookingId($row['booking_id']);
        }
    }

    public function getUserClaimBybookingIdAsArray($bookingId) {

        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("request_submit = 'none'");
        $select->where("booking_id = '{$bookingId}'");
        $requestOwners = $this->getAdapter()->fetchAll($select);

        $modelUser = new Model_User();
        $usersRequest = array();
        foreach ($requestOwners as $requestOwner) {
            $users = $modelUser->getById($requestOwner['user_id']);
            $usersRequest[$requestOwner['user_id']] = ucwords($users['username']);
        }
        return $usersRequest;
    }

}
