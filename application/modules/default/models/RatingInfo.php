<?php

class Model_RatingInfo extends Zend_Db_Table_Abstract {

    protected $_name = 'rating_info';

    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "rating_info_id = '{$id}'");
    }

    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("rating_info_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function getByContractorIdAndBookingId($contractor_id, $booking_id) {
        $contractor_id = (int) $contractor_id;
        $booking_id = (int) $booking_id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("contractor_id = '{$contractor_id}'");
        $select->where("item_id = '{$booking_id}'");
        return $this->getAdapter()->fetchRow($select);
    }
    

}
