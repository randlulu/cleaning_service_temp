<?php

class Model_GoogleDrive extends Zend_Db_Table_Abstract {


    public function getClientGmailAccounts($contractorId, &$client) {
        // load model
        $modelContractorGmailAccounts = new Model_ContractorGmailAccounts();

        // get the gmail account contractor
        $contractorAccount = $modelContractorGmailAccounts->getByContractorId($contractorId);
        $email = isset($contractorAccount['email']) ? $contractorAccount['email'] : '';
        $password = isset($contractorAccount['password']) ? $contractorAccount['password'] : '';
		
        $isValid = false;
        if ($email && $password) {
            $isValid = true;
            try {
                // Parameters for ClientAuth authentication
				$authenticationServiceName = Zend_Gdata_Docs::AUTH_SERVICE_NAME;

				// Create an authenticated HTTP client
				$client = Zend_Gdata_ClientLogin::getHttpClient($email, $password, $authenticationServiceName);
                
            } catch (Zend_Gdata_App_Exception $ae) {
                $isValid = false;
            }
        }

        return $isValid;
    }

    public function getClientGmailAccounts2($contractorId, &$client) {
        // load model
        $modelContractorGmailAccounts = new Model_ContractorGmailAccounts();

        // get the gmail account contractor
        $contractorAccount = $modelContractorGmailAccounts->getByContractorId($contractorId);
        $email = isset($contractorAccount['email']) ? $contractorAccount['email'] : '';
        $password = isset($contractorAccount['password']) ? $contractorAccount['password'] : '';
		
        $isValid = false;
        if ($email && $password) {
            $isValid = true;
            try {
                // Parameters for ClientAuth authentication
				$authenticationServiceName = Zend_Gdata_Docs::AUTH_SERVICE_NAME;

				// Create an authenticated HTTP client
				$client = Zend_Gdata_ClientLogin::getHttpClient($email, $password, $authenticationServiceName);
				
				$service = Zend_Gdata_Calendar::AUTH_SERVICE_NAME; // predefined service name for calendar

				$client = Zend_Gdata_ClientLogin::getHttpClient($email,$password,$service);
				
                
            } catch (Zend_Gdata_App_Exception $ae) {
				echo 'Error';
				echo $ae->__toString();
				
                $isValid = false;
            }
        }

        return $isValid;
    }

	
	
	function uploadDocumentToGoogleDrive($contractorId, $html, $originalFileName,$fileName) {
		$client = null;
        $isValid = $this->getClientGmailAccounts($contractorId, $client);

        if ($isValid) {
			try{
				  $fileToUpload = $originalFileName;
				  /*if ($temporaryFileLocation) {
					$fileToUpload = $temporaryFileLocation;
				  }*/
				 
				  
					$filenameParts = explode('.', $fileToUpload);
					$fileExtension = end($filenameParts);
					$filenameParts = explode('/', $fileToUpload);
					if($fileExtension =='pdf'){
						$uploader = new Ext_Gdata_Uploader($client, $fileToUpload, $fileName);
						$entry = $uploader->Upload();
					}
					else{
					   // Upload the file and convert it into a Google Document. The original
					  // file name is used as the title of the document and the MIME type
					  // is determined based on the extension on the original file name.
						$docs = new Zend_Gdata_Docs($client);
						$feed = $docs->getDocumentListFeed();
						$newDocumentEntry = $docs->uploadFile($fileToUpload, $fileName,
						Zend_Gdata_Docs::lookupMimeType($fileExtension), Zend_Gdata_Docs::DOCUMENTS_LIST_FEED_URI);
				    }
				 // $newDocumentEntry = $docs->uploadFile($fileToUpload, $originalFileName,null, Zend_Gdata_Docs::DOCUMENTS_LIST_FEED_URI);
				  return 'success';
			}
		 catch (Zend_Gdata_App_Exception $ae) {
				return 'fail';
            }
		
		}
		
		else{
		return 'invaild';
		}
	}

}
