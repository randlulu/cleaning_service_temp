<?php

class Model_Payment extends Zend_Db_Table_Abstract {

    protected $_name = 'payment';
    private $modelCustomer;
    private $modelPaymentType;
    private $modelBookingInvoice;

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null, $limit = 0, $perPage = 0, $currentPage = 0, $is_count = 0) {
        $select = $this->getAdapter()->select();
        if ($is_count) {
            $select->from(array('pay' => $this->_name), array('count' => 'count(DISTINCT(pay.payment_id))'));
            $select->join(array('book' => 'booking'), 'book.booking_id = pay.booking_id', array());
        } else {
            $select->from(array('pay' => $this->_name));
            $select->join(array('book' => 'booking'), 'book.booking_id = pay.booking_id', array('is_change', 'booking_num'));
        }
        $select->where('book.is_deleted = 0');
        $select->order($order);
        $select->distinct();

        /* if (!empty($filters['is_deleted'])) {
          $select->where('pay.is_deleted = 1');
          } else { */
        $select->where('pay.is_deleted = 0');
        //}

        $joinInner = array();
        $filters['company_id'] = CheckAuth::getCompanySession();


        $loggedUser = CheckAuth::getLoggedUser();

        if ($loggedUser) {
            if (!CheckAuth::checkCredential(array('canSeeAllPayment'))) {
                if (CheckAuth::checkCredential(array('canSeeOnlyHisPayment'))) {
                    if (CheckAuth::checkCredential(array('canSeeAssignedPayment'))) {
                        $joinInner['contractor_service_booking'] = array('name' => array('csb' => 'contractor_service_booking'), 'cond' => 'pay.booking_id = csb.booking_id', 'cols' => '');
                        $joinInner['user'] = array('name' => array('usr' => 'user'), 'cond' => 'usr.user_id = pay.user_id', 'cols' => '');

                        $select->where("pay.user_id = {$loggedUser['user_id']} OR csb.contractor_id = {$loggedUser['user_id']}");
                        $select->where('usr.is_deleted = 0');
                    } else {
                        $joinInner['user'] = array('name' => array('usr' => 'user'), 'cond' => 'usr.user_id = pay.user_id', 'cols' => '');

                        $select->where("pay.user_id = " . $loggedUser['user_id']);
                        $select->where('usr.is_deleted = 0');
                    }
                } else {
                    $joinInner['user'] = array('name' => array('usr' => 'user'), 'cond' => 'usr.user_id = pay.user_id', 'cols' => '');

                    $select->where("pay.user_id = " . $loggedUser['user_id']);
                    $select->where('usr.is_deleted = 0');
                }
            }
        }

        if ($filters) {
            if (!empty($filters['keywords'])) {
                $keywords = $this->getAdapter()->quote('%' . $filters['keywords'] . '%');
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'bok.booking_id = pay.booking_id', 'cols' => '');
                $select->where("bok.full_text_search LIKE {$keywords}");
            }

            if (!empty($filters['booking_id'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'bok.booking_id = pay.booking_id', 'cols' => '');

                $select->where("pay.booking_id = {$filters['booking_id']}");
                //$select->where('bok.is_deleted = 0');
            }

            if (!empty($filters['customer_id'])) {
                $joinInner['customer'] = array('name' => array('cus' => 'customer'), 'cond' => 'cus.customer_id = pay.customer_id', 'cols' => '');

                $select->where("pay.customer_id = {$filters['customer_id']}");
                $select->where('cus.is_deleted = 0');
            }

            if (!empty($filters['payment_type_id'])) {
                $select->where("pay.payment_type_id = {$filters['payment_type_id']}");
            }

            if (!empty($filters['is_approved'])) {
                if ('yes' == $filters['is_approved']) {
                    $select->where('pay.is_approved = 1');
                } elseif ('no' == $filters['is_approved']) {
                    if (!empty($filters['contractor_id'])) {
                        if (!isset($joinInner['contractor_service_booking'])) {
                            $joinInner['contractor_service_bookings'] = array('name' => array('csb' => 'contractor_service_booking'), 'cond' => 'pay.booking_id = csb.booking_id', 'cols' => '');
                        }
                        $select->where("csb.contractor_id = {$filters['contractor_id']}");
                        $select->where('pay.is_approved = 0');
                    } else {
                        $select->where('pay.is_approved = 0');
                    }
                }
            }
            if (!empty($filters['payment_created_between'])) {
                $paymentCreatedBetween = $filters['payment_created_between'];
                $paymentEndBetween = !empty($filters['payment_end_between']) ? $filters['payment_end_between'] : date('Y-m-d H:i:s', time());
                $select->where("pay.received_date between '" . strtotime($paymentCreatedBetween) . "' and '" . strtotime($paymentEndBetween) . "'");
            }
            if (!empty($filters['company_id'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'bok.booking_id = pay.booking_id', 'cols' => '');

                $company_id = (int) $filters['company_id'];
                $select->where("bok.company_id = {$company_id}");
            }
            if (!empty($filters['order_by']) && $filters['order_by'] == 'payment_type') {
                $joinInner['payment_type'] = array('name' => array('pyt' => 'payment_type'), 'cond' => 'pyt.id = pay.payment_type_id', 'cols' => '');
            }
            if (!empty($filters['is_mark'])) {
                $select->where('pay.is_mark = 1');
            }
            if (!empty($filters['file_import'])) {
                if (!empty($filters['is_matched']) && $filters['is_matched'] == 1) {
                    $select->where('pay.file_import = 1');
                    $select->where('pay.is_matched = 1');
                } else {
                    $select->where('pay.file_import = 1');
                    $select->where('pay.is_matched = 0');
                }
            }
            if (!empty($filters['file_number'])) {
                $file_number = $filters['file_number'];
                $select->where("pay.file_number = {$file_number}");
            }
            
             if (!empty($filters['auto_matched'])) {
                $select->where("pay.auto_matched = {$filters['auto_matched']}");
            }
        }

        if ($joinInner) {
            foreach ($joinInner as $inner) {
                $select->joinInner($inner['name'], $inner['cond'], $inner['cols']);
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        } elseif ($perPage && $currentPage) {
            $select->limit($perPage, ($currentPage - 1) * $perPage);
        }
        //echo $select;die;

		//echo $select->__toString();
        if ($is_count) {
            return $this->getAdapter()->fetchOne($select);
        } else {
            return $this->getAdapter()->fetchAll($select);
        }
    }

    //Added by Salim
    function getByRefernce($refernce, $order = null, &$pager = null, $limit = 0, $perPage = 0, $currentPage = 0) {
        $select = $this->getAdapter()->select();
        $select->from(array('pay' => $this->_name));
        $select->joinLeft(array('book' => 'booking'), 'book.booking_id = pay.booking_id', array('is_change', 'booking_num'));
        $select->order($order);
        $select->distinct();

        if (!empty($filters['is_deleted'])) {
            $select->where('pay.is_deleted = 1');
        } else {
            $select->where('pay.is_deleted = 0');
        }
  
        $joinInner = array();
        $filters['company_id'] = CheckAuth::getCompanySession();


        $loggedUser = CheckAuth::getLoggedUser();

        if ($loggedUser) {
            if (!CheckAuth::checkCredential(array('canSeeAllPayment'))) {
                if (CheckAuth::checkCredential(array('canSeeOnlyHisPayment'))) {
                    if (CheckAuth::checkCredential(array('canSeeAssignedPayment'))) {
                        $joinInner['contractor_service_booking'] = array('name' => array('csb' => 'contractor_service_booking'), 'cond' => 'pay.booking_id = csb.booking_id', 'cols' => '');
                        $joinInner['user'] = array('name' => array('usr' => 'user'), 'cond' => 'usr.user_id = pay.user_id', 'cols' => '');

                        $select->where("pay.user_id = {$loggedUser['user_id']} OR csb.contractor_id = {$loggedUser['user_id']}");
                        $select->where('usr.is_deleted = 0');
                    } else {
                        $joinInner['user'] = array('name' => array('usr' => 'user'), 'cond' => 'usr.user_id = pay.user_id', 'cols' => '');

                        $select->where("pay.user_id = " . $loggedUser['user_id']);
                        $select->where('usr.is_deleted = 0');
                    }
                } else {
                    $joinInner['user'] = array('name' => array('usr' => 'user'), 'cond' => 'usr.user_id = pay.user_id', 'cols' => '');

                    $select->where("pay.user_id = " . $loggedUser['user_id']);
                    $select->where('usr.is_deleted = 0');
                }
            }
        }
        if (!empty($refernce)) {
            $keywords = $this->getAdapter()->quote('%' . $refernce . '%');

            $select->where("pay.reference LIKE {$keywords}");
        }

        if ($joinInner) {
            foreach ($joinInner as $inner) {
                $select->joinInner($inner['name'], $inner['cond'], $inner['cols']);
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        } elseif ($perPage && $currentPage) {
            $select->limit($perPage, ($currentPage - 1) * $perPage);
        }




        //echo $select;die;
        return $this->getAdapter()->fetchAll($select);
    }

    ///By Islam

    public function getPaymentDetailsFromEwayByTransactionId($transaction_id) {

        Zend_Loader::loadFile('include_eway.php', APPLICATION_PATH . '/../library/eway-rapid-php-master');

        $apiKey = '44DD7AhRBX08hfSoic2wv9mAF/Bs5St52K15ISLq6EaA29SCLz7GuY+G6eyiJkDLUvRKRU';
        $apiPassword = '72tQqX16';
        $apiEndpoint = 'production';
        $apiEndpoint = 'https://api.ewaypayments.com/TransactionSearch.json';
        // $apiEndpoint = 'https://api.ewaypayments.com/Transaction';

        $client = \Eway\Rapid::createClient($apiKey, $apiPassword, $apiEndpoint);

        $response = $client->queryTransaction($transaction_id);

        //TransactionDateTime
        //echo 'TransactionDateTime '.$response->TransactionDateTime; 
        //if(!empty($response->Transactions)){
        //$response = $response->Transactions[0];
        print_r($response);
        return;
        if ($response->TransactionStatus) {
            $responseMessage = split(', ', $response->ResponseMessage);
            foreach ($responseMessage as $msg) {
                echo "ResponseMessage: " . \Eway\Rapid::getMessage($msg) . "<br>";
            }
            return 1;
        } else {
            $errors = split(', ', $response->ResponseMessage);
            foreach ($errors as $error) {
                echo "Payment failed: " . \Eway\Rapid::getMessage($error) . "<br>";
            }
            return 0;
        }
        /* }else{
          echo "Invalid transaction id <br>";
          return 0;
          } */
    }

    public function checkUnapprovedPaymentStatusFromEwayUsingReference() {

        $select = $this->getAdapter()->select();
        $select->from(array('pay' => $this->_name));
        $select->where('pay.is_deleted = 0');
        $select->where('pay.is_approved = 0');

        $unapprovedPayments = $this->getAdapter()->fetchAll($select);
        foreach ($unapprovedPayments as $unapprovedPayment) {
            if (!empty($unapprovedPayment['reference'])) {
                $approved = $this->getPaymentDetailsFromEwayByTransactionId($unapprovedPayment['reference']);
                if ($approved) {
                    $data = array('is_approved' => 1, 'approved_by' => 'system');
                    //$this->updateById($unapprovedPayment['payment_id'],$data);
                }
            }
        }
    }

    /**
     * update table rows according to the assigned id and date
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;

        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'changed');

        $payment = $this->getById($id);


        if ($payment['booking_id'] && isset($payment['booking_id'])) {
            $modelIosSync = new Model_IosSync();
            $modelIosSync->changeSyncBooking($payment['booking_id']);
        }

        $update_id = parent::update($data, "payment_id = '{$id}'");
        //D.A 30/09/2015 Remove Invoice Cache
        if ($update_id) {
            require_once 'Zend/Cache.php';
            $company_id = CheckAuth::getCompanySession();
            $payment = $this->getById($id);
            $modelBookingInvoice = new Model_BookingInvoice();
            $invoice = $modelBookingInvoice->getByBookingId($payment['booking_id']);
            if ($invoice) {
                $invoicePaymentToTechnicianCacheID = $invoice['id'] . '_invoicePaymentToTechnician';
                $invoiceDetailsCacheID = $invoice['id'] . '_invoiceDetails';
                $invoiceParamsCacheID = $invoice['id'] . '_invoiceParams';
                $invoiceViewDir = get_config('cache') . '/' . 'invoicesView' . '/' . $company_id;
                if (!is_dir($invoiceViewDir)) {
                    mkdir($invoiceViewDir, 0777, true);
                }
                $invoiceFrontEndOption = array('lifetime' => NULL,
                    'automatic_serialization' => true);
                $invoiceBackendOptions = array('cache_dir' => $invoiceViewDir);
                $invoiceCache = Zend_Cache::factory('Core', 'File', $invoiceFrontEndOption, $invoiceBackendOptions);
                $invoiceCache->remove($invoicePaymentToTechnicianCacheID);
                $invoiceCache->remove($invoiceDetailsCacheID);
                $invoiceCache->remove($invoiceParamsCacheID);
            }
        }
        return $update_id;
    }

    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;

        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'deleted');
        $payment = $this->getById($id);
        $booking_id = $payment['booking_id'];


        if ($booking_id && isset($booking_id)) {
            $modelIosSync = new Model_IosSync();
            $modelIosSync->changeSyncBooking($booking_id);
        }

        $payment_id = parent::delete("payment_id = '{$id}'");

        //D.A 30/09/2015 Remove Invoice Cache
        if ($payment_id) {
            require_once 'Zend/Cache.php';
            $company_id = CheckAuth::getCompanySession();
            $modelBookingInvoice = new Model_BookingInvoice();
            $invoice = $modelBookingInvoice->getByBookingId($booking_id);
            if ($invoice) {
                $invoicePaymentToTechnicianCacheID = $invoice['id'] . '_invoicePaymentToTechnician';
                $invoiceDetailsCacheID = $invoice['id'] . '_invoiceDetails';
                $invoiceParamsCacheID = $invoice['id'] . '_invoiceParams';
                $invoiceViewDir = get_config('cache') . '/' . 'invoicesView' . '/' . $company_id;
                if (!is_dir($invoiceViewDir)) {
                    mkdir($invoiceViewDir, 0777, true);
                }
                $invoiceFrontEndOption = array('lifetime' => NULL,
                    'automatic_serialization' => true);
                $invoiceBackendOptions = array('cache_dir' => $invoiceViewDir);
                $invoiceCache = Zend_Cache::factory('Core', 'File', $invoiceFrontEndOption, $invoiceBackendOptions);
                $invoiceCache->remove($invoicePaymentToTechnicianCacheID);
                $invoiceCache->remove($invoiceDetailsCacheID);
                $invoiceCache->remove($invoiceParamsCacheID);
            }
        }
        return $payment_id;
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("payment_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the id and booking id
     * 
     * @param int $id
     * @param int $booking_id
     * @return array 
     */
    public function getByIdAndBookingId($id, $booking_id) {
        $id = (int) $id;
        $booking_id = (int) $booking_id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("payment_id = '{$id}'");
        $select->where("booking_id = '{$booking_id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the booking id
     * 
     * @param int $booking_id
     * @return array 
     */
    /* public function getByBookingId($booking_id) {
      $booking_id = (int) $booking_id;
      $select = $this->getAdapter()->select();
      $select->from($this->_name);
      $select->where("booking_id = '{$booking_id}'");

      return $this->getAdapter()->fetchAll($select);
      } */
    public function getByBookingId($booking_id) {
        $booking_id = (int) $booking_id;
        $select = $this->getAdapter()->select();
        $select->from(array('p' => $this->_name));
        $select->joinInner(array('pt' => 'payment_type'), 'p.payment_type_id = pt.id', array('payment_type'));
        $select->joinLeft(array('c' => 'customer_commercial_info'), 'p.customer_id = c.customer_id', array('business_name'));
        $select->where("p.booking_id = '{$booking_id}'");
        $select->where("p.is_deleted = 0");

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * get total amount according to the assigned filters
     * 
     * @param array $filters
     * @return array 
     */
    public function getTotalAmount($filters = array()) {
        $totalPayment = $this->getTotalPayment($filters);

        $modelRefund = new Model_Refund();
        $totalRefund = $modelRefund->getTotalRefund($filters);

        return $totalPayment - $totalRefund;
    }

    public function getTotalPayment($filters = array()) {
        $select = $this->getAdapter()->select();
        $select->from(array('pay' => $this->_name), array('pay.payment_id', 'pay.amount'));

        $joinInner = array();

        $select->distinct();

        if (!empty($filters['is_approved'])) {
            if ('all' == $filters['is_approved']) {
                $select->where("pay.is_approved = 1 OR pay.is_approved = 0");
            } elseif ('yes' == $filters['is_approved']) {
                $select->where('pay.is_approved = 1');
            } elseif ('no' == $filters['is_approved']) {
                $select->where('pay.is_approved = 0');
            }
        } else {
            $select->where('pay.is_approved = 1');
        }

        $select->where('pay.is_deleted = 0');

        $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'bok.booking_id = pay.booking_id', 'cols' => '');
        $select->where('bok.is_deleted = 0');

        $filters['company_id'] = CheckAuth::getCompanySession();

        if ($filters) {
            if (!empty($filters['contractor_id'])) {
                $joinInner['contractor_service_booking'] = array('name' => array('csb' => 'contractor_service_booking'), 'cond' => 'bok.booking_id = csb.booking_id', 'cols' => '');
                $select->where("bok.created_by = " . $filters['contractor_id'] . " OR csb.contractor_id = {$filters['contractor_id']}");
            }

            if (!empty($filters['booking_id'])) {
                $booking_id = $this->getAdapter()->quote(trim($filters['booking_id']));
                $select->where("bok.booking_id = {$booking_id}");
            }

            if (!empty($filters['booking_start_between'])) {
                $bookingStartBetween = $filters['booking_start_between'];
                $bookingEndBetween = !empty($filters['booking_end_between']) ? $filters['booking_end_between'] : date('Y-m-d H:i:s', time());
                $select->where("bok.booking_start between '" . $bookingStartBetween . "' and '" . $bookingEndBetween . "'");
            }

            if (!empty($filters['invoice_type'])) {
                $joinInner['booking_invoice'] = array('name' => array('inv' => 'booking_invoice'), 'cond' => 'bok.booking_id = inv.booking_id', 'cols' => '');

                $invoice_type = $this->getAdapter()->quote(trim($filters['invoice_type']));
                if ('unpaid' == $filters['invoice_type']) {
                    $select->where("inv.invoice_type = 'overdue' OR inv.invoice_type = 'open'");
                } elseif ('all_active' == $filters['invoice_type']) {
                    $select->where("inv.invoice_type = 'overdue' OR inv.invoice_type = 'open' OR inv.invoice_type = 'closed'");
                } else {
                    $select->where("inv.invoice_type = {$invoice_type}");
                }
            }

            if (!empty($filters['booking_start'])) {
                $bookingStart = $this->getAdapter()->quote(trim($filters['booking_start']));
                $select->where("bok.booking_start >= {$bookingStart}");
            }

            if (!empty($filters['booking_end'])) {
                $bookingEnd = $this->getAdapter()->quote(trim($filters['booking_end']));
                $select->where("bok.booking_end <= {$bookingEnd}");
            }

            if (!empty($filters['payment_type'])) {
                $paymentType = $this->getAdapter()->quote(trim($filters['payment_type']));
                $select->where("pay.payment_type_id = {$paymentType}");
            }
            if (!empty($filters['payment_contractor_id'])) {
                $select->where("pay.contractor_id = {$filters['payment_contractor_id']}");
            }

            if (!empty($filters['without_cash'])) {
                $modelPaymentType = new Model_PaymentType();
                $cash = $modelPaymentType->getPaymentIdBySlug('cash');

                $select->where("pay.payment_type_id != {$cash}");
            }

            if (!empty($filters['convert_status'])) {
                $convert_status = $this->getAdapter()->quote(trim($filters['convert_status']));
                $select->where("bok.convert_status = {$convert_status}");
            }

            if (!empty($filters['status'])) {
                $status = $this->getAdapter()->quote(trim($filters['status']));
                $select->where("bok.status_id = {$status}");
            }

            if (!empty($filters['more_one_status'])) {

                $select->where('bok.status_id IN (' . implode(', ', $filters['more_one_status']) . ')');
            }

            if (!empty($filters['created_by'])) {
                $user_id = (int) $filters['created_by'];
                $select->where("bok.created_by = '{$user_id}'");
            }
            if (!empty($filters['company_id'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'bok.booking_id = pay.booking_id', 'cols' => '');

                $company_id = (int) $filters['company_id'];
                $select->where("bok.company_id = {$company_id}");
            }
        }

        if ($joinInner) {
            foreach ($joinInner as $inner) {
                $select->joinInner($inner['name'], $inner['cond'], $inner['cols']);
            }
        }

        $selectAmount = $this->getAdapter()->select();
        $selectAmount->from($select, 'SUM(amount)');

		//echo $selectAmount->__toString();
		
        return $this->getAdapter()->fetchOne($selectAmount);
    }

    public function fills(&$rows, $types = array()) {
        foreach ($rows as &$row) {
            $this->fill($row, $types);
        }
    }

    /**
     * get extra info for each row (Ex: getting the category row by c_id, author row by author id ... etc)
     * 
     * @param array $row
     * @param array $types
     * @return array
     */
    public function fill(&$row, $types = array()) {

        if (in_array('customer', $types)) {
            if (!$this->modelCustomer) {
                $this->modelCustomer = new Model_Customer();
            }
            $customer = $this->modelCustomer->getById($row['customer_id']);
            $row['customer'] = get_customer_name($customer);
        }

        if (in_array('invoice', $types)) {
            if (!$this->modelBookingInvoice) {
                $this->modelBookingInvoice = new Model_BookingInvoice();
            }
            $row['invoice'] = $this->modelBookingInvoice->getByBookingId($row['booking_id']);
        }

        if (in_array('payment_type', $types)) {
            if (!$this->modelPaymentType) {
                $this->modelPaymentType = new Model_PaymentType();
            }
            $paymentType = $this->modelPaymentType->getById($row['payment_type_id']);
            $row['payment_type'] = $paymentType['payment_type'];
        }
        if (in_array('total_amount', $types)) {
            $total = 0;
            $total +=$row['amount'];
            $row['total_amount'] = $total;
        }
    }

    public function insert(array $data) {

        $id = parent::insert($data);

        if ($data['booking_id'] && isset($data['booking_id'])) {
            $modelIosSync = new Model_IosSync();
            $modelIosSync->changeSyncBooking($data['booking_id']);
        }

        //D.A 30/09/2015 Remove Invoice Cache
        if ($id) {
            require_once 'Zend/Cache.php';
            $company_id = CheckAuth::getCompanySession();
            $modelBookingInvoice = new Model_BookingInvoice();
            $invoice = $modelBookingInvoice->getByBookingId($data['booking_id']);
            if ($invoice) {
                $invoicePaymentToTechnicianCacheID = $invoice['id'] . '_invoicePaymentToTechnician';
                $invoiceDetailsCacheID = $invoice['id'] . '_invoiceDetails';
                $invoiceParamsCacheID = $invoice['id'] . '_invoiceParams';
                $invoiceViewDir = get_config('cache') . '/' . 'invoicesView' . '/' . $company_id;
                if (!is_dir($invoiceViewDir)) {
                    mkdir($invoiceViewDir, 0777, true);
                }
                $invoiceFrontEndOption = array('lifetime' => NULL,
                    'automatic_serialization' => true);
                $invoiceBackendOptions = array('cache_dir' => $invoiceViewDir);
                $invoiceCache = Zend_Cache::factory('Core', 'File', $invoiceFrontEndOption, $invoiceBackendOptions);
                $invoiceCache->remove($invoicePaymentToTechnicianCacheID);
                $invoiceCache->remove($invoiceDetailsCacheID);
                $invoiceCache->remove($invoiceParamsCacheID);
            }
        }

        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'added');

        return $id;
    }

    public function getCountUnapprovedPayments($contractor_id = null) {

        if (isset($contractor_id)) {
            $unapprovedPayments = $this->getAll(array('is_approved' => 'no', 'contractor_id' => $contractor_id));
        } else {
            $unapprovedPayments = $this->getAll(array('is_approved' => 'no'));
        }
        return count($unapprovedPayments);
    }

    public function getLastBookingPaymentByBookingId($booking_id) {
        $booking_id = (int) $booking_id;
        $select = $this->getAdapter()->select();
        $select->from(array('p' => $this->_name));
        $select->joinInner(array('pt' => 'payment_type'), 'p.payment_type_id = pt.id', array('payment_type'));
        $select->joinLeft(array('c' => 'customer_commercial_info'), 'p.customer_id = c.customer_id', array('business_name'));
        $select->where("p.booking_id = '{$booking_id}'");
        $select->where('p.is_approved = 1');
        $select->order("payment_id DESC");
        $select->limit(1);

        return $this->getAdapter()->fetchRow($select);
    }

    public function getCountUnapprovedPaymentsForContractor($dd) {
        $filters['contractor_id'] = $dd;
        $filters['is_approved'] = 'no';
        $unapprovedPayments = $this->getAll($filters);

        return count($unapprovedPayments);
    }

    public function getCashPayments($bookingId) {

        $booking_id = (int) $bookingId;
        $select = $this->getAdapter()->select();
        $select->from(array('p' => $this->_name), array('SUM(`p`.`amount`) as cash_amount'));
        $select->joinInner(array('pt' => 'payment_type'), 'p.payment_type_id = pt.id', array());
        $select->where("p.booking_id = '{$booking_id}'");
        $select->where("pt.payment_type = 'Cash'");
        //echo $select->__toString();

        return $this->getAdapter()->fetchRow($select);
    }

    public function getPaymentByBookingIdAndContractorId($booking_id, $contractor_id) {
        $booking_id = (int) $booking_id;
        $contractor_id = (int) $contractor_id;

        $select = $this->getAdapter()->select();
        $select->from(array('p' => $this->_name));
        $select->where("p.booking_id = '{$booking_id}'");
        $select->where("p.contractor_id = '{$contractor_id}'");
		$select->where("p.is_deleted = 0");
        $payments = $this->getAdapter()->fetchRow($select);
        if (isset($payments) && !empty($payments)) {
            return 1;
        }
        return 0;
    }

    //By IBM check payment reference 
    public function getPaymentReference($reference) {

        $select = $this->getAdapter()->select();
        $select->from(array('p' => $this->_name));
        $select->where("p.reference = '{$reference}'");
        $select->where("p.is_deleted = 0");
               $select->where('p.booking_id != 0');
        $payments = $this->getAdapter()->fetchRow($select);
        return $payments;
    }

    public function getNotMatchedPayments($filters = array(), $order = null, &$pager = null, $limit = 0, $perPage = 0, $currentPage = 0) {
        $select = $this->getAdapter()->select();


        $select->from(array('pay' => $this->_name));
        $select->order('pay.received_date');
        $select->distinct();
        $select->where('pay.is_deleted = 0');
        $select->where('pay.file_import = 1');
        $select->where('pay.is_matched = 0');
        $select->where('pay.booking_id = 0');
        if (!empty($filters['file_number'])) {
            $file_number = $filters['file_number'];
            $select->where("pay.file_number = {$file_number}");
        }
        if (!empty($filters['is_approved'])) {
            if ('yes' == $filters['is_approved']) {
                $select->where('pay.is_approved = 1');
            } elseif ('no' == $filters['is_approved']) {
                if (!empty($filters['contractor_id'])) {
                    if (!isset($joinInner['contractor_service_booking'])) {
                        $joinInner['contractor_service_bookings'] = array('name' => array('csb' => 'contractor_service_booking'), 'cond' => 'pay.booking_id = csb.booking_id', 'cols' => '');
                    }
                    $select->where("csb.contractor_id = {$filters['contractor_id']}");
                    $select->where('pay.is_approved = 0');
                } else {
                    $select->where('pay.is_approved = 0');
                }
            }
        }
        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        } elseif ($perPage && $currentPage) {
            $select->limit($perPage, ($currentPage - 1) * $perPage);
        }
//        if(my_ip()){
//            echo $select;
//        }
        $result = $this->getAdapter()->fetchAll($select);
        //echo $select;die;
//        echo $select->__toString();
        if (isset($filters['is_count']) && $filters['is_count']) {
            return count($result);
        } else {
            return $result;
        }
    }

    public function getFileImportPayments($filters = array(), $order = null, &$pager = null, $limit = 0, $perPage = 0, $currentPage = 0) {
        $select = $this->getAdapter()->select();
        $select->from(array('pay' => $this->_name));
        $select->order('pay.received_date');
        $select->distinct();
        $select->joinLeft(array('book' => 'booking'), 'book.booking_id = pay.booking_id', array('is_change', 'booking_num'));
        $select->where('book.is_deleted = 0 or book.is_deleted is null');
        $select->where('pay.is_deleted = 0');
        $select->where('pay.file_import = 1');
//        $select->where('pay.is_approved = 0');
        if (!empty($filters['file_number'])) {
            $file_number = $filters['file_number'];
            $select->where("pay.file_number = {$file_number}");
        }
        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        } elseif ($perPage && $currentPage) {
            $select->limit($perPage, ($currentPage - 1) * $perPage);
        }
        $result = $this->getAdapter()->fetchAll($select);
        if (isset($filters['is_count']) && $filters['is_count']) {
            return count($result);
        } else {

            return $result;
        }
    }

    public function getPaymentByReferencAndDateAndAmount($referenceWithSpaces, $date, $amount, $approve = 'all', $payment_type = 'credit', $file_number = 0, $fromFile = 0) {
        $referenceWithSpaces = addslashes($referenceWithSpaces);
        $select = $this->getAdapter()->select();
        $select->from(array('pay' => $this->_name));
        $select->distinct();
        $select->where('pay.is_deleted = 0');

        if ($payment_type == 'bank') {
            $select->where("trim(pay.reference) = '{$referenceWithSpaces}'");
            $select->where("pay.received_date = {$date}");
            if ($fromFile) {
                $select->where("pay.file_number = {$file_number}");
            } else {
                $select->where("pay.file_number != {$file_number}");
            }
        } else {
            $select->where("trim(pay.reference) REGEXP '[[:<:]]{$referenceWithSpaces}[[:>:]]'");
        }

        $select->where("pay.amount = {$amount}");
        if ($approve == 'yes') {
            $select->where("pay.is_approved = 1");
        } else if ($approve == 'no') {
            $select->where("pay.is_approved = 0");
        }

//        if(my_ip()){
//            echo $select;
//            exit;
////            var_dump($this->getAdapter()->fetchAll($select));
//        }

        return $this->getAdapter()->fetchAll($select);
    }

    public function fixPayments() {

        $select = $this->getAdapter()->select();
        $select->from(array('p' => $this->_name));
        $select->joinInner(array('bok' => 'booking'), 'bok.booking_id = p.booking_id');
        $select->where("p.is_deleted = 0");
        $select->where("p.is_test != 0");
        $select->where("p.booking_id != 0");

        return $this->getAdapter()->fetchAll($select);
    }

	
	public function getNumberOfDistinctContractorsByBookingId($bookingId){
		$bookingId = (int) $bookingId;
		$select = $this->getAdapter()->select();
        $select->from(array('pay' => $this->_name), array('count' => 'count(DISTINCT(pay.contractor_id))'));
		$select->join(array('book' => 'booking'), 'book.booking_id = pay.booking_id', array());
		$select->where('book.is_deleted = 0');
		$select->where('pay.is_deleted = 0');
		$select->where("pay.booking_id = {$bookingId}");
		$select->where('pay.contractor_id != 0');
		$select->group('pay.booking_id');
		//echo $select->__toString();
		return $this->getAdapter()->fetchOne($select);
		
	}
	
}
