<?php

class Model_BookingStatus extends Zend_Db_Table_Abstract {

    protected $_name = 'booking_status';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order($order);
		
		
		if (!CheckAuth::checkCredential(array('canSeeAllStatuses'))) {
			
                if(!CheckAuth::checkCredential(array('canSeeInProcess'))){
					$select->where("name != 'IN PROGRESS'");
				}
				if(!CheckAuth::checkCredential(array('canSeeCompleted'))){
					$select->where("name != 'COMPLETED'");
				}
				if(!CheckAuth::checkCredential(array('canSeeFailed'))){
					$select->where("name != 'FAILED'");
				}
				if(!CheckAuth::checkCredential(array('canSeeQuoted'))){
					$select->where("name != 'QUOTED'");
				}
				if(!CheckAuth::checkCredential(array('canSeeToDo'))){
					$select->where("name != 'TO DO'");
				}
				if(!CheckAuth::checkCredential(array('canSeeOnHold'))){
					$select->where("name != 'ON HOLD'");
				}
				if(!CheckAuth::checkCredential(array('canSeeCancelled'))){
					$select->where("name != 'CANCELLED'");
				}
				if(!CheckAuth::checkCredential(array('canSeeAwaitingUpdate'))){
					$select->where("name != 'AWAITING UPDATE'");
				}
				if(!CheckAuth::checkCredential(array('canSeeTentative'))){
					$select->where("name != 'TENTATIVE'");
				}
				if(!CheckAuth::checkCredential(array('canSeeToVisit'))){
					$select->where("name != 'TO VISIT'");
				}
            } 


        if (!isset($filters['company_id'])) {
            $filters['company_id'] = CheckAuth::getCompanySession();
        }

        if ($filters) {
            if (!empty($filters['keywords'])) {
                $keywords = $this->getAdapter()->quote('%' . $filters['keywords'] . '%');
                $select->where("name LIKE {$keywords}");
            }

            if (!empty($filters['company_id'])) {
                $select->where("company_id = {$filters['company_id']} OR company_id = 0");
            }


            if (!empty($filters['withoutQouted'])) {
                $quoted = $this->getAdapter()->quote('QUOTED');
				if(CheckAuth::checkCredential(array('canSeeQuoted'))){
					$select->where("name NOT LIKE {$quoted}");
				}
            }

            if (!empty($filters['more_one_status_name'])) {
                if (is_array($filters['more_one_status_name'])) {
                    $moreOneStatus = $filters['more_one_status_name'];
                    foreach ($moreOneStatus as &$oneStatus) {
                        $oneStatus = $this->getAdapter()->quote(trim($oneStatus));
                    }
                    $select->where("name IN (" . implode(',', $moreOneStatus) . ")");
                }
            }

            if (!empty($filters['request_feedback'])) {
                $select->where("request_feedback =1");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }
		
		$sql = $select->__toString();
		echo $sql;
        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "booking_status_id = '{$id}'");
    }

    /**
     * delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("booking_status_id = '{$id}'");
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_status_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the assigned Status Name
     * 
     * @param string $name
     * @return array
     */
    public function getByStatusName($name) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("name = '{$name}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table rows as array
     * 
     * @return array
     */
    public function getAllStatusAsArray($filters = array()) {

        $all_status = $this->getAll($filters, 'name asc');

        $data = array();
        foreach ($all_status as $status) {
            $data[$status['booking_status_id']] = $status['name'];
        }
        return $data;
    }

    public function getAllStatusAsArrayIOS() {

        $all_status = $this->getAll(array(), 'name asc');

        $data = array();
        foreach ($all_status as $status) {
            $data[] = array(
                'booking_status_id' => $status['booking_status_id'],
                'booking_status' => $status['name']
            );
        }
        return $data;
    }

    public function checkConvertToInvoice($statusId) {

        $status = $this->getById($statusId);

        if ($status['name'] == 'COMPLETED' || $status['name'] == 'IN PROGRESS' || $status['name'] == 'FAILED') {
            return true;
        }
        return false;
    }

    public function getByStatusNameAndCompany($name, $companyId = 0) {

        if (!$companyId) {
            $companyId = CheckAuth::getCompanySession();
        }

        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("name = '{$name}'");
        $select->where("company_id = '{$companyId}'");



        return $this->getAdapter()->fetchRow($select);
    }

    public function getDeleteGoogleCalender($companyId = 0) {

        if (!$companyId) {
            $companyId = CheckAuth::getCompanySession();
        }

        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("delete_google_calender = 1 AND company_id = '{$companyId}'");
        $select->orWhere("delete_google_calender = 1 AND company_id = 0");

        $all_status = $this->getAdapter()->fetchAll($select);

        $data = array();
        foreach ($all_status as $status) {
            $data[$status['booking_status_id']] = $status['booking_status_id'];
        }
        return $data;
    }

    public function getPushGoogleCalender($companyId = 0) {

        if (!$companyId) {
            $companyId = CheckAuth::getCompanySession();
        }

        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("push_google_calender = 1 AND company_id = '{$companyId}'");
        $select->orWhere("push_google_calender = 1 AND company_id = 0");

        $all_status = $this->getAdapter()->fetchAll($select);
        $data = array();
        foreach ($all_status as $status) {
            $data[$status['booking_status_id']] = $status['booking_status_id'];
        }
        return $data;
    }

    public function checkBeforeDeleteBookingStatus($statusId, &$tables = array()) {

        $sucsess = true;

        $select_booking = $this->getAdapter()->select();
        $select_booking->from('booking', 'COUNT(*)');
        $select_booking->where("status_id = {$statusId}");
        $count_booking = $this->getAdapter()->fetchOne($select_booking);

        if ($count_booking) {
            $tables[] = 'booking';
            $sucsess = false;
        }

        $select_booking_status_discussion = $this->getAdapter()->select();
        $select_booking_status_discussion->from('booking_status_discussion', 'COUNT(*)');
        $select_booking_status_discussion->where("status_id = {$statusId}");
        $count_booking_status_discussion = $this->getAdapter()->fetchOne($select_booking_status_discussion);

        if ($count_booking_status_discussion) {
            $tables[] = 'booking_status_discussion';
            $sucsess = false;
        }

        $select_booking_status_history = $this->getAdapter()->select();
        $select_booking_status_history->from('booking_status_history', 'COUNT(*)');
        $select_booking_status_history->where("status_id = {$statusId}");
        $count_booking_status_history = $this->getAdapter()->fetchOne($select_booking_status_history);

        if ($count_booking_status_history) {
            $tables[] = 'booking_status_history';
            $sucsess = false;
        }

        return $sucsess;
    }

}