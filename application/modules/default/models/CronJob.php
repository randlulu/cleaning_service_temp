<?php

class Model_CronJob extends Zend_Db_Table_Abstract {

    protected $_name = 'cron_job';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from(array('cj' => $this->_name));
		$select->joinLeft(array('et' => 'email_template'), 'et.id = cj.email_template_id', array('et.name'));
        $select->joinLeft(array('smst' => 'sms_template'), 'smst.id = cj.sms_template_id', array('sms_name'=>'smst.name'));
        $select->order($order);

        if ($filters) {
            if (!empty($filters['keywords'])) {
                $keywords = $this->getAdapter()->quote("%" . $filters['keywords'] . "%");
                $select->where("cronjob_name LIKE {$keywords}");
            }

            if (!empty($filters['cronjob_name'])) {
                $cronJobName = $this->getAdapter()->quote($filters['cronjob_name']);
                $select->where("cronjob_name = {$cronJobName}");
            }

            if (!empty($filters['php_code'])) {
                $phpCode = $this->getAdapter()->quote($filters['php_code']);
                $select->where("php_code = {$phpCode}");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

    public function insert($data) {
        $id = parent::insert($data);

        if ($id) {
            $this->creatCronJobFile($id);
        }

        return $id;
    }

    /**
     * update table rows according to the assigned id and data
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data, $createFile = true) {
        $id = (int) $id;
        $success = parent::update($data, "id = '{$id}'");

        if ($success && $createFile) {
            $this->creatCronJobFile($id);
        }

        return $success;
    }

    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("id = '{$id}'");
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function deleteCronJob($id) {
        $cronjob = $this->getById($id);

        if ($cronjob) {
            $filename = CRON_PATH . '/' . strtolower($cronjob['cronjob_name']) . '.php';

            if (file_exists($filename)) {
                unlink($filename);
            }

            $this->deleteById($id);
        }

        $this->updateCrontab();
    }

    public function creatCronJobFile($id) {
        $cronjob = $this->getById($id);

        if ($cronjob) {
            $filename = CRON_PATH . '/' . strtolower($cronjob['cronjob_name']) . '.php';

            if (file_exists($filename)) {
                unlink($filename);
            }

            $data = '<?php' . "\n\n";
            $data .= 'define("DONT_RUN_APP", true);' . "\n";
            $data .= 'defined("PUBLIC_PATH") || define("PUBLIC_PATH", realpath(dirname(__FILE__) . "/../public"));' . "\n";
            $data .= 'require(realpath(PUBLIC_PATH . "/index.php"));' . "\n";
            $data .= '$application->bootstrap();' . "\n\n\n";
            $data .= $cronjob['php_code'];

            file_put_contents($filename, $data);
        }

      //  $this->updateCrontab();
    }

    public function updateCrontab() {

        $data = "# Edit this file to introduce tasks to be run by cron." . "\n";
        $data .= "#" . "\n";
        $data .= "# Each task to run has to be defined through a single line" . "\n";
        $data .= "# indicating with different fields when the task will be run" . "\n";
        $data .= "# and what command to run for the task" . "\n";
        $data .= "#" . "\n";
        $data .= "# To define the time you can provide concrete values for" . "\n";
        $data .= "# minute (m), hour (h), day of month (dom), month (mon)," . "\n";
        $data .= "# and day of week (dow) or use '*' in these fields (for 'any').#" . "\n";
        $data .= "# Notice that tasks will be started based on the cron's system" . "\n";
        $data .= "# daemon's notion of time and timezones." . "\n";
        $data .= "#" . "\n";
        $data .= "# Output of the crontab jobs (including errors) is sent through" . "\n";
        $data .= "# email to the user the crontab file belongs to (unless redirected)." . "\n";
        $data .= "#" . "\n";
        $data .= "# For example, you can run a backup of all your user accounts" . "\n";
        $data .= "# at 5 a.m every week with:" . "\n";
        $data .= "# 0 5 * * 1 tar -zcf /var/backups/home.tgz /home/" . "\n";
        $data .= "#" . "\n";
        $data .= "# For more information see the manual pages of crontab(5) and cron(8)" . "\n";
        $data .= "#" . "\n";
        $data .= "# m h  dom mon dow   command" . "\n";
        $data .= "\n";
        $data .= "\n";
        $data .= "# databases backup" . "\n";
        $data .= "0 " . gmdate("H", mktime(0)) . " * * * sh " . CRON_PATH . "/sh_files/backup.sh" . "\n";
        $data .= "0 " . gmdate("H", mktime(0)) . " 15 * * sh " . CRON_PATH . "/sh_files/remove_old_backups.sh" . "\n";
        $data .= "\n";
        $data .= "\n";
        $data .= "# update crontab" . "\n";
        $data .= "* * * * * sh " . CRON_PATH . "/sh_files/update_crontab.sh" . "\n";
        $data .= "\n";
        $data .= "\n";
        $data .= "# cronjobs" . "\n";
        $data .= "* * * * * php " . CRON_PATH . "/cronjob.php" . "\n";
        $data .= "0 " . gmdate("H", mktime(0)) . " * * * php " . CRON_PATH . "/cronjob_daily.php" . "\n";
        $data .= "0 " . gmdate("H", mktime(0)) . " * * 0 php " . CRON_PATH . "/cronjob_weekly.php" . "\n";

        $cronjobs = $this->getAll();
        foreach ($cronjobs as $cronjob) {
            if (!$cronjob['running']) {
                $data .= "# ";
            }

            $every = explode(" ", trim($cronjob['every']));

            $hours = (int) $every[1];
            $hours = gmdate("H", mktime($hours));

            $everyZone = "{$every[0]} {$hours} {$every[2]} {$every[3]} {$every[4]}";

            $data .= "{$everyZone} php " . CRON_PATH . '/' . strtolower($cronjob['cronjob_name']) . '.php' . "\n";
        }

        $filename = "/tmp/crontab.txt";

        if (file_exists($filename)) {
            unlink($filename);
        }

        file_put_contents($filename, $data);
    }

	public function getIdByName($cronjobName){
		
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("cronjob_name = '{$cronjobName}'");

        return $this->getAdapter()->fetchRow($select);
	
	}
	
}