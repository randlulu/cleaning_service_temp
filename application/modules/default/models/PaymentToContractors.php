<?php

class Model_PaymentToContractors extends Zend_Db_Table_Abstract {

    protected $_name = 'payment_to_contractor';
    private $modelCustomer;
    private $modelPaymentType;
    private $modelBookingInvoice;

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from(array('pay' => $this->_name), array('payment_to_contractor_id', 'date_of_payment', 'contractor_invoice_num', 'contractor_id', 'amount_calculated', 'amount_paid', 'cash_paid', 'invoice_total_amount', 'payment_reference', 'description', 'date_created', 'created_by'));

        $joinInner = array();
        //$select->joinLeft(array('payToContAttach' => 'payment_to_contractor_attachment'), 'pay.payment_to_contractor_id = payToContAttach.payment_to_contractor_id and payToContAttach.is_deleted != 1',array('attachment_id','path','file_name','size','description','is_deleted'));
        $select->joinInner(array('user' => 'user'), 'pay.contractor_id = user.user_id');
        $select->joinInner(array('userCom' => 'user_company'), 'userCom.user_id = user.user_id', array('company_id'));
        $select->joinInner(array('com' => 'company'), 'com.company_id = userCom.company_id', array('company_name'));
        $select->joinInner(array('creadedByUser' => 'user'), 'pay.created_by = creadedByUser.user_id', array('created_by_user' => 'username'));
        $select->joinLeft(array('bcp' => 'booking_contractor_payment'), 'pay.payment_to_contractor_id = bcp.payment_to_contractor_id', array('bookings' => 'COUNT(*)'));

        $loggedUser = CheckAuth::getLoggedUser();

        if ($filters) {
            if (!empty($filters['keywords'])) {
                $keywords = $this->getAdapter()->quote('%' . $filters['keywords'] . '%');
                $select->where("pay.reference LIKE {$keywords}");
            }

            if (!empty($filters['contractor_id'])) {
                //$keywords = $this->getAdapter()->quote('%' . $filters['keywords'] . '%');
                $contractor_id = (int) $filters['contractor_id'];
                $select->where("pay.contractor_id = {$contractor_id}");
            }

            if (!empty($filters['booking_id'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'bok.booking_id = pay.booking_id', 'cols' => '');

                $select->where("pay.booking_id = {$filters['booking_id']}");
                //$select->where('bok.is_deleted = 0');
            }

            if (!empty($filters['customer_id'])) {
                $joinInner['customer'] = array('name' => array('cus' => 'customer'), 'cond' => 'cus.customer_id = pay.customer_id', 'cols' => '');

                $select->where("pay.customer_id = {$filters['customer_id']}");
                $select->where('cus.is_deleted = 0');
            }

            if (!empty($filters['payment_type_id'])) {
                $select->where("pay.payment_type_id = {$filters['payment_type_id']}");
            }

            if (!empty($filters['is_approved'])) {
                if ('yes' == $filters['is_approved']) {
                    $select->where('pay.is_approved = 1');
                } elseif ('no' == $filters['is_approved']) {
                    $select->where('pay.is_approved = 0');
                }
            }
            ////// updated by islam 
            if (!empty($filters['payment_created_between'])) {
                $paymentCreatedBetween = $filters['payment_created_between'];
                $paymentEndBetween = !empty($filters['payment_end_between']) ? $filters['payment_end_between'] : date('Y-m-d H:i:s', time());
                $select->where("pay.date_of_payment between '" . $paymentCreatedBetween . "' and '" . $paymentEndBetween . "'");
            }
            //////
            if (!empty($filters['company_id'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'bok.booking_id = pay.booking_id', 'cols' => '');

                $company_id = (int) $filters['company_id'];
                $select->where("bok.company_id = {$company_id}");
            }
            if (!empty($filters['order_by']) && $filters['order_by'] == 'payment_type') {
                $joinInner['payment_type'] = array('name' => array('pyt' => 'payment_type'), 'cond' => 'pyt.id = pay.payment_type_id', 'cols' => '');
            }
            if (!empty($filters['is_mark'])) {
                $select->where('pay.is_mark = 1');
            }
        }

        if ($joinInner) {
            foreach ($joinInner as $inner) {
                $select->joinInner($inner['name'], $inner['cond'], $inner['cols']);
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }
        $select->group(array("bcp.payment_to_contractor_id"));
        $sql = $select->__toString();
        //echo $sql;
        //echo $select;die;
        return $this->getAdapter()->fetchAll($select);
    }

    public function insert(array $data) {

        $id = parent::insert($data);

        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'added');

        return $id;
    }

    public function updateById($id, $data) {
        $id = (int) $id;
        $updated_id = parent::update($data, "payment_to_contractor_id = '{$id}'");

        //D.A 31/09/2015 Remove Invoice Cache
        if ($updated_id) {
            $modelBookingContractorPayment = new Model_BookingContractorPayment();
            $contractorPayments = $modelBookingContractorPayment->getBookingIdsByPaymentToContractorId($id);
            foreach ($contractorPayments as $contractorPayment) {
                require_once 'Zend/Cache.php';
                $company_id = CheckAuth::getCompanySession();
                $modelBookingInvoice = new Model_BookingInvoice();
                $invoice = $modelBookingInvoice->getByBookingId($contractorPayment['booking_id']);
                $invoicePaymentToTechnicianCacheID = $invoice['id'] . '_invoicePaymentToTechnician';
                $invoiceViewDir = get_config('cache') . '/' . 'invoicesView' . '/' . $company_id;
                if (!is_dir($invoiceViewDir)) {
                    mkdir($invoiceViewDir, 0777, true);
                }
                $invoiceFrontEndOption = array('lifetime' => NULL,
                    'automatic_serialization' => true);
                $invoiceBackendOptions = array('cache_dir' => $invoiceViewDir);
                $invoiceCache = Zend_Cache::factory('Core', 'File', $invoiceFrontEndOption, $invoiceBackendOptions);
                $invoiceCache->remove($invoicePaymentToTechnicianCacheID);
            }
        }
        return $updated_id;
    }

    public function deleteById($id) {
        $id = (int) $id;

        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'deleted');

        return parent::delete("payment_to_contractor_id= '{$id}'");
    }

    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("payment_to_contractor_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function getTotalPaidAmountByInvoicesNumberArray($invoice_numbers, $contractor_id) {

        $select = $this->getAdapter()->select();
        $select->from($this->_name, array('total_amount_paid' => 'sum(amount_paid)', 'invoice_total_amount' => 'sum(invoice_total_amount)'));
        $select->where('contractor_invoice_num IN (?)', $invoice_numbers);
        if (isset($contractor_id) && $contractor_id) {
            $contractor_id = (int) $contractor_id;
            $select->where('contractor_id = ' . $contractor_id);
            $select->where('is_deleted = 0');
            $sql = $select->__toString();
            //echo $sql;
        }

        return $this->getAdapter()->fetchRow($select);
    }

    public function getIdByInvoiceNumAndContractorId($Invoice_num, $contractor_id) {

        $select = $this->getAdapter()->select();
        $select->from($this->_name, array('payment_to_contractor_id'));
        $select->where("contractor_invoice_num = '{$Invoice_num}' and contractor_id = {$contractor_id}");
        $select->limit(1);

        return $this->getAdapter()->fetchRow($select);
    }

    public function getInvoiceNumByContractorId($contractor_id) {

        $select = $this->getAdapter()->select();
        $select->from($this->_name, array('payment_to_contractor_id', 'contractor_invoice_num'));
        $select->where("contractor_id = {$contractor_id} and contractor_invoice_num is NOT Null ");

        return $this->getAdapter()->fetchAll($select);
    }

    public function getLastInvoiceNum($contractor_id) {

        $select = $this->getAdapter()->select();
        $select->from(array('pay' => $this->_name), array('payment_to_contractor_id', 'contractor_invoice_num'));
        $select->where("contractor_id = '{$contractor_id}'");
      //  $select->joinInner(array('user' => 'user'), 'pay.contractor_id = user.user_id');
        $select->order('payment_to_contractor_id DESC');
        
        $select->limit('1');
       
        return $this->getAdapter()->fetchRow($select);
    }
    

}

?>