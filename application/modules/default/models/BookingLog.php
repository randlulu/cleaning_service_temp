<?php

class Model_BookingLog extends Zend_Db_Table_Abstract {

    protected $_name = 'booking_log';
    //
    //model for fill function
    //
    private $loggedUser = array();
    private $modelContractorInfo;
    private $modelContractorServiceBooking;
    private $modelCustomer;
    private $modelCities;
    private $modelCountries;
    private $modelBookingStatus;
    private $modelServices;
    private $modelUser;
    private $modelPropertyType;
    private $modelCustomerCommercialInfo;
    private $modelCustomerContact;
    private $modelCustomerContactLabel;
    private $modelBookingContactHistory;
    private $modelBookingInvoiceLog;
    private $modelBookingEstimateLog;
    private $modelBookingAddressLog;
    private $modelBookingMultipleDaysLog;
    private $modelBookingProductLog;
    private $modelContractorServiceBookingLog;

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function updateGST() {
        $select = $this->getAdapter()->select();
        $select->from(array('bok' => $this->_name));
        $select->where("bok.total_discount != 0");
        $select->order('bok.booking_id ASC');
        $bookings = $this->getAdapter()->fetchAll($select);
        if ($bookings) {
            foreach ($bookings as $booking) {
                $new_gst = $booking['gst'] + ($booking['total_discount'] * .10);
                $new_qoute = $booking['qoute'] + ($booking['total_discount'] * 1.1);
                $data = array(
                    'gst_new' => $new_gst,
                    'qoute_new' => $new_qoute
                );
                $this->updateById($booking['booking_id'], $data);
            }
        }
    }

    public function getAll($filters = array(), $order = null, &$pager = null, $limit = 0, $perPage = 0, $currentPage = 0) {

        $joinInner = array();
        $select = $this->getAdapter()->select();
        $select->from(array('bok_log' => $this->_name));
        $select->order($order);

        if ($filters) {
            if (!empty($filters['log_user_id'])) {
                $logUserId = $this->getAdapter()->quote(trim($filters['log_user_id']));
                $select->where("log_user_id = {$logUserId}");
            }
        }
        if ($joinInner) {
            foreach ($joinInner as $inner) {
                $select->joinInner($inner['name'], $inner['cond'], $inner['cols']);
            }
        }


        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        } elseif ($limit) {
            $select->limit($limit);
        } elseif ($perPage && $currentPage) {
            $select->limit($perPage, ($currentPage - 1) * $perPage);
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table rows according to the assigned id and data
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "log_id= '{$id}'");
    }

    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("log_id= '{$id}'");
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("log_id= '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the assigned BookingId
     * 
     * @param int $id
     * @return array
     */
    public function getUsersByBookingId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from(array($this->_name), array('log_user_id'));
        $select->where("booking_id = '{$id}'");
        $select->distinct();

        $logUserIds = $this->getAdapter()->fetchAll($select);

        $userIds = array();
        if ($logUserIds) {
            foreach ($logUserIds as $logUserId) {
                if ($logUserId['log_user_id']) {
                    $userIds[] = array('user_id' => $logUserId['log_user_id']);
                }
            }
        }

        return $userIds;
    }

    public function getByBookingId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_id = '{$id}'");
        return $this->getAdapter()->fetchAll($select);
    }

    public function getLastBookingLogByBookingId($bookingId) {
        $selectB = $this->getAdapter()->select();
        $selectB->from($this->_name, array('MAX(log_id)'));
        $selectB->where("booking_id = {$bookingId}");

        $selectA = $this->getAdapter()->select();
        $selectA->from($this->_name);
        $selectA->where("log_id = ({$selectB})");
        $selectA->where("booking_id = {$bookingId}");

        return $this->getAdapter()->fetchAll($selectA);
    }

    public function getLastLogByBookingId($bookingId) {

        /* $selectB = $this->getAdapter()->select();
          $selectB->from($this->_name, array('MAX(log_id)'));
          $selectB->where("booking_id = {$bookingId}");

          $selectA = $this->getAdapter()->select();
          $selectA->from($this->_name);
          $selectA->where("log_id = ({$selectB})");
          $selectA->where("booking_id = {$bookingId}"); */

        $selectC = $this->getAdapter()->select();
        $selectC->from($this->_name);
        $selectC->where("booking_id = {$bookingId}");
        $selectC->order("log_id DESC");
        $selectC->limit(1);

        return $this->getAdapter()->fetchRow($selectC);
    }

    public function addBookingLog($id, $log_user_id = 0,$os='') {

        $modelBooking = new Model_Booking();
        $data = $modelBooking->getById($id);

        $modelVisitedExtraInfo = new Model_VisitedExtraInfo();
        $extra_info = $modelVisitedExtraInfo->getById($data['visited_extra_info_id']);
        $logedUser = CheckAuth::getLoggedUser();


        if (!$log_user_id) {
            //$logedUser = CheckAuth::getLoggedUser();
            //$log_user_id = !empty($logedUser) ? $logedUser['user_id'] : 0;
            if (isset($logedUser['user_id']) && !empty($logedUser['user_id'])) {
                $log_user_id = $logedUser['user_id'];
            } else if (isset($logedUser['customer_id']) && !empty($logedUser['customer_id'])) {
                $log_user_id = $logedUser['customer_id'];
            } else if (isset($data['created_by']) && !empty($data['created_by'])) {
                $log_user_id = $data['created_by'];
            } else {
                $log_user_id = 0;
            }
        }

        $dbParams = array();
        $dbParams['log_created'] = time();
        $dbParams['log_user_id'] = $log_user_id;
        $dbParams['booking_id'] = $data['booking_id'];
        $dbParams['status_id'] = $data['status_id'];
        $dbParams['city_id'] = $data['city_id'];
        $dbParams['customer_id'] = $data['customer_id'];
        $dbParams['created_by'] = $data['created_by'];
        $dbParams['is_deleted'] = $data['is_deleted'];
        $dbParams['created'] = $data['created'];
        $dbParams['booking_num'] = $data['booking_num'];
        $dbParams['title'] = $data['title'];
        $dbParams['booking_start'] = $data['booking_start'];
        $dbParams['booking_end'] = $data['booking_end'];
        $dbParams['is_all_day_event'] = $data['is_all_day_event'];
        $dbParams['recurring_rule'] = $data['recurring_rule'];
        $dbParams['color'] = $data['color'];
        $dbParams['description'] = $data['description'];
        $dbParams['why'] = $data['why'];
        $dbParams['full_text_search'] = $data['full_text_search'];
        $dbParams['qoute'] = $data['qoute'];
        $dbParams['sub_total'] = $data['sub_total'];
        $dbParams['total_discount'] = $data['total_discount'];
        $dbParams['total_discount_temp'] = $data['total_discount_temp'];
        $dbParams['gst'] = $data['gst'];
        $dbParams['paid_amount'] = $data['paid_amount'];
        $dbParams['onsite_client_name'] = $extra_info['onsite_client_name'];
        $dbParams['job_start_time'] = strtotime($extra_info['job_start']);
        $dbParams['job_finish_time'] = strtotime($extra_info['job_end']);
        //$dbParams['onsite_client_name'] = $data['onsite_client_name'];
        //$dbParams['job_start_time'] = $data['job_start_time'];
        //$dbParams['job_finish_time'] = $data['job_finish_time'];
        $dbParams['satisfaction'] = $data['satisfaction'];
        $dbParams['convert_status'] = $data['convert_status'];
        $dbParams['is_change'] = $data['is_change'];
        $dbParams['company_id'] = $data['company_id'];
        $dbParams['count'] = $data['count'];
        $dbParams['property_type_id'] = $data['property_type_id'];
        $dbParams['original_inquiry_id'] = $data['original_inquiry_id'];
        $dbParams['call_out_fee'] = $data['call_out_fee'];
        $dbParams['call_out_fee_temp'] = $data['call_out_fee_temp'];
        $dbParams['to_follow'] = $data['to_follow'];
        $dbParams['is_to_follow'] = $data['is_to_follow'];
        $dbParams['is_multiple_days'] = $data['is_multiple_days'];
        $dbParams['refund'] = $data['refund'];
        $dbParams['cancel_hashcode'] = $data['cancel_hashcode'];
        $dbParams['on_hold_reminded'] = $data['on_hold_reminded'];
        $dbParams['tentative_reminded'] = $data['tentative_reminded'];
        $dbParams['os'] = $os;
		
		
        return parent::insert($dbParams);
    }

    /**
     * get extra info for all the rows
     * @param array $row
     * @param array $types
     * @return array
     */
    public function fills(&$rows, $types = array()) {
        foreach ($rows as &$row) {
            $this->fill($row, $types);
        }
    }

    /**
     * get extra info for each row (Ex: getting the category row by c_id, author row by author id ... etc)
     * 
     * @param array $row
     * @param array $types
     * @return array
     */
    public function fill(&$row, $types = array()) {

        if (in_array('booking', $types)) {
            $row['booking'] = $this->getById($row['booking_id']);
        }
        if (in_array('address_log', $types)) {

            /**
             * load model
             */
            if (!$this->modelBookingAddressLog) {
                $this->modelBookingAddressLog = new Model_BookingAddressLog();
            }
            $address = $this->modelBookingAddressLog->getLastLogbyBookingIdAndLogBookingId($row['booking_id'], $row['log_id']);
            $row['address_log'] = get_line_address($address);
        }
        if (in_array('invoice_log', $types)) {
            /**
             * load model
             */
            if (!$this->modelBookingInvoiceLog) {
                $this->modelBookingInvoiceLog = new Model_BookingInvoiceLog();
            }
            $estimateLog = '';
            if ($row['convert_status'] == 'invoice') {
                $estimateLog = $this->modelBookingInvoiceLog->getLastLogbyBookingIdAndLogBookingId($row['booking_id'], $row['log_id']);
            }
            $row['invoice_log'] = $estimateLog;
        }
        if (in_array('estimate_log', $types)) {
            /**
             * load model
             */
            if (!$this->modelBookingEstimateLog) {
                $this->modelBookingEstimateLog = new Model_BookingEstimateLog();
            }
            $invoiceLog = '';
            if ($row['convert_status'] == 'estimate') {
                $invoiceLog = $this->modelBookingEstimateLog->getLastLogbyBookingIdAndLogBookingId($row['booking_id'], $row['log_id']);
            }
            $row['estimate_log'] = $invoiceLog;
        }
        if (in_array('multiple_days_log', $types)) {
            /**
             * load model
             */
            if (!empty($row['is_multiple_days'])) {

                if (!$this->modelBookingMultipleDaysLog) {
                    $this->modelBookingMultipleDaysLog = new Model_BookingMultipleDaysLog();
                }
                $row['multiple_days_log'] = $this->modelBookingMultipleDaysLog->getLastLogbyBookingIdAndLogBookingId($row['booking_id'], $row['log_id']);
            }
        }
        if (in_array('booking_product_log', $types)) {
            /**
             * load model
             */
            if (!$this->modelBookingProductLog) {
                $this->modelBookingProductLog = new Model_BookingProductLog();
            }

            $row['booking_product_log'] = $this->modelBookingProductLog->getLastLogbyBookingIdAndLogBookingId($row['booking_id'], $row['log_id']);
        }
        if (in_array('booking_service_log', $types)) {
            /**
             * load model
             */
            if (!$this->modelContractorServiceBookingLog) {
                $this->modelContractorServiceBookingLog = new Model_ContractorServiceBookingLog();
            }
            if (!$this->modelServices) {
                $this->modelServices = new Model_Services();
            }
            if (!$this->modelUser) {
                $this->modelUser = new Model_User();
            }
            if (!$this->modelContractorInfo) {
                $this->modelContractorInfo = new Model_ContractorInfo();
            }

            $bookingServices = $this->modelContractorServiceBookingLog->getByBookingLogId($row['log_id']);




            foreach ($bookingServices as &$bookingService) {

                // booking Service Details
                $bookingService['service_details'] = $this->modelServices->getById($bookingService['service_id']);

                // Price 
                $bookingService['priceArray'] = $this->modelContractorServiceBookingLog->getTotalServiceBookingQoute($bookingService['log_id']);

                //contractor
                $bookingService['contractor'] = $this->modelUser->getById($bookingService['contractor_id']);
            }

            $row['booking_service_log'] = $bookingServices;
        }

        if (in_array('contractors', $types)) {
            /**
             * load model
             */
            if (!$this->modelContractorInfo) {
                $this->modelContractorInfo = new Model_ContractorInfo();
            }
            if (!$this->modelUser) {
                $this->modelUser = new Model_User();
            }
            if (!$this->modelContractorServiceBooking) {
                $this->modelContractorServiceBooking = new Model_ContractorServiceBooking();
            }

            $allContractorServiceBooking = $this->modelContractorServiceBooking->getByBookingId($row['booking_id']);

            $allContractors = array();
            foreach ($allContractorServiceBooking as $contractorServiceBooking) {
                $contractorInfo = $this->modelContractorInfo->getByContractorId($contractorServiceBooking['contractor_id']);
                $userInfo = $this->modelUser->getById($contractorServiceBooking['contractor_id']);
                if ('contractor' == CheckAuth::getRoleName()) {
                    if ($contractorServiceBooking['contractor_id'] == $this->loggedUser['user_id']) {
                        $allContractors[$contractorServiceBooking['contractor_id']] = ucwords($userInfo['username']) . " - " . ucwords($contractorInfo['business_name'] ? $contractorInfo['business_name'] : '');
                    }
                } else {
                    $allContractors[$contractorServiceBooking['contractor_id']] = ucwords($userInfo['username']) . " - " . ucwords($contractorInfo['business_name']);
                }
            }
            $row['contractors'] = $allContractors;
        }

        if (in_array('name_contractors', $types)) {
            /**
             * load model
             */
            if (!$this->modelUser) {
                $this->modelUser = new Model_User();
            }
            if (!$this->modelContractorServiceBooking) {
                $this->modelContractorServiceBooking = new Model_ContractorServiceBooking();
            }

            $allContractorServiceBooking = $this->modelContractorServiceBooking->getByBookingId($row['booking_id']);

            $contractors = array();
            if ($allContractorServiceBooking) {
                foreach ($allContractorServiceBooking as $bookingService) {
                    $user = $this->modelUser->getById($bookingService['contractor_id']);

                    $contractorName = isset($user['display_name']) ? $user['display_name'] : '';
                    $contractors[$bookingService['contractor_id']] = ucwords($contractorName);
                }
            }
            $row['name_contractors'] = $contractors;
        }

        if (in_array('email_contractors', $types)) {
            /**
             * load model
             */
            if (!$this->modelUser) {
                $this->modelUser = new Model_User();
            }
            if (!$this->modelContractorServiceBooking) {
                $this->modelContractorServiceBooking = new Model_ContractorServiceBooking();
            }

            $allContractorServiceBooking = $this->modelContractorServiceBooking->getByBookingId($row['booking_id']);

            $contractors = array();
            if ($allContractorServiceBooking) {
                foreach ($allContractorServiceBooking as $bookingService) {
                    $user = $this->modelUser->getById($bookingService['contractor_id']);
                    $contractors[$bookingService['contractor_id']] = $user['email1'];
                }
            }
            $row['email_contractors'] = $contractors;
        }

        if (in_array('contact_history', $types)) {
            /**
             * load model
             */
            if (!$this->modelBookingContactHistory) {
                $this->modelBookingContactHistory = new Model_BookingContactHistory();
            }

            $row['contact_history'] = $this->modelBookingContactHistory->getContactHistory($row['booking_id']);
        }

        if (in_array('customer', $types)) {
            /**
             * load model
             */
            if (!$this->modelCustomer) {
                $this->modelCustomer = new Model_Customer();
            }

            $row['customer'] = $this->modelCustomer->getById($row['customer_id']);
            $row['customer_name'] = get_customer_name($row['customer']);
        }
        if (in_array('full_customer_info', $types)) {
            /**
             * load model
             */
            if (!$this->modelCustomer) {
                $this->modelCustomer = new Model_Customer();
            }

            $customer = $this->modelCustomer->getById($row['customer_id']);
            $this->modelCustomer->fill($customer, array('customer_commercial_info', 'customer_contacts'));

            $row['customer'] = $customer;
            $row['customer_name'] = get_customer_name($customer);
        }
        if (in_array('customer_contacts', $types)) {
            /**
             * load model
             */
            if (!$this->modelCustomer) {
                $this->modelCustomer = new Model_Customer();
            }
            if (!$this->modelCustomerContact) {
                $this->modelCustomerContact = new Model_CustomerContact();
            }
            if (!$this->modelCustomerContactLabel) {
                $this->modelCustomerContactLabel = new Model_CustomerContactLabel();
            }

            $row['customer'] = $this->modelCustomer->getById($row['customer_id']);
            $customerContacts = '';
            if ($row['customer']) {
                $customerContacts = $this->modelCustomerContact->getByCustomerId($row['customer']['customer_id']);
                foreach ($customerContacts as &$customerContact) {
                    $customerContactLabel = $this->modelCustomerContactLabel->getById($customerContact['customer_contact_label_id']);
                    $customerContact['contact_label'] = $customerContactLabel['contact_label'];
                }
            }

            $row['customer_contacts'] = $customerContacts;
        }
        if (in_array('customer_commercial_info', $types)) {
            /**
             * load model
             */
            if (!$this->modelCustomerCommercialInfo) {
                $this->modelCustomerCommercialInfo = new Model_CustomerCommercialInfo();
            }
            $row['customer_commercial_info'] = $this->modelCustomerCommercialInfo->getByCustomerId($row['customer_id']);
        }

        if (in_array('city', $types)) {
            /**
             * load model
             */
            if (!$this->modelCities) {
                $this->modelCities = new Model_Cities();
            }
            if (!$this->modelCountries) {
                $this->modelCountries = new Model_Countries();
            }

            $row['city'] = $this->modelCities->getById($row['city_id']);
            $row['city_name'] = $row['city']['city_name'];

            $countryName = $this->modelCountries->getById($row['city']['country_id']);
            $row['country_name'] = $countryName['country_name'];
        }

        if (in_array('status', $types)) {
            /**
             * load model
             */
            if (!$this->modelBookingStatus) {
                $this->modelBookingStatus = new Model_BookingStatus();
            }

            $row['status'] = $this->modelBookingStatus->getById($row['status_id']);
        }

        if (in_array('created_by', $types)) {
            /**
             * load model
             */
            if (!$this->modelUser) {
                $this->modelUser = new Model_User();
            }

            $row['created_by'] = $this->modelUser->getById($row['created_by']);
        }
        if (in_array('log_user', $types)) {
            /**
             * load model
             */
            if (!$this->modelUser) {
                $this->modelUser = new Model_User();
            }

            $row['log_user'] = $this->modelUser->getById($row['log_user_id']);
        }
        if (in_array('services', $types)) {
            /**
             * load model
             */
            if (!$this->modelContractorServiceBooking) {
                $this->modelContractorServiceBooking = new Model_ContractorServiceBooking();
            }
            if (!$this->modelServices) {
                $this->modelServices = new Model_Services();
            }
            $filters = array();
            $filters['booking_id'] = $row['booking_id'];
            $services = $this->modelContractorServiceBooking->getAll($filters);

            foreach ($services as &$service) {
                $service['info'] = $this->modelServices->getById($service['service_id']);
            }

            $row['services'] = $services;
        }

        if (in_array('property_type', $types)) {
            /**
             * load model
             */
            if (!$this->modelPropertyType) {
                $this->modelPropertyType = new Model_PropertyType();
            }

            $propertyType = $this->modelPropertyType->getById($row['property_type_id']);

            $row['property_type'] = $propertyType['property_type'];
        }

        if (in_array('history_details', $types)) {

            $row['history_details'] = $this->getHistoryDetails($row['log_id']);
        }

        return $row;
    }

    public function getHistoryDetails($logBookingId) {

        $history = array();

        // booking Log
        $bookingHistory = $this->getBookingLogDetails($logBookingId);


        // Invoice Estimate  Log

        $bookingLog = $this->getById($logBookingId);

        $this->fill($bookingLog, array('invoice_log', 'estimate_log'));

        $previousLog = $this->getPreviousLog($bookingLog['booking_id'], $bookingLog['log_id']);
        if (!empty($previousLog)) {
            $this->fill($previousLog, array('invoice_log', 'estimate_log'));
        }

        // Invoice Log
        if (!empty($bookingLog['invoice_log'])) {
            if (isset($previousLog['invoice_log']) && !empty($previousLog['invoice_log'])) {
                if ($bookingLog['invoice_log']['invoice_num'] != $previousLog['invoice_log']['invoice_num']) {
                    $history['invoice_num'] = 'Changed Invoice Number from ' . '<b>' . ucwords($previousLog['invoice_log']['invoice_num']) . '</b>' . ' To ' . '<b>' . $bookingLog['invoice_log']['invoice_num'] . '</b>';
                }
                if ($bookingLog['invoice_log']['invoice_type'] != $previousLog['invoice_log']['invoice_type']) {
                    $history['invoice_type'] = 'Changed Invoice Type from ' . '<b>' . ucwords($previousLog['invoice_log']['invoice_type']) . '</b>' . ' To ' . '<b>' . $bookingLog['invoice_log']['invoice_type'] . '</b>';
                }
            } else {
                $history['invoice_num'] = 'Added Invoice Number ' . '<b>' . ucwords($bookingLog['invoice_log']['invoice_num']) . '</b>';
                $history['invoice_type'] = 'Added Invoice Type ' . '<b>' . ucwords($bookingLog['invoice_log']['invoice_type']) . '</b>';
            }
        }

        // Estimate Log
        if (!empty($bookingLog['estimate_log'])) {
            if (isset($previousLog['estimate_log']) && !empty($previousLog['estimate_log'])) {
                if ($bookingLog['estimate_log']['estimate_num'] != $previousLog['estimate_log']['estimate_num']) {
                    $history['estimate_num'] = 'Changed Estimate Number from ' . '<b>' . ucwords($previousLog['estimate_log']['estimate_num']) . '</b>' . ' To ' . '<b>' . $bookingLog['estimate_log']['estimate_num'] . '</b>';
                }
                if ($bookingLog['estimate_log']['estimate_type'] != $previousLog['estimate_log']['estimate_type']) {
                    $history['estimate_type'] = 'Changed Estimate Type from ' . '<b>' . ucwords($previousLog['estimate_log']['estimate_type']) . '</b>' . ' To ' . '<b>' . $bookingLog['estimate_log']['estimate_type'] . '</b>';
                }
            } else {
                $history['estimate_num'] = 'Added Estimate Number ' . '<b>' . ucwords($bookingLog['estimate_log']['estimate_num']) . '</b>';
                $history['estimate_type'] = 'Added Estimate Type ' . '<b>' . ucwords($bookingLog['estimate_log']['estimate_type']) . '</b>';
            }
        }

        // Service Log

        $serviceHistory = $this->getServiceLogDetails($logBookingId);

        $fullHistory = array_merge($bookingHistory, $history, $serviceHistory);

        return $fullHistory;
    }

    public function getBookingLogDetails($logBookingId) {

        /**
         * load model
         */
        if (!$this->modelBookingStatus) {
            $this->modelBookingStatus = new Model_BookingStatus();
        }
        if (!$this->modelCities) {
            $this->modelCities = new Model_Cities();
        }
        if (!$this->modelCustomer) {
            $this->modelCustomer = new Model_Customer();
        }
        if (!$this->modelPropertyType) {
            $this->modelPropertyType = new Model_PropertyType();
        }

        $bookingLog = $this->getById($logBookingId);

        $this->fill($bookingLog, array('address_log'));

        $previousLog = $this->getPreviousLog($bookingLog['booking_id'], $bookingLog['log_id']);
        if (!empty($previousLog)) {
            $this->fill($previousLog, array('address_log'));
        }

        $history = array();

        foreach ($bookingLog as $key => $newValue) {
            switch ($key) {

                case 'status_id':
                    if (!empty($newValue)) {
                        $newStatus = $this->modelBookingStatus->getById($newValue);
                        if (!empty($previousLog[$key])) {
                            if ($newValue != $previousLog[$key]) {
                                $oldStatus = $this->modelBookingStatus->getById($previousLog[$key]);
                                $history['status_id'] = 'Changed Status from ' . '<b>' . $oldStatus['name'] . '</b>' . ' To ' . '<b>' . $newStatus['name'] . '</b>';
                            }
                        } else {
                            $history['status_id'] = 'Added Status ' . '<b>' . $newStatus['name'] . '</b>';
                        }
                    }
                    break;

                case 'city_id':
                    if (!empty($newValue)) {
                        $newCity = $this->modelCities->getById($newValue);
                        if (!empty($previousLog[$key])) {
                            if ($newValue != $previousLog[$key]) {
                                $oldCity = $this->modelCities->getById($previousLog[$key]);
                                $history['city_id'] = 'Changed City from ' . '<b>' . $oldCity['city_name'] . '</b>' . ' To ' . '<b>' . $newCity['city_name'] . '</b>';
                            }
                        } else {
                            $history['city_id'] = 'Added City ' . '<b>' . $newCity['city_name'] . '</b>';
                        }
                    }
                    break;

                case 'customer_id':
                    if (!empty($newValue)) {
                        $newCustomer = $this->modelCustomer->getById($newValue);
                        if (!empty($previousLog[$key])) {
                            if ($newValue != $previousLog[$key]) {
                                $oldCustomer = $this->modelCustomer->getById($previousLog[$key]);
                                $history['customer_id'] = 'Changed Customer from ' . '<b>' . ucwords($oldCustomer['first_name']) . ' ' . ucwords($oldCustomer['last_name']) . '</b>' . ' To ' . '<b>' . ucwords($newCustomer['first_name']) . ' ' . ucwords($newCustomer['last_name']) . '</b>';
                            }
                        } else {
                            $history['customer_id'] = 'Added Customer ' . '<b>' . ucwords($newCustomer['first_name']) . ' ' . ucwords($newCustomer['last_name']) . '</b>';
                        }
                    }
                    break;


                case 'booking_num':
                    if (!empty($newValue)) {
                        if (!empty($previousLog[$key])) {
                            if ($newValue != $previousLog[$key]) {
                                $history['booking_num'] = 'Changed Booking Number from ' . '<b>' . $previousLog[$key] . '</b>' . ' To ' . '<b>' . $newValue . '</b>';
                            }
                        } else {
                            $history['booking_num'] = 'Added Booking Number ' . '<b>' . $newValue . '</b>';
                        }
                    }
                    break;

                case 'title':
                    if (!empty($newValue)) {
                        if (!empty($previousLog[$key])) {
                            if ($newValue != $previousLog[$key]) {
                                $history['title'] = 'Changed Title from ' . '<b>' . $previousLog[$key] . '</b>' . ' To ' . '<b>' . $newValue . '</b>';
                            }
                        } else {
                            $history['title'] = 'Added Title ' . '<b>' . $newValue . '</b>';
                        }
                    }
                    break;

                case 'booking_start':
                    if (!empty($newValue)) {
                        if (!empty($previousLog[$key])) {
                            if ($newValue != $previousLog[$key]) {
                                $history['booking_start'] = 'Changed Start Time from ' . '<b>' . getDateFormating($previousLog[$key]) . '</b>' . ' To ' . '<b>' . getDateFormating($newValue) . '</b>';
                            }
                        } else {
                            $history['booking_start'] = 'Added Start Time ' . '<b>' . getDateFormating($newValue) . '</b>';
                        }
                    }
                    break;

                case 'booking_end':
                    if (!empty($newValue)) {
                        if (!empty($previousLog[$key])) {
                            if ($newValue != $previousLog[$key]) {
                                $history['booking_end'] = 'Changed End Time from ' . '<b>' . getDateFormating($previousLog[$key]) . '</b>' . ' To ' . '<b>' . getDateFormating($newValue) . '</b>';
                            }
                        } else {
                            $history['booking_end'] = 'Added End Time ' . '<b>' . getDateFormating($newValue) . '</b>';
                        }
                    }
                    break;

                case 'description':
                    if (!empty($newValue)) {
                        if (!empty($previousLog[$key])) {
                            if ($newValue != $previousLog[$key]) {
                                $history['description'] = 'Changed Description';
                            }
                        } else {
                            $history['description'] = 'Added Description';
                        }
                    } elseif (!empty($previousLog[$key])) {
                        $history['description'] = 'Delete Description';
                    }
                    break;

                case 'why':
                    if (!empty($newValue)) {
                        if (!empty($previousLog[$key])) {
                            if ($newValue != $previousLog[$key]) {
                                $history['why'] = 'Changed Comment Status';
                            }
                        } else {
                            $history['why'] = 'Added Comment Status';
                        }
                    } elseif (!empty($previousLog[$key])) {
                        $history['why'] = 'Delete Comment Status';
                    }
                    break;

                case 'sub_total':
                    if (!empty($newValue)) {
                        if (!empty($previousLog[$key])) {
                            if ($newValue != $previousLog[$key]) {
                                $history['sub_total'] = 'Changed Sub Total from ' . '<b>' . $previousLog[$key] . '</b>' . ' To ' . '<b>' . $newValue . '</b>';
                            }
                        } else {
                            $history['sub_total'] = 'Added Sub Total ' . '<b>' . $newValue . '</b>';
                        }
                    }
                    break;

                case 'gst':
                    if (!empty($newValue)) {
                        if (!empty($previousLog[$key])) {
                            if ($newValue != $previousLog[$key]) {
                                $history['gst'] = 'Changed Gst from ' . '<b>' . $previousLog[$key] . '</b>' . ' To ' . '<b>' . $newValue . '</b>';
                            }
                        } else {
                            $history['gst'] = 'Added Gst ' . '<b>' . $newValue . '</b>';
                        }
                    }
                    break;

                case 'total_discount':
                    if (!empty($newValue)) {
                        if (!empty($previousLog[$key])) {
                            if ($newValue != $previousLog[$key]) {
                                $history['total_discount'] = 'Changed Total Discount from ' . '<b>' . $previousLog[$key] . '</b>' . ' To ' . '<b>' . $newValue . '</b>';
                            }
                        } else {
                            $history['total_discount'] = 'Added Total Discount ' . '<b>' . $newValue . '</b>';
                        }
                    }
                    break;

                case 'qoute':
                    if (!empty($newValue)) {
                        if (!empty($previousLog[$key])) {
                            if ($newValue != $previousLog[$key]) {
                                $history['qoute'] = 'Changed Qoute from ' . '<b>' . $previousLog[$key] . '</b>' . ' To ' . '<b>' . $newValue . '</b>';
                            }
                        } else {
                            $history['qoute'] = 'Added Qoute ' . '<b>' . $newValue . '</b>';
                        }
                    }
                    break;


                case 'onsite_client_name':
                    if (!empty($newValue) && $bookingLog['convert_status'] == 'invoice') {
                        if (!empty($previousLog[$key])) {
                            if ($newValue != $previousLog[$key]) {
                                $history['onsite_client_name'] = 'Changed On-Site Client Name from ' . '<b>' . ucwords($previousLog[$key]) . '</b>' . ' To ' . '<b>' . ucwords($newValue) . '</b>';
                            }
                        } else {
                            $history['onsite_client_name'] = 'Added On-Site Client Name ' . '<b>' . ucwords($newValue) . '</b>';
                        }
                    }
                    break;

                case 'job_start_time':
                    if (!empty($newValue) && $bookingLog['convert_status'] == 'invoice') {
                        if (!empty($previousLog[$key])) {
                            if ($newValue != $previousLog[$key]) {
                                $history['job_start_time'] = 'Changed Start Time On-Site from ' . '<b>' . getDateFormating($previousLog[$key], false) . '</b>' . ' To ' . '<b>' . getDateFormating($newValue, false) . '</b>';
                            }
                        } else {
                            $history['job_start_time'] = 'Added Start Time On-Site ' . '<b>' . getDateFormating($newValue, false) . '</b>';
                        }
                    }
                    break;

                case 'job_finish_time':
                    if (!empty($newValue) && $bookingLog['convert_status'] == 'invoice') {
                        if (!empty($previousLog[$key])) {
                            if ($newValue != $previousLog[$key]) {
                                $history['job_finish_time'] = 'Changed End Time On-Site from ' . '<b>' . getDateFormating($previousLog[$key], false) . '</b>' . ' To ' . '<b>' . getDateFormating($newValue, false) . '</b>';
                            }
                        } else {
                            $history['job_finish_time'] = 'Added End Time On-Site ' . '<b>' . getDateFormating($newValue, false) . '</b>';
                        }
                    }
                    break;

                case 'satisfaction':
                    if (!empty($newValue) && $bookingLog['convert_status'] == 'invoice') {
                        if (!empty($previousLog[$key])) {
                            if ($newValue != $previousLog[$key]) {
                                $history['satisfaction'] = 'Changed Client Satisfaction';
                            }
                        } else {
                            $history['satisfaction'] = 'Added Client Satisfaction';
                        }
                    }
                    break;

                case 'convert_status':
                    if (!empty($newValue)) {
                        if (!empty($previousLog[$key])) {
                            if ($newValue != $previousLog[$key]) {
                                $history['convert_status'] = 'Changed from ' . '<b>' . ucwords($previousLog[$key]) . '</b>' . ' To ' . '<b>' . ucwords($newValue) . '</b>';
                            }
                        } else {
                            $history['convert_status'] = 'Added ' . '<b>' . ucwords($newValue) . '</b>';
                        }
                    }
                    break;

                case 'property_type_id':
                    if (!empty($newValue)) {
                        $newType = $this->modelPropertyType->getById($newValue);
                        if (!empty($previousLog[$key])) {
                            $oldType = $this->modelPropertyType->getById($previousLog[$key]);
                            if ($newValue != $previousLog[$key]) {
                                $history['property_type_id'] = 'Changed Property from ' . '<b>' . $oldType['property_type'] . '</b>' . ' To ' . '<b>' . $newType['property_type'] . '</b>';
                            }
                        } else {
                            $history['property_type_id'] = 'Added Property ' . '<b>' . $newType['property_type'] . '</b>';
                        }
                    }
                    break;

                case 'call_out_fee':
                    if (!empty($newValue)) {
                        if (!empty($previousLog[$key])) {
                            if ($newValue != $previousLog[$key]) {
                                $history['call_out_fee'] = 'Changed Call Out Fee from ' . '<b>' . $previousLog[$key] . '</b>' . ' To ' . '<b>' . $newValue . '</b>';
                            }
                        } else {
                            $history['call_out_fee'] = 'Added Call Out Fee ' . '<b>' . $newValue . '</b>';
                        }
                    }
                    break;

                case 'to_follow':
                    if (!empty($newValue)) {
                        if (!empty($previousLog[$key])) {
                            if ($newValue != $previousLog[$key]) {
                                $history['to_follow'] = 'Changed Deferred Date from ' . '<b>' . getDateFormating($previousLog[$key]) . '</b>' . ' To ' . '<b>' . getDateFormating($newValue) . '</b>';
                            }
                        } else {
                            $history['to_follow'] = 'Added Deferred Date ' . '<b>' . getDateFormating($newValue) . '</b>';
                        }
                    } elseif (!empty($previousLog[$key])) {
                        $history['to_follow'] = 'Delete Deferred Date';
                    }
                    break;

                case 'is_deleted':
                    if (!empty($newValue)) {
                        if ($newValue != $previousLog[$key]) {
                            $history['is_deleted'] = 'Delete Booking';
                        }
                    } else {
                        if ($newValue != $previousLog[$key]) {
                            $history['is_deleted'] = 'Undelete Booking';
                        }
                    }
                    break;

                case 'address_log':
                    if (!empty($newValue)) {
                        if (isset($previousLog[$key]) && !empty($previousLog[$key])) {
                            if ($newValue != $previousLog[$key]) {
                                $history['address_log'] = 'Changed Address from ' . '<b>' . $previousLog[$key] . '</b>' . ' To ' . '<b>' . $newValue . '</b>';
                            }
                        } else {
                            $history['address_log'] = 'Added Address ' . '<b>' . $newValue . '</b>';
                        }
                    }
                    break;
            }
        }

        return $history;
    }

    public function getServiceLogDetails($logBookingId) {


        $bookingLog = $this->getById($logBookingId);

        $this->fill($bookingLog, array('booking_service_log'));

        $previousLog = $this->getPreviousLog($bookingLog['booking_id'], $bookingLog['log_id']);
        if (!empty($previousLog)) {
            $this->fill($previousLog, array('booking_service_log'));
        }

        $history = array();

        if (!empty($bookingLog['booking_service_log'])) {

            if (isset($previousLog['booking_service_log']) && !empty($previousLog['booking_service_log'])) {

                // Previous Service Array
                $previousLogArray = array();
                foreach ($previousLog['booking_service_log'] as $bookingService) {
                    $key = "{$bookingService['booking_id']}_{$bookingService['service_id']}_{$bookingService['clone']}";
                    $previousLogArray[$key] = $bookingService;
                }

                // New Service Array
                $newLogArray = array();
                foreach ($bookingLog['booking_service_log'] as $bookingService) {
                    $key = "{$bookingService['booking_id']}_{$bookingService['service_id']}_{$bookingService['clone']}";
                    $newLogArray[$key] = $bookingService;
                }

                // Added Services
                $addServices = array_diff_key($newLogArray, $previousLogArray);

                if (!empty($addServices)) {
                    foreach ($addServices as $addService) {
                        $history[] = 'Added Service ' . '<b>' . $addService['service_details']['service_name'] . '</b>' . ' With Qoute ' . '<b>' . $addService['priceArray'] . '</b>';
                    }
                }

                // Delete Service



                $deleteServices = array_diff_key($previousLogArray, $newLogArray);
                if (!empty($deleteServices)) {
                    foreach ($deleteServices as $deleteService) {
                        $history[] = 'Delete Service ' . '<b>' . $deleteService['service_details']['service_name'] . '</b>' . ' With Qoute ' . '<b>' . $deleteService['priceArray'] . '</b>';
                    }
                }

                // Same Service

                $sameServices = array_intersect_key($newLogArray, $previousLogArray);

                if ($sameServices) {
                    foreach ($sameServices as $key => $sameService) {

                        $serviceName = $newLogArray[$key]['service_details']['service_name'];

                        if ($newLogArray[$key]['priceArray'] != $previousLogArray[$key]['priceArray']) {
                            $history[] = 'Changed Service Qoute With Name ' . '<b>' . $serviceName . '</b>' . ' from ' . '<b>' . $previousLogArray[$key]['priceArray'] . '</b>' . ' to ' . '<b>' . $newLogArray[$key]['priceArray'] . '</b>';
                        }
                        if ($newLogArray[$key]['contractor_id'] != $previousLogArray[$key]['contractor_id']) {

                            $newContractor = $newLogArray[$key]['contractor']['username'];
                            $previousContractor = $previousLogArray[$key]['contractor']['username'];

                            $history[] = 'Changed Technician that assign to this ' . '<b>' . $serviceName . '</b>' . ' Service from ' . '<b>' . ucwords($previousContractor) . '</b>' . ' to ' . '<b>' . ucwords($newContractor) . '</b>';
                        }
                    }
                }
            } else {
                foreach ($bookingLog['booking_service_log'] as $bookingService) {
                    $history[] = 'Added Service ' . '<b>' . $bookingService['service_details']['service_name'] . '</b>' . ' With Qoute ' . '<b>' . $bookingService['priceArray'] . '</b>';
                }
            }
        }
        return $history;
    }

    public function getTotalAmountBookingLogDetails($logBookingId) {

        $log = $this->getById($logBookingId);

        // Load Model

        $modelContractorServiceBookingLog = new Model_ContractorServiceBookingLog();


        $subTotal = $modelContractorServiceBookingLog->getTotalBookingQoute($log['booking_id'], $logBookingId);
        $totalDiscount = !empty($log['total_discount']) ? $log['total_discount'] : 0;
        $callOutFee = !empty($log['call_out_fee']) ? $log['call_out_fee'] : 0;


        $gst = ($subTotal + $callOutFee - $totalDiscount) * get_config('gst_tax');
        $paidAmount = !empty($log['paid_amount']) ? $log['paid_amount'] : 0;
        $refund = !empty($log['refund']) ? $log['refund'] : 0;
        $total = $subTotal + $callOutFee + $gst - $totalDiscount;
        $totalAmount = $total - $refund;
        $balanceDue = $totalAmount - $paidAmount;


        $totalAmountDetails = array();
        $totalAmountDetails['sub_total'] = $subTotal;
        $totalAmountDetails['total_discount'] = $totalDiscount;
        $totalAmountDetails['call_out_fee'] = $callOutFee;
        $totalAmountDetails['gst'] = $gst;
        $totalAmountDetails['paid_amount'] = $paidAmount;
        $totalAmountDetails['refund'] = $refund;
        $totalAmountDetails['total'] = $total;
        $totalAmountDetails['paid_amount'] = $paidAmount;
        $totalAmountDetails['total_amount'] = $totalAmount;
        $totalAmountDetails['balance_due'] = $balanceDue;

        return $totalAmountDetails;
    }

    public function getPreviousLog($bookingId, $LogBookingId) {

        /* $selectB = $this->getAdapter()->select();
          $selectB->from($this->_name, array('MAX(log_id)'));
          $selectB->where("booking_id = {$bookingId}");
          $selectB->where("log_id < {$LogBookingId}");

          $selectA = $this->getAdapter()->select();
          $selectA->from($this->_name);
          $selectA->where("log_id = ({$selectB})"); */

        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_id = {$bookingId}");
        $select->where("log_id < {$LogBookingId}");
        $select->order("log_id DESC");
        $select->limit(1);


        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the assigned booking ids and limit them as the $limit and $userId
     * 
     * @param arrray $bookingId
     * @param int $userId
     * @param int $limit
     * @return array  
     */
    public function getRecentBookingLog($filters = array()) {

        $loggedUser = CheckAuth::getLoggedUser();
        $companyId = CheckAuth::getCompanySession();
        $userId = $loggedUser['user_id'];

        $selectB = $this->getAdapter()->select();
        $selectB->from($this->_name, array('booking_id', 'MAX(log_id) AS log_id'));
        $selectB->group('booking_id');

        $select = $this->getAdapter()->select();
        $select->from(array('bl' => $this->_name));
        $select->joinInner(array('b' => $selectB), 'b.booking_id = bl.booking_id AND b.log_id = bl.log_id', '');
        $select->group('booking_id');
        $select->order('log_id DESC');
        $select->where("bl.company_id = {$companyId}");
        $select->limit(50);

	//echo $select;

        if (!CheckAuth::checkCredential(array('canSeeAllBookingHistory'))) {
            if (CheckAuth::checkCredential(array('canSeeOnlyHisBookingHistory'))) {
                if (CheckAuth::checkCredential(array('canSeeAssignedBookingHistory'))) {
                    $select->joinInner(array('csb' => 'contractor_service_booking'), 'bl.booking_id = csb.booking_id');
                    $select->where("bl.log_user_id = {$userId} OR csb.contractor_id = {$userId}");
                } else {
                    $select->where("bl.log_user_id = {$userId}");
                }
            } else {
                $select->where("bl.log_user_id = {$userId}");
            }
        }
        if ($filters) {

            if (!empty($filters['created_by'])) {
                $userId = (int) $filters['created_by'];
                $select->where("bl.created_by = '{$userId}'");
            }
        }

        return $this->getAdapter()->fetchall($select);
    }

    public function getLogNotificationByBookingId($booking_id) {
        $model_bookingStatus = new Model_BookingStatus();
        $booking_id = (int) $booking_id;
        $lastBookingLog = $this->getLastLogByBookingId($booking_id);
        $this->fill($lastBookingLog, array('address_log'));
        $previousBookingLog = $this->getPreviousLog($lastBookingLog['booking_id'], $lastBookingLog['log_id']);
        if (!empty($previousBookingLog)) {
            $this->fill($previousBookingLog, array('address_log'));
        }
        $history = array();
        foreach ($lastBookingLog as $key => $newValue) {
            switch ($key) {
                case 'booking_start':
                    if (!empty($newValue)) {
                        if (!empty($previousBookingLog[$key])) {
                            if ($newValue != $previousBookingLog[$key]) {
                                $prev_day = date("l", strtotime($previousBookingLog[$key]));
                                $prev_time = date("H:ia", strtotime($previousBookingLog[$key]));
                                $prev_month_day = date("M d", strtotime($previousBookingLog[$key]));

                                $new_day = date("l", strtotime($newValue));
                                $new_time = date("H:ia", strtotime($newValue));
                                $new_month_day = date("M d", strtotime($newValue));
                                $history['booking_moved'] = 'moved from ' . '<span style="color:#69bdaa;">' . $prev_day . ' ' . $prev_month_day . ', ' . $prev_time . ' </span>' . ' To ' . '<span style="color:#69bdaa;">' . $new_day . ' ' . $new_month_day . ', ' . $new_time . '</span>';
                            }
                        }
                    }
                    break;
                case 'status_id':
                    if (!empty($newValue)) {
                        $newStatus = $model_bookingStatus->getById($newValue);

                        if (!empty($previousBookingLog[$key])) {

                            if ($newValue != $previousBookingLog[$key] && $newStatus['name'] == "CANCELLED") {
                                $oldStatus = $model_bookingStatus->getById($previousBookingLog[$key]);
                                $history['status_id'] = ' CANCELLED';
                            } else if ($newValue != $previousBookingLog[$key] && $newStatus['name'] == "ON HOLD") {
                                $oldStatus = $model_bookingStatus->getById($previousBookingLog[$key]);
                                $history['status_id'] = ' placed ON HOLD';
                            }
                        }
                    }
                    break;
                case 'address_log':
                    if (!empty($newValue)) {
                        if (isset($previousBookingLog[$key]) && !empty($previousBookingLog[$key])) {
                            if ($newValue != $previousBookingLog[$key]) {
                                $history['address_log'] = $newValue . '</b>';
                            }
                        }
                    }
                    break;
            }
        }


        return $history;
    }

    public function pushNotificationByBookingId($booking_id) {
        $model_bookingStatus = new Model_BookingStatus();
        $booking_id = (int) $booking_id;
        $lastBookingLog = $this->getLastLogByBookingId($booking_id);
        $this->fill($lastBookingLog, array('address_log', 'booking_service_log'));
        $previousBookingLog = $this->getPreviousLog($lastBookingLog['booking_id'], $lastBookingLog['log_id']);
        if (!empty($previousBookingLog)) {
            $this->fill($previousBookingLog, array('address_log', 'booking_service_log'));
        }
        $history = array();
        foreach ($lastBookingLog as $key => $newValue) {
            // echo $key .' - ';
            switch ($key) {
                case 'status_id':
                    if (!empty($newValue)) {
                        $newStatus = $model_bookingStatus->getById($newValue);

                        if (!empty($previousBookingLog[$key])) {

                            if ($newValue != $previousBookingLog[$key]) {
                                if ($newStatus['name'] == "CANCELLED") {
                                    MobileNotification::notify($booking_id, "booking cancelled");
                                } else if ($newStatus['name'] == "ON HOLD") {
                                    MobileNotification::notify($booking_id, "booking on hold");
                                }
                            }
                        }
                    }
                    break;
                case 'booking_start':
                    if (!empty($newValue)) {

                        if (!empty($previousBookingLog[$key])) {
                            if ($newValue != $previousBookingLog[$key]) {
                                MobileNotification::notify($booking_id, "booking moved");
                            }
                        }
                    }
                    break;
                case 'address_log':
                    if (!empty($newValue)) {
                        if (isset($previousBookingLog[$key]) && !empty($previousBookingLog[$key])) {
                            if ($newValue != $previousBookingLog[$key]) {

                                echo MobileNotification::notify($booking_id, "booking address changed");
                            }
                        }
                    }
                    break;
                case 'booking_service_log':

                    $notify = 0;
                    if (!empty($lastBookingLog['booking_service_log'])) {
                        if (isset($previousBookingLog['booking_service_log']) && !empty($previousBookingLog['booking_service_log'])) {
                            // Previous Service Array
                            $previousLogArray = array();
                            foreach ($previousBookingLog['booking_service_log'] as $bookingService) {
                                $key = "{$bookingService['booking_id']}_{$bookingService['service_id']}_{$bookingService['clone']}";
                                $previousLogArray[$key] = $bookingService;
                            }

                            // New Service Array
                            $newLogArray = array();
                            foreach ($lastBookingLog['booking_service_log'] as $bookingService) {
                                $key = "{$bookingService['booking_id']}_{$bookingService['service_id']}_{$bookingService['clone']}";
                                $newLogArray[$key] = $bookingService;
                            }

                            $sameServices = array_intersect_key($newLogArray, $previousLogArray);

                            if ($sameServices) {

                                foreach ($sameServices as $key => $sameService) {
                                    $serviceName = $newLogArray[$key]['service_details']['service_name'];
                                    if ($newLogArray[$key]['priceArray'] != $previousLogArray[$key]['priceArray'] || $newLogArray[$key]['contractor_id'] != $previousLogArray[$key]['contractor_id']) {

                                        $notify = 1;
                                    }
                                }
                            }

                            if ($notify) {
                                MobileNotification::notify($booking_id, "booking service changed");
                            }
                        }
                    }





                    break;
            }
        }


        return $history;
    }

    public function getPreviousUserLog($LogUserId, $LogBookingId) {

        /* $selectB = $this->getAdapter()->select();
          $selectB->from($this->_name, array('MIN(log_id)'));
          $selectB->where("log_user_id = {$LogUserId}");
          $selectB->where("log_id > {$LogBookingId}");

          $selectA = $this->getAdapter()->select();
          $selectA->from($this->_name);
          $selectA->where("log_id = ({$selectB})"); */
        //$sql = $selectA->__toString();
        //echo $sql;

        $select = $this->getAdapter()->select();
        $select->from($this->_name, array('log_created'));
        $select->where("log_user_id = {$LogUserId}");
        $select->where("log_id > {$LogBookingId}");
        $select->order("log_id ASC");
        $select->limit(1);
        return $this->getAdapter()->fetchRow($select);
    }
    
    public function getFirstBookingLogByBookingId($bookingId) {
        $selectB = $this->getAdapter()->select();
        $selectB->from($this->_name, array('MIN(log_id)'));
        $selectB->where("booking_id = {$bookingId}");
        $selectA = $this->getAdapter()->select();
        $selectA->from($this->_name);
        $selectA->where("log_id = ({$selectB})");
        $selectA->where("booking_id = {$bookingId}");
        
        return $this->getAdapter()->fetchAll($selectA);
    }


}
