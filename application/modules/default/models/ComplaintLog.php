<?php

class Model_ComplaintLog extends Zend_Db_Table_Abstract {

    protected $_name = 'complaint_log';
	
	
	
	    public function getAll($filters = array(), $order = null, &$pager = null, $limit = 0, $perPage = 0, $currentPage = 0) {

        $joinInner = array();
        $select = $this->getAdapter()->select();
        $select->from(array('complaint_log' => $this->_name));
        $select->order($order);

        if ($filters) {
            if (!empty($filters['log_user_id'])) {
                $logUserId = $this->getAdapter()->quote(trim($filters['log_user_id']));
                $select->where("log_user_id = {$logUserId}");
            }
        }
        if ($joinInner) {
            foreach ($joinInner as $inner) {
                $select->joinInner($inner['name'], $inner['cond'], $inner['cols']);
            }
        }


        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        } elseif ($limit) {
            $select->limit($limit);
        } elseif ($perPage && $currentPage) {
            $select->limit($perPage, ($currentPage - 1) * $perPage);
        }

        return $this->getAdapter()->fetchAll($select);
    }
	
	
	 public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("log_id= '{$id}'");
    }
	
	 public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "log_id= '{$id}'");
    }
	
	public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("log_id= '{$id}'");
        return $this->getAdapter()->fetchRow($select);
    }
	
	    public function addComplaintLog($id, $log_user_id = 0) {

        $modelComplaint = new Model_Complaint();
        $data = $modelComplaint->getById($id);

        if (!$log_user_id) {
            $logedUser = CheckAuth::getLoggedUser();
            $log_user_id = !empty($logedUser) ? $logedUser['user_id'] : 0;
        }

        $dbParams = array();
        $dbParams['log_created'] = time();
        $dbParams['log_user_id'] = $log_user_id;
        $dbParams['complaint_id'] = $data['complaint_id'];
        $dbParams['user_id'] = $data['user_id'];
        $dbParams['complaint_type_id'] = $data['complaint_type_id'];
        $dbParams['comment'] = $data['comment'];
        $dbParams['booking_id'] = $data['booking_id'];
        $dbParams['created'] = $data['created'];
        $dbParams['complaint_num'] = $data['complaint_num'];
        $dbParams['complaint_status'] = $data['complaint_status'];
        $dbParams['to_follow'] = $data['to_follow'];
        $dbParams['is_to_follow'] = $data['is_to_follow'];
        $dbParams['company_id'] = $data['company_id'];
        $dbParams['count'] = $data['count'];
        $dbParams['full_text_search'] = $data['full_text_search'];
        $dbParams['is_approved'] = $data['is_approved'];
       
        return parent::insert($dbParams);
    }
	
	

	
	
	
}

?>