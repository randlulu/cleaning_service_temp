<?php

class Model_BookingService extends Zend_Db_Table_Abstract {

    protected $_name = 'booking_service';

    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "booking_service_id= '{$id}'");
    }

    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("booking_service_id= '{$id}'");
    }

    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_service_id= '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function deleteBybookingId($id) {
        $id = (int) $id;
        return parent::delete("booking_id= '{$id}'");
    }

    public function getByBookingId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_id= '{$id}'");

        return $this->getAdapter()->fetchAll($select);
    }

    public function isServiceInBooking($booking_id, $service_id) {
        $booking_id = (int) $booking_id;
        $service_id = (int) $service_id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("service_id='{$service_id}'");
        $select->where("booking_id='{$booking_id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function addEditBookingService($booking_id, $services=array(), $quantity=array(), $quote=array()) {
        $count = count($services);
        for ($i = 0; $i < $count; $i++) {
            $bookingService = $this->isServiceInBooking($booking_id, $services[$i]);
            if ($bookingService) {
                //
                //update
                //
                $data = array(
                    'booking_id' => $booking_id,
                    'service_id' => $services[$i],
                    'service_quote' => $quote[$i],
                    'service_quantity' => $quantity[$i]
                );
                $this->updateById($bookingService['booking_service_id'], $data);
            } else {
                //
                //insert
                //
                $data = array(
                    'booking_id' => $booking_id,
                    'service_id' => $services[$i],
                    'service_quote' => $quote[$i],
                    'service_quantity' => $quantity[$i]
                );
                $this->insert($data);
            }
        }
        //
        //prepare to delete
        //
        $bookingServiceIds = $this->getByBookingId($booking_id);
        $old_services = array();
        if ($bookingServiceIds) {
            foreach ($bookingServiceIds as $bookingServiceId) {
                $old_services[] = $bookingServiceId['service_id'];
            }
        }
        $to_delete_services = array_diff($old_services, $services);
        if ($to_delete_services) {
            //
            //delete
            //
            foreach ($to_delete_services as $to_delete_service) {
                $bookingService = $this->isServiceInBooking($booking_id, $to_delete_service);
                $this->deleteById($bookingService['booking_service_id']);
            }
        }
    }

}