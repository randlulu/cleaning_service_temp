<?php

class Model_UserCompanies extends Zend_Db_Table_Abstract {

    protected $_name = 'user_company';

     /**
     *get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);

        $select->order($order);
        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     *update table row according to the assigned $id
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "id= '{$id}'");
    }

    /**
     *delete table row according to the assigned $id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("id= '{$id}'");
    }

    /**
     *get table row according to the assigned $id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id= '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     *get table rows according to the assigned user id
     * 
     * @param int $id
     * @return array 
     */
    public function getByUserId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from(array('uc' => $this->_name));
        $select->joinInner(array('c' => 'company'), 'uc.company_id = c.company_id', array('c.city_id'));
        $select->where("user_id= '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }
    
    /**
     *get table rows according to the assigned user id
     * 
     * @param int $id
     * @return array 
     */
    public function getCompaniesByUserId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from(array('uc' => $this->_name) ,'c.*');
        $select->joinInner(array('c' => 'company'), 'uc.company_id = c.company_id', '');
        $select->where("user_id= '{$id}'");
                
        return $this->getAdapter()->fetchRow($select);
    }
    
    
    
    public function getUsersByCompanyId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from(array('uc' => $this->_name) ,'c.*');
        $select->joinInner(array('c' => 'user'), 'uc.user_id = c.user_id', '');
        $select->where("company_id= '{$id}'");
        return $this->getAdapter()->fetchAll($select);
    }
    
    

}