<?php

class Model_InvoiceSubscription extends Zend_Db_Table_Abstract {

    protected $_name = 'invoice_subscription';

    //get invoice by ID
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("invoice_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function getByInvoiceNumOld($id) {
        
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("invoice_id = '$id'");

        return $this->getAdapter()->fetchRow($select);
    }
    
    public function updateByInvoiceNum($id, $data) {
    

       $oldData =  $this->getInvoiceByInvoiceNum($id);
        
        $newData = array(
            'amount' => $oldData['amount'] + $data['amount'],
            'status' => 'paid'
        );
        return parent::update($newData, "invoice_num = '$id'");
    }

    // get table row according to the assigned invoice num  
    public function getInvoiceByInvoiceNum($invoice_num) {
        $id = $invoice_num;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("invoice_num = '$id'");
        return $this->getAdapter()->fetchRow($select);
    }

    //check if user have invoice in the current period
    public function checkInvoiceExistInCurrentPeriod($account_id) {

        $id = (int) $account_id;
        $actual_date = date("Y-m-d", time());
        $select = $this->getAdapter()->select();
        $select->from(array('inv' => $this->_name));
         $select->joinInner(array('acc' => 'account'), 'inv.account_id = acc.id', '');
        $select->where("inv.account_id = '{$id}'");
        $select->where("'". $actual_date . "' between acc.from and acc.to ");
        $ret_val = $this->getAdapter()->fetchRow($select);

        if ($ret_val) {
            $data = array(
                'invoice_num' => $ret_val['invoice_num'],
                'exist' => 'yes'
            );
        } else {
            $select = $this->getAdapter()->select();
            $select->from($this->_name, 'MAX(invoice_id)');
            $select->where("account_id = '{$id}'");
            $result = $this->getAdapter()->fetchOne($select);
            $inc = str_pad($result, 5, '0', STR_PAD_LEFT);
            $invoice_num = 'OCTO' . date('y') . $inc;

            $data = array(
                'invoice_num' => $invoice_num,
                'exist' => 'no'
            );
        }

        return $data;
    }

}
 