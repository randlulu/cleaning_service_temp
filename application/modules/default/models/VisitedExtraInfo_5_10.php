<?php

class Model_VisitedExtraInfo extends Zend_Db_Table_Abstract {

    protected $_name = 'visited_extra_info';

    /*public function insert(array $data) {
        $id = parent::insert($data);

        return $id;
    }*/
	public function insert(array $data) {
        $id = parent::insert($data);
		
		//D.A 30/08/2015 Remove Booking Scheduled Visits Cache
        if($id){						
		require_once 'Zend/Cache.php';
		$company_id = CheckAuth::getCompanySession();
		$bookingScheduledVisitsCacheID= $data['booking_id'].'_bookingScheduledVisits';
		$bookingViewDir=get_config('cache').'/'.'bookingsView'.'/'.$company_id;								
		if (!is_dir($bookingViewDir)) {
		mkdir($bookingViewDir, 0777, true);
		}
		$frontEndOption= array('lifetime'=> NULL,
		'automatic_serialization'=> true);
		$backendOptions = array('cache_dir'=>$bookingViewDir );
		$Cache = Zend_Cache::factory('Core','File',$frontEndOption,$backendOptions);			
		$Cache->remove($bookingScheduledVisitsCacheID);	
		}
        return $id;
    }

    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from(array('vei' => $this->_name));

        $select->order($order);



        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }
	
	
	public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where(" visited_extra_info_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }
	
    public function getByBookingId($id) {
        $id = (int) $id;
		$modelBookingProduct = new  Model_BookingProduct();
        $select = $this->getAdapter()->select();
        $select->from(array('vei'=>$this->_name));        
        $select->joinLeft(array('bp'=>'booking_product'),'vei.visited_extra_info_id = bp.visited_extra_info_id',array('product_id','ltr'));
        $select->joinLeft(array('p'=>'product'),'bp.product_id = p.id',array('product','id'));
        $select->joinLeft(array('bd'=>'booking_discussion'),'vei.visited_extra_info_id = bd.visited_extra_info_id',array('user_message'));
		$select->where("vei.booking_id = {$id}");
		/*if($_SERVER['REMOTE_ADDR'] == '176.106.46.142'){
			echo 'By Islam '.$select->__toString();
		}*/
		$rows = $this->getAdapter()->fetchAll($select);
		
		$infoWithIndexes = array();
		if(!empty($rows)){
			foreach($rows as &$row){
				$index = $row['visited_extra_info_id'];
				$products = $modelBookingProduct->getByVisitedExtraInfoId($index);
				$row['visit_products'] =$products;
				
				$infoWithIndexes[$index]=$row;
			}			
		}

		/*if($_SERVER['REMOTE_ADDR'] == '176.106.46.142'){
			print_r($infoWithIndexes);
		}*/
        return $infoWithIndexes;
    }
	
	
	public function getByBookingId2($id) {
        $id = (int) $id;
		$modelBookingProduct = new  Model_BookingProduct();
        $select = $this->getAdapter()->select();
        $select->from(array('vei'=>$this->_name));
		$select->join(array('b'=>'booking'),'vei.visited_extra_info_id = b.visited_extra_info_id',array());	
        $select->joinLeft(array('bp'=>'booking_product'),'vei.visited_extra_info_id = bp.visited_extra_info_id',array('product_id','ltr'));
        $select->joinLeft(array('p'=>'product'),'bp.product_id = p.id',array('product','id'));
        $select->joinLeft(array('bd'=>'booking_discussion'),'vei.visited_extra_info_id = bd.visited_extra_info_id',array('user_message'));
		$select->where("vei.booking_id = {$id}");
		/*if($_SERVER['REMOTE_ADDR'] == '176.106.46.142'){
			echo 'By Islam '.$select->__toString();
		}*/
		$rows = $this->getAdapter()->fetchAll($select);
		
		$infoWithIndexes = array();
		if(!empty($rows)){
			foreach($rows as &$row){
				$index = $row['visited_extra_info_id'];
				$products = $modelBookingProduct->getByVisitedExtraInfoId($index);
				$row['visit_products'] =$products;
				
				$infoWithIndexes[$index]=$row;
			}			
		}
		
		
		$select2 = $this->getAdapter()->select();
        $select2->from(array('vei'=>$this->_name));
		$select2->join(array('b'=>'booking'),'vei.visited_extra_info_id = b.visited_extra_info_id',array());	
        $select2->joinLeft(array('bp'=>'booking_product'),'vei.visited_extra_info_id = bp.visited_extra_info_id',array('product_id','ltr'));
        $select2->joinLeft(array('p'=>'product'),'bp.product_id = p.id',array('product','id'));
        $select2->joinLeft(array('bd'=>'booking_discussion'),'vei.visited_extra_info_id = bd.visited_extra_info_id',array('user_message'));
		$select2->where("vei.booking_id = {$id}");
		
		$rows2 = $this->getAdapter()->fetchAll($select2);
		
		
		if(!empty($rows2)){
			foreach($rows2 as &$row){
				$index = $row['visited_extra_info_id'];
				$products = $modelBookingProduct->getByVisitedExtraInfoId($index);
				$row['visit_products'] =$products;
				
				$infoWithIndexes[$index]=$row;
			}			
		} 

		/*if($_SERVER['REMOTE_ADDR'] == '176.106.46.142'){
			print_r($infoWithIndexes);
		}*/
        return $infoWithIndexes;
    }
	
    /*public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "visited_extra_info_id = {$id}");
    }*/
	
	//D.A 30/08/2015 Remove Booking Scheduled Visits Cache

public function updateById($id, $data) {
        $id = (int) $id;
		$visit_extra_info_id=parent::update($data, "visited_extra_info_id = {$id}");
		//D.A 30/08/2015 Remove Booking Scheduled Visits Cache
		if($visit_extra_info_id){						
		require_once 'Zend/Cache.php';
		$company_id = CheckAuth::getCompanySession();
		$visit_extra_info=$this->getById($id);
		$bookingScheduledVisitsCacheID= $visit_extra_info['booking_id'].'_bookingScheduledVisits';
		$bookingViewDir=get_config('cache').'/'.'bookingsView'.'/'.$company_id;								
		if (!is_dir($bookingViewDir)) {
			mkdir($bookingViewDir, 0777, true);
		}	
		$frontEndOption= array('lifetime'=> NULL,
		'automatic_serialization'=> true);
		$backendOptions = array('cache_dir'=>$bookingViewDir );
		$Cache = Zend_Cache::factory('Core','File',$frontEndOption,$backendOptions);			
		$Cache->remove($bookingScheduledVisitsCacheID);	
		}
        return $visit_extra_info_id;	
    }
     public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("visited_extra_info_id = {$id}");
    }
	
	

}

