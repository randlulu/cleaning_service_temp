<?php

class Model_Bank extends Zend_Db_Table_Abstract {

    protected $_name = 'bank';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order($order);

        $where = array();

        if (!CheckAuth::checkCredential(array('canHandleAllCompanies'))) {
            $filters['company_id'] = CheckAuth::getCompanySession();
        }

        $filters['company_id'] = CheckAuth::getCompanySession();

        if ($filters) {
            if (!empty($filters['keywords'])) {
                $keywords = $this->getAdapter()->quote('%' . $filters['keywords'] . '%');
                $where[] = ("bsb LIKE {$keywords}");
            }
            if ($filters['company_id'] != 0) {
                $where[] = ("company_id = {$filters['company_id']}");
            }
            if ($where) {
                $select->where(implode(' AND ', $where));
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "bank_id = '{$id}'");
    }

    /**
     * delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("bank_id = '{$id}'");
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("bank_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function getByCompanyId($companyId) {
        $companyId = (int) $companyId;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("company_id = '{$companyId}'");

        return $this->getAdapter()->fetchRow($select);
    }

}