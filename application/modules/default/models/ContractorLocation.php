<?php

class Model_ContractorLocation extends Zend_Db_Table_Abstract {

    protected $_name = 'contractor_location';

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
	 
	  
	  
   public function getAll($filters = array()){
    
	$select = $this->getAdapter()->select();
	$select->from($this->_name);
	return $this->getAdapter()->fetchAll($select);
   
   }
	 
	 
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, " id = '{$id}'");
    }

    /**
     * delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("id = '{$id}'");
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id){
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the assigned Contractor Id
     * 
     * @param int $id
     * @return array
     */
	 
    public function getByContractorId($contractor_ids , $bookingId) {
        //$id = (int) $id;
		$contractors = implode(",",$contractor_ids);
        $select = $this->getAdapter()->select();
        $select->from(array('con_loc'=>$this->_name));
		$select->joinInner(array('bok_att' => 'booking_attendance'),'bok_att.booking_id = con_loc.booking_id',array());
		$select->joinInner(array('u' => 'user'),'u.user_id = con_loc.user_id',array('username'));
		$select->where("con_loc.user_id in ({$contractors})");
		$select->where("con_loc.booking_id = {$bookingId}");
		$select->where("bok_att.check_in_time IS NOT NULL");
		$select->where("bok_att.check_out_time IS NULL");
		$select->order('id Desc');
		$select->limit(1);
		
		//echo $select->__toString();
		
        return $this->getAdapter()->fetchAll($select);
    }


}