<?php

class Model_ContractorMobileInfo extends Zend_Db_Table_Abstract {

    protected $_name = 'contractor_mobile_info';
    
  
	public function insert(array $data) {
		$id = 0;
        $id = parent::insert($data);
		
        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'added');

        return $id;
    }

	 public function getById($id){
     $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from(array('cmi' => $this->_name));
        $select->where("cmi.id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
	}
	
	public function getByContractorId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from(array('cmi' => $this->_name));
        $select->where("cmi.contractor_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }
	
	public function getByContractorIdAndDeviceTokenAndOs($contractor_id, $device_token, $os) {
        $contractor_id = (int) $contractor_id;
        $select = $this->getAdapter()->select();
        $select->from(array('cmi' => $this->_name));
        $select->where("cmi.contractor_id = '{$contractor_id}'");
        $select->where("cmi.devicetoken = '{$device_token}'");
        $select->where("cmi.os = '{$os}'");

        return $this->getAdapter()->fetchRow($select);
    }
	
	//By islam
	public function getAllByDeviceUid($device_uid, $os) {
        
        $select = $this->getAdapter()->select();
        $select->from(array('cmi' => $this->_name));
        
        $select->where("cmi.deviceuid = '{$device_uid}'");
        $select->where("cmi.os = '{$os}'");

        return $this->getAdapter()->fetchAll($select);
    }
	
	
	public function getByContractorIdAndOs($contractor_id, $os) {
        $contractor_id = (int) $contractor_id;
        $select = $this->getAdapter()->select();
        $select->from(array('cmi' => $this->_name));
        $select->where("cmi.contractor_id = '{$contractor_id}'");
        $select->where("cmi.os = '{$os}'");

        return $this->getAdapter()->fetchAll($select);
    }
	
	
	public function updateByContractorIdAndDeviceToken($ContractorId, $deviceToken, $data) {
        
		$ContractorId = (int) $ContractorId;
        return parent::update($data, "contractor_id = '{$ContractorId}' and device_token = '{$deviceToken}'");
       
    }
	
	public function deleteByContractorIdAndDeviceToken($ContractorId, $deviceToken) {
        $ContractorId = (int) $ContractorId;
        return parent::delete("contractor_id = '{$ContractorId}' and devicetoken = '{$deviceToken}'");
    }
	//By islam
	public function deleteByDeviceUid($device_uid) {
        
        return parent::delete("deviceuid = '{$device_uid}'");
    }
	
    
    public function getAllByContractorId($id) {

        $select = $this->getAdapter()->select();
        $select->from(array('cmi' => $this->_name));

        $select->where("cmi.contractor_id = '{$id}'");
        return $this->getAdapter()->fetchAll($select);
    }
    

}