<?php

class Model_BookingDiscussionBeforeMongo extends Zend_Db_Table_Abstract {

    protected $_name = 'booking_discussion';

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateById($id, $data) {
        $id = (int) $id;

        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'changed');

        return parent::update($data, "discussion_id = '{$id}'");
    }
	
	public function deleteByExtraInfoId($id){
	    $id = (int) $id;
        return parent::delete("visited_extra_info_id = '{$id}'");
	}
	
	public function updateByExtraInfoId($id , $data){
	
	$id = (int) $id;
    $success = parent::update($data, "visited_extra_info_id = '{$id}'");
    
	}

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("discussion_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }
	
	public function getByExtraInfoId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("visited_extra_info_id = '{$id}'"); 

		
		//echo $select->__toString();
        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get all the recent discussions that the user are able to see
     *  
     * @return array  
     */
    public function getRecentDiscussion($filters = array()) {

        $loggedUser = CheckAuth::getLoggedUser();
        $companyId = CheckAuth::getCompanySession();
        $userId = $loggedUser['user_id'];

        $select = $this->getAdapter()->select();

        $select->distinct();

        $select->from(array('bd' => $this->_name));

        $select->joinInner(array('csb' => 'contractor_service_booking'), 'bd.booking_id = csb.booking_id', 'csb.contractor_id');
        $select->joinInner(array('bok' => 'booking'), 'bd.booking_id = bok.booking_id', '');
        $select->where("bok.company_id = {$companyId}");

        if (!CheckAuth::checkCredential(array('canSeeAllBookingDiscussion'))) {
            if (CheckAuth::checkCredential(array('canSeeOnlyHisBookingDiscussion'))) {
                if (CheckAuth::checkCredential(array('canSeeAssignedBookingDiscussion'))) {
                    $select->where("bd.user_id = {$userId} OR csb.contractor_id = {$userId}");
                } else {
                    $select->where("bd.user_id = {$userId}");
                }
            }
        }
        if ($filters) {

            if (!empty($filters['created_by'])) {
                $userId = (int) $filters['created_by'];
                $select->where("bok.created_by = '{$userId}'");
            }
        }

        $select->order("created DESC");

        $select->limit(20);

        return $this->getAdapter()->fetchall($select);
    }

    /**
     * get table row according to the assigned BookingId
     * 
     * @param int $id
     * @return array
     */
    /*public function getByBookingId($id, $order = 'asc') {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order("created {$order}");
        $select->where("booking_id = '{$id}'");

        return $this->getAdapter()->fetchAll($select);
    }*/
	
		public function getByBookingId($id, $order = 'asc' , $filter = array()) {
        $id = (int) $id;

		$typeFilter = $this->getAdapter()->quote('booking_discussion' . '%');	
        $select = $this->getAdapter()->select();
        $select->from(array('bd'=>$this->_name));
		$typeFilter = $this->getAdapter()->quote('booking' . '%');	
		$select->joinLeft(array('iid' => 'item_image_discussion'), " iid.item_id = bd.discussion_id AND iid.type LIKE {$typeFilter}",array('group_id'));
		$select->joinLeft(array('im' => 'image'), "(iid.image_id = im.image_id )",array('im.compressed_path','im.thumbnail_path','im.image_id','role_id'=>'im.user_role'));
		if(isset($filter['groups'])){
		 $select->where("iid.group_id > 0");
		 $select->group("iid.group_id");
		}else{		
         $select->where("iid.group_id = 0 OR iid.group_id IS NULL");
         $select->limit(5000, (3 - 1) * 5000);		 
		}
		
		if(isset($filter['user_id'])){
		   $user_id = (int) $filter['user_id'];
		   $select->where("bd.user_id = '{$user_id}'");		  
		}
		
		
		$select->order("bd.created {$order}");
        //$select->where("booking_id = '{$id}'");
		$select->where("is_deleted = 0");
 
     
		
        return $this->getAdapter()->fetchAll($select);
    }
	
	
	/*public function getByBookingId($id, $order = 'asc' , $filter = array()) {
        $id = (int) $id;

		$typeFilter = $this->getAdapter()->quote('booking_discussion' . '%');	
        $select = $this->getAdapter()->select();
        $select->from(array('bd'=>$this->_name));
		$typeFilter = $this->getAdapter()->quote('booking' . '%');			
		if(isset($filter['groups'])){
		 $select->joinLeft(array('iid' => 'item_image_discussion'), " iid.item_id = bd.discussion_id AND iid.type LIKE {$typeFilter}",array('group_id'));
		 $select->where("iid.group_id > 0");
		}else{	
         $select->joinLeft(array('iid' => 'item_image_discussion'), " iid.item_id = bd.discussion_id AND iid.type LIKE {$typeFilter} AND iid.group_id = 0",array('group_id'));			 
		}
		$select->joinLeft(array('im' => 'image'), "(iid.image_id = im.image_id )",array('im.compressed_path','im.thumbnail_path','im.image_id','role_id'=>'im.user_role'));
		if(isset($filter['user_id'])){
		   $user_id = (int) $filter['user_id'];
		   $select->where("bd.user_id = '{$user_id}'");		  
		}
		
		$select->order("bd.created {$order}");
        $select->where("booking_id = '{$id}'");
        $select->where("user_message != ''");
        
        //exit;
		
        return $this->getAdapter()->fetchAll($select);
    }*/
	

    public function insert(array $data) {

        $id = parent::insert($data);

        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'added');

        return $id;
    }
	
	public function deleteByImageId($id) {
        $id = (int) $id;
        return parent::delete("image_id = '{$id}'");
    }
	
	public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("discussion_id = '{$id}'");
    }
	
	
	public function getByImageId($id,$order = 'asc', $group_id = 0 , $filters = array()) {
        
		$id = (int) $id;
		$typeFilter1 = $this->getAdapter()->quote('booking' . '%');	
        $select = $this->getAdapter()->select();
        $select->from(array('bd'=>$this->_name));
		$select->joinInner(array('iid' => 'item_image_discussion'), " iid.item_id = bd.discussion_id AND iid.type LIKE {$typeFilter1}",array('group_id'));		
		$select->order("bd.created {$order}");
		
		if(isset($filters)){		  
		  if(isset($filters['user_id'])){
		   $user_id = (int) $filters['user_id'];
		   $select->where("bd.user_id = '{$user_id}'");		  
		 }
		}
		
        
		if($group_id){
         $select->Where(" (iid.group_id = '{$group_id}' )");
		}else{
		 $select->where(" iid.image_id = '{$id}' ");
		}
		
		$select->group('iid.group_id');
		
		//echo $select->__toString();
		
		
        return $this->getAdapter()->fetchAll($select);
    }
    public function getLastBookingDiscussionByBookingId($bookingId){
           $bookingId = (int) $bookingId;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order("created DESC");
        $select->where("booking_id = '{$bookingId}'");
        $select->LIMIT(1);
        return $this->getAdapter()->fetchRow($select);
    }
     public function getNotesByVisitExtraInfoId($id){
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("visited_extra_info_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

}
