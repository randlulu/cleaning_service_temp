<?php

class Model_RejectBookingQuestionAnswers extends Zend_Db_Table_Abstract {

    protected $_name = 'reject_booking_question_answers';

	/**
     * get comment according to the assigned BookingId
     *
     * @param int $bookingId
     * @return array
     */
    public function getCommentByBookingId($bookingId) {
        $bookingId = (int) $bookingId;
        $select = $this->getAdapter()->select();
        $select->from(array('rbqa' => $this->_name));
        $select->where("rbqa.booking_id= '{$bookingId}'");
        return $this->getAdapter()->fetchRow($select);
    }

	
    /**
     * get table rows according to the assigned filters and page
     *
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from(array('rbqa' => $this->_name));
        $select->joinInner(array('rbq' => 'reject_booking_question'), 'rbq.id =rbqs.question_id', array());
        $select->joinInner(array('rbqo' => 'reject_booking_question_options'), 'rbq.id=rbqo.question_id', array());
        $select->order($order);

        $filters['company_id'] = CheckAuth::getCompanySession();

        if ($filters) {
            if (!empty($filters['company_id'])) {
                $select->where("rbq.company_id = {$filters['company_id']}");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }



    public function updateById($id, array $data){
        $id = (int) $id;
        return parent::update($data, "id = {$id}");

    }


    public function insert(array $data) {

        return parent::insert($data);
    }

    /**
     * delete table row according to the assigned booking id
     *
     * @param int $BookingId
     * @return boolean
     */
    public function deleteByBookingId($BookingId){
        $BookingId = (int) $BookingId;
        return parent::delete("booking_id= {$BookingId}");
    }

    /**
     * get table row according to the assigned BookingId
     *
     * @param int $bookingId
     * @return array
     */
    public function getByBookingId($bookingId) {
        $bookingId = (int) $bookingId;
        $select = $this->getAdapter()->select();
        $select->from(array('rbqa' => $this->_name));
        $select->joinInner(array('rbq' => 'reject_booking_question'), 'rbq.id =rbqa.question_id');
        // $select->joinInner(array('rbqo' => 'reject_booking_question_options'), 'rbqo.id=rbqa.answer');

        $select->where("rbqa.booking_id= '{$bookingId}'");
		$select->order('rbqa.id ASC');
        return $this->getAdapter()->fetchAll($select);
    }
	
	public function getAnswersByBookingId($bookingId){
	  
	    $bookingId = (int) $bookingId;
        $select = $this->getAdapter()->select();
        $select->from(array('rbqa' => $this->_name));
        $select->where("rbqa.booking_id= '{$bookingId}'");
		$select->order('rbqa.id ASC');
        return $this->getAdapter()->fetchAll($select);
	
	}

    /**
     * get table row according to the assigned BookingId and questionId
     *
     * @param int $bookingId
     * @param int $question_id
     * @return array
     */
    public function getByBookingIdandQuestionId($bookingId,$question_id) {
        $bookingId = (int) $bookingId;
        $select = $this->getAdapter()->select();
        $select->from(array('rbqa' => $this->_name));
        $select->joinInner(array('rbq' => 'reject_booking_question'), 'rbq.id =rbqa.question_id');

        $select->where("rbqa.booking_id= '{$bookingId}'");
        $select->where("rbqa.question_id= '{$question_id}'");
        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the assigned BookingId, questionId and answerId
     *
     * @param int $bookingId
     * @param int $question_id
     * @param int $answer_id
     * @return array
     */
    public function getByBookingIdandQuestionIdandAnswer($bookingId,$question_id,$answer_id) {
        $bookingId = (int) $bookingId;
        $select = $this->getAdapter()->select();
        $select->from(array('rbqa' => $this->_name));
        $select->joinInner(array('rbq' => 'reject_booking_question'), 'rbq.id =rbqa.question_id');

        $select->where("rbqa.booking_id= '{$bookingId}'");
        $select->where("rbqa.question_id= '{$question_id}'");
        $select->where("rbqa.answer= '{$answer_id}'");
        return $this->getAdapter()->fetchRow($select);
    }




}