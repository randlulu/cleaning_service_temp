<?php

class Model_EstimateLabel extends Zend_Db_Table_Abstract {

    protected $_name = 'estimate_label';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order($order);




        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }


        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table rows according to the assigned id and data
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        
        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'changed');
        
        return parent::update($data, "id = '{$id}'");
    }

    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        
        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'deleted');
        
        return parent::delete("id = '{$id}'");
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function deleteByEstimateIdAndLabelIds($id, $label_ids) {

        $id = (int) $id;
        $sql = "estimate_id = '{$id}'";
        if ($label_ids) {
            $sql .= ' AND label_id NOT IN (' . implode(', ', $label_ids) . ')';
        }
        return parent::delete($sql);
    }

    public function setLabelsToEstimate($estimate_id, $label_ids) {
        //
        // delete products not in the list
        //
        $estimate_id = (int) $estimate_id;
        if (!empty($label_ids)) {
            foreach ($label_ids AS &$label_id) {
                $label_id = (int) $label_id;
            }
        }

        $this->deleteByEstimateIdAndLabelIds($estimate_id, $label_ids);
        //
        // add the new products
        //
        foreach ($label_ids AS $id) {
            $params = array(
                'estimate_id' => $estimate_id,
                'label_id' => $id
            );

            $this->assignLabelToEstimate($params);
        }
    }

    public function assignLabelToEstimate($params) {

        $estimateLabelLink = $this->getByEstimateAndLabelId($params['estimate_id'], $params['label_id']);
		$add_edit_id='';
		$bookingLabelid='';
        if (!$estimateLabelLink) {
            $add_edit_id= $this->insert($params);
			$bookingLabelid=$add_edit_id;
        } else {
            $add_edit_id=$this->updateById($estimateLabelLink['id'], $params);
            $bookingLabelid= $estimateLabelLink['id'];
        }
		
		//D.A 20/09/2015 Remove Estimate Labels Cache
		if($add_edit_id){							
			require_once 'Zend/Cache.php';
			$company_id = CheckAuth::getCompanySession();
			$estimateLabelsCacheID= $params['estimate_id'].'_estimateParams';
			$estimateViewDir=get_config('cache').'/'.'estimatesView'.'/'.$company_id;	
			if (!is_dir($estimateViewDir)) {
				mkdir($estimateViewDir, 0777, true);
			}			
			$frontEndOption= array('lifetime'=> NULL,
			'automatic_serialization'=> true);
			$backendOptions = array('cache_dir'=>$estimateViewDir );
			$Cache = Zend_Cache::factory('Core','File',$frontEndOption,$backendOptions);			
			$Cache->remove($estimateLabelsCacheID);			
			}
		return $bookingLabelid;
    }	

    public function getByEstimateAndLabelId($id, $label_id) {
        $id = (int) $id;
        $label_id = (int) $label_id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("estimate_id = '{$id}' AND label_id = '{$label_id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function getByEstimateId($id) {

        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("estimate_id = '{$id}'");

        return $this->getAdapter()->fetchAll($select);
    }
    
    public function insert(array $data) {

        $id = parent::insert($data);
        
        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'added');
        
        return $id;
    }

}
