<?php

class Model_UserLoginHistory extends Zend_Db_Table_Abstract {

    protected $_name = 'user_login_history';
    private $modelContractorInfo;

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from(array('ulh' => $this->_name));
        $select->joinInner(array('u' => 'user'), 'u.user_id = ulh.user_id', array('u.username'));
        $select->order($order);
        $select->distinct();

        $filters['company_id'] = CheckAuth::getCompanySession();

        if ($filters) {
            if (!empty($filters['keywords'])) {
                $keywords = $this->getAdapter()->quote('%' . $filters['keywords'] . '%');
                $select->where("username LIKE {$keywords}");
            }
            if (!empty($filters['user_id'])) {
                $select->where("u.user_id = {$filters['user_id']}");
            }
            if (!empty($filters['role_id'])) {
                $select->where("u.role_id = {$filters['role_id']}");
            }
			if (!empty($filters['active'])) {
                $select->where("u.active = '{$filters['active']}'");
            }
			if (!empty($filters['not_username'])) {
                $select->where("u.username != '{$filters['not_username']}'");
            }
            if (!empty($filters['not_role_id'])) {
                $select->where("u.role_id != {$filters['not_role_id']}");
            }
            if (!empty($filters['company_id'])) {
                $select->joinInner(array('uc' => 'user_company'), 'uc.user_id = u.user_id', '');

                $company_id = (int) $filters['company_id'];
                $select->where("uc.company_id = {$company_id}");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }
	
	public function insert($data) {
		$data['user_device_type'] = $this->getUserDeviceType();
		$data['user_device_ip'] = $_SERVER['REMOTE_ADDR'];
        return parent::insert($data);
    }
	
	
	public function getUserDeviceType() {
		
		
		
        $tablet_browser = 0;
		$mobile_browser = 0;
		$device_type = 'web';
		 
		if (preg_match('/(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
			$tablet_browser++;
			
		}
		 
		if (preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android|iemobile)/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
			$mobile_browser++;
			
		}
		 
		if ((strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml') > 0) or ((isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE'])))) {
			$mobile_browser++;
		}
		 
		$mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'], 0, 4));
		$mobile_agents = array(
			'w3c ','acs-','alav','alca','amoi','audi','avan','benq','bird','blac',
			'blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno',
			'ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-',
			'maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-',
			'newt','noki','palm','pana','pant','phil','play','port','prox',
			'qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar',
			'sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-',
			'tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp',
			'wapr','webc','winw','winw','xda ','xda-');
		 
		if (in_array($mobile_ua,$mobile_agents)) {
			$mobile_browser++;
		}
		 
		if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'opera mini') > 0) {
			$mobile_browser++;
			//Check for tablets on opera mini alternative headers
			$stock_ua = strtolower(isset($_SERVER['HTTP_X_OPERAMINI_PHONE_UA'])?$_SERVER['HTTP_X_OPERAMINI_PHONE_UA']:(isset($_SERVER['HTTP_DEVICE_STOCK_UA'])?$_SERVER['HTTP_DEVICE_STOCK_UA']:''));
			if (preg_match('/(tablet|ipad|playbook)|(android(?!.*mobile))/i', $stock_ua)) {
			  $tablet_browser++;
			}
		}
		 
		if ($tablet_browser > 0) {
		   // do something for tablet devices
		   //print 'is tablet';
		   $device_type = 'tablet';
		   
		}
		else if ($mobile_browser > 0) {
		   // do something for mobile devices
		   //print 'is mobile';
		   $device_type = 'mobile';
		}
		else {
		   // do something for everything else
		   //print 'is desktop';
		   $device_type = 'web';
		}   
		
		return $device_type;
    }
	


}