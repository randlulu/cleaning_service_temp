<?php

class Model_Truncate extends Zend_Db_Table_Abstract {

    private $db_name;
    private $system_tables = array();

    public function init() {
        parent::init();
        
        //
        // set db_name
        //
        $dbConfig = $this->getAdapter()->getConfig();
        $this->db_name = $dbConfig['dbname'];
        
        //
        // set system_tables
        //
        $this->system_tables[] = 'auth_credential';
        $this->system_tables[] = 'auth_role';
        $this->system_tables[] = 'auth_role_credential';
        $this->system_tables[] = 'email_template';
        $this->system_tables[] = 'canned_responses';
        $this->system_tables[] = 'booking_status';
        $this->system_tables[] = 'customer_type';

        $this->system_tables[] = 'city';
        $this->system_tables[] = 'country';
        $this->system_tables[] = 'bank';
        $this->system_tables[] = 'product';
        $this->system_tables[] = 'company';

        $this->system_tables[] = 'service';
        $this->system_tables[] = 'service_attribute';
        $this->system_tables[] = 'service_attribute_value';
        $this->system_tables[] = 'attribute';
        $this->system_tables[] = 'attribute_list_value';
        $this->system_tables[] = 'attribute_type';

        $this->system_tables[] = 'complaint_type';
        $this->system_tables[] = 'inquiry_type';
        $this->system_tables[] = 'inquiry_property_type';
        $this->system_tables[] = 'inquiry_required_type';
        $this->system_tables[] = 'payment_type';
        $this->system_tables[] = 'due_date';
        $this->system_tables[] = 'label';
    }

    public function getExtraSystemTables($extras = array()) {

        if (in_array('users', $extras)) {
            $this->system_tables[] = 'user';
            $this->system_tables[] = 'user_company';
            $this->system_tables[] = 'user_info';
            $this->system_tables[] = 'contractor_info';
            $this->system_tables[] = 'vehicle';
            $this->system_tables[] = 'declaration_of_other_apparatus';
            $this->system_tables[] = 'declaration_of_equipment';
            $this->system_tables[] = 'declaration_of_chemicals';
            $this->system_tables[] = 'contractor_service_availability';
            $this->system_tables[] = 'contractor_service';
            $this->system_tables[] = 'contractor_owner';
            $this->system_tables[] = 'contractor_owner';
            $this->system_tables[] = 'contractor_gmail_accounts';
            $this->system_tables[] = 'contractor_employee';
        }
    }

    public function truncate_all_tables($extras = array()) {

        $this->getExtraSystemTables($extras);

        $tables = $this->getAdapter()->fetchAll("SHOW TABLES");
        
        foreach ($tables as $table) {
            $table_name = $table['Tables_in_' . $this->db_name];
            if (!in_array($table_name, $this->system_tables)) {
                $this->getAdapter()->query("TRUNCATE {$table_name}");
            }
        }
    }

}