<?php

class Model_InquiryService extends Zend_Db_Table_Abstract {

    protected $_name = 'inquiry_service';

    /*
     * updateById
     * @param int $id
     * @param array $data
     * @return boolean 
     */

    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "id = '{$id}'");
    }

    /*
     * deleteById
     * @param int $id
     * @return boolean 
     */

    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("id = '{$id}'");
    }

    /*
     * deleteByInquiryId
     * @param int $id
     * @return boolean 
     */

    public function deleteByInquiryId($id) {
        $id = (int) $id;
        return parent::delete("inquiry_id = '{$id}'");
    }

    /*
     * getByInquiryId
     * @param int $id
     * @return array 
     */

    public function getByInquiryId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("inquiry_id = '{$id}'");

        return $this->getAdapter()->fetchAll($select);
    }

    /*
     * setServicesToInquiry
     * @param int $inquiryID
     * @param array $services
     */

    public function setServicesToInquiry($inquiryID, $services=array()) {

        $inquiryID = (int) $inquiryID;
        $allServices = array();
        if (!empty($services)) {
            foreach ($services AS $service) {
                $serviceAndClone = explode('_', $service);

                $serviceId = (int) (isset($serviceAndClone[0]) ? $serviceAndClone[0] : 0);
                $clone = (int) (isset($serviceAndClone[1]) && $serviceAndClone[1] ? $serviceAndClone[1] : 0);

                $allServices[] = array(
                    'service_id' => $serviceId,
                    'clone' => $clone
                );
            }
        }
        //
        // delete Services not in the list
        //
        $this->deleteByInquiryIdAndServices($inquiryID, $allServices);

        //
        // assign the Services to inquiry
        //
        foreach ($allServices as $allService) {
            $this->assignServicesToInquiry(array('inquiry_id' => $inquiryID, 'service_id' => $allService['service_id'], 'clone' => $allService['clone']));
        }
    }

    /*
     * deleteByInquiryIdAndServices
     * @param int $inquiryID
     * @param array $services
     */

    public function deleteByInquiryIdAndServices($inquiryID, $services=array()) {
        $inquiryID = (int) $inquiryID;
        $oldServices = $this->getByInquiryId($inquiryID);
        $modelInquiryServiceAttributeValue = new Model_InquiryServiceAttributeValue();

        if (!empty($oldServices)) {
            foreach ($oldServices as &$oldService) {
                $serviceId = $oldService['service_id'];
                $clone = $oldService['clone'];

                $oldService = $serviceId . '_' . $clone;
            }
        }

        if (!empty($services)) {
            foreach ($services as &$service) {
                $serviceId = $service['service_id'];
                $clone = $service['clone'];

                $service = $serviceId . '_' . $clone;
            }
        }

        //
        //delete
        //
        $toDeleteServices = array_diff($oldServices, $services);
        if ($toDeleteServices) {
            foreach ($toDeleteServices as $toDeleteService) {

                $serviceAndClone = explode('_', $toDeleteService);
                $serviceId = (int) (isset($serviceAndClone[0]) ? $serviceAndClone[0] : 0);
                $clone = (int) (isset($serviceAndClone[1]) && $serviceAndClone[1] ? $serviceAndClone[1] : 0);

                //delete By Service And Inquiry And Clone
                $this->deleteByInquiryAndServiceAndClone($inquiryID, $serviceId, $clone);
                // Delete Attribute Value
                $modelInquiryServiceAttributeValue->deleteByInquiryIdServiceIdAndClone($inquiryID, $serviceId, $clone);
            }
        }
    }

    /*
     * deleteByInquiryAndServiceAndClone
     * @param int $inquiryID
     * @param array $services_id
     * @param int $clone
     * @return boolean
     */

    public function deleteByInquiryAndServiceAndClone($inquiryID, $service_id, $clone) {
        $inquiryID = (int) $inquiryID;
        $service_id = (int) $service_id;
        $clone = (int) $clone;

        return $this->delete("service_id='{$service_id}' AND inquiry_id='{$inquiryID}' AND clone='{$clone}'");
    }

    /*
     * assignServicesToInquiry
     * @param array $inquiryID
     * @return int
     */

    public function assignServicesToInquiry($params) {

        $inquiryServicelink = $this->getByInquiryAndServiceAndClone($params['inquiry_id'], $params['service_id'], $params['clone']);

        if (!$inquiryServicelink) {
            $returnId = $this->insert($params);
        } else {
            $this->updateById($inquiryServicelink['id'], $params);
            $returnId = $inquiryServicelink['id'];
        }

        $modelInquiryServiceAttributeValue = new Model_InquiryServiceAttributeValue();
        $modelInquiryServiceAttributeValue->addAttributeByInquiryIdAndServiceIdAndClone($params['inquiry_id'], $params['service_id'], $params['clone']);
        return $returnId;
    }

    /*
     * getByInquiryAndServiceAndClone
     * @param int $inquiryId
     * @param int $service_id
     * @param int $clone
     * @return array 
     */

    public function getByInquiryAndServiceAndClone($inquiryId, $service_id, $clone) {
        $inquiryId = (int) $inquiryId;
        $service_id = (int) $service_id;
        $clone = (int) $clone;

        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("service_id='{$service_id}' AND inquiry_id='{$inquiryId}' AND clone='{$clone}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /*
     * getByInquiryId
     * @param int $id
     * @return array 
     */

    public function getByInquiryIdAsText($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("inquiry_id = '{$id}'");

        $services = $this->getAdapter()->fetchAll($select);

        $serviecsAsText = "";

        if ($services) {

            $modelServices = new Model_Services();
            foreach ($services as $service) {

                $serviceId = isset($service['service_id']) ? $service['service_id'] : 0;

                //get the service
                $serviceById = $modelServices->getById($serviceId);
                $serviecsAsText .= 'Service : ' . $serviceById['service_name'] . "<br>";
            }
        }

        return $serviecsAsText;
    }

}