<?php

class Model_MailingList extends Zend_Db_Table_Abstract {

    protected $_name = 'mailing_list';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    
	 public function getAll($filters = array(), $order = null, &$pager = null) {
        

		$select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order($order);


        if ($filters) {
            if (!empty($filters['keywords'])) {
                $keywords = $this->getAdapter()->quote('%' . $filters['keywords'] . '%');
                $select->where("name LIKE {$keywords}");
            }
			if (!empty($filters['company_id'])) {
                $companyId = $filters['company_id'];	
                $select->where("company_id = {$companyId}");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

		
		
        return $this->getAdapter()->fetchAll($select);
    }
	
	 public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "mailing_list_id = '{$id}'");
    }
	
	public function getAllMailingListAsArray($companyId = null){
	  
	  if(isset($companyId)){
	    $filters = array('company_id'=>$companyId); 
	  }else{
	   $companyId = CheckAuth::getCompanySession();
	   $filters = array('company_id'=>$companyId);
	  }
	  
	  $mailingLists = $this->getAll($filters);
	  
	  $data = array();
        foreach ($mailingLists as $mailingList) {
                $data[$mailingList['mailing_list_id']] = $mailingList['name'];
            }
        return $data;
		
	
	}

    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("mailing_list_id = '{$id}'");
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("mailing_list_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }
     public function getByName($name) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("name = '{$name}'");

        return $this->getAdapter()->fetchRow($select);
    }
	
	
	

}
