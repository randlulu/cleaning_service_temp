<?php

class Model_EmailTemplate extends Zend_Db_Table_Abstract {

    protected $_name = 'email_template';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order($order);

        if ($filters) {
            if (!empty($filters['keywords'])) {
                $keywords = $this->getAdapter()->quote('%' . $filters['keywords'] . '%');
                $select->where("name LIKE {$keywords}");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table rows according to the assigned id and data
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "id = '{$id}'");
    }

    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("id = '{$id}'");
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the name
     * 
     * @param string $name
     * @return array 
     */
    public function getByName($name) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("name = '{$name}'");

        return $this->getAdapter()->fetchRow($select);
    }
/** 22/6/2015 Commented By Salim 
    public function getEmailTemplate($template_name = '', $template_params = array(), $companyId = 0) {
       
        $placeholder = array();
        foreach ($template_params as $key => $value) {
            $placeholder[] = $key;
        }

        $template = $this->getByName($template_name);
        if (!$template) {
            $db_params = array();
            $db_params['subject'] = $template_name;
            $db_params['body'] = $template_name . "<br><br>" . implode("<br>", $placeholder);
            $db_params['name'] = $template_name;
            $db_params['placeholder'] = implode(',', $placeholder);

            if ($template_name) {
                $this->insert($db_params);
            }
        } else {
            $db_params = array();
            $db_params['placeholder'] = implode(',', $placeholder);

            if ($template_name) {
                $this->updateById($template['id'],$db_params);
            }
        }
       
        
        $loggedUser = CheckAuth::getLoggedUser();
        $general_placeholders = get_config('general_placeholders', array('email_notification'))->toArray();
        if ($general_placeholders) {

            if (!$companyId) {
                $companyId = CheckAuth::getCompanySession();
            }

            $modelCompanies = new Model_Companies();
            $company = $modelCompanies->getById($companyId);

            $general_placeholders['{company_name}'] = isset($company['company_name']) && $company['company_name'] ? $company['company_name'] : 'Company Name';
            $general_placeholders['{company_website}'] = isset($company['company_website']) && $company['company_website'] ? $company['company_website'] : 'Company Website';
            $general_placeholders['{company_enquiries_email}'] = isset($company['company_enquiries_email']) && $company['company_enquiries_email'] ? $company['company_enquiries_email'] : 'Company Enquiries Email';
            $general_placeholders['{send_date}'] = date('F j, Y, g:i a', time());
            $general_placeholders['{sender_name}'] = isset($loggedUser['display_name']) ? $loggedUser['display_name'] : '';

            if(empty($general_placeholders['{sender_name}'])){
                $general_placeholders['{sender_name}'] = isset($template_params['{sender_name}']) ? $template_params['{sender_name}'] : '';
            }
            
            $template_params = array_merge($general_placeholders, $template_params);
        }

        $body = '';
        $subject = '';
        if ($template_name) {
            $template = $this->getByName($template_name);
            if ($template) {
                $subject = $template['subject'];
                $body = $template['body'];
                foreach ($template_params as $key => $value) {
                    $subject = str_replace($key, $value, $subject);
                    $body = str_replace($key, $value, $body);
                }
            }
        }

        return array('body' => $body, 'subject' => $subject);
    }
	*/
	public function getEmailTemplate($template_name = '', $template_params = array(), $companyId = 0) {
        /**
         * START TMP CODE
         */
        $placeholder = array();
        $template_images=array();
        foreach ($template_params as $key => $value) {
            $placeholder[] = $key;
        }

        $template = $this->getByName($template_name);
        if (!$template) {
            $db_params = array();
            $db_params['subject'] = $template_name;
            $db_params['body'] = $template_name . "<br><br>" . implode("<br>", $placeholder);
            $db_params['name'] = $template_name;
            $db_params['placeholder'] = implode(',', $placeholder);
            if ($template_name) {
                $this->insert($db_params);
            }
        } else {
            $db_params = array();
            $db_params['placeholder'] = implode(',', $placeholder);
            if ($template_name) {
                $this->updateById($template['id'],$db_params);
            }
        }

        /**
         * END TMP CODE
         */
        $loggedUser = CheckAuth::getLoggedUser();
        $general_placeholders = get_config('general_placeholders', array('email_notification'))->toArray();
        if ($general_placeholders) {

           if (!$companyId) {
               $companyId = CheckAuth::getCompanySession();
           }

            $modelCompanies = new Model_Companies();
            $company = $modelCompanies->getById($companyId);

            $modelTradingName = new Model_TradingName();
            $tradingName = $modelTradingName->getById($template['trading_name_id']);

            $general_placeholders['{trading_name}'] = isset($tradingName['trading_name']) && $tradingName['trading_name'] ? $tradingName['trading_name'] : 'Trading Name';
            $general_placeholders['{website_url}'] = isset($tradingName['website_url']) && $tradingName['website_url'] ? $tradingName['website_url'] : 'Website URL';
            $general_placeholders['{phone}'] = isset($tradingName['phone']) && $tradingName['phone'] ? $tradingName['phone'] : 'Phone';
            $general_placeholders['{email}'] = isset($tradingName['email']) && $tradingName['email'] ? $tradingName['email'] : 'Email';
            $general_placeholders['{color}'] = isset($tradingName['color']) && $tradingName['color'] ? $tradingName['color'] : 'color';

            //D.A 16/06/2015
            $trading_name_imgs=array();
            $imgs_name_part=array();

            $trading_namesImagesobj = new Model_TradingNameImages();
            $trading_namesImages = $trading_namesImagesobj->getByTradingnameIdandTemplateId($template['trading_name_id'],$template['id']);

            if($trading_namesImages){
                foreach ($trading_namesImages as $trading_namesImage) {
                    $trading_name_imgs[]= $trading_namesImage['trading_name_img'];
                    $imgname=explode('.', $trading_namesImage['trading_name_img']);
                    $imgs_name_part[]= $imgname[0];
                }

                $modelEmailTemplate = new Model_EmailTemplate();
                $emailTemplate = $modelEmailTemplate->getById($template['id']);

                $imagePlaceholders=explode(',', $template['imagePlaceholder']);
                if($imagePlaceholders){
                    foreach ($imagePlaceholders as $imagePlaceholder) {
                        $index = array_search($imagePlaceholder, $imgs_name_part);
                            $template_images[$imagePlaceholder]='http://cm.tilecleaners.com.au/uploads/trading_names_img/' . $tradingName['trading_name'].'/'.$emailTemplate['name'] . '/' . $trading_name_imgs[$index];
                    }
                }
            }


            $general_placeholders['{company_name}'] = isset($company['company_name']) && $company['company_name'] ? $company['company_name'] : 'Company Name';
            $general_placeholders['{company_website}'] = isset($company['company_website']) && $company['company_website'] ? $company['company_website'] : 'Company Website';
            $general_placeholders['{company_enquiries_email}'] = isset($company['company_enquiries_email']) && $company['company_enquiries_email'] ? $company['company_enquiries_email'] : 'Company Enquiries Email';

            $general_placeholders['{send_date}'] = date('F j, Y, g:i a', time());
            $general_placeholders['{sender_name}'] = isset($loggedUser['display_name']) ? $loggedUser['display_name'] : '';

            if(empty($general_placeholders['{sender_name}'])){
                $general_placeholders['{sender_name}'] = isset($template_params['{sender_name}']) ? $template_params['{sender_name}'] : '';
            }
            $template_params = array_merge($general_placeholders, $template_params,$template_images);
        }

        $body = '';
        $subject = '';
        if ($template_name) {
            $template = $this->getByName($template_name);
            if ($template) {
                $subject = $template['subject'];
                $body = $template['body'];
                foreach ($template_params as $key => $value) {
                    $subject = str_replace($key, $value, $subject);
                    $body = str_replace($key, $value, $body);
                }
            }
        }

        return array('body' => $body, 'subject' => $subject);
    }

}