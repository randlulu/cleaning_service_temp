<?php

class Model_GoogleCalendarEvent extends Zend_Db_Table_Abstract {

    protected $_name = 'google_calendar_event';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order($order);

        if ($filters) {
            if (!empty($filters['not_full_event'])) {
                $select->where("full_event = 0");
            }
            if (!empty($filters['booking_id'])) {
                $select->where("booking_id = {$filters['booking_id']}");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }


        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table rows according to the assigned id and data
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "id = '{$id}'");
    }

    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("id = '{$id}'");
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the id
     * 
     * @param int $bookingId
     * @param int $contractorId
     * @return array 
     */
    public function getByBookingIdContractorIdAndMultipleDayId($bookingId, $contractorId, $multipleDayId = 0) {
        $bookingId = (int) $bookingId;
        $contractorId = (int) $contractorId;
        $multipleDayId = (int) $multipleDayId;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_id = '{$bookingId}'");
        $select->where("contractor_id = '{$contractorId}'");
        $select->where("multiple_day_id = '{$multipleDayId}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the id
     * 
     * @param int $bookingId
     * @param int $contractorId
     * @return array 
     */
    public function getByBookingIdAndContractorId($bookingId, $contractorId) {
        $bookingId = (int) $bookingId;
        $contractorId = (int) $contractorId;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_id = '{$bookingId}'");
        $select->where("contractor_id = '{$contractorId}'");

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * get table row according to the id
     * 
     * @param int $bookingId
     * @param int $contractorId
     * @return array 
     */
    public function getByBookingId($bookingId) {
        $bookingId = (int) $bookingId;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_id = '{$bookingId}'");

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * get table row according to the id
     * 
     * @param int $bookingId
     * @param int $contractorId
     * @return array 
     */
    public function getMultipleEventByBookingId($bookingId, $contractorId) {
        $bookingId = (int) $bookingId;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_id = '{$bookingId}'");
        $select->where("contractor_id = '{$contractorId}'");
        $select->where("multiple_day_id != '0'");

        return $this->getAdapter()->fetchAll($select);
    }

    public function getClientGmailAccounts($contractorId, &$client) {
        // load model
        $modelContractorGmailAccounts = new Model_ContractorGmailAccounts();

        // get the gmail account contractor
        $contractorAccount = $modelContractorGmailAccounts->getByContractorId($contractorId);
        $email = isset($contractorAccount['email']) ? $contractorAccount['email'] : '';
        $password = isset($contractorAccount['password']) ? $contractorAccount['password'] : '';

        $isValid = false;
        if ($email && $password) {
            $isValid = true;
            try {
                // Parameters for ClientAuth authentication
                $authenticationServiceName = GdataCalendar::AUTH_SERVICE_NAME;

                // Create an authenticated HTTP client
                $client = Zend_Gdata_ClientLogin::getHttpClient($email, $password, $authenticationServiceName);
            } catch (Zend_Gdata_App_Exception $ae) {
                $isValid = false;
            }
        }

        return $isValid;
    }

    public function deleteGoogleCalendarEventByBookingId($bookingId) {

        // load model
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();

        $contractorServicesBooking = $modelContractorServiceBooking->getByBookingId($bookingId);

        $contractorIds = array();
        foreach ($contractorServicesBooking as $contractorServiceBooking) {
            $contractorIds[$contractorServiceBooking['contractor_id']] = $contractorServiceBooking['contractor_id'];
        }

        foreach ($contractorIds as $contractorId) {
            $this->deleteGoogleCalendarEventByBookingIdAndContractorId($bookingId, $contractorId);
        }

        //get general contractor
        $general = CheckAuth::getGeneralContractor();

        //delete Event from general contractor
        $this->deleteGoogleCalendarEventByBookingIdAndContractorId($bookingId, $general['user_id']);
    }

    public function deleteGoogleCalendarEventByBookingIdAndContractorId($bookingId, $contractorId) {

        //$client = null;
        //$isValid = $this->getClientGmailAccounts($contractorId, $client);

        //if ($isValid) {

            // get the calendar event id
            $googleCalendarEvents = $this->getByBookingIdAndContractorId($bookingId, $contractorId);

            // delete the event
            if ($googleCalendarEvents) {
                foreach ($googleCalendarEvents as $googleCalendarEvent) {
                    /*$gdataCal = new GdataCalendar($client);
                    $gdataCal->deleteEvent($client, $googleCalendarEvent['google_calendar_event_id']);*/
					$calendarServiceAccount = new Model_CalendarServiceAccount($googleCalendarEvent['contractor_id']);
					$calendarServiceAccount->deleteEvent($googleCalendarEvent['google_calendar_event_id']);
				

                    $this->deleteById($googleCalendarEvent['id']);
                }
            }
        //}
    }

    public function deleteGoogleCalendarEventByBookingIdContractorIdAndMultipleDaysId($bookingId, $contractorId, $multipleDayId = 0) {

            // get the calendar event id
            $googleCalendarEvent = $this->getByBookingIdContractorIdAndMultipleDayId($bookingId, $contractorId, $multipleDayId);

            // delete the event
            if ($googleCalendarEvent) {
                
				$calendarServiceAccount = new Model_CalendarServiceAccount($googleCalendarEvent['contractor_id']);
				$calendarServiceAccount->deleteEvent($googleCalendarEvent['google_calendar_event_id']);
				
                $this->deleteById($googleCalendarEvent['id']);
            }
       
    }

    public function sendCreatedBooking($bookingId) {

        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $contractorServiceBookings = $modelContractorServiceBooking->getByBookingId($bookingId);

        if ($contractorServiceBookings) {

            $allServices = array();
            $contractorServices = array();
            $contractorIds = array();

            foreach ($contractorServiceBookings AS $contractorServiceBooking) {
                //to send all service to general contractor
                $allServices[] = array(
                    'service_id' => $contractorServiceBooking['service_id'],
                    'clone' => $contractorServiceBooking['clone']
                );
                //to send for contractor only his services
                $contractorServices[$contractorServiceBooking['contractor_id']][] = array(
                    'service_id' => $contractorServiceBooking['service_id'],
                    'clone' => $contractorServiceBooking['clone']
                );
                //list of contractor assind to this booking
                $contractorIds[] = $contractorServiceBooking['contractor_id'];
            }

            //get general contractor
            $general = CheckAuth::getGeneralContractor();

            //delete all event if contractor removed from booking
            $googleCalendarEvents = $this->getByBookingId($bookingId);
            foreach ($googleCalendarEvents AS $event) {
                if (!in_array($event['contractor_id'], $contractorIds)) {
                    $this->deleteGoogleCalendarEventByBookingIdAndContractorId($event['booking_id'], $event['contractor_id']);
                }
            }

            //send to contractor his services
            foreach ($contractorServices AS $contractorId => $services) {
                if ($contractorId != $general['user_id']) {
                    $this->sendBookingToGmailAcc($bookingId, $services, $contractorId);
                }
            }

            //send all services to general contractor 
            $this->sendBookingToGmailAcc($bookingId, $allServices, $general['user_id']);
        }
    }

    public function sendBookingToGmailAcc($bookingId, $services, $contractorId = 0) {
        //
        //get logged user
        //
        if (!$contractorId) {
            $loggedUser = CheckAuth::getLoggedUser();
            $contractorId = $loggedUser['user_id'];
        }

        if (!is_array($services)) {
            exit;
        }

        // check if Contractor Accept Booking to send customer contact or not
        $fullEvent = false;
        $modelBooking = new Model_Booking();
        if ($modelBooking->checkIfContractorAcceptBooking($bookingId, $contractorId, true)) {
            $fullEvent = true;
        }

        
            $calendarServiceAccount = new Model_CalendarServiceAccount($contractorId);
            
            // put all booking date in events array
            $events = array();

            $modelBookingMultipleDays = new Model_BookingMultipleDays();
            $multipleDays = $modelBookingMultipleDays->getByBookingId($bookingId);

            // in first key the default booking date
            $events[] = array(
                'event' => $this->createdEvent($calendarServiceAccount, $bookingId, $services, $fullEvent, '', $multipleDays),
                'day_id' => 0
            );

            //get all event day if booking have Multiple Days 
            $booking = $modelBooking->getById($bookingId);
            if (!empty($multipleDays)) {
                foreach ($multipleDays as $key => $day) {
                    $remainingMultipleDays = array_diff_key($multipleDays, array($key => $day));
                    array_unshift($remainingMultipleDays, array('booking_start' => $booking['booking_start'], 'booking_end' => $booking['booking_end']));
                    $events[] = array(
                        'event' => $this->createdEvent($calendarServiceAccount, $bookingId, $services, $fullEvent, $day, $remainingMultipleDays),
                        'day_id' => $day['id']
                    );
                }
            }

            // delete all Multiple event from google calender 
            $googleCalendarEvents = $this->getMultipleEventByBookingId($bookingId, $contractorId);
            foreach ($googleCalendarEvents as $event) {
                $this->deleteGoogleCalendarEventByBookingIdContractorIdAndMultipleDaysId($bookingId, $contractorId, $event['multiple_day_id']);
            }

            foreach ($events as $event) {

                $googleCalendarEvent = $this->getByBookingIdContractorIdAndMultipleDayId($bookingId, $contractorId, $event['day_id']);

                if ($googleCalendarEvent) {

                    $oldEvent = $calendarServiceAccount->updateEvent($googleCalendarEvent['google_calendar_event_id'], $event['event']);

                    if (!$oldEvent) {
                        $createdEvent = $calendarServiceAccount->insertEvent($event['event']);
                        $createdEventId = $createdEvent;

                        $db_params = array(
                            'booking_id' => $bookingId,
                            'contractor_id' => $contractorId,
                            'google_calendar_event_id' => $createdEventId,
                            'full_event' => $fullEvent,
                            'multiple_day_id' => $event['day_id']
                        );

                        $this->updateById($googleCalendarEvent['id'], $db_params);
                    } else {
                        $this->updateById($googleCalendarEvent['id'], array('full_event' => $fullEvent));
                    }
                } else {
                    // Upload the event to the calendar server
                    // A copy of the event as it is recorded on the server is returned
                    $createdEvent = $calendarServiceAccount->insertEvent($event['event']);
                    $createdEventId = $createdEvent;

                    $db_params = array(
                        'booking_id' => $bookingId,
                        'contractor_id' => $contractorId,
                        'google_calendar_event_id' => $createdEventId,
                        'full_event' => $fullEvent,
                        'multiple_day_id' => $event['day_id']
                    );

                    $this->insert($db_params);
                }
            }
       
    }

    public function createdEvent($gdataCal, $bookingId, $services, $fullEvent = true, $day = '', $multipleDays=array()) {

        //
        // load model
        //
        $modelUser = new Model_User();
        $modelBooking = new Model_Booking();
        $modelCustomer = new Model_Customer();
        $modelBookingAddress = new Model_BookingAddress();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();

        $sub_total = 0;
        foreach ($services as $service) {

            $serviceId = isset($service['service_id']) ? $service['service_id'] : 0;
            $clone = isset($service['clone']) ? $service['clone'] : 0;

            $sub_total = $sub_total + $modelContractorServiceBooking->getTotalServiceBookingQoute($bookingId, $serviceId, $clone);
        }

        //
        // get Booking Address
        //
        $bookingAddress = $modelBookingAddress->getByBookingId($bookingId);

        if ($fullEvent) {
            $address = get_line_address($bookingAddress);
        } else {
            $suburb = isset($bookingAddress['suburb']) ? $bookingAddress['suburb'] : '';
            $state = isset($bookingAddress['state']) ? strtoupper($bookingAddress['state']) : '';
            $postcode = isset($bookingAddress['postcode']) ? $bookingAddress['postcode'] : '';

            $address = trim("{$suburb} {$state} {$postcode}");
        }

        //
        // get booking information
        //
        $booking = $modelBooking->getById($bookingId);
        $modelBooking->fill($booking, array('property_type', 'status'));

        //
        // filling extra data
        //
        $customer = $modelCustomer->getById($booking['customer_id']);
        $user = $modelUser->getById($booking['created_by']);

        // Booking Start Params
        $bookingStartParams = date("F j, Y, g:i a", strtotime($booking['booking_start']));
        if (!empty($day)) {
            $bookingStartParams = date("F j, Y, g:i a", strtotime($day['booking_start']));
        }

        $template_params = array(
            //booking
            '{booking_num}' => $booking['booking_num'],
            '{total_without_tax}' => number_format($booking['sub_total'], 2),
            '{gst_tax}' => number_format($booking['gst'], 2),
            '{total_with_tax}' => number_format($booking['qoute'], 2),
            '{description}' => $booking['description'] ? $booking['description'] : '',
            '{booking_created}' => date('d/m/Y', $booking['created']),
            '{booking_created_by}' => ucwords($user['username']),
            '{booking_start}' => $bookingStartParams,
            '{service}' => nl2br($modelContractorServiceBooking->getBookingAsText($bookingId, true, $services)),
            '{property}' => $booking['property_type'],
            '{multipleDays}' => nl2br($modelBooking->getMultipleTimeAsString($multipleDays))
        );

        if ($fullEvent) {
            $template_params['{booking_address}'] = get_line_address($modelBookingAddress->getByBookingId($booking['booking_id']));
            $template_params['{customer_name}'] = get_customer_name($customer);
            $template_params['{customer_first_name}'] = isset($customer['first_name']) && $customer['first_name'] ? ucwords($customer['first_name']) : '';
            $template_params['{customer_last_name}'] = isset($customer['last_name']) && $customer['last_name'] ? ' ' . ucwords($customer['last_name']) : '';
            $template_params['{customer_contacts}'] = nl2br($modelCustomer->getCustomerContacts($booking['customer_id']));

            $template_name = 'push_to_google_calendar_with_customer_contacts';
        } else {
            $template_name = 'push_to_google_calendar_without_customer_contacts';
        }

        $modelEmailTemplate = new Model_EmailTemplate();
        $emailTemplate = $modelEmailTemplate->getEmailTemplate($template_name, $template_params);


        $body = strip_tags(br2nl($emailTemplate['body']));
        $body = str_replace('&nbsp;', ' ', $body);
        $body = str_replace('&amp;', '&', $body);
        $body = trim($body);


        $title = $booking['status']['name'] . ' - ' . $booking['title'];


        // Set the date using RFC 3339 format.
        if ($day) {
            $start = strtotime($day['booking_start']);
            $end = strtotime($day['booking_end']);
        } else {
            $start = strtotime($booking['booking_start']);
            $end = strtotime($booking['booking_end']);
        }

        $startDate = date('Y-m-d', $start);
        $startTime = date('H:i', $start);
        $endDate = date('Y-m-d', $end);
        $endTime = date('H:i', $end);
        $tzOffset = TIMEZONE;
		
		if($start == $end){
			$endTime = '12:00' ;
		
		}
		
		/*$when = $gdataCal->newWhen();
        $when->startTime = "{$startDate}T{$startTime}:00.000{$tzOffset}:00";
        $when->endTime = "{$endDate}T{$endTime}:00.000{$tzOffset}:00";
		*/
        //
        // Creating Events, this is old way by gdata, now we should create 
        //
        /*$event = $gdataCal->newEventEntry();
        $event->title = $gdataCal->newTitle($title);
        $event->content = $gdataCal->newContent($body);
        $event->where = array($gdataCal->newWhere($address));
        $event->when = array($when);*/
		
		///her the new way
		$event = new Google_Service_Calendar_Event();		
		$event->setDescription($body);
		$event->setSummary($title);
		
		$event->setLocation($address);
		$start = new Google_Service_Calendar_EventDateTime();
		$start->setTimeZone('Australia/Sydney');
		//$start->setDateTime('2014-11-23T11:00:00');
		$start->setDateTime("{$startDate}T{$startTime}:00");
		$event->setStart($start);
		
		$end = new Google_Service_Calendar_EventDateTime();
		$end->setTimeZone('Australia/Sydney');
		//$end->setDateTime('2014-11-23T11:00:00');  //2014-11-25T00:26:00
		$end->setDateTime("{$endDate}T{$endTime}:00");
		$event->setEnd($end);
		/*$attendee1 = new Google_Service_Calendar_EventAttendee();
		$attendee1->setEmail('abusalem.islam1988@gmail.com');
		$event->attendees = $attendee1;*/
		
        return $event;
    }

    public function cronJobCheckGmailBookingFullEvent() {

        $modelBooking = new Model_Booking();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $notFullEvents = $this->getAll($filters = array('not_full_event' => 1));

        foreach ($notFullEvents as $notFullEvent) {
            $bookingId = $notFullEvent['booking_id'];
            $contractorId = $notFullEvent['contractor_id'];

            $timePeriod = $modelBooking->checkContractorTimePeriod($bookingId, $contractorId);
            if ($timePeriod) {
                $services = $modelContractorServiceBooking->getByBookingIdAndContractorId($bookingId, $contractorId);
                $this->sendBookingToGmailAcc($bookingId, $services, $contractorId);
            }
        }
    }

    public function getGoogleEventByContractorIdAndDateRange($contractorId, $start, $end = '') {

        $client = null;
        $isValid = $this->getClientGmailAccounts($contractorId, $client);

        if (!$end) {
            $end = date('Y-m-d 23:59:59', strtotime($start));
        }

        $start = date('Y-m-d H:i:s', strtotime($start));
        $end = date('Y-m-d H:i:s', strtotime($end));

        $googleEvent = array();

        if ($isValid) {
            $gdataCal = new GdataCalendar($client);
			
            $eventFeed = $gdataCal->getEventByDateRange($client, $start, $end);
            if($eventFeed)
			{
				foreach ($eventFeed as $key => $event) {

					$googleEvent[$key]['title'] = $event->title->text;
					$googleEvent[$key]['booking_id'] = sha1('google');

					foreach ($event->when as $when) {
						$googleEvent[$key]['booking_start'] = date('Y-m-d H:i:s', strtotime($when->startTime));
						$googleEvent[$key]['booking_end'] = date('Y-m-d H:i:s', strtotime($when->endTime));
					}
				}
		    }
        }

        return $googleEvent;
    }

}
