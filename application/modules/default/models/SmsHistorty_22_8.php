<?php
class Model_SmsHistorty extends Zend_Db_Table_Abstract {

    protected $_name = 'sms_history';
	public function getAll($filters = array(), $order = null, &$pager = null, $limit = 0) {
        $select = $this->getAdapter()->select();
        $select->from(array('el' => $this->_name));
		
		if ($order) {
            $select->order($order);
        }
		  if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        } elseif ($limit) {
            $select->limit($limit);
        } elseif ($perPage && $currentPage) {
            $select->limit($perPage, ($currentPage - 1) * $perPage);
        }

        if ($filters) {
            if (!empty($filters['reference_id'])) {
                $reference_id = $this->getAdapter()->quote($filters['reference_id']);
                $select->where("el.reference_id = {$reference_id}");
            }
			
			 if (!empty($filters['sms_type'])) {
                $sms_type = $this->getAdapter()->quote($filters['sms_type']);
                $select->where("el.sms_type = {$sms_type}");
            }
			
			 if (!empty($filters['sms_id'])) {
                $sms_id = $this->getAdapter()->quote($filters['sms_id']);
                $select->where("el.id = {$sms_id}");
            }
		
			
			if (!empty($filters['mobileNum'])) {
				
                $mobile_no = $this->getAdapter()->quote($filters['mobileNum']);
				
                $select->where("el.from = {$mobile_no} or el.to = {$mobile_no} ");
            }
			
			
			
        }

        return $this->getAdapter()->fetchAll($select);
    }
    /**
     * update table rows according to the assigned id and data
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
		
        return parent::update($data, "id = '{$id}'");
    }

    public function insert($data) {
        return parent::insert($data);
    }
    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("id = '{$id}'");
    }
    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id = '{$id}'");
        return $this->getAdapter()->fetchRow($select);
    }

    public function getByReferenceIdAndCronjobHistoryId($ref_id, $history_id) {

        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("reference_id = {$ref_id}");
        $select->where("cronjob_history_id = {$history_id}");
        
        return $this->getAdapter()->fetchAll($select);
    }
	
	public function IsParent($id) {
			$id = (int) $id;
			$select = $this->getAdapter()->select();
			$select->from($this->_name);
			$select->where("parent_id = '{$id}'");
			$rows=$this->getAdapter()->fetchAll($select);
			if(count($rows)>0)
				return true;
			else
				return false;
				
			
		}
		
		public function getAllChild($id) {
			$id = (int) $id;
			$select = $this->getAdapter()->select();
			$select->from($this->_name);
			$select->where("parent_id = '{$id}'");
			return $this->getAdapter()->fetchAll($select);
		
		}
		
		public function getMobileFormat($to)
		{
			if (strpos($to, '+') !== false)
				{
				 $mobile_no = ltrim($to, '+');
				 $finalFormat="00".$mobile_no;
				  return $finalFormat;
				}
				else
				{
				  $mobile_no = substr($to, 2);
				  $finalFormat="+".$mobile_no;
                  if(strlen($to)<13 ){
					 $finalFormat="";
					 return 0;
					 }
					else   
				  return $finalFormat;
				}
	
			
		/*	$mobile_format = array(
			"0061" =>"+61",
			"0093" =>"+93",
			"00355" => "+355",
			"00213" => "+213",
			"001" => "+1",
			"00376" => "+376",
			"00244" => "+244",
			"001" => "+1",
			"001" => "+1",
			"0054" =>"+54",
			"00374" =>"+374",
			"00297" =>"+297",
			"0043" => "+43",
			"00994" =>"+994",
			"00973" =>"+973",
			"00880" =>"+880",
			"001" =>"+1",
			"00375" =>"+375",
			"0032" => "+32",
			"00501" => "+501",
			"00229" => "+229",
			"001" => "+1",
			"00975" => "+975",
			"00591" => "+591",
			"00387" => "+387",
			"00267" => "+267",
			"0055" => "+55",
			"00246" =>"+246",
			"001" => "+1",
			"00673" => "+673",
			"00359" => "+359",
			"00226" => "+226",
			"0095" => "+95",
			"00257" =>"+257",
			"00855" =>"+855",
			"00237" =>"+237",
			"001" => "+1",
			"00238" => "+238",
			"001" => "+1",
			"00236" =>"+236",
			"00235" =>"+235",
			"0056" => "+56",
			"0086" => "+86",
			"0057" => "+57",
			"00269" =>"+269",
			"00682" =>"+682",
			"00506" =>"+506",
			"00225" =>"+225",
			"00385" =>"+385",
			"0053" => "+53",
			"00357" =>"+357",
			"00420" =>"+420",
			"00243" =>"+243",
			"0045" => "+45",
			"00253" =>"+253",
			"001" => "+1",
			"001" => "+1",
			"00593" =>"+593",
			"0020" => "+20",
			"00503" => "+503",
			"00240" => "+240",
			"00291" => "+291",
			"00372" => "+372",
			"00251" => "+251",
			"00500" => "+500",
			"00298" => "+298",
			"00691" => "+691",
			"00679" => "+679",
			"00358" => "+358",
			"0033" => "+33",
			"00594" =>"+594",
			"00689" =>"+689",
			"00241" =>"+241",
			"00995" =>"+995",
			"0049" => "+49",
			"00233" =>"+233",
			"00350" =>"+350",
			"0030" => "+30",
			"00299" =>"+299",
			"001" => "+1",
			"00590" =>"+590",
			"001" =>"+1",
			"00502" =>"+502",
			"00224" =>"+224",
			"00245" =>"+245",
			"00592" =>"+592",
			"00509" =>"+509",
			"00504" =>"+504",
			"00852" =>"+852",
			"0036" => "+36",
			"00354" =>"+354",
			"0091" => "+91",
			"0062" => "+62",
			"0098" => "+98",
			"00964" =>"+964",
			"00353" =>"+353",
			"00972" =>"+972",
			"0039" => "+39",
			"001" => "+1",
			"0081" =>"+81",
			"00962" =>"+962",
			"007" => "+7",
			"00254" => "+254",
			"00686" => "+686",
			"00381" => "+381",
			"00965" => "+965",
			"00996" => "+996",
			"00856" => "+856",
			"00371" => "+371",
			"00961" => "+961",
			"00266" => "+266",
			"00231" => "+231",
			"00218" => "+218",
			"00423" => "+423",
			"00370" => "+370",
			"00352" => "+352",
			"00853" => "+853",
			"00389" => "+389",
			"00261" => "+261",
			"00265" => "+265",
			"0060" => "+60",
			"00960" =>"+960",
			"00223" =>"+223",
			"00356" =>"+356",
			"00692" =>"+692",
			"00596" =>"+596",
			"00222" =>"+222",
			"00230" =>"+230",
			"00262" =>"+262",
			"0052" => "+52",
			"00373" =>"+373",
			"00377" =>"+377",
			"00976" =>"+976",
			"00382" =>"+382",
			"001" => "+1",
			"00212" => "+212",
			"00258" => "+258",
			"00264" => "+264",
			"00674" => "+674",
			"00977" => "+977",
			"0031" => "+31",
			"00599" => "+599",
			"00687" => "+687",
			"0064" => "+64",
			"00505" =>"+505",
			"00227" =>"+227",
			"00234" =>"+234",
			"00683" =>"+683",
			"00672" =>"+672",
			"00850" =>"+850",
			"001" => "+1",
			"0047" =>"+47",
			"00968" =>"+968",
			"0092" => "+92",
			"00680" =>"+680",
			"00970" =>"+970",
			"00507" =>"+507",
			"00675" =>"+675",
			"00595" =>"+595",
			"0051" => "+51",
			"0063" => "+63",
			"0048" => "+48",
			"00351" =>"+351",
			"001" => "+1",
			"00974" => "+974",
			"00242" => "+242",
			"00262" => "+262",
			"0040" => "+40",
			"007" => "+7",
			"00250" => "+250",
			"00590" => "+590",
			"00290" => "+290",
			"001" => "+1",
			"00590" => "+590",
			"00508" => "+508",
			"001" => "+1",
			"00685" => "+685",
			"00378" => "+378",
			"00239" => "+239",
			"00966" => "+966",
			"00221" => "+221",
			"00381" => "+381",
			"00248" => "+248",
			"00232" => "+232",
			"0065" => "+65",
			"00421" => "+421",
			"00386" => "+386",
			"00677" => "+677",
			"00252" => "+252",
			"0027" => "+27",
			"0082" => "+82",
			"0034" => "+34",
			"0094" => "+94",
			"001" => "+1",
			"00249" =>"+249",
			"00597" =>"+597",
			"00268" =>"+268",
			"0046" => "+46",
			"0041" => "+41",
			"00963" =>"+963",
			"00886" =>"+886",
			"00992" =>"+992",
			"00255" =>"+255",
			"0066" => "+66",
			"001" => "+1",
			"00220" => "+220",
			"00670" => "+670",
			"00228" => "+228",
			"00690" => "+690",
			"00676" => "+676",
			"001" => "+1",
			"00216" => "+216",
			"0090" => "+90",
			"00993" => "+993",
			"001" => "+1",
			"00688" => "+688",
			"00256" => "+256",
			"00380" => "+380",
			"00971" => "+971",
			"0044" => "+44",
			"001" => "+1",
			"00598" => "+598",
			"001" => "+1",
			"00998" => "+998",
			"00678" => "+678",
			"0039" => "+39",
			"0058" => "+58",
			"0084" => "+84",
			"00681" =>"+681",
			"00967" =>"+967",
			"00260" =>"+260",
			"00263" =>"+263"
			);
          
		  if (strpos($to, '+') !== false)
         	{
				$basic_num = substr($to, -8);
				$rest=strlen($to)-strlen($basic_num);
				$result = substr($to, 0, $rest);
				////
				if (false !== $key = array_search($result, $mobile_format)) {
				$prefix= $key;
			} 
			else{
				$prefix="";
			}
			
			if(strlen($to)>9){
			if (strpos($to, '+') !== false)
         	{
				$basic_num = substr($to, -8);
				$rest=strlen($to)-strlen($basic_num);
				$result = substr($to, 0, $rest);
				
				
				////
				if (false !== $key = array_search($result, $mobile_format)) {
				$prefix= $key;
			} 
			else{
				$prefix="";
			}

			//$mobileFormat = substr($to, 4);
			$finalFormat=$prefix.$basic_num;
			return $finalFormat;
			

             }
			 else{
				 $basic_num = substr($to, 2); // "quick brown fox jumps over the lazy dog."
				//$basic_num = substr($to, -8);
				//$rest=strlen($to)-strlen($basic_num);
				//$result = substr($to, 0, $rest);
                //if (false !== $value = array_search($result, $mobile_format)) {
				//$prefix= $value;
				$finalFormat="+".$basic_num;
			    return $finalFormat;
			} 
               }
			 
			 else{
				$basic_num = substr($to, -8);
    			$finalFormat="+61".$basic_num;
	    		return $finalFormat;
			 }
			 */
		}
		
		public function getLastMessageId($to)
		{
			$select = $this->getAdapter()->select();
			$select->from($this->_name);
		    $select->where("sms_history.to = '{$to}' and status!='undelivered' and status!='Failed' and parent_id=0 and sms_type='sent'");
            $select->order("id desc");
		    $select->limit(1);
			//return $this->getAdapter()->fetchAll($select);
            return $this->getAdapter()->fetchRow($select);


		}
		
		public function getLastMessageBtReceiverId($reference_id,$id,$sms_reason,$date='')
		{
			$id = (int) $id;
	      if($reference_id==-1)
				$type="contractor";
			else
				$type="customer";
			$select = $this->getAdapter()->select();
			$select->from($this->_name);
			if($date!='')
		    $select->where("(select type from sms_template where id=sms_history.reference_id )='{$type}' and sms_history.send_time <'{$date}' and receiver_id = '{$id}' and sms_reason = '{$sms_reason}' and sms_history.status!='undelivered' and sms_history.status!='Failed' and sms_type='sent' ");
			else
			$select->where("(select type from sms_template where id=sms_history.reference_id )='{$type}' and receiver_id = '{$id}' and  sms_reason = '{$sms_reason}' and status!='undelivered' and status!='Failed' and sms_type='sent' ");
			$select->order("id desc");
		    $select->limit(1);
			return $this->getAdapter()->fetchRow($select);

		}
		
     		public function getLastMessageesAtSameDay($to,$date)
		{
			$date=date('Y-m-d',strtotime($date));
			$select = $this->getAdapter()->select();
			$select->from($this->_name);
			$select->where("sms_history.to = '{$to}' and  sms_type='sent' and parent_id=0 and send_time like '{$date}%' and status!='undelivered' and status!='Failed'");
            $select->order("id desc");
			return $this->getAdapter()->fetchAll($select);

		}
		
		////get_all_mobile_number
		public function getAllMobileNumber()
		{
		    $stmt = $this->getAdapter()->query("SELECT sms_history.to AS mobile_no FROM cleaning_service.sms_history
            UNION SELECT sms_history.from AS mobile_no FROM cleaning_service.sms_history");
            $rows = $stmt->fetchAll();
			return $rows;

		}
		//
		public function sendSmsTwilio($fromNumber,$toNumber,$smsMessage)
		{
			$sid = 'ACd698f0b22aa408a0865abb86cc97b35d';
			$token = '13359760efe3d9ff0057ebf0df7213dc';
			$client = new Services_Twilio($sid, $token);
				 try{ 	
						if(is_null($sid) || is_null($token) || empty($sid) || empty($token))
						{
							echo "<p>Failed to retrieve Twilio authentication parameters.</p>";
						}
						else 
						{
							$message =$client->account->messages->sendMessage(
							$fromNumber,
							$toNumber,
							$smsMessage
							);
						$sid_msg=$message->sid;
						return $sid_msg; 
					   }
					 }
					  catch(Exception $e) 
					  {
					  echo '<p>There was an error sending an SMS using Twilio!!!</p>';
					  echo $e->getMessage();
					   }
		//      $minCheck = date(DATE_ATOM, $sd);
			 //        $select->where("date(bok.booking_start)= '{$datetomrow}'");

			    	}
					
		public function getByReasonTypeId($sms_reason,$reason_id) 
		{
        $id = (int) $reason_id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("reason_id = '{$id}' and sms_reason='{$sms_reason}'");
        return $this->getAdapter()->fetchAll($select);
        //var_dump($this->getAdapter()->fetchRow($select));
		//exit;

        }
			
        public function getOutgoingSmsSameDateType($reference_id,$user_id,$reason,$date_received)
        {
			if($reference_id==-1)
				$type="contractor";
			else
				$type="customer";
			
			$date=date('Y-m-d',strtotime($date_received));
			
			$select = $this->getAdapter()->select();
			$select->from($this->_name);
			$select->where("(select type from sms_template where id=sms_history.reference_id )='{$type}' and receiver_id= '{$user_id}' and sms_reason= '{$reason}'  and sms_type='sent' and parent_id=0 and send_time <'{$date_received}' and status!='undelivered' and status!='Failed' and template_type='standard'");
            $select->order("id desc");
			$select->limit(3);
			return $this->getAdapter()->fetchAll($select);

		}
		
			public function getLastMessageBtReceiverId1($to,$date='')
		{
			$id = (int) $id;
			$select = $this->getAdapter()->select();
			$select->from($this->_name);
			if($date!='')
		    $select->where("sms_history.send_time <'{$date}' and sms_history.to = '{$to}' and sms_reason = '{$sms_reason}' and sms_history.status!='undelivered' and sms_history.status!='Failed' and sms_type='sent' ");
			else
			$select->where("sms_history.to = '{$to}' and status!='undelivered' and status!='Failed' and sms_type='sent' ");
			$select->order("id desc");
		    $select->limit(1);
			return $this->getAdapter()->fetchRow($select);

		}
		
		
		public function getChildOfParent($incomingsid)
		{
			$incomingsid = (int) $incomingsid;
			$select = $this->getAdapter()->select();
			$select->from($this->_name);
			$select->where("parent_id = '{$incomingsid}' and status!='undelivered' and status!='Failed' and sms_type='sent' ");
			$select->order("id desc");
			return $this->getAdapter()->fetchAll($select);
		}
		
		}