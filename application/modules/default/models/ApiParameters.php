<?php

class Model_ApiParameters extends Zend_Db_Table_Abstract {

    protected $_name = 'api_parameters';

	
    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $group = false, $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order($order);

        if ($filters) {
            if (!empty($filters['api_name'])) {
                $select->where("api_name = '{$filters['api_name']}'");
            }
            if(empty($filters['company_id'])){
                $filters['company_id'] = 1;
            }
            $select->where("company_id = {$filters['company_id']}");
        }
        if($group){
            $select->group('api_name');
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }
    
    public function getParameterValue($company_id = 1, $api_name, $parameter_name) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("parameter_name = '{$parameter_name}'");
        $select->where("api_name = '{$api_name}'");
        $select->where("company_id = {$company_id}");
        return $this->getAdapter()->fetchRow($select);
    }
	
    
     /**
     * update table rows according to the assigned id and data
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */

    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "id = '{$id}'");
    }

    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("id = '{$id}'");
    }
    
    public function deleteByApiName($api_name) {
        return parent::delete("api_name = '{$api_name}'");
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
   public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        
		$select->where("id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    
	public function insert(array $data) {
		
		return parent::insert($data);	
	}


}