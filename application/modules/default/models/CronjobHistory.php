<?php

class Model_CronjobHistory extends Zend_Db_Table_Abstract {
	protected $_name = 'cronjob_history';
	
	public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name,array('start_time'=>'FROM_UNIXTIME(run_time)'));
        $select->join('cron_job','cronjob_history.cron_job_id = cron_job.id',array('cronjob_name','every'));
        $select->order($order);
		
		if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        } 
		
		return $this->getAdapter()->fetchAll($select);
	}
	
	
	
	public function insert(array $data){
	
        return parent::insert($data);
	
	}
	


}

?>