<?php

class Model_PaymentAttachment extends Zend_Db_Table_Abstract {

    protected $_name = 'payment_to_contractor_attachment';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order($order);

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        $orWheres = array();
        $wheres = array();

        if ($filters) {
            if (!empty($filters['booking_id'])) {
                $booking_id = (int) $filters['booking_id'];
                $orWheres[] = "booking_id = {$booking_id}";
            }
           
            if (!empty($filters['keywords'])) {
                $keywords = $this->getAdapter()->quote('%' . $filters['keywords'] . '%');
                $wheres[] = "file_name like {$keywords}";
            }
        }


        if ($orWheres) {
            $whereQuery = implode(" AND ", $wheres);
            foreach ($orWheres as $orWhere) {
                $orWhereQuery = $orWhere . ($whereQuery ? " AND " . $whereQuery : '');
                $select->orWhere($orWhereQuery);
            }
        } elseif ($wheres) {
            foreach ($wheres as $where) {
                $select->where($where);
            }
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        
        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'changed');
        
        return parent::update($data, "attachment_id = '{$id}'");
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("attachment_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }
	
	public function getPathById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name,array('path'));
        $select->where("attachment_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }
	
	
	public function getByPaymentToContractorId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("payment_to_contractor_id = '{$id}'");
		$select->where("is_deleted = 0 ");

        return $this->getAdapter()->fetchAll($select);
    }
	
	
	

   
   
    /**
     * delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        
        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'deleted');
        
        return parent::delete("attachment_id= '{$id}'");
    }
    
	public function deleteByPaymentToContractorId($id) {
        $id = (int) $id;
        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'deleted');

        return parent::delete("payment_to_contractor_id = '{$id}'");
    }
    
    public function insert(array $data) {

        $id = parent::insert($data);
        
        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'added');
        
        return $id;
    }

}