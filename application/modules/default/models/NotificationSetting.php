<?php

class Model_NotificationSetting extends Zend_Db_Table_Abstract {

    protected $_name = 'notification_setting';

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getByUserId($user_id) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("user_id = '{$user_id}'");

        return $this->getAdapter()->fetchAll($select);
    }

    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("notification_setting_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function getByUserIdAndCredentialId($user_id, $credential_id) {
        //    $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("user_id = {$user_id}");
        $select->where("credential_id = {$credential_id}");

        return $this->getAdapter()->fetchRow($select);
    }

    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "notification_setting_id = '{$id}'");
    }

    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("notification_setting_id = '{$id}'");
    }

}
