<?php

class Model_UpdateBookingQuestionAnswer extends Zend_Db_Table_Abstract {

    protected $_name = 'update_booking_question_answer';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from(array('ubqa' => $this->_name));
        $select->joinInner(array('ubqs' => 'update_booking_question_status'), 'ubqa.update_booking_question_status_id =ubqs.update_booking_question_status_id', array());
        $select->joinInner(array('ubq' => 'update_booking_question'), 'ubq.update_booking_question_id =ubqs.update_booking_question_status_id', array());
        $select->order($order);

        $filters['company_id'] = CheckAuth::getCompanySession();

        if ($filters) {
            if (!empty($filters['company_id'])) {
                $select->where("ubq.company_id = {$filters['company_id']}");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function getByBookingIdAndStatusId($status_id,$booking_id) {
		$status_id = (int) $status_id;
		$company_id = CheckAuth::getCompanySession();
		$booking_id = (int) $booking_id;
		/*$select = $this->getAdapter()->select();
        $select->from(array('ubqa' => $this->_name));
        $select->joinInner(array('ubqs' => 'update_booking_question_status'), 'ubqa.update_booking_question_status_id =ubqs.update_booking_question_status_id');
        $select->joinInner(array('ubq' => 'update_booking_question'), 'ubq.update_booking_question_id =ubqs.update_booking_question_status_id');
        $select->where("ubq.company_id = {$company_id}");
        $select->where("ubqs.booking_status_id = {$status_id}");
        $select->where("ubqa.booking_id = {$booking_id}");*/
		
		$select1 = $this->getAdapter()->select();
        $select1->from(array('ubq' => 'update_booking_question'),array('ubq.*'));
        $select1->joinInner(array('ubqs' => 'update_booking_question_status'), 'ubq.update_booking_question_id =ubqs.update_booking_question_id',array('ubqs.update_booking_question_status_id'));
        $select1->where("ubqs.booking_status_id = {$status_id}");
		
		$select2 = $this->getAdapter()->select();
        $select2->from(array('ubqa' => $this->_name));
        $select2->where("ubqa.booking_id = {$booking_id}");
		
		$select = $this->getAdapter()->select();
        $select->from(array('rs' => $select1));
        $select->joinLeft(array('rs2' => $select2), 'rs2.update_booking_question_status_id = rs.update_booking_question_status_id');
        
		
		//echo $select->__toString();
        return $this->getAdapter()->fetchAll($select);
        
    }
	
	
	public function getByQuestionStatusIdAndBookingId($update_booking_question_status_id,$booking_id) {
		$update_booking_question_status_id = (int) $update_booking_question_status_id;
		$booking_id = (int) $booking_id;
		
		$select = $this->getAdapter()->select();
        $select->from(array('ubqa' => $this->_name));
        $select->where("ubqa.booking_id = {$booking_id}");
        $select->where("ubqa.update_booking_question_status_id = {$update_booking_question_status_id}");
		
		
		//echo $select->__toString();
        return $this->getAdapter()->fetchRow($select);
        
    }

	public function updateById($id, array $data){
		$id = (int) $id;
        $add_id= parent::update($data, "id = {$id}");
		if($add_id){
			//D.A 20/09/2015 Remove Estimate Questions Cache
			$modelBookingEstimate = new Model_BookingEstimate();
			$estimate = $modelBookingEstimate->getByBookingId($data['booking_id']);
			require_once 'Zend/Cache.php';
			$company_id = CheckAuth::getCompanySession();
			$estimateQuestionsCacheID= $estimate['id'].'_estimateQuestions';
			//var_dump($estimateQuestionsCacheID);exit;
			$estimateViewDir=get_config('cache').'/'.'estimatesView'.'/'.$company_id;	
			if (!is_dir($estimateViewDir)) {
				mkdir($estimateViewDir, 0777, true);
			}			
			$estimateQuestionsFrontEndOption= array('lifetime'=> NULL,
			'automatic_serialization'=> true);
			$estimateQuestionsBackendOptions = array('cache_dir'=>$estimateViewDir );
			$estimateQuestionCache = Zend_Cache::factory('Core','File',$estimateQuestionsFrontEndOption,$estimateQuestionsBackendOptions);			
			$estimateQuestionCache->remove($estimateQuestionsCacheID);	
		}
			
	    return $add_id;
	}
	
    /**
     * delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean 
     */
   
	public function insert(array $data) {  
	
        $add_id= parent::insert($data);
		if($add_id){
			//D.A 20/09/2015 Remove Estimate Questions Cache
			$modelBookingEstimate = new Model_BookingEstimate();
			$estimate = $modelBookingEstimate->getByBookingId($data['booking_id']);
			require_once 'Zend/Cache.php';
			$company_id = CheckAuth::getCompanySession();
			$estimateQuestionsCacheID= $estimate['id'].'_estimateQuestions';
			//var_dump($estimateQuestionsCacheID);exit;
			$estimateViewDir=get_config('cache').'/'.'estimatesView'.'/'.$company_id;	
			if (!is_dir($estimateViewDir)) {
				mkdir($estimateViewDir, 0777, true);
			}			
			$estimateQuestionsFrontEndOption= array('lifetime'=> NULL,
			'automatic_serialization'=> true);
			$estimateQuestionsBackendOptions = array('cache_dir'=>$estimateViewDir );
			$estimateQuestionCache = Zend_Cache::factory('Core','File',$estimateQuestionsFrontEndOption,$estimateQuestionsBackendOptions);			
			$estimateQuestionCache->remove($estimateQuestionsCacheID);	
		}
			
	    return $add_id;
    }

}