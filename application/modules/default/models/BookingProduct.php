<?php

class Model_BookingProduct extends Zend_Db_Table_Abstract {

    protected $_name = 'booking_product';

    public function updateById($id, $data,$log_id = 0) {
        $id = (int) $id;
        $success = parent::update($data, "id = '{$id}'");

        /**
         * get all record and insert it in log
         */
        $modelBookingProductLog = new Model_BookingProductLog();
        $modelBookingProductLog->addBookingProductLog($id,0,$log_id);

        return $success;
    }
	
	public function deleteByExtraInfoId($id) {
        $id = (int) $id;
        return parent::delete("visited_extra_info_id = '{$id}'");
    }
	
	 public function updateByExtraInfoId($id , $data){
    
	$id = (int) $id;
    $success = parent::update($data, "visited_extra_info_id = '{$id}'");
	
  }
    
    public function insert(array $data ,$log_id = 0) {
        $id = parent::insert($data);

        /**
         * get all record and insert it in log
         */
        $modelBookingProductLog = new Model_BookingProductLog();
        $modelBookingProductLog->addBookingProductLog($id , 0,$log_id);

        return $id;
    }

    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("id = '{$id}'");
    }

    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }
	
	public function getByVisitedExtraInfoId($visitedExtraInfoId) {
        $visitedExtraInfoId = (int) $visitedExtraInfoId;
        $select = $this->getAdapter()->select();
        $select->from(array('bp'=>$this->_name));
		$select->joinInner(array('p'=>'product'),'p.id = bp.product_id',array('product'));
        $select->where("visited_extra_info_id = {$visitedExtraInfoId}");

        return $this->getAdapter()->fetchAll($select);
    }

    public function getByBookingId($booking_id) {
        $booking_id = (int) $booking_id;

        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_id = '{$booking_id}' and "."(visited_extra_info_id = 0 or visited_extra_info_id IS NULL) ");

        return $this->getAdapter()->fetchAll($select);
    }

    public function getByBookingIdProductId($booking_id, $product_id) {
        $booking_id = (int) $booking_id;
        $product_id = (int) $product_id;

        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_id = '{$booking_id}'");
        $select->where("product_id = '{$product_id}'");

        return $this->getAdapter()->fetchRow($select);
    }
	
	public function getByVisitedIdProductId($visited_id, $product_id) {
        $visited_id = (int) $visited_id;
        $product_id = (int) $product_id;

        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("visited_extra_info_id = '{$visited_id}'");
        $select->where("product_id = '{$product_id}'");
        
        return $this->getAdapter()->fetchRow($select);
    }

    public function deleteByBookingIdProductIds($booking_id, $product_ids) {
        $booking_id = (int) $booking_id;
        return parent::delete("booking_id = '{$booking_id}' and "."(visited_extra_info_id = 0 or visited_extra_info_id IS NULL) " . ' AND product_id NOT IN (' . implode(', ', $product_ids) . ')');
    }

    public function setProductsToBooking($booking_id, $product_ids, $ltrs , $visited_info_id = null, $delete_old = 1,$log_id = 0) {
       
		//
        // delete products not in the list
        //
        $booking_id = (int) $booking_id;
        if (!empty($product_ids)) {
            foreach ($product_ids AS &$product_id) {
                $product_id = (int) $product_id;
            }
        }
		if($delete_old){
			
			$this->deleteByBookingIdProductIds($booking_id, $product_ids);
			
		}
        //
        // add the new products
        //
        foreach ($product_ids as $key => $id) {
            $this->assignProductToBooking(array('booking_id' => $booking_id, 'product_id' => $id, 'ltr' => $ltrs[$key] , 'visited_extra_info_id'=>$visited_info_id), $log_id);
        }
    }

    public function assignProductToBooking($params,$log_id = 0) {

        //$bookingProductlink = $this->getByBookingIdProductId($params['booking_id'], $params['product_id']);
         $bookingProductlink = $this->getByVisitedIdProductId($params['visited_extra_info_id'], $params['product_id']);
        if (!$bookingProductlink) {
            // insert new product
            return $this->insert($params,$log_id);
        } else {
            // update product
            $this->updateById($bookingProductlink['id'], $params,$log_id);
            return $bookingProductlink['id'];
        }
    }

	public function getProductNamesByBookingId($booking_id){
        $booking_id = (int) $booking_id;

        $select = $this->getAdapter()->select();
        $select->from(array('bp'=>$this->_name),array('product_id','ltr'));
        $select->joinRight(array('p'=>'product'),'p.id = bp.product_id',array('product'));
        $select->where("bp.booking_id = {$booking_id}");
		//echo  $select->__toString();
        return $this->getAdapter()->fetchAll($select);
    }
    /*
      public function getProductLtr() {
      $query = "SHOW COLUMNS FROM {$this->_name} LIKE 'ltr'";
      $stmt = $this->getAdapter()->query($query);
      $row = $stmt->fetch();
      $row = $row['Type'];
      $regex = "/'(.*?)'/";
      preg_match_all($regex, $row, $enum_array);
      $enum_fields = $enum_array[1];
      foreach ($enum_fields as $key => $value) {
      $enums[$value] = $value;
      }
      return $enums;
      }
     * 
     */
}