<?php

class Model_BannedIpAddress extends Zend_Db_Table_Abstract {
	protected $_name = 'banned_ip_address';
	
	public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order($order);
		
		return $this->getAdapter()->fetchAll($select);
	}
	
	
	public function isBannedIP($ip){
		$select = $this->getAdapter()->select();
        $select->from($this->_name);
		$select->where("ip_address = '{$ip}'");
		$result = $this->getAdapter()->fetchRow($select);
		$is_banned = 0;
		if(!empty($result)){
			$is_banned = 1;
		}
		
        return $is_banned;
	
	}
	
	
	public function insert(array $data){
	
        return parent::insert($data);
	
	}
	
	public function deleteBannedIp($ip) {
        
        return parent::delete("ip_address = '{$ip}'");
    }
	 


}

?>