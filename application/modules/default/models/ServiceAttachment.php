<?php

class Model_ServiceAttachment extends Zend_Db_Table_Abstract {

    protected $_name = 'service_attachment';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from(array('sa' => $this->_name));
        $select->order($order);

        if ($filters) {
            if (!empty($filters['service_id'])) {
                $select->where("sa.service_id = {$filters['service_id']}");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "service_attachment_id = '{$id}'");
    }

    /**
     * delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean 
     */
	 
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("service_attachment_id = '{$id}'");
    }
	
	public function deleteByServiceId($id) {
        $id = (int) $id;
        return parent::delete(" service_id = '{$id}'");
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where(" service_attachment_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the assigned attributeValue
     * 
     * @param string $attributeValue
     * @return array
     */
	 
    public function getByServiceId($serviceId , $check_default = null) {
	    $serviceId = (int) $serviceId ; 
        $select = $this->getAdapter()->select();
        $select->from(array('sa'=>$this->_name));
        $select->joinInner(array('attach'=>'attachment') , 'sa.attachment_id = attach.attachment_id');
        $select->where("service_id = '{$serviceId}'");
        $select->where("attach.is_deleted = '0'");
		if(isset($check_default)){
		 $select->where("is_default = '{$check_default}'");
		}
		if($check_default == 1){
		  return $this->getAdapter()->fetchRow($select);
		}
		
		//echo $select->__toString();
        //$select->where("is_default = '0'");
        return $this->getAdapter()->fetchAll($select);
    }
	
	public function executeQuery(){
	  
	  $this->getAdapter()->query("ALTER TABLE `service_attachment`
	ADD COLUMN `is_default` TINYINT NULL DEFAULT '0' AFTER `service_id`");

	}
    
    
    public function getAttachmentsForUpdate() {
        $select = $this->getAdapter()->select();
        $select->from(array('sa'=>$this->_name));
        $select->joinInner(array('attach'=>'attachment') , 'sa.attachment_id = attach.attachment_id');
		$select->where("attach.is_deleted = '0'");
		
		return $this->getAdapter()->fetchAll($select);
	}

    


}