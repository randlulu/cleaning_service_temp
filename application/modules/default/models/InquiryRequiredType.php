<?php

class Model_InquiryRequiredType extends Zend_Db_Table_Abstract {

    protected $_name = 'inquiry_required_type';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order($order);

        $filters['company_id'] = CheckAuth::getCompanySession();
        if ($filters) {
            if (!empty($filters['company_id'])) {
                $company_id = (int) $filters['company_id'];
                $select->where("company_id = {$company_id}");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAllAsArray() {

        $requiredTypes = $this->getAll();

        $requiredTypeArray = array();
        foreach ($requiredTypes AS $requiredType) {
            $requiredTypeArray[$requiredType['id']] = $requiredType['required_type'];
        }
        return $requiredTypeArray;
    }

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "id = '{$id}'");
    }

    /**
     * delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("id = '{$id}'");
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the assigned name
     * 
     * @param char $type
     * @return array 
     */
    public function getByType($type) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("required_type = '{$type}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function getByRequiredTypeAndCompany($requiredType, $companyId = 0) {

        if (!$companyId) {
            $companyId = CheckAuth::getCompanySession();
        }

        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("required_type = '{$requiredType}'");
        $select->where("company_id = '{$companyId}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function checkBeforeDeleteInquiryRequiredType($requiredTypeId, &$tables = array()) {

        $sucsess = true;

        $select_inquiry = $this->getAdapter()->select();
        $select_inquiry->from('inquiry', 'COUNT(*)');
        $select_inquiry->where("required_type_id = {$requiredTypeId}");
        $count_inquiry = $this->getAdapter()->fetchOne($select_inquiry);

        if ($count_inquiry) {
            $tables[] = 'inquiry';
            $sucsess = false;
        }

        return $sucsess;
    }

}