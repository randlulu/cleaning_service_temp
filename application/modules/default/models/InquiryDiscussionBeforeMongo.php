<?php
class Model_InquiryDiscussionBeforeMongo extends Zend_Db_Table_Abstract {

    protected $_name = 'inquiry_discussion';

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        
        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'changed');
        
        return parent::update($data, "discussion_id = '{$id}'");
    }

	
	public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("discussion_id = '{$id}'");
    }
	
    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
	 
	public function getByImageId($id, $order = 'asc') {
	

		$id = (int) $id;
		$typeFilter1 = $this->getAdapter()->quote('inquiry' . '%');	
        $select = $this->getAdapter()->select();
        $select->from(array('id'=>$this->_name));
		$select->joinInner(array('iid' => 'item_image_discussion'), " iid.item_id = id.discussion_id AND iid.type LIKE {$typeFilter1}",array('group_id'));		
		$select->order("id.created {$order}");
        $select->where("iid.image_id = '{$id}'");
  		
        return $this->getAdapter()->fetchAll($select);
    }
	
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("discussion_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the assigned estimateId
     * 
     * @param int $id
     * @return array
     */
    public function getByInquiryId($id, $order = 'asc', $filter = array()) {
	
	
	    $id = (int) $id;

        $select = $this->getAdapter()->select();
        $select->from(array('id'=>$this->_name));
		$typeFilter = $this->getAdapter()->quote('inquiry' . '%');	
		$select->joinLeft(array('iid' => 'item_image_discussion'), " iid.item_id = id.discussion_id AND iid.type LIKE {$typeFilter}",array('group_id'));
		$select->joinLeft(array('im' => 'image'), "(iid.image_id = im.image_id )",array('im.thumbnail_path','im.image_id','im.compressed_path'));
		if(isset($filter['groups'])){
		 $select->where("iid.group_id > 0");
		 $select->group("iid.group_id");
		}else{		
         $select->where("iid.group_id = 0");		 
		}
		
		
		
		$select->order("id.created {$order}");
        //$select->where("inquiry_id = '{$id}'");
		$select->where("is_deleted = 0");
		
        		

        return $this->getAdapter()->fetchAll($select);
    }
    
    public function insert(array $data) {

        $id = parent::insert($data);
        
        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'added');
        
        return $id;
    }

}