<?php

class Model_BookingMultipleDays extends Zend_Db_Table_Abstract {

    protected $_name = 'booking_multiple_days';
    //
    //model for fill function
    //
    private $loggedUser = array();

    /**
     * init
     */
    public function init() {
        parent::init();
        $this->loggedUser = CheckAuth::getLoggedUser();
    }

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null, $limit = 0) {

        $columns = $this->getAdapter()->fetchAll("SHOW COLUMNS FROM `booking` where field not in ('booking_start','booking_end','is_all_day_event')");

        $bookingColumns = array();
        foreach ($columns as $column) {
            $bookingColumns[] = "bok.{$column['Field']}";
        }
        $bookingColumns[] = 'bmd.booking_start';
        $bookingColumns[] = 'bmd.booking_end';
        $bookingColumns[] = 'bmd.is_all_day_event';

        $select = $this->getAdapter()->select();
        $select->from(array('bmd' => $this->_name), $bookingColumns);
        $select->distinct();

        $joinInner = array();
        $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'bok.booking_id = bmd.booking_id', 'cols' => '');


        if ($order) {
            $select->order($order);
        }

        $filters['company_id'] = CheckAuth::getCompanySession();

        if (CheckAuth::checkCredential(array('canSeeDeletedBooking'))) {
            if (!empty($filters['is_deleted'])) {
                $select->where('bok.is_deleted = 1');
            } else {
                $select->where('bok.is_deleted = 0');
            }
        } else {
            $select->where('bok.is_deleted = 0');
        }

        if (!CheckAuth::checkCredential(array('canSeeAllBooking'))) {
            if (CheckAuth::checkCredential(array('canSeeOnlyHisBooking'))) {
                if (CheckAuth::checkCredential(array('canSeeAssignedBooking'))) {

                    $joinInner['contractor_service_booking'] = array('name' => array('csb' => 'contractor_service_booking'), 'cond' => 'bok.booking_id = csb.booking_id', 'cols' => '');

                    $select->where("bok.created_by = {$this->loggedUser['user_id']} OR csb.contractor_id = {$this->loggedUser['user_id']}");
                } else {
                    $select->where("bok.created_by = {$this->loggedUser['user_id']}");
                }
            } else {
                $select->where("bok.created_by = {$this->loggedUser['user_id']}");
            }
        }

        if ($filters) {
            if (!empty($filters['status']) && $filters['status'] != 'all') {
                if ($filters['status'] == 'current') {

                    // current include(TO VISIT,TENTATIVE,TO DO,AWAITING UPDATE,IN PROGRESS)
                    $modelBookingStatus = new Model_BookingStatus();
                    $toVisit = $modelBookingStatus->getByStatusName('TO VISIT');
                    $tentative = $modelBookingStatus->getByStatusName('TENTATIVE');
                    $toDo = $modelBookingStatus->getByStatusName('TO DO');
                    $awaitingUpdate = $modelBookingStatus->getByStatusName('AWAITING UPDATE');
                    $inProcess = $modelBookingStatus->getByStatusName('IN PROGRESS');

                    $select->where("bok.status_id IN ({$toVisit['booking_status_id']},{$tentative['booking_status_id']},{$toDo['booking_status_id']},{$awaitingUpdate['booking_status_id']},{$inProcess['booking_status_id']})");
                } else {
                    $status = $this->getAdapter()->quote(trim($filters['status']));
                    $select->where("bok.status_id = {$status}");
                }
            }
            if (!empty($filters['contractor_id'])) {
                $contractor_id = (int) $filters['contractor_id'];
                $joinInner['contractor_service_booking'] = array('name' => array('csb' => 'contractor_service_booking'), 'cond' => 'bok.booking_id = csb.booking_id', 'cols' => '');

                $select->where("bok.created_by = {$contractor_id} OR csb.contractor_id = {$contractor_id}");
            }
			
			if (!empty($filters['google_event_ids']) && $filters['google_event_ids']) {
                $joinInner['google_calendar_event'] = array('name' => array('gce' => 'google_calendar_event'), 'cond' => 'gce.multiple_day_id = bmd.id', 'cols' => 'google_calendar_event_id');

				$contractor_id = (int) $filters['contractor_id'];
                $select->where('gce.google_calendar_event_id NOt IN (' . implode(', ', $filters['google_event_ids']) . ') AND gce.contractor_id = '.$contractor_id);
				//$wheres['gce.contractor_id'] = ("gce.contractor_id = {$contractor_id}");
            }
			
            if (!empty($filters['company_id'])) {
                $company_id = (int) $filters['company_id'];
                $select->where("bok.company_id = {$company_id}");
            }
            if (!empty($filters['city_id'])) {
                $city_id = (int) $filters['city_id'];
                $select->where("bok.city_id = {$city_id}");
            }

            if (!empty($filters['calendar_booking_start']) && !empty($filters['calendar_booking_end'])) {
                $calendarBookingStart = $filters['calendar_booking_start'];
                $calendarBookingEnd = $filters['calendar_booking_end'];
                $select->where("bmd.booking_start between '" . php2MySqlTime($calendarBookingStart) . "' AND '" . php2MySqlTime($calendarBookingEnd) . "'");
            }
            if (!empty($filters['booking_in_this_day'])) {
                $bookingStart = strtotime(trim($filters['booking_in_this_day']));
                $bookingStartBetween = date("Y-m-d", $bookingStart) . " 00:00:00";
                $bookingEndBetween = date("Y-m-d", $bookingStart) . " 23:59:59";
                $select->where("(bmd.booking_start between '" . $bookingStartBetween . "' AND '" . $bookingEndBetween . "') OR (bmd.booking_end between '" . $bookingStartBetween . "' AND '" . $bookingEndBetween . "')");
            }
        }

        if ($joinInner) {
            foreach ($joinInner as $inner) {
                $select->joinInner($inner['name'], $inner['cond'], $inner['cols']);
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        } elseif ($limit) {
            $select->limit($limit);
        }

        return $this->getAdapter()->fetchAll($select);
    }

    public function updateById($id, $data) {
        $id = (int) $id;
        $suceess = parent::update($data, "id = '{$id}'");

        /**
         * get all record and insert it in log
         */
        //$modelBookingMultipleDaysLog = new Model_BookingMultipleDaysLog();
        //$modelBookingMultipleDaysLog->addBookingMultipleDaysLog($id);

        return $suceess;
    }

    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("id = '{$id}'");
    }

    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function getByBookingId($bookingId) {
        $bookingId = (int) $bookingId;

        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_id = '{$bookingId}'");

        return $this->getAdapter()->fetchAll($select);
    }

    public function deleteByBookingId($bookingId) {
        $bookingId = (int) $bookingId;
        return parent::delete("booking_id = '{$bookingId}'");
    }

    
public function saveMultipleDays($bookingId) {

        //Get Zend Request
        $request = Zend_Controller_Front::getInstance()->getRequest();
		$update_id='';
		$add_id='';
		$bookings_start = array();
        $multi_stpartdate = $request->getParam('multi_stpartdate');
        $multi_stparttime = $request->getParam('multi_stparttime');
        $multi_etpartdate = $request->getParam('multi_etpartdate');
        $multi_etparttime = $request->getParam('multi_etparttime');
        $multi_isAllDayEvent = $request->getParam('multi_isAllDayEvent');
        $multi_new_stpartdate = $request->getParam('multi_new_stpartdate');
        $multi_new_stparttime = $request->getParam('multi_new_stparttime');
        $multi_new_etpartdate = $request->getParam('multi_new_etpartdate');
        $multi_new_etparttime = $request->getParam('multi_new_etparttime');
        $onsite_client_name_new = $request->getParam('onsite_client_name_new' , array());
        $job_start_time_new = $request->getParam('job_start_time_new' , array());
        $job_finish_time_new = $request->getParam('job_finish_time_new' , array());
        $extra_comments_new = $request->getParam('extra_comments_new' , array());
        $multi_new_isAllDayEvent = $request->getParam('multi_new_isAllDayEvent');

		$multiple_days_ids = $this->getByBookingId($bookingId);
		
        if ($multiple_days_ids) {

            foreach ($multiple_days_ids as $multiple_day) {
				$key = $multiple_day['id'];
				
				
				if(!empty($multi_stpartdate[$key]) && !empty($multi_etpartdate[$key]) ){
					
					$stpartdate = !empty($multi_stpartdate[$key]) ? $multi_stpartdate[$key] : '0000-00-00';
					$stparttime = !empty($multi_stparttime[$key]) ? $multi_stparttime[$key] : '00:00:00';
					
					$etpartdate = !empty($multi_etpartdate[$key]) ? $multi_etpartdate[$key] : '0000-00-00';
					$etparttime = !empty($multi_etparttime[$key]) ? $multi_etparttime[$key] : '00:00:00';
					$isAllDayEvent = !empty($multi_isAllDayEvent[$key]) ? $multi_isAllDayEvent[$key] : 0;

					$st = $stpartdate . " " . $stparttime;
					$et = $etpartdate . " " . $etparttime;

					$booking_start = php2MySqlTime(js2PhpTimeNew($st));
					$booking_end = php2MySqlTime(js2PhpTimeNew($et));
					$is_all_day_event = $isAllDayEvent ? 1 : 0;

					$db_params = array();
					$db_params['booking_start'] = $booking_start;
					$db_params['booking_end'] = $booking_end;
					$db_params['booking_id'] = $bookingId;
					$db_params['is_all_day_event'] = $is_all_day_event;
					
					if( $multiple_day['booking_start'] != $db_params['booking_start']){
					$bookings_start[] =  $multiple_day['booking_start'];
                    $bookings_start[] =  $db_params['booking_start'];					
					}
					
					if( $multiple_day['booking_end'] != $db_params['booking_end'] ||  $multiple_day['is_all_day_event'] != $db_params['is_all_day_event']){
					$bookings_start[] =  $multiple_day['booking_start'];				
					}
	
					$booking_start = strtotime($booking_start);
					$booking_end = strtotime($booking_end);
					
					$update_id = $this->updateById($key , $db_params);	
															
				}
            }
        }
		
		if ($multi_new_stpartdate) {
            $bookinMultipleDays = array();
            foreach ($multi_new_stpartdate as $key => $new_stpartdate) {
                $stpartdate = $new_stpartdate ? $new_stpartdate : '0000-00-00';
                $stparttime = !empty($multi_new_stparttime[$key]) ? $multi_new_stparttime[$key] : '00:00:00';
                $etpartdate = !empty($multi_new_etpartdate[$key]) ? $multi_new_etpartdate[$key] : '0000-00-00';
                $etparttime = !empty($multi_new_etparttime[$key]) ? $multi_new_etparttime[$key] : '00:00:00';
                $isAllDayEvent = !empty($multi_new_isAllDayEvent[$key]) ? $multi_new_isAllDayEvent[$key] : 0;

                $st = $stpartdate . " " . $stparttime;
                $et = $etpartdate . " " . $etparttime;

                $booking_start = php2MySqlTime(js2PhpTimeNew($st));
                $booking_end = php2MySqlTime(js2PhpTimeNew($et));
                $is_all_day_event = $isAllDayEvent ? 1 : 0;

                $db_params = array();
                $db_params['booking_start'] = $booking_start;
                $db_params['booking_end'] = $booking_end;
                $db_params['booking_id'] = $bookingId;
                $db_params['is_all_day_event'] = $is_all_day_event;
				
                $bookings_start[] =  $db_params['booking_start'];
				
                $booking_start = strtotime($booking_start);
                $booking_end = strtotime($booking_end);

                $bookinMultipleDays["{$booking_start}_{$booking_end}_{$bookingId}_{$is_all_day_event}"] = $db_params;
				
				////get extra info				
                $onsite_client_name = !empty($onsite_client_name_new[$key]) ? $onsite_client_name_new[$key] : ' ';
                $job_start_time = !empty($job_start_time_new[$key]) ? $job_start_time_new[$key] : '00:00:00';
                $job_finish_time= !empty($job_finish_time_new[$key]) ? $job_finish_time_new[$key] : '00:00:00';
                $extra_comments = !empty($extra_comments_new[$key]) ? $extra_comments_new[$key] : 0;
				///By islam
				$currnetDate = date('Y-m-d H:i:s', time());
                
				if(!empty($onsite_client_name_new[$key])){
					$is_visited  = 1;
				}else if($booking_start>  $currnetDate){
					$is_visited  = 2;
				}
				else{
					$is_visited  = 0;
				}
				
				$extr_info = array(
				'job_start'=> (!empty($job_start_time)) ? php2MySqlTime(js2PhpTimeNew(date('d/m/Y H:i', strtotime($job_start_time)))) : null ,
				'job_end'=> (!empty($job_finish_time)) ? php2MySqlTime(js2PhpTimeNew(date('d/m/Y H:i', strtotime($job_finish_time)))) : null,
				'booking_id'=> $bookingId,
				'onsite_client_name'=> $onsite_client_name,
				'is_visited' => $is_visited
				);
				
				$modelVisitedExtraInfo = new Model_VisitedExtraInfo();
				$visited_info_id = $modelVisitedExtraInfo->insert($extr_info);
				$db_params['visited_extra_info_id'] = $visited_info_id;

                $bookinMultipleDays["{$booking_start}_{$booking_end}_{$bookingId}_{$is_all_day_event}"] = $db_params;
				
				/////now save extra comments 
				$loggedUser = CheckAuth::getLoggedUser();
				

				$modelBookingDiscussion = new Model_BookingDiscussion();
				if(!empty($extra_comments)){
				$modelBookingDiscussion->insert(array('booking_id'=>$bookingId , 'user_id'=>$loggedUser['user_id'] , 'user_message' => $extra_comments , 'visited_extra_info_id'=>$visited_info_id ));
				}
				
				////save products of the extra info 
				$date_products = $request->getParam('date_product_new_'.$key, array());
				$date_ltr = $request->getParam('date_ltr_new_'.$key, array());
				
				if ($date_products) {
				 if($visited_info_id){
					 $delete_old = 0;
                    $modelBookingProduct = new Model_BookingProduct();
                    $modelBookingProduct->setProductsToBooking($bookingId, $date_products, $date_ltr , $visited_info_id,$delete_old);
				 }
				}
				
            }

            foreach ($bookinMultipleDays as $key => $bookinMultipleDay) {
                $add_id = $this->insert($bookinMultipleDay);				
            }
        }
		
		//D.A 19/08/2015 Remove cache files of calendar events when Booking Multiple Days changed	
		if($update_id || $add_id){
			$localEventsCacheId='';
			$localEventsByContractorCacheId='';
			$modelContractorServiceBooking = new Model_ContractorServiceBooking();
			$booking_services = $modelContractorServiceBooking->getByBookingId($bookingId);
			foreach ($booking_services as $booking_service) {
				$contractor_id=$booking_service['contractor_id'];															
				$modelBooking = new Model_Booking();
				$oldBooking = $modelBooking->getById($bookingId);
				$booking_start=date('Y-m-d', strtotime($oldBooking['booking_start']));
				list($year, $month, $day) = explode('-', $booking_start);
				$lastMonth = mktime(0, 0, 0, $month - 1, 1, $year);																		
				$nextMonth = mktime(0, 0, 0, $month + 1, 1, $year);							
				$firstDay = date('d', strtotime($booking_start));
		
				foreach ($bookings_start as $booking_start) {
				//for month
				$localEventsCacheId= date("Y_m",strtotime($booking_start));
				$localEventsByContractorCacheId= $contractor_id.'_'.date("Y_m",strtotime($booking_start));
					require_once 'Zend/Cache.php';
					//$localEventsDir=get_config('cache').'/'.'localEvents';			
					$company_id = CheckAuth::getCompanySession();					
	                $localEventsDir=get_config('cache').'/'.'localEvents'.'/'.$company_id;
					$frontEndOption= array('lifetime'=> NULL,
					'automatic_serialization'=> true);
					$backendOptions = array('cache_dir'=>$localEventsDir );
					$cache = Zend_Cache::factory('Core','File',$frontEndOption,$backendOptions);                  				
					if($firstDay >= "01" && $firstDay <= "12"){
					$cache->remove($localEventsCacheId);						
					$cache->remove($localEventsByContractorCacheId);
					$lastMonthlocalEventsCacheId=date('Y_m', $lastMonth);
					$lastMonthEventsByContractorCacheId= $contractor_id.'_'.date('Y_m', $lastMonth);
					$cache->remove($lastMonthlocalEventsCacheId);						
					$cache->remove($lastMonthEventsByContractorCacheId);
					}else if($firstDay >= "25" && $firstDay <= "31"){
					$cache->remove($localEventsCacheId);						
					$cache->remove($localEventsByContractorCacheId);
					$nextMonthlocalEventsCacheId=date('Y_m', $nextMonth);
					$nextMonthlocalEventsByContractorCacheId= $contractor_id.'_'.date('Y_m', $nextMonth);
					$cache->remove($nextMonthlocalEventsCacheId);						
					$cache->remove($nextMonthlocalEventsByContractorCacheId);
					}else{
					$cache->remove($localEventsCacheId);						
					$cache->remove($localEventsByContractorCacheId);	
					}

					//D.A 27/08/2015 Remove Booking Scheduled Visits Cache
					$bookingScheduledVisitsCacheID= $bookingId.'_bookingScheduledVisits';
					$bookingDetailsCacheID= $bookingId.'_bookingDetails';
					$bookingViewDir=get_config('cache').'/'.'bookingsView'.'/'.$company_id;
					if (!is_dir($bookingViewDir)) {
						mkdir($bookingViewDir, 0777, true);
					}						
					$frontEndOptionOfBookingScheduledVisits= array('lifetime'=> NULL,
					'automatic_serialization'=> true);
					$backendOptionsOfBookingScheduledVisits = array('cache_dir'=>$bookingViewDir );
					$bookingScheduledVisitsCache = Zend_Cache::factory('Core','File',$frontEndOptionOfBookingScheduledVisits,$backendOptionsOfBookingScheduledVisits);
					$bookingScheduledVisitsCache->remove($bookingScheduledVisitsCacheID);
					$bookingScheduledVisitsCache->remove($bookingDetailsCacheID);
					
					//D.A 14/10/2015 Remove Estimate Scheduled Visits Cache
					$modelBookingEstimate = new Model_BookingEstimate();
					$estimate = $modelBookingEstimate->getByBookingId($bookingId);
					$estimateScheduledVisitsCacheID= $estimate['id'].'_estimateScheduledVisits';
					$estimateViewDir=get_config('cache').'/'.'estimatesView'.'/'.$company_id;	
					if (!is_dir($estimateViewDir)) {
						mkdir($estimateViewDir, 0777, true);
					}			
					$estimateFrontEndOption= array('lifetime'=> NULL,
					'automatic_serialization'=> true);
					$estimateBackendOptions = array('cache_dir'=>$estimateViewDir );
					$estimateParamsCache = Zend_Cache::factory('Core','File',$estimateFrontEndOption,$estimateBackendOptions);			
					$estimateParamsCache->remove($estimateScheduledVisitsCacheID);
					
					//D.A 29/09/2015 Remove Invoice Technician Update Details Cache
					$modelBookingInvoice = new Model_BookingInvoice();
					$invoice = $modelBookingInvoice->getByBookingId($bookingId);
					$invoiceParamsCacheID= $invoice['id'].'_invoiceParams';
					$invoiceScheduledVisitsCacheID= $invoice['id'].'_invoiceScheduledVisits';
					$invoiceViewDir=get_config('cache').'/'.'invoicesView'.'/'.$company_id;	
					if (!is_dir($invoiceViewDir)) {
						mkdir($invoiceViewDir, 0777, true);
					}			
					$invoiceParamsFrontEndOption= array('lifetime'=> NULL,
					'automatic_serialization'=> true);
					$invoiceParamsBackendOptions = array('cache_dir'=>$invoiceViewDir );
					$invoiceCache = Zend_Cache::factory('Core','File',$invoiceParamsFrontEndOption,$invoiceParamsBackendOptions);			
					$invoiceCache->remove($invoiceParamsCacheID);
					$invoiceCache->remove($invoiceScheduledVisitsCacheID);
					
				}
			}	
		}
    }
    public function getMultipleDaysWithVisitedByBookingId($booking_id){
        $bookingId = (int) $booking_id;
        $select = $this->getAdapter()->select();
        $select->from(array('bmd'=>$this->_name));
        $select->joinLeft(array('vei'=>'visited_extra_info') , 'vei.visited_extra_info_id = bmd.visited_extra_info_id');
        $select->where("bmd.booking_id = '{$bookingId}'");
        
        return $this->getAdapter()->fetchAll($select);
    }
    
    public function getLastMultipleDayByBookingId($booking_id) {
        $booking_id = (int) $booking_id;

        $select = $this->getAdapter()->select();
        $select->from(array('bmd' => $this->_name) , 'max(bmd.id) , bmd.booking_end');
        $select->joinInner(array('bok' => 'booking'), 'bmd.booking_id = bok.booking_id');
        $select->where("bmd.booking_id= '{$booking_id}'");
       

        return $this->getAdapter()->fetchRow($select);
    }

}