<?php

class Model_BookingHistory extends Zend_Db_Table_Abstract {

    protected $_name = 'booking_history';
    //fill 
    private $modelBooking;

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "history_id = '{$id}'");
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("history_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the assigned BookingId
     * 
     * @param int $id
     * @return array
     */
    public function getByBookingId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order('history_id desc');
        $select->where("booking_id = '{$id}'");

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * get table row according to the assigned BookingId
     * 
     * @param int $id
     * @return array
     */
    public function getUsersByBookingId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from(array($this->_name), array('user_id'));
        $select->where("booking_id = '{$id}'");
        $select->distinct();

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * add table row according to the assigned id
     * 
     * @param int $booking_id
     * @param string $type
     * @param string $description
     * @param string $before
     * @param string $after 
     */
    public function saveBookingHistory($booking_id, $type = 'added', $description = 'nothing', $before = 'nothing', $after = 'nothing') {
        $loggedUser = CheckAuth::getLoggedUser();
        $data = array(
            'user_id' => $loggedUser['user_id'],
            'description' => $description,
            'booking_before' => $before,
            'booking_after' => $after,
            'created' => time(),
            'booking_id' => $booking_id,
            'type' => $type
        );

        parent::insert($data);
    }

    /**
     * update table row according to the assigned id
     * 
     * @param int $booking_id
     * @param array $oldBooking
     * @param array $newBooking
     * @param string $type 
     */
    public function updateBookingHistory($booking_id, $oldBooking = array(), $newBooking = array(), $type = 'changed') {

        foreach ($newBooking as $key => $value) {
            if ($newBooking[$key] != $oldBooking[$key]) {
                self::saveBookingHistory($booking_id, $type, $key, (isset($oldBooking[$key]) && $oldBooking[$key] ? $oldBooking[$key] : 'nothing'), (isset($newBooking[$key]) && $newBooking[$key] ? $newBooking[$key] : 'nothing'));
            }
        }
    }

    /**
     * update table row according to the assigned id
     * 
     * @param int $booking_id
     * @param array $oldBooking
     * @param array $newBooking
     * @param string $type 
     */
    public function addBookingHistory($booking_id, $newBooking = array(), $type = 'added') {

        foreach ($newBooking as $key => $value) {
            self::saveBookingHistory($booking_id, $type, $key, $newBooking[$key] ? $newBooking[$key] : 'nothing');
        }
    }

    /**
     * get table row according to the assigned booking ids and limit them as the $limit and $userId
     * 
     * @param arrray $bookingId
     * @param int $userId
     * @param int $limit
     * @return array  
     */
    public function getRecentHistory($filters = array()) {

        $loggedUser = CheckAuth::getLoggedUser();
        $companyId = CheckAuth::getCompanySession();
        $userId = $loggedUser['user_id'];
        
        $selectB = $this->getAdapter()->select();
        $selectB->from($this->_name, array('booking_id', 'MAX(created) AS created'));
        $selectB->group('booking_id');
        
        $selectA = $this->getAdapter()->select();
        $selectA->from(array('a' => $this->_name));
        $selectA->joinInner(array('b' => $selectB), 'b.booking_id = a.booking_id AND b.created = a.created', '');
        $selectA->group('booking_id');
        $selectA->order('created DESC');
        
        $select = $this->getAdapter()->select();
        $select->from(array('bh' => $selectA));
        
        $select->joinInner(array('bok' => 'booking'), 'bh.booking_id = bok.booking_id', '');
        $select->where("bok.company_id = {$companyId}");

        if (!CheckAuth::checkCredential(array('canSeeAllBookingHistory'))) {
            if (CheckAuth::checkCredential(array('canSeeOnlyHisBookingHistory'))) {
                if (CheckAuth::checkCredential(array('canSeeAssignedBookingHistory'))) {
                    $select->joinInner(array('csb' => 'contractor_service_booking'), 'bh.booking_id = csb.booking_id');
                    $select->where("bh.user_id = {$userId} OR csb.contractor_id = {$userId}");
                } else {
                    $select->where("bh.user_id = {$userId}");
                }
            } else {
                $select->where("bh.user_id = {$userId}");
            }
        }
        if ($filters) {

            if (!empty($filters['created_by'])) {
                $userId = (int) $filters['created_by'];
                $select->where("bok.created_by = '{$userId}'");
            }
        }

        $select->order("created DESC");
        $select->limit(50);

        return $this->getAdapter()->fetchall($select);
    }

    /**
     * get extra info for all the rows
     * @param array $row
     * @param array $types
     * @return array
     */
    public function fills(&$rows, $types = array()) {
        foreach ($rows as &$row) {
            $this->fill($row, $types);
        }
    }

    /**
     * get extra info for each row (Ex: getting the category row by c_id, author row by author id ... etc)
     * 
     * @param array $row
     * @param array $types
     * @return array
     */
    public function fill(&$row, $types = array()) {
        if (in_array('booking', $types)) {
            /**
             * load model
             */
            if (!$this->modelBooking) {
                $this->modelBooking = new Model_Booking();
            }

            $booking = $this->modelBooking->getById($row['booking_id']);
            $row['booking'] = $booking;
        }
    }

}