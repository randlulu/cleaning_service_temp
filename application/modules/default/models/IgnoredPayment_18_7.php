<?php

class Model_IgnoredPayment extends Zend_Db_Table_Abstract {

    protected $_name = 'ignored_payment';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order($order);
        $where = array();
        if ($filters) {
            if ($where) {
                $select->where(implode(' AND ', $where));
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "ignored_payment_id = '{$id}'");
    }

    /**
     * delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("ignored_payment_id = '{$id}'");
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("ignored_payment_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function getPaymentByReferencAndDateAndAmount($referenceWithSpaces, $date, $amount) {
        $referenceWithSpaces = addslashes($referenceWithSpaces);
        $select = $this->getAdapter()->select();
        $select->from(array('ip' => $this->_name));
       
        $select->where("ip.reference = '{$referenceWithSpaces}'");
        $select->where("ip.received_date = {$date}");
        $select->where("ip.amount = {$amount}");
        return $this->getAdapter()->fetchAll($select);
    }

}
