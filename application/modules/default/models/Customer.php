<?php

class Model_Customer extends Zend_Db_Table_Abstract {

    protected $_name = 'customer';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null, $limit = 0 , $perPage = 0, $currentPage = 0, $isCronjob = 0) {
//		echo '$isCronjob  '.$isCronjob;

        $columns = $this->getAdapter()->fetchAll("SHOW COLUMNS FROM `customer` where field not in ('phone1','phone2','phone3','mobile1','mobile2','mobile3')");
		
		foreach ($columns as $column) {
            $customerColumns[] = "c.{$column['Field']}";
        }
		
		$customerColumns['phone1updated'] = new Zend_Db_Expr("IF(length(phone1)<9,phone1,CONCAT('0',right(phone1,9)))");
		$customerColumns['phone2updated'] = new Zend_Db_Expr("IF(length(phone2)<9,phone2,CONCAT('0',right(phone2,9)))");
		$customerColumns['phone3updated'] = new Zend_Db_Expr("IF(length(phone3)<9,phone3,CONCAT('0',right(phone3,9)))");
		$customerColumns['mobile1updated'] = new Zend_Db_Expr("IF(length(mobile1)<9,mobile1,CONCAT('0',right(mobile1,9)))");
		$customerColumns['mobile2updated'] = new Zend_Db_Expr("IF(length(mobile2)<9,mobile2,CONCAT('0',right(mobile2,9)))"); 
		$customerColumns['mobile3updated'] = new Zend_Db_Expr("IF(length(mobile3)<9,mobile3,CONCAT('0',right(mobile3,9)))");
		$customerColumns['phone1'] = new Zend_Db_Expr("IF(length(phone1)<9,phone1,CONCAT('0',right(phone1,9)))");
		$customerColumns['phone2'] = new Zend_Db_Expr("IF(length(phone2)<9,phone2,CONCAT('0',right(phone2,9)))");
		$customerColumns['phone3'] = new Zend_Db_Expr("IF(length(phone3)<9,phone3,CONCAT('0',right(phone3,9)))");
		$customerColumns['mobile1'] = new Zend_Db_Expr("IF(length(mobile1)<9,mobile1,CONCAT('0',right(mobile1,9)))");
		$customerColumns['mobile2'] = new Zend_Db_Expr("IF(length(mobile2)<9,mobile2,CONCAT('0',right(mobile2,9)))"); 
		$customerColumns['mobile3'] = new Zend_Db_Expr("IF(length(mobile3)<9,mobile3,CONCAT('0',right(mobile3,9)))");
		
		$joinInner = array();
        $select = $this->getAdapter()->select();
		$select->from(array('c' => $this->_name),$customerColumns);
        /*$select->from(array('c' => $this->_name),array('*',
		'phone1updated'=>new Zend_Db_Expr("IF(length(phone1)<9,phone1,CONCAT('0',right(phone1,9)))"),
		'phone2updated'=>new Zend_Db_Expr("IF(length(phone2)<9,phone2,CONCAT('0',right(phone2,9)))"),
		'phone3updated'=>new Zend_Db_Expr("IF(length(phone3)<9,phone3,CONCAT('0',right(phone3,9)))"),
		'mobile1updated'=>new Zend_Db_Expr("IF(length(mobile1)<9,mobile1,CONCAT('0',right(mobile1,9)))"),
		'mobile2updated'=>new Zend_Db_Expr("IF(length(mobile2)<9,mobile2,CONCAT('0',right(mobile2,9)))"), 
		'mobile3updated'=>new Zend_Db_Expr("IF(length(mobile3)<9,mobile3,CONCAT('0',right(mobile3,9)))"),
		'phone1'=>new Zend_Db_Expr("IF(length(phone1)<9,phone1,CONCAT('0',right(phone1,9)))"),
		'phone2'=>new Zend_Db_Expr("IF(length(phone2)<9,phone2,CONCAT('0',right(phone2,9)))"),
		'phone3'=>new Zend_Db_Expr("IF(length(phone3)<9,phone3,CONCAT('0',right(phone3,9)))"),
		'mobile1'=>new Zend_Db_Expr("IF(length(mobile1)<9,mobile1,CONCAT('0',right(mobile1,9)))"),
		'mobile2'=>new Zend_Db_Expr("IF(length(mobile2)<9,mobile2,CONCAT('0',right(mobile2,9)))"), 
		'mobile3'=>new Zend_Db_Expr("IF(length(mobile3)<9,mobile3,CONCAT('0',right(mobile3,9)))"))); */
        $select->distinct();
        $select->joinInner(array('ci' => 'city'), 'ci.city_id = c.city_id', array('ci.city_name'));
        $select->joinInner(array('co' => 'country'), 'ci.country_id = co.country_id', array('co.country_name'));
        $select->order($order);

		if($isCronjob ==0){
			$filters['company_id'] = CheckAuth::getCompanySession();
		}
        

		if($isCronjob ==0){
        if (!empty($filters['is_deleted'])) {
            $select->where('c.is_deleted = 1');
        } else {
            $select->where('c.is_deleted = 0');
        }
		}

		if($isCronjob ==0){
        $loggedUser = CheckAuth::getLoggedUser();
        if (!CheckAuth::checkCredential(array('canSeeCustomerList'))) {
            if (CheckAuth::checkCredential(array('viewHisCustomer'))) {
                $modelAuthRole = new Model_AuthRole();
                $contractorRoleId = $modelAuthRole->getRoleIdByName('contractor');
                if ($loggedUser['role_id'] == $contractorRoleId) {
                    $filters['contractor_id'] = $loggedUser['user_id'];
                } else {
                    $filters['created_by'] = $loggedUser['user_id'];
                }
            }
        }
		}

        if ($filters) {

			if (!empty($filters['fix_phone_and_mobile'])) {
                $select->where("c.phone1_modified = '' and c.mobile1_modified = ''");
            }
			if (!empty($filters['state_is_emplty'])) {
				$select->where("c.state_modified = '' and c.state = '' and CHAR_LENGTH(mobile1_modified) = 11"); 
			}
			if (!empty($filters['state_modified'])) { 
                $select->where("c.state_modified = ''");
            }
			
            if (!empty($filters['keywords'])) {
                $keywords = $this->getAdapter()->quote('%' . trim($filters['keywords']) . '%');

                //$joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.customer_id = bok.customer_id', 'cols' => '');
                //$select->where("c.full_text_search LIKE {$keywords} OR bok.full_text_search LIKE {$keywords}");
                $select->where("c.full_text_search LIKE {$keywords} ");

			
                //$keywords = '+' . preg_replace('#[\s]+#', ' +', trim($filters['keywords']));
                //$keywords = $this->getAdapter()->quote($keywords);
                //$select->where("MATCH(c.full_text_search) AGAINST({$keywords} IN BOOLEAN MODE)");
            }

            if (!empty($filters['full_name_search'])) {
                $keywords = $this->getAdapter()->quote('%' . trim($filters['full_name_search']) . '%');
                $select->where("c.full_name_search LIKE {$keywords}");
            }

            if (!empty($filters['customer_type_id'])) {
                $select->where('c.customer_type_id = ' . $filters['customer_type_id']);
            }

            if (!empty($filters['created_by'])) {
                $select->where('c.created_by = ' . $filters['created_by']);
            }

            if (!empty($filters['duplicate'])) {
                $select->where('c.duplicate > 1'); 
            }

            if (!empty($filters['customer_id'])) {
                $filters['customer_id'] = (int) $filters['customer_id'];
                $select->where("c.customer_id = {$filters['customer_id']} ");
            }

            if (!empty($filters['email'])) {
                $email = trim($filters['email']);
                $select->where("CONCAT_WS(' ',email1 ,email2 ,email3) LIKE '%{$email}%'");
            }

            if (!empty($filters['phone'])) {
                $phone = trim($filters['phone']);
                $select->where("CONCAT_WS(' ',phone1 ,phone2 ,phone3) LIKE '%{$phone}%'");
            }

            if (!empty($filters['mobile'])) {
                $mobile = trim($filters['mobile']);
                $select->where("CONCAT_WS(' ',mobile1 ,mobile2 ,mobile3) LIKE '%{$mobile}%'");
            }

            if (!empty($filters['mobile/phone'])) {
                $mobile_phone = $this->getAdapter()->quote('%' . trim($filters['mobile/phone']) . '%');
                $select->where("CONCAT_WS(' ',phone1,phone2,phone3,mobile1,mobile2,mobile3) LIKE {$mobile_phone}");
            }

            if (!empty($filters['company_id'])) {
                $company_id = (int) $filters['company_id'];
                $select->where("c.company_id = {$company_id}");
            }

            if (!empty($filters['city_id'])) {
                $city_id = (int) $filters['city_id'];
                $select->where("c.city_id = {$city_id}");
            }

            if (!empty($filters['have_inquiry'])) {
                $joinInner['inquiry'] = array('name' => array('inq' => 'inquiry'), 'cond' => 'c.customer_id = inq.customer_id', 'cols' => '');
            }

            if (!empty($filters['have_booking'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.customer_id = bok.customer_id', 'cols' => '');
            }

            if (!empty($filters['have_estimate'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.customer_id = bok.customer_id', 'cols' => '');
                $joinInner['booking_estimate'] = array('name' => array('est' => 'booking_estimate'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');
            }

            if (!empty($filters['have_invoice'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.customer_id = bok.customer_id', 'cols' => '');
                $joinInner['booking_invoice'] = array('name' => array('inv' => 'booking_invoice'), 'cond' => 'inv.booking_id = bok.booking_id', 'cols' => '');
            }

            if (!empty($filters['have_complaint'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.customer_id = bok.customer_id', 'cols' => '');
                $joinInner['complaint'] = array('name' => array('comp' => 'complaint'), 'cond' => 'bok.booking_id = comp.booking_id', 'cols' => '');
            }

            if (!empty($filters['have_nothing'])) {
                $select->joinLeft(array('linq' => 'inquiry'), 'linq.customer_id = c.customer_id', '');
                $select->joinLeft(array('lbok' => 'booking'), 'lbok.customer_id = c.customer_id', '');
                $select->where("linq.customer_id IS NULL");
                $select->where("lbok.customer_id IS NULL");
            }

            if (!empty($filters['bussiness_name'])) {
                $joinInner['customer_commercial_info'] = array('name' => array('cci' => 'customer_commercial_info'), 'cond' => 'c.customer_id = cci.customer_id', 'cols' => '');
                $select->where("cci.business_name LIKE '%{$filters['bussiness_name']}%'");
            }

            if (!empty($filters['inquiry_num'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.customer_id = bok.customer_id', 'cols' => '');
                $joinInner['inquiry'] = array('name' => array('i' => 'inquiry'), 'cond' => 'bok.original_inquiry_id = i.inquiry_id', 'cols' => '');
                $inquiry_num = $this->getAdapter()->quote('%' . trim($filters['inquiry_num']) . '%');
                $select->where("i.inquiry_num LIKE {$inquiry_num}");
            }

            if (!empty($filters['booking_num'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.customer_id = bok.customer_id', 'cols' => '');
                $booking_num = $this->getAdapter()->quote('%' . trim($filters['booking_num']) . '%');
                $select->where("bok.booking_num LIKE {$booking_num}");
            }

            if (!empty($filters['estimate_num'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.customer_id = bok.customer_id', 'cols' => '');
                $joinInner['booking_estimate'] = array('name' => array('est' => 'booking_estimate'), 'cond' => 'bok.booking_id = est.booking_id', 'cols' => '');
                $estimate_num = $this->getAdapter()->quote('%' . trim($filters['estimate_num']) . '%');
                $select->where("est.estimate_num LIKE {$estimate_num}");
            }

            if (!empty($filters['contractor_id'])) {
                $contractor_id = (int) $filters['contractor_id'];
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.customer_id = bok.customer_id', 'cols' => '');
                $joinInner['contractor_service_booking'] = array('name' => array('csb' => 'contractor_service_booking'), 'cond' => 'bok.booking_id = csb.booking_id', 'cols' => '');
                $select->where("bok.created_by = {$contractor_id} OR csb.contractor_id = {$contractor_id}");
            }

            if (!empty($filters['service_id'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.customer_id = bok.customer_id', 'cols' => '');
                $joinInner['contractor_service_booking'] = array('name' => array('csb' => 'contractor_service_booking'), 'cond' => 'bok.booking_id = csb.booking_id', 'cols' => '');
                $service_id = (int) $filters['service_id'];
                $select->where("csb.service_id = {$service_id}");
            }
        }
        if ($joinInner) {
            foreach ($joinInner as $inner) {
                $select->joinInner($inner['name'], $inner['cond'], $inner['cols']);
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }elseif ($perPage && $currentPage) {
            $select->limit($perPage, ($currentPage - 1) * $perPage);
        } else if ($limit) {
            $select->limit($limit);
        }
        $select->order('customer_id DESC');
		
		
		
		
        return $this->getAdapter()->fetchAll($select);
    }

    public function allCustomers($filters = array()) {

        $select = $this->getAdapter()->select();
        $select->from(array('c' => $this->_name));
        //$select->joinLeft(array('bok' => 'booking') ,'c.customer_id = bok.customer_id', array('booking_id'));
        //$select->joinLeft(array('inq' => 'inquiry') ,'c.customer_id = inq.customer_id', array('inquiry_id'));
        //$select->joinLeft(array('est' => 'booking_estimate') ,'est.booking_id = bok.booking_id', array('id'));
        $select->distinct();

        if (!empty($filters)) {

            if (!empty($filters['city_id']) || $filters['city_id'] != 0) {
                $city_id = (int) $filters['city_id'];
                $select->where("c.city_id = '{$city_id}'");
            }

            if (!empty($filters['state'])) {
                $state = $filters['state'];
                $select->where("c.state = '{$state}'");
            }

            if (!empty($filters['suburb'])) {
                $suburb = $filters['suburb'];
                $select->where("c.suburb = '{$suburb}'");
            }

            if (!empty($filters['customer_type_id']) || $filters['customer_type_id'] != 0) {
                $customer_type_id = (int) $filters['customer_type_id'];
                $select->where("c.customer_type_id = '{$customer_type_id}'");
            }


            if (!empty($filters['status_id']) || $filters['status_id'] != 0 || !empty($filters['booking_date']) || $filters['booking_date'] != 0 || !empty($filters['service_id']) || $filters['service_id'] != 0 || !empty($filters['label_id']) || $filters['label_id'] != 0 || !empty($filters['deferred_date']) || $filters['deferred_date'] != 0) {

                if (!empty($filters['label_id']) || $filters['label_id'] != 0) {
                    $selectA = $this->getAdapter()->select();
                    $selectA->from(array('bok_lab' => 'booking'));
                    $selectA->joinInner(array('blab' => 'booking_label'), 'blab.booking_id = bok_lab.booking_id', array('label_id'));
                    $select->joinLeft(array('bok' => $selectA), 'bok.customer_id = c.customer_id', array('booking_id'));
                    $select->group('bok.customer_id');

                    $selectB = $this->getAdapter()->select();
                    $selectB->from(array('inq_lab' => 'inquiry'));
                    $selectB->joinInner(array('ilab' => 'inquiry_label'), 'ilab.inquiry_id = inq_lab.inquiry_id', array('label_id'));
                    $select->joinLeft(array('inq' => $selectB), 'inq.customer_id = c.customer_id', array('inquiry_id'));
                    $select->group('inq.customer_id');

                    $selectC = $this->getAdapter()->select();
                    $selectC->from(array('est_lab' => 'booking_estimate'));
                    $selectC->joinInner(array('elab' => 'estimate_label'), 'elab.estimate_id = est_lab.id', array('label_id'));
                    $select->joinLeft(array('est' => $selectC), 'est.booking_id = bok.booking_id', array('id'));

                    $label_id = (int) $filters['label_id'];
                    $select->where("( bok.label_id = '{$label_id}' or inq.label_id = '{$label_id}' or est.label_id = '{$label_id}' )");
                    //$select->orWhere("inq.label_id = '{$label_id}'");
                    //$select->orWhere("est.label_id = '{$label_id}'");
                } else if (!empty($filters['status_id']) || $filters['status_id'] != 0 || !empty($filters['booking_date']) || $filters['booking_date'] != 0 || !empty($filters['service_id']) || $filters['service_id'] != 0) {
                    $select->joinLeft(array('bok' => 'booking'), 'c.customer_id = bok.customer_id', array('booking_id as book_id'));
                }

                if (!empty($filters['deferred_date']) || $filters['deferred_date'] != 0) {
                    if (empty($filters['label_id']) || $filters['label_id'] == 0) {
                        $select->joinLeft(array('inq' => 'inquiry'), 'inq.customer_id = c.customer_id', array('inquiry_id'));
                    }
                    $deferred_date = $filters['deferred_date'];
                    $select->where("(bok.to_follow = '{$deferred_date}' or inq.deferred_date = '{$deferred_date}')");
                }


                if (!empty($filters['status_id']) || $filters['status_id'] != 0) {
                    $status = (int) $filters['status_id'];
                    $select->where("bok.status_id = '{$status}'");
                    $select->group('bok.customer_id');
                }
                if (!empty($filters['booking_date']) || $filters['booking_date'] != 0) {
                    $booking_date = $filters['booking_date'];
                    $select->where("bok.created = '{$booking_date}'");
                    $select->group('bok.customer_id');
                }

                if (!empty($filters['service_id']) || $filters['service_id'] != 0) {
                    $service_id = (int) $filters['service_id'];
                    $select->joinLeft(array('csb' => 'contractor_service_booking'), 'bok.booking_id = csb.booking_id', array('service_id'));
                    $select->where("csb.service_id = '{$service_id}'");
                    $select->group('bok.customer_id');
                }
            }





            if (!empty($filters['inquiry_date']) || $filters['inquiry_date'] != 0) {
                $select->joinLeft(array('inq' => 'inquiry'), 'c.customer_id = inq.customer_id', array('inquiry_id'));
                $select->where("inq.created = '{$inquiry_date}'");
                $select->group('inq.customer_id');
            }

            if (!empty($filters['estimate_date']) || $filters['estimate_date'] != 0) {
                $select->joinLeft(array('est' => 'booking_estimate'), 'est.booking_id = bok.booking_id', array('id'));
                $estimate_date = $filters['estimate_date'];
                $select->where("est.created = '{$estimate_date}'");
            }

            if (!empty($filters['not_customer_ids'])) {
                $customer_ids = $filters['not_customer_ids'];
                $select->where('c.customer_id NOT IN (' . implode(', ', $customer_ids) . ')');
            }

            $select->group('c.customer_id');
            $select->group('c.email1');






            //$select->where("bok.booking_id IS NOT NULL ");
            //$select->where("c.customer_id IS NOT NULL ");
            //$select->where("inq.inquiry_id IS NOT NULL ");
            //$select->where("est.id IS NOT NULL");
            //echo  $select->__toString();

            return $this->getAdapter()->fetchAll($select);
        }

        return array();
    }

    public function findDuplicate($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from(array('c' => $this->_name), array("TRIM(CONCAT_WS('.',c.title,CONCAT_WS(' ',TRIM(c.first_name),TRIM(c.last_name)))) AS full_name", "*"));
        $select->distinct();
        $select->order($order);
        $select->group('full_name');

        $loggedUser = CheckAuth::getLoggedUser();
        $company_id = CheckAuth::getCompanySession();

        if (!CheckAuth::checkCredential(array('canSeeCustomerList'))) {
            if (CheckAuth::checkCredential(array('viewHisCustomer'))) {
                $select->where('c.created_by = ' . $loggedUser['user_id']);
            }
        }
        $select->where("c.company_id = {$company_id}");
        $select->where('c.is_deleted = 0');
        $select->where('c.duplicate > 1');
        $select->where('c.duplicate <= 50');

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

    public function cronJobCountDuplicate() {
        $select = $this->getAdapter()->select();
        $select->from(array('c' => $this->_name));
        $select->distinct();

        $allCustomer = $this->getAdapter()->fetchAll($select);

        foreach ($allCustomer as $customer) {

            $duplicates = $this->getAllDuplicateCustomer(0, $customer);
            $this->updateById($customer['customer_id'], array('duplicate' => count($duplicates)));
        }
    }

    /**
     * update table rows according to the assigned id and data
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;

		$address = array();
		if(isset($data['unitLotNumber']) && isset($data['streetNumber']) && isset($data['streetAddress']) && isset($data['suburb']) && isset($data['state']) && isset($data['postcode'])){
			$address['unit_lot_number'] = trim($data['unitLotNumber']);
			$address['street_number'] = trim($data['streetNumber']);
			$address['street_address'] = trim($data['streetAddress']);
			$address['suburb'] = trim($data['suburb']);
			$address['state'] = trim($data['state']);
			$address['postcode'] = trim($data['postcode']);

			$modelBookingAddress = new Model_BookingAddress();
			$geocode = $modelBookingAddress->getLatAndLon($address);
			$data['customer_lat'] = $geocode['lat'] ? $geocode['lat'] : 0;
			$data['customer_lon'] = $geocode['lon'] ? $geocode['lon'] : 0;
		}
		
        /**
         * update full text search table
         */
        $done = false;
        if (!empty($data['full_text_search'])) {
            $done = true;
        }
        $modelUpdateFullTextSearch = new Model_UpdateFullTextSearch();
        $modelUpdateFullTextSearch->updateFullTextSearchFlag($this->_name, $id, $done);

        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'changed');
        $success = parent::update($data, "customer_id = '{$id}'");
        //$this->updateFullTextSearch($id);

        return $success;
    }

    public function insert(array $data) {
		$address = array();
		if(isset($data['unitLotNumber']) && isset($data['streetNumber']) && isset($data['streetAddress']) && isset($data['suburb']) && isset($data['state']) && isset($data['postcode'])){
			$address['unit_lot_number'] = trim($data['unitLotNumber']);
			$address['street_number'] = trim($data['streetNumber']);
			$address['street_address'] = trim($data['streetAddress']);
			$address['suburb'] = trim($data['suburb']);
			$address['state'] = trim($data['state']);
			$address['postcode'] = trim($data['postcode']);

			$modelBookingAddress = new Model_BookingAddress();
			$geocode = $modelBookingAddress->getLatAndLon($address);
			$data['customer_lat'] = $geocode['lat'] ? $geocode['lat'] : 0;
			$data['customer_lon'] = $geocode['lon'] ? $geocode['lon'] : 0;
		}
		
        $id = parent::insert($data);
        //$this->updateFullTextSearch($id);

        /**
         * update full text search table
         */
        $modelUpdateFullTextSearch = new Model_UpdateFullTextSearch();
        $modelUpdateFullTextSearch->insertFullTextSearch($this->_name, $id);

        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'added');

        return $id;
    }

    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;

        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'deleted');

        return parent::delete("customer_id = '{$id}'");
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
	 
    public function getById($id , $fortwilio=0) {
		$fortwilio = (int) $fortwilio;
     	$columns = $this->getAdapter()->fetchAll("SHOW COLUMNS FROM `customer` where field not in ('phone1','phone2','phone3','mobile1','mobile2','mobile3')");
		
		foreach ($columns as $column) {
            $customerColumns[] = "c.{$column['Field']}";
        }
		
		if($fortwilio==1){
		  $select = $this->getAdapter()->select();
		   $select->from(array('c' => $this->_name));
		   $select->where("c.customer_id = '{$id}'");
			return $this->getAdapter()->fetchRow($select);
		}else{
			$customerColumns['phone1updated'] =new Zend_Db_Expr("IF((length(phone1)>9 and phone1 like '0061%'),CONCAT('0',right(phone1,9)),phone1)");
			$customerColumns['phone2updated'] = new Zend_Db_Expr("IF((length(phone2)>9 and phone2 like '0061%'),CONCAT('0',right(phone2,9)),phone2)");
			$customerColumns['phone3updated'] = new Zend_Db_Expr("IF((length(phone3)>9 and phone3 like '0061%'),CONCAT('0',right(phone3,9)),phone3)");
			$customerColumns['mobile1updated'] = new Zend_Db_Expr("IF((length(mobile1)>9 and mobile1 like '0061%'),CONCAT('0',right(mobile1,9)),mobile1)");
			$customerColumns['mobile2updated'] =new Zend_Db_Expr("IF((length(mobile2)>9 and mobile2 like '0061%'),CONCAT('0',right(mobile2,9)),mobile2)");
			$customerColumns['mobile3updated'] = new Zend_Db_Expr("IF((length(mobile3)>9 and mobile3 like '0061%'),CONCAT('0',right(mobile3,9)),mobile3)");
			$customerColumns['phone1'] = new Zend_Db_Expr("IF((length(phone1)>9 and phone1 like '0061%'),CONCAT('0',right(phone1,9)),phone1)");
			$customerColumns['phone2'] =  new Zend_Db_Expr("IF((length(phone2)>9 and phone2 like '0061%'),CONCAT('0',right(phone2,9)),phone2)");
			$customerColumns['phone3'] = new Zend_Db_Expr("IF((length(phone3)>9 and phone3 like '0061%'),CONCAT('0',right(phone3,9)),phone3)");
			$customerColumns['mobile1'] = new Zend_Db_Expr("IF((length(mobile1)>9 and mobile1 like '0061%'),CONCAT('0',right(mobile1,9)),mobile1)");
			$customerColumns['mobile2'] = new Zend_Db_Expr("IF((length(mobile2)>9 and mobile2 like '0061%'),CONCAT('0',right(mobile2,9)),mobile2)");
			$customerColumns['mobile3'] = new Zend_Db_Expr("IF((length(mobile3)>9 and mobile3 like '0061%'),CONCAT('0',right(mobile3,9)),mobile3)");
		}
		
		
        $id = (int) $id;
        $select = $this->getAdapter()->select();
		$select->from(array('c' => $this->_name),$customerColumns);
        /*$select->from(array('c' => $this->_name),array('*',
		'phone1updated'=>new Zend_Db_Expr("IF(length(phone1)<9,phone1,CONCAT('0',right(phone1,9)))"),
		'phone2updated'=>new Zend_Db_Expr("IF(length(phone2)<9,phone2,CONCAT('0',right(phone2,9)))"),
		'phone3updated'=>new Zend_Db_Expr("IF(length(phone3)<9,phone3,CONCAT('0',right(phone3,9)))"),
		'mobile1updated'=>new Zend_Db_Expr("IF(length(mobile1)<9,mobile1,CONCAT('0',right(mobile1,9)))"),
		'mobile2updated'=>new Zend_Db_Expr("IF(length(mobile2)<9,mobile2,CONCAT('0',right(mobile2,9)))"), 
		'mobile3updated'=>new Zend_Db_Expr("IF(length(mobile3)<9,mobile3,CONCAT('0',right(mobile3,9)))")));*/ 
        $select->joinLeft(array('ci' => 'city'), 'ci.city_id = c.city_id', array('ci.city_name','ci.city_id'));
        $select->joinLeft(array('co' => 'country'), 'ci.country_id = co.country_id', array('co.country_name','co.country_id'));
        $select->joinLeft(array('ct' => 'customer_type'), 'c.customer_type_id = ct.customer_type_id', array('ct.customer_type'));
        $select->joinLeft(array('comp' => 'company'), 'c.company_id = comp.company_id', array('comp.company_name'));
        $select->where("customer_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select); 
    }

	
    /**
     * get table row according to the customer id and created by
     * 
     * @param int $customerId
     * @param int $createdBy
     * @return array 
     */
    public function getByCustomerIdAndCreatedBy($customerId, $createdBy) {
        $customerId = (int) $customerId;
        $createdBy = (int) $createdBy;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("customer_id = '{$customerId}' AND created_by = '{$createdBy}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the email
     * 
     * @param int $email
     * @return array 
     */
    public function getByEmail($email) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("email1 = '{$email}'");
        $select->orWhere("email2 = '{$email}'");
        $select->orWhere("email3 = '{$email}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the phone
     * 
     * @param int $phone
     * @return array 
     */
    public function getByPhone($phone) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("phone1 = '{$phone}'");
        $select->orWhere("phone2 = '{$phone}'");
        $select->orWhere("phone3 = '{$phone}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the mobile
     * 
     * @param int $mobile
     * @return array 
     */
    public function getByMobile($mobile) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("mobile1 = '{$mobile}'");
        $select->orWhere("mobile2 = '{$mobile}'");
        $select->orWhere("mobile3 = '{$mobile}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * getcustomer id according to the full name search
     * 
     * @param string $fullNameSearch
     * @return array 
     */
    public function getCustomerIdByIdFullNameSearch($fullNameSearch) {

        $select = $this->getAdapter()->select();
        $select->from($this->_name, 'customer_id');
        $fullNameSearch = $this->getAdapter()->quote('%' . $fullNameSearch . '%');
        $select->where("full_name_search LIKE {$fullNameSearch}");
        $select->where('is_deleted = 0');

        $results = $this->getAdapter()->fetchAll($select);

        $data = array();
        if ($results) {
            foreach ($results as $result) {
                $data[] = $result['customer_id'];
            }
        }

        return $data;
    }

    /**
     * get table rows 
     * as array to choose @return as array 'user_id' => business_name or the query result
     * 
     * @param boolean $asArray
     * @return array 
     */
    public function getAllCustomer($asArray = false, $filters = array()) {

        $results = $this->getAll($filters);

        if ($asArray) {
            $data = array();
            if ($results) {
                foreach ($results as $result) {
                    $data[$result['customer_id']] = get_customer_name($result);
                }
            }
            return $data;
        } else {
            return $results;
        }
    }

    public function checkIfCanSeeCustomer($customerId, $userId = 0) {

        if (!$userId) {
            $loggedUser = CheckAuth::getLoggedUser();
            $userId = $loggedUser['user_id'];
        }

        if (CheckAuth::checkCredential(array('canSeeAllCustomer'))) {
            return true;
        }

        $customer = $this->getById($customerId);
        if ($customer['created_by'] == $userId) {
            return true;
        } else {
            //
            // load model
            //
            $modelContractorServiceBooking = new Model_ContractorServiceBooking();
            $modelBooking = new Model_Booking();

            $filters = array();
            $filters['is_accepted'] = "accepted";
            $filters['contractor_id'] = $userId;
            $contractorServiceBookings = $modelContractorServiceBooking->getAll($filters);
            if ($contractorServiceBookings) {
                foreach ($contractorServiceBookings AS $contractorServiceBooking) {
                    $bookingId = $contractorServiceBooking['booking_id'];
                    if ($modelBooking->checkBookingTimePeriod($bookingId, $userId)) {
                        $booking = $modelBooking->getById($bookingId);
                        if ($booking['customer_id'] == $customerId) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    public function checkIfCanSeeLocation($customerId, $userId = 0) {

        if (!$userId) {
            $loggedUser = CheckAuth::getLoggedUser();
            $userId = $loggedUser['user_id'];
        }

        if (CheckAuth::checkCredential(array('canSeeAllCustomerLocation'))) {
            return true;
        }

        $customer = $this->getById($customerId);
        if ($customer['created_by'] == $userId) {
            return true;
        } else {
            //
            // load model
            //
            $modelContractorServiceBooking = new Model_ContractorServiceBooking();
            $modelBooking = new Model_Booking();

            $filters = array();
            $filters['is_accepted'] = "accepted";
            $filters['contractor_id'] = $userId;
            $contractorServiceBookings = $modelContractorServiceBooking->getAll($filters);
            if ($contractorServiceBookings) {
                foreach ($contractorServiceBookings AS $contractorServiceBooking) {
                    $bookingId = $contractorServiceBooking['booking_id'];
                    if ($modelBooking->checkBookingTimePeriod($bookingId, $userId)) {
                        $booking = $modelBooking->getById($bookingId);
                        if ($booking['customer_id'] == $customerId) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    private $modelCustomerCommercialInfo;
    private $modelCities;
    private $modelCustomerContact;
    private $modelCustomerContactLabel;

    /**
     * get extra info for all the rows
     * @param array $row
     * @param array $types
     * @return array
     */
    public function fills(&$rows, $types = array()) {
        foreach ($rows as &$row) {
            $this->fill($row, $types);
        }
    }

    /**
     * get extra info for each row (Ex: getting the category row by c_id, author row by author id ... etc)
     * 
     * @param array $row
     * @param array $types
     * @return array
     */
    public function fill(&$row, $types = array()) {
	
	   if(isset($row['customer_id'])){

        if (in_array('customer_commercial_info', $types)) {
            /**
             * load model
             */
            if (!$this->modelCustomerCommercialInfo) {
                $this->modelCustomerCommercialInfo = new Model_CustomerCommercialInfo();
            }

            $row['customer_commercial_info'] = $this->modelCustomerCommercialInfo->getByCustomerId($row['customer_id']);
        }

        if (in_array('customer_contacts', $types)) {
            /**
             * load model
             */
            if (!$this->modelCustomerContact) {
                $this->modelCustomerContact = new Model_CustomerContact();
            }
            if (!$this->modelCustomerContactLabel) {
                $this->modelCustomerContactLabel = new Model_CustomerContactLabel();
            }

            $customerContacts = $this->modelCustomerContact->getByCustomerId($row['customer_id']);
            foreach ($customerContacts as &$customerContact) {
                $customerContactLabel = $this->modelCustomerContactLabel->getById($customerContact['customer_contact_label_id']);
                $customerContact['contact_label'] = $customerContactLabel['contact_label'];
            }
            $row['customer_contacts'] = $customerContacts;
        }

        if (in_array('customer_city', $types)) {
            /**
             * load model
             */
            if (!$this->modelCities) {
                $this->modelCities = new Model_Cities();
            }

            $row['customer_city'] = $this->modelCities->getById($row['city_id']);
        }

        if (in_array('duplicate_count', $types)) {

            $row['duplicate_count'] = count($this->getAllDuplicateCustomer($row['customer_id']));
        }
        return $row;
		
	  }	
    }

    public function getAllDuplicateCustomer($customerId, $cutomer = array()) {
		
        if (empty($cutomer)) {
		
            $cutomer = $this->getById($customerId);
            $company_id = CheckAuth::getCompanySession();
        } else {
		
            $company_id = isset($cutomer['company_id']) ? $cutomer['company_id'] : 0;
        }

        $select = $this->getAdapter()->select();
        $select->from(array('c' => $this->_name));
        $select->where("c.company_id = {$company_id}");
        $select->where('c.is_deleted = 0');

        $where = array();
        if (isset($cutomer['email1']) && $cutomer['email1']) {
            $where[] = "'{$cutomer['email1']}' IN (email1 ,email2 ,email3)";
        }
        if (isset($cutomer['email2']) && $cutomer['email2']) {
            $where[] = "'{$cutomer['email2']}' IN (email1 ,email2 ,email3)";
        }
        if (isset($cutomer['email3']) && $cutomer['email3']) {
            $where[] = "'{$cutomer['email3']}' IN (email1 ,email2 ,email3)";
        }

        if (isset($cutomer['phone1']) && $cutomer['phone1']) {
            $where[] = "REPLACE('{$cutomer['phone1']}', ' ', '') IN (REPLACE(phone1, ' ', '') ,REPLACE(phone2, ' ', '') ,REPLACE(phone3, ' ', ''))";
        }
        if (isset($cutomer['phone2']) && $cutomer['phone2']) {
            $where[] = "REPLACE('{$cutomer['phone2']}', ' ', '') IN (REPLACE(phone1, ' ', '') ,REPLACE(phone2, ' ', '') ,REPLACE(phone3, ' ', ''))";
        }
        if (isset($cutomer['phone3']) && $cutomer['phone3']) {
            $where[] = "REPLACE('{$cutomer['phone3']}', ' ', '') IN (REPLACE(phone1, ' ', '') ,REPLACE(phone2, ' ', '') ,REPLACE(phone3, ' ', ''))";
        }
        
    
          if (isset($cutomer['mobile1']) && $cutomer['mobile1']) {
            $where[] = "REPLACE(substr('{$cutomer['mobile1']}' , -9), ' ', '') IN (REPLACE(substr(mobile1 , -9), ' ', '') ,REPLACE(mobile2, ' ', '') ,REPLACE(mobile3, ' ', ''))";
        }
        

        /*if (isset($cutomer['mobile1']) && $cutomer['mobile1']) {
            $where[] = "REPLACE('{$cutomer['mobile1']}', ' ', '') IN (REPLACE(mobile1, ' ', '') ,REPLACE(mobile2, ' ', '') ,REPLACE(mobile3, ' ', ''))";
        }*/
        
        if (isset($cutomer['mobile2']) && $cutomer['mobile2']) {
            $where[] = "REPLACE('{$cutomer['mobile1']}', ' ', '') IN (REPLACE(mobile1, ' ', '') ,REPLACE(mobile2, ' ', '') ,REPLACE(mobile3, ' ', ''))";
        }
        if (isset($cutomer['mobile3']) && $cutomer['mobile3']) {
            $where[] = "REPLACE('{$cutomer['mobile1']}', ' ', '') IN (REPLACE(mobile1, ' ', '') ,REPLACE(mobile2, ' ', '') ,REPLACE(mobile3, ' ', ''))";
        }

        if ($where) {
            $select->where(implode(" OR ", $where));
        }
		
		
        return $this->getAdapter()->fetchAll($select);
    }

    public function getCustomerContacts($customerId, $type = 'normal', $bookingId = 0) {


        $customer = $this->getById($customerId);

        if (!$customer) {
            return null;
        }

        $this->fill($customer, array('customer_commercial_info', 'customer_contacts'));

        $customerContacts = "";
        $emailCustomerContacts = "";


        $customerContacts.="Customer : " . get_customer_name($customer) . "<br>";

        if (!empty($customer['customer_commercial_info']['business_name'])) {
            $customerContacts.="Business Name : " . ucfirst($customer['customer_commercial_info']['business_name']) . "<br>";
            $emailCustomerContacts.="Business Name : " . ucfirst($customer['customer_commercial_info']['business_name']) . "<br>";
        }

        if (!empty($customer['email1'])) {
            $customerContacts.="Email : {$customer['email1']}" . "<br>";
        }

        if (!empty($customer['email2'])) {
            $customerContacts.="Email : {$customer['email2']}" . "<br>";
        }
        if (!empty($customer['email3'])) {
            $customerContacts.="Email : {$customer['email3']}" . "<br>";
        }

        if (!empty($bookingId)){
            $this->router = Zend_Controller_Front::getInstance()->getRouter();
            $link = $this->router->assemble(array('booking_id' => $bookingId), 'openApp');
            if (!empty($customer['mobile1']) || !empty($customer['mobile2']) || !empty($customer['mobile3'])){
                $customerContacts.='<a href="'.$link.'">Call Customer</a><br>';
            }
        }

        if (!empty($customer['mobile1'])) {
            $emailCustomerContacts.="Mobile : {$customer['mobile1']}" . "<br>";
        }

        if (!empty($customer['mobile2'])) {
            $emailCustomerContacts.="Mobile : {$customer['mobile2']}" . "<br>";
        }

        if (!empty($customer['mobile3'])) {
            $emailCustomerContacts.="Mobile : {$customer['mobile3']}" . "<br>";
        }

        if (!empty($customer['phone1'])) {
            $customerContacts.="Phone : {$customer['phone1']}" . "<br>";
            $emailCustomerContacts.="Phone : {$customer['phone1']}" . "<br>";
        }

        if (!empty($customer['phone2'])) {
            $customerContacts.="Phone : {$customer['phone2']}" . "<br>";
            $emailCustomerContacts.="Phone : {$customer['phone2']}" . "<br>";
        }

        if (!empty($customer['phone3'])) {
            $customerContacts.="Phone : {$customer['phone3']}" . "<br>";
            $emailCustomerContacts.="Phone : {$customer['phone3']}" . "<br>";
        }

        if (!empty($customer['fax'])) {
            $customerContacts.="Fax : {$customer['fax']}" . "<br>";
            $emailCustomerContacts.="Fax : {$customer['fax']}" . "<br>";
        }

        if (!empty($customer['po_box'])) {
            $customerContacts.="PO Box : {$customer['po_box']}";
            $emailCustomerContacts.="PO Box : {$customer['po_box']}";
        }

        if (!empty($customer['customer_contacts'])) {
            foreach ($customer['customer_contacts'] as $customerContact) {
                $customerContacts.="{$customerContact['contact_label']} : {$customerContact['contact']}" . "<br>";
                $emailCustomerContacts.="{$customerContact['contact_label']} : {$customerContact['contact']}" . "<br>";
            }
        }

        if ($type == 'email') {
            return $emailCustomerContacts;
        }

        return $customerContacts;
    }

    public function getFullTextCustomerContacts($customerId) {

        $customer = $this->getById($customerId);

        if (!$customer) {
            return '';
        }

        $this->fill($customer, array('customer_commercial_info', 'customer_contacts'));
		
		

        $customerContacts = array();


        $customerContacts[] = trim(get_customer_name($customer));

        $customerContacts[] = trim(get_line_address($customer));

        if (!empty($customer['customer_commercial_info']['business_name'])) {
            $customerContacts[] = trim(ucfirst($customer['customer_commercial_info']['business_name']));
        }

        if (!empty($customer['email1'])) {
            $customerContacts[] = trim($customer['email1']);
        }

        if (!empty($customer['email2'])) {
            $customerContacts[] = trim($customer['email2']);
        }
        if (!empty($customer['email3'])) {
            $customerContacts[] = trim($customer['email3']);
        }

        if (!empty($customer['mobile1'])) {
            $customerContacts[] = trim($customer['mobile1']);
        }

        if (!empty($customer['mobile2'])) {
            $customerContacts[] = trim($customer['mobile2']);
        }

        if (!empty($customer['mobile3'])) {
            $customerContacts[] = trim($customer['mobile3']);
        }

        if (!empty($customer['phone1'])) {
            $customerContacts[] = trim($customer['phone1']);
        }

        if (!empty($customer['phone2'])) {
            $customerContacts[] = trim($customer['phone2']);
        }

        if (!empty($customer['phone3'])) {
            $customerContacts[] = trim($customer['phone3']);
        }

        if (!empty($customer['fax'])) {
            $customerContacts[] = trim($customer['fax']);
        }

        if (!empty($customer['po_box'])) {
            $customerContacts[] = trim($customer['po_box']);
        }

        if (!empty($customer['customer_contacts'])) {
            foreach ($customer['customer_contacts'] as $customerContact) {
				if (!empty($customerContact['email1'])) {
					$customerContacts[] = trim($customerContact['email1']);
				}

				if (!empty($customerContact['email2'])) {
					$customerContacts[] = trim($customerContact['email2']);
				}
				if (!empty($customerContact['email3'])) {
					$customerContacts[] = trim($customerContact['email3']);
				}

				if (!empty($customerContact['mobile1'])) {
					$customerContacts[] = trim($customerContact['mobile1']);
				}

				if (!empty($customerContact['mobile2'])) {
					$customerContacts[] = trim($customerContact['mobile2']);
				}

				if (!empty($customerContact['mobile3'])) {
					$customerContacts[] = trim($customerContact['mobile3']);
				}

				if (!empty($customerContact['phone1'])) {
					$customerContacts[] = trim($customerContact['phone1']);
				}

				if (!empty($customerContact['phone2'])) {
					$customerContacts[] = trim($customerContact['phone2']);
				}

				if (!empty($customerContact['phone3'])) {
					$customerContacts[] = trim($customerContact['phone3']);
				}
				if (!empty($customerContact['contact'])) {
					$customerContacts[] = trim($customerContact['contact']);
				}
				if (!empty($customerContact['first_name']) || !empty($customerContact['last_name'])) {
					$customerContacts[] = trim(get_customer_name($customerContact));
				}
	
            }
        }
		
		
		

        if ($customerContacts) {
            $customerContacts = implode(' ', $customerContacts);
        }

        return $customerContacts;
    }

    public function updateFullTextSearch($customerId) {

        // Load Model
        $modelBookingAddress = new Model_BookingAddress();
        $modelInquiry = new Model_Inquiry();
        $modelBookingEstimate = new Model_BookingEstimate();
        $modelBookingInvoice = new Model_BookingInvoice();
        $model_Complaint = new Model_Complaint();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelBookingStatus = new Model_BookingStatus();


        $fullTextSearch = array();

        //customer
        $fullTextCustomer = $this->getFullTextCustomerContacts($customerId);
        $fullTextSearch[] = trim($fullTextCustomer);

        $selectBooking = $this->getAdapter()->select();
        $selectBooking->from('booking');
        $selectBooking->where("customer_id = {$customerId}");
        $selectBooking->where('is_deleted = 0');
        $bookings = $this->getAdapter()->fetchAll($selectBooking);

        $inquirySelect = $this->getAdapter()->select();
        $inquirySelect->from('inquiry');
        $inquirySelect->where("customer_id = {$customerId}");
        $inquirySelect->where('is_deleted = 0');
        $inquiries = $this->getAdapter()->fetchAll($inquirySelect);


        if ($inquiries) {
            foreach ($inquiries as $inquiry) {
                $fullTextSearch[] = trim($inquiry['inquiry_num']);
                $fullTextSearch[] = trim($inquiry['comment']);
            }
        }

        if ($bookings) {
            foreach ($bookings as $booking) {

                $bookingId = $booking['booking_id'];

                // booking
                $fullTextSearch[] = trim($booking['booking_num']);
                $fullTextSearch[] = trim($booking['title']);
                $fullTextSearch[] = trim($booking['description']);
                $status = $modelBookingStatus->getById($booking['status_id']);
                $fullTextSearch[] = trim($status['name']);


                // address
                $bookingAddress = $modelBookingAddress->getByBookingId($bookingId);
                $fullTextSearch[] = trim(get_line_address($bookingAddress));

                // services
                $fullTextContractorServiceBooking = $modelContractorServiceBooking->getFullTextContractorServiceBooking($bookingId);
                if ($fullTextContractorServiceBooking) {
                    $fullTextSearch[] = trim($fullTextContractorServiceBooking);
                }

                //complaint
                $complaints = $model_Complaint->getFullTextComplaintByBookingId($bookingId);
                if ($complaints) {
                    $fullTextSearch[] = trim($complaints);
                }

                //inquiry
                if (isset($booking['original_inquiry_id']) && $booking['original_inquiry_id']) {
                    $inquiry = $modelInquiry->getById($booking['original_inquiry_id']);
                    if ($inquiry['inquiry_num']) {
                        $fullTextSearch[] = trim($inquiry['inquiry_num']);
                    }
                }

                //estimate
                $estimate = $modelBookingEstimate->getByBookingId($bookingId);
                if (isset($estimate['estimate_num']) && $estimate['estimate_num']) {
                    $fullTextSearch[] = trim($estimate['estimate_num']);
                }

                //invoice
                $invoice = $modelBookingInvoice->getByBookingId($bookingId);
                if (isset($invoice['invoice_num']) && $invoice['invoice_num']) {
                    $fullTextSearch[] = trim($invoice['invoice_num']);
                }
                if (isset($invoice['invoice_type']) && $invoice['invoice_type']) {
                    $fullTextSearch[] = trim($invoice['invoice_type']);
                }
            }
        }

        $data = array();
        $data['full_text_search'] = implode(' ', $fullTextSearch);
        $this->updateById($customerId, $data);
    }

    public function checkBeforeDeleteCustomer($cstomerId, &$tables = array()) {

        $sucsess = true;

        $select_inquiry = $this->getAdapter()->select();
        $select_inquiry->from('inquiry', 'COUNT(*)');
        $select_inquiry->where("customer_id = {$cstomerId}");
        $count_inquiry = $this->getAdapter()->fetchOne($select_inquiry);

        if ($count_inquiry) {
            $tables[] = 'inquiry';
            $sucsess = false;
        }

        $select_booking = $this->getAdapter()->select();
        $select_booking->from('booking', 'COUNT(*)');
        $select_booking->where("customer_id = {$cstomerId}");
        $count_booking = $this->getAdapter()->fetchOne($select_booking);

        if ($count_booking) {
            $tables[] = 'booking';
            $sucsess = false;
        }

        $select_payment = $this->getAdapter()->select();
        $select_payment->from('payment', 'COUNT(*)');
        $select_payment->where("customer_id = {$cstomerId}");
        $count_payment = $this->getAdapter()->fetchOne($select_payment);

        if ($count_payment) {
            $tables[] = 'payment';
            $sucsess = false;
        }

        $select_refund = $this->getAdapter()->select();
        $select_refund->from('refund', 'COUNT(*)');
        $select_refund->where("customer_id = {$cstomerId}");
        $count_refund = $this->getAdapter()->fetchOne($select_refund);

        if ($count_refund) {
            $tables[] = 'refund';
            $sucsess = false;
        }

        return $sucsess;
    }

    public function deleteRelatedCustomer($id) {

        //delete data from customer_notes
        $this->getAdapter()->delete('customer_notes', "customer_id = '{$id}'");

        //delete data from customer_commercial_info
        $this->getAdapter()->delete('customer_commercial_info', "customer_id = '{$id}'");

        //delete data from customer_contact
        $this->getAdapter()->delete('customer_contact', "customer_id = '{$id}'");
    }

    public function getCustomerCount($filters = array()) {

        $select = $this->getAdapter()->select();
        $select->from(array('c' => $this->_name));
        $select->where('c.is_deleted = 0');

        if (!isset($filters['company_id'])) {
            $filters['company_id'] = CheckAuth::getCompanySession();
        }

        if ($filters) {

            if (!empty($filters['customer_id'])) {
                $customer_id = (int) $filters['customer_id'];
                $select->where("c.customer_id = '{$customer_id}'");
            }

            if (!empty($filters['startTimeRange']) && !empty($filters['endTimeRange'])) {
                $startTime = $filters['startTimeRange'];
                $endTime = $filters['endTimeRange'];
                $select->where("c.created between '" . mySql2PhpTime($startTime) . "' and '" . mySql2PhpTime($endTime) . "'");
            }

            if (!empty($filters['company_id'])) {
                $company_id = (int) $filters['company_id'];
                $select->where("c.company_id = {$company_id}");
            }

            if (!empty($filters['created_by'])) {
                $created_by = (int) $filters['created_by'];
                $select->where("c.created_by = {$created_by}");
            }
        }

        $sql = $this->getAdapter()->select();
        $sql->from($select, 'COUNT(*)');
        return $this->getAdapter()->fetchOne($sql);
    }

    public function cronJobFiveStarReasonsToChooseTileCleaners() {

        $modelCronjobHistory = new Model_CronjobHistory();
        $modelCronJob = new Model_CronJob();
		$modelMailingListUnsubscribed = new Model_MailingListUnsubscribed();

        //
        // save this cron in cronjob_history table
        //
	    $cronjob = $modelCronJob->getIdByName('five_Star_Reasons_to_Choose_Tile_Cleaners');
        $cronjonID = $cronjob['id'];
		
		$date1 = strtotime('-1 day', time());
        $date2 = time();

        $cronjobHistoryData = array(
            'cron_job_id' => $cronjonID,
            'run_time' => time()
        );
        $cronjobHistoryId = $modelCronjobHistory->insert($cronjobHistoryData);

        $select_inqueries = $this->getAdapter()->select();
        $select_inqueries->from(array('inq' => 'inquiry'));
        $select_inqueries->where("inq.is_deleted = 0");
        $select_inqueries->where("inq.status = 'inquiry'");
        $select_inqueries->joinInner(array('cst' => 'customer'), 'inq.customer_id = cst.customer_id', array('customer_email1' => 'email1', 'customer_email2' => 'email2', 'customer_email3' => 'email3', 'first_name' => 'first_name'));
        //$select_inqueries->where("DATEDIFF(Date(DATE_FORMAT(FROM_UNIXTIME(inq.created),'%Y-%m-%d')),Date(NOW())) > -1");
		$select_inqueries->where("inq.created BETWEEN '" . $date1 . "' AND '" . $date2 . "'");
//        $select_inqueries->where("inq.inquiry_id = 26744");
        $customer_inqueries = $this->getAdapter()->fetchAll($select_inqueries);

        $select_booking = $this->getAdapter()->select();
        $select_booking->from(array('bok' => 'booking'));
        $select_booking->where("bok.is_deleted = 0");
        $select_booking->joinInner(array('cst' => 'customer'), 'bok.customer_id = cst.customer_id', array('customer_email1' => 'email1', 'customer_email2' => 'email2', 'customer_email3' => 'email3', 'first_name' => 'first_name'));
		//$select_booking->where("DATEDIFF(Date(DATE_FORMAT(FROM_UNIXTIME(bok.created),'%Y-%m-%d')),Date(NOW())) > -1");
		 //$select_booking->where("bok.booking_id = 17019");
		 $select_booking->where("bok.created BETWEEN '" . $date1 . "' AND '" . $date2 . "'");
		
		
		$customer_bookings = $this->getAdapter()->fetchAll($select_booking);
//        $customer_bookings = array();
          $items = array_merge($customer_bookings, $customer_inqueries);
		// $items = $customer_bookings;
		
		
        $modelCustomer = new Model_Customer();
        $trading_namesObj = new Model_TradingName();
        foreach ($items as $item) {
            if (isset($item['inquiry_id'])) {
                $item_id = $item['inquiry_id'];
                $type = 'inquiry';
            } else {
                $item_id = $item['booking_id'];
                $type = 'booking';
            }
            if (isset($item['inquiry_id'])) {
                $model_originalInquiry = new Model_OriginalInquiry();
                if ($item['original_inquiry_id'] == 0) {
                    $trading_names = $trading_namesObj->getById($item['trading_name_id']);
                } else {
                    $OriginalInquiryData = $model_originalInquiry->getById($item['original_inquiry_id']);
                    $trading_names = $trading_namesObj->getTradingNameByWebsite($OriginalInquiryData['website']);
                }
            } else {
                $trading_names = $trading_namesObj->getById($item['trading_name_id']);
            }
            $customer = $modelCustomer->getById($item['customer_id']);
			
			$to = array();
			if ($customer['email1']) {
				$to[0] = array('email' => $customer['email1'], 'number' => '1');
            }
            if ($customer['email2']) {
                $to[1] = array('email' => $customer['email2'], 'number' => '2');
            }
            if ($customer['email3']) {
				$to[2] = array('email' => $customer['email3'], 'number' => '3');
            }
			
            $unsubscribeCustomerEmails = $modelMailingListUnsubscribed->getByCronJobIdAndCustomerId($cronjonID, $customer['customer_id']);
			foreach ($unsubscribeCustomerEmails as $customerEmail) {
                    if ($customer['email1'] == $customerEmail['customer_email']) {
                        unset($to[0]);
                    } else if ($customer['email2'] == $customerEmail['customer_email']) {
                        unset($to[1]);
                    } else if ($customer['email3'] == $customerEmail['customer_email']) {
                        unset($to[2]);
                    }
                }
				
				
			//$to[0] = array('email' => 'monahussein69@gmail.com', 'number' => '1');
			//$to[1] = array('email' => 'ayman@tilecleaners.com.au', 'number' => '2');
			
            /*$to = array();
            if ($customer['email1']) {
                $to[] = $customer['email1'];
            }
            if ($customer['email2']) {
                $to[] = $customer['email2'];
            }
            if ($customer['email2']) {
                $to[] = $customer['email3'];
            }*/
//            

            //$to = implode(',', $to);
            ///get the creator of this booking
            $modelUser = new Model_User();
            $user = $modelUser->getById($item_id);
            $template_params = array(
                '{trading_name_id}' => $trading_names['trading_name_id'],
                '{trading_name}' => $trading_names['trading_name'],
                '{phone}'=> '<a href="tel:1300 771 201" style="color:white;"/>1300 771 201</a>'
            );
            $modelEmailTemplate = new Model_EmailTemplate();
            $emailTemplate = $modelEmailTemplate->getEmailTemplate('5_star_reasons_to_choose_tile_cleaners', $template_params, $customer['company_id']);

            $params = array(
                //'to' => $to,
                'reply' => array('name' => $user['display_name'], 'email' => $user['email1']),
                'body' => $emailTemplate['body'],
                'subject' => $emailTemplate['subject'],
                'companyId' => $item['company_id'],
                'trading_name' => $trading_names['trading_name'],
                'from' => $trading_names['email'],
            );

//            var_dump($emailTemplate['body']);
//            exit;
             $email_log = array('reference_id' => $item_id, 'type' => $type, 'cronjob_history_id' => $cronjobHistoryId);
             //$email_log = array();

            if ($to) {
			  foreach ($to as $toEmail) {
			   $params['to'] = $toEmail['email'];
			   $template_params['{unsubscribe_link}'] = 'http://cm.tilecleaners.com.au/unsubscribe-link/' . $cronjonID . '/' . $customer['customer_id'] . '/' .$toEmail['number'] . '/' . $trading_names['trading_name_id'];
			   EmailNotification::sendEmail($params, '5_star_reasons_to_choose_tile_cleaners', $template_params, $email_log);			   
			   }
			}		 
        }
    }
	
	
	public function getByCustomerContactsAndId($contact,$customer_id){
	  
	   $customer = $this->getById($customer_id);
	   $temp_contact = $contact;
	   $mobileContact = $contact;
       $phoneContact = $contact;
	   if(is_numeric($temp_contact)){
            $mobileContact = $this->search($temp_contact, $customer['state'], 1);
            $phoneContact = $this->search($temp_contact, $customer['state'], 0);			
	   }
	    $select = $this->getAdapter()->select();
		$select->from(array('c' => $this->_name));
	    $select->where("customer_id = '{$customer_id}'");
		$select->where("(email1 = '{$contact}' OR email2 = '{$contact}' OR email3 = '{$contact}' OR phone1 = '{$phoneContact}' 
		OR phone2 = '{$phoneContact}' OR phone3 = '{$phoneContact}' OR mobile1 = '{$mobileContact}' OR mobile2 = '{$mobileContact}' OR mobile3 = '{$mobileContact}')");	  

		return $this->getAdapter()->fetchRow($select);
	
	}
	
	
	
	////**********************************************
	/////By IBM
public function updateAllPhoneAndMobileNumbers() { 
	//($filters = array(), $order = null, &$pager = null, $limit = 0 , $perPage = 0, $currentPage = 0) {
		$pager =null;
		//ALTER TABLE `customer` ADD COLUMN `phone1_modified` VARCHAR(100) NULL DEFAULT '' AFTER `phone1`;
		//ALTER TABLE `customer` ADD COLUMN `mobile1_modified` VARCHAR(100) NULL DEFAULT '' AFTER `mobile1`;
		//ALTER TABLE `customer` ADD COLUMN `state_modified` VARCHAR(100) NULL DEFAULT '' AFTER `state`;

//phone1_modified and mobile1_modified are empty 
		//$filters = array('fix_phone_and_mobile'=>1);
		$filters = array('state_is_emplty'=>1);
		//$filters = array('state_modified'=>1);
	$customers = $this->getAll($filters,'customer_id desc',$pager,5000,0,0,1);
	echo 'count '.count($customers);
	foreach($customers as $customer){
		$newPhone = '';
		$phone = $customer['phone1'];
		$mobile = $customer['mobile1'];
		$state = $customer['state'];
		$postcodeINT = $customer['postcode']; 
		
		/*$ptn = "/^0/";  // Regex
		$phone1_modified = $customer['mobile1_modified']; //Your input, perhaps $_POST['textbox'] or whatever
		$rpltxt = "0061";  // Replacement string
		$newPhone = preg_replace($ptn, $rpltxt, $phone1_modified);*/
		$newPhone = '00'.$customer['mobile1_modified'];
		echo '  newPhone '.$newPhone.'   ';
		$data = array('mobile1_modified'=> $newPhone);
		$this->updateById($customer['customer_id'],$data);
		
		/*echo 'phone  '.$phone.'</br>';
		echo 'mobile  '.$mobile.'</br>';
		echo 'state  '.$state.'</br>';
		echo 'postcodeINT  '.$postcodeINT.'</br>';*/
		//print_r($customer);
		
		/*$newState = 'unknown';
		if(empty($state) && !empty($postcodeINT)){
			$newState = $this->getStateAndCityByPostCode($postcodeINT);
			echo '$newState    '.$newState;
			if($postcodeINT == 1234){
				$newState = 'unknown';
			}
			$data = array('state_modified'=> $newState);
			$this->updateById($customer['customer_id'],$data);
		} 
		else if(!empty($state)){
			$data = array('state_modified'=> $state);
			$this->updateById($customer['customer_id'],$data);
		}
		else{
			$data = array('state_modified'=> 'unknown');
			$this->updateById($customer['customer_id'],$data);
		}*/
			
			
			/*$state_modified = $customer['state_modified'];
			$truePhoneNumber = '';
			$trueMobileNumber = '';
		if(!empty($state_modified) && $state_modified != 'unknown'){ 
			if(!empty($phone)){
				$truePhoneNumber = $this->search($phone , $state_modified);
			}
			if(!empty($mobile)){
				$trueMobileNumber = $this->search($mobile , $state_modified);
			}
			//echo 'truePhoneNumber  '.$truePhoneNumber;
			//echo 'trueMobileNumber  '.$trueMobileNumber;
			if(!empty($truePhoneNumber)){
				$data = array('phone1_modified'=> $truePhoneNumber);
			}else{
				//$data = array('phone1_modified'=> 'unknown');
				$data = array('phone1_modified'=> $phone);
			}
			$this->updateById($customer['customer_id'],$data);
			
			if(!empty($trueMobileNumber)){
				$data = array('mobile1_modified'=> $trueMobileNumber);
			}else{
				//$data = array('mobile1_modified'=> 'unknown');
				$data = array('mobile1_modified'=> $mobile); 
			}
			$this->updateById($customer['customer_id'],$data);
			//exit; 
			
			
		}else{
			$data = array('phone1_modified'=> $phone,'mobile1_modified'=> $mobile);
			$this->updateById($customer['customer_id'],$data);
		}*/
	}
	
	
}

public function getStateAndCityByPostCode($postcodeINT,$cityFlag = false){	
		$postcodeINT=(int)$postcodeINT;
		if ( $postcodeINT >=  2600 &&  $postcodeINT<= 2609 ){

		 $city = 'Canberra';
		  $state = 'ACT';
		 
		 

		}

		else if (  $postcodeINT >=  2610 &&  $postcodeINT<= 2620 ){

		 $city = 'Regional ACT';
		  $state = 'ACT';

		}

		else if (  ($postcodeINT >=  2000  &&  $postcodeINT<= 2234 )  || ($postcodeINT >=  1100  &&  $postcodeINT<= 1299)  ){

		 $city = 'Sydney';
		 $state = 'NSW';
		}





		else if (  $postcodeINT >=  2640   &&  $postcodeINT<= 2660 ){

		 $city = 'Riverina Area';
		   $state = 'NSW';
		}


		else if (  $postcodeINT >=  2500    &&  $postcodeINT<= 2534 ){

		 $city = 'Wollongong';
		   $state = 'NSW';
		}



		else if (  $postcodeINT >=  2265     &&  $postcodeINT<= 2333 ){

		 $city = 'Newcastle';
		   $state = 'NSW';

		}


		else if ( preg_match('/^2450/', $postcodeINT ) || ( $postcodeINT >=  2413      &&  $postcodeINT<= 2484) || ($postcodeINT >=  2460      &&  $postcodeINT<= 2465) ){

		 $city = 'Northern Rivers';
			$state = 'NT';

		}

		else if (  $postcodeINT >=  2235  &&  $postcodeINT<= 2999 ){

		 $city = 'Regional NSW';
		   $state = 'NSW';
		}

		else if ( ( $postcodeINT >=3000   &&  $postcodeINT<= 3207)  || ($postcodeINT >=  8000       &&  $postcodeINT<= 8399) ){

		 $city = 'Melbourne';
		   $state = 'VIC';
		}

		else if (  $postcodeINT >=  3208  &&  $postcodeINT<= 3999 ){
		 $city = 'Regional VIC';
		  $state = 'VIC';

		}


		else if ( ( $postcodeINT >=4000  &&  $postcodeINT<= 4207) || ($postcodeINT >=4300    &&  $postcodeINT<= 4305) || ($postcodeINT >=4500    && $postcodeINT<= 4519) || ($postcodeINT >=9000    &&  $postcodeINT<= 9015) ){
		 $city = 'Brisbane';
			$state = 'QLD';	

		}

		else if ( $postcodeINT >=4208 &&  $postcodeINT<= 4287 ){
		 $city = 'Gold Coast';
			 $state = 'QLD';

		}

		else if ( $postcodeINT >=4550  &&  $postcodeINT<= 4575 ){
		 $city = 'Sunshine Coast';
			 $state = 'QLD';

		}

		else if ( ($postcodeINT >=4208 &&  $postcodeINT<= 4299) || ($postcodeINT >=4306     &&  $postcodeINT<= 4499) || ($postcodeINT >=4520     &&  $postcodeINT<= 4999) ){
		 $city = 'Regional QLD';
			$state = 'QLD';

		}




		else if ( $postcodeINT >=5000  && $postcodeINT<= 5199 ){
		 $city = 'Adelaide';
		   $state = 'SA';

		}


		else if ( $postcodeINT >=5200  && $postcodeINT<= 5999 ){
		 $city = 'Regional SA';
			$state = 'SA';

		}



		else if ( preg_match('/^6827/', $postcodeINT )  || ( $postcodeINT >=6000 &&  $postcodeINT<= 6199) || ( $postcodeINT >=6830  &&  $postcodeINT <=6832) || ( $postcodeINT >=6837  &&  $postcodeINT<= 6849) ){
		 $city = 'Perth';
		  $state = 'WA';

		}


		else if ( $postcodeINT >=6200   &&  $postcodeINT<= 6799 ){
		 $city = 'Regional WA';
		   $state = 'WA';

		}

		else if ( $postcodeINT >=7000   &&  $postcodeINT<= 7099 ){
		 $city = 'Hobart';
		  $state = 'TAS';

		}


		else if ( $postcodeINT >=7100  &&  $postcodeINT<= 7999 ){
		 $city = 'Regional TAS';
		  $state = 'TAS';

		}

		else if ( $postcodeINT >=800  &&  $postcodeINT<= 832 ){
		 $city = 'Darwin';
		   $state = 'NT';

		}

		else if ( $postcodeINT >=833  &&  $postcodeINT<= 899 ){
		 $city = 'Regional NT';
			$state = 'NT';

		}

		else {
		 $city = 'unknown';
		   $state = 'unknown';

		}
		
		if($cityFlag){
			$cityState = array();
			$cityState['city'] = $city;
			$cityState['state'] = $state;
			return $cityState;	
		}
		
		return $state;
}



public function search ($phoneNumber , $state) {

	//$phones = array();
	$validNumber = $this->chechPhoneNum($phoneNumber);
	
	

	if($validNumber == 1){

		switch ($state) {
			case 'NSW':
			case 'ACT':
				if(substr($phoneNumber, 0, 2) == '02'){
					$num 	= substr($phoneNumber, 1, 9);
					$phone 	= "0061" . $num;
					//echo "Your phone is: ". $phoneNumber ." and code state is (02) so, full phone Number is: " . $phone;
					return $phone;
					
				}
				break;

			case 'VIC':
			case 'TAS':
				if(substr($phoneNumber, 0, 2) == '03'){
					$num = substr($phoneNumber, 1, 9);
					$phone 	= "0061" . $num;
					//echo "Your phone is: ". $phoneNumber ." and code state is (03) so, full phone Number is: " . $phone;
					return $phone;
				}
				break;

			case 'QLD':
				if(substr($phoneNumber, 0, 2) == '07'){
					$num = substr($phoneNumber, 1, 9);
					$phone 	= "0061" . $num;
					//echo "Your phone is: ". $phoneNumber ." and code state is (07) so, full phone Number is: " . $phone;
					return $phone;
				}
				break;

				case 'SA':
				case 'NT':
				case 'WA':
				if(substr($phoneNumber, 0, 2) == '08'){
					$num = substr($phoneNumber, 1, 9);
					$phone 	= "0061" . $num;
					//echo "Your phone is: ". $phoneNumber ." and code state is (08) so, full phone Number is: " . $phone;
					return $phone;
				} 
				break;
			
			default:
			return '';
				break;
		}
	}
	elseif($validNumber == 2){
		$num = substr($phoneNumber, 1, 9);
		$phone 	= "0061" . $num;
		//echo "This Number: ".$phoneNumber . " for Mobile started with (04) so, full phone Number is: " . $phone;
		return $phone;

	}
	elseif ($validNumber == 3) {
		//echo "This 9 digits Number started with (1) so, it is for Non-geographic numbers (for domestic use only) :" . $phoneNumber;
		return $phoneNumber;
	}
	elseif ($validNumber == 4) {
		//echo "OPPs>> Invalid Number Maybe just for Test !!";
		return '';
		//return $phoneNumber;
	}
	elseif ($validNumber == 5) {		
		switch ($state) {
			case 'NSW':
			case 'ACT':
				if(substr($phoneNumber, 0, 5) == '00612'){
					//echo "This Number: ".$phoneNumber. " is full Number with international Key (0061) and code state is (2)";
					return $phoneNumber;

				}
				break;

			case 'VIC':
			case 'TAS':
				if(substr($phoneNumber, 0, 5) == '00613'){
					//echo "This Number: ".$phoneNumber. " is full Number with international Key (0061) and code state is (3)";
					return $phoneNumber;
				}
				break;

			case 'QLD':
				if(substr($phoneNumber, 0, 5) == '00617'){
					//echo "This Number: ".$phoneNumber. " is full Number with international Key (0061) and code state is (7)";
					return $phoneNumber;
				}
				break;

				case 'SA':
				case 'NT':
				case 'WA':
				if(substr($phoneNumber, 0, 5) == '00618'){
					//echo "This Number: ".$phoneNumber. " is full Number with international Key (0061) and code state is (8)";
					return $phoneNumber;
				}
				break;
			
			default:
			return '';
				break;
		}
	}
	elseif($validNumber == 6){
		//echo "This Number: ".$phoneNumber. " is Mobile Number with international Key (0061) and mobile code state is (4)";
		return $phoneNumber;
	}
	elseif($validNumber == 7){
		switch ($state) {
			case 'NSW':
			case 'ACT':
				if(substr($phoneNumber, 0, 3) == '612'){
					$phone 	= "00" . $phoneNumber;
					//echo "Your phone is: ". $phoneNumber ." is full Number without (00) and code state is (2) so, full phone Number is: " . $phone;
					return $phone;
				//return  "00" . $phoneNumber;
				}
				break;

			case 'VIC':
			case 'TAS':
				if(substr($phoneNumber, 0, 3) == '613'){
					$phone 	= "00" . $phoneNumber;
					//echo "Your phone is: ". $phoneNumber ." is full Number without (00) and code state is (3) so, full phone Number is: " . $phone;
					return $phone;
				}
				break;

			case 'QLD':
				if(substr($phoneNumber, 0, 3) == '617'){
					$phone 	= "00" . $phoneNumber;
					//echo "Your phone is: ". $phoneNumber ." is full Number without (00) and code state is (7) so, full phone Number is: " . $phone;
					return $phone;
				}
				break;

				case 'SA':
				case 'NT':
				case 'WA':
				if(substr($phoneNumber, 0, 3) == '618'){
					$phone 	= "00" . $phoneNumber;
					//echo "Your phone is: ". $phoneNumber ." is full Number without (00) and code state is (8) so, full phone Number is: " . $phone;
					return $phone;
				}
				break;
			
			default:
			return '';
				break;
		}
	}
	elseif($validNumber == 8){
		$phone 	= "00" . $phoneNumber;
		//echo "Your phone is: ". $phoneNumber ." is full Mobile Number without (00) and Mobile code is (4) so, full phone Number is: " . $phone;
		return $phone;

	}
	elseif($validNumber == 9){
		switch ($state) {
			case 'NSW':
			case 'ACT':
				if(substr($phoneNumber, 0, 1) == '2'){
					$phone 	= "0061" . $phoneNumber;
					//echo "This Number: ".$phoneNumber. " is Phone Number without international Key (0061) and start with code state is (2), so full Phone Number is" . $phone;
					return $phone;

				}
				break;

			case 'VIC':
			case 'TAS':
				if(substr($phoneNumber, 0, 1) == '3'){
					$phone 	= "0061" . $phoneNumber;
					//echo "This Number: ".$phoneNumber. " is Phone Number without international Key (0061) and start with code state is (3), so full Phone Number is" . $phone;
					return $phone;
				}
				break;

			case 'QLD':
				if(substr($phoneNumber, 0, 1) == '7'){
					$phone 	= "0061" . $phoneNumber;
					//echo "This Number: ".$phoneNumber. " is Phone Number without international Key (0061) and start with code state is (7), so full Phone Number is" . $phone;
					return $phone;
				}
				break;

				case 'SA':
				case 'NT':
				case 'WA':
				if(substr($phoneNumber, 0, 1) == '8'){
					$phone 	= "0061" . $phoneNumber;
					//echo "This Number: ".$phoneNumber. " is Phone Number without international Key (0061) and start with code state is (8), so full Phone Number is" . $phone;
					return $phone;
				}
				break;
			
			default:
			return '';
				break;
		}
	}
	elseif($validNumber == 10){
		$phone 	= "0061" . $phoneNumber;
		//echo "This Number: ".$phoneNumber. " is for Mobile Number without international Key (0061) and start with code (4), so full Phone Number is" . $phone;
		return $phone;
	}
	elseif($validNumber == 11){
		//echo "OPPs.. there is no phone (Empty Cell)";
		return '';
	}
	elseif($validNumber == 12){
		switch ($state) {
			case 'NSW':
			case 'ACT':
					$phone 	= "00612" . $phoneNumber;
					//echo "Your phone is: ". $phoneNumber ." with 8 digits and your state is: (" .$state .") With code state (02), Then full Number is: " . $phone ;
					return $phone;
				break;

			case 'VIC':
			case 'TAS':
					$phone 	= "00613" . $phoneNumber;
					//echo "Your phone is: ". $phoneNumber ." with 8 digits so your state is: (" .$state .") With Code state (03), Then full Number is: " . $phone ;
					return $phone;
				break;

			case 'QLD':
					$phone 	= "00617" . $phoneNumber;
					//echo "Your phone is: ". $phoneNumber ." with 8 digits so your state is: (" .$state .") With Code state (07), Then full Number is: " . $phone ;
					return $phone;
				break;

				case 'SA':
				case 'NT':
				case 'WA':
					$phone 	= "00618" . $phoneNumber;
					//echo "Your phone is: ". $phoneNumber ." with 8 digits so your state is: (" .$state .") With Code state (08), Then full Number is: " . $phone ;
					return $phone;
				break;
			
			default:
			return '';
				break;
		}
	}elseif ($validNumber == 0) {
		//echo "Wrong Phone Number !!";
		return '';
	}
	elseif($validNumber == 20){
		//echo "we want to update this phoneNumber " . $phoneNumber . " ";
		//this is for Update Mobile Numbers
			//$phone 	= "00614" . $phoneNumber;
			//echo "Your phone is: ". $phoneNumber ." with 8 digits and your state is: (" .$state .") With code state (02), Then full Number is: " . $phone ;
			//return $phone;
		
		switch ($state) {
			case 'NSW':
			case 'ACT':
					$phone 	= "00612" . $phoneNumber;
					//echo "Your phone is: ". $phoneNumber ." with 8 digits and your state is: (" .$state .") With code state (02), Then full Number is: " . $phone ;
					return $phone;
				break;

			case 'VIC':
			case 'TAS':
					$phone 	= "00613" . $phoneNumber;
					//echo "Your phone is: ". $phoneNumber ." with 8 digits so your state is: (" .$state .") With Code state (03), Then full Number is: " . $phone ;
					return $phone;
				break;

			case 'QLD':
					$phone 	= "00617" . $phoneNumber;
					//echo "Your phone is: ". $phoneNumber ." with 8 digits so your state is: (" .$state .") With Code state (07), Then full Number is: " . $phone ;
					return $phone;
				break;

				case 'SA':
				case 'NT':
				case 'WA':
					$num 	= substr($phoneNumber, -8, 8);
					$phone  = "00618" . $num;
					//echo "Now This" .$phone . " Phone updated";
					return $phone;
					
					
				
				break;
			
			default:
			return '';
				break;
		}
		
	}
	
	else{
		return '';
	}
}

public function chechPhoneNum($phoneNumber) {
	//test if there is phone or mobile Number or not
	if(isset($phoneNumber) && $phoneNumber != ""){
		//check if the number length is less than 10 character the number is not correct and return 0 
		if(strlen($phoneNumber) < 8){
			//echo "Wrong Phone Number !!";
			return 0;
		}
		//Check number if it equal to 8 digits (without international Key and state code like: XXXX XXXX)
		elseif (strlen($phoneNumber) == 8) {
			//echo "Suppose This Phone Number without International Key and State Key !" . "</br>";
			return 12;
		}
		elseif (strlen($phoneNumber) > 8 && strlen($phoneNumber) <= 13) {

			if (strlen($phoneNumber) == 9) {
				$checkPhoneKey = substr($phoneNumber, 0, 1);
				if ($checkPhoneKey == '1') {
					return 3;
					//echo "Non-geographic numbers (for domestic use only)";
				}elseif ($checkPhoneKey == '2' || $checkPhoneKey == '3'|| $checkPhoneKey == '7' || $checkPhoneKey == '8') {
					return 9;
					//echo "This is 9 digits Started with 2,3,7,8 for Phones codes";
				}elseif ($checkPhoneKey == '4') {
					return 10 ;
					//echo "This is 9 digits Started with 4 for Mobile code";
				}else {
					return 20;
					//echo "retype number using state";
					// return 0;
					// echo "fault Number just Maybe for Test !!";
				}

			}
			//if number length equal to  10 character (check the fromat without international Key ex: 0201460715)
			elseif (strlen($phoneNumber) == 10) {
				//check number if it for phones or mobile using first two digts ( ex: codes[02,03,07,08] for phones , codes[04]for Mobile, 1 for for domestic use only)
				$checkPhoneKey = substr($phoneNumber, 0, 2);
				//codes[02,03,07,08] for phones
				if ($checkPhoneKey == '02' || $checkPhoneKey == '03' || $checkPhoneKey == '07' || $checkPhoneKey == '08') {
					return 1;
					//echo "This is Phone Number without International Key";
					//codes[04] for Mobile
				}elseif ($checkPhoneKey == '04') {
					return 2;
					//echo "This is Mobile Number without International Key";
					//codes[1] for domestic use
				}else {
					return 20;
					//echo "retype number using state";
					// return 4;
					// echo "fault Number just Maybe for Test !!";
				}
				//check the phone number with international key [0061 or 61]
			}elseif (strlen($phoneNumber) > 10 && strlen($phoneNumber) <= 13) {
				if (substr($phoneNumber, 0, 2) == '00' && substr($phoneNumber, 2, 2) == '61' && strlen($phoneNumber) == 13) {
					//check the phones number with codes[02,03,07,08] with international key 
					$checkPhoneKeyWithInter = substr($phoneNumber, 4, 1);
					if ($checkPhoneKeyWithInter == '2' || $checkPhoneKeyWithInter == '3' || $checkPhoneKeyWithInter == '7' || $checkPhoneKeyWithInter == '8') {
						return 5;
						//echo "This is Phone Number with International Key";
						//check the Mobile number with codes[04] with international key
					}elseif ($checkPhoneKeyWithInter == '4') {
						return 6;
						//echo "This is Mobile Number with International Key";
					}
					else{
						return 20;
						//echo "retype number using state";
						// return 4;
						// echo "fault Number just Maybe for Test !!";	
					}
					//check if phones number was intered with 61 not 0061 international key directly 
				}elseif (substr($phoneNumber, 0, 2) == '61' && strlen($phoneNumber) == 11) {
					//check if the number is related to phones code[02,03,07,08] or Mobile[04]
					$checkPhoneKeyWithInter = substr($phoneNumber, 4, 1);
					if ($checkPhoneKeyWithInter == '2' || $checkPhoneKeyWithInter == '3' || $checkPhoneKeyWithInter == '7' || $checkPhoneKeyWithInter == '8') {
						return 7;
						//echo "This is Phone Number with International Key";
					}elseif ($checkPhoneKeyWithInter == '4') {
						return 8;
						//echo "This is Mobile Number with International Key";
					}else{
						return 20;
						//echo "retype number using state";
						// return 4;
						// echo "fault Number just Maybe for Test !!";
					}
				}else{
					return 20;
					//echo "retype number using state";
					// return 4;
					// echo "fault Number just Maybe for Test !!";
				}
			}else{
				return 20;
				//echo "retype number using state";				
				// return 4;
				// echo "fault Number just Maybe for Test !!";
			}

		}elseif (strlen($phoneNumber) > 13 ) {
			//return 20;
			//echo "retype number using state";
			 return 4;
			 //echo "fault Number Up to 13 Digits |  just Maybe for Test !!";
		}else{
			return 4;
			//echo "fault Number just Maybe for Test !!";
		}
	}else{
	return 11;
	//echo "OPPs.. there is no phone empty cell";
	}
}


    
    public function getContactByCustomerId($id) 
    {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name, array('mobile1 as mobile1', 'mobile2 as mobile2','mobile3 as mobile3'));
        $select->joinLeft('customer_contact', 'customer.customer_id = customer_contact.customer_id', 
        array('mobile1 as contactMobile1','mobile2 as contactMobile2','mobile3 as contactMobile3'));
        $select->where("customer.customer_id = '{$id}'");
        return $this->getAdapter()->fetchRow($select);
    }




	public function getAllWithoutJoin(){
		$select = $this->getAdapter()->select(); 
        $select->from(array('c' => $this->_name));
		$select->order('customer_id');
		$select->limit(2000);
		//$select->limit(2000, 2000);
		return $this->getAdapter()->fetchAll($select);
	}
	
	public function getLonAndLatForCustomers(){
		
		$customers = $this->getAllWithoutJoin();
		
		foreach($customers as $customer){
			$address = array();
			$address['unit_lot_number'] = $customer['unit_lot_number'];
			$address['street_number'] = $customer['street_number'];
			$address['street_address'] = $customer['street_address'];
			$address['suburb'] = $customer['suburb'];
			$address['state'] = $customer['state'];
			$address['postcode'] = $customer['postcode'];
			$modelBookingAddress = new Model_BookingAddress();
            $geocode = $modelBookingAddress->getLatAndLon($address);
			$lat = $geocode['lat'] ? $geocode['lat'] : 0;
            $lon = $geocode['lon'] ? $geocode['lon'] : 0;
			$data = array(
				'customer_lat' => $lat,
				'customer_lon' => $lon
			);
			$this->updateById($customer['customer_id'], $data);
		}
	} 
	
}
