<?php

class Model_Services extends Zend_Db_Table_Abstract {

    protected $_name = 'service';

    /**
     * get table rows according to the assigned filters and page
     *
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from(array('s' => $this->_name));
        $select->joinInner(array('cp' => 'company'), 's.company_id = cp.company_id', array('cp.company_name'));
        $select->order($order);

        if (!isset($filters['company_id'])) {
            $filters['company_id'] = CheckAuth::getCompanySession();
        }

        if ($filters) {
            if (!empty($filters['keywords'])) {
                $keywords = $this->getAdapter()->quote('%' . $filters['keywords'] . '%');
                $select->where("s.service_name LIKE {$keywords}");
            }
            if (!empty($filters['company_id'])) {
                $select->where("s.company_id = {$filters['company_id']}");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table row according to the assigned $id to the submited $data
     *
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "service_id= '{$id}'");
    }

    /**
     * delete table row according to the assigned $id
     *
     * @param int $id
     * @return boolean
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("service_id= '{$id}'");
    }

    /**
     * get table row according to the assigned $id
     *
     * @param int $id
     * @return array
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("service_id= '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table rows according to the assigned $serviceName
     *
     * @param string $serviceName
     * @return array
     */
    public function getByServiceName($serviceName) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("service_name= '{$serviceName}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function getByServiceNameAndCompanyId($serviceName, $companyId) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("service_name= '{$serviceName}'");
        $select->where("company_id = {$companyId}");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table all rows from services as array (id=> '' , name=>'')
     *
     * @return array
     */
    public function getServiceAsArray() {

        $services = $this->getAll();

        $data = array();
        foreach ($services as $service) {
			if(!empty($service['service_name'])){

            $data[] = array(
                'id' => $service['service_id'],
                'name' => $service['service_name']
            );
			}
        }
        return $data;
    }

    /**
     * get table all rows from services
     *
     * @return array
     */
    public function getAllService() {

        $services = $this->getAll();

        $data = array();
        foreach ($services as $service) {
            $data[$service['service_id']] = $service['service_name'];
        }
        return $data;
    }

    public function checkBeforeDeleteService($serviceId, &$tables = array()) {

        $sucsess = true;

        $select_inquiry_service = $this->getAdapter()->select();
        $select_inquiry_service->from('inquiry_service', 'COUNT(*)');
        $select_inquiry_service->where("service_id = {$serviceId}");
        $count_inquiry_service = $this->getAdapter()->fetchOne($select_inquiry_service);

        if ($count_inquiry_service) {
            $tables[] = 'inquiry_service';
            $sucsess = false;
        }


        $select_contractor_service = $this->getAdapter()->select();
        $select_contractor_service->from('contractor_service', 'COUNT(*)');
        $select_contractor_service->where("service_id = {$serviceId}");
        $count_contractor_service = $this->getAdapter()->fetchOne($select_contractor_service);

        // the servicee saved to general contractror by default , so if not used in other contractor we can deleted
        if ($count_contractor_service) {
            $modelContractorService = new Model_ContractorService();
            $justBelong = $modelContractorService->checkIfThisServiceJustBelongsToGeneralContractor($serviceId);
            if (!$justBelong) {
                $tables[] = 'contractor_service';
                $sucsess = false;
            }
        }


        $select_contractor_service_booking = $this->getAdapter()->select();
        $select_contractor_service_booking->from('contractor_service_booking', 'COUNT(*)');
        $select_contractor_service_booking->where("service_id = {$serviceId}");
        $count_contractor_service_booking = $this->getAdapter()->fetchOne($select_contractor_service_booking);

        if ($count_contractor_service_booking) {
            $tables[] = 'contractor_service_booking';
            $sucsess = false;
        }

        $select_contractor_service_booking_temp = $this->getAdapter()->select();
        $select_contractor_service_booking_temp->from('contractor_service_booking_temp', 'COUNT(*)');
        $select_contractor_service_booking_temp->where("service_id = {$serviceId}");
        $count_contractor_service_booking_temp = $this->getAdapter()->fetchOne($select_contractor_service_booking_temp);

        if ($count_contractor_service_booking_temp) {
            $tables[] = 'contractor_service_booking_temp';
            $sucsess = false;
        }

        $select_service_attribute = $this->getAdapter()->select();
        $select_service_attribute->from('service_attribute', 'COUNT(*)');
        $select_service_attribute->where("service_id = {$serviceId}");
        $count_service_attribute = $this->getAdapter()->fetchOne($select_service_attribute);

        if ($count_service_attribute) {
            $tables[] = 'service_attribute';
            $sucsess = false;
        }

        return $sucsess;
    }

    public function getAllByItem($id, $type) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from(array('s' => 'service'));
        if ($type == 'booking' or $type == 'customer_rating') {
            $select->joinInner(array('c' => 'contractor_service_booking'), 's.service_id = c.service_id');
            $select->where("c.booking_id= '{$id}'");
        } else if ($type == 'estimate') {
            $select->joinInner(array('c' => 'contractor_service_booking'), 's.service_id = c.service_id');
            $select->joinInner(array('be' => 'booking_estimate'), 'be.booking_id = c.booking_id');
            $select->where("be.id= '{$id}'");
        } else if ($type == 'inquiry') {
            $select->joinInner(array('is' => 'inquiry_service'), 's.service_id = is.service_id');
            $select->where("is.inquiry_id = '{$id}'");
        }



        return $this->getAdapter()->fetchAll($select);
    }

    public function getAllForEstimate($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from(array('s' => 'service'));
        $select->joinInner(array('c' => 'contractor_service_booking'), 's.service_id = c.service_id');
        $select->joinInner(array('be' => 'booking_estimate'), 'be.booking_id = c.booking_id');
        $select->where("be.id= '{$id}'");
        $services = $this->getAdapter()->fetchAll($select);

        $data = array();
        foreach ($services as $service) {
            $data[$service['service_id']] = $service['service_name'];
        }
        return $data;
    }

    public function getAllByItemAsArray($id, $type) {

        $services = $this->getAllByItem($id, $type);
        $data = array();
        foreach ($services as $service) {
            $data[$service['service_id']] = $service['service_name'];
        }
        return $data;
    }

    public function insert(array $data) {

        return parent::insert($data);
    }


	public function executeQuery(){

	  $this->getAdapter()->query("
		ALTER TABLE `service_attachment`
	CHANGE COLUMN `is_default` `is_default` TINYINT NULL DEFAULT '0'");

	}

  //reem start

  // -- list all services which are provided by contractor X in city Y
  // SELECT * FROM `contractor_service` AS `cs` INNER JOIN `contractor_service_availability` AS `csa` ON cs.contractor_service_id= csa.contractor_service_id WHERE (contractor_id = '1') AND (csa.city_id = '20')

  public function getServicesByContractorAndCity($contractorName,$cityName) {
    $model_Cities = new Model_Cities();
    $city_rs = $model_Cities->getByName($cityName);
    $city_id= $city_rs['city_id'];
    
      $select = $this->getAdapter()->select();
      $select->from(array('cs' => 'contractor_service'));
      $select->joinInner(array('usr' => 'user'), 'cs.contractor_id= usr.user_id');

      $select->joinInner(array('csa' => 'contractor_service_availability'), 'cs.contractor_service_id= csa.contractor_service_id');

      $select->where("usr.username = '{$contractorName}'");
      $select->where("csa.city_id = '{$city_id}'");
     return count($this->getAdapter()->fetchAll($select));

  }

  // -- list all services of booking num X
  // SELECT * FROM `service` AS `serv` INNER JOIN `contractor_service_booking` AS `csb` ON serv.contractor_service_id= csa.contractor_service_id WHERE (contractor_id = '1') AND (csa.city_id = '20')

  public function getServicesByBookingNum($company_id,$booking_num) {
        $select = $this->getAdapter()->select();
      $select->from(array('serv' => 'service'));
      $select->joinInner(array('csb' => 'contractor_service_booking'), 'serv.service_id= csb.service_id');
      $select->joinInner(array('bok' => 'booking'), 'bok.booking_id= csb.booking_id');
      $select->where("serv.company_id = '{$company_id}'");
      $select->where("bok.booking_num = '{$booking_num}'");
     return count($this->getAdapter()->fetchAll($select));


  }


  //reem end

    public function getServiceNameByCompanyId($companyId) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("company_id = {$companyId}");
        return $this->getAdapter()->fetchAll($select);
    }

}
