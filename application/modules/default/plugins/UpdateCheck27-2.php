<?php

class Plugin_UpdateCheck extends Zend_Controller_Plugin_Abstract {

    public function preDispatch(Zend_Controller_Request_Abstract $request) {
        parent::preDispatch($request);

        $controllerName = $request->getControllerName();
        $actionName = $request->getActionName();
        $moduleName = $request->getModuleName();

        $layout = Zend_Layout::getMvcInstance();
        $view = $layout->getView();

        $loggedUser = CheckAuth::getLoggedUser();
        if ($loggedUser && CheckAuth::getRoleName() != 'customer') {
            $modelBookingStatus = new Model_BookingStatus();

            //By Nour
            $model_BookingEstimate = new Model_BookingEstimate();
            $countAllBookingEstimate = $model_BookingEstimate->getCount(array('estimate_type' => 'draft'));
            $countmyBookingEstimate = $model_BookingEstimate->getCount(array('to_follow' => true, 'to_follow_date' => 'today'));
            $view->countBookingEstimate = $countAllBookingEstimate[0]['count'];
            $view->countmyBookingEstimate = $countmyBookingEstimate[0]['count'];

            $countToFollow = $model_BookingEstimate->getCount(array('to_follow' => true, 'estimate_type' => 'draft'));
            $view->countToFollow1 = $countToFollow[0]['count'];
            $time = time();
            /* $countToDraft =  $model_BookingEstimate ->getCount(array('estimate_type' => 'draft'));

              $view->countToDraft = $countToDraft[0]['count']; */
            //18-11
            $model_Inquiry = new Model_Inquiry();
            $countToFollowInquiry = $model_Inquiry->getCount(array('status' => 'inquiry', 'to_follow' => true, 'to_follow_date' => 'past'));
            $view->countToFollowInquiry = $countToFollowInquiry[0]['count'];


            //End By Nour
            //By Abdallah
            // count inquiry 
            $model_inquiry = new Model_Inquiry();
            $countInquiry = $model_inquiry->getCountInquiry(false);
            $countmyInquiry = $model_inquiry->getCountInquiry(true);
            $view->countInquiry = $countInquiry;
            $view->countmyInquiry = $countmyInquiry;


            $model_BookingInvoice = new Model_BookingInvoice();
            $countOfUnpaid = $model_BookingInvoice->getCount(array('invoice_type' => 'unpaid'));
            $countOfOpen = $model_BookingInvoice->getCount(array('invoice_type' => 'open'));
            $countOfOverdue = $model_BookingInvoice->getCount(array('invoice_type' => 'overdue'));
            $view->countOfUnpaid = $countOfUnpaid;
            $view->countOfOpen = $countOfOpen;
            $view->countOfOverdue = $countOfOverdue;



            $modelBooking = new Model_Booking();
            $modelComplaint = new Model_Complaint();
            $mongo = new Model_Mongo();

            $countAwaitingupdateBooking = $modelBooking->getCountAwaitingupdateBooking();
            $view->countAwaitingupdateBooking = $countAwaitingupdateBooking;

            //D.A 26/08/2015
            /* $loggedUser = CheckAuth::getLoggedUser();
              $companyId = CheckAuth::getCompanySession();
              $roleName = CheckAuth::getRoleName();
              if ($roleName == 'contractor') {
              $model = new Model_GmailServiceAccount($loggedUser['user_id'], 'contractor');
              } else{
              $model = new Model_GmailServiceAccount($companyId);
              }
              $view->gmailEmailsCount = $model->getAllEmailsCount();

              $modelEmailLog = new Model_EmailLog();
              $view->localEmailsCount = $modelEmailLog->getLocalEmailsCount($loggedUser['user_id']); */
//D.A 29/07/2015



            $filters = array('convert_status' => 'booking', 'to_follow' => true);

            $countToFollowBooking = $modelBooking->getCountToFollowBooking($filters);
            $view->countToFollowBooking = $countToFollowBooking['count'];

            $countAwaitingacceptBooking = $modelBooking->getCountAwaitingAcceptBooking();
            $view->countAwaitingacceptBooking = $countAwaitingacceptBooking;

            $countUnapprovedBooking = $modelBooking->getCountUnapprovedBooking();
            $view->countUnapprovedBooking = $countUnapprovedBooking;

            //by Salim 

            $today = getTimePeriodByName('today');
            $tomorrow = getTimePeriodByName('tomorrow');

//            if(my_ip()){
//                var_dump($today);
//            }
//            $tomorrowUrl = $this->escape($this->url(array(), 'booking') . '?fltr[booking_start_between]=' . $tomorrow['start'] . '&fltr[booking_end_between]=' . $tomorrow['end'] . '&fltr[multiple_start_between]=' . $tomorrow['start'] . '&fltr[multiple_end_between]=' . $tomorrow['end']);
            //get status_id of Cancelled and On Hold bookings, to exclude them from today, tomorrow and future bookings
            $cancelledStatus = $modelBookingStatus->getByStatusName('CANCELLED');
            $onHoldStatus = $modelBookingStatus->getByStatusName('ON HOLD');

            $primaryTodayFilters = array(
                'booking_start_between' => $today['start'],
                'booking_end_between' => $today['end'],
                'withoutEstimateStatus' => 1,
                'exclude_more_one_status' => $cancelledStatus['booking_status_id'] . ',' . $onHoldStatus['booking_status_id']
            );
            $multipleTodayFilter = array(
                'multiple_start_between' => $today['start'],
                'multiple_end_between' => $today['end'],
                'withoutEstimateStatus' => 1,
                'exclude_more_one_status' => $cancelledStatus['booking_status_id'] . ',' . $onHoldStatus['booking_status_id']
            );

            $sameMultipleTodayFilter = array(
                'multiple_start_between' => $today['start'],
                'multiple_end_between' => $today['end'],
                'same_dates' => 1,
                'withoutEstimateStatus' => 1,
                'exclude_more_one_status' => $cancelledStatus['booking_status_id'] . ',' . $onHoldStatus['booking_status_id']
            );

            $primaryTomorrowFilters = array(
                'booking_start_between' => $tomorrow['start'],
                'booking_end_between' => $tomorrow['end'],
                'withoutEstimateStatus' => 1,
                'exclude_more_one_status' => $cancelledStatus['booking_status_id'] . ',' . $onHoldStatus['booking_status_id']
            );
            $multipleTomorrowFilter = array(
                'multiple_start_between' => $tomorrow['start'],
                'multiple_end_between' => $tomorrow['end'],
                'withoutEstimateStatus' => 1,
                'exclude_more_one_status' => $cancelledStatus['booking_status_id'] . ',' . $onHoldStatus['booking_status_id']
            );

            $sameMultipleTomorrowFilter = array(
                'multiple_start_between' => $tomorrow['start'],
                'multiple_end_between' => $tomorrow['end'],
                'same_dates' => 1,
                'withoutEstimateStatus' => 1,
                'exclude_more_one_status' => $cancelledStatus['booking_status_id'] . ',' . $onHoldStatus['booking_status_id']
            );


            $futureFilters = array(
                'booking_not_started_yet' => true,
                'multiple_not_started_yet' => true,
                'withoutEstimateStatus' => 1,
                'exclude_more_one_status' => $cancelledStatus['booking_status_id'] . ',' . $onHoldStatus['booking_status_id']
            );
            $pager = null;
//            echo "test";
//            $countToday = $modelBooking->getAll($todayFilters, null, $pager, 0, 0, 0, 1);
//            $countTomorrow = $modelBooking->getAll($tomorrowFilters, null, $pager, 0, 0, 0, 1);
            $countFuture = $modelBooking->getAll($futureFilters, null, $pager, 0, 0, 0, 1);
            $inProcessStatus = $modelBookingStatus->getByStatusName('IN PROGRESS');
            $toDo = $modelBookingStatus->getByStatusName('TO DO');
            $toVisit = $modelBookingStatus->getByStatusName('TO VISIT');
            $notAssignedFilters = array(
                'withoutEstimateStatus' => 1,
                'more_one_status' => $inProcessStatus['booking_status_id'] . ',' . $toDo['booking_status_id'] . ',' . $toVisit['booking_status_id'],
                'notAssigned' => 1
            );




            $notAssignedCount = $modelBooking->getAll($notAssignedFilters, null, $pager, 0, 0, 0, 1);
            $countRejectBooking = $modelBooking->getCountRejectBooking();

            $view->countNotAssigned = $notAssignedCount[0]['count'] + $countRejectBooking;

            $view->countRejectBooking = $countRejectBooking;

            $newRequestsFilters = array(
                'acceptance' => 'notAcceptNorReject',
                'withoutEstimateStatus' => 1,
                'booking_not_started_yet' => true
            );
            $toConfirmFilters = array(
                'to_confirm' => true,
                'next_working_day' => true,
                'withoutEstimateStatus' => 1
            );
            $countNewRequests = $modelBooking->getAll($newRequestsFilters, null, $pager, 0, 0, 0, 1);
            $countToConfirm = $modelBooking->getAll($toConfirmFilters, null, $pager, 0, 0, 0, 1);

            $AllNotMultipleBookingToday = $modelBooking->getAll($primaryTodayFilters, null, $pager, 0, 0, 0, 1);
            $AllMultipleBookingToday = $modelBooking->getAll($multipleTodayFilter, null, $pager, 0, 0, 0, 1);
            $sameMultipleBookingToday = $modelBooking->getAll($sameMultipleTodayFilter, null, $pager, 0, 0, 0, 1);

            $AllNotMultipleBookingTomorrow = $modelBooking->getAll($primaryTomorrowFilters, null, $pager, 0, 0, 0, 1);
            $AllMultipleBookingTomorrow = $modelBooking->getAll($multipleTomorrowFilter, null, $pager, 0, 0, 0, 1);
            $sameMultipleBookingTomorrow = $modelBooking->getAll($sameMultipleTomorrowFilter, null, $pager, 0, 0, 0, 1);
            if ((!isset($sameMultipleBookingTomorrow[0]['count']))) {
                $sameData = 0;
            } else {
                $sameData = $sameMultipleBookingTomorrow[0]['count'];
            }

            if ((!isset($sameMultipleBookingToday[0]['count']))) {
                $sameDataToday = 0;
            } else {
                $sameDataToday = $sameMultipleBookingToday[0]['count'];
            }
//                echo $sameMultipleBookingTomorrow[0]['count'];
//            $AllNotMultipleBooking = $modelBooking->getAll($primaryTodayFilters, null, $pager, 0, 0, 0, 1);
//            $AllMultipleBooking = $modelBooking->getAll($multipleTodayFilter, null, $pager, 0, 0, 0, 1);

            $view->countToday = abs(($AllNotMultipleBookingToday[0]['count'] + $AllMultipleBookingToday[0]['count']) - $sameDataToday);
            $view->countTomorrow = abs(($AllNotMultipleBookingTomorrow[0]['count'] + $AllMultipleBookingTomorrow[0]['count']) - $sameData);


            $countDuplicates = $model_BookingInvoice->getDuplicatedInvoiceNumbers(1);

//            
//              $view->countTomorrow = $countTomorrow[0]['count'];
            $view->countFuture = $countFuture[0]['count'];
            $view->countNewRequests = $countNewRequests[0]['count'];
            $view->countToConfirm = $countToConfirm[0]['count'];
            $view->countDuplicates = $countDuplicates;

//            $view->countToday = 0;
//            $view->countTomorrow = 0;
//            $view->countFuture = $countFuture[0]['count'];
//            $view->countNewRequests = 0;
//            $view->countToConfirm = 0;
//            $view->countDuplicates = 0;

            $countUapprovedCompalints = $modelComplaint->countUapprovedCompalints();
            $view->countUapprovedCompalints = $countUapprovedCompalints;
            $countInProcessBooking = $modelBooking->getCountInProcessBooking();
            $view->countInProcessBooking = $countInProcessBooking;






            $countOpenComplaint = $modelComplaint->getComplaintCount(array('complaintStatus' => 'open', 'is_approved' => '1'));
            $view->countOpenComplaint = $countOpenComplaint;



            $countOpenComplaint = $modelComplaint->getCount(array('complaintStatus' => 'open', 'is_approved' => '1'));
            $view->countOpenComplaintNew = $countOpenComplaint;

            $view->countNotifincations = $mongo->countNotifications();
            // request owners

            $modelClaimOwner = new Model_ClaimOwner();
            $countClaimOwner = $modelClaimOwner->getCountClaimOwner();
            $view->countClaimOwner = $countClaimOwner;

            //missed call
            $modelMissedCalls = new Model_MissedCalls();
            $countMissedCalls = $modelMissedCalls->getCountMissedCalls();
            $view->countMissedCalls = $countMissedCalls;
            //new refunds
            $modelRefund = new Model_Refund();
            $newRefundsFilters = array('is_approved' => 'no');
            $newRefunds = $modelRefund->getAll($newRefundsFilters);
            $view->countNewRefunds = count($newRefunds);

            // Unapproved Account
            $modelPayment = new Model_Payment();
            $countUnapprovedPayments = $modelPayment->getCountUnapprovedPayments();
            $view->countUnapprovedPayments = $countUnapprovedPayments;

            $modelBookingStatus = new Model_BookingStatus();
            $inProcess = $modelBookingStatus->getByStatusName('IN PROGRESS');

            $url = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '';
            if (strpos($url, 'add') == false && strpos($url, 'edit') == false && strpos($url, 'save') == false && strpos($url, 'view') == false && strpos($url, 'invoice/') == false && strpos($url, 'ewayAdd') == false) {
                if ('contractor' == CheckAuth::getRoleName()) {
                    //$flashMessenger = new Zend_Controller_Action_Helper_FlashMessenger();
                    if ($countAwaitingupdateBooking && $countInProcessBooking) {
                        $view->messages[] = (array('type' => 'error', 'message' => "<div>You have ({$countAwaitingupdateBooking}) Booking awaiting update, please <a href=\"/booking/awaiting_update\">update</a> your previous Booking(s) first.</div><div  style=\"padding-left: 40px;\">You have ({$countInProcessBooking}) Booking InProcess, please <a href=\"/booking?fltr[status]={$inProcess['booking_status_id']}\">update</a> your previous Booking(s) first. </div>"));
                    } elseif ($countAwaitingupdateBooking) {
                        $view->messages[] = (array('type' => 'error', 'message' => "You have ({$countAwaitingupdateBooking}) Booking awaiting update, please <a href=\"/booking/awaiting_update\">update</a> your previous Booking(s) first."));
                    } elseif ($countInProcessBooking) {
                        $view->messages[] = (array('type' => 'error', 'message' => "You have ({$countInProcessBooking}) Booking InProgress, please <a href=\"/booking?fltr[status]={$inProcess['booking_status_id']}\">update</a> your previous Booking(s) first."));
                    }
                }
            }
        }
    }

}
