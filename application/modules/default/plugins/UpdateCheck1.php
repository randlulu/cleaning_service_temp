<?php
class Plugin_UpdateCheck extends Zend_Controller_Plugin_Abstract {

    public function preDispatch(Zend_Controller_Request_Abstract $request) {
        parent::preDispatch($request);

        $controllerName = $request->getControllerName();
        $actionName = $request->getActionName();
        $moduleName = $request->getModuleName();

        $layout = Zend_Layout::getMvcInstance();
        $view = $layout->getView();

        $loggedUser = CheckAuth::getLoggedUser();
        if ($loggedUser) {
            $modelBooking = new Model_Booking();

            $countAwaitingupdateBooking = $modelBooking->getCountAwaitingupdateBooking();
            $view->countAwaitingupdateBooking = $countAwaitingupdateBooking;

			$countAwaitingacceptBooking = $modelBooking->getCountAwaitingAcceptBooking();
            $view->countAwaitingacceptBooking = $countAwaitingacceptBooking;

            $countUnapprovedBooking = $modelBooking->getCountUnapprovedBooking();
            $view->countUnapprovedBooking = $countUnapprovedBooking;

            $countInProcessBooking = $modelBooking->getCountInProcessBooking();
            $view->countInProcessBooking = $countInProcessBooking;

            // request owners
            $modelClaimOwner = new Model_ClaimOwner();
            $countClaimOwner = $modelClaimOwner->getCountClaimOwner();
            $view->countClaimOwner = $countClaimOwner;

            //missed call
            $modelMissedCalls = new Model_MissedCalls();
            $countMissedCalls = $modelMissedCalls->getCountMissedCalls();
            $view->countMissedCalls = $countMissedCalls;

            // Unapproved Account
            $modelPayment = new Model_Payment();
            $countUnapprovedPayments = $modelPayment->getCountUnapprovedPayments();
            $view->countUnapprovedPayments = $countUnapprovedPayments;

            $modelBookingStatus = new Model_BookingStatus();
            $inProcess = $modelBookingStatus->getByStatusName('IN PROGRESS');
			
			$url = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '';
			if (strpos($url,'add') == false && strpos($url,'edit') == false && strpos($url,'save') == false && strpos($url,'view') == false && strpos($url,'invoice/') == false) {
				
				if ('contractor' == CheckAuth::getRoleName()) {
					//$flashMessenger = new Zend_Controller_Action_Helper_FlashMessenger();
					if ($countAwaitingupdateBooking && $countInProcessBooking) {
						$view->messages[] = (array('type' => 'error', 'message' => "<div>You have ({$countAwaitingupdateBooking}) Booking awaiting update, please <a href=\"/booking/awaiting_update\">update</a> your previous Booking(s) first.</div><div  style=\"padding-left: 40px;\">You have ({$countInProcessBooking}) Booking InProcess, please <a href=\"/booking?fltr[status]={$inProcess['booking_status_id']}\">update</a> your previous Booking(s) first. </div>"));
					} elseif ($countAwaitingupdateBooking) {
						$view->messages[] = (array('type' => 'error', 'message' => "You have ({$countAwaitingupdateBooking}) Booking awaiting update, please <a href=\"/booking/awaiting_update\">update</a> your previous Booking(s) first."));
					} elseif ($countInProcessBooking) {
						$view->messages[] = (array('type' => 'error', 'message' => "You have ({$countInProcessBooking}) Booking InProcess, please <a href=\"/booking?fltr[status]={$inProcess['booking_status_id']}\">update</a> your previous Booking(s) first."));
					}
				}
			
			}
        }
    }

}