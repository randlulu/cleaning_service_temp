<?php

class Model_Customer extends Zend_Db_Table_Abstract {

    protected $_name = 'customer';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null, $limit = 0) {
        $joinInner = array();
        $select = $this->getAdapter()->select();
        $select->from(array('c' => $this->_name));
        $select->distinct();
        $select->joinInner(array('ci' => 'city'), 'ci.city_id = c.city_id', array('ci.city_name'));
        $select->joinInner(array('co' => 'country'), 'ci.country_id = co.country_id', array('co.country_name'));
        $select->order($order);

        $filters['company_id'] = CheckAuth::getCompanySession();

        if (!empty($filters['is_deleted'])) {
            $select->where('c.is_deleted = 1');
        } else {
            $select->where('c.is_deleted = 0');
        }

        $loggedUser = CheckAuth::getLoggedUser();
        if (!CheckAuth::checkCredential(array('canSeeCustomerList'))) {
            if (CheckAuth::checkCredential(array('viewHisCustomer'))) {
                $filters['created_by'] = $loggedUser['user_id'];
            }
        }

        if ($filters) {

            if (!empty($filters['keywords'])) {
                $keywords = $this->getAdapter()->quote('%' . trim($filters['keywords']) . '%');
                $select->where("c.full_text_search LIKE {$keywords}");

                //$keywords = '+' . preg_replace('#[\s]+#', ' +', trim($filters['keywords']));
                //$keywords = $this->getAdapter()->quote($keywords);
                //$select->where("MATCH(c.full_text_search) AGAINST({$keywords} IN BOOLEAN MODE)");
            }

            if (!empty($filters['full_name_search'])) {
                $keywords = $this->getAdapter()->quote('%' . trim($filters['full_name_search']) . '%');
                $select->where("c.full_name_search LIKE {$keywords}");
            }

            if (!empty($filters['customer_type_id'])) {
                $select->where('c.customer_type_id = ' . $filters['customer_type_id']);
            }

            if (!empty($filters['created_by'])) {
                $select->where('c.created_by = ' . $filters['created_by']);
            }

            if (!empty($filters['duplicate'])) {
                $select->where('c.duplicate > 1');
            }

            if (!empty($filters['customer_id'])) {
                $filters['customer_id'] = (int) $filters['customer_id'];
                $select->where("c.customer_id = {$filters['customer_id']} ");
            }

            if (!empty($filters['email'])) {
                $email = trim($filters['email']);
                $select->where("CONCAT_WS(' ',email1 ,email2 ,email3) LIKE '%{$email}%'");
            }

            if (!empty($filters['phone'])) {
                $phone = trim($filters['phone']);
                $select->where("CONCAT_WS(' ',phone1 ,phone2 ,phone3) LIKE '%{$phone}%'");
            }

            if (!empty($filters['mobile'])) {
                $mobile = trim($filters['mobile']);
                $select->where("CONCAT_WS(' ',mobile1 ,mobile2 ,mobile3) LIKE '%{$mobile}%'");
            }

            if (!empty($filters['mobile/phone'])) {
                $mobile_phone = $this->getAdapter()->quote('%' . trim($filters['mobile/phone']) . '%');
                $select->where("CONCAT_WS(' ',phone1,phone2,phone3,mobile1,mobile2,mobile3) LIKE {$mobile_phone}");
            }

            if (!empty($filters['company_id'])) {
                $company_id = (int) $filters['company_id'];
                $select->where("c.company_id = {$company_id}");
            }

            if (!empty($filters['city_id'])) {
                $city_id = (int) $filters['city_id'];
                $select->where("c.city_id = {$city_id}");
            }

            if (!empty($filters['have_inquiry'])) {
                $joinInner['inquiry'] = array('name' => array('inq' => 'inquiry'), 'cond' => 'c.customer_id = inq.customer_id', 'cols' => '');
            }

            if (!empty($filters['have_booking'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.customer_id = bok.customer_id', 'cols' => '');
            }

            if (!empty($filters['have_estimate'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.customer_id = bok.customer_id', 'cols' => '');
                $joinInner['booking_estimate'] = array('name' => array('est' => 'booking_estimate'), 'cond' => 'est.booking_id = bok.booking_id', 'cols' => '');
            }

            if (!empty($filters['have_invoice'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.customer_id = bok.customer_id', 'cols' => '');
                $joinInner['booking_invoice'] = array('name' => array('inv' => 'booking_invoice'), 'cond' => 'inv.booking_id = bok.booking_id', 'cols' => '');
            }

            if (!empty($filters['have_complaint'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.customer_id = bok.customer_id', 'cols' => '');
                $joinInner['complaint'] = array('name' => array('comp' => 'complaint'), 'cond' => 'bok.booking_id = comp.booking_id', 'cols' => '');
            }

            if (!empty($filters['have_nothing'])) {
                $select->joinLeft(array('linq' => 'inquiry'), 'linq.customer_id = c.customer_id', '');
                $select->joinLeft(array('lbok' => 'booking'), 'lbok.customer_id = c.customer_id', '');
                $select->where("linq.customer_id IS NULL");
                $select->where("lbok.customer_id IS NULL");
            }

            if (!empty($filters['bussiness_name'])) {
                $joinInner['customer_commercial_info'] = array('name' => array('cci' => 'customer_commercial_info'), 'cond' => 'c.customer_id = cci.customer_id', 'cols' => '');
                $select->where("cci.business_name LIKE '%{$filters['bussiness_name']}%'");
            }

            if (!empty($filters['inquiry_num'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.customer_id = bok.customer_id', 'cols' => '');
                $joinInner['inquiry'] = array('name' => array('i' => 'inquiry'), 'cond' => 'bok.original_inquiry_id = i.inquiry_id', 'cols' => '');
                $inquiry_num = $this->getAdapter()->quote('%' . trim($filters['inquiry_num']) . '%');
                $select->where("i.inquiry_num LIKE {$inquiry_num}");
            }

            if (!empty($filters['booking_num'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.customer_id = bok.customer_id', 'cols' => '');
                $booking_num = $this->getAdapter()->quote('%' . trim($filters['booking_num']) . '%');
                $select->where("bok.booking_num LIKE {$booking_num}");
            }

            if (!empty($filters['estimate_num'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.customer_id = bok.customer_id', 'cols' => '');
                $joinInner['booking_estimate'] = array('name' => array('est' => 'booking_estimate'), 'cond' => 'bok.booking_id = est.booking_id', 'cols' => '');
                $estimate_num = $this->getAdapter()->quote('%' . trim($filters['estimate_num']) . '%');
                $select->where("est.estimate_num LIKE {$estimate_num}");
            }

            if (!empty($filters['contractor_id'])) {
                $contractor_id = (int) $filters['contractor_id'];
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.customer_id = bok.customer_id', 'cols' => '');
                $joinInner['contractor_service_booking'] = array('name' => array('csb' => 'contractor_service_booking'), 'cond' => 'bok.booking_id = csb.booking_id', 'cols' => '');
                $select->where("bok.created_by = {$contractor_id} OR csb.contractor_id = {$contractor_id}");
            }

            if (!empty($filters['service_id'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.customer_id = bok.customer_id', 'cols' => '');
                $joinInner['contractor_service_booking'] = array('name' => array('csb' => 'contractor_service_booking'), 'cond' => 'bok.booking_id = csb.booking_id', 'cols' => '');
                $service_id = (int) $filters['service_id'];
                $select->where("csb.service_id = {$service_id}");
            }
        }
        if ($joinInner) {
            foreach ($joinInner as $inner) {
                $select->joinInner($inner['name'], $inner['cond'], $inner['cols']);
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        } else if ($limit) {
            $select->limit($limit);
        }

        return $this->getAdapter()->fetchAll($select);
    }

    public function findDuplicate($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from(array('c' => $this->_name), array("TRIM(CONCAT_WS('.',c.title,CONCAT_WS(' ',TRIM(c.first_name),TRIM(c.last_name)))) AS full_name", "*"));
        $select->distinct();
        $select->order($order);
        $select->group('full_name');

        $loggedUser = CheckAuth::getLoggedUser();
        $company_id = CheckAuth::getCompanySession();

        if (!CheckAuth::checkCredential(array('canSeeCustomerList'))) {
            if (CheckAuth::checkCredential(array('viewHisCustomer'))) {
                $select->where('c.created_by = ' . $loggedUser['user_id']);
            }
        }
        $select->where("c.company_id = {$company_id}");
        $select->where('c.is_deleted = 0');
        $select->where('c.duplicate > 1');
        $select->where('c.duplicate <= 50');

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

    public function cronJobCountDuplicate() {
        $select = $this->getAdapter()->select();
        $select->from(array('c' => $this->_name));
        $select->distinct();

        $allCustomer = $this->getAdapter()->fetchAll($select);

        foreach ($allCustomer as $customer) {

            $duplicates = $this->getAllDuplicateCustomer(0, $customer);
            $this->updateById($customer['customer_id'], array('duplicate' => count($duplicates)));
        }
    }

    /**
     * update table rows according to the assigned id and data
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;

        /**
         * update full text search table
         */
        $done = false;
        if (!empty($data['full_text_search'])) {
            $done = true;
        }
        $modelUpdateFullTextSearch = new Model_UpdateFullTextSearch();
        $modelUpdateFullTextSearch->updateFullTextSearchFlag($this->_name, $id, $done);

        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'changed');

        return parent::update($data, "customer_id = '{$id}'");
    }

    public function insert(array $data) {
        $id = parent::insert($data);

        /**
         * update full text search table
         */
        $modelUpdateFullTextSearch = new Model_UpdateFullTextSearch();
        $modelUpdateFullTextSearch->insertFullTextSearch($this->_name, $id);

        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'added');

        return $id;
    }

    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;

        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'deleted');

        return parent::delete("customer_id = '{$id}'");
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from(array('c' => $this->_name));
        $select->joinInner(array('ci' => 'city'), 'ci.city_id = c.city_id', array('ci.city_name'));
        $select->joinInner(array('co' => 'country'), 'ci.country_id = co.country_id', array('co.country_name'));
        $select->where("customer_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the customer id and created by
     * 
     * @param int $customerId
     * @param int $createdBy
     * @return array 
     */
    public function getByCustomerIdAndCreatedBy($customerId, $createdBy) {
        $customerId = (int) $customerId;
        $createdBy = (int) $createdBy;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("customer_id = '{$customerId}' AND created_by = '{$createdBy}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the email
     * 
     * @param int $email
     * @return array 
     */
    public function getByEmail($email) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("email1 = '{$email}'");
        $select->orWhere("email2 = '{$email}'");
        $select->orWhere("email3 = '{$email}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the phone
     * 
     * @param int $phone
     * @return array 
     */
    public function getByPhone($phone) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("phone1 = '{$phone}'");
        $select->orWhere("phone2 = '{$phone}'");
        $select->orWhere("phone3 = '{$phone}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the mobile
     * 
     * @param int $mobile
     * @return array 
     */
    public function getByMobile($mobile) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("mobile1 = '{$mobile}'");
        $select->orWhere("mobile2 = '{$mobile}'");
        $select->orWhere("mobile3 = '{$mobile}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * getcustomer id according to the full name search
     * 
     * @param string $fullNameSearch
     * @return array 
     */
    public function getCustomerIdByIdFullNameSearch($fullNameSearch) {

        $select = $this->getAdapter()->select();
        $select->from($this->_name, 'customer_id');
        $fullNameSearch = $this->getAdapter()->quote('%' . $fullNameSearch . '%');
        $select->where("full_name_search LIKE {$fullNameSearch}");
        $select->where('is_deleted = 0');

        $results = $this->getAdapter()->fetchAll($select);

        $data = array();
        if ($results) {
            foreach ($results as $result) {
                $data[] = $result['customer_id'];
            }
        }

        return $data;
    }

    /**
     * get table rows 
     * as array to choose @return as array 'user_id' => business_name or the query result
     * 
     * @param boolean $asArray
     * @return array 
     */
    public function getAllCustomer($asArray = false, $filters = array()) {

        $results = $this->getAll($filters);

        if ($asArray) {
            $data = array();
            if ($results) {
                foreach ($results as $result) {
                    $data[$result['customer_id']] = get_customer_name($result);
                }
            }
            return $data;
        } else {
            return $results;
        }
    }

    public function checkIfCanSeeCustomer($customerId, $userId = 0) {

        if (!$userId) {
            $loggedUser = CheckAuth::getLoggedUser();
            $userId = $loggedUser['user_id'];
        }

        if (CheckAuth::checkCredential(array('canSeeAllCustomer'))) {
            return true;
        }

        $customer = $this->getById($customerId);
        if ($customer['created_by'] == $userId) {
            return true;
        } else {
            //
            // load model
            //
            $modelContractorServiceBooking = new Model_ContractorServiceBooking();
            $modelBooking = new Model_Booking();

            $filters = array();
            $filters['is_accepted'] = "accepted";
            $filters['contractor_id'] = $userId;
            $contractorServiceBookings = $modelContractorServiceBooking->getAll($filters);
            if ($contractorServiceBookings) {
                foreach ($contractorServiceBookings AS $contractorServiceBooking) {
                    $bookingId = $contractorServiceBooking['booking_id'];
                    if ($modelBooking->checkBookingTimePeriod($bookingId, $userId)) {
                        $booking = $modelBooking->getById($bookingId);
                        if ($booking['customer_id'] == $customerId) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    public function checkIfCanSeeLocation($customerId, $userId = 0) {

        if (!$userId) {
            $loggedUser = CheckAuth::getLoggedUser();
            $userId = $loggedUser['user_id'];
        }

        if (CheckAuth::checkCredential(array('canSeeAllCustomerLocation'))) {
            return true;
        }

        $customer = $this->getById($customerId);
        if ($customer['created_by'] == $userId) {
            return true;
        } else {
            //
            // load model
            //
            $modelContractorServiceBooking = new Model_ContractorServiceBooking();
            $modelBooking = new Model_Booking();

            $filters = array();
            $filters['is_accepted'] = "accepted";
            $filters['contractor_id'] = $userId;
            $contractorServiceBookings = $modelContractorServiceBooking->getAll($filters);
            if ($contractorServiceBookings) {
                foreach ($contractorServiceBookings AS $contractorServiceBooking) {
                    $bookingId = $contractorServiceBooking['booking_id'];
                    if ($modelBooking->checkBookingTimePeriod($bookingId, $userId)) {
                        $booking = $modelBooking->getById($bookingId);
                        if ($booking['customer_id'] == $customerId) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    private $modelCustomerCommercialInfo;
    private $modelCities;
    private $modelCustomerContact;
    private $modelCustomerContactLabel;

    /**
     * get extra info for all the rows
     * @param array $row
     * @param array $types
     * @return array
     */
    public function fills(&$rows, $types = array()) {
        foreach ($rows as &$row) {
            $this->fill($row, $types);
        }
    }

    /**
     * get extra info for each row (Ex: getting the category row by c_id, author row by author id ... etc)
     * 
     * @param array $row
     * @param array $types
     * @return array
     */
    public function fill(&$row, $types = array()) {

        if (in_array('customer_commercial_info', $types)) {
            /**
             * load model
             */
            if (!$this->modelCustomerCommercialInfo) {
                $this->modelCustomerCommercialInfo = new Model_CustomerCommercialInfo();
            }

            $row['customer_commercial_info'] = $this->modelCustomerCommercialInfo->getByCustomerId($row['customer_id']);
        }

        if (in_array('customer_contacts', $types)) {
            /**
             * load model
             */
            if (!$this->modelCustomerContact) {
                $this->modelCustomerContact = new Model_CustomerContact();
            }
            if (!$this->modelCustomerContactLabel) {
                $this->modelCustomerContactLabel = new Model_CustomerContactLabel();
            }

            $customerContacts = $this->modelCustomerContact->getByCustomerId($row['customer_id']);
            foreach ($customerContacts as &$customerContact) {
                $customerContactLabel = $this->modelCustomerContactLabel->getById($customerContact['customer_contact_label_id']);
                $customerContact['contact_label'] = $customerContactLabel['contact_label'];
            }
            $row['customer_contacts'] = $customerContacts;
        }

        if (in_array('customer_city', $types)) {
            /**
             * load model
             */
            if (!$this->modelCities) {
                $this->modelCities = new Model_Cities();
            }

            $row['customer_city'] = $this->modelCities->getById($row['city_id']);
        }

        if (in_array('duplicate_count', $types)) {

            $row['duplicate_count'] = count($this->getAllDuplicateCustomer($row['customer_id']));
        }
        return $row;
    }

    public function getAllDuplicateCustomer($customerId, $cutomer = array()) {

        if (!$cutomer) {
            $cutomer = $this->getById($customerId);
            $company_id = CheckAuth::getCompanySession();
        } else {
            $company_id = isset($cutomer['company_id']) ? $cutomer['company_id'] : 0;
        }


        $select = $this->getAdapter()->select();
        $select->from(array('c' => $this->_name));
        $select->where("c.company_id = {$company_id}");
        $select->where('c.is_deleted = 0');

        $where = array();
        if (isset($cutomer['email1']) && $cutomer['email1']) {
            $where[] = "'{$cutomer['email1']}' IN (email1 ,email2 ,email3)";
        }
        if (isset($cutomer['email2']) && $cutomer['email2']) {
            $where[] = "'{$cutomer['email2']}' IN (email1 ,email2 ,email3)";
        }
        if (isset($cutomer['email3']) && $cutomer['email3']) {
            $where[] = "'{$cutomer['email3']}' IN (email1 ,email2 ,email3)";
        }

        if (isset($cutomer['phone1']) && $cutomer['phone1']) {
            $where[] = "REPLACE('{$cutomer['phone1']}', ' ', '') IN (REPLACE(phone1, ' ', '') ,REPLACE(phone2, ' ', '') ,REPLACE(phone3, ' ', ''))";
        }
        if (isset($cutomer['phone2']) && $cutomer['phone2']) {
            $where[] = "REPLACE('{$cutomer['phone2']}', ' ', '') IN (REPLACE(phone1, ' ', '') ,REPLACE(phone2, ' ', '') ,REPLACE(phone3, ' ', ''))";
        }
        if (isset($cutomer['phone3']) && $cutomer['phone3']) {
            $where[] = "REPLACE('{$cutomer['phone3']}', ' ', '') IN (REPLACE(phone1, ' ', '') ,REPLACE(phone2, ' ', '') ,REPLACE(phone3, ' ', ''))";
        }

        if (isset($cutomer['mobile1']) && $cutomer['mobile1']) {
            $where[] = "REPLACE('{$cutomer['mobile1']}', ' ', '') IN (REPLACE(mobile1, ' ', '') ,REPLACE(mobile2, ' ', '') ,REPLACE(mobile3, ' ', ''))";
        }
        if (isset($cutomer['mobile2']) && $cutomer['mobile2']) {
            $where[] = "REPLACE('{$cutomer['mobile1']}', ' ', '') IN (REPLACE(mobile1, ' ', '') ,REPLACE(mobile2, ' ', '') ,REPLACE(mobile3, ' ', ''))";
        }
        if (isset($cutomer['mobile3']) && $cutomer['mobile3']) {
            $where[] = "REPLACE('{$cutomer['mobile1']}', ' ', '') IN (REPLACE(mobile1, ' ', '') ,REPLACE(mobile2, ' ', '') ,REPLACE(mobile3, ' ', ''))";
        }

        if ($where) {
            $select->where(implode(" OR ", $where));
        }

        return $this->getAdapter()->fetchAll($select);
    }

    public function getCustomerContacts($customerId) {


        $customer = $this->getById($customerId);

        if (!$customer) {
            return null;
        }

        $this->fill($customer, array('customer_commercial_info', 'customer_contacts'));

        $customerContacts = "";


        $customerContacts.="Customer : " . get_customer_name($customer) . "<br>";

        if (!empty($customer['customer_commercial_info']['business_name'])) {
            $customerContacts.="Business Name : " . ucfirst($customer['customer_commercial_info']['business_name']) . "<br>";
        }

        if (!empty($customer['email1'])) {
            $customerContacts.="Email : {$customer['email1']}" . "<br>";
        }

        if (!empty($customer['email2'])) {
            $customerContacts.="Email : {$customer['email2']}" . "<br>";
        }
        if (!empty($customer['email3'])) {
            $customerContacts.="Email : {$customer['email3']}" . "<br>";
        }

        if (!empty($customer['mobile1'])) {
            $customerContacts.="Mobile : {$customer['mobile1']}" . "<br>";
        }

        if (!empty($customer['mobile2'])) {
            $customerContacts.="Mobile : {$customer['mobile2']}" . "<br>";
        }

        if (!empty($customer['mobile3'])) {
            $customerContacts.="Mobile : {$customer['mobile3']}" . "<br>";
        }

        if (!empty($customer['phone1'])) {
            $customerContacts.="Phone : {$customer['phone1']}" . "<br>";
        }

        if (!empty($customer['phone2'])) {
            $customerContacts.="Phone : {$customer['phone2']}" . "<br>";
        }

        if (!empty($customer['phone3'])) {
            $customerContacts.="Phone : {$customer['phone3']}" . "<br>";
        }

        if (!empty($customer['fax'])) {
            $customerContacts.="Fax : {$customer['fax']}" . "<br>";
        }

        if (!empty($customer['po_box'])) {
            $customerContacts.="PO Box : {$customer['po_box']}";
        }

        if (!empty($customer['customer_contacts'])) {
            foreach ($customer['customer_contacts'] as $customerContact) {
                $customerContacts.="{$customerContact['contact_label']} : {$customerContact['contact']}" . "<br>";
            }
        }

        return $customerContacts;
    }

    public function getFullTextCustomerContacts($customerId) {

        $customer = $this->getById($customerId);

        if (!$customer) {
            return '';
        }

        $this->fill($customer, array('customer_commercial_info', 'customer_contacts'));

        $customerContacts = array();


        $customerContacts[] = trim(get_customer_name($customer));

        $customerContacts[] = trim(get_line_address($customer));

        if (!empty($customer['customer_commercial_info']['business_name'])) {
            $customerContacts[] = trim(ucfirst($customer['customer_commercial_info']['business_name']));
        }

        if (!empty($customer['email1'])) {
            $customerContacts[] = trim($customer['email1']);
        }

        if (!empty($customer['email2'])) {
            $customerContacts[] = trim($customer['email2']);
        }
        if (!empty($customer['email3'])) {
            $customerContacts[] = trim($customer['email3']);
        }

        if (!empty($customer['mobile1'])) {
            $customerContacts[] = trim($customer['mobile1']);
        }

        if (!empty($customer['mobile2'])) {
            $customerContacts[] = trim($customer['mobile2']);
        }

        if (!empty($customer['mobile3'])) {
            $customerContacts[] = trim($customer['mobile3']);
        }

        if (!empty($customer['phone1'])) {
            $customerContacts[] = trim($customer['phone1']);
        }

        if (!empty($customer['phone2'])) {
            $customerContacts[] = trim($customer['phone2']);
        }

        if (!empty($customer['phone3'])) {
            $customerContacts[] = trim($customer['phone3']);
        }

        if (!empty($customer['fax'])) {
            $customerContacts[] = trim($customer['fax']);
        }

        if (!empty($customer['po_box'])) {
            $customerContacts[] = trim($customer['po_box']);
        }

        if (!empty($customer['customer_contacts'])) {
            foreach ($customer['customer_contacts'] as $customerContact) {
                $customerContacts[] = trim($customerContact['contact']);
            }
        }

        if ($customerContacts) {
            $customerContacts = implode(' ', $customerContacts);
        }

        return $customerContacts;
    }

    public function updateFullTextSearch($customerId) {

        // Load Model
        $modelBookingAddress = new Model_BookingAddress();
        $modelInquiry = new Model_Inquiry();
        $modelBookingEstimate = new Model_BookingEstimate();
        $modelBookingInvoice = new Model_BookingInvoice();
        $model_Complaint = new Model_Complaint();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelBookingStatus = new Model_BookingStatus();


        $fullTextSearch = array();

        //customer
        $fullTextCustomer = $this->getFullTextCustomerContacts($customerId);
        $fullTextSearch[] = trim($fullTextCustomer);

        $selectBooking = $this->getAdapter()->select();
        $selectBooking->from('booking');
        $selectBooking->where("customer_id = {$customerId}");
        $selectBooking->where('is_deleted = 0');
        $bookings = $this->getAdapter()->fetchAll($selectBooking);

        $inquirySelect = $this->getAdapter()->select();
        $inquirySelect->from('inquiry');
        $inquirySelect->where("customer_id = {$customerId}");
        $inquirySelect->where('is_deleted = 0');
        $inquiries = $this->getAdapter()->fetchAll($inquirySelect);


        if ($inquiries) {
            foreach ($inquiries as $inquiry) {
                $fullTextSearch[] = trim($inquiry['inquiry_num']);
                $fullTextSearch[] = trim($inquiry['comment']);
            }
        }

        if ($bookings) {
            foreach ($bookings as $booking) {

                $bookingId = $booking['booking_id'];

                // booking
                $fullTextSearch[] = trim($booking['booking_num']);
                $fullTextSearch[] = trim($booking['title']);
                $fullTextSearch[] = trim($booking['description']);
                $status = $modelBookingStatus->getById($booking['status_id']);
                $fullTextSearch[] = trim($status['name']);


                // address
                $bookingAddress = $modelBookingAddress->getByBookingId($bookingId);
                $fullTextSearch[] = trim(get_line_address($bookingAddress));

                // services
                $fullTextContractorServiceBooking = $modelContractorServiceBooking->getFullTextContractorServiceBooking($bookingId);
                if ($fullTextContractorServiceBooking) {
                    $fullTextSearch[] = trim($fullTextContractorServiceBooking);
                }

                //complaint
                $complaints = $model_Complaint->getFullTextComplaintByBookingId($bookingId);
                if ($complaints) {
                    $fullTextSearch[] = trim($complaints);
                }

                //inquiry
                if (isset($booking['original_inquiry_id']) && $booking['original_inquiry_id']) {
                    $inquiry = $modelInquiry->getById($booking['original_inquiry_id']);
                    if ($inquiry['inquiry_num']) {
                        $fullTextSearch[] = trim($inquiry['inquiry_num']);
                    }
                }

                //estimate
                $estimate = $modelBookingEstimate->getByBookingId($bookingId);
                if (isset($estimate['estimate_num']) && $estimate['estimate_num']) {
                    $fullTextSearch[] = trim($estimate['estimate_num']);
                }

                //invoice
                $invoice = $modelBookingInvoice->getByBookingId($bookingId);
                if (isset($invoice['invoice_num']) && $invoice['invoice_num']) {
                    $fullTextSearch[] = trim($invoice['invoice_num']);
                }
                if (isset($invoice['invoice_type']) && $invoice['invoice_type']) {
                    $fullTextSearch[] = trim($invoice['invoice_type']);
                }
            }
        }

        $data = array();
        $data['full_text_search'] = implode(' ', $fullTextSearch);
        $this->updateById($customerId, $data);
    }

    public function checkBeforeDeleteCustomer($cstomerId, &$tables = array()) {

        $sucsess = true;

        $select_inquiry = $this->getAdapter()->select();
        $select_inquiry->from('inquiry', 'COUNT(*)');
        $select_inquiry->where("customer_id = {$cstomerId}");
        $count_inquiry = $this->getAdapter()->fetchOne($select_inquiry);

        if ($count_inquiry) {
            $tables[] = 'inquiry';
            $sucsess = false;
        }

        $select_booking = $this->getAdapter()->select();
        $select_booking->from('booking', 'COUNT(*)');
        $select_booking->where("customer_id = {$cstomerId}");
        $count_booking = $this->getAdapter()->fetchOne($select_booking);

        if ($count_booking) {
            $tables[] = 'booking';
            $sucsess = false;
        }

        $select_payment = $this->getAdapter()->select();
        $select_payment->from('payment', 'COUNT(*)');
        $select_payment->where("customer_id = {$cstomerId}");
        $count_payment = $this->getAdapter()->fetchOne($select_payment);

        if ($count_payment) {
            $tables[] = 'payment';
            $sucsess = false;
        }

        $select_refund = $this->getAdapter()->select();
        $select_refund->from('refund', 'COUNT(*)');
        $select_refund->where("customer_id = {$cstomerId}");
        $count_refund = $this->getAdapter()->fetchOne($select_refund);

        if ($count_refund) {
            $tables[] = 'refund';
            $sucsess = false;
        }

        return $sucsess;
    }

    public function deleteRelatedCustomer($id) {

        //delete data from customer_notes
        $this->getAdapter()->delete('customer_notes', "customer_id = '{$id}'");

        //delete data from customer_commercial_info
        $this->getAdapter()->delete('customer_commercial_info', "customer_id = '{$id}'");

        //delete data from customer_contact
        $this->getAdapter()->delete('customer_contact', "customer_id = '{$id}'");
    }

    
    public function getCustomerCount($filters = array()) {

        $select = $this->getAdapter()->select();
        $select->from(array('c' => $this->_name));
        $select->where('c.is_deleted = 0');

        if (!isset($filters['company_id'])) {
            $filters['company_id'] = CheckAuth::getCompanySession();
        }

        if ($filters) {

            if (!empty($filters['customer_id'])) {
                $customer_id = (int) $filters['customer_id'];
                $select->where("c.customer_id = '{$customer_id}'");
            }

            if (!empty($filters['startTimeRange']) && !empty($filters['endTimeRange'])) {
                $startTime = $filters['startTimeRange'];
                $endTime = $filters['endTimeRange'];
                $select->where("c.created between '" . mySql2PhpTime($startTime) . "' and '" . mySql2PhpTime($endTime) . "'");
            }
            
            if (!empty($filters['company_id'])) {
                $company_id = (int) $filters['company_id'];
                $select->where("c.company_id = {$company_id}");
            }
            
            if (!empty($filters['created_by'])) {
                $created_by = (int) $filters['created_by'];
                $select->where("c.created_by = {$created_by}");
            }
        }

        $sql = $this->getAdapter()->select();
        $sql->from($select, 'COUNT(*)');
        return $this->getAdapter()->fetchOne($sql);
    }
    public function sendEmailToNewCustomers() {

        $modelCronjobHistory = new Model_CronjobHistory();
        $modelCronJob = new Model_CronJob();

        //
        // save this cron in cronjob_history table
        //
	$cronjob = $modelCronJob->getIdByName('5 Star Reasons to Choose Tile Cleaners');
        $cronjonID = $cronjob['id'];

        $cronjobHistoryData = array(
            'cron_job_id' => $cronjonID,
            'run_time' => time()
        );
        $cronjobHistoryId = $modelCronjobHistory->insert($cronjobHistoryData);
        
        $startDay = strtotime(date('Y-m-d 00:00:00'));
        $endDay = strtotime(date('Y-m-d 23:59:00'));

        $select = $this->getAdapter()->select();
        $select->from(array('c' => $this->_name));
        $select->where("c.created BETWEEN '{$startDay}' AND '{$endDay}'");

        $newCustomers = $this->getAdapter()->fetchAll($select);

        foreach ($newCustomers as $key => $customer) {

            $to = array("salim.3bd@gmail.com");
//            if ($customer['email1']) {
//                $to[] = $customer['email1'];
//            }
//            if ($customer['email2']) {
//                $to[] = $customer['email2'];
//            }
//            if ($customer['email2']) {
//                $to[] = $customer['email3'];
//            }
            $to = implode(',', $to);


            $email_log = array('reference_id' => $customer['customer_id'], 'type' => 'customer', 'cronjob_history_id' => $cronjobHistoryId);

            EmailNotification::sendEmail(array('to' => $to), '5 Star Reasons to Choose Tile Cleaners', array(), $email_log);
        }
    }
    
}