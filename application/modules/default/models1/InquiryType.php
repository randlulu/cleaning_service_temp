<?php

class Model_InquiryType extends Zend_Db_Table_Abstract {

    protected $_name = 'inquiry_type';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order($order);

        $filters['company_id'] = CheckAuth::getCompanySession();

        if ($filters) {
            if (!empty($filters['keywords'])) {
                $keywords = $this->getAdapter()->quote('%' . $filters['keywords'] . '%');
                $select->where("inquiry_name LIKE {$keywords}");
            }
            if (!empty($filters['company_id'])) {
                $company_id = (int) $filters['company_id'];
                $select->where("company_id = {$company_id}");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }
        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table rows according to the assigned id and data
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "inquiry_type_id = '{$id}'");
    }

    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("inquiry_type_id = '{$id}'");
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("inquiry_type_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get By Type
     * 
     * @param string $inquiryName
     * @return array 
     */
    public function getByType($inquiryName) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("inquiry_name = '{$inquiryName}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function checkBeforeDeleteInquiryType($inquiryTypeId, &$tables = array()) {

        $sucsess = true;

        $select_inquiry = $this->getAdapter()->select();
        $select_inquiry->from('inquiry', 'COUNT(*)');
        $select_inquiry->where("inquiry_type_id = {$inquiryTypeId}");
        $count_inquiry = $this->getAdapter()->fetchOne($select_inquiry);

        if ($count_inquiry) {
            $tables[] = 'inquiry';
            $sucsess = false;
        }

        $select_inquiry_type_attribute = $this->getAdapter()->select();
        $select_inquiry_type_attribute->from('inquiry_type_attribute', 'COUNT(*)');
        $select_inquiry_type_attribute->where("inquiry_type_id = {$inquiryTypeId}");
        $count_inquiry_type_attribute = $this->getAdapter()->fetchOne($select_inquiry_type_attribute);

        if ($count_inquiry_type_attribute) {
            $tables[] = 'inquiry_type_attribute';
            $sucsess = false;
        }

        return $sucsess;
    }

}