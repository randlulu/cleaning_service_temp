<?php

class Model_CustomerCommercialInfo extends Zend_Db_Table_Abstract {

    protected $_name = 'customer_commercial_info';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order($order);

        $where = array();

        if ($filters) {
            if (!empty($filters['keywords'])) {
                $keywords = $this->getAdapter()->quote('%' . $filters['keywords'] . '%');
                $where[] = ("customer_type LIKE {$keywords}");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table rows according to the assigned id and data
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "id = {$id}");
    }

    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("id = '{$id}'");
    }

    /**
     * delete table row according to the customer id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteByCustomerId($id) {
        $id = (int) $id;
        return parent::delete("customer_id = '{$id}'");
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the customer id
     * 
     * @param int $id
     * @return array 
     */
    public function getByCustomerId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select('business_name');
        $select->from($this->_name);
        $select->where("customer_id = '{$id}'");
		
        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * set customer commercial info accourfding to customer id and customer type id and business name
     * 
     * @param int $customerId
     * @param int $customerTypeId
     * @param string $businessName 
     */
    public function setCustomerCommercialInfo($customerId, $customerTypeId, $businessName) {

        $modelCustomerType = new Model_CustomerType();
//        $customerType = $modelCustomerType->getByTypeName("Commercial");
//        $commercialId = $customerType['customer_type_id'];

        $residential = $modelCustomerType->getByTypeName("Residential");
        $residentialId = $residential['customer_type_id'];

        if ($customerTypeId != $residentialId) {
            $this->addCommercialInfo($customerId, $businessName);
        } else {
            $this->deleteByCustomerId($customerId);
        }
    }

    /**
     * add table row to commercial table accourding to customer id and business name
     * 
     * @param int $customerId
     * @param string $businessName
     * @return int 
     */
    public function addCommercialInfo($customerId, $businessName) {
        $commercialInfo = $this->getByCustomerId($customerId);

        $data = array(
            'customer_id' => $customerId,
            'business_name' => $businessName
        );

        if ($commercialInfo) {
            $this->updateById($commercialInfo['id'], $data);
            $returnId = $commercialInfo['id'];
        } else {
            $returnId = $this->insert($data);
        }
        return $returnId;
    }
	
	/**
	 * By islam get all customer ids according to business_name
	 **/
	public function getCustomerIdsByBusinessName($businessName) {
		$select = $this->getAdapter()->select();
        $select->from($this->_name,array('customer_id'));
        $select->where("business_name = '{$businessName}'");
		
        return $this->getAdapter()->fetchAll($select);
	}

}

	
	