<?php

class Model_Inquiry extends Zend_Db_Table_Abstract {

    protected $_name = 'inquiry';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {

        $select = $this->getAdapter()->select();
        $select->from(array('i' => $this->_name));
        $select->order($order);
        $select->distinct();

        $loggedUser = CheckAuth::getLoggedUser();
        $filters['company_id'] = CheckAuth::getCompanySession();

        $joinInner = array();
        $joinLeft = array();

        $loggedUser = CheckAuth::getLoggedUser();

        if (!CheckAuth::checkCredential(array('canSeeAllInquiry'))) {
            if (CheckAuth::checkCredential(array('canSeeOnlyHisInquiry'))) {
                $select->where("i.user_id = " . $loggedUser['user_id']);
            }
        }

        if (CheckAuth::checkCredential(array('canSeeDeletedInquiry'))) {
            if (!empty($filters['is_deleted'])) {
                $select->where('i.is_deleted = 1');
            } else {
                $select->where('i.is_deleted = 0');
            }
        } else {
            $select->where('i.is_deleted = 0');
        }



        if ($filters) {
            if (!empty($filters['keywords'])) {
                $keywords = $this->getAdapter()->quote('%' . trim($filters['keywords']) . '%');
                $select->where("i.full_text_search LIKE {$keywords}");

                //$keywords = '+' . preg_replace('#[\s]+#', ' +', trim($filters['keywords']));
                //$keywords = $this->getAdapter()->quote($keywords);
                //$select->where("MATCH(i.full_text_search) AGAINST({$keywords} IN BOOLEAN MODE)");
            }

            if (!empty($filters['created_from'])) {
                $created_from = $this->getAdapter()->quote(strtotime(trim($filters['created_from'])));
                $select->where("i.created >= {$created_from}");
            }


            if (!empty($filters['created_to'])) {
                $created_to = $this->getAdapter()->quote(strtotime(trim($filters['created_to'])));
                $select->where("i.created <= {$created_to}");
            }

            if (!empty($filters['customer_id'])) {
                $customer_id = $this->getAdapter()->quote(trim($filters['customer_id']));
                $select->where("i.customer_id = {$customer_id}");
            }

            if (!empty($filters['customer_name'])) {
                $modelCustomer = new Model_Customer();
                $allCustomer = $modelCustomer->getCustomerIdByIdFullNameSearch(trim($filters['customer_name']));

                $customerIds = implode(', ', $allCustomer);
                if ($customerIds) {
                    $select->where("i.customer_id in ( $customerIds )");
                }
            }

            if (!empty($filters['service_id'])) {

                $service_id = (int) $filters['service_id'];

                $select->where("is.service_id = {$service_id}");
                $joinInner['inquiry_service'] = array('name' => array('is' => 'inquiry_service'), 'cond' => 'i.inquiry_id = is.inquiry_id', 'cols' => array('is.service_id'));
            }

            if (!empty($filters['inquiry_num'])) {
                $inquiry_num = $this->getAdapter()->quote('%' . trim($filters['inquiry_num']) . '%');
                $select->where("i.inquiry_num LIKE {$inquiry_num}");
            }

            if (!empty($filters['city_id'])) {
                $city_id = (int) $filters['city_id'];
                $select->where("i.city_id = {$city_id}");
            }

            if (!empty($filters['inquiry_type_id'])) {
                $inquiry_type_id = (int) trim($filters['inquiry_type_id']);
                $select->where("i.inquiry_type_id = {$inquiry_type_id}");
            }

            if (!empty($filters['status'])) {
                if ($filters['status'] != 'all') {
                    $status = $this->getAdapter()->quote(trim($filters['status']));
                    $select->where("i.status = {$status}");
                }
            }

            if (!empty($filters['label_ids'])) {
                if ($filters['label_ids']) {
                    $joinInner['inquiry_label'] = array('name' => array('il' => 'inquiry_label'), 'cond' => 'i.inquiry_id = il.inquiry_id', 'cols' => '');
                    $select->where('il.label_id IN (' . implode(', ', $filters['label_ids']) . ')');
                }
            }

            if (!empty($filters['unlabeled']) && $filters['unlabeled'] == true) {

                $sql = $this->getAdapter()->select();
                $sql->from('inquiry_label', 'inquiry_id');
                $sql->distinct();
                $results = $this->getAdapter()->fetchAll($sql);

                $inquiry_ids = array();
                foreach ($results as $result) {
                    $inquiry_ids[] = $result['inquiry_id'];
                }

                if ($inquiry_ids) {
                    $select->where('i.inquiry_id NOT IN (' . implode(', ', $inquiry_ids) . ')');
                }
            }

            if (!empty($filters['address'])) {
                $address = trim($filters['address']);
                $joinInner['inquiry_address'] = array('name' => array('ia' => 'inquiry_address'), 'cond' => 'i.inquiry_id = ia.inquiry_id', 'cols' => '');
                $select->where("CONCAT_WS(' ',ia.street_number ,ia.street_address ,ia.suburb,ia.state,ia.postcode) LIKE '%{$address}%'");
            }

            if (!empty($filters['email'])) {
                $email = trim($filters['email']);
                $joinInner['customer'] = array('name' => array('c' => 'customer'), 'cond' => 'i.customer_id = c.customer_id', 'cols' => '');
                $select->where("CONCAT_WS(' ',email1 ,email2 ,email3) LIKE '%{$email}%'");
            }

            if (!empty($filters['mobile/phone'])) {
                $mobile_phone = trim($filters['mobile/phone']);
                $joinInner['customer'] = array('name' => array('c' => 'customer'), 'cond' => 'i.customer_id = c.customer_id', 'cols' => '');
                $select->where("CONCAT_WS(' ',phone1 ,phone2 ,phone3,mobile1 ,mobile2 ,mobile3) LIKE '%{$mobile_phone}%'");
            }

            if (!empty($filters['bussiness_name'])) {
                $joinInner['customer_commercial_info'] = array('name' => array('cci' => 'customer_commercial_info'), 'cond' => 'i.customer_id = cci.customer_id', 'cols' => '');
                $select->where("cci.business_name LIKE '%{$filters['bussiness_name']}%'");
            }

            if (!empty($filters['deferred_to'])) {
                $deferred_to = $this->getAdapter()->quote(strtotime(trim($filters['deferred_to'])));
                $select->where("i.deferred_date = {$deferred_to}");
            }

            if (!empty($filters['to_follow_date'])) {
                switch ($filters['to_follow_date']) {
                    case 'yesterday':
                        $yesterday = getTimePeriodByName('yesterday');
                        $select->where("i.deferred_date between '" . strtotime($yesterday['start']) . "' and '" . strtotime($yesterday['end']) . "'");
                        break;
                    case 'today':
                        $today = getTimePeriodByName('today');
                        $select->where("i.deferred_date between '" . strtotime($today['start']) . "' and '" . strtotime($today['end']) . "'");
                        break;
                    case 'tomorrow':
                        $tomorrow = getTimePeriodByName('tomorrow');
                        $select->where("i.deferred_date between '" . strtotime($tomorrow['start']) . "' and '" . strtotime($tomorrow['end']) . "'");
                        break;
                    case 'past':
                        $today = getTimePeriodByName('today');
                        $select->where("i.deferred_date < '" . strtotime($today['start']) . "'");
                        break;
                    case 'future':
                        $today = getTimePeriodByName('today');
                        $select->where("i.deferred_date > '" . strtotime($today['end']) . "'");
                        break;
                }
            }

//            if (!empty($filters['to_follow']) && $filters['to_follow'] == true) {
//                $time = time();
//                $select->where("i.deferred_date <= {$time} ");
//                $select->where("i.deferred_date > 0 ");
//            }

            if (!empty($filters['to_follow']) && $filters['to_follow'] == true) {
                $select->where("i.is_to_follow = 1");
            }

            if (!empty($filters['company_id'])) {
                $company_id = (int) $filters['company_id'];
                $select->where("i.company_id = {$company_id}");
            }

            if (!empty($filters['un_reminded']) && $filters['un_reminded'] == true) {

                $sql = $this->getAdapter()->select();
                $sql->from('inquiry_reminder', 'inquiry_id');
                $sql->distinct();
                $results = $this->getAdapter()->fetchAll($sql);

                $inquiry_ids = array();
                foreach ($results as $result) {
                    $inquiry_ids[] = $result['inquiry_id'];
                }

                if ($inquiry_ids) {
                    $select->where('i.inquiry_id NOT IN (' . implode(', ', $inquiry_ids) . ')');
                }
            }

            if (!empty($filters['my_inquiries'])) {

                $user_id = (int) $filters['my_inquiries'];

                $my_inquiry_sql = $this->getAdapter()->select();
                $my_inquiry_sql->from('inquiry_reminder', 'inquiry_id');
                $my_inquiry_sql->where("user_id = {$user_id}");
                $my_inquiry_sql->distinct();

                $my_inquiry_results = $this->getAdapter()->fetchAll($my_inquiry_sql);

                $not_my_inquiry_sql = $this->getAdapter()->select();
                $not_my_inquiry_sql->from('inquiry_reminder', 'inquiry_id');
                $not_my_inquiry_sql->distinct();

                if ($my_inquiry_results) {
                    $my_inquiry_ids = array();
                    foreach ($my_inquiry_results as $my_result) {
                        $my_inquiry_ids[$my_result['inquiry_id']] = $my_result['inquiry_id'];
                    }
                    $not_my_inquiry_sql->where('inquiry_id NOT IN (' . implode(', ', $my_inquiry_ids) . ')');
                }

                $not_my_inquiry_results = $this->getAdapter()->fetchAll($not_my_inquiry_sql);

                $not_my_inquiry_ids = array();
                foreach ($not_my_inquiry_results as $not_my_result) {
                    $not_my_inquiry_ids[$not_my_result['inquiry_id']] = $not_my_result['inquiry_id'];
                }

                if ($not_my_inquiry_ids) {
                    $select->where('i.inquiry_id NOT IN (' . implode(', ', $not_my_inquiry_ids) . ')');
                }
            }

            if (!empty($filters['booking_num'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'i.inquiry_id = bok.original_inquiry_id', 'cols' => '');
                $booking_num = $this->getAdapter()->quote('%' . trim($filters['booking_num']) . '%');
                $select->where("bok.booking_num LIKE {$booking_num}");
            }

            if (!empty($filters['estimate_num'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'i.inquiry_id = bok.original_inquiry_id', 'cols' => '');
                $joinInner['booking_estimate'] = array('name' => array('est' => 'booking_estimate'), 'cond' => 'bok.booking_id = est.booking_id', 'cols' => '');
                $estimate_num = $this->getAdapter()->quote('%' . trim($filters['estimate_num']) . '%');
                $select->where("est.estimate_num LIKE {$estimate_num}");
            }

            if (!empty($filters['contractor_id'])) {
                $contractor_id = (int) $filters['contractor_id'];
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'i.inquiry_id = bok.original_inquiry_id', 'cols' => '');
                $joinInner['contractor_service_booking'] = array('name' => array('csb' => 'contractor_service_booking'), 'cond' => 'bok.booking_id = csb.booking_id', 'cols' => '');
                $select->where("bok.created_by = {$contractor_id} OR csb.contractor_id = {$contractor_id}");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        if ($joinInner) {
            foreach ($joinInner as $inner) {
                $select->joinInner($inner['name'], $inner['cond'], $inner['cols']);
            }
        }

        if ($joinLeft) {
            foreach ($joinLeft as $left) {
                $select->joinLeft($left['name'], $left['cond'], $left['cols']);
            }
        }

        return $this->getAdapter()->fetchAll($select);
    }

    public function insert(array $data, $company_id = 0) {

        if (!$company_id) {
            $company_id = CheckAuth::getCompanySession();
        }

        $select = $this->getAdapter()->select();
        $select->from($this->_name, 'MAX(count)');
        $select->where("company_id = {$company_id}");
        $result = $this->getAdapter()->fetchOne($select);

        $data['count'] = $result + 1;
        $data['inquiry_num'] = 'INQ-' . ($result + 1);

        $id = parent::insert($data);

        /**
         * update full text search table
         */
        $modelUpdateFullTextSearch = new Model_UpdateFullTextSearch();
        $modelUpdateFullTextSearch->insertFullTextSearch($this->_name, $id);

        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'added');

        return $id;
    }

    /**
     * update table rows according to the assigned id and data
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;

        /**
         * update full text search table
         */
        $done = false;
        if (!empty($data['full_text_search'])) {
            $done = true;
        }
        $modelUpdateFullTextSearch = new Model_UpdateFullTextSearch();
        $modelUpdateFullTextSearch->updateFullTextSearchFlag($this->_name, $id, $done);

        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'changed');

        return parent::update($data, "inquiry_id = '{$id}'");
    }

    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;

        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'deleted');

        return parent::delete("inquiry_id = '{$id}'");
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("inquiry_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the customer id
     * 
     * @param int $id
     * @return array 
     */
    public function getByCustomerId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("customer_id = '{$id}'");
        $select->where("is_deleted = 0");
        $result = $this->getAdapter()->fetchAll($select);

        return $result;
    }

    public function getInquiryCount($filters = array()) {

        $select = $this->getAdapter()->select();
        $select->from(array('i' => $this->_name));
        $select->where('i.is_deleted = 0');

        if (!isset($filters['company_id'])) {
            $filters['company_id'] = CheckAuth::getCompanySession();
        }

        if ($filters) {

            if (!empty($filters['customer_id'])) {
                $customer_id = (int) $filters['customer_id'];
                $select->where("i.customer_id = '{$customer_id}'");
            }
			//////////by islam select * inquiries of business name
			if (!empty($filters['customer_ids']) && $filters['customer_ids']) {
                $select->where('i.customer_id IN(?)', $filters['customer_ids']);
            }

            if (!empty($filters['startTimeRange']) && !empty($filters['endTimeRange'])) {
                $startTime = $filters['startTimeRange'];
                $endTime = $filters['endTimeRange'];
                $select->where("i.created between '" . mySql2PhpTime($startTime) . "' and '" . mySql2PhpTime($endTime) . "'");
            }

            if (!empty($filters['inquiry_status'])) {
                $inquiry_status = $this->getAdapter()->quote(trim($filters['inquiry_status']));
                $select->where("i.status = {$inquiry_status}");
            }
            if (!empty($filters['company_id'])) {
                $company_id = (int) $filters['company_id'];
                $select->where("i.company_id = {$company_id}");
            }
            if (!empty($filters['user_id'])) {
                $user_id = (int) $filters['user_id'];
                $select->where("i.user_id = {$user_id}");
            }
            if (!empty($filters['inquiry_type_id'])) {
                $inquiry_type_id = (int) $filters['inquiry_type_id'];
                $select->where("i.inquiry_type_id = {$inquiry_type_id}");
            }
        }

        $sql = $this->getAdapter()->select();
        $sql->from($select, 'COUNT(*)');
        return $this->getAdapter()->fetchOne($sql);
    }

    public function getEnamStatus() {
        $query = "SHOW COLUMNS FROM {$this->_name} LIKE 'status'";
        $stmt = $this->getAdapter()->query($query);
        $row = $stmt->fetch();
        $row = $row['Type'];
        $regex = "/'(.*?)'/";
        preg_match_all($regex, $row, $enum_array);
        $enum_fields = $enum_array[1];
        foreach ($enum_fields as $key => $value) {
            $enums[$value] = $value;
        }
        return $enums;
    }

    public function checkIfCanSeeInquiry($inquiryId, $userId = 0) {

        if (!$userId) {
            $loggedUser = CheckAuth::getLoggedUser();
            $userId = $loggedUser['user_id'];
        }

        if (CheckAuth::checkCredential(array('canSeeAllInquiry'))) {
            return true;
        }

        $inquiry = $this->getById($inquiryId);
        if ($inquiry['user_id'] == $userId) {
            return true;
        }
        return false;
    }

    public function checkIfCanSeeLocation($inquiryId, $userId = 0) {

        if (!$userId) {
            $loggedUser = CheckAuth::getLoggedUser();
            $userId = $loggedUser['user_id'];
        }

        if (CheckAuth::checkCredential(array('canSeeAllInquiryLocation'))) {
            return true;
        }

        $inquiry = $this->getById($inquiryId);
        if ($inquiry['user_id'] == $userId) {
            return true;
        }
        return false;
    }

    /**
     * get extra info for all the rows
     * @param array $row
     * @param array $types
     * @return array
     */
    public function fills(&$rows, $types = array()) {
        foreach ($rows as &$row) {
            $this->fill($row, $types);
        }
    }

    private $modelInquiryService;
    private $modelService;
    private $modelCustomer;
    private $modelCities;
    private $modelCountries;
    private $modelInquiryLabel;
    private $modelLabel;
    private $modelInquiryReminder;
    private $modelServiceAttribute;
    private $modelAttributeListValue;
    private $modelInquiryServiceAttributeValue;
    private $modelAttributeType;
    private $modelBookingAttachment;

    /**
     * get extra info for each row (Ex: getting the category row by c_id, author row by author id ... etc)
     * 
     * @param array $row
     * @param array $types
     * @return array
     */
    public function fill(&$row, $types = array()) {

        if (in_array('allService', $types)) {
            /**
             * load model
             */
            if (!$this->modelInquiryService) {
                $this->modelInquiryService = new Model_InquiryService();
            }
            if (!$this->modelService) {
                $this->modelService = new Model_Services();
            }

            $inquiryServices = $this->modelInquiryService->getByInquiryId($row['inquiry_id']);
            $allService = array();
            foreach ($inquiryServices as $inquiryService) {
                $service = $this->modelService->getById($inquiryService['service_id']);

                $allService[$service['service_id']] = $service['service_name'];
            }

            $row['allService'] = $allService;
        }
        if (in_array('service_attribute', $types)) {

            /**
             * load model
             */
            if (!$this->modelInquiryService) {
                $this->modelInquiryService = new Model_InquiryService();
            }
            if (!$this->modelService) {
                $this->modelService = new Model_Services();
            }
            if (!$this->modelServiceAttribute) {
                $this->modelServiceAttribute = new Model_ServiceAttribute();
            }
            if (!$this->modelAttributeListValue) {
                $this->modelAttributeListValue = new Model_AttributeListValue();
            }
            if (!$this->modelInquiryServiceAttributeValue) {
                $this->modelInquiryServiceAttributeValue = new Model_InquiryServiceAttributeValue();
            }
            if (!$this->modelAttributeType) {
                $this->modelAttributeType = new Model_AttributeType();
            }

            $allInquiryService = $this->modelInquiryService->getByInquiryId($row['inquiry_id']);
            foreach ($allInquiryService as &$inquiryService) {

                $service = $this->modelService->getById($inquiryService['service_id']);

                $inquiryService['service_name'] = $service['service_name'];

                //get service attribute
                $allServiceAttribute = $this->modelServiceAttribute->getAttributeByServiceId($inquiryService['service_id'], true);

                $attributes = array();
                foreach ($allServiceAttribute as &$serviceAttribute) {
                    $inquiryServiceAttributeValue = $this->modelInquiryServiceAttributeValue->getByInquiryIdAndServiceAttributeIdAndClone($row['inquiry_id'], $serviceAttribute['service_attribute_id'], $inquiryService['clone']);
                    $attributeType = $this->modelAttributeType->getById($serviceAttribute['attribute_type_id']);

                    $value = '';
                    if ($attributeType['is_list']) {
                        if ($inquiryServiceAttributeValue['is_serialized_array']) {
                            $unserializeValues = unserialize($inquiryServiceAttributeValue['value']);
                            if (is_array($unserializeValues)) {
                                $values = array();
                                foreach ($unserializeValues as $unserializeValue) {
                                    $attributeListValue = $this->modelAttributeListValue->getById($unserializeValue);
                                    if ($attributeListValue['unit_price']) {
                                        $values[] = number_format($attributeListValue['unit_price'], 2);
                                    } else {
                                        $values[] = $attributeListValue['attribute_value'];
                                    }
                                }
                                $value = $values;
                            }
                        } else {
                            $attributeListValue = $this->modelAttributeListValue->getById($inquiryServiceAttributeValue['value']);
                            if ($attributeListValue['unit_price']) {
                                $value = number_format($attributeListValue['unit_price'], 2);
                            } else {
                                $value = $attributeListValue['attribute_value'];
                            }
                        }
                    } else {
                        $value = $inquiryServiceAttributeValue['value'];
                    }

                    $serviceAttribute['value'] = $value;
                    $inquiryService['attributes'] = $allServiceAttribute;
                }
            }
            $row['service_attribute'] = $allInquiryService;
        }

        if (in_array('customer', $types)) {
            /**
             * load model
             */
            if (!$this->modelCustomer) {
                $this->modelCustomer = new Model_Customer();
            }

            $row['customer'] = $this->modelCustomer->getById($row['customer_id']);
            $row['customer_name'] = get_customer_name($row['customer']);
        }
        if (in_array('full_customer_info', $types)) {
            /**
             * load model
             */
            if (!$this->modelCustomer) {
                $this->modelCustomer = new Model_Customer();
            }

            $customer = $this->modelCustomer->getById($row['customer_id']);
            $this->modelCustomer->fill($customer, array('customer_commercial_info', 'customer_contacts'));

            $row['customer'] = $customer;
            $row['customer_name'] = get_customer_name($customer);
        }
        if (in_array('reminder', $types)) {
            /**
             * load model
             */
            if (!$this->modelInquiryReminder) {
                $this->modelInquiryReminder = new Model_InquiryReminder();
            }

            $row['reminder'] = $this->modelInquiryReminder->getContactHistory($row['inquiry_id']);
        }
        if (in_array('city', $types)) {
            /**
             * load model
             */
            if (!$this->modelCities) {
                $this->modelCities = new Model_Cities();
            }
            if (!$this->modelCountries) {
                $this->modelCountries = new Model_Countries();
            }

            $row['city'] = $this->modelCities->getById($row['city_id']);
            $row['city_name'] = $row['city']['city_name'];
            $countryName = $this->modelCountries->getById($row['city']['country_id']);
            $row['country_name'] = $countryName['country_name'];
        }

        if (in_array('labels', $types)) {
            /**
             * load model
             */
            if (!$this->modelInquiryLabel) {
                $this->modelInquiryLabel = new Model_InquiryLabel();
            }
            if (!$this->modelLabel) {
                $this->modelLabel = new Model_Label();
            }

            $LabelIds = $this->modelInquiryLabel->getByInquiryId($row['inquiry_id']);
            $labels = array();

            foreach ($LabelIds as $LabelId) {
                $label = $this->modelLabel->getById($LabelId['label_id']);
                $labels[$label['id']] = $label['label_name'];
            }
            $row['labels'] = $labels;
        }
        if (in_array('have_attachment', $types)) {
            if (!$this->modelBookingAttachment) {
                $this->modelBookingAttachment = new Model_BookingAttachment();
            }
            $have_attachment = $this->modelBookingAttachment->getByInquiryId($row['inquiry_id']);
            $row['have_attachment'] = !empty($have_attachment) ? 1 : 0;
        }
        return $row;
    }

    public function updateFullTextSearch($inquiryId) {


        $inquiry = $this->getById($inquiryId);


        $fullTextSearch = array();


        // inquiry       
        $fullTextSearch[] = trim($inquiry['inquiry_num']);
        $fullTextSearch[] = trim($inquiry['comment']);

        //customer
        $modelCustomer = new Model_Customer();
        $fullTextCustomerInquiry = $modelCustomer->getFullTextCustomerContacts($inquiry['customer_id']);
        $fullTextSearch[] = trim($fullTextCustomerInquiry);

        // booking
        $modelBooking = new Model_Booking();
        $modelBookingStatus = new Model_BookingStatus();
        $booking = $modelBooking->getByInquiryId($inquiryId);

        if ($booking) {
            $bookingId = $booking['booking_id'];
            $fullTextSearch[] = trim($booking['booking_num']);
            $fullTextSearch[] = trim($booking['title']);
            $fullTextSearch[] = trim($booking['description']);
            $status = $modelBookingStatus->getById($booking['status_id']);
            $fullTextSearch[] = trim($status['name']);


            // address
            $modelBookingAddress = new Model_BookingAddress();
            $bookingAddress = $modelBookingAddress->getByBookingId($bookingId);
            $fullTextSearch[] = trim(get_line_address($bookingAddress));

            //customer
            $fullTextCustomer = $modelCustomer->getFullTextCustomerContacts($booking['customer_id']);
            $fullTextSearch[] = trim($fullTextCustomer);

            // services
            $modelContractorServiceBooking = new Model_ContractorServiceBooking();
            $fullTextContractorServiceBooking = $modelContractorServiceBooking->getFullTextContractorServiceBooking($bookingId);
            if ($fullTextContractorServiceBooking) {
                $fullTextSearch[] = trim($fullTextContractorServiceBooking);
            }

            //estimate
            $modelBookingEstimate = new Model_BookingEstimate();
            $estimate = $modelBookingEstimate->getByBookingId($bookingId);
            if (isset($estimate['estimate_num']) && $estimate['estimate_num']) {
                $fullTextSearch[] = trim($estimate['estimate_num']);
            }

            //invoice
            $modelBookingInvoice = new Model_BookingInvoice();
            $invoice = $modelBookingInvoice->getByBookingId($bookingId);
            if (isset($invoice['invoice_num']) && $invoice['invoice_num']) {
                $fullTextSearch[] = trim($invoice['invoice_num']);
            }
            if (isset($invoice['invoice_type']) && $invoice['invoice_type']) {
                $fullTextSearch[] = trim($invoice['invoice_type']);
            }

            //complaint
            $model_Complaint = new Model_Complaint();
            $complaints = $model_Complaint->getFullTextComplaintByBookingId($bookingId);
            if ($complaints) {
                $fullTextSearch[] = trim($complaints);
            }
        }

        $data = array();
        $data['full_text_search'] = implode(' ', $fullTextSearch);

        $this->updateById($inquiryId, $data);
    }

    public function deleteRelatedInquiry($id) {

        //delete data from inquiry_label
        $this->getAdapter()->delete('inquiry_label', "inquiry_id = '{$id}'");

        //delete data from inquiry_reminder
        $this->getAdapter()->delete('inquiry_reminder', "inquiry_id = '{$id}'");

        //delete data from inquiry_discussion
        $this->getAdapter()->delete('inquiry_discussion', "inquiry_id = '{$id}'");

        //delete data from customer_reminder
        $this->getAdapter()->delete('customer_reminder', "inquiry_id = '{$id}'");

        //delete data from inquiry_address
        $this->getAdapter()->delete('inquiry_address', "inquiry_id = '{$id}'");

        //delete data from booking_attachment
        $this->getAdapter()->delete('booking_attachment', "inquiry_id = '{$id}'");

        //delete data from inquiry_service
        $this->getAdapter()->delete('inquiry_service', "inquiry_id = '{$id}'");

        //delete data from inquiry_service_attribute_value
        $this->getAdapter()->delete('inquiry_service_attribute_value', "inquiry_id = '{$id}'");

        //delete data from inquiry_type_attribute_value
        $this->getAdapter()->delete('inquiry_type_attribute_value', "inquiry_id = '{$id}'");
    }

    public function cronJobSendInquiryContactAttempt() {

        //
        // load models
        //
        $modelCustomer = new Model_Customer();
        $modelInquiryAddress = new Model_InquiryAddress();
        $modelInquiryService = new Model_InquiryService();
        $modelUser = new Model_User();

        // sub query
        $selectInquiryLabel = $this->getAdapter()->select();
        $selectInquiryLabel->from('inquiry_label', array('inquiry_id'));

        $select = $this->getAdapter()->select();
        $select->from(array('i' => $this->_name));
        $select->distinct();
        $select->where("i.status = 'inquiry'");
        $select->where("i.is_deleted = 0");
        $select->where("i.send_inquiry_contact_attempt != 'done'");
        $select->where("i.inquiry_id NOT IN ?", $selectInquiryLabel);
        $select->joinInner(array('ir' => 'inquiry_reminder'), 'i.inquiry_id = ir.inquiry_id', '');
        $inquiries = $this->getAdapter()->fetchAll($select);

        foreach ($inquiries as $inquiry) {

            $customer = $modelCustomer->getById($inquiry['customer_id']);
            $user = $modelUser->getById($inquiry['user_id']);

            $template_params = array(
                //inquiry
                '{inquiry_num}' => $inquiry['inquiry_num'],
                '{comment}' => $inquiry['comment'] ? $inquiry['comment'] : '',
                '{inquiry_created}' => date('d/m/Y', $inquiry['created']),
                '{inquiry_address}' => get_line_address($modelInquiryAddress->getByInquiryId($inquiry['inquiry_id'])),
                '{service}' => nl2br($modelInquiryService->getByInquiryIdAsText($inquiry['inquiry_id'])),
                //customer
                '{customer_name}' => get_customer_name($customer),
                '{customer_first_name}' => isset($customer['first_name']) && $customer['first_name'] ? ucwords($customer['first_name']) : '',
                '{customer_last_name}' => isset($customer['last_name']) && $customer['last_name'] ? ' ' . ucwords($customer['last_name']) : '',
                '{customer_contacts}' => nl2br($modelCustomer->getCustomerContacts($inquiry['customer_id'])),
                '{sender_name}' => ucwords($user['username'])
            );


            $to = array();
            if ($customer['email1']&& filter_var($customer['email1'], FILTER_VALIDATE_EMAIL)) {
                $to[] = $customer['email1'];
            }
            if ($customer['email2']&& filter_var($customer['email2'], FILTER_VALIDATE_EMAIL)) {
                $to[] = $customer['email2'];
            }
            if ($customer['email3']&& filter_var($customer['email3'], FILTER_VALIDATE_EMAIL)) {
                $to[] = $customer['email3'];
            }
            $to = implode(',', $to);

            $email_log = array('reference_id' => $inquiry['inquiry_id'], 'type' => 'inquiry');

            $companyId = $inquiry['company_id'];

            $success = false;
            if ($to) {
                try {
					EmailNotification::sendEmail(array('to' => $to), 'send_inquiry_contact_attempt', $template_params, $email_log, $companyId);
					$sendInquiryContactAttempt = 'done';
					} 
				catch( Zend_Mail_Transport_Exception $e ) {
					$sendInquiryContactAttempt = 'error';
					}
			}
            $this->updateById($inquiry['inquiry_id'], array('send_inquiry_contact_attempt' => $sendInquiryContactAttempt));
        }
    }

    public function cronJobResetSendInquiryContactAttempt() {

        // sub query
        $selectInquiryLabel = $this->getAdapter()->select();
        $selectInquiryLabel->from('inquiry_label', array('inquiry_id'));

        $select = $this->getAdapter()->select();
        $select->from(array('i' => $this->_name));
        $select->distinct();
        $select->where("i.status = 'inquiry'");
        $select->where("i.is_deleted = 0");
        $select->where("i.inquiry_id NOT IN ?", $selectInquiryLabel);
        $select->joinInner(array('ir' => 'inquiry_reminder'), 'i.inquiry_id = ir.inquiry_id', '');
        $inquiries = $this->getAdapter()->fetchAll($select);

        foreach ($inquiries as $inquiry) {
            $this->updateById($inquiry['inquiry_id'], array('send_inquiry_contact_attempt' => 'none'));
        }
    }

}