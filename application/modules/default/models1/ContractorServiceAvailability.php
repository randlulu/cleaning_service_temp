<?php

class Model_ContractorServiceAvailability extends Zend_Db_Table_Abstract {

    protected $_name = 'contractor_service_availability';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from(array('csa' => $this->_name));
        $select->joinInner(array('c' => 'city'), 'csa.city_id=c.city_id', array('c.city_name'));
        $select->order($order);

        if ($filters) {
            if (!empty($filters['contractor_service_id'])) {
                $select->where("csa.contractor_service_id = {$filters['contractor_service_id']}");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAllService($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from(array('csa' => $this->_name), array('csa.city_id'));
        $select->joinInner(array('cs' => 'contractor_service'), 'csa.contractor_service_id=cs.contractor_service_id', array('cs.service_id'));
        $select->joinInner(array('s' => 'service'), 'cs.service_id=s.service_id', array('*'));
        $select->joinInner(array('c' => 'city'), 'csa.city_id=c.city_id', array('c.city_name'));
        $select->distinct();
        $select->order($order);

        $filters['company_id'] = CheckAuth::getCompanySession();

        if ($filters) {
            if (!empty($filters['keywords'])) {
                $keywords = $this->getAdapter()->quote('%' . $filters['keywords'] . '%');
                $select->where("s.service_name LIKE {$keywords}");
            }

            if (!empty($filters['city_id'])) {
                $select->where("c.city_id = {$filters['city_id']}");
            }

            if (!empty($filters['services'])) {
                $services = implode(', ', $filters['services']);
                $select->where("s.service_id NOT IN ({$services})");
            }

            if (!empty($filters['company_id'])) {
                $select->where("s.company_id = {$filters['company_id']}");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "id = '{$id}'");
    }

    /**
     * delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("id = '{$id}'");
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the assigned CityId
     * 
     * @param int $id
     * @return array
     */
    public function getServiceByCityId($cityId) {
        $cityId = (int) $cityId;
        $filters['city_id'] = $cityId;

        return $this->getAllService($filters);
    }
    
    /**
     * get By City Id And Service Id
     *
     * @param int $cityId
     * @param int $serviceId
     * @return array 
     */
    public function getByCityIdAndServiceId($cityId,$serviceId) {
        $cityId = (int) $cityId;
        $serviceId = (int) $serviceId;
        $filters['city_id'] = $cityId;
        $filters['services'] = array($serviceId);

        return $this->getAllService($filters);
    }

    /**
     * get table row according to the assigned Service Id
     * 
     * @param int $id
     * @return array
     */
    public function getCitiesByContractorServiceId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("contractor_service_id = '{$id}'");

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * get table row according to the assigned CityId and ServiceId
     * 
     * @param int $city_id
     * @param int $contractor_service_id
     * @return array
     */
    public function getByCityIdAndContractorServiceId($city_id, $contractor_service_id) {
        $contractor_service_id = (int) $contractor_service_id;
        $city_id = (int) $city_id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("contractor_service_id = '{$contractor_service_id}'");
        $select->where("city_id = '{$city_id}'");

        return $this->getAdapter()->fetchRow($select);
    }

}