<?php

class Model_Label extends Zend_Db_Table_Abstract {

    protected $_name = 'label';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order($order);

        $filters['company_id'] = CheckAuth::getCompanySession();

        if ($filters) {
            if (!empty($filters['label_name'])) {
                $select->where("label_name = {$filters['label_name']}");
            }
            if (!empty($filters['keywords'])) {
                $keywords = $this->getAdapter()->quote('%' . $filters['keywords'] . '%');
                $select->where("label_name like {$keywords}");
            }
            if (!empty($filters['company_id'])) {
                $company_id = (int) $filters['company_id'];
                $select->where("company_id = {$company_id}");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }


        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table rows according to the assigned id and data
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "id = '{$id}'");
    }

    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("id = '{$id}'");
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function getByLabelName($labelName) {

        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("label_name = '{$labelName}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function getByLabelNameAndCompany($label_name, $companyId = 0) {

        if (!$companyId) {
            $companyId = CheckAuth::getCompanySession();
        }

        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("label_name = '{$label_name}'");
        $select->where("company_id = '{$companyId}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function checkBeforeDeleteLabel($labelId, &$tables = array()) {

        $sucsess = true;

        $select_booking_label = $this->getAdapter()->select();
        $select_booking_label->from('booking_label', 'COUNT(*)');
        $select_booking_label->where("label_id = {$labelId}");
        $count_booking_label = $this->getAdapter()->fetchOne($select_booking_label);

        if ($count_booking_label) {
            $tables[] = 'booking_label';
            $sucsess = false;
        }

        $select_estimate_label = $this->getAdapter()->select();
        $select_estimate_label->from('estimate_label', 'COUNT(*)');
        $select_estimate_label->where("label_id = {$labelId}");
        $count_estimate_label = $this->getAdapter()->fetchOne($select_estimate_label);

        if ($count_estimate_label) {
            $tables[] = 'estimate_label';
            $sucsess = false;
        }

        $select_invoice_label = $this->getAdapter()->select();
        $select_invoice_label->from('invoice_label', 'COUNT(*)');
        $select_invoice_label->where("label_id = {$labelId}");
        $count_invoice_label = $this->getAdapter()->fetchOne($select_invoice_label);

        if ($count_invoice_label) {
            $tables[] = 'invoice_label';
            $sucsess = false;
        }

        $select_inquiry_label = $this->getAdapter()->select();
        $select_inquiry_label->from('inquiry_label', 'COUNT(*)');
        $select_inquiry_label->where("label_id = {$labelId}");
        $count_inquiry_label = $this->getAdapter()->fetchOne($select_inquiry_label);

        if ($count_inquiry_label) {
            $tables[] = 'inquiry_label';
            $sucsess = false;
        }

        return $sucsess;
    }

}
