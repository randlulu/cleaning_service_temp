<?php

class Model_Companies extends Zend_Db_Table_Abstract {

    protected $_name = 'company';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from(array('cp' => $this->_name));
        $select->joinInner(array('ci' => 'city'), 'ci.city_id = cp.city_id', array('ci.city_name'));
        $select->joinInner(array('co' => 'country'), 'ci.country_id = co.country_id', array('co.country_name'));

        if (!CheckAuth::checkCredential(array('canHandleAllCompanies'))) {
            $filters['company_id'] = CheckAuth::getCompanySession();
        }

        if ($filters) {
            if (!empty($filters['company_id'])) {
                $select->where("company_id = {$filters['company_id']}");
            }
        }

        $select->order($order);

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }
	
	

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "company_id= '{$id}'");
    }

    /**
     * delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("company_id= '{$id}'");
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("company_id= '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getByName($name) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("company_name= '{$name}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table rows as array
     * 
     * @return array
     */
    public function getCompaniesAsArray() {

        $companies = $this->getAll(array(), 'company_name asc');

        $data = array();
        foreach ($companies as $company) {
            $data[] = array(
                'id' => $company['company_id'],
                'name' => $company['company_name']
            );
        }
        return $data;
    }
    
    public function getCronJobCompanies() {

        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        
        return $this->getAdapter()->fetchAll($select);
    }

    public function getByCallOutFeeAndCompany($callOutFee, $companyId = 0) {

        if (!$companyId) {
            $companyId = CheckAuth::getCompanySession();
        }

        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("call_out_fee = '{$callOutFee}'");
        $select->where("company_id = '{$companyId}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function checkBeforeDeleteCompany($id, &$tables = array()) {

        $sucsess = true;

        $select_booking = $this->getAdapter()->select();
        $select_booking->from('booking', 'COUNT(*)');
        $select_booking->where("company_id = {$id}");
        $count_booking = $this->getAdapter()->fetchOne($select_booking);

        if ($count_booking) {
            $tables[] = 'booking';
            $sucsess = false;
        }

        $select_inquiry = $this->getAdapter()->select();
        $select_inquiry->from('inquiry', 'COUNT(*)');
        $select_inquiry->where("company_id = {$id}");
        $count_inquiry = $this->getAdapter()->fetchOne($select_inquiry);

        if ($count_inquiry) {
            $tables[] = 'inquiry';
            $sucsess = false;
        }

        $select_booking_status = $this->getAdapter()->select();
        $select_booking_status->from('booking_status', 'COUNT(*)');
        $select_booking_status->where("company_id = {$id}");
        $count_booking_status = $this->getAdapter()->fetchOne($select_booking_status);

        if ($count_booking_status) {
            $tables[] = 'booking_status';
            $sucsess = false;
        }

        $select_inquiry_required_type = $this->getAdapter()->select();
        $select_inquiry_required_type->from('inquiry_required_type', 'COUNT(*)');
        $select_inquiry_required_type->where("company_id = {$id}");
        $count_inquiry_required_type = $this->getAdapter()->fetchOne($select_inquiry_required_type);

        if ($count_inquiry_required_type) {
            $tables[] = 'inquiry_required_type';
            $sucsess = false;
        }

        $select_property_type = $this->getAdapter()->select();
        $select_property_type->from('property_type', 'COUNT(*)');
        $select_property_type->where("company_id = {$id}");
        $count_property_type = $this->getAdapter()->fetchOne($select_property_type);

        if ($count_property_type) {
            $tables[] = 'property_type';
            $sucsess = false;
        }

        $select_label = $this->getAdapter()->select();
        $select_label->from('label', 'COUNT(*)');
        $select_label->where("company_id = {$id}");
        $count_label = $this->getAdapter()->fetchOne($select_label);

        if ($count_label) {
            $tables[] = 'label';
            $sucsess = false;
        }

        $select_canned_responses = $this->getAdapter()->select();
        $select_canned_responses->from('canned_responses', 'COUNT(*)');
        $select_canned_responses->where("company_id = {$id}");
        $count_canned_responses = $this->getAdapter()->fetchOne($select_canned_responses);

        if ($count_canned_responses) {
            $tables[] = 'canned_responses';
            $sucsess = false;
        }

        $select_bank = $this->getAdapter()->select();
        $select_bank->from('bank', 'COUNT(*)');
        $select_bank->where("company_id = {$id}");
        $count_bank = $this->getAdapter()->fetchOne($select_bank);

        if ($count_bank) {
            $tables[] = 'bank';
            $sucsess = false;
        }

        $select_product = $this->getAdapter()->select();
        $select_product->from('product', 'COUNT(*)');
        $select_product->where("company_id = {$id}");
        $count_product = $this->getAdapter()->fetchOne($select_product);

        if ($count_product) {
            $tables[] = 'product';
            $sucsess = false;
        }

        $select_payment_type = $this->getAdapter()->select();
        $select_payment_type->from('payment_type', 'COUNT(*)');
        $select_payment_type->where("company_id = {$id}");
        $count_payment_type = $this->getAdapter()->fetchOne($select_payment_type);

        if ($count_payment_type) {
            $tables[] = 'payment_type';
            $sucsess = false;
        }

        $select_service = $this->getAdapter()->select();
        $select_service->from('service', 'COUNT(*)');
        $select_service->where("company_id = {$id}");
        $count_service = $this->getAdapter()->fetchOne($select_service);

        if ($count_service) {
            $tables[] = 'service';
            $sucsess = false;
        }

        $select_image_attachment = $this->getAdapter()->select();
        $select_image_attachment->from('image_attachment', 'COUNT(*)');
        $select_image_attachment->where("company_id = {$id}");
        $count_image_attachment = $this->getAdapter()->fetchOne($select_image_attachment);

        if ($count_image_attachment) {
            $tables[] = 'image_attachment';
            $sucsess = false;
        }

        $select_attribute = $this->getAdapter()->select();
        $select_attribute->from('attribute', 'COUNT(*)');
        $select_attribute->where("company_id = {$id}");
        $count_attribute = $this->getAdapter()->fetchOne($select_attribute);

        if ($count_attribute) {
            $tables[] = 'attribute';
            $sucsess = false;
        }

        $select_user_company = $this->getAdapter()->select();
        $select_user_company->from('user_company', 'COUNT(*)');
        $select_user_company->where("company_id = {$id}");
        $count_user_company = $this->getAdapter()->fetchOne($select_user_company);

        if ($count_user_company) {
            $tables[] = 'user_company';
            $sucsess = false;
        }

        $select_page = $this->getAdapter()->select();
        $select_page->from('page', 'COUNT(*)');
        $select_page->where("company_id = {$id}");
        $count_page = $this->getAdapter()->fetchOne($select_page);

        if ($count_page) {
            $tables[] = 'page';
            $sucsess = false;
        }

        $select_report = $this->getAdapter()->select();
        $select_report->from('report', 'COUNT(*)');
        $select_report->where("company_id = {$id}");
        $count_report = $this->getAdapter()->fetchOne($select_report);

        if ($count_report) {
            $tables[] = 'report';
            $sucsess = false;
        }

        $select_company_invoice_note = $this->getAdapter()->select();
        $select_company_invoice_note->from('company_invoice_note', 'COUNT(*)');
        $select_company_invoice_note->where("company_id = {$id}");
        $count_company_invoice_note = $this->getAdapter()->fetchOne($select_company_invoice_note);

        if ($count_company_invoice_note) {
            $tables[] = 'company_invoice_note';
            $sucsess = false;
        }

        $select_complaint_type = $this->getAdapter()->select();
        $select_complaint_type->from('complaint_type', 'COUNT(*)');
        $select_complaint_type->where("company_id = {$id}");
        $count_complaint_type = $this->getAdapter()->fetchOne($select_complaint_type);

        if ($count_complaint_type) {
            $tables[] = 'complaint_type';
            $sucsess = false;
        }

        $select_complaint = $this->getAdapter()->select();
        $select_complaint->from('complaint', 'COUNT(*)');
        $select_complaint->where("company_id = {$id}");
        $count_complaint = $this->getAdapter()->fetchOne($select_complaint);

        if ($count_complaint) {
            $tables[] = 'complaint';
            $sucsess = false;
        }

        $select_customer = $this->getAdapter()->select();
        $select_customer->from('customer', 'COUNT(*)');
        $select_customer->where("company_id = {$id}");
        $count_customer = $this->getAdapter()->fetchOne($select_customer);

        if ($count_customer) {
            $tables[] = 'customer';
            $sucsess = false;
        }

        $select_customer_type = $this->getAdapter()->select();
        $select_customer_type->from('customer_type', 'COUNT(*)');
        $select_customer_type->where("company_id = {$id}");
        $count_customer_type = $this->getAdapter()->fetchOne($select_customer_type);

        if ($count_customer_type) {
            $tables[] = 'customer_type';
            $sucsess = false;
        }

        $select_customer_contact_label = $this->getAdapter()->select();
        $select_customer_contact_label->from('customer_contact_label', 'COUNT(*)');
        $select_customer_contact_label->where("company_id = {$id}");
        $count_customer_contact_label = $this->getAdapter()->fetchOne($select_customer_contact_label);

        if ($count_customer_contact_label) {
            $tables[] = 'customer_contact_label';
            $sucsess = false;
        }

        $select_inquiry_type = $this->getAdapter()->select();
        $select_inquiry_type->from('inquiry_type', 'COUNT(*)');
        $select_inquiry_type->where("company_id = {$id}");
        $count_inquiry_type = $this->getAdapter()->fetchOne($select_inquiry_type);

        if ($count_inquiry_type) {
            $tables[] = 'inquiry_type';
            $sucsess = false;
        }

        return $sucsess;
    }
	
	
	

}