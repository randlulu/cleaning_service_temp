<?php

class Model_Cities extends Zend_Db_Table_Abstract {

    protected $_name = 'city';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from(array('c' => $this->_name));
        $select->joinInner(array('co' => 'country'), 'c.country_id = co.country_id', array('co.country_name'));
        $select->order($order);

        $where = array();

        if ($filters) {
            if (!empty($filters['keywords'])) {
                $keywords = $this->getAdapter()->quote('%' . $filters['keywords'] . '%');
                $where[] = ("city_name LIKE {$keywords}");
            }
            if (isset($filters['country_id']) && $filters['country_id'] != 0) {
                $where[] = ("c.country_id = {$filters['country_id']}");
            }
            if (!empty($filters['state'])) {
                $state = $this->getAdapter()->quote($filters['state']);
                $where[] = ("state = {$state}");
            }
            if ($where) {
                $select->where(implode(' AND ', $where));
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "city_id= '{$id}'");
    }

    /**
     * delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("city_id= '{$id}'");
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("city_id= '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the assigned City Name
     * 
     * @param string $city_name
     * @return array
     */
    public function getByName($city_name) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("city_name= '{$city_name}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get City and Country Name according to the assigned City Id
     * 
     * @param id $city_id
     * @return array
     */
    public function getCityAndCountryNameByCityId($city_id) {
        $city_id = (int) $city_id;
        $select = $this->getAdapter()->select();
        $select->from(array('c' => $this->_name), array('c.city_name', 'c.state'));
        $select->joinInner(array('co' => 'country'), 'c.country_id = co.country_id', array('co.country_name'));
        $select->where("city_id= '{$city_id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @param boolean $asArray
     * @return array
     */
    public function getByCountryId($id, $asArray = false) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("country_id= '{$id}'");

        $countries = $this->getAdapter()->fetchAll($select);

        if ($asArray) {
            $data = array();
            foreach ($countries as $country) {
                $data[$country['country_id']] = $country['country_name'];
            }
            return $data;
        } else {
            return $countries;
        }
    }

    /**
     * get table rows as array
     * 
     * @return array
     */
    public function getStateCitiesAsArray() {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order('city_name asc');

        $cities = $this->getAdapter()->fetchAll($select);

        $data = array();
        foreach ($cities as $city) {
            $data[] = array(
                'id' => $city['city_id'],
                'name' => $city['city_name']
            );
        }
        return $data;
    }

    /**
     * get table rows as array
     * 
     * @return array
     */
    public function getCitiesAsArray() {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order('city_name asc');

        $cities = $this->getAdapter()->fetchAll($select);

        $data = array();
        foreach ($cities as $city) {
            $data[$city['city_id']] = $city['city_name'];
        }
        return $data;
    }

    /**
     * get table row according to the assigned state
     * 
     * @param string $state
     * @param boolean $asArray
     * @return array
     */
    public function getByState($state, $asArray = false) {
        $state = $this->getAdapter()->quote($state);
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("state = {$state}");

        $states = $this->getAdapter()->fetchAll($select);

        if ($asArray) {
            $data = array();
            foreach ($states as $state) {
                $data[$state['state']] = $state['state'];
            }
            return $data;
        } else {
            return $states;
        }
    }

    /**
     * get table rows as array
     * 
     * @return array
     */
    public function getStateAsArray() {
        $select = $this->getAdapter()->select();
        $select->from(array('c' => $this->_name), 'c.state');
        $select->distinct();
        $select->order('state asc');

        $states = $this->getAdapter()->fetchAll($select);

        $data = array();
        foreach ($states as $state) {
            $data[$state['state']] = $state['state'];
        }
        return $data;
    }

    /**
     * get table row according to the assigned CountryId
     * 
     * @param int $id
     * @param boolean $asArray
     * @return array
     */
    public function getStateByCountryId($id, $asArray = false) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from(array('c' => $this->_name), 'c.state');
        $select->distinct();
        $select->order('c.state asc');
        $select->where("c.country_id= '{$id}'");

        $states = $this->getAdapter()->fetchAll($select);

        $data = array();
        foreach ($states as $state) {
            $data[$state['state']] = $state['state'];
        }
        return $data;
    }

    /**
     * get table row according to the assigned CountryId and State
     * 
     * @param string $id
     * @param string $state
     * @param boolean $asArray
     * @return array
     */
    public function getCitiesByCountryIdAndState($id, $state, $asArray = false) {
        $id = (int) $id;
        $state = $this->getAdapter()->quote($state);

        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("country_id= '{$id}'");
        $select->where("state = {$state}");

        $cities = $this->getAdapter()->fetchAll($select);

        if ($asArray) {
            $data = array();
            foreach ($cities as $city) {
                $data[$city['city_id']] = $city['city_name'];
            }
            return $data;
        } else {
            return $cities;
        }
    }

    public function checkBeforeDeleteCity($cityId, &$tables = array()) {

        $sucsess = true;

        $select_booking = $this->getAdapter()->select();
        $select_booking->from('booking', 'COUNT(*)');
        $select_booking->where("city_id = {$cityId}");
        $count_booking = $this->getAdapter()->fetchOne($select_booking);

        if ($count_booking) {
            $tables[] = 'booking';
            $sucsess = false;
        }

        $select_inquiry = $this->getAdapter()->select();
        $select_inquiry->from('inquiry', 'COUNT(*)');
        $select_inquiry->where("city_id = {$cityId}");
        $count_inquiry = $this->getAdapter()->fetchOne($select_inquiry);

        if ($count_inquiry) {
            $tables[] = 'inquiry';
            $sucsess = false;
        }

        $select_contractor_owner = $this->getAdapter()->select();
        $select_contractor_owner->from('contractor_owner', 'COUNT(*)');
        $select_contractor_owner->where("city_id = {$cityId}");
        $count_contractor_owner = $this->getAdapter()->fetchOne($select_contractor_owner);

        if ($count_contractor_owner) {
            $tables[] = 'contractor_owner';
            $sucsess = false;
        }

        $select_contractor_employee = $this->getAdapter()->select();
        $select_contractor_employee->from('contractor_employee', 'COUNT(*)');
        $select_contractor_employee->where("city_id = {$cityId}");
        $count_contractor_employee = $this->getAdapter()->fetchOne($select_contractor_employee);

        if ($count_contractor_employee) {
            $tables[] = 'contractor_employee';
            $sucsess = false;
        }

        $select_company = $this->getAdapter()->select();
        $select_company->from('company', 'COUNT(*)');
        $select_company->where("city_id = {$cityId}");
        $count_company = $this->getAdapter()->fetchOne($select_company);

        if ($count_company) {
            $tables[] = 'company';
            $sucsess = false;
        }

        $select_customer = $this->getAdapter()->select();
        $select_customer->from('customer', 'COUNT(*)');
        $select_customer->where("city_id = {$cityId}");
        $count_customer = $this->getAdapter()->fetchOne($select_customer);

        if ($count_customer) {
            $tables[] = 'customer';
            $sucsess = false;
        }

        $select_user = $this->getAdapter()->select();
        $select_user->from('user', 'COUNT(*)');
        $select_user->where("city_id = {$cityId}");
        $count_user = $this->getAdapter()->fetchOne($select_user);

        if ($count_user) {
            $tables[] = 'user';
            $sucsess = false;
        }

        $select_postal_address = $this->getAdapter()->select();
        $select_postal_address->from('postal_address', 'COUNT(*)');
        $select_postal_address->where("city_id = {$cityId}");
        $count_postal_address = $this->getAdapter()->fetchOne($select_postal_address);

        if ($count_postal_address) {
            $tables[] = 'postal_address';
            $sucsess = false;
        }


        return $sucsess;
    }

}