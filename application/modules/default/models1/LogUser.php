<?php

class Model_LogUser extends Zend_Db_Table_Abstract {

    protected $_name = 'log_user';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order($order);


        if ($filters) {
            if (!empty($filters['user_id'])) {
                $select->where("user_id = {$filters['user_id']}");
            }
            if (!empty($filters['item_id'])) {
                $select->where("item_id = {$filters['item_id']}");
            }
            if (!empty($filters['item_type'])) {
                $select->where("item_type = {$filters['item_type']}");
            }
            if (!empty($filters['event'])) {
                $select->where("event = {$filters['event']}");
            }
            if (!empty($filters['from'])) {
                $from = is_int($filters['from']) ? $filters['from'] : strtotime(date('Y-m-d', strtotime($filters['from'])) . ' 00:00:00');
                $to = !empty($filters['to']) ? (is_int($filters['to']) ? $filters['to'] : strtotime(date('Y-m-d', strtotime($filters['to'])) . ' 23:59:59')) : time();
                $select->where("created BETWEEN '" . $from . "' and '" . $to . "'");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }


        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table rows according to the assigned id and data
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "log_id = '{$id}'");
    }

    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("log_id = '{$id}'");
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("log_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function getEventByTime($item_id, $item_type, $event) {
        $item_id = (int) $item_id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("item_id = '{$item_id}'");
        $select->where("item_type = '{$item_type}'");

        if (!in_array($item_type, array('booking', 'inquiry', 'booking_estimate', 'booking_invoice', 'booking_attachment'))) {
            $select->where("event = '{$event}'");
        }

        $time = time() - 30; //Before 30 seconds
        $select->where("created > '$time'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * insert table row according to the event
     * 
     * @param int $id
     * @return array 
     */
    public function addUserLogEvent($item_id, $item_type, $event) {

        $loggedUser = CheckAuth::getLoggedUser();

        if ($loggedUser) {
            $data = array();
            $data['created'] = time();
            $data['event'] = $event;
            $data['item_id'] = $item_id;
            $data['item_type'] = $item_type;
            $data['user_id'] = $loggedUser['user_id'];

            $event = $this->getEventByTime($item_id, $item_type, $event);

            if (!$event) {
                $this->insert($data);
            }
        }
    }

}

