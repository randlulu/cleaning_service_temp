<?php

class Model_ServiceAttribute extends Zend_Db_Table_Abstract {

    protected $_name = 'service_attribute';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from(array('sa' => $this->_name));
        $select->joinInner(array('s' => 'service'), 'sa.service_id=s.service_id', array('s.service_name'));
        $select->joinInner(array('a' => 'attribute'), 'sa.attribute_id=a.attribute_id', array('a.attribute_name'));
        $select->order($order);

        if ($filters) {
            if (!empty($filters['service_id'])) {
                $select->where("sa.service_id = {$filters['service_id']}");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table rows according to the assigned id and date
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "service_attribute_id = '{$id}'");
    }

    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("service_attribute_id = '{$id}'");
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("service_attribute_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getByServiceId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("service_id = '{$id}'");

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * get attribute according to the id without_pricing
     * 
     * @param int $id
     * @param tinyint $without_pricing
     * @return array 
     */
    public function getAttributeByServiceId($id, $without_pricing = 0) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from(array('sa' => $this->_name));
        $select->joinInner(array('a' => 'attribute'), 'sa.attribute_id=a.attribute_id', array('a.attribute_name', 'a.attribute_variable_name', 'a.is_price_attribute', 'a.attribute_type_id', 'a.extra_info'));
        $select->where("service_id = '{$id}'");
        if ($without_pricing) {
            $select->where("a.is_price_attribute = 0");
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * get attribute according to the id without_pricing
     * 
     * @param int $id
     * @param tinyint $without_pricing
     * @return array 
     */
    public function getByAttributeId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("attribute_id= '{$id}'");

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * get table row according to the attributeId, serviceId
     * 
     * @param int $attributeId
     * @param int $serviceId
     * @return array 
     */
    public function getByAttributeIdAndServiceId($attributeId, $serviceId) {
        $attributeId = (int) $attributeId;
        $serviceId = (int) $serviceId;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("attribute_id = '{$attributeId}' AND service_id = '{$serviceId}' ");

        return $this->getAdapter()->fetchRow($select);
    }

    public function checkBeforeDeleteServiceAttribute($serviceAttributeId, &$tables = array()) {

        $sucsess = true;

        $select_inquiry_service_attribute_value = $this->getAdapter()->select();
        $select_inquiry_service_attribute_value->from('inquiry_service_attribute_value', 'COUNT(*)');
        $select_inquiry_service_attribute_value->where("service_attribute_id = {$serviceAttributeId}");
        $count_inquiry_service_attribute_value = $this->getAdapter()->fetchOne($select_inquiry_service_attribute_value);

        if ($count_inquiry_service_attribute_value) {
            $tables[] = 'inquiry_service_attribute_value';
            $sucsess = false;
        }

        $select_service_attribute_value = $this->getAdapter()->select();
        $select_service_attribute_value->from('service_attribute_value', 'COUNT(*)');
        $select_service_attribute_value->where("service_attribute_id = {$serviceAttributeId}");
        $count_service_attribute_value = $this->getAdapter()->fetchOne($select_service_attribute_value);

        if ($count_service_attribute_value) {
            $tables[] = 'service_attribute_value';
            $sucsess = false;
        }

        $select_service_attribute_value_temp = $this->getAdapter()->select();
        $select_service_attribute_value_temp->from('service_attribute_value_temp', 'COUNT(*)');
        $select_service_attribute_value_temp->where("service_attribute_id = {$serviceAttributeId}");
        $count_service_attribute_value_temp = $this->getAdapter()->fetchOne($select_service_attribute_value_temp);

        if ($count_service_attribute_value_temp) {
            $tables[] = 'service_attribute_value_temp';
            $sucsess = false;
        }

        return $sucsess;
    }

}