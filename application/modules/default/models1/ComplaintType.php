<?php

class Model_ComplaintType extends Zend_Db_Table_Abstract {

    protected $_name = 'complaint_type';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order($order);

        $filters['company_id'] = CheckAuth::getCompanySession();

        if ($filters) {
            if (!empty($filters['keywords'])) {
                $keywords = $this->getAdapter()->quote('%' . $filters['keywords'] . '%');
                $select->where("name LIKE {$keywords}");
            }
            if (!empty($filters['company_id'])) {
                $company_id = (int) $filters['company_id'];
                $select->where("company_id = {$company_id}");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "complaint_type_id = '{$id}'");
    }

    /**
     * delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("complaint_type_id = '{$id}'");
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("complaint_type_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table rows as array
     * 
     * @return array
     */
    public function getAllComplaintTypeAsArray() {
        $allComplaintType = $this->getAll(array(), 'name asc');

        $data = array();
        foreach ($allComplaintType as $complaintType) {
            $data[$complaintType['complaint_type_id']] = $complaintType['name'];
        }
        return $data;
    }

    public function checkBeforeDeleteInquiryComplaintType($complaintTypeId, &$tables = array()) {

        $sucsess = true;

        $select_complaint = $this->getAdapter()->select();
        $select_complaint->from('complaint', 'COUNT(*)');
        $select_complaint->where("complaint_type_id = {$complaintTypeId}");
        $count_complaint = $this->getAdapter()->fetchOne($select_complaint);

        if ($count_complaint) {
            $tables[] = 'complaint';
            $sucsess = false;
        }

        return $sucsess;
    }

}