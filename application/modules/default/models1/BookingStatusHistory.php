<?php

class Model_BookingStatusHistory extends Zend_Db_Table_Abstract {

    protected $_name = 'booking_status_history';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order($order);



        if ($filters) {
            
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "id = '{$id}'");
    }

    /**
     * delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("id = '{$id}'");
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function getLastStatusIdByBookingId($bookingId) {

        $selectB = $this->getAdapter()->select();
        $selectB->from($this->_name, array('MAX(created) AS created'));
        $selectB->where("booking_id = '{$bookingId}'");
        $selectB->group('booking_id');

        $select = $this->getAdapter()->select();
        $select->from($this->_name,'status_id');
        $select->where("booking_id = '{$bookingId}'");
        $select->where("created = ({$selectB})");

        return $this->getAdapter()->fetchOne($select);
    }
    
    public function getLastStatusByBookingId($bookingId) {

        $selectB = $this->getAdapter()->select();
        $selectB->from($this->_name, array('MAX(created) AS created'));
        $selectB->where("booking_id = '{$bookingId}'");
        $selectB->group('booking_id');

        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_id = '{$bookingId}'");
        $select->where("created = ({$selectB})");

        return $this->getAdapter()->fetchRow($select);
    }
    
    public function getOldStatusIdByBookingId($bookingId) {

        $last = $this->getLastStatusByBookingId($bookingId);
        
        $selectB = $this->getAdapter()->select();
        $selectB->from($this->_name, array('MAX(created) AS created'));
        $selectB->where("booking_id = '{$bookingId}'");
        $selectB->where("id != '{$last['id']}'");
        $selectB->group('booking_id');

        $select = $this->getAdapter()->select();
        $select->from($this->_name,'status_id');
        $select->where("booking_id = '{$bookingId}'");
        $select->where("created = ({$selectB})");

        return $this->getAdapter()->fetchOne($select);
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getByBookingIdAndStatusId($bookingId, $statusId) {
        $bookingId = (int) $bookingId;
        $statusId = (int) $statusId;

        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_id = '{$bookingId}'");
        $select->where("status_id = '{$statusId}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function modifyStatusHistory($bookingId, $statusId, $parentStatusId = 0, $userId = 0) {

        $bookingId = (int) $bookingId;
        $statusId = (int) $statusId;
        $parentStatusId = (int) $parentStatusId;
        $userId = (int) $userId;

        if (!$userId) {
            $loggedUser = CheckAuth::getLoggedUser();
            $userId = isset($loggedUser['user_id']) && $loggedUser['user_id'] ? $loggedUser['user_id'] : 0;
        }

        if (!$parentStatusId) {
            $modelBooking = new Model_Booking();
            $booking = $modelBooking->getById($bookingId);

            $parentStatusId = isset($booking['status_id']) && $booking['status_id'] ? $booking['status_id'] : 0;
        }

        $this->addStatusHistory($bookingId, $statusId, $parentStatusId, $userId);
    }

    public function addStatusHistory($bookingId, $statusId, $parentStatusId = 0, $userId = 0) {

        $bookingId = (int) $bookingId;
        $statusId = (int) $statusId;
        $parentStatusId = (int) $parentStatusId;
        $userId = (int) $userId;

        if (!$userId) {
            $loggedUser = CheckAuth::getLoggedUser();
            $userId = isset($loggedUser['user_id']) && $loggedUser['user_id'] ? $loggedUser['user_id'] : 0;
        }

        $data = array(
            'booking_id' => $bookingId,
            'status_id' => $statusId,
            'parent_status_id' => $parentStatusId,
            'user_id' => $userId,
            'created' => time()
        );

        $this->insert($data);
    }

}