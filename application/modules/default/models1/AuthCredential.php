<?php

class Model_AuthCredential extends Zend_Db_Table_Abstract {

    protected $_name = 'auth_credential';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order($order);

        if ($filters) {
            if (!empty($filters['keywords'])) {
                $keywords = $this->getAdapter()->quote('%' . $filters['keywords'] . '%');
                $select->where("credential_name LIKE {$keywords} OR parent LIKE {$keywords} OR child LIKE {$keywords}");
            }
            if (!empty($filters['credential_name'])) {
                $credential_name = $this->getAdapter()->quote($filters['credential_name']);
                $select->where("credential_name = {$credential_name}");
            }
            if (!empty($filters['parent_id'])) {
                $parent_id = $this->getAdapter()->quote($filters['parent_id']);
                if ($filters['parent_id'] == 'root') {
                    $select->where("parent_id = 0");
                } else {
                    $select->where("parent_id = {$parent_id}");
                }
            }
            if (!empty($filters['is_hidden'])) {
                $is_hidden = (bool) $filters['is_hidden'];
                $is_hidden = $is_hidden ? 1 : 0;
                $select->where("is_hidden = {$is_hidden}");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "credential_id = '{$id}'");
    }

    /**
     * delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("credential_id = '{$id}'");
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("credential_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the assigned credentialName
     * 
     * @param string $credentialName
     * @return array
     */
    public function getByCredentialName($credentialName) {

        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("credential_name = '{$credentialName}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function getAllCredentialTree() {
        $credentials = $this->getAll(array('parent_id' => 'root'));
        $this->getCredentialTree($credentials);
        return $credentials;
    }

    public function getCredentialTree(&$credentials) {
        if (count($credentials) >= 1) {
            foreach ($credentials as $key => $credential) {
                $credentials[$key]['childs'] = $this->getAll(array('parent_id' => $credential['credential_id']));
                if (count($credentials[$key]['childs']) >= 1) {
                    $this->getCredentialTree($credentials[$key]['childs']);
                }
            }
        }
    }

    public function drowChilds($parentId, $childs, $old_credentials) {
        $out_child = '<ul>' . "\n";
        foreach ($childs as $child) {
            if ($child['childs']) {
                $out_child .= '<li class="tree_box" style="margin: 5px 0;">' . "\n";
            } else {
                $out_child .= '<li>' . "\n";
            }
            $out_child .= '<input type="checkbox" id="credential_' . ($parentId) . '_' . ($child['credential_id']) . '" name="credentials[]" ' . ($old_credentials ? (in_array($child['credential_id'], $old_credentials) ? 'checked' : '') : '') . ' value="' . ($child['credential_id']) . '"/>' . "\n";
            $out_child .= '<label for="credential_' . ($parentId) . '_' . ($child['credential_id']) . '">' . (spaceBeforeCapital(ucfirst($child['credential_name']))) . '</label>' . "\n";
            if ($child['childs']) {
                $out_child .= $this->drowChilds($child['credential_id'], $child['childs'], $old_credentials);
            }
            $out_child .= '</li>' . "\n";
        }
        $out_child .= '</ul>' . "\n";
        return $out_child;
    }

    public function drowChildsToEdit($parentId, $childs, $old_credentials) {
        $router = Zend_Controller_Front::getInstance()->getRouter();
        $out_child = '<ul>' . "\n";
        foreach ($childs as $child) {
            if ($child['childs']) {
                $out_child .= '<li class="tree_box" style="margin: 5px 0;">' . "\n";
            } else {
                $out_child .= '<li>' . "\n";
            }
            $out_child .= '<label for="credential_' . ($parentId) . '_' . ($child['credential_id']) . '">' . (spaceBeforeCapital(ucfirst($child['credential_name']))) . '</label>' . "\n";
            $out_child .= '<a href="javascript:;" onclick="showPopupWindow(\'' . $router->assemble(array('id' => $child['credential_id']), 'settingsAuthCredentialEdit') . '\',\'160\',\'450\');" style="font-size: 11px; font-weight: normal; color: #0099CC;">Edit</a> ';
            $out_child .= '<a href="javascript:;" onclick="showPopupWindow(\'' . $router->assemble(array('parent_id' => $child['credential_id']), 'settingsAuthCredentialAddParent') . '\',\'160\',\'450\');" style="font-size: 11px; font-weight: normal; color: #0099CC;">Add</a> ';
            if (!$child['childs']) {
                $out_child .= '<a href="' . $router->assemble(array('id' => $child['credential_id']), 'settingsAuthCredentialDelete') . '" onclick="return confirm(\'are you sure you want to delete this Credential?\');" style="font-size: 11px; font-weight: normal; color: #0099CC;">Delete</a> ';
            }
            if ($child['childs']) {
                $out_child .= $this->drowChildsToEdit($child['credential_id'], $child['childs'], $old_credentials);
            }
            $out_child .= '</li>' . "\n";
        }
        $out_child .= '</ul>' . "\n";
        return $out_child;
    }

    public function getAllAuthCredential($asArray = false) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);

        $results = $this->getAdapter()->fetchAll($select);

        if ($asArray) {
            $data = array();
            if ($results) {
                foreach ($results as $result) {
                    $data[$result['credential_id']] = spaceBeforeCapital(ucfirst($result['credential_name']));
                }
            }
            return $data;
        } else {
            return $results;
        }
    }

    public function getByParent($parent) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("parent_id = '{$parent}'");

        return $this->getAdapter()->fetchRow($select);
    }

}