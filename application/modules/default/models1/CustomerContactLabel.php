<?php

class Model_CustomerContactLabel extends Zend_Db_Table_Abstract {

    protected $_name = 'customer_contact_label';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from(array('ccl' => $this->_name));
        $select->order($order);

        if ($filters) {
            
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }
        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table rows according to the assigned id and data
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "id = '{$id}'");
    }

    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("id = '{$id}'");
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table rows as array
     * 
     * @return array
     */
    public function getContactLabelAsArray($company_id =0) {
        if (empty($company_id)) {
            $company_id = CheckAuth::getCompanySession();
        }
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order('contact_label');
        $select->where("company_id = '{$company_id}'");
        $contactLabels = $this->getAdapter()->fetchAll($select);

        $data = array();
        foreach ($contactLabels as $contactLabel) {

            $data[$contactLabel['id']] = $contactLabel['contact_label'];
        }
        return $data;
    }

    public function getByContactLabelAndCompany($contact_label, $companyId = 0) {

        if (!$companyId) {
            $companyId = CheckAuth::getCompanySession();
        }

        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("contact_label = '{$contact_label}'");
        $select->where("company_id = '{$companyId}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function checkBeforeDeleteCustomerContactLabelType($customerContactLabelId, &$tables = array()) {

        $sucsess = true;

        $select_customer_contact = $this->getAdapter()->select();
        $select_customer_contact->from('customer_contact', 'COUNT(*)');
        $select_customer_contact->where("customer_contact_label_id = {$customerContactLabelId}");
        $count_customer_contact = $this->getAdapter()->fetchOne($select_customer_contact);

        if ($count_customer_contact) {
            $tables[] = 'customer_contact';
            $sucsess = false;
        }

        return $sucsess;
    }

}

