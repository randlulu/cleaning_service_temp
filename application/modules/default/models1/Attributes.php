<?php

class Model_Attributes extends Zend_Db_Table_Abstract {

    protected $_name = 'attribute';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from(array('a' => $this->_name));
        $select->joinInner(array('at' => 'attribute_type'), 'a.attribute_type_id=at.attribute_type_id', array('at.is_list'));
        $select->order($order);

        $filters['company_id'] = CheckAuth::getCompanySession();

        if ($filters) {
            if (!empty($filters['keywords'])) {
                $keywords = $this->getAdapter()->quote('%' . $filters['keywords'] . '%');
                $select->where("a.attribute_name LIKE {$keywords}");
            }

            if (!empty($filters['company_id'])) {
                $select->where("a.company_id = {$filters['company_id']} OR a.company_id = 0");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "attribute_id= '{$id}'");
    }

    /**
     * delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("attribute_id= '{$id}'");
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("attribute_id= '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the assigned variable name
     * 
     * @param string $variable_name
     * @return array
     */
    public function getByVariableName($variable_name) {
        $select = $this->getAdapter()->select();
        $select->from(array('a' => $this->_name));
        $select->joinInner(array('at' => 'attribute_type'), 'a.attribute_type_id=at.attribute_type_id', array('is_list'));
        $select->where("attribute_variable_name= '{$variable_name}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table rows as array
     * 
     * @return array
     */
    public function getAttributeAsArray() {

        $attributes = $this->getAll(array(), 'attribute_name asc');

        $data = array();
        foreach ($attributes as $attribute) {
            $data[] = array(
                'id' => $attribute['attribute_id'],
                'name' => $attribute['attribute_name']
            );
        }
        return $data;
    }

    public function checkBeforeDeleteAttribute($attributeId, &$tables = array()) {

        $sucsess = true;

        $select_inquiry_type_attribute = $this->getAdapter()->select();
        $select_inquiry_type_attribute->from('inquiry_type_attribute', 'COUNT(*)');
        $select_inquiry_type_attribute->where("attribute_id = {$attributeId}");
        $count_inquiry_type_attribute = $this->getAdapter()->fetchOne($select_inquiry_type_attribute);

        if ($count_inquiry_type_attribute) {
            $tables[] = 'inquiry_type_attribute';
            $sucsess = false;
        }

        $select_service_attribute = $this->getAdapter()->select();
        $select_service_attribute->from('service_attribute', 'COUNT(*)');
        $select_service_attribute->where("attribute_id = {$attributeId}");
        $count_service_attribute = $this->getAdapter()->fetchOne($select_service_attribute);

        if ($count_service_attribute) {
            $tables[] = 'service_attribute';
            $sucsess = false;
        }

        $select_attribute_list_value = $this->getAdapter()->select();
        $select_attribute_list_value->from('attribute_list_value', 'COUNT(*)');
        $select_attribute_list_value->where("attribute_id = {$attributeId}");
        $count_attribute_list_value = $this->getAdapter()->fetchOne($select_attribute_list_value);

        if ($count_attribute_list_value) {
            $tables[] = 'attribute_list_value';
            $sucsess = false;
        }

        return $sucsess;
    }

}