<?php

class Model_EmailTemplate extends Zend_Db_Table_Abstract {

    protected $_name = 'email_template';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order($order);

        if ($filters) {
            if (!empty($filters['keywords'])) {
                $keywords = $this->getAdapter()->quote('%' . $filters['keywords'] . '%');
                $select->where("name LIKE {$keywords}");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table rows according to the assigned id and data
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "id = '{$id}'");
    }

    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("id = '{$id}'");
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the name
     * 
     * @param string $name
     * @return array 
     */
    public function getByName($name) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("name = '{$name}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function getEmailTemplate($template_name = '', $template_params = array(), $companyId = 0) {
        /**
         * START TMP CODE
         */
        $placeholder = array();
        foreach ($template_params as $key => $value) {
            $placeholder[] = $key;
        }

        $template = $this->getByName($template_name);
        if (!$template) {
            $db_params = array();
            $db_params['subject'] = $template_name;
            $db_params['body'] = $template_name . "<br><br>" . implode("<br>", $placeholder);
            $db_params['name'] = $template_name;
            $db_params['placeholder'] = implode(',', $placeholder);

            if ($template_name) {
                $this->insert($db_params);
            }
        } else {
            $db_params = array();
            $db_params['placeholder'] = implode(',', $placeholder);

            if ($template_name) {
                $this->updateById($template['id'],$db_params);
            }
        }
        /**
         * END TMP CODE
         */
        
        $loggedUser = CheckAuth::getLoggedUser();
        $general_placeholders = get_config('general_placeholders', array('email_notification'))->toArray();
        if ($general_placeholders) {

            if (!$companyId) {
                $companyId = CheckAuth::getCompanySession();
            }

            $modelCompanies = new Model_Companies();
            $company = $modelCompanies->getById($companyId);

            $general_placeholders['{company_name}'] = isset($company['company_name']) && $company['company_name'] ? $company['company_name'] : 'Company Name';
            $general_placeholders['{company_website}'] = isset($company['company_website']) && $company['company_website'] ? $company['company_website'] : 'Company Website';
            $general_placeholders['{company_enquiries_email}'] = isset($company['company_enquiries_email']) && $company['company_enquiries_email'] ? $company['company_enquiries_email'] : 'Company Enquiries Email';
            $general_placeholders['{send_date}'] = date('F j, Y, g:i a', time());
            $general_placeholders['{sender_name}'] = isset($loggedUser['display_name']) ? $loggedUser['display_name'] : '';

            if(empty($general_placeholders['{sender_name}'])){
                $general_placeholders['{sender_name}'] = isset($template_params['{sender_name}']) ? $template_params['{sender_name}'] : '';
            }
            
            $template_params = array_merge($general_placeholders, $template_params);
        }

        $body = '';
        $subject = '';
        if ($template_name) {
            $template = $this->getByName($template_name);
            if ($template) {
                $subject = $template['subject'];
                $body = $template['body'];
                foreach ($template_params as $key => $value) {
                    $subject = str_replace($key, $value, $subject);
                    $body = str_replace($key, $value, $body);
                }
            }
        }

        return array('body' => $body, 'subject' => $subject);
    }

}