<?php

class Model_BookingStatusDiscussion extends Zend_Db_Table_Abstract {

    protected $_name = 'booking_status_discussion';

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "id = '{$id}'");
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the assigned BookingId
     * 
     * @param int $id
     * @return array
     */
    public function getByBookingId($bookingId) {
        $bookingId = (int) $bookingId;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order('created ASC');
        $select->where("booking_id = '{$bookingId}'");

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * get By Booking Id And Status Id
     *
     * @param type $bookingId
     * @param type $statusId
     * @return type 
     */
    public function getLastDiscussionByBookingIdAndStatusId($bookingId, $statusId) {
        $bookingId = (int) $bookingId;
        $statusId = (int) $statusId;

        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order('created DESC');
        $select->where("booking_id = '{$bookingId}'");
        $select->where("status_id = '{$statusId}'");

        $discussions = $this->getAdapter()->fetchAll($select);
        $lastDiscussion = (isset($discussions[0]) && $discussions[0]) ? $discussions[0] : array();

        $discussion = array();
        if ($lastDiscussion) {
            $modelBookingDiscussion = new Model_BookingDiscussion();
            $discussion = $modelBookingDiscussion->getById($lastDiscussion['discussion_id']);
        }
        return $discussion;
    }

    public function getByBookingIdAndStatusIdAndDiscussionId($bookingId, $statusId, $discussionId) {
        $bookingId = (int) $bookingId;
        $statusId = (int) $statusId;
        $discussionId = (int) $discussionId;

        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_id = '{$bookingId}'");
        $select->where("status_id = '{$statusId}'");
        $select->where("discussion_id = '{$discussionId}'");

        return $this->getAdapter()->fetchRow($select);
    }

}