<?php

class Model_Code extends Zend_Db_Table_Abstract {

    protected $_name = 'code';
    
   /**
    * update table row according to the assigned id
    * 
    * @param int $id
    * @param array $data
    * @return boolean
    */ 
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "id = '{$id}'");
    }

    /**
     *delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("id = '{$id}'");
    }

    /**
     *get table row according to the assigned code
     * 
     * @param string $code
     * @return array
     */
    public function getByCode($code) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("code = '{$code}'");

        return $this->getAdapter()->fetchRow($select);
    }
  
}