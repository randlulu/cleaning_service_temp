<?php

class Model_AuthRole extends Zend_Db_Table_Abstract {

    protected $_name = 'auth_role';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order($order);

        if ($filters) {
            if (!empty($filters['keywords'])) {
                $keywords = $this->getAdapter()->quote('%' . $filters['keywords'] . '%');
                $select->where("role_name LIKE {$keywords}");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "role_id = '{$id}'");
    }

    /**
     * delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("role_id = '{$id}'");
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("role_id = {$id}");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the assigned Role Id
     * 
     * @param int $id
     * @return array
     */
    public function getRoleName($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name, array('role_name'));
        $select->where("role_id = {$id}");

        return $this->getAdapter()->fetchOne($select);
    }

    /**
     * get table row according to the assigned Role Name
     * 
     * @param string $name
     * @return array
     */
    public function getRoleIdByName($name) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name, array('role_id'));
        $select->where("role_name = '{$name}'");
        return $this->getAdapter()->fetchOne($select);
    }

    /**
     * get table rows as array
     * 
     * @return array
     */
    public function getRoleAsArray() {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order('role_name asc');

        if (!CheckAuth::checkCredential(array('canHandleAllCompanies'))) {
            $select->where("role_name != 'super_admin'");
        }
        $roles = $this->getAdapter()->fetchAll($select);

        $data = array();
        foreach ($roles as $role) {
            $data[] = array(
                'id' => $role['role_id'],
                'name' => $role['view_role_name']
            );
        }
        return $data;
    }

    /**
     * create Role Name with slug filters 
     * 
     * @param string $string
     * @return string 
     */
    public function createRoleName($string) {

        $slug = preg_replace('#[\:\;\"\'\|\<\,\>\.\?\`\~\!\@\#\$\%\^\&\*\(\)\_\-\+\=\{\[\}\]\/\t\n]#', ' ', $string);
        $slug = preg_replace('#[\s]+#', '_', trim($slug));
        $slug = strtolower($slug);

        $tmp_slug = $slug;
        $counter = 0;
        while ($this->getRoleIdByName($tmp_slug)) {
            $tmp_slug = $slug . '-' . rand(1, 1000000);
            $counter++;
            if ($counter >= 10) {
                return uniqid('role');
            }
        }
        return $tmp_slug;
    }

    public function checkBeforeDeleteAuthRole($roleId, &$tables = array()) {

        $sucsess = true;

        $select_user = $this->getAdapter()->select();
        $select_user->from('user', 'COUNT(*)');
        $select_user->where("role_id = {$roleId}");
        $count_user = $this->getAdapter()->fetchOne($select_user);

        if ($count_user) {
            $tables[] = 'user';
            $sucsess = false;
        }

        $select_auth_role_credential = $this->getAdapter()->select();
        $select_auth_role_credential->from('auth_role_credential', 'COUNT(*)');
        $select_auth_role_credential->where("role_id = {$roleId}");
        $count_auth_role_credential = $this->getAdapter()->fetchOne($select_auth_role_credential);

        if ($count_auth_role_credential) {
            $tables[] = 'auth_role_credential';
            $sucsess = false;
        }

        return $sucsess;
    }

}