<?php

class Model_AttributeType extends Zend_Db_Table_Abstract {

    protected $_name = 'attribute_type';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order($order);

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "attribute_type_id = '{$id}'");
    }

    /**
     * delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("attribute_type_id = '{$id}'");
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("attribute_type_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table rows as array
     * 
     * @return array
     */
    public function getAttributeTypeAsArray() {
        $attribute_types = $this->getAll(array(), 'attribute_type asc');

        $data = array();
        foreach ($attribute_types as $attribute_type) {
            $data[] = array(
                'id' => $attribute_type['attribute_type_id'],
                'name' => $attribute_type['attribute_type']
            );
        }
        return $data;
    }

}