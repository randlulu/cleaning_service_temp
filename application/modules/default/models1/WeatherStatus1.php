<?php

class Model_WeatherStatus extends Zend_Db_Table_Abstract {

    protected $_name = 'weather_status';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
	public function updateAll(){
		$this->updateWeatherStatusByCityId('14');
		$this->updateWeatherStatusByCityId('15');
		$this->updateWeatherStatusByCityId('16');
		$this->updateWeatherStatusByCityId('17');
		$this->updateWeatherStatusByCityId('18');
		$this->updateWeatherStatusByCityId('20');
		
	}
	
    public function updateWeatherStatusByCityId($id) {
						$id = (int) $id;
						$link=$this->getRSSLinkByCityId($id);
						$doc = new DOMDocument();                                                                                                                 
						$doc->load($link);
						$arrFeeds = array();
						$items=$doc->getElementsByTagName('item');
						if($id==14){
						$node=$items->item(2);
						}
						else
						$node=$items->item(1);
							$desc=$node->getElementsByTagName('description');
							$lines  = strip_tags($desc->item(0)->nodeValue, '<br/>');
							$arr = explode("\n", $lines);
							
							$doc = new DOMDocument();
							$doc->loadHTML($desc->item(0)->nodeValue);
							$imgList = $doc->getElementsByTagName('img');
							$imgValues = array();
							$i=2;
							foreach ($imgList as $img) {
								//echo $img->getAttribute('src');
								$imgValues[] = $img->getAttribute('src');
								$arr[$i]=$img->getAttribute('src');
								$i=$i+4;
							}
							unset($arr[0]);
							var_dump($arr);  
									
		
		$data = array(
                    'city_id' => $id ,
                    'date' => date("Y/m/d"),
                    'day' => $arr[1],
                    'img_link' => $arr[2],
					'status' => $arr[3],
					'temperature' => $arr[4],
                );	
		$statusId = parent::insert($data);
		
				
		$data = array(
                    'city_id' => $id ,
                    'date' => date("Y/m/d", strtotime("+1 day", time())),
                    'day' => $arr[5],
                    'img_link' => $arr[6],
					'status' => $arr[7],
					'temperature' => $arr[8],
                );	
		$statusId = parent::insert($data);
		
		$data = array(
                    'city_id' => $id ,
                    'date' => date("Y/m/d", strtotime("+2 day", time())),
                    'day' => $arr[9],
                    'img_link' => $arr[10],
					'status' => $arr[11],
					'temperature' => $arr[12],
                );	
		$statusId = parent::insert($data);
		
		$data = array(
                    'city_id' => $id ,
                    'date' => date("Y/m/d", strtotime("+3 day", time())),
                    'day' => $arr[13],
                    'img_link' => $arr[14],
					'status' => $arr[15],
					'temperature' => $arr[16],
                );	
		$statusId = parent::insert($data);
		
    }
	
	 public function getByDateAndCityId($date,$id) {
        $id = (int) $id;
	
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("date like '{$date}%' and city_id={$id}");
		
        return $this->getAdapter()->fetchRow($select);
    }
	
	
	public function getRSSLinkByCityId($id) {
        $id = (int) $id;
		switch($id){
			case 14:
				return "http://rss.weatherzone.com.au/?u=12994-1285&lt=aploc&lc=624&obs=1&fc=1&warn=1";
			case 15:
				return "http://rss.weatherzone.com.au/?u=12994-1285&lt=aploc&lc=9388&obs=1&fc=1&warn=1";
			case 16:
				return "http://rss.weatherzone.com.au/?u=12994-1285&lt=aploc&lc=5594&obs=1&fc=1&warn=1";
			case 17:
				return "http://rss.weatherzone.com.au/?u=12994-1285&lt=aploc&lc=12495&obs=1&fc=1&warn=1";
			case 18:
				return "http://rss.weatherzone.com.au/?u=12994-1285&lt=aploc&lc=13896&obs=1&fc=1&warn=1";
			case 20:
				return "http://rss.weatherzone.com.au/?u=12994-1285&lt=aploc&lc=3928&obs=1&fc=1&warn=1";
		}
		
       
    }
	
   
}