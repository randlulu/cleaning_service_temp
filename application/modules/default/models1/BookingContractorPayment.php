<?php

class Model_BookingContractorPayment extends Zend_Db_Table_Abstract {

    protected $_name = 'booking_contractor_payment';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from(array('bcp' => $this->_name));
        $select->order($order);

        $joinInner = array();

        if ($filters) {
            if (!empty($filters['booking_id'])) {
                $select->where("bcp.booking_id = {$filters['booking_id']}");
            }
            if (!empty($filters['contractor_id'])) {
                $select->where("bcp.contractor_id = {$filters['contractor_id']}");
            }
            if (!empty($filters['booking_start_between'])) {
                $bookingStartBetween = $filters['booking_start_between'];
                $bookingEndBetween = !empty($filters['booking_end_between']) ? $filters['booking_end_between'] : date('Y-m-d H:i:s', time());
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'bcp.booking_id = bok.booking_id', 'cols' => '');
                $select->where("bok.booking_start between '" . $bookingStartBetween . "' and '" . $bookingEndBetween . "'");
            }
        }

        if ($joinInner) {
            foreach ($joinInner as $inner) {
                $select->joinInner($inner['name'], $inner['cond'], $inner['cols']);
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table rows according to the assigned id and data
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "id = '{$id}'");
    }

    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("id = '{$id}'");
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getBybookingIdAndContractorId($bookingId, $contractorId) {
        $bookingId = (int) $bookingId;
        $contractorId = (int) $contractorId;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_id = '{$bookingId}'");
        $select->where("contractor_id = '{$contractorId}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getTotalPaymentByContractorId($contractorId, $filters=array()) {
        $contractorId = (int) $contractorId;

        $filters['contractor_id'] = $contractorId;

        $results = $this->getAll($filters);
       
        $totalPayment = 0;
        foreach ($results as $result) {
            $totalPayment = $totalPayment + $result['payment_to_contractor'];
        }

        return $totalPayment;
    }

}
