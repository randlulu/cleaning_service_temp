<?php

class Model_CustomerType extends Zend_Db_Table_Abstract {

    protected $_name = 'customer_type';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order($order);
        $filters['company_id'] = CheckAuth::getCompanySession();
        $where = array();

        if ($filters) {
            if (!empty($filters['keywords'])) {
                $keywords = $this->getAdapter()->quote('%' . $filters['keywords'] . '%');
                $where[] = ("customer_type LIKE {$keywords}");
            }
            if (!empty($filters['company_id'])) {
                $select->where("company_id = {$filters['company_id']} OR company_id = 0");
            }
            if (!empty($filters['is_work_order'])) {
                $select->where("is_work_order = {$filters['is_work_order']}");
            }
            if (!empty($filters['as_booking_address'])) {
                $select->where("as_booking_address = {$filters['as_booking_address']}");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table rows according to the assigned id and data
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "customer_type_id = '{$id}'");
    }

    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("customer_type_id = '{$id}'");
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("customer_type_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the type name
     * 
     * @param string $name
     * @return array 
     */
    public function getByTypeName($name) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("customer_type = '{$name}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get customer type as array
     * 
     * @return array 
     */
    public function getCustomerTypeAsArray() {
        $customertypes = $this->getAll(array(), 'customer_type asc');

        $data = array();
        foreach ($customertypes as $customerType) {
            $data[$customerType['customer_type_id']] = $customerType['customer_type'];
        }
        return $data;
    }

    /**
     * get customer type as array
     * 
     * @return array 
     */
    public function getCustomerTypeHaveBookingAddressAsArray() {

        $customertypes = $this->getAll(array('as_booking_address' => true), 'customer_type asc');

        $data = array();
        foreach ($customertypes as $customerType) {
            $data[] = $customerType['customer_type_id'];
        }
        return $data;
    }

    public function getByTypeNameAndCompany($customerType, $companyId = 0) {

        if (!$companyId) {
            $companyId = CheckAuth::getCompanySession();
        }

        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("customer_type = '{$customerType}'");
        $select->where("company_id = '{$companyId}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get customer type as array
     * 
     * @return array 
     */
    public function getCustomerTypeIsWorkOrder() {

        $filters = array('is_work_order' => 1);
        $customertypes = $this->getAll($filters, 'customer_type asc');

        $data = array();
        foreach ($customertypes as $customerType) {
            $data[$customerType['customer_type_id']] = $customerType['customer_type_id'];
        }
        return $data;
    }

    public function checkBeforeDeleteInquiryCustomerType($customerTypeId, &$tables = array()) {

        $sucsess = true;

        $select_customer = $this->getAdapter()->select();
        $select_customer->from('customer', 'COUNT(*)');
        $select_customer->where("customer_type_id = {$customerTypeId}");
        $count_customer = $this->getAdapter()->fetchOne($select_customer);

        if ($count_customer) {
            $tables[] = 'customer';
            $sucsess = false;
        }

        return $sucsess;
    }

}