<?php

class Model_AuthRoleCredential extends Zend_Db_Table_Abstract {

    protected $_name = 'auth_role_credential';

    /**
     *get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from(array('arc' => $this->_name));
        $select->joinInner(array('ar' => 'auth_role'), 'arc.role_id=ar.role_id', array('ar.role_name'));
        $select->joinInner(array('ac' => 'auth_credential'), 'arc.credential_id=ac.credential_id');
        $select->order($order);

        if ($filters) {
            if (!empty($filters['keywords'])) {
                $keywords = $this->getAdapter()->quote('%' . $filters['keywords'] . '%');
                $select->where("arc.credential_name LIKE {$keywords}");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

   /**
    * update table row according to the assigned id
    * 
    * @param int $id
    * @param array $data
    * @return boolean
    */ 
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "id = '{$id}'");
    }

    /**
     *delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("id = '{$id}'");
    }

    /**
     *get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     *get table row according to the assigned Role Id
     * 
     * @param int $role_id
     * @return array
     */
    public function getByRoleId($role_id) {
        $role_id = (int) $role_id;
        $select = $this->getAdapter()->select();
        $select->from(array('arc' => $this->_name));
        $select->joinInner(array('ac' => 'auth_credential'), 'arc.credential_id = ac.credential_id');
        $select->where("arc.role_id = '{$role_id}'");

        return $this->getAdapter()->fetchAll($select);
    }
    
    /**
     *get table row according to the assigned Role Id
     * 
     * @param int $role_id
     * @return array
     */
    public function getAllowCredentialByUserRoleId($role_id) {
        $role_id = (int) $role_id;
        $select = $this->getAdapter()->select();
        $select->from(array('arc' => $this->_name));
        $select->joinInner(array('ac' => 'auth_credential'), 'arc.credential_id = ac.credential_id');
        $select->where("arc.role_id = '{$role_id}'");
        $select->where("ac.is_hidden = 0");

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     *delete table row according to the assigned Role Id
     * 
     * @param int $role_id
     * @return boolean 
     */
    public function deleteByRoleId($role_id) {
        $role_id = (int) $role_id;
        return parent::delete("role_id = '{$role_id}'");
    }

     /**
     *get table row according to the assigned Role Id and Credential Id
     * 
     * @param int $role_id
     * @param int $credential_id
     * @return array
     */
    public function getByRoleIdAndCredentialId($role_id, $credential_id) {
        $role_id = (int) $role_id;
        $credential_id = (int) $credential_id;

        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("credential_id = '{$credential_id}'");
        $select->where("role_id = '{$role_id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     *check auth Credintial to the logged user
     * 
     * @param string $auth
     * @return boolean
     */
    public function checkAuth($auth) {
        $user_type = 'anonymous';
        $loggedUser = CheckAuth::getLoggedUser();
        if ($loggedUser) {
            $user_obj = new Model_User();
            $logged_user = $user_obj->getById($loggedUser['user_id']);
            $user_type = $logged_user['role_id'];
        }

        $authCredential_obj = new Model_AuthCredential();
        $authorities = $authCredential_obj->getByCredentialName($auth);
        if ($authorities) {
            if ($this->getByRoleIdAndCredentialId($user_type, $authorities['credential_id'])) {
                return true;
            }
        }
        return false;
    }

}