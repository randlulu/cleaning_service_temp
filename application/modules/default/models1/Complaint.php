<?php

class Model_Complaint extends Zend_Db_Table_Abstract {

    protected $_name = 'complaint';
    //fill 
    private $modelBooking;
    private $modelUser;
    private $modelContractorServiceBooking;
    private $modelCustomer;
    private $modelComplaintType;
    private $modelComplaintDiscussion;

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $joinInner = array();
        $select = $this->getAdapter()->select();
        $select->from(array('c' => $this->_name));
        $select->distinct();
        $select->order($order);

        $loggedUser = CheckAuth::getLoggedUser();
        $filters['company_id'] = CheckAuth::getCompanySession();

        $select->joinInner(array('csb' => 'contractor_service_booking'), 'c.booking_id = csb.booking_id', '');

        if (!CheckAuth::checkCredential(array('canSeeAllComplaint'))) {
            if (CheckAuth::checkCredential(array('canSeeOnlyHisComplaint'))) {
                if (CheckAuth::checkCredential(array('canSeeAssignedComplaint'))) {
                    $select->where("c.user_id = " . $loggedUser['user_id'] . " OR csb.contractor_id = {$loggedUser['user_id']}");
                } else {
                    $select->where("c.user_id = " . $loggedUser['user_id']);
                }
            }
        }

        if ($filters) {
            if (!empty($filters['keywords'])) {
                $keywords = $this->getAdapter()->quote('%' . trim($filters['keywords']) . '%');
                $select->where("c.full_text_search LIKE {$keywords}");

                //$keywords = '+' . preg_replace('#[\s]+#', ' +', trim($filters['keywords']));
                //$keywords = $this->getAdapter()->quote($keywords);
                //$select->where("MATCH(c.full_text_search) AGAINST({$keywords} IN BOOLEAN MODE)");
            }
            if (!empty($filters['complaintType'])) {
                $select->where("c.complaint_type_id = {$filters['complaintType']}");
            }

            if (!empty($filters['complaintStatus']) && $filters['complaintStatus'] != 'all') {
                $select->where("c.complaint_status = '{$filters['complaintStatus']}'");
            }

            if (!empty($filters['company_id'])) {
                $company_id = (int) $filters['company_id'];
                $select->where("c.company_id = {$company_id}");
            }
            if (!empty($filters['complaint_num'])) {
                $complaint_num = $this->getAdapter()->quote('%' . trim($filters['complaint_num']) . '%');
                $select->where("c.complaint_num LIKE {$complaint_num}");
            }
            if (!empty($filters['comment'])) {
                $comment = $this->getAdapter()->quote('%' . trim($filters['comment']) . '%');
                $select->where("c.comment LIKE {$comment}");
            }
            if (!empty($filters['booking_num'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.booking_id = bok.booking_id', 'cols' => '');
                $booking_num = $this->getAdapter()->quote('%' . trim($filters['booking_num']) . '%');
                $select->where("bok.booking_num LIKE {$booking_num}");
            }
            if (!empty($filters['customer_id'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.booking_id = bok.booking_id', 'cols' => '');
                $filters['customer_id'] = (int) $filters['customer_id'];
                $select->where("bok.customer_id = {$filters['customer_id']} ");
            }
            if (!empty($filters['contractor_id'])) {
                $contractor_id = (int) $filters['contractor_id'];
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.booking_id = bok.booking_id', 'cols' => '');
                $select->where("bok.created_by = {$contractor_id} OR csb.contractor_id = {$contractor_id}");
            }
            if (!empty($filters['user_id'])) {
                $user_id = (int) $filters['user_id'];
                $select->where("c.user_id = {$user_id}");
            }
            if (!empty($filters['bussiness_name'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.booking_id = bok.booking_id', 'cols' => '');
                $joinInner['customer_commercial_info'] = array('name' => array('cci' => 'customer_commercial_info'), 'cond' => 'bok.customer_id = cci.customer_id', 'cols' => '');
                $select->where("cci.business_name LIKE '%{$filters['bussiness_name']}%'");
            }
            if (!empty($filters['inquiry_num'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.booking_id = bok.booking_id', 'cols' => '');
                $joinInner['inquiry'] = array('name' => array('i' => 'inquiry'), 'cond' => 'bok.original_inquiry_id = i.inquiry_id', 'cols' => '');
                $inquiry_num = $this->getAdapter()->quote('%' . trim($filters['inquiry_num']) . '%');
                $select->where("i.inquiry_num LIKE {$inquiry_num}");
            }
            if (!empty($filters['estimate_num'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.booking_id = bok.booking_id', 'cols' => '');
                $joinInner['booking_estimate'] = array('name' => array('est' => 'booking_estimate'), 'cond' => 'bok.booking_id = est.booking_id', 'cols' => '');
                $estimate_num = $this->getAdapter()->quote('%' . trim($filters['estimate_num']) . '%');
                $select->where("est.estimate_num LIKE {$estimate_num}");
            }
            if (!empty($filters['mobile/phone'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.booking_id = bok.booking_id', 'cols' => '');
                $joinInner['customer'] = array('name' => array('cu' => 'customer'), 'cond' => 'bok.customer_id = cu.customer_id', 'cols' => '');
                $mobile_phone = trim($filters['mobile/phone']);
                $select->where("CONCAT_WS(' ',phone1 ,phone2 ,phone3,mobile1 ,mobile2 ,mobile3) LIKE '%{$mobile_phone}%'");
            }
            if (!empty($filters['email'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.booking_id = bok.booking_id', 'cols' => '');
                $joinInner['customer'] = array('name' => array('cu' => 'customer'), 'cond' => 'bok.customer_id = cu.customer_id', 'cols' => '');
                $email = trim($filters['email']);
                $select->where("CONCAT_WS(' ',email1 ,email2 ,email3) LIKE '%{$email}%'");
            }
            if (!empty($filters['service_id'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'c.booking_id = bok.booking_id', 'cols' => '');
                $service_id = (int) $filters['service_id'];
                $select->where("csb.service_id = {$service_id}");
            }
        }

        if ($joinInner) {
            foreach ($joinInner as $inner) {
                $select->joinInner($inner['name'], $inner['cond'], $inner['cols']);
            }
        }
        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

    public function insert(array $data) {

        $company_id = CheckAuth::getCompanySession();

        $select = $this->getAdapter()->select();
        $select->from($this->_name, 'MAX(count)');
        $select->where("company_id = {$company_id}");
        $result = $this->getAdapter()->fetchOne($select);

        $data['count'] = $result + 1;
        $data['complaint_num'] = 'CMP-' . ($result + 1);

        $id = parent::insert($data);

        /**
         * update full text search table
         */
        $modelUpdateFullTextSearch = new Model_UpdateFullTextSearch();
        $modelUpdateFullTextSearch->insertFullTextSearch($this->_name, $id);

        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'added');

        return $id;
    }

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateById($id, $data) {
        $id = (int) $id;

        /**
         * update full text search table
         */
        $done = false;
        if (!empty($data['full_text_search'])) {
            $done = true;
        }
        $modelUpdateFullTextSearch = new Model_UpdateFullTextSearch();
        $modelUpdateFullTextSearch->updateFullTextSearchFlag($this->_name, $id, $done);

        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'changed');

        return parent::update($data, "complaint_id = '{$id}'");
    }

    /**
     * delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;

        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'deleted');

        return parent::delete("complaint_id = '{$id}'");
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("complaint_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the assigned BookingId
     * 
     * @param int $id
     * @return array
     */
    public function getByBookingId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_id = '{$id}'");

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * get all the recent complaints that the user are able to see
     * 
     * @return array  
     */
    public function getRecentComplaint($filters = array()) {

        $loggedUser = CheckAuth::getLoggedUser();
        $userId = $loggedUser['user_id'];
        $company_id = CheckAuth::getCompanySession();

        $select = $this->getAdapter()->select();

        $select->distinct();

        $select->from(array('bc' => $this->_name));

        $select->joinInner(array('csb' => 'contractor_service_booking'), 'bc.booking_id = csb.booking_id', 'csb.contractor_id');

        if (!CheckAuth::checkCredential(array('canSeeAllComplaint'))) {
            if (CheckAuth::checkCredential(array('canSeeOnlyHisComplaint'))) {
                if (CheckAuth::checkCredential(array('canSeeAssignedComplaint'))) {
                    $select->where("bc.user_id = {$userId} OR csb.contractor_id = {$userId}");
                } else {
                    $select->where("bc.user_id = {$userId}");
                }
            }
        }

        if ($company_id) {
            $select->where("bc.company_id = {$company_id}");
        }
        if ($filters) {
            $select->joinInner(array('bok' => 'booking'), 'bc.booking_id = bok.booking_id', '');
            if (!empty($filters['created_by'])) {
                $userId = (int) $filters['created_by'];
                $select->where("bok.created_by = '{$userId}'");
            }
        }

        $select->order("created DESC");

        $select->limit(20);

        return $this->getAdapter()->fetchall($select);
    }

    public function getComplaintCount($filters = array()) {

        $select = $this->getAdapter()->select();
        $select->from(array('com' => $this->_name));
        $select->distinct();

        $joinInner = array();

        if (!isset($filters['company_id'])) {
            $filters['company_id'] = CheckAuth::getCompanySession();
        }

        if ($filters) {
            if (!empty($filters['created_by'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'com.booking_id = bok.booking_id', 'cols' => '');

                $userId = (int) $filters['created_by'];
                $select->where("bok.created_by = '{$userId}'");
            }

            if (!empty($filters['company_id'])) {
                $company_id = (int) $filters['company_id'];
                $select->where("com.company_id = {$company_id}");
            }

            if (!empty($filters['startTimeRange']) && !empty($filters['endTimeRange'])) {
                $startTime = $filters['startTimeRange'];
                $endTime = $filters['endTimeRange'];
                $select->where("com.created between '" . mySql2PhpTime($startTime) . "' and '" . mySql2PhpTime($endTime) . "'");
            }
        }

        if ($joinInner) {
            foreach ($joinInner as $inner) {
                $select->joinInner($inner['name'], $inner['cond'], $inner['cols']);
            }
        }

        $sql = $this->getAdapter()->select();
        $sql->from($select, 'COUNT(*)');
        return $this->getAdapter()->fetchOne($sql);
    }

    /**
     * get extra info for all the rows
     * @param array $row
     * @param array $types
     * @return array
     */
    public function fills(&$rows, $types = array()) {
        foreach ($rows as &$row) {
            $this->fill($row, $types);
        }
    }

    /**
     * get extra info for each row (Ex: getting the category row by c_id, author row by author id ... etc)
     * 
     * @param array $row
     * @param array $types
     * @return array
     */
    public function fill(&$row, $types = array()) {
        if (in_array('booking', $types)) {
            /**
             * load model
             */
            if (!$this->modelBooking) {
                $this->modelBooking = new Model_Booking();
            }

            $booking = $this->modelBooking->getById($row['booking_id']);
            $row['booking'] = $booking;
        }

        if (in_array('complaint_type', $types)) {
            /**
             * load model
             */
            if (!$this->modelComplaintType) {
                $this->modelComplaintType = new Model_ComplaintType();
            }

            $booking = $this->modelComplaintType->getById($row['complaint_type_id']);
            $row['complaint_type'] = $booking;
        }

        if (in_array('complaint_discussions', $types)) {
            /**
             * load model
             */
            if (!$this->modelComplaintDiscussion) {
                $this->modelComplaintDiscussion = new Model_ComplaintDiscussion();
            }
            if (!$this->modelUser) {
                $this->modelUser = new Model_User();
            }

            $complaintDiscussions = $this->modelComplaintDiscussion->getByComplaintId($row['complaint_id'], 'created DESC');
            foreach ($complaintDiscussions as &$complaintDiscussion) {
                $complaintDiscussion['user'] = $this->modelUser->getById($complaintDiscussion['user_id']);
            }

            $row['complaint_discussions'] = $complaintDiscussions;
        }

        if (in_array('email_contractors', $types)) {
            /**
             * load model
             */
            if (!$this->modelUser) {
                $this->modelUser = new Model_User();
            }
            if (!$this->modelContractorServiceBooking) {
                $this->modelContractorServiceBooking = new Model_ContractorServiceBooking();
            }

            $allContractorServiceBooking = $this->modelContractorServiceBooking->getByBookingId($row['booking_id']);

            $contractors = array();
            if ($allContractorServiceBooking) {
                foreach ($allContractorServiceBooking as $bookingService) {
                    $user = $this->modelUser->getById($bookingService['contractor_id']);
                    $contractors[$bookingService['contractor_id']] = $user['email1'];
                }
            }
            $row['email_contractors'] = $contractors;
        }

        if (in_array('customer', $types)) {
            /**
             * load model
             */
            if (!$this->modelCustomer) {
                $this->modelCustomer = new Model_Customer();
            }
            if (!$this->modelBooking) {
                $this->modelBooking = new Model_Booking();
            }

            $booking = $this->modelBooking->getById($row['booking_id']);

            $row['customer'] = $this->modelCustomer->getById($booking['customer_id']);
            $row['customer_name'] = get_customer_name($row['customer']);
        }
    }

    public function getFullTextComplaintByBookingId($bookingId) {

        $complaints = $this->getByBookingId($bookingId);

        $complaint_array = array();
        foreach ($complaints as $complaint) {
            if ($complaint['complaint_num']) {
                $complaint_array[] = trim($complaint['complaint_num']);
            }
            if ($complaint['comment']) {
                $complaint_array[] = trim($complaint['comment']);
            }
        }

        if ($complaint_array) {
            $complaint_array = implode(' ', $complaint_array);
        }

        return $complaint_array;
    }

    public function updateFullTextSearch($complaintId) {

        $complaint = $this->getById($complaintId);

        $fullTextSearch = array();

        // complaint     
        $fullTextSearch[] = trim($complaint['complaint_num']);
        $fullTextSearch[] = trim($complaint['comment']);

        // booking
        $model_Booking = new Model_Booking();
        $modelBookingStatus = new Model_BookingStatus();
        $bookingId = $complaint['booking_id'];
        $booking = $model_Booking->getById($bookingId);
        $fullTextSearch[] = trim($booking['booking_num']);
        $fullTextSearch[] = trim($booking['title']);
        $fullTextSearch[] = trim($booking['description']);
        $status = $modelBookingStatus->getById($booking['status_id']);
        $fullTextSearch[] = trim($status['name']);


        // address
        $modelBookingAddress = new Model_BookingAddress();
        $bookingAddress = $modelBookingAddress->getByBookingId($bookingId);
        $fullTextSearch[] = trim(get_line_address($bookingAddress));

        //customer
        $modelCustomer = new Model_Customer();
        $fullTextCustomer = $modelCustomer->getFullTextCustomerContacts($booking['customer_id']);
        $fullTextSearch[] = trim($fullTextCustomer);

        // services
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $fullTextContractorServiceBooking = $modelContractorServiceBooking->getFullTextContractorServiceBooking($bookingId);
        if ($fullTextContractorServiceBooking) {
            $fullTextSearch[] = trim($fullTextContractorServiceBooking);
        }

        //inquiry
        if (isset($booking['original_inquiry_id']) && $booking['original_inquiry_id']) {
            $modelInquiry = new Model_Inquiry();
            $Inquiry = $modelInquiry->getById($booking['original_inquiry_id']);
            if ($Inquiry['inquiry_num']) {
                $fullTextSearch[] = trim($Inquiry['inquiry_num']);
            }
        }

        //estimate
        $modelBookingEstimate = new Model_BookingEstimate();
        $estimate = $modelBookingEstimate->getByBookingId($bookingId);
        if (isset($estimate['estimate_num']) && $estimate['estimate_num']) {
            $fullTextSearch[] = trim($estimate['estimate_num']);
        }

        //invoice
        $modelBookingInvoice = new Model_BookingInvoice();
        $invoice = $modelBookingInvoice->getByBookingId($bookingId);
        if (isset($invoice['invoice_num']) && $invoice['invoice_num']) {
            $fullTextSearch[] = trim($invoice['invoice_num']);
        }
        if (isset($invoice['invoice_type']) && $invoice['invoice_type']) {
            $fullTextSearch[] = trim($invoice['invoice_type']);
        }


        $data = array();
        $data['full_text_search'] = implode(' ', $fullTextSearch);

        $this->updateById($complaintId, $data);
    }

    public function deleteRelatedComplaint($id) {

        //delete data from complaint_discussion
        $this->getAdapter()->delete('complaint_discussion', "complaint_id = '{$id}'");
    }

    public function sendComplaintAsEmailToContractor($complaintId) {

        $complaint = $this->getById($complaintId);

        $this->fill($complaint, array('booking', 'email_contractors', 'customer', 'complaint_type'));

        $modelUser = new Model_User();
        $user = $modelUser->getById($complaint['booking']['created_by']);

        $template_params = array(
            //booking
            '{booking_num}' => $complaint['booking']['booking_num'],
            '{booking_created}' => date('d/m/Y', $complaint['booking']['created']),
            '{booking_created_by}' => ucwords($user['username']),
            '{booking_start}' => date("F j, Y, g:i a", strtotime($complaint['booking']['booking_start'])),
            //complaint
            '{complaint}' => $complaint['comment'],
            '{complaint_num}' => $complaint['complaint_num'],
            '{complaint_type}' => $complaint['complaint_type']['name'],
            '{complaint_view}' => $this->getComplaintViewAsPlaceHolder($complaintId),
            '{complaint_discussion_view}' => $this->getComplaintViewDiscussionAsPlaceHolder($complaintId),
            //customer
            '{customer_name}' => get_customer_name($complaint['customer']),
            '{customer_first_name}' => isset($complaint['customer']['first_name']) && $complaint['customer']['first_name'] ? ucwords($complaint['customer']['first_name']) : '',
            '{customer_last_name}' => isset($complaint['customer']['last_name']) && $complaint['customer']['last_name'] ? ' ' . ucwords($complaint['customer']['last_name']) : ''
        );

        foreach ($complaint['email_contractors'] as $contractorEmail) {
            $email_log = array('reference_id' => $complaint['complaint_id'], 'type' => 'complaint');
            EmailNotification::sendEmail(array('to' => $contractorEmail), 'send_complaint_as_email_to_contractor ', $template_params, $email_log, $complaint['booking']['company_id']);
        }
    }

    public function getComplaintViewAsPlaceHolder($complaintId) {
        $complaint = $this->getById($complaintId);
        $this->fill($complaint, array('complaint_type'));
        $view = new Zend_View();
        $view->setScriptPath(APPLICATION_PATH . '/modules/complaint/views/scripts/index');
        $view->complaint = $complaint;
        $bodyComplaint = $view->render('complaint.phtml');
        return $bodyComplaint;
    }

    public function getComplaintViewDiscussionAsPlaceHolder($complaintId) {
        $bodyComplaintDiscussion = "";
        $complaint = $this->getById($complaintId);
        $this->fill($complaint, array('complaint_discussions'));
        if (!empty($complaint['complaint_discussions'])) {
            $view = new Zend_View();
            $view->setScriptPath(APPLICATION_PATH . '/modules/complaint/views/scripts/index');
            $view->complaint = $complaint;
            $bodyComplaintDiscussion = $view->render('complaint_discussion.phtml');
        }

        return $bodyComplaintDiscussion;
    }

}