<?php

class Model_PaymentToContractors extends Zend_Db_Table_Abstract {

    protected $_name = 'payment_to_contractors';
    private $modelCustomer;
    private $modelPaymentType;
    private $modelBookingInvoice;

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from(array('pay' => $this->_name));
        $joinInner = array();
		$select->joinLeft(array('payToContAttach' => 'payment_to_contractor_attachment'), 'pay.payment_to_contractor_id = payToContAttach.payment_to_contractor_id');
		$select->joinInner(array('user' => 'user'), 'pay.contractor_id = user.user_id');

        $loggedUser = CheckAuth::getLoggedUser();

        if ($filters) {
            if (!empty($filters['keywords'])) {
                $keywords = $this->getAdapter()->quote('%' . $filters['keywords'] . '%');
                $select->where("pay.reference LIKE {$keywords}");
            }
			
			if (!empty($filters['contractor_id'])) {
                //$keywords = $this->getAdapter()->quote('%' . $filters['keywords'] . '%');
				$contractor_id= (int)$filters['contractor_id'];
                $select->where("pay.contractor_id = {$contractor_id}");
            }

            if (!empty($filters['booking_id'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'bok.booking_id = pay.booking_id', 'cols' => '');

                $select->where("pay.booking_id = {$filters['booking_id']}");
                //$select->where('bok.is_deleted = 0');
            }

            if (!empty($filters['customer_id'])) {
                $joinInner['customer'] = array('name' => array('cus' => 'customer'), 'cond' => 'cus.customer_id = pay.customer_id', 'cols' => '');

                $select->where("pay.customer_id = {$filters['customer_id']}");
                $select->where('cus.is_deleted = 0');
            }

            if (!empty($filters['payment_type_id'])) {
                $select->where("pay.payment_type_id = {$filters['payment_type_id']}");
            }

            if (!empty($filters['is_approved'])) {
                if ('yes' == $filters['is_approved']) {
                    $select->where('pay.is_approved = 1');
                } elseif ('no' == $filters['is_approved']) {
                    $select->where('pay.is_approved = 0');
                }
            }
           ////// updated by islam 
		   if (!empty($filters['payment_created_between'])) {
                $paymentCreatedBetween = $filters['payment_created_between'];
                $paymentEndBetween = !empty($filters['payment_end_between']) ? $filters['payment_end_between'] : date('Y-m-d H:i:s', time());
                $select->where("pay.date_of_payment between '" . $paymentCreatedBetween . "' and '" . $paymentEndBetween . "'");
            }
			//////
            if (!empty($filters['company_id'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'bok.booking_id = pay.booking_id', 'cols' => '');

                $company_id = (int) $filters['company_id'];
                $select->where("bok.company_id = {$company_id}");
            }
            if (!empty($filters['order_by']) && $filters['order_by'] == 'payment_type') {
                $joinInner['payment_type'] = array('name' => array('pyt' => 'payment_type'), 'cond' => 'pyt.id = pay.payment_type_id', 'cols' => '');
            }
            if (!empty($filters['is_mark'])) {
                $select->where('pay.is_mark = 1');
            }
        }

        if ($joinInner) {
            foreach ($joinInner as $inner) {
                $select->joinInner($inner['name'], $inner['cond'], $inner['cols']);
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }
        //echo $select;die;
        return $this->getAdapter()->fetchAll($select);
    }

	
	public function insert(array $data) {

        $id = parent::insert($data);

        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'added');

        return $id;
    }
	
	public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "payment_to_contractor_id = '{$id}'");
    }

 
}

?>