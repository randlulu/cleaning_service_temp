<?php

class Model_BookingAddressTemp extends Zend_Db_Table_Abstract {

    protected $_name = 'booking_address_temp';
    
    
   /**
    * update table row according to the assigned id
    * 
    * @param int $id
    * @param array $data
    * @return boolean
    */ 
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "booking_address_id = '{$id}'");
    }
    
    /**
     *get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_address_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     *get table row according to the assigned BookingId
     * 
     * @param int $id
     * @return array
     */
    public function getByBookingId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_id = '{$id}'");
        
        return $this->getAdapter()->fetchRow($select);
    }
     /**
    * update table row according to the assigned  bookingId
    * 
    * @param int $id
    * @param array $data
    * @return boolean
    */ 
    public function updateByBookingId($bookingId, $data) {
        $bookingId = (int) $bookingId;
        return parent::update($data, "booking_id = '{$bookingId}'");
    }
    
      /**
     * delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("booking_address_id = '{$id}'");
    }

}