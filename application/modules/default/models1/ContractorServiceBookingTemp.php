<?php

class Model_ContractorServiceBookingTemp extends Zend_Db_Table_Abstract {

    protected $_name = 'contractor_service_booking_temp';
    // model for function

    private $modelAttributeListValue;
    private $modelServiceAttribute;
    private $modelServiceAttributeValueTemp;
    private $modelServices;
    private $modelAttributes;

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order($order);

        if ($filters) {
            if (!empty($filters['booking_id'])) {
                $select->where("booking_id = {$filters['booking_id']}");
            }
            if (!empty($filters['contractor_id'])) {
                $select->where("contractor_id = {$filters['contractor_id']}");
            }
            if (!empty($filters['service_id'])) {
                $select->where("service_id = {$filters['service_id']}");
            }

            if (isset($filters['is_accepted'])) {
                if ($filters['is_accepted'] == "all") {
                    $select->where("is_accepted = 0 OR is_accepted = 1");
                } elseif ($filters['is_accepted'] == "accepted") {
                    $select->where("is_accepted = 1");
                } elseif ($filters['is_accepted'] == "not_accepted") {
                    $select->where("is_accepted = 0");
                }
            }
            if (isset($filters['is_rejected'])) {
                if ($filters['is_rejected'] == "all") {
                    $select->where("is_rejected = 0 OR is_rejected = 1");
                } elseif ($filters['is_rejected'] == "rejected") {
                    $select->where("is_rejected = 1");
                } elseif ($filters['is_rejected'] == "not_rejected") {
                    $select->where("is_rejected = 0");
                }
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateById($id, $data) {
        $id = (int) $id;

        if (!empty($data['contractor_id']) || !empty($data['service_id'])) {

            $oldData = $this->getById($id);

            if (!empty($data['contractor_id']) && $oldData['contractor_id'] != $data['contractor_id']) {
                $data['is_accepted'] = 0;
                $data['is_rejected'] = 0;
            }

            if (!empty($data['service_id']) && $oldData['service_id'] != $data['service_id']) {
                $data['is_accepted'] = 0;
                $data['is_rejected'] = 0;
            }
        }

        return parent::update($data, "id= '{$id}'");
    }

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateByServiceId($id, $data) {
        $id = (int) $id;

        if (!empty($data['contractor_id']) || !empty($data['service_id'])) {

            $oldData = $this->getById($id);

            if (!empty($data['contractor_id']) && $oldData['contractor_id'] != $data['contractor_id']) {
                $data['is_accepted'] = 0;
                $data['is_rejected'] = 0;
            }

            if (!empty($data['service_id']) && $oldData['service_id'] != $data['service_id']) {
                $data['is_accepted'] = 0;
                $data['is_rejected'] = 0;
            }
        }

        return parent::update($data, "service_id= '{$id}'");
    }

    /**
     * delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("id= '{$id}'");
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id= '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * delete table row according to the assigned bookingId
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteBybookingId($id) {
        $id = (int) $id;
        return parent::delete("booking_id= '{$id}'");
    }

    /**
     * get table row according to the assigned BookingId
     * 
     * @param int $bookingId
     * @return array
     */
    public function getByBookingId($bookingId) {
        $bookingId = (int) $bookingId;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_id= '{$bookingId}'");

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * get table row according to the assigned contractor Id
     * 
     * @param int $contractorId
     * @return array
     */
    public function getByContractorId($contractorId) {
        $contractorId = (int) $contractorId;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("contractor_id= '{$contractorId}'");

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * get By Booking And Contractor Not Rejected Bookings
     * 
     * @param int $bookingId
     * @param int $contractorId
     * @return array 
     */
    public function getNotRejectedOrAcceptedBookings($bookingId, $contractorId) {
        $bookingId = (int) $bookingId;
        $contractorId = (int) $contractorId;

        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("contractor_id= '{$contractorId}' AND booking_id='{$bookingId}' AND is_accepted = 0 AND is_rejected = 0");

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * get table row according to the assigned BookingId Service and Clone
     * 
     * @param int $bookingId
     * @param int $service_id
     * @param int $clone
     * @return array
     */
    public function getByBookingAndServiceAndClone($bookingId, $service_id, $clone) {
        $bookingId = (int) $bookingId;
        $service_id = (int) $service_id;
        $clone = (int) $clone;

        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("service_id='{$service_id}' AND booking_id='{$bookingId}' AND clone='{$clone}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * delete table row according to the assigned BookingId Service and Clone
     * 
     * @param int $bookingId
     * @param int $service_id
     * @param int $clone
     * @return boolean 
     */
    public function deleteByBookingAndServiceAndClone($bookingId, $service_id, $clone) {
        $bookingId = (int) $bookingId;
        $service_id = (int) $service_id;
        $clone = (int) $clone;

        return $this->delete("service_id='{$service_id}' AND booking_id='{$bookingId}' AND clone='{$clone}'");
    }

    /**
     * delete table row according to the assigned BookingId Services 
     * 
     * @param int $bookingId
     * @param array $services
     * @return boolean 
     */
    public function deleteByBookingIdAndServices($bookingId, $services = array()) {
        $bookingId = (int) $bookingId;

        $modelServiceAttributeValueTemp = new Model_ServiceAttributeValueTemp();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelBooking = new Model_Booking();
        $oldServices = $this->getByBookingId($bookingId);

        if (!empty($oldServices)) {
            foreach ($oldServices as &$oldService) {
                $serviceId = $oldService['service_id'];
                $clone = $oldService['clone'];

                $oldService = $serviceId . '_' . $clone;
            }
        }

        if (!empty($services)) {
            foreach ($services as &$service) {
                $serviceId = $service['service_id'];
                $clone = $service['clone'];

                $service = $serviceId . '_' . $clone;
            }
        }

        //
        //delete
        //
        $toDeleteServices = array_diff($oldServices, $services);
        if ($toDeleteServices) {
            foreach ($toDeleteServices as $toDeleteService) {

                $serviceAndClone = explode('_', $toDeleteService);
                $serviceId = (int) (isset($serviceAndClone[0]) ? $serviceAndClone[0] : 0);
                $clone = (int) (isset($serviceAndClone[1]) && $serviceAndClone[1] ? $serviceAndClone[1] : 0);

                $db_params = array();
                $db_params['is_change'] = 1;

                $bookingServicelink = $modelContractorServiceBooking->getByBookingAndServiceAndClone($bookingId, $serviceId, $clone);
                if ($bookingServicelink) {
                    $modelContractorServiceBooking->updateById($bookingServicelink['id'], $db_params);
                }
                $bookingServiceTemplink = $this->getByBookingAndServiceAndClone($bookingId, $serviceId, $clone);
                $this->updateById($bookingServiceTemplink['id'], $db_params);

                $modelBooking->updateById($bookingId, $db_params);


                //delete By Service And Booking And Clone
                $this->deleteByBookingAndServiceAndClone($bookingId, $serviceId, $clone);
                // Delete Attribute Value
                $modelServiceAttributeValueTemp->deleteByBookingIdServiceIdAndClone($bookingId, $serviceId, $clone);
            }
        }
    }

    /*
     * setServicesToBooking
     */

    public function setServicesToBooking($bookingId, $services = array()) {

        //Get Zend Request
        $request = Zend_Controller_Front::getInstance()->getRequest();

        //
        // delete Services not in the list
        //
        $bookingId = (int) $bookingId;
        $allServices = array();
        if (!empty($services)) {
            foreach ($services AS $service) {
                $serviceAndClone = explode('_', $service);

                $serviceId = (int) (isset($serviceAndClone[0]) ? $serviceAndClone[0] : 0);
                $clone = (int) (isset($serviceAndClone[1]) && $serviceAndClone[1] ? $serviceAndClone[1] : 0);
                $contractorId = (int) $request->getParam('contractor_' . $serviceId . ($clone ? '_' . $clone : ''));
                $considerMinPrice = (int) $request->getParam('consider_min_price_' . $serviceId . ($clone ? '_' . $clone : ''));

                $allServices[] = array(
                    'service_id' => $serviceId,
                    'clone' => $clone,
                    'consider_min_price' => $considerMinPrice,
                    'contractor_id' => $contractorId
                );
            }
        }
        $this->deleteByBookingIdAndServices($bookingId, $allServices);

        //
        // assign the Services to Booking
        //
        foreach ($allServices as $allService) {
            $this->assignServicesToBooking(array('booking_id' => $bookingId, 'contractor_id' => $allService['contractor_id'], 'service_id' => $allService['service_id'], 'clone' => $allService['clone'], 'consider_min_price' => $allService['consider_min_price']));
        }

        //
        //get all service temp for this booking 
        //
        $bookingServicesTemp = $this->getByBookingId($bookingId);
        $contractorServiceBookingTemp = array();
        $tempKeys = array();
        foreach ($bookingServicesTemp as $bookingServiceTemp) {
            $tempKeys[] = $bookingServiceTemp['booking_id'] . '_' . $bookingServiceTemp['contractor_id'] . '_' . $bookingServiceTemp['service_id'] . '_' . $bookingServiceTemp['clone'];
            $contractorServiceBookingTemp[] = array(
                'booking_id' => $bookingServiceTemp['booking_id'],
                'contractor_id' => $bookingServiceTemp['contractor_id'],
                'service_id' => $bookingServiceTemp['service_id'],
                'clone' => $bookingServiceTemp['clone'],
                'consider_min_price' => $bookingServiceTemp['consider_min_price']
            );
        }

        //
        //get all service for this booking 
        //
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $bookingServices = $modelContractorServiceBooking->getByBookingId($bookingId);
        $contractorServiceBooking = array();
        $keys = array();
        foreach ($bookingServices as $bookingService) {
            $keys[] = $bookingService['booking_id'] . '_' . $bookingService['contractor_id'] . '_' . $bookingService['service_id'] . '_' . $bookingService['clone'];
            $contractorServiceBooking[] = array(
                'booking_id' => $bookingService['booking_id'],
                'contractor_id' => $bookingService['contractor_id'],
                'service_id' => $bookingService['service_id'],
                'clone' => $bookingService['clone'],
                'consider_min_price' => $bookingService['consider_min_price']
            );
        }

        $differences = array_diff($keys, $tempKeys);

        if ($differences) {
            foreach ($differences as $difference) {
                $serviceAndCloneKey = explode('_', $difference);
                $bookingId = (int) (isset($serviceAndCloneKey[0]) ? $serviceAndCloneKey[0] : 0);
                $contractorId = (int) (isset($serviceAndCloneKey[1]) ? $serviceAndCloneKey[1] : 0);
                $serviceId = (int) (isset($serviceAndCloneKey[2]) ? $serviceAndCloneKey[2] : 0);
                $clone = (int) (isset($serviceAndCloneKey[3]) && $serviceAndCloneKey[3] ? $serviceAndCloneKey[3] : 0);

                $db_params = array();
                $db_params['is_change'] = 1;

                $bookingServicelink = $modelContractorServiceBooking->getByBookingAndServiceAndClone($bookingId, $serviceId, $clone);
                $modelContractorServiceBooking->updateById($bookingServicelink['id'], $db_params);

                $bookingServiceTemplink = $this->getByBookingAndServiceAndClone($bookingId, $serviceId, $clone);
                $this->updateById($bookingServiceTemplink['id'], $db_params);
            }
        }

        // check if the consider min price is change between same services


        $params = array();
        $params['is_change'] = 1;
        foreach ($bookingServices as $bookingService) {
            $booking_service_temp = $this->getByBookingAndServiceAndClone($bookingService['booking_id'], $bookingService['service_id'], $bookingService['clone']);
            if ($booking_service_temp && $bookingService['consider_min_price'] != $booking_service_temp['consider_min_price']) {
                $this->updateById($booking_service_temp['id'], $params);
                $modelContractorServiceBooking->updateById($bookingService['id'], $params);
            }
        }





        if ($contractorServiceBooking != $contractorServiceBookingTemp) {
            $db_params = array();
            $db_params['is_change'] = 1;
            $modelBooking = new Model_Booking();
            $modelBooking->updateById($bookingId, $db_params);
        }
    }

    /*
     * assignServicesToBooking
     */

    public function assignServicesToBooking($params) {

        $bookingServicelink = $this->getByBookingAndServiceAndClone($params['booking_id'], $params['service_id'], $params['clone']);

        $loggedUser = CheckAuth::getLoggedUser();
        if ($loggedUser['user_id'] == $params['contractor_id']) {
            $params['is_accepted'] = 1;
        }

        if (!$bookingServicelink) {
            $returnId = $this->insert($params);
        } else {
            $this->updateById($bookingServicelink['id'], $params);
            $returnId = $bookingServicelink['id'];
        }

        $modelServiceAttributeValueTemp = new Model_ServiceAttributeValueTemp();
        $modelServiceAttributeValueTemp->addAttributeByBookingIdAndServiceIdAndClone($params['booking_id'], $params['service_id'], $params['clone']);
        return $returnId;
    }

    /**
     * get the accepted services in the booking of the logged user
     *
     * @param int $bookingId
     * @return array 
     */
    public function getContractorServicesByBookingId($bookingId) {
        $bookingId = (int) $bookingId;

        $loggedUser = CheckAuth::getLoggedUser();
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_id= '{$bookingId}'");

        if (!CheckAuth::checkCredential(array('canSeeAllServices'))) {
            if (CheckAuth::checkCredential(array('canSeeOnlyHisServices'))) {
                $select->where("contractor_id = '{$loggedUser['user_id']}'");
            }
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * get count of tables rows to the assigned filters
     * 
     * @param array $filters
     * @return array
     */
    public function getServicesCount($filters = array()) {


        $loggedUser = CheckAuth::getLoggedUser();

        $select = $this->getAdapter()->select();
        $select->from(array('csb' => $this->_name));

        $select->joinInner(array('b' => 'booking'), 'b.booking_id = csb.booking_id', '');

        $select->where('b.is_deleted = 0');

        if (!CheckAuth::checkCredential(array('canSeeAllServices'))) {
            if (CheckAuth::checkCredential(array('canSeeOnlyHisServices'))) {
                $select->where("contractor_id = '{$loggedUser['user_id']}'");
            }
        }

        if ($filters) {

            if (!empty($filters['is_accepted'])) {
                $select->where("csb.is_accepted = '{$filters['is_accepted']}'");
            }

            if (!empty($filters['is_rejected'])) {
                $select->where("csb.is_rejected = '{$filters['is_rejected']}'");
            }



            if (!empty($filters['NotAcceptedAndRejected'])) {
                $select->where("csb.is_accepted = 0 AND csb.is_rejected = 0");
            }
        }

        $sql = $this->getAdapter()->select();
        $sql->from($select, 'COUNT(*)');

        return $this->getAdapter()->fetchOne($sql);
    }

//    public function getTotalBookingQoute($bookingId) {
//        //
//        // load models
//        //
//        $modelAttributeListValue = new Model_AttributeListValue();
//        $modelServiceAttribute = new Model_ServiceAttribute();
//        $modelServiceAttributeValueTemp = new Model_ServiceAttributeValueTemp();
//        $modelServices = new Model_Services();
//        $modelAttributes = new Model_Attributes();
//
//        //
//        //get all service for this booking 
//        //
//        $bookingServices = $this->getByBookingId($bookingId);
//
//        $totalQoute = 0;
//
//        if ($bookingServices) {
//            foreach ($bookingServices as $bookingService) {
//                $serviceId = $bookingService['service_id'];
//                $clone = $bookingService['clone'];
//                $uniqid = uniqid();
//
//                $service = $modelServices->getById($serviceId);
//
//                if (preg_match_all('#\[(.*?)\]#', $service['price_equasion'], $matches)) {
//                    $param = array();
//                    foreach ($matches[1] as $attributeVariableName) {
//                        $attribute = $modelAttributes->getByVariableName($attributeVariableName);
//
//                        $serviceAttribute = $modelServiceAttribute->getByAttributeIdAndServiceId($attribute['attribute_id'], $serviceId);
//
//                        $value = $modelServiceAttributeValueTemp->getByBookingIdAndServiceAttributeIdAndClone($bookingService['booking_id'], $serviceAttribute['service_attribute_id'], $clone);
//
//                        if ($attribute['is_list']) {
//                            $attributeListValue = $modelAttributeListValue->getById($value['value']);
//                            $param[$attributeVariableName] = $attributeListValue['unit_price'];
//                        } else {
//
//                            $param[$attributeVariableName] = $value['value'];
//                        }
//                    }
//
//                    $equasion = $service['price_equasion'];
//                    $min_price = $service['min_price'];
//
//                    //build getPrice function
//                    createPriceEquasionFunction("getPrice_{$uniqid}", $equasion);
//
//                    $serviceQoute = call_user_func("getPrice_{$uniqid}", $param);
//
//                    if ($serviceQoute >= $min_price) {
//                        $totalQoute = $totalQoute + $serviceQoute;
//                    } else {
//                        $totalQoute = $totalQoute + $min_price;
//                    }
//                }
//            }
//        }
//
//        return $totalQoute;
//    }


    public function getTotalBookingQoute($bookingId) {
        //
        // load models
        //
        $modelServices = new Model_Services();

        //
        //get all service for this booking 
        //
        $bookingServices = $this->getByBookingId($bookingId);

        $totalQoute = 0;

        if ($bookingServices) {
            foreach ($bookingServices as $bookingService) {
                $serviceId = $bookingService['service_id'];
                $clone = $bookingService['clone'];
                $considerMinPrice = $bookingService['consider_min_price'];

                $service = $modelServices->getById($serviceId);
                $min_price = $service['min_price'];

                $serviceQoute = $this->getServiceBookingQoute($bookingId, $serviceId, $clone);

                if (($serviceQoute < $min_price) && $considerMinPrice === 1) {
                    $totalQoute = $totalQoute + $min_price;
                } else {
                    $totalQoute = $totalQoute + $serviceQoute;
                }
            }
        }

        return $totalQoute;
    }

    public function getTotalBookingQouteBalance($bookingId) {
        $modelBooking = new Model_Booking();
        $booking = $modelBooking->getById($bookingId);
        $subTotalTemp = $this->getTotalBookingQoute($bookingId);

        $totalDiscountTemp = $booking['total_discount_temp'];
        $callOutFeeTemp = $booking['call_out_fee_temp'];
        $gstTaxTemp = ($subTotalTemp + $callOutFeeTemp - $totalDiscountTemp) * get_config('gst_tax');
        $totalQouteTemp = ($subTotalTemp + $callOutFeeTemp - $totalDiscountTemp) + $gstTaxTemp;
        return $totalQouteTemp;
    }

    public function getTotalServiceBookingQoute($bookingId, $serviceId, $clone = 0) {

        //
        // load models
        //
        $modelServices = new Model_Services();

        $bookingService = $this->getByBookingAndServiceAndClone($bookingId, $serviceId, $clone);
        $considerMinPrice = $bookingService['consider_min_price'];

        $service = $modelServices->getById($serviceId);
        $min_price = $service['min_price'];

        $serviceQoute = $this->getServiceBookingQoute($bookingId, $serviceId, $clone);

        if (($serviceQoute < $min_price) && $considerMinPrice === 1) {
            $serviceQoute = $min_price;
        }

        return $serviceQoute;
    }

    public function getServiceBookingQoute($bookingId, $serviceId, $clone = 0) {

        //
        // load models
        //
        $modelAttributeListValue = new Model_AttributeListValue();
        $modelServiceAttribute = new Model_ServiceAttribute();
        $modelServiceAttributeValueTemp = new Model_ServiceAttributeValueTemp();
        $modelServices = new Model_Services();
        $modelAttributes = new Model_Attributes();

        $uniqid = uniqid();
        $serviceQoute = 0;

        $service = $modelServices->getById($serviceId);
        if (preg_match_all('#\[(.*?)\]#', $service['price_equasion'], $matches)) {
            $param = array();
            foreach ($matches[1] as $attributeVariableName) {
                $attribute = $modelAttributes->getByVariableName($attributeVariableName);

                $serviceAttribute = $modelServiceAttribute->getByAttributeIdAndServiceId($attribute['attribute_id'], $serviceId);

                $value = $modelServiceAttributeValueTemp->getByBookingIdAndServiceAttributeIdAndClone($bookingId, $serviceAttribute['service_attribute_id'], $clone);

                if ($attribute['is_list']) {
                    $attributeListValue = $modelAttributeListValue->getById($value['value']);
                    $param[$attributeVariableName] = $attributeListValue['unit_price'];
                } else {
                    $param[$attributeVariableName] = $value['value'];
                }
            }

            $equasion = $service['price_equasion'];

            //build getPrice function
            createPriceEquasionFunction("getPrice_{$uniqid}", $equasion);

            $serviceQoute = call_user_func("getPrice_{$uniqid}", $param);
        }

        return $serviceQoute;
    }

//    public function getServiceBookingQoute($bookingId, $serviceId, $clone = 0) {
//        //
//        // load models
//        //
//        if (!$this->modelAttributeListValue) {
//            $this->modelAttributeListValue = new Model_AttributeListValue();
//        }
//
//        if (!$this->modelServiceAttribute) {
//            $this->modelServiceAttribute = new Model_ServiceAttribute();
//        }
//
//        if (!$this->modelServiceAttributeValueTemp) {
//            $this->modelServiceAttributeValueTemp = new Model_ServiceAttributeValueTemp();
//        }
//
//        if (!$this->modelServices) {
//            $this->modelServices = new Model_Services();
//        }
//
//        if (!$this->modelAttributes) {
//            $this->modelAttributes = new Model_Attributes();
//        }
//
//        $uniqid = uniqid();
//        $serviceQoute = 0;
//
//        $service = $this->modelServices->getById($serviceId);
//
//        if (preg_match_all('#\[(.*?)\]#', $service['price_equasion'], $matches)) {
//            $param = array();
//            foreach ($matches[1] as $attributeVariableName) {
//                $attribute = $this->modelAttributes->getByVariableName($attributeVariableName);
//
//                $serviceAttribute = $this->modelServiceAttribute->getByAttributeIdAndServiceId($attribute['attribute_id'], $serviceId);
//
//                $value = $this->modelServiceAttributeValueTemp->getByBookingIdAndServiceAttributeIdAndClone($bookingId, $serviceAttribute['service_attribute_id'], $clone);
//
//                if ($attribute['is_list']) {
//                    $attributeListValue = $this->modelAttributeListValue->getById($value['value']);
//                    $param[$attributeVariableName] = $attributeListValue['unit_price'];
//                } else {
//                    $param[$attributeVariableName] = $value['value'];
//                }
//            }
//
//            $equasion = $service['price_equasion'];
//
//            //build getPrice function
//            createPriceEquasionFunction("getPrice_{$uniqid}", $equasion);
//
//            $serviceQoute = call_user_func("getPrice_{$uniqid}", $param);
//        }
//
//        return $serviceQoute;
//    }
}