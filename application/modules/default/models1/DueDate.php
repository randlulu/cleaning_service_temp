<?php

class Model_DueDate extends Zend_Db_Table_Abstract {

    protected $_name = 'due_date';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order($order);

        $filters['company_id'] = CheckAuth::getCompanySession();

        if ($filters) {
            if (!empty($filters['due_date'])) {
                $select->where("due_date = {$filters['due_date']}");
            }
            if (!empty($filters['company_id'])) {
                $company_id = (int) $filters['company_id'];
                $select->where("company_id = {$company_id} OR is_core = 1");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }


        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table rows according to the assigned id and data
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "id = '{$id}'");
    }

    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("id = '{$id}'");
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get due date as array
     * 
     * @return array 
     */
    public function getDueDateAsArray() {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order('due_date asc');

        $duedates = $this->getAdapter()->fetchAll($select);

        $data = array();
        foreach ($duedates as $duedate) {
            $data[$duedate['id']] = $duedate['due_date'];
        }
        return $data;
    }

    /**
     * get default id as array
     * 
     * @return array 
     */
    public function getDefaultId() {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("is_default = 1");
        return $this->getAdapter()->fetchRow($select);
    }

    public function getByDueDateAndCompany($dueDate, $companyId = 0) {

        if (!$companyId) {
            $companyId = CheckAuth::getCompanySession();
        }

        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("due_date = '{$dueDate}'");
        $select->where("company_id = '{$companyId}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function checkBeforeDeleteDueDate($dueDateId, &$tables = array()) {

        $sucsess = true;

        $select_booking_due_date = $this->getAdapter()->select();
        $select_booking_due_date->from('booking_due_date', 'COUNT(*)');
        $select_booking_due_date->where("due_date_id = {$dueDateId}");
        $count_booking_due_date = $this->getAdapter()->fetchOne($select_booking_due_date);

        if ($count_booking_due_date) {
            $tables[] = 'booking_due_date';
            $sucsess = false;
        }

        return $sucsess;
    }

}
