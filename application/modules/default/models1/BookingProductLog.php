
<?php

class Model_BookingProductLog extends Zend_Db_Table_Abstract {

    protected $_name = 'booking_product_log';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {

        $joinInner = array();
        $select = $this->getAdapter()->select();
        $select->from(array('add_log' => $this->_name));
        $select->order($order);


        if ($joinInner) {
            foreach ($joinInner as $inner) {
                $select->joinInner($inner['name'], $inner['cond'], $inner['cols']);
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table rows according to the assigned id and data
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "log_id= '{$id}'");
    }

    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("log_id= '{$id}'");
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("log_id= '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function getByBookingId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_id = '{$id}'");
        return $this->getAdapter()->fetchAll($select);
    }

    public function getByLogBookingId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("log_booking_id = '{$id}'");
        return $this->getAdapter()->fetchAll($select);
    }

    public function addBookingProductLog($id, $log_user_id = 0) {

        $modelBookingProduct = new Model_BookingProduct();
        $data = $modelBookingProduct->getById($id);

        $modelBookingLog = new Model_BookingLog();
        $last = $modelBookingLog->getLastLogByBookingId($data['booking_id']);
        $lastLogBookingId = !empty($last) ? $last['log_id'] : 0;

        if (!$log_user_id) {
            $logedUser = CheckAuth::getLoggedUser();
            $log_user_id = !empty($logedUser) ? $logedUser['user_id'] : 0;
        }

        $dbParams = array();
        $dbParams['log_created'] = time();
        $dbParams['log_user_id'] = $log_user_id;
        $dbParams['log_booking_id'] = $lastLogBookingId;
        $dbParams['id'] = $data['id'];
        $dbParams['booking_id'] = $data['booking_id'];
        $dbParams['product_id'] = $data['product_id'];
        $dbParams['ltr'] = $data['ltr'];

        return parent::insert($dbParams);
    }

    public function getLastLogbyBookingIdAndLogBookingId($bookingId, $LogBookingId) {

        $selectB = $this->getAdapter()->select();
        $selectB->from($this->_name, array('MAX(log_Booking_id)'));
        $selectB->where("booking_id = {$bookingId}");
        $selectB->where("log_booking_id <= {$LogBookingId}");

        $selectA = $this->getAdapter()->select();
        $selectA->from($this->_name);
        $selectA->where("log_booking_id = ({$selectB})");

        return $this->getAdapter()->fetchAll($selectA);
    }

}