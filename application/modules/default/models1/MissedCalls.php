<?php

class Model_MissedCalls extends Zend_Db_Table_Abstract {

    protected $_name = 'missed_calls';
    private $modelCustomer;
    private $modelUser;

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {

        $joinInner = array();
        $select = $this->getAdapter()->select();
        $select->from(array('mc' => $this->_name));
        $select->order($order);

        if ($filters) {

            if (!empty($filters['user_id'])) {
                $userId = (int) trim($filters['user_id']);
                $select->where("user_id = {$userId}");
            }
            if (isset($filters['is_read'])) {
                if ($filters['is_read'] == 'yes') {
                    $select->where("is_read = 1");
                } else {
                    $select->where("is_read = 0");
                }
            }
        }

        if ($joinInner) {
            foreach ($joinInner as $inner) {
                $select->joinInner($inner['name'], $inner['cond'], $inner['cols']);
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table rows according to the assigned id and data
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "id= '{$id}'");
    }

    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("id= '{$id}'");
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id= '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function getCountMissedCalls() {

        $loggedUser = CheckAuth::getLoggedUser();
        $user_id = isset($loggedUser['user_id']) ? $loggedUser['user_id'] : 0;

        $missedCalls = $this->getAll(array('user_id' => $user_id, 'is_read' => 'no'));

        return count($missedCalls);
    }

    /**
     * get extra info for all the rows
     * @param array $row
     * @param array $types
     * @return array
     */
    public function fills(&$rows, $types = array()) {
        foreach ($rows as &$row) {
            $this->fill($row, $types);
        }
    }

    /**
     * get extra info for each row (Ex: getting the category row by c_id, author row by author id ... etc)
     * 
     * @param array $row
     * @param array $types
     * @return array
     */
    public function fill(&$row, $types = array()) {

        if (in_array('customer', $types)) {
            /**
             * load model
             */
            if (!$this->modelCustomer) {
                $this->modelCustomer = new Model_Customer();
            }
            $row['customer'] = $this->modelCustomer->getById($row['customer_id']);
            $row['customer_name'] = get_customer_name($row['customer']);
        }

        if (in_array('created_by', $types)) {
            /**
             * load model
             */
            if (!$this->modelUser) {
                $this->modelUser = new Model_User();
            }

            $row['created_by'] = $this->modelUser->getById($row['created_by']);
        }
        if (in_array('user_id', $types)) {
            /**
             * load model
             */
            if (!$this->modelUser) {
                $this->modelUser = new Model_User();
            }

            $row['user_id'] = $this->modelUser->getById($row['user_id']);
        }
    }

}