<?php

class Model_PagerDB extends Zend_Paginator_Adapter_DbSelect {

    public function __construct(Zend_Db_Select $select) {
        parent::__construct($select);
    }
    
    /**
     *function to over-ride the zend framework to count the table rows
     * @param  from the select state.
     * @return int 
     */
    public function count() {

        $sql = $this->_select;
        $sql->limit();

        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $select = $dbAdapter->select();
        $select->from($sql, 'COUNT(*)');

        return $dbAdapter->fetchOne($select);
    }

}