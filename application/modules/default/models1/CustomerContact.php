<?php

class Model_CustomerContact extends Zend_Db_Table_Abstract {

    protected $_name = 'customer_contact';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from(array('cc' => $this->_name));
        $select->order($order);

        if ($filters) {
            
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }
        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table rows according to the assigned id and data
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "id = '{$id}'");
    }

    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("id = '{$id}'");
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getByCustomerId($customer_id) {
        $customer_id = (int) $customer_id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("customer_id = '{$customer_id}'");
        
        return $this->getAdapter()->fetchAll($select);
    }

      /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteByCustomerId($customer_id) {
        $customer_id = (int) $customer_id;
        return parent::delete("customer_id = '{$customer_id}'");
    }

}


