<?php
class Model_InquiryDiscussion extends Zend_Db_Table_Abstract {

    protected $_name = 'inquiry_discussion';

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        
        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'changed');
        
        return parent::update($data, "discussion_id = '{$id}'");
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("discussion_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the assigned estimateId
     * 
     * @param int $id
     * @return array
     */
    public function getByInquiryId($id, $order = 'asc') {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order("created {$order}");
        $select->where("inquiry_id = '{$id}'");

        return $this->getAdapter()->fetchAll($select);
    }
    
    public function insert(array $data) {

        $id = parent::insert($data);
        
        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'added');
        
        return $id;
    }

}