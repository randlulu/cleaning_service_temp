<?php

class Model_Report extends Zend_Db_Table_Abstract {

    protected $_name = 'report';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from(array('r' => $this->_name));
        $select->order($order);

        $filters['company_id'] = CheckAuth::getCompanySession();

        if ($filters) {
            if (!empty($filters['report_type'])) {
                $report_type = $this->getAdapter()->quote($filters['report_type']);
                $select->where("r.report_type = {$report_type}");
            }

            if (!empty($filters['company_id'])) {
                $company_id = $this->getAdapter()->quote($filters['company_id']);
                $select->where("r.company_id = {$company_id}");
            }

            if (!empty($filters['user_id'])) {
                $user_id = $this->getAdapter()->quote($filters['user_id']);
                $select->where("r.user_id = {$user_id}");
            }
        }


        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }
        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table rows according to the assigned id and data
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        
        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'changed');
        
        return parent::update($data, "report_id = '{$id}'");
    }

    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        
        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'deleted');
        
        return parent::delete("report_id = '{$id}'");
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("report_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according
     * 
     * @param int $id
     * @param boolean $asArray
     * @return array
     */
    public function getReportType() {
        $select = $this->getAdapter()->select();
        $select->from(array('r' => $this->_name), 'r.report_type');
        $select->distinct();
        $select->order('report_type asc');
        $company_id = CheckAuth::getCompanySession();
        $select->where("r.company_id = {$company_id}");

        $reports = $this->getAdapter()->fetchAll($select);

        $data = array();
        foreach ($reports as $report) {
            $data[$report['report_type']] = $report['report_type'];
        }
        return $data;
    }
    
    public function insert(array $data) {

        $id = parent::insert($data);
        
        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'added');
        
        return $id;
    }

}