<?php

class Model_BookingInvoice extends Zend_Db_Table_Abstract {

    protected $_name = 'booking_invoice';
    //
    //model for fill function
    //
    
    private $loggedUser = array();
    private $modelBooking;
    private $modelContractorInfo;
    private $modelContractorServiceBooking;
    private $modelCustomer;
    private $modelInvoiceLabel;
    private $modelLabel;
    private $modelCities;
    private $modelUser;
    private $modelBookingLog;
    private $modelCustomerCommercialInfo;
    private $modelBookingMultipleDays;
    private $modelBookingAttachment;
    private $modelServices;

    public function init() {
        parent::init();
        $this->loggedUser = CheckAuth::getLoggedUser();
    }

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from(array('inv' => $this->_name));
        $select->order($order);
        $select->distinct();

        $joinInner = array();
        $filters['company_id'] = CheckAuth::getCompanySession();

        if (CheckAuth::checkCredential(array('canSeeDeletedInvoice'))) {
            if (!empty($filters['is_deleted'])) {
                $select->where('inv.is_deleted = 1');
            } else {
                $select->where('inv.is_deleted = 0');
            }
        } else {
            $select->where('inv.is_deleted = 0');
        }

        $loggedUser = CheckAuth::getLoggedUser();
        if (!CheckAuth::checkCredential(array('canSeeAllInvoice'))) {
            if (CheckAuth::checkCredential(array('canSeeOnlyHisInvoice'))) {
                if (CheckAuth::checkCredential(array('canSeeAssignedInvoice'))) {

                    $joinInner['contractor_service_booking'] = array('name' => array('csb' => 'contractor_service_booking'), 'cond' => 'inv.booking_id = csb.booking_id', 'cols' => '');
                    $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'inv.booking_id = bok.booking_id', 'cols' => '');

                    $select->where("bok.created_by = " . $loggedUser['user_id'] . " OR csb.contractor_id = {$loggedUser['user_id']}");
                } else {
                    $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'inv.booking_id = bok.booking_id', 'cols' => '');
                    $select->where("bok.created_by = " . $loggedUser['user_id']);
                }
            } else {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'inv.booking_id = bok.booking_id', 'cols' => '');
                $select->where("bok.created_by = " . $loggedUser['user_id']);
            }
        }

        if ($filters) {

            if (!empty($filters['keywords'])) {
                $keywords = $this->getAdapter()->quote('%' . trim($filters['keywords']) . '%');
                $select->where("inv.full_text_search LIKE {$keywords}");

                //$keywords = '+' . preg_replace('#[\s]+#', ' +', trim($filters['keywords']));
                //$keywords = $this->getAdapter()->quote($keywords);
                //$select->where("MATCH(inv.full_text_search) AGAINST({$keywords} IN BOOLEAN MODE)");
            }

            if (!empty($filters['invoice_num'])) {
                $invoice_num = $this->getAdapter()->quote('%' . trim($filters['invoice_num']) . '%');
                $select->where("inv.invoice_num LIKE {$invoice_num}");
            }

            if (!empty($filters['booking_num'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'inv.booking_id = bok.booking_id', 'cols' => '');

                $booking_num = $this->getAdapter()->quote('%' . trim($filters['booking_num']) . '%');
                $select->where("bok.booking_num LIKE {$booking_num}");
            }

            if (!empty($filters['invoice_type']) && $filters['invoice_type'] != 'all') {
                $invoice_type = $this->getAdapter()->quote(trim($filters['invoice_type']));
                if ('unpaid' == $filters['invoice_type']) {
                    $select->where("inv.invoice_type = 'overdue' OR inv.invoice_type = 'open'");
                } elseif ('all_active' == $filters['invoice_type']) {
                    $select->where("inv.invoice_type = 'overdue' OR inv.invoice_type = 'open' OR inv.invoice_type = 'closed'");
                } else {
                    $select->where("inv.invoice_type = {$invoice_type}");
                }
            }

            if (!empty($filters['booking_start'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'inv.booking_id = bok.booking_id', 'cols' => '');

                $bookingStart = $this->getAdapter()->quote(trim($filters['booking_start']));
                $select->where("bok.booking_start >= {$bookingStart}");
            }

            if (!empty($filters['booking_end'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'inv.booking_id = bok.booking_id', 'cols' => '');

                $bookingEnd = $this->getAdapter()->quote(trim($filters['booking_end']));
                $select->where("bok.booking_end <= {$bookingEnd}");
            }

            if (!empty($filters['customer_id'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'inv.booking_id = bok.booking_id', 'cols' => '');

                $filters['customer_id'] = (int) $filters['customer_id'];
                $select->where("bok.customer_id = {$filters['customer_id']} ");
            }

            if (!empty($filters['contractor_id'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'inv.booking_id = bok.booking_id', 'cols' => '');
                $joinInner['contractor_service_booking'] = array('name' => array('csb' => 'contractor_service_booking'), 'cond' => 'inv.booking_id = csb.booking_id', 'cols' => '');

                $contractor_id = (int) $filters['contractor_id'];
                $select->where("bok.created_by = {$contractor_id} OR csb.contractor_id = {$contractor_id}");
            }

            if (!empty($filters['created_by'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'inv.booking_id = bok.booking_id', 'cols' => '');
                $userId = (int) $filters['created_by'];
                $select->where("bok.created_by = '{$userId}'");
            }

            if (!empty($filters['convert_status'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'inv.booking_id = bok.booking_id', 'cols' => '');

                $convert_status = $this->getAdapter()->quote(trim($filters['convert_status']));
                $select->where("bok.convert_status = {$convert_status}");
            }

            if (!empty($filters['service_id'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'inv.booking_id = bok.booking_id', 'cols' => '');
                $joinInner['contractor_service_booking'] = array('name' => array('csb' => 'contractor_service_booking'), 'cond' => 'inv.booking_id = csb.booking_id', 'cols' => '');

                $service_id = (int) $filters['service_id'];
                $select->where("csb.service_id = {$service_id}");
            }

            if (!empty($filters['created_from'])) {
                $created_from = $this->getAdapter()->quote(trim($filters['created_from']));
                $select->where("inv.created >= {$created_from}");
            }

            if (!empty($filters['created_to'])) {
                $created_to = $this->getAdapter()->quote(trim($filters['created_to']));
                $select->where("inv.created <= {$created_to}");
            }

            if (!empty($filters['qoute_from'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'inv.booking_id = bok.booking_id', 'cols' => '');

                $qoute_from = $this->getAdapter()->quote(trim($filters['qoute_from']));
                $select->where("bok.qoute >= {$qoute_from}");
            }

            if (!empty($filters['qoute_to'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'inv.booking_id = bok.booking_id', 'cols' => '');

                $qoute_to = $this->getAdapter()->quote(trim($filters['qoute_to']));
                $select->where("bok.qoute <= {$qoute_to}");
            }

            if (!empty($filters['city_id'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'inv.booking_id = bok.booking_id', 'cols' => '');

                $city_id = (int) $filters['city_id'];
                $select->where("bok.city_id = {$city_id}");
            }
            if (!empty($filters['invoice_label_ids']) && $filters['invoice_label_ids']) {
                $joinInner['invoice_label'] = array('name' => array('il' => 'invoice_label'), 'cond' => 'inv.id = il.invoice_id', 'cols' => '');
                $select->where('il.label_id IN (' . implode(', ', $filters['invoice_label_ids']) . ')');
            }
            if (!empty($filters['company_id'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'inv.booking_id = bok.booking_id', 'cols' => '');

                $company_id = (int) $filters['company_id'];
                $select->where("bok.company_id = {$company_id}");
            }
            if (!empty($filters['booking_start_between'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'inv.booking_id = bok.booking_id', 'cols' => '');

                $bookingStartBetween = $filters['booking_start_between'];
                $bookingEndBetween = !empty($filters['booking_end_between']) ? $filters['booking_end_between'] : date('Y-m-d H:i:s', time());
                $select->where("bok.booking_start between '" . $bookingStartBetween . "' and '" . $bookingEndBetween . "'");
            }
            if (!empty($filters['address'])) {
                $address = trim($filters['address']);
                $joinInner['booking_address'] = array('name' => array('ba' => 'booking_address'), 'cond' => 'inv.booking_id = ba.booking_id', 'cols' => '');
                $select->where("CONCAT_WS(' ',ba.street_number ,ba.street_address ,ba.suburb,ba.state,ba.postcode) LIKE '%{$address}%'");
            }
            if (!empty($filters['email'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'inv.booking_id = bok.booking_id', 'cols' => '');
                $joinInner['customer'] = array('name' => array('c' => 'customer'), 'cond' => 'bok.customer_id = c.customer_id', 'cols' => '');

                $email = trim($filters['email']);
                $select->where("CONCAT_WS(' ',email1 ,email2 ,email3) LIKE '%{$email}%'");
            }
            if (!empty($filters['mobile/phone'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'inv.booking_id = bok.booking_id', 'cols' => '');
                $joinInner['customer'] = array('name' => array('c' => 'customer'), 'cond' => 'bok.customer_id = c.customer_id', 'cols' => '');

                $mobile_phone = trim($filters['mobile/phone']);
                $select->where("CONCAT_WS(' ',phone1 ,phone2 ,phone3,mobile1 ,mobile2 ,mobile3) LIKE '%{$mobile_phone}%'");
            }
            if (!empty($filters['bussiness_name'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'inv.booking_id = bok.booking_id', 'cols' => '');
                $joinInner['customer_commercial_info'] = array('name' => array('cci' => 'customer_commercial_info'), 'cond' => 'bok.customer_id = cci.customer_id', 'cols' => '');
                $select->where("cci.business_name LIKE '%{$filters['bussiness_name']}%'");
            }
            if (!empty($filters['inquiry_num'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'inv.booking_id = bok.booking_id', 'cols' => '');
                $joinInner['inquiry'] = array('name' => array('i' => 'inquiry'), 'cond' => 'bok.original_inquiry_id = i.inquiry_id', 'cols' => '');
                $inquiry_num = $this->getAdapter()->quote('%' . trim($filters['inquiry_num']) . '%');
                $select->where("i.inquiry_num LIKE {$inquiry_num}");
            }
            if (!empty($filters['estimate_num'])) {
                $joinInner['booking_estimate'] = array('name' => array('est' => 'booking_estimate'), 'cond' => 'inv.booking_id = est.booking_id', 'cols' => '');
                $estimate_num = $this->getAdapter()->quote('%' . trim($filters['estimate_num']) . '%');
                $select->where("est.estimate_num LIKE {$estimate_num}");
            }
            if (!empty($filters['not_paid'])) {
                $paymentSelect = $this->getAdapter()->select();
                $paymentSelect->from(array('pa' => 'payment'), array('pa.booking_id', 'SUM(pa.amount) AS payment_amount'));
                $paymentSelect->group('pa.booking_id');

                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'inv.booking_id = bok.booking_id', 'cols' => '');
                $joinInner['payment_amount'] = array('name' => array('p_amount' => $paymentSelect), 'cond' => 'inv.booking_id = p_amount.booking_id AND bok.qoute > p_amount.payment_amount', 'cols' => '');
            }
        }

        if ($joinInner) {
            foreach ($joinInner as $inner) {
                $select->joinInner($inner['name'], $inner['cond'], $inner['cols']);
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

    public function insert(array $data) {

        $company_id = CheckAuth::getCompanySession();

        $select = $this->getAdapter()->select();
        $select->from(array('inv' => $this->_name), 'MAX(inv.count)');
        $select->joinInner(array('bok' => 'booking'), 'inv.booking_id = bok.booking_id', '');
        $select->where("bok.company_id = {$company_id}");
        $result = $this->getAdapter()->fetchOne($select);

        $data['count'] = $result + 1;
        $data['invoice_num'] = 'INV-' . ($result + 1);

        $id = parent::insert($data);

        /**
         * update full text search table
         */
        $modelUpdateFullTextSearch = new Model_UpdateFullTextSearch();
        $modelUpdateFullTextSearch->insertFullTextSearch($this->_name, $id);

        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'added');

        /**
         * get all record and insert it in log
         */
        $modelBookingInvoiceLog = new Model_BookingInvoiceLog();
        $modelBookingInvoiceLog->addBookingInvoiceLog($id);

        return $id;
    }

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateById($id, $data, $addLog = true) {
        $id = (int) $id;

        /**
         * update full text search table
         */
        $done = false;
        if (!empty($data['full_text_search'])) {
            $done = true;
        }
        $modelUpdateFullTextSearch = new Model_UpdateFullTextSearch();
        $modelUpdateFullTextSearch->updateFullTextSearchFlag($this->_name, $id, $done);

        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'changed');

        $success = parent::update($data, "id = '{$id}'");

        /**
         * get all record and insert it in log
         */
        $modelBookingInvoiceLog = new Model_BookingInvoiceLog();

        if ($addLog) {
            $modelBookingInvoiceLog->addBookingInvoiceLog($id);
        } else {
            $last = $modelBookingInvoiceLog->getLastLogByInvoiceId($id);
            $lastLogInvoiceId = !empty($last) ? $last['log_id'] : 0;
            $data = $this->getById($id);
            $modelBookingInvoiceLog->updateById($lastLogInvoiceId, $data);
        }

        return $success;
    }

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateByBookingId($bookingId, $data) {
        $bookingId = (int) $bookingId;
        $bookingInvoice = $this->getByBookingIdWhithOutDeleted($bookingId);
        if ($bookingInvoice) {
            $this->updateById($bookingInvoice['id'], $data);
        }
    }

    /**
     * delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean
     */
    public function deleteForeverByBookingId($bookingId) {
        $bookingId = (int) $bookingId;

        return parent::delete("booking_id = '{$bookingId}'");
    }

    /**
     * delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;

        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'deleted');

        return parent::delete("id = '{$id}'");
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getByBookingId($bookingId) {
        $bookingId = (int) $bookingId;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_id = '{$bookingId}' AND is_deleted = 0");

        return $this->getAdapter()->fetchRow($select);
    }

    public function getByBookingIdWhithOutDeleted($bookingId) {
        $bookingId = (int) $bookingId;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_id = '{$bookingId}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function getEnamInvoiceType() {
        $query = "SHOW COLUMNS FROM {$this->_name} LIKE 'invoice_type'";
        $stmt = $this->getAdapter()->query($query);
        $row = $stmt->fetch();
        $row = $row['Type'];
        $regex = "/'(.*?)'/";
        preg_match_all($regex, $row, $enum_array);
        $enum_fields = $enum_array[1];
        foreach ($enum_fields as $key => $value) {
            $enums[$value] = $value;
        }
        return $enums;
    }

    /**
     * getInvoiceByCustomerId
     * 
     * @param int $customerId
     * @return array 
     */
    public function getInvoiceByCustomerId($customerId) {

        $filters = array();
        $filters['customer_id'] = $customerId;

        return $this->getAll($filters);
    }

    /**
     * get extra info for all the rows
     * @param array $row
     * @param array $types
     * @return array
     */
    public function fills(&$rows, $types = array()) {
        foreach ($rows as &$row) {
            $this->fill($row, $types);
        }
    }

    /**
     * get extra info for each row (Ex: getting the category row by c_id, author row by author id ... etc)
     * 
     * @param array $row
     * @param array $types
     * @return array
     */
    public function fill(&$row, $types = array()) {

        if (in_array('booking', $types)) {
            /**
             * load model
             */
            if (!$this->modelBooking) {
                $this->modelBooking = new Model_Booking();
            }

            $booking = $this->modelBooking->getById($row['booking_id']);
            $row['booking'] = $booking;
        }

        if (in_array('contractors', $types)) {
            /**
             * load model
             */
            if (!$this->modelContractorInfo) {
                $this->modelContractorInfo = new Model_ContractorInfo();
            }
            if (!$this->modelContractorServiceBooking) {
                $this->modelContractorServiceBooking = new Model_ContractorServiceBooking();
            }
            if (!$this->modelUser) {
                $this->modelUser = new Model_User();
            }

            $allContractorServiceBooking = $this->modelContractorServiceBooking->getByBookingId($row['booking_id']);

            $allContractors = array();

            foreach ($allContractorServiceBooking as $contractorServiceBooking) {
                $contractorInfo = $this->modelContractorInfo->getByContractorId($contractorServiceBooking['contractor_id']);
                $userInfo = $this->modelUser->getById($contractorServiceBooking['contractor_id']);
                if ('contractor' == CheckAuth::getRoleName()) {
                    if ($contractorServiceBooking['contractor_id'] == $this->loggedUser['user_id']) {
                        $allContractors[$contractorServiceBooking['contractor_id']] = ucwords($userInfo['username']) . " - " . ucwords($contractorInfo['business_name'] ? $contractorInfo['business_name'] : '');
                    }
                } else {
                    $allContractors[$contractorServiceBooking['contractor_id']] = ucwords($userInfo['username']) . " - " . ucwords($contractorInfo['business_name']);
                }
            }
            $row['contractors'] = $allContractors;
        }

        if (in_array('name_contractors', $types)) {
            /**
             * load model
             */
            if (!$this->modelUser) {
                $this->modelUser = new Model_User();
            }
            if (!$this->modelContractorServiceBooking) {
                $this->modelContractorServiceBooking = new Model_ContractorServiceBooking();
            }

            $allContractorServiceBooking = $this->modelContractorServiceBooking->getByBookingId($row['booking_id']);

            $contractors = array();
            if ($allContractorServiceBooking) {
                foreach ($allContractorServiceBooking as $bookingService) {
                    $user = $this->modelUser->getById($bookingService['contractor_id']);

                    $contractorName = isset($user['display_name']) ? $user['display_name'] : '';
                    $contractors[$bookingService['contractor_id']] = ucwords($contractorName);
                }
            }
            $row['name_contractors'] = $contractors;
        }

        if (in_array('customer', $types)) {
            /**
             * load model
             */
            if (!$this->modelBooking) {
                $this->modelBooking = new Model_Booking();
            }
            if (!$this->modelCustomer) {
                $this->modelCustomer = new Model_Customer();
            }

            $booking = $this->modelBooking->getById($row['booking_id']);
            $row['customer'] = $this->modelCustomer->getById($booking['customer_id']);
            $row['customer_name'] = get_customer_name($row['customer']);
        }
        if (in_array('customer_commercial_info', $types)) {
            /**
             * load model
             */
            if (!$this->modelBooking) {
                $this->modelBooking = new Model_Booking();
            }
            if (!$this->modelCustomerCommercialInfo) {
                $this->modelCustomerCommercialInfo = new Model_CustomerCommercialInfo();
            }
            $booking = $this->modelBooking->getById($row['booking_id']);
            $row['customer_commercial_info'] = $this->modelCustomerCommercialInfo->getByCustomerId($booking['customer_id']);
            // print_r($row['customer_commercial_info']);die;
        }

        if (in_array('city', $types)) {
            /**
             * load model
             */
            if (!$this->modelBooking) {
                $this->modelBooking = new Model_Booking();
            }
            if (!$this->modelCities) {
                $this->modelCities = new Model_Cities();
            }

            $booking = $this->modelBooking->getById($row['booking_id']);
            $row['city'] = $this->modelCities->getById($booking['city_id']);
            $row['city_name'] = $row['city']['city_name'];
        }

        if (in_array('labels', $types)) {
            /**
             * load model
             */
            if (!$this->modelInvoiceLabel) {
                $this->modelInvoiceLabel = new Model_InvoiceLabel();
            }
            if (!$this->modelLabel) {
                $this->modelLabel = new Model_Label();
            }

            $LabelIds = $this->modelInvoiceLabel->getByInvoiceId($row['id']);

            $labels = array();
            foreach ($LabelIds as $LabelId) {
                $label = $this->modelLabel->getById($LabelId['label_id']);
                $labels[$label['id']] = $label['label_name'];
            }
            $row['labels'] = $labels;
        }


        if (in_array('due_date', $types)) {
            $row['due_date'] = $this->getDueDate($row['booking_id']);
        }

        if (in_array('number_of_due_days', $types)) {
            $cdate = $row['created'];
            $today = time();
            $difference = $today - $cdate;
            if ($difference < 0) {
                $difference = 0;
            }
            $ago = (int) floor($difference / 60 / 60 / 24);
            $due = $this->getDueDate($row['booking_id']);

            if ($ago > $due) {
                $row['due'] = $ago - $due;
            } else {
                $row['due'] = $due - $ago;
            }
        }

        if (in_array('paid_amount', $types)) {
            /**
             * load model
             */
            if (!$this->modelBooking) {
                $this->modelBooking = new Model_Booking();
            }

            $filters = array('invoice_type' => 'unpaid', 'convert_status' => 'invoice', 'booking_id' => $row['booking_id']);
            $row['amount'] = $this->modelBooking->total($filters, 'qoute');
            $row['paid_amount'] = $this->modelBooking->total($filters, 'paid_amount');
        }
        if (in_array('booking_users', $types)) {
            /**
             * load model
             */
            if (!$this->modelBookingLog) {
                $this->modelBookingLog = new Model_BookingLog();
            }
            if (!$this->modelUser) {
                $this->modelUser = new Model_User();
            }

            $bookingUsers = $this->modelBookingLog->getUsersByBookingId($row['booking_id']);
            $this->modelUser->fills($bookingUsers, array('user'));

            $row['booking_users'] = $bookingUsers;
        }
        if (in_array('multiple_days', $types)) {
            /**
             * load model
             */
            if (!$this->modelBookingMultipleDays) {
                $this->modelBookingMultipleDays = new Model_BookingMultipleDays();
            }

            $row['multiple_days'] = $this->modelBookingMultipleDays->getByBookingId($row['booking_id']);
        }
        if (in_array('have_attachment', $types)) {
            if (!$this->modelBookingAttachment) {
                $this->modelBookingAttachment = new Model_BookingAttachment();
            }
            $booking = $this->modelBooking->getById($row['booking_id']);
            $have_attachment = $this->modelBookingAttachment->getByBookingIdOrInquiryId($row['booking_id'], $booking['original_inquiry_id']);
            $row['have_attachment'] = !empty($have_attachment) ? 1 : 0;
        }
        if (in_array('services', $types)) {
            /**
             * load model
             */
            if (!$this->modelContractorServiceBooking) {
                $this->modelContractorServiceBooking = new Model_ContractorServiceBooking();
            }
            if (!$this->modelServices) {
                $this->modelServices = new Model_Services();
            }
            $filters = array();
            $filters['booking_id'] = $row['booking_id'];
            $services = $this->modelContractorServiceBooking->getAll($filters);

            foreach ($services as &$service) {
                $service['info'] = $this->modelServices->getById($service['service_id']);
            }

            $row['services'] = $services;
        }

        return $row;
    }

    public function getDueDate($bookingId) {
        /**
         * load model
         */
        $modelDueDate = new Model_DueDate();
        $modelBookingDueDate = new Model_BookingDueDate();


        $bookingDueDate = $modelBookingDueDate->getByBookingId($bookingId);
        $dueDate = $modelDueDate->getById($bookingDueDate['due_date_id']);
        $defaultDueDate = $modelDueDate->getDefaultId();
        $due = empty($dueDate['due_date']) ? $defaultDueDate['due_date'] : $dueDate['due_date'];

        return $due;
    }

    public function cronJobChangeToOverdue() {

        $select = $this->getAdapter()->select();

        $select->from(array('inv' => $this->_name));
        $select->joinInner(array('bok' => 'booking'), 'inv.booking_id = bok.booking_id', '');
        $select->where("bok.convert_status = 'invoice'");
        $select->where("inv.invoice_type = 'open' OR inv.invoice_type = 'draft'");
        $select->where("inv.is_deleted = 0");

        $invoices = $this->getAdapter()->fetchAll($select);

        $modelBookingDueDate = new Model_BookingDueDate();
        $modelDueDate = new Model_DueDate();
        $defaultDueDate = $modelDueDate->getDefaultId();


        foreach ($invoices as $invoice) {

            $bookingDueDate = $modelBookingDueDate->getByBookingId($invoice['booking_id']);
            $dueDate = $modelDueDate->getById($bookingDueDate['due_date_id']);

            $cdate = $invoice['created'];
            $today = time();
            $difference = $today - $cdate;
            if ($difference < 0) {
                $difference = 0;
            }

            $ago = (int) floor($difference / 60 / 60 / 24);
            $due = empty($dueDate['due_date']) ? $defaultDueDate['due_date'] : $dueDate['due_date'];

            if ($ago > $due) {
                $this->updateById($invoice['id'], array('invoice_type' => 'overdue'));
            }
        }
    }

    public function cronJobChangeToClose() {

        $select = $this->getAdapter()->select();

        $select->from(array('inv' => $this->_name));
        $select->joinInner(array('bok' => 'booking'), 'inv.booking_id = bok.booking_id', '');
        $select->where("bok.convert_status = 'invoice'");
        $select->where("inv.invoice_type = 'open' OR inv.invoice_type = 'draft' OR inv.invoice_type = 'overdue'");
        $select->where("inv.is_deleted = 0");

        $invoices = $this->getAdapter()->fetchAll($select);
        $this->fills($invoices, array('booking'));

        $modelPayment = new Model_Payment();

        foreach ($invoices as $invoice) {
            $paid_amount = $modelPayment->getTotalAmount(array('booking_id' => $invoice['booking_id'], 'is_approved' => 'yes'));

            $paid_amount = round($paid_amount, 2);
            $qoute = round($invoice['booking']['qoute'], 2);

            if ($paid_amount >= $qoute) {
                $this->updateByBookingId($invoice['booking_id'], array('invoice_type' => 'closed'));
            }
        }
    }

    public function cronJobChickAllPaidAmount() {

        $select = $this->getAdapter()->select();

        $select->from(array('inv' => $this->_name));
        $select->joinInner(array('bok' => 'booking'), 'inv.booking_id = bok.booking_id', '');
        $select->where("bok.convert_status = 'invoice'");

        $invoices = $this->getAdapter()->fetchAll($select);
        $this->fills($invoices, array('booking'));

        $modelPayment = new Model_Payment();
        $modelBooking = new Model_Booking();

        foreach ($invoices as $invoice) {
            $this->updateByBookingId($invoice['booking_id'], array('invoice_type' => 'open'));

            $paid_amount = $modelPayment->getTotalAmount(array('booking_id' => $invoice['booking_id'], 'is_approved' => 'yes'));

            $modelBooking->updateById($invoice['booking_id'], array('paid_amount' => $paid_amount));
        }
    }

    public function cronJobChangeToOpen() {
        $select = $this->getAdapter()->select();

        $select->from(array('inv' => $this->_name));
        $select->joinInner(array('bok' => 'booking'), 'inv.booking_id = bok.booking_id', '');
        $select->where("bok.convert_status = 'invoice'");
        $select->where("inv.invoice_type = 'overdue'");
        $select->where("inv.is_deleted = 0");

        $invoices = $this->getAdapter()->fetchAll($select);

        $modelBookingDueDate = new Model_BookingDueDate();
        $modelDueDate = new Model_DueDate();
        $defaultDueDate = $modelDueDate->getDefaultId();

        foreach ($invoices as $invoice) {
            $bookingDueDate = $modelBookingDueDate->getByBookingId($invoice['booking_id']);
            $dueDate = $modelDueDate->getById($bookingDueDate['due_date_id']);

            $cdate = $invoice['created'];
            $today = time();
            $difference = $today - $cdate;
            if ($difference < 0) {
                $difference = 0;
            }

            $ago = (int) floor($difference / 60 / 60 / 24);
            $due = empty($dueDate['due_date']) ? $defaultDueDate['due_date'] : $dueDate['due_date'];

            if ($ago <= $due) {
                $this->updateById($invoice['id'], array('invoice_type' => 'open'));
            }
        }
    }

    public function cronJobReminderOverdueInvoice() {

        //
        // load models
        //
        $modelUser = new Model_User();
        $modelCustomer = new Model_Customer();
        $modelBookingAddress = new Model_BookingAddress();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelBooking = new Model_Booking();

        $selectPayment = $this->getAdapter()->select();
        $selectPayment->from('payment', array('booking_id', 'SUM(`amount`) AS payment_amount'));
        $selectPayment->group("booking_id");

        $selectRefund = $this->getAdapter()->select();
        $selectRefund->from('refund', array('booking_id', 'SUM(`amount`) AS refund_amount'));
        $selectRefund->group("booking_id");

        $select = $this->getAdapter()->select();
        $select->from(array('inv' => $this->_name));
        $select->joinInner(array('bok' => 'booking'), '`inv`.`booking_id` = `bok`.`booking_id`', '');
        $select->joinLeft(array('p' => $selectPayment), '`inv`.`booking_id` = `p`.`booking_id`', '');
        $select->joinLeft(array('r' => $selectRefund), '`inv`.`booking_id` = `r`.`booking_id`', '');
        $select->where("`bok`.`convert_status` = 'invoice'");
        $select->where("`inv`.`invoice_type` = 'overdue'");
        $select->where("`inv`.`is_deleted` = 0");
        $select->where("`inv`.`overdue_reminded` != 'done'");
        $select->where("(IFNULL(`p`.`payment_amount`,0) - IFNULL(`r`.`refund_amount`,0)) < `bok`.`qoute`");

        $invoices = $this->getAdapter()->fetchAll($select);
        $this->fills($invoices, array('booking', 'number_of_due_days', 'due_date'));
		

        foreach ($invoices as $invoice) {

            $booking = $modelBooking->getById($invoice['booking']['booking_id']);
            $this->fill($booking, array('property_type', 'contractors', 'email_contractors', 'name_contractors', 'address', 'customer_commercial_info'));

            $customer = $modelCustomer->getById($booking['customer_id']);
            $modelCustomer->fill($customer, array('customer_commercial_info', 'customer_contacts'));
            $user = $modelUser->getById($booking['created_by']);

            $bookingServices = $modelContractorServiceBooking->getServicesAndTotalServiceBookingQouteAsViewParam($booking['booking_id']);

            // Create pdf
            $view = new Zend_View();
            $view->setScriptPath(APPLICATION_PATH . '/modules/invoices/views/scripts/index');
            $view->bookingServices = $bookingServices['bookingServices'];
            $view->thisBookingServices = $bookingServices['thisBookingServices'];
            $view->priceArray = $bookingServices['priceArray'];
            $view->invoice = $invoice;
            $view->customer = $customer;
            $view->isAccepted = true;
            $view->booking = $booking;
            $view->customerContacts = $customer['customer_contacts'];
            $bodyInvoice = $view->render('invoice.phtml');

            $pdfPath = createPdfPath();
            $destination = $pdfPath['fullDir'] . $invoice['invoice_num'] . '.pdf';
            wkhtmltopdf($bodyInvoice, $destination);

            $template_params = array(
                //invoice
                '{invoice_num}' => $invoice['invoice_num'],
                '{invoice_created}' => date('d/m/Y', $invoice['created']),
                '{due}' => $invoice['due'] > 1 ? $invoice['due'] . ' Days' : $invoice['due'] . ' Day',
                '{due_date}' => $invoice['due_date'] > 1 ? $invoice['due_date'] . ' Days' : $invoice['due_date'] . ' Day',
                '{invoice_view}' => $bodyInvoice,
                //booking
                '{booking_num}' => $booking['booking_num'],
                '{total_without_tax}' => number_format($booking['sub_total'], 2),
                '{gst_tax}' => number_format($booking['gst'], 2),
                '{total_with_tax}' => number_format($booking['qoute'], 2),
                '{paid_amount}' => number_format($booking['paid_amount'], 2),
                '{unpaid_amount}' => number_format($booking['qoute'] - $booking['paid_amount'], 2),
                '{description}' => nl2br($booking['description'] ? $booking['description'] : ''),
                '{booking_created}' => date('d/m/Y', $booking['created']),
                '{booking_created_by}' => ucwords($user['username']),
                '{booking_start}' => date("F j, Y, g:i a", strtotime($booking['booking_start'])),
                '{booking_address}' => get_line_address($modelBookingAddress->getByBookingId($booking['booking_id'])),
                '{service}' => nl2br($modelContractorServiceBooking->getBookingAsText($booking['booking_id'], true)),
                //customer
                '{customer_name}' => get_customer_name($customer),
                '{customer_first_name}' => isset($customer['first_name']) && $customer['first_name'] ? ucwords($customer['first_name']) : '',
                '{customer_last_name}' => isset($customer['last_name']) && $customer['last_name'] ? ' ' . ucwords($customer['last_name']) : '',
                '{customer_contacts}' => nl2br($modelCustomer->getCustomerContacts($booking['customer_id'])),
                '{sender_name}' => ucwords($user['username'])
            );

            $to = array();
			
            if ($customer['email1'] && filter_var($customer['email1'], FILTER_VALIDATE_EMAIL)) {
                $to[] = $customer['email1'];
            }
            if ($customer['email2']&& filter_var($customer['email2'], FILTER_VALIDATE_EMAIL)) {
                $to[] = $customer['email2'];
            }
            if ($customer['email3']&& filter_var($customer['email3'], FILTER_VALIDATE_EMAIL)) {
                $to[] = $customer['email3'];
            }
            
            $email_log = array('reference_id' => $invoice['id'], 'type' => 'invoice');

            $companyId = $booking['company_id'];

            $success = false;

            
            if ($to) {
			$to = implode(',', $to);
			
			$data = array(
                'to' => $to,
                'reply' => array('name' => $user['display_name'], 'email' => $user['email1'],),
                'attachment' => $destination
            );
			try {
					EmailNotification::sendEmail($data, 'reminder_overdue_invoice', $template_params, $email_log, $companyId);
					$overdueReminded = 'done';
					} 
				catch( Zend_Mail_Transport_Exception $e ) {
					$overdueReminded = 'error';
					}
			}
            $this->updateById($invoice['id'], array('overdue_reminded' => $overdueReminded));
        }
    }

    public function cronJobResetOverdueInvoiceReminder() {

        $selectPayment = $this->getAdapter()->select();
        $selectPayment->from('payment', array('booking_id', 'SUM(`amount`) AS payment_amount'));
        $selectPayment->group("booking_id");

        $selectRefund = $this->getAdapter()->select();
        $selectRefund->from('refund', array('booking_id', 'SUM(`amount`) AS refund_amount'));
        $selectRefund->group("booking_id");

        $select = $this->getAdapter()->select();
        $select->from(array('inv' => $this->_name));
        $select->joinInner(array('bok' => 'booking'), '`inv`.`booking_id` = `bok`.`booking_id`', '');
        $select->joinLeft(array('p' => $selectPayment), '`inv`.`booking_id` = `p`.`booking_id`', '');
        $select->joinLeft(array('r' => $selectRefund), '`inv`.`booking_id` = `r`.`booking_id`', '');
        $select->where("`bok`.`convert_status` = 'invoice'");
        $select->where("`inv`.`invoice_type` = 'overdue'");
        $select->where("`inv`.`is_deleted` = 0");
        $select->where("(IFNULL(`p`.`payment_amount`,0) - IFNULL(`r`.`refund_amount`,0)) < `bok`.`qoute`");

        $invoices = $this->getAdapter()->fetchAll($select);

        foreach ($invoices as $invoice) {
            $this->updateById($invoice['id'], array('overdue_reminded' => 'none'));
        }
    }

    public function cronJobResetDailyOverdueInvoiceReminder() {

        $selectPayment = $this->getAdapter()->select();
        $selectPayment->from('payment', array('booking_id', 'SUM(`amount`) AS payment_amount'));
        $selectPayment->group("booking_id");

        $selectRefund = $this->getAdapter()->select();
        $selectRefund->from('refund', array('booking_id', 'SUM(`amount`) AS refund_amount'));
        $selectRefund->group("booking_id");

        $select = $this->getAdapter()->select();
        $select->from(array('inv' => $this->_name));
        $select->joinInner(array('bok' => 'booking'), '`inv`.`booking_id` = `bok`.`booking_id`', '');
        $select->joinLeft(array('p' => $selectPayment), '`inv`.`booking_id` = `p`.`booking_id`', '');
        $select->joinLeft(array('r' => $selectRefund), '`inv`.`booking_id` = `r`.`booking_id`', '');
        $select->where("`bok`.`convert_status` = 'invoice'");
        $select->where("`inv`.`invoice_type` = 'overdue'");
        $select->where("`inv`.`is_deleted` = 0");
        $select->where("(IFNULL(`p`.`payment_amount`,0) - IFNULL(`r`.`refund_amount`,0)) < `bok`.`qoute`");

        $invoices = $this->getAdapter()->fetchAll($select);
        $this->fills($invoices, array('due_date'));

        foreach ($invoices as $invoice) {

            if ($invoice['invoice_type'] != 'closed') {

                $cdate = $invoice['created'];
                $today = time();
                $difference = $today - $cdate;

                if ($difference < 0) {
                    $difference = 0;
                }

                $ago = (int) floor($difference / 60 / 60 / 24);
                $due = $invoice['due_date'];

                $overdue = 0;
                if ($ago > $due) {
                    $overdue = $ago - $due;
                }

                if ($overdue > 30) {
                    $this->updateById($invoice['id'], array('overdue_reminded' => 'none'));
                }
            }
        }
    }

    public function checkIfCanSeeHisOrAssignedInvoice($invoiceId, $userId = 0) {

        if (!$userId) {
            $loggedUser = CheckAuth::getLoggedUser();
            $userId = $loggedUser['user_id'];
        }

        if (CheckAuth::checkCredential(array('canSeeAllInvoice'))) {
            return true;
        }

        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelBooking = new Model_Booking();

        $invoice = $this->getById($invoiceId);
        $booking = $modelBooking->getById($invoice['booking_id']);
        if ($booking['created_by'] == $userId) {
            return true;
        }

        $contractorServiceBookings = $modelContractorServiceBooking->getByBookingId($booking['booking_id']);

        if ($contractorServiceBookings) {
            foreach ($contractorServiceBookings AS $contractorServiceBooking) {
                if ($contractorServiceBooking['contractor_id'] == $userId && $contractorServiceBooking['is_accepted']) {
                    return true;
                }
            }
        }
        return false;
    }

    public function convertToInvoice($bookingId, $addLog = true) {

        $modelBooking = new Model_Booking();
        $modelBookingEstimate = new Model_BookingEstimate();
        $modelBookingDueDate = new Model_BookingDueDate();
        $modelDueDate = new Model_DueDate();

        $bookingInvoice = $this->getByBookingId($bookingId);

        $db_params = array();
        $db_params['booking_id'] = $bookingId;
        $db_params['invoice_type'] = 'open';

        $invoiceId = 0;
        if ($bookingInvoice) {
            $invoiceId = $bookingInvoice['id'];
            //add User Log
            $modelLogUser = new Model_LogUser();
            $modelLogUser->addUserLogEvent($bookingId, $this->_name, 'added');
            $this->updateById($invoiceId, $db_params);
        } else {
            $db_params['created'] = time();
            $invoiceId = $this->insert($db_params);
        }

        $modelBooking->updateById($bookingId, array('convert_status' => 'invoice'), $addLog);

        $bookingEstimate = $modelBookingEstimate->getNotDeletedByBookingId($bookingId);
        if ($bookingEstimate) {
            $modelBookingEstimate->updateById($bookingEstimate['id'], array('estimate_type' => 'booking'));
        }

        //
        // Due Date
        //
        $dueDateId = $modelDueDate->getDefaultId();

        $db_params = array();
        $db_params['booking_id'] = $bookingId;
        $db_params['due_date_id'] = $dueDateId['id'];

        $bookingDueDate = $modelBookingDueDate->getByBookingId($bookingId);
        if ($bookingDueDate) {
            $modelBookingDueDate->updateById($bookingDueDate['id'], $db_params);
        } else {
            $modelBookingDueDate->insert($db_params);
        }

        return $invoiceId;
    }

    public function updateFullTextSearch($invoiceId) {

        $invoice = $this->getById($invoiceId);
        $bookingId = $invoice['booking_id'];
        $fullTextSearch = array();


        // invoice        
        $fullTextSearch[] = trim($invoice['invoice_num']);
        $fullTextSearch[] = trim($invoice['invoice_type']);

        // booking
        $model_Booking = new Model_Booking();
        $modelBookingStatus = new Model_BookingStatus();
        $booking = $model_Booking->getById($bookingId);
        $fullTextSearch[] = trim($booking['booking_num']);
        $fullTextSearch[] = trim($booking['title']);
        $fullTextSearch[] = trim($booking['description']);
        $status = $modelBookingStatus->getById($booking['status_id']);
        $fullTextSearch[] = trim($status['name']);


        // address
        $modelBookingAddress = new Model_BookingAddress();
        $bookingAddress = $modelBookingAddress->getByBookingId($bookingId);
        $fullTextSearch[] = trim(get_line_address($bookingAddress));

        //customer
        $modelCustomer = new Model_Customer();
        $fullTextCustomer = $modelCustomer->getFullTextCustomerContacts($booking['customer_id']);
        $fullTextSearch[] = trim($fullTextCustomer);

        // services
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $fullTextContractorServiceBooking = $modelContractorServiceBooking->getFullTextContractorServiceBooking($bookingId);
        if ($fullTextContractorServiceBooking) {
            $fullTextSearch[] = trim($fullTextContractorServiceBooking);
        }

        //inquiry
        if (isset($booking['original_inquiry_id']) && $booking['original_inquiry_id']) {
            $modelInquiry = new Model_Inquiry();
            $Inquiry = $modelInquiry->getById($booking['original_inquiry_id']);
            if ($Inquiry['inquiry_num']) {
                $fullTextSearch[] = trim($Inquiry['inquiry_num']);
            }
        }

        //estimate
        $modelBookingEstimate = new Model_BookingEstimate();
        $estimate = $modelBookingEstimate->getByBookingId($bookingId);
        if (isset($estimate['estimate_num']) && $estimate['estimate_num']) {
            $fullTextSearch[] = trim($estimate['estimate_num']);
        }

        //complaint
        $model_Complaint = new Model_Complaint();
        $complaints = $model_Complaint->getFullTextComplaintByBookingId($bookingId);
        if ($complaints) {
            $fullTextSearch[] = trim($complaints);
        }

        $data = array();
        $data['full_text_search'] = implode(' ', $fullTextSearch);

        $this->updateById($invoiceId, $data, false);
    }

    public function deleteRelatedInvoice($id) {

        //delete data from invoice_label
        $this->getAdapter()->delete('invoice_label', "invoice_id = '{$id}'");

        //delete data from invoice_discussion
        $this->getAdapter()->delete('invoice_discussion', "invoice_id = '{$id}'");
    }

    public function checkBeforeDeleteInvoiceByBookingId($bookingId, &$tables = array()) {

        $sucsess = true;

        $select_payment = $this->getAdapter()->select();
        $select_payment->from('payment', 'COUNT(*)');
        $select_payment->where("booking_id = {$bookingId}");
        $count_payment = $this->getAdapter()->fetchOne($select_payment);

        if ($count_payment) {
            $tables[] = 'payment';
            $sucsess = false;
        }

        $select_refund = $this->getAdapter()->select();
        $select_refund->from('refund', 'COUNT(*)');
        $select_refund->where("booking_id = {$bookingId}");
        $count_refund = $this->getAdapter()->fetchOne($select_refund);

        if ($count_refund) {
            $tables[] = 'refund';
            $sucsess = false;
        }

        $select_booking_contractor_payment = $this->getAdapter()->select();
        $select_booking_contractor_payment->from('booking_contractor_payment', 'COUNT(*)');
        $select_booking_contractor_payment->where("booking_id = {$bookingId}");
        $count_booking_contractor_payment = $this->getAdapter()->fetchOne($select_booking_contractor_payment);

        if ($count_booking_contractor_payment) {
            $tables[] = 'booking_contractor_payment';
            $sucsess = false;
        }

        $select_contractor_share_booking = $this->getAdapter()->select();
        $select_contractor_share_booking->from('contractor_share_booking', 'COUNT(*)');
        $select_contractor_share_booking->where("booking_id = {$bookingId}");
        $count_contractor_share_booking = $this->getAdapter()->fetchOne($select_contractor_share_booking);

        if ($count_contractor_share_booking) {
            $tables[] = 'contractor_share_booking';
            $sucsess = false;
        }

        return $sucsess;
    }

    public function getInvoiceViewParam($invoiceId, $isTemp = false) {

        $viewParam = array();

        $invoice = $this->getById($invoiceId);
        if ($invoice) {

            // filling extra data
            $this->fill($invoice, array('booking', 'labels', 'due_date', 'multiple_days'));

            // load model
            $modelContractorServiceBooking = new Model_ContractorServiceBooking();
            $modelContractorServiceBookingTemp = new Model_ContractorServiceBookingTemp();
            $modelBooking = new Model_Booking();
            $modelCustomer = new Model_Customer();

            $customer = $modelCustomer->getById($invoice['booking']['customer_id']);
            $modelCustomer->fill($customer, array('customer_commercial_info', 'customer_contacts'));

            // $thisBookingServices : to put all service for this booking 
            $thisBookingServices = array();

            // $priceArray : to put all price and service for this booking 
            $priceArray = array();

            $contractorServiceBookings = $modelContractorServiceBooking->getByBookingId($invoice['booking_id']);
            $contractorServiceBookingsTemp = $modelContractorServiceBookingTemp->getByBookingId($invoice['booking_id']);

            $bookingServices = array();
            if ($contractorServiceBookingsTemp && !$modelBooking->checkCanEditBookingDetails($invoice['booking_id']) && !$isTemp) {
                $bookingServices = $contractorServiceBookingsTemp;
                foreach ($bookingServices as $bookingService) {

                    $serviceId = $bookingService['service_id'];
                    $clone = $bookingService['clone'];
                    $bookingId = $bookingService['booking_id'];

                    $service_and_clone = $serviceId . ($clone ? '_' . $clone : '');

                    $thisBookingServices[] = $service_and_clone;

                    $priceArray[$service_and_clone] = $modelContractorServiceBookingTemp->getTotalServiceBookingQoute($bookingId, $serviceId, $clone);
                }
            } elseif ($contractorServiceBookings) {
                $bookingServices = $contractorServiceBookings;
                foreach ($bookingServices as $bookingService) {

                    $serviceId = $bookingService['service_id'];
                    $clone = $bookingService['clone'];
                    $bookingId = $bookingService['booking_id'];

                    $service_and_clone = $serviceId . ($clone ? '_' . $clone : '');

                    $thisBookingServices[] = $service_and_clone;

                    $priceArray[$service_and_clone] = $modelContractorServiceBooking->getTotalServiceBookingQoute($bookingId, $serviceId, $clone);
                }
            }


            $viewParam['isAccepted'] = $modelBooking->checkBookingIfAccepted($invoice['booking_id']);
            $viewParam['customerContacts'] = $customer['customer_contacts'];
            $viewParam['customer'] = $customer;
            $viewParam['invoice'] = $invoice;
            $viewParam['bookingServices'] = $bookingServices;
            $viewParam['thisBookingServices'] = $thisBookingServices;
            $viewParam['priceArray'] = $priceArray;
            $viewParam['isTemp'] = $isTemp;
        }
        return $viewParam;
    }

}