<?php

class Model_DailyReport extends Zend_Db_Table_Abstract {

    protected $_name = 'daily_report';

    private function getWheresAndJoinsByFilters($filters, $isCronJob = false) {
        $wheres = array();
        $joinInner = array();

        if (!$isCronJob) {
            if (!isset($filters['company_id'])) {
                $filters['company_id'] = CheckAuth::getCompanySession();
            }
        }

        if (!isset($filters['status_id'])) {
            $filters['status_id'] = 0;
        }

        if ($filters) {
            if (!empty($filters['company_id'])) {
                $wheres['company_id'] = ("company_id = {$filters['company_id']}");
            }

            if (!empty($filters['type'])) {
                $type = $this->getAdapter()->quote($filters['type']);
                $wheres['type'] = ("type LIKE {$type}");
            }

            if (isset($filters['status_id'])) {
                $status_id = (int) $filters['status_id'];
                $wheres['status_id'] = ("status_id = {$status_id}");
            }

            if (!empty($filters['start_between'])) {
                $startBetween = strtotime($filters['start_between']);
                $endBetween = !empty($filters['end_between']) ? strtotime($filters['end_between']) : time();
                $wheres['date_between'] = ("date between '" . $startBetween . "' and '" . $endBetween . "'");
            }
        }

        return array('wheres' => $wheres, 'joinInner' => $joinInner);
    }

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {

        $joinInner = array();
        $select = $this->getAdapter()->select();
        $select->from(array('bok_log' => $this->_name));
        $select->order($order);


        $wheresAndJoins = $this->getWheresAndJoinsByFilters($filters);

        $joinInner = $wheresAndJoins['joinInner'];
        $wheres = $wheresAndJoins['wheres'];

        if ($wheres) {
            foreach ($wheres as $where) {
                $select->where($where);
            }
        }

        if ($joinInner) {
            foreach ($joinInner as $inner) {
                $select->joinInner($inner['name'], $inner['cond'], $inner['cols']);
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

    public function getCountAndTotal($filters = array()) {

        $select = $this->getAdapter()->select();
        $select->from(array('bok' => $this->_name));
        $select->distinct();

        $wheresAndJoins = $this->getWheresAndJoinsByFilters($filters);

        $joinInner = $wheresAndJoins['joinInner'];
        $wheres = $wheresAndJoins['wheres'];

        if ($wheres) {
            foreach ($wheres as $where) {
                $select->where($where);
            }
        }

        if ($joinInner) {
            foreach ($joinInner as $inner) {
                $select->joinInner($inner['name'], $inner['cond'], $inner['cols']);
            }
        }


        $countSql = $this->getAdapter()->select();
        $countSql->from($select, 'SUM(count)');
        $count = $this->getAdapter()->fetchOne($countSql);

        $totalSql = $this->getAdapter()->select();
        $totalSql->from($select, 'SUM(value)');
        $total = $this->getAdapter()->fetchOne($totalSql);

        $result = array();
        $result['count'] = !empty($count) ? $count : 0;
        $result['total'] = !empty($total) ? round($total, 2) : 0;

        return $result;
    }

    /**
     * update table rows according to the assigned id and data
     * 
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "id = '{$id}'");
    }

    /**
     * delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("id = '{$id}'");
    }

    /**
     * get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    public function getTypes() {
        $query = "SHOW COLUMNS FROM {$this->_name} LIKE 'type'";
        $stmt = $this->getAdapter()->query($query);
        $row = $stmt->fetch();
        $row = $row['Type'];
        $regex = "/'(.*?)'/";
        $enum_array = array();
        preg_match_all($regex, $row, $enum_array);
        $enum_fields = $enum_array[1];
        $enums = array();
        foreach ($enum_fields as $value) {
            $enums[$value] = $value;
        }
        return $enums;
    }

    public function getRealData($filters = array()) {

        if (!isset($filters['company_id'])) {
            $filters['company_id'] = CheckAuth::getCompanySession();
        }

        $count = 0;
        $value = 0;

        switch ($filters['type']) {

            case 'booking':

                $filtr = array();
                $filtr['booking_start_between'] = $filters['start_between'];
                $filtr['booking_end_between'] = $filters['end_between'];
                $filtr['convert_status'] = 'booking';
                $filtr['company_id'] = $filters['company_id'];
                if (!empty($filters['status_id'])) {
                    $filtr['status'] = (int) $filters['status_id'];
                }

                //model
                $modelBooking = new Model_Booking();
                $result = $modelBooking->getCronJobCountAndTotalBooking($filtr);

                $count = !empty($result['count']) ? $result['count'] : 0;
                $value = !empty($result['total']) ? $result['total'] : 0;

                break;

            case 'invoice':

                $filtr = array();
                $filtr['booking_start_between'] = $filters['start_between'];
                $filtr['booking_end_between'] = $filters['end_between'];
                $filtr['convert_status'] = 'invoice';
                $filtr['invoice_type'] = 'all_active';
                $filtr['company_id'] = $filters['company_id'];

                //model
                $modelBooking = new Model_Booking();
                $result = $modelBooking->getCronJobCountAndTotalBooking($filtr);

                $count = !empty($result['count']) ? $result['count'] : 0;
                $value = !empty($result['total']) ? $result['total'] : 0;

                break;

            case 'estimate':

                $filtr = array();
                $filtr['booking_start_between'] = $filters['start_between'];
                $filtr['booking_end_between'] = $filters['end_between'];
                $filtr['convert_status'] = 'estimate';
                $filtr['company_id'] = $filters['company_id'];

                //model
                $modelBooking = new Model_Booking();
                $result = $modelBooking->getCronJobCountAndTotalBooking($filtr);

                $count = !empty($result['count']) ? $result['count'] : 0;
                $value = !empty($result['total']) ? $result['total'] : 0;

                break;

            case 'inquiry':

                $filtr = array();
                $filtr['startTimeRange'] = $filters['start_between'];
                $filtr['endTimeRange'] = $filters['end_between'];
                $filtr['company_id'] = $filters['company_id'];

                //model
                $modelInquiry = new Model_Inquiry();
                $result = $modelInquiry->getInquiryCount($filtr);

                $count = !empty($result) ? $result : 0;
                $value = 0;

                break;

            case 'customer':

                $filtr = array();
                $filtr['startTimeRange'] = $filters['start_between'];
                $filtr['endTimeRange'] = $filters['end_between'];
                $filtr['company_id'] = $filters['company_id'];

                //model
                $modelCustomer = new Model_Customer();
                $result = $modelCustomer->getCustomerCount($filtr);

                $count = !empty($result) ? $result : 0;
                $value = 0;

                break;

            case 'complaint':

                $filtr = array();
                $filtr['startTimeRange'] = $filters['start_between'];
                $filtr['endTimeRange'] = $filters['end_between'];
                $filtr['company_id'] = $filters['company_id'];

                //model
                $modelComplaint = new Model_Complaint();
                $result = $modelComplaint->getComplaintCount($filtr);

                $count = !empty($result) ? $result : 0;
                $value = 0;

                break;
        }


        $result = array();
        $result['count'] = !empty($count) ? $count : 0;
        $result['total'] = !empty($value) ? round($value, 2) : 0;

        return $result;
    }
    
    public function getDateForChart($mode, $date) {

        $date = strtotime($date);
        $dateForChart = '';

        switch ($mode) {
            case 'yearly':
                $dateForChart = date('Y', $date);
                break;

            case 'monthly':
                $dateForChart = date('F Y', $date);
                break;

            case 'weekly':
                $dateForChart = date('F j, Y', $date);
                break;

            case 'daily':
                $dateForChart = date('F j, Y', $date);
                break;
        }

        return $dateForChart;
    }

    public function chartArray($results, $by = 'count') {

        if (!in_array($by, array('count', 'total'))) {
            $by = 'count';
        }

        $chartArray = '[';
        if ($results) {
            $chartArray .= "['Year', 'Real', 'Expected'],";

            $data = array();
            foreach ($results as $result) {
                $data[] = "['{$result['date']}', {$result['real'][$by]}, {$result['expected'][$by]}]";
            }

            $chartArray .= implode(',', $data);
        }
        $chartArray .= ']';

        return $chartArray;
    }

    public function cronJobDailyReport() {

        $yesterday = getTimePeriodByName('yesterday');

        //model
        $modelBooking = new Model_Booking();
        $modelInquiry = new Model_Inquiry();
        $modelCustomer = new Model_Customer();
        $modelComplaint = new Model_Complaint();
        $modelCompanies = new Model_Companies();
        $modelBookingStatus = new Model_BookingStatus();

        //companies
        $companies = $modelCompanies->getCronJobCompanies();

        //types
        $types = $this->getTypes();

        foreach ($companies as $company) {
            foreach ($types as $type) {
                switch ($type) {

                    case 'booking':

                        $filters = array();
                        $filters['booking_start_between'] = $yesterday['start'];
                        $filters['booking_end_between'] = $yesterday['end'];
                        $filters['convert_status'] = 'booking';
                        $filters['company_id'] = $company['company_id'];

                        $result = $modelBooking->getCronJobCountAndTotalBooking($filters);

                        $dbParams = array();
                        $dbParams['count'] = !empty($result['count']) ? $result['count'] : 0;
                        $dbParams['value'] = !empty($result['total']) ? $result['total'] : 0;
                        $dbParams['date'] = strtotime($yesterday['start']);
                        $dbParams['type'] = 'booking';
                        $dbParams['status_id'] = 0;
                        $dbParams['company_id'] = $company['company_id'];

                        $this->insert($dbParams);

                        //Booking Status
                        $allBookingStatus = $modelBookingStatus->getAll(array('company_id' => $company['company_id']));
                        if ($allBookingStatus) {
                            foreach ($allBookingStatus as $bookingStatus) {

                                $filters = array();
                                $filters['booking_start_between'] = $yesterday['start'];
                                $filters['booking_end_between'] = $yesterday['end'];
                                $filters['convert_status'] = 'booking';
                                $filters['company_id'] = $company['company_id'];
                                $filters['status'] = $bookingStatus['booking_status_id'];

                                $result = $modelBooking->getCronJobCountAndTotalBooking($filters);

                                $dbParams = array();
                                $dbParams['count'] = !empty($result['count']) ? $result['count'] : 0;
                                $dbParams['value'] = !empty($result['total']) ? $result['total'] : 0;
                                $dbParams['date'] = strtotime($yesterday['start']);
                                $dbParams['type'] = 'booking';
                                $dbParams['status_id'] = $bookingStatus['booking_status_id'];
                                $dbParams['company_id'] = $company['company_id'];

                                $this->insert($dbParams);
                            }
                        }

                        break;

                    case 'invoice':

                        $filters = array();
                        $filters['booking_start_between'] = $yesterday['start'];
                        $filters['booking_end_between'] = $yesterday['end'];
                        $filters['convert_status'] = 'invoice';
                        $filters['invoice_type'] = 'all_active';
                        $filters['company_id'] = $company['company_id'];

                        $result = $modelBooking->getCronJobCountAndTotalBooking($filters);

                        $dbParams = array();
                        $dbParams['count'] = !empty($result['count']) ? $result['count'] : 0;
                        $dbParams['value'] = !empty($result['total']) ? $result['total'] : 0;
                        $dbParams['date'] = strtotime($yesterday['start']);
                        $dbParams['type'] = 'invoice';
                        $dbParams['status_id'] = 0;
                        $dbParams['company_id'] = $company['company_id'];

                        $this->insert($dbParams);

                        break;

                    case 'estimate':

                        $filters = array();
                        $filters['booking_start_between'] = $yesterday['start'];
                        $filters['booking_end_between'] = $yesterday['end'];
                        $filters['convert_status'] = 'estimate';
                        $filters['company_id'] = $company['company_id'];

                        $result = $modelBooking->getCronJobCountAndTotalBooking($filters);

                        $dbParams = array();
                        $dbParams['count'] = !empty($result['count']) ? $result['count'] : 0;
                        $dbParams['value'] = !empty($result['total']) ? $result['total'] : 0;
                        $dbParams['date'] = strtotime($yesterday['start']);
                        $dbParams['type'] = 'estimate';
                        $dbParams['status_id'] = 0;
                        $dbParams['company_id'] = $company['company_id'];

                        $this->insert($dbParams);

                        break;

                    case 'inquiry':

                        $filters = array();
                        $filters['startTimeRange'] = $yesterday['start'];
                        $filters['endTimeRange'] = $yesterday['end'];
                        $filters['company_id'] = $company['company_id'];

                        $result = $modelInquiry->getInquiryCount($filters);

                        $dbParams = array();
                        $dbParams['count'] = !empty($result) ? $result : 0;
                        $dbParams['value'] = 0;
                        $dbParams['date'] = strtotime($yesterday['start']);
                        $dbParams['type'] = 'inquiry';
                        $dbParams['status_id'] = 0;
                        $dbParams['company_id'] = $company['company_id'];

                        $this->insert($dbParams);

                        break;

                    case 'customer':

                        $filters = array();
                        $filters['startTimeRange'] = $yesterday['start'];
                        $filters['endTimeRange'] = $yesterday['end'];
                        $filters['company_id'] = $company['company_id'];

                        $result = $modelCustomer->getCustomerCount($filters);

                        $dbParams = array();
                        $dbParams['count'] = !empty($result) ? $result : 0;
                        $dbParams['value'] = 0;
                        $dbParams['date'] = strtotime($yesterday['start']);
                        $dbParams['type'] = 'customer';
                        $dbParams['status_id'] = 0;
                        $dbParams['company_id'] = $company['company_id'];

                        $this->insert($dbParams);

                        break;

                    case 'complaint':

                        $filters = array();
                        $filters['startTimeRange'] = $yesterday['start'];
                        $filters['endTimeRange'] = $yesterday['end'];
                        $filters['company_id'] = $company['company_id'];

                        $result = $modelComplaint->getComplaintCount($filters);

                        $dbParams = array();
                        $dbParams['count'] = !empty($result) ? $result : 0;
                        $dbParams['value'] = 0;
                        $dbParams['date'] = strtotime($yesterday['start']);
                        $dbParams['type'] = 'complaint';
                        $dbParams['status_id'] = 0;
                        $dbParams['company_id'] = $company['company_id'];

                        $this->insert($dbParams);

                        break;
                }
            }
        }
    }

}