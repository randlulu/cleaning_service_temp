<?php

class Model_InvoiceDiscussion extends Zend_Db_Table_Abstract {

    protected $_name = 'invoice_discussion';

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        
        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'changed');
        
        return parent::update($data, "discussion_id = '{$id}'");
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("discussion_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get all the recent discussions that the user are able to see
     *  
     * @return array  
     */
    public function getRecentDiscussion() {

        $loggedUser = CheckAuth::getLoggedUser();
        $userId = $loggedUser['user_id'];

        $select = $this->getAdapter()->select();

        $select->distinct();

        $select->from(array('id' => $this->_name));

        $select->joinInner(array('csb' => 'contractor_service_booking'), 'id.invoice_id = csb.booking_id', 'csb.contractor_id');

        if (!CheckAuth::checkCredential(array('canSeeAllInvoiceDiscussion'))) {
            if (CheckAuth::checkCredential(array('canSeeOnlyHisInvoiceDiscussion'))) {
                if (CheckAuth::checkCredential(array('canSeeAssignedInvoiceDiscussion'))) {
                    $select->where("id.user_id = {$userId} OR csb.contractor_id = {$userId}");
                } else {
                    $select->where("id.user_id = {$userId}");
                }
            }
        }

        $select->order("created DESC");

        $select->limit(20);

        return $this->getAdapter()->fetchall($select);
    }

    /**
     * get table row according to the assigned estimateId
     * 
     * @param int $id
     * @return array
     */
    public function getByInvoiceId($id, $order = 'asc') {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order("created {$order}");
        $select->where("invoice_id = '{$id}'");

        return $this->getAdapter()->fetchAll($select);
    }
    
    public function insert(array $data) {

        $id = parent::insert($data);
        
        /**
         * add User Log 
         */
        $modelLogUser = new Model_LogUser();
        $modelLogUser->addUserLogEvent($id, $this->_name, 'added');
        
        return $id;
    }

}