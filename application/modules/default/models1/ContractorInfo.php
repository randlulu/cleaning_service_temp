<?php

class Model_ContractorInfo extends Zend_Db_Table_Abstract {

    protected $_name = 'contractor_info';

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "contractor_info_id = '{$id}'");
    }

    /**
     * delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("contractor_info_id = '{$id}'");
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("contractor_info_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the assigned Contractor Id
     * 
     * @param int $id
     * @return array
     */
    public function getByContractorId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("contractor_id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get extra info for all the rows
     * @param array $row
     * @param array $types
     * @return array
     */
    public function fills(&$rows, $types = array()) {
        foreach ($rows as &$row) {
            $this->fill($row, $types);
        }
    }

    /**
     * get extra info for each row (Ex: getting the category row by c_id, author row by author id ... etc)
     * 
     * @param array $row
     * @param array $types
     * @return array
     */
    public function fill(&$row, $types = array()) {

        if (in_array('contractor_info_by_contractor_id', $types)) {
            $row['contractor_info'] = $this->getByContractorId($row['contractor_id']);
        }
        if (in_array('contractor_info_by_user_id', $types)) {
            $row['contractor_info'] = $this->getByContractorId($row['user_id']);
        }

        return $row;
    }

    public function deleteRelatedContractorInfo($id) {

        //delete data from contractor_owner
        $this->getAdapter()->delete('contractor_owner', "contractor_info_id = '{$id}'");

        //delete data from contractor_employee
        $this->getAdapter()->delete('contractor_employee', "contractor_info_id = '{$id}'");

        //delete data from vehicle
        $this->getAdapter()->delete('vehicle', "contractor_info_id = '{$id}'");
    }

}