<?php

class Model_ContractorServiceBooking extends Zend_Db_Table_Abstract {

    protected $_name = 'contractor_service_booking';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->order($order);

        if ($filters) {
            if (!empty($filters['booking_id'])) {
                $select->where("booking_id = {$filters['booking_id']}");
            }
            if (!empty($filters['contractor_id'])) {
                $select->where("contractor_id = {$filters['contractor_id']}");
            }
            if (!empty($filters['service_id'])) {
                $select->where("service_id = {$filters['service_id']}");
            }

            if (isset($filters['is_accepted'])) {
                if ($filters['is_accepted'] == "all") {
                    $select->where("is_accepted = 0 OR is_accepted = 1");
                } elseif ($filters['is_accepted'] == "accepted") {
                    $select->where("is_accepted = 1");
                } elseif ($filters['is_accepted'] == "not_accepted") {
                    $select->where("is_accepted = 0");
                }
            }

            if (isset($filters['not_accepted_or_rejected'])) {
                $select->where("is_accepted = 0 AND is_rejected = 0");
            }

            if (isset($filters['is_rejected'])) {
                if ($filters['is_rejected'] == "all") {
                    $select->where("is_rejected = 0 OR is_rejected = 1");
                } elseif ($filters['is_rejected'] == "rejected") {
                    $select->where("is_rejected = 1");
                } elseif ($filters['is_rejected'] == "not_rejected") {
                    $select->where("is_rejected = 0");
                }
            }
        }
        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateById($id, $data) {
        $id = (int) $id;

        if (!empty($data['contractor_id']) || !empty($data['service_id'])) {

            $oldData = $this->getById($id);

            if (!empty($data['contractor_id']) && $oldData['contractor_id'] != $data['contractor_id']) {
                $data['is_accepted'] = 0;
                $data['is_rejected'] = 0;
            }

            if (!empty($data['service_id']) && $oldData['service_id'] != $data['service_id']) {
                $data['is_accepted'] = 0;
                $data['is_rejected'] = 0;
            }
        }

        $success = parent::update($data, "id = '{$id}'");

        /**
         * get all record and insert it in log
         */
        $modelContractorServiceBookingLog = new Model_ContractorServiceBookingLog();
        $modelContractorServiceBookingLog->addContractorServiceBookingLog($id);

        return $success;
    }

    public function insert(array $data) {
        $id = parent::insert($data);
        
        /**
         * get all record and insert it in log
         */
        $modelContractorServiceBookingLog = new Model_ContractorServiceBookingLog();
        $modelContractorServiceBookingLog->addContractorServiceBookingLog($id);
        
        return $id;
    }

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateByServiceId($id, $data) {
        $id = (int) $id;
        return parent::update($data, "service_id = '{$id}'");
    }

    /**
     * update table row according to the assigned id
     * 
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function updateByBookingId($id, $data) {
        $id = (int) $id;
        return parent::update($data, "booking_id = '{$id}'");
    }

    /**
     * delete table row according to the assigned id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("id= '{$id}'");
    }

    /**
     * get table row according to the assigned id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id= '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * delete table row according to the assigned bookingId
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteBybookingId($id) {
        $id = (int) $id;
        return parent::delete("booking_id= '{$id}'");
    }

    /**
     * get table row according to the assigned BookingId
     * 
     * @param int $bookingId
     * @return array
     */
    public function getByBookingId($bookingId) {
        $bookingId = (int) $bookingId;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_id= '{$bookingId}'");

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * get table row according to the assigned contractor Id
     * 
     * @param int $contractorId
     * @return array
     */
    public function getByContractorId($contractorId) {
        $contractorId = (int) $contractorId;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("contractor_id= '{$contractorId}'");

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * get the acceptrd services in the booking of the logged user
     *
     * @param int $bookingId
     * @return array 
     */
    public function getByBookingIdAndContractorId($bookingId, $contractorId) {
        $bookingId = (int) $bookingId;
        $contractorId = (int) $contractorId;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_id= '{$bookingId}'");
        $select->where("contractor_id= '{$contractorId}'");

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * get By Booking And Contractor Not Rejected Bookings
     * 
     * @param int $bookingId
     * @param int $contractorId
     * @return array 
     */
    public function getNotRejectedOrAcceptedBookings($bookingId, $contractorId) {
        $bookingId = (int) $bookingId;
        $contractorId = (int) $contractorId;

        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("contractor_id= '{$contractorId}' AND booking_id='{$bookingId}' AND is_accepted = 0 AND is_rejected = 0");

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * get By Booking And Contractor Not Rejected Bookings
     * 
     * @param int $bookingId
     * @param int $contractorId
     * @return array 
     */
    public function getNotAcceptedBookings($bookingId, $contractorId) {
        $bookingId = (int) $bookingId;
        $contractorId = (int) $contractorId;

        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("contractor_id= '{$contractorId}' AND booking_id='{$bookingId}' AND is_accepted = 0");

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * get table row according to the assigned BookingId Service and Clone
     * 
     * @param int $bookingId
     * @param int $service_id
     * @param int $clone
     * @return array
     */
    public function getByBookingAndServiceAndClone($bookingId, $service_id, $clone) {
        $bookingId = (int) $bookingId;
        $service_id = (int) $service_id;
        $clone = (int) $clone;

        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("service_id='{$service_id}' AND booking_id='{$bookingId}' AND clone='{$clone}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * get table row according to the assigned BookingId Service and Contractor
     * 
     * @param int $bookingId
     * @param int $service_id
     * @param int $clone
     * @return array
     */
    public function getByBookingAndServiceAndCloneAndContractor($bookingId, $service_id, $clone, $contractorId) {
        $bookingId = (int) $bookingId;
        $service_id = (int) $service_id;
        $contractorId = (int) $contractorId;
        $clone = (int) $clone;

        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("service_id='{$service_id}' AND booking_id='{$bookingId}' AND clone='{$clone}' AND contractor_id='{$contractorId}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     * delete table row according to the assigned BookingId Service and Clone
     * 
     * @param int $bookingId
     * @param int $service_id
     * @param int $clone
     * @return boolean 
     */
    public function deleteByBookingAndServiceAndClone($bookingId, $service_id, $clone) {
        $bookingId = (int) $bookingId;
        $service_id = (int) $service_id;
        $clone = (int) $clone;

        return $this->delete("service_id='{$service_id}' AND booking_id='{$bookingId}' AND clone='{$clone}'");
    }

    /**
     * delete table row according to the assigned BookingId Services 
     * 
     * @param int $bookingId
     * @param array $services
     * @return boolean 
     */
    public function deleteByBookingIdAndServices($bookingId, $services = array()) {
        $bookingId = (int) $bookingId;
        $oldServices = $this->getByBookingId($bookingId);
        $modelServiceAttributeValue = new Model_ServiceAttributeValue();

        if (!empty($oldServices)) {
            foreach ($oldServices as &$oldService) {
                $serviceId = $oldService['service_id'];
                $clone = $oldService['clone'];

                $oldService = $serviceId . '_' . $clone;
            }
        }

        if (!empty($services)) {
            foreach ($services as &$service) {
                $serviceId = $service['service_id'];
                $clone = $service['clone'];

                $service = $serviceId . '_' . $clone;
            }
        }

        //
        //delete
        //
        $toDeleteServices = array_diff($oldServices, $services);
        if ($toDeleteServices) {

            foreach ($toDeleteServices as $toDeleteService) {

                $serviceAndClone = explode('_', $toDeleteService);
                $serviceId = (int) (isset($serviceAndClone[0]) ? $serviceAndClone[0] : 0);
                $clone = (int) (isset($serviceAndClone[1]) && $serviceAndClone[1] ? $serviceAndClone[1] : 0);

                //delete By Service And Booking And Clone
                $this->deleteByBookingAndServiceAndClone($bookingId, $serviceId, $clone);

                // Delete Attribute Value
                $modelServiceAttributeValue->deleteByBookingIdServiceIdAndClone($bookingId, $serviceId, $clone);
            }
        }
    }

    /*
     * setServicesToBooking
     */

    public function setServicesToBooking($bookingId, $services = array()) {

        //Get Zend Request
        $request = Zend_Controller_Front::getInstance()->getRequest();

        //
        // delete Services not in the list
        //
        $bookingId = (int) $bookingId;
        $allServices = array();
        if (!empty($services)) {
            foreach ($services AS $service) {
                $serviceAndClone = explode('_', $service);
                $serviceId = (int) (isset($serviceAndClone[0]) ? $serviceAndClone[0] : 0);
                $clone = (int) (isset($serviceAndClone[1]) && $serviceAndClone[1] ? $serviceAndClone[1] : 0);
                $contractorId = (int) $request->getParam('contractor_' . $serviceId . ($clone ? '_' . $clone : ''));
                $considerMinPrice = (int) $request->getParam('consider_min_price_' . $serviceId . ($clone ? '_' . $clone : ''));

                $allServices[] = array(
                    'service_id' => $serviceId,
                    'clone' => $clone,
                    'contractor_id' => $contractorId,
                    'consider_min_price' => $considerMinPrice,
                    'booking_id' => $bookingId
                );
            }
        }

        $this->deleteByBookingIdAndServices($bookingId, $allServices);

        //
        // assign the Services to Booking
        //
        foreach ($allServices as $allService) {
            $this->assignServicesToBooking($allService);
        }
    }

    /*
     * assignServicesToBooking
     */

    public function assignServicesToBooking($params) {
        
        $bookingServicelink = $this->getByBookingAndServiceAndClone($params['booking_id'], $params['service_id'], $params['clone']);

        $loggedUser = CheckAuth::getLoggedUser();
        if ('contractor' == CheckAuth::getRoleName()) {
            $params['contractor_id'] = $loggedUser['user_id'];
        }
        if ($loggedUser['user_id'] == $params['contractor_id']) {
            $params['is_accepted'] = 1;
        }

        if (!$bookingServicelink) {
            $returnId = $this->insert($params);
        } else {
            $this->updateById($bookingServicelink['id'], $params);
            $returnId = $bookingServicelink['id'];
        }

        $modelServiceAttributeValue = new Model_ServiceAttributeValue();
        $modelServiceAttributeValue->addAttributeByBookingIdAndServiceIdAndClone($params['booking_id'], $params['service_id'], $params['clone']);
        return $returnId;
    }

    /**
     * get the acceptrd services in the booking of the logged user
     *
     * @param int $bookingId
     * @return array 
     */
    public function getContractorServicesByBookingId($bookingId) {
        $bookingId = (int) $bookingId;

        $loggedUser = CheckAuth::getLoggedUser();
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("booking_id= '{$bookingId}'");

        if (!CheckAuth::checkCredential(array('canSeeAllServices'))) {
            if (CheckAuth::checkCredential(array('canSeeOnlyHisServices'))) {
                $select->where("contractor_id = '{$loggedUser['user_id']}'");
            }
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * get count of tables rows to the assigned filters
     * 
     * @param array $filters
     * @return array
     */
    public function getServicesCount($filters = array()) {


        $loggedUser = CheckAuth::getLoggedUser();
        $companyId = CheckAuth::getCompanySession();

        $select = $this->getAdapter()->select();
        $select->from(array('csb' => $this->_name));

        $select->joinInner(array('b' => 'booking'), 'b.booking_id = csb.booking_id', '');
        $select->where("b.company_id = {$companyId}");
        $select->where('b.is_deleted = 0');

        if (!CheckAuth::checkCredential(array('canSeeAllServices'))) {
            if (CheckAuth::checkCredential(array('canSeeOnlyHisServices'))) {
                $select->where("contractor_id = '{$loggedUser['user_id']}'");
            }
        }

        if ($filters) {

            if (!empty($filters['is_accepted'])) {
                $select->where("csb.is_accepted = '{$filters['is_accepted']}'");
            }

            if (!empty($filters['is_rejected'])) {
                $select->where("csb.is_rejected = '{$filters['is_rejected']}'");
            }

            if (!empty($filters['NotAcceptedAndRejected'])) {
                $select->where("csb.is_accepted = 0 AND csb.is_rejected = 0");
            }
            if (!empty($filters['created_by'])) {
                $userId = (int) $filters['created_by'];
                $select->where("b.created_by = '{$userId}'");
            }
        }

        $sql = $this->getAdapter()->select();
        $sql->from($select, 'COUNT(*)');

        return $this->getAdapter()->fetchOne($sql);
    }

    public function getTotalBookingQoute($bookingId) {
        //
        // load models
        //
        $modelServices = new Model_Services();

        //
        //get all service for this booking 
        //
        $bookingServices = $this->getByBookingId($bookingId);

        $totalQoute = 0;

        if ($bookingServices) {
            foreach ($bookingServices as $bookingService) {
                $serviceId = $bookingService['service_id'];
                $clone = $bookingService['clone'];
                $considerMinPrice = $bookingService['consider_min_price'];

                $service = $modelServices->getById($serviceId);
                $min_price = $service['min_price'];

                $serviceQoute = $this->getServiceBookingQoute($bookingId, $serviceId, $clone);

                if (($serviceQoute < $min_price) && $considerMinPrice === 1) {
                    $totalQoute = $totalQoute + $min_price;
                } else {
                    $totalQoute = $totalQoute + $serviceQoute;
                }
            }
        }

        return $totalQoute;
    }

    public function getServiceBookingQoute($bookingId, $serviceId, $clone = 0) {

        //
        // load models
        //
        $modelAttributeListValue = new Model_AttributeListValue();
        $modelServiceAttribute = new Model_ServiceAttribute();
        $modelServiceAttributeValue = new Model_ServiceAttributeValue();
        $modelServices = new Model_Services();
        $modelAttributes = new Model_Attributes();

        $uniqid = uniqid();
        $serviceQoute = 0;

        $service = $modelServices->getById($serviceId);
        if (preg_match_all('#\[(.*?)\]#', $service['price_equasion'], $matches)) {
            $param = array();
            foreach ($matches[1] as $attributeVariableName) {
                $attribute = $modelAttributes->getByVariableName($attributeVariableName);

                $serviceAttribute = $modelServiceAttribute->getByAttributeIdAndServiceId($attribute['attribute_id'], $serviceId);

                $value = $modelServiceAttributeValue->getByBookingIdAndServiceAttributeIdAndClone($bookingId, $serviceAttribute['service_attribute_id'], $clone);

                if ($attribute['is_list']) {
                    $attributeListValue = $modelAttributeListValue->getById($value['value']);
                    $param[$attributeVariableName] = $attributeListValue['unit_price'];
                } else {
                    $param[$attributeVariableName] = $value['value'];
                }
            }

            $equasion = $service['price_equasion'];

            //build getPrice function
            createPriceEquasionFunction("getPrice_{$uniqid}", $equasion);

            $serviceQoute = call_user_func("getPrice_{$uniqid}", $param);
        }

        return $serviceQoute;
    }

    public function getPriceEquasionToIOS($serviceId, $clone = 0) {

        //
        // load models
        //
        $modelServices = new Model_Services();
        $modelAttributes = new Model_Attributes();

        $service = $modelServices->getById($serviceId);
        $priceEquasion = $service['price_equasion'];
        if (preg_match_all('#\[(.*?)\]#', $service['price_equasion'], $matches)) {
            foreach ($matches[1] as $attributeVariableName) {
                $attribute = $modelAttributes->getByVariableName($attributeVariableName);

                $name = 'attribute_' . $serviceId . $attribute['attribute_id'] . ($clone ? '_' . $clone : '');

                $priceEquasion = str_replace($attributeVariableName, $name, $priceEquasion);
            }
        }

        $priceEquasion .= '=[total_' . $serviceId . ($clone ? '_' . $clone : '') . ']';

        return $priceEquasion;
    }

    public function getTotalServiceBookingQoute($bookingId, $serviceId, $clone = 0) {

        //
        // load models
        //
        $modelServices = new Model_Services();

        $bookingService = $this->getByBookingAndServiceAndClone($bookingId, $serviceId, $clone);
        $considerMinPrice = $bookingService['consider_min_price'];

        $service = $modelServices->getById($serviceId);
        $min_price = $service['min_price'];

        $serviceQoute = $this->getServiceBookingQoute($bookingId, $serviceId, $clone);

        if (($serviceQoute < $min_price) && $considerMinPrice === 1) {
            $serviceQoute = $min_price;
        }

        return $serviceQoute;
    }

    public function getTotalContractorServicesQoute($bookingId, $contractorId) {

        $servicesQoute = 0;

        $contractorServices = $this->getByBookingIdAndContractorId($bookingId, $contractorId);

        foreach ($contractorServices as $contractorService) {
            $serviceQoute = $this->getTotalServiceBookingQoute($bookingId, $contractorService['service_id'], $contractorService['clone']);
            $servicesQoute = $servicesQoute + $serviceQoute;
        }

        return $servicesQoute;
    }

    public function getContractorsBybookingId($bookingId) {
        $contractors = array();
        $contractorServiceBookings = $this->getByBookingId($bookingId);
        if ($contractorServiceBookings) {
            foreach ($contractorServiceBookings as $contractorServiceBooking) {
                $contractors[$contractorServiceBooking['contractor_id']] = $contractorServiceBooking['contractor_id'];
            }
        }

        return $contractors;
    }

    public function getTotalContractorShare($contractorId, $filters = array()) {

        // Load Model
        $modelRefund = new Model_Refund();
        $modelBooking = new Model_Booking();
        $modelContractorInfo = new Model_ContractorInfo();
        $modelContractorShareBooking = new Model_ContractorShareBooking();

        //Get Contractor Info
        $contractorInfo = $modelContractorInfo->getByContractorId($contractorId);

        // get all booking for contractor
        $filters['contractor_id'] = $contractorId;
        $filters['convert_status'] = 'invoice';
        $bookings = $modelBooking->getAll($filters);

        $totalContractorShare = 0;

        if ($bookings) {
            foreach ($bookings as $booking) {
                $bookingId = $booking['booking_id'];
                //check if the contractor have fixed contractor share
                $contractorShareBooking = $modelContractorShareBooking->getByBookingIdAndContractorId($bookingId, $contractorId);

                if (!empty($contractorShareBooking['contractor_share'])) {
                    $contractorShare = $contractorShareBooking['contractor_share'];
                } else {
                    //get total refund
                    $totalRefund = $modelRefund->getTotalRefund(array('booking_id' => $bookingId, 'is_approved' => 'yes'));
                    //booking Qoute without tax
                    $bookingQouteWithoutTax = $this->getTotalBookingQoute($bookingId);
                    //Contractor Services Qoute
                    $contractorServicesQoute = $this->getTotalContractorServicesQoute($bookingId, $contractorId);

                    if ($booking['call_out_fee']) {
                        $contractorServicesQoute = $this->getCallOutFeeByBookingIdAndContractorId($bookingId);
                    }


                    // if the contractor not pay taxes
                    $contractorShare = $contractorServicesQoute * $contractorInfo['commission'] / 100;

                    // percentage calculator of discount
                    $discount = 0;
                    if ($bookingQouteWithoutTax) {
                        $discount = ($contractorServicesQoute / $bookingQouteWithoutTax) * $booking['total_discount'];
                    } else if ($booking['call_out_fee']) {
                        $discount = ($contractorServicesQoute / $booking['call_out_fee']) * $booking['total_discount'];
                    }

                    // percentage calculator of refund
                    $refund = 0;
                    if ($totalRefund) {
                        if ($bookingQouteWithoutTax) {
                            $refund = ($contractorShare / $bookingQouteWithoutTax) * $totalRefund;
                        } else {
                            $contractors = $this->getContractorsBybookingId($bookingId);
                            $subRefund = 0;
                            if (count($contractors)) {
                                $subRefund = $totalRefund / count($contractors);
                            }
                            $refund = $subRefund * $contractorInfo['commission'] / 100;
                        }
                    }

                    // if the contractor pay taxes
                    if ($contractorInfo['gst']) {
                        $servicesQouteWithTax = ($contractorServicesQoute - $discount) * (1 + get_config('gst_tax'));
                        $contractorShare = $servicesQouteWithTax * $contractorInfo['commission'] / 100;
                    } else {
                        $contractorShare = $contractorShare - $discount;
                    }
                    $contractorShare = $contractorShare - $refund;
                }
                $totalContractorShare += $contractorShare;
            }
        }

        return $totalContractorShare;
    }

    public function getTotalContractorRefund($contractorId, $filters = array(), $isApproved = 'yes') {

        // Load Model
        $modelRefund = new Model_Refund();
        $modelBooking = new Model_Booking();
        $modelContractorInfo = new Model_ContractorInfo();

        //Get Contractor Info
        $contractorInfo = $modelContractorInfo->getByContractorId($contractorId);




        // get all booking for contractor
        $filters['contractor_id'] = $contractorId;
        $filters['convert_status'] = 'invoice';
        $bookings = $modelBooking->getAll($filters);


        $totalContractorRefund = 0;

        if ($bookings) {
            foreach ($bookings as $booking) {
                $bookingId = $booking['booking_id'];

                //get total refund
                $totalRefund = $modelRefund->getTotalRefund(array('booking_id' => $bookingId, 'is_approved' => $isApproved));

                //booking Qoute without tax
                $bookingQouteWithoutTax = $this->getTotalBookingQoute($bookingId);

                // total Contractor Services Qoute
                $contractorServicesQoute = $this->getTotalContractorServicesQoute($bookingId, $contractorId);

                $contractorShare = $contractorServicesQoute * $contractorInfo['commission'] / 100;


                // percentage calculator of refund(must be without tax)
                $refund = 0;
                if ($totalRefund) {
                    if ($bookingQouteWithoutTax) {
                        $refund = ($contractorShare / $bookingQouteWithoutTax) * $totalRefund;
                    } else {
                        $contractors = $this->getContractorsBybookingId($bookingId);
                        $subRefund = 0;
                        if (count($contractors)) {
                            $subRefund = $totalRefund / count($contractors);
                        }
                        $refund = $subRefund * $contractorInfo['commission'] / 100;
                    }
                }

                $totalContractorRefund += $refund;
            }
        }

        return $totalContractorRefund;
    }

    public function getTotalServiceRefund($contractorId, $filters = array()) {

        // Load Model
        $modelRefund = new Model_Refund();
        $modelBooking = new Model_Booking();
        $modelContractorInfo = new Model_ContractorInfo();

        //Get Contractor Info
        $contractorInfo = $modelContractorInfo->getByContractorId($contractorId);

        // get all booking for contractor
        $filters['contractor_id'] = $contractorId;
        $filters['convert_status'] = 'invoice';
        $bookings = $modelBooking->getAll($filters);

        $totalServiceRefund = 0;

        if ($bookings) {
            foreach ($bookings as $booking) {
                $bookingId = $booking['booking_id'];

                //get total refund
                $totalRefund = $modelRefund->getTotalRefund(array('booking_id' => $bookingId, 'is_approved' => 'yes'));

                //booking Qoute without tax
                $bookingQouteWithoutTax = $this->getTotalBookingQoute($bookingId);

                // total Contractor Services Qoute
                $contractorServicesQoute = $this->getTotalContractorServicesQoute($bookingId, $contractorId);


                // percentage calculator of refund(must be without tax)
                $refund = 0;
                if ($totalRefund) {
                    if ($bookingQouteWithoutTax) {
                        $refund = ($contractorServicesQoute / $bookingQouteWithoutTax) * $totalRefund;
                    } else {
                        $contractors = $this->getContractorsBybookingId($bookingId);
                        if (count($contractors)) {
                            $refund = $totalRefund / count($contractors);
                        }
                    }
                }
                $totalServiceRefund += $refund;
            }
        }

        return $totalServiceRefund;
    }

    public function getTotalServiceDiscount($contractorId, $filters = array()) {

        // Load Model
        $modelBooking = new Model_Booking();


        // get all booking for contractor
        $filters['contractor_id'] = $contractorId;
        $filters['convert_status'] = 'invoice';
        $bookings = $modelBooking->getAll($filters);

        $totalServiceDiscount = 0;

        if ($bookings) {
            foreach ($bookings as $booking) {

                //booking Qoute without tax
                $bookingQouteWithoutTax = $this->getTotalBookingQoute($booking['booking_id']);

                // total Contractor Services Qoute
                $contractorServicesQoute = $this->getTotalContractorServicesQoute($booking['booking_id'], $contractorId);

                if ($booking['call_out_fee']) {
                    $contractorServicesQoute = $this->getCallOutFeeByBookingIdAndContractorId($booking['booking_id']);
                }

                // percentage calculator of discount
                $discount = 0;
                if ($bookingQouteWithoutTax) {
                    $discount = ($contractorServicesQoute / $bookingQouteWithoutTax) * $booking['total_discount'];
                } else if ($booking['call_out_fee']) {
                    $discount = ($contractorServicesQoute / $booking['call_out_fee']) * $booking['total_discount'];
                }

                $totalServiceDiscount += $discount;
            }
        }

        return $totalServiceDiscount;
    }

    public function getCallOutFeeByBookingIdAndContractorId($bookingId) {
        $modelBooking = new Model_Booking();

        $booking = $modelBooking->getById($bookingId);

        $call_out_fee = 0;
        if ($booking['call_out_fee']) {
            $contractors = $this->getContractorsBybookingId($bookingId);
            if (count($contractors)) {
                $call_out_fee = ($booking['call_out_fee'] / count($contractors));
            }
        }
        return $call_out_fee;
    }

    public function getTotalAmountServicesInvoicedByContractorId($contractorId, $filters = array()) {
        $filters['convert_status'] = 'invoice';
        return $this->getTotalAmountServicesByContractorIdAndStatusId($contractorId, 0, $filters);
    }

    public function getTotalAmountServicesByContractorIdAndStatusId($contractorId, $statusId = 0, $filters = array()) {

        // Load Model
        $modelBooking = new Model_Booking();
        $modelRefund = new Model_Refund();
        $modelContractorInfo = new Model_ContractorInfo();

        //Get Contractor Info
        $contractorInfo = $modelContractorInfo->getByContractorId($contractorId);

        // get all booking for contractor
        $filters['contractor_id'] = $contractorId;
        if ($statusId) {
            $filters['status'] = $statusId;
        }

        $bookings = $modelBooking->getAll($filters);

        $totalAmountServices = 0;

        if ($bookings) {
            foreach ($bookings as $booking) {

                $bookingId = $booking['booking_id'];

                //booking Qoute without tax
                $bookingQouteWithoutTax = $this->getTotalBookingQoute($bookingId);

                //Contractor Services Qoute
                $contractorServicesQoute = $this->getTotalContractorServicesQoute($bookingId, $contractorId);

                if ($booking['call_out_fee']) {
                    $contractorServicesQoute = $this->getCallOutFeeByBookingIdAndContractorId($bookingId);
                }

                // percentage calculator of discount
                $discount = 0;
                if ($bookingQouteWithoutTax) {
                    $discount = ($contractorServicesQoute / $bookingQouteWithoutTax) * $booking['total_discount'];
                } else if ($booking['call_out_fee']) {
                    $discount = ($contractorServicesQoute / $booking['call_out_fee']) * $booking['total_discount'];
                }

                //calculate percentage  total refund when status is completed or faild
                $totalRefund = $modelRefund->getTotalRefund(array('booking_id' => $bookingId, 'is_approved' => 'yes'));

                $refund = 0;
                if ($totalRefund) {
                    if ($bookingQouteWithoutTax) {
                        $refund = ($contractorServicesQoute / $bookingQouteWithoutTax) * $totalRefund;
                    } else {
                        $contractors = $this->getContractorsBybookingId($bookingId);
                        if (count($contractors)) {
                            $refund = $totalRefund / count($contractors);
                        }
                    }
                }

                $totalAmountServices += ($contractorServicesQoute - $discount) * (1 + get_config('gst_tax')) - $refund;
            }
        }

        return $totalAmountServices;
    }

    public function getUnapprovedBooking() {
        $select = $this->getAdapter()->select();
        $select->from($this->_name, 'booking_id');
        $select->distinct();
        $select->where("is_change = 1");

        return $this->getAdapter()->fetchAll($select);
    }

    public function changeAttributIfFaild($bookingId, $services) {

        // Load Model

        $modelAttributes = new Model_Attributes();
        $modelServiceAttribute = new Model_ServiceAttribute();
        $modelServiceAttributeValue = new Model_ServiceAttributeValue();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();

        foreach ($services as $service) {

            $serviceAndClone = explode('_', $service);
            $serviceId = (int) (isset($serviceAndClone[0]) ? $serviceAndClone[0] : 0);
            $clone = (int) (isset($serviceAndClone[1]) && $serviceAndClone[1] ? $serviceAndClone[1] : 0);

            // Quantity
            $quantityAttribute = $modelAttributes->getByVariableName('Quantity');
            $serviceQuantityAttribute = $modelServiceAttribute->getByAttributeIdAndServiceId($quantityAttribute['attribute_id'], $serviceId);
            $oldServiceQuantityValue = $modelServiceAttributeValue->getByBookingIdAndServiceAttributeIdAndClone($bookingId, $serviceQuantityAttribute['service_attribute_id'], $clone);
            $modelServiceAttributeValue->updateById($oldServiceQuantityValue['service_attribute_value_id'], array('value' => ''));

            // Discount

            $discountAttribute = $modelAttributes->getByVariableName('Discount');
            $serviceDiscountAttribute = $modelServiceAttribute->getByAttributeIdAndServiceId($discountAttribute['attribute_id'], $serviceId);
            $oldServiceDiscountValue = $modelServiceAttributeValue->getByBookingIdAndServiceAttributeIdAndClone($bookingId, $serviceDiscountAttribute['service_attribute_id'], $clone);
            $modelServiceAttributeValue->updateById($oldServiceDiscountValue['service_attribute_value_id'], array('value' => ''));

            // Change Min Price to zero

            $booking_service = $modelContractorServiceBooking->getByBookingAndServiceAndClone($bookingId, $serviceId, $clone);
            $modelContractorServiceBooking->updateById($booking_service['id'], $data = array('consider_min_price' => 0));
        }
    }

    public function getBookingAsText($bookingId, $full = false, $services = array()) {

        //
        // load models
        //
        $modelServices = new Model_Services();
        $modelServiceAttribute = new Model_ServiceAttribute();
        $modelServiceAttributeValue = new Model_ServiceAttributeValue();
        $modelAttributes = new Model_Attributes();
        $modelAttributeType = new Model_AttributeType();
        $modelAttributeListValue = new Model_AttributeListValue();
        $modelUser = new Model_User();
        $modelContractorinfo = new Model_ContractorInfo();

        if (!$services) {
            $services = $this->getByBookingId($bookingId);
        }

        $serviecsAsText = "";

        foreach ($services as $service) {

//            if ($full) {
//                $contractorServiceBooking = $this->getByBookingAndServiceAndClone($bookingId, $service['service_id'], $service['clone']);
//                $contractorInfo = $modelContractorinfo->getByContractorId($contractorServiceBooking['contractor_id']);
//                $userInfo = $modelUser->getById($contractorServiceBooking['contractor_id']);
//
//                $contractorName = ucwords($userInfo['username']) . ucwords($contractorInfo['business_name'] ? " - " . $contractorInfo['business_name'] : '');
//                $serviecsAsText .= 'Technician : ' . $contractorName . "<br>";
//            }

            $serviceId = isset($service['service_id']) ? $service['service_id'] : 0;
            $clone = isset($service['clone']) ? $service['clone'] : 0;

            //get the service
            $serviceById = $modelServices->getById($serviceId);
            $serviecsAsText .= 'Service : ' . $serviceById['service_name'] . "<br>";

            $unit_price = 0;
            $qty = 0;
            //get the service attribute id
            $service_attributes = $modelServiceAttribute->getByServiceId($serviceId);
            foreach ($service_attributes as $service_attribute) {

                $attribute = $modelAttributes->getById($service_attribute['attribute_id']);

                //get the service attribute value
                $attributeValue = $modelServiceAttributeValue->getByBookingIdAndServiceAttributeIdAndClone($bookingId, $service_attribute['service_attribute_id'], $clone);

                switch ($attribute['attribute_name']) {
                    case 'Quantity':
                        $qty = $attributeValue['value'];
                        break;
                    case 'Price':
                        $unit_price = $attributeValue['value'];
                        break;
                    default:
                        if ($full) {
                            $atributetype = $modelAttributeType->getById($attribute['attribute_type_id']);

                            // check if the attribute type is list
                            if ($atributetype['is_list']) {
                                // check if the attribute value is serialized
                                if ($attributeValue['is_serialized_array']) {

                                    $unserializeValues = unserialize($attributeValue['value']);

                                    $values = array();
                                    foreach ($unserializeValues as $unserializeValue) {
                                        $attributeListValue = $modelAttributeListValue->getById($unserializeValue);
                                        $values[] = $attributeListValue['attribute_value'];
                                    }

                                    $value = implode(', ', $values);
                                } else {
                                    $attributeListValue = $modelAttributeListValue->getById($attributeValue['value']);
                                    $value = $attributeListValue['attribute_value'];
                                }
                            } else {
                                $value = $attributeValue['value'];
                            }

                            if (!empty($value)) {

                                //$numberOfSpaces = strlen($attribute['attribute_name']) + 3;
                                //
                                //  $spaces = '';
                                //for ($i = 0; $i < $numberOfSpaces; $i++) {
                                //  $spaces .= ' ';
                                // }
                                //
                                //$value = str_replace("\n", "<br>" . $spaces, $value);

                                $serviecsAsText .= empty($attribute['attribute_name']) ? '' : $attribute['attribute_name'] . " : " . $value . "<br>";
                            }
                        }
                        break;
                }
            }

            $serviecsAsText .= 'Service minimum price : $' . $serviceById['min_price'] . "<br>";

            $unit_price = empty($unit_price) ? 0 : $unit_price;
            $qty = empty($qty) ? 0 : $qty;

            $serviecsAsText .= "Quote : $" . $unit_price . " * (Around) " . $qty . " m2 <br>";

            $total = $this->getTotalServiceBookingQoute($bookingId, $serviceId, $clone);
            $serviecsAsText .= "Total Quote : Around $" . number_format($total, 2) . " + GST<br><br>";
        }

        return $serviecsAsText;
    }

    public function getFullTextContractorServiceBooking($bookingId) {

        //
        // load models
        //
        $modelServices = new Model_Services();
        $modelServiceAttribute = new Model_ServiceAttribute();
        $modelServiceAttributeValue = new Model_ServiceAttributeValue();
        $modelAttributes = new Model_Attributes();
        $modelAttributeType = new Model_AttributeType();
        $modelAttributeListValue = new Model_AttributeListValue();
        $modelUser = new Model_User();
        $modelContractorinfo = new Model_ContractorInfo();

        $services = $this->getByBookingId($bookingId);
        $attribute = $modelAttributes->getByVariableName('Floor');

        $contractorServices = array();
        if ($services) {
            foreach ($services as $service) {


                $contractorServiceBooking = $this->getByBookingAndServiceAndClone($bookingId, $service['service_id'], $service['clone']);
                $contractorInfo = $modelContractorinfo->getByContractorId($contractorServiceBooking['contractor_id']);
                $userInfo = $modelUser->getById($contractorServiceBooking['contractor_id']);


                // get Contractors Name
                if (!empty($userInfo['username'])) {
                    $contractorServices[] = $userInfo['username'];
                }
                if (!empty($contractorInfo['business_name'])) {
                    $contractorServices[] = $contractorInfo['business_name'];
                }

                //get Service Name
                $serviceById = $modelServices->getById($service['service_id']);

                if (!empty($serviceById['service_name'])) {
                    $contractorServices[] = $serviceById['service_name'];
                }

                // get floor type
                $ServiceAttribute = $modelServiceAttribute->getByAttributeIdAndServiceId($attribute['attribute_id'], $service['service_id']);
                $ServiceAttributeValue = $modelServiceAttributeValue->getByBookingIdAndServiceAttributeIdAndClone($bookingId, $ServiceAttribute['service_attribute_id'], $service['clone']);
                $attributeListValue = $modelAttributeListValue->getById($ServiceAttributeValue['value']);
                $floorType = $attributeListValue['attribute_value'];
                if (!empty($floorType)) {
                    $contractorServices[] = $floorType;
                }
            }
        }

        if ($contractorServices) {
            $contractorServices = implode(' ', $contractorServices);
        }

        return $contractorServices;
    }

    public function getBookingForEachContractor($filters = array(), $order = null) {

        // get booking for each contractor, so the booking will duplicate if they have more one contractor
						
        $select = $this->getAdapter()->select();
        $select->from(array('csb' => $this->_name), array('csb.booking_id', 'csb.contractor_id'));
        $select->order($order);
        $select->distinct();

        if ($filters) {
            if (!empty($filters['order_by_booking_start'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'csb.booking_id = bok.booking_id', 'cols' => '');
                $select->order('bok.booking_start ASC');
            }
            if (!empty($filters['booking_id'])) {
                $select->where("csb.booking_id = {$filters['booking_id']}");
            }
            if (!empty($filters['contractor_id'])) {
                $select->where("csb.contractor_id = {$filters['contractor_id']}");
            }
            if (!empty($filters['service_id'])) {
                $select->where("csb.service_id = {$filters['service_id']}");
            }

            if (isset($filters['is_accepted'])) {
                if ($filters['is_accepted'] == "all") {
                    $select->where("csb.is_accepted = 0 OR csb.is_accepted = 1");
                } elseif ($filters['is_accepted'] == "accepted") {
                    $select->where("csb.is_accepted = 1");
                } elseif ($filters['is_accepted'] == "not_accepted") {
                    $select->where("csb.is_accepted = 0");
                }
            }

            if (isset($filters['not_accepted_or_rejected'])) {
                $select->where("csb.is_accepted = 0 AND csb.is_rejected = 0");
            }

            if (isset($filters['is_rejected'])) {
                if ($filters['is_rejected'] == "all") {
                    $select->where("csb.is_rejected = 0 OR csb.is_rejected = 1");
                } elseif ($filters['is_rejected'] == "rejected") {
                    $select->where("csb.is_rejected = 1");
                } elseif ($filters['is_rejected'] == "not_rejected") {
                    $select->where("csb.is_rejected = 0");
                }
            }

            if (!empty($filters['booking_start_between'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'csb.booking_id = bok.booking_id', 'cols' => '');
                $bookingStartBetween = $filters['booking_start_between'];
                $bookingEndBetween = !empty($filters['booking_end_between']) ? $filters['booking_end_between'] : date('Y-m-d H:i:s', time());
                $select->where("bok.booking_start between '" . $bookingStartBetween . "' and '" . $bookingEndBetween . "'");
            }
			///////////By Islam join with booking_contractor_payment
			if (!empty($filters['Inv'])) {
                $joinInner['booking_contractor_payment'] = array('name' => array('bcp' => 'booking_contractor_payment'), 'cond' => 'csb.booking_id = bcp.booking_id', 'cols' => '');
                 //$select->where("bcb.contractor_id = bcp.contractor_id '");
                $select->where("bcp.contractor_invoice_num = '" . $filters['Inv'] . "'");
            }
			
			if (!empty($filters['status_id'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'csb.booking_id = bok.booking_id', 'cols' => '');
                $select->where("bok.status_id = {$filters['status_id']}");
				$select->order('bok.booking_start ASC');
            }
			
			///////////////////
			
			
        }

        if ($joinInner) {
            foreach ($joinInner as $inner) {
                $select->joinInner($inner['name'], $inner['cond'], $inner['cols']);
            }
        }

        return $this->getAdapter()->fetchAll($select);
    }

    public function getBookingsbyServiceIdandAttributeValueId($filters = array(), $order = null) {

        // get booking for each contractor, so the booking will duplicate if they have more one contractor

		
        $select = $this->getAdapter()->select();
        $select->from(array('csb' => $this->_name), array('csb.booking_id', 'csb.contractor_id'));
        $select->order($order);
        $select->distinct();
		$this->getAdapter()->fetchAll($select);
        if ($filters) {
            if (!empty($filters['booking_id'])) {
                $select->where("csb.booking_id = {$filters['booking_id']}");
            }
            if (!empty($filters['contractor_id'])) {
                $select->where("csb.contractor_id = {$filters['contractor_id']}");
            }
            if (!empty($filters['service_id'])) {
                $select->where("csb.service_id = {$filters['service_id']}");
            }

            if (isset($filters['is_accepted'])) {
                if ($filters['is_accepted'] == "all") {
                    $select->where("csb.is_accepted = 0 OR csb.is_accepted = 1");
                } elseif ($filters['is_accepted'] == "accepted") {
                    $select->where("csb.is_accepted = 1");
                } elseif ($filters['is_accepted'] == "not_accepted") {
                    $select->where("csb.is_accepted = 0");
                }
            }

            if (isset($filters['not_accepted_or_rejected'])) {
                $select->where("csb.is_accepted = 0 AND csb.is_rejected = 0");
            }

            if (isset($filters['is_rejected'])) {
                if ($filters['is_rejected'] == "all") {
                    $select->where("csb.is_rejected = 0 OR csb.is_rejected = 1");
                } elseif ($filters['is_rejected'] == "rejected") {
                    $select->where("csb.is_rejected = 1");
                } elseif ($filters['is_rejected'] == "not_rejected") {
                    $select->where("csb.is_rejected = 0");
                }
            }

            if (!empty($filters['booking_start_between'])) {
                $joinInner['booking'] = array('name' => array('bok' => 'booking'), 'cond' => 'csb.booking_id = bok.booking_id', 'cols' => '');
                $bookingStartBetween = $filters['booking_start_between'];
                $bookingEndBetween = !empty($filters['booking_end_between']) ? $filters['booking_end_between'] : date('Y-m-d H:i:s', time());
                $select->where("bok.booking_start between '" . $bookingStartBetween . "' and '" . $bookingEndBetween . "'");
            }
            if (!empty($filters['attribute_value_id'])) {
                $joinInner['service_attribute_value'] = array('name' => array('sav' => 'service_attribute_value'), 'cond' => 'sav.booking_id = csb.booking_id', 'cols' => '');
                $joinInner['service_attribute'] = array('name' => array('sa' => 'service_attribute'), 'cond' => 'sa.service_attribute_id = sav.service_attribute_id', 'cols' => '');
                $joinInner['attribute'] = array('name' => array('a' => 'attribute'), 'cond' => 'sa.attribute_id = a.attribute_id', 'cols' => '');
                $joinInner['attribute_type'] = array('name' => array('at' => 'attribute_type'), 'cond' => 'at.attribute_type_id = a.attribute_type_id', 'cols' => '');

                $attribute_value_id = (int) trim($filters['attribute_value_id']);
                $select->where("sav.value = {$attribute_value_id}");
                $select->where("at.is_list = 1");
            }
        }

        if ($joinInner) {
            foreach ($joinInner as $inner) {
                $select->joinInner($inner['name'], $inner['cond'], $inner['cols']);
            }
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * get the acceptrd services in the booking of the logged user
     *
     * @param int $bookingId
     * @return array 
     */
    public function getServicesAndTotalServiceBookingQouteAsViewParam($bookingId) {
        $bookingServices = $this->getByBookingId($bookingId);
        // $thisBookingServices : to put all service for this booking 
        $thisBookingServices = array();
        // $priceArray : to put all price and service for this booking 
        $priceArray = array();

        if ($bookingServices) {
            foreach ($bookingServices as $bookingService) {

                $serviceId = $bookingService['service_id'];
                $clone = $bookingService['clone'];
                $bookingId = $bookingService['booking_id'];

                $service_and_clone = $serviceId . ($clone ? '_' . $clone : '');

                $thisBookingServices[] = $service_and_clone;

                $priceArray[$service_and_clone] = $this->getTotalServiceBookingQoute($bookingId, $serviceId, $clone);
            }
        }
        $viewParam = array();

        $viewParam['bookingServices'] = $bookingServices;
        $viewParam['thisBookingServices'] = $thisBookingServices;
        $viewParam['priceArray'] = $priceArray;

        return $viewParam;
    }

}

