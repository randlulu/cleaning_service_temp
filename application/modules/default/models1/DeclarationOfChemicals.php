<?php

class Model_DeclarationOfChemicals extends Zend_Db_Table_Abstract {

    protected $_name = 'declaration_of_chemicals';

    /**get table row accourding to filters order and pager
     *
     * @param array $filters
     * @param string $order
     * @param object $pager
     * @return array 
     */
    public function getAll($filters = array(), $order = null, &$pager = null) {
        $select = $this->getAdapter()->select();
        $select->from(array('doc' => $this->_name));
        $select->joinInner(array('ci' => 'contractor_info'), 'ci.contractor_info_id=doc.contractor_info_id', array('ci.contractor_id'));
        $select->joinInner(array('u' => 'user'), 'ci.contractor_id=u.user_id', array('u.username'));
        $select->order($order);

        if ($filters) {
            if (!empty($filters['contractor_info_id'])) {
                $select->where("doc.contractor_info_id = {$filters['contractor_info_id']}");
            }
        }

        if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**update table row accourding to id and data 
     *
     * @param int $id
     * @param array $data
     * @return boolean 
     */
    public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "id = '{$id}'");
    }

    /**
     *delete table row according to the id
     * 
     * @param int $id
     * @return boolean 
     */
    public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("id = '{$id}'");
    }

    /**
     *get table row according to the id
     * 
     * @param int $id
     * @return array 
     */
    public function getById($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("id = '{$id}'");

        return $this->getAdapter()->fetchRow($select);
    }

    /**
     *get table row according to the contractor info id
     * 
     * @param int $id
     * @return array 
     */
    public function getByContractorInfoId($id) {
        $id = (int) $id;
        $select = $this->getAdapter()->select();
        $select->from($this->_name);
        $select->where("contractor_info_id = '{$id}'");

        return $this->getAdapter()->fetchAll($select);
    }
    
}