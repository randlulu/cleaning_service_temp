<?php

function js2PhpTime($jsdate) {
    $ret = 0;
    if (preg_match('@(\d+)/(\d+)/(\d+)\s+(\d+):(\d+)@', $jsdate, $matches) == 1) {
        $ret = mktime($matches[4], $matches[5], 0, $matches[1], $matches[2], $matches[3]);
        //echo $matches[4] ."-". $matches[5] ."-". 0  ."-". $matches[1] ."-". $matches[2] ."-". $matches[3];
    } else if (preg_match('@(\d+)/(\d+)/(\d+)@', $jsdate, $matches) == 1) {
        $ret = mktime(0, 0, 0, $matches[1], $matches[2], $matches[3]);
        //echo 0 ."-". 0 ."-". 0 ."-". $matches[1] ."-". $matches[2] ."-". $matches[3];
    }
    return $ret;
}

function php2JsTime($phpDate) {
    //echo $phpDate;
    //return "/Date(" . $phpDate*1000 . ")/";
    return date("m/d/Y H:i", $phpDate);
}

function php2MySqlTime($phpDate) {
    return date("Y-m-d H:i:s", $phpDate);
}

function mySql2PhpTime($sqlDate) {
    $arr = date_parse($sqlDate);
    return mktime($arr["hour"], $arr["minute"], $arr["second"], $arr["month"], $arr["day"], $arr["year"]);
}

function br2nl($string) {
    $return = preg_replace('/<br ?\/?>/i', "\n", $string);
    return $return;
}

function spaceBeforeCapital($string) {
    return preg_replace('/(?<!\ )[A-Z]/', ' $0', $string);
}

function get_config($key, $sections = array()) {
    // default config
    $sections[] = 'common_config';
    $sections[] = 'path_config';

    $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/config.ini', $sections);

    return $config->$key;
}

function getDateFormating($phpDate, $enabled = true) {

    if (!is_int($phpDate)) {
        $phpDate = strtotime($phpDate);
    }

    if ($enabled) {
        $today = strtotime(date("Y-m-d", $phpDate));

        if ($today == strtotime(date("Y-m-d"))) {
            return 'Today, ' . date("g:i a", $phpDate);
        } else {
            return date("F j, Y, g:i a", $phpDate);
        }
    } else {
        return date("F j, Y", $phpDate);
    }
}

function get_shortcut($string, $word = '') {
    $strings = explode(" ", $string);

    if (count($strings) <= 1) {
        return $string;
    } else {
        $shortcut = '';
        foreach ($strings as $string) {
            $shortcut .= strtoupper(substr($string, 0, 1));
        }

        if ($word) {
            $shortcut = "{$shortcut} {$word}";
        }

        return $shortcut;
    }
}

function get_line_address($address, $is_company = false) {
    if (is_array($address)) {
        if (!$is_company) {
            $unit_lot_number = isset($address['unit_lot_number']) ? $address['unit_lot_number'] : '';
            $street_number = isset($address['street_number']) ? $address['street_number'] : '';
            $street_address = isset($address['street_address']) ? $address['street_address'] : '';
            $suburb = isset($address['suburb']) ? $address['suburb'] : '';
            $state = isset($address['state']) ? strtoupper($address['state']) : '';
            $postcode = isset($address['postcode']) ? $address['postcode'] : '';
        } else {
            $unit_lot_number = isset($address['company_unit_lot_number']) ? $address['company_unit_lot_number'] : '';
            $street_number = isset($address['company_street_number']) ? $address['company_street_number'] : '';
            $street_address = isset($address['company_street_address']) ? $address['company_street_address'] : '';
            $suburb = isset($address['company_suburb']) ? $address['company_suburb'] : '';
            $state = isset($address['company_state']) ? strtoupper($address['company_state']) : '';
            $postcode = isset($address['company_postcode']) ? $address['company_postcode'] : '';
        }
        $line_address = '';
        $line_address .= $unit_lot_number ? "{$unit_lot_number}/" : '';
        $line_address .= $street_number ? "{$street_number}" : '';
        $line_address .= $street_address ? " {$street_address}" : '';
        $line_address .= $suburb ? " {$suburb}" : '';
        $line_address .= $state ? " {$state}" : '';
        $line_address .= $postcode ? " {$postcode}" : '';
    } else {
        $line_address = $address;
    }
    return trim($line_address);
}

function get_some_details_line_address($address, $is_company = false) {
    if (is_array($address)) {
        if (!$is_company) {
            $suburb = isset($address['suburb']) ? $address['suburb'] : '';
            $state = isset($address['state']) ? strtoupper($address['state']) : '';
            $postcode = isset($address['postcode']) ? $address['postcode'] : '';
        } else {
            $suburb = isset($address['company_suburb']) ? $address['company_suburb'] : '';
            $state = isset($address['company_state']) ? strtoupper($address['company_state']) : '';
            $postcode = isset($address['company_postcode']) ? $address['company_postcode'] : '';
        }
        $line_address = '';
        $line_address .= $suburb ? " {$suburb}" : '';
        $line_address .= $state ? " {$state}" : '';
        $line_address .= $postcode ? " {$postcode}" : '';
    } else {
        $line_address = $address;
    }
    return trim($line_address);
}

function get_last_url_element($url) {
    $end_url = '';

    $path = parse_url($url, PHP_URL_PATH);
    $pathFragments = explode('/', $path);
    $end_url = end($pathFragments);

    return $end_url;
}

function get_customer_name($customer) {

    $customer_name = '';

    if ($customer) {
        //$title = isset($customer['title']) && $customer['title'] ? ucfirst($customer['title']) . '.' : '';
        $first_name = isset($customer['first_name']) && $customer['first_name'] ? ucwords($customer['first_name']) : '';
        $last_name = isset($customer['last_name']) && $customer['last_name'] ? ' ' . ucwords($customer['last_name']) : '';

        $customer_name = $first_name . $last_name;
    }
    return $customer_name;
}

function preparer_number($number) {

    if (!$number) {
        return '';
    }

    if (!is_string($number)) {
        return '';
    }

    $number = preg_replace("#^\+#", "00", $number);
    $number = preg_replace('/[^0-9]/', '', $number);

    return $number;
}

/**
 * getTimePeriodByName
 * saturday to friday
 *
 * @param type $name 
 */
function getTimePeriodByName($name, $time = 0) {

    if (!$time) {
        $time = time();
    }
    if (!is_int($time)) {
        $time = strtotime($time);
    }

    $start = '';
    $end = '';

    switch ($name) {

        case 'last_year':
            $last_year = date('Y', $time) - 1;

            $start = $last_year . '-01-01 00:00:00';
            $end = $last_year . '-12-31 23:59:59';
            break;

        case 'this_year':
            $this_year = date('Y', $time);

            $start = $this_year . '-01-01 00:00:00';
            $end = $this_year . '-12-31 23:59:59';
            break;

        case 'next_year':
            $next_year = date('Y', $time) + 1;

            $start = $next_year . '-01-01 00:00:00';
            $end = $next_year . '-12-31 23:59:59';
            break;

        case 'last_quarter':

            $last_year = date('Y', $time) - 1;
            $this_year = date('Y', $time);
            $next_year = date('Y', $time) + 1;

            $quarter = floor((date('m', $time) - 1) / 3) + 1;

            $quarter = $quarter - 1;

            if ($quarter == 0) {
                $this_year = $last_year;
                $quarter = 4;
            }

            switch ($quarter) {
                case 1:
                    $start = $this_year . '-01-01 00:00:00';
                    $end = $this_year . '-03-31 23:59:59';
                    break;
                case 2:
                    $start = $this_year . '-04-01 00:00:00';
                    $end = $this_year . '-06-30 23:59:59';
                    break;
                case 3:
                    $start = $this_year . '-07-01 00:00:00';
                    $end = $this_year . '-09-30 23:59:59';
                    break;
                case 4:
                    $start = $this_year . '-10-01 00:00:00';
                    $end = $this_year . '-12-31 23:59:59';
                    break;
            }
            break;

        case 'this_quarter':
            $this_year = date('Y', $time);
            $next_year = date('Y', $time) + 1;

            $quarter = floor((date('m', $time) - 1) / 3) + 1;

            switch ($quarter) {
                case 1:
                    $start = $this_year . '-01-01 00:00:00';
                    $end = $this_year . '-03-31 23:59:59';
                    break;
                case 2:
                    $start = $this_year . '-04-01 00:00:00';
                    $end = $this_year . '-06-30 23:59:59';
                    break;
                case 3:
                    $start = $this_year . '-07-01 00:00:00';
                    $end = $this_year . '-09-30 23:59:59';
                    break;
                case 4:
                    $start = $this_year . '-10-01 00:00:00';
                    $end = $this_year . '-12-31 23:59:59';
                    break;
            }
            break;

        case 'next_quarter':
            $this_year = date('Y', $time);
            $next_year = date('Y', $time) + 1;

            $quarter = floor((date('m', $time) - 1) / 3) + 1;

            $quarter = $quarter + 1;

            if ($quarter == 5) {
                $this_year = $next_year;
                $quarter = 1;
            }

            switch ($quarter) {
                case 1:
                    $start = $this_year . '-01-01 00:00:00';
                    $end = $this_year . '-03-31 23:59:59';
                    break;
                case 2:
                    $start = $this_year . '-04-01 00:00:00';
                    $end = $this_year . '-06-30 23:59:59';
                    break;
                case 3:
                    $start = $this_year . '-07-01 00:00:00';
                    $end = $this_year . '-09-30 23:59:59';
                    break;
                case 4:
                    $start = $this_year . '-10-01 00:00:00';
                    $end = $this_year . '-12-31 23:59:59';
                    break;
            }
            break;

        case 'last_month':
            $last_year = date('Y', $time) - 1;
            $this_year = date('Y', $time);

            $last_month = date('m', $time) - 1;

            if ($last_month == 0) {
                $last_month = 12;
                $start = $last_year . '-' . $last_month . '-01 00:00:00';
                $end = date('Y-m-d', strtotime($last_year . '-' . $last_month . " next month - 1 hour")) . ' 23:59:59';
            } else {
                $start = $this_year . '-' . $last_month . '-01 00:00:00';
                $end = date('Y-m-d', strtotime($this_year . '-' . $last_month . " next month - 1 hour")) . ' 23:59:59';
            }
            break;

        case 'this_month':
            $this_year = date('Y', $time);

            $this_month = date('m', $time);

            $start = $this_year . '-' . $this_month . '-01 00:00:00';
            $end = date('Y-m-d', strtotime($this_year . '-' . $this_month . " next month - 1 hour")) . ' 23:59:59';
            break;

        case 'next_month':
            $this_year = date('Y', $time);
            $next_year = date('Y', $time) + 1;

            $next_month = date('m', $time) + 1;

            if ($next_month == 13) {
                $next_month = 1;
                $start = $next_year . '-' . $next_month . '-01 00:00:00';
                $end = date('Y-m-d', strtotime($next_year . '-' . $next_month . " next month - 1 hour")) . ' 23:59:59';
            } else {
                $start = $this_year . '-' . $next_month . '-01 00:00:00';
                $end = date('Y-m-d', strtotime($this_year . '-' . $next_month . " next month - 1 hour")) . ' 23:59:59';
            }
            break;

        case 'last_week':

            $dom = date('d', $time);
            $dow = date('w', $time);

            $day = ($dom - $dow - 7 - 1);

            $start = date("Y-m-d 00:00:00", mktime(0, 0, 0, date("m", $time), $day, date("Y", $time)));
            $end = date("Y-m-d 23:59:59", mktime(0, 0, 0, date("m", strtotime($start)), date("d", strtotime($start)) + 6, date("Y", strtotime($start))));

            break;

        case 'this_week':

            $dom = date('d', $time);
            $dow = date('w', $time);

            $day = ($dom - $dow - 1);

            $start = date("Y-m-d 00:00:00", mktime(0, 0, 0, date("m", $time), $day, date("Y", $time)));
            $end = date("Y-m-d 23:59:59", mktime(0, 0, 0, date("m", strtotime($start)), date("d", strtotime($start)) + 6, date("Y", strtotime($start))));

            break;

        case 'next_week':

            $dom = date('d', $time);
            $dow = date('w', $time);

            $day = ($dom - $dow + 7 - 1);

            $start = date("Y-m-d 00:00:00", mktime(0, 0, 0, date("m", $time), $day, date("Y", $time)));
            $end = date("Y-m-d 23:59:59", mktime(0, 0, 0, date("m", strtotime($start)), date("d", strtotime($start)) + 6, date("Y", strtotime($start))));

            break;

        case 'last_day':
        case 'yesterday':
            $start = date("Y-m-d 00:00:00", mktime(0, 0, 0, date("m", $time), date("d", $time) - 1, date("Y", $time)));
            $end = date("Y-m-d 23:59:59", mktime(0, 0, 0, date("m", strtotime($start)), date("d", strtotime($start)), date("Y", strtotime($start))));
            break;

        case 'this_day':
        case 'today':
            $start = date("Y-m-d 00:00:00", mktime(0, 0, 0, date("m", $time), date("d", $time), date("Y", $time)));
            $end = date("Y-m-d 23:59:59", mktime(0, 0, 0, date("m", strtotime($start)), date("d", strtotime($start)), date("Y", strtotime($start))));
            break;

        case 'next_day':
        case 'tomorrow':
            $start = date("Y-m-d 00:00:00", mktime(0, 0, 0, date("m", $time), date("d", $time) + 1, date("Y", $time)));
            $end = date("Y-m-d 23:59:59", mktime(0, 0, 0, date("m", strtotime($start)), date("d", strtotime($start)), date("Y", strtotime($start))));
            break;
    }


    return array('start' => $start, 'end' => $end);
}