<?php

$GLOBALS['WKPDF_PATH'] = realpath(APPLICATION_PATH . '/../public/WKPDF') . '/';
$GLOBALS['PDF_FILE_PATH'] = realpath(APPLICATION_PATH . '/../public/WKPDF/pdf_files') . '/';
$GLOBALS['PDF_FILE_PATH1'] = realpath(APPLICATION_PATH . '/public/WKPDF/pdf_files1') . '/';
$GLOBALS['PDF_FILE_PATH2'] = realpath(APPLICATION_PATH . '/public/WKPDF/pdf_files2') . '/';
$GLOBALS['TEMP_WKPDF_PATH'] = realpath(APPLICATION_PATH . '/../public/WKPDF/tmp') . '/';

function wkhtmltopdf($source, $destination) {
    $tmp = createTempFile($source);
    $web = $GLOBALS['TEMP_WKPDF_PATH'] . $tmp;

    //mac 64
    //$command = $GLOBALS['WKPDF_PATH'] . "wkhtmltopdf --page-size A4 {$web} {$destination}";
    //linux 64
    $command = $GLOBALS['WKPDF_PATH'] . "wkhtmltopdf-amd64 --page-size A4 {$web} {$destination}";
    //linux 32
    //$command = $GLOBALS['WKPDF_PATH'] . "wkhtmltopdf-i386 --page-size A4 {$web} {$destination}";
    
    exec($command);
    unlink($GLOBALS['TEMP_WKPDF_PATH'] . $tmp);
}

function createTempFile($html) {
    do {
        $temp_name = time() . '.html';
        $tmp = $GLOBALS['TEMP_WKPDF_PATH'] . $temp_name;
    } while (file_exists($tmp));

    file_put_contents($tmp, $html);

    return $temp_name;
}

function createPdfPath() {
//// test by islam
$dir1 = $GLOBALS['PDF_FILE_PATH1'];
$subdir1 = date('Y/m/d/');
mkdir($dir1 . '/' . $subdir1, 0777, true);
//////////////
$dir2 = $GLOBALS['PDF_FILE_PATH2'];
$subdir2 = date('Y/m/d/');
mkdir($dir2 . '/' . $subdir2, 0777, true);
$apacheUser = getApacheUser();
        chown($dir2 . '/' . $subdir2, $apacheUser);
        chgrp($dir2 . '/' . $subdir2, $apacheUser);



////////
    //get dir
    $dir = $GLOBALS['PDF_FILE_PATH'];
	
    //get sub dir
    $subdir = date('Y/m/d/');
    if (!is_dir($dir . '/' . $subdir)) {
        mkdir($dir . '/' . $subdir, 0777, true);

        $apacheUser = getApacheUser();
        chown($dir . '/' . $subdir, $apacheUser);
        chgrp($dir . '/' . $subdir, $apacheUser);
    }

    //check if file exists or not
    $fullDir = $dir . $subdir;
    if (!is_dir($fullDir)) {
        mkdir($fullDir, 0777, true);

        $apacheUser = getApacheUser();
        chown($dir . '/' . $subdir, $apacheUser);
        chgrp($dir . '/' . $subdir, $apacheUser);
    }

    return array('fullDir' => $fullDir, 'dir' => $dir, 'subdir' => $subdir);
}

function getApacheUser() {

    $apacheUser = 'www-data';
	

//    $output = array();
//    exec("cat /etc/passwd | cut -d\":\" -f1", $output);
//    foreach ($output as $user_name) {
//        $user = posix_getpwnam($user_name);
//        if (!empty($user['gecos'])) {
//            if ($user['gecos'] == 'World Wide Web Server' || $user['gecos'] == 'www-data') {
//                $apacheUser = $user['name'];
//                break;
//            }
//        }
//    }

    return $apacheUser;
}
