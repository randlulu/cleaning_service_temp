<?php

class Form_ForgetPasswordStep2 extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('ForgetPasswordStep2');

        $request = Zend_Controller_Front::getInstance()->getRequest();

        $password = new Zend_Form_Element_Password('password');
        $password->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->addValidator(new Zend_Validate_StringLength(array('min' => 6, 'max' => 20)));

        $passwordConfirm = new Zend_Form_Element_Password('password_confirm');
        $passwordConfirm->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'));

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Resat');
        $button->setAttribs(array('class' => 'button'));

        $this->addElements(array($password, $passwordConfirm, $button));
        $this->setMethod('post');
        $this->setAction($_SERVER['REQUEST_URI']);
    }

}

