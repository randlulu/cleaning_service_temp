<?php

class Form_ChangeAccountInfo extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('ChangeAccountInfo');
        $user = (isset($options['user']) ? $options['user'] : '');
        $countryId = (isset($options['country_id']) ? $options['country_id'] : 0);


        $router = Zend_Controller_Front::getInstance()->getRouter();
        $request = Zend_Controller_Front::getInstance()->getRequest();

        $username = new Zend_Form_Element_Text('username');
        $username->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($user['username']) ? $user['username'] : ''));
        if ($user['username'] != $request->getParam('username')) {
            $username->addValidator(new Zend_Validate_Db_NoRecordExists('user', 'username'));
        }



        $email2 = new Zend_Form_Element_Text('email2');
        $email2->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($user['email2']) ? $user['email2'] : ''));
        $email2->addValidator(new Zend_Validate_EmailAddress());
        $email2->addErrorMessage("Invalid e-mail");

        $email3 = new Zend_Form_Element_Text('email3');
        $email3->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($user['email3']) ? $user['email3'] : ''));
        $email3->addValidator(new Zend_Validate_EmailAddress());
        $email3->addErrorMessage("Invalid e-mail");



        $mobile1 = new Zend_Form_Element_Text('mobile1');
        $mobile1->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($user['mobile1']) ? $user['mobile1'] : ''));

        $mobile2 = new Zend_Form_Element_Text('mobile2');
        $mobile2->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($user['mobile2']) ? $user['mobile2'] : ''));

        $mobile3 = new Zend_Form_Element_Text('mobile3');
        $mobile3->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($user['mobile3']) ? $user['mobile3'] : ''));



        $phone1 = new Zend_Form_Element_Text('phone1');
        $phone1->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($user['phone1']) ? $user['phone1'] : ''));

        $phone2 = new Zend_Form_Element_Text('phone2');
        $phone2->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($user['phone2']) ? $user['phone2'] : ''));
        $phone3 = new Zend_Form_Element_Text('phone3');
        $phone3->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($user['phone3']) ? $user['phone3'] : ''));



        $fax = new Zend_Form_Element_Text('fax');
        $fax->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($user['fax']) ? $user['fax'] : ''));

        $emergencyPhone = new Zend_Form_Element_Text('emergency_phone');
        $emergencyPhone->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($user['emergency_phone']) ? $user['emergency_phone'] : ''));

        $unitLotNumber = new Zend_Form_Element_Text('unit_lot_number');
        $unitLotNumber->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($user['unit_lot_number']) ? $user['unit_lot_number'] : ''));

        $streetNumber = new Zend_Form_Element_Text('street_number');
        $streetNumber->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($user['street_number']) ? $user['street_number'] : ''));

        $streetAddress = new Zend_Form_Element_Text('street_address');
        $streetAddress->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($user['street_address']) ? $user['street_address'] : ''));

        $suburb = new Zend_Form_Element_Text('suburb');
        $suburb->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($user['suburb']) ? $user['suburb'] : ''));

        //get country & city for ajax
        //
        $city_obj = new Model_Cities();
        $city = $city_obj->getById((!empty($user['city_id']) ? $user['city_id'] : 0));

        $country_id = new Zend_Form_Element_Select('country_id');
        $country_id->removeDecorator('HtmlTag')
                ->removeDecorator('Label')
                ->setAttribs(array('class' => 'select_field', 'onchange' => 'getCities();'))
                ->setRequired()
                ->setValue((!empty($city['country_id']) ? $city['country_id'] : ''))
                ->addDecorator('Errors', array('class' => 'errors'));

        $table = new Model_Countries();
        $country_id->addMultiOption('', 'Select One');
        foreach ($table->getCountriesAsArray() as $c) {
            $country_id->addMultiOption($c['id'], $c['name']);
        }

        $city_id = new Zend_Form_Element_Select('city_id');
        $city_id->removeDecorator('HtmlTag')
                ->removeDecorator('Label')
                ->setAttribs(array('class' => 'select_field'))
                ->setRequired()
                ->setValue((!empty($city['city_id']) ? $city['city_id'] : ''))
                ->addDecorator('Errors', array('class' => 'errors'));

        $city_id->addMultiOption('', 'Select One');
        foreach ($city_obj->getByCountryId((!empty($countryId) ? $countryId : $city['country_id'])) as $c) {
            $city_id->addMultiOption($c['city_id'], $c['city_name']);
        }

        $state = new Zend_Form_Element_Text('state');
        $state->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($user['state']) ? $user['state'] : ''));

        $postcode = new Zend_Form_Element_Text('postcode');
        $postcode->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($user['postcode']) ? $user['postcode'] : ''));

        $poBox = new Zend_Form_Element_Text('po_box');
        $poBox->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($user['po_box']) ? $user['po_box'] : ''));

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'button'));


        $this->setMethod('post');

        $this->addElements(array($username, $email2, $email3, $mobile1, $mobile2, $mobile3, $phone1, $phone2, $phone3, $fax, $emergencyPhone, $unitLotNumber, $streetNumber, $streetAddress, $state, $suburb, $country_id, $city_id, $postcode, $poBox, $button));
        $this->setAction($router->assemble(array(), 'changeAccountInfo'));
    }

}

