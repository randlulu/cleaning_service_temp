<?php

class Form_ChangePassword extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('ChangePassword');

        $user = isset($options['user']) ? $options['user'] : '';

        $router = Zend_Controller_Front::getInstance()->getRouter();
        $request = Zend_Controller_Front::getInstance()->getRequest();

        $old_password = $request->getParam('old_password');
        $new_password = $request->getParam('new_password');
        $confirm_new_password = $request->getParam('confirm_new_password');

        $oldPassword = new Zend_Form_Element_Password('old_password');
        $oldPassword->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired(true)
                ->setAttribs(array('class' => 'text_field'));
        if ($old_password) {
            if (sha1($old_password) != $user['password']) {
                $oldPassword->addError('invalid old password');
            }
        }

        $newPassword = new Zend_Form_Element_Password('new_password');
        $newPassword->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired(true)
                ->setAttribs(array('class' => 'text_field'));
        $newPassword->addValidator(new Zend_Validate_StringLength(array('min' => 6, 'max' => 20)));


        $confirmNewPassword = new Zend_Form_Element_Password('confirm_new_password');
        $confirmNewPassword->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired(true)
                ->setAttribs(array('class' => 'text_field'));
        if ($new_password && $confirm_new_password) {
            if ($new_password != $confirm_new_password) {
                $confirmNewPassword->addError('new password and confirm new password not match');
            }
        }

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'button'));

        $this->addElements(array($oldPassword, $newPassword, $confirmNewPassword, $button));
        $this->setMethod('post');
        $this->setAction($router->assemble(array(), 'changePassword'));
    }

}
