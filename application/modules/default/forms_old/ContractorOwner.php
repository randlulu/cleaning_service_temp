<?php

class Form_ContractorOwner extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('ContractorOwner');

        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $contractor_info_id = (isset($options['contractor_info_id']) ? $options['contractor_info_id'] : '');
        $contractorOwner = (isset($options['contractorOwner']) ? $options['contractorOwner'] : '');
        $countryId = (isset($options['country_id']) ? $options['country_id'] : 0);
        $optionState = (isset($options['state']) ? $options['state'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();
        $request = Zend_Controller_Front::getInstance()->getRequest();
  

        $name = new Zend_Form_Element_Text('name');
        $name->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue(!empty($contractorOwner['name']) ? $contractorOwner['name'] : '')
                ->setErrorMessages(array('Required' => 'Please enter the Contractor Owner Name'));

        

        $contractorOwnerEmail1 = (!empty($contractorOwner['email1']) ? $contractorOwner['email1'] : '');
        $email1 = new Zend_Form_Element_Text('email1');
        $email1->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue($contractorOwnerEmail1);
        $email1->addValidator(new Zend_Validate_EmailAddress());
        if ($request->getParam('email1') && $email1->isValid($request->getParam('email1')) && $contractorOwnerEmail1 != $request->getParam('email1')) {
            $email1->addValidator(new Zend_Validate_Db_NoRecordExists('user', 'email1'));
        }

        $email2 = new Zend_Form_Element_Text('email2');
        $email2->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($contractorOwner['email2']) ? $contractorOwner['email2'] : ''));
        $email2->addValidator(new Zend_Validate_EmailAddress());


        $email3 = new Zend_Form_Element_Text('email3');
        $email3->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($contractorOwner['email3']) ? $contractorOwner['email3'] : ''));
        $email3->addValidator(new Zend_Validate_EmailAddress());

        $mobile1 = new Zend_Form_Element_Text('mobile1');
        $mobile1->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($contractorOwner['mobile1']) ? $contractorOwner['mobile1'] : ''));


        $mobile2 = new Zend_Form_Element_Text('mobile2');
        $mobile2->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($contractorOwner['mobile2']) ? $contractorOwner['mobile2'] : ''));

        $mobile3 = new Zend_Form_Element_Text('mobile3');
        $mobile3->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($contractorOwner['mobile3']) ? $contractorOwner['mobile3'] : ''));

        $phone1 = new Zend_Form_Element_Text('phone1');
        $phone1->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($contractorOwner['phone1']) ? $contractorOwner['phone1'] : ''));

        $phone2 = new Zend_Form_Element_Text('phone2');
        $phone2->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($contractorOwner['phone2']) ? $contractorOwner['phone2'] : ''));
        $phone3 = new Zend_Form_Element_Text('phone3');
        $phone3->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($contractorOwner['phone3']) ? $contractorOwner['phone3'] : ''));

        $fax = new Zend_Form_Element_Text('fax');
        $fax->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($contractorOwner['fax']) ? $contractorOwner['fax'] : ''));

        $emergencyPhone = new Zend_Form_Element_Text('emergency_phone');
        $emergencyPhone->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($contractorOwner['emergency_phone']) ? $contractorOwner['emergency_phone'] : ''));

        $unitLotNumber = new Zend_Form_Element_Text('unit_lot_number');
        $unitLotNumber->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($contractorOwner['unit_lot_number']) ? $contractorOwner['unit_lot_number'] : ''));

        $streetNumber = new Zend_Form_Element_Text('street_number');
        $streetNumber->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($contractorOwner['street_number']) ? $contractorOwner['street_number'] : ''));

        $streetAddress = new Zend_Form_Element_Text('street_address');
        $streetAddress->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($contractorOwner['street_address']) ? $contractorOwner['street_address'] : ''));

        $suburb = new Zend_Form_Element_Text('suburb');
        $suburb->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($contractorOwner['suburb']) ? $contractorOwner['suburb'] : ''));

        //
        //get country & city for ajax
        //
      
        $city_obj = new Model_Cities();
        $city = $city_obj->getById((!empty($contractorOwner['city_id']) ? $contractorOwner['city_id'] : CheckAuth::getCityId()));

        $country_id = new Zend_Form_Element_Select('country_id');
        $country_id->removeDecorator('HtmlTag')
                ->removeDecorator('Label')
                ->setAttribs(array('class' => 'select_field', 'onchange' => 'getState();'))
                ->setRequired()
                ->setValue((!empty($city['country_id']) ? $city['country_id'] : ''))
                ->addDecorator('Errors', array('class' => 'errors'));

        $table = new Model_Countries();
        $country_id->addMultiOption('', 'Select One');
        foreach ($table->getCountriesAsArray() as $c) {
            $country_id->addMultiOption($c['id'], $c['name']);
        }

        $state = new Zend_Form_Element_Select('state');
        $state->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'select_field', 'onchange' => 'getCities();'))
                ->setValue((!empty($city['state']) ? $city['state'] : ''));

        $state->addMultiOption('', 'Select One');
        $state->addMultiOptions($city_obj->getStateByCountryId((!empty($countryId) ? $countryId : $city['country_id'])));

        $city_id = new Zend_Form_Element_Select('city_id');
        $city_id->removeDecorator('HtmlTag')
                ->removeDecorator('Label')
                ->setAttribs(array('class' => 'select_field'))
                ->setRequired()
                ->setValue((!empty($city['city_id']) ? $city['city_id'] : ''))
                ->addDecorator('Errors', array('class' => 'errors'));

        $city_id->addMultiOption('', 'Select One');
        $city_id->addMultiOptions($city_obj->getCitiesByCountryIdAndState((!empty($countryId) ? $countryId : $city['country_id']), (!empty($optionState) ? $optionState : $city['state']), true));


        $postcode = new Zend_Form_Element_Text('postcode');
        $postcode->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($contractorOwner['postcode']) ? $contractorOwner['postcode'] : ''));

        $poBox = new Zend_Form_Element_Text('po_box');
        $poBox->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue((!empty($contractorOwner['po_box']) ? $contractorOwner['po_box'] : ''));


        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'button'));

     
      
        $this->addElements(array($name, $email1, $email2, $email3, $mobile1, $mobile2, $mobile3, $phone1, $phone2, $phone3, $fax, $emergencyPhone, $unitLotNumber, $streetNumber, $streetAddress, $state, $suburb, $country_id, $city_id, $postcode, $poBox, $button));
        $this->setMethod('post');
        if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $contractorOwner['contractor_owner_id']), 'contractorOwnerEdit'));
        } else {
            $this->setAction($router->assemble(array(), 'contractorOwnerAdd'));
        }
    }

}

