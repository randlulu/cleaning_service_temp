<?php

class Form_LicenceInfo extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('ContractorInfo');
        $contractorInfo = (isset($options['contractorInfo']) ? $options['contractorInfo'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();
		
	
       

        $drivers_licence_number = new Zend_Form_Element_Text('drivers_licence_number');
        $drivers_licence_number->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                // ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($contractorInfo['drivers_licence_number']) ? $contractorInfo['drivers_licence_number'] : ''));
        // ->setErrorMessages(array('Required' => 'Please enter the Drivers licence Number'));

        $drivers_licence_expiry = new Zend_Form_Element_Text('drivers_licence_expiry');
        $drivers_licence_expiry->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($contractorInfo['drivers_licence_expiry']) ? (!empty($contractorInfo['drivers_licence_expiry']) ? (strlen($contractorInfo['drivers_licence_expiry']) < 10)? $contractorInfo['drivers_licence_expiry']:date('d-m-Y',$contractorInfo['drivers_licence_expiry']): '') : ''));
        // ->setErrorMessages(array('Required' => 'Please enter the Drivers licence Expiry'));

		
		$driver_licence_type = new Zend_Form_Element_Text('driver_licence_type');
        $driver_licence_type->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                // ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($contractorInfo['driver_licence_type']) ? $contractorInfo['driver_licence_type'] : ''));
    

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));


		$this->addElements(array($drivers_licence_expiry, $drivers_licence_number, $button , $driver_licence_type ));
				
        $this->setMethod('post');
		$this->setAction($router->assemble(array(), 'editLicence'));
        
    }

}

