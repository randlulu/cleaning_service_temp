<?php

class Form_ContractorInfo extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('ContractorInfo');
        $contractorInfo = (isset($options['contractorInfo']) ? $options['contractorInfo'] : '');
        $contractor_id = (isset($options['contractor_id']) ? $options['contractor_id'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();
		
        $contractorInfo = (isset($options['contractorInfo']) ? $options['contractorInfo'] : '');
        $contractor_id = (isset($options['contractor_id']) ? $options['contractor_id'] : '');
		
		
		
		
		

        $businessName = new Zend_Form_Element_Text('business_name');
        $businessName->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($contractorInfo['business_name']) ? $contractorInfo['business_name'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the Business Name'));


        $abn = new Zend_Form_Element_Text('abn');
        $abn->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($contractorInfo['abn']) ? $contractorInfo['abn'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the ABN'));

        $tfn = new Zend_Form_Element_Text('tfn');
        $tfn->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($contractorInfo['tfn']) ? $contractorInfo['tfn'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the TFN'));


        $acn = new Zend_Form_Element_Text('acn');
        $acn->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($contractorInfo['acn']) ? $contractorInfo['acn'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the ACN'));

        $gst = new Zend_Form_Element_Checkbox('gst');
        $gst->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'checkbox_field'))
                ->setValue((!empty($contractorInfo['gst']) ? $contractorInfo['gst'] : ''));
        //->setErrorMessages(array('Required' => 'Please enter the GST'));

		$db_format = 0;
		$dateTimeObj= get_settings_date_format();
		if($dateTimeObj){
			$db_format = 1;
		}
		$dateFormated = '';
		if(!empty($contractorInfo['gst_date_registered']))
			$dateFormated = getNewDateFormat($contractorInfo['gst_date_registered']);
		
        $gstDateRegistered = new Zend_Form_Element_Text('gst_date_registered');
        $gstDateRegistered->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setAttrib('readonly', TRUE)
                ->setValue((!empty($contractorInfo['gst_date_registered']) ? ($dateFormated ? $dateFormated : date('d-m-Y', $contractorInfo['gst_date_registered'])) : ''));
        //->setErrorMessages(array('Required' => 'Please enter the  GST Date Registered'));

        /*$insurance_policy_number = new Zend_Form_Element_Text('insurance_policy_number');
        $insurance_policy_number->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($contractorInfo['insurance_policy_number']) ? $contractorInfo['insurance_policy_number'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the Insurance Policy Number'));

        $insurance_policy_start = new Zend_Form_Element_Text('insurance_policy_start');
        $insurance_policy_start->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($contractorInfo['insurance_policy_start']) ? (strlen($contractorInfo['insurance_policy_start']) < 10)? $contractorInfo['insurance_policy_start']:date('d-m-Y',$contractorInfo['insurance_policy_start']) : ''))
                ->setErrorMessages(array('Required' => 'Please enter the Insurance Policy Start'));

        $insurance_policy_expiry = new Zend_Form_Element_Text('insurance_policy_expiry');
        $insurance_policy_expiry->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($contractorInfo['insurance_policy_expiry']) ? (strlen($contractorInfo['insurance_policy_expiry']) < 10)? $contractorInfo['insurance_policy_expiry']:date('d-m-Y',$contractorInfo['insurance_policy_expiry']): ''))
                ->setErrorMessages(array('Required' => 'Please enter the Insurance Policy Expiry'));


        $insurance_listed_services_covered = new Zend_Form_Element_Text('insurance_listed_services_covered');
        $insurance_listed_services_covered->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($contractorInfo['insurance_listed_services_covered']) ? $contractorInfo['insurance_listed_services_covered'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the Insurance listed services covered'));

        $drivers_licence_number = new Zend_Form_Element_Text('drivers_licence_number');
        $drivers_licence_number->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($contractorInfo['drivers_licence_number']) ? $contractorInfo['drivers_licence_number'] : ''))
                ->setErrorMessages(array('Required' => 'Please enter the Drivers licence Number'));

        $drivers_licence_expiry = new Zend_Form_Element_Text('drivers_licence_expiry');
        $drivers_licence_expiry->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue((!empty($contractorInfo['drivers_licence_expiry']) ? (strlen($contractorInfo['drivers_licence_expiry']) < 10)? $contractorInfo['drivers_licence_expiry']:date('d-m-Y',$contractorInfo['drivers_licence_expiry']): ''))
                ->setErrorMessages(array('Required' => 'Please enter the Drivers licence Expiry'));*/


        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));

        //$this->addElements(array($businessName, $abn, $tfn, $acn, $gst, $gstDateRegistered, $insurance_listed_services_covered, $insurance_policy_expiry, $insurance_policy_number, $insurance_policy_start, $drivers_licence_expiry, $drivers_licence_number, $button));
        
		$this->addElements(array($businessName, $abn, $tfn, $acn, $gst, $gstDateRegistered, $button));
		$this->setMethod('post');

        $this->setAction($router->assemble(array(), 'modfyContractorInfo'));
    }

}

