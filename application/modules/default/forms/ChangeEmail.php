<?php

class Form_ChangeEmail extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('ForgetPassword');

        $router = Zend_Controller_Front::getInstance()->getRouter();
        $request = Zend_Controller_Front::getInstance()->getRequest();

        $user = isset($options['user']) ? $options['user'] : array();

        $oldEmail = new Zend_Form_Element_Text('old_email');
        $oldEmail->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired(true)
                ->setAttribs(array('class' => 'form-control'))
                ->setValue(isset($user['email1']) ? $user['email1'] : '');
        $oldEmail->addValidator(new Zend_Validate_EmailAddress());
        if ($request->getParam('old_email') && $oldEmail->isValid($request->getParam('old_email'))) {
            $oldEmail->addValidator(new Zend_Validate_Db_RecordExists('user', 'email1'));
        }


        $newEmail = new Zend_Form_Element_Text('new_email');
        $newEmail->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired(true)
                ->setAttribs(array('class' => 'form-control'));
        $newEmail->addValidator(new Zend_Validate_EmailAddress());
        if ($request->getParam('new_email') && $newEmail->isValid($request->getParam('new_email'))) {
            $newEmail->addValidator(new Zend_Validate_Db_NoRecordExists('user', 'email1'));
        }


        $confirmNewEmail = new Zend_Form_Element_Text('confirm_new_email');
        $confirmNewEmail->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired(true)
                ->setAttribs(array('class' => 'form-control'));


        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Change');
        $button->setAttribs(array('class' => 'btn btn-primary'));

        $this->addElements(array($oldEmail, $newEmail, $confirmNewEmail, $button));
        $this->setMethod('post');
        $this->setAction($router->assemble(array(), 'changeEmail'));
    }

}
