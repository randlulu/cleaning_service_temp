<?php

class Form_DeclarationOfChemicals extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('DeclarationOfChemicals');
        $contractor_info_id = (isset($options['contractor_info_id']) ? $options['contractor_info_id'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();

        $chemicals = new Zend_Form_Element_Text('chemicals');
        $chemicals->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setErrorMessages(array('Required' => 'Please enter the Chemicals'));


        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));

        $this->addElements(array($chemicals, $button));
        $this->setMethod('post');
        $this->setAction($router->assemble(array(), 'declarationOfChemicalsAdd'));
    }

}

