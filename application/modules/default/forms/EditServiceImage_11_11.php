<?php

class Form_EditServiceImage extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);
		

        $this->setName('EditServiceImage');
        $imageAttachment = (isset($options['imageAttachment']) ? $options['imageAttachment'] : '');
		
		
        $router = Zend_Controller_Front::getInstance()->getRouter();



        $modelServices = new Model_Services();
		if($options['type'] == 'service'){
		 $allService = $modelServices->getAllService();
		}else{
		 $allService = $modelServices->getAllByItemAsArray($options['itemId'] , $options['type']);
		}        
        $service = new Zend_Form_Element_Select('service_id');
        $service->setDecorators(array('ViewHelper'));
        $service->setAttrib("class", "select_field form-control");
        $service->setValue((isset($imageAttachment['service_id'] ) ? $imageAttachment['service_id'] : ''));
        $service->addMultiOption('', 'Select One');
        $service->addMultiOptions($allService);

        
        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));

        $this->addElements(array($service,$button));
        $this->setMethod(Zend_Form::METHOD_POST);
        $this->setEnctype(Zend_Form::ENCTYPE_MULTIPART);

        $this->setAction($router->assemble(array('id' => $imageAttachment['image_id'],'type'=>$options['type'],'itemid'=>$options['itemId']), 'ItemImageAttachmentEdit'));
        
    }

}