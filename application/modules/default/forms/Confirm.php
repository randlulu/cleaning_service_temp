<?php

class Form_Confirm extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('confirm');
        
        $router = Zend_Controller_Front::getInstance()->getRouter();
        $request = Zend_Controller_Front::getInstance()->getRequest();


        $contact = new Zend_Form_Element_Text('contact');
        $contact->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class'=>'form-control','placeholder'=>'Email or Contact related'))
                ->setErrorMessages(array('Required' => 'This Field is Required'));

        $number = new Zend_Form_Element_Text('number');
        $number->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class'=>'form-control','placeholder'=>'Booking, Estimate or Invoice Number'))
                ->setErrorMessages(array('Required' => 'This Field is Required'));

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('View Details');
        $button->setAttribs(array('class' => 'btn btn-lg btn-primary btn-block login-btn'));

       
        $this->setMethod('post');
        
        $this->addElements(array($contact,$number,$button));
        $this->setAction($router->assemble(array(),'confirm'));
    }
}

