<?php

class Form_ContractorEmployeePhotoUpload extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $contractorEmpId = (isset($options['contractorEmpId']) ? $options['contractorEmpId'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();
        $request = Zend_Controller_Front::getInstance()->getRequest();


        $image = new Zend_Form_Element_File('image');
        $image->setDestination(get_config('temp_uploads'))
                ->setDecorators(array('File', 'Errors', array('Description', array('tag' => 'p', 'class' => 'hint'))))
                ->setRequired(true)
                ->setMaxFileSize(get_config('upload_max_filesize')) // limits the filesize on the client side
                ->setDescription('Click Browse and click on the  file you would like to upload');
        $image->addValidator('Count', false, 1);  // ensure only 1 file
        $image->addValidator('Size', false, get_config('upload_max_filesize')); //10240000 limit to 10 meg
        $image->addValidator('Extension', false, 'jpg,jpeg,png,gif'); // only JPEG, PNG, and GIFs


        $this->addElements(array($image));

        $this->setName('avatar');
        $this->setMethod(Zend_Form::METHOD_POST);
        $this->setEnctype(Zend_Form::ENCTYPE_MULTIPART);
        $this->setAction($router->assemble(array('id' => $contractorEmpId), 'contractorEmployeePhotoUpload'));
    }

}