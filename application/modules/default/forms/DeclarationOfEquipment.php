<?php
  
class Form_DeclarationOfEquipment extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('DeclarationOfEquipment');
        $equipmentInfo = (isset($options['equipmentInfo']) ? $options['equipmentInfo'] : '');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
		$contractor_info_id = (isset($options['contractor_info_id']) ? $options['contractor_info_id'] : '');
                
                

        $router = Zend_Controller_Front::getInstance()->getRouter();

        $equipment = new Zend_Form_Element_Text('equipment');
        $equipment->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setErrorMessages(array('Required' => 'Please enter the Equipment'))
                ->setValue((!empty($equipmentInfo['equipment']) ? $equipmentInfo['equipment'] : ''));


        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));

        $this->addElements(array($equipment, $button));
        $this->setMethod('post');
        //$this->setAction($router->assemble(array(), 'declarationOfEquipmentAdd'));
        
        	if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $equipmentInfo['id']), 'declarationOfEquipmentEdit'));
        } else {
		    
            $this->setAction($router->assemble(array(), 'declarationOfEquipmentAdd'));
        }
    }

}

