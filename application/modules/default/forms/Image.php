<?php

class Model_Image extends Zend_Db_Table_Abstract {

    protected $_name = 'image';

    /**
     * get table rows according to the assigned filters and page
     * 
     * @param array $filters
     * @param string $order
     * @param  object $pager
     * @return array 
     */
    
	public function updateById($id, $data) {
        $id = (int) $id;
        return parent::update($data, "image_id = '{$id}'");
    }
	
	public function deleteById($id) {
        $id = (int) $id;
        return parent::delete("image_id = '{$id}'");
    }
	
	
	public function getTotalGroups($id = null , $type = null, $per_group = null){
	
	 $total_records = count($this->getAll($id,$type));
	 $total_groups = ceil($total_records/$per_group);
	 return  $total_groups;
	 
	}
	
	public function getMaxGroup(){
	   
	    $select = $this->getAdapter()->select();
        $select->from(array('iu' => $this->_name),array('iu.group_id'));
		return $this->getAdapter()->fetchAll($select);	
	}
	
	
	
	public function getAll($id = null , $type = null,  $order = null, &$pager = null , $filter = array() , $limit = 0, $perPage = 0, $currentPage = 0){
        //$id = (int) $id;
	    $select = $this->getAdapter()->select();
        $select->from(array('iu' => $this->_name));
	    $select->order($order);
		$typeFilter = $this->getAdapter()->quote($type . '%');	
        $select->joinInner(array('ii' => 'item_image'), "iu.image_id = ii.image_id",array('ii.item_image_id','ii.item_id','ii.type','ii.service_id','ii.discussion_id'));
		$select->joinLeft(array('s' => 'service'), "s.service_id = ii.service_id",array('s.service_name'));
		$select->where("ii.type LIKE {$typeFilter}");
		if(!($id == 0))
		 $select->where("ii.item_id = '{$id}' ");
		 
		 
		if($filter){
		  if(!empty($filter['image_type']) && !($filter['image_type'] == 'all')){
		    $select->where("iu.image_types_id = '{$filter['image_type']}' ");     
		   
		  }
		 
		} 

		
		
		if ($pager) {
            $select->limit($pager->perPage, ($pager->currentPage - 1) * $pager->perPage);
            $pager->dbSelect = $select;
        } elseif ($limit) {
            
			$select->limit($limit);
        } elseif ($perPage && $currentPage) {
            $select->limit($perPage, ($currentPage -1 ) * $perPage);
        }
		
		
		
		
		
		//echo $select->__toString();
		//exit;
		return $this->getAdapter()->fetchAll($select);
	
	}
	
	public function getByGroupId($group_id){
	  
	    $group_id = (int) $group_id;
	    $select = $this->getAdapter()->select();
        $select->from(array('iu' => $this->_name));
		$select->where("iu.group_id = {$group_id}");
		$select->limit(4);
		return $this->getAdapter()->fetchAll($select);
	
	}
	
	public function getById($id,$type){
	 
	    $id = (int) $id;
	    $select = $this->getAdapter()->select();
        $select->from(array('iu' => $this->_name));
        $select->joinInner(array('ii' => 'item_image'), "iu.image_id = ii.image_id",array('ii.item_id','ii.item_id','ii.type','ii.service_id'));
		$select->joinLeft(array('s' => 'service'), "s.service_id = ii.service_id",array('s.service_name'));
		$select->where("iu.image_id = {$id}");
		
	 
	 return $this->getAdapter()->fetchRow($select);
	  
	 
	}





}