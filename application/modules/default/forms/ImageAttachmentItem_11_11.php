<?php

class Form_ImageAttachmentItem extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);
		

        $this->setName('ImageAttachmentItem');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $imageAttachment = (isset($options['imageAttachment']) ? $options['imageAttachment'] : '');

		
		if(!empty($imageAttachment['service_id'])){
		  $service_value = $imageAttachment['service_id'];
		}elseif(isset($options['service'])){
		 $service_value = $options['service'];
		}else{
		   $service_value = 0 ;
		}
		
		
		
        $router = Zend_Controller_Front::getInstance()->getRouter();


        $modelServices = new Model_Services();
		if($options['type'] == 'service'){
		 $allService = $modelServices->getAllService();
		}else{
		 $allService = $modelServices->getAllByItemAsArray($options['itemId'] , $options['type']);
		}      
        $service = new Zend_Form_Element_Select('service_id');
        $service->setDecorators(array('ViewHelper'));
        $service->setAttrib("class", "select_field form-control");
        $service->setValue($service_value);
        $service->addMultiOption('', 'Select One');
        $service->addMultiOptions($allService);
		
		if($options['type'] == 'service'){
		  $service->setRequired();
		  $service->setErrorMessages(array('Required' => 'Please select service '));
		  $service->addDecorator('Errors', array('class' => 'errors'));
		}
		
		
		$modelImageTypes = new Model_ImageTypes();	   
	    $imagetypes = $modelImageTypes->getAllTypesAsArray();
		
		$before_after_job = new Zend_Form_Element_Radio('before_after_job');
        $before_after_job->setDecorators(array('ViewHelper'));
		$before_after_job->addDecorator('Errors', array('class' => 'errors'));
		$before_after_job->setAttrib('label_class', 'radio-inline');
		$before_after_job->setSeparator('');
		
		if($options['type'] == 'booking'){
		  $before_after_job->setRequired();
		}
        
		$before_after_job->setErrorMessages(array('Required' => 'Please select image type.'));
        $before_after_job->setValue((isset($imageAttachment['before_after_job']) ? $imageAttachment['before_after_job'] : ''));
        $before_after_job->addMultiOptions($imagetypes);

        

        $discussion = new Zend_Form_Element_Textarea('discussion');
        $discussion->setDecorators(array('ViewHelper', 'Errors'))
                ->setAttribs(array('class' => 'textarea_field form-control','cols'=>'80','rows'=>'5'))
                ->setValue((!empty($imageAttachment['discussion']) ? $imageAttachment['discussion'] : ''));


        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));
        $button->setAttribs(array('id' => 'button_cont'));

        $this->addElements(array($service, $discussion, $button,$before_after_job));
        $this->setMethod(Zend_Form::METHOD_POST);
        $this->setEnctype(Zend_Form::ENCTYPE_MULTIPART);

        if ($mode == 'update') {
           $this->setAction($router->assemble(array('id' => $imageAttachment['image_id'],'type'=>$options['type'],'itemid'=>$options['itemId']), 'ItemImageAttachmentEdit'));
        } else {
		   $this->setAction($router->assemble(array('type'=>$options['type'],'itemid'=>$options['itemId']), 'ItemImageAttachmentAdd'));            

		}
    }

}