<?php

class Form_ForgetPassword extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('ForgetPassword');

        $router = Zend_Controller_Front::getInstance()->getRouter();
        $request = Zend_Controller_Front::getInstance()->getRequest();


        $email = new Zend_Form_Element_Text('email');
        $email->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired(true)
                ->setAttribs(array('class' => 'form-control'));
        if ($request->getParam('email')) {
            $email->addValidator(new Zend_Validate_EmailAddress());
            if ($email->isValid($request->getParam('email'))) {
                $email->addValidator(new Zend_Validate_Db_RecordExists('user', 'email1'));
            }
        }



        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Reset');
        $button->setAttribs(array('class' => 'btn btn-primary'));

        $this->addElements(array($email, $button));
        $this->setMethod('post');
        $this->setAction($router->assemble(array(), 'forgetPassword'));
    }

}

