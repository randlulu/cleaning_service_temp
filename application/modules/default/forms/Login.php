<?php

class Form_Login extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('Login');

        $router = Zend_Controller_Front::getInstance()->getRouter();


        $email = new Zend_Form_Element_Text('email');
        $email->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setErrorMessages(array('Required' => 'Please enter the email'));

        $password = new Zend_Form_Element_Password('password');
        $password->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setErrorMessages(array('Required' => 'Please enter the password'));

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Login');
        $button->setAttribs(array('class' => 'button'));

        $this->addElements(array($email, $password, $button));
        $this->setMethod('post');
        $this->setAction($router->assemble(array(), 'Login'));
    }

}

