<?php

class Form_EditServiceImage extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);


        $this->setName('EditServiceImage');
        $imageAttachment = (isset($options['imageAttachment']) ? $options['imageAttachment'] : '');


        $router = Zend_Controller_Front::getInstance()->getRouter();



        $modelServices = new Model_Services();
        if ($options['type'] == 'service') {
            $allService = $modelServices->getAllService();
        } else {
            $allService = $modelServices->getAllByItemAsArray($options['itemId'], $options['type']);
        }
        $service = new Zend_Form_Element_Select('service_id');
        $service->setDecorators(array('ViewHelper'));
        $service->setAttribs(array("class" => "select_field form-control", 'id' => 'service_field'));
        $service->setValue((isset($imageAttachment['service_id']) ? $imageAttachment['service_id'] : ''));
        $service->addMultiOption('', 'Select One');
        $service->addMultiOptions($allService);

        $floor = new Zend_Form_Element_Select('floor_id');
        if ($options['type'] == 'service') {

            if (!empty($imageAttachment['floor_id'])) {
                $floor_value = $imageAttachment['floor_id'];
            } else {
                $floor_value = 0;
            }


            $model_attributeListValue = new Model_AttributeListValue();
            $floorData = $model_attributeListValue->getAllFloorByServiceID($imageAttachment['service_id']);
            $floor_array = "";
            foreach ($floorData as $floor) {
                $floor_array[$floor['attribute_value_id']] = $floor['attribute_value'];
            }


            
            $floor->setDecorators(array('ViewHelper'));
            $floor->setAttribs(array("class" => "select_field form-control"));
            $floor->setValue($floor_value);
            $floor->addMultiOption('', 'Select One');
            $floor->addMultiOptions($floor_array);
            $floor->setRequired();
            $floor->setErrorMessages(array('Required' => 'Please select Floor '));
            $floor->addDecorator('Errors', array('class' => 'errors'));
        }


        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));

        $this->addElements(array($service, $button, $floor));
        $this->setMethod(Zend_Form::METHOD_POST);
        $this->setEnctype(Zend_Form::ENCTYPE_MULTIPART);

        $this->setAction($router->assemble(array('id' => $imageAttachment['image_id'], 'type' => $options['type'], 'itemid' => $options['itemId']), 'ItemImageAttachmentEdit'));
    }

}
