<?php

class Form_UnapprovedFilters extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('filters');
        $actionUrl = (isset($options['action_url']) ? $options['action_url'] : '');
        $typeValue = (isset($options['type']) ? $options['type'] : '');

        $request = Zend_Controller_Front::getInstance()->getRequest();
        $filters = $request->getParam('fltr', array());

        $contractor = new Zend_Form_Element_Select('contractor_id');
        $contractor->setBelongsTo('fltr');
        $contractor->setDecorators(array('ViewHelper'));
        $contractor->setValue(isset($filters['service_history_contractor_id']) ? $filters['service_history_contractor_id'] : '');
        $contractor->setAttribs(array('class' => 'form-control'));
        $contractor->addMultiOption('', 'Select One');
        $modelUser = new Model_User();
        $options = $modelUser->getAllContractor(true);
        $contractor->addMultiOptions($options);
		
		

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Search');
        $button->setAttribs(array('class' => 'btn btn-primary'));

       
        $this->addElements(array($contractor, $button));
        $this->setMethod('POST');

        $this->setAction($actionUrl);
    }

}

