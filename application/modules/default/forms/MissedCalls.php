<?php

class Form_MissedCalls extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('MissedCalls');

        $router = Zend_Controller_Front::getInstance()->getRouter();
        $request = Zend_Controller_Front::getInstance()->getRequest();

        $customer = new Zend_Form_Element_Text('auto_customer_id');
        $customer->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                //->setRequired()
                ->setAttribs(array('class' => 'form-control' , 'id'=>'auto_customer_id' ,'onkeyup'=> 'customerSearchInput()'));
        //->setValue((!empty($user['username']) ? $user['username'] : ''));


        $modelUser = new Model_User();
        $allEmployee = $modelUser->getAllEmployee(true);

        $employee = new Zend_Form_Element_Select('employee_id');
        $employee->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->addMultiOption('', 'Select One')
                ->addMultiOptions($allEmployee);
				

        $comment = new Zend_Form_Element_Textarea('comment');
        $comment->setDecorators(array('ViewHelper', 'Errors'))
                ->setAttribs(array('class' => 'form-control'))
                 ->setRequired();

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Send');
        $button->setAttribs(array('class' => 'btn btn-primary'));


        $this->setMethod('post');

        $this->addElements(array($customer, $employee,$comment, $button));
        $this->setAction($router->assemble(array(), 'missedCallAdd'));
    }

}

