public function getAllByItem($id) {
	  $id = (int) $id;
	  $select = $this->getAdapter()->select();
	  $select->from(array('s' => 'service'));
	  $select->joinInner(array('c' => 'contractor_service_booking'), 's.service_id = c.service_id');
	  $select->where("c.booking_id= '{$id}'");
	  return   $this->getAdapter()->fetchAll($select);

	 }
	 
	 public function getAllForEstimate($id) {
	  $id = (int) $id;
	  $select = $this->getAdapter()->select();
      $select->from(array('s' => 'service'));
	  $select->joinInner(array('c' => 'contractor_service_booking'), 's.service_id = c.service_id');
	  $select->joinInner(array('be' => 'booking_estimate'), 'be.booking_id = c.booking_id');
	  $select->where("be.id= '{$id}'");
	  $services = $this->getAdapter()->fetchAll($select);
	  
	  $data = array();
        foreach ($services as $service) {
            $data[$service['service_id']] = $service['service_name'];
        }
		return $data;
		
	 }
	 
	 public function getAllByItemAsArray($id){
	  
	   $services = $this->getAllByItem($id);
	   $data = array();
        foreach ($services as $service) {
            $data[$service['service_id']] = $service['service_name'];
        }
		return $data;
	 }