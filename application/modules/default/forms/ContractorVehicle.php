<?php

class Form_ContractorVehicle extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('ContractorVehicle');
        $contractor_info_id = (isset($options['contractor_info_id']) ? $options['contractor_info_id'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();

        $registrationNumber = new Zend_Form_Element_Text('registration_number');
        $registrationNumber->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setErrorMessages(array('Required' => 'Please enter the registration number'));

        $make = new Zend_Form_Element_Text('make');
        $make->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setErrorMessages(array('Required' => 'Please enter the make'));

        $model = new Zend_Form_Element_Text('model');
        $model->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setErrorMessages(array('Required' => 'Please enter the Model'));

        $colour = new Zend_Form_Element_Text('colour');
        $colour->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setErrorMessages(array('Required' => 'Please enter the Colour'));


        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));

        $this->addElements(array($registrationNumber, $make, $model, $colour, $button));
        $this->setMethod('post');
        $this->setAction($router->assemble(array(), 'contractorVehicleAdd'));
    }

}

