Add  those functions 

	public function deleteByImageId($id) {
        $id = (int) $id;
        return parent::delete("image_id = '{$id}'");
    }
	
	
	public function getByImageId($id, $order = 'asc') {
        
		$id = (int) $id;
		$typeFilter = $this->getAdapter()->quote('booking_discussion' . '%');	
        $select = $this->getAdapter()->select();
        $select->from(array('bd'=>$this->_name));
		$select->order("bd.created {$order}");
        $select->where("bd.image_id = '{$id}'");
  		
        return $this->getAdapter()->fetchAll($select);
    }
	
	
	---------------------------------------------------------
	
	update this one :
	
	public function getByBookingId($id, $order = 'asc' , $filter = array()) {
        $id = (int) $id;

		$typeFilter = $this->getAdapter()->quote('booking_discussion' . '%');	
        $select = $this->getAdapter()->select();
        $select->from(array('bd'=>$this->_name));
		$select->joinLeft(array('im' => 'image'), "(im.image_id = bd.image_id ) ",array('im.thumbnail_path'));
		if(isset($filter['groups'])){
		 $select->where("bd.group_id > 0 ");
		 $select->group("bd.group_id");
		}else{		
        $select->where("bd.group_id = 0 ");
		}
		
		$select->order("bd.created {$order}");
        $select->where("booking_id = '{$id}'");
 
        //echo $select->__toString();
        //exit;
		
        return $this->getAdapter()->fetchAll($select);
    }