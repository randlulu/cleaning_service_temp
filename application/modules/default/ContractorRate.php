<?php

class Model_ContractorRate extends Zend_Db_Table_Abstract {

    protected $_name = 'contractor_rate';

    public function getAll() {
        $select = $this->getAdapter()->select();
        $select->from(array('cr' => $this->_name));
//        $select->joinLeft(array('rt'=>'rating_tag'), 'cr.rating_tag_id = rt.rating_tag_id');
        $rates = $this->getAdapter()->fetchAll($select);
        
        return $rates;
    }

    public function insert(array $data) {
        $id = parent::insert($data);
        return $id;
    }

    public function getByContractorIdAndRatedByAndItemIdAndTagId($contractor_id, $rated_by, $item_id, $tag_id = 0) {
        $contractor_id = (int) $contractor_id;
        $rated_by = (int) $rated_by;
        $item_id = (int) $item_id;
        $select = $this->getAdapter()->select();
        $select->from(array('cr' => $this->_name));
        $select->where("cr.contractor_id = $contractor_id");
        $select->where("cr.rated_by = $rated_by");
        $select->where("cr.item_id = $item_id");
        if ($tag_id) {
            $select->where("cr.rating_tag_id = $tag_id");
        }
        $rates = $this->getAdapter()->fetchAll($select);
        return $rates;
    }

    public function updateById($id, $data) {
        $id = (int) $id;
        $success = parent::update($data, "contractor_rate_id = '{$id}'");
        return $success;
    }

}
