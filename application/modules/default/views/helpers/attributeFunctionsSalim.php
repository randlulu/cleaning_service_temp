<?php

function drowAttribute($attribute, $bookingId = 0, $inquiryId = 0, $clone = 0, $isTempValue = false, $disabled = false, $style = '') {
    $modelAttributeType = new Model_AttributeType();
    $attributeType = $modelAttributeType->getById($attribute['attribute_type_id']);

    $modelServiceAttribute = new Model_ServiceAttribute();
    $serviceAttribute = $modelServiceAttribute->getByAttributeIdAndServiceId($attribute['attribute_id'], $attribute['service_id']);
    $readonly = isset($serviceAttribute['is_readonly']) && $serviceAttribute['is_readonly'] ? true : false;

    $name = 'attribute_' . $attribute['service_id'] . $attribute['attribute_id'] . ($clone ? '_' . $clone : '');

    //refiller from booking
    if ($bookingId) {
        if (!$isTempValue) {
            $modelServiceAttributeValue = new Model_ServiceAttributeValue();
            $serviceAttributeValue = $modelServiceAttributeValue->getByBookingIdAndServiceAttributeIdAndClone($bookingId, $serviceAttribute['service_attribute_id'], $clone);
            $value = isset($serviceAttributeValue['value']) ? $serviceAttributeValue['value'] : '';
            $is_serialized_array = isset($serviceAttributeValue['is_serialized_array']) ? $serviceAttributeValue['is_serialized_array'] : 0;
        } else {
            $modelServiceAttributeValueTemp = new Model_ServiceAttributeValueTemp();
            $serviceAttributeValueTemp = $modelServiceAttributeValueTemp->getByBookingIdAndServiceAttributeIdAndClone($bookingId, $serviceAttribute['service_attribute_id'], $clone);
            $value = isset($serviceAttributeValueTemp['value']) ? $serviceAttributeValueTemp['value'] : '';
            $is_serialized_array = isset($serviceAttributeValueTemp['is_serialized_array']) ? $serviceAttributeValueTemp['is_serialized_array'] : 0;
        }
        if ($is_serialized_array) {
            $value = unserialize($value);
        }
    }

    //refiller from inquiry
    if ($inquiryId) {
        $modelInquiryServiceAttributeValue = new Model_InquiryServiceAttributeValue();
        $inquiryServiceAttributeValue = $modelInquiryServiceAttributeValue->getByInquiryIdAndServiceAttributeIdAndClone($inquiryId, $serviceAttribute['service_attribute_id'], $clone);

        $value = isset($inquiryServiceAttributeValue['value']) ? $inquiryServiceAttributeValue['value'] : '';

        $is_serialized_array = isset($inquiryServiceAttributeValue['is_serialized_array']) ? $inquiryServiceAttributeValue['is_serialized_array'] : 0;

        if ($is_serialized_array) {
            $value = unserialize($value);
        }
    }

    //refiller from default value
    if (empty($value)) {
        $value = isset($serviceAttribute['default_value']) ? $serviceAttribute['default_value'] : '';
    }

    //refiller from post
    $valuePOST = isset($_POST[$name]) ? $_POST[$name] : '';
    if ($value != $valuePOST && $valuePOST) {
        $value = $valuePOST;
    }

	$type = $attributeType['attribute_type'];
    $listOptions = array();
    if ($attributeType['is_list']) {
	  if(isset($attribute['attribute_type']) && $attribute['attribute_type'] == 'for_customer'){
	    $type = 'slide_image';
	  }
        $modelAttributeListValue = new Model_AttributeListValue();
        $attributeListValue = $modelAttributeListValue->getByAttributeId($attribute['attribute_id']);

        if ($attributeListValue) {
            foreach ($attributeListValue as $attributeValue) {
                $listOptions[$attributeValue['attribute_value_id']] = $attributeValue['attribute_value'];
            }
        }
    }

    

    if ($attribute['is_price_attribute']) {
        $onchange = 'countServicePrice(' . $attribute['service_id'] . ($clone ? ',' . $clone : '') . ')';
    } else {
        $onchange = '';
    }

    $id = '';
    $class = '';
    if ('quantity' == $attribute['attribute_variable_name'] || 'discount' == $attribute['attribute_variable_name']) {
        $class = 'form-control text_input ' . $attribute['attribute_variable_name'] . '_attribute';
    }

    return renderHtmlFormElement($type, $name, $value, $readonly, $listOptions, $onchange, $disabled, $style, $id, $class);
}

function drowInquiryTypeAttribute($attribute, $inquiryId = 0) {
    $modelAttributeType = new Model_AttributeType();
    $attributeType = $modelAttributeType->getById($attribute['attribute_type_id']);

    //use the inquiry_type_attribute table
    $modelInquiryTypeAttribute = new Model_InquiryTypeAttribute();
    $inquiryTypeAttribute = $modelInquiryTypeAttribute->getByAttributeIdInquiryTypeId($attribute['attribute_id'], $attribute['inquiry_type_id']);

    $name = 'attribute_' . $attribute['attribute_id'];

    //refiller from inquiry
    if ($inquiryId) {
        $modelInquiryTypeAttributeValue = new Model_InquiryTypeAttributeValue();
        $inquiryTypeAttributeValue = $modelInquiryTypeAttributeValue->getByInquiryIdAndInquiryTypeAttributeId($inquiryId, $inquiryTypeAttribute['inquiry_type_attribute_id']);

        $value = isset($inquiryTypeAttributeValue['value']) ? $inquiryTypeAttributeValue['value'] : '';

        $is_serialized = isset($inquiryTypeAttributeValue['is_serialized']) ? $inquiryTypeAttributeValue['is_serialized'] : 0;

        if ($is_serialized) {
            $value = unserialize($value);
        }
    }

    //refiller from default value
    if (empty($value)) {
        $value = '';
    }

    //refiller from post
    $valuePOST = isset($_POST[$name]) ? $_POST[$name] : '';
    if ($value != $valuePOST && $valuePOST) {
        $value = $valuePOST;
    }

    $listOptions = array();
    if ($attributeType['is_list']) {
        $modelAttributeListValue = new Model_AttributeListValue();
        $attributeListValue = $modelAttributeListValue->getByAttributeId($attribute['attribute_id']);

        if ($attributeListValue) {
            foreach ($attributeListValue as $attributeValue) {
                $listOptions[$attributeValue['attribute_value_id']] = $attributeValue['attribute_value'];
            }
        }
    }

    $readonly = false;

    $type = $attributeType['attribute_type'];

    return renderHtmlFormElement($type, $name, $value, $readonly, $listOptions);
}

function renderHtmlFormElement($type, $name, $value = '', $readonly = false, $listOptions = array(), $onchange = '', $disabled = false, $style = '', $id = '', $class = '') {

    $drowAttribute = '';

    switch ($type) {

        case 'checkbox': {
                $checkbox = new Zend_Form_Element_Checkbox($name);
                $checkbox->setDecorators(array('ViewHelper'));

                $checkbox->setAttrib('class', $class ? $class : 'checkbox');

                if ($value) {
                    $checkbox->setValue($value);
                }
                if ($style) {
                    $checkbox->setAttrib('style', $style);
                }
                if ($id) {
                    $checkbox->setAttrib('id', $id);
                }
                if ($onchange) {
                    $checkbox->setAttrib('onchange', $onchange);
                }
                if ($readonly) {
                    $checkbox->setAttrib('readonly', 'readonly');
                }
                if ($disabled) {
                    $checkbox->setAttrib('disabled', 'disabled');
                }
                $drowAttribute = $checkbox->render();
                break;
            }

        case 'date': {
                $date = new Zend_Form_Element_Text($name);
                $date->setDecorators(array('ViewHelper'));

                $date->setAttrib('class', $class ? $class : 'date form-control');

                if ($value) {
                    $date->setValue($value);
                }
                if ($style) {
                    $date->setAttrib('style', $style);
                }
                if ($id) {
                    $date->setAttrib('id', $id);
                }
                if ($onchange) {
                    $date->setAttrib('onchange', $onchange);
                }
                $readonly = true;
                if ($readonly) {
                    $date->setAttrib('readonly', 'readonly');
                }
                if ($disabled) {
                    $date->setAttrib('disabled', 'disabled');
                }

                $drowAttribute = $date->render();

                $id = $id ? $id : $name;

                $drowAttribute .='<script type="text/javascript">';
                $drowAttribute .='$(document).ready(function() {';
                $drowAttribute .='$("#' . $id . '").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true });';
                $drowAttribute .='});';
                $drowAttribute .='</script>';

                break;
            }

        case 'time': {
                $time = new Zend_Form_Element_Text($name);
                $time->setDecorators(array('ViewHelper'));

                $time->setAttrib('class', $class ? $class : 'time form-control');

                if ($value) {
                    $time->setValue($value);
                }
                if ($style) {
                    $time->setAttrib('style', $style);
                }
                if ($id) {
                    $time->setAttrib('id', $id);
                }
                if ($onchange) {
                    $time->setAttrib('onchange', $onchange);
                }
                $readonly = true;
                if ($readonly) {
                    $time->setAttrib('readonly', 'readonly');
                }
                if ($disabled) {
                    $time->setAttrib('disabled', 'disabled');
                }

                $drowAttribute = $time->render();

                $id = $id ? $id : $name;

                $drowAttribute .='<script type="text/javascript">';
                $drowAttribute .='$(document).ready(function() {';
                $drowAttribute .='$("#' . $id . '").timepicker({ timeFormat: "hh:mm" });';
                $drowAttribute .='});';
                $drowAttribute .='</script>';

                break;
            }

        case 'long_text': {
                $long_text = new Zend_Form_Element_Textarea($name);
                $long_text->setDecorators(array('ViewHelper'));

                $long_text->setAttrib('class', $class ? $class : 'long_text form-control');

                if ($value) {
                    $long_text->setValue($value);
                }
                if ($style) {
                    $long_text->setAttrib('style', $style);
                }
                if ($id) {
                    $long_text->setAttrib('id', $id);
                }
                if ($onchange) {
                    $long_text->setAttrib('onchange', $onchange);
                }
                if ($readonly) {
                    $long_text->setAttrib('readonly', 'readonly');
                }
                if ($disabled) {
                    $long_text->setAttrib('disabled', 'disabled');
                }

                $drowAttribute = $long_text->render();
                break;
            }

        case 'text_input': {
                $text_input = new Zend_Form_Element_Text($name);
                $text_input->setDecorators(array('ViewHelper'));

                $text_input->setAttrib('class', $class ? $class : 'text_input form-control');

                if ($value) {
                    $text_input->setValue($value);
                }
                if ($style) {
                    $text_input->setAttrib('style', $style);
                }
                if ($id) {
                    $text_input->setAttrib('id', $id);
                }
                if ($onchange) {
                    $text_input->setAttrib('onchange', $onchange);
                }
                if ($readonly) {
                    $text_input->setAttrib('readonly', 'readonly');
                }
                if ($disabled) {
                    $text_input->setAttrib('disabled', 'disabled');
                }

                $drowAttribute = $text_input->render();
                break;
            }

        case 'enum': {
                $enum = new Zend_Form_Element_Radio($name);
                $enum->setDecorators(array('ViewHelper'));

                $enum->setAttrib('class', $class ? $class : 'enum form-control');

                if ($value) {
                    $enum->setValue($value);
                }
                if ($style) {
                    $enum->setAttrib('style', $style);
                }
                if ($id) {
                    $enum->setAttrib('id', $id);
                }
                if ($onchange) {
                    $enum->setAttrib('onchange', $onchange);
                }
                if ($readonly) {
                    $enum->setAttrib('readonly', 'readonly');
                }
                if ($disabled) {
                    $enum->setAttrib('disabled', 'disabled');
                }
                if ($listOptions) {
                    $enum->addMultiOptions($listOptions);
                }

                $drowAttribute = $enum->render();
                break;
                break;
            }

        case 'set': {
                $set = new Zend_Form_Element_MultiCheckbox($name);
                $set->setDecorators(array('ViewHelper'));

                $set->setAttrib('class', $class ? $class : 'checkbox');

                if ($value) {
                    $set->setValue($value);
                }
                if ($style) {
                    $set->setAttrib('style', $style);
                }
                if ($id) {
                    $set->setAttrib('id', $id);
                }
                if ($onchange) {
                    $set->setAttrib('onchange', $onchange);
                }
                if ($readonly) {
                    $set->setAttrib('readonly', 'readonly');
                }
                if ($disabled) {
                    $set->setAttrib('disabled', 'disabled');
                }
                if ($listOptions) {
                    $set->addMultiOptions($listOptions);
                }

                $drowAttribute = $set->render();
                break;
            }
			
		case 'slide_image' : {
		
		$attributeListValueAttachmentObj = new Model_AttributeListValueAttachment();  
		  
		  $element =  '<div class="slider_container2">
		  <input type="hidden" name="'.$name.'" />
		  <ul id="lightSlider">';
		 
		 foreach ($listOptions as $key=>$listOption){
		    $attribute_value_images = $attributeListValueAttachmentObj->getByAttributeValueId($key,false,true);
            $attributeValueImages = array();
            if($attribute_value_images){
			  $parts = explode("public",$attribute_value_images[0]['path']);
              $defualt_image = $parts['1'];                               
            }else{
              $defualt_image = '/pic/attribute_pics/default_image.png';
            }
		   
		    $element .= '<li >
		  <div class="option-image">
		    <img src="'.$defualt_image.'" width="110px" height="100px"></div>
			<div class="option-text ellipsis" style="text-align:center" id="'.$key.'">'.$listOption.'</div>
		  </li>';
		 }
		 
		  
		  
		  $element .= '</ul></div>';
		  $drowAttribute = $element;
		  break;
		}	

        case 'dropdown': {

                $dropdown = new Zend_Form_Element_Select($name);
                $dropdown->setDecorators(array('ViewHelper'));

                $dropdown->setAttrib('class', $class ? $class .' list_attribute' : 'dropdown form-control list_attribute');

                if ($value) {
                    $dropdown->setValue($value);
                }
                if ($style) {
                    $dropdown->setAttrib('style', $style);
                }
                if ($id) {
                    $dropdown->setAttrib('id', $id);
                }
                if ($onchange) {
                    $dropdown->setAttrib('onchange', $onchange);
                }
                if ($readonly) {
                    $dropdown->setAttrib('readonly', 'readonly');
                }
                if ($disabled) {
                    $dropdown->setAttrib('disabled', 'disabled');
                }
                $dropdown->addMultiOption("", "Select One");
                if ($listOptions) {
                    $dropdown->addMultiOptions($listOptions);
                }

                $drowAttribute = $dropdown->render();
                break;
            }

        case 'list': {

                $list = new Zend_Form_Element_Multiselect($name);
                $list->setDecorators(array('ViewHelper'));

                $list->setAttrib('class', $class ? $class : 'list form-control');

                if ($value) {
                    $list->setValue($value);
                }
                if ($style) {
                    $list->setAttrib('style', $style);
                }
                if ($id) {
                    $list->setAttrib('id', $id);
                }
                if ($onchange) {
                    $list->setAttrib('onchange', $onchange);
                }
                if ($readonly) {
                    $list->setAttrib('readonly', 'readonly');
                }
                if ($disabled) {
                    $list->setAttrib('disabled', 'disabled');
                }
                if ($listOptions) {
                    $list->addMultiOptions($listOptions);
                }

                $drowAttribute = $list->render();
                break;
            }
    }
    return $drowAttribute;
}

function createPriceEquasionFunction($methodName, $equasion) {
    $equasion = str_replace('[', '$params[\'', $equasion);
    $equasion = str_replace(']', '\']', $equasion);

    $function = "function $methodName(\$params){return $equasion;}";

    eval($function);
}