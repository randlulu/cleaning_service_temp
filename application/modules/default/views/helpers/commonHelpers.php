<?php

function js2PhpTime($jsdate) {
    $ret = 0;
    if (preg_match('@(\d+)/(\d+)/(\d+)\s+(\d+):(\d+)@', $jsdate, $matches) == 1) {
        $ret = mktime($matches[4], $matches[5], 0, $matches[1], $matches[2], $matches[3]);
        // echo $matches[4] ."-". $matches[5] ."-". 0  ."-". $matches[1] ."-". $matches[2] ."-". $matches[3];
    } else if (preg_match('@(\d+)/(\d+)/(\d+)@', $jsdate, $matches) == 1) {
        $ret = mktime(0, 0, 0, $matches[1], $matches[2], $matches[3]);
        //echo 0 ."-". 0 ."-". 0 ."-". $matches[1] ."-". $matches[2] ."-". $matches[3];
    }
    return $ret;
}

function php2JsTime($phpDate) {
    //echo $phpDate;
    //return "/Date(" . $phpDate*1000 . ")/";
    return date("m/d/Y H:i", $phpDate);
}

function php2MySqlTime($phpDate) {
    return date("Y-m-d H:i:s", $phpDate);
}

function mySql2PhpTime($sqlDate) {
    $arr = date_parse($sqlDate);
    return mktime($arr["hour"], $arr["minute"], $arr["second"], $arr["month"], $arr["day"], $arr["year"]);
}

function br2nl($string) {
    $return = preg_replace('/<br ?\/?>/i', "\n", $string);
    return $return;
}

function spaceBeforeCapital($string) {
    return preg_replace('/(?<!\ )[A-Z]/', ' $0', $string);
}

function get_config($key, $sections = array()) {
    // default config
    $sections[] = 'common_config';
    $sections[] = 'path_config';

    $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/config.ini', $sections);

    return $config->$key;
}

/* * ***Original****** */
/* function getDateFormating($phpDate, $enabled = true) {

  if (!is_int($phpDate)) {
  $phpDate = strtotime($phpDate);
  }

  if ($enabled) {
  $today = strtotime(date("Y-m-d", $phpDate));

  if ($today == strtotime(date("Y-m-d"))) {
  return 'Today, ' . date("g:i a", $phpDate);
  } else {
  return date("F j ,Y, g:i a", $phpDate);
  }
  } else {
  return date("F j ,Y", $phpDate);
  }
  } */
/* * ********* */

function getDateFormating($phpDate, $enabled = true) {
    if (is_numeric($phpDate)) {
        $phpDate = (int) $phpDate;
    }

    if (!is_int($phpDate)) {
        $phpDate = strtotime($phpDate);
    }

    $companyId = CheckAuth::getCompanySession();
    $dateTimeModel = new Model_DateTime();
    $dateTimeObj = $dateTimeModel->getByCompanyId($companyId);
    $countryModel = new Model_Countries();

    $settingsFlag = false;
    if (isset($dateTimeObj) && !empty($dateTimeObj)) {
        $country = $countryModel->getById($dateTimeObj['country_id']);
        $locale = Zend_Locale::getLocaleToTerritory($country['country_code']);
        $dateFormat = $dateTimeObj['date_format'];
        $timeFormat = $dateTimeObj['time_format'];
        $timeZone = $dateTimeObj['time_zone'];
        // echo $timeZone;
        date_default_timezone_set($timeZone);
        if (is_int($phpDate)) {
            $phpDateAsString = date('Y-m-d g:i a', $phpDate);
        }
        $date = new Zend_Date($phpDateAsString, false, $locale);
        $settingsFlag = true;
    }

    if ($enabled) {
        $today = strtotime(date("Y-m-d", $phpDate));

        if ($today == strtotime(date("Y-m-d"))) {
            if ($settingsFlag)
                return 'Today, ' . $date->toString($timeFormat);
            else
                return 'Today, ' . date("g:i a", $phpDate);
        } else {
            if ($settingsFlag)
                return $date->toString($dateFormat) . ' ' . $date->toString($timeFormat);
            else
                return date("F j ,Y, g:i a", $phpDate);
        }
    } else {
        if ($settingsFlag)
            return $date->toString($dateFormat);
        else
            return date("F j ,Y", $phpDate);
    }
}

/* * *****New one*******IBM */

// function getDateFormatingByDateTimeConfigration($phpDate, $enabled = true) { 
/* function getDateFormating($phpDate, $enabled = true) {

  $companyId = CheckAuth::getCompanySession();
  $dateTimeModel = new Model_DateTime();
  $dateTimeObj = $dateTimeModel->getByCompanyId($companyId);
  $countryModel = new Model_Countries();

  if(isset($dateTimeObj) && !empty($dateTimeObj)){
  $country = $countryModel->getById($dateTimeObj['country_id']);
  $locale = Zend_Locale::getLocaleToTerritory($country['country_code']);
  $dateFormat = $dateTimeObj['date_format'];
  $timeFormat = $dateTimeObj['time_format'];
  $timeZone =   $dateTimeObj['time_zone'];
  // echo $timeZone;
  date_default_timezone_set($timeZone);
  } else{
  $companyModel = new Model_Companies;
  $cityModel    = new Model_Cities;
  $companyObj   = $companyModel->getById($companyId);
  $cityObj      = $cityModel ->getById($companyObj['city_id']);
  // $country_id   = $cityObj['country_id'];
  $country = $countryModel->getById($cityObj['country_id']);
  $locale = Zend_Locale::getLocaleToTerritory($country['country_code']);
  $dateFormat = "EEEE, MMMM d, y";//Zend_Locale_Format::STANDARD;
  $timeFormat = "h:mm";//Zend_Locale_Format::STANDARD;
  }


  if (!is_int($phpDate)) {
  $phpDate = strtotime($phpDate);
  }
  $date = new Zend_Date($phpDate, false, $locale);

  if ($enabled) {
  $today = strtotime(date("Y-m-d", $phpDate));

  if ($today == strtotime(date("Y-m-d"))) {
  // return 'Today, ' . date("g:i a", $phpDate);
  return 'Today, ' . $date->toString($timeFormat);
  } else {
  // return date("F j ,Y, g:i a", $phpDate);
  return $date->toString($dateFormat) . ' ' . $date->toString($timeFormat);
  // return $date->toString($dateFormat);
  }
  } else {
  // return date("F j ,Y", $phpDate);
  return $date->toString($dateFormat);
  }
  } */

/* * ******End****** */


/*
  function getBookingDateFormating($SDate,$EDate){
  $yesterDay = strtotime('yesterday');
  $today = strtotime('today');
  $tomorrow = strtotime('tomorrow');
  $SDate = strtotime($SDate);
  $EDate = strtotime($EDate);
  $Start = strtotime(date("Y-m-d", $SDate));
  $End = strtotime(date("Y-m-d", $EDate));
  $startYear = strtotime(date("Y", $SDate));
  $endYear = strtotime(date("Y", $EDate));
  if(($Start == $End) && ($today == $Start)){
  return 'Today at ' . date("g:i a", $SDate). ' - '.date("g:i a", $EDate);
  }else if ($Start == $End){
  return date("F j, Y, g:i a", $SDate) . ' - '.date("g:i a", $EDate);
  }else if(($Start == $yesterDay) && ($End == $today)){
  return 'Yesterday at ' .date("g:i a", $SDate).' - '.'Today at ' .date("g:i a", $EDate);
  }else if(($Start == $today) && ($End == $tomorrow)){
  return 'Today at ' .date("g:i a", $SDate).' - '.'Tomorrow at ' .date("g:i a", $EDate);
  }else if($startYear == $endYear){
  return date("F j,  g:i a", $SDate) . ' - '.date("F j,  g:i a", $EDate);
  }else{
  return date("F j, Y,  g:i a", $SDate) . ' - '.date("F j, Y,  g:i a", $EDate);
  }
  } */
/* * ******Orginal********** */
/* function getBookingDateFormating($SDate, $EDate) {
  $yesterDay = strtotime('yesterday');
  $today = strtotime('today');
  $tomorrow = strtotime('tomorrow');
  $SDate = strtotime($SDate);
  $EDate = strtotime($EDate);
  $Start = strtotime(date("Y-m-d", $SDate));
  $End = strtotime(date("Y-m-d", $EDate));
  $startYear = strtotime(date("Y", $SDate));
  $endYear = strtotime(date("Y", $EDate));
  if (($Start == $End) && ($today == $Start)) {
  return 'Today at ' . date("g:i a", $SDate) . ' - ' . date("g:i a", $EDate);
  } else if ($Start == $End) {
  return date('l', $SDate) . '  ' . date("F j, Y, g:i a", $SDate) . ' - ' . date("g:i a", $EDate);
  } else if (($Start == $yesterDay) && ($End == $today)) {
  return 'Yesterday at ' . date("g:i a", $SDate) . ' - ' . 'Today at ' . date("g:i a", $EDate);
  } else if (($Start == $today) && ($End == $tomorrow)) {
  return 'Today at ' . date("g:i a", $SDate) . ' - ' . 'Tomorrow at ' . date("g:i a", $EDate);
  } else if ($startYear == $endYear) {
  return date('l', $SDate) . '  ' . date("F j,  g:i a", $SDate) . ' - ' . date('l', $EDate) . '  ' . date("F j,  g:i a", $EDate);
  } else {
  return date('l', $SDate) . '  ' . date("F j, Y,  g:i a", $SDate) . ' - ' . date('l', $EDate) . '  ' . date("F j, Y,  g:i a", $EDate);
  }
  } */
/* * ****End***** */

function getBookingDateFormating($SDate, $EDate) {
    $yesterDay = strtotime('yesterday');
    $today = strtotime('today');
    $tomorrow = strtotime('tomorrow');
    $SDate = strtotime($SDate);
    $EDate = strtotime($EDate);
    $Start = strtotime(date("Y-m-d", $SDate));
    $End = strtotime(date("Y-m-d", $EDate));
    $startYear = strtotime(date("Y", $SDate));
    $endYear = strtotime(date("Y", $EDate));

    $companyId = CheckAuth::getCompanySession();
    $dateTimeModel = new Model_DateTime();
    $dateTimeObj = $dateTimeModel->getByCompanyId($companyId);
    $countryModel = new Model_Countries();

    $settingsFlag = false;
    if (isset($dateTimeObj) && !empty($dateTimeObj)) {
        $country = $countryModel->getById($dateTimeObj['country_id']);
        $locale = Zend_Locale::getLocaleToTerritory($country['country_code']);
        $dateFormat = $dateTimeObj['date_format'];
        $timeFormat = $dateTimeObj['time_format'];
        $timeZone = $dateTimeObj['time_zone'];
        date_default_timezone_set($timeZone);
        $sdate = new Zend_Date($SDate, false, $locale);
        $edate = new Zend_Date($EDate, false, $locale);
        $settingsFlag = true;
    }

    if (($Start == $End) && ($today == $Start)) {
        if ($settingsFlag)
            return 'Today at ' . $sdate->toString($timeFormat) . ' - ' . $edate->toString($timeFormat);
        else
            return 'Today at ' . date("g:i a", $SDate) . ' - ' . date("g:i a", $EDate);
    } else if ($Start == $End) {
        if ($settingsFlag)
            return $sdate->toString($dateFormat) . ' ' . $sdate->toString($timeFormat) . ' - ' . $edate->toString($timeFormat);
        else
            return date('l', $SDate) . '  ' . date("F j, Y, g:i a", $SDate) . ' - ' . date("g:i a", $EDate);
    } else if (($Start == $yesterDay) && ($End == $today)) {
        if ($settingsFlag)
            return 'Yesterday at ' . $sdate->toString($timeFormat) . ' - ' . 'Today at ' . $edate->toString($timeFormat);
        else
            return 'Yesterday at ' . date("g:i a", $SDate) . ' - ' . 'Today at ' . date("g:i a", $EDate);
    } else if (($Start == $today) && ($End == $tomorrow)) {
        if ($settingsFlag)
            return 'Today at ' . $sdate->toString($timeFormat) . ' - ' . 'Tomorrow at ' . $edate->toString($timeFormat);
        else
            return 'Today at ' . date("g:i a", $SDate) . ' - ' . 'Tomorrow at ' . date("g:i a", $EDate);
    } else if ($startYear == $endYear) {
        if ($settingsFlag)
            return $sdate->toString($dateFormat) . ' ' . $sdate->toString($timeFormat) . ' - ' . $edate->toString($dateFormat) . ' ' . $edate->toString($timeFormat);
        else
            return date('l', $SDate) . '  ' . date("F j,  g:i a", $SDate) . ' - ' . date('l', $EDate) . '  ' . date("F j,  g:i a", $EDate);
    } else {
        if ($settingsFlag)
            return $sdate->toString($dateFormat) . ' ' . $sdate->toString($timeFormat) . ' - ' . $edate->toString($dateFormat) . ' ' . $edate->toString($timeFormat);
        else
            return date('l', $SDate) . '  ' . date("F j, Y,  g:i a", $SDate) . ' - ' . date('l', $EDate) . '  ' . date("F j, Y,  g:i a", $EDate);
    }
}

/* * ****New******IBM */
// function getBookingDateFormatByDateTimeConfigration($SDate, $EDate){
/* function getBookingDateFormating($SDate, $EDate){
  $companyId = CheckAuth::getCompanySession();
  $dateTimeModel = new Model_DateTime();
  $dateTimeObj = $dateTimeModel->getByCompanyId($companyId);
  $countryModel = new Model_Countries();

  // var_dump($dateTimeObj);

  if(isset($dateTimeObj) && !empty($dateTimeObj)){
  $country = $countryModel->getById($dateTimeObj['country_id']);
  $locale = Zend_Locale::getLocaleToTerritory($country['country_code']);
  $dateFormat = $dateTimeObj['date_format'];
  $timeFormat = $dateTimeObj['time_format'];
  $timeZone =   $dateTimeObj['time_zone'];
  // echo $timeZone;
  date_default_timezone_set($timeZone);

  // $localeObj = new Zend_Locale($locale);
  } else{
  $companyModel = new Model_Companies;
  $cityModel    = new Model_Cities;
  $companyObj   = $companyModel->getById($companyId);
  $cityObj      = $cityModel ->getById($companyObj['city_id']);
  // $country_id   = $cityObj['country_id'];
  $country = $countryModel->getById($cityObj['country_id']);
  $locale = Zend_Locale::getLocaleToTerritory($country['country_code']);
  $dateFormat = "EEEE, MMMM d, y";//Zend_Locale_Format::STANDARD;
  $timeFormat = "h:mm";//Zend_Locale_Format::STANDARD;
  // $currentTimeZone = date_default_timezone_get();
  // date_default_timezone_set($currentTimeZone);
  //         echo $locale;
  // echo $dateFormat;
  }

  $yesterDay = strtotime('yesterday');
  $today = strtotime('today');
  $tomorrow = strtotime('tomorrow');
  $SDate = strtotime($SDate);
  $EDate = strtotime($EDate);
  $Start = strtotime(date("Y-m-d", $SDate));
  $End = strtotime(date("Y-m-d", $EDate));
  $startYear = strtotime(date("Y", $SDate));
  $endYear = strtotime(date("Y", $EDate));



  // $locale = new Zend_Locale('de_AT'); //2012-05-13 09:00:00       //13.04.2005 22:14:55
  // $date = Zend_Locale_Format::getDate('2012-05-13 09:00:00', array('date_format' =>  $dateFormat, 'locale' => $locale));
  // $date = Zend_Locale_Format::getDate($Date,
  //                                 array('date_format' => 'yyyy-MM-dd',
  //                                       'fix_date' => false)
  //                                );

  // $date = new Zend_Date();
  // $date($Start);
  // $datesss =  $date->toString($dateFormat);


  // $date = new Zend_Date($SDate, false, $locale);
  // $myDate = new Zend_Date($Start1, $dateFormat, $locale);

  // $date = new Zend_Date($SDate, false, $locale);
  // $datesss =  $date->toString($timeFormat);

  //     echo $dateFormat;
  //      echo "22222" . $datesss;
  // print $date->toString('F j, Y, g:i a');
  // print $date->toString($dateFormat);
  // echo $date->get($dateFormat);
  //echo $Date;
  // var_dump($date) ;



  $sdate = new Zend_Date($SDate, false, $locale);
  $edate = new Zend_Date($EDate, false, $locale);

  if (($Start == $End) && ($today == $Start)) {
  return 'Today at ' . $sdate->toString($timeFormat) . ' - ' . $edate->toString($timeFormat);
  } else if ($Start == $End) {
  // return date('l', $SDate) . '  ' . date("F j, Y, g:i a", $SDate) . ' - ' . date("g:i a", $EDate);
  return $sdate->toString($dateFormat) . ' ' .  $sdate->toString($timeFormat) . ' - ' .  $edate->toString($timeFormat);
  } else if (($Start == $yesterDay) && ($End == $today)) {
  return 'Yesterday at ' . $sdate->toString($timeFormat) . ' - ' . 'Today at ' . $edate->toString($timeFormat);
  } else if (($Start == $today) && ($End == $tomorrow)) {
  return 'Today at ' . $sdate->toString($timeFormat) . ' - ' . 'Tomorrow at ' . $edate->toString($timeFormat);
  } else if ($startYear == $endYear) {
  // return date('l', $SDate) . '  ' . date("F j,  g:i a", $SDate) . ' - ' . date('l', $EDate) . '  ' . date("F j,  g:i a", $EDate);
  return $sdate->toString($dateFormat) . ' ' . $sdate->toString($timeFormat) . ' - ' . $edate->toString($dateFormat) . ' ' . $edate->toString($timeFormat);
  } else {
  // return date('l', $SDate) . '  ' . date("F j, Y,  g:i a", $SDate) . ' - ' . date('l', $EDate) . '  ' . date("F j, Y,  g:i a", $EDate);

  return $sdate->toString($dateFormat) . ' ' . $sdate->toString($timeFormat) . ' - ' . $edate->toString($dateFormat) . ' ' . $edate->toString($timeFormat);

  }



  } */

/* function getNewDateFormat($phpDate, $enabled = true) {

  $companyId = CheckAuth::getCompanySession();
  $dateTimeModel = new Model_DateTime();
  $dateTimeObj = $dateTimeModel->getByCompanyId($companyId);
  $countryModel = new Model_Countries();

  if(isset($dateTimeObj) && !empty($dateTimeObj)){
  $country = $countryModel->getById($dateTimeObj['country_id']);
  $locale = Zend_Locale::getLocaleToTerritory($country['country_code']);
  $dateFormat = $dateTimeObj['date_format'];
  $timeFormat = $dateTimeObj['time_format'];
  $timeZone =   $dateTimeObj['time_zone'];
  // echo $timeZone;
  date_default_timezone_set($timeZone);
  } else{
  $companyModel = new Model_Companies;
  $cityModel    = new Model_Cities;
  $companyObj   = $companyModel->getById($companyId);
  $cityObj      = $cityModel ->getById($companyObj['city_id']);
  // $country_id   = $cityObj['country_id'];
  $country = $countryModel->getById($cityObj['country_id']);
  $locale = Zend_Locale::getLocaleToTerritory($country['country_code']);
  $dateFormat = "EEEE, MMMM d, y";//Zend_Locale_Format::STANDARD;
  $timeFormat = "h:mm";//Zend_Locale_Format::STANDARD;
  }
  if (!is_int($phpDate)) {
  $phpDate = strtotime($phpDate);
  }
  $date = new Zend_Date($phpDate, false, $locale);

  if ($enabled) {
  $today = strtotime(date("Y-m-d", $phpDate));

  if ($today == strtotime(date("Y-m-d"))) {
  return 'Today '/* . $date->toString($dateFormat) */;
/* } else {
  return $date->toString($dateFormat);
  }
  } else {
  return $date->toString($dateFormat);
  }
  }
 */

function getNewDateFormat($dateToBeFormatted, $part = 'date', $sqlDateFormat = false) {
    if ($sqlDateFormat) {
        if (!is_int($dateToBeFormatted)) {
            $dateToBeFormatted = strtotime($dateToBeFormatted);
        }
    }

    $companyId = CheckAuth::getCompanySession();
    $dateTimeModel = new Model_DateTime();
    $dateTimeObj = $dateTimeModel->getByCompanyId($companyId);
    $countryModel = new Model_Countries();

    if (isset($dateTimeObj) && !empty($dateTimeObj)) {
        $country = $countryModel->getById($dateTimeObj['country_id']);
        $locale = Zend_Locale::getLocaleToTerritory($country['country_code']);
        $dateFormat = $dateTimeObj['date_format'];
        $timeFormat = $dateTimeObj['time_format'];
        $timeZone = $dateTimeObj['time_zone'];
        //echo $timeZone;

        date_default_timezone_set($timeZone);
        $date = new Zend_Date($dateToBeFormatted, false, $locale);
        if ($part == 'date')
            return $date->toString($dateFormat);
        else if ($part == 'time') {
            return $date->toString($timeFormat);
        } else if ($part == 'all') {
            return $date->toString($dateFormat) . ' ' . $date->toString($timeFormat);
        }
    }
    return 0;
}

/* * ***END****** */

function get_shortcut($string, $word = '') {
    $strings = explode(" ", $string);

    if (count($strings) <= 1) {
        return $string;
    } else {
        $shortcut = '';
        foreach ($strings as $string) {
            $shortcut .= strtoupper(substr($string, 0, 1));
        }

        if ($word) {
            $shortcut = "{$shortcut} {$word}";
        }

        return $shortcut;
    }
}

function get_line_address($address, $is_company = false) {
    if (is_array($address)) {
        if (!$is_company) {
            $unit_lot_number = isset($address['unit_lot_number']) ? $address['unit_lot_number'] : '';
            $street_number = isset($address['street_number']) ? $address['street_number'] : '';
            $street_address = isset($address['street_address']) ? $address['street_address'] : '';
            $suburb = isset($address['suburb']) ? $address['suburb'] : '';
            $state = isset($address['state']) ? strtoupper($address['state']) : '';
            $postcode = isset($address['postcode']) ? $address['postcode'] : '';
        } else {
            $unit_lot_number = isset($address['company_unit_lot_number']) ? $address['company_unit_lot_number'] : '';
            $street_number = isset($address['company_street_number']) ? $address['company_street_number'] : '';
            $street_address = isset($address['company_street_address']) ? $address['company_street_address'] : '';
            $suburb = isset($address['company_suburb']) ? $address['company_suburb'] : '';
            $state = isset($address['company_state']) ? strtoupper($address['company_state']) : '';
            $postcode = isset($address['company_postcode']) ? $address['company_postcode'] : '';
        }
        $line_address = '';
        $line_address .= $unit_lot_number ? "{$unit_lot_number}/" : '';
        $line_address .= $street_number ? "{$street_number}" : '';
        $line_address .= $street_address ? " {$street_address}" : '';
        $line_address .= $suburb ? " {$suburb}" : '';
        $line_address .= $state ? " {$state}" : '';
        $line_address .= $postcode ? " {$postcode}" : '';
    } else {
        $line_address = $address;
    }
    return trim($line_address);
}

function get_some_details_line_address($address, $is_company = false) {
    if (is_array($address)) {
        if (!$is_company) {
            $suburb = isset($address['suburb']) ? $address['suburb'] : '';
            $state = isset($address['state']) ? strtoupper($address['state']) : '';
            $postcode = isset($address['postcode']) ? $address['postcode'] : '';
        } else {
            $suburb = isset($address['company_suburb']) ? $address['company_suburb'] : '';
            $state = isset($address['company_state']) ? strtoupper($address['company_state']) : '';
            $postcode = isset($address['company_postcode']) ? $address['company_postcode'] : '';
        }
        $line_address = '';
        $line_address .= $suburb ? " {$suburb}" : '';
        $line_address .= $state ? " {$state}" : '';
        $line_address .= $postcode ? " {$postcode}" : '';
    } else {
        $line_address = $address;
    }
    return trim($line_address);
}

function get_last_url_element($url) {
    $end_url = '';

    $path = parse_url($url, PHP_URL_PATH);
    $pathFragments = explode('/', $path);
    $end_url = end($pathFragments);

    return $end_url;
}

function get_customer_name($customer) {

    $customer_name = '';

    if ($customer) {
        //$title = isset($customer['title']) && $customer['title'] ? ucfirst($customer['title']) . '.' : '';
        $first_name = isset($customer['first_name']) && $customer['first_name'] ? ucwords($customer['first_name']) : '';
        $last_name = isset($customer['last_name']) && $customer['last_name'] ? ' ' . ucwords($customer['last_name']) : '';

        $customer_name = $first_name . $last_name;
    }
    return $customer_name;
}

function get_customer_names($customer) {


    if ($customer) {

        $first_name2 = isset($customer['first_name2']) && $customer['first_name2'] ? ucwords($customer['first_name2']) : '';
        $last_name2 = isset($customer['last_name2']) && $customer['last_name2'] ? ' ' . ucwords($customer['last_name2']) : '';

        $customer_name2 = $first_name2 . $last_name2;

        $first_name3 = isset($customer['first_name3']) && $customer['first_name3'] ? ucwords($customer['first_name3']) : '';
        $last_name3 = isset($customer['last_name3']) && $customer['last_name3'] ? ' ' . ucwords($customer['last_name3']) : '';

        $customer_name3 = $first_name3 . $last_name3;
    }

    return array('customer_name2' => $customer_name2, 'customer_name3' => $customer_name3);
}

function preparer_number($number) {

    if (!$number) {
        return '';
    }

    if (!is_string($number)) {
        return '';
    }

    $number = preg_replace("#^\+#", "00", $number);
    $number = preg_replace('/[^0-9]/', '', $number);

    return $number;
}

function js2PhpTimeNew($jsdate) {
    $ret = 0;
    if (preg_match('@(\d+)/(\d+)/(\d+)\s+(\d+):(\d+)@', $jsdate, $matches) == 1) {
        $ret = mktime($matches[4], $matches[5], 0, $matches[2], $matches[1], $matches[3]);
        //echo $matches[4] ."-". $matches[5] ."-". 0  ."-". $matches[1] ."-". $matches[2] ."-". $matches[3];
    } else if (preg_match('@(\d+)/(\d+)/(\d+)@', $jsdate, $matches) == 1) {

        $ret = mktime(0, 0, 0, $matches[2], $matches[1], $matches[3]);
        //echo 0 ."-". 0 ."-". 0 ."-". $matches[1] ."-". $matches[2] ."-". $matches[3];
    }
    return $ret;
}

/**
 * getTimePeriodByName
 * saturday to friday
 *
 * @param type $name 
 */
function getTimePeriodByName($name, $time = 0) {

    if (!$time) {
        $time = time();
    }
    if (!is_int($time)) {
        $time = strtotime($time);
    }

    $start = '';
    $end = '';

    switch ($name) {

        case 'last_year':
            $last_year = date('Y', $time) - 1;

            $start = $last_year . '-01-01 00:00:00';
            $end = $last_year . '-12-31 23:59:59';
            break;

        case 'this_year':
            $this_year = date('Y', $time);

            $start = $this_year . '-01-01 00:00:00';
            $end = $this_year . '-12-31 23:59:59';
            break;

        case 'next_year':
            $next_year = date('Y', $time) + 1;

            $start = $next_year . '-01-01 00:00:00';
            $end = $next_year . '-12-31 23:59:59';
            break;

        case 'last_quarter':

            $last_year = date('Y', $time) - 1;
            $this_year = date('Y', $time);
            $next_year = date('Y', $time) + 1;

            $quarter = floor((date('m', $time) - 1) / 3) + 1;

            $quarter = $quarter - 1;

            if ($quarter == 0) {
                $this_year = $last_year;
                $quarter = 4;
            }

            switch ($quarter) {
                case 1:
                    $start = $this_year . '-01-01 00:00:00';
                    $end = $this_year . '-03-31 23:59:59';
                    break;
                case 2:
                    $start = $this_year . '-04-01 00:00:00';
                    $end = $this_year . '-06-30 23:59:59';
                    break;
                case 3:
                    $start = $this_year . '-07-01 00:00:00';
                    $end = $this_year . '-09-30 23:59:59';
                    break;
                case 4:
                    $start = $this_year . '-10-01 00:00:00';
                    $end = $this_year . '-12-31 23:59:59';
                    break;
            }
            break;

        case 'this_quarter':
            $this_year = date('Y', $time);
            $next_year = date('Y', $time) + 1;

            $quarter = floor((date('m', $time) - 1) / 3) + 1;

            switch ($quarter) {
                case 1:
                    $start = $this_year . '-01-01 00:00:00';
                    $end = $this_year . '-03-31 23:59:59';
                    break;
                case 2:
                    $start = $this_year . '-04-01 00:00:00';
                    $end = $this_year . '-06-30 23:59:59';
                    break;
                case 3:
                    $start = $this_year . '-07-01 00:00:00';
                    $end = $this_year . '-09-30 23:59:59';
                    break;
                case 4:
                    $start = $this_year . '-10-01 00:00:00';
                    $end = $this_year . '-12-31 23:59:59';
                    break;
            }
            break;

        case 'next_quarter':
            $this_year = date('Y', $time);
            $next_year = date('Y', $time) + 1;

            $quarter = floor((date('m', $time) - 1) / 3) + 1;

            $quarter = $quarter + 1;

            if ($quarter == 5) {
                $this_year = $next_year;
                $quarter = 1;
            }

            switch ($quarter) {
                case 1:
                    $start = $this_year . '-01-01 00:00:00';
                    $end = $this_year . '-03-31 23:59:59';
                    break;
                case 2:
                    $start = $this_year . '-04-01 00:00:00';
                    $end = $this_year . '-06-30 23:59:59';
                    break;
                case 3:
                    $start = $this_year . '-07-01 00:00:00';
                    $end = $this_year . '-09-30 23:59:59';
                    break;
                case 4:
                    $start = $this_year . '-10-01 00:00:00';
                    $end = $this_year . '-12-31 23:59:59';
                    break;
            }
            break;

        case 'last_month':
            $last_year = date('Y', $time) - 1;
            $this_year = date('Y', $time);

            $last_month = date('m', $time) - 1;

            if ($last_month == 0) {
                $last_month = 12;
                $start = $last_year . '-' . $last_month . '-01 00:00:00';
                $end = date('Y-m-d', strtotime($last_year . '-' . $last_month . " next month - 1 hour")) . ' 23:59:59';
            } else {
                $start = $this_year . '-' . $last_month . '-01 00:00:00';
                $end = date('Y-m-d', strtotime($this_year . '-' . $last_month . " next month - 1 hour")) . ' 23:59:59';
            }
            break;

        case 'this_month':
            $this_year = date('Y', $time);

            $this_month = date('m', $time);

            $start = $this_year . '-' . $this_month . '-01 00:00:00';
            $end = date('Y-m-d', strtotime($this_year . '-' . $this_month . " next month - 1 hour")) . ' 23:59:59';
            break;

        case 'next_month':
            $this_year = date('Y', $time);
            $next_year = date('Y', $time) + 1;

            $next_month = date('m', $time) + 1;

            if ($next_month == 13) {
                $next_month = 1;
                $start = $next_year . '-' . $next_month . '-01 00:00:00';
                $end = date('Y-m-d', strtotime($next_year . '-' . $next_month . " next month - 1 hour")) . ' 23:59:59';
            } else {
                $start = $this_year . '-' . $next_month . '-01 00:00:00';
                $end = date('Y-m-d', strtotime($this_year . '-' . $next_month . " next month - 1 hour")) . ' 23:59:59';
            }
            break;

        case 'last_week':

            $dom = date('d', $time);
            $dow = date('w', $time);

            $day = ($dom - $dow - 7 - 1);

            $start = date("Y-m-d 00:00:00", mktime(0, 0, 0, date("m", $time), $day, date("Y", $time)));
            $end = date("Y-m-d 23:59:59", mktime(0, 0, 0, date("m", strtotime($start)), date("d", strtotime($start)) + 6, date("Y", strtotime($start))));

            break;

        case 'this_week':

            $dom = date('d', $time);
            $dow = date('w', $time);

            $day = ($dom - $dow - 1);

            $start = date("Y-m-d 00:00:00", mktime(0, 0, 0, date("m", $time), $day, date("Y", $time)));
            $end = date("Y-m-d 23:59:59", mktime(0, 0, 0, date("m", strtotime($start)), date("d", strtotime($start)) + 6, date("Y", strtotime($start))));

            break;

        case 'next_week':

            $dom = date('d', $time);
            $dow = date('w', $time);

            $day = ($dom - $dow + 7 - 1);

            $start = date("Y-m-d 00:00:00", mktime(0, 0, 0, date("m", $time), $day, date("Y", $time)));
            $end = date("Y-m-d 23:59:59", mktime(0, 0, 0, date("m", strtotime($start)), date("d", strtotime($start)) + 6, date("Y", strtotime($start))));

            break;

        case 'last_day':
        case 'yesterday':
            $start = date("Y-m-d 00:00:00", mktime(0, 0, 0, date("m", $time), date("d", $time) - 1, date("Y", $time)));
            $end = date("Y-m-d 23:59:59", mktime(0, 0, 0, date("m", strtotime($start)), date("d", strtotime($start)), date("Y", strtotime($start))));
            break;

        case 'this_day':
        case 'today':
            $start = date("Y-m-d 00:00:00", mktime(0, 0, 0, date("m", $time), date("d", $time), date("Y", $time)));
            $end = date("Y-m-d 23:59:59", mktime(0, 0, 0, date("m", strtotime($start)), date("d", strtotime($start)), date("Y", strtotime($start))));
            break;

        case 'next_day':
        case 'tomorrow':
            $start = date("Y-m-d 00:00:00", mktime(0, 0, 0, date("m", $time), date("d", $time) + 1, date("Y", $time)));
            $end = date("Y-m-d 23:59:59", mktime(0, 0, 0, date("m", strtotime($start)), date("d", strtotime($start)), date("Y", strtotime($start))));
            break;
    }


    return array('start' => $start, 'end' => $end);
}

function time_ago($ptime) {
    $etime = time() - $ptime;

    if ($etime < 1) {
        return '0 seconds';
    }

    $a = array(365 * 24 * 60 * 60 => 'year',
        30 * 24 * 60 * 60 => 'month',
        24 * 60 * 60 => 'day',
        60 * 60 => 'hour',
        60 => 'minute',
        1 => 'second'
    );
    $a_plural = array('year' => 'years',
        'month' => 'months',
        'day' => 'days',
        'hour' => 'hours',
        'minute' => 'minutes',
        'second' => 'seconds'
    );

    foreach ($a as $secs => $str) {
        $d = $etime / $secs;
        if ($d >= 1) {
            $r = round($d);
            return $r . ' ' . ($r > 1 ? $a_plural[$str] : $str) . ' ago';
        }
    }
}

function getUserDeviceType() {
    $tablet_browser = 0;
    $mobile_browser = 0;
    $device_type = 'web';

    if (preg_match('/(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
        $tablet_browser++;
    }

    if (preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android|iemobile)/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
        $mobile_browser++;
    }

    if ((strpos(strtolower($_SERVER['HTTP_ACCEPT']), 'application/vnd.wap.xhtml+xml') > 0) or ( (isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE'])))) {
        $mobile_browser++;
    }

    $mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'], 0, 4));
    $mobile_agents = array(
        'w3c ', 'acs-', 'alav', 'alca', 'amoi', 'audi', 'avan', 'benq', 'bird', 'blac',
        'blaz', 'brew', 'cell', 'cldc', 'cmd-', 'dang', 'doco', 'eric', 'hipt', 'inno',
        'ipaq', 'java', 'jigs', 'kddi', 'keji', 'leno', 'lg-c', 'lg-d', 'lg-g', 'lge-',
        'maui', 'maxo', 'midp', 'mits', 'mmef', 'mobi', 'mot-', 'moto', 'mwbp', 'nec-',
        'newt', 'noki', 'palm', 'pana', 'pant', 'phil', 'play', 'port', 'prox',
        'qwap', 'sage', 'sams', 'sany', 'sch-', 'sec-', 'send', 'seri', 'sgh-', 'shar',
        'sie-', 'siem', 'smal', 'smar', 'sony', 'sph-', 'symb', 't-mo', 'teli', 'tim-',
        'tosh', 'tsm-', 'upg1', 'upsi', 'vk-v', 'voda', 'wap-', 'wapa', 'wapi', 'wapp',
        'wapr', 'webc', 'winw', 'winw', 'xda ', 'xda-');

    if (in_array($mobile_ua, $mobile_agents)) {
        $mobile_browser++;
    }

    if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'opera mini') > 0) {
        $mobile_browser++;
        //Check for tablets on opera mini alternative headers
        $stock_ua = strtolower(isset($_SERVER['HTTP_X_OPERAMINI_PHONE_UA']) ? $_SERVER['HTTP_X_OPERAMINI_PHONE_UA'] : (isset($_SERVER['HTTP_DEVICE_STOCK_UA']) ? $_SERVER['HTTP_DEVICE_STOCK_UA'] : ''));
        if (preg_match('/(tablet|ipad|playbook)|(android(?!.*mobile))/i', $stock_ua)) {
            $tablet_browser++;
        }
    }

    if ($tablet_browser > 0) {
        // do something for tablet devices
        //print 'is tablet';
        $device_type = 'tablet';
    } else if ($mobile_browser > 0) {
        // do something for mobile devices
        //print 'is mobile';
        $device_type = 'mobile';
    } else {
        // do something for everything else
        //print 'is desktop';
        $device_type = 'web';
    }

    return $device_type;
}

function my_ip($ip = '176.106.46.142') {
    if ($_SERVER['REMOTE_ADDR'] == $ip) {
        return TRUE;
    } else {
        return FALSE;
    }
}

function getActivityLogDateFormating($phpDate, $previousPhpDate, $enabled = false) {
    $currentLogCreated = strtotime(date("Y-m-d", $phpDate));

    $activityLogDate = array();
    if ($enabled) { //if(isset($previousPhpDate) && !empty $previousPhpDate){
        $previousLogCreated = strtotime(date("Y-m-d", $previousPhpDate));
        if ((date('Y', $currentLogCreated) != date('Y', $previousLogCreated)) ||
                ((date('Y', $currentLogCreated) == date('Y', $previousLogCreated)) && (date('m', $currentLogCreated) != date('m', $previousLogCreated)))) {
            $activityLogDate['MonthYear'] = date('F Y', $currentLogCreated);
            if ($currentLogCreated == strtotime(date('Y-m-d', strtotime('-1 day'))))
                $activityLogDate['MonthDay'] = "Yesterday";
            else
                $activityLogDate['MonthDay'] = date('F j', $currentLogCreated);
        }else if (date('d', $currentLogCreated) != date('d', $previousLogCreated)) {
            if ($currentLogCreated == date('Y-m-d', strtotime('-1 day')))
                $activityLogDate['MonthDay'] = "Yesterday";
            else
                $activityLogDate['MonthDay'] = date('F j', $currentLogCreated);
        }
    }else {
        $activityLogDate['MonthYear'] = date('F Y', $currentLogCreated);
        if ($currentLogCreated == strtotime(date("Y-m-d")))
            $activityLogDate['MonthDay'] = "Today";
        else if ($currentLogCreated == strtotime(date('Y-m-d', strtotime('-1 day'))))
            $activityLogDate['MonthDay'] = "Yesterday";
        else
            $activityLogDate['MonthDay'] = date('F j', $currentLogCreated);
    }
    $activityLogDate['Time'] = date("g:i a", $phpDate);
    return $activityLogDate;
}

function base64UrlEncode($inputStr) {
    return strtr(base64_encode($inputStr), '+/=', '-_,');
}

function base64UrlDecode($inputStr) {
    return base64_decode(strtr($inputStr, '-_,', '+/='));
}

//Rand
/*
 * Matches each symbol of PHP date format standard
 * with jQuery equivalent codeword
 */
/*
  function dateformat_PHP_to_jQueryUI($php_format)
  {
  $SYMBOLS_MATCHING = array(
  // Day
  'd' => 'dd',
  'D' => 'D',
  'j' => 'd',
  'l' => 'DD',
  'N' => '',
  'S' => '',
  'w' => '',
  'z' => 'o',
  // Week
  'W' => '',
  // Month
  'F' => 'MM',
  'm' => 'mm',
  'M' => 'M',
  'n' => 'm',
  't' => '',
  // Year
  'L' => '',
  'o' => '',
  'Y' => 'yy',
  'y' => 'y',
  // Time
  'a' => '',
  'A' => '',
  'B' => '',
  'g' => '',
  'G' => '',
  'h' => '',
  'H' => '',
  'i' => '',
  's' => '',
  'u' => ''
  );
  $jqueryui_format = "";
  $escaping = false;
  for($i = 0; $i < strlen($php_format); $i++)
  {
  $char = $php_format[$i];
  if($char === '\\') // PHP date format escaping character
  {
  $i++;
  if($escaping) $jqueryui_format .= $php_format[$i];
  else $jqueryui_format .= '\'' . $php_format[$i];
  $escaping = true;
  }
  else
  {
  if($escaping) { $jqueryui_format .= "'"; $escaping = false; }
  if(isset($SYMBOLS_MATCHING[$char]))
  $jqueryui_format .= $SYMBOLS_MATCHING[$char];
  else
  $jqueryui_format .= $char;
  }
  }
  return $jqueryui_format;
  }
 */
//dateFormat_php_to_jquery ---> dateFormat_zend_to_jqueryUI
function dateFormat_zend_to_jqueryUI($php_format) {

    $SYMBOLS_MATCHING = array(
        // Day
        'd' => 'd', //9
        'dd' => 'dd', //09
        'D' => 'o', // day of year 7
        'DD' => 'o', // day of year 07 --
        'DDD' => 'oo', // day of year 007
        'E' => 'D', //M --
        'EE' => 'D', //Mo --
        'EEE' => 'D', //Mon
        'EEEE' => 'DD', //Monday
        'EEEEE' => 'D', //M --
        'e' => '', //week day 4 
        'ee' => '', // week day 04
        // Week
        'w' => '', //5
        'ww' => '', //05
        // Month
        'M' => 'm', //2
        'MM' => 'mm', //02
        'MMM' => 'M', //Feb
        'MMMM' => 'MM', //February
        'MMMMM' => 'M', //F --
        // Year
        'y' => 'yy', //9 --
        'yy' => 'yy', //09
        'yyy' => 'yyyy', //2009
        'yyyy' => 'yyyy', //2009
        'yyyyy' => 'yyyy', //02009 --
        'Y' => 'yy', //9 --
        'YY' => 'yy', //09 
        'YYY' => 'yyyy', //2009
        'YYYY' => 'yyyy', //2009
        'YYYYY' => 'yyyy', //02009 --
        // Time
        'a' => 'TT', //Time of day, localized	Zend_Date::MERIDIEM	vorm.
        'A' => '', //Milliseconds from the actual day 20563
        'h' => 'h', //Hour, (1-12), one or two digit	Zend_Date::HOUR_SHORT_AM	2
        'hh' => 'hh', //Hour, (01-12), two digit	Zend_Date::HOUR_AM	02
        'H' => 'H', //Hour, (0-23), one or two digit	Zend_Date::HOUR_SHORT	2
        'HH' => 'HH', //Hour, (00-23), two digit	Zend_Date::HOUR	02
        'm' => 'm', //Minute, (0-59), one or two digit	Zend_Date::MINUTE_SHORT	2
        'mm' => 'mm', //Minute, (00-59), two digit	Zend_Date::MINUTE	02
        's' => 's', //Second, (0-59), one or two digit	Zend_Date::SECOND_SHORT	2
        'ss' => 'ss', //Second, (00-59), two digit	Zend_Date::SECOND	02
        'S' => 'ss', //Millisecond	Zend_Date::MILLISECOND	20536 --
        'z' => 'z', //Time zone, localized, abbreviated	Zend_Date::TIMEZONE	CET
        'zz' => 'z', //Time zone, localized, abbreviated	Zend_Date::TIMEZONE	CET --
        'zzz' => 'z', //Time zone, localized, abbreviated	Zend_Date::TIMEZONE	CET --
        'zzzz' => 'z', //Time zone, localized, complete	Zend_Date::TIMEZONE_NAME	Europe/Paris --
        'Z' => 'Z', //Difference of time zone	Zend_Date::GMT_DIFF	+0100 --
        'ZZ' => 'Z', //Difference of time zone	Zend_Date::GMT_DIFF	+0100 --
        'ZZZ' => 'Z', //Difference of time zone	Zend_Date::GMT_DIFF	+0100 --
        'ZZZZ' => 'Z' //Difference of time zone, separated	Zend_Date::GMT_DIFF_SEP	+01:00 --
    );
    $jqueryui_format = "";
    $escaping = false;
    $alphabets = array();
    $all_symbols = array();
    $symbols = array();

    $alphabet_pattern = "/[\s,-.:]+/";
    $symbols_pattern = "/[A-Za-z]+/";
    $alphabets = preg_split($alphabet_pattern, $php_format);
    $all_symbols = preg_split($symbols_pattern, $php_format);
    $symbols = array_slice($all_symbols, 1, -1); // remove first and last element of the array	

    for ($i = 0; $i < sizeof($alphabets); $i++) {
        if (isset($symbols[$i])) {
            $jqueryui_format .= $SYMBOLS_MATCHING[$alphabets[$i]] . $symbols[$i];
        } else {
            $jqueryui_format .= $SYMBOLS_MATCHING[$alphabets[$i]];
        }
    }
    return $jqueryui_format;
}

//dateFormat_php_to_js ---> dateFormat_zend_to_js
function dateFormat_zend_to_js($php_format) {
    //‎To get equivilant jquery date format from php date format ‎
    //I got the jquery symbols from this website:  https://api.jqueryui.com/datepicker/ 
    // zend symbols from this website : https://framework.zend.com/manual/1.10/en/zend.date.constants.html
    $SYMBOLS_MATCHING = array(
        // Day
        'd' => 'd', //9
        'dd' => 'dd', //09
        'D' => 'o', // day of year 7
        'DD' => 'o', // day of year 07 --
        'DDD' => 'oo', // day of year 007
        'E' => 'D', //M --
        'EE' => 'D', //Mo --
        'EEE' => 'D', //Mon
        'EEEE' => 'DD', //Monday
        'EEEEE' => 'D', //M --
        'e' => '', //week day 4 
        'ee' => '', // week day 04
        // Week
        'w' => '', //5
        'ww' => '', //05
        // Month
        'M' => 'm', //2
        'MM' => 'mm', //02
        'MMM' => 'M', //Feb
        'MMMM' => 'MM', //February
        'MMMMM' => 'M', //F --
        // Year
        'y' => 'yyyy', //9 --
        'yy' => 'yy', //09
        'yyy' => 'yyyy', //2009
        'yyyy' => 'yyyy', //2009
        'yyyyy' => 'yyyy', //02009 --
        'Y' => 'yyyy', //9 --
        'YY' => 'yy', //09 
        'YYY' => 'yyyy', //2009
        'YYYY' => 'yyyy', //2009
        'YYYYY' => 'yyyy', //02009 --
        //this time format is according to moment js time format  http://momentjs.com/docs/
        // Time
        'a' => 'a', //Time of day, localized	Zend_Date::MERIDIEM	vorm.
        'A' => '', //Milliseconds from the actual day 20563
        'h' => 'h', //Hour, (1-12), one or two digit	Zend_Date::HOUR_SHORT_AM	2
        'hh' => 'hh', //Hour, (01-12), two digit	Zend_Date::HOUR_AM	02
        'H' => 'H', //Hour, (0-23), one or two digit	Zend_Date::HOUR_SHORT	2
        'HH' => 'HH', //Hour, (00-23), two digit	Zend_Date::HOUR	02
        'm' => 'm', //Minute, (0-59), one or two digit	Zend_Date::MINUTE_SHORT	2
        'mm' => 'mm', //Minute, (00-59), two digit	Zend_Date::MINUTE	02
        's' => 's', //Second, (0-59), one or two digit	Zend_Date::SECOND_SHORT	2
        'ss' => 'ss', //Second, (00-59), two digit	Zend_Date::SECOND	02
        'S' => 'ss', //Millisecond	Zend_Date::MILLISECOND	20536 --
        'z' => 'z', //Time zone, localized, abbreviated	Zend_Date::TIMEZONE	CET
        'zz' => 'z', //Time zone, localized, abbreviated	Zend_Date::TIMEZONE	CET --
        'zzz' => 'z', //Time zone, localized, abbreviated	Zend_Date::TIMEZONE	CET --
        'zzzz' => 'z', //Time zone, localized, complete	Zend_Date::TIMEZONE_NAME	Europe/Paris --
        'Z' => 'Z', //Difference of time zone	Zend_Date::GMT_DIFF	+0100 --
        'ZZ' => 'Z', //Difference of time zone	Zend_Date::GMT_DIFF	+0100 --
        'ZZZ' => 'Z', //Difference of time zone	Zend_Date::GMT_DIFF	+0100 --
        'ZZZZ' => 'Z' //Difference of time zone, separated	Zend_Date::GMT_DIFF_SEP	+01:00 --
    );
    $jqueryui_format = "";
    $escaping = false;
    $alphabets = array();
    $all_symbols = array();
    $symbols = array();

    $alphabet_pattern = "/[\s,-.:]+/";
    $symbols_pattern = "/[A-Za-z]+/";
    $alphabets = preg_split($alphabet_pattern, $php_format);
    $all_symbols = preg_split($symbols_pattern, $php_format);
    $symbols = array_slice($all_symbols, 1, -1); // remove first and last element of the array	

    for ($i = 0; $i < sizeof($alphabets); $i++) {
        if (isset($symbols[$i])) {
            $jqueryui_format .= $SYMBOLS_MATCHING[$alphabets[$i]] . $symbols[$i];
        } else {
            $jqueryui_format .= $SYMBOLS_MATCHING[$alphabets[$i]];
        }
    }
    return $jqueryui_format;
}

//dateFormat_php_to_php ---> dateFormat_zend_to_purePhp
function dateFormat_zend_to_purePhp($date_format, $date_value) {
    $old_date_timestamp = strtotime($date_value);
    $new_date = date($date_format, $old_date_timestamp);
    return $new_date;
}

//timeFormat_php_to_php ---> timeFormat_zend_to_purePhp
function timeFormat_zend_to_purePhp($time_format, $time_value) {
    $dateTimeObj = get_settings_date_format();
    $timeZoneString = get_timeZone($dateTimeObj['time_format']);
    if ($timeZoneString != '') {
		//$time_value = trim($time_value, $timeZoneString);
		$time_value = explode(" ", $time_value);
		array_pop($time_value);
		$time_value=implode(" ",$time_value);
			//$time_value= preg_replace('/\W\w+\s*(\W*)$/', '$1', $time_value);
    }

    $dt = new DateTime($time_value);
    $time = $dt->format($time_format);

    return $time;
}

function get_settings_date_format() {
    $companyId = CheckAuth::getCompanySession();
    $dateTimeModel = new Model_DateTime();
    $dateTimeObj = $dateTimeModel->getByCompanyId($companyId);

    return $dateTimeObj;
}

//timeFormat_php_to_Momentjs ---> timeFormat_zend_to_Momentjs
function timeFormat_zend_to_Momentjs($php_format) {
    //this time format is according to moment js time format  http://momentjs.com/docs/
    $SYMBOLS_MATCHING = array(
        // Time
        'a' => 'A', //Time of day, localized	Zend_Date::MERIDIEM	vorm.
        'A' => '', //Milliseconds from the actual day 20563
        'h' => 'h', //Hour, (1-12), one or two digit	Zend_Date::HOUR_SHORT_AM	2
        'hh' => 'hh', //Hour, (01-12), two digit	Zend_Date::HOUR_AM	02
        'H' => 'H', //Hour, (0-23), one or two digit	Zend_Date::HOUR_SHORT	2
        'HH' => 'HH', //Hour, (00-23), two digit	Zend_Date::HOUR	02
        'm' => 'm', //Minute, (0-59), one or two digit	Zend_Date::MINUTE_SHORT	2
        'mm' => 'mm', //Minute, (00-59), two digit	Zend_Date::MINUTE	02
        's' => 's', //Second, (0-59), one or two digit	Zend_Date::SECOND_SHORT	2
        'ss' => 'ss', //Second, (00-59), two digit	Zend_Date::SECOND	02
        'S' => 'ss', //Millisecond	Zend_Date::MILLISECOND	20536 --
        'z' => '', //Time zone, localized, abbreviated	Zend_Date::TIMEZONE	CET
        'zz' => '', //Time zone, localized, abbreviated	Zend_Date::TIMEZONE	CET --
        'zzz' => '', //Time zone, localized, abbreviated	Zend_Date::TIMEZONE	CET --
        'zzzz' => '', //Time zone, localized, complete	Zend_Date::TIMEZONE_NAME	Europe/Paris --
        'Z' => 'z', //Difference of time zone	Zend_Date::GMT_DIFF	+0100 --
        'ZZ' => 'z', //Difference of time zone	Zend_Date::GMT_DIFF	+0100 --
        'ZZZ' => 'z', //Difference of time zone	Zend_Date::GMT_DIFF	+0100 --
        'ZZZZ' => 'z', //Difference of time zone, separated	Zend_Date::GMT_DIFF_SEP	+01:00 --
        // Day
        'd' => 'D', //9
        'dd' => 'DD', //09
        'D' => 'DDD', // day of year 7
        'DD' => 'DDD', // day of year 07 --
        'DDD' => 'DDD', // day of year 007
        'E' => 'ddd', //M --
        'EE' => 'ddd', //Mo --
        'EEE' => 'ddd', //Mon
        'EEEE' => 'dddd', //Monday
        'EEEEE' => 'ddd', //M --
        'e' => 'e', //week day 4 
        'ee' => 'e', // week day 04
        // Week
        'w' => 'w', //5
        'ww' => 'ww', //05
        // Month
        'M' => 'M', //2
        'MM' => 'MM', //02
        'MMM' => 'MMM', //Feb
        'MMMM' => 'MMMM', //February
        'MMMMM' => 'M', //F in zend 2 in moment js --
        // Year
        'y' => 'YYYY', //2009 
        'yy' => 'YY', //09
        'yyy' => 'YYYY', //2009
        'yyyy' => 'YYYY', //2009
        'yyyyy' => 'YYYY', //02009 --
        'Y' => 'YYYY', //9 --
        'YY' => 'YY', //09 
        'YYY' => 'YYYY', //2009
        'YYYY' => 'YYYY', //2009
        'YYYYY' => 'YYYY', //02009 --
    );
    $jqueryui_format = "";
    $escaping = false;
    $alphabets = array();
    $all_symbols = array();
    $symbols = array();

    $alphabet_pattern = "/[\s,-.:]+/";
    $symbols_pattern = "/[A-Za-z]+/";
    $alphabets = preg_split($alphabet_pattern, $php_format);
    $all_symbols = preg_split($symbols_pattern, $php_format);
    $symbols = array_slice($all_symbols, 1, -1); // remove first and last element of the array	

    for ($i = 0; $i < sizeof($alphabets); $i++) {
        if (isset($symbols[$i])) {
            $jqueryui_format .= $SYMBOLS_MATCHING[$alphabets[$i]] . $symbols[$i];
        } else {
            $jqueryui_format .= $SYMBOLS_MATCHING[$alphabets[$i]];
        }
    }
    return $jqueryui_format;
}

function get_timeZone($format) {
    $z_count = substr_count($format, 'z');
    $append_string = '';
    $date_format = get_settings_date_format();
    if ($z_count == 1 || $z_count == 2 || $z_count == 3) {
        $dateTime = new DateTime();
        $dateTime->setTimeZone(new DateTimeZone($date_format['time_zone']));
        $append_string = $dateTime->format('T');
    } else if ($z_count == 4) {
        $append_string = $date_format['time_zone'];
    }

    return $append_string;
}

function getCredentialFormat($credential) {
    $words = explode(" ", $credential);
    $str = "";
    foreach ($words as $key => $word) {
        if ($key == 0) {
           $word = strtolower($word);
        } else {
            $word = ucfirst($word);
        }
        $str .= $word;
    }
    return $str;
}


// End Rand
