<?php

class Gmail_IndexController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'gmail';
    }

    /**
     * Items list action
     */
    public function indexAction() {

        $this->view->main_menu = 'Emails';
        $this->view->sub_menu = 'Gmail';

        $loggedUser = CheckAuth::getLoggedUser();
        $companyId = CheckAuth::getCompanySession();
        $roleName = CheckAuth::getRoleName();
		
		
        if ($roleName == 'contractor') {
            $model = new Model_GmailServiceAccount($loggedUser['user_id'], 'contractor');
        } else {
            $model = new Model_GmailServiceAccount($companyId, 'company');
        }		
		
		
        $emails = $model->getAllEmails();
        $service = $model->getService();
		
		$optParams = array();
		$messageIds = array();
		$optParams['labelIds'] = 'UNREAD';			
		$messages = $service->users_messages->listUsersMessages('me',$optParams);
		$list = $messages->getMessages();
		
		foreach ($messages as $message) {
		$messageIds[]=$message->getId();
		}
		
		//var_dump($messageIds);
        //exit;
		
		$this->view->messageIds = $messageIds;
        $this->view->contractor_id = $loggedUser['user_id'];
        $this->view->emails = $emails;
        $this->view->service = $service;
    }

	
    public function getEmailsByBookingNumAndIdAction() {
        $booking_num = $this->request->getParam('booking_num');
        $id = $this->request->getParam('id');
        $type = $this->request->getParam('type');


        //get estimate , invoice , complaint Number ,customer emails, 
        $bookingObj = new Model_Booking();
        $booking = $bookingObj->getIdByBookingNum($booking_num);
        $bookingId = $booking['booking_id'];
        $customerEmails = $bookingObj->getCustomerByBookingId($bookingId);
        $primaryCustomerEmail = $customerEmails['email1'];
        $estimateObj = new Model_BookingEstimate();
        $estimate = $estimateObj->getByBookingId($bookingId);
        $estimateNum = $estimate['estimate_num'];
        $invoiceObj = new Model_BookingInvoice();
        $invoice = $invoiceObj->getByBookingId($bookingId);
        $invoiceNum = $invoice['invoice_num'];
        $complaintObj = new Model_Complaint();
        $complaint = $complaintObj->getByBookingId($bookingId);

        //$complaintNum = $complaint['complaint_num'];


        if ($type == 'contractor') {
            $model = new Model_GmailServiceAccount($id, 'contractor');
        } else {
            $model = new Model_GmailServiceAccount($id, 'company');
        }


        $emails = $model->getAllEmails();
        $service = $model->getService();
        $headerArry = array();



        foreach ($emails as $key => $msgList) {
            $messageId = $msgList->getId(); // Grab first Message
            $optParamsGet = array();
            $optParamsGet['format'] = 'full'; // Display message in payload
            //get service
            $message = $service->users_messages->get('me', $messageId, $optParamsGet);
            $messagePayload = $message->getPayload();
            $headers = $message->getPayload()->getHeaders();
           
            $headerArry = $this->filterEmailsAction($headers, $booking_num, $estimateNum, $invoiceNum, $primaryCustomerEmail, $complaint);
           // echo "salim";
            $headerArry['id'] = $messageId;
            $final_result[$key] = $headerArry;
        }

       
        echo json_encode($final_result);
        exit;
    }

  public function viewAction() {
        $messageId = $this->request->getParam('id');
        $id = $this->request->getParam('contractor_id');

        $type = $this->request->getParam('type');
        if ($type == 'contractor') {
            $model = new Model_GmailServiceAccount($id, 'contractor');
        } else {
            $model = new Model_GmailServiceAccount($id, 'company');
        }
        //	$emails = $model->getAllEmails();
        $service = $model->getService();

        $optParamsGet = array();
        $optParamsGet['format'] = 'full'; // Display message in payload
        $message = $service->users_messages->get('me', $messageId, $optParamsGet);
        $messagePayload = $message->getPayload();
        $headers = $message->getPayload()->getHeaders();
        $paryload = $message->getPayload();
        $parts = $message->getPayload()->getParts();
        $getBody = $paryload->getBody();

        $body['data'] = '';
        if ($getBody['data']) {
            $body = $getBody;
        } else {
            if (isset($parts[0]['parts'][1]['body'])) {
                $body = $parts[0]['parts'][1]['body'];
            } elseif (isset($parts[0]['parts'][0]['body'])) {
                $body = $parts[0]['parts'][0]['body'];
            } elseif (isset($parts[1]['parts'][1]['body'])) {
                $body = $parts[1]['parts'][1]['body'];
            } elseif (isset($parts[1]['parts'][0]['body'])) {
                $body = $parts[1]['parts'][0]['body'];
            } elseif (isset($parts[0]['modelData']['body'])) {
                $body = $parts[0]['modelData']['body'];
            } elseif (isset($parts[1]['body'])) {
                $body = $parts[1]['body'];
            } elseif (isset($parts[0]['body'])) {
                $body = $parts[0]['body'];
            }
        }
        $rawData = $body['data'];

        $sanitizedData = strtr($rawData, '-_', '+/');
        $decodedMessage = base64_decode($sanitizedData);

        //D.A 09/09/2015 Mark email as read 
		$email='';		
		$modelContractorGmailAccounts = new Model_ContractorGmailAccounts();
		if ($type == 'contractor') {
            $contractorAccount = $modelContractorGmailAccounts->getByContractorId($id);
            $email = isset($contractorAccount['email']) ? $contractorAccount['email'] : '';
        } else {
            $company = new Model_Companies();
            $email = $company->getEnquiryEmailByCompanyId($id);
        }
		
		$labelsToRemove=array();
		$labelsToRemove[]="UNREAD";
		$mods = new Google_Service_Gmail_ModifyMessageRequest();
		$mods->setRemoveLabelIds($labelsToRemove);

		try {
		$message = $service->users_messages->modify($email, $messageId, $mods);
		} catch (Exception $e) {
		print 'An error occurred: ' . $e->getMessage();
		}		
		
		//////////////////////////////////

        $this->view->decodedMessage = $decodedMessage;
        $this->view->headers = $headers;

        //$this->view->id = $id;
        //$this->view->emails = $emails;
        //$this->view->service = $service;
    }



    public function filterEmailsAction($data, $bok_num = null, $est_num = null, $inv_num = null, $customerEmail = null, $comp_nums = null) {
        
        foreach ($data as $header) {
            //echo "salim2";
//     var_dump($data);
            if ('Subject' == $header->name) {
                if (strstr($header->value, $bok_num)) {
                    if (!strstr($header->value, "Complaint")) {
                        $headerArry['sub_booking'] = $header->value;
                    }
                }if (strstr($header->value, $est_num)) {
                    $headerArry['sub_estimate'] = $header->value;
                }if (strstr($header->value, $inv_num)) {
                    $headerArry['sub_invoice'] = $header->value;
                }if (isset($comp_nums)) {
                    foreach ($comp_nums as $comp_num) {

                        if (strstr($header->value, $comp_num['complaint_num']) && strstr($header->value, 'Complaint')) {
                            $headerArry['sub_compliant'] = $header->value;
                        }if (strstr($header->value, $bok_num) && strstr($header->value, 'Complaint')) {
                            $headerArry['sub_compliant'] = $header->value;
                        }
                    }
                }
            } elseif ('Date' == $header->name) {
                $date = explode(' ', $header->value);
                $headerArry['date'] = $date;
            } elseif ('From' == $header->name) {
                $headerArry['from'] = $header->value;
            } elseif ('To' == $header->name) {
                $headerArry['to'] = $header->value;
            } elseif ('Delivered-To' == $header->name) {
                $headerArry['deliver'] = $header->value;
            }
        }
        return $headerArry;
    }

    // 20/05/2015 D.A
    // To view the Local emails of contractor on the left side menu
    public function localAction() {

        // check Auth for logged user
        // CheckAuth::checkPermission(array('localEmailsView'));
        $currentPage = $this->request->getParam('page', 1);
        //get Params
        $loggedUser = CheckAuth::getLoggedUser();
        // load model
        $modelEmailLog = new Model_EmailLog();

        //init pager and articles model object
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];
        $emailLog = $modelEmailLog->getByContractorId($loggedUser['user_id'], $pager);
        $this->view->emailLog = $emailLog;
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
    }

    //D.A 20/05/2015 
    // To view the message details of Local emails on the left side menu
    public function localMessageAction() {

        // check Auth for logged user
        // CheckAuth::checkPermission(array('localEmailsViewMessage'));
        //get Params
        $msgId = $this->request->getParam('id', 0);

        // load model
        $modelEmailLog = new Model_EmailLog();
        $emailLog = $modelEmailLog->getById($msgId);
        $this->view->emailLog = $emailLog;
    }

	public function changeStatusAction() {

   
        //get Params
        $id = $this->request->getParam('id');
	  
	  
        // load model
        $modelEmailLog = new Model_EmailLog();
		$data=array('is_read'=>'1');
        $emailLog = $modelEmailLog->updateById($id,$data);
		echo 1;
		exit;
	}
    
	public function gmailChangeStatusAction() {
		$messageId = $this->request->getParam('id');
		
		
		$loggedUser = CheckAuth::getLoggedUser();
        $companyId = CheckAuth::getCompanySession();
        $roleName = CheckAuth::getRoleName();
		
		
        if ($roleName == 'contractor') {
            $model = new Model_GmailServiceAccount($loggedUser['user_id'], 'contractor');
        } else {
            $model = new Model_GmailServiceAccount($companyId, 'company');
        }		
		
		
        $emails = $model->getAllEmails();
        $service = $model->getService();
		
		$email='';		
		$modelContractorGmailAccounts = new Model_ContractorGmailAccounts();
		if ($roleName == 'contractor') {
            $contractorAccount = $modelContractorGmailAccounts->getByContractorId($loggedUser['user_id']);
            $email = isset($contractorAccount['email']) ? $contractorAccount['email'] : '';
        } else {
            $company = new Model_Companies();
            $email = $company->getEnquiryEmailByCompanyId($companyId);
        }
		
		$labelsToRemove=array();
		$labelsToRemove[]="UNREAD";
		$mods = new Google_Service_Gmail_ModifyMessageRequest();
		$mods->setRemoveLabelIds($labelsToRemove);

		try {
		$message = $service->users_messages->modify($email, $messageId, $mods);
		} catch (Exception $e) {
		print 'An error occurred: ' . $e->getMessage();
		}
        
		echo 1;
		exit;
	}
	
	
	//D.A 20/05/2015 
    // To view the message details of Gmail emails on the left side menu
    public function viewGmailAction() {

        $messageId = $this->request->getParam('id');
        $loggedUser = CheckAuth::getLoggedUser();
        $companyId = CheckAuth::getCompanySession();
        $roleName = CheckAuth::getRoleName();
        if ($roleName == 'contractor') {
            $model = new Model_GmailServiceAccount($loggedUser['user_id'], 'contractor');
        } elseif ($roleName == 'super_admin') {
            $model = new Model_GmailServiceAccount($companyId);
        }

        $emails = $model->getAllEmails();
        $service = $model->getService();

        $body['data'] = '';
        $optParamsGet = array();
        $optParamsGet['format'] = 'full';
        $message = $service->users_messages->get('me', $messageId, $optParamsGet);
        $messagePayload = $message->getPayload();
        $headers = $message->getPayload()->getHeaders();
        $parts = $message->getPayload()->getParts();

        if (isset($parts[0]['parts'][1]['body'])) {
            $body = $parts[0]['parts'][1]['body'];
        } elseif (isset($parts[0]['parts'][0]['body'])) {
            $body = $parts[0]['parts'][0]['body'];
        } elseif (isset($parts[1]['parts'][1]['body'])) {
            $body = $parts[1]['parts'][1]['body'];
        } elseif (isset($parts[1]['parts'][0]['body'])) {
            $body = $parts[1]['parts'][0]['body'];
        } elseif (isset($parts[0]['modelData']['body'])) {
            $body = $parts[0]['modelData']['body'];
        } elseif (isset($parts[1]['body'])) {
            $body = $parts[1]['body'];
        } elseif (isset($parts[0]['body'])) {
            $body = $parts[0]['body'];
        }

        $rawData = $body['data'];

        $sanitizedData = strtr($rawData, '-_', '+/');
        $decodedMessage = base64_decode($sanitizedData);

        $this->view->decodedMessage = $decodedMessage;
        $this->view->headers = $headers;
    }

}
