<?php

class Gmail_IndexController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'gmail';
    }

    /**
     * Items list action
     */
    public function indexAction() {

       $this->view->main_menu = 'gmail';
		$this->view->sub_menu = 'inbox';
		//
        // check Auth for logged user
        //
        //CheckAuth::checkPermission(array('contractors'));

        //
        // get request parameters
        //
		
        $model = new Model_GmailServiceAccount(8);
		$emails = $model->getAllEmails();
		$service = $model->getService();
        //$this->view->emails = $emails;
        //$this->view->service = $service;
		foreach($emails as $msgList){
			$messageId = $msgList->getId(); // Grab first Message
			

        $optParamsGet = array();
        $optParamsGet['format'] = 'full'; // Display message in payload
		//get service
		
        $message = $service->users_messages->get('me',$messageId,$optParamsGet);
		
        $messagePayload = $message->getPayload();
		
        $headers = $message->getPayload()->getHeaders();
		
        //$parts = $message->getPayload()->getParts();

        //$body = $parts[0]['body'];
       // $rawData = $body->data;
       // $sanitizedData = strtr($rawData,'-_', '+/');
       // $decodedMessage = base64_decode($sanitizedData);
		
		//var_dump($headers);
		//exit;
		}
		foreach( $headers as $header ) {
			//echo "Name :". $header->name;
			if('Date' == $header->name){
				$headerArry['date'] = $header->value;
			} else if('From' == $header->name){
				$headerArry['from'] = $header->value;
			} else if('To' == $header->name){
				$headerArry['to'] = $header->value;
			} else if('Subject' == $header->name){
				$headerArry['sub'] = $header->value;
			}
			else if('Delivered-To' == $header->name){
				$headerArry['deliver'] = $header->value;
			}
					}
				var_dump($headerArry);
					exit;		
				
					
		//$dateTime=$headerArry['date'];			
		//$date=explode(' ',$dateTime);
		
		$this->view->header = $headerArry;
		
    }
	public function viewAction(){
		$id = $this->request->getParam('id');
		
		$model = new Model_GmailServiceAccount(8);
		$emails = $model->getAllEmails();
		$service = $model->getService();
		$this->view->id = $id;
        $this->view->emails = $emails;
        $this->view->service = $service;
			
	}
	

    
}