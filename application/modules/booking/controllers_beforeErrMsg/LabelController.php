<?php

class Booking_LabelController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
    }

    public function selectLabelAction() {

        //
        //checkLoggedIn
        //
        CheckAuth::checkPermission(array('bookingLabel'));

        $bookingId = $this->request->getParam('id');
        $modelBooking = new Model_Booking();
        if (!$modelBooking->checkIfCanSeeBookingWithOutTimePeriod($bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this Booking"));
            $this->_redirect($this->router->assemble(array(), 'booking'));
        }
        if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Booking not belongs to your Company"));
            $this->_redirect($this->router->assemble(array(), 'booking'));
        }

        $label_ids = $this->request->getParam('label_ids', array());

        //
        //load model
        //
        $modelLabel = new Model_Label();
        $modelbookingLabel = new Model_BookingLabel();
        $bookingLabels = $modelbookingLabel->getByBookingId($bookingId);

        $labelIds = array();
        if ($bookingLabels) {
            foreach ($bookingLabels as $bookingLabel) {
                $labelIds[] = $bookingLabel['label_id'];
            }
        }

        //
        //get data
        //
        $labels = $modelLabel->getAll();

        $this->view->labels = $labels;

        if ($this->request->isPost()) {

            $modelbookingLabel->setLabelsToBooking($bookingId, $label_ids);

            if (!empty($label_ids)) {
                foreach ($label_ids as $label_id) {
                    $label = $modelLabel->getById($label_id);
                    $json_labels[$label_id] = $label['label_name'];
                }
                echo json_encode(array('result' => 1, 'labels' => $json_labels, 'entityId' => $bookingId));
            } else {
                echo json_encode(array('result' => 0, 'entityId' => $bookingId));
            }
            exit;
        }

        $this->view->bookingId = $bookingId;
        $this->view->label_ids = $labelIds;

        echo $this->view->render('label/select-label.phtml');
        exit;
    }

    public function searchLabelAction() {

        //
        //checkLoggedIn
        //
        CheckAuth::checkPermission(array('bookingLabel'));

        $modelLabel = new Model_Label();

        $filters['keywords'] = $this->request->getParam('label-like');
        $labels = $modelLabel->getAll($filters);
        $this->view->labels = $labels;

        echo $this->view->render('label/search-label.phtml');
        exit;
    }

    public function filterLabelAction() {

        //
        //checkLoggedIn
        //
        CheckAuth::checkPermission(array('bookingFilterLabel'));

        $label_ids = $this->request->getParam('label_ids', array());

        $my_bookings = $this->request->getParam('my_bookings', false);
        $this->view->my_bookings = $my_bookings;
        

        //
        //load model
        //
        $modelLabel = new Model_Label();

        $labels = $modelLabel->getAll();
        $this->view->labels = $labels;

        if ($this->request->isPost()) {
            $fltrLabels = array();
            if ($label_ids) {
                foreach ($label_ids as $label_id) {
                    $fltrLabels[] = 'fltr[label_ids][]=' . $label_id;
                }
            }

            $fltr = implode('&', $fltrLabels);
            if ($my_bookings) {
                $fltr .= ($fltr ? '&' : '') . 'my_bookings=true';
            }

            echo $this->router->assemble(array(), 'booking') . ($fltr ? '?' . $fltr : '');
            exit;
        }

        echo $this->view->render('label/filter-label.phtml');
        exit;
    }

    public function addLabelAction() {
        //
        //checkLoggedIn
        //
        CheckAuth::checkPermission(array('bookingAddLabel'));


        //
        // get request parameters
        //
        $labelName = $this->request->getParam('label_name', '');
        $bookingId = $this->request->getParam('id');

        if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Booking not belongs to your Company"));
            $this->_redirect($this->router->assemble(array(), 'booking'));
        }

        //
        //load model
        //
        $modelLabel = new Model_Label();
        $modelbookingLabel = new Model_BookingLabel();

        //
        // init action form
        //
        $form = new Booking_Form_Label();


        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'label_name' => $labelName,
                    'company_id' => CheckAuth::getCompanySession()
                );

                $labelId = $modelLabel->insert($data);

                $params = array(
                    'booking_id' => $bookingId,
                    'label_id' => $labelId
                );
                $modelbookingLabel->assignLabelToBooking($params);


                $bookingLabels = $modelbookingLabel->getByBookingId($bookingId);
                foreach ($bookingLabels AS $bookingLabel) {
                    $label = $modelLabel->getById($bookingLabel['label_id']);
                    $json_labels[$bookingLabel['label_id']] = $label['label_name'];
                }
                echo json_encode(array('result' => 1, 'labels' => $json_labels, 'entityId' => $bookingId));
                exit;
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('label/add_edit.phtml');
        exit;
    }

}

