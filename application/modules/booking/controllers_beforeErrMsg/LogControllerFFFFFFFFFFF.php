<?php

class Booking_LogController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'bookings';
        $this->view->sub_menu = 'booking';
    }

    public function viewAction() {

        //check Auth for logged user
        CheckAuth::checkPermission(array('bookingHistory'));


        $logId = $this->request->getParam('id', 0);

        $modelBookingLog = new Model_BookingLog();
        $bookingLog = $modelBookingLog->getById($logId);


        if (!$bookingLog) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Booking not have log"));
            $this->_redirect($this->router->assemble(array(), 'booking'));
        }

        $bookingId = $bookingLog['booking_id'];
        $this->view->bookingId = $bookingId;

        if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Booking not belongs to your Company"));
            $this->_redirect($this->router->assemble(array(), 'booking'));
        }

        // get log data
        $modelBookingLog->fill($bookingLog, array('invoice_log', 'estimate_log','address_log','multiple_days_log','booking_product_log','booking_service_log'));
        $this->view->bookingLog = $bookingLog;

    }

}
