<?php

class Booking_ReminderController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
    }

    public function reminderAddAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('bookingReminderAdd'));

        //load model
        $modelUser = new Model_User();

        //
        // get request parameters
        //
        $bookingId = $this->request->getParam('booking_id');

        //
        // init action form
        //
        $form = new Booking_Form_Reminder(array('booking_id' => $bookingId));

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $comment = $this->request->getParam('comment');
                $type = $this->request->getParam('type', 'customer');
                $loggedUser = CheckAuth::getLoggedUser();
                $modelBookingReminder = new Model_BookingReminder();


                $data = array(
                    'booking_id' => $bookingId,
                    'user_id' => $loggedUser['user_id'],
                    'created' => time(),
                    'comment' => $comment,
                    'type' => $type,
                    'user_role' => $loggedUser['role_id']
                );
                $modelBookingReminder->insert($data);
                MobileNotification::notify($bookingId, 'booking confirmation');
                $reminders = array();
                $bookingReminder = $modelBookingReminder->getContactHistory($bookingId);
                foreach ($bookingReminder as $reminder) {

                    if ($reminder['user_role'] == 9) {
                        $modelCustomer = new Model_Customer();
                        $user = $modelCustomer->getById($reminder['user_id']);
                        $username = $user['first_name'] . ' ' . $user['last_name'];
                    } else {
                        $user = $modelUser->getById($reminder['user_id']);
                        $username = $user['username'];
                    }
                    $reminders[] = array(
                        'reminder' => $reminder['type'] . ' - ' . $username . ' - ' . getDateFormating($reminder['created']),
                        'url' => $this->router->assemble(array('id' => $reminder['id']), 'bookingReminderView'),
                        'remove_url' => $this->router->assemble(array('id' => $bookingId, 'reminder_id' => $reminder['id']), 'bookingReminderDelete')
                    );
                }
                $add_url = $this->router->assemble(array('booking_id' => $bookingId), 'bookingReminderAdd');

                $json_array = array('id' => $bookingId, 'reminders' => $reminders, 'add_url' => $add_url);
                echo json_encode($json_array);
                exit;
            }
        }

        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('reminder/add.phtml');
        exit;
    }

    public function reminderDeleteAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('bookingReminderDelete'));

        //load model
        $modelUser = new Model_User();

        //
        // get request parameters
        //
        $bookingId = $this->request->getParam('id');
        $reminderId = $this->request->getParam('reminder_id');

        $modelBookingReminder = new Model_BookingReminder();
        $modelBookingReminder->deleteById($reminderId);

        $reminders = array();
        $bookingReminder = $modelBookingReminder->getContactHistory($bookingId);
        foreach ($bookingReminder as $reminder) {
            $user = $modelUser->getById($reminder['user_id']);
            $reminders[] = array(
                'reminder' => $reminder['type'] . ' - ' . $user['username'] . ' - ' . getDateFormating($reminder['created']),
                'url' => $this->router->assemble(array('id' => $reminder['id']), 'bookingReminderView'),
                'remove_url' => $this->router->assemble(array('id' => $bookingId, 'reminder_id' => $reminder['id']), 'bookingReminderDelete')
            );
        }
        $add_url = $this->router->assemble(array('booking_id' => $bookingId), 'bookingReminderAdd');

        $json_array = array('id' => $bookingId, 'reminders' => $reminders, 'add_url' => $add_url);
        echo json_encode($json_array);
        exit;
    }

    public function reminderViewAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('bookingReminder'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id');
        $modelBookingReminder = new Model_BookingReminder();
        $reminder = $modelBookingReminder->getById($id);

        //
        // init action form
        //
        $form = new Booking_Form_Reminder(array('reminder' => $reminder, 'mode' => 'update'));
        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('reminder/view.phtml');
        exit;
    }

}
