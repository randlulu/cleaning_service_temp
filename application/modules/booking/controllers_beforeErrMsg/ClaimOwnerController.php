<?php

class Booking_ClaimOwnerController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
    }

    public function indexAction() {

        //get request parameters
        $orderBy = $this->request->getParam('sort', 'created');
        $sortingMethod = $this->request->getParam('method', 'desc');
        $currentPage = $this->request->getParam('page', 1);

        //Load Model
        $modelClaimOwner = new Model_ClaimOwner();

        //init pager and articles model object
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        //get data list
        $data = $modelClaimOwner->getAll(array(), "{$orderBy} {$sortingMethod}", $pager);

        $modelClaimOwner->fills($data, array('booking','estimate','invoice', 'customer', 'city', 'address', 'status', 'booking_users'));

        //set view params
        $this->view->data = $data;
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
    }

    public function sendClaimOwnerAction() {

        //
        //CheckAuth
        //
        CheckAuth::checkPermission(array('claimOwner'));

        // get params 
        $bookingId = $this->request->getParam('id', 0);

        $loggedUser = CheckAuth::getLoggedUser();

        // load models
        $modelBooking = new Model_Booking();
        $modelClaimOwner = new Model_ClaimOwner();

        // get booking
        $booking = $modelBooking->getById($bookingId);

        // validation
        if (!$booking) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Booking not exist"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $data = array(
            'booking_id' => $bookingId,
            'user_id' => $loggedUser['user_id'],
            'created' => time()
        );

        $success = $modelClaimOwner->insert($data);

        if ($success) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Claim Request sent successfully"));
        } else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Claim Request not sent"));
        }

        $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
    }

    public function submitClaimOwnerAction() {

        // get params 
        $bookingId = $this->request->getParam('id', 0);
        $userId = $this->request->getParam('user_id', 0);
        $request = $this->request->getParam('request', 'none');

        // load model
        $modelClaimOwner = new Model_ClaimOwner();
        $modelBooking = new Model_Booking();

        $loggedUser = CheckAuth::getLoggedUser();

        $booking = $modelBooking->getById($bookingId);
        //
        // validation
        //
        if (!$booking) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Booking not exist"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        if ($booking['created_by'] != $loggedUser['user_id']) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        if (!in_array($request, array('none', 'accept', 'not_accept'))) {
            $request = 'none';
        }

        $form = new Booking_Form_ClaimOwner(array('booking_id' => $bookingId));

        if ($this->request->isPost()) {
            if ($request != 'none') {
                $claimOwner = $modelClaimOwner->getByBookingAndUserId($bookingId, $userId);
                if ($request == 'accept') {
                    $success = $modelClaimOwner->updateById($claimOwner['request_id'], array('request_submit' => 'accept'));
                    if ($success) {
                        $modelBooking->updateById($bookingId, array('created_by' => $userId));
                    }
                } else {
                    $modelClaimOwner->updateById($claimOwner['request_id'], array('request_submit' => 'not_accept'));
                }
            }

            echo 1;
            exit;
        }
        $this->view->form = $form;
        $this->view->booking_id = $bookingId;
        echo $this->view->render('claim-owner/submit-claim-owner.phtml');
        exit;
    }

}