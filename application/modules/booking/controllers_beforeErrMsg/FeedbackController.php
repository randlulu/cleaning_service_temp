<?php

class Booking_FeedbackController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'bookings';
        $this->view->sub_menu = 'booking';
    }

    public function feedbackListAction() {

        //check Auth for logged user
        CheckAuth::checkPermission(array('feedbackList'));

        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        //
        // load models
        //
       
        $modelCustomerFeedback = new Model_CustomerFeedback();
        $modelBookingStatus = new Model_BookingStatus();

        //get Feedback Status for drop down menu
        $feedbackStatus = $modelBookingStatus->getAllStatusAsArray(array('request_feedback' => true));
        $select = new Zend_Form_Element_Select('fltr');
        $select->setBelongsTo('feedback_status');
        $select->setDecorators(array('ViewHelper'));
        $select->setValue(isset($filters['feedback_status']) ? $filters['feedback_status'] : '');
        $select->setAttrib('onchange', "fltr('" . $this->router->assemble(array(), 'feedbackList') . "?fltr[feedback_status]=','feedback_status-fltr')");
        $select->setAttrib('style', "width: 150px;");
        $select->addMultiOption('', 'Booking Status');
        $select->addMultiOptions($feedbackStatus);
        $this->view->feedbackStatus = $select;


        //init pager and articles model object
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        //get Feedback
        $customerFeedback = $modelCustomerFeedback->getAll($filters, 'created ASC', $pager);
        $modelCustomerFeedback->fills($customerFeedback, array('booking'));

        $this->view->data = $customerFeedback;
        $this->view->sub_menu = 'feedback_booking';
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
    }

    public function feedbackViewAction() {

        //check Auth for logged user
        CheckAuth::checkPermission(array('feedbackView'));

        $feedBackId = $this->request->getParam('id');

        //
        // load models
        //
       
        $modelCustomerFeedback = new Model_CustomerFeedback();
        $feedback = $modelCustomerFeedback->getById($feedBackId);

        //
        // validation
        //
        if (!$feedback) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Feedback not exist"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $this->view->data = $feedback;
        echo $this->view->render('feedback/feedback-view.phtml');
        exit;
    }

}