<?php

class Booking_Form_ContractorShare extends Zend_Form {

   public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('contractorShare');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $booking = (isset($options['booking']) ? $options['booking'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();
        $request = Zend_Controller_Front::getInstance()->getRequest();

        $contractorShareVal = (!empty($booking['contractor_share']) ? $booking['contractor_share'] : '');

        $contractorShare = new Zend_Form_Element_Text('contractor_share');
        $contractorShare->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue($contractorShareVal);
     

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'button'));


        $this->addElements(array($contractorShare, $button));
        $this->setMethod('post');
        
        if ('update' == $mode) {
           // $this->setAction($router->assemble(array(''), ''));
        } else {
            $this->setAction($router->assemble(array('id' => $booking['booking_id']), 'setContractorShare'));
        }
    }

}

