<?php

class Booking_Form_Booking extends Zend_Form {
    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('Booking');
        $mode          = (isset($options['mode']) ? $options['mode'] : '');
        $booking = (isset($options['booking']) ? $options['booking'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();
         

        $title = new Zend_Form_Element_Text('title');
        $title->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control'))
                ->setValue( (!empty($booking['title']) ? $booking['title'] : '') )
                ->setErrorMessages(array('Required' => 'Please enter the booking title'));

        $button = new Zend_Form_Element_Submit('button');
        $button-->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'button btn-primary'));

        $this->addElements(array($title, $button));
        $this->setMethod('post');
        if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $booking['booking_id']), 'bookingEdit'));
        } else {
            $this->setAction($router->assemble(array(), 'booking'));
        }
    }

}

