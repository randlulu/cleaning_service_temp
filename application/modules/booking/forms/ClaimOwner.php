<?php

class Booking_Form_ClaimOwner extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('claimOwner');

        $bookingId = $options['booking_id'];

        $router = Zend_Controller_Front::getInstance()->getRouter();
        $request = Zend_Controller_Front::getInstance()->getRequest();


        // load model
        $modelClaimOwner = new Model_ClaimOwner();
        $requestUsers = $modelClaimOwner->getUserClaimBybookingIdAsArray($bookingId);

        $users = new Zend_Form_Element_Select('user_id');
        $users->setDecorators(array('ViewHelper'));
        $users->setAttrib('class', "form-control");
        $users->addMultiOptions($requestUsers);

        $request = new Zend_Form_Element_Radio('request');
        //$request->setAttrib('style', "vertical-align: text-bottom;");
        $request->addMultiOption('accept', 'Accept');
        $request->addMultiOption('not_accept', 'Not Accept');
        $request->removeDecorator('DtDdWrapper');
        $request->removeDecorator('HtmlTag');
        $request->removeDecorator('Label');
        $request->setSeparator('<br/>');

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));

        $this->addElements(array($users, $request, $button));
        $this->setMethod('post');
        $this->setAction($router->assemble(array('id' => $bookingId), 'submitClaimOwner'));
    }

}

