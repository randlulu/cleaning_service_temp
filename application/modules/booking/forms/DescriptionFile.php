<?php

class Booking_Form_DescriptionFile extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('DescriptionFile');

        $description = (isset($options['description']) ? $options['description'] : '');
        $id = (isset($options['id']) ? $options['id'] : '');
        
        $router = Zend_Controller_Front::getInstance()->getRouter();

        $descriptionFile = new Zend_Form_Element_Textarea('description');
        $descriptionFile->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control', 'cols' => '40', 'rows' => '10'))
                ->setValue($description);


        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'btn btn-primary'));

        $this->addElements(array($descriptionFile, $button));
        $this->setMethod('post');
        $this->setAction($router->assemble(array('id' => $id), 'bookingFileEditDescription'));
    
    }

}
