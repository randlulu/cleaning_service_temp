<?php

class Booking_Form_ContactHistory extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('ContactHistory');

        $contactHistory = (isset($options['contact_history']) ? $options['contact_history'] : '');
        $bookingId = (isset($options['booking_id']) ? $options['booking_id'] : '');
        $mode = (isset($options['mode']) ? $options['mode'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();


        //
        // comment
        //
        $comment = new Zend_Form_Element_Textarea('comment');
        $comment->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'textarea_field', 'style' => 'width: 330px;'))
                ->setValue((!empty($contactHistory['comment']) ? $contactHistory['comment'] : ''));

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'button'));

        $this->addElements(array($comment, $button));

        if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $contactHistory['id']), 'bookingContactHistoryView'));
        } else {
            $this->setAction($router->assemble(array('booking_id' => $bookingId), 'bookingContactHistoryAdd'));
        }
    }

}

