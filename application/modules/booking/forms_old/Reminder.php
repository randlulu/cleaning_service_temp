<?php

class Booking_Form_Reminder extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('Reminder');

        $reminder = (isset($options['reminder']) ? $options['reminder'] : '');
        $bookingId = (isset($options['booking_id']) ? $options['booking_id'] : '');
        $mode = (isset($options['mode']) ? $options['mode'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();


        //
        // comment
        //
        $comment = new Zend_Form_Element_Textarea('comment');
        $comment->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'textarea_field', 'style' => 'width: 330px;'))
                ->setValue((!empty($reminder['comment']) ? $reminder['comment'] : ''));


        $type = new Zend_Form_Element_Select('type');
        $type->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setValue((!empty($reminder['type']) ? $reminder['type'] : ''));
        $modelBookingReminder = new Model_BookingReminder();
        $reminderTypes = $modelBookingReminder->getReminderType();
        $type->addMultiOptions($reminderTypes);

        
        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'button'));

        $this->addElements(array($comment,$type, $button));

        if ('update' == $mode) {
            $this->setAction($router->assemble(array('id' => $reminder['id']), 'bookingReminderView'));
        } else {
            $this->setAction($router->assemble(array('booking_id' => $bookingId), 'bookingReminderAdd'));
        }
    }

}

