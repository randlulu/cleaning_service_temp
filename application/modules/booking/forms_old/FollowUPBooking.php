<?php

class Booking_Form_FollowUPBooking extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('FollowUPBooking');

        $booking = (isset($options['booking']) ? $options['booking'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();

        
        $followDateVal = (!empty($booking['to_follow']) ? php2JsTime($booking['to_follow']) : '');

        $followDate = new Zend_Form_Element_Text('followDate');
        $followDate->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'date text_input'))
                ->setValue($followDateVal);


        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'button'));
        $button->setAttribs(array('style' => 'margin-top: 10px;'));

        $this->addElements(array($followDate, $button));
        $this->setMethod('post');
        $this->setAction($router->assemble(array('id' => $booking['booking_id']), 'followUpBooking'));
    }

}

