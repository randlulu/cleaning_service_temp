<form class="form-horizontal" id="form_follow" method="<?php echo $this->escape($this->form->getMethod()); ?>" onsubmit="do_submit('<?php echo $this->escape($this->form->getAction()); ?>','#form_follow','#content_modal_popup');return false;">
    <div class="success_message"><?php echo $this->escape($this->successMessage); ?></div>
	
	<div class="form-group">
				<label class="col-md-3">Follow Date:</label>
				<div class="col-md-9 " id="button_cont">
				   
					 <div class='input-group date' id='datetimepicker2'>
                   <?php echo $this->form->getElement('followDate')->render(); ?>
                    <span class="input-group-addon">
						<i class="fa fa-calendar"></i>
                    </span>
                </div>
				</div>
		</div>
		
	<div class="form-group">
				
				<div class="col-md-9 col-md-offset-3" id="button_cont">
						<?php echo $this->form->getElement('button')->render(); ?>
					
				</div>
		</div>	
		
</form>

<script type="text/javascript">
    
    $(document).ready(function() {
		   $('#datetimepicker2').datetimepicker({
			   format:'D-M-YYYY LT'
			   
		   });
			
    });

</script>