<?php

class Booking_ReminderController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
    }

    public function reminderAddAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('bookingReminderAdd'));

        //load model
        $modelUser = new Model_User();

        //
        // get request parameters
        //
        $bookingId = $this->request->getParam('booking_id');

        //
        // init action form
        //
        $form = new Booking_Form_Reminder(array('booking_id' => $bookingId));

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $comment = $this->request->getParam('comment');
                $type = $this->request->getParam('type', 'customer');
                $loggedUser = CheckAuth::getLoggedUser();
                $modelBookingReminder = new Model_BookingReminder();


                $data = array(
                    'booking_id' => $bookingId,
                    'user_id' => $loggedUser['user_id'],
                    'created' => time(),
                    'comment' => $comment,
                    'type' => $type,
                    'user_role' => $loggedUser['role_id'],
                    'customer_contact_id' => ''
                );
                $modelBookingReminder->insert($data);
                MobileNotificationNew::notify('booking confirmation', array('item_id' => $bookingId, 'item_type' => 'booking'));
                $reminders = array();
                $bookingReminder = $modelBookingReminder->getContactHistory($bookingId, 'All');


                foreach ($bookingReminder as $reminder) {

                    if ($reminder['user_role'] == 9) {
                        if ($reminder['customer_contact_id'] != null) {
                            $modelCustomerContact = new Model_CustomerContact();
                            $user = $modelCustomerContact->getById($reminder['customer_contact_id']);
                            $username = $user['first_name'] . ' ' . $user['last_name'];
                        } else {
                            $modelCustomer = new Model_Customer();
                            $user = $modelCustomer->getById($reminder['user_id']);
                            $username = $user['first_name'] . ' ' . $user['last_name'];
                        }
                    } else {
                        $user = $modelUser->getById($reminder['user_id']);
                        $username = $user['username'];
                    }
                    $deleteUrl = $this->router->assemble(array('id' => $bookingId, 'reminder_id' => $reminder['id']), 'bookingReminderDelete');
                    $customerName = '';
                    if ($reminder['type'] == 'Customer') {
                        if ($reminder['customer_contact_id'] != null) {
                            $modelCustomerContact = new Model_CustomerContact();
                            $user = $modelCustomerContact->getById($reminder['customer_contact_id']);
                            $customerName = ' ( ' . $user['first_name'] . ' ' . $user['last_name'] . ' ) ';
                        } else {

                            $modelBooking = new Model_Booking();
                            $booking = $modelBooking->getById($bookingId);
                            $modelCustomer = new Model_Customer();
                            $user = $modelCustomer->getById($booking['customer_id']);
                            $customerName = ' ( ' . $user['first_name'] . ' ' . $user['last_name'] . ' ) ';
                        }
                    }
                    $reminders[] = array(
                        'reminder' => $reminder['type'] . $customerName . ' - ' . $username . ' - ' . getDateFormating($reminder['created']),
                        'url' => $this->router->assemble(array('id' => $reminder['id']), 'bookingReminderView'),
                        'remove_url' => $deleteUrl,
                        'booking_id' => $bookingId,
                        'reminder_id' => $reminder['id'],
                        'comment' => $reminder['comment']
                    );
                }

                $add_url = $this->router->assemble(array('booking_id' => $bookingId), 'bookingReminderAdd');

                $json_array = array('id' => $bookingId, 'reminders' => $reminders, 'add_url' => $add_url, 'succsses' => '1');
                echo json_encode($json_array);
                exit;
            }
        }

        $this->view->form = $form;
        ///
        // render views
        //
		$this->view->bookingId = $bookingId;
        echo $this->view->render('reminder/add.phtml');
        exit;
    }

    public function customerContactReminderAddAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('bookingReminderAdd'));

        //load model
        $modelUser = new Model_User();

        //
        // get request parameters
        //
        $bookingId = $this->request->getParam('booking_id');

        $customer_contact_id = $this->request->getParam('customer_contact_id');
        //echo 'customer_contact_id is : '.$customer_contact_id;
        //
        // init action form
        //
        $form = new Booking_Form_Reminder(array('booking_id' => $bookingId));

        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $comment = $this->request->getParam('comment');
                $type = $this->request->getParam('type', 'customer');
                $loggedUser = CheckAuth::getLoggedUser();
                $modelBookingReminder = new Model_BookingReminder();

                $data = array(
                    'booking_id' => $bookingId,
                    'user_id' => $loggedUser['user_id'],
                    'created' => time(),
                    'comment' => $comment,
                    'type' => $type,
                    'user_role' => $loggedUser['role_id'],
                    'customer_contact_id' => $customer_contact_id
                );
                $modelBookingReminder->insert($data);
                //MobileNotification::notify($bookingId, 'booking confirmation');
                $reminders = array();
                $bookingReminder = $modelBookingReminder->getByBookingIdAndCustomerContact($bookingId, $customer_contact_id);

                foreach ($bookingReminder as $reminder) {
                    $customerName = '';
                    if ($reminder['user_role'] == 9) {
                        if ($reminder['customer_contact_id'] != null) {
                            $modelCustomerContact = new Model_CustomerContact();
                            $user = $modelCustomerContact->getById($reminder['customer_contact_id']);
                            $username = $user['first_name'] . ' ' . $user['last_name'];
                        } else {
                            $modelCustomer = new Model_Customer();
                            $user = $modelCustomer->getById($reminder['user_id']);
                            $username = $user['first_name'] . ' ' . $user['last_name'];
                        }
                    } else {
                        $user = $modelUser->getById($reminder['user_id']);
                        $username = $user['username'];
                    }
                    if ($reminder['type'] == 'Customer') {
                        if ($reminder['customer_contact_id'] != null) {
                            $modelCustomerContact = new Model_CustomerContact();
                            $user = $modelCustomerContact->getById($reminder['customer_contact_id']);
                            $customerName = ' ( ' . $user['first_name'] . ' ' . $user['last_name'] . ' ) ';
                        } else {
                            $modelBooking = new Model_Booking();
                            $booking = $modelBooking->getById($this->bookingId);
                            $modelCustomer = new Model_Customer();
                            $user = $modelCustomer->getById($booking['customer_id']);
                            $customerName = ' ( ' . $user['first_name'] . ' ' . $user['last_name'] . ' ) ';
                        }
                    }
                    $deleteUrl = $this->router->assemble(array('id' => $bookingId, 'reminder_id' => $reminder['id'], 'customer_contact_id' => $customer_contact_id), 'bookingReminderDelete_CustomerContact');

                    $reminders[] = array(
                        'reminder' => $reminder['type'] . $customerName . ' - ' . $username . ' - ' . getDateFormating($reminder['created']),
                        'url' => $this->router->assemble(array('id' => $reminder['id']), 'bookingReminderView'),
                        'remove_url' => $deleteUrl,
                        'booking_id' => $bookingId,
                        'reminder_id' => $reminder['id'],
                        'comment' => $reminder['comment']
                    );
                }

                $add_url = $this->router->assemble(array('booking_id' => $bookingId, 'customer_contact_id' => $customer_contact_id), 'bookingReminderAdd_CustomerContact');

                $json_array = array('id' => $bookingId, 'reminders' => $reminders, 'add_url' => $add_url, 'succsses' => '1', 'customer_contact_id' => $customer_contact_id);
                echo json_encode($json_array);
                exit;
            }
        }

        $this->view->form = $form;
        $this->view->customer_contact_id = $customer_contact_id;
        ///
        // render views
        //
		$this->view->bookingId = $bookingId;
        echo $this->view->render('reminder/customerContactAdd.phtml');
        exit;
    }

    public function reminderDeleteAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('bookingReminderDelete'));

        //load model
        $modelUser = new Model_User();

        //
        // get request parameters
        //
		
        $bookingId = $this->request->getParam('id');
        $reminderId = $this->request->getParam('reminder_id');

        $modelBookingReminder = new Model_BookingReminder();
        $modelBookingReminder->deleteById($reminderId);

        $reminders = array();
        ////

        $bookingReminder = $modelBookingReminder->getContactHistory($bookingId, 'All');
        foreach ($bookingReminder as $reminder) {

            if ($reminder['user_role'] == 9) {
                if ($reminder['customer_contact_id'] != null) {
                    $modelCustomerContact = new Model_CustomerContact();
                    $user = $modelCustomerContact->getById($reminder['customer_contact_id']);
                    $username = $user['first_name'] . ' ' . $user['last_name'];
                } else {
                    $modelCustomer = new Model_Customer();
                    $user = $modelCustomer->getById($reminder['user_id']);
                    $username = $user['first_name'] . ' ' . $user['last_name'];
                }
            } else {
                $user = $modelUser->getById($reminder['user_id']);
                $username = $user['username'];
            }
            $customerName = '';
            if ($reminder['type'] == 'Customer') {
                if ($reminder['customer_contact_id'] != null) {
                    $modelCustomerContact = new Model_CustomerContact();
                    $user = $modelCustomerContact->getById($reminder['customer_contact_id']);
                    $customerName = ' ( ' . $user['first_name'] . ' ' . $user['last_name'] . ' ) ';
                } else {
                    $modelBooking = new Model_Booking();
                    $booking = $modelBooking->getById($this->bookingId);
                    $modelCustomer = new Model_Customer();
                    $user = $modelCustomer->getById($booking['customer_id']);
                    $customerName = ' ( ' . $user['first_name'] . ' ' . $user['last_name'] . ' ) ';
                }
            }
            $deleteUrl = $this->router->assemble(array('id' => $bookingId, 'reminder_id' => $reminder['id']), 'bookingReminderDelete');

            $reminders[] = array(
                'reminder' => $reminder['type'] . $customerName . ' - ' . $username . ' - ' . getDateFormating($reminder['created']),
                'url' => $this->router->assemble(array('id' => $reminder['id']), 'bookingReminderView'),
                'remove_url' => $deleteUrl,
                'booking_id' => $bookingId,
                'reminder_id' => $reminder['id'],
                'comment' => $reminder['comment']
            );
        }

        $add_url = $this->router->assemble(array('booking_id' => $bookingId), 'bookingReminderAdd');

        $json_array = array('id' => $bookingId, 'reminders' => $reminders, 'add_url' => $add_url, 'succsses' => '1');
        echo json_encode($json_array);
        exit;
    }

    //Added By Rand
    public function customerContactReminderDeleteAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('bookingReminderDelete'));

        //load model
        $modelUser = new Model_User();

        //
        // get request parameters
        //
		
        $bookingId = $this->request->getParam('id');
        $reminderId = $this->request->getParam('reminder_id');
        $customer_contact_id = $this->request->getParam('customer_contact_id');

        $modelBookingReminder = new Model_BookingReminder();
        $modelBookingReminder->deleteById($reminderId);

        $reminders = array();
        ////

        $bookingReminder = $modelBookingReminder->getContactHistory($bookingId, 'All');
        $bookingReminder = $modelBookingReminder->getByBookingIdAndCustomerContact($bookingId, $customer_contact_id);

        foreach ($bookingReminder as $reminder) {

            if ($reminder['user_role'] == 9) {
                if ($reminder['customer_contact_id'] != null) {
                    $modelCustomerContact = new Model_CustomerContact();
                    $user = $modelCustomerContact->getById($reminder['customer_contact_id']);
                    $username = $user['first_name'] . ' ' . $user['last_name'];
                } else {
                    $modelCustomer = new Model_Customer();
                    $user = $modelCustomer->getById($reminder['user_id']);
                    $username = $user['first_name'] . ' ' . $user['last_name'];
                }
            } else {
                $user = $modelUser->getById($reminder['user_id']);
                $username = $user['username'];
            }
            $deleteUrl = $this->router->assemble(array('id' => $bookingId, 'reminder_id' => $reminder['id'], 'customer_contact_id' => $customer_contact_id), 'bookingReminderDelete_CustomerContact');

            $customerName = '';
            if ($reminder['type'] == 'Customer') {
                if ($reminder['customer_contact_id'] != null) {
                    $modelCustomerContact = new Model_CustomerContact();
                    $user = $modelCustomerContact->getById($reminder['customer_contact_id']);
                    $customerName = ' ( ' . $user['first_name'] . ' ' . $user['last_name'] . ' ) ';
                } else {
                    $modelBooking = new Model_Booking();
                    $booking = $modelBooking->getById($this->bookingId);
                    $modelCustomer = new Model_Customer();
                    $user = $modelCustomer->getById($booking['customer_id']);
                    $customerName = ' ( ' . $user['first_name'] . ' ' . $user['last_name'] . ' ) ';
                }
            }

            $reminders[] = array(
                'reminder' => $reminder['type'] . $customerName . ' - ' . $username . ' - ' . getDateFormating($reminder['created']),
                'url' => $this->router->assemble(array('id' => $reminder['id']), 'bookingReminderView'),
                'remove_url' => $deleteUrl,
                'booking_id' => $bookingId,
                'reminder_id' => $reminder['id'],
                'comment' => $reminder['comment']
            );
        }

        $add_url = $this->router->assemble(array('booking_id' => $bookingId, 'customer_contact_id' => $customer_contact_id), 'bookingReminderAdd_CustomerContact');


        $json_array = array('id' => $bookingId, 'reminders' => $reminders, 'add_url' => $add_url, 'succsses' => '1');
        echo json_encode($json_array);
        exit;
    }

    public function reminderViewAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('bookingReminder'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id');
        $modelBookingReminder = new Model_BookingReminder();
        $reminder = $modelBookingReminder->getById($id);

        //
        // init action form
        //
        $form = new Booking_Form_Reminder(array('reminder' => $reminder, 'mode' => 'update'));
        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('reminder/view.phtml');
        exit;
    }

}
