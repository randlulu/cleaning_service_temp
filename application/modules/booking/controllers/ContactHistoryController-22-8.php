<?php

class Booking_ContactHistoryController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
    }

    public function contactHistoryAddAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('bookingContactHistoryAdd'));

        //load model
        $modelUser = new Model_User();

        //
        // get request parameters
        //
        $bookingId = $this->request->getParam('booking_id');

        //
        // init action form
        //
        $form = new Booking_Form_ContactHistory(array('booking_id' => $bookingId));
        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $comment = $this->request->getParam('comment');
                $loggedUser = CheckAuth::getLoggedUser();
                $modelInquiryReminder = new Model_InquiryReminder();
                $modelBookingContactHistory = new Model_BookingContactHistory();

                $data = array(
                    'booking_id' => $bookingId,
                    'user_id' => $loggedUser['user_id'],
                    'created' => time(),
                    'comment' => $comment
                );
                $modelBookingContactHistory->insert($data);

                $modelBooking = new Model_Booking();
                $booking = $modelBooking->getById($bookingId);
                $reminders = array();
                if ($booking['original_inquiry_id']) {
                    $inquiryReminder = $modelInquiryReminder->getContactHistory($booking['original_inquiry_id']);
                    foreach ($inquiryReminder as $reminder) {
                        $user = $modelUser->getById($reminder['user_id']);
                        $reminders[] = array(
                            'reminder' => $user['username'] . ' - ' . getDateFormating($reminder['created']),
                            'url' => $this->router->assemble(array('id' => $reminder['id']), 'inquiryReminderView'),
                            'remove_url' => $this->router->assemble(array('id' => $booking['original_inquiry_id'], 'reminder_id' => $reminder['id']), 'inquiryReminderDelete')
                        );
                    }
                }

                $contactHistory = array();
                $bookingContactHistories = $modelBookingContactHistory->getContactHistory($bookingId);
                foreach ($bookingContactHistories as $bookingContactHistory) {
                    $user = $modelUser->getById($bookingContactHistory['user_id']);
                    $contactHistory[] = array(
                        'reminder' => $user['username'] . ' - ' . getDateFormating($bookingContactHistory['created']),
                        'url' => $this->router->assemble(array('id' => $bookingContactHistory['id']), 'bookingContactHistoryView'),
                        'remove_url' => $this->router->assemble(array('id' => $bookingId, 'contact_history_id' => $bookingContactHistory['id']), 'bookingContactHistoryDelete')
                    );
                }
                $add_url = $this->router->assemble(array('booking_id' => $bookingId), 'bookingContactHistoryAdd');

                $contactHistory = array_merge($reminders, $contactHistory);

                $json_array = array('id' => $bookingId, 'reminders' => $contactHistory, 'add_url' => $add_url);
                echo json_encode($json_array);
                exit;
            }
        }

        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('contact-history/add.phtml');
        exit;
    }

	//ADDED BY RAND
	public function contactHistoryAddReminderAction() {
        // check Auth for logged user
        CheckAuth::checkPermission(array('bookingContactHistoryAdd'));

        //load model
        $modelUser = new Model_User();

        // get request parameters
        $bookingId = $this->request->getParam('booking_id');

        // init action form
        $form = new Booking_Form_ContactHistory(array('booking_id' => $bookingId));
        
        // handling the insertion process
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $comment = $this->request->getParam('comment');
                $loggedUser = CheckAuth::getLoggedUser();
                $modelInquiryReminder = new Model_InquiryReminder();
                $modelBookingContactHistory = new Model_BookingContactHistory();

                $data = array(
                    'booking_id' => $bookingId,
                    'user_id' => $loggedUser['user_id'],
                    'created' => time(),
                    'comment' => $comment
                );
                $modelBookingContactHistory->insert($data);

                $modelBooking = new Model_Booking();
                $booking = $modelBooking->getById($bookingId);
                $reminders = array();
                if ($booking['original_inquiry_id']) {
                    $inquiryReminder = $modelInquiryReminder->getContactHistory($booking['original_inquiry_id']);
                    foreach ($inquiryReminder as $reminder) {
                        $user = $modelUser->getById($reminder['user_id']);
						if ($user['avatar'] != null) {
							$avatar_url= $this->getRequest()->getBaseUrl('/uploads/user_pic/thumb_medium/' . $user['avatar']);
							} 
						else {
							$avatar_url=  $this->getRequest()->getBaseUrl('/pic/contractor_pic1.png');
							}
                        $reminders[] = array(
						    'avatar' => $avatar_url,
                            'reminder' => $user['username'] . ' - ' . getDateFormating($reminder['created']),
							'comment' => $reminder['comment'],
                            'url' => $this->router->assemble(array('id' => $reminder['id']), 'inquiryReminderView'),
                            'remove_url' => $this->router->assemble(array('booking_id' => $bookingId, 'id' => $booking['original_inquiry_id'], 'reminder_id' => $reminder['id']), 'inquiryReminderDeleteHistory')
                        );
                    }
                }

                $contactHistory = array();
                $bookingContactHistories = $modelBookingContactHistory->getContactHistory($bookingId);
                foreach ($bookingContactHistories as $bookingContactHistory) {
                    $user = $modelUser->getById($bookingContactHistory['user_id']);
					if ($user['avatar'] != null) {
							$avatar_url= $this->getRequest()->getBaseUrl('/uploads/user_pic/thumb_medium/' . $user['avatar']);
							} 
						else {
							$avatar_url= $this->getRequest()->getBaseUrl('/pic/contractor_pic1.png');
							}
                    $contactHistory[] = array(
					    'avatar' => $avatar_url,
                        'reminder' => $user['username'] . ' - ' . getDateFormating($bookingContactHistory['created']),
						'comment' => $bookingContactHistory['comment'],
                        'url' => $this->router->assemble(array('id' => $bookingContactHistory['id']), 'bookingContactHistoryView'),
                        'remove_url' => $this->router->assemble(array('id' => $bookingId, 'contact_history_id' => $bookingContactHistory['id']), 'bookingContactHistoryDeleteReminder')
                    );
                }
                $add_url = $this->router->assemble(array('booking_id' => $bookingId), 'bookingContactHistoryAddReminder');

                $contactHistory = array_merge($reminders, $contactHistory);

                $json_array = array('id' => $bookingId, 'reminders' => $contactHistory, 'add_url' => $add_url);
                echo json_encode($json_array);
                exit;
            }
        }

        $this->view->form = $form;
        $this->view->booking_id =$bookingId;
        //
        // render views
        //
        echo $this->view->render('contact-history/addContactHistory.phtml');
        exit;
    }
	
	public function contactHistoryDeleteReminderAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('bookingContactHistoryDelete'));

        //load model
        $modelUser = new Model_User();

        //
        // get request parameters
        //
        $bookingId = $this->request->getParam('id');
        $contactHistoryId = $this->request->getParam('contact_history_id');

        $modelBookingContactHistory = new Model_BookingContactHistory();
        $modelInquiryReminder = new Model_InquiryReminder();
        $modelBookingContactHistory->deleteById($contactHistoryId);

        $modelBooking = new Model_Booking();
        $booking = $modelBooking->getById($bookingId);
        $reminders = array();
        if ($booking['original_inquiry_id']) {
            $inquiryReminder = $modelInquiryReminder->getContactHistory($booking['original_inquiry_id']);
            foreach ($inquiryReminder as $reminder) {
                $user = $modelUser->getById($reminder['user_id']);
				if ($user['avatar'] != null) {
							$avatar_url= $this->getRequest()->getBaseUrl('/uploads/user_pic/thumb_medium/' . $user['avatar']);
							} 
						else {
							$avatar_url=  $this->getRequest()->getBaseUrl('/pic/contractor_pic1.png');
							}
                $reminders[] = array(
                            'avatar' => $avatar_url,
                            'reminder' => $user['username'] . ' - ' . getDateFormating($reminder['created']),
							'comment' => $reminder['comment'],
                            'url' => $this->router->assemble(array('id' => $reminder['id']), 'inquiryReminderView'),
                            'remove_url' => $this->router->assemble(array('booking_id' => $bookingId, 'id' => $booking['original_inquiry_id'], 'reminder_id' => $reminder['id']), 'inquiryReminderDeleteHistory')
                        );
            }
        }

        $contactHistory = array();
        $bookingContactHistories = $modelBookingContactHistory->getContactHistory($bookingId);
        foreach ($bookingContactHistories as $bookingContactHistory) {
            $user = $modelUser->getById($bookingContactHistory['user_id']);
			if ($user['avatar'] != null) {
							$avatar_url= $this->getRequest()->getBaseUrl('/uploads/user_pic/thumb_medium/' . $user['avatar']);
							} 
						else {
							$avatar_url=  $this->getRequest()->getBaseUrl('/pic/contractor_pic1.png');
							}
            $contactHistory[] = array(
					    'avatar' => $avatar_url,
                        'reminder' => $user['username'] . ' - ' . getDateFormating($bookingContactHistory['created']),
                        'url' => $this->router->assemble(array('id' => $bookingContactHistory['id']), 'bookingContactHistoryView'),
						'comment' => $bookingContactHistory['comment'],
                        'remove_url' => $this->router->assemble(array('id' => $bookingId, 'contact_history_id' => $bookingContactHistory['id']), 'bookingContactHistoryDeleteReminder')
                    );
        }
        $add_url = $this->router->assemble(array('booking_id' => $bookingId), 'bookingContactHistoryAddReminder');

        $contactHistory = array_merge($reminders, $contactHistory);

        $json_array = array('id' => $bookingId, 'reminders' => $contactHistory, 'add_url' => $add_url);
        echo json_encode($json_array);
        exit;
    }
	///End RAND
	
    public function contactHistoryDeleteAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('bookingContactHistoryDelete'));

        //load model
        $modelUser = new Model_User();

        //
        // get request parameters
        //
        $bookingId = $this->request->getParam('id');
        $contactHistoryId = $this->request->getParam('contact_history_id');

        $modelBookingContactHistory = new Model_BookingContactHistory();
        $modelInquiryReminder = new Model_InquiryReminder();
        $modelBookingContactHistory->deleteById($contactHistoryId);

        $modelBooking = new Model_Booking();
        $booking = $modelBooking->getById($bookingId);
        $reminders = array();
        if ($booking['original_inquiry_id']) {
            $inquiryReminder = $modelInquiryReminder->getContactHistory($booking['original_inquiry_id']);
            foreach ($inquiryReminder as $reminder) {
                $user = $modelUser->getById($reminder['user_id']);
                $reminders[] = array(
                    'reminder' => $user['username'] . ' - ' . getDateFormating($reminder['created']),
                    'url' => $this->router->assemble(array('id' => $reminder['id']), 'inquiryReminderView'),
                    'remove_url' => $this->router->assemble(array('id' => $booking['original_inquiry_id'], 'reminder_id' => $reminder['id']), 'inquiryReminderDelete')
                );
            }
        }

        $contactHistory = array();
        $bookingContactHistories = $modelBookingContactHistory->getContactHistory($bookingId);
        foreach ($bookingContactHistories as $bookingContactHistory) {
            $user = $modelUser->getById($bookingContactHistory['user_id']);
            $contactHistory[] = array(
                'reminder' => $user['username'] . ' - ' . getDateFormating($bookingContactHistory['created']),
                'url' => $this->router->assemble(array('id' => $bookingContactHistory['id']), 'bookingContactHistoryView'),
                'remove_url' => $this->router->assemble(array('id' => $bookingId, 'contact_history_id' => $bookingContactHistory['id']), 'bookingContactHistoryDelete')
            );
        }
        $add_url = $this->router->assemble(array('booking_id' => $bookingId), 'bookingContactHistoryAdd');

        $contactHistory = array_merge($reminders, $contactHistory);

        $json_array = array('id' => $bookingId, 'reminders' => $contactHistory, 'add_url' => $add_url);
        echo json_encode($json_array);
        exit;
    }

    public function contactHistoryViewAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('bookingContactHistoryView'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id');
        $modelBookingContactHistory = new Model_BookingContactHistory();
        $contactHistory = $modelBookingContactHistory->getById($id);

        //
        // init action form
        //
        $form = new Booking_Form_ContactHistory(array('contact_history' => $contactHistory, 'mode' => 'update'));
        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('contact-history/view.phtml');
        exit;
    }

}

