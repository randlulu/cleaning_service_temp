<?php
// Test comit
class Booking_AttachmentController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
        $pageName = $this->request->getParam('page_name', '');
        $this->view->page_title = (!empty($pageName))?$pageName." - Attachments - Bookings & appointments":"Attachments - Bookings & appointments";

    }

    public function indexAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('attachment'));

        // get request parameters
        $bookingId = $this->request->getParam('id');
        $orderBy = $this->request->getParam('sort', 'attachment_id');
        $sortingMethod = $this->request->getParam('method', 'asc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        // Load Model
        $modelBookingAttachment = new Model_BookingAttachment();
        $modelBooking = new Model_Booking();

        $booking = $modelBooking->getById($bookingId);

        if (!$booking) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Booking not found"));
            $this->_redirect($this->router->assemble(array(), 'booking'));
        }
        if (!$modelBooking->checkIfCanSeeBooking($bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this booking"));
            $this->_redirect($this->router->assemble(array(), 'booking'));
        }

        $filters['booking_id'] = $bookingId;
        $filters['inquiry_id'] = $booking['original_inquiry_id'];

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];

        //
        // get data list
        //
        $bookingAttachment = $modelBookingAttachment->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);


        //
        // set view params
        //
        $this->view->bookingAttachment = $bookingAttachment;
        $this->view->booking_id = $bookingId;
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
    }

    public function fileUploadAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('attachmentFileUpload'));

        //
        // get request parameters
        //
        $bookingId = $this->request->getParam('booking_id');

        // load model
        $modelBookingAttachment = new Model_BookingAttachment();
        $modelBooking = new Model_Booking();

        if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this page"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        if (!$modelBooking->checkIfCanSeeBooking($bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this booking"));
            $this->_redirect($this->router->assemble(array('id' => $bookingId), 'bookingAttachment'));
        }
        $loggedUser = CheckAuth::getLoggedUser();
        $userId = $loggedUser['user_id'];


        //init action form
        $form = new Booking_Form_Attachment(array('booking_id' => $bookingId));
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {

            if (!$form->isValid($this->getRequest()->getParams())) {
                echo $this->view->render('attachment/file-upload.phtml');
                exit;
                //return $this->render('file-upload');
            }

            if (!$form->file->receive()) {
                $this->view->message = '<div class="errors">Error: Failed to receive file</div>';
                echo $this->view->render('attachment/file-upload.phtml');
                exit;
                //return $this->render('file-upload');
            }

            if ($form->file->isUploaded()) {
                $description = $form->description->getvalue();
                $source = $form->file->getFileName();
                $size = $form->file->getfilesize();
                $fileInfo = pathinfo($source);
                $ext = $fileInfo['extension'];
                $workOrder = $form->work_order->getvalue();


                //to re-name the file, all you need to do is save it with a new name, instead of the name they uploaded it with. Normally, I use the primary key of the database row where I'm storing the name of the image. For example, if it's an image of Person 1, I call it 1.jpg. The important thing is that you make sure the image name will be unique in whatever directory you save it to.
                //get sub dir
                $dir = get_config('attachment') . '/';
                $subdir = date('Y/m/d/');

                //check if file exists or not
                $fullDir = $dir . $subdir;

                if (!is_dir($fullDir)) {
                    mkdir($fullDir, 0777, true);
                }

                $data = array(
                    'booking_id' => $bookingId,
                    'created' => time(),
                    'size' => $size,
                    'description' => $description,
                    'created_by' => $userId,
                    'work_order' => $workOrder
                );

                $attachmentId = $modelBookingAttachment->insert($data);
                $fileName = $attachmentId . '.' . $ext;

                $data = array(
                    'path' => $subdir . $fileName,
                    'file_name' => $fileInfo['basename']
                );

                $modelBookingAttachment->updateById($attachmentId, $data);

                //save image to database and filesystem here
                $file_saved = copy($source, $fullDir . $fileName);

                if ($file_saved) {

                    if (file_exists($source)) {
                        unlink($source);
                    }
                    //$this->_redirect($this->router->assemble(array('id' => $bookingId), 'bookingAttachment'));
                    echo 1;
                    exit;
                }
            }
        }

        //
        // render views
        //
        echo $this->view->render('attachment/file-upload.phtml');
        exit;
    }

    public function deleteAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('attachmentFileDelete'));

        // load model
        $modelBookingAttachment = new Model_BookingAttachment();
        $modelBooking = new Model_Booking();
        $modelInquiry = new Model_Inquiry();


        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $ids = $this->request->getParam('ids', array());


        $dir = get_config('attachment') . '/';
        if ($id) {
            $ids[] = $id;
        }
        foreach ($ids as $id) {
            $isValid = true;
            $bookingAttachment = $modelBookingAttachment->getById($id);

            if (!$bookingAttachment) {
                $isValid = false;
            }

            $bookingId = !empty($bookingAttachment['booking_id']) ? $bookingAttachment['booking_id'] : 0;
            $inquiryId = !empty($bookingAttachment['inquiry_id']) ? $bookingAttachment['inquiry_id'] : 0;

            if ($bookingId) {
                if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
                    $isValid = false;
                }
                if (!$modelBooking->checkIfCanSeeBooking($bookingId)) {
                    $isValid = false;
                }
            } elseif ($inquiryId) {
                if (!CheckAuth::checkIfCanHandelAllCompany('inquiry', $inquiryId)) {
                    $isValid = false;
                }
                if (!$modelInquiry->checkIfCanSeeInquiry($inquiryId)) {
                    $isValid = false;
                }
            } else {
                $isValid = false;
            }

            if ($isValid) {
                $bookingAttachment = $modelBookingAttachment->getById($id);
                // get source file
                $source = $dir . $bookingAttachment['path'];

                // delete source file
                unlink($source);

                $modelBookingAttachment->deleteById($id);
            }
        }
        $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
    }

    public function downloadAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('attachmentFileDownload'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);

        // load model
        $modelBookingAttachment = new Model_BookingAttachment();
        $modelBooking = new Model_Booking();
        $modelInquiry = new Model_Inquiry();

        $bookingAttachment = $modelBookingAttachment->getById($id);

        if (!$bookingAttachment) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Attachment not found"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $bookingId = !empty($bookingAttachment['booking_id']) ? $bookingAttachment['booking_id'] : 0;
        $inquiryId = !empty($bookingAttachment['inquiry_id']) ? $bookingAttachment['inquiry_id'] : 0;

        if ($bookingId) {
            if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this page"));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            }
            if (!$modelBooking->checkIfCanSeeBooking($bookingId)) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this booking"));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            }
        } elseif ($inquiryId) {
            if (!CheckAuth::checkIfCanHandelAllCompany('inquiry', $inquiryId)) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this page"));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            }
            if (!$modelInquiry->checkIfCanSeeInquiry($inquiryId)) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this inquiry"));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            }
        } else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Booking or inquiry not found"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $path = $bookingAttachment['path'];
        $dir = get_config('attachment') . '/';
        $filename = "{$dir}/{$path}";
        $file_extension = strtolower(substr(strrchr($filename, "."), 1));

        switch ($file_extension) {
            case "pdf": $ctype = "application/pdf";
                break;
            case "exe": $ctype = "application/octet-stream";
                break;
            case "zip": $ctype = "application/zip";
                break;
            case "doc": $ctype = "application/msword";
                break;
            case "xls": $ctype = "application/vnd.ms-excel";
                break;
            case "ppt": $ctype = "application/vnd.ms-powerpoint";
                break;
            case "gif": $ctype = "image/gif";
                break;
            case "png": $ctype = "image/png";
                break;
            case "jpe": case "jpeg":
            case "jpg": $ctype = "image/jpg";
                break;
            default: $ctype = "application/force-download";
        }

        if (!file_exists($filename)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => 'Attachment not found'));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private", false);
        header("Content-Type: $ctype");
        header("Content-Disposition: attachment; filename=\"" . basename($filename) . "\";");
        header("Content-Transfer-Encoding: binary");
        header("Content-Length: " . @filesize($filename));
        set_time_limit(0);
        readfile("$filename") or die("File not found.");
        exit;
    }

    public function editDescriptionAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('attachmentFileEditDescription'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $description = $this->request->getParam('description');

        // load model
        $modelBookingAttachment = new Model_BookingAttachment();
        $modelBooking = new Model_Booking();
        $modelInquiry = new Model_Inquiry();

        $bookingAttachment = $modelBookingAttachment->getById($id);

        if (!$bookingAttachment) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Attachment not found"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $bookingId = !empty($bookingAttachment['booking_id']) ? $bookingAttachment['booking_id'] : 0;
        $inquiryId = !empty($bookingAttachment['inquiry_id']) ? $bookingAttachment['inquiry_id'] : 0;

        if ($bookingId) {
            if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this page"));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            }
            if (!$modelBooking->checkIfCanSeeBooking($bookingId)) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this booking"));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            }
        } elseif ($inquiryId) {
            if (!CheckAuth::checkIfCanHandelAllCompany('inquiry', $inquiryId)) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this page"));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            }
            if (!$modelInquiry->checkIfCanSeeInquiry($inquiryId)) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this inquiry"));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            }
        } else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Booking or inquiry not found"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // init action form
        //
        $form = new Booking_Form_DescriptionFile(array('description' => $bookingAttachment['description'], 'id' => $id));

        //
        // handling the updating process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'description' => $description
                );

                $success = $modelBookingAttachment->updateById($id, $data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes in description File"));
                }

                echo 1;
                exit;
            }
        }

        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('attachment/edit-description.phtml');
        exit;
    }

    public function editWorkOrderAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('attachmentFileEditWorkOrder'));

        //
        // get request parameters
        //
        $id = $this->request->getParam('id', 0);
        $workOrder = $this->request->getParam('work_order');

        // load model
        $modelBookingAttachment = new Model_BookingAttachment();
        $modelBooking = new Model_Booking();
        $modelInquiry = new Model_Inquiry();

        $bookingAttachment = $modelBookingAttachment->getById($id);

        if (!$bookingAttachment) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Attachment not found"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $bookingId = !empty($bookingAttachment['booking_id']) ? $bookingAttachment['booking_id'] : 0;
        $inquiryId = !empty($bookingAttachment['inquiry_id']) ? $bookingAttachment['inquiry_id'] : 0;

        if ($bookingId) {
            if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this page"));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            }
            if (!$modelBooking->checkIfCanSeeBooking($bookingId)) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this booking"));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            }
        } elseif ($inquiryId) {
            if (!CheckAuth::checkIfCanHandelAllCompany('inquiry', $inquiryId)) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this page"));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            }
            if (!$modelInquiry->checkIfCanSeeInquiry($inquiryId)) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this inquiry"));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            }
        } else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Booking or inquiry not found"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }


        $workOrder = !empty($workOrder) ? 0 : 1;

        $data = array(
            'work_order' => $workOrder
        );

        $success = $modelBookingAttachment->updateById($id, $data);

        if ($success) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Changes saved"));
        } else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Changes in Work Order"));
        }

        $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
    }

    public function viewDescriptionAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('attachmentFileViewDescription'));

        //
        // get request parameters
        //
       
        $id = $this->request->getParam('id');


        // load model
        $modelBookingAttachment = new Model_BookingAttachment();
        $modelBooking = new Model_Booking();
        $modelInquiry = new Model_Inquiry();
        $bookingAttachment = $modelBookingAttachment->getById($id);

        $bookingId = !empty($bookingAttachment['booking_id']) ? $bookingAttachment['booking_id'] : 0;
        $inquiryId = !empty($bookingAttachment['inquiry_id']) ? $bookingAttachment['inquiry_id'] : 0;

        if ($bookingId) {
            if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this page"));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            }
            if (!$modelBooking->checkIfCanSeeBooking($bookingId)) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this booking"));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            }
        } elseif ($inquiryId) {
            if (!CheckAuth::checkIfCanHandelAllCompany('inquiry', $inquiryId)) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this page"));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            }
            if (!$modelInquiry->checkIfCanSeeInquiry($inquiryId)) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this inquiry"));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            }
        } else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Booking or inquiry not found"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $this->view->bookingAttachment = $bookingAttachment;
        //
        // render views
        //
        echo $this->view->render('attachment/view-description.phtml');
        exit;
    }

}

