<?php

class Booking_IndexController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'bookings';
        $this->view->sub_menu = 'booking';
//           BreadCrumbs::setLevel(2, 'Bookings');
    }

    /**
     * Items list action
     */
	 
    public function indexAction() {
     
//         BreadCrumbs::setLevel(3, 'Booking List' , ' ');
        
		/*$data = array(
                    'cronjob_name' => 'Send_Sms_Inquiry_Contact_Attempt',
                    'php_code' => str_replace("\r\n", "\n", '$Model_Inquiry= new Model_Inquiry();$Model_Inquiry->cronJobSendSmsInquiryContactAttempt();'),
                    'every' => '0 1,4,6,7 ***',
                    'running' => 0
                );
				
	$Model_CronJob = new Model_CronJob();	
    $Model_CronJob->insert($data);	*/
		
        //check Auth for logged user
        CheckAuth::checkPermission(array('booking'));
        $modelBooking = new Model_Booking();
		
		
        //get request parameters
        $orderBy = $this->request->getParam('sort', 'created');
        $sortingMethod = $this->request->getParam('method', 'desc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array('convert_status' => 'booking'));
        $isDeleted = $this->request->getParam('is_deleted', 0);
        $Date = $this->request->getParam('date');
        $is_first_time = $this->request->getParam('is_first_time');
        $page_number = $this->request->getParam('page_number');
        $this->view->isDeleted = $isDeleted;

        if (isset($page_number)) {
            $perPage = 15;
            $currentPage = $page_number + 1;
        }

        $filters['withoutEstimateStatus'] = true;

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        $booking_ids = $this->request->getParam('booking_ids');
        if ($booking_ids) {
            $filters['my_booking_ids'] = $booking_ids;
        }

        $loggedUser = CheckAuth::getLoggedUser();
        $my_bookings = $this->request->getParam('my_bookings', false);


        if ($my_bookings) {
            $this->view->sub_menu = 'my_bookings';
            $filters['my_bookings'] = $loggedUser['user_id'];
        }


        if ($this->request->isPost()) {
          
            if (isset($filters['rejectBookings']) && $filters['rejectBookings'] == 'reject') {

                $bookingIdArray = array();
                $modelContractorServiceBooking = new Model_ContractorServiceBooking();
                $rejectBookingIds = $modelContractorServiceBooking->getRejectBookingsIds();
                foreach ($rejectBookingIds as $rejectBookingId) {
                    $bookingIdArray[] = $rejectBookingId['booking_id'];
                }

                $filters['booking_ids'] = $bookingIdArray;
            }

            if (isset($filters['user_id']) && !empty($filters['user_id'])) {
                $filters['my_bookings'] = $filters['user_id'];
            }

            if (isset($filters['convert_status']) && $filters['convert_status'] == 'all') {
                unset($filters['convert_status']);
            }

            if ($isDeleted) {
                if (CheckAuth::checkCredential(array('canSeeDeletedBooking'))) {
                    //get all deleted booking
                    $filters['is_deleted'] = $isDeleted;
                    unset($filters['convert_status']);
                }
            }


            if (isset($filters['booking_start_between']) && $filters['booking_start_between']) {
               
                // $Model_Booking = new  Model_Booking();
                $filtersNotMultiple = array('booking_start_between' => $filters['booking_start_between'], 'booking_end_between' => $filters['booking_end_between'], 'withoutEstimateStatus' => 1);
                if (isset($filters['exclude_more_one_status']) && !empty($filters['exclude_more_one_status'])) {
                    $filtersNotMultiple['exclude_more_one_status'] = $filters['exclude_more_one_status'];
                }
				
				$filtersNotMultiple = array_merge($filters, $filtersNotMultiple);
				unset($filtersNotMultiple['multiple_start_between']);
				unset($filtersNotMultiple['multiple_end_between']);
                $AllNotMultipleBooking = $modelBooking->getAll($filtersNotMultiple, "{$orderBy} {$sortingMethod}", $pager, 0, $perPage, $currentPage);
                ///////
				$AllMultipleBooking = array();
				if(isset($filters['multiple_start_between'])|| isset($filters['multiple_end_between'])){
                $filtersMultiple = array('multiple_start_between' => $filters['multiple_start_between'], 'multiple_end_between' => $filters['multiple_end_between']);
                if (isset($filters['exclude_more_one_status']) && !empty($filters['exclude_more_one_status'])) {
                    $filtersMultiple['exclude_more_one_status'] = $filters['exclude_more_one_status'];
                }

                $AllMultipleBooking = $modelBooking->getAll($filtersMultiple, "{$orderBy} {$sortingMethod}", $pager, 0, $perPage, $currentPage);
				}
                $data = array_unique(array_merge($AllNotMultipleBooking, $AllMultipleBooking), SORT_REGULAR);
            } else if (isset($filters['notAssigned'])) {

                $modelBooking = new Model_Booking();
                $modelContractorServiceBooking = new Model_ContractorServiceBooking();
                $notAssignedBooking = $modelBooking->getAll($filters, "{$orderBy} {$sortingMethod}", $pager, 0, $perPage, $currentPage);
                $rejectBookingIds = $modelContractorServiceBooking->getRejectBookingsIds();

                foreach ($rejectBookingIds as $rejectBookingId) {
                    $bookingIdArray[] = $rejectBookingId['booking_id'];
                }
//                $filters['booking_ids'] = $bookingIdArray;
                $rejectedFilters = array(
                    'booking_not_finished_yet' => true,
                    'exclude_status' => 'CANCELLED',
                    'booking_ids' => $bookingIdArray,
                );
                $rejectedBooking = $modelBooking->getAll($rejectedFilters, "{$orderBy} {$sortingMethod}", $pager, 0, $perPage, $currentPage);
                $data = array_merge($notAssignedBooking, $rejectedBooking);
//                if (my_ip()) {
//                    echo "beeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeb";
//                    var_dump($rejectedBooking);
//                    exit;
//                }
            } else {
                $data = $modelBooking->getAll($filters, "{$orderBy} {$sortingMethod}", $pager, 0, $perPage, $currentPage);
            }

            // $data = $modelBooking->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);
            $types = array('contractors', 'customer', 'city', 'labels', 'address', 'status', 'services', 'services_temp', 'is_accepted', 'can_delete', 'not_accepted_or_rejected', 'not_accepted', 'reminder', 'booking_users', 'have_attachment', 'multiple_days');
            $result = array();
            $modelBooking->fills($data, $types);
            $this->view->data = $data;
            $this->view->is_first_time = $is_first_time;
            $this->view->isDeleted = isset($filters['is_deleted']) ? 1 : 0;

            $result['data'] = $this->view->render('index/draw-node.phtml');
            if ($data) {

                $result['is_last_request'] = 0;
            } else {

                $result['is_last_request'] = 1;
            }
            echo json_encode($result);
            exit;
        }
        // by doaa
        // to get all booking that belong to this user selected from user search


        if ($isDeleted) {
            if (CheckAuth::checkCredential(array('canSeeDeletedBooking'))) {
                $this->view->sub_menu = 'deleted_booking';

                //get all deleted booking
                $filters['is_deleted'] = $isDeleted;
                unset($filters['convert_status']);
            } else {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission"));
                $this->_redirect($this->router->assemble(array(), 'booking'));
            }
        }


        //Load Model
        $modelBookingStatus = new Model_BookingStatus();
        $modelBooking = new Model_Booking();


        //get all booking Status for drop dawn menu
        $allStatus = $modelBookingStatus->getAllStatusAsArray(array('withoutQouted' => true));


        $select = new Zend_Form_Element_Select('fltr');
        $select->setBelongsTo('status');
        $select->setDecorators(array('ViewHelper'));
        $select->setValue(isset($filters['status']) ? $filters['status'] : '');
        $select->setAttrib('onchange', "fltr('" . $this->router->assemble(array(), 'booking') . "?fltr[status]=','status-fltr')");
        $select->setAttrib('style', "width: 150px;");
        $select->addMultiOption('', 'Booking Status');
        $select->addMultiOption('current', 'CURRENT');
        $select->addMultiOptions($allStatus);
        $this->view->allStatus = $select;



        //init pager and articles model object
        /* $pager = new Model_Pager();
          $pager->perPage = get_config('perPage');
          $pager->currentPage = $currentPage;
          $pager->url = $_SERVER['REQUEST_URI']; */


        //get data list
        //$modelBooking->cronJobReminderCustomerBookingConfirmationOneDay();
        //set view params
        //$this->view->data = $data;
        //$this->view->currentPage = $currentPage;
        //$this->view->perPage = $pager->perPage;
        //$this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
    }

    public function editDescriptionAction() {

        $bookingId = $this->request->getParam('id', 0);
        $description = $this->request->getParam('description', 0);

        $data = array('description' => $description);

        $modelBooking = new Model_Booking();
        $success = $modelBooking->updateById($bookingId, $data);

        if ($success) {
            echo 1;
            exit;
        }
        exit;
    }

    public function removeFollowUpBookingAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('bookingEdit'));

        //
        // get params
        //
        $bookingId = $this->request->getParam('id');


        //
        // load model
        //
        $modelBooking = new Model_Booking();
        $booking = $modelBooking->getById($bookingId);
        if (!$booking) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Estimate not found"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this page"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $modelBooking->updateById($bookingId, array('is_to_follow' => 0));

        $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
    }

    public function followUpBookingAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('bookingEdit'));

        //
        // get params
        //
        $bookingId = $this->request->getParam('id');
        $followDate = $this->request->getParam('followDate');

        //
        // load model
        //
        
        $modelBooking = new Model_Booking();
        $booking = $modelBooking->getById($bookingId);

        if (!$booking) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Estimate not found"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this page"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // init action form
        //
        $form = new Booking_Form_FollowUPBooking(array('booking' => $booking));

        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'is_to_follow' => 1,
                    'to_follow' => mySql2PhpTime($followDate),
                );

                $success = $modelBooking->updateById($bookingId, $data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No changes"));
                }

                echo 1;
                exit;
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('index/follow-up-booking.phtml');
        exit;
    }

    public function deleteAction() {

        $modelBooking = new Model_Booking();
        $modelGoogleCalendarEvent = new Model_GoogleCalendarEvent();
        $modelLogUser = new Model_LogUser();

        //get request parameters
        $bookingId = $this->request->getParam('id', 0);
        $bookingIds = $this->request->getParam('ids', array());
        if ($bookingId) {
            $bookingIds[] = $bookingId;
        }

        $success_array = array();
        $tables = array();

        foreach ($bookingIds as $bookingId) {
            $success_array[$bookingId] = 0;
            if (CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
                if ($modelBooking->checkBeforeDeleteBooking($bookingId, $tables)) {
                    if ($modelBooking->checkIfCanDeleteBooking($bookingId)) {
                        //add User Log 
                        $modelLogUser->addUserLogEvent($bookingId, 'booking', 'deleted');
                        $updated = $modelBooking->updateById($bookingId, array('is_deleted' => 1));
                        if ($updated) {
                            MobileNotification::notify($bookingId, 'booking deleted');
                        }

                        // delete google Calendar event
                        $modelGoogleCalendarEvent->deleteGoogleCalendarEventByBookingId($bookingId);
                        $success_array[$bookingId] = 1;
                    }
                }
            }
        }

        if (!in_array(0, $success_array)) {
            if (count($success_array) > 1) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Selected bookings deleted"));
            } else {
                $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Booking deleted"));
            }
        } else {
            if (count($success_array) > 1) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Error: Failed to delete, one or more bookings are in use"));
            } else {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Error: Failed to delete, this booking is used in (" . implode(',', $tables) . ')'));
            }
        }
        $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
    }

    public function deleteForeverAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('bookingDeleteForever'));

        //
        // get request parameters
        //
        $bookingId = $this->request->getParam('id', 0);

        $modelBooking = new Model_Booking();
        $modelBookingEstimate = new Model_BookingEstimate();
        $modelBookingInvoice = new Model_BookingInvoice();

        $tables = array();
        if (!$modelBooking->checkBeforeDeleteBooking($bookingId, $tables)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Error: Failed to delete, this booking is used in (" . implode(',', $tables) . ')'));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //check if can delete booking
        if (CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
            if ($modelBooking->checkIfCanDeleteBooking($bookingId)) {
                MobileNotification::notify($bookingId, 'booking deleted forever');

                //getting the booking and checking its convert status
                $booking = $modelBooking->getById($bookingId);
                if ($booking['convert_status'] == 'estimate') {

                    //check if the estimate is deleted
                    $estimate = $modelBookingEstimate->getByBookingId($bookingId);
                    if ($estimate['is_deleted']) {

                        //delete the estimate forever
                        $modelBookingEstimate->deleteForeverByBookingId($bookingId);
                    }
                } elseif ($booking['convert_status'] == 'invoice') {

                    //check if the invoice is deleted
                    $invoice = $modelBookingInvoice->getByBookingId($bookingId);
                    if ($invoice['is_deleted']) {

                        //delete the invoice forever
                        $modelBookingInvoice->deleteForeverByBookingId($bookingId);
                    }
                }
                //delete the booking forever
                $success = $modelBooking->deleteById($bookingId);
                //$modelBooking->deleteRelatedBooking($bookingId);
            }
        }

        if ($success) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Booking deleted"));
        } else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Error: Failed to delete booking"));
        }

        $this->_redirect($this->router->assemble(array('is_deleted' => 'deleted'), 'isDeletedBooking'));
    }

    //////////// By Islam Change visited value of the booking
    public function notVisitedBookingAction($Id, $estimate_id) {
        $Id = $this->request->getParam('id', 0);
        //$estimate_id = $this->request->getParam('estimate_id', 0);
        //echo 'estimate_id'.$estimate_id;
        $modelBooking = new Model_Booking();
        $success = $modelBooking->notVisitedBooking($Id);


        if ($success) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Converted to Not Quoted Onsite"));
            $this->_redirect($this->router->assemble(array(), 'estimateViewAll'));
        } else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Error: Failed to convert"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
    }

    public function visitedBookingAction($Id, $estimate_id) {
        $Id = $this->request->getParam('id', 0);
        //$estimate_id = $this->request->getParam('estimate_id', 0);
        //echo 'estimate_id'.$estimate_id;
        $modelBooking = new Model_Booking();
        $success = $modelBooking->visitedBooking($Id);
        if ($success) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Converted to Quoted Onsite"));
            $this->_redirect($this->router->assemble(array(), 'estimateViewAll'));
        } else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Error: Failed to convert"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
    }

////////////////////////

    public function undeleteAction() {
        //check Auth for logged user
        CheckAuth::checkPermission(array('bookingUndelete'));

        //get request parameters
        $bookingId = $this->request->getParam('id', 0);
        $modelBooking = new Model_Booking();
        $modelLogUser = new Model_LogUser();

        $success = false;
        if (CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
            if ($modelBooking->checkIfCanDeleteBooking($bookingId)) {
                //add User Log
                $modelLogUser->addUserLogEvent($bookingId, 'booking', 'added');
                $success = $modelBooking->updateById($bookingId, array('is_deleted' => 0));
                $Booking = $modelBooking->getById($bookingId);
                if ($success) {
                    $modelBookingStatus = new Model_BookingStatus();
                    $pushGoogleCalender = $modelBookingStatus->getPushGoogleCalender();
                    if (in_array($Booking['status_id'], $pushGoogleCalender)) {
                        $modelGoogleCalendarEvent = new Model_GoogleCalendarEvent();
                        $modelGoogleCalendarEvent->sendCreatedBooking($bookingId);
                    }
                }
            }
        }
        if ($success) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Booking restored"));
        } else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "The selected booking can't be restored"));
        }
        $this->_redirect($this->router->assemble(array(), 'booking'));
    }

    public function editAction() {


        //check Auth for logged user
        CheckAuth::checkPermission(array('bookingEdit'));

        //get request parameters
        $bookingId = $this->request->getParam('id');

        $modelBooking = new Model_Booking();
	
        $booking = $modelBooking->getById($bookingId);

		 if (!$modelBooking->checkIfCanEditBooking($bookingId)) {

            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permession to edit this booking. if you still want to edit this booking please  "  . '<a class="btn btn-warning btn-xs" href="#" onclick="requestAccessToEdit();">Request access</a>'));
            $this->_redirect($this->router->assemble(array('id' => $bookingId), 'bookingView'));
        }
		
        /*if (!$modelBooking->checkIfCanEditBooking($bookingId)) {

            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to edit this booking"));
            $this->_redirect($this->router->assemble(array(), 'booking'));
        }*/
        if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this page"));
            $this->_redirect($this->router->assemble(array(), 'booking'));
        }
/*
        $booking = $modelBooking->getById($bookingId);
        $isEditable = $booking['is_editable'];*/
    /*    $isAccepted = $modelBooking->checkBookingIfAccepted($bookingId);
        $this->view->isAccepted = $isAccepted;*/
      /*  if(!$isAccepted && $isEditable == 0) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permession to edit this booking. if you still want to edit this booking please  "  . '<a class="btn btn-warning btn-xs" href="#" onclick="requestAccessToEdit();">Request access</a>'));
            $this->_redirect($this->router->assemble(array('id' => $bookingId), 'bookingView'));
        }
		*/
        $this->_redirect('booking-edit?booking_id=' . $bookingId);
        exit;
    }
/*******Ediable Request*****IBM*/
    public function requestAccessAction(){
        // $this->_helper->layout->disableLayout();
        // $this->_helper->viewRenderer->setNoRender(TRUE);
        $bookingId = $this->request->getParam('id'); 
        MobileNotification::notify($bookingId, "request access");
        echo json_encode(array('request' => '1'));
        exit;

    }
    public function acceptRequestAction(){
        $bookingId = $this->request->getParam('id'); 
        $contractor_name = $this->request->getParam('name'); 
        $modelBooking = new Model_Booking();
        $booking = $modelBooking->getById($bookingId);

        $data = array('is_editable' => 1);
        $modelBooking->updateById($bookingId , $data);
        MobileNotification::notify($bookingId, "accept request");
        $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "You have accepted " . $contractor_name . "'s" . " " . "request to edit Booking " . $booking['booking_num']));
            $this->_redirect($this->router->assemble(array('id' => $bookingId), 'bookingView'));        
        exit;


    }
    public function rejectRequestAction(){
        $bookingId = $this->request->getParam('id'); 
        $contractor_name = $this->request->getParam('name'); 
        $modelBooking = new Model_Booking();
        $booking = $modelBooking->getById($bookingId);

        MobileNotification::notify($bookingId, "reject request");
            $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "You have rejected " . $contractor_name . "'" . "s request to edit Booking " . $booking['booking_num']));
            $this->_redirect($this->router->assemble(array('id' => $bookingId), 'bookingView'));
        exit;
     

    }
    public function toApproveEditAction(){
        $bookingId = $this->request->getParam('id');
        $modelBooking = new Model_Booking();
        $data = array('is_editable' => 0);
        $modelBooking->updateById($bookingId , $data);
        MobileNotification::notify($bookingId, "approve edit");
        $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "The changes you have approved saved successfully"));
        $this->_redirect($this->router->assemble(array('id' => $bookingId), 'bookingView'));
        exit;
    }

    ////////////By Islam coppy current estimate
    public function copyAction() {


        //check Auth for logged user
        CheckAuth::checkPermission(array('bookingEdit'));

        //get request parameters
        $bookingId = $this->request->getParam('id');

        $modelBooking = new Model_Booking();
        if (!$modelBooking->checkIfCanEditBooking($bookingId)) {

            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to edit this booking"));
            $this->_redirect($this->router->assemble(array(), 'booking'));
        }
        if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this page"));
            $this->_redirect($this->router->assemble(array(), 'booking'));
        }

        $this->_redirect('booking-copy?booking_id=' . $bookingId);
        exit;
    }

//////////////////////////
    /* public function viewAction() {


      //check Auth for logged user
      CheckAuth::checkPermission(array('bookingView'));

      $bookingId = $this->request->getParam('id', 0);
      $this->view->bookingId = $bookingId;

      if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
      $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this page"));
      $this->_redirect($this->router->assemble(array(), 'booking'));
      }

      //getting the users service-booking
      $modelBooking = new Model_Booking();


      //$modelBooking->addContractorsLog($bookingId);

      $booking = $modelBooking->getById($bookingId);


      $canEditDetails = $modelBooking->checkCanEditBookingDetails($bookingId);
      $this->view->canEditDetails = $canEditDetails;

      /////Islam
      $modelBooking->fill($booking, array('contractors', 'customer', 'city', 'labels', 'address', 'status', 'services', 'services_temp', 'is_accepted', 'can_delete', 'not_accepted_or_rejected', 'not_accepted', 'reminder', 'booking_users', 'have_attachment'));
      ///////////
      $this->view->booking = $booking;

      $isAccepted = $modelBooking->checkBookingIfAccepted($bookingId);
      $this->view->isAccepted = $isAccepted;

      $canDelete = $modelBooking->checkIfCanDeleteBooking($bookingId);
      $this->view->canDelete = $canDelete;

      $modelBookingInvoice = new Model_BookingInvoice();
      $this->view->bookingInvoice = $modelBookingInvoice->getByBookingId($bookingId);

      $modelBookingEstimate = new Model_BookingEstimate();
      $this->view->bookingEstimate = $modelBookingEstimate->getByBookingId($bookingId);


      $modelBookingMultipleDays = new Model_BookingMultipleDays();
      $multipleDays = $modelBookingMultipleDays->getByBookingId($bookingId);
      $this->view->primaryDateExtraInfo = $modelBooking->getExtraInfoBookingPrimaryDatesByBooking($bookingId);
      $this->view->multipleDaysWithVisitedExtraInfo = $modelBookingMultipleDays->getMultipleDaysWithVisitedByBookingId($bookingId);
      $this->view->multipleDays = $multipleDays;


      $modelBookingLabel = new Model_BookingLabel();
      $modelLabel = new Model_Label();
      $bookingLabels = $modelBookingLabel->getByBookingId($bookingId);
      if ($bookingLabels) {
      foreach ($bookingLabels as &$bookingLabel) {
      $bookingLabel['label'] = $modelLabel->getById($bookingLabel['label_id']);
      }
      }
      $this->view->bookingLabels = $bookingLabels;

      $modelCustomer = new Model_Customer();
      $customer = $modelCustomer->getById($booking['customer_id']);

      if ($customer) {

      // customer type work order is_required Message
      $modelCustomerType = new Model_CustomerType();

      $customerType = $modelCustomerType->getById($customer['customer_type_id']);
      $isWorkOrder = false;
      $workOrder = $modelCustomerType->getCustomerTypeIsWorkOrder();
      if (in_array($customer['customer_type_id'], $workOrder)) {
      $modelBookingAttachment = new Model_BookingAttachment();
      $bookingAttachments = $modelBookingAttachment->getByBookingIdOrInquiryId($bookingId, $booking['original_inquiry_id']);
      $isWorkOrder = true;
      if (!empty($bookingAttachments)) {
      foreach ($bookingAttachments as $attachment) {
      if ($attachment['work_order'] == 1) {
      $isWorkOrder = false;
      }
      }
      }
      }
      $this->view->isWorkOrder = $isWorkOrder;
      $this->view->customerType = $customerType;
      }



      //address

      $modelBookingAddress = new Model_BookingAddress();
      $bookingAddress = $modelBookingAddress->getByBookingId($bookingId);
      $this->view->lineAddress = get_line_address($bookingAddress);

      /**
     *  get service from booking service
     * if the contractor change in the service untill approved , it get from booking service temp
      /
      $modelContractorServiceBooking = new Model_ContractorServiceBooking();
      $contractorServiceBookings = $modelContractorServiceBooking->getByBookingId($bookingId);





      $modelContractorServiceBookingTemp = new Model_ContractorServiceBookingTemp();
      $contractorServiceBookingsTemp = $modelContractorServiceBookingTemp->getByBookingId($bookingId);

      $thisBookingServices = array();
      $priceArray = array();
      $isTemp = false;
      if ($contractorServiceBookingsTemp && !$modelBooking->checkCanEditBookingDetails($bookingId)) {
      $this->view->bookingServices = $contractorServiceBookingsTemp;
      $isTemp = true;
      foreach ($contractorServiceBookingsTemp as $contractorServiceBookingTemp) {

      $serviceId = $contractorServiceBookingTemp['service_id'];
      $clone = $contractorServiceBookingTemp['clone'];
      $bookingId = $contractorServiceBookingTemp['booking_id'];

      $service_and_clone = $serviceId . ($clone ? '_' . $clone : '');

      $thisBookingServices[] = $service_and_clone;

      $priceArray[$service_and_clone] = $modelContractorServiceBookingTemp->getTotalServiceBookingQoute($bookingId, $serviceId, $clone);
      }
      } elseif ($contractorServiceBookings) {

      $this->view->bookingServices = $contractorServiceBookings;
      foreach ($contractorServiceBookings as $contractorServiceBooking) {

      $serviceId = $contractorServiceBooking['service_id'];
      $clone = $contractorServiceBooking['clone'];
      $bookingId = $contractorServiceBooking['booking_id'];

      $service_and_clone = $serviceId . ($clone ? '_' . $clone : '');

      $thisBookingServices[] = $service_and_clone;

      $priceArray[$service_and_clone] = $modelContractorServiceBooking->getTotalServiceBookingQoute($bookingId, $serviceId, $clone);
      }
      }

      $this->view->thisBookingServices = $thisBookingServices;
      $this->view->priceArray = $priceArray;
      $this->view->isTemp = $isTemp;

      //Contractor Distance
      $this->view->contractorsDistance = $this->getContractorsDistance($bookingId);

      ////get all dates Extra Info
      $modelVisitedExtraInfo = new Model_VisitedExtraInfo();
      $extraInfo = $modelVisitedExtraInfo->getByBookingId2($bookingId);
      $this->view->extraInfo = $extraInfo;


      ////get all questions
      $modelUpdateBookingQuestionAnswer = new Model_UpdateBookingQuestionAnswer();
      $booking = $modelBooking->getById($bookingId);
      $status_id = $booking['status_id'];
      $questions = $modelUpdateBookingQuestionAnswer->getByBookingIdAndStatusId($status_id, $bookingId);
      $this->view->questions = $questions;

      /////get Contractors Distances
      $removeRedundancy = array();
      foreach ($contractorServiceBookings as &$bookingService) {
      ///set service ids in array to get distance between them and contractors
      $removeRedundancy[$bookingService['service_id']] = $bookingService['service_id'];
      }

      $modelContractorService = new Model_ContractorService();
      $modelUser = new Model_User();
      $cityId = $booking['city_id'];

      $service_ids = $removeRedundancy;

      /// get get contractor ids by city_id and service_id
      if (!empty($service_ids)) {
      foreach ($service_ids as $service_id) {
      if ($cityId) {
      $ContractorServices = $modelContractorService->getContractorByServiceIdAndCityId($service_id, $cityId);
      } else {
      $ContractorServices = $modelContractorService->getContractorByServiceId($service_id);
      }
      foreach ($ContractorServices as $ContractorService) {
      $contractor_ids[$ContractorService['contractor_id']] = $ContractorService['contractor_id'];
      }
      }
      }

      /// here calculate the distance between each contractor and inquiry address
      $contractorDistances = array();
      if (isset($contractor_ids)) {
      foreach ($contractor_ids as $contractorId) {
      //// delete inquiries from contractor list
      if ($contractorId != 1) {
      $contractor = $modelUser->getById($contractorId);

      $contractorDistances[$contractor['user_id']] = array(
      'contractor_id' => $contractor['user_id'],
      'name' => $contractor['username'],
      'email1' => $contractor['email1'],
      'distance' => $modelBookingAddress->getDistanceByTwoAddress($contractor, $bookingAddress)
      );
      }
      }
      }

      //By Mona
      $orderBy = 'iu.created';
      $sortingMethod = 'desc';
      $type = 'booking';


      $modelImageAttachment = new Model_Image();
      $pager = null;
      $this->view->photo = $modelImageAttachment->getAll($bookingId, $type, "{$orderBy} {$sortingMethod}", $pager, $filter = array(), $limit = 10);
      $this->view->type = $type;
      $this->view->photoCount = count($modelImageAttachment->getAll($bookingId, $type, "{$orderBy} {$sortingMethod}"));
      //End By Mona
      $this->view->contractorDistances = $contractorDistances;
      //Booking Address Map  By Salim 21/6/2015
      if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
      $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this page"));
      $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
      }
      if (!$booking) {
      $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Booking not found"));
      $this->_redirect($this->router->assemble(array(), 'Login'));
      }
      /* if (!$modelBooking->checkIfCanSeeLocation($bookingId)) {
      $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this booking location"));
      $this->_redirect($this->router->assemble(array(), 'Login'));
      } /

      if ($bookingAddress) {

      $line_address = get_line_address($bookingAddress);
      $is_address = false;

      if (empty($line_address)) {
      $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "There is no address for this booking"));
      $this->_redirect($this->router->assemble(array(), 'Login'));
      } else {
      $MAP_OBJECT_Booking = new GoogleMapAPI();

      $MAP_OBJECT_Booking->setHeight(200);
      $MAP_OBJECT_Booking->setWidth('100%');
      $MAP_OBJECT_Booking->setMapType('map');

      //$MAP_OBJECT->addDirections($address, $link, 'map_directions', true);
      $MAP_OBJECT_Booking->addMarkerByAddress($line_address);

      $this->view->MAP_OBJECT_Booking = $MAP_OBJECT_Booking;
      $is_address = true;
      }

      $this->view->is_address = $is_address;
      //$this->view->booking = $booking;
      $this->view->line_address = $line_address;
      }
      /*else {
      $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "There is no address for this booking"));
      $this->_redirect($this->router->assemble(array(), 'Login'));
      }/
      // End Booking Address Map
      // by doaa get regected booking questions answer

      $modelRejectBookingQuestionAnswers = new Model_RejectBookingQuestionAnswers();
      $rejectBookingQuestionAnswers = $modelRejectBookingQuestionAnswers->getByBookingId($bookingId);
      $this->view->rejectBookingQuestions = $rejectBookingQuestionAnswers;
      } */
    public function viewAction() {
//        BreadCrumbs::setLevel(2, 'Bookings');
//         BreadCrumbs::setLevel(3, 'Booking Details' , ' ');
        
        $modelBooking = new Model_Booking();
        //check Auth for logged user
//        BreadCrumbs::setLevel(2, 'Booking');
//         BreadCrumbs::setLevel(3, 'Booking Details' , ' ');
         
         
		

        CheckAuth::checkPermission(array('bookingView'));

        $bookingId = $this->request->getParam('id', 0);
        $this->view->bookingId = $bookingId;

        //check in can see his or assigned bookings
        if (!$modelBooking->checkIfCanSeeBooking($bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this booking"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this page"));
            $this->_redirect($this->router->assemble(array(), 'booking'));
        }

        //D.A 26/08/2015 Caching booking view blocks
        require_once 'Zend/Cache.php';
        $company_id = CheckAuth::getCompanySession();
        $bookingViewDir = get_config('cache') . '/' . 'bookingsView' . '/' . $company_id;
        if (!is_dir($bookingViewDir)) {
            mkdir($bookingViewDir, 0777, true);
        }
        $frontEndOption = array('lifetime' => NULL,
            'automatic_serialization' => true);
        $backendOptions = array('cache_dir' => $bookingViewDir);
        $cache = Zend_Cache::factory('Core', 'File', $frontEndOption, $backendOptions);

        //D.A 26/08/2015
        $cacheID = $bookingId . '_rejectBookingQuestions';
        if (($result = $cache->load($cacheID)) === false) {
            $modelRejectBookingQuestionAnswers = new Model_RejectBookingQuestionAnswers();
            $rejectBookingQuestionAnswers = $modelRejectBookingQuestionAnswers->getByBookingId($bookingId);
            $result = $rejectBookingQuestionAnswers;
            if ($result) {
                $cache->save($result, $cacheID);
            }
        } else {
            $rejectBookingQuestionAnswers = $result;
        }
        $this->view->rejectBookingQuestions = $rejectBookingQuestionAnswers;

        //getting the users service-booking

        $modelBookingAddress = new Model_BookingAddress();
        $modelBookingMultipleDays = new Model_BookingMultipleDays();

        $canEditDetails = $modelBooking->checkCanEditBookingDetails($bookingId);
        $this->view->canEditDetails = $canEditDetails;



        //D.A 26/08/2015
        $cacheID = $bookingId . '_bookingDetails';
		
		
		 
        if (($result = $cache->load($cacheID)) === false) {
            $booking = $modelBooking->getById($bookingId);
		
            //Islam
            $modelBooking->fill($booking, array('contractors', 'customer', 'city', 'labels', 'address', 'status', 'services', 'services_temp', 'is_accepted', 'can_delete', 'not_accepted_or_rejected', 'not_accepted', 'reminder', 'booking_users', 'have_attachment'));
            $modelBookingInvoice = new Model_BookingInvoice();
            $bookingInvoice = $modelBookingInvoice->getByBookingId($bookingId);
            $modelBookingEstimate = new Model_BookingEstimate();
            $bookingEstimate = $modelBookingEstimate->getByBookingId($bookingId);
            $multipleDays = $modelBookingMultipleDays->getByBookingId($bookingId);
            $bookingAddress = $modelBookingAddress->getByBookingId($bookingId);

            $result = array(
                'booking' => $booking,
                'bookingInvoice' => $bookingInvoice,
                'bookingEstimate' => $bookingEstimate,
                'multipleDays' => $multipleDays,
                'bookingAddress' => $bookingAddress
            );
            if ($result) {
                $cache->save($result, $cacheID);
            }
        } else {
            $booking = $result['booking'];
            $bookingInvoice = $result['bookingInvoice'];
            $bookingEstimate = $result['bookingEstimate'];
            $multipleDays = $result['multipleDays'];
            $bookingAddress = $result['bookingAddress'];
        }

        $this->view->booking = $booking;
        $this->view->bookingInvoice = $bookingInvoice;
        $this->view->bookingEstimate = $bookingEstimate;
        $this->view->multipleDays = $multipleDays;
        $this->view->lineAddress = get_line_address($bookingAddress);


        //Booking Address Map  By Salim 21/6/2015
        if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this page"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        if (!$booking) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Booking not found"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }

        if ($bookingAddress) {
            $line_address = get_line_address($bookingAddress);
            $is_address = false;

            if (empty($line_address)) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "There is no address for this booking"));
                $this->_redirect($this->router->assemble(array(), 'Login'));
            } else {
                $MAP_OBJECT_Booking = new GoogleMapAPI();

                $MAP_OBJECT_Booking->setHeight(200);
                $MAP_OBJECT_Booking->setWidth('100%');
                $MAP_OBJECT_Booking->setMapType('map');

                $MAP_OBJECT_Booking->addMarkerByAddress($line_address);

                $this->view->MAP_OBJECT_Booking = $MAP_OBJECT_Booking;
                $is_address = true;
            }

            $this->view->is_address = $is_address;
            $this->view->line_address = $line_address;
        }

        $isAccepted = $modelBooking->checkBookingIfAccepted($bookingId);
        $this->view->isAccepted = $isAccepted;

        $canDelete = $modelBooking->checkIfCanDeleteBooking($bookingId);
        $this->view->canDelete = $canDelete;

        //D.A 26/08/2015		
        $cacheID = $bookingId . '_bookingScheduledVisits';
        if (($result = $cache->load($cacheID)) === false) {
            $primaryDateExtraInfo = $modelBooking->getExtraInfoBookingPrimaryDatesByBooking($bookingId);
            $multipleDaysWithVisitedExtraInfo = $modelBookingMultipleDays->getMultipleDaysWithVisitedByBookingId($bookingId);
            $result = array(
                'primaryDateExtraInfo' => $primaryDateExtraInfo,
                'multipleDaysWithVisitedExtraInfo' => $multipleDaysWithVisitedExtraInfo
            );
            if ($result) {
                $cache->save($result, $cacheID);
            }
        } else {
            $primaryDateExtraInfo = $result['primaryDateExtraInfo'];
            $multipleDaysWithVisitedExtraInfo = $result['multipleDaysWithVisitedExtraInfo'];
        }

        $this->view->primaryDateExtraInfo = $primaryDateExtraInfo;
        $this->view->multipleDaysWithVisitedExtraInfo = $multipleDaysWithVisitedExtraInfo;

        //Comment by salim		
        //$this->view->primaryDateExtraInfo = $modelBooking->getExtraInfoBookingPrimaryDatesByBooking($bookingId);
        //$this->view->multipleDaysWithVisitedExtraInfo = $modelBookingMultipleDays->getMultipleDaysWithVisitedByBookingId($bookingId);
        //D.A 26/08/2015
        $cacheID = $bookingId . '_bookingLabels';
        if (($result = $cache->load($cacheID)) === false) {
            $modelBookingLabel = new Model_BookingLabel();
            $modelLabel = new Model_Label();
            $bookingLabels = $modelBookingLabel->getByBookingId($bookingId);
            if ($bookingLabels) {
                foreach ($bookingLabels as &$bookingLabel) {
                    $bookingLabel['label'] = $modelLabel->getById($bookingLabel['label_id']);
                }
            }
            $result = $bookingLabels;
            if ($result) {
                $cache->save($result, $cacheID);
            }
        } else {
            $bookingLabels = $result;
        }
        $this->view->bookingLabels = $bookingLabels;

        $modelCustomer = new Model_Customer();
        $customer = $modelCustomer->getById($booking['customer_id']);

        if ($customer) {

            // customer type work order is_required Message
            $modelCustomerType = new Model_CustomerType();

            $customerType = $modelCustomerType->getById($customer['customer_type_id']);
            $isWorkOrder = false;
            $workOrder = $modelCustomerType->getCustomerTypeIsWorkOrder();
            if (in_array($customer['customer_type_id'], $workOrder)) {
                $modelBookingAttachment = new Model_BookingAttachment();
                $bookingAttachments = $modelBookingAttachment->getByBookingIdOrInquiryId($bookingId, $booking['original_inquiry_id']);
                $isWorkOrder = true;
                if (!empty($bookingAttachments)) {
                    foreach ($bookingAttachments as $attachment) {
                        if ($attachment['work_order'] == 1) {
                            $isWorkOrder = false;
                        }
                    }
                }
            }
            $this->view->isWorkOrder = $isWorkOrder;
            $this->view->customerType = $customerType;
        }

        /**
         *  get service from booking service
         * if the contractor change in the service untill approved , it get from booking service temp
         */
        //D.A 26/08/2015
        $cacheID = $bookingId . '_bookingServices';
        if (($result = $cache->load($cacheID)) === false) {
            $modelContractorServiceBooking = new Model_ContractorServiceBooking();
            $contractorServiceBookings = $modelContractorServiceBooking->getByBookingId($bookingId);

            $modelContractorServiceBookingTemp = new Model_ContractorServiceBookingTemp();
            $contractorServiceBookingsTemp = $modelContractorServiceBookingTemp->getByBookingId($bookingId);

            $thisBookingServices = array();
            $priceArray = array();
            $isTemp = false;
            if ($contractorServiceBookingsTemp && !$modelBooking->checkCanEditBookingDetails($bookingId)) {
                $bookingServices = $contractorServiceBookingsTemp;
                $isTemp = true;
                foreach ($contractorServiceBookingsTemp as $contractorServiceBookingTemp) {
                    $serviceId = $contractorServiceBookingTemp['service_id'];
                    $clone = $contractorServiceBookingTemp['clone'];
                    $bookingId = $contractorServiceBookingTemp['booking_id'];
                    $service_and_clone = $serviceId . ($clone ? '_' . $clone : '');
                    $thisBookingServices[] = $service_and_clone;
                    $priceArray[$service_and_clone] = $modelContractorServiceBookingTemp->getTotalServiceBookingQoute($bookingId, $serviceId, $clone);
                }
            } elseif ($contractorServiceBookings) {
                $bookingServices = $contractorServiceBookings;
                foreach ($contractorServiceBookings as $contractorServiceBooking) {
                    $serviceId = $contractorServiceBooking['service_id'];
                    $clone = $contractorServiceBooking['clone'];
                    $bookingId = $contractorServiceBooking['booking_id'];
                    $service_and_clone = $serviceId . ($clone ? '_' . $clone : '');
                    $thisBookingServices[] = $service_and_clone;
                    $priceArray[$service_and_clone] = $modelContractorServiceBooking->getTotalServiceBookingQoute($bookingId, $serviceId, $clone);
                }
            }

            $contractorsDistance = $this->getContractorsDistance($bookingId);
            $result = array(
                'bookingServices' => $bookingServices,
                'thisBookingServices' => $thisBookingServices,
                'priceArray' => $priceArray,
                'isTemp' => $isTemp,
                'contractorsDistance' => $contractorsDistance
            );
            if ($result) {
                $cache->save($result, $cacheID);
            }
        } else {
            $bookingServices = $result['bookingServices'];
            $thisBookingServices = $result['thisBookingServices'];
            $priceArray = $result['priceArray'];
            $isTemp = $result['isTemp'];
            $contractorsDistance = $result['contractorsDistance'];
        }

        $this->view->bookingServices = $bookingServices;
        $this->view->thisBookingServices = $thisBookingServices;
        $this->view->priceArray = $priceArray;
        $this->view->isTemp = $isTemp;
        //Contractor Distance
        $this->view->contractorsDistance = $contractorsDistance;


        /* D.A 26/08/2015
          $cacheID= $bookingId.'_technicianUpdateDetails';
          if (($result = $cache->load($cacheID)) === false ) {
          $modelVisitedExtraInfo = new Model_VisitedExtraInfo();
          $extraInfo = $modelVisitedExtraInfo->getByBookingId2($bookingId);
          $result=$extraInfo;
          if($result){
          $cache->save($result,$cacheID);
          }
          }
          else{
          $extraInfo=$result;
          }
          $this->view->extraInfo = $extraInfo;
         */

        ////get all questions
        //D.A 26/08/2015
        $cacheID = $bookingId . '_updateBookingQuestions';
        if (($result = $cache->load($cacheID)) === false) {
            $modelUpdateBookingQuestionAnswer = new Model_UpdateBookingQuestionAnswer();
            $booking = $modelBooking->getById($bookingId);
            $status_id = $booking['status_id'];
            $questions = $modelUpdateBookingQuestionAnswer->getByBookingIdAndStatusId($status_id, $bookingId);
            $result = $questions;
            if ($result) {
                $cache->save($result, $cacheID);
            }
        } else {
            $questions = $result;
        }
        $this->view->questions = $questions;


        //get Contractors Distances	
        //D.A 26/08/2015
        $cacheID = $bookingId . '_bookingAvailableTechnicians';
        if (($result = $cache->load($cacheID)) === false) {
            $removeRedundancy = array();
            $modelContractorServiceBooking = new Model_ContractorServiceBooking();
            $contractorServiceBookings = $modelContractorServiceBooking->getByBookingId($bookingId);
            foreach ($contractorServiceBookings as &$bookingService) {
                ///set service ids in array to get distance between them and contractors
                $removeRedundancy[$bookingService['service_id']] = $bookingService['service_id'];
            }

            $modelContractorService = new Model_ContractorService();
            $modelUser = new Model_User();
            $cityId = $booking['city_id'];

            $service_ids = $removeRedundancy;

            /// get get contractor ids by city_id and service_id
            if (!empty($service_ids)) {
                foreach ($service_ids as $service_id) {
                    if ($cityId) {
                        $ContractorServices = $modelContractorService->getContractorByServiceIdAndCityId($service_id, $cityId);
                    } else {
                        $ContractorServices = $modelContractorService->getContractorByServiceId($service_id);
                    }
                    foreach ($ContractorServices as $ContractorService) {
                        $contractor_ids[$ContractorService['contractor_id']] = $ContractorService['contractor_id'];
                    }
                }
            }

            /// here calculate the distance between each contractor and inquiry address
            $contractorDistances = array();
            if (isset($contractor_ids)) {
                foreach ($contractor_ids as $contractorId) {
                    //// delete inquiries from contractor list
                    if ($contractorId != 1) {
                        $contractor = $modelUser->getById($contractorId);

                        $contractorDistances[$contractor['user_id']] = array(
                            'contractor_id' => $contractor['user_id'],
                            'name' => $contractor['username'],
                            'email1' => $contractor['email1'],
                            'distance' => $modelBookingAddress->getDistanceByTwoAddress($contractor, $bookingAddress)
                        );
                    }
                }
            }

            $result = $contractorDistances;
            if ($result) {
                $cache->save($result, $cacheID);
            }
        } else {
            $contractorDistances = $result;
        }
        $this->view->contractorDistances = $contractorDistances;

        //D.A 26/08/2015
        $cacheID = $bookingId . '_bookingPhotos';
        if (($result = $cache->load($cacheID)) === false) {
            $orderBy = 'i.created';
            $sortingMethod = 'desc';
            $type = 'booking';

            $modelImage = new Model_Image();
            $pager = null;
            $photo = $modelImage->getAll($bookingId, $type, "{$orderBy} {$sortingMethod}", $pager, $filter = array(), $limit = 10);
            $photoCount = count($modelImage->getAll($bookingId, $type, "{$orderBy} {$sortingMethod}"));

            $result = array(
                'photo' => $photo,
                'photoCount' => $photoCount,
                'type' => $type,
            );
            if ($result) {
                $cache->save($result, $cacheID);
            }
        } else {
            $photo = $result['photo'];
            $photoCount = $result['photoCount'];
            $type = $result['type'];
        }
        $this->view->type = $type;

        // Rating Technicans  ////////////////////////////// Salim 29-12 ///////////////////////////////
        $model_bookingMultipleDays = new Model_BookingMultipleDays();
        $lastBookingDay = $model_bookingMultipleDays->getLastMultipleDayByBookingId($bookingId);

        if (!empty($lastBookingDay['booking_end'])) {
            $bookingEnd = strtotime($lastBookingDay['booking_end']);
        } else {
            $bookingEnd = strtotime($booking['booking_end']);
        }
        $this->view->lastBookingDay = $bookingEnd;
        if (time() > $bookingEnd) {
            $model_ratingTag = new Model_RatingTag();
            $model_ratingInfo = new Model_RatingInfo();
            $ratingTag = array();
            $type = "customer_rating";
            $model_image = new Model_Image();
            $rating_info = array();
            $model_contractorServiceBooking = new Model_ContractorServiceBooking();
            $contractors = $model_contractorServiceBooking->getContractorsDataByBookingId($bookingId, 1);
			$rating_info_for_each_contractor = array();
            foreach ($contractors as $key => $contractor) {
                $model_rating_info = new Model_RatingInfo();
                $rating_info = $model_rating_info->getByContractorIdAndBookingId($contractor['contractor_id'], $bookingId);

                $rating_info_for_each_contractor[$contractor['contractor_id']] = $rating_info;
                if (!empty($rating_info)) {
                    $photo = array_merge($photo, $model_image->getAll($rating_info['rating_info_id'], $type));
//                    $customerRatingPhotoCount[$contractor['contractor_id']] = count($model_image->getAll($rating_info['rating_info_id'], $type));
                }
                $ratingTag[$contractor['contractor_id']] = $model_ratingTag->getAll($contractor['contractor_id'], 1, $bookingId);
            }

//            if (!empty($rating_info)) {
//                $this->view->customerRatingPhoto = $customerRatingPhoto;
//                $this->view->customerRatingPhotoCount = $customerRatingPhotoCount;
//            }
            $this->view->item_id = $bookingId;
            $this->view->contractors = $contractors;
            $this->view->ratingTag = $ratingTag;
            $this->view->rating_info = $rating_info_for_each_contractor;
        }


        $this->view->photo = $photo;
        $this->view->photoCount = count($photo);
    }

    public function getContractorsDistance($bookingId) {

        // Load Model
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelBookingAddress = new Model_BookingAddress();
        $modelUser = new Model_User();

        $bookingAddress = $modelBookingAddress->getByBookingId($bookingId);
        $contractorServiceBookings = $modelContractorServiceBooking->getByBookingId($bookingId);

        $contractorIds = array();
        foreach ($contractorServiceBookings as $contractorServiceBooking) {
            $contractorIds[$contractorServiceBooking['contractor_id']] = $contractorServiceBooking['contractor_id'];
        }

        $contractorDistance = array();
        foreach ($contractorIds as $contractorId) {

            $contractor = $modelUser->getById($contractorId);

            $contractorDistance[$contractor['user_id']] = array(
                'contractor_id' => $contractor['user_id'],
                'name' => $contractor['username'],
                'distance' => $modelBookingAddress->getDistanceByTwoAddress($contractor, $bookingAddress)
            );
        }

        return $contractorDistance;
    }

    public function addAction() {


        //check Auth for logged user
        CheckAuth::checkPermission(array('bookingAdd'));


        //get request parameters
        $this->_redirect($this->router->assemble(array(), 'addBooking'));
        //$this->_redirect('booking-add');
        exit;
    }

    public function addBookingToCustomerIdAction() {


        //check Auth for logged user
        CheckAuth::checkPermission(array('bookingAdd'));
        $customerId = $this->request->getParam('customer_id');

        //get request parameters
        $this->_redirect($this->router->assemble(array('customer_id' => $customerId), 'addBookingToCustomerId'));
        //$this->_redirect('booking-add');
        exit;
    }

    public function previewAction() {

        //
        // get params 
        //
        $bookingId = $this->request->getParam('id', 0);

        if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this page"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // load models
        //
        $modelBooking = new Model_Booking();

        //
        // geting data
        //
        
        $booking = $modelBooking->getById($bookingId);
        //
        // validation
        //
        if (!$booking) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Booking not found"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        //check in can see his or assigned bookings
        if (!$modelBooking->checkIfCanSeeBooking($bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this booking"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $this->getBookingViewParam($bookingId);


        echo $this->view->render('index/preview.phtml');
        exit;
    }

    public function bookingCustomerPreviewAction() {

        //
        // get params 
        //
        $bookingId = $this->request->getParam('id', 0);

        if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this page"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // load models
        //
        $modelBooking = new Model_Booking();

        //
        // geting data
        //
        
        $booking = $modelBooking->getById($bookingId);
        //
        // validation
        //
        if (!$booking) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Booking not found"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        //check in can see his or assigned bookings
        if (!$modelBooking->checkIfCanSeeBooking($bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this booking"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $this->getBookingViewParam($bookingId);


        echo $this->view->render('index/booking-customer-preview.phtml');
        exit;
    }

    public function historyAction() {

        //check Auth for logged user
        CheckAuth::checkPermission(array('bookingHistory'));

        $bookingId = $this->request->getParam('id', 0);

        if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this page"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $modelBooking = new Model_Booking();
        $modelBookingLog = new Model_BookingLog();

        //Booking
        $booking = $modelBooking->getById($bookingId);
        $this->view->booking = $booking;

        //History
        $bookingLogs = $modelBookingLog->getByBookingId($bookingId);

        $modelBookingLog->fills($bookingLogs, array('history_details'));

        $this->view->bookingLogs = $bookingLogs;
    }

    public function sendBookingAsEmailAction() {

        //
        // get params 
        //
        $bookingId = $this->request->getParam('id', 0);

        if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this page"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // load models
        //
        $modelUser = new Model_User();
        $modelBooking = new Model_Booking();
        $modelCustomer = new Model_Customer();
        $modelBookingAddress = new Model_BookingAddress();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();

        //
        // geting data
        //
        $booking = $modelBooking->getById($bookingId);
        $modelBooking->fill($booking, array('property_type', 'multiple_days'));

        //By Islam 
        $multiple = '';
        if (!empty($booking['multiple_days'])) {
            foreach ($booking['multiple_days'] as $day) {
                $multiple = $multiple . ' ' . getBookingDateFormating($day['booking_start'], $day['booking_end']);
                $multiple = $multiple . ' ' . '<br />';
            }
        }

        //
        // validation
        //
        if (!$booking) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Booking not found"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        //check in can see his or assigned bookings
        if (!$modelBooking->checkIfCanSeeBooking($bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this booking"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // filling extra data
        //
        $customer = $modelCustomer->getById($booking['customer_id']);
        $user = $modelUser->getById($booking['created_by']);

        // booking view Params
        $viewParam = $this->getBookingViewParam($bookingId, true);
        $view = new Zend_View();
        $view->setScriptPath(APPLICATION_PATH . '/modules/booking/views/scripts/index');
        $view->booking = $viewParam['booking'];
        $view->bookingServices = $viewParam['bookingServices'];
        $view->thisBookingServices = $viewParam['thisBookingServices'];
        $view->priceArray = $viewParam['priceArray'];
        $modelAttributeListValueAttachment = new Model_AttributeListValueAttachment();
        $modelServiceAttributeValue = new Model_ServiceAttributeValue();
        $serviceAttributeValues = $modelServiceAttributeValue->getByBookingIdListAttributeValues($bookingId);


        $AttributeListValueAttachments = array();
        foreach ($serviceAttributeValues as $serviceAttributeValue) {
            $listAttachment = $modelAttributeListValueAttachment->getByAttributeValueId($serviceAttributeValue['value']);
            if ($listAttachment) {
                $AttributeListValueAttachments[] = array('attachments' => $listAttachment, 'attribute_name' => $serviceAttributeValue['attribute_name']);
            }
        }

        $bodyBooking = $view->render('booking-customer.phtml');
		
		
		 /// get faq for booking

        $mdoel_contractorServiceBooking = new Model_ContractorServiceBooking();
		$trading_namesObj = new Model_TradingName();
        $allItemService = $mdoel_contractorServiceBooking->getByBookingId($booking['booking_id']);
        $trading_names = $trading_namesObj->getById($booking['trading_name_id']);
        $faq_html = "";
        $wheres = array();

        foreach ($allItemService as $key => $itemService) {
            $model_attributes = new Model_Attributes();
			$modelServiceAttribute = new Model_ServiceAttribute();
            $attribute = $model_attributes->getByAttributeName("Floor", $booking['company_id']);
            $allServiceAttribute = $modelServiceAttribute->getByAttributeIdAndServiceId($attribute['attribute_id'], $itemService['service_id']);
            $modelServiceAtrributeValue = new Model_ServiceAttributeValue();
            $serviceAttributeValue = $modelServiceAtrributeValue->getByBookingIdAndServiceAttributeIdAndClone($booking['booking_id'], $allServiceAttribute['service_attribute_id'], $itemService['clone']);

            if (!empty($serviceAttributeValue['value'])) {
                $wheres[] = 'service_id =' . $itemService['service_id'] . ' and floor_id = ' . $serviceAttributeValue['value'];
            }
        }

        if (!empty($wheres)) {
            $model_faq = new Model_Faq();
            $faq = $model_faq->getFaqByServiceIdAndFloorId($wheres);
            if (empty($faq)) {
                $faq = $model_faq->getFaqByServiceIdAndFloorId($wheres, 1);
            }

            foreach ($faq as $key => $faq_value) {
                $question = str_replace('$custom_city', $customer['city_name'], $faq_value['question']);
                $answer = str_replace('$custom_city', $customer['city_name'], $faq_value['answer']);
                $faq_html .= '<ul><li><p><a style="color:#0966c2;font-weight:bold;" href="' . $trading_names['website_url'] . '">' . $question . '</a></p></li><li style="list-style:none;"><p>' . $answer . '</p></li></ul>';
            }
        }

        $template_params = array(
            //booking
            '{booking_num}' => $booking['booking_num'],
            '{total_without_tax}' => number_format($booking['sub_total'], 2),
            '{gst_tax}' => number_format($booking['gst'], 2),
			'{faq}' => $faq_html,
            '{total_with_tax}' => number_format($booking['qoute'], 2),
            '{description}' => $booking['description'] ? $booking['description'] : '',
            '{booking_created}' => date('d/m/Y', $booking['created']),
            '{booking_created_by}' => ucwords($user['username']),
            '{booking_start}' => date("F j, Y, g:i a", strtotime($booking['booking_start'])),
            '{booking_address}' => get_line_address($modelBookingAddress->getByBookingId($booking['booking_id'])),
            '{service}' => nl2br($modelContractorServiceBooking->getBookingAsText($booking['booking_id'], true)),
            '{property}' => $booking['property_type'],
            '{booking_view}' => $bodyBooking,
            //customer
            '{customer_name}' => get_customer_name($customer),
            '{customer_first_name}' => isset($customer['first_name']) && $customer['first_name'] ? ucwords($customer['first_name']) : '',
            '{customer_last_name}' => isset($customer['last_name']) && $customer['last_name'] ? ' ' . ucwords($customer['last_name']) : '',
            '{customer_contacts}' => nl2br($modelCustomer->getCustomerContacts($booking['customer_id'])),
        );
        $template_params['{multiple}'] = $multiple;

        $modelEmailTemplate = new Model_EmailTemplate();
        $emailTemplate = $modelEmailTemplate->getEmailTemplate('send_booking_as_email', $template_params);

        $body = $emailTemplate['body'];
        $subject = $emailTemplate['subject'];
        $to = array();
        if ($customer['email1']) {
            $to[] = $customer['email1'];
        }
        if ($customer['email2']) {
            $to[] = $customer['email2'];
        }
        if ($customer['email2']) {
            $to[] = $customer['email3'];
        }
        $to = implode(',', $to);


        if ($this->request->isPost()) {
            $to = $this->request->getParam('to');
            $cc = $this->request->getParam('cc');
            $subject = $this->request->getParam('subject');
            $body = $this->request->getParam('body');
            $pdf_attachment = $this->request->getParam('pdf_attachment', 0);
            $selected_attachments = $this->request->getParam('attachment', array());
            $trading_namesObj = new Model_TradingName();
            $trading_names = $trading_namesObj->getById($booking['trading_name_id']);


            $params = array(
                'to' => $to,
                'cc' => $cc,
                'body' => $body,
                'subject' => $subject,
                'from' => $trading_names['email'],
                'trading_name' => $trading_names['trading_name']
            );

            $error_mesages = array();
            if (EmailNotification::validation($params, $error_mesages)) {
                $attachments = array();
                $modelAttachment = new Model_Attachment();
                if (!empty($selected_attachments)) {
                    foreach ($selected_attachments as $attachment_id) {
                        $attachment = $modelAttachment->getById($attachment_id);
                        $attachments[] = $attachment['path'];
                    }
                }

                if (!empty($pdf_attachment)) {
                    // Create pdf
                    $pdfPath = createPdfPath();
                    $destination = $pdfPath['fullDir'] . $viewParam['booking']['booking_num'] . '.pdf';
                    wkhtmltopdf($bodyBooking, $destination);
                    $attachments[] = $destination;
                }
                if (!empty($pdf_attachment) || !empty($selected_attachments)) {
                    $attachments = implode(",", $attachments);
                    $params['attachment'] = $attachments;
                }


                /* if (!empty($pdf_attachment)) {
                  // Create pdf
                  $pdfPath = createPdfPath();
                  $destination = $pdfPath['fullDir'] . $viewParam['booking']['booking_num'] . '.pdf';
                  wkhtmltopdf($bodyBooking, $destination);
                  $params['attachment'] = $destination;
                  } */

                // Send Email
                $success = EmailNotification::sendEmail($params, '', array(), array('reference_id' => $booking['booking_id'], 'type' => 'booking'));

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Email sent"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Failed to send booking"));
                }

                echo 1;
                exit;
            }
            $this->view->error_mesages = $error_mesages;
        }

        $this->view->booking = $booking;
        $this->view->to = $to;
        $this->view->subject = $subject;
        $this->view->body = $body;
        $this->view->cc = isset($cc) ? $cc : '';
        $this->view->AttributeListValueAttachments = $AttributeListValueAttachments;

        echo $this->view->render('index/send-booking-as-email.phtml');
        exit;
    }

    public function sendRequestBookingFeedbackAction() {
        $router = Zend_Controller_Front::getInstance()->getRouter();
        //
        // get params 
        //
        $bookingId = $this->request->getParam('id', 0);
        $goToUrl = $this->request->getParam('go_to_url', isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '/');


        if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this page"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // load models
        //
        $modelUser = new Model_User();
        $modelBooking = new Model_Booking();
        $modelCustomer = new Model_Customer();
        $modelBookingAddress = new Model_BookingAddress();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();


        //
        // geting data
        //
        $booking = $modelBooking->getById($bookingId);
        $modelBooking->fill($booking, array('property_type', 'status'));


        //
        // validation
        //
        if (!$booking) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Booking not found"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        //check in can see his or assigned bookings
        if (!$modelBooking->checkIfCanSeeBooking($bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this booking"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $modelCustomerFeedback = new Model_CustomerFeedback();
        $customerFeedback = $modelCustomerFeedback->getBybookingAndstatusId($bookingId, $booking['status_id']);

        if ($customerFeedback['feedback']) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Booking sent before"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //create code

        $feedback_hashcode = sha1(uniqid());

        $feedback_link = $router->assemble(array('feedback_hashcode' => $feedback_hashcode, 'booking_id' => $booking['booking_id'], 'status_id' => $booking['status_id']), 'feedbackBooking');

        $customer = $modelCustomer->getById($booking['customer_id']);
        $user = $modelUser->getById($booking['created_by']);

        //
        // filling extra data
        //
        $customer = $modelCustomer->getById($booking['customer_id']);
        $user = $modelUser->getById($booking['created_by']);

        // booking view Params
        $viewParam = $this->getBookingViewParam($bookingId, true);
        $view = new Zend_View();
        $view->setScriptPath(APPLICATION_PATH . '/modules/booking/views/scripts/index');
        $view->booking = $viewParam['booking'];
        $view->bookingServices = $viewParam['bookingServices'];
        $view->thisBookingServices = $viewParam['thisBookingServices'];
        $view->priceArray = $viewParam['priceArray'];

        $bodyBooking = $view->render('booking-customer.phtml');
        $template_params = array(
            //link
            '{feedback_link}' => '<a href="' . $feedback_link . '">' . $feedback_link . '</a>',
            //booking
            '{booking_num}' => $booking['booking_num'],
            '{total_without_tax}' => number_format($booking['sub_total'], 2),
            '{gst_tax}' => number_format($booking['gst'], 2),
            '{total_with_tax}' => number_format($booking['qoute'], 2),
            '{description}' => nl2br($booking['description'] ? $booking['description'] : ''),
            '{booking_created}' => date('d/m/Y', $booking['created']),
            '{booking_created_by}' => ucwords($user['username']),
            '{booking_start}' => date("F j, Y, g:i a", strtotime($booking['booking_start'])),
            '{booking_address}' => get_line_address($modelBookingAddress->getByBookingId($booking['booking_id'])),
            '{service}' => nl2br($modelContractorServiceBooking->getBookingAsText($booking['booking_id'], true)),
            '{property}' => $booking['property_type'],
            '{booking_view}' => $bodyBooking,
            //status name
            '{status_name}' => $booking['status']['name'],
            //
            //customer
            '{customer_name}' => get_customer_name($customer),
            '{customer_first_name}' => isset($customer['first_name']) && $customer['first_name'] ? ucwords($customer['first_name']) : '',
            '{customer_last_name}' => isset($customer['last_name']) && $customer['last_name'] ? ' ' . ucwords($customer['last_name']) : '',
            '{customer_contacts}' => nl2br($modelCustomer->getCustomerContacts($booking['customer_id'])),
            '{sender_name}' => ucwords($user['username'])
        );

        $modelEmailTemplate = new Model_EmailTemplate();
        $emailTemplate = $modelEmailTemplate->getEmailTemplate('feedback_booking', $template_params);

        $body = $emailTemplate['body'];
        $subject = $emailTemplate['subject'];
        $to = array();
        if ($customer['email1']) {
            $to[] = $customer['email1'];
        }
        if ($customer['email2']) {
            $to[] = $customer['email2'];
        }
        if ($customer['email2']) {
            $to[] = $customer['email3'];
        }
        $to = implode(',', $to);


        if ($this->request->isPost()) {
            $to = $this->request->getParam('to');
            $cc = $this->request->getParam('cc');
            $subject = $this->request->getParam('subject');
            $body = $this->request->getParam('body');
            $feedback_hashcode = $this->request->getParam('hashcode');
            $pdf_attachment = $this->request->getParam('pdf_attachment', 0);
            $trading_namesObj = new Model_TradingName();
            $trading_names = $trading_namesObj->getById($booking['trading_name_id']);
            $selected_attachments = $this->request->getParam('attachment', array());

            $params = array(
                'to' => $to,
                'cc' => $cc,
                'body' => $body,
                'subject' => $subject,
                'trading_name' => $trading_names['trading_name'],
                'from' => $trading_names['email'],
            );
            $attachments = array();
            $modelAttachment = new Model_Attachment();
            if (!empty($selected_attachments)) {
                foreach ($selected_attachments as $attachment_id) {
                    $attachment = $modelAttachment->getById($attachment_id);
                    $attachments[] = $attachment['path'];
                }
            }

            if (!empty($pdf_attachment)) {
                // Create pdf
                $pdfPath = createPdfPath();
                $destination = $pdfPath['fullDir'] . $viewParam['booking']['booking_num'] . '.pdf';
                wkhtmltopdf($bodyBooking, $destination);
                $attachments[] = $destination;
            }

            if (!empty($pdf_attachment) || !empty($selected_attachments)) {
                $attachments = implode(",", $attachments);
                $params['attachment'] = $attachments;
            }

            /* if (!empty($pdf_attachment)) {
              // Create pdf
              $pdfPath = createPdfPath();
              $destination = $pdfPath['fullDir'] . $viewParam['booking']['booking_num'] . '.pdf';
              wkhtmltopdf($bodyBooking, $destination);
              $params['attachment'] = $destination;
              } */

            $error_mesages = array();
            if (EmailNotification::validation($params, $error_mesages)) {

                // Send Email
                $success = EmailNotification::sendEmail($params, '', array(), array('reference_id' => $booking['booking_id'], 'type' => 'booking'));

                if ($success) {
                    $data = array(
                        'booking_id' => $booking['booking_id'],
                        'status_id' => $booking['status_id'],
                        'hashcode' => $feedback_hashcode,
                        'created' => time()
                    );
                    $modelCustomerFeedback = new Model_CustomerFeedback();
                    $modelCustomerFeedback->insert($data);

                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Email sent"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Failed to send email"));
                }

                echo 1;
                exit;
            }
            $this->view->error_mesages = $error_mesages;
        }
        $modelAttributeListValueAttachment = new Model_AttributeListValueAttachment();
        $modelServiceAttributeValue = new Model_ServiceAttributeValue();
        $serviceAttributeValues = $modelServiceAttributeValue->getByBookingIdListAttributeValues($bookingId);


        $AttributeListValueAttachments = array();
        foreach ($serviceAttributeValues as $serviceAttributeValue) {
            $listAttachment = $modelAttributeListValueAttachment->getByAttributeValueId($serviceAttributeValue['value']);
            if ($listAttachment) {
                $AttributeListValueAttachments[] = array('attachments' => $listAttachment, 'attribute_name' => $serviceAttributeValue['attribute_name']);
            }
        }


        $this->view->goToUrl = $goToUrl;
        $this->view->booking = $booking;
        $this->view->to = $to;
        $this->view->subject = $subject;
        $this->view->body = $body;
        $this->view->hashcode = $feedback_hashcode;
        $this->view->AttributeListValueAttachments = $AttributeListValueAttachments;
        $this->view->cc = isset($cc) ? $cc : '';

        echo $this->view->render('index/send-request-booking-feedback.phtml');
        exit;
    }

    public function sendBookingConfirmationAsEmailAction() {

        //
        // get params 
        //
        $bookingId = $this->request->getParam('id', 0);
        $goToUrl = $this->request->getParam('go_to_url', isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '/');


        if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this page"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // load models
        //
        $modelUser = new Model_User();
        $modelBooking = new Model_Booking();
        $modelCustomer = new Model_Customer();
        $modelBookingAddress = new Model_BookingAddress();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelBookingStatus = new Model_BookingStatus();
        $trading_namesObj = new Model_TradingName();
        $modelServiceAttribute = new Model_ServiceAttribute();

        //
        // geting data
        //
        $booking = $modelBooking->getById($bookingId);
        $modelBooking->fill($booking, array('property_type', 'multiple_days'));

        //By Islam 
        $multiple = '';
        if (!empty($booking['multiple_days'])) {
            foreach ($booking['multiple_days'] as $day) {
                $multiple = $multiple . ' ' . getBookingDateFormating($day['booking_start'], $day['booking_end']);
                $multiple = $multiple . ' ' . '<br />';
            }
        }




        ////
        $template_name = 'send_booking_as_email';

        $quoted = $modelBookingStatus->getByStatusName('QUOTED');
        if ($booking['status_id'] == $quoted['booking_status_id']) {
            $template_name = 'send_estimate_as_email';
            $modelBooking->fill($booking, array('estimate', 'multiple_days'));
        }

        //
        // validation
        //
        if (!$booking) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Booking not found"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        //check in can see his or assigned bookings
        if (!$modelBooking->checkIfCanSeeBooking($bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this booking"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // filling extra data
        //
        $customer = $modelCustomer->getById($booking['customer_id']);
        $user = $modelUser->getById($booking['created_by']);
        $isChecked = false;

        /// get faq for booking

        $mdoel_contractorServiceBooking = new Model_ContractorServiceBooking();
        $allItemService = $mdoel_contractorServiceBooking->getByBookingId($booking['booking_id']);
        $trading_names = $trading_namesObj->getById($booking['trading_name_id']);
        $faq_html = "";
        $wheres = array();

        foreach ($allItemService as $key => $itemService) {
            $model_attributes = new Model_Attributes();
            $attribute = $model_attributes->getByAttributeName("Floor", $booking['company_id']);
            $allServiceAttribute = $modelServiceAttribute->getByAttributeIdAndServiceId($attribute['attribute_id'], $itemService['service_id']);
            $modelServiceAtrributeValue = new Model_ServiceAttributeValue();
            $serviceAttributeValue = $modelServiceAtrributeValue->getByBookingIdAndServiceAttributeIdAndClone($booking['booking_id'], $allServiceAttribute['service_attribute_id'], $itemService['clone']);

            if (!empty($serviceAttributeValue['value'])) {
                $wheres[] = 'service_id =' . $itemService['service_id'] . ' and floor_id = ' . $serviceAttributeValue['value'];
            }
        }

        if (!empty($wheres)) {
            $model_faq = new Model_Faq();
            $faq = $model_faq->getFaqByServiceIdAndFloorId($wheres);
            if (empty($faq)) {
                $faq = $model_faq->getFaqByServiceIdAndFloorId($wheres, 1);
            }

            foreach ($faq as $key => $faq_value) {
                $question = str_replace('$custom_city', $customer['city_name'], $faq_value['question']);
                $answer = str_replace('$custom_city', $customer['city_name'], $faq_value['answer']);
                $faq_html .= '<ul><li><p><a style="color:#0966c2;font-weight:bold;" href="' . $trading_names['website_url'] . '">' . $question . '</a></p></li><li style="list-style:none;"><p>' . $answer . '</p></li></ul>';
            }
        }


        if ($booking['status_id'] != $quoted['booking_status_id']) {


            $viewParam = $this->getBookingViewParam($bookingId, true);
            $view = new Zend_View();
            $view->setScriptPath(APPLICATION_PATH . '/modules/booking/views/scripts/index');
            $view->booking = $viewParam['booking'];
            $view->bookingServices = $viewParam['bookingServices'];
            $view->thisBookingServices = $viewParam['thisBookingServices'];
            $view->priceArray = $viewParam['priceArray'];
            $bodyView = $view->render('booking-customer.phtml');

            $template_params = array(
                //booking
                '{booking_num}' => $booking['booking_num'],
                '{booking_link}' => $this->router->assemble(array('id' => $booking['booking_id']), 'confirm'),
                '{faq}' => $faq_html,
                '{total_without_tax}' => number_format($booking['sub_total'], 2),
                '{gst_tax}' => number_format($booking['gst'], 2),
                '{total_with_tax}' => number_format($booking['qoute'], 2),
                '{description}' => $booking['description'] ? $booking['description'] : '',
                '{booking_created}' => date('d/m/Y', $booking['created']),
                '{booking_created_by}' => ucwords($user['username']),
                '{booking_start}' => date("F j, Y, g:i a", strtotime($booking['booking_start'])),
                '{booking_address}' => get_line_address($modelBookingAddress->getByBookingId($booking['booking_id'])),
                '{service}' => nl2br($modelContractorServiceBooking->getBookingAsText($booking['booking_id'], true)),
                '{property}' => $booking['property_type'],
                '{booking_view}' => $bodyView,
                //customer
                '{customer_name}' => get_customer_name($customer),
                '{customer_first_name}' => isset($customer['first_name']) && $customer['first_name'] ? ucwords($customer['first_name']) : '',
                '{customer_last_name}' => isset($customer['last_name']) && $customer['last_name'] ? ' ' . ucwords($customer['last_name']) : '',
                '{customer_contacts}' => nl2br($modelCustomer->getCustomerContacts($booking['customer_id']))
            );
        } else {
            $isChecked = true;
            //estimate view param
            $viewParam = $this->getBookingViewParam($bookingId, true);
            $view = new Zend_View();
            $view->setScriptPath(APPLICATION_PATH . '/modules/estimates/views/scripts/index');
            $view->bookingServices = $viewParam['bookingServices'];
            $view->thisBookingServices = $viewParam['thisBookingServices'];
            $view->priceArray = $viewParam['priceArray'];

            $estimate = $booking['estimate'];

            $modelBookingEstimate = new Model_BookingEstimate();
            $modelBookingEstimate->fill($estimate, array('booking', 'customer_commercial_info'));

            $view->estimate = $estimate;
            $bodyView = $view->render('estimate.phtml');

            $template_params = array(
                //estimate
                '{estimate_num}' => $booking['estimate']['estimate_num'],
                '{estimate_link}' => $this->router->assemble(array('id' => $booking['estimate']['id']), 'estimateView'),
                '{estimate_created}' => date('d/m/Y', $booking['estimate']['created']),
                //booking
                '{booking_num}' => $booking['booking_num'],
                '{total_without_tax}' => number_format($booking['sub_total'], 2),
                '{gst_tax}' => number_format($booking['gst'], 2),
                '{total_with_tax}' => number_format($booking['qoute'], 2),
                '{description}' => $booking['description'] ? $booking['description'] : '',
                '{booking_created}' => date('d/m/Y', $booking['created']),
                '{booking_created_by}' => ucwords($user['username']),
                '{booking_start}' => date("F j, Y, g:i a", strtotime($booking['booking_start'])),
                '{booking_address}' => get_line_address($modelBookingAddress->getByBookingId($booking['booking_id'])),
                '{service}' => nl2br($modelContractorServiceBooking->getBookingAsText($booking['booking_id'], true)),
                '{estimate_view}' => $bodyView,
                //customer
                '{customer_name}' => get_customer_name($customer),
                '{customer_first_name}' => isset($customer['first_name']) && $customer['first_name'] ? ucwords($customer['first_name']) : '',
                '{customer_last_name}' => isset($customer['last_name']) && $customer['last_name'] ? ' ' . ucwords($customer['last_name']) : '',
                '{customer_contacts}' => nl2br($modelCustomer->getCustomerContacts($booking['customer_id'])),
            );
        }
        $template_params['{multiple}'] = $multiple;
        $modelEmailTemplate = new Model_EmailTemplate();
        $emailTemplate = $modelEmailTemplate->getEmailTemplate($template_name, $template_params);

        $body = $emailTemplate['body'];
        $subject = $emailTemplate['subject'];
        $to = array();
        if ($customer['email1']) {
            $to[] = $customer['email1'];
        }
        if ($customer['email2']) {
            $to[] = $customer['email2'];
        }
        if ($customer['email2']) {
            $to[] = $customer['email3'];
        }
        $to = implode(',', $to);
        $modelAttributeListValueAttachment = new Model_AttributeListValueAttachment();
        $modelServiceAttributeValue = new Model_ServiceAttributeValue();
        $serviceAttributeValues = $modelServiceAttributeValue->getByBookingIdListAttributeValues($bookingId);


        $AttributeListValueAttachments = array();
        foreach ($serviceAttributeValues as $serviceAttributeValue) {
            $listAttachment = $modelAttributeListValueAttachment->getByAttributeValueId($serviceAttributeValue['value']);
            if ($listAttachment) {
                $AttributeListValueAttachments[] = array('attachments' => $listAttachment, 'attribute_name' => $serviceAttributeValue['attribute_name']);
            }
        }



        if ($this->request->isPost()) {
            $to = $this->request->getParam('to');
            $cc = $this->request->getParam('cc');
            $subject = $this->request->getParam('subject');
            $body = $this->request->getParam('body');
            $pdf_attachment = $this->request->getParam('pdf_attachment', 0);
            $selected_attachments = $this->request->getParam('attachment', array());

            $trading_namesObj = new Model_TradingName();
            $trading_names = $trading_namesObj->getById($booking['trading_name_id']);


            $params = array(
                'to' => $to,
                'cc' => $cc,
                'body' => $body,
                'subject' => $subject,
                'trading_name' => $trading_names['trading_name'],
                'from' => $trading_names['email'],
            );

            $error_mesages = array();
            if (EmailNotification::validation($params, $error_mesages)) {
                $attachments = array();
                $modelAttachment = new Model_Attachment();
                if (!empty($selected_attachments)) {
                    foreach ($selected_attachments as $attachment_id) {
                        $attachment = $modelAttachment->getById($attachment_id);
                        $attachments[] = $attachment['path'];
                    }
                }

                if (!empty($pdf_attachment)) {
                    // Create pdf
                    $pdfPath = createPdfPath();
                    if ($booking['status_id'] != $quoted['booking_status_id']) {
                        $destination = $pdfPath['fullDir'] . $viewParam['booking']['booking_num'] . '.pdf';
                    } else {
                        $destination = $pdfPath['fullDir'] . $booking['estimate']['estimate_num'] . '.pdf';
                    }
                    wkhtmltopdf($bodyView, $destination);
                    $attachments[] = $destination;
                    //$params['attachment'] = $destination;
                }

                if (!empty($pdf_attachment) || !empty($selected_attachments)) {
                    $attachments = implode(",", $attachments);
                    $params['attachment'] = $attachments;
                }

                /* if (!empty($pdf_attachment)) {
                  $pdfPath = createPdfPath();
                  if ($booking['status_id'] != $quoted['booking_status_id']) {
                  $destination = $pdfPath['fullDir'] . $viewParam['booking']['booking_num'] . '.pdf';
                  } else {
                  $destination = $pdfPath['fullDir'] . $booking['estimate']['estimate_num'] . '.pdf';
                  }
                  wkhtmltopdf($bodyView, $destination);
                  $params['attachment'] = $destination;
                  } */

                // Send Email
                if ($booking['status_id'] != $quoted['booking_status_id']) {
                    $success = EmailNotification::sendEmail($params, '', array(), array('reference_id' => $booking['booking_id'], 'type' => 'booking'));
                } else {
                    $success = EmailNotification::sendEmail($params, '', array(), array('reference_id' => $booking['estimate']['id'], 'type' => 'estimate'));
                }

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Email sent"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Failed to send email"));
                }

                echo 1;
                exit;
            }
            $this->view->error_mesages = $error_mesages;
        }

        $this->view->is_checked = $isChecked;
        $this->view->goToUrl = $goToUrl;
        $this->view->booking = $booking;
        $this->view->to = $to;
        $this->view->subject = $subject;
        $this->view->body = $body;
        $this->view->AttributeListValueAttachments = $AttributeListValueAttachments;
        $this->view->cc = isset($cc) ? $cc : '';

        echo $this->view->render('index/send-booking-confirmation-as-email.phtml');
        exit;
    }

    public function sendReminderOnHoldBookingAsEmailAction() {

        //
        // get params 
        //
        $bookingId = $this->request->getParam('id', 0);

        if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this page"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // load models
        //
        $modelUser = new Model_User();
        $modelBooking = new Model_Booking();
        $modelCustomer = new Model_Customer();
        $modelBookingAddress = new Model_BookingAddress();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelBookingStatus = new Model_BookingStatus();
        //
        // geting data
        //
        $booking = $modelBooking->getById($bookingId);
        $modelBooking->fill($booking, array('property_type'));

        $onHold = $modelBookingStatus->getByStatusName('ON HOLD');

        //
        // validation
        //
        if (!$booking) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Booking not found"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        if ($booking['status_id'] != $onHold['booking_status_id']) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This booking is not ON HOLD"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        //check in can see his or assigned bookings
        if (!$modelBooking->checkIfCanSeeBooking($bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this booking"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        // booking view Params
        $viewParam = $this->getBookingViewParam($bookingId, true);
        $view = new Zend_View();
        $view->setScriptPath(APPLICATION_PATH . '/modules/booking/views/scripts/index');
        $view->booking = $viewParam['booking'];
        $view->bookingServices = $viewParam['bookingServices'];
        $view->thisBookingServices = $viewParam['thisBookingServices'];
        $view->priceArray = $viewParam['priceArray'];
        $modelAttributeListValueAttachment = new Model_AttributeListValueAttachment();
        $modelServiceAttributeValue = new Model_ServiceAttributeValue();
        $serviceAttributeValues = $modelServiceAttributeValue->getByBookingIdListAttributeValues($bookingId);


        $AttributeListValueAttachments = array();
        foreach ($serviceAttributeValues as $serviceAttributeValue) {
            $listAttachment = $modelAttributeListValueAttachment->getByAttributeValueId($serviceAttributeValue['value']);
            if ($listAttachment) {
                $AttributeListValueAttachments[] = array('attachments' => $listAttachment, 'attribute_name' => $serviceAttributeValue['attribute_name']);
            }
        }

        $bodyBooking = $view->render('booking-customer.phtml');

        if (!$this->request->isPost()) {
            //
            // filling extra data
            //
            $cancel_hashcode = sha1(uniqid());
            $modelBooking->updateById($booking['booking_id'], array('cancel_hashcode' => $cancel_hashcode));

            $cancel_link = $this->router->assemble(array('cancel_hashcode' => $cancel_hashcode, 'booking_id' => $booking['booking_id'], 'status_id' => $onHold['booking_status_id']), 'cancelBooking');

            $customer = $modelCustomer->getById($booking['customer_id']);
            $user = $modelUser->getById($booking['created_by']);

            $template_params = array(
                //link
                '{cancel_link}' => '<a href="' . $cancel_link . '">' . $cancel_link . '</a>',
                //booking
                '{booking_num}' => $booking['booking_num'],
                '{to_follow}' => getDateFormating($booking['to_follow']),
                '{total_without_tax}' => number_format($booking['sub_total'], 2),
                '{gst_tax}' => number_format($booking['gst'], 2),
                '{total_with_tax}' => number_format($booking['qoute'], 2),
                '{description}' => $booking['description'] ? $booking['description'] : '',
                '{booking_created}' => date('d/m/Y', $booking['created']),
                '{booking_created_by}' => ucwords($user['username']),
                '{booking_start}' => date("F j, Y, g:i a", strtotime($booking['booking_start'])),
                '{booking_address}' => get_line_address($modelBookingAddress->getByBookingId($booking['booking_id'])),
                '{service}' => nl2br($modelContractorServiceBooking->getBookingAsText($booking['booking_id'], true)),
                '{property}' => $booking['property_type'],
                '{booking_view}' => $bodyBooking,
                //customer
                '{customer_name}' => get_customer_name($customer),
                '{customer_first_name}' => isset($customer['first_name']) && $customer['first_name'] ? ucwords($customer['first_name']) : '',
                '{customer_last_name}' => isset($customer['last_name']) && $customer['last_name'] ? ' ' . ucwords($customer['last_name']) : '',
                '{customer_contacts}' => nl2br($modelCustomer->getCustomerContacts($booking['customer_id'])),
            );

            $modelEmailTemplate = new Model_EmailTemplate();
            $emailTemplate = $modelEmailTemplate->getEmailTemplate('reminder_on_hold_booking', $template_params);

            $body = $emailTemplate['body'];
            $subject = $emailTemplate['subject'];
            $to = array();
            if ($customer['email1']) {
                $to[] = $customer['email1'];
            }
            if ($customer['email2']) {
                $to[] = $customer['email2'];
            }
            if ($customer['email2']) {
                $to[] = $customer['email3'];
            }
            $to = implode(',', $to);
        } else {
            $to = $this->request->getParam('to');
            $cc = $this->request->getParam('cc');
            $subject = $this->request->getParam('subject');
            $body = $this->request->getParam('body');
            $pdf_attachment = $this->request->getParam('pdf_attachment', 0);
            $selected_attachments = $this->request->getParam('attachment', array());


            $trading_namesObj = new Model_TradingName();
            $trading_names = $trading_namesObj->getById($booking['trading_name_id']);

            $params = array(
                'to' => $to,
                'cc' => $cc,
                'body' => $body,
                'subject' => $subject,
                'trading_name' => $trading_names['trading_name'],
                'from' => $trading_names['email'],
            );

            $modelAttachment = new Model_Attachment();
            $attachments = array();
            if (!empty($selected_attachments)) {
                foreach ($selected_attachments as $attachment_id) {
                    $attachment = $modelAttachment->getById($attachment_id);
                    $attachments[] = $attachment['path'];
                }
            }
            if (!empty($pdf_attachment)) {
                // Create pdf
                $pdfPath = createPdfPath();
                $destination = $pdfPath['fullDir'] . $viewParam['booking']['booking_num'] . '.pdf';
                wkhtmltopdf($bodyBooking, $destination);
                $attachments[] = $destination;
            }

            if (!empty($pdf_attachment) || !empty($selected_attachments)) {
                $attachments = implode(",", $attachments);
                $params['attachment'] = $attachments;
            }

            /* if (!empty($pdf_attachment)) {
              // Create pdf
              $pdfPath = createPdfPath();
              $destination = $pdfPath['fullDir'] . $viewParam['booking']['booking_num'] . '.pdf';
              wkhtmltopdf($bodyBooking, $destination);
              $params['attachment'] = $destination;
              } */



            $error_mesages = array();
            if (EmailNotification::validation($params, $error_mesages)) {

                // Send Email
                $success = EmailNotification::sendEmail($params, '', array(), array('reference_id' => $booking['booking_id'], 'type' => 'booking'));

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Email sent"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Failed to send email"));
                }

                echo 1;
                exit;
            }
            $this->view->error_mesages = $error_mesages;
        }

        $this->view->booking = $booking;
        $this->view->to = $to;
        $this->view->subject = $subject;
        $this->view->body = $body;
        $this->view->cc = isset($cc) ? $cc : '';
        $this->view->AttributeListValueAttachments = $AttributeListValueAttachments;

        echo $this->view->render('index/send-reminder-on-hold-booking-as-email.phtml');
        exit;
    }

    public function sendReminderTentativeBookingAsEmailAction() {

        //
        // get params 
        //
        $bookingId = $this->request->getParam('id', 0);

        if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this page"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // load models
        //
        $modelUser = new Model_User();
        $modelBooking = new Model_Booking();
        $modelCustomer = new Model_Customer();
        $modelBookingAddress = new Model_BookingAddress();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelBookingStatus = new Model_BookingStatus();
        $modelAttachment = new Model_Attachment();

        //
        // geting data
        //
        $booking = $modelBooking->getById($bookingId);
        $modelBooking->fill($booking, array('property_type'));

        $tentative = $modelBookingStatus->getByStatusName('TENTATIVE');

        //
        // validation
        //
        if (!$booking) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Booking not found"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        if ($booking['status_id'] != $tentative['booking_status_id']) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This booking is not ON HOLD"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        //check in can see his or assigned bookings
        if (!$modelBooking->checkIfCanSeeBooking($bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this booking"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        // booking view Params
        $viewParam = $this->getBookingViewParam($bookingId, true);
        $view = new Zend_View();
        $view->setScriptPath(APPLICATION_PATH . '/modules/booking/views/scripts/index');
        $view->booking = $viewParam['booking'];
        $view->bookingServices = $viewParam['bookingServices'];
        $view->thisBookingServices = $viewParam['thisBookingServices'];
        $view->priceArray = $viewParam['priceArray'];
        $modelAttributeListValueAttachment = new Model_AttributeListValueAttachment();
        $modelServiceAttributeValue = new Model_ServiceAttributeValue();
        $serviceAttributeValues = $modelServiceAttributeValue->getByBookingIdListAttributeValues($bookingId);


        $AttributeListValueAttachments = array();
        foreach ($serviceAttributeValues as $serviceAttributeValue) {
            $listAttachment = $modelAttributeListValueAttachment->getByAttributeValueId($serviceAttributeValue['value']);
            if ($listAttachment) {
                $AttributeListValueAttachments[] = array('attachments' => $listAttachment, 'attribute_name' => $serviceAttributeValue['attribute_name']);
            }
        }

        $bodyBooking = $view->render('booking-customer.phtml');
        if (!$this->request->isPost()) {
            //
            // filling extra data
            //
            $cancel_hashcode = sha1(uniqid());
            $modelBooking->updateById($booking['booking_id'], array('cancel_hashcode' => $cancel_hashcode));

            $cancel_link = $this->router->assemble(array('cancel_hashcode' => $cancel_hashcode, 'booking_id' => $booking['booking_id'], 'status_id' => $tentative['booking_status_id']), 'cancelBooking');

            $customer = $modelCustomer->getById($booking['customer_id']);
            $user = $modelUser->getById($booking['created_by']);

            $template_params = array(
                //link
                '{cancel_link}' => '<a href="' . $cancel_link . '">' . $cancel_link . '</a>',
                //booking
                '{booking_num}' => $booking['booking_num'],
                '{to_follow}' => getDateFormating($booking['to_follow']),
                '{total_without_tax}' => number_format($booking['sub_total'], 2),
                '{gst_tax}' => number_format($booking['gst'], 2),
                '{total_with_tax}' => number_format($booking['qoute'], 2),
                '{description}' => $booking['description'] ? $booking['description'] : '',
                '{booking_created}' => date('d/m/Y', $booking['created']),
                '{booking_created_by}' => ucwords($user['username']),
                '{booking_start}' => date("F j, Y, g:i a", strtotime($booking['booking_start'])),
                '{booking_address}' => get_line_address($modelBookingAddress->getByBookingId($booking['booking_id'])),
                '{service}' => nl2br($modelContractorServiceBooking->getBookingAsText($booking['booking_id'], true)),
                '{property}' => $booking['property_type'],
                '{booking_view}' => $bodyBooking,
                //customer
                '{customer_name}' => get_customer_name($customer),
                '{customer_first_name}' => isset($customer['first_name']) && $customer['first_name'] ? ucwords($customer['first_name']) : '',
                '{customer_last_name}' => isset($customer['last_name']) && $customer['last_name'] ? ' ' . ucwords($customer['last_name']) : '',
                '{customer_contacts}' => nl2br($modelCustomer->getCustomerContacts($booking['customer_id'])),
            );

            $modelEmailTemplate = new Model_EmailTemplate();
            $emailTemplate = $modelEmailTemplate->getEmailTemplate('reminder_tentative_booking', $template_params);

            $body = $emailTemplate['body'];
            $subject = $emailTemplate['subject'];
            $to = array();
            if ($customer['email1']) {
                $to[] = $customer['email1'];
            }
            if ($customer['email2']) {
                $to[] = $customer['email2'];
            }
            if ($customer['email2']) {
                $to[] = $customer['email3'];
            }
            $to = implode(',', $to);
        } else {
            $to = $this->request->getParam('to');
            $cc = $this->request->getParam('cc');
            $subject = $this->request->getParam('subject');
            $body = $this->request->getParam('body');
            $pdf_attachment = $this->request->getParam('pdf_attachment', 0);
            $selected_attachments = $this->request->getParam('attachment', array());


            $trading_namesObj = new Model_TradingName();
            $trading_names = $trading_namesObj->getById($booking['trading_name_id']);


            $params = array(
                'to' => $to,
                'cc' => $cc,
                'body' => $body,
                'subject' => $subject,
                'trading_name' => $trading_names['trading_name'],
                'from' => $trading_names['email'],
            );

            $error_mesages = array();
            if (EmailNotification::validation($params, $error_mesages)) {
                $attachments = array();
                if (!empty($selected_attachments)) {
                    foreach ($selected_attachments as $attachment_id) {
                        $attachment = $modelAttachment->getById($attachment_id);
                        $attachments[] = $attachment['path'];
                    }
                }

                if (!empty($pdf_attachment)) {
                    // Create pdf
                    $pdfPath = createPdfPath();
                    $destination = $pdfPath['fullDir'] . $viewParam['booking']['booking_num'] . '.pdf';
                    wkhtmltopdf($bodyBooking, $destination);
                    $attachments[] = $destination;
                }

                if (!empty($pdf_attachment) || !empty($selected_attachments)) {
                    $attachments = implode(",", $attachments);
                    $params['attachment'] = $attachments;
                }

                /* if (!empty($pdf_attachment)) {
                  // Create pdf
                  $pdfPath = createPdfPath();
                  $destination = $pdfPath['fullDir'] . $viewParam['booking']['booking_num'] . '.pdf';
                  wkhtmltopdf($bodyBooking, $destination);
                  $params['attachment'] = $destination;
                  } */
                // Send Email
                $success = EmailNotification::sendEmail($params, '', array(), array('reference_id' => $booking['booking_id'], 'type' => 'booking'));

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Email sent"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Failed to send email"));
                }

                echo 1;
                exit;
            }
            $this->view->error_mesages = $error_mesages;
        }

        $this->view->booking = $booking;
        $this->view->to = $to;
        $this->view->subject = $subject;
        $this->view->body = $body;
        $this->view->cc = isset($cc) ? $cc : '';

        echo $this->view->render('index/send-reminder-tentative-booking-as-email.phtml');
        exit;
    }

    public function sendBookingAsEmailToContractorAction() {

        //
        // get params 
        //
        $bookingId = $this->request->getParam('id', 0);

        if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this page"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // load models
        //
        $modelUser = new Model_User();
        $modelBooking = new Model_Booking();
        $modelCustomer = new Model_Customer();
        $modelBookingAddress = new Model_BookingAddress();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();

        //
        // geting data
        //
        $booking = $modelBooking->getById($bookingId);
        $modelBooking->fill($booking, array('property_type', 'status', 'name_contractors', 'multiple_days'));

        //By Islam 
        $multiple = '';
        if (!empty($booking['multiple_days'])) {
            foreach ($booking['multiple_days'] as $day) {
                $multiple = $multiple . ' ' . getBookingDateFormating($day['booking_start'], $day['booking_end']);
                $multiple = $multiple . ' ' . '<br />';
            }
        }

        //
        // validation
        //
        if (!$booking) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Booking not found"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        //check in can see his or assigned bookings
        if (!$modelBooking->checkIfCanSeeBooking($bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this booking"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // filling extra data
        //
        
        $customer = $modelCustomer->getById($booking['customer_id']);
        $user = $modelUser->getById($booking['created_by']);

        // Get contractors email
        $contractorServices = $modelContractorServiceBooking->getContractorServicesByBookingId($bookingId);
        $contractorEmails = array();
        foreach ($contractorServices as $contractorService) {
            $contractor = $modelUser->getById($contractorService['contractor_id']);
            $emails = array();
            if ($contractor['email1'] && filter_var($contractor['email1'], FILTER_VALIDATE_EMAIL)) {
                $emails[] = $contractor['email1'];
            }
            if ($contractor['email2'] && filter_var($contractor['email2'], FILTER_VALIDATE_EMAIL)) {
                $emails[] = $contractor['email2'];
            }
            if ($contractor['email3'] && filter_var($contractor['email3'], FILTER_VALIDATE_EMAIL)) {
                $emails[] = $contractor['email3'];
            }
            $emails = implode(',', $emails);
            $contractorEmails[$contractorService['contractor_id']] = $emails;
        }

        // Create pdf
        $viewParam = $this->getBookingViewParam($bookingId, true);
        $view = new Zend_View();
        $view->setScriptPath(APPLICATION_PATH . '/modules/booking/views/scripts/index');
        $view->booking = $viewParam['booking'];
        $view->bookingServices = $viewParam['bookingServices'];
        $view->thisBookingServices = $viewParam['thisBookingServices'];
        $view->priceArray = $viewParam['priceArray'];
        $bodyBooking = $view->render('booking.phtml');

        $template_params = array(
            //booking
            '{booking_num}' => $booking['booking_num'],
            '{total_without_tax}' => number_format($booking['sub_total'], 2),
            '{gst_tax}' => number_format($booking['gst'], 2),
            '{total_with_tax}' => number_format($booking['qoute'], 2),
            '{description}' => $booking['description'] ? $booking['description'] : '',
            '{booking_created}' => date('d/m/Y', $booking['created']),
            '{booking_created_by}' => ucwords($user['username']),
            '{booking_start}' => date("F j, Y, g:i a", strtotime($booking['booking_start'])) . '<br />' . $multiple,
            '{booking_address}' => get_line_address($modelBookingAddress->getByBookingId($booking['booking_id'])),
            '{service}' => nl2br($modelContractorServiceBooking->getBookingAsText($booking['booking_id'], true)),
            '{property}' => $booking['property_type'],
            '{booking_view}' => $bodyBooking,
            //customer
            '{customer_name}' => get_customer_name($customer),
            '{customer_first_name}' => isset($customer['first_name']) && $customer['first_name'] ? ucwords($customer['first_name']) : '',
            '{customer_last_name}' => isset($customer['last_name']) && $customer['last_name'] ? ' ' . ucwords($customer['last_name']) : '',
            '{customer_contacts}' => nl2br($modelCustomer->getCustomerContacts($booking['customer_id'])),
            '{booking_status}' => $booking['status']['name'],
            '{technician_display_name}' => implode(',', $booking['name_contractors'])
        );

        $modelEmailTemplate = new Model_EmailTemplate();
        $emailTemplate = $modelEmailTemplate->getEmailTemplate('send_booking_as_email_to_contractor', $template_params);

        $body = $emailTemplate['body'];
        $subject = $emailTemplate['subject'];
        $to = implode(',', $contractorEmails);
        $modelAttributeListValueAttachment = new Model_AttributeListValueAttachment();
        $modelServiceAttributeValue = new Model_ServiceAttributeValue();
        $serviceAttributeValues = $modelServiceAttributeValue->getByBookingIdListAttributeValues($bookingId);


        $AttributeListValueAttachments = array();
        foreach ($serviceAttributeValues as $serviceAttributeValue) {
            $listAttachment = $modelAttributeListValueAttachment->getByAttributeValueId($serviceAttributeValue['value']);
            if ($listAttachment) {
                $AttributeListValueAttachments[] = array('attachments' => $listAttachment, 'attribute_name' => $serviceAttributeValue['attribute_name']);
            }
        }

        if ($this->request->isPost()) {

            $to = $this->request->getParam('to');
            $cc = $this->request->getParam('cc');
            $subject = $this->request->getParam('subject');
            $body = $this->request->getParam('body');
            $pdf_attachment = $this->request->getParam('pdf_attachment', 0);
            $selected_attachments = $this->request->getParam('attachment', array());

            $params = array(
                'to' => $to,
                'cc' => $cc,
                'body' => $body,
                'subject' => $subject,
            );

            $error_mesages = array();

            if (EmailNotification::validation($params, $error_mesages)) {
                $attachments = array();
                $modelAttachment = new Model_Attachment();
                if (!empty($selected_attachments)) {
                    foreach ($selected_attachments as $attachment_id) {
                        $attachment = $modelAttachment->getById($attachment_id);
                        $attachments[] = $attachment['path'];
                    }
                }

                if (!empty($pdf_attachment)) {
                    // Create pdf
                    $pdfPath = createPdfPath();
                    $destination = $pdfPath['fullDir'] . $viewParam['booking']['booking_num'] . '.pdf';
                    wkhtmltopdf($bodyBooking, $destination);
                    $attachments[] = $destination;
                }

                if (!empty($pdf_attachment) || !empty($selected_attachments)) {
                    $attachments = implode(",", $attachments);
                    $params['attachment'] = $attachments;
                }

                /* if (!empty($pdf_attachment)) {
                  $pdfPath = createPdfPath();
                  $destination = $pdfPath['fullDir'] . $viewParam['booking']['booking_num'] . '.pdf';
                  wkhtmltopdf($bodyBooking, $destination);
                  $params['attachment'] = $destination;
                  } */

                // Send Email
                $success = EmailNotification::sendEmail($params, '', array(), array('reference_id' => $booking['booking_id'], 'type' => 'booking'));


                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Email sent"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Failed to send booking"));
                }

                echo 1;
                exit;
            }
            $this->view->error_mesages = $error_mesages;
        }

        $this->view->booking = $booking;
        $this->view->to = $to;
        $this->view->subject = $subject;
        $this->view->body = $body;
        $this->view->cc = isset($cc) ? $cc : '';
        $this->view->AttributeListValueAttachments = $AttributeListValueAttachments;


        echo $this->view->render('index/send-booking-as-email-to-contractor.phtml');
        exit;
    }

    public function sendWorkOrderRequestEmailAction() {

        //
        // get params 
        //
        $bookingId = $this->request->getParam('id', 0);

        if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this page"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // load models
        //
        $modelUser = new Model_User();
        $modelBooking = new Model_Booking();
        $modelCustomer = new Model_Customer();
        $modelBookingAddress = new Model_BookingAddress();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelCustomerType = new Model_CustomerType();

        //
        // geting data
        //
        $booking = $modelBooking->getById($bookingId);
        $modelBooking->fill($booking, array('property_type'));

        //
        // validation
        //
        if (!$booking) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Booking not found"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        //check in can see his or assigned bookings
        if (!$modelBooking->checkIfCanSeeBooking($bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this booking"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        // check if the customer must have work order
        $customer = $modelCustomer->getById($booking['customer_id']);
        $customerType = $modelCustomerType->getById($booking['customer_id']);
        if (!empty($customerType['is_work_order'])) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This customer dosen't have a work order"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // filling extra data
        //
        $user = $modelUser->getById($booking['created_by']);

        $viewParam = $this->getBookingViewParam($bookingId, true);
        $view = new Zend_View();
        $view->setScriptPath(APPLICATION_PATH . '/modules/booking/views/scripts/index');
        $view->booking = $viewParam['booking'];
        $view->bookingServices = $viewParam['bookingServices'];
        $view->thisBookingServices = $viewParam['thisBookingServices'];
        $view->priceArray = $viewParam['priceArray'];
        $bodyBooking = $view->render('booking-customer.phtml');

        $template_params = array(
            //booking
            '{booking_num}' => $booking['booking_num'],
            '{total_without_tax}' => number_format($booking['sub_total'], 2),
            '{gst_tax}' => number_format($booking['gst'], 2),
            '{total_with_tax}' => number_format($booking['qoute'], 2),
            '{description}' => $booking['description'] ? $booking['description'] : '',
            '{booking_created}' => date('d/m/Y', $booking['created']),
            '{booking_created_by}' => ucwords($user['username']),
            '{booking_start}' => date("F j, Y, g:i a", strtotime($booking['booking_start'])),
            '{booking_address}' => get_line_address($modelBookingAddress->getByBookingId($booking['booking_id'])),
            '{service}' => nl2br($modelContractorServiceBooking->getBookingAsText($booking['booking_id'], true)),
            '{property}' => $booking['property_type'],
            '{booking_view}' => $bodyBooking,
            //customer
            '{customer_name}' => get_customer_name($customer),
            '{customer_first_name}' => isset($customer['first_name']) && $customer['first_name'] ? ucwords($customer['first_name']) : '',
            '{customer_last_name}' => isset($customer['last_name']) && $customer['last_name'] ? ' ' . ucwords($customer['last_name']) : '',
            '{customer_contacts}' => nl2br($modelCustomer->getCustomerContacts($booking['customer_id'])),
        );

        $modelEmailTemplate = new Model_EmailTemplate();
        $emailTemplate = $modelEmailTemplate->getEmailTemplate('request_work_order_email', $template_params);

        $body = $emailTemplate['body'];
        $subject = $emailTemplate['subject'];
        $to = array();
        if ($customer['email1']) {
            $to[] = $customer['email1'];
        }
        if ($customer['email2']) {
            $to[] = $customer['email2'];
        }
        if ($customer['email2']) {
            $to[] = $customer['email3'];
        }
        $to = implode(',', $to);
        $modelAttributeListValueAttachment = new Model_AttributeListValueAttachment();
        $modelServiceAttributeValue = new Model_ServiceAttributeValue();
        $serviceAttributeValues = $modelServiceAttributeValue->getByBookingIdListAttributeValues($bookingId);



        $AttributeListValueAttachments = array();
        foreach ($serviceAttributeValues as $serviceAttributeValue) {
            $listAttachment = $modelAttributeListValueAttachment->getByAttributeValueId($serviceAttributeValue['value']);
            if ($listAttachment) {
                $AttributeListValueAttachments[] = array('attachments' => $listAttachment, 'attribute_name' => $serviceAttributeValue['attribute_name']);
            }
        }

        if ($this->request->isPost()) {
            $to = $this->request->getParam('to');
            $cc = $this->request->getParam('cc');
            $subject = $this->request->getParam('subject');
            $body = $this->request->getParam('body');
            $pdf_attachment = $this->request->getParam('pdf_attachment', 0);
            $selected_attachments = $this->request->getParam('attachment', array());

            $trading_namesObj = new Model_TradingName();
            $trading_names = $trading_namesObj->getById($booking['trading_name_id']);


            $params = array(
                'to' => $to,
                'cc' => $cc,
                'body' => $body,
                'subject' => $subject,
                'from' => $trading_names['email'],
                'trading_name' => $trading_names['trading_name'],
            );

            $error_mesages = array();
            $modelAttachment = new Model_Attachment();
            if (EmailNotification::validation($params, $error_mesages)) {
                $attachments = array();
                if (!empty($selected_attachments)) {
                    foreach ($selected_attachments as $attachment_id) {
                        $attachment = $modelAttachment->getById($attachment_id);
                        $attachments[] = $attachment['path'];
                    }
                }

                if (!empty($pdf_attachment)) {
                    // Create pdf
                    $pdfPath = createPdfPath();
                    $destination = $pdfPath['fullDir'] . $viewParam['booking']['booking_num'] . '.pdf';
                    wkhtmltopdf($bodyBooking, $destination);
                    $attachments[] = $destination;
                    //$params['attachment'] = $destination;
                }
                if (!empty($pdf_attachment) || !empty($selected_attachments)) {
                    $attachments = implode(",", $attachments);
                    $params['attachment'] = $attachments;
                }





                // Send Email
                $success = EmailNotification::sendEmail($params, '', array(), array('reference_id' => $booking['booking_id'], 'type' => 'booking'));

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Email sent"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Failed to send email"));
                }

                echo 1;
                exit;
            }
            $this->view->error_mesages = $error_mesages;
        }

        $this->view->booking = $booking;
        $this->view->to = $to;
        $this->view->subject = $subject;
        $this->view->body = $body;
        $this->view->cc = isset($cc) ? $cc : '';
        $this->view->AttributeListValueAttachments = $AttributeListValueAttachments;

        echo $this->view->render('index/send-work-order-request-email.phtml');
        exit;
    }

    public function downloadBookingAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('downloadBooking'));

        //
        // get params 
        //
        $bookingId = $this->request->getParam('id', 0);

        if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this page"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // load models
        //
        $modelBooking = new Model_Booking();

        //
        // geting data
        //
        $booking = $modelBooking->getById($bookingId);

        //
        // validation
        //
        if (!$booking) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Booking not found"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        //check in can see his or assigned bookings
        if (!$modelBooking->checkIfCanSeeBooking($bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this booking"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $viewParam = $this->getBookingViewParam($bookingId, true);

        $view = new Zend_View();
        $view->setScriptPath(APPLICATION_PATH . '/modules/booking/views/scripts/index');

        $view->booking = $viewParam['booking'];
        $view->bookingServices = $viewParam['bookingServices'];
        $view->thisBookingServices = $viewParam['thisBookingServices'];
        $view->priceArray = $viewParam['priceArray'];

        $bodyBooking = $view->render('booking-customer.phtml');

        $pdfPath = createPdfPath();

        $filename = $pdfPath['fullDir'] . $viewParam['booking']['booking_num'] . '.pdf';

        wkhtmltopdf($bodyBooking, $filename);


        header("Pragma: public");
        header("Expires: 0");
        header("Pragma: no-cache");
        header("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header('Content-disposition: attachment; filename=' . basename($filename));
        header("Content-Type: application/pdf");
        header("Content-Transfer-Encoding: binary");
        header('Content-Length: ' . filesize($filename));
        @readfile($filename);
        exit(0);
    }

    public function downloadContractorBookingAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('downloadContractorBooking'));

        //
        // get params 
        //
        $bookingId = $this->request->getParam('id', 0);

        if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this page"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // load models
        //
        $modelBooking = new Model_Booking();

        //
        // geting data
        //
        $booking = $modelBooking->getById($bookingId);

        //
        // validation
        //
        if (!$booking) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Booking not found"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        //check in can see his or assigned bookings
        if (!$modelBooking->checkIfCanSeeBooking($bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this booking"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $viewParam = $this->getBookingViewParam($bookingId, true);

        $view = new Zend_View();
        $view->setScriptPath(APPLICATION_PATH . '/modules/booking/views/scripts/index');

        $view->booking = $viewParam['booking'];
        $view->bookingServices = $viewParam['bookingServices'];
        $view->thisBookingServices = $viewParam['thisBookingServices'];
        $view->priceArray = $viewParam['priceArray'];

        $bodyBooking = $view->render('booking.phtml');

        $pdfPath = createPdfPath();

        $filename = $pdfPath['fullDir'] . $viewParam['booking']['booking_num'] . '.pdf';

        wkhtmltopdf($bodyBooking, $filename);


        header("Pragma: public");
        header("Expires: 0");
        header("Pragma: no-cache");
        header("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header('Content-disposition: attachment; filename=' . basename($filename));
        header("Content-Type: application/pdf");
        header("Content-Transfer-Encoding: binary");
        header('Content-Length: ' . filesize($filename));
        @readfile($filename);
        exit(0);
    }

    public function sendBookingAsSmsAction() {

        //
        // get params 
        //
        $bookingId = $this->request->getParam('id', 0);

        if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this page"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // load models
        //
        $modelUser = new Model_User();
        $modelBooking = new Model_Booking();
        $modelCustomer = new Model_Customer();
        $modelBookingAddress = new Model_BookingAddress();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();

        //
        // geting data
        //
        $booking = $modelBooking->getById($bookingId);
        $modelBooking->fill($booking, array('property_type'));

        //
        // validation
        //
        if (!$booking) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Booking not found"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        //check in can see his or assigned bookings
        if (!$modelBooking->checkIfCanSeeBooking($bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this booking"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // filling extra data
        //
        $customer = $modelCustomer->getById($booking['customer_id']);
        $user = $modelUser->getById($booking['created_by']);

        $template_params = array(
            //booking
            '{booking_num}' => $booking['booking_num'],
            '{total_without_tax}' => number_format($booking['sub_total'], 2),
            '{gst_tax}' => number_format($booking['gst'], 2),
            '{total_with_tax}' => number_format($booking['qoute'], 2),
            '{description}' => $booking['description'] ? $booking['description'] : '',
            '{booking_created}' => date('d/m/Y', $booking['created']),
            '{booking_created_by}' => ucwords($user['username']),
            '{booking_start}' => date("F j, Y, g:i a", strtotime($booking['booking_start'])),
            '{booking_address}' => get_line_address($modelBookingAddress->getByBookingId($booking['booking_id'])),
            '{service}' => nl2br($modelContractorServiceBooking->getBookingAsText($booking['booking_id'])),
            '{property}' => $booking['property_type'],
            //customer
            '{customer_name}' => get_customer_name($customer),
            '{customer_first_name}' => isset($customer['first_name']) && $customer['first_name'] ? ucwords($customer['first_name']) : '',
            '{customer_last_name}' => isset($customer['last_name']) && $customer['last_name'] ? ' ' . ucwords($customer['last_name']) : '',
            '{customer_contacts}' => nl2br($modelCustomer->getCustomerContacts($booking['customer_id'])),
        );

        $modelEmailTemplate = new Model_EmailTemplate();
        $emailTemplate = $modelEmailTemplate->getEmailTemplate('send_booking_as_sms', $template_params);

        $body = $emailTemplate['body'];

        $this->view->booking = $booking;
        $this->view->body = $body;

        echo $this->view->render('index/send-booking-as-sms.phtml');
        exit;
    }

    public function sendBookingAsSmsToContractorAction() {

        //
        // get params 
        //
        $bookingId = $this->request->getParam('id', 0);

        if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this page"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // load models
        //
        $modelUser = new Model_User();
        $modelBooking = new Model_Booking();
        $modelCustomer = new Model_Customer();
        $modelBookingAddress = new Model_BookingAddress();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();

        //
        // geting data
        //
        $booking = $modelBooking->getById($bookingId);
        $modelBooking->fill($booking, array('property_type'));

        //
        // validation
        //
        if (!$booking) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Booking not found"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        //check in can see his or assigned bookings
        if (!$modelBooking->checkIfCanSeeBooking($bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this booking"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // filling extra data
        //
        $customer = $modelCustomer->getById($booking['customer_id']);
        $user = $modelUser->getById($booking['created_by']);

        $template_params = array(
            //booking
            '{booking_num}' => $booking['booking_num'],
            '{total_without_tax}' => number_format($booking['sub_total'], 2),
            '{gst_tax}' => number_format($booking['gst'], 2),
            '{total_with_tax}' => number_format($booking['qoute'], 2),
            '{description}' => $booking['description'] ? $booking['description'] : '',
            '{booking_created}' => date('d/m/Y', $booking['created']),
            '{booking_created_by}' => ucwords($user['username']),
            '{booking_start}' => date("F j, Y, g:i a", strtotime($booking['booking_start'])),
            '{booking_address}' => get_line_address($modelBookingAddress->getByBookingId($booking['booking_id'])),
            '{service}' => nl2br($modelContractorServiceBooking->getBookingAsText($booking['booking_id'])),
            '{property}' => $booking['property_type'],
            //customer
            '{customer_name}' => get_customer_name($customer),
            '{customer_first_name}' => isset($customer['first_name']) && $customer['first_name'] ? ucwords($customer['first_name']) : '',
            '{customer_last_name}' => isset($customer['last_name']) && $customer['last_name'] ? ' ' . ucwords($customer['last_name']) : '',
            '{customer_contacts}' => nl2br($modelCustomer->getCustomerContacts($booking['customer_id'])),
        );

        $modelEmailTemplate = new Model_EmailTemplate();
        $emailTemplate = $modelEmailTemplate->getEmailTemplate('send_booking_as_sms_to_contractor', $template_params);

        $body = $emailTemplate['body'];

        $this->view->booking = $booking;
        $this->view->body = $body;

        echo $this->view->render('index/send-booking-as-sms-to-contractor.phtml');
        exit;
    }

    public function getBookingViewParam($bookingId, $toBuffer = false) {

        //
        // load models
        //
        $modelBooking = new Model_Booking();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();

        //
        // geting data
        //
        
        $booking = $modelBooking->getById($bookingId);
        //
        // validation
        //
        if (!$booking) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Booking not found"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        //check in can see his or assigned bookings
        if (!$modelBooking->checkIfCanSeeBooking($bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this booking"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        // fill data
        $modelBooking->fill($booking, array('contractors', 'name_contractors', 'address', 'customer', 'city', 'labels', 'status', 'is_accepted', 'can_delete', 'not_accepted_or_rejected', 'discussion', 'complaint', 'email_log', 'customer_commercial_info', 'customer_contacts'));

        $bookingServices = $modelContractorServiceBooking->getByBookingId($bookingId);
        // $thisBookingServices : to put all service for this booking 
        $thisBookingServices = array();
        // $priceArray : to put all price and service for this booking 
        $priceArray = array();

        if ($bookingServices) {
            foreach ($bookingServices as $bookingService) {

                $serviceId = $bookingService['service_id'];
                $clone = $bookingService['clone'];
                $bookingId = $bookingService['booking_id'];

                $service_and_clone = $serviceId . ($clone ? '_' . $clone : '');

                $thisBookingServices[] = $service_and_clone;

                $priceArray[$service_and_clone] = $modelContractorServiceBooking->getTotalServiceBookingQoute($bookingId, $serviceId, $clone);
            }
        }

        if (!$toBuffer) {
            $this->view->booking = $booking;
            $this->view->bookingServices = $bookingServices;
            $this->view->thisBookingServices = $thisBookingServices;
            $this->view->priceArray = $priceArray;
        } else {
            $viewParam = array();

            $viewParam['booking'] = $booking;
            $viewParam['bookingServices'] = $bookingServices;
            $viewParam['thisBookingServices'] = $thisBookingServices;
            $viewParam['priceArray'] = $priceArray;

            return $viewParam;
        }
        return false;
    }

    public function getAwaitingUpdateBookingAction() {


        //check Auth for logged user
        CheckAuth::checkPermission(array('booking'));

        //get request parameters
        $orderBy = $this->request->getParam('sort', 'booking_id');
        $sortingMethod = $this->request->getParam('method', 'desc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array());
        $is_first_time = $this->request->getParam('is_first_time');
        $page_number = $this->request->getParam('page_number');
        // $isDeleted = $this->request->getParam('is_deleted', 0);
        // $this->view->isDeleted = $isDeleted;
        //Load Model
        $modelBookingStatus = new Model_BookingStatus();
        $modelBooking = new Model_Booking();


        $awaitingUpdateStatus = $modelBookingStatus->getByStatusName('AWAITING UPDATE');
        $filters['status'] = $awaitingUpdateStatus['booking_status_id'];

        //init pager and articles model object

        /* $pager = new Model_Pager();
          $pager->perPage = get_config('perPage');
          $pager->currentPage = $currentPage;
          $pager->url = $_SERVER['REQUEST_URI']; */


        //get data list
        if ($this->request->isPost()) {

            if (isset($page_number)) {
                $perPage = 15;
                $currentPage = $page_number + 1;
            }

            $data = $modelBooking->getAll($filters, "{$orderBy} {$sortingMethod}", $pager, 0, $perPage, $currentPage);
            $modelBooking->fills($data, array('contractors', 'customer', 'city', 'labels', 'status', 'services', 'is_accepted', 'can_delete', 'not_accepted_or_rejected', 'not_accepted', 'booking_users', 'multiple_days'));
            $this->view->data = $data;
            $this->view->filters = $filters;
            $this->view->is_first_time = $is_first_time;
            echo $this->view->render('index/awaiting-update-draw-node.phtml');
            exit;
        }



        //set view params
        //$this->view->data = $data;
        //$this->view->currentPage = $currentPage;
        //$this->view->perPage = $pager->perPage;
        //$this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
    }

    public function getAwaitingAcceptBookingAction() {


        //check Auth for logged user
        CheckAuth::checkPermission(array('booking'));

        //get request parameters
        $orderBy = $this->request->getParam('sort', 'booking_id');
        $sortingMethod = $this->request->getParam('method', 'desc');
        $currentPage = $this->request->getParam('page', 1);
        // $filters = $this->request->getParam('fltr', array('convert_status' => 'booking'));
        // $isDeleted = $this->request->getParam('is_deleted', 0);
        // $this->view->isDeleted = $isDeleted;
        //Load Model
        $modelBookingStatus = new Model_BookingStatus();
        $modelBooking = new Model_Booking();
        $modelAuthRole = new Model_AuthRole();

        //get logged user 
        $loggedUser = CheckAuth::getLoggedUser();
        $loggedUserId = $loggedUser['user_id'];
        $contractorRoleId = $modelAuthRole->getRoleIdByName('contractor');

        //$awaitingUpdateStatus = $modelBookingStatus->getByStatusName('AWAITING UPDATE');
        //$filters = array('acceptance' => 'not_accepted_or_rejected');
        $filters = $this->request->getParam('fltr', array());
        $filters['acceptance'] = 'not_accepted_or_rejected';
        $filters['booking_not_started_yet'] = 'yes';
        $filters['convert_status'] = 'booking';
		$filters['exclude_status']= 'CANCELLED';
        if ($contractorRoleId == $loggedUser['role_id']) {
            $filters['contractor_id'] = $loggedUser['user_id'];
        }
		

        //init pager and articles model object
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];


        //get data list
        $data = $modelBooking->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);
        $modelBooking->fills($data, array('contractors', 'customer', 'city', 'labels', 'status', 'services', 'is_accepted', 'can_delete', 'not_accepted_or_rejected', 'not_accepted', 'booking_users', 'multiple_days'));

        //set view params
        $this->view->data = $data;
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
    }

    public function defultPageAction() {

        $this->view->main_menu = '';
        $this->view->sub_menu = '';
        //check Auth for logged user
        CheckAuth::checkPermission(array('booking'));

        //get request parameters
        $contractorId = $this->request->getParam('contractor_id', 0);

        //Load Model
        $modelBooking = new Model_Booking();
        $modelAuthRole = new Model_AuthRole();


        $loggedUser = CheckAuth::getLoggedUser();
        $loggedUserId = $loggedUser['user_id'];
        $contractorRoleId = $modelAuthRole->getRoleIdByName('contractor');
        $data = array();
        if ($contractorId == 0 && $loggedUser['role_id'] == $contractorRoleId) {
            $contractorId = $loggedUser['user_id'];
        }

        $data = $modelBooking->getUrgentBookingByContractorId($contractorId);
        $modelBooking->fills($data, array('contractors', 'customer', 'city', 'labels', 'status', 'services', 'is_accepted', 'can_delete', 'not_accepted_or_rejected', 'not_accepted', 'booking_users'));

        //set view params
        $this->view->data = $data;
    }
	
	function setContractorServiceShareByContractorShareAction(){
	  
	  $contractor_share = $this->request->getParam('contractor_share', 0);
	  $old_contractor_share = $this->request->getParam('old_contractor_share', 0);
	  $old_contractor_share = str_replace(',', '', $old_contractor_share);
	  $bookingId = $this->request->getParam('bookingId', 0);
	  $contractorId = $this->request->getParam('contractorId', 0);
	  $modelContractorServiceBooking = new Model_ContractorServiceBooking();
	  $modelContractorInfo = new Model_ContractorInfo();
	  $modelBooking = new Model_Booking();

	  
	  $booking = $modelBooking->getById($bookingId);
	  $contractorInfo = $modelContractorInfo->getByContractorId($contractorId);
	  $contractorServices = $modelContractorServiceBooking->getByBookingIdAndContractorId($bookingId, $contractorId);
	  $bookingQoute = $modelContractorServiceBooking->getTotalBookingQoute($bookingId);
	  $servicesQoute = $modelContractorServiceBooking->getTotalContractorServicesQoute($bookingId, $contractorId);
      $contractorShareWithoutTax = $servicesQoute * $contractorInfo['commission'] / 100;
	  $contractorDiscount = 0;
      if ($bookingQoute) {
        $contractorDiscount = ($contractorShareWithoutTax / $bookingQoute) * $booking['total_discount'];
       }
	   
	 
	   
	  $servicesShare = array();	
      $prev_service_id = 0;
      $count = 1;
      $serviceCommission = 0;	  
	   foreach($contractorServices as $key=>$contractorService){
			 $serviceQuote =  $modelContractorServiceBooking->getTotalServiceBookingQoute($bookingId, $contractorService['service_id'], $contractorService['clone']);
			 $modelContractorService = new Model_ContractorService();
             $contractorService = $modelContractorService->getByContractorIdAndServiceId($contractorId,$contractorService['service_id']);
			  if(isset($contractorService['commission']) && !empty($contractorService['commission'])){
			   $service_commission = $contractorService['commission'];
               $serviceCommission = 1;			   
			   }else{
			    $service_commission = $contractorInfo['commission'];
			   }
			   
			   //$service_commission = $contractorInfo['commission'];
			   $serviceShareWithoutTax = $serviceQuote * $service_commission / 100;
			
			
			    //echo $serviceQuote.' '.$service_commission.' '.$serviceShareWithoutTax;
				//echo '<br/>';
			 
			 
			 // the discount must be without taxes
            $discount = 0;
			$old_serviceShare = $serviceShareWithoutTax;
            if ($bookingQoute) {
                $discount = ($serviceShareWithoutTax / $contractorShareWithoutTax) * $contractorDiscount;
            }
			
			
			
			
			
			// if the contractor pay taxes
            if ($contractorInfo['gst']) {
                $serviceQouteWithTax = ($serviceQuote - $discount) * (1 + get_config('gst_tax'));
                $old_serviceShare = $serviceQouteWithTax * $service_commission / 100;				
            } else {
                $old_serviceShare = $old_serviceShare - $discount;
            }
			
			
			  
			 $new_serviceShare = ($old_serviceShare * $contractor_share) / $old_contractor_share;
			 if($prev_service_id == $contractorService['service_id']){
			  $serviceId = $contractorService['service_id'].'_'.$count;
			  $count = $count +1;
			 }else{
			  $serviceId = $contractorService['service_id'];
			 }
			 //$servicesShare[$serviceId] = $new_serviceShare;
			 $new_serviceShare = (float)$new_serviceShare;
			 $servicesShare[$serviceId] = round($new_serviceShare, 2);
			 $prev_service_id = $contractorService['service_id'];
			 
			

	    }
		
		$totalServiceShare = 0;
		foreach($servicesShare as $serviceShare){
		    $totalServiceShare = $totalServiceShare + $serviceShare;
		}
		if($totalServiceShare != $contractor_share){
		  if(!($serviceCommission)){
		    $diff = abs($totalServiceShare - $contractor_share);
		    $servicesShare[$prev_service_id] =  $servicesShare[$prev_service_id] + $diff;
		  }
		    $contractor_share = $totalServiceShare;
		  }
		
		echo json_encode(array('services_share' => $servicesShare,'contractor_share'=>$contractor_share));
		exit;

	}
	
	
	    public function setContractorSharePerServiceAction() {

        //
        //CheckAuth
        //
        CheckAuth::checkPermission(array('setContractorShare'));
        // get params 
        $bookingId = $this->request->getParam('id', 0);
        $contractor_shares = $this->request->getParam('contractor_shares', array());

        // load models
        $modelBooking = new Model_Booking();
        $modelContractorInfo = new Model_ContractorInfo();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelUser = new Model_User();
        $modelContractorShareBooking = new Model_ContractorShareBooking();
        $modelRefund = new Model_Refund();

        // get booking
        $booking = $modelBooking->getById($bookingId);

        // validation
        if (!$booking) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Booking not exist"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //get total refund
        $totalRefund = $modelRefund->getTotalRefund(array('booking_id' => $bookingId, 'is_approved' => 'yes'));

        // get extra info
        $contractorServicesBooking = $modelContractorServiceBooking->getByBookingId($bookingId);
        $bookingQoute = $modelContractorServiceBooking->getTotalBookingQoute($bookingId);
   

        $contractors = array();
        foreach ($contractorServicesBooking as $contractorServiceBooking) {
            $contractors[$contractorServiceBooking['contractor_id']] = array('contractor_id' => $contractorServiceBooking['contractor_id']);
        }
		

        foreach ($contractors as $contractorId => &$contractor) {

            $user = $modelUser->getById($contractorId);
            $contractorInfo = $modelContractorInfo->getByContractorId($contractorId);
            $servicesQoute = $modelContractorServiceBooking->getTotalContractorServicesQoute($bookingId, $contractorId);
            $contractorShareWithoutTax = $servicesQoute * $contractorInfo['commission'] / 100;
            $contractorShare = $contractorShareWithoutTax;
			$contractorServices = $modelContractorServiceBooking->getByBookingIdAndContractorId($bookingId, $contractorId);
			
			/*foreach($contractorServices as $key=>$contractorService){
			 $contractorServices[$key]['service_qoute'] = $modelContractorServiceBooking->getTotalServiceBookingQoute($bookingId, $contractorService['service_id'], $contractorService['clone']);
			}*/


            // the discount must be without taxes
            $discount = 0;
            $refund = 0;
            if ($bookingQoute) {
                $discount = ($contractorShareWithoutTax / $bookingQoute) * $booking['total_discount'];
                $refund = ($contractorShareWithoutTax / $bookingQoute) * $totalRefund;
            }

            // if the contractor pay taxes
            if ($contractorInfo['gst']) {
                $servicesQouteWithTax = ($servicesQoute - $discount) * (1 + get_config('gst_tax'));
                $contractor['services_qoute_with_tax'] = $servicesQouteWithTax;
                $contractorShare = $servicesQouteWithTax * $contractorInfo['commission'] / 100;
                $contractor['have_gst'] = $contractorInfo['gst'];
            } else {
                $contractorShare = $contractorShare - $discount;
            }


            $oldContractorShareBooking = $modelContractorShareBooking->getByBookingIdAndContractorId($bookingId, $contractorId);
            $contractor['old_contractor_share'] = !empty($oldContractorShareBooking['contractor_share']) ? $oldContractorShareBooking['contractor_share'] : '';
            $contractor['contractor_name'] = $user['username'];
            $contractor['total_contractor_share'] = $contractorShare - $refund;
			$contractor['contractor_services'] = $contractorServices;
        }

        if ($this->request->isPost()) { // check if POST request method
		
            foreach ($contractor_shares as $contractor_id => $shareValue) {
			  $contractorServices = $modelContractorServiceBooking->getByBookingIdAndContractorId($bookingId, $contractor_id);
			  $prev_serviceId = 0;
			  $count=1;
			  foreach($contractorServices as $contractorService){
               if($prev_serviceId == $contractorService['service_id']){
			     $service_shares = $this->request->getParam('contractor_share_service_'.$contractorService['service_id'].'_'.$count.'_'.$contractor_id.'', 0);
				 $count = $count + 1;
			   }else{
			     $service_shares = $this->request->getParam('contractor_share_service_'.$contractorService['service_id'].'_'.$contractor_id.'', 0);
			   }		  			   		
			   $service_shares_data = array('contractor_share'=>$service_shares);
			   $modelContractorServiceBooking->updateById($contractorService['id'],$service_shares_data);
			   $prev_serviceId  = $contractorService['service_id'];
			  }

                $data = array(
                    'booking_id' => $bookingId,
                    'contractor_id' => $contractor_id,
                    'contractor_share' => str_replace(",", "", $shareValue)
                );

                $contractorShareBooking = $modelContractorShareBooking->getByBookingIdAndContractorId($bookingId, $contractor_id);

                if ($contractorShareBooking) {
                    $modelContractorShareBooking->updateById($contractorShareBooking['id'], $data);
                } else {
                    $modelContractorShareBooking->insert($data);
                }
            }
            echo json_encode(array('msg' => '1'));
            //echo 1;
            exit;
        } 

        $this->view->booking_id = $bookingId;
        $this->view->booking = $booking;
        $this->view->contractors = $contractors;
        echo $this->view->render('index/set-contractor-share-per-service.phtml');
        exit;
    }
   public function setContractorShareAction() {

        //
        //CheckAuth
        //
        CheckAuth::checkPermission(array('setContractorShare'));

        // get params 
        $bookingId = $this->request->getParam('id', 0);
        $flag = $this->request->getParam('flag', 0);
        $contractor_shares = $this->request->getParam('contractor_shares', array());

        // load models
        $modelBooking = new Model_Booking();
        $modelContractorInfo = new Model_ContractorInfo();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelUser = new Model_User();
        $modelContractorShareBooking = new Model_ContractorShareBooking();
        $modelRefund = new Model_Refund();

        // get booking
        $booking = $modelBooking->getById($bookingId);

        // validation
        if (!$booking) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Booking not found"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //get total refund
        $totalRefund = $modelRefund->getTotalRefund(array('booking_id' => $bookingId, 'is_approved' => 'yes'));

        // get extra info
        $contractorServicesBooking = $modelContractorServiceBooking->getByBookingId($bookingId);
        $bookingQoute = $modelContractorServiceBooking->getTotalBookingQoute($bookingId);
        $totalWithoutTax = $bookingQoute - $booking['total_discount'] - $totalRefund;
        $total = $totalWithoutTax + $booking['gst'];

        $contractors = array();
        foreach ($contractorServicesBooking as $contractorServiceBooking) {
            $contractors[$contractorServiceBooking['contractor_id']] = array('contractor_id' => $contractorServiceBooking['contractor_id']);
        }

        foreach ($contractors as $contractorId => &$contractor) {

            $user = $modelUser->getById($contractorId);
            $contractorInfo = $modelContractorInfo->getByContractorId($contractorId);
            $servicesQoute = $modelContractorServiceBooking->getTotalContractorServicesQoute($bookingId, $contractorId);
            $contractorShareWithoutTax = $servicesQoute * $contractorInfo['commission'] / 100;
            $contractorShare = $contractorShareWithoutTax;
			$contractorServices = $modelContractorServiceBooking->getByBookingIdAndContractorId($bookingId, $contractorId);
			
			/*foreach($contractorServices as $key=>$contractorService){
			 $contractorServices[$key]['service_qoute'] = $modelContractorServiceBooking->getTotalServiceBookingQoute($bookingId, $contractorService['service_id'], $contractorService['clone']);
			}*/


            // the discount must be without taxes
            $discount = 0;
            $refund = 0;
            if ($bookingQoute) {
                $discount = ($contractorShareWithoutTax / $bookingQoute) * $booking['total_discount'];
                $refund = ($contractorShareWithoutTax / $bookingQoute) * $totalRefund;
            }

            // if the contractor pay taxes
            if ($contractorInfo['gst']) {
                $servicesQouteWithTax = ($servicesQoute - $discount) * (1 + get_config('gst_tax'));
                $contractor['services_qoute_with_tax'] = $servicesQouteWithTax;
                $contractorShare = $servicesQouteWithTax * $contractorInfo['commission'] / 100;
                $contractor['have_gst'] = $contractorInfo['gst'];
            } else {
                $contractorShare = $contractorShare - $discount;
            }


            $oldContractorShareBooking = $modelContractorShareBooking->getByBookingIdAndContractorId($bookingId, $contractorId);
            $contractor['old_contractor_share'] = !empty($oldContractorShareBooking['contractor_share']) ? $oldContractorShareBooking['contractor_share'] : '';
            $contractor['commission'] = $contractorInfo['commission'];
            $contractor['contractor_name'] = $user['username'];
            $contractor['services_qoute'] = $servicesQoute;
            $contractor['discount'] = $discount;
            $contractor['refund'] = $refund;
            $contractor['contractor_share'] = $contractorShare;
            $contractor['total_contractor_share'] = $contractorShare - $refund;
			$contractor['contractor_services'] = $contractorServices;
        }

        if ($this->request->isPost() && $flag == 0) { // check if POST request method
            foreach ($contractor_shares as $contractor_id => $shareValue) {
			  $contractorServices = $modelContractorServiceBooking->getByBookingIdAndContractorId($bookingId, $contractor_id);
			  $prev_serviceId = 0;
			  $count=1;
			  foreach($contractorServices as $contractorService){
			  if($prev_serviceId == $contractorService['service_id']){
			     $service_shares = $this->request->getParam('contractor_share_service_'.$contractorService['service_id'].'_'.$count.'_'.$contractor_id.'', 0);
				 $count = $count + 1;
			   }else{
			     $service_shares = $this->request->getParam('contractor_share_service_'.$contractorService['service_id'].'_'.$contractor_id.'', 0);
			   }		 
			   
			   $service_shares_data = array('contractor_share'=>$service_shares);
			   $prev_serviceId  = $contractorService['service_id'];
			   $modelContractorServiceBooking->updateById($contractorService['id'],$service_shares_data);
			  }

                $data = array(
                    'booking_id' => $bookingId,
                    'contractor_id' => $contractor_id,
                    'contractor_share' => str_replace(",", "", $shareValue)
                );

                $contractorShareBooking = $modelContractorShareBooking->getByBookingIdAndContractorId($bookingId, $contractor_id);

                if ($contractorShareBooking) {
                    $modelContractorShareBooking->updateById($contractorShareBooking['id'], $data);
                } else {
                    $modelContractorShareBooking->insert($data);
                }
            }
            echo json_encode(array('msg' => '1'));
            //echo 1;
            exit;
        } else if ($this->request->isPost() && $flag == 1) { // check if POST request method
            foreach ($contractor_shares as $contractor_id => $shareValue) {
			  $contractorServices = $modelContractorServiceBooking->getByBookingIdAndContractorId($bookingId, $contractor_id);
			  $prev_serviceId = 0;
			  $count=1;
			  foreach($contractorServices as $contractorService){
			  if($prev_serviceId == $contractorService['service_id']){
			     $service_shares = $this->request->getParam('contractor_share_service_'.$contractorService['service_id'].'_'.$count.'_'.$contractor_id.'', 0);
				 $count = $count + 1;
			   }else{
			     $service_shares = $this->request->getParam('contractor_share_service_'.$contractorService['service_id'].'_'.$contractor_id.'', 0);
			   }		 
			   
			   $service_shares_data = array('contractor_share'=>$service_shares);
			   $prev_serviceId  = $contractorService['service_id'];
			   $modelContractorServiceBooking->updateById($contractorService['id'],$service_shares_data);
			  }

                $data = array(
                    'booking_id' => $bookingId,
                    'contractor_id' => $contractor_id,
                    'contractor_share' => $shareValue
                );

                $contractorShareBooking = $modelContractorShareBooking->getByBookingIdAndContractorId($bookingId, $contractor_id);

                if ($contractorShareBooking) {
                    $modelContractorShareBooking->updateById($contractorShareBooking['id'], $data);
                } else {
                    $modelContractorShareBooking->insert($data);
                }
            }
            echo json_encode(array('msg' => 'done', 'booking' => $data));
            //echo 'done';
            exit;
        }

        $this->view->booking_id = $bookingId;
        $this->view->bookingQoute = $bookingQoute;
        $this->view->booking = $booking;
        $this->view->contractors = $contractors;
        $this->view->totalWithoutTax = $totalWithoutTax;
        $this->view->total = $total;
        $this->view->totalRefund = $totalRefund;
        $this->view->flag = $flag;
        echo $this->view->render('index/set-contractor-share.phtml');
        exit;
    }

   /* public function setContractorShareAction() {

        //
        //CheckAuth
        //
        CheckAuth::checkPermission(array('setContractorShare'));

        // get params 
        $bookingId = $this->request->getParam('id', 0);
        $flag = $this->request->getParam('flag', 0);
        $contractor_shares = $this->request->getParam('contractor_shares', array());

        // load models
        $modelBooking = new Model_Booking();
        $modelContractorInfo = new Model_ContractorInfo();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelUser = new Model_User();
        $modelContractorShareBooking = new Model_ContractorShareBooking();
        $modelRefund = new Model_Refund();

        // get booking
        $booking = $modelBooking->getById($bookingId);

        // validation
        if (!$booking) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Booking not found"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //get total refund
        $totalRefund = $modelRefund->getTotalRefund(array('booking_id' => $bookingId, 'is_approved' => 'yes'));

        // get extra info
        $contractorServicesBooking = $modelContractorServiceBooking->getByBookingId($bookingId);
        $bookingQoute = $modelContractorServiceBooking->getTotalBookingQoute($bookingId);
        $totalWithoutTax = $bookingQoute - $booking['total_discount'] - $totalRefund;
        $total = $totalWithoutTax + $booking['gst'];

        $contractors = array();
        foreach ($contractorServicesBooking as $contractorServiceBooking) {
            $contractors[$contractorServiceBooking['contractor_id']] = array('contractor_id' => $contractorServiceBooking['contractor_id']);
        }

        foreach ($contractors as $contractorId => &$contractor) {

            $user = $modelUser->getById($contractorId);
            $contractorInfo = $modelContractorInfo->getByContractorId($contractorId);
            $servicesQoute = $modelContractorServiceBooking->getTotalContractorServicesQoute($bookingId, $contractorId);
            $contractorShareWithoutTax = $servicesQoute * $contractorInfo['commission'] / 100;
            $contractorShare = $contractorShareWithoutTax;


            // the discount must be without taxes
            $discount = 0;
            $refund = 0;
            if ($bookingQoute) {
                $discount = ($contractorShareWithoutTax / $bookingQoute) * $booking['total_discount'];
                $refund = ($contractorShareWithoutTax / $bookingQoute) * $totalRefund;
            }

            // if the contractor pay taxes
            if ($contractorInfo['gst']) {
                $servicesQouteWithTax = ($servicesQoute - $discount) * (1 + get_config('gst_tax'));
                $contractor['services_qoute_with_tax'] = $servicesQouteWithTax;
                $contractorShare = $servicesQouteWithTax * $contractorInfo['commission'] / 100;
                $contractor['have_gst'] = $contractorInfo['gst'];
            } else {
                $contractorShare = $contractorShare - $discount;
            }


            $oldContractorShareBooking = $modelContractorShareBooking->getByBookingIdAndContractorId($bookingId, $contractorId);
            $contractor['old_contractor_share'] = !empty($oldContractorShareBooking['contractor_share']) ? $oldContractorShareBooking['contractor_share'] : '';
            $contractor['commission'] = $contractorInfo['commission'];
            $contractor['contractor_name'] = $user['username'];
            $contractor['services_qoute'] = $servicesQoute;
            $contractor['discount'] = $discount;
            $contractor['refund'] = $refund;
            $contractor['contractor_share'] = $contractorShare;
            $contractor['total_contractor_share'] = $contractorShare - $refund;
        }

        if ($this->request->isPost() && $flag == 0) { // check if POST request method
            foreach ($contractor_shares as $contractor_id => $shareValue) {

                $data = array(
                    'booking_id' => $bookingId,
                    'contractor_id' => $contractor_id,
                    'contractor_share' => str_replace(",", "", $shareValue)
                );

                $contractorShareBooking = $modelContractorShareBooking->getByBookingIdAndContractorId($bookingId, $contractor_id);

                if ($contractorShareBooking) {
                    $modelContractorShareBooking->updateById($contractorShareBooking['id'], $data);
                } else {
                    $modelContractorShareBooking->insert($data);
                }
            }
            echo json_encode(array('msg' => '1'));
            //echo 1;
            exit;
        } else if ($this->request->isPost() && $flag == 1) { // check if POST request method
            foreach ($contractor_shares as $contractor_id => $shareValue) {

                $data = array(
                    'booking_id' => $bookingId,
                    'contractor_id' => $contractor_id,
                    'contractor_share' => $shareValue
                );

                $contractorShareBooking = $modelContractorShareBooking->getByBookingIdAndContractorId($bookingId, $contractor_id);

                if ($contractorShareBooking) {
                    $modelContractorShareBooking->updateById($contractorShareBooking['id'], $data);
                } else {
                    $modelContractorShareBooking->insert($data);
                }
            }
            echo json_encode(array('msg' => 'done', 'booking' => $data));
            //echo 'done';
            exit;
        }

        $this->view->booking_id = $bookingId;
        $this->view->bookingQoute = $bookingQoute;
        $this->view->booking = $booking;
        $this->view->contractors = $contractors;
        $this->view->totalWithoutTax = $totalWithoutTax;
        $this->view->total = $total;
        $this->view->totalRefund = $totalRefund;
        $this->view->flag = $flag;
        echo $this->view->render('index/set-contractor-share.phtml');
        exit;
    }*/

    public function getEmailTemplateAction() {

        $contractorId = $this->request->getParam('to', 0);

        // load model
        $modelBooking = new Model_Booking();

        $emailTemplate = $modelBooking->getAwaitingUpdateAndInProcessEmailTemplateByContractor($contractorId);

        echo json_encode($emailTemplate);
        exit;
    }

    public function sendReminderUpdateBookingToContractorAction() {
        //
        // get params 
        //
        $bookingId = $this->request->getParam('id', 0);
        $toValue = $this->request->getParam('to', 0);


        if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this page"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // load models
        //
        $modelUser = new Model_User();
        $modelBooking = new Model_Booking();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelBookingStatus = new Model_BookingStatus();
        $modelAttachment = new Model_Attachment();

        //
        // geting data
        //
        $booking = $modelBooking->getById($bookingId);
        $modelBooking->fill($booking, array('property_type'));

        //
        // validation
        //
        if (!$booking) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Booking not found"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //check in can see his or assigned bookings
        if (!$modelBooking->checkIfCanSeeBooking($bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this booking"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $inProcess = $modelBookingStatus->getByStatusName('IN PROGRESS');
        $awaitingUpdate = $modelBookingStatus->getByStatusName('AWAITING UPDATE');

        //check in can see his or assigned bookings
        if (!($booking['status_id'] == $inProcess['booking_status_id'] || $awaitingUpdate['booking_status_id'])) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This booking status is not IN PROGRESS or AWAITING UPDATE"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }


        //Get Attribute Attachments 
        $modelAttributeListValueAttachment = new Model_AttributeListValueAttachment();
        $modelServiceAttributeValue = new Model_ServiceAttributeValue();
        $serviceAttributeValues = $modelServiceAttributeValue->getByBookingIdListAttributeValues($bookingId);


        $AttributeListValueAttachments = array();
        foreach ($serviceAttributeValues as $serviceAttributeValue) {
            $listAttachment = $modelAttributeListValueAttachment->getByAttributeValueId($serviceAttributeValue['value']);
            if ($listAttachment) {
                $AttributeListValueAttachments[] = array('attachments' => $listAttachment, 'attribute_name' => $serviceAttributeValue['attribute_name']);
            }
        }


        // Get contractors email
        $contractorServices = $modelContractorServiceBooking->getContractorServicesByBookingId($bookingId);
        $contractorEmails = array();
        foreach ($contractorServices as $contractorService) {
            $user = $modelUser->getById($contractorService['contractor_id']);
            $emails = array();
            if ($user['email1'] && filter_var($user['email1'], FILTER_VALIDATE_EMAIL)) {
                $emails[] = $user['email1'];
            }
            if ($user['email2'] && filter_var($user['email2'], FILTER_VALIDATE_EMAIL)) {
                $emails[] = $user['email2'];
            }
            if ($user['email3'] && filter_var($user['email3'], FILTER_VALIDATE_EMAIL)) {
                $emails[] = $user['email3'];
            }
            $emails = implode(',', $emails);
            $contractorEmails[$contractorService['contractor_id']] = $emails;
        }

        if (count($contractorEmails) == 1) {
            $toValue = key($contractorEmails);
        }

        // drop dpwn list for Contractor Emial
        $to = new Zend_Form_Element_Select('to');
        $to->setDecorators(array('ViewHelper'));
        $to->setRequired();
        $to->setAttribs(array('style' => "width: 100%;", 'onchange' => "get_template()"));
        $to->setValue(!empty($toValue) ? $toValue : '');
        $to->addMultiOption('', 'Select One');
        $to->addMultiOptions($contractorEmails);

        $body = '';
        $subject = '';

        if ($this->request->isPost()) {

            $cc = $this->request->getParam('cc');
            $subject = $this->request->getParam('subject');
            $body = $this->request->getParam('body');
            $pdf_attachment = $this->request->getParam('pdf_attachment', 0);
            $selected_attachments = $this->request->getParam('attachment', array());

            $contractor = $modelUser->getById($toValue);

            $params = array(
                'to' => $contractor['email1'],
                'cc' => $cc,
                'body' => $body,
                'subject' => $subject,
            );

            $error_mesages = array();

            if (EmailNotification::validation($params, $error_mesages)) {
                $attachments = array();
                if (!empty($selected_attachments)) {
                    foreach ($selected_attachments as $attachment_id) {
                        $attachment = $modelAttachment->getById($attachment_id);
                        $attachments[] = $attachment['path'];
                    }
                }

                if (!empty($pdf_attachment)) {
                    // Create pdf
                    $viewParam = $this->getBookingViewParam($bookingId, true);
                    $view = new Zend_View();
                    $view->setScriptPath(APPLICATION_PATH . '/modules/booking/views/scripts/index');
                    $view->booking = $viewParam['booking'];
                    $view->bookingServices = $viewParam['bookingServices'];
                    $view->thisBookingServices = $viewParam['thisBookingServices'];
                    $view->priceArray = $viewParam['priceArray'];
                    $bodyBooking = $view->render('booking.phtml');

                    $pdfPath = createPdfPath();
                    $destination = $pdfPath['fullDir'] . $viewParam['booking']['booking_num'] . '.pdf';
                    wkhtmltopdf($bodyBooking, $destination);
                    $attachments[] = $destination;
                }
                if (!empty($pdf_attachment) || !empty($selected_attachments)) {
                    $attachments = implode(",", $attachments);
                    $params['attachment'] = $attachments;
                }

                /* if (!empty($pdf_attachment)) {
                  // Create pdf
                  $viewParam = $this->getBookingViewParam($bookingId, true);
                  $view = new Zend_View();
                  $view->setScriptPath(APPLICATION_PATH . '/modules/booking/views/scripts/index');
                  $view->booking = $viewParam['booking'];
                  $view->bookingServices = $viewParam['bookingServices'];
                  $view->thisBookingServices = $viewParam['thisBookingServices'];
                  $view->priceArray = $viewParam['priceArray'];
                  $bodyBooking = $view->render('booking.phtml');

                  $pdfPath = createPdfPath();
                  $destination = $pdfPath['fullDir'] . $viewParam['booking']['booking_num'] . '.pdf';
                  wkhtmltopdf($bodyBooking, $destination);
                  $params['attachment'] = $destination;
                  } */
                // Send Email
                $success = EmailNotification::sendEmail($params, '', array(), array('reference_id' => $booking['booking_id'], 'type' => 'booking'));

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Email sent"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Failed to send email"));
                }

                echo 1;
                exit;
            }
            $this->view->error_mesages = $error_mesages;
        }

        $this->view->booking = $booking;
        $this->view->to = $to;
        $this->view->subject = $subject;
        $this->view->body = $body;
        $this->view->cc = isset($cc) ? $cc : '';
        $this->view->AttributeListValueAttachments = $AttributeListValueAttachments;

        echo $this->view->render('index/send-reminder-update-booking-to-contractor.phtml');
        exit;
    }

    public function contractorBookingLocationAction() {

        $bookingId = $this->request->getParam('booking_id', 0);
        $contractorId = $this->request->getParam('contractor_id', 0);

        if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this page"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $modelBooking = new Model_Booking();
        $booking = $modelBooking->getById($bookingId);

        if (!$booking) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Booking not found"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }
        if (!$modelBooking->checkIfCanSeeLocation($bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this booking location"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }

        $modelUser = new Model_User();
        $contractor = $modelUser->getById($contractorId);
        if (!$contractor) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Technician not found"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }

        $modelBookingAddress = new Model_BookingAddress();
        $bookingAddress = $modelBookingAddress->getByBookingId($bookingId);

        //Contractor Address

        $contractorAddress = array();
        $contractorAddress['unit_lot_number'] = $contractor['unit_lot_number'];
        $contractorAddress['street_number'] = $contractor['street_number'];
        $contractorAddress['street_address'] = $contractor['street_address'];
        $contractorAddress['suburb'] = $contractor['suburb'];
        $contractorAddress['state'] = $contractor['state'];
        $contractorAddress['postcode'] = $contractor['postcode'];

        if ($bookingAddress && !empty($contractorAddress)) {

            $line_booking_address = get_line_address($bookingAddress);
            $line_contractor_address = get_line_address($contractorAddress);
            $is_address = false;

            if (empty($line_booking_address)) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "There is no address for this booking"));
                $this->_redirect($this->router->assemble(array(), 'Login'));
            } else {
                $MAP_OBJECT = new GoogleMapAPI();

                $MAP_OBJECT->setHeight(500);
                $MAP_OBJECT->setWidth('100%');
                $MAP_OBJECT->setMapType('map');

                $MAP_OBJECT->addDirections($line_contractor_address, $line_booking_address, 'map_directions', true);
                $MAP_OBJECT->addPolyLineByAddress($line_contractor_address, $line_booking_address);
                $MAP_OBJECT->addMarkerByAddress($line_booking_address);

                $this->view->MAP_OBJECT = $MAP_OBJECT;
                $is_address = true;
            }
            $this->view->sub_menu = "";
            $this->view->is_sub_menu = 1;
            $this->view->is_address = $is_address;
            $this->view->booking = $booking;
            $this->view->line_address = $line_booking_address;
        } else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "There is no address for this booking"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }
    }

    public function modifyFollowDateAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('bookingEdit'));


        $bookingId = $this->request->getParam('booking_id', 0);
        $followDate = $this->request->getParam('follow_date', 0);

        $followDate = strtotime($followDate);


        $modelBooking = new Model_Booking();
        $booking = $modelBooking->getById($bookingId);


        if (!$booking) {
            echo 1;
            exit;
        }

        $modelBooking->updateById($bookingId, array('to_follow' => $followDate));
        $followDates = getDateFormating($followDate);

        echo json_encode(array('st' => 2, 'msg' => $followDates));
        exit;
    }

    public function pauseResumeBookingEmailsAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('bookingEdit'));

        //
        // get params
        //
        $bookingId = $this->request->getParam('id');
        $process = $this->request->getParam('process', 'pause');

        //
        // load model
        //
        $modelBooking = new Model_Booking();
        $booking = $modelBooking->getById($bookingId);
        if (!$booking) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Booking not found"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this page"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        if ($process == 'pause') {
            $modelBooking->updateById($bookingId, array('pause_emails' => 1));
        } else if ($process == 'resume') {
            $modelBooking->updateById($bookingId, array('pause_emails' => 0));
        }


        $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
    }

    public function advanceSearchAction() {
        echo $this->view->render('index/filters.phtml');
        exit;
    }

    //D.A 03/09/2015 clear booking cache
    public function bookingCacheClearAction() {

        //check Auth for logged user
        CheckAuth::checkPermission(array('bookingEdit'));

        //get request parameters
        $bookingId = $this->request->getParam('id');

        require_once 'Zend/Cache.php';
        $company_id = CheckAuth::getCompanySession();
        $bookingViewDir = get_config('cache') . '/' . 'bookingsView' . '/' . $company_id;
        if (!is_dir($bookingViewDir)) {
            mkdir($bookingViewDir, 0777, true);
        }
        $frontEndOption = array('lifetime' => NULL,
            'automatic_serialization' => true);
        $backendOptions = array('cache_dir' => $bookingViewDir);
        $Cache = Zend_Cache::factory('Core', 'File', $frontEndOption, $backendOptions);
        $Cache->remove($bookingId . '_bookingDetails');
        $Cache->remove($bookingId . '_rejectBookingQuestions');
        $Cache->remove($bookingId . '_bookingLabels');
        $Cache->remove($bookingId . '_bookingServices');
        $Cache->remove($bookingId . '_updateBookingQuestions');
        $Cache->remove($bookingId . '_bookingAvailableTechnicians');
        $Cache->remove($bookingId . '_bookingPhotos');
        $Cache->remove($bookingId . '_bookingScheduledVisits');
        $Cache->remove($bookingId . '_technicianUpdateDetails');

        $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Cache cleared"));
        $this->_redirect($this->router->assemble(array('id' => $bookingId), 'bookingView'));
    }

    //D.A 12/10/2015 return bookings of selected date
    public function bookingsForDateAction() {
        $bookings_date = $this->request->getParam('bookings_date');

        if ($this->request->isPost()) {
            $start = date("Y-m-d 00:00:00", mktime(0, 0, 0, date("m", strtotime($bookings_date)), date("d", strtotime($bookings_date)), date("Y", strtotime($bookings_date))));
            $end = date("Y-m-d 23:59:59", mktime(0, 0, 0, date("m", strtotime($bookings_date)), date("d", strtotime($bookings_date)), date("Y", strtotime($bookings_date))));

            //echo json_encode(array('url' => $this->router->assemble(array(), 'booking') . '?fltr[booking_start_between]=' . $start . '&fltr[booking_end_between]=' . $end . '&fltr[multiple_booking_start_between]=' . $start . '&fltr[multiple_booking_end_between]=' . $end));
            echo json_encode(array('url' => $this->router->assemble(array(), 'booking') . '?fltr[booking_start_between]=' . $start . '&fltr[booking_end_between]=' . $end . '&fltr[multiple_start_between]=' . $start . '&fltr[multiple_end_between]=' . $end));
            exit;
        }

        // render views
        echo $this->view->render('index/bookings-for-date.phtml');
        exit;
    }

    public function notAssignedBookingAction() {
        $filters = $this->request->getParam('fltr', array('convert_status' => 'booking'));
        $modelBooking = new Model_Booking();
        $notAssignedBooking = $modelBooking->getAll($filters);
        $rejectedFilters = array(
            'rejectBookings' => 'reject',
            'booking_not_finished_yet' => true,
            'exclude_status' => 'CANCELLED',
            'withoutEstimateStatus' => 1
        );
        $rejectedBooking = $modelBooking->getAll($rejectedFilters);
        $data = array_merge($notAssignedBooking, $rejectedBooking);
        $this->view->filters = $filters;
        $this->view->data = $data;
        echo $this->view->render('index/draw-node.phtml');
        exit;
    }

    public function bookingAddressAction() {
        $modelBookingAddress = new Model_BookingAddress();
        $bookingId = $this->request->getParam('booking_id', 0);
        $bookingAddress = $modelBookingAddress->getByBookingId($bookingId);
        
        if ($this->request->isPost()) {

            $unit_lot_number = $this->request->getParam('unit_lot_number');
            $street_number = $this->request->getParam('street_number');
            $street_address = $this->request->getParam('street_address');
            $suburb = $this->request->getParam('suburb');
            $postcode = $this->request->getParam('postcode');
      
            $data = array(
                'unit_lot_number' => $unit_lot_number,
                'street_number' => $street_number,
                'street_address' => $street_address,
                'suburb' => $suburb,
                'postcode' => $postcode,
            
            );

            $modelBookingAddress->updateByBookingId($bookingId, $data);

            echo 1;
            exit;
        }
        $this->view->bookingId = $bookingId;
        $this->view->bookingAddress = $bookingAddress;
        echo $this->view->render('index/booking-address-details.phtml');
        exit;
    }

    public function emailTestAction() {

////         echo "test first";
//        $model_Inquiry = new Model_Inquiry();
//        $model_Inquiry->cronJobPhotoRequest();
//        exit;
//        $emailTemplate = $modelEmailTemplate->getEmailTemplate('test2_template');
//        $body = $emailTemplate['body'];
//        $subject = $emailTemplate['subject'];
//
//        $params = array(
//            'to' => array("ayah.doha20@gmail.com", "bos.company.2015@gmail.com", "salim.3bd@gmail.com", "SalahAliMohamed1995@gmail.com", "monahussein69@gmail.com", "dev.shayek@gmail.com"),
//            'body' => $body,
//            'subject' => $subject,
//            'from' => "enquiries@tilecleaners.com.au",
//        );
//
//        
//        $template_params = array(
//            "isTest" => 1,
//            "{phone}" => (getUserDeviceType() != 'web') ? '<a href="tel:1300 771 201"/>1300 771 201</a>' : '1300 771 201'
//        );
//
////        var_dump($template_params);
////        exit;
//        $success = EmailNotification::sendEmail($params, 'test2_template', $template_params, array());
//
//        if ($success) {
//            echo "yes";
//            $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Email sent"));
//        } else {
//            echo "No";
//            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Could not send the Booking"));
//        }
        exit;
    }
    
    public function sendBookingAsSmsByTwilioAction() 
	{

		// CheckAuth::checkPermission(array('sendSmsTwilio'));
		 $bookingId = $this->request->getParam('id', 0);
		 $type = $this->request->getParam('type');
		 $template_id = $this->request->getParam('template_id', 0);
		 $get_template = $this->request->getParam('getemp', 0);
		 $user_id = $this->request->getParam('user_id', 0);
		 //for view
        if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) 
		{
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Booking not belongs to your Company"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
		$modelUser = new Model_User();
        $modelBooking = new Model_Booking();
        $modelCustomer = new Model_Customer();
        $modelBookingAddress = new Model_BookingAddress();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
		$modelSmsTemplate = new Model_SmsTemplate();
		$modelCompanies=new Model_Companies();
		$modelTradingName=new Model_TradingName();
        $booking = $modelBooking->getById($bookingId);
        /*$multiple = '';
        if (!empty($booking['multiple_days'])) {
            foreach ($booking['multiple_days'] as $day) {
                $multiple = $multiple . ' ' . getBookingDateFormating($day['booking_start'], $day['booking_end']);
                $multiple = $multiple . ' ' . '<br />';
            }
        }*/
		
		//for view
        if (!$booking) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Booking not exist"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        //check in can see his or assigned bookings
        if (!$modelBooking->checkIfCanSeeBooking($bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this Booking"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
		//company_phone1
		$company_id=$booking['company_id'];
		$companyInfo=$modelCompanies->getById($company_id);
		$company_name=$companyInfo['company_name'];
		$trading_name_id=$booking['trading_name_id'];
        $trading_info=$modelTradingName->getById($trading_name_id);
		$trading_name=$trading_info['trading_name'];
		$phone=$trading_info['phone'];
		
		$for_twilio_mobile_format=1;
		$customer = $modelCustomer->getById($booking['customer_id'],$for_twilio_mobile_format);
		$customer_contacts=$customer['mobile1'].','. $customer['mobile2'].','.$customer['mobile3'];
        $customer_first_name=$customer['first_name'];
		//$user = $modelUser->getById($booking['created_by'],1);
        $contractorServices = $modelContractorServiceBooking->getContractorServicesByBookingId($bookingId);
		$contractors=array();
		$contractors_names=array();
		$contractors_ids=array();
		$to_customer=array();
		array_push($to_customer, $customer['mobile1'], $customer['mobile2'],$customer['mobile3']);
        foreach ($contractorServices as $contractorService) 
		{
            $contractor = $modelUser->getById($contractorService['contractor_id'],1);
            array_push($contractors, $contractor);
			array_push($contractors_names, $contractor['display_name']);
			array_push($contractors_ids, $contractor['user_id']);
        }
			 if($type=='contractor')
			 {
			 $user_name=$contractors_names[$user_id];
             $user_id=$contractors_ids[$user_id];
		      }
			 if($type=='customer'){
		     $customer = $modelCustomer->getById($booking['customer_id']);
			 $user_name=$customer['first_name'];
			 $user_id=$booking['customer_id'];
			 }
			 
			 $template_params = array(
            '{booking_num}'             =>$booking['booking_num'],
			'{user_name}'               =>$user_name,
            '{booking_id}'              =>$bookingId,
			'{customer_contacts}'       =>$customer_contacts,
			'{company_name}'            =>$company_name,
			'{trading_name}'            =>$trading_name,
			'{customer_first_name}'     =>$customer_first_name,
			'{phone}'                   =>$phone,
            '{booking_start}'           => date("F j, Y, g:i a", strtotime($booking['booking_start'])),
            '{booking_address}'         => get_line_address($modelBookingAddress->getByBookingId($booking['booking_id'])),
        );
        //$template_params['{multiple}'] = $multiple;
		 if($get_template==1)
		{
		$templateInfo=$modelSmsTemplate->getByName('send message to contractor  for booking');
		$template_id=$templateInfo['id'];
		$smsTemplate = $modelSmsTemplate->getsmsTemplate($template_id, $template_params);
		$message = $smsTemplate['message'];
        echo json_encode(array('message' =>$message,'user_id'=>$user_id));
				 exit;
			 
			 }
		else{
		$templateInfo=$modelSmsTemplate->getByName('send message to  customer  for booking');
		$template_id=$templateInfo['id'];
		$smsTemplate = $modelSmsTemplate->getsmsTemplate($template_id, $template_params);
		$message = $smsTemplate['message'];
			 }
		
		if ($this->request->isPost() && $get_template!=1) {
		$mobile_no = $this->request->getParam('mobile_no');
		$messages = $this->request->getParam('message');
		$message_type = $this->request->getParam('message_type');
		$user_id = $this->request->getParam('user_id');
	    $modelSmsHistory = new Model_SmsHistorty();
        $mobile=explode(",",$mobile_no);
		
		for($i=0;$i<count($mobile);$i++)
		{
				$fromNumber = "+61447075733";
				if($mobile[$i]!='')
				$toNumber = "$mobile[$i]";
				else
				continue;
	
                $sms_id=$modelSmsHistory->sendSmsTwilio($fromNumber,$toNumber,$messages);
			    if($sms_id){
				$params = array(
				 'reference_id'      => $template_id,
				 'from'              => '+61447075733',
				 'to'                => $toNumber,
				 'message_sid'       => $sms_id,
				 'message'           => $messages,
				 'receiver_id'       => $user_id,
				 'status'            => '',
				 'sms_type'          => 'sent',
				 'sms_reason'        => 'booking',
				 'reason_id'         => $bookingId,
				 'template_type'     => 'standard',

				);
			$modelSmsHistory->insert($params);
			   }
			 }
			  
			   $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
				}
				
			 else
			 {
			 $this->view->contractors = $contractors;
			 $this->view->to_customer = $to_customer;
			 $this->view->id=$bookingId;
			 $this->view->type=$type;
			 $this->view->message = $message;
			 $form = new Booking_Form_sendSms();
			 $this->view->form = $form;
			 echo $this->view->render('index/send-booking-as-sms-by-twilio.phtml');
			 exit;
			 }
	}
	
	
	
	public function viewReplySmsByTwilioAction() 
	{
		$id = $this->request->getParam('id');
        $data=array();
		$modelSmsHistorty=new Model_SmsHistorty();								
		$data=$modelSmsHistorty->getAllChild($id);
        $this->view->data = $data;
		echo $this->view->render('index/view-reply-sms-by-twilio.phtml');
		exit;
	}
	
	
	public function replySmsByTwilioAction() 
	{
		//replyTwilioSms
		//CheckAuth::checkPermission(array('replyTwilioSms'));
       $to = $this->request->getParam('to');
	   $id = $this->request->getParam('id');
       $sender_type = $this->request->getParam('sender_type','');
	   $smsMessage = $this->request->getParam('message','');
       $modelSmsHistory = new Model_SmsHistorty();
 	   $modelBooking = new Model_Booking();
	   $mobileFormat=$modelSmsHistory->getMobileFormat($to);
	   $smsInfo=$modelSmsHistory->getLastMessageId($to);
	   $referenceId=$smsInfo['reference_id'];
	   $smsIncominginfo=$modelSmsHistory->getById($id);
	   $modelCustomer=new Model_Customer();

	 
	   
	   $senderType=$smsIncominginfo['sender_type'];
	   if($senderType=="both")
	   {
		 $this->view->sender_type=$senderType;
	   }
	   $send_time=$smsInfo['send_time'];
	   $sid=$smsInfo['id'];
       if($send_time == '')
	   {
		$modelBooking->cronJobGetSmsInfo();
  	    $send_time1=$smsInfo['send_time'];
       }
	   else{
	    $send_time1=$smsInfo['send_time'];
	   }
	   $lastMessages=$modelSmsHistory->getLastMessageesAtSameDay($to,$send_time1);
       if(count($lastMessages)>1)
	   $this->view->lastMessages=$lastMessages;
	   $modelSmsTemplate = new Model_SmsTemplate();
	   $smsTemplateInfo=$modelSmsTemplate->getById($referenceId);
	   $placeholder=$smsTemplateInfo['placeholder'];
	   $userType=$smsTemplateInfo['type'];
	   $modelUser=new Model_User();
	   $userInfo=$modelUser->getByMobile($mobileFormat);
	   $modelContractorServiceBooking = new Model_ContractorServiceBooking();
	   if($userInfo && ($sender_type!='customer' || $sender_type=='') && $userType != 'customer')
		  {
				$userId=$userInfo['user_id'];
				$reference_id=-1;
				$userName=$userInfo['username'];
				$this->view->userName = $userName;
				
			}
		else
			{
                
				$customerInfo=$modelCustomer->getByMobile($mobileFormat);
				$userId=$customerInfo['customer_id'];
				$reference_id=-2;
				$userName=$customerInfo['first_name'];
				$this->view->userName = $userName;
	 		}
			
			
			
			$isLastBooking=1;
            if($sender_type==""){		
		if($smsInfo['cron_job_history']>0)
			{
				$smsReson='booking';
				if($userType=='contractor'){
				$bookingContractorInfo=$modelContractorServiceBooking->getByContractorId($userId,$isLastBooking);
				$booking_ids=$bookingContractorInfo[0]['booking_id'];
				$booking_num=$modelBooking->getBookingNumById($bookingContractorInfo[0]['booking_id']);
				$bookingNumerString="booking number:".$booking_num;
				$this->view->bookingNum = $bookingNumerString;

				}
				else{
				$bookingInfo=$modelBooking->getByCustomerId($userId,$isLastBooking);//fetchall
				$bookingNumerString="booking number:".$bookingInfo[0]['booking_num'];
				$this->view->bookingNum = $bookingNumerString;
				}
			}
			else{  
		        if($smsInfo['sms_reason'] == 'booking') {
				$smsReson='booking';
				if($userType=='contractor'){
				$bookingContractorInfo=$modelContractorServiceBooking->getByContractorId($userId,$isLastBooking);
				$booking_ids=$bookingContractorInfo[0]['booking_id'];
				$booking_num=$modelBooking->getBookingNumById($bookingContractorInfo[0]['booking_id']);
				$bookingNumerString="booking number:".$booking_num;
				$this->view->bookingNum = $bookingNumerString;

				}
				else{
				$bookingInfo=$modelBooking->getByCustomerId($userId,$isLastBooking);//fetchall
				$bookingNumerString="booking number:".$bookingInfo[0]['booking_num'];
				$this->view->bookingNum = $bookingNumerString;
				}
                }
		   else if ($smsInfo['sms_reason'] == 'estimates') {
				    $smsReson='estimates';
		   			$modelBookingEstimate = new Model_BookingEstimate();
					/*if($userType=='contractor'){
					$bookingContractorInfo=$modelContractorServiceBooking->getByContractorId($userId,$isLastBooking);
					$bookingId=$bookingContractorInfo['booking_id'];
					$estimateInfo=$modelBookingEstimate->getByBookingId($bookingId);
					$estimateNumberString="estimation number".$estimateInfo['estimate_num'];
					$this->view->estimateNum = $estimateNumberString;
			
			    }
				else{
					$bookingInfo=$modelBooking->getByCustomerId($userId,$isLastBooking);
					$bookingId= $bookingInfo['booking_id'];
					$estimateInfo=$modelBookingEstimate->getByBookingId($bookingId);
					$estimateNumberString="estimation number".$estimateInfo['estimate_num'];
					$this->view->estimateNum = $estimateNumberString;
				   }
          */
					$bookingInfo=$modelBooking->getByCustomerId($userId,$isLastBooking);
					$bookingId= $bookingInfo[0]['booking_id'];
					$estimateInfo=$modelBookingEstimate->getByBookingId($bookingId);//fetchRow
					$estimateNumberString="estimation number".$estimateInfo['estimate_num'];
					$this->view->estimateNum = $estimateNumberString;		  
					}
		   
		      else if ($smsInfo['sms_reason'] == 'inquiry')
			{
				 $smsReson='inquiry';
				 $modelInquiry = new Model_Inquiry();
				 $inquiryInfo=$modelInquiry->getByCustomerId($userId,$isLastBooking);//fetchAll
				 $inquiryNumberString= "inquiry number".$inquiryInfo[0]['inquiry_num'];
				 $this->view->inquiryNum = $inquiryNumberString;
		   }
		   
		   else if($smsInfo['sms_reason'] == 'invoices') {
				$smsReson='invoices';
				$modelBookingInvoice = new Model_BookingInvoice();
				/*	if($userType=='contractor'){
					$bookingInfo=$modelContractorServiceBooking->getByContractorId($userId,$isLastBooking);
					$bookingId=$bookingInfo['booking_id'];
					$invoiceInfo=$modelBookingInvoice->getByBookingId($bookingId);
					$invoiceNumberString= "Invoice Number".$invoiceInfo['invoice_num'];
					$this->view->invoiceNum = $invoiceNumberString;
			
			    }
				else{*/
					$bookingInfo=$modelBooking->getByCustomerId($userId,$isLastBooking);
					$bookingId= $bookingInfo[0]['booking_id'];
					$invoiceInfo=$modelBookingInvoice->getByBookingId($bookingId);//fetchRow					
					$invoiceNumberString= "Invoice Number".$invoiceInfo['invoice_num'];
					$this->view->invoiceNum = $invoiceNumberString;

			}
			 else if($smsInfo['sms_reason'] == 'complaint') {
				$smsReson='complaint';
     		        $complaintModel = new Model_Complaint();
					$bookingContractorInfo=$modelContractorServiceBooking->getByContractorId($userId,$isLastBooking);
			     	$bookingId=$bookingContractorInfo[0]['booking_id'];
					$islast=1;
					$complaintInfo = $complaintModel->getByBookingId($bookingId,$islast);
					$complaintNumberString= "Complaint Number".$complaintInfo[0]['complaint_num'];
					$this->view->complaintNum = $complaintNumberString;
			}
			}
			}
			
		if($this->request->isPost())
		{
	  
			 $smsInfo=$modelSmsHistory->getById($id);
			 $id_type=$smsInfo['reason_id'];
			 $sms_types=$smsInfo['sms_reason'];

			$fromNumber="+61447075733";
			//$smsMessage = $this->request->getParam('message');
		    $sms_id=$modelSmsHistory->sendSmsTwilio($fromNumber,$to,$smsMessage);
			
		    if($sms_id){
				$params = array(
				'reference_id'     => $reference_id,
				'from'             => $fromNumber,
				'to'               => $to,
				'message_sid'      => $sms_id,
				'message'          => $smsMessage,
				'receiver_id'      => $userId,
				'status'           => '',
				'receive_time'     =>'',
				'sms_type'         =>'sent',
				'parent_id'        =>$id,
				'sms_reason'       =>$sms_types,
                'reason_id'        =>$id_type,
				'template_type'     =>'free',

				);
				$modelSmsHistory->insert($params);
				if($sender_type=='customer')
				{
				  $Incomingdata=array(
				  'receiver_id'      => $userId
				  );
				  $modelSmsHistory->updateById($id,$Incomingdata);
				}
			   }
			   $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
				}
			 else{
			 $this->view->to = $to;
             $this->view->id = $id;
			 $form = new Booking_Form_sendSms();
			 $this->view->form = $form;
			 echo $this->view->render('index/reply-sms-by-twilio.phtml');
			 exit;
			 }
		
	}
    
    
    
    
    /// Added BY RAND
	public function sendEmailToContractorAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('sendEmail'));
        $contractorId = $this->request->getParam('contractor_id', 0);
        $type = $this->request->getParam('type', 'contractor');
        $bookingId = $this->request->getParam('booking_id', 0);
        $modelUser = new Model_User();
        $contractor = $modelUser->getById($contractorId);
		$modelBooking = new Model_Booking();

        if (!$contractor) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Contractor Doesn't Exist"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }
		if($bookingId != 0)
		{
		if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Booking not belongs to your Company"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
		}
		
		//check in can see his or assigned bookings
		if($bookingId != 0)
		{
        if (!$modelBooking->checkIfCanSeeBooking($bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this Booking"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
		}
		
        $to = array();
        if ($contractor['email1']) {
            $to[] = $contractor['email1'];
        }
        if ($contractor['email2']) {
            $to[] = $contractor['email2'];
        }
        if ($contractor['email3']) {
            $to[] = $contractor['email3'];
        }
        $to = implode(',', $to);

        if ($this->request->isPost()) {

            $to = $this->request->getParam('to');
            $cc = $this->request->getParam('cc');
            $subject = $this->request->getParam('subject');
            $body = $this->request->getParam('body');
            $cannedResponsesValue = $this->request->getParam('canned-responses');


            $params = array(
                'to' => $to,
                'cc' => $cc,
                'body' => $body,
                'subject' => $subject
            );

      
            $email_log = array(
                'type' => $type,
                'booking_id' => $bookingId
            );

            $error_mesages = array();
            if (EmailNotification::validation($params, $error_mesages)) {
                $success = EmailNotification::sendEmail($params, '', array(), $email_log);
                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Email sent successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Email not sent"));
                }

                echo 1;
                exit;
            }
            $this->view->error_mesages = $error_mesages;
        }

        $modelCannedResponses = new Model_CannedResponses();
        $cannedResponses = $modelCannedResponses->getAll();
        $this->view->cannedResponses = $cannedResponses;

        $this->view->to = $to;
        $this->view->cc = isset($cc) ? $cc : '';
        $this->view->subject = isset($subject) ? $subject : '';
        $this->view->body = isset($body) ? $body : '';
        $this->view->contractor_id = $contractorId;
        $this->view->type = $type;
        $this->view->booking_id = $bookingId;
        $this->view->cannedResponsesValue = isset($cannedResponsesValue) ? $cannedResponsesValue : 0;
        echo $this->view->render('index/send-email-to-contractor.phtml');
        exit;
    }
    
    
    
    
    ///By Yasmeen
    
    public function viewOutgoingSmsSameDateTypeAction() 
	{
		//CheckAuth::checkPermission(array('viewReplySms'));
		$id = $this->request->getParam('id');
		$reason = $this->request->getParam('reason');
		$date_received = $this->request->getParam('date_received');
		$user_id = $this->request->getParam('user_id');
		$reference_id = $this->request->getParam('reference_id');
        //$data=array();
		$modelSmsHistorty=new Model_SmsHistorty();								
		$data=$modelSmsHistorty->getOutgoingSmsSameDateType($reference_id,$user_id,$reason,$date_received);
        $this->view->data = $data;//
	    $this->view->sid_incoming =$id;
		echo $this->view->render('index/view-outgoing-sms-same-date-type.phtml');
		exit;
	}
    
   public function convertSmsDependOnOtherSmsAction() 
	    {
		//sid incoming_sid
		//CheckAuth::checkPermission(array('viewReplySms'));
		$sid = $this->request->getParam('sid');
		$incoming_sid = $this->request->getParam('incoming_sid');
		$modelSmsHistorty=new Model_SmsHistorty();
		$smsInfo=$modelSmsHistorty->getById($sid);
	    $id_type=$smsInfo['reason_id'];
	    $sms_reason=$smsInfo['sms_reason'];
        $data=array(
		'sms_reason'=>$sms_reason,
		'reason_id'=>$id_type,
		);
		$modelSmsHistorty->updateById($incoming_sid,$data);
         //getChildOfParent
        $child=$modelSmsHistorty->getChildOfParent($incoming_sid);
		for($i=0;$i<count($child);$i++)
		{		
	     $modelSmsHistorty->updateById($child['id'],$data);
		}

		$this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
	}
    
    
    
    
    public function convertSmsReasonAction() 
	{
		//CheckAuth::checkPermission(array('viewReplySms'));
		//reference_id
		$id = $this->request->getParam('id');
		$sms_id = $this->request->getParam('sid');
		$sms_reason = $this->request->getParam('sms_reason');
		$date = $this->request->getParam('send_time');
		$reference_id = $this->request->getParam('reference_id');
        $modelBooking = new Model_Booking();
		$modelSmsHistorty=new Model_SmsHistorty();
		
        if($sms_reason=='')
		{
            $data=array(
            'sms_reason'=>'',
            'reason_id'=>'',
            );
		}
       else{		
           if($date=='')
           {
                $modelBooking->cronJobGetSmsInfo();
                $smsIncominginfo=$modelSmsHistory->getById($sms_id);
                $date1=$smsIncominginfo['send_time'];

           }
           else{
                $date1=$date;
           }
           //inquiry booking  estimate  invoice  complaint
           $smsInfo=$modelSmsHistorty->getLastMessageBtReceiverId($reference_id,$id,$sms_reason,$date1);
           $id_type=$smsInfo['reason_id'];
            $data=array(
            'sms_reason'=>$sms_reason,
            'reason_id'=>$id_type,
            );
		}
		$modelSmsHistorty->updateById($sms_id,$data);
			   $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
	}
    
    ///End By Yasmeen
    
     
    }
