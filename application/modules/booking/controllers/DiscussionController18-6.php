<?php

class Booking_DiscussionController extends Zend_Controller_Action {

    private $request;
    private $router;
    private $loggedUser;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        //
        //checkLoggedIn
        //
        CheckAuth::checkLoggedIn();

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('bookingDiscussion'));

        //
        //get Logged User
        //
        $this->loggedUser = CheckAuth::getLoggedUser();
    }

    public function indexAction() {

        //
        //get Params
        //
        $bookingId = $this->request->getParam('id', 0);

        //
        // load model
        //
        $modelBooking = new Model_Booking();
        $modelBookingDiscussion = new Model_BookingDiscussion();
        $modelContractorInfo = new Model_ContractorInfo();
        $modelUser = new Model_User();

        if (!$modelBooking->checkIfCanSeeBookingWithOutTimePeriod($bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this Booking"));
            $this->_redirect($this->router->assemble(array(), 'booking'));
        }
        $booking = $modelBooking->getById($bookingId);
        $this->view->booking = $booking;

        if (!$booking) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Booking not exist"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $bookingDiscussions = $modelBookingDiscussion->getByBookingId($bookingId);
        foreach ($bookingDiscussions as &$bookingDiscussion) {
            $bookingDiscussion['user'] = $modelUser->getById($bookingDiscussion['user_id']);
            if ($bookingDiscussion['user']['role_name'] == 'contractor') {
                $contractorInfo = $modelContractorInfo->getByContractorId($bookingDiscussion['user']['user_id']);
                $bookingDiscussion['user']['username'] = ucwords($bookingDiscussion['user']['username']) . ' - ' . ucwords($contractorInfo['business_name']);
            } else {
                $bookingDiscussion['user']['username'] = ucwords($bookingDiscussion['user']['username']);
            }
        }

        $this->view->bookingDiscussions = $bookingDiscussions;

        echo $this->view->render('discussion/index.phtml');
        exit;
    }

    public function submitAction() {

        //
        //get Params
        //
        $bookingId = $this->request->getParam('id', 0);
        $discussion = $this->request->getParam('discussion', '');

        //
        // load model
        //
        $modelBookingDiscussion = new Model_BookingDiscussion();

        $success = 0;
        if ($discussion) {
            $data = array(
                'booking_id' => $bookingId,
                'user_id' => $this->loggedUser['user_id'],
                'user_message' => $discussion,
                'created' => time()
            );
            $success = $modelBookingDiscussion->insert($data);
        }

        if ($success) {
            echo json_encode(array('success' => 1));
        } else {
            echo json_encode(array('success' => 0, 'errMsg' => 'Invalid Discussion'));
        }
        exit;
    }

    public function getAllDiscussionAction() {

        //
        //get Params
        //
        $bookingId = $this->request->getParam('id', 0);

        //
        // load model
        //
        $modelBookingDiscussion = new Model_BookingDiscussion();
        $modelContractorInfo = new Model_ContractorInfo();
        $modelUser = new Model_User();

        $bookingDiscussions = $modelBookingDiscussion->getByBookingId($bookingId);
        foreach ($bookingDiscussions as &$bookingDiscussion) {
            $bookingDiscussion['user'] = $modelUser->getById($bookingDiscussion['user_id']);
            $bookingDiscussion['discussion_date'] = getDateFormating( $bookingDiscussion['created']);
            $bookingDiscussion['user_message'] = nl2br(htmlentities($bookingDiscussion['user_message'], ENT_NOQUOTES, 'UTF-8'));
            if ($bookingDiscussion['user']['role_name'] == 'contractor') {
                $contractorInfo = $modelContractorInfo->getByContractorId($bookingDiscussion['user']['user_id']);
                $bookingDiscussion['user']['username'] = ucwords($bookingDiscussion['user']['username']) . ' - ' . ucwords($contractorInfo['business_name']);
            } else {
                $bookingDiscussion['user']['username'] = ucwords($bookingDiscussion['user']['username']);
            }
        }

        $json_array = array(
            'discussions' => $bookingDiscussions,
            'count' => count($bookingDiscussions)
        );

        echo json_encode($json_array);
        exit;
    }

}

