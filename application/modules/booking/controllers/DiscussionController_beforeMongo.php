<?php

class Booking_DiscussionController extends Zend_Controller_Action {

    private $request;
    private $router;
    private $loggedUser;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        //
        //checkLoggedIn
        //
        CheckAuth::checkLoggedIn();

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('bookingDiscussion'));

        //
        //get Logged User
        //
        $this->loggedUser = CheckAuth::getLoggedUser();
    }

    public function indexAction() {

        //
        //get Params
        //
        $bookingId = $this->request->getParam('id', 0);

        //
        // load model
        //
        $modelBooking = new Model_Booking();
        $modelBookingDiscussion = new Model_BookingDiscussion();
        $modelContractorInfo = new Model_ContractorInfo();
        $modelUser = new Model_User();
		$modelImageAttachment = new Model_Image();

        if (!$modelBooking->checkIfCanSeeBookingWithOutTimePeriod($bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this booking"));
            $this->_redirect($this->router->assemble(array(), 'booking'));
        }
        $booking = $modelBooking->getById($bookingId);
        $this->view->booking = $booking;

        if (!$booking) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Booking not found"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $bookingDiscussions = $modelBookingDiscussion->getByBookingId($bookingId, 'DESC');
        $GroupsImageDiscussions = $modelBookingDiscussion->getByBookingId($bookingId, 'DESC' , array('groups'=>'groups'));
	    $bookingDiscussions = array_merge($bookingDiscussions,$GroupsImageDiscussions);
	    array_multisort($bookingDiscussions ,SORT_ASC );
		
		
        foreach ($bookingDiscussions as &$bookingDiscussion) {
            $bookingDiscussion['user'] = $modelUser->getById($bookingDiscussion['user_id']);
            if ($bookingDiscussion['user']['role_name'] == 'contractor') {
                $contractorInfo = $modelContractorInfo->getByContractorId($bookingDiscussion['user']['user_id']);
                $bookingDiscussion['user']['username'] = ucwords($bookingDiscussion['user']['username']) . ' - ' . ucwords($contractorInfo['business_name']);
            } else {
                $bookingDiscussion['user']['username'] = ucwords($bookingDiscussion['user']['username']);
            }
        }

        $this->view->bookingDiscussions = $bookingDiscussions;

        echo $this->view->render('discussion/index.phtml');
        exit;
    }

       public function submitAction() {

        //
        //get Params
        //
        $bookingId = $this->request->getParam('id', 0);
        $discussion = $this->request->getParam('discussion', '');
		$imageId = $this->request->getParam('imageId', 0);

		
        //
        // load model
        //
        $modelBookingDiscussion = new Model_BookingDiscussion();
		$modelImage = new Model_Image();

        $success = 0;
        if ($discussion) {
           			
			$upload = new Zend_File_Transfer_Adapter_Http();
            $files = $upload->getFileInfo();
			
			
            $counter = 0;
			if(isset($files) && !empty($files)){
				$countFiles = count($files);
                $image_group = 0;
                if ($countFiles > 1) {
                    $maxGroup = max($modelImage->getMaxGroup());
                    $image_group = $maxGroup['group_id'] + 1;
					$data = array(
						'booking_id' => $bookingId,
						'user_id' => $this->loggedUser['user_id'],
						'user_message' => $discussion,
						'created' => time()
					);
					$success = $modelBookingDiscussion->insert($data);
					$modelItemImageDiscussion = new Model_ItemImageDiscussion();
					$dataDiscssion = array(
						'image_id' => 0,
						'group_id' => $image_group,
						'item_id' => $success,
						'type' => 'booking'
					);
					$modelItemImageDiscussion->insert($dataDiscssion);
                }
			foreach ($files as $file => $fileInfo) {
                    if ($upload->isUploaded($file)) {
                        if ($upload->receive($file)) {
						
						    $info = $upload->getFileInfo($file);
                            $source = $info[$file]['tmp_name'];
                            $imageInfo = pathinfo($source);
                            $ext = $imageInfo['extension'];
                            $dir = get_config('image_attachment') . '/';
                            $subdir = date('Y/m/d/');
                            $fullDir = $dir . $subdir;
                            if (!is_dir($fullDir)) {
                                mkdir($fullDir, 0777, true);
                            }

                            $ip = $_SERVER['REMOTE_ADDR'];
                            $loggedUser = CheckAuth::getLoggedUser();
						
							if(isset($loggedUser['user_id'])){
							 $user_id = $loggedUser['user_id'];
							}else if($loggedUser['customer_id']){
							 $user_id =  $loggedUser['customer_id'];
							}else{
							 $user_id = 0;
							}
							
							$data = array(
                                'user_ip' => $ip
                                , 'created_by' => $user_id
                                , 'image_types_id' => 0
                                , 'group_id' => $image_group
								, 'user_role' => $loggedUser['role_id']
                            );



                            $id = $modelImage->insert($data);

                            $image_id = $id;
                            if ($countFiles > 1) {
                                $image_id = 0;
                            }
							if ($countFiles == 1) {
								$data = array(
									'booking_id' => $bookingId,
									'user_id' => $this->loggedUser['user_id'],
									'user_message' => $discussion,
									'created' => time()
								);
								$success = $modelBookingDiscussion->insert($data);
								$modelItemImageDiscussion = new Model_ItemImageDiscussion();
								$dataDiscssion = array(
												'image_id' => $image_id,
												'group_id' => $image_group,
												'item_id' => $success,
												'type' => 'booking'
										 );
																	 
								$modelItemImageDiscussion->insert($dataDiscssion);
								}
							
							$Itemdata = array(
                                'item_id' => $bookingId,
                                'service_id' => 0,
                                'discussion_id' => $success,
                                'image_id' => $id,
                                'type' => 'booking_discussion',
                                'floor_id' => 0
                            );


                            $modelItemImage = new Model_ItemImage();
                            $Item_image_id = $modelItemImage->insert($Itemdata);
							
							
							 

                            $original_path = "original_{$id}.{$ext}";
                            $large_path = "large_{$id}.{$ext}";
                            $small_path = "small_{$id}.{$ext}";
                            $thumbnail_path = "thumbnail_{$id}.{$ext}";
                            $compressed_path = "compressed_{$id}.jpg";

                            $data = array(
                                'original_path' => $subdir . $original_path,
                                'large_path' => $subdir . $large_path,
                                'small_path' => $subdir . $small_path,
                                'thumbnail_path' => $subdir . $thumbnail_path,
                                'compressed_path' => $subdir . $compressed_path
                            );

                            $modelImage->updateById($id, $data);
                            
                            //save image to database and filesystem here
                            $image_saved = copy($source, $fullDir . $original_path);
                            $compressed_saved = copy($source, $fullDir . $compressed_path);
                            ImageMagick::scale_image($source, $fullDir . $large_path, 510);
                            ImageMagick::scale_image($source, $fullDir . $small_path, 250);
                            ImageMagick::create_thumbnail($source, $fullDir . $thumbnail_path, 78, 64);
                            ImageMagick::convert($source, $fullDir . $compressed_path);
                            ImageMagick::compress_image($source, $fullDir . $compressed_path);
						

                            if ($image_saved) {
                                if (file_exists($source)) {
                                    unlink($source);									
                                }
                            }
							
							
						
						}
					}	
            }
			    require_once 'Zend/Cache.php';				
                $company_id = CheckAuth::getCompanySession();

                    //D.A 30/08/2015 Remove Booking Photos Cache				
                    $bookingPhotosCacheID = $bookingId . '_bookingPhotos';
                    $bookingViewDir = get_config('cache') . '/' . 'bookingsView' . '/' . $company_id;
                    if (!is_dir($bookingViewDir)) {
                        mkdir($bookingViewDir, 0777, true);
                    }
                    $frontEndOption = array('lifetime' => NULL,
                        'automatic_serialization' => true);
                    $backendOptions = array('cache_dir' => $bookingViewDir);
                    $Cache = Zend_Cache::factory('Core', 'File', $frontEndOption, $backendOptions);
                    $Cache->remove($bookingPhotosCacheID);
					
					
					
                    $modelBooking = new Model_Booking();
                    $modelBookingEstimate = new Model_BookingEstimate();
                    $booking = $modelBooking->getById($bookingId);
                    $estimate = $modelBookingEstimate->getByBookingId($bookingId);
                    if ($estimate) {
                        $estimateLabelsCacheID = $estimate['id'] . '_estimatePhoto';
                        $estimateViewDir = get_config('cache') . '/' . 'estimatesView' . '/' . $company_id;
                        if (!is_dir($estimateViewDir)) {
                            mkdir($estimateViewDir, 0777, true);
                        }
                        $estimatePhotoFrontEndOption = array('lifetime' => NULL,
                            'automatic_serialization' => true);
                        $estimatePhotoBackendOptions = array('cache_dir' => $estimateViewDir);
                        $estimatePhotoCache = Zend_Cache::factory('Core', 'File', $estimatePhotoFrontEndOption, $estimatePhotoBackendOptions);
                        $estimatePhotoCache->remove($estimateLabelsCacheID);
                    }
                    if ($booking['original_inquiry_id']) {
                        $inquiryPhotosCacheID = $booking['original_inquiry_id'] . '_inquiryPhotos';
                        $inquiryViewDir = get_config('cache') . '/' . 'inquiriesView' . '/' . $company_id;
                        if (!is_dir($inquiryViewDir)) {
                            mkdir($inquiryViewDir, 0777, true);
                        }
                        $inquiryPhotosFrontEndOption = array('lifetime' => NULL,
                            'automatic_serialization' => true);
                        $inquiryPhotosBackendOptions = array('cache_dir' => $inquiryViewDir);
                        $inquiryPhotosCache = Zend_Cache::factory('Core', 'File', $inquiryPhotosFrontEndOption, $inquiryPhotosBackendOptions);
                        $inquiryPhotosCache->remove($inquiryPhotosCacheID);
                    }
                
		}else{
			 $data = array(
						'booking_id' => $bookingId,
						'user_id' => $this->loggedUser['user_id'],
						'user_message' => $discussion,
						'created' => time()
					);
			  $success = $modelBookingDiscussion->insert($data);
			 $modelItemImageDiscussion = new Model_ItemImageDiscussion();
             $dataDiscssion = array(
                                    'image_id' => 0,
                                    'group_id' => 0,
                                    'item_id' => $success,
                                    'type' => 'booking'
                             );
							 							 
             $modelItemImageDiscussion->insert($dataDiscssion);
		}	
 
        }

        if ($success) {

             MobileNotification::notify($bookingId , 'new discussion');
            echo json_encode(array('success' => 1));
        } else {
            echo json_encode(array('success' => 0, 'errMsg' => 'Invalid discussion entry'));
        }
        exit;
    }

    public function getAllDiscussionAction() {

       
     $bookingId = $this->request->getParam('id', 0);

        //
        // load model
        //
        $modelBookingDiscussion = new Model_BookingDiscussion();
        $modelContractorInfo = new Model_ContractorInfo();
        $modelUser = new Model_User();
		$modelImageAttachment = new Model_Image();

		
        $bookingDiscussions = $modelBookingDiscussion->getByBookingId($bookingId, 'DESC');
        $GroupsImageDiscussions = $modelBookingDiscussion->getByBookingId($bookingId, 'DESC' , array('groups'=>'groups'));
	    $bookingDiscussions = array_merge($bookingDiscussions,$GroupsImageDiscussions);
	    array_multisort($bookingDiscussions ,SORT_ASC );
        foreach ($bookingDiscussions as &$bookingDiscussion) {
            $bookingDiscussion['user'] = $modelUser->getById($bookingDiscussion['user_id']);
            $bookingDiscussion['discussion_date'] = getDateFormating( $bookingDiscussion['created']);
            $bookingDiscussion['user_message'] = nl2br(htmlentities($bookingDiscussion['user_message'], ENT_NOQUOTES, 'UTF-8'));
            if ($bookingDiscussion['user']['role_name'] == 'contractor') {
                $contractorInfo = $modelContractorInfo->getByContractorId($bookingDiscussion['user']['user_id']);
                $bookingDiscussion['user']['username'] = ucwords($bookingDiscussion['user']['username']) . ' - ' . ucwords($contractorInfo['business_name']);
            } else {
                $bookingDiscussion['user']['username'] = ucwords($bookingDiscussion['user']['username']);
				if($bookingDiscussion['group_id']){
			      $bookingDiscussion['grouped_images'] = $modelImageAttachment->getByGroupId($bookingDiscussion['group_id']);
			      }
            }
        }

        $json_array = array(
            'discussions' => $bookingDiscussions,
            'count' => count($bookingDiscussions)
        );

        echo json_encode($json_array);
        exit;
    }
	
	public function getAllImageDiscussionAction() {
      
        //get Params
        //
       $imageId = $this->request->getParam('id', 0);

        //
        // load model
        //
        $modelBookingDiscussion = new Model_BookingDiscussion();
        $modelContractorInfo = new Model_ContractorInfo();
        $modelUser = new Model_User();

        $imageDiscussions = $modelBookingDiscussion->getByImageId($imageId,'Desc');
       
        foreach ($imageDiscussions as &$imageDiscussion) {
            $imageDiscussion['user'] = $modelUser->getById($imageDiscussion['user_id']);
            $imageDiscussion['discussion_date'] = getDateFormating( $imageDiscussion['created']);
            $imageDiscussion['user_message'] = nl2br(htmlentities($imageDiscussion['user_message'], ENT_NOQUOTES, 'UTF-8'));
            if ($imageDiscussion['user']['role_name'] == 'contractor') {
                $contractorInfo = $modelContractorInfo->getByContractorId($imageDiscussion['user']['user_id']);
                $imageDiscussion['user']['username'] = ucwords($imageDiscussion['user']['username']) . ' - ' . ucwords($contractorInfo['business_name']);
            } else {
                $imageDiscussion['user']['username'] = ucwords($imageDiscussion['user']['username']);
            }
        }

        $json_array = array(
            'discussions' => $imageDiscussions,
            'count' => count($imageDiscussions)
        );
		
	

        echo json_encode($json_array);
        exit;
    }
	
	
	public function getAllTabsDiscussionAction() {
	  
	    $bookingId = $this->request->getParam('bookingId', 0);
		$complaintId = $this->request->getParam('complaintId',0);
		$inquiry_id = $this->request->getParam('inquiryId',0);
		
		$modelBooking = new Model_Booking();
		$booking = $modelBooking->getById($bookingId);	
		//$bookingIsDeleted = !empty($booking['is_deleted']) ? true : false;

		$this->view->bookingId = $bookingId;
		
		//
		// load model
		//
		$modelBookingDiscussion = new Model_BookingDiscussion();
		$modelContractorInfo = new Model_ContractorInfo();
		$modelUser = new Model_User();
		$modelImageAttachment = new Model_Image();
		$modelAuthRole = new Model_AuthRole();
		
		$modelInvoiceDiscussion = new Model_InvoiceDiscussion();
		$modelEstimateDiscussion = new Model_EstimateDiscussion();
		$modelInquiryDiscussion = new Model_InquiryDiscussion();
		$modelBookingReminder = new Model_BookingReminder();
		$modelComplaintDiscussion = new Model_ComplaintDiscussion();
		
		$modelBookingInvoice = new Model_BookingInvoice();
		$modelBookingEstimate = new Model_BookingEstimate();
		
		$bookingDiscussions = array();
		$invoiceDiscussions = array();
		$estimateDiscussions = array();
		$inquiryDiscussions = array();
		$complaintDiscussions = array();
		
		
		if($bookingId){
			$invoice = $modelBookingInvoice->getByBookingId($bookingId);
			$estimate = $modelBookingEstimate->getByBookingId($bookingId);
			
			$invoiceId = !empty($invoice['id']) ? $invoice['id'] : 0;
			$estimateId = !empty($estimate['id']) ? $estimate['id'] : 0;
			$inquiryId = !empty($booking['original_inquiry_id']) ? $booking['original_inquiry_id'] : 0;
			
			//booking discussion
			$bookingDiscussions = $modelBookingDiscussion->getByBookingId($bookingId, 'DESC');
			//var_dump($bookingDiscussions);
			$GroupsImageDiscussions = $modelBookingDiscussion->getByBookingId($bookingId, 'DESC' , array('groups'=>'groups'));
			//var_dump($GroupsImageDiscussions);
			$bookingDiscussions = array_merge($bookingDiscussions,$GroupsImageDiscussions);
			array_multisort($bookingDiscussions ,SORT_DESC );
			foreach ($bookingDiscussions as &$bookingDiscussion) {
				
				if ($bookingDiscussion['user_role'] == 9) {
					$modelCustomer = new Model_Customer();
					$bookingDiscussion['user'] = $modelCustomer->getById($bookingDiscussion['user_id']);
					$username = $bookingDiscussion['user']['first_name'] . ' ' . $bookingDiscussion['user']['last_name'];
				} else {
					$bookingDiscussion['user'] = $modelUser->getById($bookingDiscussion['user_id']);
					$username = $bookingDiscussion['user']['username'];
				}
				if (isset($bookingDiscussion['user']['role_id']) && $bookingDiscussion['user']['role_id'] == $modelAuthRole->getRoleIdByName('contractor')){
					$contractorInfo = $modelContractorInfo->getByContractorId($bookingDiscussion['user']['user_id']);
					$bookingDiscussion['user']['username'] = ucwords($username) . ($contractorInfo['business_name'] ? ' - ' . ucwords($contractorInfo['business_name']) : '');
				}else{
					$bookingDiscussion['user']['username'] = ucwords($username);
				}
				
				$user_message ='';
				if(isset($bookingDiscussion['user_message'])){
					$user_message = $bookingDiscussion['user_message'];
				}else if(isset($bookingDiscussion['comment'])){
					$user_message = $bookingDiscussion['comment'];
				}
				
				if ($bookingDiscussion['created']) {
                     $created = getDateFormating($bookingDiscussion['created']);
                                                    } else {
                     $created = getDateFormating($booking['created']);
                       }
				
				$bookingDiscussion['discussion_date'] = $created;
				$bookingDiscussion['user_message'] = nl2br(htmlentities($user_message, ENT_NOQUOTES, 'UTF-8'));
				
				
				if(isset($bookingDiscussion['group_id']) && $bookingDiscussion['group_id'] != 0){
					$bookingDiscussion['grouped_images'] = $modelImageAttachment->getByGroupId($bookingDiscussion['group_id']);
					//$grouped_images in tabs.phtml
					$bookingDiscussion['booking_grouped_images'] = $modelImageAttachment->getByGroupIdAndType($bookingDiscussion['group_id'], $bookingId, 'booking');
					foreach($bookingDiscussion['booking_grouped_images'] as &$bookingGroupedImage){
						$bookingGroupedImage['imageDiscussions'] = $modelBookingDiscussion->getByImageId($bookingGroupedImage['image_id'], 'DESC');
						
						if($bookingGroupedImage['imageDiscussions']){	
							foreach ($bookingGroupedImage['imageDiscussions'] as &$imageDiscussion) {
								
								if ($imageDiscussion['user_role'] == 9) {
									$modelCustomer = new Model_Customer();
									$imageDiscussion['user'] = $modelCustomer->getById($imageDiscussion['user_id']);
									$username = $imageDiscussion['user']['first_name'] . ' ' . $imageDiscussion['user']['last_name'];
								} else {
									$imageDiscussion['user'] = $modelUser->getById($imageDiscussion['user_id']);
									$username = $imageDiscussion['user']['username'];
								}
								
								if (isset($imageDiscussion['user']['role_id']) && $imageDiscussion['user']['role_id'] == $modelAuthRole->getRoleIdByName('contractor')){
									$contractorInfo = $modelContractorInfo->getByContractorId($user['user_id']);
									$imageDiscussion['user']['username'] = ucwords($user['username']) . ($contractorInfo['business_name'] ? ' - ' . ucwords($contractorInfo['business_name']) : '');
								}else{
									$imageDiscussion['user']['username'] = ucwords($user['username']);
								}
								
								$imageDiscussion['discussion_date'] = getDateFormating( $imageDiscussion['created']);
								$imageDiscussion['user_message'] = nl2br(htmlentities($imageDiscussion['user_message'], ENT_NOQUOTES, 'UTF-8'));
								
								
							}
								
							
						}	
					}					
						
				}else if(isset($bookingDiscussion['thumbnail_path'])){
					$bookingDiscussion['imageDiscussions'] = $modelBookingDiscussion->getByImageId($bookingDiscussion['image_id'], 'DESC');	
					
					if($bookingDiscussion['imageDiscussions']){
						foreach ($bookingDiscussion['imageDiscussions'] as &$imageDiscussion) {
							$imageDiscussion['user'] = $modelUser->getById($imageDiscussion['user_id']);
							$imageDiscussion['discussion_date'] = getDateFormating( $imageDiscussion['created']);
							$imageDiscussion['user_message'] = nl2br(htmlentities($imageDiscussion['user_message'], ENT_NOQUOTES, 'UTF-8'));
							
							if ($imageDiscussion['user']['role_name'] == 'contractor') {
								$contractorInfo = $modelContractorInfo->getByContractorId($imageDiscussion['user']['user_id']);
								$imageDiscussion['user']['username'] = ucwords($imageDiscussion['user']['username']) . ($contractorInfo['business_name'] ? ' - ' . ucwords($contractorInfo['business_name']) : '');
							} else {
								$imageDiscussion['user']['username'] = ucwords($imageDiscussion['user']['username']);				
							}
						}
					}

					
				}
			}
			$this->view->bookingDiscussions = $bookingDiscussions;		
			/////////////////////////////////////////////////////
			//invoiceDiscussions
			$invoiceDiscussions = $modelInvoiceDiscussion->getByInvoiceId($invoiceId, 'DESC');
			foreach ($invoiceDiscussions as &$invoiceDiscussion) {
				$invoiceDiscussion['user'] = $modelUser->getById($invoiceDiscussion['user_id']);
				$invoiceDiscussion['discussion_date'] = getDateFormating( $invoiceDiscussion['created']);
				$invoiceDiscussion['user_message'] = nl2br(htmlentities($invoiceDiscussion['user_message'], ENT_NOQUOTES, 'UTF-8'));
				if ($invoiceDiscussion['user']['role_name'] == 'contractor') {
					$contractorInfo = $modelContractorInfo->getByContractorId($invoiceDiscussion['user']['user_id']);
					$invoiceDiscussion['user']['username'] = ucwords($invoiceDiscussion['user']['username']) . ($contractorInfo['business_name'] ? ' - ' . ucwords($contractorInfo['business_name']) : '');
				} else {
					$invoiceDiscussion['user']['username'] = ucwords($invoiceDiscussion['user']['username']);				
				}
			}
			$this->view->invoiceDiscussions = $invoiceDiscussions;
			/////////////////////////////////////////////////////
			
			//estimateDiscussions
			$estimateDiscussions = $modelEstimateDiscussion->getByEstimateId($estimateId, 'DESC');
			$GroupsEstimateImageDiscussions = $modelEstimateDiscussion->getByEstimateId($estimateId, 'DESC', array('groups' => 'groups'));
			$estimateDiscussions = array_merge($estimateDiscussions, $GroupsEstimateImageDiscussions);
			array_multisort($estimateDiscussions, SORT_DESC);
			foreach ($estimateDiscussions as &$estimateDiscussion) {
				$estimateDiscussion['user'] = $modelUser->getById($estimateDiscussion['user_id']);
				$estimateDiscussion['discussion_date'] = getDateFormating( $estimateDiscussion['created']);
				$estimateDiscussion['user_message'] = nl2br(htmlentities($estimateDiscussion['user_message'], ENT_NOQUOTES, 'UTF-8'));
				
				if($estimateDiscussion['group_id']){
					//$grouped_images in tabs.phtml
					$estimateDiscussion['estimate_grouped_images'] = $modelImageAttachment->getByGroupIdAndType($estimateDiscussion['group_id'], $estimateId, 'estimate');
					foreach($estimateDiscussion['estimate_grouped_images'] as &$estimateGroupedImage){
						$estimateGroupedImage['imageDiscussions'] = $modelEstimateDiscussion->getByImageId($estimateGroupedImage['image_id'], 'DESC');
						if($estimateGroupedImage['imageDiscussions']){	
							foreach ($estimateGroupedImage['imageDiscussions'] as &$estimateImageDiscussion) {
									$estimateImageDiscussion['user'] = $modelUser->getById($estimateImageDiscussion['user_id']);
									if ($estimateImageDiscussion['user']['role_name'] == 'contractor') {
										$contractorInfo = $modelContractorInfo->getByContractorId($estimateImageDiscussion['user']['user_id']);
										$estimateImageDiscussion['user']['username'] = ucwords($estimateImageDiscussion['user']['username']) . ($contractorInfo['business_name'] ? ' - ' . ucwords($contractorInfo['business_name']) : '');
									} else {
										$estimateImageDiscussion['user']['username'] = ucwords($estimateImageDiscussion['user']['username']);
									}
							
							
								$estimateImageDiscussion['discussion_date'] = getDateFormating( $estimateImageDiscussion['created']);
								$estimateImageDiscussion['user_message'] = nl2br(htmlentities($estimateImageDiscussion['user_message'], ENT_NOQUOTES, 'UTF-8'));
								
							}	
						}
					}
				}else if(isset($estimateDiscussion['thumbnail_path'])){
					$estimateDiscussion['imageDiscussions'] = $modelEstimateDiscussion->getByImageId($estimateDiscussion['image_id'], 'DESC');	

					if($estimateDiscussion['imageDiscussions']){	
						foreach ($estimateDiscussion['imageDiscussions'] as &$estimateImageDiscussion) {
							$imageDiscussion['user'] = $modelUser->getById($imageDiscussion['user_id']);
							$imageDiscussion['discussion_date'] = getDateFormating( $imageDiscussion['created']);
							$imageDiscussion['user_message'] = nl2br(htmlentities($imageDiscussion['user_message'], ENT_NOQUOTES, 'UTF-8'));
							if ($imageDiscussion['user']['role_name'] == 'contractor') {
								$contractorInfo = $modelContractorInfo->getByContractorId($imageDiscussion['user']['user_id']);
								$imageDiscussion['user']['username'] = ucwords($imageDiscussion['user']['username']) . ($contractorInfo['business_name'] ? ' - ' . ucwords($contractorInfo['business_name']) : '');
							} else {
								$imageDiscussion['user']['username'] = ucwords($imageDiscussion['user']['username']);				
							}
						}	
					}
				}
			}
			
			$this->view->estimateDiscussions = $estimateDiscussions;
			/////////////////////////////////////////////////////
			
			//inquiryDiscussions
			$inquiryDiscussions = $modelInquiryDiscussion->getByInquiryId($inquiryId, 'DESC');
			$GroupsinquiryImageDiscussions = $modelInquiryDiscussion->getByInquiryId($inquiryId, 'DESC', array('groups' => 'groups'));
			$inquiryDiscussions = array_merge($inquiryDiscussions, $GroupsinquiryImageDiscussions);
			array_multisort($inquiryDiscussions, SORT_DESC);
			foreach ($inquiryDiscussions as &$inquiryDiscussion) {
				$inquiryDiscussion['discussion_date'] = getDateFormating( $inquiryDiscussion['created']);
				$inquiryDiscussion['user_message'] = nl2br(htmlentities($inquiryDiscussion['user_message'], ENT_NOQUOTES, 'UTF-8'));
				if (isset($inquiryDiscussion['user_role']) && $inquiryDiscussion['user_role'] == 9) {
					$modelCustomer = new Model_Customer();
					$inquiryDiscussion['user'] = $modelCustomer->getById($inquiryDiscussion['user_id']);
					$inquiryDiscussion['user']['username'] = $inquiryDiscussion['user']['first_name'] . ' ' . $inquiryDiscussion['user']['last_name'];
				} else {
					$inquiryDiscussion['user'] = $modelUser->getById($inquiryDiscussion['user_id']);
					$inquiryDiscussion['user']['username'] = ucwords($inquiryDiscussion['user']['username']);				
				}
				
				if($inquiryDiscussion['group_id']){
					//$grouped_images in tabs.phtml
					$inquiryDiscussion['inquiry_grouped_images'] = $modelImageAttachment->getByGroupIdAndType($inquiryDiscussion['group_id'], $inquiryId, 'inquiry');
					foreach($inquiryDiscussion['inquiry_grouped_images'] as &$inquiryGroupedImage){
						$inquiryGroupedImage['imageDiscussions'] = $modelInquiryDiscussion->getByImageId($inquiryGroupedImage['image_id'], 'DESC');
						if($inquiryGroupedImage['imageDiscussions']){	
							foreach ($inquiryGroupedImage['imageDiscussions'] as &$inquiryImageDiscussion) {
								$inquiryImageDiscussion['discussion_date'] = getDateFormating( $inquiryImageDiscussion['created']);
								$inquiryImageDiscussion['user_message'] = nl2br(htmlentities($inquiryImageDiscussion['user_message'], ENT_NOQUOTES, 'UTF-8'));
								
								if (isset($inquiryImageDiscussion['user_role']) && $inquiryImageDiscussion['user_role'] == $modelAuthRole->getRoleIdByName('customer')) {
									$modelCustomer = new Model_Customer();
									$inquiryImageDiscussion['user'] = $modelCustomer->getById($inquiryImageDiscussion['user_id']);
									$username = $inquiryImageDiscussion['user']['first_name'] . ' ' . $inquiryImageDiscussion['user']['last_name'];
								} else {
									$inquiryImageDiscussion['user'] = $modelUser->getById($inquiryImageDiscussion['user_id']);
									$username = $inquiryImageDiscussion['user']['username'];
								}
								
								if ($inquiryImageDiscussion['user']['role_id'] == $modelAuthRole->getRoleIdByName('contractor')){
									$contractorInfo = $modelContractorInfo->getByContractorId($inquiryImageDiscussion['user']['user_id']);
									$inquiryImageDiscussion['user']['username'] = ucwords($inquiryImageDiscussion['user']['username']) . ($contractorInfo['business_name'] ? ' - ' . ucwords($contractorInfo['business_name']) : '');
								}else{
									$inquiryImageDiscussion['user']['username'] = ucwords($username);
								} 
								
							}
						}						
					
						
					} 
				}else if(isset($inquiryDiscussion['thumbnail_path'])){
					$inquiryDiscussion['imageDiscussions'] = $modelInquiryDiscussion->getByImageId($inquiryDiscussion['image_id'], 'DESC');

					if($inquiryDiscussion['imageDiscussions']){
						foreach($inquiryDiscussion['imageDiscussions'] as $inquiryImageDiscussion){
							
							$inquiryImageDiscussion['discussion_date'] = getDateFormating( $inquiryImageDiscussion['created']);
							$inquiryImageDiscussion['user_message'] = nl2br(htmlentities($inquiryImageDiscussion['user_message'], ENT_NOQUOTES, 'UTF-8'));
							
							if (isset($inquiryImageDiscussion['user_role']) && $inquiryImageDiscussion['user_role'] == $modelAuthRole->getRoleIdByName('customer')) {
								$modelCustomer = new Model_Customer();
								$inquiryImageDiscussion['user'] = $modelCustomer->getById($inquiryImageDiscussion['user_id']);
								$username = $inquiryImageDiscussion['user']['first_name'] . ' ' . $inquiryImageDiscussion['user']['last_name'];
							} else {
								$inquiryImageDiscussion['user'] = $modelUser->getById($inquiryImageDiscussion['user_id']);
								$username = $inquiryImageDiscussion['user']['username'];
							}
							
							if (isset($inquiryImageDiscussion['user']['role_id']) && $inquiryImageDiscussion['user']['role_id'] == $modelAuthRole->getRoleIdByName('contractor')){
								$contractorInfo = $modelContractorInfo->getByContractorId($inquiryImageDiscussion['user']['user_id']);
								$inquiryImageDiscussion['user']['username'] = ucwords($inquiryImageDiscussion['user']['username']) . ($contractorInfo['business_name'] ? ' - ' . ucwords($contractorInfo['business_name']) : '');
							}else{
								$inquiryImageDiscussion['user']['username'] = ucwords($username);
							} 
						}
					}
				}
			}
			
			$this->view->inquiryDiscussions = $inquiryDiscussions;
		}else if($inquiry_id && (CheckAuth::getRoleName() != 'customer')){
			//inquiryDiscussions
			$inquiryDiscussions = $modelInquiryDiscussion->getByInquiryId($inquiry_id, 'DESC');
			$GroupsinquiryImageDiscussions = $modelInquiryDiscussion->getByInquiryId($inquiry_id, 'DESC', array('groups' => 'groups'));
			$inquiryDiscussions = array_merge($inquiryDiscussions, $GroupsinquiryImageDiscussions);
			array_multisort($inquiryDiscussions, SORT_DESC);
			//var_dump($inquiryDiscussions);
			foreach ($inquiryDiscussions as &$inquiryDiscussion) {
				$inquiryDiscussion['discussion_date'] = getDateFormating( $inquiryDiscussion['created']);
				$inquiryDiscussion['user_message'] = nl2br(htmlentities($inquiryDiscussion['user_message'], ENT_NOQUOTES, 'UTF-8'));
				if (isset($inquiryDiscussion['user_role']) && $inquiryDiscussion['user_role'] == 9) {
					$modelCustomer = new Model_Customer();
					$inquiryDiscussion['user'] = $modelCustomer->getById($inquiryDiscussion['user_id']);
					$inquiryDiscussion['user']['username'] = $inquiryDiscussion['user']['first_name'] . ' ' . $inquiryDiscussion['user']['last_name'];
				} else {
					$inquiryDiscussion['user'] = $modelUser->getById($inquiryDiscussion['user_id']);
					$inquiryDiscussion['user']['username'] = ucwords($inquiryDiscussion['user']['username']);				
				}
				
				if($inquiryDiscussion['group_id']){
					//$grouped_images in tabs.phtml
					$inquiryDiscussion['inquiry_grouped_images'] = $modelImageAttachment->getByGroupIdAndType($inquiryDiscussion['group_id'], $inquiryId, 'inquiry');
					foreach($inquiryDiscussion['inquiry_grouped_images'] as &$inquiryGroupedImage){
						$inquiryGroupedImage['imageDiscussions'] = $modelInquiryDiscussion->getByImageId($inquiryGroupedImage['image_id'], 'DESC');
						if($inquiryGroupedImage['imageDiscussions']){	
							foreach ($inquiryGroupedImage['imageDiscussions'] as &$inquiryImageDiscussion) {
								$inquiryImageDiscussion['discussion_date'] = getDateFormating( $inquiryImageDiscussion['created']);
								$inquiryImageDiscussion['user_message'] = nl2br(htmlentities($inquiryImageDiscussion['user_message'], ENT_NOQUOTES, 'UTF-8'));
								
								if (isset($inquiryImageDiscussion['user_role']) && $inquiryImageDiscussion['user_role'] == $modelAuthRole->getRoleIdByName('customer')) {
									$modelCustomer = new Model_Customer();
									$inquiryImageDiscussion['user'] = $modelCustomer->getById($inquiryImageDiscussion['user_id']);
									$username = $inquiryImageDiscussion['user']['first_name'] . ' ' . $inquiryImageDiscussion['user']['last_name'];
								} else {
									$inquiryImageDiscussion['user'] = $modelUser->getById($inquiryImageDiscussion['user_id']);
									$username = $inquiryImageDiscussion['user']['username'];
								}
								
								if ($inquiryImageDiscussion['user']['role_id'] == $modelAuthRole->getRoleIdByName('contractor')){
									$contractorInfo = $modelContractorInfo->getByContractorId($inquiryImageDiscussion['user']['user_id']);
									$inquiryImageDiscussion['user']['username'] = ucwords($inquiryImageDiscussion['user']['username']) . ($contractorInfo['business_name'] ? ' - ' . ucwords($contractorInfo['business_name']) : '');
								}else{
									$inquiryImageDiscussion['user']['username'] = ucwords($username);
								} 
								
							}
						}						
					
						
					} 
				}else if(isset($inquiryDiscussion['thumbnail_path'])){
					$inquiryDiscussion['imageDiscussions'] = $modelInquiryDiscussion->getByImageId($inquiryDiscussion['image_id'], 'DESC');

					if($inquiryDiscussion['imageDiscussions']){
						foreach($inquiryDiscussion['imageDiscussions'] as $inquiryImageDiscussion){
							
							$inquiryImageDiscussion['discussion_date'] = getDateFormating( $inquiryImageDiscussion['created']);
							$inquiryImageDiscussion['user_message'] = nl2br(htmlentities($inquiryImageDiscussion['user_message'], ENT_NOQUOTES, 'UTF-8'));
							
							if (isset($inquiryImageDiscussion['user_role']) && $inquiryImageDiscussion['user_role'] == $modelAuthRole->getRoleIdByName('customer')) {
								$modelCustomer = new Model_Customer();
								$inquiryImageDiscussion['user'] = $modelCustomer->getById($inquiryImageDiscussion['user_id']);
								$username = $inquiryImageDiscussion['user']['first_name'] . ' ' . $inquiryImageDiscussion['user']['last_name'];
							} else {
								$inquiryImageDiscussion['user'] = $modelUser->getById($inquiryImageDiscussion['user_id']);
								$username = $inquiryImageDiscussion['user']['username'];
							}
							
							if (isset($inquiryImageDiscussion['user']['role_id']) && $inquiryImageDiscussion['user']['role_id'] == $modelAuthRole->getRoleIdByName('contractor')){
								$contractorInfo = $modelContractorInfo->getByContractorId($inquiryImageDiscussion['user']['user_id']);
								$inquiryImageDiscussion['user']['username'] = ucwords($inquiryImageDiscussion['user']['username']) . ($contractorInfo['business_name'] ? ' - ' . ucwords($contractorInfo['business_name']) : '');
							}else{
								$inquiryImageDiscussion['user']['username'] = ucwords($username);
							} 
						}
					}
				}
			}
			
			$this->view->inquiryDiscussions = $inquiryDiscussions;
		}
		if($complaintId && (CheckAuth::getRoleName() != 'customer')){
			//Complaint discussion 
			
			$complaintDiscussions = $modelComplaintDiscussion->getByComplaintId($complaintId);
			$GroupscomplaintImageDiscussions = $modelComplaintDiscussion->getByComplaintId($complaintId, 'created DESC', array('groups' => 'groups'));
			$complaintDiscussions = array_merge($complaintDiscussions, $GroupscomplaintImageDiscussions);
			array_multisort($complaintDiscussions, SORT_DESC);
			foreach ($complaintDiscussions as &$complaintDiscussion) {
				$complaintDiscussion['user'] = $modelUser->getById($complaintDiscussion['user_id']);
				$complaintDiscussion['discussion_date'] = getDateFormating( $complaintDiscussion['created']);
				$complaintDiscussion['user_message'] = nl2br(htmlentities($complaintDiscussion['user_message'], ENT_NOQUOTES, 'UTF-8'));
				
				if($complaintDiscussion['group_id']){
					//$grouped_images in tabs.phtml
					$complaintDiscussion['complaint_grouped_images'] = $modelImageAttachment->getByGroupIdAndType($complaintDiscussion['group_id'], $complaintId, 'complaint');
					foreach($complaintDiscussion['complaint_grouped_images'] as &$complaintGroupedImage){
						$complaintGroupedImage['imageDiscussions'] = $modelComplaintDiscussion->getDiscussionByImageId($complaintGroupedImage['image_id'], 'DESC');
						
						if($complaintGroupedImage['imageDiscussions']){	
							foreach ($complaintGroupedImage['imageDiscussions'] as &$complaintImageDiscussion) {
								$complaintImageDiscussion['user'] = $modelUser->getById($complaintImageDiscussion['user_id']);
								if ($complaintImageDiscussion['user']['role_name'] == 'contractor') {
									$contractorInfo = $modelContractorInfo->getByContractorId($complaintImageDiscussion['user']['user_id']);
									$complaintImageDiscussion['user']['username'] = ucwords($complaintImageDiscussion['user']['username']) . ($contractorInfo['business_name'] ? ' - ' . ucwords($contractorInfo['business_name']) : '');
								} else {
									$complaintImageDiscussion['user']['username'] = ucwords($complaintImageDiscussion['user']['username']);
								}
							
							
								$complaintImageDiscussion['discussion_date'] = getDateFormating( $complaintImageDiscussion['created']);
								$complaintImageDiscussion['user_message'] = nl2br(htmlentities($complaintImageDiscussion['user_message'], ENT_NOQUOTES, 'UTF-8'));
								
							}	
						}
						
					}
				}else if(isset($complaintDiscussion['thumbnail_path'])){
					$complaintDiscussion['imageDiscussions'] = $modelComplaintDiscussion->getDiscussionByImageId($complaintDiscussion['image_id'], 'DESC');	
					
					if($complaintDiscussion['imageDiscussions']){	
						foreach ($complaintDiscussion['imageDiscussions'] as &$complaintImageDiscussion) {
							$complaintImageDiscussion['user'] = $modelUser->getById($complaintImageDiscussion['user_id']);
							if ($complaintImageDiscussion['user']['role_name'] == 'contractor') {
								$contractorInfo = $modelContractorInfo->getByContractorId($complaintImageDiscussion['user']['user_id']);
								$complaintImageDiscussion['user']['username'] = ucwords($complaintImageDiscussion['user']['username']) . ($contractorInfo['business_name'] ? ' - ' . ucwords($contractorInfo['business_name']) : '');
							} else {
								$complaintImageDiscussion['user']['username'] = ucwords($complaintImageDiscussion['user']['username']);
							}
						
						
							$complaintImageDiscussion['discussion_date'] = getDateFormating( $complaintImageDiscussion['created']);
							$complaintImageDiscussion['user_message'] = nl2br(htmlentities($complaintImageDiscussion['user_message'], ENT_NOQUOTES, 'UTF-8'));
							
						}	
					}
				}
			}
		
			$this->view->complaintDiscussions = $complaintDiscussions;
			
		}
		
		
		
		
        $json_array = array(
			'result' => $this->view->render('discussion/get-all-tabs-discussion.phtml'),
			'countAll' => count($bookingDiscussions) + count($invoiceDiscussions) + count($estimateDiscussions) + count($inquiryDiscussions) + count($complaintDiscussions)
        );

        echo json_encode($json_array);
        exit;
       
	
	}
	
	
	public function updateDeletedCommentAction(){
		$discussion_id = $this->request->getParam('discussion_id');
		$booking_id = $this->request->getParam('booking_id');
		$have_images = $this->request->getParam('have_images', 0);
		$image_id = $this->request->getParam('image_id',0);
		$group_id = $this->request->getParam('group_id',0);
		$success = 0;
		
		$modelBookingDiscussion = new Model_BookingDiscussion();
		
		$data = array(
            'is_deleted' => 1             
        );
		
		$success = $modelBookingDiscussion->updateById($discussion_id, $data);
		if($have_images){
			
			$successDelete = 0;
			$successDeletes = array();
			$modelImage = new Model_Image();
			
			if($image_id){
				
				$successDelete = $modelImage->deleteById($image_id);
			}else if($group_id){
							
				$booking_grouped_images = $modelImage->getByGroupIdAndType($group_id, $booking_id, 'booking');
 
				//var_dump($booking_grouped_images);
				foreach($booking_grouped_images as $k => $img){
					$successDeletes[$k] = $modelImage->deleteById($img['image_id']);
				}
			}
			
			if($success && ($successDelete || !in_array(0,$successDeletes))){
				echo 1;				
			}
			exit;
		}
		
		if($success){
			echo 1;
		}
		exit;
		
	}

}

