<?php

class Booking_DiscussionController extends Zend_Controller_Action {

    private $request;
    private $router;
    private $loggedUser;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        //
        //checkLoggedIn
        //
        CheckAuth::checkLoggedIn();

        //
        // check Auth for logged user
        //
        //CheckAuth::checkPermission(array('bookingDiscussion'));
        //
        //get Logged User
        //
        $this->loggedUser = CheckAuth::getLoggedUser();
    }

    public function indexAction() {

        $item_id = $this->request->getParam('id', 0);
        $type = $this->request->getParam('type', '');
        $process = $this->request->getParam('process', 'send');
        //
        // load model
        //
		
		//echo "item_id: " . $item_id . " type: " . $type . " process: " . $process;

        $this->view->item_id = $item_id;
        $this->view->type = $type;
        $this->view->process = $process;

        $modelBookingDiscussion = new Model_BookingDiscussion();
        $modelComplaintDiscussion = new Model_ComplaintDiscussion();
        $modelEstimateDiscussion = new Model_EstimateDiscussion();
        $modelInquiryDiscussion = new Model_InquiryDiscussion();
        $modelInvoiceDiscussion = new Model_InvoiceDiscussion();
        $modelComplaint = new Model_Complaint();
        $modelImage = new Model_Image();

        $loggedUser = $this->loggedUser;
        $isContractor = false;

        $modelAuthRole = new Model_AuthRole();
        $contractorRoleId = $modelAuthRole->getRoleIdByName('contractor');
        if ($loggedUser['role_id'] == $contractorRoleId) {
            $isContractor = true;
        }

        if ($type == 'booking') {
            CheckAuth::checkPermission(array('bookingDiscussion'));
            $modelBooking = new Model_Booking();
            if (!$modelBooking->checkIfCanSeeBookingWithOutTimePeriod($item_id)) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view this booking"));
                $this->_redirect($this->router->assemble(array(), 'booking'));
            }
            $booking = $modelBooking->getById($item_id);
            if (!$booking) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Booking not found"));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            }
            //$this->view->discussions = $modelBookingDiscussion->getByBookingId($item_id, $isContractor);
            $discussions = $modelBookingDiscussion->getByBookingId($item_id, 'DESC');
        } else if ($type == 'estimate') {
            CheckAuth::checkPermission(array('estimateDiscussion'));
            $modelBookingEstimate = new Model_BookingEstimate;

            $estimate = $modelBookingEstimate->getById($item_id);

            if (!$estimate) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "There Is No Item Exists"));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            }
            //$this->view->discussions = $modelEstimateDiscussion->getByEstimateId($item_id, $isContractor);
            $discussions = $modelEstimateDiscussion->getByEstimateId($item_id, 'DESC');
        } else if ($type == 'inquiry') {
            CheckAuth::checkPermission(array('inquiryDiscussion'));
            $modelInquiry = new Model_Inquiry();
            if (!$modelInquiry->checkIfCanSeeInquiry($item_id)) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
                $this->_redirect($this->router->assemble(array(), 'inquiry'));
            }

            $inquiry = $modelInquiry->getById($item_id);

            if (!$inquiry) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "There Is No Item Exists"));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            }

            //$this->view->discussions = $modelInquiryDiscussion->getByInquiryId($item_id, $isContractor);
            $discussions = $modelInquiryDiscussion->getByInquiryId($item_id, 'DESC');
        } else if ($type == 'invoice') {
            CheckAuth::checkPermission(array('invoiceDiscussion'));
            $paymentId = $this->request->getParam('payment_id', 0);
            $modelInvoice = new Model_BookingInvoice();
            $modelPayment = new Model_Payment();

            $invoice = $modelInvoice->getById($item_id);

            if (!$invoice) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "There Is No Item Exists"));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            }

            if ($paymentId != 0) {
                $payment = $modelPayment->getById($paymentId);
                $this->view->payment = $payment;
            }

            //$this->view->discussions = $modelInvoiceDiscussion->getByInvoiceId($item_id, $isContractor);
            $discussions = $modelInvoiceDiscussion->getByInvoiceId($item_id, 'DESC');
        } else if ($type == 'complaint') {
            CheckAuth::checkPermission(array('complaintDiscussion'));
            //$process = $this->request->getParam('process', 'send');
            $complaint = $modelComplaint->getById($item_id);

            if (!$complaint) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Complaint not exist"));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            }

            //$this->view->discussions = $modelComplaintDiscussion->getByComplaintId($item_id, $isContractor);
            $discussions = $modelComplaintDiscussion->getByComplaintId($item_id, 'DESC');
        }

        if ($discussions) {

            $modelItemImageDiscussion = new Model_ItemImageDiscussion();
            foreach ($discussions as &$discussion) {
                $itemImageDiscussion = $modelItemImageDiscussion->getByDiscussionIdAndType($discussion['discussion_id'], $type);
                if ($itemImageDiscussion) {
                    if ($itemImageDiscussion[0]['group_id'] != 0) {
                        $discussion['group_id'] = $itemImageDiscussion[0]['group_id'];
                        $discussion['images'] = $modelImage->getByGroupId($itemImageDiscussion[0]['group_id']);
                    } else if ($itemImageDiscussion[0]['image_id'] != 0) {
                        $image = $modelImage->getByImageId($itemImageDiscussion[0]['image_id']);
                        $discussion['image_id'] = $itemImageDiscussion[0]['image_id'];
                        $imageAfterProccessing = array();
                        $imageAfterProccessing[] = $image;
                        $discussion['images'] = $imageAfterProccessing;
                        //var_dump($image);
                    }
                }
            }
        }

        $this->view->discussions = $discussions;

        $adminRoleId = $modelAuthRole->getRoleIdByName('super_admin');
        $manggerRoleId = $modelAuthRole->getRoleIdByName('manager');
        $salesRoleId = $modelAuthRole->getRoleIdByName('sales');
        //echo $adminRoleId . $manggerRoleId . $salesRoleId;exit;

        $filters['roles'] = array($adminRoleId, $manggerRoleId, $salesRoleId);
        $filters['active'] = 'TRUE';
        $filters['logged_user_id'] = $loggedUser['user_id'];

        $modelUser = new Model_User();
        $users = $modelUser->getAll($filters);
        $this->view->users = $users;

        echo $this->view->render('discussion/index.phtml');
        exit;
    }

    public function submitAction() {
        //get Params
        //
        $item_id = $this->request->getParam('id', 0);
        $type = $this->request->getParam('type', 0);
        $discussion = $this->request->getParam('discussion', '');
        $visibility = $this->request->getParam('visibility', 1);
        $notifyPersons = $this->request->getParam('notifyPersons', '');
        $imageId = $this->request->getParam('imageId', 0);
        $notifyusersString = $notifyCustomersString = '';
        $notifyCustomers = array();
        $notifyUsers = array();

        if ($type == 'booking') {
            CheckAuth::checkPermission(array('bookingDiscussion'));
        } else if ($type == 'estimate') {
            CheckAuth::checkPermission(array('estimateDiscussion'));
        } else if ($type == 'inquiry') {
            CheckAuth::checkPermission(array('inquiryDiscussion'));
        } else if ($type == 'invoice') {
            CheckAuth::checkPermission(array('invoiceDiscussion'));
        } else if ($type == 'complaint') {
            CheckAuth::checkPermission(array('complaintDiscussion'));
        }

        $modelBookingDiscussion = new Model_BookingDiscussion();
        $modelComplaintDiscussion = new Model_ComplaintDiscussion();
        $modelEstimateDiscussion = new Model_EstimateDiscussion();
        $modelInquiryDiscussion = new Model_InquiryDiscussion();
        $modelInvoiceDiscussion = new Model_InvoiceDiscussion();

        if ($notifyPersons) {
            $notify = explode(',', $notifyPersons);


            foreach ($notify as $value) {
                if (substr($value, 0, 1) == 'c') {
                    $id = substr($value, 1);
                    $notifyCustomers[] = $id;
                } else {
                    $notifyUsers[] = $value;
                }
            }

            $notifyusersString = $notifyUsers;
            $notifyCustomersString = implode(',', $notifyCustomers);
        }

        if ($type == 'complaint') {
            $process = $this->request->getParam('process', 'send');
            if ($process == 'open' || $process == 'close') {
                //
                // check Auth for logged user
                //
				CheckAuth::checkPermission(array('complaintConvertStatus'));

                if (!CheckAuth::checkIfCanHandelAllCompany('complaint', $item_id)) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Booking not belongs to your Company"));
                    $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
                }
            }
        }

        $imagesAfterProcessing = array();

        //
        // load model
        //
		$modelComplaint = new Model_Complaint();
        $modelImage = new Model_Image();
        $modelUser = new Model_User();
        $modelContractorInfo = new Model_ContractorInfo();
        //$modelComplaint = new Model_Complaint();
        $modelComplaintTemp = new Model_ComplaintTemp();

        $success = 0;
        if ($discussion) {

            $user = $modelUser->getById($this->loggedUser['user_id']);
            if ($user['role_name'] == 'contractor') {
                $contractorInfo = $modelContractorInfo->getByContractorId($user['user_id']);
                $user['username'] = ucwords($user['username']) . ' - ' . ucwords($contractorInfo['business_name']);
            } else {
                $user['username'] = ucwords($user['username']);
            }

            $upload = new Zend_File_Transfer_Adapter_Http();
            $files = $upload->getFileInfo();


            $counter = 0;
            if (isset($files) && !empty($files)) {
                $countFiles = count($files);
                $image_group = 0;
                if ($countFiles > 1) {
                    $maxGroup = max($modelImage->getMaxGroup());
                    $image_group = $maxGroup['group_id'] + 1;
                    if ($type == 'booking') {
                        $doc = array(
                            'booking_id' => (int) $item_id,
                            'user_id' => $this->loggedUser['user_id'],
                            'user_name' => $user['username'],
                            'avatar' => $user['avatar'],
                            'user_message' => $discussion,
                            'created' => time(),
                            'user_role_name' => $user['role_name'],
                            'user_role' => $this->loggedUser['role_id'],
                            'seen_by_ids' => "",
                            'seen_by_names' => "",
                            'visibility' => (int) $visibility,
                            'visited_extra_info_id' => 0
                        );
                        $success = $modelBookingDiscussion->insert($doc);
                    } else if ($type == 'estimate') {
                        $doc = array(
                            'estimate_id' => (int) $item_id,
                            'user_id' => $this->loggedUser['user_id'],
                            'user_name' => $user['username'],
                            'avatar' => $user['avatar'],
                            'user_message' => $discussion,
                            'created' => time(),
                            'user_role_name' => $user['role_name'],
                            'user_role' => $this->loggedUser['role_id'],
                            'seen_by_ids' => "",
                            'seen_by_names' => "",
                            'visibility' => (int) $visibility
                        );

                        $success = $modelEstimateDiscussion->insert($doc);
                    } else if ($type == 'inquiry') {
                        $doc = array(
                            'inquiry_id' => (int) $item_id,
                            'user_id' => $this->loggedUser['user_id'],
                            'user_name' => $user['username'],
                            'avatar' => $user['avatar'],
                            'user_message' => $discussion,
                            'created' => time(),
                            'user_role_name' => $user['role_name'],
                            'user_role' => $this->loggedUser['role_id'],
                            'seen_by_ids' => "",
                            'seen_by_names' => "",
                            'visibility' => (int) $visibility
                        );

                        $success = $modelInquiryDiscussion->insert($doc);
                    } else if ($type == 'complaint') {
                        $doc = array(
                            'complaint_id' => (int) $item_id,
                            'user_id' => $this->loggedUser['user_id'],
                            'user_name' => $user['username'],
                            'avatar' => $user['avatar'],
                            'user_message' => $discussion,
                            'created' => time(),
                            'user_role_name' => $user['role_name'],
                            'user_role' => $this->loggedUser['role_id'],
                            'seen_by_ids' => "",
                            'seen_by_names' => "",
                            'visibility' => (int) $visibility
                        );

                        $success = $modelComplaintDiscussion->insert($doc);
                    } else if ($type == 'invoice') {

                        $doc = array(
                            'invoice_id' => (int) $item_id,
                            'user_id' => $this->loggedUser['user_id'],
                            'user_name' => $user['username'],
                            'avatar' => $user['avatar'],
                            'user_message' => $discussion,
                            'created' => time(),
                            'user_role_name' => $user['role_name'],
                            'user_role' => $this->loggedUser['role_id'],
                            'seen_by_ids' => "",
                            'seen_by_names' => "",
                            'visibility' => (int) $visibility
                        );

                        $paymentId = $this->request->getParam('payment_id', 0);
                        if ($paymentId != 0) {
                            $doc['payment_id'] = $paymentId;

                            $modelPayment = new Model_Payment();
                            $payment = array('is_rejected' => 1);
                            $modelPayment->updateById($paymentId, $payment);
                        } else {
                            $doc['payment_id'] = 0;
                        }

                        $success = $modelInvoiceDiscussion->insert($doc);
                    }
                    $modelItemImageDiscussion = new Model_ItemImageDiscussion();
                    $dataDiscssion = array(
                        'image_id' => 0,
                        'group_id' => $image_group,
                        'item_id' => $success,
                        'type' => $type
                    );
                    $modelItemImageDiscussion->insert($dataDiscssion);
                }
                foreach ($files as $file => $fileInfo) {
                    if ($upload->isUploaded($file)) {
                        if ($upload->receive($file)) {

                            $info = $upload->getFileInfo($file);
                            $source = $info[$file]['tmp_name'];
                            $imageInfo = pathinfo($source);
                            $ext = $imageInfo['extension'];
                            $dir = get_config('image_attachment') . '/';
                            $subdir = date('Y/m/d/');
                            $fullDir = $dir . $subdir;
                            if (!is_dir($fullDir)) {
                                mkdir($fullDir, 0777, true);
                            }

                            $ip = $_SERVER['REMOTE_ADDR'];
                            $loggedUser = CheckAuth::getLoggedUser();

                            if (isset($loggedUser['user_id'])) {
                                $user_id = $loggedUser['user_id'];
                            } else if ($loggedUser['customer_id']) {
                                $user_id = $loggedUser['customer_id'];
                            } else {
                                $user_id = 0;
                            }

                            $data = array(
                                'user_ip' => $ip
                                , 'created_by' => $user_id
                                , 'image_types_id' => 0
                                , 'group_id' => $image_group
                                , 'user_role' => $loggedUser['role_id']
                            );



                            $id = $modelImage->insert($data);

                            $image_id = $id;
                            if ($countFiles > 1) {
                                $image_id = 0;
                            }
                            if ($countFiles == 1) {
                                if ($type == 'booking') {
                                    $doc = array(
                                        'booking_id' => (int) $item_id,
                                        'user_id' => $this->loggedUser['user_id'],
                                        'user_name' => $user['username'],
                                        'avatar' => $user['avatar'],
                                        'user_message' => $discussion,
                                        'created' => time(),
                                        'user_role_name' => $user['role_name'],
                                        'user_role' => $this->loggedUser['role_id'],
                                        'seen_by_ids' => "",
                                        'seen_by_names' => "",
                                        'visibility' => (int) $visibility,
                                        'visited_extra_info_id' => 0
                                    );

                                    $success = $modelBookingDiscussion->insert($doc);
                                } else if ($type == 'estimate') {
                                    $doc = array(
                                        'estimate_id' => (int) $item_id,
                                        'user_id' => $this->loggedUser['user_id'],
                                        'user_name' => $user['username'],
                                        'avatar' => $user['avatar'],
                                        'user_message' => $discussion,
                                        'created' => time(),
                                        'user_role_name' => $user['role_name'],
                                        'user_role' => $this->loggedUser['role_id'],
                                        'seen_by_ids' => "",
                                        'seen_by_names' => "",
                                        'visibility' => (int) $visibility
                                    );

                                    $success = $modelEstimateDiscussion->insert($doc);
                                } else if ($type == 'inquiry') {
                                    $doc = array(
                                        'inquiry_id' => (int) $item_id,
                                        'user_id' => $this->loggedUser['user_id'],
                                        'user_name' => $user['username'],
                                        'avatar' => $user['avatar'],
                                        'user_message' => $discussion,
                                        'created' => time(),
                                        'user_role_name' => $user['role_name'],
                                        'user_role' => $this->loggedUser['role_id'],
                                        'seen_by_ids' => "",
                                        'seen_by_names' => "",
                                        'visibility' => (int) $visibility
                                    );

                                    $success = $modelInquiryDiscussion->insert($doc);
                                } else if ($type == 'complaint') {
                                    $doc = array(
                                        'complaint_id' => (int) $item_id,
                                        'user_id' => $this->loggedUser['user_id'],
                                        'user_name' => $user['username'],
                                        'avatar' => $user['avatar'],
                                        'user_message' => $discussion,
                                        'created' => time(),
                                        'user_role_name' => $user['role_name'],
                                        'user_role' => $this->loggedUser['role_id'],
                                        'seen_by_ids' => "",
                                        'seen_by_names' => "",
                                        'visibility' => (int) $visibility
                                    );

                                    $success = $modelComplaintDiscussion->insert($doc);
                                } else if ($type == 'invoice') {

                                    $doc = array(
                                        'invoice_id' => (int) $item_id,
                                        'user_id' => $this->loggedUser['user_id'],
                                        'user_name' => $user['username'],
                                        'avatar' => $user['avatar'],
                                        'user_message' => $discussion,
                                        'created' => time(),
                                        'user_role_name' => $user['role_name'],
                                        'user_role' => $this->loggedUser['role_id'],
                                        'seen_by_ids' => "",
                                        'seen_by_names' => "",
                                        'visibility' => (int) $visibility
                                    );

                                    $paymentId = $this->request->getParam('payment_id', 0);
                                    if ($paymentId != 0) {
                                        $doc['payment_id'] = $paymentId;

                                        $modelPayment = new Model_Payment();
                                        $payment = array('is_rejected' => 1);
                                        $modelPayment->updateById($paymentId, $payment);
                                    } else {
                                        $doc['payment_id'] = 0;
                                    }

                                    $success = $modelInvoiceDiscussion->insert($doc);
                                }

                                $modelItemImageDiscussion = new Model_ItemImageDiscussion();
                                $dataDiscssion = array(
                                    'image_id' => $image_id,
                                    'group_id' => $image_group,
                                    'item_id' => $success,
                                    'type' => $type
                                );

                                $modelItemImageDiscussion->insert($dataDiscssion);
                            }

                            if ($type == 'booking') {
                                $Itemdata = array(
                                    'item_id' => $item_id,
                                    'service_id' => 0,
                                    'discussion_id' => $success,
                                    'image_id' => $id,
                                    'type' => 'booking_discussion',
                                    'floor_id' => 0
                                );


                                $modelItemImage = new Model_ItemImage();
                                $Item_image_id = $modelItemImage->insert($Itemdata);
                            }

                            $original_path = "original_{$id}.{$ext}";
                            $large_path = "large_{$id}.{$ext}";
                            $small_path = "small_{$id}.{$ext}";
                            $thumbnail_path = "thumbnail_{$id}.{$ext}";
                            $compressed_path = "compressed_{$id}.jpg";

                            $data = array(
                                'original_path' => $subdir . $original_path,
                                'large_path' => $subdir . $large_path,
                                'small_path' => $subdir . $small_path,
                                'thumbnail_path' => $subdir . $thumbnail_path,
                                'compressed_path' => $subdir . $compressed_path
                            );

                            $modelImage->updateById($id, $data);

                            //save image to database and filesystem here
                            $image_saved = copy($source, $fullDir . $original_path);
                            $compressed_saved = copy($source, $fullDir . $compressed_path);
                            ImageMagick::scale_image($source, $fullDir . $large_path, 510);
                            ImageMagick::scale_image($source, $fullDir . $small_path, 250);
                            ImageMagick::create_thumbnail($source, $fullDir . $thumbnail_path, 78, 64);
                            ImageMagick::convert($source, $fullDir . $compressed_path);
                            ImageMagick::compress_image($source, $fullDir . $compressed_path);


                            if ($image_saved) {
                                if (file_exists($source)) {
                                    unlink($source);
                                }
                            }
                        }
                    }
                }

                require_once 'Zend/Cache.php';
                $company_id = CheckAuth::getCompanySession();

                if ($type == 'booking') {

                    //D.A 30/08/2015 Remove Booking Photos Cache				
                    $bookingPhotosCacheID = $item_id . '_bookingPhotos';
                    $bookingViewDir = get_config('cache') . '/' . 'bookingsView' . '/' . $company_id;
                    if (!is_dir($bookingViewDir)) {
                        mkdir($bookingViewDir, 0777, true);
                    }
                    $frontEndOption = array('lifetime' => NULL,
                        'automatic_serialization' => true);
                    $backendOptions = array('cache_dir' => $bookingViewDir);
                    $Cache = Zend_Cache::factory('Core', 'File', $frontEndOption, $backendOptions);
                    $Cache->remove($bookingPhotosCacheID);
                    $modelBooking = new Model_Booking();
                    $modelBookingEstimate = new Model_BookingEstimate();
                    $booking = $modelBooking->getById($item_id);
                    $estimate = $modelBookingEstimate->getByBookingId($item_id);
                    if ($estimate) {
                        $estimateLabelsCacheID = $estimate['id'] . '_estimatePhoto';
                        $estimateViewDir = get_config('cache') . '/' . 'estimatesView' . '/' . $company_id;
                        if (!is_dir($estimateViewDir)) {
                            mkdir($estimateViewDir, 0777, true);
                        }
                        $estimatePhotoFrontEndOption = array('lifetime' => NULL,
                            'automatic_serialization' => true);
                        $estimatePhotoBackendOptions = array('cache_dir' => $estimateViewDir);
                        $estimatePhotoCache = Zend_Cache::factory('Core', 'File', $estimatePhotoFrontEndOption, $estimatePhotoBackendOptions);
                        $estimatePhotoCache->remove($estimateLabelsCacheID);
                    }
                    if ($booking['original_inquiry_id']) {
                        $inquiryPhotosCacheID = $booking['original_inquiry_id'] . '_inquiryPhotos';
                        $inquiryViewDir = get_config('cache') . '/' . 'inquiriesView' . '/' . $company_id;
                        if (!is_dir($inquiryViewDir)) {
                            mkdir($inquiryViewDir, 0777, true);
                        }
                        $inquiryPhotosFrontEndOption = array('lifetime' => NULL,
                            'automatic_serialization' => true);
                        $inquiryPhotosBackendOptions = array('cache_dir' => $inquiryViewDir);
                        $inquiryPhotosCache = Zend_Cache::factory('Core', 'File', $inquiryPhotosFrontEndOption, $inquiryPhotosBackendOptions);
                        $inquiryPhotosCache->remove($inquiryPhotosCacheID);
                    }
                }

                //D.A 14/09/2015 Remove inquiry Photos Cache
                if ($type == 'inquiry') {
                    $inquiryPhotosCacheID = $item_id . '_inquiryPhotos';
                    $inquiryViewDir = get_config('cache') . '/' . 'inquiriesView' . '/' . $company_id;
                    if (!is_dir($inquiryViewDir)) {
                        mkdir($inquiryViewDir, 0777, true);
                    }
                    $inquiryPhotosFrontEndOption = array('lifetime' => NULL,
                        'automatic_serialization' => true);
                    $inquiryPhotosBackendOptions = array('cache_dir' => $inquiryViewDir);
                    $inquiryPhotosCache = Zend_Cache::factory('Core', 'File', $inquiryPhotosFrontEndOption, $inquiryPhotosBackendOptions);
                    $inquiryPhotosCache->remove($inquiryPhotosCacheID);

                    //
                    $modelBooking = new Model_Booking();
                    $modelBookingEstimate = new Model_BookingEstimate();
                    $booking = $modelBooking->getByInquiryId($item_id);
                    if (isset($booking) && !empty($booking)) {
                        $bookingPhotosCacheID = $booking['booking_id'] . '_bookingPhotos';
                        $bookingViewDir = get_config('cache') . '/' . 'bookingsView' . '/' . $company_id;
                        if (!is_dir($bookingViewDir)) {
                            mkdir($bookingViewDir, 0777, true);
                        }
                        $frontEndOption = array('lifetime' => NULL,
                            'automatic_serialization' => true);
                        $backendOptions = array('cache_dir' => $bookingViewDir);
                        $Cache = Zend_Cache::factory('Core', 'File', $frontEndOption, $backendOptions);
                        $Cache->remove($bookingPhotosCacheID);
                        $estimate = $modelBookingEstimate->getByBookingId($booking['booking_id']);
                        if ($estimate) {
                            $estimatePhotosCacheID = $estimate['id'] . '_estimatePhoto';
                            $estimateViewDir = get_config('cache') . '/' . 'estimatesView' . '/' . $company_id;
                            if (!is_dir($estimateViewDir)) {
                                mkdir($estimateViewDir, 0777, true);
                            }
                            $estimatePhotoFrontEndOption = array('lifetime' => NULL,
                                'automatic_serialization' => true);
                            $estimatePhotoBackendOptions = array('cache_dir' => $estimateViewDir);
                            $estimatePhotoCache = Zend_Cache::factory('Core', 'File', $estimatePhotoFrontEndOption, $estimatePhotoBackendOptions);
                            $estimatePhotoCache->remove($estimatePhotosCacheID);
                        }
                    }
                }

                //D.A 20/09/2015 Remove Estimate Photos Cache
                if ($type == 'estimate') {
                    $modelBooking = new Model_Booking();
                    $modelBookingEstimate = new Model_BookingEstimate();
                    $estimate = $modelBookingEstimate->getById($item_id);
                    $booking = $modelBooking->getById($estimate['booking_id']);
                    if ($booking['original_inquiry_id']) {
                        $inquiryPhotosCacheID = $booking['original_inquiry_id'] . '_inquiryPhotos';
                        $inquiryViewDir = get_config('cache') . '/' . 'inquiriesView' . '/' . $company_id;
                        if (!is_dir($inquiryViewDir)) {
                            mkdir($inquiryViewDir, 0777, true);
                        }
                        $inquiryPhotosFrontEndOption = array('lifetime' => NULL,
                            'automatic_serialization' => true);
                        $inquiryPhotosBackendOptions = array('cache_dir' => $inquiryViewDir);
                        $inquiryPhotosCache = Zend_Cache::factory('Core', 'File', $inquiryPhotosFrontEndOption, $inquiryPhotosBackendOptions);
                        $inquiryPhotosCache->remove($inquiryPhotosCacheID);
                    }
                    $estimateLabelsCacheID = $item_id . '_estimatePhoto';
                    $estimateViewDir = get_config('cache') . '/' . 'estimatesView' . '/' . $company_id;
                    if (!is_dir($estimateViewDir)) {
                        mkdir($estimateViewDir, 0777, true);
                    }
                    $estimatePhotoFrontEndOption = array('lifetime' => NULL,
                        'automatic_serialization' => true);
                    $estimatePhotoBackendOptions = array('cache_dir' => $estimateViewDir);
                    $estimatePhotoCache = Zend_Cache::factory('Core', 'File', $estimatePhotoFrontEndOption, $estimatePhotoBackendOptions);
                    $estimatePhotoCache->remove($estimateLabelsCacheID);

                    //D.A 20/09/2015 Remove Booking Photos Cache

                    $bookingPhotosCacheID = $estimate['booking_id'] . '_bookingPhotos';
                    $bookingViewDir = get_config('cache') . '/' . 'bookingsView' . '/' . $company_id;
                    if (!is_dir($bookingViewDir)) {
                        mkdir($bookingViewDir, 0777, true);
                    }
                    $frontEndOption = array('lifetime' => NULL,
                        'automatic_serialization' => true);
                    $backendOptions = array('cache_dir' => $bookingViewDir);
                    $Cache = Zend_Cache::factory('Core', 'File', $frontEndOption, $backendOptions);
                    $Cache->remove($bookingPhotosCacheID);
                }

                /* require_once 'Zend/Cache.php';				
                  $company_id = CheckAuth::getCompanySession();

                  //D.A 30/08/2015 Remove Booking Photos Cache
                  $bookingPhotosCacheID = $item_id . '_bookingPhotos';
                  $bookingViewDir = get_config('cache') . '/' . 'bookingsView' . '/' . $company_id;
                  if (!is_dir($bookingViewDir)) {
                  mkdir($bookingViewDir, 0777, true);
                  }
                  $frontEndOption = array('lifetime' => NULL,
                  'automatic_serialization' => true);
                  $backendOptions = array('cache_dir' => $bookingViewDir);
                  $Cache = Zend_Cache::factory('Core', 'File', $frontEndOption, $backendOptions);
                  $Cache->remove($bookingPhotosCacheID);



                  $modelBooking = new Model_Booking();
                  $modelBookingEstimate = new Model_BookingEstimate();
                  $booking = $modelBooking->getById($item_id);
                  $estimate = $modelBookingEstimate->getByBookingId($item_id);
                  if ($estimate) {
                  $estimateLabelsCacheID = $estimate['id'] . '_estimatePhoto';
                  $estimateViewDir = get_config('cache') . '/' . 'estimatesView' . '/' . $company_id;
                  if (!is_dir($estimateViewDir)) {
                  mkdir($estimateViewDir, 0777, true);
                  }
                  $estimatePhotoFrontEndOption = array('lifetime' => NULL,
                  'automatic_serialization' => true);
                  $estimatePhotoBackendOptions = array('cache_dir' => $estimateViewDir);
                  $estimatePhotoCache = Zend_Cache::factory('Core', 'File', $estimatePhotoFrontEndOption, $estimatePhotoBackendOptions);
                  $estimatePhotoCache->remove($estimateLabelsCacheID);
                  }
                  if ($booking['original_inquiry_id']) {
                  $inquiryPhotosCacheID = $booking['original_inquiry_id'] . '_inquiryPhotos';
                  $inquiryViewDir = get_config('cache') . '/' . 'inquiriesView' . '/' . $company_id;
                  if (!is_dir($inquiryViewDir)) {
                  mkdir($inquiryViewDir, 0777, true);
                  }
                  $inquiryPhotosFrontEndOption = array('lifetime' => NULL,
                  'automatic_serialization' => true);
                  $inquiryPhotosBackendOptions = array('cache_dir' => $inquiryViewDir);
                  $inquiryPhotosCache = Zend_Cache::factory('Core', 'File', $inquiryPhotosFrontEndOption, $inquiryPhotosBackendOptions);
                  $inquiryPhotosCache->remove($inquiryPhotosCacheID);
                  } */
            } else {
                if ($type == 'booking') {
                    $doc = array(
                        'booking_id' => (int) $item_id,
                        'user_id' => $this->loggedUser['user_id'],
                        'user_name' => $user['username'],
                        'avatar' => $user['avatar'],
                        'user_message' => $discussion,
                        'created' => time(),
                        'user_role_name' => $user['role_name'],
                        'user_role' => $this->loggedUser['role_id'],
                        'seen_by_ids' => "",
                        'seen_by_names' => "",
                        'visibility' => (int) $visibility,
                        'visited_extra_info_id' => 0
                    );

                    $success = $modelBookingDiscussion->insert($doc);
                } else if ($type == 'estimate') {
                    $doc = array(
                        'estimate_id' => (int) $item_id,
                        'user_id' => $this->loggedUser['user_id'],
                        'user_name' => $user['username'],
                        'avatar' => $user['avatar'],
                        'user_message' => $discussion,
                        'created' => time(),
                        'user_role_name' => $user['role_name'],
                        'user_role' => $this->loggedUser['role_id'],
                        'seen_by_ids' => "",
                        'seen_by_names' => "",
                        'visibility' => (int) $visibility
                    );

                    $success = $modelEstimateDiscussion->insert($doc);
                } else if ($type == 'inquiry') {
                    $doc = array(
                        'inquiry_id' => (int) $item_id,
                        'user_id' => $this->loggedUser['user_id'],
                        'user_name' => $user['username'],
                        'avatar' => $user['avatar'],
                        'user_message' => $discussion,
                        'created' => time(),
                        'user_role_name' => $user['role_name'],
                        'user_role' => $this->loggedUser['role_id'],
                        'seen_by_ids' => "",
                        'seen_by_names' => "",
                        'visibility' => (int) $visibility
                    );

                    $success = $modelInquiryDiscussion->insert($doc);
                } else if ($type == 'complaint') {
                    $doc = array(
                        'complaint_id' => (int) $item_id,
                        'user_id' => $this->loggedUser['user_id'],
                        'user_name' => $user['username'],
                        'avatar' => $user['avatar'],
                        'user_message' => $discussion,
                        'created' => time(),
                        'user_role_name' => $user['role_name'],
                        'user_role' => $this->loggedUser['role_id'],
                        'seen_by_ids' => "",
                        'seen_by_names' => "",
                        'visibility' => (int) $visibility
                    );

                    $success = $modelComplaintDiscussion->insert($doc);
                } else if ($type == 'invoice') {

                    $doc = array(
                        'invoice_id' => (int) $item_id,
                        'user_id' => $this->loggedUser['user_id'],
                        'user_name' => $user['username'],
                        'avatar' => $user['avatar'],
                        'user_message' => $discussion,
                        'created' => time(),
                        'user_role_name' => $user['role_name'],
                        'user_role' => $this->loggedUser['role_id'],
                        'seen_by_ids' => "",
                        'seen_by_names' => "",
                        'visibility' => (int) $visibility
                    );

                    $paymentId = $this->request->getParam('payment_id', 0);
                    if ($paymentId != 0) {
                        $doc['payment_id'] = $paymentId;

                        $modelPayment = new Model_Payment();
                        $payment = array('is_rejected' => 1);
                        $modelPayment->updateById($paymentId, $payment);
                    } else {
                        $doc['payment_id'] = 0;
                    }

                    $success = $modelInvoiceDiscussion->insert($doc);
                }

                $modelItemImageDiscussion = new Model_ItemImageDiscussion();

                if ($type == 'booking' || $type == 'estimate' || $type == 'inquiry' || $type == 'complaint') {
                    $dataDiscssion = array(
                        'image_id' => $imageId,
                        'group_id' => 0,
                        'item_id' => $success,
                        'type' => $type
                    );

                    $modelItemImageDiscussion->insert($dataDiscssion);
                }
            }

            $updated = 0;
            if ($type == 'complaint') {
                $modelComplaint->sendComplaintAsEmailToContractor($item_id);

                ////change status
                //$updated = 0;
                if ($process == 'open') {
                    $data = array(
                        'complaint_status' => 'open'
                    );
                } elseif ($process == 'close') {
                    $data = array(
                        'complaint_status' => 'closed'
                    );
                }
                if ($process == 'close') {

                    $loggedUser = CheckAuth::getLoggedUser();
                    $user_id = !empty($loggedUser) ? $loggedUser['user_id'] : 0;

                    $modelAuthRole = new Model_AuthRole();
                    $contractor_role_id = $modelAuthRole->getRoleIdByName('contractor');
                    if ($contractor_role_id == $loggedUser['role_id']) {
                        $db_params = array();
                        $db_params['is_approved'] = 0;
                        $modelComplaint->updateById($item_id, $db_params);
                        $complaintTemp = $modelComplaintTemp->getByComplaintId($item_id);
                        $data['user_id'] = $this->loggedUser['user_id'];
                        if ($complaintTemp) {
                            $updated = $modelComplaintTemp->updateById($complaintTemp['id'], $data);
                        } else {
                            $added = $modelComplaintTemp->addComplaintTemp($item_id, $data);
                            if ($added) {
                                $updated = 1;
                            }
                        }
                    } else {
                        $updated = $modelComplaint->updateById($item_id, $data);
                    }
                } else if ($process == 'open') {

                    $complaintTemp = $modelComplaintTemp->getByComplaintId($item_id);
                    if ($complaintTemp) {
                        $modelComplaintTemp->deleteById($complaintTemp['id']);
                    }
                    $updated = $modelComplaint->updateById($item_id, array('complaint_status' => 'open', 'is_approved' => 1));
                }
            }

            if ($success) {
                $notifyContractor = 1;
                if ($visibility == 1) {
                    $notifyContractor = 0;
                }
                //echo $success;
                if ($type == 'booking') {
                    MobileNotificationNew::notify('booking discussion for notification', array('item_type' => 'booking', 'item_id' => $item_id, 'discussion_id' => $success, 'notify_multiple' => $notifyusersString, 'visibility' => $visibility));
                } else if ($type == 'estimate') {
                    MobileNotificationNew::notify('estimate discussion for notification', array('item_type' => $type, 'item_id' => $item_id, 'discussion_id' => $success, 'notify_multiple' => $notifyusersString,'visibility' => $visibility));
                } else if ($type == 'complaint') {
                    MobileNotificationNew::notify('complaint discussion for notification', array('item_type' => $type, 'item_id' => $item_id, 'discussion_id' => $success, 'notify_multiple' => $notifyusersString, 'visibility' => $visibility));
                } else if ($type == 'inquiry') {
                    MobileNotificationNew::notify('inquiry discussion for notification', array('item_type' => $type, 'item_id' => $item_id, 'discussion_id' => $success, 'notify_multiple' => $notifyusersString, 'visibility' => $visibility));
                } else if ($type == 'invoice') {
                    MobileNotificationNew::notify('invoice discussion for notification', array('item_type' => $type, 'item_id' => $item_id, 'discussion_id' => $success, 'notify_multiple' => $notifyusersString, 'visibility' => $visibility));
                }
            }

            if ($notifyCustomers) {
                $modelCustomer = new Model_Customer();
                $modelEmailTemplate = new Model_EmailTemplate();
                foreach ($notifyCustomers as $notifyCustomer) {
                    $customer = $modelCustomer->getById($notifyCustomer);
                    if ($customer['email1'] && filter_var($customer['email1'], FILTER_VALIDATE_EMAIL)) {
                        $to[0] = array('email' => $customer['email1'], 'number' => '1');
                    }
                    if ($customer['email2'] && filter_var($customer['email2'], FILTER_VALIDATE_EMAIL)) {
                        $to[1] = array('email' => $customer['email2'], 'number' => '2');
                    }
                    if ($customer['email3'] && filter_var($customer['email3'], FILTER_VALIDATE_EMAIL)) {
                        $to[2] = array('email' => $customer['email3'], 'number' => '3');
                    }

                    $template_params = array(
                        "{customer_display_name}" => get_customer_name($customer),
                        "{logged_username}" => $this->loggedUser['username'],
                        "{type}" => $type,
                        "{item_id}" => $item_id,
                        "{user_message}" => nl2br(htmlentities($discussion, ENT_NOQUOTES, 'UTF-8'))
                    );

                    /* $emailTemplate = $modelEmailTemplate->getEmailTemplate('notify_customer_for_new_discussion', $template_params);

                      var_dump($emailTemplate);
                      exit;
                     */


                    $email_log = array('reference_id' => $item_id, 'type' => 'discussion');

                    if ($to) {

                        //var_dump($to);
                        foreach ($to as $toEmail) {

                            $params['to'] = $toEmail['email'];
                            EmailNotification::sendEmail($params, 'notify_customer_for_new_discussion', $template_params, $email_log);
                        }
                    }
                }
            }
        }

        if ($type == 'booking') {
            $modelBookingDiscussion->closeConnection();
        } else if ($type == 'estimate') {
            $modelEstimateDiscussion->closeConnection();
        } else if ($type == 'complaint') {
            $modelComplaintDiscussion->closeConnection();
        } else if ($type == 'inquiry') {
            $modelInquiryDiscussion->closeConnection();
        } else if ($type == 'invoice') {
            $modelInvoiceDiscussion->closeConnection();
        }

        if ($success) {
            if ($type == 'complaint') {
                echo json_encode(array('success' => 1, 'updated' => $updated));
            } else {
                echo json_encode(array('success' => 1));
            }
        } else {
            echo json_encode(array('success' => 0, 'errMsg' => 'Invalid discussion entry'));
        }
        exit;
    }

    /* public function submitAction() {


      //
      //get Params
      //
      $item_id = $this->request->getParam('id', 0);
      $type = $this->request->getParam('type', 0);
      $discussion = $this->request->getParam('discussion', '');
      $visibility = $this->request->getParam('visibility', 1);
      $notifyPersons = $this->request->getParam('notifyPersons','');
      $imageId = $this->request->getParam('imageId', 0);
      $notifyusersString = $notifyCustomersString = '';
      $notifyCustomers = array();
      $notifyUsers = array();

      if($type == 'booking'){
      CheckAuth::checkPermission(array('bookingDiscussion'));
      }else if($type == 'estimate'){
      CheckAuth::checkPermission(array('estimateDiscussion'));
      }else if($type == 'inquiry'){
      CheckAuth::checkPermission(array('inquiryDiscussion'));
      }else if($type == 'invoice'){
      CheckAuth::checkPermission(array('invoiceDiscussion'));
      }else if($type == 'complaint'){
      CheckAuth::checkPermission(array('complaintDiscussion'));
      }

      $modelBookingDiscussion = new Model_BookingDiscussion();
      $modelComplaintDiscussion = new Model_ComplaintDiscussion();
      $modelEstimateDiscussion = new Model_EstimateDiscussion();
      $modelInquiryDiscussion = new Model_InquiryDiscussion();
      $modelInvoiceDiscussion = new Model_InvoiceDiscussion();

      if($notifyPersons){
      $notify = explode(',',$notifyPersons);


      foreach($notify as $value){
      if(substr($value,0,1) == 'c'){
      $id = substr($value,1);
      $notifyCustomers[] = $id;
      }else{
      $notifyUsers[] = $value;
      }
      }

      $notifyusersString = implode(',', $notifyUsers);
      $notifyCustomersString = implode(',', $notifyCustomers);
      }
      if($type == 'complaint'){
      $process = $this->request->getParam('process', 'send');
      if ($process == 'open' || $process == 'close') {
      //
      // check Auth for logged user
      //
      CheckAuth::checkPermission(array('complaintConvertStatus'));

      if (!CheckAuth::checkIfCanHandelAllCompany('complaint', $item_id)) {
      $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Booking not belongs to your Company"));
      $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
      }
      }
      }

      $imagesAfterProcessing = array();

      //
      // load model
      //
      $modelComplaint = new Model_Complaint();
      //$modelDiscussionMongo = new Model_DiscussionMongo();
      $modelImage = new Model_Image();
      $modelUser = new Model_User();
      $modelContractorInfo = new Model_ContractorInfo();
      //$modelComplaint = new Model_Complaint();
      $modelComplaintTemp = new Model_ComplaintTemp();

      $success = 0;
      if ($discussion) {

      $user = $modelUser->getById($this->loggedUser['user_id']);
      if ($user['role_name'] == 'contractor') {
      $contractorInfo = $modelContractorInfo->getByContractorId($user['user_id']);
      $user['username'] = ucwords($user['username']) . ' - ' . ucwords($contractorInfo['business_name']);
      } else {
      $user['username'] = ucwords($user['username']);
      }

      if($type == 'booking'){

      $doc = array(
      'booking_id' => (int) $item_id,
      'user_id' => $this->loggedUser['user_id'],
      'user_name' => $user['username'],
      'avatar' => $user['avatar'],
      'user_message' => $discussion,
      'created' => time(),
      'user_role_name' => $user['role_name'],
      'images' => array(),
      'seen_by_ids' => "",
      'seen_by_names' => "",
      'visibility' => (int) $visibility,
      'visited_extra_info_id' => ""
      );

      $success = $modelBookingDiscussion->insert($doc);
      }else if($type == 'estimate'){
      $doc = array(
      'estimate_id' => (int) $item_id,
      'user_id' => $this->loggedUser['user_id'],
      'user_name' => $user['username'],
      'avatar' => $user['avatar'],
      'user_message' => $discussion,
      'created' => time(),
      'user_role_name' => $user['role_name'],
      'images' => array(),
      'seen_by_ids' => "",
      'seen_by_names' => "",
      'visibility' => (int) $visibility
      );

      $success = $modelEstimateDiscussion->insert($doc);
      }else if($type == 'inquiry'){
      $doc = array(
      'inquiry_id' => (int) $item_id,
      'user_id' => $this->loggedUser['user_id'],
      'user_name' => $user['username'],
      'avatar' => $user['avatar'],
      'user_message' => $discussion,
      'created' => time(),
      'user_role_name' => $user['role_name'],
      'images' => array(),
      'seen_by_ids' => "",
      'seen_by_names' => "",
      'visibility' => (int) $visibility
      );

      $success = $modelInquiryDiscussion->insert($doc);
      }else if($type == 'complaint'){
      $doc = array(
      'complaint_id' => (int) $item_id,
      'user_id' => $this->loggedUser['user_id'],
      'user_name' => $user['username'],
      'avatar' => $user['avatar'],
      'user_message' => $discussion,
      'created' => time(),
      'user_role_name' => $user['role_name'],
      'images' => array(),
      'seen_by_ids' => "",
      'seen_by_names' => "",
      'visibility' => (int) $visibility
      );

      $success = $modelComplaintDiscussion->insert($doc);
      }else if($type == 'invoice'){

      $doc = array(
      'invoice_id' => (int) $item_id,
      'user_id' => $this->loggedUser['user_id'],
      'user_name' => $user['username'],
      'avatar' => $user['avatar'],
      'user_message' => $discussion,
      'created' => time(),
      'user_role_name' => $user['role_name'],
      'images' => array(),
      'seen_by_ids' => "",
      'seen_by_names' => "",
      'visibility' => (int) $visibility
      );

      $paymentId = $this->request->getParam('payment_id', 0);
      if ($paymentId != 0) {
      $doc['payment_id'] = $paymentId;

      $modelPayment = new Model_Payment();
      $payment = array('is_rejected' => 1);
      $modelPayment->updateById($paymentId, $payment);
      }else{
      $doc['payment_id'] = 0;
      }

      $success = $modelInvoiceDiscussion->insert($doc);
      }

      //echo $success;
      $newDocID = $success;
      //echo $newDsocID;exit;

      //foreach($newDocID as $key => $value)
      $disc_id = $success;

      $upload = new Zend_File_Transfer_Adapter_Http();
      $files = $upload->getFileInfo();
      $countFiles = count($files);

      if(isset($files) && !empty($files)){
      $i = 1;
      foreach ($files as $file => $fileInfo) {
      if ($upload->isUploaded($file)) {
      if ($upload->receive($file)) {
      $info = $upload->getFileInfo($file);
      $source = $info[$file]['tmp_name'];
      $imageInfo = pathinfo($source);
      $ext = $imageInfo['extension'];
      $dir = get_config('image_attachment') . '/';
      $subdir = date('Y/m/d/');
      $fullDir = $dir . $subdir;
      if (!is_dir($fullDir)) {
      mkdir($fullDir, 0777, true);
      }

      $loggedUser = CheckAuth::getLoggedUser();

      $id = $disc_id . $i;

      $original_path = "original_{$id}.{$ext}";
      $large_path = "large_{$id}.{$ext}";
      $small_path = "small_{$id}.{$ext}";
      $thumbnail_path = "thumbnail_{$id}.{$ext}";
      $compressed_path = "compressed_{$id}.jpg";

      $images = array(
      "image_id" => $id,
      "thumbnail_path" => $subdir . $thumbnail_path,
      "original_path" => $subdir . $original_path,
      "compressed_path" => $subdir . $compressed_path,
      "large_path" => $subdir . $large_path,
      "small_path" => $subdir . $small_path
      );

      $imagesAfterProcessing[] = $images;

      $image_saved = copy($source, $fullDir . $original_path);
      $compressed_saved = copy($source, $fullDir . $compressed_path);
      ImageMagick::scale_image($source, $fullDir . $large_path, 510);
      ImageMagick::scale_image($source, $fullDir . $small_path, 250);
      ImageMagick::create_thumbnail($source, $fullDir . $thumbnail_path, 78, 64);
      ImageMagick::convert($source, $fullDir . $compressed_path);
      ImageMagick::compress_image($source, $fullDir . $compressed_path);

      if ($image_saved) {
      if (file_exists($source)) {
      unlink($source);
      }
      }
      }else{
      echo json_encode(array('success' => 0, 'errMsg' => 'Errors Receiving File'));
      exit;
      }
      }
      $i++;
      }
      //////
      /*$images = array(
      'thumbnail_images' => $thumbnails_paths,
      'large_images' => $larges_paths,
      'small_images' => $small_paths,
      'original_images' => $originals_paths,
      'compressed_images' => $compressed_paths
      ); */
    /* if($type == 'booking'){
      $modelBookingDiscussion->updateDiscImages($disc_id, $imagesAfterProcessing);
      }else if($type == 'estimate'){
      $modelEstimateDiscussion->updateDiscImages($disc_id, $imagesAfterProcessing);
      }else if($type == 'inquiry'){
      $modelInquiryDiscussion->updateDiscImages($disc_id, $imagesAfterProcessing);
      }else if($type == 'invoice'){
      $modelInvoiceDiscussion->updateDiscImages($disc_id, $imagesAfterProcessing);
      }else if($type == 'complaint'){
      $modelComplaintDiscussion->updateDiscImages($disc_id, $imagesAfterProcessing);
      }
      }
      $updated = 0;
      if($type == 'complaint'){
      $modelComplaint->sendComplaintAsEmailToContractor($item_id);

      ////change status
      //$updated = 0;
      if ($process == 'open') {
      $data = array(
      'complaint_status' => 'open'
      );
      } elseif ($process == 'close') {
      $data = array(
      'complaint_status' => 'closed'
      );
      }
      if ($process == 'close') {

      $loggedUser = CheckAuth::getLoggedUser();
      $user_id = !empty($loggedUser) ? $loggedUser['user_id'] : 0;

      $modelAuthRole = new Model_AuthRole();
      $contractor_role_id = $modelAuthRole->getRoleIdByName('contractor');
      if ($contractor_role_id == $loggedUser['role_id']) {
      $db_params = array();
      $db_params['is_approved'] = 0;
      $modelComplaint->updateById($item_id, $db_params);
      $complaintTemp = $modelComplaintTemp->getByComplaintId($item_id);
      $data['user_id'] = $this->loggedUser['user_id'];
      if ($complaintTemp) {
      $updated = $modelComplaintTemp->updateById($complaintTemp['id'], $data);
      } else {
      $added = $modelComplaintTemp->addComplaintTemp($item_id, $data);
      if ($added) {
      $updated = 1;
      }
      }
      } else {
      $updated = $modelComplaint->updateById($item_id, $data);
      }
      } else if ($process == 'open') {

      $complaintTemp = $modelComplaintTemp->getByComplaintId($item_id);
      if ($complaintTemp) {
      $modelComplaintTemp->deleteById($complaintTemp['id']);
      }
      $updated = $modelComplaint->updateById($item_id, array('complaint_status' => 'open', 'is_approved' => 1));
      }
      }

      if ($success) {
      $notifyContractor = 1;
      if($visibility == 1){
      $notifyContractor = 0;
      }

      if($type == 'booking'){
      MobileNotification::notify($item_id, 'new discussion', array('discussion_id' => $disc_id,'notidy_multiple' => $notifyusersString, 'notify_contractor' => $notifyContractor));
      }else if($type == 'estimate'){
      MobileNotification::notify(0, 'estimate discussion', array('estimate_id' => $item_id, 'discussion_id' => $disc_id, 'notidy_multiple' => $notifyusersString, 'notify_contractor' => $notifyContractor));
      }else if($type == 'complaint'){
      MobileNotification::notify(0, 'complaint discussion', array('complaint_id' => $item_id, 'discussion_id' => $disc_id, 'notidy_multiple' => $notifyusersString, 'notify_contractor' => $notifyContractor));
      }else if($type == 'inquiry'){
      MobileNotification::notify(0, 'inquiry discussion' , array('inquiry_id'=>$item_id , 'discussion_id'=>$disc_id, 'notidy_multiple' => $notifyusersString, 'notify_contractor' => $notifyContractor));
      }else if($type == 'invoice'){
      MobileNotification::notify(0, 'invoice discussion', array('invoice_id' => $item_id, 'discussion_id' =>$disc_id, 'notidy_multiple' => $notifyusersString, 'notify_contractor' => $notifyContractor));
      }
      }

      if($notifyCustomers){
      $modelCustomer = new Model_Customer();
      $modelEmailTemplate = new Model_EmailTemplate();
      foreach($notifyCustomers as $notifyCustomer){
      $customer = $modelCustomer->getById($notifyCustomer);
      if ($customer['email1'] && filter_var($customer['email1'], FILTER_VALIDATE_EMAIL)) {
      $to[0] = array('email' => $customer['email1'], 'number' => '1');
      }
      if ($customer['email2'] && filter_var($customer['email2'], FILTER_VALIDATE_EMAIL)) {
      $to[1] = array('email' => $customer['email2'], 'number' => '2');
      }
      if ($customer['email3'] && filter_var($customer['email3'], FILTER_VALIDATE_EMAIL)) {
      $to[2] = array('email' => $customer['email3'], 'number' => '3');
      }

      $template_params = array(
      "{customer_display_name}" => get_customer_name($customer),
      "{logged_username}" => $this->loggedUser['username'],
      "{type}"  => $type,
      "{item_id}" => $item_id,
      "{user_message}" => nl2br(htmlentities($discussion, ENT_NOQUOTES, 'UTF-8'))
      );
      $emailTemplate = $modelEmailTemplate->getEmailTemplate('notify_customer_for_new_discussion', $template_params);

      $params = array(
      'to' => $to,
      'body' => $emailTemplate['body'],
      'subject' => $emailTemplate['subject']
      );

      $email_log = array('reference_id' => $item_id, 'type' => 'discussion');

      if ($to) {
      //var_dump($to);
      foreach ($to as $toEmail) {
      $params['to'] = $toEmail['email'];
      EmailNotification::sendEmail($params, 'notify_customer_for_new_discussion', $template_params, $email_log);
      }
      }
      }

      }
      }

      if($type == 'booking'){
      $modelBookingDiscussion->closeConnection();
      }else if($type == 'estimate'){
      $modelEstimateDiscussion->closeConnection();
      }else if($type == 'complaint'){
      $modelComplaintDiscussion->closeConnection();
      }else if($type == 'inquiry'){
      $modelInquiryDiscussion->closeConnection();
      }else if($type == 'invoice'){
      $modelInvoiceDiscussion->closeConnection();
      }

      if ($success) {
      if($type == 'complaint'){
      echo json_encode(array('success' => 1, 'updated' => $updated));
      }else{
      echo json_encode(array('success' => 1));
      }

      } else {
      echo json_encode(array('success' => 0, 'errMsg' => 'Invalid discussion entry'));
      }
      exit;
      } */

    public function getAllDiscussionAction() {

        $item_id = $this->request->getParam('id', 0);
        $type = $this->request->getParam('type', '');

        if ($type == 'booking') {
            CheckAuth::checkPermission(array('bookingDiscussion'));
        } else if ($type == 'estimate') {
            CheckAuth::checkPermission(array('estimateDiscussion'));
        } else if ($type == 'inquiry') {
            CheckAuth::checkPermission(array('inquiryDiscussion'));
        } else if ($type == 'invoice') {
            CheckAuth::checkPermission(array('invoiceDiscussion'));
        } else if ($type == 'complaint') {
            CheckAuth::checkPermission(array('complaintDiscussion'));
        }

        //echo "item_id: " . $item_id . " type: " . $type; exit;
        $loggedUser = CheckAuth::getLoggedUser();
        $discussions = array();
        $discussions2 = array();
        $isContractor = false;
        //
        // load model
        //
		$modelAuthRole = new Model_AuthRole();
        $modelBookingDiscussion = new Model_BookingDiscussion();
        $modelComplaintDiscussion = new Model_ComplaintDiscussion();
        $modelEstimateDiscussion = new Model_EstimateDiscussion();
        $modelInquiryDiscussion = new Model_InquiryDiscussion();
        $modelInvoiceDiscussion = new Model_InvoiceDiscussion();
        $modelItemImageDiscussion = new Model_ItemImageDiscussion();
        $modelImage = new Model_Image();

        $contractorRoleId = $modelAuthRole->getRoleIdByName('contractor');

        if ($loggedUser['role_id'] == $contractorRoleId) {
            $isContractor = true;
        }

        if ($type == 'booking') {
            $discussions = $modelBookingDiscussion->getByBookingId($item_id, 'DESC');
        } else if ($type == 'estimate') {
            $discussions = $modelEstimateDiscussion->getByEstimateId($item_id, 'DESC');
        } else if ($type == 'complaint') {
            $discussions = $modelComplaintDiscussion->getByComplaintId($item_id, 'DESC');
        } else if ($type == 'inquiry') {
            $discussions = $modelInquiryDiscussion->getByInquiryId($item_id, 'DESC');
        } else if ($type == 'invoice') {
            $discussions = $modelInvoiceDiscussion->getByInvoiceId($item_id, 'DESC');
        }

        if ($discussions) {

            foreach ($discussions as &$discussion) {
                $itemImageDiscussion = $modelItemImageDiscussion->getByDiscussionIdAndType($discussion['discussion_id'], $type);

                if ($itemImageDiscussion) {
                    if ($itemImageDiscussion[0]['group_id'] != 0) {
                        $discussion['group_id'] = $itemImageDiscussion[0]['group_id'];
                        $discussion['images'] = $modelImage->getByGroupId($itemImageDiscussion[0]['group_id']);
                    } else if ($itemImageDiscussion[0]['image_id'] != 0) {
                        $image = $modelImage->getByImageId($itemImageDiscussion[0]['image_id']);
                        $discussion['image_id'] = $itemImageDiscussion[0]['image_id'];
                        $imageAfterProccessing = array();
                        $imageAfterProccessing[] = $image;
                        $discussion['images'] = $imageAfterProccessing;
                        //var_dump($image);
                    }
                }

                /* $lastFourImages = array();
                  if($discussion['images'] && $type == 'booking'){
                  if(count($discussion['images']) > 4){
                  $lastFourImages = array_slice($discussion['images'],0,4,true);
                  }else{
                  $lastFourImages = $discussion['images'];
                  }
                  $discussion['images'] = $lastFourImages;
                  } */
                $discussion['seen_ids'] = explode(',', $discussion['seen_by_ids']);
                $discussion['seen_names'] = explode(',', $discussion['seen_by_names']);
                $discussion['discussion_date'] = time_ago($discussion['created']);
            }

            //var_dump($discussions2);exit;
        }

        $adminRoleId = $modelAuthRole->getRoleIdByName('super_admin');
        $manggerRoleId = $modelAuthRole->getRoleIdByName('manager');
        $salesRoleId = $modelAuthRole->getRoleIdByName('sales');
        //echo $adminRoleId . $manggerRoleId . $salesRoleId;exit;

        $filters['roles'] = array($adminRoleId, $manggerRoleId, $salesRoleId);
        $filters['active'] = 'TRUE';
        $filters['logged_user_id'] = $loggedUser['user_id'];

        $modelUser = new Model_User();
        $users = $modelUser->getAll($filters);

        $json_array = array(
            'discussions' => $discussions,
            'count' => count($discussions),
            'users' => $users
        );

        echo json_encode($json_array);
        exit;
    }

    /* public function getAllTabsDiscussionAction(){

      $bookingId = $this->request->getParam('bookingId', 0);
      $complaintId = $this->request->getParam('complaintId', 0);
      $inquiry_id = $this->request->getParam('inquiryId', 0);

      $modelBooking = new Model_Booking();
      $booking = $modelBooking->getById($bookingId);

      //$modelDiscussionMongo = new Model_DiscussionMongo();
      $modelBookingDiscussion = new Model_BookingDiscussion();
      $modelComplaintDiscussion = new Model_ComplaintDiscussion();
      $modelEstimateDiscussion = new Model_EstimateDiscussion();
      $modelInquiryDiscussion = new Model_InquiryDiscussion();
      $modelInvoiceDiscussion = new Model_InvoiceDiscussion();
      $modelBookingInvoice = new Model_BookingInvoice();
      $modelBookingEstimate = new Model_BookingEstimate();
      $modelAuthRole = new Model_AuthRole();
      $modelItemImageDiscussion = new Model_ItemImageDiscussion();
      $modelImage = new Model_Image();
      $modelUser = new Model_User();
      $modelContractorInfo = new Model_ContractorInfo();

      $invoiceId = $estimateId = $inquiryId = 0;
      $countBookingDiscussions = $countInvoiceDiscussions = $countInquiryDiscussions = $countEstimateDiscussions = $countComplaintDiscussions = 0;
      $isContractor = false;
      $loggedUser = CheckAuth::getLoggedUser();

      $contractorRoleId = $modelAuthRole->getRoleIdByName('contractor');
      if($loggedUser['role_id'] == $contractorRoleId){
      $isContractor = true;
      }

      if ($bookingId) {
      $invoice = $modelBookingInvoice->getByBookingId($bookingId);
      $estimate = $modelBookingEstimate->getByBookingId($bookingId);

      $invoiceId = !empty($invoice['id']) ? $invoice['id'] : 0;
      $estimateId = !empty($estimate['id']) ? $estimate['id'] : 0;
      $inquiryId = !empty($booking['original_inquiry_id']) ? $booking['original_inquiry_id'] : 0;

      /*$this->view->bookingDiscussions = $modelBookingDiscussion->getByBookingId($bookingId);
      $this->view->invoiceDiscussions = $modelInvoiceDiscussion->getByInvoiceId($invoiceId);
      $this->view->estimateDiscussions = $modelEstimateDiscussion->getByEstimateId($estimateId);
      $this->view->inquiryDiscussions = $modelInquiryDiscussion->getByInquiryId($inquiryId);/
      $bookingDiscussions = $modelBookingDiscussion->getByBookingId($bookingId, 'DESC');
      $invoiceDiscussions = $modelInvoiceDiscussion->getByInvoiceId($invoiceId, 'DESC');
      $estimateDiscussions = $modelEstimateDiscussion->getByEstimateId($estimateId, 'DESC');
      $inquiryDiscussions = $modelInquiryDiscussion->getByInquiryId($inquiryId, 'DESC');

      $countBookingDiscussions = $modelBookingDiscussion->countDiscussions($bookingId, $isContractor);
      $countInvoiceDiscussions = $modelInvoiceDiscussion->countDiscussions($invoiceId, $isContractor);
      $countEstimateDiscussions = $modelEstimateDiscussion->countDiscussions($estimateId, $isContractor);
      $countInquiryDiscussions = $modelInquiryDiscussion->countDiscussions($inquiryId, $isContractor);

      if($bookingDiscussions){

      foreach($bookingDiscussions as &$discussion){
      if ($discussion['user_role'] == 9) {
      $modelCustomer = new Model_Customer();
      $discussion['user'] = $modelCustomer->getById($discussion['user_id']);
      $username = $discussion['user']['first_name'] . ' ' . $discussion['user']['last_name'];
      } else {
      $discussion['user'] = $modelUser->getById($discussion['user_id']);
      $username = $discussion['user']['username'];
      }
      if (isset($discussion['user']['role_id']) && $discussion['user']['role_id'] == $modelAuthRole->getRoleIdByName('contractor')){
      $contractorInfo = $modelContractorInfo->getByContractorId($discussion['user']['user_id']);
      $discussion['user']['username'] = ucwords($username) . ($contractorInfo['business_name'] ? ' - ' . ucwords($contractorInfo['business_name']) : '');
      }else{
      $discussion['user']['username'] = ucwords($username);
      }
      $discussion['discussion_date'] = getDateFormating( $discussion['created']);
      $discussion['user_message'] = nl2br(htmlentities($discussion['user_message'], ENT_NOQUOTES, 'UTF-8'));


      $itemImageDiscussion = $modelItemImageDiscussion->getByDiscussionIdAndType($discussion['discussion_id'], 'booking');
      if($itemImageDiscussion){
      if($itemImageDiscussion[0]['group_id'] != 0){
      $discussion['group_id'] = $itemImageDiscussion[0]['group_id'];
      $discussion['images'] = $modelImage->getByGroupId($itemImageDiscussion[0]['group_id']);
      }else if($itemImageDiscussion[0]['image_id'] != 0){
      $image = $modelImage->getByImageId($itemImageDiscussion[0]['image_id']);
      $discussion['image_id'] = $itemImageDiscussion[0]['image_id'];
      $imageAfterProccessing = array();
      $imageAfterProccessing[] = $image;
      $discussion['images'] = $imageAfterProccessing;
      //var_dump($image);
      }

      if(!empty($discussion['images'])){
      foreach($discussion['images'] as &$bookingGroupedImage){
      $bookingGroupedImage['imageDiscussions'] = $modelBookingDiscussion->getByImageId($bookingGroupedImage['image_id'], 'DESC');

      if($bookingGroupedImage['imageDiscussions']){
      foreach ($bookingGroupedImage['imageDiscussions'] as &$imageDiscussion) {

      if ($imageDiscussion['user_role'] == 9) {
      $modelCustomer = new Model_Customer();
      $imageDiscussion['user'] = $modelCustomer->getById($imageDiscussion['user_id']);
      $username = $imageDiscussion['user']['first_name'] . ' ' . $imageDiscussion['user']['last_name'];
      } else {
      $imageDiscussion['user'] = $modelUser->getById($imageDiscussion['user_id']);
      $username = $imageDiscussion['user']['username'];
      }

      if (isset($imageDiscussion['user']['role_id']) && $imageDiscussion['user']['role_id'] == $modelAuthRole->getRoleIdByName('contractor')){
      $contractorInfo = $modelContractorInfo->getByContractorId($imageDiscussion['user']['user_id']);
      $imageDiscussion['user']['username'] = ucwords($imageDiscussion['user']['username']) . ($contractorInfo['business_name'] ? ' - ' . ucwords($contractorInfo['business_name']) : '');
      }else{
      $imageDiscussion['user']['username'] = ucwords($imageDiscussion['user']['username']);
      }

      $imageDiscussion['discussion_date'] = getDateFormating( $imageDiscussion['created']);
      $imageDiscussion['user_message'] = nl2br(htmlentities($imageDiscussion['user_message'], ENT_NOQUOTES, 'UTF-8'));


      }


      }
      }

      }
      }
      }
      //var_dump($bookingDiscussions);
      }
      if($invoiceDiscussions){

      foreach($invoiceDiscussions as &$discussion){
      $itemImageDiscussion = $modelItemImageDiscussion->getByDiscussionIdAndType($discussion['discussion_id'], 'invoice');
      if($itemImageDiscussion){
      if($itemImageDiscussion[0]['group_id'] != 0){
      $discussion['group_id'] = $itemImageDiscussion[0]['group_id'];
      $discussion['images'] = $modelImage->getByGroupId($itemImageDiscussion[0]['group_id']);
      }else if($itemImageDiscussion[0]['image_id'] != 0){
      $image = $modelImage->getByImageId($itemImageDiscussion[0]['image_id']);
      $discussion['image_id'] = $itemImageDiscussion[0]['image_id'];
      $imageAfterProccessing = array();
      $imageAfterProccessing[] = $image;
      $discussion['images'] = $imageAfterProccessing;
      //var_dump($image);
      }
      }
      }
      }
      if($estimateDiscussions){

      foreach($estimateDiscussions as &$discussion){
      $discussion['user_message'] = nl2br(htmlentities($discussion['user_message'], ENT_NOQUOTES, 'UTF-8'));
      $itemImageDiscussion = $modelItemImageDiscussion->getByDiscussionIdAndType($discussion['discussion_id'], 'estimate');
      if($itemImageDiscussion){
      if($itemImageDiscussion[0]['group_id'] != 0){
      $discussion['group_id'] = $itemImageDiscussion[0]['group_id'];
      $discussion['images'] = $modelImage->getByGroupId($itemImageDiscussion[0]['group_id']);
      }else if($itemImageDiscussion[0]['image_id'] != 0){
      $image = $modelImage->getByImageId($itemImageDiscussion[0]['image_id']);
      $discussion['image_id'] = $itemImageDiscussion[0]['image_id'];
      $imageAfterProccessing = array();
      $imageAfterProccessing[] = $image;
      $discussion['images'] = $imageAfterProccessing;
      //var_dump($image);
      }
      if(!empty($discussion['images'])){
      foreach($discussion['images'] as &$estimateGroupedImage){
      $estimateGroupedImage['imageDiscussions'] = $modelEstimateDiscussion->getByImageId($estimateGroupedImage['image_id'], 'DESC');
      if($estimateGroupedImage['imageDiscussions']){
      foreach ($estimateGroupedImage['imageDiscussions'] as &$estimateImageDiscussion) {
      $estimateImageDiscussion['user'] = $modelUser->getById($estimateImageDiscussion['user_id']);
      if ($estimateImageDiscussion['user']['role_name'] == 'contractor') {
      $contractorInfo = $modelContractorInfo->getByContractorId($estimateImageDiscussion['user']['user_id']);
      $estimateImageDiscussion['user']['username'] = ucwords($estimateImageDiscussion['user']['username']) . ($contractorInfo['business_name'] ? ' - ' . ucwords($contractorInfo['business_name']) : '');
      } else {
      $estimateImageDiscussion['user']['username'] = ucwords($estimateImageDiscussion['user']['username']);
      }


      $estimateImageDiscussion['discussion_date'] = getDateFormating( $estimateImageDiscussion['created']);
      $estimateImageDiscussion['user_message'] = nl2br(htmlentities($estimateImageDiscussion['user_message'], ENT_NOQUOTES, 'UTF-8'));

      }
      }
      }
      }
      }
      }
      }
      if($inquiryDiscussions){

      foreach($inquiryDiscussions as &$discussion){
      $discussion['user_message'] = nl2br(htmlentities($discussion['user_message'], ENT_NOQUOTES, 'UTF-8'));
      if (isset($discussion['user_role']) && $discussion['user_role'] == 9) {
      $modelCustomer = new Model_Customer();
      $discussion['user'] = $modelCustomer->getById($discussion['user_id']);
      $discussion['user']['username'] = $discussion['user']['first_name'] . ' ' . $discussion['user']['last_name'];
      } else {
      $discussion['user'] = $modelUser->getById($discussion['user_id']);
      $discussion['user']['username'] = ucwords($discussion['user']['username']);
      }
      $itemImageDiscussion = $modelItemImageDiscussion->getByDiscussionIdAndType($discussion['discussion_id'], 'inquiry');
      if($itemImageDiscussion){
      if($itemImageDiscussion[0]['group_id'] != 0){
      $discussion['group_id'] = $itemImageDiscussion[0]['group_id'];
      $discussion['images'] = $modelImage->getByGroupId($itemImageDiscussion[0]['group_id']);
      }else if($itemImageDiscussion[0]['image_id'] != 0){
      $image = $modelImage->getByImageId($itemImageDiscussion[0]['image_id']);
      $discussion['image_id'] = $itemImageDiscussion[0]['image_id'];
      $imageAfterProccessing = array();
      $imageAfterProccessing[] = $image;
      $discussion['images'] = $imageAfterProccessing;
      //var_dump($image);
      }

      if(!empty($discussion['images'])){
      foreach($discussion['images'] as &$inquiryGroupedImage){
      $inquiryGroupedImage['imageDiscussions'] = $modelInquiryDiscussion->getByImageId($inquiryGroupedImage['image_id'], 'DESC');
      if($inquiryGroupedImage['imageDiscussions']){
      foreach ($inquiryGroupedImage['imageDiscussions'] as &$inquiryImageDiscussion) {
      $inquiryImageDiscussion['discussion_date'] = getDateFormating( $inquiryImageDiscussion['created']);
      $inquiryImageDiscussion['user_message'] = nl2br(htmlentities($inquiryImageDiscussion['user_message'], ENT_NOQUOTES, 'UTF-8'));

      if (isset($inquiryImageDiscussion['user_role']) && $inquiryImageDiscussion['user_role'] == $modelAuthRole->getRoleIdByName('customer')) {
      $modelCustomer = new Model_Customer();
      $inquiryImageDiscussion['user'] = $modelCustomer->getById($inquiryImageDiscussion['user_id']);
      $username = $inquiryImageDiscussion['user']['first_name'] . ' ' . $inquiryImageDiscussion['user']['last_name'];
      } else {
      $inquiryImageDiscussion['user'] = $modelUser->getById($inquiryImageDiscussion['user_id']);
      $username = $inquiryImageDiscussion['user']['username'];
      }

      if ($inquiryImageDiscussion['user']['role_id'] == $modelAuthRole->getRoleIdByName('contractor')){
      $contractorInfo = $modelContractorInfo->getByContractorId($inquiryImageDiscussion['user']['user_id']);
      $inquiryImageDiscussion['user']['username'] = ucwords($inquiryImageDiscussion['user']['username']) . ($contractorInfo['business_name'] ? ' - ' . ucwords($contractorInfo['business_name']) : '');
      }else{
      $inquiryImageDiscussion['user']['username'] = ucwords($username);
      }

      }
      }


      }
      }
      }
      }
      }

      $this->view->bookingDiscussions = $bookingDiscussions;
      $this->view->invoiceDiscussions = $invoiceDiscussions;
      $this->view->estimateDiscussions = $estimateDiscussions;
      $this->view->inquiryDiscussions = $inquiryDiscussions;


      }else if ($inquiry_id && (CheckAuth::getRoleName() != 'customer')) {
      //$this->view->inquiryDiscussions = $modelInquiryDiscussion->getByInquiryId($inquiry_id);
      $inquiryDiscussions = $modelInquiryDiscussion->getByInquiryId($inquiry_id, 'DESC');
      $countInquiryDiscussions = $modelInquiryDiscussion->countDiscussions($inquiry_id, $isContractor);
      if($inquiryDiscussions){

      foreach($inquiryDiscussions as &$discussion){
      $discussion['user_message'] = nl2br(htmlentities($discussion['user_message'], ENT_NOQUOTES, 'UTF-8'));
      if (isset($discussion['user_role']) && $discussion['user_role'] == 9) {
      $modelCustomer = new Model_Customer();
      $discussion['user'] = $modelCustomer->getById($discussion['user_id']);
      $discussion['user']['username'] = $discussion['user']['first_name'] . ' ' . $discussion['user']['last_name'];
      } else {
      $discussion['user'] = $modelUser->getById($discussion['user_id']);
      $discussion['user']['username'] = ucwords($discussion['user']['username']);
      }
      $itemImageDiscussion = $modelItemImageDiscussion->getByDiscussionIdAndType($discussion['discussion_id'], 'inquiry');
      if($itemImageDiscussion){
      if($itemImageDiscussion[0]['group_id'] != 0){
      $discussion['group_id'] = $itemImageDiscussion[0]['group_id'];
      $discussion['images'] = $modelImage->getByGroupId($itemImageDiscussion[0]['group_id']);
      }else if($itemImageDiscussion[0]['image_id'] != 0){
      $image = $modelImage->getByImageId($itemImageDiscussion[0]['image_id']);
      $discussion['image_id'] = $itemImageDiscussion[0]['image_id'];
      $imageAfterProccessing = array();
      $imageAfterProccessing[] = $image;
      $discussion['images'] = $imageAfterProccessing;
      //var_dump($image);
      }

      if(!empty($discussion['images'])){
      foreach($discussion['images'] as &$inquiryGroupedImage){
      $inquiryGroupedImage['imageDiscussions'] = $modelInquiryDiscussion->getByImageId($inquiryGroupedImage['image_id'], 'DESC');
      if($inquiryGroupedImage['imageDiscussions']){
      foreach ($inquiryGroupedImage['imageDiscussions'] as &$inquiryImageDiscussion) {
      $inquiryImageDiscussion['discussion_date'] = getDateFormating( $inquiryImageDiscussion['created']);
      $inquiryImageDiscussion['user_message'] = nl2br(htmlentities($inquiryImageDiscussion['user_message'], ENT_NOQUOTES, 'UTF-8'));

      if (isset($inquiryImageDiscussion['user_role']) && $inquiryImageDiscussion['user_role'] == $modelAuthRole->getRoleIdByName('customer')) {
      $modelCustomer = new Model_Customer();
      $inquiryImageDiscussion['user'] = $modelCustomer->getById($inquiryImageDiscussion['user_id']);
      $username = $inquiryImageDiscussion['user']['first_name'] . ' ' . $inquiryImageDiscussion['user']['last_name'];
      } else {
      $inquiryImageDiscussion['user'] = $modelUser->getById($inquiryImageDiscussion['user_id']);
      $username = $inquiryImageDiscussion['user']['username'];
      }

      if ($inquiryImageDiscussion['user']['role_id'] == $modelAuthRole->getRoleIdByName('contractor')){
      $contractorInfo = $modelContractorInfo->getByContractorId($inquiryImageDiscussion['user']['user_id']);
      $inquiryImageDiscussion['user']['username'] = ucwords($inquiryImageDiscussion['user']['username']) . ($contractorInfo['business_name'] ? ' - ' . ucwords($contractorInfo['business_name']) : '');
      }else{
      $inquiryImageDiscussion['user']['username'] = ucwords($username);
      }

      }
      }


      }
      }
      }
      }
      }
      $this->view->inquiryDiscussions = $inquiryDiscussions;
      }
      if ($complaintId && (CheckAuth::getRoleName() != 'customer')) {
      //$this->view->complaintDiscussions = $modelComplaintDiscussion->getByComplaintId($complaintId);
      $complaintDiscussions = $modelComplaintDiscussion->getByComplaintId($complaintId, 'DESC');
      $countComplaintDiscussions = $modelComplaintDiscussion->countDiscussions($complaintId, $isContractor);
      if($complaintDiscussions){

      foreach($complaintDiscussions as &$discussion){
      $discussion['user_message'] = nl2br(htmlentities($discussion['user_message'], ENT_NOQUOTES, 'UTF-8'));
      $itemImageDiscussion = $modelItemImageDiscussion->getByDiscussionIdAndType($discussion['discussion_id'], 'complaint');
      if($itemImageDiscussion){
      if($itemImageDiscussion[0]['group_id'] != 0){
      $discussion['group_id'] = $itemImageDiscussion[0]['group_id'];
      $discussion['images'] = $modelImage->getByGroupId($itemImageDiscussion[0]['group_id']);
      }else if($itemImageDiscussion[0]['image_id'] != 0){
      $image = $modelImage->getByImageId($itemImageDiscussion[0]['image_id']);
      $discussion['image_id'] = $itemImageDiscussion[0]['image_id'];
      $imageAfterProccessing = array();
      $imageAfterProccessing[] = $image;
      $discussion['images'] = $imageAfterProccessing;
      //var_dump($image);
      }

      if(!empty($discussion['images'])){
      foreach($discussion['images'] as &$complaintGroupedImage){
      $complaintGroupedImage['imageDiscussions'] = $modelComplaintDiscussion->getDiscussionByImageId($complaintGroupedImage['image_id'], 'DESC');

      if($complaintGroupedImage['imageDiscussions']){
      foreach ($complaintGroupedImage['imageDiscussions'] as &$complaintImageDiscussion) {
      $complaintImageDiscussion['user'] = $modelUser->getById($complaintImageDiscussion['user_id']);
      if ($complaintImageDiscussion['user']['role_name'] == 'contractor') {
      $contractorInfo = $modelContractorInfo->getByContractorId($complaintImageDiscussion['user']['user_id']);
      $complaintImageDiscussion['user']['username'] = ucwords($complaintImageDiscussion['user']['username']) . ($contractorInfo['business_name'] ? ' - ' . ucwords($contractorInfo['business_name']) : '');
      } else {
      $complaintImageDiscussion['user']['username'] = ucwords($complaintImageDiscussion['user']['username']);
      }


      $complaintImageDiscussion['discussion_date'] = getDateFormating( $complaintImageDiscussion['created']);
      $complaintImageDiscussion['user_message'] = nl2br(htmlentities($complaintImageDiscussion['user_message'], ENT_NOQUOTES, 'UTF-8'));

      }
      }

      }
      }
      }
      }
      }
      $this->view->complaintDiscussions = $complaintDiscussions;
      }





      $countAll = $countBookingDiscussions + $countInvoiceDiscussions + $countInquiryDiscussions + $countEstimateDiscussions + $countComplaintDiscussions;

      $json_array = array(
      'result' => $this->view->render('discussion/get-all-tabs-discussion.phtml'),
      'countAll' => $countAll
      );

      echo json_encode($json_array);
      exit;

      } */

    public function getAllTabsDiscussionAction() {

        $bookingId = $this->request->getParam('bookingId', 0);
        $complaintId = $this->request->getParam('complaintId', 0);
        $inquiry_id = $this->request->getParam('inquiryId', 0);

        $modelBooking = new Model_Booking();
        $booking = $modelBooking->getById($bookingId);

        //$modelDiscussionMongo = new Model_DiscussionMongo();
        $modelBookingDiscussion = new Model_BookingDiscussion();
        $modelComplaintDiscussion = new Model_ComplaintDiscussion();
        $modelEstimateDiscussion = new Model_EstimateDiscussion();
        $modelInquiryDiscussion = new Model_InquiryDiscussion();
        $modelInvoiceDiscussion = new Model_InvoiceDiscussion();
        $modelBookingInvoice = new Model_BookingInvoice();
        $modelBookingEstimate = new Model_BookingEstimate();
        $modelAuthRole = new Model_AuthRole();
        $modelItemImageDiscussion = new Model_ItemImageDiscussion();
        $modelImage = new Model_Image();
        $modelUser = new Model_User();
        $modelContractorInfo = new Model_ContractorInfo();

        $invoiceId = $estimateId = $inquiryId = 0;
        $countBookingDiscussions = $countInvoiceDiscussions = $countInquiryDiscussions = $countEstimateDiscussions = $countComplaintDiscussions = 0;
        $isContractor = false;
        $loggedUser = CheckAuth::getLoggedUser();

        $contractorRoleId = $modelAuthRole->getRoleIdByName('contractor');
        if ($loggedUser['role_id'] == $contractorRoleId) {
            $isContractor = true;
        }

        if ($bookingId) {
            $invoice = $modelBookingInvoice->getByBookingId($bookingId);
            $estimate = $modelBookingEstimate->getByBookingId($bookingId);

            $invoiceId = !empty($invoice['id']) ? $invoice['id'] : 0;
            $estimateId = !empty($estimate['id']) ? $estimate['id'] : 0;
            $inquiryId = !empty($booking['original_inquiry_id']) ? $booking['original_inquiry_id'] : 0;

            /* $this->view->bookingDiscussions = $modelBookingDiscussion->getByBookingId($bookingId);
              $this->view->invoiceDiscussions = $modelInvoiceDiscussion->getByInvoiceId($invoiceId);
              $this->view->estimateDiscussions = $modelEstimateDiscussion->getByEstimateId($estimateId);
              $this->view->inquiryDiscussions = $modelInquiryDiscussion->getByInquiryId($inquiryId); */
            $bookingDiscussions = $modelBookingDiscussion->getByBookingId($bookingId, 'DESC');
            $invoiceDiscussions = $modelInvoiceDiscussion->getByInvoiceId($invoiceId, 'DESC');
            $estimateDiscussions = $modelEstimateDiscussion->getByEstimateId($estimateId, 'DESC');
            $inquiryDiscussions = $modelInquiryDiscussion->getByInquiryId($inquiryId, 'DESC');

            $countBookingDiscussions = $modelBookingDiscussion->countDiscussions($bookingId, $isContractor);
            $countInvoiceDiscussions = $modelInvoiceDiscussion->countDiscussions($invoiceId, $isContractor);
            $countEstimateDiscussions = $modelEstimateDiscussion->countDiscussions($estimateId, $isContractor);
            $countInquiryDiscussions = $modelInquiryDiscussion->countDiscussions($inquiryId, $isContractor);

            if ($bookingDiscussions) {

                foreach ($bookingDiscussions as &$discussion) {
                    if ($discussion['user_role'] == 9) {
                        $modelCustomer = new Model_Customer();
                        $discussion['user'] = $modelCustomer->getById($discussion['user_id']);
                        $username = $discussion['user']['first_name'] . ' ' . $discussion['user']['last_name'];
                    } else {
                        $discussion['user'] = $modelUser->getById($discussion['user_id']);
                        $username = $discussion['user']['username'];
                    }
                    if (isset($discussion['user']['role_id']) && $discussion['user']['role_id'] == $modelAuthRole->getRoleIdByName('contractor')) {
                        $contractorInfo = $modelContractorInfo->getByContractorId($discussion['user']['user_id']);
                        $discussion['user']['username'] = ucwords($username) . ($contractorInfo['business_name'] ? ' - ' . ucwords($contractorInfo['business_name']) : '');
                    } else {
                        $discussion['user']['username'] = ucwords($username);
                    }
                    $discussion['discussion_date'] = getDateFormating($discussion['created']);
                    $discussion['user_message'] = nl2br(htmlentities($discussion['user_message'], ENT_NOQUOTES, 'UTF-8'));


                    $itemImageDiscussion = $modelItemImageDiscussion->getByDiscussionIdAndType($discussion['discussion_id'], 'booking');
                    if ($itemImageDiscussion) {
                        if ($itemImageDiscussion[0]['group_id'] != 0) {
                            $discussion['group_id'] = $itemImageDiscussion[0]['group_id'];
                            $discussion['images'] = $modelImage->getByGroupId($itemImageDiscussion[0]['group_id']);
                        } else if ($itemImageDiscussion[0]['image_id'] != 0) {
                            $image = $modelImage->getByImageId($itemImageDiscussion[0]['image_id']);
                            $discussion['image_id'] = $itemImageDiscussion[0]['image_id'];
                            $imageAfterProccessing = array();
                            $imageAfterProccessing[] = $image;
                            $discussion['images'] = $imageAfterProccessing;
                            //var_dump($image);
                        }

                        if (!empty($discussion['images'])) {
                            foreach ($discussion['images'] as &$bookingGroupedImage) {
                                $bookingGroupedImage['imageDiscussions'] = $modelBookingDiscussion->getByImageId($bookingGroupedImage['image_id'], 'DESC');

                                if ($bookingGroupedImage['imageDiscussions']) {
                                    foreach ($bookingGroupedImage['imageDiscussions'] as &$imageDiscussion) {

                                        if ($imageDiscussion['user_role'] == 9) {
                                            $modelCustomer = new Model_Customer();
                                            $imageDiscussion['user'] = $modelCustomer->getById($imageDiscussion['user_id']);
                                            $username = $imageDiscussion['user']['first_name'] . ' ' . $imageDiscussion['user']['last_name'];
                                        } else {
                                            $imageDiscussion['user'] = $modelUser->getById($imageDiscussion['user_id']);
                                            $username = $imageDiscussion['user']['username'];
                                        }

                                        if (isset($imageDiscussion['user']['role_id']) && $imageDiscussion['user']['role_id'] == $modelAuthRole->getRoleIdByName('contractor')) {
                                            $contractorInfo = $modelContractorInfo->getByContractorId($imageDiscussion['user']['user_id']);
                                            $imageDiscussion['user']['username'] = ucwords($username) . ($contractorInfo['business_name'] ? ' - ' . ucwords($contractorInfo['business_name']) : '');
                                        } else {
                                            $imageDiscussion['user']['username'] = ucwords($username);
                                        }

                                        $imageDiscussion['discussion_date'] = getDateFormating($imageDiscussion['created']);
                                        $imageDiscussion['user_message'] = nl2br(htmlentities($imageDiscussion['user_message'], ENT_NOQUOTES, 'UTF-8'));
                                    }
                                }
                            }
                        }
                    }
                }
                //var_dump($bookingDiscussions);
            }
            if ($invoiceDiscussions) {

                foreach ($invoiceDiscussions as &$discussion) {
                    if ($discussion['user_role'] == 9) {
                        $modelCustomer = new Model_Customer();
                        $discussion['user'] = $modelCustomer->getById($discussion['user_id']);
                        $username = $discussion['user']['first_name'] . ' ' . $discussion['user']['last_name'];
                    } else {
                        $discussion['user'] = $modelUser->getById($discussion['user_id']);
                        $username = $discussion['user']['username'];
                    }
                    if (isset($discussion['user']['role_id']) && $discussion['user']['role_id'] == $modelAuthRole->getRoleIdByName('contractor')) {
                        $contractorInfo = $modelContractorInfo->getByContractorId($discussion['user']['user_id']);
                        $discussion['user_name'] = ucwords($username) . ($contractorInfo['business_name'] ? ' - ' . ucwords($contractorInfo['business_name']) : '');
                    } else {
                        $discussion['user_name'] = ucwords($username);
                    }
                    $itemImageDiscussion = $modelItemImageDiscussion->getByDiscussionIdAndType($discussion['discussion_id'], 'invoice');
                    if ($itemImageDiscussion) {
                        if ($itemImageDiscussion[0]['group_id'] != 0) {
                            $discussion['group_id'] = $itemImageDiscussion[0]['group_id'];
                            $discussion['images'] = $modelImage->getByGroupId($itemImageDiscussion[0]['group_id']);
                        } else if ($itemImageDiscussion[0]['image_id'] != 0) {
                            $image = $modelImage->getByImageId($itemImageDiscussion[0]['image_id']);
                            $discussion['image_id'] = $itemImageDiscussion[0]['image_id'];
                            $imageAfterProccessing = array();
                            $imageAfterProccessing[] = $image;
                            $discussion['images'] = $imageAfterProccessing;
                            //var_dump($image);
                        }
                    }
                }
            }
            if ($estimateDiscussions) {

                foreach ($estimateDiscussions as &$discussion) {
                    if ($discussion['user_role'] == 9) {
                        $modelCustomer = new Model_Customer();
                        $discussion['user'] = $modelCustomer->getById($discussion['user_id']);
                        $username = $discussion['user']['first_name'] . ' ' . $discussion['user']['last_name'];
                    } else {
                        $discussion['user'] = $modelUser->getById($discussion['user_id']);
                        $username = $discussion['user']['username'];
                    }
                    if (isset($discussion['user']['role_id']) && $discussion['user']['role_id'] == $modelAuthRole->getRoleIdByName('contractor')) {
                        $contractorInfo = $modelContractorInfo->getByContractorId($discussion['user']['user_id']);
                        $discussion['user_name'] = ucwords($username) . ($contractorInfo['business_name'] ? ' - ' . ucwords($contractorInfo['business_name']) : '');
                    } else {
                        $discussion['user_name'] = ucwords($username);
                    }
                    $discussion['user_message'] = nl2br(htmlentities($discussion['user_message'], ENT_NOQUOTES, 'UTF-8'));
                    $itemImageDiscussion = $modelItemImageDiscussion->getByDiscussionIdAndType($discussion['discussion_id'], 'estimate');
                    if ($itemImageDiscussion) {
                        if ($itemImageDiscussion[0]['group_id'] != 0) {
                            $discussion['group_id'] = $itemImageDiscussion[0]['group_id'];
                            $discussion['images'] = $modelImage->getByGroupId($itemImageDiscussion[0]['group_id']);
                        } else if ($itemImageDiscussion[0]['image_id'] != 0) {
                            $image = $modelImage->getByImageId($itemImageDiscussion[0]['image_id']);
                            $discussion['image_id'] = $itemImageDiscussion[0]['image_id'];
                            $imageAfterProccessing = array();
                            $imageAfterProccessing[] = $image;
                            $discussion['images'] = $imageAfterProccessing;
                            //var_dump($image);
                        }
                        if (!empty($discussion['images'])) {
                            foreach ($discussion['images'] as &$estimateGroupedImage) {
                                $estimateGroupedImage['imageDiscussions'] = $modelEstimateDiscussion->getByImageId($estimateGroupedImage['image_id'], 'DESC');
                                if ($estimateGroupedImage['imageDiscussions']) {
                                    foreach ($estimateGroupedImage['imageDiscussions'] as &$estimateImageDiscussion) {
                                        $estimateImageDiscussion['user'] = $modelUser->getById($estimateImageDiscussion['user_id']);
                                        if (isset($estimateImageDiscussion['user_role']) && $estimateImageDiscussion['user_role'] == $modelAuthRole->getRoleIdByName('customer')) {
                                            $modelCustomer = new Model_Customer();
                                            $estimateImageDiscussion['user'] = $modelCustomer->getById($estimateImageDiscussion['user_id']);
                                            $username = $estimateImageDiscussion['user']['first_name'] . ' ' . $estimateImageDiscussion['user']['last_name'];
                                        } else {
                                            $estimateImageDiscussion['user'] = $modelUser->getById($estimateImageDiscussion['user_id']);
                                            $username = $estimateImageDiscussion['user']['username'];
                                        }
                                        if ($estimateImageDiscussion['user']['role_name'] == 'contractor') {
                                            $contractorInfo = $modelContractorInfo->getByContractorId($estimateImageDiscussion['user']['user_id']);
                                            $estimateImageDiscussion['user']['username'] = ucwords($estimateImageDiscussion['user']['username']) . ($contractorInfo['business_name'] ? ' - ' . ucwords($contractorInfo['business_name']) : '');
                                        } else {
                                            $estimateImageDiscussion['user']['username'] = ucwords($username);
                                        }


                                        $estimateImageDiscussion['discussion_date'] = getDateFormating($estimateImageDiscussion['created']);
                                        $estimateImageDiscussion['user_message'] = nl2br(htmlentities($estimateImageDiscussion['user_message'], ENT_NOQUOTES, 'UTF-8'));
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if ($inquiryDiscussions) {

                foreach ($inquiryDiscussions as &$discussion) {
                    $discussion['user_message'] = nl2br(htmlentities($discussion['user_message'], ENT_NOQUOTES, 'UTF-8'));
                    if (isset($discussion['user_role']) && $discussion['user_role'] == 9) {
                        $modelCustomer = new Model_Customer();
                        $discussion['user'] = $modelCustomer->getById($discussion['user_id']);
                        $discussion['user']['username'] = $discussion['user']['first_name'] . ' ' . $discussion['user']['last_name'];
                    } else {
                        $discussion['user'] = $modelUser->getById($discussion['user_id']);
                        $discussion['user']['username'] = ucwords($discussion['user']['username']);
                    }
                    $itemImageDiscussion = $modelItemImageDiscussion->getByDiscussionIdAndType($discussion['discussion_id'], 'inquiry');
                    if ($itemImageDiscussion) {
                        if ($itemImageDiscussion[0]['group_id'] != 0) {
                            $discussion['group_id'] = $itemImageDiscussion[0]['group_id'];
                            $discussion['images'] = $modelImage->getByGroupId($itemImageDiscussion[0]['group_id']);
                        } else if ($itemImageDiscussion[0]['image_id'] != 0) {
                            $image = $modelImage->getByImageId($itemImageDiscussion[0]['image_id']);
                            $discussion['image_id'] = $itemImageDiscussion[0]['image_id'];
                            $imageAfterProccessing = array();
                            $imageAfterProccessing[] = $image;
                            $discussion['images'] = $imageAfterProccessing;
                            //var_dump($image);
                        }

                        if (!empty($discussion['images'])) {
                            foreach ($discussion['images'] as &$inquiryGroupedImage) {
                                $inquiryGroupedImage['imageDiscussions'] = $modelInquiryDiscussion->getByImageId($inquiryGroupedImage['image_id'], 'DESC');
                                if ($inquiryGroupedImage['imageDiscussions']) {
                                    foreach ($inquiryGroupedImage['imageDiscussions'] as &$inquiryImageDiscussion) {
                                        $inquiryImageDiscussion['discussion_date'] = getDateFormating($inquiryImageDiscussion['created']);
                                        $inquiryImageDiscussion['user_message'] = nl2br(htmlentities($inquiryImageDiscussion['user_message'], ENT_NOQUOTES, 'UTF-8'));

                                        if (isset($inquiryImageDiscussion['user_role']) && $inquiryImageDiscussion['user_role'] == $modelAuthRole->getRoleIdByName('customer')) {
                                            $modelCustomer = new Model_Customer();
                                            $inquiryImageDiscussion['user'] = $modelCustomer->getById($inquiryImageDiscussion['user_id']);
                                            $username = $inquiryImageDiscussion['user']['first_name'] . ' ' . $inquiryImageDiscussion['user']['last_name'];
                                        } else {
                                            $inquiryImageDiscussion['user'] = $modelUser->getById($inquiryImageDiscussion['user_id']);
                                            $username = $inquiryImageDiscussion['user']['username'];
                                        }

                                        if ($inquiryImageDiscussion['user']['role_id'] == $modelAuthRole->getRoleIdByName('contractor')) {
                                            $contractorInfo = $modelContractorInfo->getByContractorId($inquiryImageDiscussion['user']['user_id']);
                                            $inquiryImageDiscussion['user']['username'] = ucwords($inquiryImageDiscussion['user']['username']) . ($contractorInfo['business_name'] ? ' - ' . ucwords($contractorInfo['business_name']) : '');
                                        } else {
                                            $inquiryImageDiscussion['user']['username'] = ucwords($username);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            $this->view->bookingDiscussions = $bookingDiscussions;
            $this->view->invoiceDiscussions = $invoiceDiscussions;
            $this->view->estimateDiscussions = $estimateDiscussions;
            $this->view->inquiryDiscussions = $inquiryDiscussions;
        } else if ($inquiry_id && (CheckAuth::getRoleName() != 'customer')) {
            //$this->view->inquiryDiscussions = $modelInquiryDiscussion->getByInquiryId($inquiry_id);
            $inquiryDiscussions = $modelInquiryDiscussion->getByInquiryId($inquiry_id, 'DESC');
            $countInquiryDiscussions = $modelInquiryDiscussion->countDiscussions($inquiry_id, $isContractor);
            if ($inquiryDiscussions) {

                foreach ($inquiryDiscussions as &$discussion) {
                    $discussion['user_message'] = nl2br(htmlentities($discussion['user_message'], ENT_NOQUOTES, 'UTF-8'));
                    if (isset($discussion['user_role']) && $discussion['user_role'] == 9) {
                        $modelCustomer = new Model_Customer();
                        $discussion['user'] = $modelCustomer->getById($discussion['user_id']);
                        $discussion['user']['username'] = $discussion['user']['first_name'] . ' ' . $discussion['user']['last_name'];
                    } else {
                        $discussion['user'] = $modelUser->getById($discussion['user_id']);
                        $discussion['user']['username'] = ucwords($discussion['user']['username']);
                    }
                    $itemImageDiscussion = $modelItemImageDiscussion->getByDiscussionIdAndType($discussion['discussion_id'], 'inquiry');
                    if ($itemImageDiscussion) {
                        if ($itemImageDiscussion[0]['group_id'] != 0) {
                            $discussion['group_id'] = $itemImageDiscussion[0]['group_id'];
                            $discussion['images'] = $modelImage->getByGroupId($itemImageDiscussion[0]['group_id']);
                        } else if ($itemImageDiscussion[0]['image_id'] != 0) {
                            $image = $modelImage->getByImageId($itemImageDiscussion[0]['image_id']);
                            $discussion['image_id'] = $itemImageDiscussion[0]['image_id'];
                            $imageAfterProccessing = array();
                            $imageAfterProccessing[] = $image;
                            $discussion['images'] = $imageAfterProccessing;
                            //var_dump($image);
                        }

                        if (!empty($discussion['images'])) {
                            foreach ($discussion['images'] as &$inquiryGroupedImage) {
                                $inquiryGroupedImage['imageDiscussions'] = $modelInquiryDiscussion->getByImageId($inquiryGroupedImage['image_id'], 'DESC');
                                if ($inquiryGroupedImage['imageDiscussions']) {
                                    foreach ($inquiryGroupedImage['imageDiscussions'] as &$inquiryImageDiscussion) {
                                        $inquiryImageDiscussion['discussion_date'] = getDateFormating($inquiryImageDiscussion['created']);
                                        $inquiryImageDiscussion['user_message'] = nl2br(htmlentities($inquiryImageDiscussion['user_message'], ENT_NOQUOTES, 'UTF-8'));

                                        if (isset($inquiryImageDiscussion['user_role']) && $inquiryImageDiscussion['user_role'] == $modelAuthRole->getRoleIdByName('customer')) {
                                            $modelCustomer = new Model_Customer();
                                            $inquiryImageDiscussion['user'] = $modelCustomer->getById($inquiryImageDiscussion['user_id']);
                                            $username = $inquiryImageDiscussion['user']['first_name'] . ' ' . $inquiryImageDiscussion['user']['last_name'];
                                        } else {
                                            $inquiryImageDiscussion['user'] = $modelUser->getById($inquiryImageDiscussion['user_id']);
                                            $username = $inquiryImageDiscussion['user']['username'];
                                        }

                                        if ($inquiryImageDiscussion['user']['role_id'] == $modelAuthRole->getRoleIdByName('contractor')) {
                                            $contractorInfo = $modelContractorInfo->getByContractorId($inquiryImageDiscussion['user']['user_id']);
                                            $inquiryImageDiscussion['user']['username'] = ucwords($inquiryImageDiscussion['user']['username']) . ($contractorInfo['business_name'] ? ' - ' . ucwords($contractorInfo['business_name']) : '');
                                        } else {
                                            $inquiryImageDiscussion['user']['username'] = ucwords($username);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            $this->view->inquiryDiscussions = $inquiryDiscussions;
        }
        if ($complaintId && (CheckAuth::getRoleName() != 'customer')) {
            //$this->view->complaintDiscussions = $modelComplaintDiscussion->getByComplaintId($complaintId);
            $complaintDiscussions = $modelComplaintDiscussion->getByComplaintId($complaintId, 'DESC');
            $countComplaintDiscussions = $modelComplaintDiscussion->countDiscussions($complaintId, $isContractor);
            if ($complaintDiscussions) {

                foreach ($complaintDiscussions as &$discussion) {
                    if ($discussion['user_role'] == 9) {
                        $modelCustomer = new Model_Customer();
                        $discussion['user'] = $modelCustomer->getById($discussion['user_id']);
                        $username = $discussion['user']['first_name'] . ' ' . $discussion['user']['last_name'];
                    } else {
                        $discussion['user'] = $modelUser->getById($discussion['user_id']);
                        $username = $discussion['user']['username'];
                    }
                    if (isset($discussion['user']['role_id']) && $discussion['user']['role_id'] == $modelAuthRole->getRoleIdByName('contractor')) {
                        $contractorInfo = $modelContractorInfo->getByContractorId($discussion['user']['user_id']);
                        $discussion['user_name'] = ucwords($username) . ($contractorInfo['business_name'] ? ' - ' . ucwords($contractorInfo['business_name']) : '');
                    } else {
                        $discussion['user_name'] = ucwords($username);
                    }
                    $discussion['user_message'] = nl2br(htmlentities($discussion['user_message'], ENT_NOQUOTES, 'UTF-8'));
                    $itemImageDiscussion = $modelItemImageDiscussion->getByDiscussionIdAndType($discussion['discussion_id'], 'complaint');
                    if ($itemImageDiscussion) {
                        if ($itemImageDiscussion[0]['group_id'] != 0) {
                            $discussion['group_id'] = $itemImageDiscussion[0]['group_id'];
                            $discussion['images'] = $modelImage->getByGroupId($itemImageDiscussion[0]['group_id']);
                        } else if ($itemImageDiscussion[0]['image_id'] != 0) {
                            $image = $modelImage->getByImageId($itemImageDiscussion[0]['image_id']);
                            $discussion['image_id'] = $itemImageDiscussion[0]['image_id'];
                            $imageAfterProccessing = array();
                            $imageAfterProccessing[] = $image;
                            $discussion['images'] = $imageAfterProccessing;
                            //var_dump($image);
                        }

                        if (!empty($discussion['images'])) {
                            foreach ($discussion['images'] as &$complaintGroupedImage) {
                                $complaintGroupedImage['imageDiscussions'] = $modelComplaintDiscussion->getDiscussionByImageId($complaintGroupedImage['image_id'], 'DESC');

                                if ($complaintGroupedImage['imageDiscussions']) {
                                    foreach ($complaintGroupedImage['imageDiscussions'] as &$complaintImageDiscussion) {
                                        //$complaintImageDiscussion['user'] = $modelUser->getById($complaintImageDiscussion['user_id']);

                                        if (isset($complaintImageDiscussion['user_role']) && $complaintImageDiscussion['user_role'] == $modelAuthRole->getRoleIdByName('customer')) {
                                            $modelCustomer = new Model_Customer();
                                            $complaintImageDiscussion['user'] = $modelCustomer->getById($complaintImageDiscussion['user_id']);
                                            $username = $complaintImageDiscussion['user']['first_name'] . ' ' . $complaintImageDiscussion['user']['last_name'];
                                        } else {
                                            $complaintImageDiscussion['user'] = $modelUser->getById($complaintImageDiscussion['user_id']);
                                            $username = $complaintImageDiscussion['user']['username'];
                                        }


                                        if ($complaintImageDiscussion['user']['role_name'] == 'contractor') {
                                            $contractorInfo = $modelContractorInfo->getByContractorId($complaintImageDiscussion['user']['user_id']);
                                            $complaintImageDiscussion['user']['username'] = ucwords($complaintImageDiscussion['user']['username']) . ($contractorInfo['business_name'] ? ' - ' . ucwords($contractorInfo['business_name']) : '');
                                        } else {
                                            $complaintImageDiscussion['user']['username'] = ucwords($username);
                                        }


                                        $complaintImageDiscussion['discussion_date'] = getDateFormating($complaintImageDiscussion['created']);
                                        $complaintImageDiscussion['user_message'] = nl2br(htmlentities($complaintImageDiscussion['user_message'], ENT_NOQUOTES, 'UTF-8'));
                                    }
                                }
                            }
                        }
                    }
                }
            }
            $this->view->complaintDiscussions = $complaintDiscussions;
        }





        $countAll = $countBookingDiscussions + $countInvoiceDiscussions + $countInquiryDiscussions + $countEstimateDiscussions + $countComplaintDiscussions;

        $json_array = array(
            'result' => $this->view->render('discussion/get-all-tabs-discussion.phtml'),
            'countAll' => $countAll
        );

        echo json_encode($json_array);
        exit;
    }

    public function deleteItemCommentAction() {

        $discussion_id = $this->request->getParam("discussion_id");
        $type = $this->request->getParam("type");
        $have_images = $this->request->getParam('have_images', 0);
        $image_id = $this->request->getParam('image_id', 0);
        $group_id = $this->request->getParam('group_id', 0);
        $deleteDis = 0;
        $item_id = $this->request->getParam('id', 0);

        if ($type == 'booking') {
            CheckAuth::checkPermission(array('bookingDiscussion'));
        } else if ($type == 'estimate') {
            CheckAuth::checkPermission(array('estimateDiscussion'));
        } else if ($type == 'inquiry') {
            CheckAuth::checkPermission(array('inquiryDiscussion'));
        } else if ($type == 'invoice') {
            CheckAuth::checkPermission(array('invoiceDiscussion'));
        } else if ($type == 'complaint') {
            CheckAuth::checkPermission(array('complaintDiscussion'));
        }

        $modelBookingDiscussion = new Model_BookingDiscussion();
        $modelComplaintDiscussion = new Model_ComplaintDiscussion();
        $modelEstimateDiscussion = new Model_EstimateDiscussion();
        $modelInquiryDiscussion = new Model_InquiryDiscussion();
        $modelInvoiceDiscussion = new Model_InvoiceDiscussion();
        $modelItemImageDiscussion = new Model_ItemImageDiscussion();

        if ($type == 'booking') {
            $deleteDis = $modelBookingDiscussion->deleteByMongoId($discussion_id);
        } else if ($type == 'invoice') {
            $deleteDis = $modelInvoiceDiscussion->deleteByMongoId($discussion_id);
        } else if ($type == 'inquiry') {
            $deleteDis = $modelInquiryDiscussion->deleteByMongoId($discussion_id);
        } else if ($type == 'estimate') {
            $deleteDis = $modelEstimateDiscussion->deleteByMongoId($discussion_id);
        } else if ($type == 'complaint') {
            $deleteDis = $modelComplaintDiscussion->deleteByMongoId($discussion_id);
        }

        if ($have_images) {

            $successDelete = 0;
            $successDeletes = array();
            $modelImage = new Model_Image();

            if ($image_id) {

                $successDelete = $modelImage->deleteById($image_id);
                $filters['image_id'] = $image_id;
                $itemImageDiscussions = $modelItemImageDiscussion->getAll($filters);
                $data = array(
                    'image_id' => 0
                );
                foreach ($itemImageDiscussions as $itemImageDiscussion) {
                    $update = $modelItemImageDiscussion->updateById($itemImageDiscussion['item_image_discussion_id'], $data);
                }
            } else if ($group_id) {

                $booking_grouped_images = $modelImage->getByGroupId($group_id);

                //var_dump($booking_grouped_images);
                foreach ($booking_grouped_images as $k => $img) {
                    $successDeletes[$k] = $modelImage->deleteById($img['image_id']);
                    $filters['image_id'] = $img['image_id'];
                    $itemImageDiscussions = $modelItemImageDiscussion->getAll($filters);
                    $data = array(
                        'image_id' => 0
                    );
                    foreach ($itemImageDiscussions as $itemImageDiscussion) {
                        $update = $modelItemImageDiscussion->updateById($itemImageDiscussion['item_image_discussion_id'], $data);
                    }
                }
            }


            $company_id = CheckAuth::getCompanySession();

            if ($type == 'booking') {
                require_once 'Zend/Cache.php';
                $company_id = CheckAuth::getCompanySession();

                //D.A 30/08/2015 Remove Booking Photos Cache				
                $bookingPhotosCacheID = $item_id . '_bookingPhotos';
                $bookingViewDir = get_config('cache') . '/' . 'bookingsView' . '/' . $company_id;
                if (!is_dir($bookingViewDir)) {
                    mkdir($bookingViewDir, 0777, true);
                }
                $frontEndOption = array('lifetime' => NULL,
                    'automatic_serialization' => true);
                $backendOptions = array('cache_dir' => $bookingViewDir);
                $Cache = Zend_Cache::factory('Core', 'File', $frontEndOption, $backendOptions);
                $Cache->remove($bookingPhotosCacheID);



                $modelBooking = new Model_Booking();
                $modelBookingEstimate = new Model_BookingEstimate();
                $booking = $modelBooking->getById($item_id);
                $estimate = $modelBookingEstimate->getByBookingId($item_id);
                if ($estimate) {
                    $estimateLabelsCacheID = $estimate['id'] . '_estimatePhoto';
                    $estimateViewDir = get_config('cache') . '/' . 'estimatesView' . '/' . $company_id;
                    if (!is_dir($estimateViewDir)) {
                        mkdir($estimateViewDir, 0777, true);
                    }
                    $estimatePhotoFrontEndOption = array('lifetime' => NULL,
                        'automatic_serialization' => true);
                    $estimatePhotoBackendOptions = array('cache_dir' => $estimateViewDir);
                    $estimatePhotoCache = Zend_Cache::factory('Core', 'File', $estimatePhotoFrontEndOption, $estimatePhotoBackendOptions);
                    $estimatePhotoCache->remove($estimateLabelsCacheID);
                }
                if ($booking['original_inquiry_id']) {
                    $inquiryPhotosCacheID = $booking['original_inquiry_id'] . '_inquiryPhotos';
                    $inquiryViewDir = get_config('cache') . '/' . 'inquiriesView' . '/' . $company_id;
                    if (!is_dir($inquiryViewDir)) {
                        mkdir($inquiryViewDir, 0777, true);
                    }
                    $inquiryPhotosFrontEndOption = array('lifetime' => NULL,
                        'automatic_serialization' => true);
                    $inquiryPhotosBackendOptions = array('cache_dir' => $inquiryViewDir);
                    $inquiryPhotosCache = Zend_Cache::factory('Core', 'File', $inquiryPhotosFrontEndOption, $inquiryPhotosBackendOptions);
                    $inquiryPhotosCache->remove($inquiryPhotosCacheID);
                }
            }

            //D.A 14/09/2015 Remove inquiry Photos Cache
            if ($type == 'inquiry') {
                $inquiryPhotosCacheID = $item_id . '_inquiryPhotos';
                $inquiryViewDir = get_config('cache') . '/' . 'inquiriesView' . '/' . $company_id;
                if (!is_dir($inquiryViewDir)) {
                    mkdir($inquiryViewDir, 0777, true);
                }
                $inquiryPhotosFrontEndOption = array('lifetime' => NULL,
                    'automatic_serialization' => true);
                $inquiryPhotosBackendOptions = array('cache_dir' => $inquiryViewDir);
                $inquiryPhotosCache = Zend_Cache::factory('Core', 'File', $inquiryPhotosFrontEndOption, $inquiryPhotosBackendOptions);
                $inquiryPhotosCache->remove($inquiryPhotosCacheID);

                //
                $modelBooking = new Model_Booking();
                $modelBookingEstimate = new Model_BookingEstimate();
                $booking = $modelBooking->getByInquiryId($item_id);
                if (isset($booking) && !empty($booking)) {
                    $bookingPhotosCacheID = $booking['booking_id'] . '_bookingPhotos';
                    $bookingViewDir = get_config('cache') . '/' . 'bookingsView' . '/' . $company_id;
                    if (!is_dir($bookingViewDir)) {
                        mkdir($bookingViewDir, 0777, true);
                    }
                    $frontEndOption = array('lifetime' => NULL,
                        'automatic_serialization' => true);
                    $backendOptions = array('cache_dir' => $bookingViewDir);
                    $Cache = Zend_Cache::factory('Core', 'File', $frontEndOption, $backendOptions);
                    $Cache->remove($bookingPhotosCacheID);
                    $estimate = $modelBookingEstimate->getByBookingId($booking['booking_id']);
                    if ($estimate) {
                        $estimatePhotosCacheID = $estimate['id'] . '_estimatePhoto';
                        $estimateViewDir = get_config('cache') . '/' . 'estimatesView' . '/' . $company_id;
                        if (!is_dir($estimateViewDir)) {
                            mkdir($estimateViewDir, 0777, true);
                        }
                        $estimatePhotoFrontEndOption = array('lifetime' => NULL,
                            'automatic_serialization' => true);
                        $estimatePhotoBackendOptions = array('cache_dir' => $estimateViewDir);
                        $estimatePhotoCache = Zend_Cache::factory('Core', 'File', $estimatePhotoFrontEndOption, $estimatePhotoBackendOptions);
                        $estimatePhotoCache->remove($estimatePhotosCacheID);
                    }
                }
            }

            //D.A 20/09/2015 Remove Estimate Photos Cache
            if ($type == 'estimate') {
                $modelBooking = new Model_Booking();
                $modelBookingEstimate = new Model_BookingEstimate();
                $estimate = $modelBookingEstimate->getById($item_id);
                $booking = $modelBooking->getById($estimate['booking_id']);
                if ($booking['original_inquiry_id']) {
                    $inquiryPhotosCacheID = $booking['original_inquiry_id'] . '_inquiryPhotos';
                    $inquiryViewDir = get_config('cache') . '/' . 'inquiriesView' . '/' . $company_id;
                    if (!is_dir($inquiryViewDir)) {
                        mkdir($inquiryViewDir, 0777, true);
                    }
                    $inquiryPhotosFrontEndOption = array('lifetime' => NULL,
                        'automatic_serialization' => true);
                    $inquiryPhotosBackendOptions = array('cache_dir' => $inquiryViewDir);
                    $inquiryPhotosCache = Zend_Cache::factory('Core', 'File', $inquiryPhotosFrontEndOption, $inquiryPhotosBackendOptions);
                    $inquiryPhotosCache->remove($inquiryPhotosCacheID);
                }
                $estimateLabelsCacheID = $item_id . '_estimatePhoto';
                $estimateViewDir = get_config('cache') . '/' . 'estimatesView' . '/' . $company_id;
                if (!is_dir($estimateViewDir)) {
                    mkdir($estimateViewDir, 0777, true);
                }
                $estimatePhotoFrontEndOption = array('lifetime' => NULL,
                    'automatic_serialization' => true);
                $estimatePhotoBackendOptions = array('cache_dir' => $estimateViewDir);
                $estimatePhotoCache = Zend_Cache::factory('Core', 'File', $estimatePhotoFrontEndOption, $estimatePhotoBackendOptions);
                $estimatePhotoCache->remove($estimateLabelsCacheID);

                //D.A 20/09/2015 Remove Booking Photos Cache

                $bookingPhotosCacheID = $estimate['booking_id'] . '_bookingPhotos';
                $bookingViewDir = get_config('cache') . '/' . 'bookingsView' . '/' . $company_id;
                if (!is_dir($bookingViewDir)) {
                    mkdir($bookingViewDir, 0777, true);
                }
                $frontEndOption = array('lifetime' => NULL,
                    'automatic_serialization' => true);
                $backendOptions = array('cache_dir' => $bookingViewDir);
                $Cache = Zend_Cache::factory('Core', 'File', $frontEndOption, $backendOptions);
                $Cache->remove($bookingPhotosCacheID);
            }

            if ($deleteDis && ($successDelete || !in_array(0, $successDeletes))) {
                echo 1;
            }
            exit;
        }

        if ($deleteDis) {
            echo 1;
        }
        exit;

        /* $modelBookingDiscussionBeforeMongo = new Model_BookingDiscussionBeforeMongo();
          $lastId = $modelBookingDiscussionBeforeMongo->getLastBookingDiscussionByBookingId(1);
          var_dump($lastId);
          exit;

          /*$modelBookingDiscussionSeq = new Model_BookingDiscussionSeq();
          $result = $modelBookingDiscussionSeq->updateSeq();
          echo $result;
          exit; */

        /* $modelBookingDiscussion = new Model_BookingDiscussion();
          $result = $modelBookingDiscussion->getByImageId(2409);
          var_dump($result);exit; */
        /* $modelComplaintDiscussionBeforeMongo = new Model_ComplaintDiscussionBeforeMongo();
          //$modelBookingDiscussionBeforeMongo->getByImageId(2409, 'DESC', 200);
          $modelComplaintDiscussionBeforeMongo->getLastDiscussionByComplaintId(1234); */

        /* $modelComplaintDiscussionBeforeMongo = new Model_ComplaintDiscussionBeforeMongo();
          $lastId = $modelComplaintDiscussionBeforeMongo->getLastDiscussionId();
          var_dump($lastId);
          exit; */

        /* $modelEstimateDiscussionBeforeMongo = new Model_EstimateDiscussionBeforeMongo();
          $lastId = $modelEstimateDiscussionBeforeMongo->getLastDiscussionId();
          var_dump($lastId);
          exit; */

        /* $modelInquiryDiscussionBeforeMongo = new Model_InquiryDiscussionBeforeMongo();
          $lastId = $modelInquiryDiscussionBeforeMongo->getLastDiscussionId();
          var_dump($lastId);
          exit; */

        /* $modelInvoiceDiscussionBeforeMongo = new Model_InvoiceDiscussionBeforeMongo();
          $lastId = $modelInvoiceDiscussionBeforeMongo->getLastDiscussionId();
          var_dump($lastId);
          exit; */

        /* $modelBookingDiscussionBeforeMongo = new Model_BookingDiscussionBeforeMongo();
          $discussions = $modelBookingDiscussionBeforeMongo->getById(0);
          var_dump($discussions);

          echo "walaa echo";
          $modelBookingDiscussion = new Model_BookingDiscussion();
          $discussions = $modelBookingDiscussion->getById(0);
          var_dump($discussions);
          exit;

          $modelComplaintDiscussion = new Model_ComplaintDiscussion();
          $result = $modelComplaintDiscussion->getLastDiscussionByComplaintId(505);
          var_dump($result);
          exit; */

        /* $modelBookingDiscussionBeforeMongo = new Model_BookingDiscussionBeforeMongo();
          $discussions = $modelBookingDiscussionBeforeMongo->getRecentDiscussion();
          var_dump($discussions);

          echo "walaa echo";exit;
          $modelBookingDiscussion = new Model_BookingDiscussion();
          $discussions = $modelBookingDiscussion->getRecentDiscussion();
          var_dump($discussions);exit;
          /*$modelComplaintDiscussionBeforeMongo = new Model_ComplaintDiscussionBeforeMongo();
          $result = $modelComplaintDiscussionBeforeMongo->getLastDiscussionByComplaintId(10);
          var_dump($result);
          echo "walaa echo";
          $modelComplaintDiscussion = new Model_ComplaintDiscussion();
          $result = $modelComplaintDiscussion->getLastDiscussionByComplaintId(10);
          var_dump($result);
          exit; */
    }

    public function changeCommentSeenFlagAction() {

        $id = $this->request->getParam("discussion_id");
        $type = $this->request->getParam("type");

        if ($type == 'booking') {
            CheckAuth::checkPermission(array('bookingDiscussion'));
        } else if ($type == 'estimate') {
            CheckAuth::checkPermission(array('estimateDiscussion'));
        } else if ($type == 'inquiry') {
            CheckAuth::checkPermission(array('inquiryDiscussion'));
        } else if ($type == 'invoice') {
            CheckAuth::checkPermission(array('invoiceDiscussion'));
        } else if ($type == 'complaint') {
            CheckAuth::checkPermission(array('complaintDiscussion'));
        }

        $loggedUser = CheckAuth::getLoggedUser();

        $modelBookingDiscussion = new Model_BookingDiscussion();
        $modelComplaintDiscussion = new Model_ComplaintDiscussion();
        $modelEstimateDiscussion = new Model_EstimateDiscussion();
        $modelInquiryDiscussion = new Model_InquiryDiscussion();
        $modelInvoiceDiscussion = new Model_InvoiceDiscussion();

        if ($type == 'booking') {
            $success = $modelBookingDiscussion->changeCommentSeenFlagById($id, $loggedUser['user_id'], $loggedUser['username']);
        } else if ($type == 'invoice') {
            $success = $modelInvoiceDiscussion->changeCommentSeenFlagById($id, $loggedUser['user_id'], $loggedUser['username']);
        } else if ($type == 'inquiry') {
            $success = $modelInquiryDiscussion->changeCommentSeenFlagById($id, $loggedUser['user_id'], $loggedUser['username']);
        } else if ($type == 'estimate') {
            $success = $modelEstimateDiscussion->changeCommentSeenFlagById($id, $loggedUser['user_id'], $loggedUser['username']);
        } else if ($type == 'complaint') {
            $success = $modelComplaintDiscussion->changeCommentSeenFlagById($id, $loggedUser['user_id'], $loggedUser['username']);
        }

        if ($success) {
            echo 1;
        }
        exit;
    }

    public function getRelatedCustomersAction() {
        $item_id = $this->request->getParam('id', 0);
        $type = $this->request->getParam('type', '');
        $modelBooking = new Model_Booking();
        if ($type == 'booking') {
            $customer = $modelBooking->getCustomerByBookingId($item_id);
        } else if ($type == 'estimate') {
            $modelBookingEstimate = new Model_BookingEstimate();
            $bookingEstimate = $modelBookingEstimate->getById($item_id);
            $customer = $modelBooking->getCustomerByBookingId($bookingEstimate['booking_id']);
        } else if ($type == 'invoice') {
            $modelBookingInvoice = new Model_BookingInvoice();
            $bookingInvoice = $modelBookingInvoice->getById($item_id);
            $customer = $modelBooking->getCustomerByBookingId($bookingInvoice['booking_id']);
        } else if ($type == 'inquiry') {
            $modelInquiry = new Model_Inquiry();
            $customer = $modelInquiry->getCustomerByInquiryId($item_id);
        } else if ($type == 'complaint') {
            $modelComplaint = new Model_Complaint();
            $complaint = $modelComplaint->getById($item_id);
            $customer = $modelBooking->getCustomerByBookingId($complaint['booking_id']);
        }
        $customer['new_name'] = get_customer_name($customer);
        $customer['new_customer_id'] = 'c' . $customer['customer_id'];

        $result['data'] = $customer;
        echo json_encode($result);
        exit;
    }

    public function getAllImageDiscussionAction() {


        //get Params
        //
       $imageId = $this->request->getParam('id', 0);
        $type = $this->request->getParam('type');

        if ($type == 'booking') {
            CheckAuth::checkPermission(array('bookingDiscussion'));
        } else if ($type == 'estimate') {
            CheckAuth::checkPermission(array('estimateDiscussion'));
        } else if ($type == 'inquiry') {
            CheckAuth::checkPermission(array('inquiryDiscussion'));
        } else if ($type == 'invoice') {
            CheckAuth::checkPermission(array('invoiceDiscussion'));
        } else if ($type == 'complaint') {
            CheckAuth::checkPermission(array('complaintDiscussion'));
        }



        //
        // load model
        //
        $modelBookingDiscussion = new Model_BookingDiscussion();
        $modelComplaintDiscussion = new Model_ComplaintDiscussion();
        $modelEstimateDiscussion = new Model_EstimateDiscussion();
        $modelInquiryDiscussion = new Model_InquiryDiscussion();
        $modelContractorInfo = new Model_ContractorInfo();
        $modelUser = new Model_User();

        if ($type == 'booking') {
            $imageDiscussions = $modelBookingDiscussion->getByImageId($imageId, 'asc');
        } else if ($type == 'complaint') {
            $imageDiscussions = $modelComplaintDiscussion->getDiscussionByImageId($imageId, 'asc');
        } else if ($type == 'estimate') {
            $imageDiscussions = $modelEstimateDiscussion->getByImageId($imageId, 'asc');
        } else if ($type == 'inquiry') {
            $imageDiscussions = $modelInquiryDiscussion->getByImageId($imageId, 'asc');
        }

        if ($imageDiscussions) {
            foreach ($imageDiscussions as &$imageDiscussion) {
                $imageDiscussion['user'] = $modelUser->getById($imageDiscussion['user_id']);
                $imageDiscussion['discussion_date'] = time_ago($imageDiscussion['created']);
                $imageDiscussion['user_message'] = nl2br(htmlentities($imageDiscussion['user_message'], ENT_NOQUOTES, 'UTF-8'));
                if ($imageDiscussion['user']['role_name'] == 'contractor') {
                    $contractorInfo = $modelContractorInfo->getByContractorId($imageDiscussion['user']['user_id']);
                    $imageDiscussion['user']['username'] = ucwords($imageDiscussion['user']['username']) . ' - ' . ucwords($contractorInfo['business_name']);
                } else {
                    $imageDiscussion['user']['username'] = ucwords($imageDiscussion['user']['username']);
                }
            }
        }

        $json_array = array(
            'discussions' => $imageDiscussions,
            'count' => count($imageDiscussions)
        );



        echo json_encode($json_array);
        exit;
    }

    public function markAllCommentsAsSeenAction() {


        $item_id = $this->request->getParam("id", 0);
        $type = $this->request->getParam("type", '');
        $logged_user_id = $this->request->getParam("logged_user_id");

        if ($type == 'booking') {
            CheckAuth::checkPermission(array('bookingDiscussion'));
        } else if ($type == 'estimate') {
            CheckAuth::checkPermission(array('estimateDiscussion'));
        } else if ($type == 'inquiry') {
            CheckAuth::checkPermission(array('inquiryDiscussion'));
        } else if ($type == 'invoice') {
            CheckAuth::checkPermission(array('invoiceDiscussion'));
        } else if ($type == 'complaint') {
            CheckAuth::checkPermission(array('complaintDiscussion'));
        }

        $loggedUser = CheckAuth::getLoggedUser();
        $isContractor = false;

        $modelAuthRole = new Model_AuthRole();
        $contractorRoleId = $modelAuthRole->getRoleIdByName('contractor');

        if ($loggedUser['role_id'] == $contractorRoleId) {
            $isContractor = true;
        }

        $modelBookingDiscussion = new Model_BookingDiscussion();
        $modelComplaintDiscussion = new Model_ComplaintDiscussion();
        $modelEstimateDiscussion = new Model_EstimateDiscussion();
        $modelInquiryDiscussion = new Model_InquiryDiscussion();
        $modelInvoiceDiscussion = new Model_InvoiceDiscussion();
        $success = 0;
        if ($type == 'booking') {
            $success = $modelBookingDiscussion->markAllAsSeen($item_id, $loggedUser['username'], $logged_user_id, $isContractor);
        } else if ($type == 'invoice') {
            $success = $modelInvoiceDiscussion->markAllAsSeen($item_id, $loggedUser['username'], $logged_user_id, $isContractor);
        } else if ($type == 'inquiry') {
            $success = $modelInquiryDiscussion->markAllAsSeen($item_id, $loggedUser['username'], $logged_user_id, $isContractor);
        } else if ($type == 'estimate') {
            $success = $modelEstimateDiscussion->markAllAsSeen($item_id, $loggedUser['username'], $logged_user_id, $isContractor);
        } else if ($type == 'complaint') {
            $success = $modelComplaintDiscussion->markAllAsSeen($item_id, $loggedUser['username'], $logged_user_id, $isContractor);
        }

        if ($success) {
            echo 1;
        }
        exit;
    }

    public function markTabsCommentsAsSeenAction() {
        $bookingId = $this->request->getParam('bookingId', 0);
        $complaintId = $this->request->getParam('complaintId', 0);
        $inquiry_id = $this->request->getParam('inquiryId', 0);

        $modelBooking = new Model_Booking();
        $booking = $modelBooking->getById($bookingId);

        $modelBookingInvoice = new Model_BookingInvoice();
        $modelBookingEstimate = new Model_BookingEstimate();
        $modelAuthRole = new Model_AuthRole();

        $modelBookingDiscussion = new Model_BookingDiscussion();
        $modelComplaintDiscussion = new Model_ComplaintDiscussion();
        $modelEstimateDiscussion = new Model_EstimateDiscussion();
        $modelInquiryDiscussion = new Model_InquiryDiscussion();
        $modelInvoiceDiscussion = new Model_InvoiceDiscussion();

        $invoiceId = $estimateId = $inquiryId = 0;
        $updateBookingDiscussionsSuccess = $updateInvoiceDiscussionsSuccess = $updateInquiryDiscussionsSuccess = $updateEstimateDiscussionsSuccess = $updateComplaintDiscussionsSuccess = 0;
        $isContractor = false;
        $loggedUser = CheckAuth::getLoggedUser();

        $contractorRoleId = $modelAuthRole->getRoleIdByName('contractor');
        if ($loggedUser['role_id'] == $contractorRoleId) {
            $isContractor = true;
        }

        if ($bookingId) {
            $invoice = $modelBookingInvoice->getByBookingId($bookingId);
            $estimate = $modelBookingEstimate->getByBookingId($bookingId);

            $invoiceId = !empty($invoice['id']) ? $invoice['id'] : 0;
            $estimateId = !empty($estimate['id']) ? $estimate['id'] : 0;
            $inquiryId = !empty($booking['original_inquiry_id']) ? $booking['original_inquiry_id'] : 0;

            $updateBookingDiscussionsSuccess = $modelBookingDiscussion->markAllAsSeen($bookingId, $loggedUser['username'], $loggedUser['user_id'], $isContractor);

            $updateInvoiceDiscussionsSuccess = $modelInvoiceDiscussion->markAllAsSeen($invoiceId, $loggedUser['username'], $loggedUser['user_id'], $isContractor);

            $updateInquiryDiscussionsSuccess = $modelInquiryDiscussion->markAllAsSeen($inquiryId, $loggedUser['username'], $loggedUser['user_id'], $isContractor);

            $updateEstimateDiscussionsSuccess = $modelEstimateDiscussion->markAllAsSeen($estimateId, $loggedUser['username'], $loggedUser['user_id'], $isContractor);
        } else if ($inquiry_id) {
            $updateInquiryDiscussionsSuccess = $modelInquiryDiscussion->markAllAsSeen($inquiryId, $loggedUser['username'], $loggedUser['user_id'], $isContractor);
        }
        if ($complaintId) {
            $updateComplaintDiscussionsSuccess = $modelComplaintDiscussion->markAllAsSeen($complaintId, $loggedUser['username'], $loggedUser['user_id'], $isContractor);
        }
        if ($updateBookingDiscussionsSuccess || $updateEstimateDiscussionsSuccess || $updateInquiryDiscussionsSuccess || $updateInvoiceDiscussionsSuccess || $updateComplaintDiscussionsSuccess) {
            echo 1;
        }

        exit;

        /* $bookingId = $this->request->getParam('bookingId', 0);
          $complaintId = $this->request->getParam('complaintId', 0);
          $inquiry_id = $this->request->getParam('inquiryId', 0);
          //$logged_user_id = $this->request->getParam("logged_user_id");
          $type = '';

          $modelBooking = new Model_Booking();
          $booking = $modelBooking->getById($bookingId);

          $modelDiscussionMongo = new Model_DiscussionMongo();
          $modelBookingInvoice = new Model_BookingInvoice();
          $modelBookingEstimate = new Model_BookingEstimate();
          $modelAuthRole = new Model_AuthRole();

          $invoiceId = $estimateId = $inquiryId = 0;
          $isContractor = false;
          $loggedUser = CheckAuth::getLoggedUser();

          $contractorRoleId = $modelAuthRole->getRoleIdByName('contractor');
          if($loggedUser['role_id'] == $contractorRoleId){
          $isContractor = true;
          }

          if ($bookingId) {
          $invoice = $modelBookingInvoice->getByBookingId($bookingId);
          $estimate = $modelBookingEstimate->getByBookingId($bookingId);

          $invoiceId = !empty($invoice['id']) ? $invoice['id'] : 0;
          $estimateId = !empty($estimate['id']) ? $estimate['id'] : 0;
          $inquiryId = !empty($booking['original_inquiry_id']) ? $booking['original_inquiry_id'] : 0;
          }

          $success = $modelDiscussionMongo->markAllAsSeen(0, 0, $type, $loggedUser['username'], $loggedUser['user_id'], $isContractor, array('bookingId' => $bookingId, 'invoiceId' => $invoiceId, 'estimateId' => $estimateId, 'inquiryId' => $inquiryId), $inquiry_id, $complaintId);

          if($success){
          echo 1;
          }
          exit; */
    }

    public function editDiscussionVisibilityAction() {
        $discussion_id = $this->request->getParam('discussion_id');
        $type = $this->request->getParam('type');
        $newVisibility = $this->request->getParam('newVisibility');
        $newVisibility = (int) $newVisibility;

        $modelBookingDiscussion = new Model_BookingDiscussion();
        $modelComplaintDiscussion = new Model_ComplaintDiscussion();
        $modelEstimateDiscussion = new Model_EstimateDiscussion();
        $modelInquiryDiscussion = new Model_InquiryDiscussion();
        $modelInvoiceDiscussion = new Model_InvoiceDiscussion();

        $data = array('visibility' => $newVisibility);

        if ($type == 'booking') {
            $success = $modelBookingDiscussion->updateById($discussion_id, $data);
        } else if ($type == 'estimate') {
            $success = $modelEstimateDiscussion->updateById($discussion_id, $data);
        } else if ($type == 'inquiry') {
            $success = $modelInquiryDiscussion->updateById($discussion_id, $data);
        } else if ($type == 'invoice') {
            $success = $modelInvoiceDiscussion->updateById($discussion_id, $data);
        } else if ($type == 'complaint') {
            $success = $modelComplaintDiscussion->updateById($discussion_id, $data);
        }

        //echo $discussion_id;
        echo $success;
        exit;
    }

}
