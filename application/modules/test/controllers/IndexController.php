<?php

class Test_IndexController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        if (empty($_SERVER['REMOTE_ADDR']) || $_SERVER['REMOTE_ADDR'] != '127.0.0.1') {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You Don't have permission"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
    }

    /**
     * Items list action
     */
    public function indexAction() {

        $getTable = $this->request->getParam('table');

        $data = array(
//            'Booking' => array(
//                'auto' => array(
//                    'booking_attachment',
//                    'booking_label',
//                    'booking_product',
//                    'booking_discussion',
//                    'booking_status_discussion',
//                    'google_calendar_event',
//                    'booking_status_history',
//                    'booking_multiple_days',
//                    'booking_contact_history',
//                    'booking_reminder',
//                    'booking_address',
//                    'booking_address_temp',
//                    'booking_due_date',
//                    'ios_sync',
//                    'contractor_service_booking',
//                    'contractor_service_booking_temp',
//                    'service_attribute_value',
//                    'service_attribute_value_temp'
//                ),
//                'check' => array(
//                    'booking_invoice',
//                    'booking_estimate',
//                    'payment',
//                    'refund',
//                    'booking_contractor_payment',
//                    'contractor_share_booking',
//                    'complaint'
//                )
//            ),
//            'Inquiry' => array(
//                'auto' => array(
//                    'inquiry_label',
//                    'inquiry_reminder',
//                    'inquiry_discussion',
//                    'customer_reminder',
//                    'inquiry_address',
//                    'booking_attachment',
//                    'inquiry_service',
//                    'inquiry_service_attribute_value',
//                    'inquiry_type_attribute_value',
//                )
//            ),
//            'Invoice' => array(
//                'auto' => array(
//                    'invoice_label',
//                    'invoice_discussion'
//                ),
//                'check' => array(
//                    'payment',
//                    'refund',
//                    'booking_contractor_payment',
//                    'contractor_share_booking',
//                )
//            ),
//            'Estimate' => array(
//                'auto' => array(
//                    // 'estimate_reminder',
//                    'estimate_label',
//                    'estimate_discussion'
//                )
//            ),
//            'Complaint' => array(
//                'auto' => array(
//                    'complaint_discussion'
//                )
//            ),
//            'Customer' => array(
//                'auto' => array(
//                    'customer_notes',
//                    'customer_commercial_info',
//                    'customer_contact'
//                ),
//                'check' => array(
//                    'inquiry',
//                    'booking',
//                    'payment',
//                    'refund'
//                )
//            ),
//            'User' => array(
//                'auto' => array(
//                    'user_info',
//                    'ios_user',
//                    'log_user',
//                    'user_company',
//                    'company_invoice',
//                    'email_log',
//                    'report',
//                    'code',
//                    'contractor_service' => 'contractor_id',
//                    'contractor_gmail_accounts' => 'contractor_id',
//                    'postal_address' => 'contractor_id',
//                ),
//                'check' => array(
//                    'inquiry',
//                    'inquiry_discussion',
//                    'customer_reminder',
//                    'booking' => 'created_by',
//                    'booking_attachment' => 'created_by',
//                    'booking_reminder',
//                    'booking_discussion',
//                    'booking_contact_history',
//                    'booking_status_history',
//                    'contractor_share_booking' => 'contractor_id',
//                    'contractor_service_booking_temp' => 'contractor_id',
//                    'contractor_service_booking' => 'contractor_id',
//                    'ios_sync' => 'ios_user_id',
//                    'complaint',
//                    'complaint_discussion',
//                    'payment',
//                    'refund',
//                    'booking_contractor_payment' => 'contractor_id',
//                    //'estimate_reminder',
//                    'estimate_discussion',
//                    'invoice_discussion',
//                    'contractor_info' => 'contractor_id',
//                    'customer' => 'created_by',
//                    'customer_notes' => 'created_by'
//                )
//            ),
//            'ContractorInfo' => array(
//                'auto' => array(
//                    'contractor_owner',
//                    'contractor_employee',
//                    'vehicle'
//                )
//            ),
//            'ContractorService' => array(
//                'auto' => array(
//                    'contractor_service_availability'
//                )
//            ),
//            'Product' => array(
//                'check' => array(
//                    'booking_product'
//                )
//            ),
//            'PaymentType' => array(
//                'check' => array(
//                    'payment',
//                    'refund'
//                )
//            ),
//            'DueDate' => array(
//                'check' => array(
//                    'booking_due_date'
//                )
//            ),
//            'Status' => array(
//                'check' => array(
//                    'booking',
//                    'booking_status_discussion',
//                    'booking_status_history'
//                )
//            ),
//            'InquiryType' => array(
//                'check' => array(
//                    'inquiry',
//                    'inquiry_type_attribute'
//                )
//            ),
//            'PropertyType' => array(
//                'check' => array(
//                    'inquiry',
//                    'booking'
//                )
//            ),
//            'RequiredType' => array(
//                'check' => array(
//                    'inquiry'
//                )
//            ),
//            'ComplaintType' => array(
//                'check' => array(
//                    'complaint'
//                )
//            ),
//            'CustomerType' => array(
//                'check' => array(
//                    'customer'
//                )
//            ),
//            'Label' => array(
//                'check' => array(
//                    'booking_label',
//                    'estimate_label',
//                    'invoice_label',
//                    'inquiry_label'
//                )
//            ),
//            'CustomerContactLabel' => array(
//                'check' => array(
//                    'customer_contact'
//                )
//            ),
//            'Role' => array(
//                'check' => array(
//                    'user',
//                    'auth_role_credential'
//                )
//            ),
//            'Company' => array(
//                'check' => array(
//                    'booking',
//                    'inquiry',
//                    'booking_status',
//                    'inquiry_required_type',
//                    'property_type',
//                    'label',
//                    'canned_responses',
//                    'bank',
//                    'product',
//                    'payment_type',
//                    'service',
//                    'image_attachment',
//                    'attribute',
//                    'user_company',
//                    'page',
//                    'report',
//                    'company_invoice_note',
//                    'complaint_type',
//                    'complaint',
//                    'customer',
//                    'customer_type',
//                    'customer_contact_label',
//                    'inquiry_type'
//                )
//            ),
//            'City' => array(
//                'check' => array(
//                    'booking',
//                    'inquiry',
//                    'contractor_owner',
//                    'contractor_employee',
//                    'company',
//                    'customer',
//                    'user',
//                    'postal_address',
//                    'contractor_service_availability'
//                )
//            ),
//            'Country' => array(
//                'check' => array(
//                    'city'
//                )
//            ),
//            'Attribute' => array(
//                'check' => array(
//                    'inquiry_type_attribute',
//                    'service_attribute',
//                    'attribute_list_value',
//                )
//            ),
//            'Service' => array(
//                'check' => array(
//                    'inquiry_service',
//                    'contractor_service',
//                    'contractor_service_booking',
//                    'contractor_service_booking_temp',
//                    'service_attribute'
//                )
//            ),
//            'ServiceAttribute' => array(
//                'check' => array(
//                    'inquiry_service_attribute_value',
//                    'service_attribute_value',
//                    'service_attribute_value_temp',
//                )
//            ),
//            'InquiryTypeAttribute' => array(
//                'check' => array(
//                    'inquiry_type_attribute_value',
//                )
//            ),
//            'AttributeListValue' => array(
//                'check' => array(
//                    'image_attachment',
//                )
//            )
        );

        if ($getTable) {
            if (isset($data[$getTable])) {
                $getData[$getTable] = $data[$getTable];
                $data = $getData;
            }
        }

        foreach ($data as $name => $value) {

            $idName = strtolower(trim(preg_replace('/(?<!\ )[A-Z]/', '_$0', $name) . '_id', '_'));

            if (isset($value['check']) && $value['check']) {
                echo "<br>----Start Check {$name}----<br>";

                $result = "";
                $result .= "public function checkBeforeDelete{$name}(\$id,&\$tables = array()) {" . "<br>" . "<br>";
                $result .= "\$sucsess = true;" . "<br>" . "<br>";
                foreach ($value['check'] as $key => $table) {

                    $id = $idName;
                    if (!is_int($key)) {
                        $id = $table;
                        $table = $key;
                    }

                    $result .= "\$select_{$table} = \$this->getAdapter()->select();" . "<br>";
                    $result .= "\$select_{$table}->from('{$table}','COUNT(*)');" . "<br>";
                    $result .= "\$select_{$table}->where(\"{$id} = {\$id}\");" . "<br>";
                    $result .= "\$count_{$table} = \$this->getAdapter()->fetchOne(\$select_{$table});" . "<br>" . "<br>";

                    $result .= "if(\$count_{$table}){" . "<br>";
                    $result .= "\$tables[] = '$table';" . "<br>";
                    $result .= "\$sucsess = false;" . "<br>";
                    $result .= "}" . "<br>" . "<br>";
                }
                $result .= "return \$sucsess;" . "<br>";
                $result .= "}" . "<br>";

                echo $result;

                echo "<br>----End Check {$name}----<br>";
            }

            if (isset($value['auto']) && $value['auto']) {
                echo "<br>----Start Auto {$name}----<br>";

                $result2 = "";
                $result2 .= "public function deleteRelated{$name}(\$id) {" . "<br>" . "<br>";
                foreach ($value['auto'] as $key => $table) {

                    $id = $idName;
                    if (!is_int($key)) {
                        $id = $table;
                        $table = $key;
                    }

                    $result2 .= "//delete data from {$table}" . "<br>";
                    $result2 .= "\$this->getAdapter()->delete('{$table}', \"{$id} = '{\$id}'\");" . "<br>" . "<br>";
                }
                $result2 .= "}" . "<br>";
                echo $result2;

                echo "<br>----End Auto {$name}----<br>";
            }

            echo '<br>---------------------------------------<br>';
        }
        die;
    }

}