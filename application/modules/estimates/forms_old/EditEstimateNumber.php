<?php

class Estimates_Form_EditEstimateNumber extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $estimate_id = isset($options['estimate_id']) ? $options['estimate_id'] : '';
        $estimateNumber = isset($options['estimate_number']) ? $options['estimate_number'] : '';

        $this->setName('EditEstimateNumber');

        $router = Zend_Controller_Front::getInstance()->getRouter();

        $estimate_number = new Zend_Form_Element_Text('estimate_number');
        $estimate_number->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue($estimateNumber)
                ->setErrorMessages(array('Required' => 'Please Enter Estimate Number.'));


        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'button'));


        $this->addElements(array($estimate_number, $button));

        $this->setMethod('post');

        $this->setAction($router->assemble(array('id' => $estimate_id), 'editEstimateNumber'));
    }

}