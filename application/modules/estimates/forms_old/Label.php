<?php

class Estimates_Form_Label extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('LabelType');
        $mode = (isset($options['mode']) ? $options['mode'] : '');
        $labelName = (isset($options['label_name']) ? $options['label_name'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();
        $request = Zend_Controller_Front::getInstance()->getRequest();

        $labelNameVal = (!empty($labelName['label_name']) ? $labelName['label_name'] : '');
        $label_name = new Zend_Form_Element_Text('label_name');
        $label_name->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'text_field'))
                ->setValue($labelNameVal);
        if ($labelNameVal != $request->getParam('label_name')) {
            $label_name->addValidator(new Zend_Validate_Db_NoRecordExists('label', 'label_name'));
        }

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'button'));

        $this->addElements(array($label_name, $button));
        $this->setMethod('post');

        $this->setAction($router->assemble(array('id'=> $request->getParam('id')), 'estimateAddLabel'));
    }

}

