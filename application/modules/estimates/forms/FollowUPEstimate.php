<?php

class Estimates_Form_FollowUPEstimate extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setName('FollowUPEstimate');

        $estimate = (isset($options['estimate']) ? $options['estimate'] : '');

        $router = Zend_Controller_Front::getInstance()->getRouter();

        $modelBooking = new Model_Booking();
        $booking = $modelBooking->getById($estimate['booking_id']);
		$dateFormated = '';
		if(!empty($booking['to_follow']))
			$dateFormated = getNewDateFormat($booking['to_follow'], 'all');
		//echo "at form date formated: " . $dateFormated; 
		$dateTimeObj= get_settings_date_format();
		if($dateTimeObj){
			$timeZoneString  =  get_timeZone($dateTimeObj['time_format']);
		}else{
			$timeZoneString = '';
		}
		
        $followDateVal = !empty($booking['to_follow']) ? $dateFormated ? $dateFormated :php2JsTime($booking['to_follow']) : '';
        //$followDateVal = (!empty($booking['to_follow']) ? php2JsTime($booking['to_follow']) : '');

        $followDate = new Zend_Form_Element_Text('followDate');
        $followDate->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'date text_input form-control'))
                ->setValue($followDateVal);


        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save');
        $button->setAttribs(array('class' => 'button btn btn-primary'));
        $button->setAttribs(array('style' => 'margin-top: 10px;'));

        $this->addElements(array($followDate, $button));
        $this->setMethod('post');
        $this->setAction($router->assemble(array('id' => $estimate['id']), 'followUpEstimate'));
    }

}

