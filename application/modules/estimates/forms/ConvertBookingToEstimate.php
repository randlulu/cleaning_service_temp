<?php

class Estimates_Form_ConvertBookingToEstimate extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);

        $bookingId = isset($options['booking_id']) ? $options['booking_id'] : '';

        $this->setName('ConvertBookingToEstimate');

        $router = Zend_Controller_Front::getInstance()->getRouter();

        $to_follow = new Zend_Form_Element_Text('to_follow');
        $to_follow->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setRequired()
                ->setAttribs(array('class' => 'form-control date_time', 'style' => 'width: 220px;', 'readonly' => 'readonly'))
                ->setErrorMessages(array('Required' => 'Please Enter To Follow Date.'));

        $isToFollow = new Zend_Form_Element_Checkbox('is_to_follow');
        $isToFollow->setDecorators(array('ViewHelper'))
                ->addDecorator('Errors', array('class' => 'errors'))
                ->setAttribs(array('class' => 'checkbox_field','checked'=>'checked'));
                

        $button = new Zend_Form_Element_Submit('button');
        $button->setDecorators(array('ViewHelper'));
        $button->setLabel('Save And Continue');
        $button->setAttribs(array('class' => 'btn btn-primary'));


        $this->addElements(array($to_follow, $isToFollow, $button));

        $this->setMethod('post');

        $this->setAction($router->assemble(array('id' => $bookingId), 'convertBookingToEstimate'));
    }

}