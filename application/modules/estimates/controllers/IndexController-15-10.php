<?php

class Estimates_IndexController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        $this->view->main_menu = 'estimates';
        $this->view->sub_menu = 'estimates';
    }

    /**
     * Items list action
     */
    public function indexAction() {

        // check Auth for logged user
        CheckAuth::checkPermission(array('estimates'));

        // get request parameters
        $orderBy = $this->request->getParam('sort', 'id');
        $sortingMethod = $this->request->getParam('method', 'desc');
        $currentPage = $this->request->getParam('page', 1);
        $filters = $this->request->getParam('fltr', array('estimate_type' => 'draft'));

        if ($filters) {
            foreach ($filters as &$filter) {
                if (!is_array($filter)) {
                    $filter = trim($filter);
                }
            }
        }

        $loggedUser = CheckAuth::getLoggedUser();
        $my_estimates = $this->request->getParam('my_estimates', false);

        if ($my_estimates) {
            $this->view->sub_menu = 'my_estimates';
            $filters['my_estimates'] = $loggedUser['user_id'];
        }
		//////////////By Islam
		//
        //get all visited Estimates
        //
		$visited_estimates = $this->request->getParam('visited_estimates', false);

        if ($visited_estimates) {
            $this->view->sub_menu = 'visited_estimates';
            $filters['visited'] = 1;
        }
		/////////End
		
        // to get all estimate that belong to this user selected from user search
        if (isset($filters['user_id']) && !empty($filters['user_id'])) {
            $filters['my_estimates'] = $filters['user_id'];
        }

        // Load Model
        $modelBookingEstimate = new Model_BookingEstimate();

        //
        //get all deleted Estimates
        //
        $isDeleted = $this->request->getParam('is_deleted', 0);
        $this->view->isDeleted = $isDeleted;

        if ($isDeleted) {
            if (CheckAuth::checkCredential(array('canSeeDeletedEstimate'))) {
                $this->view->sub_menu = 'deleted_estimate';
                $filters['is_deleted'] = $isDeleted;
            } else {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have permission to view Deleted Estimate."));
                $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
            }
        }

        //
        // init pager and articles model object
        //
        $pager = new Model_Pager();
        $pager->perPage = get_config('perPage');
        $pager->currentPage = $currentPage;
        $pager->url = $_SERVER['REQUEST_URI'];


        //
        // get data list
        //
        $data = $modelBookingEstimate->getAll($filters, "{$orderBy} {$sortingMethod}", $pager);
        $modelBookingEstimate->fills($data, array('booking', 'contractors', 'customer', 'city', 'labels', 'booking_users', 'have_attachment', 'services', 'services_temp', 'isAccepted', 'booking_contact_history', 'not_accepted_or_rejected', 'not_accepted'));

        //
        // set view params
        //
        $this->view->data = $data;
        $this->view->currentPage = $currentPage;
        $this->view->perPage = $pager->perPage;
        $this->view->pageLinks = $pager->getPager();
        $this->view->sortingMethod = $sortingMethod;
        $this->view->orderBy = $orderBy;
        $this->view->filters = $filters;
    }
	
    public function deleteAction() {

        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('estimateDelete'));

        //
        // get request parameters
        //
        $estimateId = $this->request->getParam('id', 0);
        $estimateIds = $this->request->getParam('ids', array());
        if ($estimateId) {
            $estimateIds[] = $estimateId;
        }

        $modelBooking = new Model_Booking();
        $modelBookingEstimate = new Model_BookingEstimate();

        $success_array = array();
        foreach ($estimateIds as $estimateId) {
            $success_array[$estimateId] = 0;
            if (CheckAuth::checkIfCanHandelAllCompany('estimate', $estimateId)) {
                $bookingEstimate = $modelBookingEstimate->getById($estimateId);
                if ($bookingEstimate && $modelBooking->checkIfCanDeleteBooking($bookingEstimate['booking_id'])) {
                    $success_array[$estimateId] = $modelBookingEstimate->deleteEstimate($bookingEstimate['booking_id']);
                }
            }
        }
        if (!in_array(0, $success_array)) {
            if (count($success_array) > 1) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "The Selected Estimates have been deleted"));
            } else {
                $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "This Estimate has been deleted"));
            }
        } else {
            if (count($success_array) > 1) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "The Selected  Estimates could not be deleted."));
            } else {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Estimate could not be deleted."));
            }
        }

        $this->_redirect($this->router->assemble(array(), 'estimates'));
    }

    public function undeleteAction() {

        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('estimateUndelete'));

        //
        // get request parameters
        //
        $estimateId = $this->request->getParam('id', 0);

        $modelBooking = new Model_Booking();
        $modelBookingEstimate = new Model_BookingEstimate();

        $bookingEstimate = $modelBookingEstimate->getById($estimateId);

        if (!$bookingEstimate) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Esitmate not exist"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $success = false;
        if (CheckAuth::checkIfCanHandelAllCompany('estimate', $estimateId)) {
            if ($modelBooking->checkIfCanDeleteBooking($bookingEstimate['booking_id'])) {
                $success = $modelBookingEstimate->unDeleteEstimate($bookingEstimate['booking_id']);
            }
        }

        if ($success) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "The Selected Estimates have been restored"));
        } else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "The Selected Estimates could not be restored."));
        }

        $this->_redirect($this->router->assemble(array(), 'estimates'));
    }

    public function deleteForEverAction() {

        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('estimateDeleteForEver'));

        //
        // get request parameters
        //
        $estimateId = $this->request->getParam('id', 0);

        $modelBooking = new Model_Booking();
        $modelBookingEstimate = new Model_BookingEstimate();

        $bookingEstimate = $modelBookingEstimate->getById($estimateId);

        if (!$bookingEstimate) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Esitmate not exist"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $success = false;
        if (CheckAuth::checkIfCanHandelAllCompany('estimate', $estimateId)) {
            if ($modelBooking->checkIfCanDeleteBooking($bookingEstimate['booking_id'])) {
                $success = $modelBookingEstimate->deleteEstimateForEver($bookingEstimate['booking_id']);
            }
        }

        if ($success) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "The Selected Estimates have been deleted For Ever"));
        } else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "The Selected Estimates could not be deleted For Ever."));
        }

        $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : $this->router->assemble(array(), 'estimates')));
    }

	/*
    public function viewAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('estimateView'));

        //
        // get params 
        //
        $estimateId = $this->request->getParam('id', 0);

        if (!CheckAuth::checkIfCanHandelAllCompany('estimate', $estimateId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Estimate not belongs to your Company"));
            $this->_redirect($this->router->assemble(array(), 'estimates'));
        }

        //
        // load models
        //
        $modelBookingEstimate = new Model_BookingEstimate();

        //
        // geting data
        //
        $estimate = $modelBookingEstimate->getById($estimateId);

        //
        // validation
        //
        if (!$estimate) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Esitmate not exist"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $modelBookingEstimate->fill($estimate, array('booking', 'labels', 'customer', 'isAccepted', 'multiple_days', 'booking_address'));
        $this->view->estimateParams = $estimate;

        //
        // get Estimate View Param
        //
        $this->getEstimateViewParam($estimateId);
		
		////get all questions
		$modelBooking = new Model_Booking();
		$modelUpdateBookingQuestionAnswer = new Model_UpdateBookingQuestionAnswer();
		$booking = $modelBooking->getById($estimate['booking_id']);
		$status_id = $booking['status_id'];
		$questions = $modelUpdateBookingQuestionAnswer->getByBookingIdAndStatusId($status_id,$estimate['booking_id']);
		$this->view->questions = $questions ;
		
		$canEditDetails = $modelBooking->checkCanEditBookingDetails($estimate['booking_id']);
        $this->view->canEditDetails = $canEditDetails;
		
		/////get Contractors Distances	
		$modelContractorServiceBooking = new Model_ContractorServiceBooking();
		$modelBookingAddress = new Model_BookingAddress();
		$bookingAddress = $modelBookingAddress->getByBookingId($booking['booking_id']);
        $contractorServiceBookings = $modelContractorServiceBooking->getByBookingId($booking['booking_id']);

		$removeRedundancy = array();
        foreach ($contractorServiceBookings as &$bookingService) {
			///set service ids in array to get distance between them and contractors
			 $removeRedundancy[$bookingService['service_id']] = $bookingService['service_id'];
        }
		
		$modelContractorService = new Model_ContractorService();
		$modelUser = new Model_User();
		$cityId = $booking['city_id'];
		
        $service_ids = $removeRedundancy;
		
		/// get get contractor ids by city_id and service_id
		if (!empty($service_ids)) {
            foreach ($service_ids as $service_id) {
					if ($cityId) {
						$ContractorServices = $modelContractorService->getContractorByServiceIdAndCityId($service_id, $cityId);
					} else {
						$ContractorServices = $modelContractorService->getContractorByServiceId($service_id);
					}
				foreach ($ContractorServices as $ContractorService) {
					 $contractor_ids[$ContractorService['contractor_id']] = $ContractorService['contractor_id'];
				}
            }
        }
		
		/// here calculate the distance between each contractor and inquiry address
		$contractorDistances = array();
        foreach ($contractor_ids as $contractorId) {
			//// delete inquiries from contractor list
			if($contractorId != 1){
				$contractor = $modelUser->getById($contractorId);

				$contractorDistances[$contractor['user_id']] = array(
					'contractor_id' => $contractor['user_id'],
					'name' => $contractor['username'],
					'email1' => $contractor['email1'],
					'distance' => $modelBookingAddress->getDistanceByTwoAddress($contractor, $bookingAddress)
				);
			}
        }        
		
		$modelImageAttachment = new Model_Image();
	   $pager = null ;
       $this->view->photo = $modelImageAttachment->getAll($estimateId , 'estimate', 'iu.created desc' , $pager, $filter = array() , $limit = 10);
       $this->view->type = 'estimate';
       $this->view->photoCount = count($modelImageAttachment->getAll($estimateId , 'estimate', 'iu.created desc'));
	   
	   
		$this->view->contractorDistances = $contractorDistances;		
		
    }
	*/

	public function viewAction() {
		
        // check Auth for logged user
        CheckAuth::checkPermission(array('estimateView'));

        // get params 
        $estimateId = $this->request->getParam('id', 0);
		$modelBooking = new Model_Booking();
        if (!CheckAuth::checkIfCanHandelAllCompany('estimate', $estimateId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Estimate not belongs to your Company"));
            $this->_redirect($this->router->assemble(array(), 'estimates'));
        }

		//D.A 17/09/2015 Caching estimate view blocks
		require_once 'Zend/Cache.php';	
		$company_id = CheckAuth::getCompanySession();					
		$estimateViewDir=get_config('cache').'/'.'estimatesView'.'/'.$company_id;
		if (!is_dir($estimateViewDir)) {
		mkdir($estimateViewDir, 0777, true);
		}						
		$frontEndOption= array('lifetime'=> NULL,
		'automatic_serialization'=> true);
		$backendOptions = array('cache_dir'=>$estimateViewDir );
		$cache = Zend_Cache::factory('Core','File',$frontEndOption,$backendOptions);
		
        //D.A 17/09/2015 Estimate Parameters Cache 
        $cacheID= $estimateId.'_estimateParams';	
		if (($result = $cache->load($cacheID)) === false ) {	
        $modelBookingEstimate = new Model_BookingEstimate();
        $estimate = $modelBookingEstimate->getById($estimateId);
		
        if (!$estimate) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Esitmate not exist"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $modelBookingEstimate->fill($estimate, array('booking', 'labels', 'customer', 'isAccepted', 'multiple_days', 'booking_address'));		
		$result=$estimate;
		if($result){
			$cache->save($result,$cacheID);	
			}					
		}				
		else{		
			$estimate=$result;		
			$isAccepted = $modelBooking->checkBookingIfAccepted($estimate['booking_id']);
            if($estimate['isAccepted'] != $isAccepted){				
			$estimate['isAccepted']=$isAccepted;				
			}			
		}
		$this->view->estimateParams = $estimate;

        // get Estimate View Param
        $this->getEstimateViewParam($estimateId);
		
		$booking = $modelBooking->getById($estimate['booking_id']);		
		//D.A 17/09/2015 Estimate Questions Cache 
		//get all questions
        $cacheID= $estimateId.'_estimateQuestions';	
		if (($result = $cache->load($cacheID)) === false ) {		
			$modelUpdateBookingQuestionAnswer = new Model_UpdateBookingQuestionAnswer();			
			$status_id = $booking['status_id'];
			$questions = $modelUpdateBookingQuestionAnswer->getByBookingIdAndStatusId($status_id,$estimate['booking_id']);
			$result=$questions;
			if($result){
				$cache->save($result,$cacheID);	
			}					
		}				
		else{			
			$questions=$result;		
		}		
		$this->view->questions = $questions ;
		
		$canEditDetails = $modelBooking->checkCanEditBookingDetails($estimate['booking_id']);
        $this->view->canEditDetails = $canEditDetails;
		
		//get Contractors Distances	
		//D.A 17/09/2015 Estimate Available Technicians Cache 
        $cacheID= $estimateId.'_estimateAvailableTechnicians';	
		if (($result = $cache->load($cacheID)) === false ) {
			$modelContractorServiceBooking = new Model_ContractorServiceBooking();
			$modelBookingAddress = new Model_BookingAddress();
			$bookingAddress = $modelBookingAddress->getByBookingId($booking['booking_id']);
			$contractorServiceBookings = $modelContractorServiceBooking->getByBookingId($booking['booking_id']);

			$removeRedundancy = array();
			foreach ($contractorServiceBookings as &$bookingService) {
				///set service ids in array to get distance between them and contractors
				 $removeRedundancy[$bookingService['service_id']] = $bookingService['service_id'];
			}
			
			$modelContractorService = new Model_ContractorService();
			$modelUser = new Model_User();
			$cityId = $booking['city_id'];
			
			$service_ids = $removeRedundancy;
			
			/// get get contractor ids by city_id and service_id
			if (!empty($service_ids)) {
				foreach ($service_ids as $service_id) {
						if ($cityId) {
							$ContractorServices = $modelContractorService->getContractorByServiceIdAndCityId($service_id, $cityId);
						} else {
							$ContractorServices = $modelContractorService->getContractorByServiceId($service_id);
						}
					foreach ($ContractorServices as $ContractorService) {
						 $contractor_ids[$ContractorService['contractor_id']] = $ContractorService['contractor_id'];
					}
				}
			
				
				/// here calculate the distance between each contractor and inquiry address
				$contractorDistances = array();
				foreach ($contractor_ids as $contractorId) {
					//// delete inquiries from contractor list
					if($contractorId != 1){
						$contractor = $modelUser->getById($contractorId);

						$contractorDistances[$contractor['user_id']] = array(
							'contractor_id' => $contractor['user_id'],
							'name' => $contractor['username'],
							'email1' => $contractor['email1'],
							'distance' => $modelBookingAddress->getDistanceByTwoAddress($contractor, $bookingAddress)
						);
					}
				}        
				
				$result=$contractorDistances;
			}
			if($result){
				$cache->save($result,$cacheID);	
			}					
		}				
		else{			
			$contractorDistances=$result;		
		}
		
		if (!empty($contractorDistances)) {
		$this->view->contractorDistances = $contractorDistances;
	    }
		
	   //D.A 17/09/2015 Estimate Photo Cache 
       $cacheID= $estimateId.'_estimatePhoto';	
	   if (($result = $cache->load($cacheID)) === false ) {
		   $modelImageAttachment = new Model_Image();
		   $pager = null ;
		   $photo=$modelImageAttachment->getAll($estimateId , 'estimate', 'iu.created desc' , $pager, $filter = array() , $limit = 10);
		   $photoCount = count($modelImageAttachment->getAll($estimateId , 'estimate', 'iu.created desc'));
		   $type = 'estimate';
		   $result = array(
						'photo' =>$photo,
						'photoCount' =>$photoCount,
						'type' =>$type,
						);
		   if($result){
				$cache->save($result,$cacheID);	
			}
		}				
		else{			
			$photo=$result['photo'];	
			$photoCount=$result['photoCount'];	
			$type=$result['type'];		
		}	  
	    $this->view->photo =$photo;
        $this->view->photoCount = $photoCount;
	    $this->view->type =  $type;
	   		
    }
	
    public function previewAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('estimateView'));

        //
        // get params 
        //
        $estimateId = $this->request->getParam('id', 0);

        if (!CheckAuth::checkIfCanHandelAllCompany('estimate', $estimateId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Estimate not belongs to your Company"));
            $this->_redirect($this->router->assemble(array(), 'estimates'));
        }
        //
        // load models
        //
        $modelBookingEstimate = new Model_BookingEstimate();

        //
        // geting data
        //
        $estimate = $modelBookingEstimate->getById($estimateId);

        //
        // validation
        //
        if (!$estimate) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Estimate not exist"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        if ($estimate['estimate_type'] == 'booking') {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Estimate has been converted to booking"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // get estimate View Param
        //
        $this->getEstimateViewParam($estimateId);

        echo $this->view->render('index/preview.phtml');
        exit;
    }

	/*
    public function convertBookingToEstimateAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('convertBookingToEstimate'));

        //
        // get request parameters
        //
        $bookingId = $this->request->getParam('id', 0);
        $toFollow = $this->request->getParam('to_follow', 0);
        $isToFollow = $this->request->getParam('is_to_follow', 0);

        
        //load model
         
        $modelBookingEstimate = new Model_BookingEstimate();
        $modelBooking = new Model_Booking();

        //
        // validation
        //
        
        if (!$modelBooking->checkBookingIfAccepted($bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Booking not accepted"));
            $this->_redirect($this->router->assemble(array(), 'booking'));
        }
        if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Booking not belongs to your Company"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $form = new Estimates_Form_ConvertBookingToEstimate(array('booking_id' => $bookingId));

        if ($this->request->isPost()) {
            if ($form->isValid($this->request->getPost())) { // validate form data
                $modelBookingEstimate->convertToEstimate($bookingId, $toFollow, true, $isToFollow);
                echo 1;
                exit;
            }
        }
        $this->view->form = $form;
		//D.A 27/08/2015 Remove Booking Scheduled Visits Cache
		$bookingDetailsCacheID= $bookingId.'_bookingDetails';
		$company_id = CheckAuth::getCompanySession();
		$bookingViewDir=get_config('cache').'/'.'bookingsView'.'/'.$company_id;
		if (!is_dir($bookingViewDir)) {
			mkdir($bookingViewDir, 0777, true);
		}								
		$frontEndOption= array('lifetime'=> 24 * 3600,
		'automatic_serialization'=> true);
		$backendOptions = array('cache_dir'=>$bookingViewDir );
		$cache = Zend_Cache::factory('Core','File',$frontEndOption,$backendOptions);
		$cache->remove($bookingDetailsCacheID);	
		

        //
        // render views
        //
        echo $this->view->render('index/convert-booking-to-estimate.phtml');
        exit;
    }
	*/
	
	 public function convertBookingToEstimateAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('convertBookingToEstimate'));

        //
        // get request parameters
        //
        $bookingId = $this->request->getParam('id', 0);
        $toFollow = $this->request->getParam('to_follow', 0);
        $isToFollow = $this->request->getParam('is_to_follow', 0);

        /*
         * load model
         */
        $modelBookingEstimate = new Model_BookingEstimate();
        $modelBooking = new Model_Booking();

        //
        // validation
        //
        
        if (!$modelBooking->checkBookingIfAccepted($bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Booking not accepted"));
            $this->_redirect($this->router->assemble(array(), 'booking'));
        }
        if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Booking not belongs to your Company"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $form = new Estimates_Form_ConvertBookingToEstimate(array('booking_id' => $bookingId));

        if ($this->request->isPost()) {
            if ($form->isValid($this->request->getPost())) { // validate form data
                $modelBookingEstimate->convertToEstimate($bookingId, $toFollow, true, $isToFollow);
                echo 1;
                exit;
            }
        }
        $this->view->form = $form;

		//D.A 27/08/2015 Remove Booking Scheduled Visits Cache
		$bookingDetailsCacheID= $bookingId.'_bookingDetails';
		$company_id = CheckAuth::getCompanySession();
		$bookingViewDir=get_config('cache').'/'.'bookingsView'.'/'.$company_id;
		if (!is_dir($bookingViewDir)) {
			mkdir($bookingViewDir, 0777, true);
		}								
		$frontEndOption= array('lifetime'=> NULL,
		'automatic_serialization'=> true);
		$backendOptions = array('cache_dir'=>$bookingViewDir );
		$cache = Zend_Cache::factory('Core','File',$frontEndOption,$backendOptions);
		$cache->remove($bookingDetailsCacheID);	
		
		//D.A 10/09/2015 Remove inquiry Details Cache
		$modelBooking = new Model_Booking();
        $booking = $modelBooking->getById($bookingId);
		$inquiryDetailsCacheID= $booking['original_inquiry_id'].'_inquiryDetails';							
		$inquiryViewDir=get_config('cache').'/'.'inquiriesView'.'/'.$company_id;
		if (!is_dir($inquiryViewDir)) {
		mkdir($inquiryViewDir, 0777, true);
		}						
		$inquiryDetailsFrontEndOption= array('lifetime'=> NULL,
		'automatic_serialization'=> true);
		$inquiryDetailsBackendOptions = array('cache_dir'=>$inquiryViewDir );
		$inquiryDetailsCache = Zend_Cache::factory('Core','File',$inquiryDetailsFrontEndOption,$inquiryDetailsBackendOptions);
		$inquiryDetailsCache->remove($inquiryDetailsCacheID);
			
        //
        // render views
        //
        echo $this->view->render('index/convert-booking-to-estimate.phtml');
        exit;
    }

    public function sendEstimateAsEmailAction() {


        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('sendEstimateAsEmail'));

        $estimateId = $this->request->getParam('id', 0);


        if (!CheckAuth::checkIfCanHandelAllCompany('estimate', $estimateId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Estimate not belongs to your Company"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        //
        // load models
        //
        $modelUser = new Model_User();
        $modelBooking = new Model_Booking();
        $modelCustomer = new Model_Customer();
        $modelBookingAddress = new Model_BookingAddress();
        $modelBookingEstimate = new Model_BookingEstimate();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();

        //
        // geting data
        //
        $estimate = $modelBookingEstimate->getById($estimateId);
        $modelBookingEstimate->fill($estimate, array('booking'));

        //
        // validation
        //
        if (!$estimate) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Estimate not exist"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        if ($estimate['estimate_type'] == 'booking') {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Estimate has been converted to booking"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        if (!$modelBooking->checkBookingIfAccepted($estimate['booking_id'])) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Booking not accepted"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }


        if ($estimate) {

            //
            // filling extra data
            //
            $customer = $modelCustomer->getById($estimate['booking']['customer_id']);
            $user = $modelUser->getById($estimate['booking']['created_by']);

            $viewParam = $this->getEstimateViewParam($estimateId, true);
            $view = new Zend_View();
            $view->setScriptPath(APPLICATION_PATH . '/modules/estimates/views/scripts/index');
            $view->bookingServices = $viewParam['bookingServices'];
            $view->thisBookingServices = $viewParam['thisBookingServices'];
            $view->priceArray = $viewParam['priceArray'];
            $view->estimate = $viewParam['estimate'];
            $bodyEstimate = $view->render('estimate.phtml');

            $template_params = array(
                //estimate
                '{estimate_num}' => $estimate['estimate_num'],
                '{estimate_created}' => date('d/m/Y', $estimate['created']),
                //booking
                '{booking_num}' => $estimate['booking']['booking_num'],
                '{total_without_tax}' => number_format($estimate['booking']['sub_total'], 2),
                '{gst_tax}' => number_format($estimate['booking']['gst'], 2),
                '{total_with_tax}' => number_format($estimate['booking']['qoute'], 2),
                '{description}' => $estimate['booking']['description'] ? $estimate['booking']['description'] : '',
                '{booking_created}' => date('d/m/Y', $estimate['booking']['created']),
                '{booking_created_by}' => ucwords($user['username']),
                '{booking_start}' => date("F j, Y, g:i a", strtotime($estimate['booking']['booking_start'])),
                '{booking_address}' => get_line_address($modelBookingAddress->getByBookingId($estimate['booking']['booking_id'])),
                '{service}' => nl2br($modelContractorServiceBooking->getBookingAsText($estimate['booking']['booking_id'], true)),
                '{estimate_view}' => $bodyEstimate,
                //customer
                '{customer_name}' => get_customer_name($customer),
                '{customer_first_name}' => isset($customer['first_name']) && $customer['first_name'] ? ucwords($customer['first_name']) : '',
                '{customer_last_name}' => isset($customer['last_name']) && $customer['last_name'] ? ' ' . ucwords($customer['last_name']) : '',
                '{customer_contacts}' => nl2br($modelCustomer->getCustomerContacts($estimate['booking']['customer_id'])),
            );


            $modelEmailTemplate = new Model_EmailTemplate();
            $emailTemplate = $modelEmailTemplate->getEmailTemplate('send_estimate_as_email', $template_params);

            $body = $emailTemplate['body'];
            $subject = $emailTemplate['subject'];
            $to = array();
            if ($customer['email1']) {
                $to[] = $customer['email1'];
            }
            if ($customer['email2']) {
                $to[] = $customer['email2'];
            }
            if ($customer['email2']) {
                $to[] = $customer['email3'];
            }
            $to = implode(',', $to);

            if ($this->request->isPost()) {
                $to = $this->request->getParam('to');
                $cc = $this->request->getParam('cc');
                $subject = $this->request->getParam('subject');
                $body = $this->request->getParam('body');
                $pdf_attachment = $this->request->getParam('pdf_attachment', 0);
				
				
		$trading_namesObj = new Model_TradingName();
        $trading_names = $trading_namesObj->getById($estimate['booking']['trading_name_id']);
		
                $params = array(
                    'to' => $to,
                    'cc' => $cc,
                    'body' => $body,
                    'subject' => $subject,
					'trading_name' => $trading_names['trading_name'],
					'from' => $trading_names['email'],	
                );

                $error_mesages = array();
                if (EmailNotification::validation($params, $error_mesages)) {
				try{
                    if (!empty($pdf_attachment)) {
                        // Create pdf
                        $pdfPath = createPdfPath();
                        $destination = $pdfPath['fullDir'] . $viewParam['estimate']['estimate_num'] . '.pdf';
                        wkhtmltopdf($bodyEstimate, $destination);
                        $params['attachment'] = $destination;
                    }

                    // Send Email
                    $success = EmailNotification::sendEmail($params, '', array(), array('reference_id' => $estimate['id'], 'type' => 'estimate'));
				}
				
				catch (Exception $e) {
					echo 'Caught exception: ',  $e->getMessage(), "\n";
				}
                    if ($success) {
                        MobileNotification::notify($estimate['booking']['booking_id'] , 'estimate email to client');
                        $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Email sent successfully"));
                    } else {
                        $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Email not sent"));
                    }
                    echo 1;
                    exit;
                }
                $this->view->error_mesages = $error_mesages;
            }

            $this->view->estimate = $estimate;
            $this->view->to = $to;
            $this->view->subject = $subject;
            $this->view->body = $body;
            $this->view->cc = isset($cc) ? $cc : '';

            echo $this->view->render('index/send-estimate-as-email.phtml');
            exit;
        } else {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Estimate not exist"));
            $this->_redirect($this->router->assemble(array(), 'Login'));
        }
    }

    public function sendReminderEstimateAsEmailAction() {


        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('sendEstimateAsEmail'));

        $estimateId = $this->request->getParam('id', 0);


        if (!CheckAuth::checkIfCanHandelAllCompany('estimate', $estimateId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Estimate not belongs to your Company"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        //
        // load models
        //
        $modelUser = new Model_User();
        $modelBooking = new Model_Booking();
        $modelCustomer = new Model_Customer();
        $modelBookingAddress = new Model_BookingAddress();
        $modelBookingEstimate = new Model_BookingEstimate();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelBookingStatus = new Model_BookingStatus();

        //
        // geting data
        //
        $estimate = $modelBookingEstimate->getById($estimateId);
        $modelBookingEstimate->fill($estimate, array('booking'));

        $quoted = $modelBookingStatus->getByStatusName('QUOTED');

        //
        // validation
        //
        if (!$estimate) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Estimate not exist"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        if ($estimate['estimate_type'] == 'booking') {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Estimate has been converted to booking"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        if (!$modelBooking->checkBookingIfAccepted($estimate['booking_id'])) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Esitmate not accepted"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }


        if (!$this->request->isPost()) {
            //
            // filling extra data
            //
            $cancel_hashcode = sha1(uniqid());
            $modelBooking->updateById($estimate['booking_id'], array('cancel_hashcode' => $cancel_hashcode));

            $cancel_link = $this->router->assemble(array('cancel_hashcode' => $cancel_hashcode, 'booking_id' => $estimate['booking_id'], 'status_id' => $quoted['booking_status_id']), 'cancelBooking');

            $customer = $modelCustomer->getById($estimate['booking']['customer_id']);
            $user = $modelUser->getById($estimate['booking']['created_by']);

            $viewParam = $this->getEstimateViewParam($estimateId, true);
            $view = new Zend_View();
            $view->setScriptPath(APPLICATION_PATH . '/modules/estimates/views/scripts/index');
            $view->bookingServices = $viewParam['bookingServices'];
            $view->thisBookingServices = $viewParam['thisBookingServices'];
            $view->priceArray = $viewParam['priceArray'];
            $view->estimate = $viewParam['estimate'];
            $bodyEstimate = $view->render('estimate.phtml');
            $template_params = array(
                //link
                '{cancel_link}' => '<a href="' . $cancel_link . '">' . $cancel_link . '</a>',
                //estimate
                '{estimate_num}' => $estimate['estimate_num'],
                '{estimate_created}' => date('d/m/Y', $estimate['created']),
                //booking
                '{booking_num}' => $estimate['booking']['booking_num'],
                '{total_without_tax}' => number_format($estimate['booking']['sub_total'], 2),
                '{gst_tax}' => number_format($estimate['booking']['gst'], 2),
                '{total_with_tax}' => number_format($estimate['booking']['qoute'], 2),
                '{description}' => $estimate['booking']['description'] ? $estimate['booking']['description'] : '',
                '{booking_created}' => date('d/m/Y', $estimate['booking']['created']),
                '{booking_created_by}' => ucwords($user['username']),
                '{booking_start}' => date("F j, Y, g:i a", strtotime($estimate['booking']['booking_start'])),
                '{booking_address}' => get_line_address($modelBookingAddress->getByBookingId($estimate['booking']['booking_id'])),
                '{service}' => nl2br($modelContractorServiceBooking->getBookingAsText($estimate['booking']['booking_id'], true)),
                '{estimate_view}' => $bodyEstimate,
                //customer
                '{customer_name}' => get_customer_name($customer),
                '{customer_first_name}' => isset($customer['first_name']) && $customer['first_name'] ? ucwords($customer['first_name']) : '',
                '{customer_last_name}' => isset($customer['last_name']) && $customer['last_name'] ? ' ' . ucwords($customer['last_name']) : '',
                '{customer_contacts}' => nl2br($modelCustomer->getCustomerContacts($estimate['booking']['customer_id'])),
            );


            $modelEmailTemplate = new Model_EmailTemplate();
            $emailTemplate = $modelEmailTemplate->getEmailTemplate('reminder_estimate', $template_params);

            $body = $emailTemplate['body'];
            $subject = $emailTemplate['subject'];
            $to = array();
            if ($customer['email1']) {
                $to[] = $customer['email1'];
            }
            if ($customer['email2']) {
                $to[] = $customer['email2'];
            }
            if ($customer['email2']) {
                $to[] = $customer['email3'];
            }
            $to = implode(',', $to);
        } else {
            $to = $this->request->getParam('to');
            $cc = $this->request->getParam('cc');
            $subject = $this->request->getParam('subject');
            $body = $this->request->getParam('body');
            $pdf_attachment = $this->request->getParam('pdf_attachment', 0);
			
			
			$trading_namesObj = new Model_TradingName();
            $trading_names = $trading_namesObj->getById($estimate['booking']['trading_name_id']);

		
			
            $params = array(
                'to' => $to,
                'cc' => $cc,
                'body' => $body,
                'subject' => $subject,
				'trading_name' => $trading_names['trading_name'],
				'from' => $trading_names['email'],	
            );

            $error_mesages = array();
            if (EmailNotification::validation($params, $error_mesages)) {

                if (!empty($pdf_attachment)) {
                    // Create pdf
                    $pdfPath = createPdfPath();
                    $destination = $pdfPath['fullDir'] . $viewParam['estimate']['estimate_num'] . '.pdf';
                    wkhtmltopdf($bodyEstimate, $destination);
                    $params['attachment'] = $destination;
                }
                // Send Email
                $success = EmailNotification::sendEmail($params, '', array(), array('reference_id' => $estimate['id'], 'type' => 'estimate'));

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Email sent successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Email not sent"));
                }
                echo 1;
                exit;
            }
            $this->view->error_mesages = $error_mesages;
        }

        $this->view->estimate = $estimate;
        $this->view->to = $to;
        $this->view->subject = $subject;
        $this->view->body = $body;
        $this->view->cc = isset($cc) ? $cc : '';

        echo $this->view->render('index/send-reminder-estimate-as-email.phtml');
        exit;
    }

    public function downloadEstimateAction() {


        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('downloadEstimate'));

        $estimateId = $this->request->getParam('id', 0);

        if (!CheckAuth::checkIfCanHandelAllCompany('estimate', $estimateId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Estimate not belongs to your Company"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // load models
        //
        $modelBookingEstimate = new Model_BookingEstimate();
        $modelBooking = new Model_Booking();

        //
        // geting data
        //
        $estimate = $modelBookingEstimate->getById($estimateId);

        //
        // validation
        //
        if (!$estimate) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Estimate not exist"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        if ($estimate['estimate_type'] == 'booking') {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Estimate has been converted to booking"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        if (!$modelBooking->checkBookingIfAccepted($estimate['booking_id'])) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Esitmate not accepted"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $viewParam = $this->getEstimateViewParam($estimateId, true);

        $view = new Zend_View();
        $view->setScriptPath(APPLICATION_PATH . '/modules/estimates/views/scripts/index');
        $view->bookingServices = $viewParam['bookingServices'];
        $view->thisBookingServices = $viewParam['thisBookingServices'];
        $view->priceArray = $viewParam['priceArray'];
        $view->estimate = $viewParam['estimate'];

        $html = $view->render('estimate.phtml');

        $pdfPath = createPdfPath();

        $filename = $pdfPath['fullDir'] . $viewParam['estimate']['estimate_num'] . '.pdf';
        wkhtmltopdf($html, $filename);

        header("Pragma: public");
        header("Expires: 0");
        header("Pragma: no-cache");
        header("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header('Content-disposition: attachment; filename=' . basename($filename));
        header("Content-Type: application/pdf");
        header("Content-Transfer-Encoding: binary");
        header('Content-Length: ' . filesize($filename));
        @readfile($filename);
        exit(0);
    }

	/*
    public function getEstimateViewParam($estimateId, $toBuffer = false) {

        //
        // load model
        //
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelBookingEstimate = new Model_BookingEstimate();

        //
        // get estimate data
        //
        $estimate = $modelBookingEstimate->getById($estimateId);

        //
        // validation
        //
        if (!$estimate) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Estimate not exist"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // filling extra data
        //
        $modelBookingEstimate->fill($estimate, array('booking', 'customer_commercial_info', 'customer_contacts'));

        $this->view->customer = $estimate['customer'];
        $this->view->customer_commercial_info = $estimate['customer_commercial_info'];



        $contractorServiceBookings = $modelContractorServiceBooking->getByBookingId($estimate['booking_id']);
        $modelBooking = new Model_Booking();
        $modelContractorServiceBookingTemp = new Model_ContractorServiceBookingTemp();
        $contractorServiceBookingsTemp = $modelContractorServiceBookingTemp->getByBookingId($estimate['booking_id']);

        $thisBookingServices = array();
        $priceArray = array();
        $isTemp = false;
        $bookingServices = array();
        if ($contractorServiceBookingsTemp && !$modelBooking->checkCanEditBookingDetails($estimate['booking_id']) && !$toBuffer) {
            $bookingServices = $contractorServiceBookingsTemp;
            $isTemp = true;
            foreach ($bookingServices as $bookingService) {

                $serviceId = $bookingService['service_id'];
                $clone = $bookingService['clone'];
                $bookingId = $bookingService['booking_id'];

                $service_and_clone = $serviceId . ($clone ? '_' . $clone : '');

                $thisBookingServices[] = $service_and_clone;

                $priceArray[$service_and_clone] = $modelContractorServiceBookingTemp->getTotalServiceBookingQoute($bookingId, $serviceId, $clone);
            }
        } elseif ($contractorServiceBookings) {
            $bookingServices = $contractorServiceBookings;
            foreach ($bookingServices as $bookingService) {

                $serviceId = $bookingService['service_id'];
                $clone = $bookingService['clone'];
                $bookingId = $bookingService['booking_id'];

                $service_and_clone = $serviceId . ($clone ? '_' . $clone : '');

                $thisBookingServices[] = $service_and_clone;

                $priceArray[$service_and_clone] = $modelContractorServiceBooking->getTotalServiceBookingQoute($bookingId, $serviceId, $clone);
            }
        }

        // customer type work order is_required Message

        $modelCustomerType = new Model_CustomerType();

        $isWorkOrder = false;
        $workOrder = $modelCustomerType->getCustomerTypeIsWorkOrder();

        if (in_array($estimate['customer']['customer_type_id'], $workOrder)) {
            $modelBookingAttachment = new Model_BookingAttachment();
            $bookingAttachments = $modelBookingAttachment->getByBookingIdOrInquiryId($bookingId, $estimate['booking']['original_inquiry_id']);
            $isWorkOrder = true;
            if (!empty($bookingAttachments)) {
                foreach ($bookingAttachments as $attachment) {
                    if ($attachment['work_order'] == 1) {
                        $isWorkOrder = false;
                    }
                }
            }
        }
        $this->view->isWorkOrder = $isWorkOrder;


        if (!$toBuffer) {
            $this->view->estimate = $estimate;
            $this->view->bookingServices = $bookingServices;
            $this->view->thisBookingServices = $thisBookingServices;
            $this->view->priceArray = $priceArray;
            $this->view->isTemp = $isTemp;
        } else {
            $viewParam = array();

            $viewParam['estimate'] = $estimate;
            $viewParam['bookingServices'] = $bookingServices;
            $viewParam['thisBookingServices'] = $thisBookingServices;
            $viewParam['priceArray'] = $priceArray;
            //$viewParam['isTemp'] = $isTemp;
            return $viewParam;
        }
        return false;
    }
	*/
	
	public function getEstimateViewParam($estimateId, $toBuffer = false) {
        // load model
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();
        $modelBookingEstimate = new Model_BookingEstimate();

        // get estimate data
        $estimate = $modelBookingEstimate->getById($estimateId);

        // validation
        if (!$estimate) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Estimate not exist"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        // filling extra data
        $modelBookingEstimate->fill($estimate, array('booking', 'customer_commercial_info', 'customer_contacts'));

		
		
        $this->view->customer = $estimate['customer'];
        $this->view->customer_commercial_info = $estimate['customer_commercial_info'];

		//D.A 17/09/2015 Caching estimate view blocks
		require_once 'Zend/Cache.php';	
		$company_id = CheckAuth::getCompanySession();					
		$estimateViewDir=get_config('cache').'/'.'estimatesView'.'/'.$company_id;
		if (!is_dir($estimateViewDir)) {
		mkdir($estimateViewDir, 0777, true);
		}						
		$frontEndOption= array('lifetime'=> NULL,
		'automatic_serialization'=> true);
		$backendOptions = array('cache_dir'=>$estimateViewDir );
		$cache = Zend_Cache::factory('Core','File',$frontEndOption,$backendOptions);
		
        //D.A 17/09/2015 Estimate Parameters Cache 
        $cacheID= $estimateId.'_estimateServices';	
		if (($result = $cache->load($cacheID)) === false ) {		
        $contractorServiceBookings = $modelContractorServiceBooking->getByBookingId($estimate['booking_id']);
        $modelBooking = new Model_Booking();
        $modelContractorServiceBookingTemp = new Model_ContractorServiceBookingTemp();
        $contractorServiceBookingsTemp = $modelContractorServiceBookingTemp->getByBookingId($estimate['booking_id']);

        $thisBookingServices = array();
        $priceArray = array();
        $isTemp = false;
        $bookingServices = array();
        if ($contractorServiceBookingsTemp && !$modelBooking->checkCanEditBookingDetails($estimate['booking_id']) && !$toBuffer) {
            $bookingServices = $contractorServiceBookingsTemp;
            $isTemp = true;
            foreach ($bookingServices as $bookingService) {
                $serviceId = $bookingService['service_id'];
                $clone = $bookingService['clone'];
                $bookingId = $bookingService['booking_id'];

                $service_and_clone = $serviceId . ($clone ? '_' . $clone : '');

                $thisBookingServices[] = $service_and_clone;

                $priceArray[$service_and_clone] = $modelContractorServiceBookingTemp->getTotalServiceBookingQoute($bookingId, $serviceId, $clone);
            }
        } elseif ($contractorServiceBookings) {
            $bookingServices = $contractorServiceBookings;
            foreach ($bookingServices as $bookingService) {
                $serviceId = $bookingService['service_id'];
                $clone = $bookingService['clone'];
                $bookingId = $bookingService['booking_id'];

                $service_and_clone = $serviceId . ($clone ? '_' . $clone : '');

                $thisBookingServices[] = $service_and_clone;

                $priceArray[$service_and_clone] = $modelContractorServiceBooking->getTotalServiceBookingQoute($bookingId, $serviceId, $clone);
            }
        }

		$result = array(
						'bookingServices' =>$bookingServices,
						'thisBookingServices' =>$thisBookingServices,
						'priceArray' =>$priceArray,
						'isTemp' =>$isTemp
						);
		   if($result){
				$cache->save($result,$cacheID);	
			}
		}				
		else{			
			$bookingServices=$result['bookingServices'];	
			$thisBookingServices=$result['thisBookingServices'];	
			$priceArray=$result['priceArray'];	
			$isTemp=$result['isTemp'];			
		}
        // customer type work order is_required Message
        $modelCustomerType = new Model_CustomerType();

        $isWorkOrder = false;
        $workOrder = $modelCustomerType->getCustomerTypeIsWorkOrder();

        if (in_array($estimate['customer']['customer_type_id'], $workOrder)) {
            $modelBookingAttachment = new Model_BookingAttachment();
            $bookingAttachments = $modelBookingAttachment->getByBookingIdOrInquiryId($estimate['booking_id'], $estimate['booking']['original_inquiry_id']);
            $isWorkOrder = true;
            if (!empty($bookingAttachments)) {
                foreach ($bookingAttachments as $attachment) {
                    if ($attachment['work_order'] == 1) {
                        $isWorkOrder = false;
                    }
                }
            }
        }
        $this->view->isWorkOrder = $isWorkOrder;

        if (!$toBuffer) {
            $this->view->estimate = $estimate;
            $this->view->bookingServices = $bookingServices;
            $this->view->thisBookingServices = $thisBookingServices;
            $this->view->priceArray = $priceArray;
            $this->view->isTemp = $isTemp;
        } else {
            $viewParam = array();
            $viewParam['estimate'] = $estimate;
            $viewParam['bookingServices'] = $bookingServices;
            $viewParam['thisBookingServices'] = $thisBookingServices;
            $viewParam['priceArray'] = $priceArray;
            return $viewParam;
        }
        return false;
    }

    public function editEstimateNumberAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('editEstimateNumber'));

        //
        // get params
        //
        $estimateId = $this->request->getParam('id');
        $estimate_number = $this->request->getParam('estimate_number');

        //
        // load model
        //
        $modelBookingLog = new Model_BookingLog();
        $modelBookingEstimate = new Model_BookingEstimate();
        $estimate = $modelBookingEstimate->getById($estimateId);
        if (!$estimate) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Estimate not exist"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        if ($estimate['estimate_type'] == 'booking') {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Estimate has been converted to booking"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        if (!CheckAuth::checkIfCanHandelAllCompany('estimate', $estimateId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Estimate not belongs to your Company"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }


        if (!$this->request->isPost()) {
            $estimate_number = $estimate['estimate_num'];
        }
        $form = new Estimates_Form_EditEstimateNumber(array('estimate_id' => $estimateId, 'estimate_number' => $estimate_number));

        if ($this->request->isPost()) {
            if ($form->isValid($this->request->getPost())) {
                // validate form data
                $dbParams = array(
                    'estimate_num' => $estimate_number
                );

                // add  data log
                $modelBookingLog->addBookingLog($estimate['booking_id']);

                // update estimate
                $modelBookingEstimate->updateById($estimateId, $dbParams);

                echo 1;
                exit;
            }
        }
        $this->view->form = $form;

        //
        // render views
        //
        echo $this->view->render('index/edit-estimate-number.phtml');
        exit;
    }

    public function addAction() {
        /**
         * check Auth for logged user
         */
        CheckAuth::checkPermission(array('bookingAdd'));

        $this->_redirect('booking-add?toEstimate=1');
        exit;
    }
	
	public function addToCustomerIdAction() {
        /**
         * check Auth for logged user
         */
        CheckAuth::checkPermission(array('bookingAdd'));
		
		$customerId = $this->request->getParam('customer_id');
        $this->_redirect('booking-add?toEstimate=1&customer_id='.$customerId);
        exit;
    }
	///////////////By Islam
	public function coppyAction() {
        /**
         * check Auth for logged user
         */
        CheckAuth::checkPermission(array('bookingAdd'));

        $this->_redirect('booking-add?toEstimate=1');
        exit;
    }
//////////////end
    public function removeFollowUpEstimateAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('bookingEdit'));

        //
        // get params
        //
        $estimateId = $this->request->getParam('id');


        //
        // load model
        //
        $modelBooking = new Model_Booking();
        $modelBookingEstimate = new Model_BookingEstimate();
        $estimate = $modelBookingEstimate->getById($estimateId);
        if (!$estimate) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Estimate not exist"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        if ($estimate['estimate_type'] == 'booking') {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Estimate has been converted to booking"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        if (!CheckAuth::checkIfCanHandelAllCompany('estimate', $estimateId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Estimate not belongs to your Company"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $modelBooking->updateById($estimate['booking_id'], array('is_to_follow' => 0));

        $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
    }

    public function followUpEstimateAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('bookingEdit'));

        //
        // get params
        //
        $estimateId = $this->request->getParam('id');
        $followDate = $this->request->getParam('followDate');

        //
        // load model
        //
        
        $modelBookingEstimate = new Model_BookingEstimate();
        $estimate = $modelBookingEstimate->getById($estimateId);

        if (!$estimate) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Estimate not exist"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        if ($estimate['estimate_type'] == 'booking') {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Estimate has been converted to booking"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        if (!CheckAuth::checkIfCanHandelAllCompany('estimate', $estimateId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Estimate not belongs to your Company"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // init action form
        //
        $form = new Estimates_Form_FollowUPEstimate(array('estimate' => $estimate));

        if ($this->request->isPost()) { // check if POST request method
            $modelBooking = new Model_Booking();
            $booking = $modelBooking->getById($estimate['booking_id']);

            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'is_to_follow' => 1,
                    'to_follow' => mySql2PhpTime($followDate),
                );

                $success = $modelBooking->updateById($booking['booking_id'], $data);

                if ($success) {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Saved successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No Change in Follow up"));
                }

                echo 1;
                exit;
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('index/follow-up-estimate.phtml');
        exit;
    }

    public function convertEstimateToBookingAction() {


        //check Auth for logged user
        CheckAuth::checkPermission(array('convertEstimateToBooking'));

        //get request parameters
        $bookingId = $this->request->getParam('id');

        $modelBooking = new Model_Booking();
        if (!$modelBooking->checkIfCanEditBooking($bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "You don't have edit permission in this Estimate"));
            $this->_redirect($this->router->assemble(array(), 'booking'));
        }
        if (!CheckAuth::checkIfCanHandelAllCompany('booking', $bookingId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Estimate not belongs to your Company"));
            $this->_redirect($this->router->assemble(array(), 'booking'));
        }
        $modelBookingEstimate = new Model_BookingEstimate();
        $estimate = $modelBookingEstimate->getNotDeletedByBookingId($bookingId);
        if (!$estimate) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Estimate not exist"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        if ($estimate['estimate_type'] == 'booking') {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Estimate has been converted to booking"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $modelBookingStatus = new Model_BookingStatus();
        $bookingStatus = $modelBookingStatus->getByStatusName('TO DO');

        $this->_redirect('booking-edit?booking_id=' . $bookingId . '&status_id=' . $bookingStatus['booking_status_id']);
        exit;
    }

    public function sendAdvertisingEmailAction() {

        //
        // get params 
        //
        $estimateId = $this->request->getParam('id', 0);


        if (!CheckAuth::checkIfCanHandelAllCompany('estimate', $estimateId)) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Estimate not belongs to your Company"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // load models
        //
        $modelUser = new Model_User();
        $modelBooking = new Model_Booking();
        $modelCustomer = new Model_Customer();
        $modelBookingEstimate = new Model_BookingEstimate();
        $modelBookingAddress = new Model_BookingAddress();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();

        //
        // get estimate data
        //
        $estimate = $modelBookingEstimate->getById($estimateId);

        //
        // validation
        //
        if (!$estimate) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Estimate not exist"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }
        if ($estimate['estimate_type'] == 'booking') {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "This Estimate has been converted to booking"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        //
        // filling extra data
        //
        $booking = $modelBooking->getById($estimate['booking_id']);
        $customer = $modelCustomer->getById($booking['customer_id']);
        $user = $modelUser->getById($booking['created_by']);

        // get all images
        $allIimageAttachments = $this->getImageAttachmentsbyBookingId($estimate['booking_id']);

        $image_attachments = '';
        foreach ($allIimageAttachments as $key => $imageAttachments) {
            $image_attachments .= "<div><span style='font-weight: bold;'>Floor :{$key}</span>";
            foreach ($imageAttachments as $imageAttachment) {

                $large_path = Zend_Controller_Front::getInstance()->getBaseUrl() . '/uploads/image_attachment/' . $imageAttachment['large_path'];
                $description = $imageAttachment['description'];

                $image_attachments .= "<div style='margin-top: 15px;'>";
                if ($description) {
                    $image_attachments .= "<div>";
                    $image_attachments .= "<span style='font-weight: bold;'>{$description}</span>";
                    $image_attachments .= "</div>";
                }
                $image_attachments .= "<div>";
                $image_attachments .= "<img width='510' data-mce-src='{$large_path}' alt='{$description}' src='{$large_path}'/>";
                $image_attachments .= "</div>";
                $image_attachments .= "</div>";
            }
            $image_attachments .= "</div>";
        }

        //estimate view param
        $viewParam = $this->getEstimateViewParam($estimateId, true);
        $view = new Zend_View();
        $view->setScriptPath(APPLICATION_PATH . '/modules/estimates/views/scripts/index');
        $view->bookingServices = $viewParam['bookingServices'];
        $view->thisBookingServices = $viewParam['thisBookingServices'];
        $view->priceArray = $viewParam['priceArray'];
        $view->estimate = $viewParam['estimate'];
        $bodyEstimate = $view->render('estimate.phtml');

        $template_params = array(
            //imageAttachments
            '{image_attachments}' => $image_attachments,
            //estimate
            '{estimate_num}' => $estimate['estimate_num'],
            '{estimate_created}' => date('d/m/Y', $estimate['created']),
            //booking
            '{booking_num}' => $booking['booking_num'],
            '{total_without_tax}' => number_format($booking['sub_total'], 2),
            '{gst_tax}' => number_format($booking['gst'], 2),
            '{total_with_tax}' => number_format($booking['qoute'], 2),
            '{description}' => $booking['description'] ? $booking['description'] : '',
            '{booking_created}' => date('d/m/Y', $booking['created']),
            '{booking_created_by}' => ucwords($user['username']),
            '{booking_start}' => date("F j, Y, g:i a", strtotime($booking['booking_start'])),
            '{booking_address}' => get_line_address($modelBookingAddress->getByBookingId($booking['booking_id'])),
            '{service}' => nl2br($modelContractorServiceBooking->getBookingAsText($booking['booking_id'])),
            '{estimate_view}' => $bodyEstimate,
            //customer
            '{customer_name}' => get_customer_name($customer),
            '{customer_first_name}' => isset($customer['first_name']) && $customer['first_name'] ? ucwords($customer['first_name']) : '',
            '{customer_last_name}' => isset($customer['last_name']) && $customer['last_name'] ? ' ' . ucwords($customer['last_name']) : '',
            '{customer_contacts}' => nl2br($modelCustomer->getCustomerContacts($booking['customer_id'])),
        );

        $modelEmailTemplate = new Model_EmailTemplate();
        $emailTemplate = $modelEmailTemplate->getEmailTemplate('send_estimate_advertising_email', $template_params);

        $body = $emailTemplate['body'];
        $subject = $emailTemplate['subject'];
        $to = array();
        if ($customer['email1']) {
            $to[] = $customer['email1'];
        }
        if ($customer['email2']) {
            $to[] = $customer['email2'];
        }
        if ($customer['email2']) {
            $to[] = $customer['email3'];
        }
        $to = implode(',', $to);

        if ($this->request->isPost()) {
            $to = $this->request->getParam('to');
            $cc = $this->request->getParam('cc');
            $subject = $this->request->getParam('subject');
            $body = $this->request->getParam('body');
            $pdf_attachment = $this->request->getParam('pdf_attachment', 0);
			
			
	$trading_namesObj = new Model_TradingName();
        $trading_names = $trading_namesObj->getById($estimate['booking']['trading_name_id']);
		
			

            $params = array(
                'to' => $to,
                'cc' => $cc,
                'body' => $body,
                'subject' => $subject,
                'layout' => 'designed_email_template',
				'trading_name' => $trading_names['trading_name'],
		'from' => $trading_names['email'],	

            );

            $error_mesages = array();
            if (EmailNotification::validation($params, $error_mesages)) {

                if (!empty($pdf_attachment)) {
                    // Create pdf
                    $pdfPath = createPdfPath();
                    $destination = $pdfPath['fullDir'] . $viewParam['estimate']['estimate_num'] . '.pdf';
                    wkhtmltopdf($bodyEstimate, $destination);
                    $params['attachment'] = $destination;
                }
                // Send Email
                $success = EmailNotification::sendEmail($params, '', array(), array('reference_id' => $estimateId, 'type' => 'estimate'));

                if ($success) {

                    $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Email sent successfully"));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Email not sent"));
                }

                echo 1;
                exit;
            }
            $this->view->error_mesages = $error_mesages;
        }

        $this->view->estimate = $estimate;
        $this->view->to = $to;
        $this->view->subject = $subject;
        $this->view->body = $body;
        $this->view->cc = isset($cc) ? $cc : '';

        echo $this->view->render('index/send-advertising-email.phtml');
        exit;
    }

    public function getImageAttachmentsbyBookingId($bookingId) {

        // load model
        $modelServiceAttribute = new Model_ServiceAttribute();
        $modelAttributeListValue = new Model_AttributeListValue();
        $modelServiceAttributeValue = new Model_ServiceAttributeValue();
        $modelAttributes = new Model_Attributes();
        $modelImageAttachment = new Model_ImageAttachment();
        $modelContractorServiceBooking = new Model_ContractorServiceBooking();


        $contractorServiceBookings = $modelContractorServiceBooking->getByBookingId($bookingId);

        $allImagesBooking = array();
        foreach ($contractorServiceBookings as $bookingService) {

            $attribute = $modelAttributes->getByVariableName('Floor');

            //get service attribute
            $serviceAttribute = $modelServiceAttribute->getByAttributeIdAndServiceId($attribute['attribute_id'], $bookingService['service_id']);

            $serviceAttributeValue = $modelServiceAttributeValue->getByBookingIdAndServiceAttributeIdAndClone($bookingId, $serviceAttribute['service_attribute_id'], $bookingService['clone']);

            $attributeListValue = $modelAttributeListValue->getById($serviceAttributeValue['value']);

            // get images
            $limit = get_config('image_attachments_limit');
            $allImagesBooking[$attributeListValue['attribute_value']] = $modelImageAttachment->getByAttributListValueId($attributeListValue['attribute_value_id'], $limit);
        }

        return $allImagesBooking;
    }
	
	public function advanceSearchAction() {
			
				echo $this->view->render('index/filters.phtml');
			exit;
			}
			
	//D.A 17/09/2015 clear inquiry cache
	public function estimateCacheClearAction() {
		
        //get request parameters
        $estimateId = $this->request->getParam('id');

		require_once 'Zend/Cache.php';
		$company_id = CheckAuth::getCompanySession();		
		$inquiryViewDir=get_config('cache').'/'.'estimatesView'.'/'.$company_id;								
		$frontEndOption= array('lifetime'=> NULL,
		'automatic_serialization'=> true);
		$backendOptions = array('cache_dir'=>$inquiryViewDir );
		$Cache = Zend_Cache::factory('Core','File',$frontEndOption,$backendOptions);	
		$Cache->remove($estimateId.'_estimateParams');
		$Cache->remove($estimateId.'_estimateQuestions');
		$Cache->remove($estimateId.'_estimateAvailableTechnicians');
		$Cache->remove($estimateId.'_estimatePhoto');
		$Cache->remove($estimateId.'_estimateServices');
		
		$this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Cache Cleared Successfully"));
		$this->_redirect($this->router->assemble(array('id' => $estimateId), 'estimateView'));	
    }		

}