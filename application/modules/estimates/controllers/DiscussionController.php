<?php

class Estimates_DiscussionController extends Zend_Controller_Action {

    private $request;
    private $router;
    private $loggedUser;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        //
        //checkLoggedIn
        //
        CheckAuth::checkLoggedIn();

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('estimateDiscussion'));

        //
        //get Logged User
        //
        $this->loggedUser = CheckAuth::getLoggedUser();
    }

    public function indexAction() {

        //
        //get Params
        //
      
        $estimateId = $this->request->getParam('id', 0);

        //
        // load model
        //
        $modelEstimateDiscussion = new Model_EstimateDiscussion();
        $modelBookingEstimate = new Model_BookingEstimate;
        $modelContractorInfo = new Model_ContractorInfo();
        $modelUser = new Model_User();
        $modelImageAttachment = new Model_Image();


        $estimate = $modelBookingEstimate->getById($estimateId);
        $this->view->estimate = $estimate;

        if (!$estimate) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "No discussion"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $estimateDiscussions = $modelEstimateDiscussion->getByEstimateId($estimateId, 'DESC');
        $GroupsEstimateImageDiscussions = $modelEstimateDiscussion->getByEstimateId($estimateId, 'DESC', array('groups' => 'groups'));
        $estimateDiscussions = array_merge($estimateDiscussions, $GroupsEstimateImageDiscussions);
        array_multisort($estimateDiscussions, SORT_ASC);

        foreach ($estimateDiscussions as &$estimateDiscussion) {
            $estimateDiscussion['user'] = $modelUser->getById($estimateDiscussion['user_id']);
            if ($estimateDiscussion['user']['role_name'] == 'contractor') {
                $contractorInfo = $modelContractorInfo->getByContractorId($estimateDiscussion['user']['user_id']);
                $estimateDiscussion['user']['username'] = ucwords($estimateDiscussion['user']['username']) . ' - ' . ucwords($contractorInfo['business_name']);
            } else {
                $estimateDiscussion['user']['username'] = ucwords($estimateDiscussion['user']['username']);
            }
        }

        $this->view->estimateDiscussions = $estimateDiscussions;

        echo $this->view->render('discussion/index.phtml');
        exit;
    }

    public function submitAction() {

        //
        //get Params
        //
        $estimateId = $this->request->getParam('id', 0);
        $discussion = $this->request->getParam('discussion', '');
        $imageId = $this->request->getParam('imageId', 0);
        //
        // load model
        //
        $modelEstimateDiscussion = new Model_EstimateDiscussion();

        $success = 0;
        if ($discussion) {
            $data = array(
                'estimate_id' => $estimateId,
                'user_id' => $this->loggedUser['user_id'],
                'user_message' => $discussion,
                'created' => time()
            );
            $success = $modelEstimateDiscussion->insert($data);

            $modelItemImageDiscussion = new Model_ItemImageDiscussion();

            $dataDiscssion = array(
                'image_id' => $imageId,
                'group_id' => 0,
                'item_id' => $success,
                'type' => 'estimate'
            );
            $modelItemImageDiscussion->insert($dataDiscssion);
        }

        if ($success) {
            MobileNotification::notify(0, 'estimate discussion', array('estimate_id' => $estimateId, 'discussion_id' => $success));
            echo json_encode(array('success' => 1));
        } else {
            echo json_encode(array('success' => 0, 'errMsg' => 'Invalid discussion'));
        }
        exit;
    }

    public function getAllDiscussionAction() {

        //
        //get Params
        //
        $estimateId = $this->request->getParam('id', 0);

        //
        // load model
        //
        $modelEstimateDiscussion = new Model_EstimateDiscussion();
        $modelUser = new Model_User();
        $modelContractorInfo = new Model_ContractorInfo();
        $modelImageAttachment = new Model_Image();

        $estimateDiscussions = $modelEstimateDiscussion->getByEstimateId($estimateId, 'DESC');
        $GroupsEstimateImageDiscussions = $modelEstimateDiscussion->getByEstimateId($estimateId, 'DESC', array('groups' => 'groups'));
        $estimateDiscussions = array_merge($estimateDiscussions, $GroupsEstimateImageDiscussions);
        array_multisort($estimateDiscussions, SORT_ASC);

        foreach ($estimateDiscussions as &$estimateDiscussion) {
            $estimateDiscussion['user'] = $modelUser->getById($estimateDiscussion['user_id']);
            if ($estimateDiscussion['user']['role_name'] == 'contractor') {
                $contractorInfo = $modelContractorInfo->getByContractorId($estimateDiscussion['user']['user_id']);
                $estimateDiscussion['user']['username'] = ucwords($estimateDiscussion['user']['username']) . ' - ' . ucwords($contractorInfo['business_name']);
            } else {
                $estimateDiscussion['user']['username'] = ucwords($estimateDiscussion['user']['username']);
            }
            $estimateDiscussion['discussion_date'] = getDateFormating($estimateDiscussion['created']);
            $estimateDiscussion['user_message'] = nl2br(htmlentities($estimateDiscussion['user_message'], ENT_NOQUOTES, 'UTF-8'));
            if ($estimateDiscussion['group_id']) {
                $estimateDiscussion['grouped_images'] = $modelImageAttachment->getByGroupId($estimateDiscussion['group_id']);
            }
        }

        $json_array = array(
            'discussions' => $estimateDiscussions,
            'count' => count($estimateDiscussions)
        );

        echo json_encode($json_array);
        exit;
    }

    public function getAllImageDiscussionAction() {

        //
        //get Params
        //
        $imageId = $this->request->getParam('id', 0);

        //
        // load model
        //
        $modelEstimateDiscussion = new Model_EstimateDiscussion();
        $modelContractorInfo = new Model_ContractorInfo();
        $modelUser = new Model_User();

        $imageDiscussions = $modelEstimateDiscussion->getByImageId($imageId, 'Desc');


        foreach ($imageDiscussions as &$imageDiscussion) {
            $imageDiscussion['user'] = $modelUser->getById($imageDiscussion['user_id']);
            $imageDiscussion['discussion_date'] = getDateFormating($imageDiscussion['created']);
            $imageDiscussion['user_message'] = nl2br(htmlentities($imageDiscussion['user_message'], ENT_NOQUOTES, 'UTF-8'));
            if ($imageDiscussion['user']['role_name'] == 'contractor') {
                $contractorInfo = $modelContractorInfo->getByContractorId($imageDiscussion['user']['user_id']);
                $imageDiscussion['user']['username'] = ucwords($imageDiscussion['user']['username']) . ' - ' . ucwords($contractorInfo['business_name']);
            } else {
                $imageDiscussion['user']['username'] = ucwords($imageDiscussion['user']['username']);
            }
        }

        $json_array = array(
            'discussions' => $imageDiscussions,
            'count' => count($imageDiscussions)
        );



        echo json_encode($json_array);
        exit;
    }
	
	public function updateDeletedCommentAction(){
		$discussion_id = $this->request->getParam('discussion_id');
		$estimate_id = $this->request->getParam('estimate_id');
		$have_images = $this->request->getParam('have_images', 0);
		$image_id = $this->request->getParam('image_id',0);
		$group_id = $this->request->getParam('group_id',0);
		$success = 0;
		
		$modelEstimateDiscussion = new Model_EstimateDiscussion();
		
		$data = array(
            'is_deleted' => 1             
        );
		
		$success = $modelEstimateDiscussion->updateById($discussion_id, $data);
		if($have_images){
			
			$successDelete = 0;
			$successDeletes = array();
			$modelImage = new Model_Image();
			
			if($image_id){
				
				$successDelete = $modelImage->deleteById($image_id);
			}else if($group_id){
							
				$estimate_grouped_images = $modelImage->getByGroupIdAndType($group_id, $estimate_id, 'estimate');
 
				foreach($estimate_grouped_images as $k => $img){
					$successDeletes[$k] = $modelImage->deleteById($img['image_id']);
				}
			}
			
			if($success && ($successDelete || !in_array(0,$successDeletes))){
				echo 1;				
			}
			exit;
		}
		
		if($success){
			echo 1;
		}
		exit;
		
	}

}
