<?php

class Estimates_DiscussionController extends Zend_Controller_Action {

    private $request;
    private $router;
    private $loggedUser;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();

        //
        //checkLoggedIn
        //
        CheckAuth::checkLoggedIn();

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('estimateDiscussion'));

        //
        //get Logged User
        //
        $this->loggedUser = CheckAuth::getLoggedUser();
    }

    public function indexAction() {

        //
        //get Params
        //
      
        $estimateId = $this->request->getParam('id', 0);

        //
        // load model
        //
        $modelEstimateDiscussion = new Model_EstimateDiscussion();
        $modelBookingEstimate = new Model_BookingEstimate;
        $modelContractorInfo = new Model_ContractorInfo();
        $modelUser = new Model_User();


        $estimate = $modelBookingEstimate->getById($estimateId);
        $this->view->estimate = $estimate;

        if (!$estimate) {
            $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "There Is No Item Exists"));
            $this->_redirect((isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''));
        }

        $estimateDiscussions = $modelEstimateDiscussion->getByEstimateId($estimateId);
        foreach ($estimateDiscussions as &$estimateDiscussion) {
            $estimateDiscussion['user'] = $modelUser->getById($estimateDiscussion['user_id']);
            if ($estimateDiscussion['user']['role_name'] == 'contractor') {
                $contractorInfo = $modelContractorInfo->getByContractorId($estimateDiscussion['user']['user_id']);
                $estimateDiscussion['user']['username'] = ucwords($estimateDiscussion['user']['username']) . ' - ' . ucwords($contractorInfo['business_name']);
            } else {
                $estimateDiscussion['user']['username'] = ucwords($estimateDiscussion['user']['username']);
            }
        }

        $this->view->estimateDiscussions = $estimateDiscussions;

        echo $this->view->render('discussion/index.phtml');
        exit;
    }

    public function submitAction() {

        //
        //get Params
        //
        $estimateId = $this->request->getParam('id', 0);
        $discussion = $this->request->getParam('discussion', '');
        //
        // load model
        //
        $modelEstimateDiscussion = new Model_EstimateDiscussion();

        $success = 0;
        if ($discussion) {
            $data = array(
                'estimate_id' => $estimateId,
                'user_id' => $this->loggedUser['user_id'],
                'user_message' => $discussion,
                'created' => time()
            );
            $success = $modelEstimateDiscussion->insert($data);
        }

        if ($success) {
            echo json_encode(array('success' => 1));
        } else {
            echo json_encode(array('success' => 0, 'errMsg' => 'Invalid Discussion'));
        }
        exit;
    }

    public function getAllDiscussionAction() {

        //
        //get Params
        //
        $estimateId = $this->request->getParam('id', 0);

        //
        // load model
        //
        $modelEstimateDiscussion = new Model_EstimateDiscussion();
        $modelUser = new Model_User();
        $modelContractorInfo = new Model_ContractorInfo();

        $estimateDiscussions = $modelEstimateDiscussion->getByEstimateId($estimateId);
        foreach ($estimateDiscussions as &$estimateDiscussion) {
            $estimateDiscussion['user'] = $modelUser->getById($estimateDiscussion['user_id']);
            if ($estimateDiscussion['user']['role_name'] == 'contractor') {
                $contractorInfo = $modelContractorInfo->getByContractorId($estimateDiscussion['user']['user_id']);
                $estimateDiscussion['user']['username'] = ucwords($estimateDiscussion['user']['username']) . ' - ' . ucwords($contractorInfo['business_name']);
            } else {
                $estimateDiscussion['user']['username'] = ucwords($estimateDiscussion['user']['username']);
            }
            $estimateDiscussion['discussion_date'] = getDateFormating( $estimateDiscussion['created']);
            $estimateDiscussion['user_message'] = nl2br(htmlentities($estimateDiscussion['user_message'], ENT_NOQUOTES, 'UTF-8'));
        }

        $json_array = array(
            'discussions' => $estimateDiscussions,
            'count' => count($estimateDiscussions)
        );

        echo json_encode($json_array);
        exit;
    }

}