<?php

class Estimates_LabelController extends Zend_Controller_Action {

    private $request;
    private $router;

    public function init() {
        parent::init();
        $this->request = $this->getRequest();
        $this->router = Zend_Controller_Front::getInstance()->getRouter();
    }

    public function selectLabelAction() {

        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('estimateLabel'));

        $estimateId = $this->request->getParam('id');
        $label_ids = $this->request->getParam('label_ids', array());

        //
        //load model
        //
        $modelLabel = new Model_Label();
        $modelEstimateLabel = new Model_EstimateLabel();
        $estimateLabels = $modelEstimateLabel->getByEstimateId($estimateId);
        $labelIds = array();
        if ($estimateLabels) {
            foreach ($estimateLabels as $estimateLabel) {
                $labelIds[] = $estimateLabel['label_id'];
            }
        }

        //
        //get data
        //
        $labels = $modelLabel->getAll();

        $this->view->labels = $labels;

        if ($this->request->isPost()) {

            $modelEstimateLabel->setLabelsToEstimate($estimateId, $label_ids);

            if (!empty($label_ids)) {
                foreach ($label_ids as $label_id) {
                    $label = $modelLabel->getById($label_id);
                    $json_labels[$label_id] = $label['label_name'];
                }
                echo json_encode(array('result' => 1, 'labels' => $json_labels, 'entityId' => $estimateId));
            } else {
                echo json_encode(array('result' => 0, 'entityId' => $estimateId));
            }
            exit;
        }

        $this->view->estimateId = $estimateId;
        $this->view->label_ids = $labelIds;

        echo $this->view->render('label/select-label.phtml');
        exit;
    }

    public function searchLabelAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('estimateLabel'));

        $modelLabel = new Model_Label();

        $filters['keywords'] = $this->request->getParam('label-like');
        $labels = $modelLabel->getAll($filters);
        $this->view->labels = $labels;

        echo $this->view->render('label/search-label.phtml');
        exit;
    }

    public function filterLabelAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('estimateFilterLabel'));

        $label_ids = $this->request->getParam('label_ids', array());
        $my_estimates = $this->request->getParam('my_estimates', false);
        $this->view->my_estimates = $my_estimates;

        //
        //load model
        //
        $modelLabel = new Model_Label();

        $labels = $modelLabel->getAll();
        $this->view->labels = $labels;

        if ($this->request->isPost()) {

            $fltrLabels = array();
            if ($label_ids) {
                foreach ($label_ids as $label_id) {
                    $fltrLabels[] = 'fltr[estimate_label_ids][]=' . $label_id;
                }
            }

            $fltr = implode('&', $fltrLabels);
            if ($my_estimates) {
                $fltr .= ($fltr ? '&' : '') . 'my_estimates=true';
            }

            echo $this->router->assemble(array(), 'estimates') . ($fltr ? '?' . $fltr : '');
            exit;
        }

        echo $this->view->render('label/filter-label.phtml');
        exit;
    }

    public function addLabelAction() {
        //
        // check Auth for logged user
        //
        CheckAuth::checkPermission(array('estimateAddLabel'));

        //
        // get request parameters
        //
        $labelName = $this->request->getParam('label_name', '');
        $estimateId = $this->request->getParam('id');

        //
        //load model
        //
        $modelLabel = new Model_Label();
        $modelEstimateLabel = new Model_EstimateLabel();

        //
        // init action form
        //
        $form = new Estimates_Form_Label();


        //
        // handling the insertion process
        //
        if ($this->request->isPost()) { // check if POST request method
            if ($form->isValid($this->request->getPost())) { // validate form data
                $data = array(
                    'label_name' => $labelName,
                    'company_id' => CheckAuth::getCompanySession()
                );

                $labelId = $modelLabel->insert($data);

                $params = array(
                    'estimate_id' => $estimateId,
                    'label_id' => $labelId
                );
                $modelEstimateLabel->assignLabelToEstimate($params);

                $estimateLabels = $modelEstimateLabel->getByEstimateId($estimateId);
                foreach ($estimateLabels AS $estimateLabel) {
                    $label = $modelLabel->getById($estimateLabel['label_id']);
                    $json_labels[$estimateLabel['label_id']] = $label['label_name'];
                }
                echo json_encode(array('result' => 1, 'labels' => $json_labels, 'entityId' => $estimateId));
                exit;
            }
        }

        $this->view->form = $form;
        //
        // render views
        //
        echo $this->view->render('label/add_edit.phtml');
        exit;
    }

}

