<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {

    protected function _initRoutes() {

        Zend_Session::start();

        //create a router
        $router = Zend_Controller_Front::getInstance()->getRouter();

        $router->addRoute('openApp', new Zend_Controller_Router_Route('webservice/open-app/:booking_id', array('module' => 'webservice', 'controller' => 'index', 'action' => 'open-app')));

        $router->addRoute('updateOldFiles', new Zend_Controller_Router_Route('webservice/update-old-files', array('module' => 'webservice', 'controller' => 'index', 'action' => 'update-old-files')));
        //by salim for test 
        $router->addRoute('salimtest', new Zend_Controller_Router_Route('webservice/salim-test', array('module' => 'webservice', 'controller' => 'index', 'action' => 'salim-test')));

        //not loged user
        $router->addRoute('pageNotFound', new Zend_Controller_Router_Route('page-not-found', array('module' => 'default', 'controller' => 'index', 'action' => 'page-not-found')));
        $router->addRoute('cancelBooking', new Zend_Controller_Router_Route('cancel/:cancel_hashcode/:booking_id/:status_id', array('module' => 'default', 'controller' => 'index', 'action' => 'cancel-booking')));
        $router->addRoute('feedbackBooking', new Zend_Controller_Router_Route('booking/feedback/:feedback_hashcode/:booking_id/:status_id', array('module' => 'default', 'controller' => 'index', 'action' => 'feedback-booking')));
        //By Islam
        $router->addRoute('orderAttribute', new Zend_Controller_Router_Route('/ajax/orderAttribute', array('module' => 'settings', 'controller' => 'attribute', 'action' => 'order-attribute')));

        $router->addRoute('getItemCounts', new Zend_Controller_Router_Route('/ajax/getItemCounts', array('module' => 'default', 'controller' => 'index', 'action' => 'get-item-counts')));

        $router->addRoute('completeProfileVideo', new Zend_Controller_Router_Route('completeProfileVideo', array('module' => 'default', 'controller' => 'index', 'action' => 'complete-profile-video')));
        $router->addRoute('completeProfile', new Zend_Controller_Router_Route('completeProfile', array('module' => 'default', 'controller' => 'index', 'action' => 'complete-profile')));

        //
        //test
        //
		//D.A 12/10/2015
        $router->addRoute('bookingForOneDate', new Zend_Controller_Router_Route('booking/index/bookings-for-date', array('module' => 'booking', 'controller' => 'index', 'action' => 'bookings-for-date')));



        $router->addRoute('bookingCheckInCheckOutList', new Zend_Controller_Router_Route('booking-attendance', array('module' => 'default', 'controller' => 'booking-attendance', 'action' => 'index')));

        $router->addRoute('test', new Zend_Controller_Router_Route('test', array('module' => 'test', 'controller' => 'index', 'action' => 'index')));

        $router->addRoute('unavailableTime', new Zend_Controller_Router_Route('calendar/index/unavailable-time', array('module' => 'calendar', 'controller' => 'index', 'action' => 'unavailable-time')));

        $router->addRoute('addBannedIp', new Zend_Controller_Router_Route('inquiry/add_banned_ip/:ip', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'add-banned-ip')));
        $router->addRoute('removeBannedIp', new Zend_Controller_Router_Route('inquiry/remove_banned_ip/:ip', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'remove-banned-ip')));


        /* Route time date configration ***IBM */
        $router->addRoute('settingsDateTime', new Zend_Controller_Router_Route('settings/date-time-configuration', array('module' => 'settings', 'controller' => 'date-time', 'action' => 'index')));
        $router->addRoute('settingsAddDateTime', new Zend_Controller_Router_Route('settings/date-time-configuration/add', array('module' => 'settings', 'controller' => 'date-time', 'action' => 'add')));
        $router->addRoute('settingsEditDateTime', new Zend_Controller_Router_Route('settings/date-time-configuration/edit/:id', array('module' => 'settings', 'controller' => 'date-time', 'action' => 'edit')));
        $router->addRoute('getTimeZoneByCountry', new Zend_Controller_Router_Route('dropdown/timezone', array('module' => 'settings', 'controller' => 'date-time', 'action' => 'get-time-zone-by-country')));
        $router->addRoute('getDateFormatByCountry', new Zend_Controller_Router_Route('dropdown/datefromat', array('module' => 'settings', 'controller' => 'date-time', 'action' => 'get-date-format-by-country')));
        $router->addRoute('getTimeFormatByCountry', new Zend_Controller_Router_Route('dropdown/timefromat', array('module' => 'settings', 'controller' => 'date-time', 'action' => 'get-time-format-by-country')));

        //D.A 31/08/2015 
        $router->addRoute('settingsCache', new Zend_Controller_Router_Route('settings/cache', array('module' => 'settings', 'controller' => 'cache', 'action' => 'index')));
        $router->addRoute('settingsDeleteAllCache', new Zend_Controller_Router_Route('settings/cache/delete-all-cache', array('module' => 'settings', 'controller' => 'cache', 'action' => 'delete-all-cache')));
        $router->addRoute('settingsDeleteBookingCache', new Zend_Controller_Router_Route('settings/cache/delete-booking-cache', array('module' => 'settings', 'controller' => 'cache', 'action' => 'delete-booking-cache')));
        $router->addRoute('settingsDeleteCalendarCache', new Zend_Controller_Router_Route('settings/cache/delete-calendar-cache', array('module' => 'settings', 'controller' => 'cache', 'action' => 'delete-calendar-cache')));
        $router->addRoute('bookingCacheClear', new Zend_Controller_Router_Route('bookingCacheClear/:id', array('module' => 'booking', 'controller' => 'index', 'action' => 'booking-cache-clear')));

        $router->addRoute('inquiryCacheClear', new Zend_Controller_Router_Route('inquiryCacheClear/:id', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'inquiry-cache-clear')));

        $router->addRoute('settingsDeleteInquiryCache', new Zend_Controller_Router_Route('settings/cache/delete-inquiry-cache', array('module' => 'settings', 'controller' => 'cache', 'action' => 'delete-inquiry-cache')));
        //D.A 17/09/2015 estimate Cache Clear
        $router->addRoute('estimateCacheClear', new Zend_Controller_Router_Route('estimateCacheClear/:id', array('module' => 'estimates', 'controller' => 'index', 'action' => 'estimate-cache-clear')));
        $router->addRoute('settingsDeleteEstimateCache', new Zend_Controller_Router_Route('settings/cache/delete-estimate-cache', array('module' => 'settings', 'controller' => 'cache', 'action' => 'delete-estimate-cache')));

        //by haneen 
        $router->addRoute('confirm', new Zend_Controller_Router_Route('confirm/:id', array('module' => 'default', 'controller' => 'index', 'action' => 'confirm')));
        $router->addRoute('customerBookingLogin', new Zend_Controller_Router_Route('customer/:type/:booking_id', array('module' => 'default', 'controller' => 'index', 'action' => 'customer-booking-login')));

        // end haneen
        // mailing list routes

        $router->addRoute('settingsMailingList', new Zend_Controller_Router_Route('settings/mailing-list', array('module' => 'settings', 'controller' => 'mailing-list', 'action' => 'index')));
        $router->addRoute('settingsMailingListAdd', new Zend_Controller_Router_Route('settings/mailing-list/add', array('module' => 'settings', 'controller' => 'mailing-list', 'action' => 'add')));
        $router->addRoute('settingsMailingListEdit', new Zend_Controller_Router_Route('settings/mailing-list/edit/:id', array('module' => 'settings', 'controller' => 'mailing-list', 'action' => 'edit')));
        $router->addRoute('settingsMailingListDelete', new Zend_Controller_Router_Route('settings/mailing-list/delete/:id', array('module' => 'settings', 'controller' => 'mailing-list', 'action' => 'delete')));
        $router->addRoute('settingsMailingListDeleteBtn', new Zend_Controller_Router_Route('settings/mailing-list/delete', array('module' => 'settings', 'controller' => 'mailing-list', 'action' => 'delete')));
        $router->addRoute('settingsMailingListSendEmail', new Zend_Controller_Router_Route('settings/mailing-list/send-email', array('module' => 'settings', 'controller' => 'mailing-list', 'action' => 'send-email')));
        $router->addRoute('settingsMailingListConfirmSendEmail', new Zend_Controller_Router_Route('settings/mailing-list/confirm-send-email', array('module' => 'settings', 'controller' => 'mailing-list', 'action' => 'confirm-send-email')));
        $router->addRoute('MailingListEmails', new Zend_Controller_Router_Route('settings/mailing-list/mailing-list-emails/:id', array('module' => 'settings', 'controller' => 'mailing-list', 'action' => 'mailing-list-emails')));
        $router->addRoute('settingsMailingListResendEmail', new Zend_Controller_Router_Route('settings/mailing-list/send-email/:id', array('module' => 'settings', 'controller' => 'mailing-list', 'action' => 'send-email')));
        $router->addRoute('settingsMailingListUnsubscribe', new Zend_Controller_Router_Route('settings/mailing-list/unsubscribe/:mailing_id/:customer_id/:email', array('module' => 'settings', 'controller' => 'mailing-list', 'action' => 'unsubscribe')));
        $router->addRoute('UnsubscribeEmails', new Zend_Controller_Router_Route('unsubscribe-link/:cronjob_id/:customer_id/:email/:trading_name_id', array('module' => 'settings', 'controller' => 'mailing-list', 'action' => 'unsubscribe')));

        $router->addRoute('UnsubscribeEmails2', new Zend_Controller_Router_Route('unsubscribe-link/:cronjob_id/:customer_id/:email', array('module' => 'settings', 'controller' => 'mailing-list', 'action' => 'unsubscribe')));

        $router->addRoute('getSystemAttachments', new Zend_Controller_Router_Route('getSystemAttachments', array('module' => 'default', 'controller' => 'common', 'action' => 'get-system-attachments')));


        $router->addRoute('UnsubscribeLink', new Zend_Controller_Router_Route('unsubscribe/:email_template_id/:customer_id/:email/:trading_name_id', array('module' => 'settings', 'controller' => 'mailing-list', 'action' => 'unsubscribe')));
        $router->addRoute('downloadCustomersMailingList', new Zend_Controller_Router_Route('settings/mailing-list/download-customers-xl/:mailing_list_id', array('module' => 'settings', 'controller' => 'mailing-list', 'action' => 'download-customers-xl')));
        $router->addRoute('syncEmailsToMailChimp', new Zend_Controller_Router_Route('settings/mail-chimp/:name', array('module' => 'settings', 'controller' => 'mailing-list', 'action' => 'sync-emails-to-mail-chimp')));

        // unsubscribe reason

        $router->addRoute('settingsUnsubscribeReasonsList', new Zend_Controller_Router_Route('settings/unsubscribe-reason', array('module' => 'settings', 'controller' => 'unsubscribe-reason', 'action' => 'index')));
        $router->addRoute('settingsUnsubscribeReasonsAdd', new Zend_Controller_Router_Route('settings/unsubscribe-reason/add', array('module' => 'settings', 'controller' => 'unsubscribe-reason', 'action' => 'add')));
        $router->addRoute('settingsUnsubscribeReasonsEdit', new Zend_Controller_Router_Route('settings/unsubscribe-reason/edit/:id', array('module' => 'settings', 'controller' => 'unsubscribe-reason', 'action' => 'edit')));
        $router->addRoute('settingsUnsubscribeReasonsDelete', new Zend_Controller_Router_Route('settings/unsubscribe-reason/delete/:id', array('module' => 'settings', 'controller' => 'unsubscribe-reason', 'action' => 'delete')));
        $router->addRoute('settingsUnsubscribeReasonsDeleteBtn', new Zend_Controller_Router_Route('settings/unsubscribe-reason/delete', array('module' => 'settings', 'controller' => 'unsubscribe-reason', 'action' => 'delete')));
        $router->addRoute('changeStatusOfServices', new Zend_Controller_Router_Route('change-status-service', array('module' => 'settings', 'controller' => 'services', 'action' => 'change-service-status')));

        //logout all users by salim
        $router->addRoute('forceLogout', new Zend_Controller_Router_Route('force-logout/', array('module' => 'default', 'controller' => 'common', 'action' => 'logout-all-users')));
        //
        // create settings routers
        //
        //
        //
           // By Salim Booking Address Details
        $router->addRoute('bookingAddressDetails', new Zend_Controller_Router_Route('booking-address-details/:booking_id', array('module' => 'booking', 'controller' => 'index', 'action' => 'booking-address')));
        // By Salim remove Assign Invoice from payment
        $router->addRoute('removeAssign', new Zend_Controller_Router_Route('payment/remove-assign/:payment_id', array('module' => 'invoices', 'controller' => 'index', 'action' => 'remove-assign')));

        //Add Notifications Routes By Salim
        $router->addRoute('notifications', new Zend_Controller_Router_Route('notifications', array('module' => 'notification', 'controller' => 'index', 'action' => 'index')));
        $router->addRoute('notificationsSettings', new Zend_Controller_Router_Route('notifications/settings', array('module' => 'notification', 'controller' => 'index', 'action' => 'notifications-settings')));
        $router->addRoute('readOneNotification', new Zend_Controller_Router_Route('notifications/read-one-notification', array('module' => 'notification', 'controller' => 'index', 'action' => 'read')));
        $router->addRoute('markAllAsRead', new Zend_Controller_Router_Route('notifications/MarkAllAsRead', array('module' => 'notification', 'controller' => 'index', 'action' => 'mark-all-as-read')));
        $router->addRoute('resetCount', new Zend_Controller_Router_Route('notifications/resetCount', array('module' => 'notification', 'controller' => 'index', 'action' => 'reset-count')));
        $router->addRoute('discussionNotifications', new Zend_Controller_Router_Route('discussion-notifications', array('module' => 'notification', 'controller' => 'index', 'action' => 'discussion-notifications')));

        //by Salim import bank payment 
        $router->addRoute('importBankPayments', new Zend_Controller_Router_Route('invoices/bank-payments', array('module' => 'invoices', 'controller' => 'index', 'action' => 'import-bank-payments')));
        // by Salim Import Credit Cared Payments
        $router->addRoute('importCreditCardPayments', new Zend_Controller_Router_Route('payments/credit-card-payments', array('module' => 'invoices', 'controller' => 'index', 'action' => 'import-credit-card-payments')));
        //by salim change invoice 
        $router->addRoute('changeInvoiceNumber', new Zend_Controller_Router_Route('invoice/change-invoice/:id', array('module' => 'invoices', 'controller' => 'payment', 'action' => 'change-invoice')));
        //by Salim ignored keyword 
        $router->addRoute('ignoredKeywords', new Zend_Controller_Router_Route('ignored-keywords', array('module' => 'invoices', 'controller' => 'index', 'action' => 'ignored-keywords')));
        // By Salim Ignored Payments Section 
        $router->addRoute('ignoredPayments', new Zend_Controller_Router_Route('ignored-payments', array('module' => 'invoices', 'controller' => 'payment', 'action' => 'ignored-payments')));
        //By Salim Matched Payments 
        $router->addRoute('matchedPayments', new Zend_Controller_Router_Route('payments/matched-payments', array('module' => 'invoices', 'controller' => 'payment', 'action' => 'matched-payments')));


        //By Salim working Hours 

        $router->addRoute('editWorkingHours', new Zend_Controller_Router_Route('edit-working-hours/:contractor_id', array('module' => 'default', 'controller' => 'index', 'action' => 'edit-working-hours')));
        $router->addRoute('deleteWorkingHour', new Zend_Controller_Router_Route('delete-working-hours', array('module' => 'default', 'controller' => 'index', 'action' => 'delete-working-hours')));

        // 22/06/2015 D.A
        $router->addRoute('settingsCompaniesTradingNamesImages', new Zend_Controller_Router_Route('settings/images-trading-names/:id', array('module' => 'settings', 'controller' => 'trading-names', 'action' => 'assign-images')));

        $router->addRoute('imagesbytemplateId', new Zend_Controller_Router_Route('settings/email-template-images', array('module' => 'settings', 'controller' => 'email-template', 'action' => 'trading-images')));

        $router->addRoute('deleteTrdadingnameImage', new Zend_Controller_Router_Route('settings/deleteTrdadingnameImage', array('module' => 'settings', 'controller' => 'trading-names', 'action' => 'delete-trdading-name-image')));

        $router->addRoute('updateTrdadingnameImage', new Zend_Controller_Router_Route('settings/updateTrdadingnameImage', array('module' => 'settings', 'controller' => 'trading-names', 'action' => 'update-trdading-name-image')));
        $router->addRoute('removeTradingNameLogo', new Zend_Controller_Router_Route('remove-logo', array('module' => 'settings', 'controller' => 'trading-names', 'action' => 'remove-logo')));

        $router->addRoute('tradingNamesById', new Zend_Controller_Router_Route('settings/trading-names-template-images', array('module' => 'settings', 'controller' => 'trading-names', 'action' => 'trading-names-template-images')));
        /// End Doaa;
        $router->addRoute('settings', new Zend_Controller_Router_Route('settings', array('module' => 'settings', 'controller' => 'settings', 'action' => 'index')));
        $router->addRoute('settingsLocation', new Zend_Controller_Router_Route('settings/location', array('module' => 'settings', 'controller' => 'settings', 'action' => 'settings-location')));
        $router->addRoute('settingsPayment', new Zend_Controller_Router_Route('settings/payment', array('module' => 'settings', 'controller' => 'settings', 'action' => 'settings-payment')));
        $router->addRoute('settingsUser', new Zend_Controller_Router_Route('settings/users', array('module' => 'settings', 'controller' => 'settings', 'action' => 'settings-user')));
        $router->addRoute('settingsServices', new Zend_Controller_Router_Route('settings/service', array('module' => 'settings', 'controller' => 'settings', 'action' => 'settings-services')));
        $router->addRoute('settingsType', new Zend_Controller_Router_Route('settings/type', array('module' => 'settings', 'controller' => 'settings', 'action' => 'settings-type')));
        $router->addRoute('settingsEmail', new Zend_Controller_Router_Route('settings/email', array('module' => 'settings', 'controller' => 'settings', 'action' => 'settings-email')));
        $router->addRoute('settingsLabel', new Zend_Controller_Router_Route('settings/label', array('module' => 'settings', 'controller' => 'settings', 'action' => 'settings-label')));
        $router->addRoute('settingsCompanies', new Zend_Controller_Router_Route('settings/company', array('module' => 'settings', 'controller' => 'settings', 'action' => 'settings-company')));

        $router->addRoute('checkIfUserActiveOrNot', new Zend_Controller_Router_Route('settings/user', array('module' => 'settings', 'controller' => 'settings', 'action' => 'check')));

        $router->addRoute('settingsSecurity', new Zend_Controller_Router_Route('settings/security', array('module' => 'settings', 'controller' => 'settings', 'action' => 'settings-security')));
        $router->addRoute('settingsSecurityBannedIpsList', new Zend_Controller_Router_Route('settings/security_banned_ips', array('module' => 'settings', 'controller' => 'security-banned-ips', 'action' => 'index')));
        $router->addRoute('inquiryAddToCustomerID', new Zend_Controller_Router_Route('inquiry/add/:customer_id', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'add')));
        //
        // By Abdallah routes for advance search 
        $router->addRoute('advanceBookingSearch', new Zend_Controller_Router_Route('booking/index/advance_filters/', array('module' => 'booking', 'controller' => 'index', 'action' => 'advance-search')));
        $router->addRoute('advanceInquirySearch', new Zend_Controller_Router_Route('inquiries/advance_filters', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'advance-search')));
        $router->addRoute('advanceEstimateSearch', new Zend_Controller_Router_Route('estimates/advance_filters', array('module' => 'estimates', 'controller' => 'index', 'action' => 'advance-search')));
        $router->addRoute('advanceInvoiceSearch', new Zend_Controller_Router_Route('invoices/advance_filters', array('module' => 'invoices', 'controller' => 'index', 'action' => 'advance-search')));
        $router->addRoute('advanceComplaintSearch', new Zend_Controller_Router_Route('complaint/advance_filters', array('module' => 'complaint', 'controller' => 'index', 'action' => 'advance-search')));
        $router->addRoute('advanceCustomerSearch', new Zend_Controller_Router_Route('customer/advance_filters', array('module' => 'customer', 'controller' => 'index', 'action' => 'advance-search')));

        $router->addRoute('settingsContractorpicupload', new Zend_Controller_Router_Route('settings/contractor-info/upload/:contractor_id', array('module' => 'settings', 'controller' => 'contractor-info', 'action' => 'contractor-pic')));
        $router->addRoute('settingUserPicUpload', new Zend_Controller_Router_Route('settings/user/upload', array('module' => 'default', 'controller' => 'index', 'action' => 'user-pic')));
        $router->addRoute('settingContractorPicUpload', new Zend_Controller_Router_Route('settings/user/upload/:id', array('module' => 'default', 'controller' => 'index', 'action' => 'user-pic')));

        $router->addRoute('localEmailsViewMessage', new Zend_Controller_Router_Route('local/view/:id', array('module' => 'gmail', 'controller' => 'index', 'action' => 'local-message')));
        $router->addRoute('changeEmailStatus', new Zend_Controller_Router_Route('local/change_status', array('module' => 'gmail', 'controller' => 'index', 'action' => 'change-status')));
        $router->addRoute('localEmailsViewMessages', new Zend_Controller_Router_Route('local/view', array('module' => 'gmail', 'controller' => 'index', 'action' => 'local-message')));


        $router->addRoute('toggleActivateOrDeactivateUsers', new Zend_Controller_Router_Route('settings/user/toggle_active/:id', array('module' => 'settings', 'controller' => 'user', 'action' => 'toggle-activate-or-deactivate-users')));
        $router->addRoute('toggleActivateOrDeactivateUsers2', new Zend_Controller_Router_Route('settings/users/toggle_active', array('module' => 'settings', 'controller' => 'user', 'action' => 'toggle-activate-or-deactivate-users')));


        $router->addRoute('settingsContractorInfoAdd', new Zend_Controller_Router_Route('settings/contractor-info/add/:contractor_id', array('module' => 'settings', 'controller' => 'contractor-info', 'action' => 'add')));
        $router->addRoute('settingsContractorInfoChangeStatus', new Zend_Controller_Router_Route('settings/contractor-info/change-status/:contractor_id', array('module' => 'settings', 'controller' => 'contractor-info', 'action' => 'change-contractor-status')));

        $router->addRoute('settingsContractorInfoEditInsurance', new Zend_Controller_Router_Route('settings/contractor-info/edit_insurance/:contractor_id', array('module' => 'settings', 'controller' => 'contractor-info', 'action' => 'edit-Insurance')));


        $router->addRoute('settingsContractorInfoEditBankDetails', new Zend_Controller_Router_Route('settings/contractor-info/edit-bank/:contractor_id', array('module' => 'settings', 'controller' => 'contractor-info', 'action' => 'edit-bank')));

        $router->addRoute('settingsContractorInfoEditLicence', new Zend_Controller_Router_Route('settings/contractor-info/edit_licence/:contractor_id', array('module' => 'settings', 'controller' => 'contractor-info', 'action' => 'edit-licence')));

        $router->addRoute('settingsContractorInfoEditPayment', new Zend_Controller_Router_Route('settings/contractor-info/edit_Payment/:contractor_id', array('module' => 'settings', 'controller' => 'contractor-info', 'action' => 'edit-Payment')));

        $router->addRoute('settingsContractorServiceList2', new Zend_Controller_Router_Route('settings/contractor-service_2/:contractor_id', array('module' => 'settings', 'controller' => 'contractor-service', 'action' => 'contractor-services')));

        $router->addRoute('settingsEditServiceCommission', new Zend_Controller_Router_Route('settings/edit-service-commission/:contractor_id/:contractor_service_id', array('module' => 'settings', 'controller' => 'contractor-service', 'action' => 'edit-service-commission')));

        $router->addRoute('allContractorDiscussion', new Zend_Controller_Router_Route('settings/contractor-discussion/all-contractor-discussion/:contractor_id', array('module' => 'settings', 'controller' => 'contractor-discussion', 'action' => 'all-contractor-discussion')));

        $router->addRoute('settingsContractorInfoGetAttachment', new Zend_Controller_Router_Route('settings/contractor-info/get-attacments/:contractor_info_id/:type', array('module' => 'settings', 'controller' => 'contractor-info', 'action' => 'get-attacments')));



        $router->addRoute('settingsContractorInfoActivityLog', new Zend_Controller_Router_Route('settings/contractor-info/log-activity/:contractor_id', array('module' => 'settings', 'controller' => 'contractor-info', 'action' => 'log-activity')));


        /*         * ***Device/s information for contractor****IBM */
        $router->addRoute('settingsDeviceContractorInfo', new Zend_Controller_Router_Route('settings/contractor-info/device/:contractor_id', array('module' => 'settings', 'controller' => 'contractor-info', 'action' => 'get-all-contractor-device-info')));





        //image galary By Mona

        $router->addRoute('ItemAttachmentList', new Zend_Controller_Router_Route('item-attachment/:type/:itemid', array('module' => 'default', 'controller' => 'item-attachment', 'action' => 'index')));
        $router->addRoute('ItemAttachmentListByScroll', new Zend_Controller_Router_Route('item-attachment/', array('module' => 'default', 'controller' => 'item-attachment', 'action' => 'get-attachment-by-scroll')));
        $router->addRoute('ServiceImageAttachmentList', new Zend_Controller_Router_Route('item-attachment/:type', array('module' => 'default', 'controller' => 'item-attachment', 'action' => 'index')));
        $router->addRoute('ItemImageAttachmentAdd', new Zend_Controller_Router_Route('item-attachment/image-upload/:type/:itemid', array('module' => 'default', 'controller' => 'item-attachment', 'action' => 'image-upload')));
        $router->addRoute('ItemImageAttachmentAttach', new Zend_Controller_Router_Route('item-attachment/image-attach/:type/:itemid', array('module' => 'default', 'controller' => 'item-attachment', 'action' => 'image-attach')));
        $router->addRoute('ItemImageAttachmentAttachByService', new Zend_Controller_Router_Route('item-attachment/image-attach/:type/:itemid/:serviceid', array('module' => 'default', 'controller' => 'item-attachment', 'action' => 'image-attach')));
        $router->addRoute('ItemImageAttachmentEdit', new Zend_Controller_Router_Route('item-attachment/image-edit/:type/:itemid/:id/', array('module' => 'default', 'controller' => 'item-attachment', 'action' => 'image-edit')));
        $router->addRoute('ServiceImageAttachmentEdit', new Zend_Controller_Router_Route('item-attachment/image-edit/:type/:id/', array('module' => 'default', 'controller' => 'item-attachment', 'action' => 'image-edit')));
        $router->addRoute('ItemImageAttachmentDeleteBtn', new Zend_Controller_Router_Route('item-attachment/delete/:type/:itemid', array('module' => 'default', 'controller' => 'item-attachment', 'action' => 'delete')));
        $router->addRoute('ItemImageAttachmentDelete', new Zend_Controller_Router_Route('item-attachment/delete/:type/:itemId/:id', array('module' => 'default', 'controller' => 'item-attachment', 'action' => 'delete')));
        $router->addRoute('ItemAttachmentDelete', new Zend_Controller_Router_Route('item-attachment/delete/:id', array('module' => 'default', 'controller' => 'item-attachment', 'action' => 'delete')));
        $router->addRoute('ServiceImageAttachmentDelete', new Zend_Controller_Router_Route('item-attachment/delete/:type/:id', array('module' => 'default', 'controller' => 'item-attachment', 'action' => 'delete')));
        $router->addRoute('OpenFileAttachment', new Zend_Controller_Router_Route('item-attachment/read-file', array('module' => 'default', 'controller' => 'item-attachment', 'action' => 'read-file')));
        $router->addRoute('slider', new Zend_Controller_Router_Route('default/slider/:processID/:imageID/:type', array('module' => 'default', 'controller' => 'index', 'action' => 'slider')));
        $router->addRoute('sliderTabs', new Zend_Controller_Router_Route('default/sliderTabs/:processID/:imageID/:type/:group_id', array('module' => 'default', 'controller' => 'index', 'action' => 'slider-tabs')));

        $router->addRoute('allImageDiscussion', new Zend_Controller_Router_Route('booking/discussion/get-all-image-discussion', array('module' => 'booking', 'controller' => 'discussion', 'action' => 'get-all-image-discussion')));

        // image types routes

        $router->addRoute('settingsImageTypesList', new Zend_Controller_Router_Route('settings/image-types', array('module' => 'settings', 'controller' => 'image-types', 'action' => 'index')));
        $router->addRoute('settingsImageTypesAdd', new Zend_Controller_Router_Route('settings/image-types/add', array('module' => 'settings', 'controller' => 'image-types', 'action' => 'add')));
        $router->addRoute('settingsImageTypesEdit', new Zend_Controller_Router_Route('settings/image-types/edit/:id', array('module' => 'settings', 'controller' => 'image-types', 'action' => 'edit')));
        $router->addRoute('settingsImageTypesDelete', new Zend_Controller_Router_Route('settings/image-types/delete/:id', array('module' => 'settings', 'controller' => 'image-types', 'action' => 'delete')));
        $router->addRoute('settingsImageTypesDeleteBtn', new Zend_Controller_Router_Route('settings/image-types/delete', array('module' => 'settings', 'controller' => 'image-types', 'action' => 'delete')));
        $router->addRoute('settingsImageTypesAddDescription', new Zend_Controller_Router_Route('settings/image-types/add-desription/:id', array('module' => 'settings', 'controller' => 'image-types', 'action' => 'add-description')));
        $router->addRoute('settingsImageTypesShowDescription', new Zend_Controller_Router_Route('settings/image-types/show-desription/:id', array('module' => 'settings', 'controller' => 'image-types', 'action' => 'show-description')));


        ////End By Mona
        //D.A 26/07/2015
        $router->addRoute('settingsBookingRejectQuestions', new Zend_Controller_Router_Route('settings/booking-reject-questions', array('module' => 'settings', 'controller' => 'booking-reject-questions', 'action' => 'index')));
        $router->addRoute('settingsBookingRejectQuestionsAdd', new Zend_Controller_Router_Route('settings/booking-reject-questions/add-edit-reject-question', array('module' => 'settings', 'controller' => 'booking-reject-questions', 'action' => 'add-reject-question')));
        $router->addRoute('settingsBookingRejectQuestionsEdit', new Zend_Controller_Router_Route('settings/booking-reject-questions/add-edit-update-question/:question_id', array('module' => 'settings', 'controller' => 'booking-reject-questions', 'action' => 'edit-reject-question')));
        $router->addRoute('settingsBookingRejectQuestionsDelete', new Zend_Controller_Router_Route('settings/booking-reject-questions/delete/:question_id', array('module' => 'settings', 'controller' => 'booking-reject-questions', 'action' => 'delete-reject-question')));
        $router->addRoute('settingsBookingRejectQuestionseditOption', new Zend_Controller_Router_Route('settings/booking-reject-questions_option_edit', array('module' => 'settings', 'controller' => 'booking-reject-questions', 'action' => 'edit-reject-question-option')));
        $router->addRoute('settingsBookingRejectQuestionsDeleteOption', new Zend_Controller_Router_Route('settings/booking-reject-questions_option_delete', array('module' => 'settings', 'controller' => 'booking-reject-questions', 'action' => 'delete-reject-question-option')));

        //by Salim 
        $router->addRoute('notAssignedBooking', new Zend_Controller_Router_Route('booking/notAssigned', array('module' => 'booking', 'controller' => 'index', 'action' => 'not-assigned-booking')));

        // By Mona 
        $router->addRoute('bookingEditDescription', new Zend_Controller_Router_Route('booking/edit-description/:id', array('module' => 'booking', 'controller' => 'index', 'action' => 'edit-description')));

        $router->addRoute('DeleteMultipleDay', new Zend_Controller_Router_Route('calendar/delete-multiple-day', array('module' => 'calendar', 'controller' => 'index', 'action' => 'delete-multiple-day')));

        ///By Islam Update booking Qestions
        $router->addRoute('settingsBookingUpdateQuestions', new Zend_Controller_Router_Route('settings/booking-update-questions', array('module' => 'settings', 'controller' => 'booking-update-questions', 'action' => 'index')));
        $router->addRoute('settingsBookingUpdateQuestionsViewStatus', new Zend_Controller_Router_Route('settings/booking-update-questions/view-booking-status/:question_id', array('module' => 'settings', 'controller' => 'booking-update-questions', 'action' => 'view-booking-status')));
        $router->addRoute('settingsBookingUpdateQuestionsAdd', new Zend_Controller_Router_Route('settings/booking-update-questions/add-edit-update-question', array('module' => 'settings', 'controller' => 'booking-update-questions', 'action' => 'add-update-question')));
        $router->addRoute('settingsBookingUpdateQuestionsEdit', new Zend_Controller_Router_Route('settings/booking-update-questions/add-edit-update-question/:question_id', array('module' => 'settings', 'controller' => 'booking-update-questions', 'action' => 'edit-update-question')));
        $router->addRoute('settingsBookingUpdateQuestionsDelete', new Zend_Controller_Router_Route('settings/booking-update-questions/delete/:question_id', array('module' => 'settings', 'controller' => 'booking-update-questions', 'action' => 'delete-update-question')));

        $router->addRoute('copyBooking', new Zend_Controller_Router_Route('calendar/index/booking-copy/:booking_id', array('module' => 'calendar', 'controller' => 'index', 'action' => 'copy')));
        $router->addRoute('settingsBookingStatusList', new Zend_Controller_Router_Route('settings/booking-status', array('module' => 'settings', 'controller' => 'booking-status', 'action' => 'index')));
        $router->addRoute('settingsBookingStatusAdd', new Zend_Controller_Router_Route('settings/booking-status/add', array('module' => 'settings', 'controller' => 'booking-status', 'action' => 'add')));
        $router->addRoute('settingsBookingStatusEdit', new Zend_Controller_Router_Route('settings/booking-status/edit/:id', array('module' => 'settings', 'controller' => 'booking-status', 'action' => 'edit')));
        $router->addRoute('settingsBookingStatusDelete', new Zend_Controller_Router_Route('settings/booking-status/delete/:id', array('module' => 'settings', 'controller' => 'booking-status', 'action' => 'delete')));
        $router->addRoute('settingsBookingStatusDeleteBtn', new Zend_Controller_Router_Route('settings/booking-status/delete', array('module' => 'settings', 'controller' => 'booking-status', 'action' => 'delete')));
        $router->addRoute('settingsBookingStatusChangeColor', new Zend_Controller_Router_Route('settings/booking-status/change-color/:id', array('module' => 'settings', 'controller' => 'booking-status', 'action' => 'change-color')));
        $router->addRoute('settingsBookingStatusAddDescription', new Zend_Controller_Router_Route('settings/booking-status/add-desription/:id', array('module' => 'settings', 'controller' => 'booking-status', 'action' => 'add-description')));
        $router->addRoute('settingsBookingStatusShowDescription', new Zend_Controller_Router_Route('settings/booking-status/show-desription/:id', array('module' => 'settings', 'controller' => 'booking-status', 'action' => 'show-description')));
        $router->addRoute('settingsBookingStatusGoogleCalender', new Zend_Controller_Router_Route('settings/booking-status/google-calender/:id', array('module' => 'settings', 'controller' => 'booking-status', 'action' => 'google-calender')));

        $router->addRoute('settingsImageAttachmentList', new Zend_Controller_Router_Route('settings/image-attachment', array('module' => 'settings', 'controller' => 'image-attachment', 'action' => 'index')));
        $router->addRoute('settingsImageAttachmentAdd', new Zend_Controller_Router_Route('settings/image-attachment/add', array('module' => 'settings', 'controller' => 'image-attachment', 'action' => 'add')));
        $router->addRoute('settingsImageAttachmentEdit', new Zend_Controller_Router_Route('settings/image-attachment/edit/:id', array('module' => 'settings', 'controller' => 'image-attachment', 'action' => 'edit')));
        $router->addRoute('settingsImageAttachmentDescription', new Zend_Controller_Router_Route('settings/image-attachment/description/:id', array('module' => 'settings', 'controller' => 'image-attachment', 'action' => 'description')));
        $router->addRoute('settingsImageAttachmentDelete', new Zend_Controller_Router_Route('settings/image-attachment/delete/:id', array('module' => 'settings', 'controller' => 'image-attachment', 'action' => 'delete')));
        $router->addRoute('settingsImageAttachmentDeleteBtn', new Zend_Controller_Router_Route('settings/image-attachment/delete', array('module' => 'settings', 'controller' => 'image-attachment', 'action' => 'delete')));

        $router->addRoute('settingsCompaniesList', new Zend_Controller_Router_Route('settings/companies', array('module' => 'settings', 'controller' => 'companies', 'action' => 'index')));
        $router->addRoute('settingsCompaniesAdd', new Zend_Controller_Router_Route('settings/companies/add', array('module' => 'settings', 'controller' => 'companies', 'action' => 'add')));
        $router->addRoute('settingsCompaniesLogoUpload', new Zend_Controller_Router_Route('settings/companies/logo-upload/:id', array('module' => 'settings', 'controller' => 'companies', 'action' => 'logo-upload')));
        $router->addRoute('settingsCompaniesInvoiceLogoUpload', new Zend_Controller_Router_Route('settings/companies/invoice-logo-upload/:id', array('module' => 'settings', 'controller' => 'companies', 'action' => 'invoice-logo-upload')));
        $router->addRoute('settingsCompaniesEdit', new Zend_Controller_Router_Route('settings/companies/edit/:id', array('module' => 'settings', 'controller' => 'companies', 'action' => 'edit')));
        $router->addRoute('settingsCompaniesDelete', new Zend_Controller_Router_Route('settings/companies/delete/:id', array('module' => 'settings', 'controller' => 'companies', 'action' => 'delete')));
        $router->addRoute('settingsCompaniesDeleteBtn', new Zend_Controller_Router_Route('settings/companies/delete', array('module' => 'settings', 'controller' => 'companies', 'action' => 'delete')));
        $router->addRoute('settingsCompaniesSignatureLogoUpload', new Zend_Controller_Router_Route('settings/companies/signature-logo-upload/:id', array('module' => 'settings', 'controller' => 'companies', 'action' => 'signature-logo-upload')));

        $router->addRoute('settingsCompanyInvoiceNoteList', new Zend_Controller_Router_Route('settings/company-invoice-note/:company_id', array('module' => 'settings', 'controller' => 'company-invoice-note', 'action' => 'index')));
        $router->addRoute('settingsCompanyInvoiceNoteAdd', new Zend_Controller_Router_Route('settings/company-invoice-note/add/:company_id', array('module' => 'settings', 'controller' => 'company-invoice-note', 'action' => 'add')));
        $router->addRoute('settingsCompanyInvoiceNoteEdit', new Zend_Controller_Router_Route('settings/company-invoice-note/edit/:company_id/:id', array('module' => 'settings', 'controller' => 'company-invoice-note', 'action' => 'edit')));
        $router->addRoute('settingsCompanyInvoiceNoteDelete', new Zend_Controller_Router_Route('settings/company-invoice-note/delete/:company_id/:id', array('module' => 'settings', 'controller' => 'company-invoice-note', 'action' => 'delete')));
        $router->addRoute('settingsCompanyInvoiceNoteDeleteBtn', new Zend_Controller_Router_Route('settings/company-invoice-note/delete/:company_id', array('module' => 'settings', 'controller' => 'company-invoice-note', 'action' => 'delete')));

        $router->addRoute('settingsInquiryTypeList', new Zend_Controller_Router_Route('settings/inquiry-type', array('module' => 'settings', 'controller' => 'inquiry-type', 'action' => 'index')));
        $router->addRoute('settingsInquiryTypeAdd', new Zend_Controller_Router_Route('settings/inquiry-type/add', array('module' => 'settings', 'controller' => 'inquiry-type', 'action' => 'add')));
        $router->addRoute('settingsInquiryTypeEdit', new Zend_Controller_Router_Route('settings/inquiry-type/edit/:id', array('module' => 'settings', 'controller' => 'inquiry-type', 'action' => 'edit')));
        $router->addRoute('settingsInquiryTypeDelete', new Zend_Controller_Router_Route('settings/inquiry-type/delete/:id', array('module' => 'settings', 'controller' => 'inquiry-type', 'action' => 'delete')));
        $router->addRoute('settingsInquiryTypeDeleteBtn', new Zend_Controller_Router_Route('settings/inquiry-type/delete', array('module' => 'settings', 'controller' => 'inquiry-type', 'action' => 'delete')));

        $router->addRoute('settingsPropertyTypes', new Zend_Controller_Router_Route('settings/property-type', array('module' => 'settings', 'controller' => 'property-type', 'action' => 'index')));
        $router->addRoute('settingsPropertyTypeAdd', new Zend_Controller_Router_Route('settings/property-type/add', array('module' => 'settings', 'controller' => 'property-type', 'action' => 'add')));
        $router->addRoute('settingsPropertyTypeDelete', new Zend_Controller_Router_Route('settings/property-type/delete/:id', array('module' => 'settings', 'controller' => 'property-type', 'action' => 'delete')));
        $router->addRoute('settingsPropertyTypeEdit', new Zend_Controller_Router_Route('settings/property-type/edit/:id', array('module' => 'settings', 'controller' => 'property-type', 'action' => 'edit')));

        $router->addRoute('settingsInquiryRequiredTypes', new Zend_Controller_Router_Route('settings/required-type', array('module' => 'settings', 'controller' => 'inquiry-required-type', 'action' => 'index')));
        $router->addRoute('settingsInquiryRequiredTypeAdd', new Zend_Controller_Router_Route('settings/required-type/add', array('module' => 'settings', 'controller' => 'inquiry-required-type', 'action' => 'add')));
        $router->addRoute('settingsInquiryRequiredTypeDelete', new Zend_Controller_Router_Route('settings/required-type/delete/:id', array('module' => 'settings', 'controller' => 'inquiry-required-type', 'action' => 'delete')));
        $router->addRoute('settingsInquiryRequiredTypeEdit', new Zend_Controller_Router_Route('settings/required-type/edit/:id', array('module' => 'settings', 'controller' => 'inquiry-required-type', 'action' => 'edit')));

        $router->addRoute('settingsComplaintTypeList', new Zend_Controller_Router_Route('settings/complaint-type', array('module' => 'settings', 'controller' => 'complaint-type', 'action' => 'index')));
        $router->addRoute('settingsComplaintTypeAdd', new Zend_Controller_Router_Route('settings/complaint-type/add', array('module' => 'settings', 'controller' => 'complaint-type', 'action' => 'add')));
        $router->addRoute('settingsComplaintTypeEdit', new Zend_Controller_Router_Route('settings/complaint-type/edit/:id', array('module' => 'settings', 'controller' => 'complaint-type', 'action' => 'edit')));
        $router->addRoute('settingsComplaintTypeDelete', new Zend_Controller_Router_Route('settings/complaint-type/delete/:id', array('module' => 'settings', 'controller' => 'complaint-type', 'action' => 'delete')));
        $router->addRoute('settingsComplaintTypeDeleteBtn', new Zend_Controller_Router_Route('settings/complaint-type/delete', array('module' => 'settings', 'controller' => 'complaint-type', 'action' => 'delete')));

        $router->addRoute('settingsCountriesList', new Zend_Controller_Router_Route('settings/countries', array('module' => 'settings', 'controller' => 'countries', 'action' => 'index')));
        $router->addRoute('settingsCountriesAdd', new Zend_Controller_Router_Route('settings/countries/add', array('module' => 'settings', 'controller' => 'countries', 'action' => 'add')));
        $router->addRoute('settingsCountriesEdit', new Zend_Controller_Router_Route('settings/countries/edit/:id', array('module' => 'settings', 'controller' => 'countries', 'action' => 'edit')));
        $router->addRoute('settingsCountriesDelete', new Zend_Controller_Router_Route('settings/countries/delete/:id', array('module' => 'settings', 'controller' => 'countries', 'action' => 'delete')));
        $router->addRoute('settingsCountriesDeleteBtn', new Zend_Controller_Router_Route('settings/countries/delete', array('module' => 'settings', 'controller' => 'countries', 'action' => 'delete')));

        $router->addRoute('settingsCitiesList', new Zend_Controller_Router_Route('settings/cities', array('module' => 'settings', 'controller' => 'cities', 'action' => 'index')));
        $router->addRoute('settingsCitiesAdd', new Zend_Controller_Router_Route('settings/cities/add', array('module' => 'settings', 'controller' => 'cities', 'action' => 'add')));
        $router->addRoute('settingsCitiesEdit', new Zend_Controller_Router_Route('settings/cities/edit/:id', array('module' => 'settings', 'controller' => 'cities', 'action' => 'edit')));
        $router->addRoute('settingsCitiesDelete', new Zend_Controller_Router_Route('settings/cities/delete/:id', array('module' => 'settings', 'controller' => 'cities', 'action' => 'delete')));
        $router->addRoute('settingsCitiesDeleteBtn', new Zend_Controller_Router_Route('settings/cities/delete', array('module' => 'settings', 'controller' => 'cities', 'action' => 'delete')));

        $router->addRoute('settingsBankList', new Zend_Controller_Router_Route('settings/bank', array('module' => 'settings', 'controller' => 'bank', 'action' => 'index')));
        $router->addRoute('settingsBankAdd', new Zend_Controller_Router_Route('settings/bank/add', array('module' => 'settings', 'controller' => 'bank', 'action' => 'add')));
        $router->addRoute('settingsBankEdit', new Zend_Controller_Router_Route('settings/bank/edit/:id', array('module' => 'settings', 'controller' => 'bank', 'action' => 'edit')));
        $router->addRoute('settingsBankDelete', new Zend_Controller_Router_Route('settings/bank/delete/:id', array('module' => 'settings', 'controller' => 'bank', 'action' => 'delete')));
        $router->addRoute('settingsBankDeleteBtn', new Zend_Controller_Router_Route('settings/bank/delete', array('module' => 'settings', 'controller' => 'bank', 'action' => 'delete')));

        $router->addRoute('settingsAttributeList', new Zend_Controller_Router_Route('settings/attribute', array('module' => 'settings', 'controller' => 'attribute', 'action' => 'index')));

        //Rand
        $router->addRoute('settingsServiceAttributePriceAdd', new Zend_Controller_Router_Route('settings/serviceAttributePrice/add/:attribute_id/:service_id', array('module' => 'settings', 'controller' => 'service-attribute', 'action' => 'add-service-attribute-price')));
        $router->addRoute('settingsServiceAttributePriceEdit', new Zend_Controller_Router_Route('settings/serviceAttributePrice/edit/:id', array('module' => 'settings', 'controller' => 'service-attribute', 'action' => 'edit-service-attribute-price')));
        //End Rand


        $router->addRoute('settingsAttributeAdd', new Zend_Controller_Router_Route('settings/attribute/add', array('module' => 'settings', 'controller' => 'attribute', 'action' => 'add')));
        $router->addRoute('settingsAttributeEdit', new Zend_Controller_Router_Route('settings/attribute/edit/:id', array('module' => 'settings', 'controller' => 'attribute', 'action' => 'edit')));
        $router->addRoute('settingsAttributeDelete', new Zend_Controller_Router_Route('settings/attribute/delete/:id', array('module' => 'settings', 'controller' => 'attribute', 'action' => 'delete')));
        $router->addRoute('settingsAttributeDeleteBtn', new Zend_Controller_Router_Route('settings/attribute/delete', array('module' => 'settings', 'controller' => 'attribute', 'action' => 'delete')));

        $router->addRoute('settingSetAttributeDefaultImage', new Zend_Controller_Router_Route('settings/attributes/set-attribute-default-image', array('module' => 'settings', 'controller' => 'attribute', 'action' => 'set-attribute-default-image')));

        $router->addRoute('settingAttributeValueImageUpload', new Zend_Controller_Router_Route('settings/attribute-value', array('module' => 'settings', 'controller' => 'attribute-value', 'action' => 'attribute-value-image')));

        $router->addRoute('settingSetServiceDefaultImage', new Zend_Controller_Router_Route('settings/services/set-service-default-image', array('module' => 'settings', 'controller' => 'services', 'action' => 'set-service-default-image')));

        $router->addRoute('settingsGetServiceImages', new Zend_Controller_Router_Route('settings/services/get-service-images/:service_id', array('module' => 'settings', 'controller' => 'services', 'action' => 'get-service-images')));

        $router->addRoute('GetServiceImages', new Zend_Controller_Router_Route('settings/services/get-service-images', array('module' => 'settings', 'controller' => 'services', 'action' => 'get-service-images')));

        $router->addRoute('settingsServicesList', new Zend_Controller_Router_Route('settings/services', array('module' => 'settings', 'controller' => 'services', 'action' => 'index')));
        $router->addRoute('settingsServicesAdd', new Zend_Controller_Router_Route('settings/services/add', array('module' => 'settings', 'controller' => 'services', 'action' => 'add')));
        $router->addRoute('settingsServicesEdit', new Zend_Controller_Router_Route('settings/services/edit/:id', array('module' => 'settings', 'controller' => 'services', 'action' => 'edit')));
        $router->addRoute('settingsServicesDelete', new Zend_Controller_Router_Route('settings/services/delete/:id', array('module' => 'settings', 'controller' => 'services', 'action' => 'delete')));
        $router->addRoute('settingsServicesDeleteBtn', new Zend_Controller_Router_Route('settings/services/delete', array('module' => 'settings', 'controller' => 'services', 'action' => 'delete')));

        //Rand
        $router->addRoute('settingsServiceDiscountAdd', new Zend_Controller_Router_Route('settings/servicesDiscount/add/:service_id', array('module' => 'settings', 'controller' => 'services', 'action' => 'add-service-discount')));
        $router->addRoute('settingsServiceDiscountEdit', new Zend_Controller_Router_Route('settings/servicesDiscount/edit/:id', array('module' => 'settings', 'controller' => 'services', 'action' => 'edit-service-discount')));

        //End Rand



        $router->addRoute('settingsAuthRoleList', new Zend_Controller_Router_Route('settings/auth-role', array('module' => 'settings', 'controller' => 'auth-role', 'action' => 'index')));
        $router->addRoute('settingsAuthRoleAdd', new Zend_Controller_Router_Route('settings/auth-role/add', array('module' => 'settings', 'controller' => 'auth-role', 'action' => 'add')));
        $router->addRoute('settingsAuthRoleEdit', new Zend_Controller_Router_Route('settings/auth-role/edit/:id', array('module' => 'settings', 'controller' => 'auth-role', 'action' => 'edit')));
        $router->addRoute('settingsAuthRoleDelete', new Zend_Controller_Router_Route('settings/auth-role/delete/:id', array('module' => 'settings', 'controller' => 'auth-role', 'action' => 'delete')));
        $router->addRoute('settingsAuthRoleDeleteBtn', new Zend_Controller_Router_Route('settings/auth-role/delete', array('module' => 'settings', 'controller' => 'auth-role', 'action' => 'delete')));

        $router->addRoute('settingsAuthCredentialList', new Zend_Controller_Router_Route('settings/auth-credential', array('module' => 'settings', 'controller' => 'auth-credential', 'action' => 'index')));
        $router->addRoute('settingsAuthCredentialAdd', new Zend_Controller_Router_Route('settings/auth-credential/add', array('module' => 'settings', 'controller' => 'auth-credential', 'action' => 'add')));
        $router->addRoute('settingsAuthCredentialAddParent', new Zend_Controller_Router_Route('settings/auth-credential/add/:parent_id', array('module' => 'settings', 'controller' => 'auth-credential', 'action' => 'add')));
        $router->addRoute('settingsAuthCredentialEdit', new Zend_Controller_Router_Route('settings/auth-credential/edit/:id', array('module' => 'settings', 'controller' => 'auth-credential', 'action' => 'edit')));
        $router->addRoute('settingsAuthCredentialDelete', new Zend_Controller_Router_Route('settings/auth-credential/delete/:id', array('module' => 'settings', 'controller' => 'auth-credential', 'action' => 'delete')));
        $router->addRoute('settingsAuthCredentialDeleteBtn', new Zend_Controller_Router_Route('settings/auth-credential/delete', array('module' => 'settings', 'controller' => 'auth-credential', 'action' => 'delete')));

        $router->addRoute('settingsUserList', new Zend_Controller_Router_Route('settings/users', array('module' => 'settings', 'controller' => 'user', 'action' => 'index')));
        $router->addRoute('settingsUserAdd', new Zend_Controller_Router_Route('settings/user/add', array('module' => 'settings', 'controller' => 'user', 'action' => 'add')));
        $router->addRoute('settingsGetCompanyInquiryEmail', new Zend_Controller_Router_Route('settings/user/get_company_inquiry_email', array('module' => 'settings', 'controller' => 'user', 'action' => 'get-company-inquiry-email')));
        $router->addRoute('settingsUserEdit', new Zend_Controller_Router_Route('settings/user/edit/:id', array('module' => 'settings', 'controller' => 'user', 'action' => 'edit')));
        $router->addRoute('settingsContractorEdit', new Zend_Controller_Router_Route('settings/user/ContractorAddressEdit/:id', array('module' => 'settings', 'controller' => 'user', 'action' => 'edit-contractor-address')));
        $router->addRoute('settingsUserPassword', new Zend_Controller_Router_Route('settings/user/change-password/:id', array('module' => 'settings', 'controller' => 'user', 'action' => 'change-password')));
        $router->addRoute('settingsUserLoginAsThisUser', new Zend_Controller_Router_Route('settings/user/login_as_this_user/:id', array('module' => 'settings', 'controller' => 'user', 'action' => 'login-as-this-user')));
        $router->addRoute('settingsUserDelete', new Zend_Controller_Router_Route('settings/user/delete/:id', array('module' => 'settings', 'controller' => 'user', 'action' => 'delete')));
        $router->addRoute('settingsUserDeleteBtn', new Zend_Controller_Router_Route('settings/user/delete', array('module' => 'settings', 'controller' => 'user', 'action' => 'delete')));

        $router->addRoute('settingsUserProfileDeleteBtn', new Zend_Controller_Router_Route('settings/user/delete-user-profile', array('module' => 'settings', 'controller' => 'user', 'action' => 'delete-user-profile')));

        $router->addRoute('settingsUserProfileDelete', new Zend_Controller_Router_Route('settings/user/delete-user-profile/:id', array('module' => 'settings', 'controller' => 'user', 'action' => 'delete-user-profile')));

        $router->addRoute('activateOrDeactivateUsers', new Zend_Controller_Router_Route('settings/user/active/:id', array('module' => 'settings', 'controller' => 'user', 'action' => 'activate-or-deactivate-users')));

        //Eman
        $router->addRoute('blockOrUnblockUsers', new Zend_Controller_Router_Route('settings/user/block/', array('module' => 'settings', 'controller' => 'user', 'action' => 'block-or-unblock-users')));

        //$router->addRoute('checkIfUserActiveOrNot', new Zend_Controller_Router_Route('settings/user/check', array('module' => 'settings', 'controller' => 'settings', 'action' => 'check-if-user-active-oR-not')));
        //$router->addRoute('checkDomainExist', new Zend_Controller_Router_Route('settings/user/ajax', array('module' => 'settings', 'controller' => 'user', 'action' => 'check-domain-exist')));

        $router->addRoute('settingsAttributeValueList', new Zend_Controller_Router_Route('settings/attribute-value/:attribute_id', array('module' => 'settings', 'controller' => 'attribute-value', 'action' => 'index')));
        $router->addRoute('settingsAttributeValueAdd', new Zend_Controller_Router_Route('settings/attribute-value/add/:attribute_id', array('module' => 'settings', 'controller' => 'attribute-value', 'action' => 'add')));
        $router->addRoute('settingsAttributeValueDelete', new Zend_Controller_Router_Route('settings/attribute-value/delete/:attribute_id/:id', array('module' => 'settings', 'controller' => 'attribute-value', 'action' => 'delete')));
        $router->addRoute('settingsAttributeValueDeleteBtn', new Zend_Controller_Router_Route('settings/attribute-value/delete/:attribute_id', array('module' => 'settings', 'controller' => 'attribute-value', 'action' => 'delete')));
        $router->addRoute('settingsAttributeValueEdit', new Zend_Controller_Router_Route('settings/attribute-value/edit/:id', array('module' => 'settings', 'controller' => 'attribute-value', 'action' => 'edit')));

        $router->addRoute('settingsAttributeValueGetImages', new Zend_Controller_Router_Route('settings/attribute-value/get-images/:attribute_value_id', array('module' => 'settings', 'controller' => 'attribute-value', 'action' => 'get-images')));



        $router->addRoute('settingsServiceAttributeList', new Zend_Controller_Router_Route('settings/service-attribute/:service_id', array('module' => 'settings', 'controller' => 'service-attribute', 'action' => 'index')));
        $router->addRoute('settingsServiceAttributeAdd', new Zend_Controller_Router_Route('settings/service-attribute/add/:service_id', array('module' => 'settings', 'controller' => 'service-attribute', 'action' => 'add')));
        $router->addRoute('settingsServiceAttributeDelete', new Zend_Controller_Router_Route('settings/service-attribute/delete/:service_id/:id', array('module' => 'settings', 'controller' => 'service-attribute', 'action' => 'delete')));
        $router->addRoute('settingsServiceAttributeEdit', new Zend_Controller_Router_Route('settings/service-attribute/edit/:service_id/:id', array('module' => 'settings', 'controller' => 'service-attribute', 'action' => 'edit')));
        $router->addRoute('settingsServiceAttributeDeleteBtn', new Zend_Controller_Router_Route('settings/service-attribute/delete/:service_id', array('module' => 'settings', 'controller' => 'service-attribute', 'action' => 'delete')));
        //by salim 

        $router->addRoute('settingsDiscountRangeList', new Zend_Controller_Router_Route('settings/service-discount/:service_id', array('module' => 'settings', 'controller' => 'service-discount', 'action' => 'index')));
        $router->addRoute('settingsDiscountRangeAdd', new Zend_Controller_Router_Route('settings/service-discount/add/:service_id', array('module' => 'settings', 'controller' => 'service-discount', 'action' => 'add')));
        $router->addRoute('settingsDiscountRangeDelete', new Zend_Controller_Router_Route('settings/service-discount/delete/:service_id/:id', array('module' => 'settings', 'controller' => 'service-discount', 'action' => 'delete')));
        $router->addRoute('settingsDiscountRangeEdit', new Zend_Controller_Router_Route('settings/service-discount/edit/:service_id/:id', array('module' => 'settings', 'controller' => 'service-discount', 'action' => 'edit')));
        $router->addRoute('settingsDiscountRangeDeleteBtn', new Zend_Controller_Router_Route('settings/service-discount/delete/:service_id', array('module' => 'settings', 'controller' => 'service-discount', 'action' => 'delete')));

        $router->addRoute('settingsServiceAttributeDiscountList', new Zend_Controller_Router_Route('settings/service-attribute-discount/:service_id/:attribute_id', array('module' => 'settings', 'controller' => 'service-attribute-discount', 'action' => 'index')));
        $router->addRoute('settingsServiceAttributeDiscountAdd', new Zend_Controller_Router_Route('settings/service-attribute-discount/add/:service_id/:attribute_id', array('module' => 'settings', 'controller' => 'service-attribute-discount', 'action' => 'add')));
        $router->addRoute('settingsServiceAttributeDiscountDelete', new Zend_Controller_Router_Route('settings/service-attribute-discount/delete/:service_id/:attribute_id/:id', array('module' => 'settings', 'controller' => 'service-attribute-discount', 'action' => 'delete')));
        $router->addRoute('settingsServiceAttributeDiscountEdit', new Zend_Controller_Router_Route('settings/service-attribute-discount/edit/:service_id/:attribute_id/:id', array('module' => 'settings', 'controller' => 'service-attribute-discount', 'action' => 'edit')));
        $router->addRoute('settingsServiceAttributeDiscountDeleteBtn', new Zend_Controller_Router_Route('settings/service-attribute-discount/delete/:service_id/:attribute_id', array('module' => 'settings', 'controller' => 'service-attribute-discount', 'action' => 'delete')));



        $router->addRoute('getDefaultValue', new Zend_Controller_Router_Route('settings/get-default-value', array('module' => 'settings', 'controller' => 'service-attribute', 'action' => 'get-default-value')));
        $router->addRoute('settingsContractorServiceList', new Zend_Controller_Router_Route('settings/contractor-service/:contractor_id', array('module' => 'settings', 'controller' => 'contractor-service', 'action' => 'index')));
        $router->addRoute('settingsContractorServiceAdd', new Zend_Controller_Router_Route('settings/contractor-service/add/:contractor_id', array('module' => 'settings', 'controller' => 'contractor-service', 'action' => 'add')));
        $router->addRoute('settingsContractorServiceDelete', new Zend_Controller_Router_Route('settings/contractor-service/delete/:contractor_id/:id', array('module' => 'settings', 'controller' => 'contractor-service', 'action' => 'delete')));
        $router->addRoute('settingsContractorServiceDeleteBtn', new Zend_Controller_Router_Route('settings/contractor-service/delete/:contractor_id', array('module' => 'settings', 'controller' => 'contractor-service', 'action' => 'delete')));

        $router->addRoute('settingsContractorOwnerList', new Zend_Controller_Router_Route('settings/contractor-owner/:contractor_info_id', array('module' => 'settings', 'controller' => 'contractor-owner', 'action' => 'index')));
        $router->addRoute('settingsContractorOwnerAdd', new Zend_Controller_Router_Route('settings/contractor-owner/add/:contractor_info_id', array('module' => 'settings', 'controller' => 'contractor-owner', 'action' => 'add')));
        $router->addRoute('settingsContractorOwnerDelete', new Zend_Controller_Router_Route('settings/contractor-owner/delete/:contractor_info_id/:id', array('module' => 'settings', 'controller' => 'contractor-owner', 'action' => 'delete')));
        $router->addRoute('settingsContractorOwnerDeleteBtn', new Zend_Controller_Router_Route('settings/contractor-owner/delete/:contractor_info_id', array('module' => 'settings', 'controller' => 'contractor-owner', 'action' => 'delete')));
        $router->addRoute('settingsContractorOwnerEdit', new Zend_Controller_Router_Route('settings/contractor-owner/edit/:id', array('module' => 'settings', 'controller' => 'contractor-owner', 'action' => 'edit')));
        $router->addRoute('settingsContractorOwnerPhotoUpload', new Zend_Controller_Router_Route('settings/contractor-owner/photo_upload/:id', array('module' => 'settings', 'controller' => 'contractor-owner', 'action' => 'photo-upload')));
        $router->addRoute('settingsContractorOwnerview', new Zend_Controller_Router_Route('settings/contractor-owner/view/:id', array('module' => 'settings', 'controller' => 'contractor-owner', 'action' => 'view')));

        $router->addRoute('settingsContractorEmployeeList', new Zend_Controller_Router_Route('settings/contractor-employee/:contractor_info_id', array('module' => 'settings', 'controller' => 'contractor-employee', 'action' => 'index')));
        $router->addRoute('settingsContractorEmployeeAdd', new Zend_Controller_Router_Route('settings/contractor-employee/add/:contractor_info_id', array('module' => 'settings', 'controller' => 'contractor-employee', 'action' => 'add')));
        $router->addRoute('settingsContractorEmployeeDelete', new Zend_Controller_Router_Route('settings/contractor-employee/delete/:contractor_info_id/:id', array('module' => 'settings', 'controller' => 'contractor-employee', 'action' => 'delete')));
        $router->addRoute('settingsContractorEmployeeDeleteBtn', new Zend_Controller_Router_Route('settings/contractor-employee/delete/:contractor_info_id', array('module' => 'settings', 'controller' => 'contractor-employee', 'action' => 'delete')));
        $router->addRoute('settingsContractorEmployeeEdit', new Zend_Controller_Router_Route('settings/contractor-employee/edit/:id', array('module' => 'settings', 'controller' => 'contractor-employee', 'action' => 'edit')));
        $router->addRoute('settingsContractorEmployeePhotoUpload', new Zend_Controller_Router_Route('settings/contractor-employee/photo_upload/:id', array('module' => 'settings', 'controller' => 'contractor-employee', 'action' => 'photo-upload')));
        $router->addRoute('settingsContractorEmployeeview', new Zend_Controller_Router_Route('settings/contractor-employeer/view/:id', array('module' => 'settings', 'controller' => 'contractor-employee', 'action' => 'view')));

        $router->addRoute('settingsDeclarationOfChemicalsList', new Zend_Controller_Router_Route('settings/declaration-of-chemicals/:contractor_info_id', array('module' => 'settings', 'controller' => 'declaration-of-chemicals', 'action' => 'index')));
        $router->addRoute('settingsDeclarationOfChemicalsAdd', new Zend_Controller_Router_Route('settings/declaration-of-chemicals/add/:contractor_info_id', array('module' => 'settings', 'controller' => 'declaration-of-chemicals', 'action' => 'add')));
        $router->addRoute('settingsDeclarationOfChemicalsDelete', new Zend_Controller_Router_Route('settings/declaration-of-chemicals/delete/:contractor_info_id/:id', array('module' => 'settings', 'controller' => 'declaration-of-chemicals', 'action' => 'delete')));
        $router->addRoute('settingsDeclarationOfChemicalsDeleteBtn', new Zend_Controller_Router_Route('settings/declaration-of-chemicals/delete/:contractor_info_id', array('module' => 'settings', 'controller' => 'declaration-of-chemicals', 'action' => 'delete')));

        $router->addRoute('settingsDeclarationOfEquipmentList', new Zend_Controller_Router_Route('settings/declaration-of-equipment/:contractor_info_id', array('module' => 'settings', 'controller' => 'declaration-of-equipment', 'action' => 'index')));
        $router->addRoute('settingsDeclarationOfEquipmentAdd', new Zend_Controller_Router_Route('settings/declaration-of-equipment/add/:contractor_info_id', array('module' => 'settings', 'controller' => 'declaration-of-equipment', 'action' => 'add')));
        $router->addRoute('settingsDeclarationOfEquipmentDelete', new Zend_Controller_Router_Route('settings/declaration-of-equipment/delete/:contractor_info_id/:id', array('module' => 'settings', 'controller' => 'declaration-of-equipment', 'action' => 'delete')));
        $router->addRoute('settingsDeclarationOfEquipmentDeleteBtn', new Zend_Controller_Router_Route('settings/declaration-of-equipment/delete/:contractor_info_id', array('module' => 'settings', 'controller' => 'declaration-of-equipment', 'action' => 'delete')));
        $router->addRoute('settingsDeclarationOfEquipmentview', new Zend_Controller_Router_Route('settings/DeclarationOfEquipment/view/:id', array('module' => 'settings', 'controller' => 'declaration-of-equipment', 'action' => 'view')));
        $router->addRoute('settingsDeclarationOfEquipmentEdit', new Zend_Controller_Router_Route('settings/declaration-of-equipment/edit/:id', array('module' => 'settings', 'controller' => 'declaration-of-equipment', 'action' => 'edit')));



        $router->addRoute('settingsDeclarationOfOtherApparatusList', new Zend_Controller_Router_Route('settings/declaration-of-other-apparatus/:contractor_info_id', array('module' => 'settings', 'controller' => 'declaration-of-other-apparatus', 'action' => 'index')));
        $router->addRoute('settingsDeclarationOfOtherApparatusAdd', new Zend_Controller_Router_Route('settings/declaration-of-other-apparatus/add/:contractor_info_id', array('module' => 'settings', 'controller' => 'declaration-of-other-apparatus', 'action' => 'add')));
        $router->addRoute('settingsDeclarationOfOtherApparatusDelete', new Zend_Controller_Router_Route('settings/declaration-of-other-apparatus/delete/:contractor_info_id/:id', array('module' => 'settings', 'controller' => 'declaration-of-other-apparatus', 'action' => 'delete')));
        $router->addRoute('settingsDeclarationOfOtherApparatusDeleteBtn', new Zend_Controller_Router_Route('settings/declaration-of-other-apparatus/delete/:contractor_info_id', array('module' => 'settings', 'controller' => 'declaration-of-other-apparatus', 'action' => 'delete')));


        $router->addRoute('settingsContractorVehicleList', new Zend_Controller_Router_Route('settings/contractor-vehicle/:contractor_info_id', array('module' => 'settings', 'controller' => 'contractor-vehicle', 'action' => 'index')));
        $router->addRoute('settingsContractorVehicleAdd', new Zend_Controller_Router_Route('settings/contractor-vehicle/add/:contractor_info_id', array('module' => 'settings', 'controller' => 'contractor-vehicle', 'action' => 'add')));
        $router->addRoute('settingsContractorVehicleEdit', new Zend_Controller_Router_Route('settings/contractor-vehicle/edit/:contractor_info_id/:id', array('module' => 'settings', 'controller' => 'contractor-vehicle', 'action' => 'edit')));
        $router->addRoute('settingsContractorVehicleDelete', new Zend_Controller_Router_Route('settings/contractor-vehicle/delete/:contractor_info_id/:id', array('module' => 'settings', 'controller' => 'contractor-vehicle', 'action' => 'delete')));
        $router->addRoute('settingsContractorVehicleDeleteBtn', new Zend_Controller_Router_Route('settings/contractor-vehicle/delete/:contractor_info_id', array('module' => 'settings', 'controller' => 'contractor-vehicle', 'action' => 'delete')));

        $router->addRoute('settingsContractorInsuranceList', new Zend_Controller_Router_Route('settings/contractor-insurance/:contractor_info_id', array('module' => 'settings', 'controller' => 'contractor-insurance', 'action' => 'index')));
        $router->addRoute('settingsContractorInsuranceAdd', new Zend_Controller_Router_Route('settings/contractor-insurance/add/:contractor_info_id', array('module' => 'settings', 'controller' => 'contractor-insurance', 'action' => 'add')));
        $router->addRoute('settingsContractorInsuranceEdit', new Zend_Controller_Router_Route('settings/contractor-insurance/edit/:id', array('module' => 'settings', 'controller' => 'contractor-insurance', 'action' => 'edit')));
        $router->addRoute('settingsContractorInsuranceDelete', new Zend_Controller_Router_Route('settings/contractor-insurance/delete/:contractor_info_id/:id', array('module' => 'settings', 'controller' => 'contractor-insurance', 'action' => 'delete')));
        $router->addRoute('settingsContractorInsuranceGetImages', new Zend_Controller_Router_Route('settings/contractor-insurance/get-images/:contractor_insurance_id', array('module' => 'settings', 'controller' => 'contractor-insurance', 'action' => 'get-images')));
        $router->addRoute('settingsContractorInsuranceDeleteBtn', new Zend_Controller_Router_Route('settings/contractor-insurance/delete', array('module' => 'settings', 'controller' => 'contractor-insurance', 'action' => 'delete')));


        $router->addRoute('settingsContractorInfoList', new Zend_Controller_Router_Route('settings/contractor-info/:contractor_id', array('module' => 'settings', 'controller' => 'contractor-info', 'action' => 'index')));
        $router->addRoute('settingsContractorInfoAdd', new Zend_Controller_Router_Route('settings/contractor-info/add/:contractor_id', array('module' => 'settings', 'controller' => 'contractor-info', 'action' => 'add')));
        $router->addRoute('settingsContractorInfoDelete', new Zend_Controller_Router_Route('settings/contractor-info/delete/:contractor_id/:id', array('module' => 'settings', 'controller' => 'contractor-info', 'action' => 'delete')));
        $router->addRoute('settingsContractorInfoEdit', new Zend_Controller_Router_Route('settings/contractor-info/edit/:contractor_id/:id', array('module' => 'settings', 'controller' => 'contractor-info', 'action' => 'edit')));


        $router->addRoute('contractorDiscussion', new Zend_Controller_Router_Route('settings/contractor-discussion/:contractor_id', array('module' => 'settings', 'controller' => 'contractor-discussion', 'action' => 'index')));
        $router->addRoute('submitContractorDiscussion', new Zend_Controller_Router_Route('settings/contractor-discussion/submit-contractor-discussion/:contractor_id', array('module' => 'settings', 'controller' => 'contractor-discussion', 'action' => 'submit-contractor-discussion')));
        $router->addRoute('allContractorDiscussion', new Zend_Controller_Router_Route('settings/contractor-discussion/all-contractor-discussion/:contractor_id', array('module' => 'settings', 'controller' => 'contractor-discussion', 'action' => 'all-contractor-discussion')));

        //Eman
        $router->addRoute('activeOrInactive', new Zend_Controller_Router_Route('active-or-inactive', array('module' => 'settings', 'controller' => 'auth-role-credential', 'action' => 'active-or-inactive')));

        $router->addRoute('settingsAuthRoleCredentialList', new Zend_Controller_Router_Route('settings/auth-role-credential/:role_id', array('module' => 'settings', 'controller' => 'auth-role-credential', 'action' => 'index')));
        $router->addRoute('updateGeneralContractor', new Zend_Controller_Router_Route('settings/update-general-contractor', array('module' => 'settings', 'controller' => 'user', 'action' => 'update-general-contractor')));
        $router->addRoute('settingsContractorGmailAdd', new Zend_Controller_Router_Route('settings/contractor-info/add-gmail-accounts/:contractor_id', array('module' => 'settings', 'controller' => 'contractor-info', 'action' => 'add-gmail-accounts')));
        $router->addRoute('settingsContractorGmailEdit', new Zend_Controller_Router_Route('settings/contractor-info/edit-gmail-accounts/:contractor_id/:id', array('module' => 'settings', 'controller' => 'contractor-info', 'action' => 'edit-gmail-accounts')));

        //Gmail By Salim 
        $router->addRoute('gmailEmailsTabsView', new Zend_Controller_Router_Route('gmail/view/:type/:contractor_id', array('module' => 'gmail', 'controller' => 'index', 'action' => 'view')));
        $router->addRoute('inquiryAddPhoto', new Zend_Controller_Router_Route('inquiryaddPhoto/:id', array('module' => 'default', 'controller' => 'index', 'action' => 'add-Photo')));
        $router->addRoute('addPhoto', new Zend_Controller_Router_Route('addPhoto/:type/:id', array('module' => 'default', 'controller' => 'index', 'action' => 'add-Photo')));
        $router->addRoute('inquiryAddPhotoTradingName', new Zend_Controller_Router_Route('inquiry/view/:id/:trading_name', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'view')));
        $router->addRoute('bookingAddPhotoTradingName', new Zend_Controller_Router_Route('booking/view/:id/:trading_name', array('module' => 'booking', 'controller' => 'index', 'action' => 'view')));
        $router->addRoute('estimateAddPhotoTradingName', new Zend_Controller_Router_Route('estimate/view/:id/:trading_name', array('module' => 'estimates', 'controller' => 'index', 'action' => 'view')));

        //$router->addRoute('gmailEmails', new Zend_Controller_Router_Route('gmail/:contractor_id', array('module' => 'gmail', 'controller' => 'index', 'action' => 'index')));

        $router->addRoute('localEmails', new Zend_Controller_Router_Route('email/details/:id', array('module' => 'gmail', 'controller' => 'index', 'action' => 'local')));
        $router->addRoute('changeGmailStatus', new Zend_Controller_Router_Route('gmail/change_status', array('module' => 'gmail', 'controller' => 'index', 'action' => 'gmail-change-status')));

        //Added By Doaa
        $router->addRoute('tradingNamesById', new Zend_Controller_Router_Route('settings/trading-names-template-images', array('module' => 'settings', 'controller' => 'trading-names', 'action' => 'trading-names-template-images')));



        //$router->addRoute('gmailEmailsAjax1', new Zend_Controller_Router_Route('gmail/view/:contractor_id/:booking_num', array('module' => 'gmail', 'controller' => 'index', 'action' => 'index')));

        $router->addRoute('gmailEmailsAjax', new Zend_Controller_Router_Route('gmail/getEmails/:id/:booking_num/:type', array('module' => 'gmail', 'controller' => 'index', 'action' => 'get-emails-by-booking-num-and-id')));
        $router->addRoute('gmailInquiryEmailsAjax', new Zend_Controller_Router_Route('gmail/getEmails/:inquiry_num/:type', array('module' => 'gmail', 'controller' => 'index', 'action' => 'get-emails-by-booking-num-and-id')));
        //$router->addRoute('localEmails', new Zend_Controller_Router_Route('email/:contractor_id', array('module' => 'gmail', 'controller' => 'index', 'action' => 'local')));
        // D.A 20/05/2015 Local Emails
        $router->addRoute('localEmailsView', new Zend_Controller_Router_Route('local', array('module' => 'gmail', 'controller' => 'index', 'action' => 'local')));
        $router->addRoute('localEmailsViewMessage', new Zend_Controller_Router_Route('local/view/:id', array('module' => 'gmail', 'controller' => 'index', 'action' => 'local-message')));
        //D.A 24/08/2015 Gmail Emails
        $router->addRoute('gmailEmailsView', new Zend_Controller_Router_Route('gmail', array('module' => 'gmail', 'controller' => 'index', 'action' => 'index')));
        $router->addRoute('gmailEmailsViewMessage', new Zend_Controller_Router_Route('gmail/view/:id', array('module' => 'gmail', 'controller' => 'index', 'action' => 'view-gmail')));

        $router->addRoute('settingsUserInfoList', new Zend_Controller_Router_Route('settings/user-info/:user_id', array('module' => 'settings', 'controller' => 'user-info', 'action' => 'index')));
        $router->addRoute('settingsUserInfoAdd', new Zend_Controller_Router_Route('settings/user-info/add/:user_id', array('module' => 'settings', 'controller' => 'user-info', 'action' => 'add')));
        $router->addRoute('settingsUserInfoDelete', new Zend_Controller_Router_Route('settings/user-info/delete/:user_id/:id', array('module' => 'settings', 'controller' => 'user-info', 'action' => 'delete')));
        $router->addRoute('settingsUserInfoEdit', new Zend_Controller_Router_Route('settings/user-info/edit/:user_id/:id', array('module' => 'settings', 'controller' => 'user-info', 'action' => 'edit')));

        $router->addRoute('settingsContractorServiceAvailabilityList', new Zend_Controller_Router_Route('settings/contractor-service-availability/:contractor_service_id', array('module' => 'settings', 'controller' => 'contractor-service-availability', 'action' => 'index')));
        $router->addRoute('settingsContractorServiceAvailabilityAdd', new Zend_Controller_Router_Route('settings/contractor-service-availability/add/:contractor_service_id', array('module' => 'settings', 'controller' => 'contractor-service-availability', 'action' => 'add')));
        $router->addRoute('settingsContractorServiceAvailabilityDelete', new Zend_Controller_Router_Route('settings/contractor-service-availability/delete/:contractor_service_id/:id', array('module' => 'settings', 'controller' => 'contractor-service-availability', 'action' => 'delete')));
        $router->addRoute('settingsContractorServiceAvailabilityDeleteBtn', new Zend_Controller_Router_Route('settings/contractor-service-availability/delete/:contractor_service_id', array('module' => 'settings', 'controller' => 'contractor-service-availability', 'action' => 'delete')));
        $router->addRoute('contractors', new Zend_Controller_Router_Route('contractor', array('module' => 'contractor', 'controller' => 'index', 'action' => 'index')));
        $router->addRoute('invoiceByContractor', new Zend_Controller_Router_Route('invoice-by-contrator', array('module' => 'invoices', 'controller' => 'index', 'action' => 'get-invoices-by-contractor-name')));
        $router->addRoute('settingsInquiryTypeAttributeList', new Zend_Controller_Router_Route('settings/inquiry-type-attribute/:inquiry_type_id', array('module' => 'settings', 'controller' => 'inquiry-type-attribute', 'action' => 'index')));
        $router->addRoute('settingsInquiryTypeAttributeAdd', new Zend_Controller_Router_Route('settings/inquiry-type-attribute/add/:inquiry_type_id', array('module' => 'settings', 'controller' => 'inquiry-type-attribute', 'action' => 'add')));
        $router->addRoute('settingsInquiryTypeAttributeDelete', new Zend_Controller_Router_Route('settings/inquiry-type-attribute/delete/:inquiry_type_id/:id', array('module' => 'settings', 'controller' => 'inquiry-type-attribute', 'action' => 'delete')));
        $router->addRoute('settingsInquiryTypeAttributeDeleteBtn', new Zend_Controller_Router_Route('settings/inquiry-type-attribute/delete/:inquiry_type_id', array('module' => 'settings', 'controller' => 'inquiry-type-attribute', 'action' => 'delete')));

        $router->addRoute('settingsPaymentTypeList', new Zend_Controller_Router_Route('settings/payment-type', array('module' => 'settings', 'controller' => 'payment-type', 'action' => 'index')));
        $router->addRoute('settingsPaymentTypeAdd', new Zend_Controller_Router_Route('settings/payment-type/add', array('module' => 'settings', 'controller' => 'payment-type', 'action' => 'add')));
        $router->addRoute('settingsPaymentTypeEdit', new Zend_Controller_Router_Route('settings/payment-type/edit/:id', array('module' => 'settings', 'controller' => 'payment-type', 'action' => 'edit')));
        $router->addRoute('settingsPaymentTypeDelete', new Zend_Controller_Router_Route('settings/payment-type/delete/:id', array('module' => 'settings', 'controller' => 'payment-type', 'action' => 'delete')));
        $router->addRoute('settingsPaymentTypeDeleteBtn', new Zend_Controller_Router_Route('settings/payment-type/delete', array('module' => 'settings', 'controller' => 'payment-type', 'action' => 'delete')));

        $router->addRoute('settingsCustomerTypeList', new Zend_Controller_Router_Route('settings/customer-type', array('module' => 'settings', 'controller' => 'customer-type', 'action' => 'index')));
        $router->addRoute('settingsCustomerTypeAdd', new Zend_Controller_Router_Route('settings/customer-type/add', array('module' => 'settings', 'controller' => 'customer-type', 'action' => 'add')));
        $router->addRoute('settingsCustomerTypeEdit', new Zend_Controller_Router_Route('settings/customer-type/edit/:id', array('module' => 'settings', 'controller' => 'customer-type', 'action' => 'edit')));
        $router->addRoute('settingsCustomerTypeDelete', new Zend_Controller_Router_Route('settings/customer-type/delete/:id', array('module' => 'settings', 'controller' => 'customer-type', 'action' => 'delete')));
        $router->addRoute('settingsCustomerTypeDeleteBtn', new Zend_Controller_Router_Route('settings/customer-type/delete', array('module' => 'settings', 'controller' => 'customer-type', 'action' => 'delete')));
        $router->addRoute('settingsCustomerTypeChangeWorkOrder', new Zend_Controller_Router_Route('settings/customer-type/change-work-order/:id', array('module' => 'settings', 'controller' => 'customer-type', 'action' => 'change-work-order')));
        $router->addRoute('settingsCustomerTypeAsBookingAddress', new Zend_Controller_Router_Route('settings/customer-type/as-booking-address/:id', array('module' => 'settings', 'controller' => 'customer-type', 'action' => 'as-booking-address')));

        $router->addRoute('settingsLabelTypeList', new Zend_Controller_Router_Route('settings/label-type', array('module' => 'settings', 'controller' => 'label-type', 'action' => 'index')));
        $router->addRoute('settingsLabelTypeAdd', new Zend_Controller_Router_Route('settings/label-type/add', array('module' => 'settings', 'controller' => 'label-type', 'action' => 'add')));
        $router->addRoute('settingsLabelTypeEdit', new Zend_Controller_Router_Route('settings/label-type/edit/:id', array('module' => 'settings', 'controller' => 'label-type', 'action' => 'edit')));
        $router->addRoute('settingsLabelTypeDelete', new Zend_Controller_Router_Route('settings/label-type/delete/:id', array('module' => 'settings', 'controller' => 'label-type', 'action' => 'delete')));
        $router->addRoute('settingsLabelTypeDeleteBtn', new Zend_Controller_Router_Route('settings/label-type/delete', array('module' => 'settings', 'controller' => 'label-type', 'action' => 'delete')));

        $router->addRoute('settingsCustomerContactLabelList', new Zend_Controller_Router_Route('settings/customer_contact_label', array('module' => 'settings', 'controller' => 'customer-contact-label', 'action' => 'index')));
        $router->addRoute('settingsCustomerContactLabelAdd', new Zend_Controller_Router_Route('settings/customer_contact_label/add', array('module' => 'settings', 'controller' => 'customer-contact-label', 'action' => 'add')));
        $router->addRoute('settingsCustomerContactLabelEdit', new Zend_Controller_Router_Route('settings/customer_contact_label/edit/:id', array('module' => 'settings', 'controller' => 'customer-contact-label', 'action' => 'edit')));
        $router->addRoute('settingsCustomerContactLabelDelete', new Zend_Controller_Router_Route('settings/customer_contact_label/delete/:id', array('module' => 'settings', 'controller' => 'customer-contact-label', 'action' => 'delete')));
        $router->addRoute('settingsCustomerContactLabelDeleteBtn', new Zend_Controller_Router_Route('settings/customer_contact_label/delete', array('module' => 'settings', 'controller' => 'customer-contact-label', 'action' => 'delete')));

        $router->addRoute('settingsDueDateList', new Zend_Controller_Router_Route('settings/due-date', array('module' => 'settings', 'controller' => 'due-date', 'action' => 'index')));
        $router->addRoute('settingsDueDateAdd', new Zend_Controller_Router_Route('settings/due-date/add', array('module' => 'settings', 'controller' => 'due-date', 'action' => 'add')));
        $router->addRoute('settingsDueDateEdit', new Zend_Controller_Router_Route('settings/due-date/edit/:id', array('module' => 'settings', 'controller' => 'due-date', 'action' => 'edit')));
        $router->addRoute('settingsDueDateDelete', new Zend_Controller_Router_Route('settings/due-date/delete/:id', array('module' => 'settings', 'controller' => 'due-date', 'action' => 'delete')));
        $router->addRoute('settingsDueDateDeleteBtn', new Zend_Controller_Router_Route('settings/due-date/delete', array('module' => 'settings', 'controller' => 'due-date', 'action' => 'delete')));
        $router->addRoute('settingsDueDateAsDefault', new Zend_Controller_Router_Route('settings/due-date/set-as-default/:id', array('module' => 'settings', 'controller' => 'due-date', 'action' => ' set-as-default')));
        $router->addRoute('settingsCallOutFee', new Zend_Controller_Router_Route('settings/call-out-fee', array('module' => 'settings', 'controller' => 'call-out-fee', 'action' => 'index')));

        $router->addRoute('settingsProductList', new Zend_Controller_Router_Route('settings/product', array('module' => 'settings', 'controller' => 'product', 'action' => 'index')));
        $router->addRoute('settingsProductAdd', new Zend_Controller_Router_Route('settings/product/add', array('module' => 'settings', 'controller' => 'product', 'action' => 'add')));
        $router->addRoute('settingsProductEdit', new Zend_Controller_Router_Route('settings/product/edit/:id', array('module' => 'settings', 'controller' => 'product', 'action' => 'edit')));
        $router->addRoute('settingsProductDelete', new Zend_Controller_Router_Route('settings/product/delete/:id', array('module' => 'settings', 'controller' => 'product', 'action' => 'delete')));
        $router->addRoute('settingsProductDeleteBtn', new Zend_Controller_Router_Route('settings/product/delete', array('module' => 'settings', 'controller' => 'product', 'action' => 'delete')));

        $router->addRoute('settingsEmailTemplateList', new Zend_Controller_Router_Route('settings/email-template', array('module' => 'settings', 'controller' => 'email-template', 'action' => 'index')));
        $router->addRoute('settingsEmailTemplateAdd', new Zend_Controller_Router_Route('settings/email-template/add', array('module' => 'settings', 'controller' => 'email-template', 'action' => 'add')));
        $router->addRoute('settingsEmailTemplateEdit', new Zend_Controller_Router_Route('settings/email-template/edit/:id', array('module' => 'settings', 'controller' => 'email-template', 'action' => 'edit')));
        $router->addRoute('settingsEmailTemplateDelete', new Zend_Controller_Router_Route('settings/email-template/delete/:id', array('module' => 'settings', 'controller' => 'email-template', 'action' => 'delete')));
        $router->addRoute('settingsEmailTemplateDeleteBtn', new Zend_Controller_Router_Route('settings/email-template/delete', array('module' => 'settings', 'controller' => 'email-template', 'action' => 'delete')));

        $router->addRoute('settingsCannedResponsesList', new Zend_Controller_Router_Route('settings/canned-responses', array('module' => 'settings', 'controller' => 'canned-responses', 'action' => 'index')));
        $router->addRoute('settingsCannedResponsesAdd', new Zend_Controller_Router_Route('settings/canned-responses/add', array('module' => 'settings', 'controller' => 'canned-responses', 'action' => 'add')));
        $router->addRoute('settingsCannedResponsesEdit', new Zend_Controller_Router_Route('settings/canned-responses/edit/:id', array('module' => 'settings', 'controller' => 'canned-responses', 'action' => 'edit')));
        $router->addRoute('settingsCannedResponsesDelete', new Zend_Controller_Router_Route('settings/canned-responses/delete/:id', array('module' => 'settings', 'controller' => 'canned-responses', 'action' => 'delete')));
        $router->addRoute('settingsCannedResponsesDeleteBtn', new Zend_Controller_Router_Route('settings/canned-responses/delete', array('module' => 'settings', 'controller' => 'canned-responses', 'action' => 'delete')));

        $router->addRoute('settingsCronJobList', new Zend_Controller_Router_Route('settings/cron-job', array('module' => 'settings', 'controller' => 'cron-job', 'action' => 'index')));
        $router->addRoute('settingsCronJobAdd', new Zend_Controller_Router_Route('settings/cron-job/add', array('module' => 'settings', 'controller' => 'cron-job', 'action' => 'add')));
        $router->addRoute('settingsCronJobEdit', new Zend_Controller_Router_Route('settings/cron-job/edit/:id', array('module' => 'settings', 'controller' => 'cron-job', 'action' => 'edit')));
        $router->addRoute('settingsCronJobDelete', new Zend_Controller_Router_Route('settings/cron-job/delete/:id', array('module' => 'settings', 'controller' => 'cron-job', 'action' => 'delete')));
        $router->addRoute('settingsCronJobDeleteBtn', new Zend_Controller_Router_Route('settings/cron-job/delete', array('module' => 'settings', 'controller' => 'cron-job', 'action' => 'delete')));
        $router->addRoute('settingsCronjobAddDescription', new Zend_Controller_Router_Route('settings/cron-job/add-desription/:id', array('module' => 'settings', 'controller' => 'cron-job', 'action' => 'add-description')));
        $router->addRoute('settingsCronJobView', new Zend_Controller_Router_Route('settings/cron-job/view/:id', array('module' => 'settings', 'controller' => 'cron-job', 'action' => 'view')));
        $router->addRoute('settingsCronJobRunNow', new Zend_Controller_Router_Route('settings/cron-job/:id', array('module' => 'settings', 'controller' => 'cron-job', 'action' => 'run-now')));

        //
        //customer
        //
        $router->addRoute('customerList', new Zend_Controller_Router_Route('customers', array('module' => 'customer', 'controller' => 'index', 'action' => 'index')));
        $router->addRoute('customerAdd', new Zend_Controller_Router_Route('customer/add', array('module' => 'customer', 'controller' => 'index', 'action' => 'add')));
        $router->addRoute('customerEdit', new Zend_Controller_Router_Route('customer/edit/:id', array('module' => 'customer', 'controller' => 'index', 'action' => 'edit')));
        $router->addRoute('customerEdit1', new Zend_Controller_Router_Route('customer/edit', array('module' => 'customer', 'controller' => 'index', 'action' => 'edit')));
        $router->addRoute('customerDelete', new Zend_Controller_Router_Route('customer/delete/:id', array('module' => 'customer', 'controller' => 'index', 'action' => 'delete')));
        $router->addRoute('customerUndelete', new Zend_Controller_Router_Route('customer/undelete/:id', array('module' => 'customer', 'controller' => 'index', 'action' => 'undelete')));
        $router->addRoute('customerDeleteBtn', new Zend_Controller_Router_Route('customer/delete', array('module' => 'customer', 'controller' => 'index', 'action' => 'delete')));
        $router->addRoute('customerView', new Zend_Controller_Router_Route('customer/view/:id', array('module' => 'customer', 'controller' => 'index', 'action' => 'view')));
        $router->addRoute('customerLocation', new Zend_Controller_Router_Route('customer/location/:id', array('module' => 'customer', 'controller' => 'index', 'action' => 'location')));
        $router->addRoute('bookingLocation', new Zend_Controller_Router_Route('booking/location/:id', array('module' => 'customer', 'controller' => 'index', 'action' => 'booking-location')));

        //$router->addRoute('showDeletedCustomer', new Zend_Controller_Router_Route('customer/deleted', array('module' => 'customer', 'controller' => 'index', 'action' => 'show-deleted')));

        $router->addRoute('showDeletedCustomer', new Zend_Controller_Router_Route('customers/:is_deleted', array('module' => 'customer', 'controller' => 'index', 'action' => 'index')));

        //$router->addRoute('showDeletedInquiry', new Zend_Controller_Router_Route('inquiry/deleted', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'show-deleted')));

        $router->addRoute('showDeletedInquiry', new Zend_Controller_Router_Route('inquiry/:is_deleted', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'index')));

        $router->addRoute('customerDeleteForever', new Zend_Controller_Router_Route('customer/delete-forever/:id', array('module' => 'customer', 'controller' => 'index', 'action' => 'delete-forever')));
        $router->addRoute('customerDeleteForeverBtn', new Zend_Controller_Router_Route('customer/delete-forever', array('module' => 'customer', 'controller' => 'index', 'action' => 'delete-forever')));
        $router->addRoute('inquiryLocation', new Zend_Controller_Router_Route('inquiry/location/:id', array('module' => 'customer', 'controller' => 'index', 'action' => 'inquiry-location')));
        $router->addRoute('customerNotes', new Zend_Controller_Router_Route('customer/notes/:id', array('module' => 'customer', 'controller' => 'index', 'action' => 'customer-notes')));
        $router->addRoute('customerInfoAsJson', new Zend_Controller_Router_Route('customer/info_as_json', array('module' => 'customer', 'controller' => 'index', 'action' => 'customer-info-as-json')));
        $router->addRoute('findDuplicateCustomer', new Zend_Controller_Router_Route('customer/find_duplicate_customer', array('module' => 'customer', 'controller' => 'duplicate-customer', 'action' => 'find-duplicate-customer')));
        $router->addRoute('fixDuplicateCustomer', new Zend_Controller_Router_Route('customer/fix_duplicate_customer/:id', array('module' => 'customer', 'controller' => 'duplicate-customer', 'action' => 'fix-duplicate-customer')));
        $router->addRoute('getCustomerContact', new Zend_Controller_Router_Route('customer/get_customer_contact', array('module' => 'customer', 'controller' => 'index', 'action' => 'get-customer-contact')));

        //By Walaa
        $router->addRoute('changeCommentSeenFlag', new Zend_Controller_Router_Route('settings/contractor-discussion/change-comment-seen-flag', array('module' => 'settings', 'controller' => 'contractor-discussion', 'action' => 'change-comment-seen-flag')));
        $router->addRoute('markAllCommentsAsSeen', new Zend_Controller_Router_Route('settings/contractor-discussion/mark-all-comments-as-seen', array('module' => 'settings', 'controller' => 'contractor-discussion', 'action' => 'mark-all-comments-as-seen')));
        $router->addRoute('updateDeletedComment', new Zend_Controller_Router_Route('settings/contractor-discussion/update-deleted-comment', array('module' => 'settings', 'controller' => 'contractor-discussion', 'action' => 'update-deleted-comment')));
        $router->addRoute('getUsers', new Zend_Controller_Router_Route('settings/contractor-discussion/get-users', array('module' => 'settings', 'controller' => 'contractor-discussion', 'action' => 'get-users')));
        $router->addRoute('getCustomers', new Zend_Controller_Router_Route('settings/contractor-discussion/get-customers', array('module' => 'settings', 'controller' => 'contractor-discussion', 'action' => 'get-customers')));

        //By Walaa 12-6
        $router->addRoute('deleteItemComment', new Zend_Controller_Router_Route('booking/discussion/deleteItemComment/:flag', array('module' => 'booking', 'controller' => 'discussion', 'action' => 'delete-item-comment')));
        $router->addRoute('itemDiscussion', new Zend_Controller_Router_Route('booking/discussion/:id/:process/:type', array('module' => 'booking', 'controller' => 'discussion', 'action' => 'index')));
        $router->addRoute('submitDiscussion', new Zend_Controller_Router_Route('booking/discussion/send/:id/:process/:type', array('module' => 'booking', 'controller' => 'discussion', 'action' => 'submit')));
        $router->addRoute('allDiscussion', new Zend_Controller_Router_Route('booking/discussion/all/:id/:type', array('module' => 'booking', 'controller' => 'discussion', 'action' => 'get-all-discussion')));
        $router->addRoute('markAllCommentsOfSpecificTypeAsSeen', new Zend_Controller_Router_Route('booking/discussion/markAllCommentsAsSeen/:id', array('module' => 'booking', 'controller' => 'discussion', 'action' => 'mark-all-comments-as-seen')));

        $router->addRoute('markTabsCommentsAsSeen', new Zend_Controller_Router_Route('booking/discussion/markTabsCommentsAsSeen/:id', array('module' => 'booking', 'controller' => 'discussion', 'action' => 'mark-tabs-comments-as-seen')));
        $router->addRoute('getBookingCustomers', new Zend_Controller_Router_Route('booking/discussion/getBookingCustomers/:id/:type', array('module' => 'booking', 'controller' => 'discussion', 'action' => 'get-related-customers')));
        $router->addRoute('changeItemCommentSeenFlag', new Zend_Controller_Router_Route('booking/discussion/change-item-comment-seen-flag/:id', array('module' => 'booking', 'controller' => 'discussion', 'action' => 'change-comment-seen-flag')));
        $router->addRoute('allItemImageDiscussion', new Zend_Controller_Router_Route('item/discussion/all/:type', array('module' => 'booking', 'controller' => 'discussion', 'action' => 'get-all-image-discussion')));

        // By Yassmeen 8/8/2016 Recurring Booking
        $router->addRoute('recurring', new Zend_Controller_Router_Route('recurringbooking/:booking_id', array('module' => 'calendar', 'controller' => 'index', 'action' => 'add-recurring')));

        $router->addRoute('Login', new Zend_Controller_Router_Route('login', array('module' => 'default', 'controller' => 'index', 'action' => 'index')));
        $router->addRoute('Logout', new Zend_Controller_Router_Route('logout', array('module' => 'default', 'controller' => 'index', 'action' => 'logout')));
        $router->addRoute('myAccount', new Zend_Controller_Router_Route('myAccount', array('module' => 'default', 'controller' => 'index', 'action' => 'my-account')));
        $router->addRoute('forgetPassword', new Zend_Controller_Router_Route('forget/password', array('module' => 'default', 'controller' => 'index', 'action' => 'forget-password')));
        $router->addRoute('forgetPasswordStep2', new Zend_Controller_Router_Route('reset/password/:code/:id', array('module' => 'default', 'controller' => 'index', 'action' => 'forget-password-step2')));
        $router->addRoute('changeEmail', new Zend_Controller_Router_Route('change/email', array('module' => 'default', 'controller' => 'index', 'action' => 'change-email')));
        $router->addRoute('changePassword', new Zend_Controller_Router_Route('change/password', array('module' => 'default', 'controller' => 'index', 'action' => 'change-password')));
        $router->addRoute('changeEmailStep2', new Zend_Controller_Router_Route('change/email/step2/:code/:id', array('module' => 'default', 'controller' => 'index', 'action' => 'change-email-step2')));
        $router->addRoute('changeAccountInfo', new Zend_Controller_Router_Route('change/account', array('module' => 'default', 'controller' => 'index', 'action' => 'change-account-info')));
        $router->addRoute('modfyUserInfo', new Zend_Controller_Router_Route('myAccount/userInfo', array('module' => 'default', 'controller' => 'index', 'action' => 'modfy-user-info')));
        $router->addRoute('modfyContractorInfo', new Zend_Controller_Router_Route('myAccount/contractorInfo', array('module' => 'default', 'controller' => 'index', 'action' => 'modfy-contractor-info')));

        $router->addRoute('editInsurance', new Zend_Controller_Router_Route('myAccount/editInsurance', array('module' => 'default', 'controller' => 'index', 'action' => 'edit-insurance')));

        $router->addRoute('editLicence', new Zend_Controller_Router_Route('myAccount/editLicence', array('module' => 'default', 'controller' => 'index', 'action' => 'edit-licence')));

        $router->addRoute('editBank', new Zend_Controller_Router_Route('myAccount/editBank', array('module' => 'default', 'controller' => 'index', 'action' => 'edit-bank')));

        $router->addRoute('getAttacments', new Zend_Controller_Router_Route('myAccount/get-attacments/:contractor_info_id/:type', array('module' => 'default', 'controller' => 'index', 'action' => 'get-attacments')));

        $router->addRoute('dashboard', new Zend_Controller_Router_Route('dashboard', array('module' => 'default', 'controller' => 'dashboard', 'action' => 'index')));
        $router->addRoute('recentDiscussions', new Zend_Controller_Router_Route('dashboard/recentDiscussions', array('module' => 'default', 'controller' => 'dashboard', 'action' => 'recent-discussions')));
        $router->addRoute('recentHistory', new Zend_Controller_Router_Route('dashboard/recentHistory', array('module' => 'default', 'controller' => 'dashboard', 'action' => 'recent-history')));
        $router->addRoute('recentComplaints', new Zend_Controller_Router_Route('dashboard/recentComplaints', array('module' => 'default', 'controller' => 'dashboard', 'action' => 'recent-complaints')));
        $router->addRoute('acceptServices', new Zend_Controller_Router_Route('dashboard/acceptServices/:bookingId', array('module' => 'default', 'controller' => 'dashboard', 'action' => 'accept-services')));
        $router->addRoute('rejectServices', new Zend_Controller_Router_Route('dashboard/rejectServices/:bookingId', array('module' => 'default', 'controller' => 'dashboard', 'action' => 'reject-services')));
        $router->addRoute('acceptAdminServices', new Zend_Controller_Router_Route('dashboard/acceptAdminServices/:bookingId', array('module' => 'default', 'controller' => 'dashboard', 'action' => 'accept-admin-services')));
        /////By Islam reject services
        $router->addRoute('rejectAdminServices', new Zend_Controller_Router_Route('dashboard/rejectAdminServices/:bookingId', array('module' => 'default', 'controller' => 'dashboard', 'action' => 'reject-admin-services')));
        $router->addRoute('getCompanyContractorShare', new Zend_Controller_Router_Route('dashboard/get-company-contractor-share', array('module' => 'default', 'controller' => 'dashboard', 'action' => 'get-company-contractor-share')));


        $router->addRoute('resendAdminServices', new Zend_Controller_Router_Route('dashboard/resendAdminServices/:bookingId', array('module' => 'default', 'controller' => 'dashboard', 'action' => 'resend-admin-services')));
        $router->addRoute('recentByConvertStatus', new Zend_Controller_Router_Route('dashboard/recentByConvertStatus/:convert_status', array('module' => 'default', 'controller' => 'dashboard', 'action' => 'recent-by-convert-status')));
        $router->addRoute('recentByBookingStatus', new Zend_Controller_Router_Route('dashboard/recentByBookingStatus/:status_id', array('module' => 'default', 'controller' => 'dashboard', 'action' => 'recent-by-booking-status')));
        $router->addRoute('showStatusDiscussion', new Zend_Controller_Router_Route('dashboard/show-status-discussion/:id/:status_id', array('module' => 'default', 'controller' => 'dashboard', 'action' => 'show-status-discussion')));
        $router->addRoute('logUserActivity', new Zend_Controller_Router_Route('dashboard/log_user_activity', array('module' => 'default', 'controller' => 'dashboard', 'action' => 'log-user-activity')));
        $router->addRoute('recentEmails', new Zend_Controller_Router_Route('dashboard/recentEmails', array('module' => 'default', 'controller' => 'dashboard', 'action' => 'recent-emails')));
        $router->addRoute('recentCalls', new Zend_Controller_Router_Route('dashboard/recent-calls', array('module' => 'default', 'controller' => 'dashboard', 'action' => 'recent-calls')));

        $router->addRoute('customRecentEmails', new Zend_Controller_Router_Route('dashboard/customRecentEmails', array('module' => 'default', 'controller' => 'dashboard', 'action' => 'custom-recent-emails')));
        $router->addRoute('replayRecentEmail', new Zend_Controller_Router_Route('dashboard/replayRecentEmail/:id', array('module' => 'default', 'controller' => 'dashboard', 'action' => 'replay-recent-emails')));
        $router->addRoute('downloadEmailPdfFile', new Zend_Controller_Router_Route('dashboard/downloadEmailPdfFile/:id', array('module' => 'default', 'controller' => 'dashboard', 'action' => 'download-email-pdf-file')));

        $router->addRoute('unapproved', new Zend_Controller_Router_Route('approve-booking/unapproved', array('module' => 'default', 'controller' => 'approve-booking', 'action' => 'unapproved')));
        $router->addRoute('approvedService', new Zend_Controller_Router_Route('approve-booking/accept/service/:id', array('module' => 'default', 'controller' => 'approve-booking', 'action' => 'approved-service')));
        //$router->addRoute('ignoreAll', new Zend_Controller_Router_Route('approve-booking/ignore/all/:id', array('module' => 'default', 'controller' => 'approve-booking', 'action' => 'ignore-all')));
        $router->addRoute('ignoreAll', new Zend_Controller_Router_Route('approve-booking/ignore/all/:id/:status_id', array('module' => 'default', 'controller' => 'approve-booking', 'action' => 'ignore-all')));
        $router->addRoute('ignoreBookingAddress', new Zend_Controller_Router_Route('approve-booking/ignore/aooking-address/:id', array('module' => 'default', 'controller' => 'approve-booking', 'action' => 'ignore-booking-address')));
        $router->addRoute('ignoreTotalDiscount', new Zend_Controller_Router_Route('approve-booking/ignore/discount/:id', array('module' => 'default', 'controller' => 'approve-booking', 'action' => 'ignore-total-discount')));
        $router->addRoute('ignoreService', new Zend_Controller_Router_Route('approve-booking/ignore/service/:id/:service_id/:clone', array('module' => 'default', 'controller' => 'approve-booking', 'action' => 'ignore-service')));
        $router->addRoute('ignoreCallOutFee', new Zend_Controller_Router_Route('approve-booking/ignore/call_out_fee/:id', array('module' => 'default', 'controller' => 'approve-booking', 'action' => 'ignore-call-out-fee')));


        $router->addRoute('contractorServiceList', new Zend_Controller_Router_Route('myAccount/contractor-service/', array('module' => 'default', 'controller' => 'contractor-service', 'action' => 'index')));
        $router->addRoute('contractorServiceList2', new Zend_Controller_Router_Route('myAccount/contractor-service_2/', array('module' => 'default', 'controller' => 'contractor-service', 'action' => 'contractor-services')));
        $router->addRoute('contractorServiceAdd', new Zend_Controller_Router_Route('myAccount/contractor-service/add', array('module' => 'default', 'controller' => 'contractor-service', 'action' => 'add')));
        $router->addRoute('contractorServiceDelete', new Zend_Controller_Router_Route('myAccount/contractor-service/delete/:id', array('module' => 'default', 'controller' => 'contractor-service', 'action' => 'delete')));
        $router->addRoute('contractorServiceDeleteBtn', new Zend_Controller_Router_Route('myAccount/contractor-service/delete', array('module' => 'default', 'controller' => 'contractor-service', 'action' => 'delete')));

        $router->addRoute('contractorOwnerList', new Zend_Controller_Router_Route('myAccount/contractor-owner', array('module' => 'default', 'controller' => 'contractor-owner', 'action' => 'index')));
        $router->addRoute('contractorOwnerAdd', new Zend_Controller_Router_Route('myAccount/contractor-owner/add', array('module' => 'default', 'controller' => 'contractor-owner', 'action' => 'add')));
        $router->addRoute('contractorOwnerDelete', new Zend_Controller_Router_Route('/contractor-owner/delete/:id', array('module' => 'default', 'controller' => 'contractor-owner', 'action' => 'delete')));
        $router->addRoute('contractorOwnerDeleteBtn', new Zend_Controller_Router_Route('myAccount/contractor-owner/delete', array('module' => 'default', 'controller' => 'contractor-owner', 'action' => 'delete')));
        $router->addRoute('contractorOwnerEdit', new Zend_Controller_Router_Route('myAccount/contractor-owner/edit/:id', array('module' => 'default', 'controller' => 'contractor-owner', 'action' => 'edit')));
        $router->addRoute('contractorOwnerPhotoUpload', new Zend_Controller_Router_Route('myAccount/contractor-owner/photo_upload/:id', array('module' => 'default', 'controller' => 'contractor-owner', 'action' => 'photo-upload')));
        $router->addRoute('contractorOwnerview', new Zend_Controller_Router_Route('myAccount/contractor-owner/view/:id', array('module' => 'default', 'controller' => 'contractor-owner', 'action' => 'view')));

        $router->addRoute('contractorEmployeeList', new Zend_Controller_Router_Route('myAccount/contractor-employee', array('module' => 'default', 'controller' => 'contractor-employee', 'action' => 'index')));
        $router->addRoute('contractorEmployeeAdd', new Zend_Controller_Router_Route('myAccount/contractor-employee/add', array('module' => 'default', 'controller' => 'contractor-employee', 'action' => 'add')));
        $router->addRoute('contractorEmployeeDelete', new Zend_Controller_Router_Route('myAccount/contractor-employee/delete/:id', array('module' => 'default', 'controller' => 'contractor-employee', 'action' => 'delete')));
        $router->addRoute('contractorEmployeeDeleteBtn', new Zend_Controller_Router_Route('myAccount/contractor-employee/delete', array('module' => 'default', 'controller' => 'contractor-employee', 'action' => 'delete')));
        $router->addRoute('contractorEmployeePhotoUpload', new Zend_Controller_Router_Route('myAccount/contractor-employee/photo_upload/:id', array('module' => 'default', 'controller' => 'contractor-employee', 'action' => 'photo-upload')));
        $router->addRoute('contractorEmployeeEdit', new Zend_Controller_Router_Route('myAccount/contractor-employee/edit/:id', array('module' => 'default', 'controller' => 'contractor-employee', 'action' => 'edit')));
        $router->addRoute('contractorEmployeeview', new Zend_Controller_Router_Route('myAccount/contractor-employeer/view/:id', array('module' => 'default', 'controller' => 'contractor-employee', 'action' => 'view')));

        $router->addRoute('declarationOfChemicalsList', new Zend_Controller_Router_Route('myAccount/declaration-of-chemicals', array('module' => 'default', 'controller' => 'declaration-of-chemicals', 'action' => 'index')));
        $router->addRoute('declarationOfChemicalsAdd', new Zend_Controller_Router_Route('myAccount/declaration-of-chemicals/add', array('module' => 'default', 'controller' => 'declaration-of-chemicals', 'action' => 'add')));
        $router->addRoute('declarationOfChemicalsDelete', new Zend_Controller_Router_Route('myAccount/declaration-of-chemicals/delete/:id', array('module' => 'default', 'controller' => 'declaration-of-chemicals', 'action' => 'delete')));
        $router->addRoute('declarationOfChemicalsDeleteBtn', new Zend_Controller_Router_Route('myAccount/declaration-of-chemicals/delete', array('module' => 'default', 'controller' => 'declaration-of-chemicals', 'action' => 'delete')));
        $router->addRoute('defaultDeclarationOfEquipmentview', new Zend_Controller_Router_Route('myAccount/DeclarationOfEquipment/view/:id', array('module' => 'default', 'controller' => 'declaration-of-equipment', 'action' => 'view')));


        $router->addRoute('declarationOfEquipmentList', new Zend_Controller_Router_Route('myAccount/declaration-of-equipment', array('module' => 'default', 'controller' => 'declaration-of-equipment', 'action' => 'index')));
        $router->addRoute('declarationOfEquipmentAdd', new Zend_Controller_Router_Route('myAccount/declaration-of-equipment/add', array('module' => 'default', 'controller' => 'declaration-of-equipment', 'action' => 'add')));
        $router->addRoute('declarationOfEquipmentDelete', new Zend_Controller_Router_Route('myAccount/declaration-of-equipment/delete/:id', array('module' => 'default', 'controller' => 'declaration-of-equipment', 'action' => 'delete')));
        $router->addRoute('declarationOfEquipmentDeleteBtn', new Zend_Controller_Router_Route('myAccount/declaration-of-equipment/delete', array('module' => 'default', 'controller' => 'declaration-of-equipment', 'action' => 'delete')));
        $router->addRoute('declarationOfEquipmentEdit', new Zend_Controller_Router_Route('myAccount/declaration-of-equipment/edit/:id', array('module' => 'default', 'controller' => 'declaration-of-equipment', 'action' => 'edit')));
        $router->addRoute('settingsdeclarationOfEquipmentGetImages', new Zend_Controller_Router_Route('settings/declaration-of-equipment/get-images/:id', array('module' => 'settings', 'controller' => 'declaration-of-equipment', 'action' => 'get-images')));



        $router->addRoute('declarationOfOtherApparatusList', new Zend_Controller_Router_Route('myAccount/declaration-of-other-apparatus', array('module' => 'default', 'controller' => 'declaration-of-other-apparatus', 'action' => 'index')));
        $router->addRoute('declarationOfOtherApparatusAdd', new Zend_Controller_Router_Route('myAccount/declaration-of-other-apparatus/add', array('module' => 'default', 'controller' => 'declaration-of-other-apparatus', 'action' => 'add')));
        $router->addRoute('declarationOfOtherApparatusDelete', new Zend_Controller_Router_Route('myAccount/declaration-of-other-apparatus/delete/:id', array('module' => 'default', 'controller' => 'declaration-of-other-apparatus', 'action' => 'delete')));
        $router->addRoute('declarationOfOtherApparatusDeleteBtn', new Zend_Controller_Router_Route('myAccount/declaration-of-other-apparatus/delete', array('module' => 'default', 'controller' => 'declaration-of-other-apparatus', 'action' => 'delete')));

        $router->addRoute('contractorVehicleList', new Zend_Controller_Router_Route('myAccount/contractor-vehicle', array('module' => 'default', 'controller' => 'contractor-vehicle', 'action' => 'index')));
        $router->addRoute('contractorVehicleAdd', new Zend_Controller_Router_Route('myAccount/contractor-vehicle/add', array('module' => 'default', 'controller' => 'contractor-vehicle', 'action' => 'add')));
        $router->addRoute('contractorVehicleDelete', new Zend_Controller_Router_Route('myAccount/contractor-vehicle/delete/:id', array('module' => 'default', 'controller' => 'contractor-vehicle', 'action' => 'delete')));
        $router->addRoute('contractorVehicleDeleteBtn', new Zend_Controller_Router_Route('myAccount/contractor-vehicle/delete', array('module' => 'default', 'controller' => 'contractor-vehicle', 'action' => 'delete')));


        $router->addRoute('ContractorInsuranceList', new Zend_Controller_Router_Route('myAccount/contractor-insurance', array('module' => 'default', 'controller' => 'contractor-insurance', 'action' => 'index')));
        $router->addRoute('ContractorInsuranceAdd', new Zend_Controller_Router_Route('myAccount/contractor-insurance/add', array('module' => 'default', 'controller' => 'contractor-insurance', 'action' => 'add')));
        $router->addRoute('ContractorInsuranceEdit', new Zend_Controller_Router_Route('myAccount/contractor-insurance/edit/:id', array('module' => 'default', 'controller' => 'contractor-insurance', 'action' => 'edit')));
        $router->addRoute('ContractorInsuranceDelete', new Zend_Controller_Router_Route('myAccount/contractor-insurance/delete/:id', array('module' => 'default', 'controller' => 'contractor-insurance', 'action' => 'delete')));
        $router->addRoute('ContractorInsuranceGetImages', new Zend_Controller_Router_Route('myAccount/contractor-insurance/get-images/:contractor_insurance_id', array('module' => 'default', 'controller' => 'contractor-insurance', 'action' => 'get-images')));
        $router->addRoute('ContractorInsuranceDeleteBtn', new Zend_Controller_Router_Route('myAccount/contractor-insurance/delete', array('module' => 'default', 'controller' => 'contractor-insurance', 'action' => 'delete')));


        $router->addRoute('calendar', new Zend_Controller_Router_Route('calendar', array('module' => 'calendar', 'controller' => 'index', 'action' => 'index')));
        $router->addRoute('addBooking', new Zend_Controller_Router_Route('booking-add', array('module' => 'calendar', 'controller' => 'index', 'action' => 'add')));
        $router->addRoute('editBooking', new Zend_Controller_Router_Route('booking-edit', array('module' => 'calendar', 'controller' => 'index', 'action' => 'edit')));
        $router->addRoute('manageCalendar', new Zend_Controller_Router_Route('calendar-manage', array('module' => 'calendar', 'controller' => 'index', 'action' => 'manage')));

        $router->addRoute('getAuthKey', new Zend_Controller_Router_Route('getAuthKey', array('module' => 'calendar', 'controller' => 'index', 'action' => 'get-auth-key')));


        $router->addRoute('dailyBookingAmount', new Zend_Controller_Router_Route('dashboard/booking-amount', array('module' => 'default', 'controller' => 'dashboard', 'action' => 'daily-booking-amount')));

        //
        //booking
        //
		//////////Islam
        $router->addRoute('paymentFileUpload', new Zend_Controller_Router_Route('reports/file-upload', array('module' => 'reports', 'controller' => 'attachment', 'action' => 'file-upload')));
        $router->addRoute('paymentsOfBooking', new Zend_Controller_Router_Route('reports/booking-payments/:booking_id', array('module' => 'reports', 'controller' => 'contractor-report', 'action' => 'booking-payments')));

        ////////////
        $router->addRoute('booking', new Zend_Controller_Router_Route('booking', array('module' => 'booking', 'controller' => 'index', 'action' => 'index')));
        $router->addRoute('isDeletedBooking', new Zend_Controller_Router_Route('booking/:is_deleted', array('module' => 'booking', 'controller' => 'index', 'action' => 'index')));

        $router->addRoute('unApprovedBooking', new Zend_Controller_Router_Route('unapproved-booking/:unapproved', array('module' => 'booking', 'controller' => 'index', 'action' => 'index')));
        $router->addRoute('awaitingUpdateBookings', new Zend_Controller_Router_Route('awaitingUpdate-booking/:awaitingUpdate', array('module' => 'booking', 'controller' => 'index', 'action' => 'index')));
        $router->addRoute('awaitingAcceptBookings', new Zend_Controller_Router_Route('awaitingAccept-booking/:awaitingAccept', array('module' => 'booking', 'controller' => 'index', 'action' => 'index')));

        $router->addRoute('bookingEdit', new Zend_Controller_Router_Route('booking/edit/:id', array('module' => 'booking', 'controller' => 'index', 'action' => 'edit')));

        $router->addRoute('pauseResumeBookingEmails', new Zend_Controller_Router_Route('pause-resume-booking-emails/:id/:process', array('module' => 'booking', 'controller' => 'index', 'action' => 'pause-resume-booking-emails')));
        $router->addRoute('pauseResumeBookingSms', new Zend_Controller_Router_Route('pause-resume-booking-sms/:id/:process', array('module' => 'booking', 'controller' => 'index', 'action' => 'pause-resume-booking-sms')));
        $router->addRoute('pauseResumeCustomerSms', new Zend_Controller_Router_Route('pause-resume-customer-sms/:id/:process', array('module' => 'customer', 'controller' => 'index', 'action' => 'pause-resume-customer-sms')));
        $router->addRoute('pauseResumeInquirySms', new Zend_Controller_Router_Route('pause-resume-inquiry-sms/:id/:process', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'pause-resume-inquiry-sms')));


        $router->addRoute('bookingView', new Zend_Controller_Router_Route('booking/view/:id', array('module' => 'booking', 'controller' => 'index', 'action' => 'view')));
        $router->addRoute('bookingHistory', new Zend_Controller_Router_Route('booking/history/:id', array('module' => 'booking', 'controller' => 'index', 'action' => 'history')));
        $router->addRoute('bookingDiscussion', new Zend_Controller_Router_Route('booking/discussion/:id', array('module' => 'booking', 'controller' => 'discussion', 'action' => 'index')));
        $router->addRoute('submitBookingDiscussion', new Zend_Controller_Router_Route('booking/discussion/send/:id', array('module' => 'booking', 'controller' => 'discussion', 'action' => 'submit')));
        $router->addRoute('allBookingDiscussion', new Zend_Controller_Router_Route('booking/discussion/all/:id', array('module' => 'booking', 'controller' => 'discussion', 'action' => 'get-all-discussion')));
        $router->addRoute('allBookingImageDiscussion', new Zend_Controller_Router_Route('booking/discussion/all/', array('module' => 'booking', 'controller' => 'discussion', 'action' => 'get-all-image-discussion')));
        $router->addRoute('bookingAdd', new Zend_Controller_Router_Route('booking/add', array('module' => 'booking', 'controller' => 'index', 'action' => 'add')));
        /*         * ***Start ***** */
        $router->addRoute('requestAccess', new Zend_Controller_Router_Route('booking/requestAccess/:id', array('module' => 'booking', 'controller' => 'index', 'action' => 'request-access')));
        $router->addRoute('acceptRequest', new Zend_Controller_Router_Route('booking/acceptRequest/:id/:name', array('module' => 'booking', 'controller' => 'index', 'action' => 'accept-request')));
        $router->addRoute('rejectRequest', new Zend_Controller_Router_Route('booking/rejectRequest/:id/:name', array('module' => 'booking', 'controller' => 'index', 'action' => 'reject-request')));
        $router->addRoute('toApproveEdit', new Zend_Controller_Router_Route('booking/toapproveEdit/:id', array('module' => 'booking', 'controller' => 'index', 'action' => 'to-approve-edit')));
        /*         * ****End**** */

        $router->addRoute('addBookingToCustomerId', new Zend_Controller_Router_Route('booking-add/:customer_id', array('module' => 'calendar', 'controller' => 'index', 'action' => 'add')));
        $router->addRoute('bookingAddToCustomerId', new Zend_Controller_Router_Route('booking/add/:customer_id', array('module' => 'booking', 'controller' => 'index', 'action' => 'add-booking-to-customer-id')));

        $router->addRoute('settingsContractorAvailability', new Zend_Controller_Router_Route('settings/contractor-availability/:contractor_id', array('module' => 'settings', 'controller' => 'contractor-service-availability', 'action' => 'index')));
        $router->addRoute('settingsContractorAvailabilityAdd', new Zend_Controller_Router_Route('settings/contractor-availability/add/:contractor_id', array('module' => 'settings', 'controller' => 'contractor-service-availability', 'action' => 'add')));

        $router->addRoute('bookingDelete', new Zend_Controller_Router_Route('booking/delete/:id', array('module' => 'booking', 'controller' => 'index', 'action' => 'delete')));
        $router->addRoute('bookingDeleteBtn', new Zend_Controller_Router_Route('booking/delete', array('module' => 'booking', 'controller' => 'index', 'action' => 'delete')));
        $router->addRoute('bookingUndelete', new Zend_Controller_Router_Route('booking/undelete/:id', array('module' => 'booking', 'controller' => 'index', 'action' => 'undelete')));
        $router->addRoute('sendDiscussion', new Zend_Controller_Router_Route('booking/discussion/send', array('module' => 'booking', 'controller' => 'discussion', 'action' => 'index')));
        $router->addRoute('bookingAddLabel', new Zend_Controller_Router_Route('booking/label/add-label/:id', array('module' => 'booking', 'controller' => 'label', 'action' => 'add-label')));
        $router->addRoute('bookingSelectLabel', new Zend_Controller_Router_Route('booking/label/select-label/:id', array('module' => 'booking', 'controller' => 'label', 'action' => 'select-label')));
        $router->addRoute('filterBookingLabel', new Zend_Controller_Router_Route('booking/label/filter-label', array('module' => 'booking', 'controller' => 'label', 'action' => 'filter-label')));
        $router->addRoute('bookingSearchLabel', new Zend_Controller_Router_Route('booking/label/search-label', array('module' => 'booking', 'controller' => 'label', 'action' => 'search-label')));
        $router->addRoute('bookingAttachment', new Zend_Controller_Router_Route('booking/attachment/:id', array('module' => 'booking', 'controller' => 'attachment', 'action' => 'index')));
        $router->addRoute('bookingFileUpload', new Zend_Controller_Router_Route('booking/file_upload/:booking_id', array('module' => 'booking', 'controller' => 'attachment', 'action' => 'file-upload')));
        $router->addRoute('bookingFileUploadDelete', new Zend_Controller_Router_Route('booking/file_upload/delete/:id', array('module' => 'booking', 'controller' => 'attachment', 'action' => 'delete')));
        $router->addRoute('bookingFileUploadDeleteBtn', new Zend_Controller_Router_Route('booking/file_upload/delete', array('module' => 'booking', 'controller' => 'attachment', 'action' => 'delete')));
        $router->addRoute('bookingFileDownload', new Zend_Controller_Router_Route('booking/file_download/:id', array('module' => 'booking', 'controller' => 'attachment', 'action' => 'download')));
        $router->addRoute('bookingFileEditWorkOrder', new Zend_Controller_Router_Route('booking/file_upload/edit_work_order/:id/:work_order', array('module' => 'booking', 'controller' => 'attachment', 'action' => 'edit-work-order')));
        $router->addRoute('bookingFileEditDescription', new Zend_Controller_Router_Route('booking/file_upload/edit_desription/:id', array('module' => 'booking', 'controller' => 'attachment', 'action' => 'edit-description')));
        $router->addRoute('bookingFileViewDescription', new Zend_Controller_Router_Route('booking/file_upload/view_desription/:id', array('module' => 'booking', 'controller' => 'attachment', 'action' => 'view-description')));
        $router->addRoute('setContractorShare', new Zend_Controller_Router_Route('booking/contractor_share/:id', array('module' => 'booking', 'controller' => 'index', 'action' => 'set-contractor-share')));
        $router->addRoute('setContractorSharePerService', new Zend_Controller_Router_Route('booking/set-contractor-share-per-service/:id', array('module' => 'booking', 'controller' => 'index', 'action' => 'set-contractor-share-per-service')));
        $router->addRoute('setContractorServiceShareByContractorShare', new Zend_Controller_Router_Route('booking/set-contractor-service-share-by-contractor-share', array('module' => 'booking', 'controller' => 'index', 'action' => 'set-contractor-service-share-by-contractor-share')));

        $router->addRoute('feedbackList', new Zend_Controller_Router_Route('booking/feedback', array('module' => 'booking', 'controller' => 'feedback', 'action' => 'feedback-list')));
        $router->addRoute('feedbackView', new Zend_Controller_Router_Route('booking/feedback_view/:id', array('module' => 'booking', 'controller' => 'feedback', 'action' => 'feedback-view')));
        $router->addRoute('sendClaimOwner', new Zend_Controller_Router_Route('claim_owner/send/:id', array('module' => 'booking', 'controller' => 'claim-owner', 'action' => 'send-claim-owner')));
        $router->addRoute('claimOwner', new Zend_Controller_Router_Route('claim_owner', array('module' => 'booking', 'controller' => 'claim-owner', 'action' => 'index')));
        $router->addRoute('submitClaimOwner', new Zend_Controller_Router_Route('claim_owner/submit/:id', array('module' => 'booking', 'controller' => 'claim-owner', 'action' => 'submit-claim-owner')));
        $router->addRoute('contractorBookingLocation', new Zend_Controller_Router_Route('contractor_booking_location/:booking_id/:contractor_id', array('module' => 'booking', 'controller' => 'index', 'action' => 'contractor-booking-location')));
        $router->addRoute('modifyFollowDate', new Zend_Controller_Router_Route('/ajax/modifyFollowDate', array('module' => 'booking', 'controller' => 'index', 'action' => 'modify-follow-date')));
        //$router->addRoute('logBookingView', new Zend_Controller_Router_Route('log-booking/view/:id', array('module' => 'booking', 'controller' => 'log', 'action' => 'view')));
        $router->addRoute('logBookingView', new Zend_Controller_Router_Route('log-booking/view/:id', array('module' => 'booking', 'controller' => 'log-history', 'action' => 'view')));

        $router->addRoute('sendReminderTentativeBookingAsEmail', new Zend_Controller_Router_Route('email/reminder-tentative-booking/:id', array('module' => 'booking', 'controller' => 'index', 'action' => 'send-reminder-tentative-booking-as-email')));
        $router->addRoute('sendReminderOnHoldBookingAsEmail', new Zend_Controller_Router_Route('email/reminder-on-hold-booking/:id', array('module' => 'booking', 'controller' => 'index', 'action' => 'send-reminder-on-hold-booking-as-email')));
        $router->addRoute('sendBookingConfirmationAsEmail', new Zend_Controller_Router_Route('email/booking-confirmation', array('module' => 'booking', 'controller' => 'index', 'action' => 'send-booking-confirmation-as-email')));
        $router->addRoute('sendBookingAsEmail', new Zend_Controller_Router_Route('email/booking/:id', array('module' => 'booking', 'controller' => 'index', 'action' => 'send-booking-as-email')));
        $router->addRoute('sendBookingAsEmailToContractor', new Zend_Controller_Router_Route('email/booking_to_contractor/:id', array('module' => 'booking', 'controller' => 'index', 'action' => 'send-booking-as-email-to-contractor')));
        $router->addRoute('sendWorkOrderRequestEmail', new Zend_Controller_Router_Route('work_order_request_email/booking/:id', array('module' => 'booking', 'controller' => 'index', 'action' => 'send-work-order-request-email')));
        $router->addRoute('sendBookingAsSms', new Zend_Controller_Router_Route('sms/booking/:id', array('module' => 'booking', 'controller' => 'index', 'action' => 'send-booking-as-sms')));
        $router->addRoute('sendBookingAsSmsToContractor', new Zend_Controller_Router_Route('sms/booking/contractor/:id', array('module' => 'booking', 'controller' => 'index', 'action' => 'send-booking-as-sms-to-contractor')));
        $router->addRoute('bookingPreview', new Zend_Controller_Router_Route('booking/preview/:id', array('module' => 'booking', 'controller' => 'index', 'action' => 'preview')));
        $router->addRoute('bookingCustomerPreview', new Zend_Controller_Router_Route('booking/customer_preview/:id', array('module' => 'booking', 'controller' => 'index', 'action' => 'booking-customer-preview')));
        $router->addRoute('downloadBooking', new Zend_Controller_Router_Route('download/booking/:id', array('module' => 'booking', 'controller' => 'index', 'action' => 'download-booking')));
        $router->addRoute('downloadContractorBooking', new Zend_Controller_Router_Route('download/contractor_booking/:id', array('module' => 'booking', 'controller' => 'index', 'action' => 'download-contractor-booking')));
        $router->addRoute('bookingDeleteForever', new Zend_Controller_Router_Route('booking/delete-forever/:id', array('module' => 'booking', 'controller' => 'index', 'action' => 'delete-forever')));
        $router->addRoute('bookingDeleteForeverBtn', new Zend_Controller_Router_Route('booking/delete-forever', array('module' => 'booking', 'controller' => 'index', 'action' => 'delete-forever')));
        $router->addRoute('awaitingUpdateBooking', new Zend_Controller_Router_Route('booking/awaiting_update', array('module' => 'booking', 'controller' => 'index', 'action' => 'get-awaiting-update-booking')));
        $router->addRoute('bookingReminderAdd', new Zend_Controller_Router_Route('booking/reminder/add/:booking_id', array('module' => 'booking', 'controller' => 'reminder', 'action' => 'reminder-add')));
        $router->addRoute('bookingReminderView', new Zend_Controller_Router_Route('booking/reminder/view/:id', array('module' => 'booking', 'controller' => 'reminder', 'action' => 'reminder-view')));
        $router->addRoute('bookingReminderDelete', new Zend_Controller_Router_Route('booking/reminder/delete/:id/:reminder_id', array('module' => 'booking', 'controller' => 'reminder', 'action' => 'reminder-delete')));
        $router->addRoute('sendRequestBookingFeedback', new Zend_Controller_Router_Route('booking/sendRequestBookingFeedback/:id', array('module' => 'booking', 'controller' => 'index', 'action' => 'send-request-booking-feedback')));
        $router->addRoute('awaitingAcceptBooking', new Zend_Controller_Router_Route('booking/awaiting_accept', array('module' => 'booking', 'controller' => 'index', 'action' => 'get-awaiting-accept-booking')));

        $router->addRoute('getContractorsByStatus', new Zend_Controller_Router_Route('default/get-contractors-by-status', array('module' => 'default', 'controller' => 'common', 'action' => 'get-contractors-by-status')));

        $router->addRoute('getItemImages', new Zend_Controller_Router_Route('default/get-item-images/:item_id/:type', array('module' => 'default', 'controller' => 'common', 'action' => 'get-item-images')));

        $router->addRoute('getHistoryData', new Zend_Controller_Router_Route('default/get-history-data', array('module' => 'default', 'controller' => 'common', 'action' => 'get-history-data')));

        //D.A 29/09/2015 invoice Cache Clear
        $router->addRoute('invoiceCacheClear', new Zend_Controller_Router_Route('invoiceCacheClear/:id', array('module' => 'invoices', 'controller' => 'index', 'action' => 'invoice-cache-clear')));
        $router->addRoute('settingsDeleteInvoiceCache', new Zend_Controller_Router_Route('settings/cache/delete-invoice-cache', array('module' => 'settings', 'controller' => 'cache', 'action' => 'delete-invoice-cache')));

        $router->addRoute('bookingContactHistoryAdd', new Zend_Controller_Router_Route('booking/contact_history/add/:booking_id', array('module' => 'booking', 'controller' => 'contact-history', 'action' => 'contact-history-add')));
        $router->addRoute('bookingContactHistoryView', new Zend_Controller_Router_Route('booking/contact_history/view/:id', array('module' => 'booking', 'controller' => 'contact-history', 'action' => 'contact-history-view')));
        $router->addRoute('bookingContactHistoryDelete', new Zend_Controller_Router_Route('booking/contact_history/delete/:id/:contact_history_id', array('module' => 'booking', 'controller' => 'contact-history', 'action' => 'contact-history-delete')));
        $router->addRoute('sendReminderUpdateBookingToContractor', new Zend_Controller_Router_Route('email/reminder_booking_to_contractor/:id', array('module' => 'booking', 'controller' => 'index', 'action' => 'send-reminder-update-booking-to-contractor')));
        $router->addRoute('getEmailTemplate', new Zend_Controller_Router_Route('ajax/get_email_template', array('module' => 'booking', 'controller' => 'index', 'action' => 'get-email-template')));


        //
        //calendar service
        //
        $router->addRoute('getRelatedAddress', new Zend_Controller_Router_Route('ajax/getRelatedAddress', array('module' => 'default', 'controller' => 'common', 'action' => 'get-related-address')));

        $router->addRoute('getRelatedAddressByMaps', new Zend_Controller_Router_Route('ajax/getRelatedAddress', array('module' => 'default', 'controller' => 'common', 'action' => 'get-related-address')));
        $router->addRoute('getDateInSelectedFormat', new Zend_Controller_Router_Route('ajax/getDateInSelectedFormat', array('module' => 'default', 'controller' => 'common', 'action' => 'get-date-with-settings-format')));
        $router->addRoute('findAvailability', new Zend_Controller_Router_Route('ajax/findAvailability', array('module' => 'default', 'controller' => 'common', 'action' => 'find-availability')));
        $router->addRoute('testZendDatePicker', new Zend_Controller_Router_Route('ajax/testZendDatePicker', array('module' => 'default', 'controller' => 'common', 'action' => 'test')));
        $router->addRoute('setCompanySession', new Zend_Controller_Router_Route('ajax/setCompanySession', array('module' => 'default', 'controller' => 'common', 'action' => 'set-company-session')));
        $router->addRoute('saveAsXls', new Zend_Controller_Router_Route('ajax/saveAsXls', array('module' => 'default', 'controller' => 'common', 'action' => 'save-as-xls')));
        $router->addRoute('dropDownCity', new Zend_Controller_Router_Route('drop_down/city', array('module' => 'default', 'controller' => 'common', 'action' => 'cities-by-country-id')));
        $router->addRoute('getCitiesByState', new Zend_Controller_Router_Route('get_cities_by_state/state', array('module' => 'default', 'controller' => 'common', 'action' => 'get-cities-by-state')));

        $router->addRoute('dropDownState', new Zend_Controller_Router_Route('drop_down/state', array('module' => 'default', 'controller' => 'common', 'action' => 'get-by-state')));
        $router->addRoute('emailTemplatePreview', new Zend_Controller_Router_Route('ajax/get-email-template', array('module' => 'default', 'controller' => 'common', 'action' => 'get-email-template')));
        $router->addRoute('bookingAddressBlock', new Zend_Controller_Router_Route('common/booking-address', array('module' => 'default', 'controller' => 'common', 'action' => 'get-booking-address')));
        $router->addRoute('InquiryAddressBlock', new Zend_Controller_Router_Route('common/inquiry-address', array('module' => 'default', 'controller' => 'common', 'action' => 'get-inquiry-address')));
        $router->addRoute('servicesAvailable', new Zend_Controller_Router_Route('common/services_available', array('module' => 'default', 'controller' => 'common', 'action' => 'services-available-by-city-id')));
        $router->addRoute('searchServices', new Zend_Controller_Router_Route('common/search_services', array('module' => 'default', 'controller' => 'common', 'action' => 'search-services')));
        $router->addRoute('contractor', new Zend_Controller_Router_Route('common/contractor', array('module' => 'default', 'controller' => 'common', 'action' => 'contractor-available-by-service-id')));
        $router->addRoute('drowAttribute', new Zend_Controller_Router_Route('common/drow_attribute', array('module' => 'default', 'controller' => 'attribute', 'action' => 'drow-attribute')));

        $router->addRoute('getAttributeValues', new Zend_Controller_Router_Route('common/get-attribute-values', array('module' => 'default', 'controller' => 'attribute', 'action' => 'get-attribute-values')));

        $router->addRoute('countServicePrice', new Zend_Controller_Router_Route('common/count_service_price', array('module' => 'default', 'controller' => 'attribute', 'action' => 'count-service-price')));
        $router->addRoute('dropDownCustomerList', new Zend_Controller_Router_Route('common/customerList', array('module' => 'default', 'controller' => 'common', 'action' => 'customer-list')));
        $router->addRoute('customerSearch', new Zend_Controller_Router_Route('common/customerSearch', array('module' => 'default', 'controller' => 'common', 'action' => 'customer-search')));
        $router->addRoute('checkBookingStatus', new Zend_Controller_Router_Route('common/checkBookingStatus', array('module' => 'default', 'controller' => 'common', 'action' => 'check-booking-status')));
        $router->addRoute('setMessage', new Zend_Controller_Router_Route('common/setMessage', array('module' => 'default', 'controller' => 'common', 'action' => 'set-message')));
        $router->addRoute('inquiryTypeAttributes', new Zend_Controller_Router_Route('common/inquiry_type_attributes', array('module' => 'default', 'controller' => 'common', 'action' => 'inquiry-type-attributes')));
        $router->addRoute('fillCannedResponse', new Zend_Controller_Router_Route('ajax/fill_canned_response', array('module' => 'default', 'controller' => 'common', 'action' => 'fill-canned-response')));
        $router->addRoute('customerLike', new Zend_Controller_Router_Route('common/customerLike', array('module' => 'default', 'controller' => 'common', 'action' => 'customer-like')));
        $router->addRoute('search', new Zend_Controller_Router_Route('search', array('module' => 'default', 'controller' => 'search', 'action' => 'index')));
        $router->addRoute('setDashboardStatusSession', new Zend_Controller_Router_Route('ajax/setDashboardStatusSession', array('module' => 'default', 'controller' => 'common', 'action' => 'set-dashboard-status-session')));
        $router->addRoute('autoCompleteCustomer', new Zend_Controller_Router_Route('ajax/autoCompleteCustomer', array('module' => 'default', 'controller' => 'common', 'action' => 'auto-complete-customer')));
        $router->addRoute('cronjobHistory', new Zend_Controller_Router_Route('dashboard/cronjobHistory', array('module' => 'default', 'controller' => 'dashboard', 'action' => 'cronjob-history')));
        $router->addRoute('missedCallAdd', new Zend_Controller_Router_Route('missedCallAdd', array('module' => 'default', 'controller' => 'missed-calls', 'action' => 'add')));
        $router->addRoute('missedCall', new Zend_Controller_Router_Route('missedCall', array('module' => 'default', 'controller' => 'missed-calls', 'action' => 'index')));
        $router->addRoute('missedCallView', new Zend_Controller_Router_Route('missedCallView/:id', array('module' => 'default', 'controller' => 'missed-calls', 'action' => 'view')));
        $router->addRoute('missedCallSent', new Zend_Controller_Router_Route('missedCall/sent', array('module' => 'default', 'controller' => 'missed-calls', 'action' => 'missed-call-sent')));

        $router->addRoute('updateFullTextSearch', new Zend_Controller_Router_Route('ajax/update_full_text_search', array('module' => 'default', 'controller' => 'search', 'action' => 'update-full-text-search')));
        $router->addRoute('updateFullTextSearchByTypeAndTypeId', new Zend_Controller_Router_Route('ajax/update_full_text_search_by_type_and_type_id/:type/:type_id', array('module' => 'default', 'controller' => 'search', 'action' => 'update-full-text-search-by-type-and-type-id')));

        //
        //complaint
        //
        $router->addRoute('complaint', new Zend_Controller_Router_Route('complaint', array('module' => 'complaint', 'controller' => 'index', 'action' => 'index')));
        $router->addRoute('complaintAdd', new Zend_Controller_Router_Route('complaint/add/:booking_id', array('module' => 'complaint', 'controller' => 'index', 'action' => 'add')));

        $router->addRoute('complaintDelete', new Zend_Controller_Router_Route('complaint/delete/:id', array('module' => 'complaint', 'controller' => 'index', 'action' => 'delete')));
        $router->addRoute('complaintEdit', new Zend_Controller_Router_Route('complaint/edit/:id', array('module' => 'complaint', 'controller' => 'index', 'action' => 'edit')));
        $router->addRoute('complaintView', new Zend_Controller_Router_Route('complaint/view/:id', array('module' => 'complaint', 'controller' => 'index', 'action' => 'view')));
        $router->addRoute('complaintDeleteBtn', new Zend_Controller_Router_Route('complaint/delete', array('module' => 'complaint', 'controller' => 'index', 'action' => 'delete')));
        $router->addRoute('complaintReplay', new Zend_Controller_Router_Route('complaint/replay/:id', array('module' => 'complaint', 'controller' => 'index', 'action' => 'replay-complaint')));
        $router->addRoute('complaintReplayToContractor', new Zend_Controller_Router_Route('complaint/replay_to_contractor/:id', array('module' => 'complaint', 'controller' => 'index', 'action' => 'replay-complaint-to-contractor')));
        $router->addRoute('convertStatus', new Zend_Controller_Router_Route('complaint/convertStatus/:id', array('module' => 'complaint', 'controller' => 'index', 'action' => 'convert-status')));
        $router->addRoute('complaintDisscussion', new Zend_Controller_Router_Route('complaint/discussion/:id/:process', array('module' => 'complaint', 'controller' => 'discussion', 'action' => 'index')));
        $router->addRoute('submitComplaintDiscussion', new Zend_Controller_Router_Route('complaint/discussion/send/:id/:process', array('module' => 'complaint', 'controller' => 'discussion', 'action' => 'submit')));
        $router->addRoute('allComplaintImageDiscussion', new Zend_Controller_Router_Route('complaint/discussion/all/', array('module' => 'complaint', 'controller' => 'discussion', 'action' => 'get-all-image-discussion')));
        $router->addRoute('allComplaintDiscussion', new Zend_Controller_Router_Route('complaint/discussion/all/:id', array('module' => 'complaint', 'controller' => 'discussion', 'action' => 'get-all-discussion')));
        $router->addRoute('createComplaint', new Zend_Controller_Router_Route('complaint/create', array('module' => 'complaint', 'controller' => 'index', 'action' => 'create-complaint')));
        $router->addRoute('sendComplaintAcknowledgementAsEmail', new Zend_Controller_Router_Route('complaint/acknowledgement_as_email/:id', array('module' => 'complaint', 'controller' => 'index', 'action' => 'send-complaint-acknowledgement-as-email')));


        $router->addRoute('modifyFollowDateComplaint', new Zend_Controller_Router_Route('/ajax/modifyFollowDateComplaint', array('module' => 'complaint', 'controller' => 'index', 'action' => 'modify-follow-date')));

        //Added by mona 

        $router->addRoute('approvedComplaint', new Zend_Controller_Router_Route('approve-complaint/approved-complaint/:id', array('module' => 'default', 'controller' => 'approve-complaint', 'action' => 'approved-complaint')));

        $router->addRoute('unapprovedComplaint', new Zend_Controller_Router_Route('approve-complaint/unapproved', array('module' => 'default', 'controller' => 'approve-complaint', 'action' => 'unapproved')));


        // added by Yassmein
        $router->addRoute('settingsSmsTemplateList', new Zend_Controller_Router_Route('settings/sms-template', array('module' => 'settings', 'controller' => 'sms-template', 'action' => 'index')));
        $router->addRoute('settingsSmsTemplateAdd', new Zend_Controller_Router_Route('settings/sms-template/add', array('module' => 'settings', 'controller' => 'sms-template', 'action' => 'add')));
        $router->addRoute('settingsSmsTemplateEdit', new Zend_Controller_Router_Route('settings/sms-template/edit/:id', array('module' => 'settings', 'controller' => 'sms-template', 'action' => 'edit')));
        $router->addRoute('settingsSmsTemplateDelete', new Zend_Controller_Router_Route('settings/sms-template/delete/:id', array('module' => 'settings', 'controller' => 'sms-template', 'action' => 'delete')));
        $router->addRoute('settingsSmsTemplateDeleteBtn', new Zend_Controller_Router_Route('settings/sms-template/delete', array('module' => 'settings', 'controller' => 'sms-template', 'action' => 'delete')));
        $router->addRoute('testCron', new Zend_Controller_Router_Route('test_cron', array('module' => 'booking', 'controller' => 'index', 'action' => 'test-cron')));
        $router->addRoute('convertToContractor', new Zend_Controller_Router_Route('convert_to_contractor/:sms_id', array('module' => 'booking', 'controller' => 'index', 'action' => 'convert-to-contractor')));
        $router->addRoute('smsInbox', new Zend_Controller_Router_Route('sms_inbox/:sms_id/:user_id', array('module' => 'settings', 'controller' => 'twilio-account-info', 'action' => 'sms-inbox')));

        //
        //inquiry
        //
		$router->addRoute('inquiryMarkAsSpam', new Zend_Controller_Router_Route('inquiry/mark_as_spam/', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'mark-as-spam')));
        $router->addRoute('inquiryDeleteSpam', new Zend_Controller_Router_Route('inquiry/delete_spam/', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'delete-spam')));


        $router->addRoute('inquiry', new Zend_Controller_Router_Route('inquiry', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'index')));
        $router->addRoute('inquiryDelete', new Zend_Controller_Router_Route('inquiry/delete/:id', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'delete')));
        $router->addRoute('inquiryUndelete', new Zend_Controller_Router_Route('inquiry/undelete/:id', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'undelete')));
        $router->addRoute('inquiryDeleteBtn', new Zend_Controller_Router_Route('inquiry/delete', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'delete')));
        $router->addRoute('inquiryEdit', new Zend_Controller_Router_Route('inquiry/edit/:id', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'edit')));
        $router->addRoute('inquiryView', new Zend_Controller_Router_Route('inquiry/view/:id', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'view')));
        //by Salim Inquiry view with trading name variable for customer 
        $router->addRoute('inquiryaddPhotoTradingName', new Zend_Controller_Router_Route('inquiry/view/:id/:trading_name', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'view')));
        $router->addRoute('inquiryEditDescription', new Zend_Controller_Router_Route('inquiry/edit-description/:id', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'edit-description')));
        $router->addRoute('inquiryAdd', new Zend_Controller_Router_Route('inquiry/add', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'add')));
        $router->addRoute('inquiryOutComing', new Zend_Controller_Router_Route('inquiry/out-coming', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'out-coming')));
        $router->addRoute('inquiryConvertToBooking', new Zend_Controller_Router_Route('inquiry/convert/booking/:id', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'convert-to-booking')));
        $router->addRoute('inquiryConvertToEstimate', new Zend_Controller_Router_Route('inquiry/convert/estimate/:id', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'convert-to-estimate')));
        $router->addRoute('inquiryDeleteForever', new Zend_Controller_Router_Route('inquiry/delete-forever/:id', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'delete-forever')));
        $router->addRoute('inquiryDeleteForeverBtn', new Zend_Controller_Router_Route('inquiry/delete-forever', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'delete-forever')));
        $router->addRoute('inquirySelectLabel', new Zend_Controller_Router_Route('inquiry/label/select-label/:inquiry_id', array('module' => 'inquiry', 'controller' => 'label', 'action' => 'select-label')));
        $router->addRoute('inquirySearchLabel', new Zend_Controller_Router_Route('inquiry/label/search-label', array('module' => 'inquiry', 'controller' => 'label', 'action' => 'search-label')));
        $router->addRoute('inquiryAddLabel', new Zend_Controller_Router_Route('inquiry/label/add-label/:inquiry_id', array('module' => 'inquiry', 'controller' => 'label', 'action' => 'add-label')));
        $router->addRoute('filterInquiryLabel', new Zend_Controller_Router_Route('inquiry/label/filter-label', array('module' => 'inquiry', 'controller' => 'label', 'action' => 'filter-label')));
        $router->addRoute('inquiryDisscussion', new Zend_Controller_Router_Route('inquiry/discussion/:id', array('module' => 'inquiry', 'controller' => 'discussion', 'action' => 'index')));
        $router->addRoute('submitInquiryDiscussion', new Zend_Controller_Router_Route('inquiry/discussion/send/:id', array('module' => 'inquiry', 'controller' => 'discussion', 'action' => 'submit')));
        $router->addRoute('allInquiryDiscussion', new Zend_Controller_Router_Route('inquiry/discussion/all/:id', array('module' => 'inquiry', 'controller' => 'discussion', 'action' => 'get-all-discussion')));
        $router->addRoute('allInquiryImageDiscussion', new Zend_Controller_Router_Route('inquiry/discussion/all/', array('module' => 'inquiry', 'controller' => 'discussion', 'action' => 'get-all-image-discussion')));
        $router->addRoute('invoiceConditionReport', new Zend_Controller_Router_Route('inquiry/condition-report/:id', array('module' => 'inquiry', 'controller' => 'condition-report', 'action' => 'index')));
        $router->addRoute('inquiryReminderAdd', new Zend_Controller_Router_Route('inquiry/reminder/reminder-add/:inquiry_id', array('module' => 'inquiry', 'controller' => 'reminder', 'action' => 'reminder-add')));
        $router->addRoute('inquiryReminderView', new Zend_Controller_Router_Route('inquiry/reminder/view/:id', array('module' => 'inquiry', 'controller' => 'reminder', 'action' => 'reminder-view')));
        $router->addRoute('inquiryReminderDelete', new Zend_Controller_Router_Route('inquiry/reminder/delete/:id/:reminder_id', array('module' => 'inquiry', 'controller' => 'reminder', 'action' => 'reminder-delete')));
        $router->addRoute('inquiryAttachment', new Zend_Controller_Router_Route('inquiry/attachment/:id', array('module' => 'inquiry', 'controller' => 'attachment', 'action' => 'index')));
        $router->addRoute('inquiryFileUpload', new Zend_Controller_Router_Route('inquiry/file_upload/:inquiry_id', array('module' => 'inquiry', 'controller' => 'attachment', 'action' => 'file-upload')));

        $router->addRoute('inquiryFileUploadDelete', new Zend_Controller_Router_Route('inquiry/file_upload/delete/:id', array('module' => 'inquiry', 'controller' => 'attachment', 'action' => 'delete')));
        $router->addRoute('inquiryFileUploadDeleteBtn', new Zend_Controller_Router_Route('inquiry/file_upload/delete', array('module' => 'inquiry', 'controller' => 'attachment', 'action' => 'delete')));
        $router->addRoute('inquiryFileDownload', new Zend_Controller_Router_Route('inquiry/file_download/:id', array('module' => 'inquiry', 'controller' => 'attachment', 'action' => 'download')));
        $router->addRoute('inquiryFileEditDescription', new Zend_Controller_Router_Route('inquiry/file_upload/edit_desription/:id', array('module' => 'inquiry', 'controller' => 'attachment', 'action' => 'edit-description')));
        $router->addRoute('inquirygFileEditWorkOrder', new Zend_Controller_Router_Route('inquiry/file_upload/edit_work_order/:id/:work_order', array('module' => 'inquiry', 'controller' => 'attachment', 'action' => 'edit-work-order')));
        $router->addRoute('inquiryFileViewDescription', new Zend_Controller_Router_Route('inquiry/file_upload/view_desription/:id', array('module' => 'inquiry', 'controller' => 'attachment', 'action' => 'view-description')));
        $router->addRoute('sendInquiryAdvertisingEmail', new Zend_Controller_Router_Route('inquiry/advertising_email/:id', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'send-inquiry-advertising-email')));
        $router->addRoute('removeFollowUpInquiry', new Zend_Controller_Router_Route('inquiry/remove_follow_up/:id', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'remove-follow-up-inquiry')));
        $router->addRoute('followUpInquiry', new Zend_Controller_Router_Route('inquiry/follow_up/:id', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'follow-up-inquiry')));
        $router->addRoute('modifyInquiryFollowDate', new Zend_Controller_Router_Route('/ajax/modifyInquiryFollowDate', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'modify-inquiry-follow-date')));
        //
        $router->addRoute('getAllContractorDistances', new Zend_Controller_Router_Route('ajax/get_all_contractor_distances/:inquiry_id/:service_ids', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'get-all-contractor-distances')));

        //
        //estimates
        //
		//////////Islam
        $router->addRoute('defultPage', new Zend_Controller_Router_Route('booking/index/defult_page/:contractor_id', array('module' => 'booking', 'controller' => 'index', 'action' => 'defult-page')));
        $router->addRoute('estimates', new Zend_Controller_Router_Route('estimates', array('module' => 'estimates', 'controller' => 'index', 'action' => 'index')));
        $router->addRoute('convertBookingToEstimate', new Zend_Controller_Router_Route('booking/convert/estimate/:id', array('module' => 'estimates', 'controller' => 'index', 'action' => 'convert-booking-to-estimate')));
        $router->addRoute('convertBookingToEstimateBtn', new Zend_Controller_Router_Route('booking/convert/estimate', array('module' => 'estimates', 'controller' => 'index', 'action' => 'convert-booking-to-estimate')));
        $router->addRoute('estimateView', new Zend_Controller_Router_Route('estimate/view/:id', array('module' => 'estimates', 'controller' => 'index', 'action' => 'view')));
        $router->addRoute('estimateDelete', new Zend_Controller_Router_Route('estimate/delete/:id', array('module' => 'estimates', 'controller' => 'index', 'action' => 'delete')));
        $router->addRoute('estimateViewAll', new Zend_Controller_Router_Route('estimate/view/', array('module' => 'estimates', 'controller' => 'index', 'action' => 'view')));

        $router->addRoute('estimatesByFilters', new Zend_Controller_Router_Route('estimates/filters', array('module' => 'estimates', 'controller' => 'index', 'action' => 'filters')));

        $router->addRoute('removeFollowUpEstimate', new Zend_Controller_Router_Route('estimate/remove_follow_up/:id', array('module' => 'estimates', 'controller' => 'index', 'action' => 'remove-follow-up-estimate')));
        $router->addRoute('followUpEstimate', new Zend_Controller_Router_Route('estimate/follow-up/:id', array('module' => 'estimates', 'controller' => 'index', 'action' => 'follow-up-estimate')));
        $router->addRoute('estimateUndelete', new Zend_Controller_Router_Route('estimate/undelete/:id', array('module' => 'estimates', 'controller' => 'index', 'action' => 'undelete')));
        $router->addRoute('estimateDeleteForEver', new Zend_Controller_Router_Route('estimate/deleteForEver/:id', array('module' => 'estimates', 'controller' => 'index', 'action' => 'delete-for-ever')));
        $router->addRoute('editEstimateNumber', new Zend_Controller_Router_Route('estimate/edit-estimate-number/:id', array('module' => 'estimates', 'controller' => 'index', 'action' => 'edit-estimate-number')));
        $router->addRoute('estimateAdd', new Zend_Controller_Router_Route('estimates/add', array('module' => 'estimates', 'controller' => 'index', 'action' => 'add')));
        $router->addRoute('estimateAddToCustomerId', new Zend_Controller_Router_Route('estimates/add/:customer_id', array('module' => 'estimates', 'controller' => 'index', 'action' => 'add-to-customer-id')));


        $router->addRoute('convertEstimateToBooking', new Zend_Controller_Router_Route('estimate/convert/booking/:id', array('module' => 'estimates', 'controller' => 'index', 'action' => 'convert-estimate-to-booking')));
        $router->addRoute('sendEstimateAsEmail', new Zend_Controller_Router_Route('email/estimate/:id', array('module' => 'estimates', 'controller' => 'index', 'action' => 'send-estimate-as-email')));
        $router->addRoute('sendReminderEstimateAsEmail', new Zend_Controller_Router_Route('email/reminder-estimate/:id', array('module' => 'estimates', 'controller' => 'index', 'action' => 'send-reminder-estimate-as-email')));
        $router->addRoute('sendEmail', new Zend_Controller_Router_Route('email/customer/:id/:type/:reference_id', array('module' => 'customer', 'controller' => 'index', 'action' => 'send-email')));
        $router->addRoute('downloadEstimate', new Zend_Controller_Router_Route('download/estimate/:id', array('module' => 'estimates', 'controller' => 'index', 'action' => 'download-estimate')));
        $router->addRoute('estimateSelectLabel', new Zend_Controller_Router_Route('estimate/label/select-label/:id', array('module' => 'estimates', 'controller' => 'label', 'action' => 'select-label')));
        $router->addRoute('estimateAddLabel', new Zend_Controller_Router_Route('estimate/label/add-label/:id', array('module' => 'estimates', 'controller' => 'label', 'action' => 'add-label')));
        $router->addRoute('filterEstimateLabel', new Zend_Controller_Router_Route('estimate/label/filter-label', array('module' => 'estimates', 'controller' => 'label', 'action' => 'filter-label')));
        $router->addRoute('estimateSearchLabel', new Zend_Controller_Router_Route('estimate/label/search-label', array('module' => 'estimates', 'controller' => 'label', 'action' => 'search-label')));
        $router->addRoute('estimateDisscussion', new Zend_Controller_Router_Route('estimate/discussion/:id', array('module' => 'estimates', 'controller' => 'discussion', 'action' => 'index')));
        $router->addRoute('submitEstimateDiscussion', new Zend_Controller_Router_Route('estimate/discussion/send/:id', array('module' => 'estimates', 'controller' => 'discussion', 'action' => 'submit')));
        $router->addRoute('allEstimateDiscussion', new Zend_Controller_Router_Route('estimate/discussion/all/:id', array('module' => 'estimates', 'controller' => 'discussion', 'action' => 'get-all-discussion')));
        $router->addRoute('allEstimateImageDiscussion', new Zend_Controller_Router_Route('estimate/discussion/all/', array('module' => 'estimates', 'controller' => 'discussion', 'action' => 'get-all-image-discussion')));

        $router->addRoute('emailDetails', new Zend_Controller_Router_Route('email/details/:id', array('module' => 'default', 'controller' => 'email-details', 'action' => 'index')));
        $router->addRoute('isDeletedEstimate', new Zend_Controller_Router_Route('estimate/:is_deleted', array('module' => 'estimates', 'controller' => 'index', 'action' => 'index')));
        $router->addRoute('estimatePreview', new Zend_Controller_Router_Route('estimate/preview/:id', array('module' => 'estimates', 'controller' => 'index', 'action' => 'preview')));
        $router->addRoute('estimateDeleteBtn', new Zend_Controller_Router_Route('estimate/delete', array('module' => 'estimates', 'controller' => 'index', 'action' => 'delete')));
        $router->addRoute('sendAdvertisingEmail', new Zend_Controller_Router_Route('estimate/advertising_email/:id', array('module' => 'estimates', 'controller' => 'index', 'action' => 'send-advertising-email')));
        $router->addRoute('sendEmailToAllContractors', new Zend_Controller_Router_Route('email/all-contractors', array('module' => 'contractor', 'controller' => 'index', 'action' => 'send-email-to-all')));
        $router->addRoute('sendEmailToOneContractor', new Zend_Controller_Router_Route('email/one-contractor/:contractor_id', array('module' => 'contractor', 'controller' => 'index', 'action' => 'send-email-to-one-contractor')));

        //invoices
        //
        $router->addRoute('invoices', new Zend_Controller_Router_Route('invoices', array('module' => 'invoices', 'controller' => 'index', 'action' => 'index')));
        $router->addRoute('duplicatedInvoices', new Zend_Controller_Router_Route('invoices/duplicated-invoices', array('module' => 'invoices', 'controller' => 'index', 'action' => 'duplicated-invoices')));
        $router->addRoute('convertBookingToInvoice', new Zend_Controller_Router_Route('booking/convert/invoice/:id', array('module' => 'invoices', 'controller' => 'index', 'action' => 'convert-booking-to-invoice')));
        $router->addRoute('convertBookingToInvoiceBtn', new Zend_Controller_Router_Route('booking/convert/invoice', array('module' => 'invoices', 'controller' => 'index', 'action' => 'convert-booking-to-invoice')));
        $router->addRoute('invoiceView', new Zend_Controller_Router_Route('invoice/:id', array('module' => 'invoices', 'controller' => 'index', 'action' => 'view')));
        $router->addRoute('withholdReleasePayment', new Zend_Controller_Router_Route('invoice/withhold-release-payment/:id/:process', array('module' => 'invoices', 'controller' => 'index', 'action' => 'withhold-release-payment')));
        $router->addRoute('editInvoiceNumber', new Zend_Controller_Router_Route('invoice/edit-invoice-number/:id', array('module' => 'invoices', 'controller' => 'index', 'action' => 'edit-invoice-number')));
        $router->addRoute('checkDuplicateInvoice', new Zend_Controller_Router_Route('invoice', array('module' => 'invoices', 'controller' => 'index', 'action' => 'check-duplicate-invoice')));
        $router->addRoute('invoicePreview', new Zend_Controller_Router_Route('invoice/preview/:id', array('module' => 'invoices', 'controller' => 'index', 'action' => 'preview')));
        $router->addRoute('invoiceDelete', new Zend_Controller_Router_Route('invoice/delete/:id', array('module' => 'invoices', 'controller' => 'index', 'action' => 'delete')));
        $router->addRoute('invoiceDeleteBtn', new Zend_Controller_Router_Route('invoice/delete', array('module' => 'invoices', 'controller' => 'index', 'action' => 'delete')));
        $router->addRoute('convertToOpen', new Zend_Controller_Router_Route('invoice/convert/open', array('module' => 'invoices', 'controller' => 'index', 'action' => 'convert-to-open')));
        $router->addRoute('convertToOpenLink', new Zend_Controller_Router_Route('invoice/convert-link/open/:id', array('module' => 'invoices', 'controller' => 'index', 'action' => 'convert-to-open')));
        $router->addRoute('convert', new Zend_Controller_Router_Route('invoice/convert/:type/:id', array('module' => 'invoices', 'controller' => 'index', 'action' => 'convert')));
        $router->addRoute('sendInvoiceAsEmail', new Zend_Controller_Router_Route('email/invoice/:id', array('module' => 'invoices', 'controller' => 'index', 'action' => 'send-invoice-as-email')));
        $router->addRoute('sendReminderOverdueInvoiceAsEmail', new Zend_Controller_Router_Route('email/reminder-overdue-invoice/:id', array('module' => 'invoices', 'controller' => 'index', 'action' => 'send-reminder-overdue-invoice-as-email')));
        $router->addRoute('downloadInvoice', new Zend_Controller_Router_Route('download/invoice/:id', array('module' => 'invoices', 'controller' => 'index', 'action' => 'download-invoice')));
        $router->addRoute('setDueDate', new Zend_Controller_Router_Route('invoices/set-due-date/:booking_id', array('module' => 'invoices', 'controller' => 'index', 'action' => 'set-due-date')));
        $router->addRoute('invoiceAddLabel', new Zend_Controller_Router_Route('invoice/label/add-label/:id', array('module' => 'invoices', 'controller' => 'label', 'action' => 'add-label')));
        $router->addRoute('filterInvoiceLabel', new Zend_Controller_Router_Route('invoice/label/filter-label', array('module' => 'invoices', 'controller' => 'label', 'action' => 'filter-label')));
        $router->addRoute('invoiceSearchLabel', new Zend_Controller_Router_Route('invoice/label/search-label', array('module' => 'invoices', 'controller' => 'label', 'action' => 'search-label')));
        $router->addRoute('invoiceSelectLabel', new Zend_Controller_Router_Route('invoice/label/select-label/:id', array('module' => 'invoices', 'controller' => 'label', 'action' => 'select-label')));
        $router->addRoute('invoiceDisscussion', new Zend_Controller_Router_Route('invoice/discussion/:id', array('module' => 'invoices', 'controller' => 'discussion', 'action' => 'index')));
        $router->addRoute('rejectPayment', new Zend_Controller_Router_Route('invoice/discussion/:id/:payment_id', array('module' => 'invoices', 'controller' => 'discussion', 'action' => 'index')));

        $router->addRoute('submitInvoiceDiscussion', new Zend_Controller_Router_Route('invoice/discussion/send/:id', array('module' => 'invoices', 'controller' => 'discussion', 'action' => 'submit')));
        $router->addRoute('allInvoiceDiscussion', new Zend_Controller_Router_Route('invoice/discussion/all/:id', array('module' => 'invoices', 'controller' => 'discussion', 'action' => 'get-all-discussion')));
        $router->addRoute('invoiceConditionReport', new Zend_Controller_Router_Route('invoice/condition-report/:id', array('module' => 'invoices', 'controller' => 'condition-report', 'action' => 'index')));

        $router->addRoute('editContractorInvoiceNumberOnly', new Zend_Controller_Router_Route('contractor-report/edit-invoice-number/:payment_to_contractor_id', array('module' => 'reports', 'controller' => 'contractor-report', 'action' => 'edit-contractor-invoice-number-only')));

        $router->addRoute('editContractorInvoiceNumber', new Zend_Controller_Router_Route('ajax/editContractorInvoiceNumber', array('module' => 'reports', 'controller' => 'contractor-report', 'action' => 'edit-contractor-invoice-number')));
        $router->addRoute('editPaymentToContractor', new Zend_Controller_Router_Route('ajax/editPaymentToContractor', array('module' => 'reports', 'controller' => 'contractor-report', 'action' => 'edit-payment-to-contractor')));

        // Changed by Salim


        $router->addRoute('editPaymentToContractor1', new Zend_Controller_Router_Route('ajax/editPaymentToContractor/:payment_to_contractor_id/:invoice_id', array('module' => 'reports', 'controller' => 'contractor-report', 'action' => 'edit-payment-to-contractor')));

        $router->addRoute('deletePaymentToContractors', new Zend_Controller_Router_Route('ajax/payment_to_contractors/:id', array('module' => 'reports', 'controller' => 'payment-report', 'action' => 'delete-payment-to-contractors')));
        $router->addRoute('deletePaymentToContractorAttachment', new Zend_Controller_Router_Route('reports/payment_to_contractors/:attachment_id', array('module' => 'reports', 'controller' => 'payment-report', 'action' => 'delete-payment-to-contractor-attachment')));
        $router->addRoute('uploadDocumentToGoogleDrive', new Zend_Controller_Router_Route('reports/payment_to_contractors/:contractor_id/:attachment_id/:file_name', array('module' => 'reports', 'controller' => 'payment-report', 'action' => 'upload-document-to-google-drive')));

        $router->addRoute('accountSummary', new Zend_Controller_Router_Route('dashboard/accountSummary/:account', array('module' => 'reports', 'controller' => 'contractor-report', 'action' => 'contractors-bookings-summary')));


        //
        //payment
        //
		$router->addRoute('paymentAttachmentDownload', new Zend_Controller_Router_Route('reports/file_download/:id/:file_name', array('module' => 'reports', 'controller' => 'attachment', 'action' => 'download')));


        $router->addRoute('paymentList', new Zend_Controller_Router_Route('payment', array('module' => 'invoices', 'controller' => 'payment', 'action' => 'index')));
        $router->addRoute('bookingPaymentList', new Zend_Controller_Router_Route('payment/:booking_id', array('module' => 'invoices', 'controller' => 'payment', 'action' => 'index')));
        $router->addRoute('statment', new Zend_Controller_Router_Route('statment/:id', array('module' => 'invoices', 'controller' => 'payment', 'action' => 'statment')));
        $router->addRoute('sendStatmentsAsEmail', new Zend_Controller_Router_Route('email/statment/:id', array('module' => 'invoices', 'controller' => 'payment', 'action' => 'send-statments-as-email')));
        $router->addRoute('paymentAdd', new Zend_Controller_Router_Route('payment/add/:booking_id', array('module' => 'invoices', 'controller' => 'payment', 'action' => 'add')));
        $router->addRoute('checkDuplicatePaymentReference', new Zend_Controller_Router_Route('payment/add/chech-duplicate-payment', array('module' => 'invoices', 'controller' => 'payment', 'action' => 'check-duplicate-payment-reference')));
        // payment invoice match By Salim
        $router->addRoute('paymentInvoiceMatch', new Zend_Controller_Router_Route('payment/invoice-match/:payment_id', array('module' => 'invoices', 'controller' => 'payment', 'action' => 'get-payment-invoices-match')));

        $router->addRoute('paymentEwayAdd', new Zend_Controller_Router_Route('payment/ewayAdd/:booking_id', array('module' => 'invoices', 'controller' => 'payment', 'action' => 'eway-add')));
        $router->addRoute('checkFullPaidPayment', new Zend_Controller_Router_Route('payment/check-full-paid-payment', array('module' => 'invoices', 'controller' => 'payment', 'action' => 'check-full-paid-payment')));
        //By Islam prevent user change contractor if the contractor has payment
        $router->addRoute('checkPayments', new Zend_Controller_Router_Route('ajax/invoices/check-payments', array('module' => 'invoices', 'controller' => 'payment', 'action' => 'check-payments')));

        $router->addRoute('paymentDeleteCheckedBtn', new Zend_Controller_Router_Route('payment/delete', array('module' => 'invoices', 'controller' => 'payment', 'action' => 'delete')));
        $router->addRoute('paymentDelete', new Zend_Controller_Router_Route('payment/delete', array('module' => 'invoices', 'controller' => 'payment', 'action' => 'delete')));
        // By Salim Ignore Unknown Payments
        $router->addRoute('paymentIgnore', new Zend_Controller_Router_Route('payment/ignore/', array('module' => 'invoices', 'controller' => 'payment', 'action' => 'ignore-unknown-payment')));
        // By Salim Remove Diff Amount Label
        $router->addRoute('removeDifferentAmount', new Zend_Controller_Router_Route('payment/remove-diff-amount', array('module' => 'invoices', 'controller' => 'payment', 'action' => 'remove-diff-amount')));


        $router->addRoute('paymentApprove', new Zend_Controller_Router_Route('payment/approve/:booking_id/:id', array('module' => 'invoices', 'controller' => 'payment', 'action' => 'payment-approve')));
        $router->addRoute('paymentUnapprove', new Zend_Controller_Router_Route('payment/unapprove/:booking_id/:id', array('module' => 'invoices', 'controller' => 'payment', 'action' => 'payment-unapprove')));
        $router->addRoute('paymentDeleteBtn', new Zend_Controller_Router_Route('payment/delete/:booking_id', array('module' => 'invoices', 'controller' => 'payment', 'action' => 'delete')));
        $router->addRoute('checkStatusToAddPayment', new Zend_Controller_Router_Route('payment/checkStatus/:booking_id', array('module' => 'invoices', 'controller' => 'payment', 'action' => 'check-status-to-add-payment')));
        $router->addRoute('editPayment', new Zend_Controller_Router_Route('payment/edit/:id', array('module' => 'invoices', 'controller' => 'payment', 'action' => 'edit')));
        $router->addRoute('markPayment', new Zend_Controller_Router_Route('markPayment/:payment_id', array('module' => 'invoices', 'controller' => 'payment', 'action' => 'mark-payment')));
        $router->addRoute('unmarkPayment', new Zend_Controller_Router_Route('/ajax/unmarkPayment', array('module' => 'invoices', 'controller' => 'payment', 'action' => 'unmark-payment')));
        $router->addRoute('modifyMarkComment', new Zend_Controller_Router_Route('modify_mark_comment/:payment_id', array('module' => 'invoices', 'controller' => 'payment', 'action' => 'modify-mark-comment')));
        $router->addRoute('markPaymentView', new Zend_Controller_Router_Route('mark_payment_view/:payment_id', array('module' => 'invoices', 'controller' => 'payment', 'action' => 'mark-payment-view')));
        $router->addRoute('changeReceivedDate', new Zend_Controller_Router_Route('/ajax/changeReceivedDate', array('module' => 'invoices', 'controller' => 'payment', 'action' => 'change-received-date')));

        $router->addRoute('editDiscussionVisibility', new Zend_Controller_Router_Route('booking/discussion/editDiscussionVisibility', array('module' => 'booking', 'controller' => 'discussion', 'action' => 'edit-discussion-visibility')));
        $router->addRoute('editContractorDiscussionVisibility', new Zend_Controller_Router_Route('settings/contractorDiscussion/editDiscussionVisibility', array('module' => 'settings', 'controller' => 'contractor-discussion', 'action' => 'edit-discussion-visibility')));

        $router->addRoute('removerateTagImage', new Zend_Controller_Router_Route('remove-tag-image', array('module' => 'settings', 'controller' => 'rate-tag', 'action' => 'remove-tag-image')));

        //
        //refund
        //
		//IBM
        $router->addRoute('refundtest', new Zend_Controller_Router_Route('refundsss', array('module' => 'invoices', 'controller' => 'refund', 'action' => 'refund-eway')));

        $router->addRoute('refundAdd', new Zend_Controller_Router_Route('refund/add/:booking_id', array('module' => 'invoices', 'controller' => 'refund', 'action' => 'add')));
        $router->addRoute('refundAddByEway', new Zend_Controller_Router_Route('refund/addrefund/:booking_id/:payment_id', array('module' => 'invoices', 'controller' => 'refund', 'action' => 'add-refund-by-eway')));

        $router->addRoute('bookingRefundList', new Zend_Controller_Router_Route('refund/:booking_id', array('module' => 'invoices', 'controller' => 'refund', 'action' => 'index')));
        $router->addRoute('refundList', new Zend_Controller_Router_Route('refund', array('module' => 'invoices', 'controller' => 'refund', 'action' => 'index')));
        $router->addRoute('refundDelete', new Zend_Controller_Router_Route('refund/delete/:booking_id/:id', array('module' => 'invoices', 'controller' => 'refund', 'action' => 'delete')));
        $router->addRoute('refundApprove', new Zend_Controller_Router_Route('refund/approve/:booking_id/:id', array('module' => 'invoices', 'controller' => 'refund', 'action' => 'refund-approve')));
        $router->addRoute('refundUnapprove', new Zend_Controller_Router_Route('refund/unapprove/:booking_id/:id', array('module' => 'invoices', 'controller' => 'refund', 'action' => 'refund-unapprove')));
        $router->addRoute('refundDeleteBtn', new Zend_Controller_Router_Route('refund/delete/:booking_id', array('module' => 'invoices', 'controller' => 'refund', 'action' => 'delete')));
        $router->addRoute('editRefund', new Zend_Controller_Router_Route('refund/edit/:id', array('module' => 'invoices', 'controller' => 'refund', 'action' => 'edit')));

        $router->addRoute('reports', new Zend_Controller_Router_Route('reports', array('module' => 'reports', 'controller' => 'index', 'action' => 'index')));
        $router->addRoute('addPaymentToContractor', new Zend_Controller_Router_Route('ajax/addPaymentToContractor', array('module' => 'reports', 'controller' => 'contractor-report', 'action' => 'add-payment-to-contractor')));
        $router->addRoute('addContractorInvoiceNumber', new Zend_Controller_Router_Route('ajax/addContractorInvoiceNumber/:flag', array('module' => 'reports', 'controller' => 'contractor-report', 'action' => 'add-contractor-invoice-number')));
        $router->addRoute('contractorInvoiceNumbersByContractorId', new Zend_Controller_Router_Route('ajax/contractorInvoiceNumbersByContractorId/', array('module' => 'reports', 'controller' => 'contractor-report', 'action' => 'contractor-invoice-numbers-by-contractor-id')));
        $router->addRoute('paymentToContractorById', new Zend_Controller_Router_Route('ajax/paymentToContractorById/', array('module' => 'reports', 'controller' => 'contractor-report', 'action' => 'payment-to-contractor-by-id')));

        $router->addRoute('unPaidContractor', new Zend_Controller_Router_Route('reports/un_paid_contractor/:id', array('module' => 'reports', 'controller' => 'contractor-report', 'action' => 'un-paid-contractor')));
        $router->addRoute('reportContractorsBookingsSummary', new Zend_Controller_Router_Route('reports/contractors_bookings_summary', array('module' => 'reports', 'controller' => 'contractor-report', 'action' => 'contractors-bookings-summary')));
        $router->addRoute('reportContractorsDetailedSummary', new Zend_Controller_Router_Route('reports/contractors_detailed_summary', array('module' => 'reports', 'controller' => 'contractor-report', 'action' => 'contractors-detailed-summary')));
        $router->addRoute('reportContractorsSummary', new Zend_Controller_Router_Route('reports/contractors_summary', array('module' => 'reports', 'controller' => 'contractor-report', 'action' => 'contractors-summary')));
        $router->addRoute('contractorsBookingPayment', new Zend_Controller_Router_Route('reports/contractors_booking_payment', array('module' => 'reports', 'controller' => 'contractor-report', 'action' => 'contractors-booking-payment')));
        $router->addRoute('reportContractorsPaymentDetailedSummary', new Zend_Controller_Router_Route('reports/contractors_payment_detailed_summary', array('module' => 'reports', 'controller' => 'contractor-report', 'action' => 'contractors-payment-detailed-summary')));
        $router->addRoute('bookingSummary', new Zend_Controller_Router_Route('reports/booking_summary', array('module' => 'reports', 'controller' => 'common-report', 'action' => 'booking-summary')));
        $router->addRoute('bookingSummaryByChart', new Zend_Controller_Router_Route('reports/booking_summary_by_chart', array('module' => 'reports', 'controller' => 'common-report', 'action' => 'booking-summary-by-chart')));
        $router->addRoute('convertToBookingDuration', new Zend_Controller_Router_Route('reports/convert_to_booking_duration', array('module' => 'reports', 'controller' => 'common-report', 'action' => 'convert-to-booking-duration')));
        $router->addRoute('reportPaymentToContractors', new Zend_Controller_Router_Route('reports/payment_to_contractors', array('module' => 'reports', 'controller' => 'payment-report', 'action' => 'payment-to-contractors')));

        $router->addRoute('sendpaymentAsEmail', new Zend_Controller_Router_Route('payment_report/send_payment_as_email/:id/:contractor_id', array('module' => 'reports', 'controller' => 'payment-report', 'action' => 'send-payment-as-email')));
        $router->addRoute('sendpayAsEmail', new Zend_Controller_Router_Route('payment_report/send_payment_as_email', array('module' => 'reports', 'controller' => 'payment-report', 'action' => 'send-payment-as-email')));

        $router->addRoute('bookingServiceTileSummary', new Zend_Controller_Router_Route('reports/booking_service_tile_summary', array('module' => 'reports', 'controller' => 'common-report', 'action' => 'booking-service-tile-summary')));
        $router->addRoute('reportSalesByCustomer', new Zend_Controller_Router_Route('reports/sales_by_customer', array('module' => 'reports', 'controller' => 'customer-report', 'action' => 'sales-by-customer')));
        $router->addRoute('reportPaymentReceived', new Zend_Controller_Router_Route('reports/payment_received', array('module' => 'reports', 'controller' => 'payment-report', 'action' => 'payment-received')));
        $router->addRoute('reportSalesByEmployee', new Zend_Controller_Router_Route('reports/sales_By_Employee', array('module' => 'reports', 'controller' => 'employee-report', 'action' => 'sales-by-employee')));
        $router->addRoute('reportOverdueInvoices', new Zend_Controller_Router_Route('reports/overdue_invoices', array('module' => 'reports', 'controller' => 'payment-report', 'action' => 'overdue-invoices')));
        $router->addRoute('reportRefundHistory', new Zend_Controller_Router_Route('reports/refund_history', array('module' => 'reports', 'controller' => 'payment-report', 'action' => 'refund-history')));


        $router->addRoute('editPaidAmount', new Zend_Controller_Router_Route('reports/edit_paid_amount/:id/:contractor_id/:old_paid_amount', array('module' => 'reports', 'controller' => 'contractor-report', 'action' => 'edit-paid-amount')));


        $router->addRoute('savedReport', new Zend_Controller_Router_Route('reports/saved-report', array('module' => 'reports', 'controller' => 'index', 'action' => 'saved-report')));
        $router->addRoute('savedReportDeleteBtn', new Zend_Controller_Router_Route('reports/saved-report/delete', array('module' => 'reports', 'controller' => 'index', 'action' => 'delete')));
        $router->addRoute('savedReportDelete', new Zend_Controller_Router_Route('reports/saved-report/delete/:id', array('module' => 'reports', 'controller' => 'index', 'action' => 'delete')));
        $router->addRoute('savedReportDownload', new Zend_Controller_Router_Route('reports/saved-report/download/:id', array('module' => 'reports', 'controller' => 'index', 'action' => 'download')));

        $router->addRoute('bookedCompletedAmountReport', new Zend_Controller_Router_Route('reports/bookedAndCompletedAmountReport', array('module' => 'reports', 'controller' => 'payment-report', 'action' => 'booked-completed-amount-report')));

        //the websites module
        $router->addRoute('websites', new Zend_Controller_Router_Route('websites', array('module' => 'websites', 'controller' => 'index', 'action' => 'index')));
        $router->addRoute('companyWebsite', new Zend_Controller_Router_Route('company_websites/:id', array('module' => 'websites', 'controller' => 'index', 'action' => 'company-website')));

        $router->addRoute('pageAdd', new Zend_Controller_Router_Route('add_page', array('module' => 'websites', 'controller' => 'index', 'action' => 'page-add')));
        $router->addRoute('pageEdit', new Zend_Controller_Router_Route('edit_page/:id', array('module' => 'websites', 'controller' => 'index', 'action' => 'page-edit')));
        $router->addRoute('pageDelete', new Zend_Controller_Router_Route('delete_page/:id', array('module' => 'websites', 'controller' => 'index', 'action' => 'page-delete')));

        $router->addRoute('pageBlock', new Zend_Controller_Router_Route('page_block/:id', array('module' => 'websites', 'controller' => 'block', 'action' => 'index')));
        $router->addRoute('blockAdd', new Zend_Controller_Router_Route('add/:page_id', array('module' => 'websites', 'controller' => 'block', 'action' => 'add')));
        $router->addRoute('blockEdit', new Zend_Controller_Router_Route('edit', array('module' => 'websites', 'controller' => 'block', 'action' => 'edit')));
        $router->addRoute('getType', new Zend_Controller_Router_Route('get_type', array('module' => 'websites', 'controller' => 'block', 'action' => 'get-type')));

        //Added by Salim for Trading Names
        $router->addRoute('settingsCompaniesTwilioAccountInfo', new Zend_Controller_Router_Route('settings/twilio-account', array('module' => 'settings', 'controller' => 'twilio-account-info', 'action' => 'index')));
        $router->addRoute('settingsCompaniesTradingNames', new Zend_Controller_Router_Route('settings/trading-names', array('module' => 'settings', 'controller' => 'trading-names', 'action' => 'index')));
        $router->addRoute('settingsCompaniesTradingNamesAdd', new Zend_Controller_Router_Route('settings/trading-names/add', array('module' => 'settings', 'controller' => 'trading-names', 'action' => 'add')));
        $router->addRoute('settingsCompaniesTradingNamesEdit', new Zend_Controller_Router_Route('settings/trading-names/edit/:id', array('module' => 'settings', 'controller' => 'trading-names', 'action' => 'edit')));
        $router->addRoute('settingsCompaniesTradingNamesDelete', new Zend_Controller_Router_Route('settings/trading-names/delete/:id', array('module' => 'settings', 'controller' => 'trading-names', 'action' => 'delete')));
        

        //Eman
        
        $router->addRoute('settingsCompaniesApis', new Zend_Controller_Router_Route('settings/api', array('module' => 'settings', 'controller' => 'api-parameters', 'action' => 'index')));
        $router->addRoute('settingsCompaniesApisAddEdit', new Zend_Controller_Router_Route('settings/api/add-edit', array('module' => 'settings', 'controller' => 'api-parameters', 'action' => 'add-edit')));
        $router->addRoute('apiDelete', new Zend_Controller_Router_Route('settings/api/delete/:api_name', array('module' => 'settings', 'controller' => 'api-parameters', 'action' => 'delete')));

        //SMS
        $router->addRoute('recentSms', new Zend_Controller_Router_Route('dashboard/recentSms', array('module' => 'default', 'controller' => 'dashboard', 'action' => 'recent-sms')));
        $router->addRoute('sendBookingAsSmsByTwilio', new Zend_Controller_Router_Route('sms_twilio/booking/:id/:type', array('module' => 'booking', 'controller' => 'index', 'action' => 'send-booking-as-sms-by-twilio')));
        $router->addRoute('receiveIncomingMsgTwilio', new Zend_Controller_Router_Route('sms_twilio/receive/incoming_sms', array('module' => 'default', 'controller' => 'index', 'action' => 'receive-incoming-msg-twilio')));
        $router->addRoute('replySmsByTwilio', new Zend_Controller_Router_Route('sms_twilio/reply/:to/:id', array('module' => 'booking', 'controller' => 'index', 'action' => 'reply-sms-by-twilio')));
        $router->addRoute('viewReplySmsByTwilio', new Zend_Controller_Router_Route('sms_twilio/view_reply/:id', array('module' => 'booking', 'controller' => 'index', 'action' => 'view-reply-sms-by-twilio'))); //view-reply-sms-by-twilio
        $router->addRoute('sendInvoiceAsSms', new Zend_Controller_Router_Route('sms/invoice/:id', array('module' => 'invoices', 'controller' => 'index', 'action' => 'send-invoice-as-sms')));
        $router->addRoute('sendInquiryAsSms', new Zend_Controller_Router_Route('sms/iquiry/:id', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'send-inquiry-as-sms'))); //inquiry
        $router->addRoute('sendComplaintAsSms', new Zend_Controller_Router_Route('sms/complaint/:id', array('module' => 'complaint', 'controller' => 'index', 'action' => 'send-complaint-as-sms'))); //inquiry
        $router->addRoute('sendEstimatesAsSms', new Zend_Controller_Router_Route('sms/estimates/:id', array('module' => 'estimates', 'controller' => 'index', 'action' => 'send-estimates-as-sms')));

        $router->addRoute('convertSmsReason', new Zend_Controller_Router_Route('convert/sms_reason/:sid/:reference_id/:id/:sms_reason/:send_time', array('module' => 'booking', 'controller' => 'index', 'action' => 'convert-sms-reason'))); //convert sms reason
        $router->addRoute('convertSmsDependOnOtherSms', new Zend_Controller_Router_Route('convert/reason_change/:sid/:incoming_sid', array('module' => 'booking', 'controller' => 'index', 'action' => 'convert-sms-depend-on-other-sms'))); //convert sms reason
        $router->addRoute('viewOutgoingSmsSameDateType', new Zend_Controller_Router_Route('sms_twilio/view_outgoing/:id/:reason/:date_received/:user_id/:reference_id', array('module' => 'booking', 'controller' => 'index', 'action' => 'view-outgoing-sms-same-date-type'))); //view-reply-sms-by-twilio

        $router->addRoute('smsNotifications', new Zend_Controller_Router_Route('sms/notifications', array('module' => 'notification', 'controller' => 'index', 'action' => 'index-sms')));
        $router->addRoute('sendSms', new Zend_Controller_Router_Route('sms/customer/:id/:user_type/:type/:reference_id', array('module' => 'customer', 'controller' => 'index', 'action' => 'send-sms')));



        //
        //Web Services 
        //
		$router->addRoute('getAllCompanies', new Zend_Controller_Router_Route('webservice/get-all-companies', array('module' => 'webservice', 'controller' => 'index', 'action' => 'get-all-companies')));
        $router->addRoute('uploadImageNewBooking', new Zend_Controller_Router_Route('webservice/upload-image-new-booking', array('module' => 'webservice', 'controller' => 'index', 'action' => 'upload-image-new-booking')));
        $router->addRoute('requestEditAccess', new Zend_Controller_Router_Route('webservice/request-edit-access', array('module' => 'webservice', 'controller' => 'index', 'action' => 'request-edit-access')));

        $router->addRoute('inactiveContractorCredentials', new Zend_Controller_Router_Route('webservice/inactive-contractor-credentials', array('module' => 'webservice', 'controller' => 'index', 'action' => 'inactive-contractor-credentials')));

        $router->addRoute('addContractorDiscussion', new Zend_Controller_Router_Route('webservice/add-contractor-discussion', array('module' => 'webservice', 'controller' => 'index', 'action' => 'add-contractor-discussion')));


        $router->addRoute('webservice', new Zend_Controller_Router_Route('webservice', array('module' => 'webservice', 'controller' => 'index', 'action' => 'index')));
        $router->addRoute('login', new Zend_Controller_Router_Route('webservice/login', array('module' => 'webservice', 'controller' => 'index', 'action' => 'login')));
        $router->addRoute('customerLogin', new Zend_Controller_Router_Route('webservice/customer-login', array('module' => 'webservice', 'controller' => 'index', 'action' => 'customer-login')));
        $router->addRoute('getAllBookings', new Zend_Controller_Router_Route('webservice/get-all-bookings', array('module' => 'webservice', 'controller' => 'index', 'action' => 'get-all-bookings')));
        $router->addRoute('getUpdatedBookings', new Zend_Controller_Router_Route('webservice/get-updated-bookings', array('module' => 'webservice', 'controller' => 'index', 'action' => 'get-updated-bookings')));
        $router->addRoute('updateSync', new Zend_Controller_Router_Route('webservice/update-sync', array('module' => 'webservice', 'controller' => 'index', 'action' => 'update-sync')));
        $router->addRoute('acceptOrRejectBooking', new Zend_Controller_Router_Route('webservice/accept-or-reject-booking', array('module' => 'webservice', 'controller' => 'index', 'action' => 'accept-or-reject-booking')));
        $router->addRoute('addPayment', new Zend_Controller_Router_Route('webservice/add-payment', array('module' => 'webservice', 'controller' => 'index', 'action' => 'add-payment')));
        $router->addRoute('getWorkingHours', new Zend_Controller_Router_Route('webservice/get-working-hours', array('module' => 'webservice', 'controller' => 'index', 'action' => 'get-working-hours')));
        $router->addRoute('addWorkingHours', new Zend_Controller_Router_Route('webservice/add-working-hours', array('module' => 'webservice', 'controller' => 'index', 'action' => 'add-working-hours')));
        $router->addRoute('deleteWorkingHours', new Zend_Controller_Router_Route('webservice/delete-working-hours', array('module' => 'webservice', 'controller' => 'index', 'action' => 'delete-working-hours')));

        $router->addRoute('editPaymentWebservice', new Zend_Controller_Router_Route('webservice/edit-payment', array('module' => 'webservice', 'controller' => 'index', 'action' => 'edit-payment')));

        $router->addRoute('getPaymentTypes', new Zend_Controller_Router_Route('webservice/get-payment-types', array('module' => 'webservice', 'controller' => 'index', 'action' => 'get-payment-types')));
        $router->addRoute('getBookingStatus', new Zend_Controller_Router_Route('webservice/get-booking-status', array('module' => 'webservice', 'controller' => 'index', 'action' => 'get-booking-status')));
        $router->addRoute('prepareToEdit', new Zend_Controller_Router_Route('webservice/prepare-to-edit', array('module' => 'webservice', 'controller' => 'index', 'action' => 'prepare-to-edit')));
        $router->addRoute('saveBooking', new Zend_Controller_Router_Route('webservice/save-booking', array('module' => 'webservice', 'controller' => 'index', 'action' => 'save-booking')));
        $router->addRoute('getAllProducts', new Zend_Controller_Router_Route('webservice/get-all-products', array('module' => 'webservice', 'controller' => 'index', 'action' => 'get-all-products')));
        $router->addRoute('getAllPropertyType', new Zend_Controller_Router_Route('webservice/get-all-property-type', array('module' => 'webservice', 'controller' => 'index', 'action' => 'get-all-property-type')));
        $router->addRoute('getCallOutFee', new Zend_Controller_Router_Route('webservice/get-call-out-fee', array('module' => 'webservice', 'controller' => 'index', 'action' => 'get-call-out-fee')));
        $router->addRoute('getAllFloor', new Zend_Controller_Router_Route('webservice/get-all-floor', array('module' => 'webservice', 'controller' => 'index', 'action' => 'get-all-floor')));
        $router->addRoute('test', new Zend_Controller_Router_Route('webservice/test', array('module' => 'webservice', 'controller' => 'index', 'action' => 'test')));
        $router->addRoute('receiveDataFromAndroid', new Zend_Controller_Router_Route('webservice/receive-data-from-android', array('module' => 'webservice', 'controller' => 'index', 'action' => 'receive-data-from-android')));


        $router->addRoute('getAllSealer', new Zend_Controller_Router_Route('webservice/get-all-sealer', array('module' => 'webservice', 'controller' => 'index', 'action' => 'get-all-sealer')));
        $router->addRoute('getRequestAccess', new Zend_Controller_Router_Route('webservice/get-request-access', array('module' => 'webservice', 'controller' => 'index', 'action' => 'get-request-access')));


        $router->addRoute('registerMobileIos', new Zend_Controller_Router_Route('webservice/register-mobile-ios', array('module' => 'webservice', 'controller' => 'index', 'action' => 'register-mobile-ios')));
        $router->addRoute('registerMobileAndroid', new Zend_Controller_Router_Route('webservice/register-mobile-android', array('module' => 'webservice', 'controller' => 'index', 'action' => 'register-mobile-android')));
        $router->addRoute('getIosNotificationSettings', new Zend_Controller_Router_Route('webservice/get-ios-notification-settings', array('module' => 'webservice', 'controller' => 'index', 'action' => 'get-ios-notification-settings')));
        $router->addRoute('iosUserNotificationSettings', new Zend_Controller_Router_Route('webservice/ios-user-notification-settings', array('module' => 'webservice', 'controller' => 'index', 'action' => 'ios-user-notification-settings')));
        $router->addRoute('iosUserNotification', new Zend_Controller_Router_Route('webservice/ios-user-notification', array('module' => 'webservice', 'controller' => 'index', 'action' => 'ios-user-notification')));
        $router->addRoute('iosUpdateNotificationSeen', new Zend_Controller_Router_Route('webservice/ios-update-notification-seen', array('module' => 'webservice', 'controller' => 'index', 'action' => 'ios-update-notification-seen')));
        $router->addRoute('getPermissions', new Zend_Controller_Router_Route('webservice/get-permissions', array('module' => 'webservice', 'controller' => 'index', 'action' => 'get-permissions')));
        $router->addRoute('saveBookingsOffline', new Zend_Controller_Router_Route('webservice/save-bookings-offline', array('module' => 'webservice', 'controller' => 'index', 'action' => 'save-bookings-offline')));
        $router->addRoute('contractorServices', new Zend_Controller_Router_Route('webservice/contractor-services', array('module' => 'webservice', 'controller' => 'index', 'action' => 'contractor-services')));
        $router->addRoute('counts', new Zend_Controller_Router_Route('webservice/get-counts', array('module' => 'webservice', 'controller' => 'index', 'action' => 'get-counts')));
        $router->addRoute('disablecontractorServices', new Zend_Controller_Router_Route('webservice/disable-contractor-service', array('module' => 'webservice', 'controller' => 'index', 'action' => 'disable-contractor-service')));


        $router->addRoute('getfaqs', new Zend_Controller_Router_Route('webservice/get-faqs', array('module' => 'webservice', 'controller' => 'index', 'action' => 'get-faqs')));

        $router->addRoute('iosBookingDistance', new Zend_Controller_Router_Route('webservice/ios-booking-distance', array('module' => 'webservice', 'controller' => 'index', 'action' => 'ios-booking-distance')));
        $router->addRoute('receiveEmailsParameters', new Zend_Controller_Router_Route('webservice/receive-emails-parameters', array('module' => 'webservice', 'controller' => 'index', 'action' => 'receive-emails-parameters')));
        $router->addRoute('updateSentEmails', new Zend_Controller_Router_Route('webservice/update-sent-emails', array('module' => 'webservice', 'controller' => 'index', 'action' => 'update-sent-emails')));

        $router->addRoute('getComplaintTypes', new Zend_Controller_Router_Route('webservice/get-complaint-types', array('module' => 'webservice', 'controller' => 'index', 'action' => 'get-complaint-types')));

        $router->addRoute('contractorServiceCitiesAvailablitiy', new Zend_Controller_Router_Route('webservice/contractor-service-cities-availablitiy', array('module' => 'webservice', 'controller' => 'index', 'action' => 'contractor-service-cities-availablitiy')));

        $router->addRoute('checkInOut', new Zend_Controller_Router_Route('webservice/check-in-out', array('module' => 'webservice', 'controller' => 'index', 'action' => 'check-in-out')));

        $router->addRoute('changeBookingTime', new Zend_Controller_Router_Route('webservice/change-booking-time', array('module' => 'webservice', 'controller' => 'index', 'action' => 'change-booking-time')));

        $router->addRoute('deleteAttachment', new Zend_Controller_Router_Route('webservice/delete-attachment', array('module' => 'webservice', 'controller' => 'index', 'action' => 'delete-attachment')));

        $router->addRoute('saveCallLog', new Zend_Controller_Router_Route('webservice/save-call-log', array('module' => 'webservice', 'controller' => 'index', 'action' => 'save-call-log')));

        $router->addRoute('markAllContractorCommentsAsSeen', new Zend_Controller_Router_Route('webservice/mark-all-contractor-comments-as-seen', array('module' => 'webservice', 'controller' => 'index', 'action' => 'mark-all-contractor-comments-as-seen')));


        $router->addRoute('unavailableTimeWebService', new Zend_Controller_Router_Route('webservice/unavailable-time', array('module' => 'webservice', 'controller' => 'index', 'action' => 'unavailable-time')));
        $router->addRoute('allunavailableTimeWebService', new Zend_Controller_Router_Route('webservice/get-all-unavailable-event', array('module' => 'webservice', 'controller' => 'index', 'action' => 'get-all-unavailable-event')));

        $router->addRoute('deleteUnavailableTimeWebService', new Zend_Controller_Router_Route('webservice/delete-unavailable-event', array('module' => 'webservice', 'controller' => 'index', 'action' => 'delete-unavailable-event')));

        $router->addRoute('convertStatusWebService', new Zend_Controller_Router_Route('webservice/change-complaint-status', array('module' => 'webservice', 'controller' => 'index', 'action' => 'change-complaint-status')));

        $router->addRoute('sendEwayPaymentDetails', new Zend_Controller_Router_Route('webservice/send-eway-payment-details', array('module' => 'webservice', 'controller' => 'index', 'action' => 'send-eway-payment-details')));



        $router->addRoute('imageUpload', new Zend_Controller_Router_Route('webservice/image-upload', array('module' => 'webservice', 'controller' => 'index', 'action' => 'image-upload')));

        $router->addRoute('getPhotos', new Zend_Controller_Router_Route('webservice/get-photos', array('module' => 'webservice', 'controller' => 'index', 'action' => 'get-photos')));


        $router->addRoute('getImageTags', new Zend_Controller_Router_Route('webservice/get-image-tags', array('module' => 'webservice', 'controller' => 'index', 'action' => 'get-image-tags')));

        $router->addRoute('getServices', new Zend_Controller_Router_Route('webservice/get-services', array('module' => 'webservice', 'controller' => 'index', 'action' => 'get-services')));


        $router->addRoute('iosUpdateNotificationRead', new Zend_Controller_Router_Route('webservice/ios-update-notification-read', array('module' => 'webservice', 'controller' => 'index', 'action' => 'ios-update-notification-read')));

        $router->addRoute('getEmailContent', new Zend_Controller_Router_Route('webservice/get-email-content', array('module' => 'webservice', 'controller' => 'index', 'action' => 'get-email-content')));

        $router->addRoute('sendEmailFromApp', new Zend_Controller_Router_Route('webservice/send-email-from-app', array('module' => 'webservice', 'controller' => 'index', 'action' => 'send-email-from-app')));

        $router->addRoute('changeComplaintStatus', new Zend_Controller_Router_Route('webservice/change-complaint-status', array('module' => 'webservice', 'controller' => 'index', 'action' => 'change-complaint-status')));

        $router->addRoute('editInvoiceNum', new Zend_Controller_Router_Route('webservice/edit-invoice-num', array('module' => 'webservice', 'controller' => 'index', 'action' => 'edit-invoice-num')));

        $router->addRoute('getAllRejectQuestions', new Zend_Controller_Router_Route('webservice/get-all-reject-questions', array('module' => 'webservice', 'controller' => 'index', 'action' => 'get-all-reject-questions')));

        $router->addRoute('getAllRejectQuestions', new Zend_Controller_Router_Route('webservice/get-all-reject-questions', array('module' => 'webservice', 'controller' => 'index', 'action' => 'get-all-reject-questions')));

        $router->addRoute('getAllDiscussion', new Zend_Controller_Router_Route('webservice/get-all-discussion', array('module' => 'webservice', 'controller' => 'index', 'action' => 'get-all-discussion')));
        
         $router->addRoute('getAllDiscussionByContractor', new Zend_Controller_Router_Route('webservice/get-all-discussion-by-contractor', array('module' => 'webservice', 'controller' => 'index', 'action' => 'get-all-discussion-by-contractor')));

        $router->addRoute('addDiscussion', new Zend_Controller_Router_Route('webservice/add-discussion', array('module' => 'webservice', 'controller' => 'index', 'action' => 'add-discussion')));

        $router->addRoute('getAllImageDiscussion', new Zend_Controller_Router_Route('webservice/get-all-image-discussion', array('module' => 'webservice', 'controller' => 'index', 'action' => 'get-all-image-discussion')));

        $router->addRoute('addDiscussionForIos', new Zend_Controller_Router_Route('webservice/add-discussion-for-ios', array('module' => 'webservice', 'controller' => 'index', 'action' => 'add-discussion-for-ios')));

        $router->addRoute('answerRejectQuestions', new Zend_Controller_Router_Route('webservice/answer-reject-questions', array('module' => 'webservice', 'controller' => 'index', 'action' => 'answer-reject-questions')));

        $router->addRoute('addEditRefund', new Zend_Controller_Router_Route('webservice/add-edit-refund', array('module' => 'webservice', 'controller' => 'index', 'action' => 'add-edit-refund')));

        $router->addRoute('deleteRefund', new Zend_Controller_Router_Route('webservice/delete-refund', array('module' => 'webservice', 'controller' => 'index', 'action' => 'delete-refund')));

        $router->addRoute('profile', new Zend_Controller_Router_Route('webservice/profile', array('module' => 'webservice', 'controller' => 'index', 'action' => 'profile')));
        $router->addRoute('deleteContractorVehicle', new Zend_Controller_Router_Route('webservice/delete-contractor-vehicle', array('module' => 'webservice', 'controller' => 'index', 'action' => 'delete-contractor-vehicle')));
        $router->addRoute('addContractorVehicle', new Zend_Controller_Router_Route('webservice/add-contractor-vehicle', array('module' => 'webservice', 'controller' => 'index', 'action' => 'add-contractor-vehicle')));

        $router->addRoute('deleteContractorInsurance', new Zend_Controller_Router_Route('webservice/delete-contractor-insurance', array('module' => 'webservice', 'controller' => 'index', 'action' => 'delete-contractor-insurance')));
        $router->addRoute('addEditContractorInsurance', new Zend_Controller_Router_Route('webservice/add-edit-contractor-insurance', array('module' => 'webservice', 'controller' => 'index', 'action' => 'add-edit-contractor-insurance')));

        $router->addRoute('addEditContractorOwnerEmployee', new Zend_Controller_Router_Route('webservice/add-edit-contractor-owner-employee', array('module' => 'webservice', 'controller' => 'index', 'action' => 'add-edit-contractor-owner-employee')));
        $router->addRoute('deleteContractorOwner', new Zend_Controller_Router_Route('webservice/delete-contractor-owner', array('module' => 'webservice', 'controller' => 'index', 'action' => 'delete-contractor-owner')));
        $router->addRoute('contractorOwnerEmployeePhotoUpload', new Zend_Controller_Router_Route('webservice/contractor-owner-employee-photo-upload', array('module' => 'webservice', 'controller' => 'index', 'action' => 'contractor-owner-employee-photo-upload')));
        $router->addRoute('deleteContractorEmployee', new Zend_Controller_Router_Route('webservice/delete-contractor-employee', array('module' => 'webservice', 'controller' => 'index', 'action' => 'delete-contractor-employee')));
        $router->addRoute('deleteContractorEquipment', new Zend_Controller_Router_Route('webservice/delete-contractor-equipment', array('module' => 'webservice', 'controller' => 'index', 'action' => 'delete-contractor-equipment')));
        $router->addRoute('addContractorEquipment', new Zend_Controller_Router_Route('webservice/add-contractor-equipment', array('module' => 'webservice', 'controller' => 'index', 'action' => 'add-contractor-equipment')));
        $router->addRoute('addContractorChemicals', new Zend_Controller_Router_Route('webservice/add-contractor-chemicals', array('module' => 'webservice', 'controller' => 'index', 'action' => 'add-contractor-chemicals')));
        $router->addRoute('deleteContractorChemicals', new Zend_Controller_Router_Route('webservice/delete-contractor-chemicals', array('module' => 'webservice', 'controller' => 'index', 'action' => 'delete-contractor-chemicals')));
        $router->addRoute('deleteContractorApparatus', new Zend_Controller_Router_Route('webservice/delete-contractor-apparatus', array('module' => 'webservice', 'controller' => 'index', 'action' => 'delete-contractor-Apparatus')));
        $router->addRoute('addContractorApparatus', new Zend_Controller_Router_Route('webservice/add-contractor-apparatus', array('module' => 'webservice', 'controller' => 'index', 'action' => 'add-contractor-Apparatus')));
        $router->addRoute('changeContractorPassword', new Zend_Controller_Router_Route('webservice/change-contractor-password', array('module' => 'webservice', 'controller' => 'index', 'action' => 'change-contractor-password')));
        $router->addRoute('changeContractorEmail', new Zend_Controller_Router_Route('webservice/change-contractor-email', array('module' => 'webservice', 'controller' => 'index', 'action' => 'change-contractor-email')));
        $router->addRoute('uploadContractorPhoto', new Zend_Controller_Router_Route('webservice/upload-contractor-photo', array('module' => 'webservice', 'controller' => 'index', 'action' => 'upload-contractor-photo')));
        $router->addRoute('getState', new Zend_Controller_Router_Route('webservice/get-state', array('module' => 'webservice', 'controller' => 'index', 'action' => 'get-state')));
        $router->addRoute('getCities', new Zend_Controller_Router_Route('webservice/get-cities', array('module' => 'webservice', 'controller' => 'index', 'action' => 'get-cities')));
        $router->addRoute('getCountries', new Zend_Controller_Router_Route('webservice/get-countries', array('module' => 'webservice', 'controller' => 'index', 'action' => 'get-countries')));
        $router->addRoute('getCompanies', new Zend_Controller_Router_Route('webservice/get-companies', array('module' => 'webservice', 'controller' => 'index', 'action' => 'get-companies')));
        $router->addRoute('signUp', new Zend_Controller_Router_Route('webservice/sign-up', array('module' => 'webservice', 'controller' => 'index', 'action' => 'sign-up')));
        $router->addRoute('changeContractorInfo', new Zend_Controller_Router_Route('webservice/change-contractor-info', array('module' => 'webservice', 'controller' => 'index', 'action' => 'change-contractor-info')));
        $router->addRoute('changeContractorAccount', new Zend_Controller_Router_Route('webservice/change-contractor-account', array('module' => 'webservice', 'controller' => 'index', 'action' => 'change-contractor-account')));
        $router->addRoute('changeContractorStatus', new Zend_Controller_Router_Route('webservice/change-contractor-status', array('module' => 'webservice', 'controller' => 'index', 'action' => 'change-contractor-status')));

        //by walaa
        $router->addRoute('deleteDiscussion', new Zend_Controller_Router_Route('webservice/delete-discussion', array('module' => 'webservice', 'controller' => 'index', 'action' => 'delete-discussion')));

        $router->addRoute('contractorBookingAttendance', new Zend_Controller_Router_Route('webservice/contractor-booking-attendance', array('module' => 'webservice', 'controller' => 'index', 'action' => 'contractor-booking-attendance')));

        $router->addRoute('uploadCustomerSignatureImage', new Zend_Controller_Router_Route('webservice/upload-customer-signature-image', array('module' => 'webservice', 'controller' => 'index', 'action' => 'upload-customer-signature-image')));

        $router->addRoute('addNewBookingDateByCustomer', new Zend_Controller_Router_Route('webservice/add-new-booking-date-by-customer', array('module' => 'webservice', 'controller' => 'index', 'action' => 'add-new-booking-date-by-customer')));

        $router->addRoute('customerComplaints', new Zend_Controller_Router_Route('webservice/customer-complaints', array('module' => 'webservice', 'controller' => 'index', 'action' => 'customer-complaints')));

        ///End By Walaa

        $router->addRoute('sendSmsContractor', new Zend_Controller_Router_Route('sms/contractors', array('module' => 'contractor', 'controller' => 'index', 'action' => 'send-sms-contractor')));
        $router->addRoute('sendSmsForContractor', new Zend_Controller_Router_Route('sms/contractor/:user_id', array('module' => 'contractor', 'controller' => 'index', 'action' => 'send-sms-for-contractor')));


        $router->addRoute('getCustomerTypes', new Zend_Controller_Router_Route('webservice/get-customer-types', array('module' => 'webservice', 'controller' => 'index', 'action' => 'get-customer-types')));

        $router->addRoute('getCustomerRate', new Zend_Controller_Router_Route('webservice/get-customer-rate', array('module' => 'webservice', 'controller' => 'index', 'action' => 'get-customer-rate')));

        $router->addRoute('getSystemSettings', new Zend_Controller_Router_Route('webservice/get-system-settings', array('module' => 'webservice', 'controller' => 'index', 'action' => 'get-system-settings')));



        $router->addRoute('saveCustomerRate', new Zend_Controller_Router_Route('webservice/save-customer-rate', array('module' => 'webservice', 'controller' => 'index', 'action' => 'save-customer-rate')));


        $router->addRoute('editCustomerInfo', new Zend_Controller_Router_Route('webservice/edit-customer-info', array('module' => 'webservice', 'controller' => 'index', 'action' => 'edit-customer-info')));

        $router->addRoute('getContractorLocation', new Zend_Controller_Router_Route('webservice/get-contractor-location', array('module' => 'webservice', 'controller' => 'index', 'action' => 'get-contractor-location')));

        $router->addRoute('saveContractorLocation', new Zend_Controller_Router_Route('webservice/save-contractor-location', array('module' => 'webservice', 'controller' => 'index', 'action' => 'save-contractor-location')));

        $router->addRoute('confirmBookingByCustomer', new Zend_Controller_Router_Route('webservice/confirm-booking-by-customer', array('module' => 'webservice', 'controller' => 'index', 'action' => 'confirm-booking-by-customer')));

        $router->addRoute('cancelConfirmBookingByCustomer', new Zend_Controller_Router_Route('webservice/cancel-confirm-booking-by-customer', array('module' => 'webservice', 'controller' => 'index', 'action' => 'confirm-booking-by-customer')));


        $router->addRoute('cancelBookingByCustomer', new Zend_Controller_Router_Route('webservice/cancel-booking-by-customer', array('module' => 'webservice', 'controller' => 'index', 'action' => 'cancel-booking-by-customer')));

        $router->addRoute('addComplaintByCustomer', new Zend_Controller_Router_Route('webservice/add-complaint-by-customer', array('module' => 'webservice', 'controller' => 'index', 'action' => 'add-complaint-by-customer')));

        $router->addRoute('getWeatherForecast', new Zend_Controller_Router_Route('webservice/get-weather-forecast', array('module' => 'webservice', 'controller' => 'index', 'action' => 'get-weather-forecast')));


        $router->addRoute('checkPayment', new Zend_Controller_Router_Route('webservice/check-payment', array('module' => 'webservice', 'controller' => 'index', 'action' => 'check-payment')));
        $router->addRoute('contractorDiscussionForMobile', new Zend_Controller_Router_Route('webservice/contractor-discussion-for-mobile', array('module' => 'webservice', 'controller' => 'index', 'action' => 'contractor-discussion-for-mobile')));

        $router->addRoute('getSystemSettings', new Zend_Controller_Router_Route('webservice/get-system-settings', array('module' => 'webservice', 'controller' => 'index', 'action' => 'get-system-settings')));

        $router->addRoute('addBookingByCustomer', new Zend_Controller_Router_Route('webservice/add-booking', array('module' => 'webservice', 'controller' => 'index', 'action' => 'add-booking')));

        $router->addRoute('getCalendarContent', new Zend_Controller_Router_Route('webservice/get-calendar-content', array('module' => 'webservice', 'controller' => 'index', 'action' => 'get-calendar-content')));

        $router->addRoute('getContractorsWithUnavailableTime', new Zend_Controller_Router_Route('webservice/get-contractors-with-unavailable-time', array('module' => 'webservice', 'controller' => 'index', 'action' => 'get-contractors-with-unavailable-time')));

        $router->addRoute('getRatingDetailsForBooking', new Zend_Controller_Router_Route('webservice/get-rating-details-for-booking', array('module' => 'webservice', 'controller' => 'index', 'action' => 'get-rating-details-for-booking')));
        //by salim check app version 

        $router->addRoute('checkAppVersion', new Zend_Controller_Router_Route('webservice/check-app-version', array('module' => 'webservice', 'controller' => 'index', 'action' => 'check-app-version')));

        $router->addRoute('getFloorValues', new Zend_Controller_Router_Route('getFloorValues', array('module' => 'default', 'controller' => 'common', 'action' => 'get-floor-dropdown')));
        $router->addRoute('emailTest', new Zend_Controller_Router_Route('email-test', array('module' => 'booking', 'controller' => 'index', 'action' => 'email-test')));


        $router->addRoute('getTabsData', new Zend_Controller_Router_Route('default/get-tabs-data', array('module' => 'default', 'controller' => 'common', 'action' => 'get-tabs-data')));

        $router->addRoute('saveBookingInSession', new Zend_Controller_Router_Route('default/save-booking-in-session', array('module' => 'default', 'controller' => 'common', 'action' => 'save-booking-in-session')));

        $router->addRoute('sliderImage', new Zend_Controller_Router_Route('slide_image', array('module' => 'default', 'controller' => 'index', 'action' => 'slider-image')));




        $router->addRoute('getTabsCount', new Zend_Controller_Router_Route('default/get-tabs-count', array('module' => 'default', 'controller' => 'common', 'action' => 'get-tabs-count')));

        //FAQ Reoutes Added By Salim 1/12/2015
        $router->addRoute('faq', new Zend_Controller_Router_Route('faqs', array('module' => 'settings', 'controller' => 'faq', 'action' => 'index')));
        $router->addRoute('addFaq', new Zend_Controller_Router_Route('faq/add', array('module' => 'settings', 'controller' => 'faq', 'action' => 'add')));
        $router->addRoute('editFaq', new Zend_Controller_Router_Route('faq/edit/:id', array('module' => 'settings', 'controller' => 'faq', 'action' => 'edit')));
        $router->addRoute('deleteFaq', new Zend_Controller_Router_Route('faq/delete', array('module' => 'settings', 'controller' => 'faq', 'action' => 'delete')));
        $router->addRoute('addServiceFloor', new Zend_Controller_Router_Route('faq/addServiceFloor/:faq_id', array('module' => 'settings', 'controller' => 'faq', 'action' => 'add-service-floor')));
        $router->addRoute('deleteServiceFloor', new Zend_Controller_Router_Route('faq/deleteServiceFloor', array('module' => 'settings', 'controller' => 'faq', 'action' => 'delete-service-floor')));
        //Rating System By Salim 15/12/2015
        $router->addRoute('customerRate', new Zend_Controller_Router_Route('customer-rating/:type/:item_id', array('module' => 'default', 'controller' => 'rate', 'action' => 'customer-rate')));
        $router->addRoute('customerRateEmail', new Zend_Controller_Router_Route('customerRating/:type/:item_id', array('module' => 'default', 'controller' => 'rate', 'action' => 'customer-rate-email')));

        $router->addRoute('rateTag', new Zend_Controller_Router_Route('settings/rate-tag', array('module' => 'settings', 'controller' => 'rate-tag', 'action' => 'index')));
        $router->addRoute('rateTagAdd', new Zend_Controller_Router_Route('settings/rate-tag/add', array('module' => 'settings', 'controller' => 'rate-tag', 'action' => 'add')));
        $router->addRoute('rateTagEdit', new Zend_Controller_Router_Route('settings/rate-tag/edit/:id', array('module' => 'settings', 'controller' => 'rate-tag', 'action' => 'edit')));
        $router->addRoute('rateTagDelete', new Zend_Controller_Router_Route('settings/rate-tag/delete/:id', array('module' => 'settings', 'controller' => 'rate-tag', 'action' => 'delete')));
        /////By Walaa
        $router->addRoute('settingsEmailTemplateGetAttachment', new Zend_Controller_Router_Route('settings/email-template/get-attachments/:email_template_id/', array('module' => 'settings', 'controller' => 'email-template', 'action' => 'get-attachments')));


        $router->addRoute('allTabsDiscussion', new Zend_Controller_Router_Route('tabs/discussion/all/', array('module' => 'booking', 'controller' => 'discussion', 'action' => 'get-all-tabs-discussion')));

        $router->addRoute('updateDeletedBookingComment', new Zend_Controller_Router_Route('booking/discussion/updateDeletedComment/', array('module' => 'booking', 'controller' => 'discussion', 'action' => 'update-deleted-comment')));

        $router->addRoute('updateDeletedComplaintComment', new Zend_Controller_Router_Route('complaint/discussion/updateDeletedComment/', array('module' => 'complaint', 'controller' => 'discussion', 'action' => 'update-deleted-comment')));

        $router->addRoute('updateDeletedEstimateComment', new Zend_Controller_Router_Route('estimate/discussion/updateDeletedComment/', array('module' => 'estimates', 'controller' => 'discussion', 'action' => 'update-deleted-comment')));

        $router->addRoute('updateDeletedInquiryComment', new Zend_Controller_Router_Route('inquiry/discussion/updateDeletedComment/', array('module' => 'inquiry', 'controller' => 'discussion', 'action' => 'update-deleted-comment')));

        $router->addRoute('updateDeletedInvoiceComment', new Zend_Controller_Router_Route('invoices/discussion/updateDeletedComment/', array('module' => 'invoices', 'controller' => 'discussion', 'action' => 'update-deleted-comment')));

        ///subscription
        /*         * **** Subscription C.P By IBM ********* */
        $router->addRoute('completeCompanyContact', new Zend_Controller_Router_Route('complete-Company-Contact', array('module' => 'subscription', 'controller' => 'index', 'action' => 'complete-company-contact')));

        $router->addRoute('signupwithoutid', new Zend_Controller_Router_Route('signup', array('module' => 'subscription', 'controller' => 'index', 'action' => 'index')));
        $router->addRoute('completeCompanyInformation', new Zend_Controller_Router_Route('complete-Company-Information', array('module' => 'subscription', 'controller' => 'index', 'action' => 'complete-company-information')));

        $router->addRoute('completeCompanyContact', new Zend_Controller_Router_Route('complete-Company-Contact', array('module' => 'subscription', 'controller' => 'index', 'action' => 'complete-company-contact')));

        $router->addRoute('completeCompanyAddress', new Zend_Controller_Router_Route('complete-Company-Address', array('module' => 'subscription', 'controller' => 'index', 'action' => 'complete-company-address')));
        $router->addRoute('settingCompanyPicUpload', new Zend_Controller_Router_Route('settings/company/upload', array('module' => 'default', 'controller' => 'index', 'action' => 'company-pic')));
        $router->addRoute('completeCompanyInformation', new Zend_Controller_Router_Route('complete-Company-Information', array('module' => 'subscription', 'controller' => 'index', 'action' => 'complete-company-information')));
        $router->addRoute('employeeLogin', new Zend_Controller_Router_Route('subscription', array('module' => 'subscription', 'controller' => 'employee', 'action' => 'index')));
        $router->addRoute('employeelogout', new Zend_Controller_Router_Route('subscription/logout', array('module' => 'subscription', 'controller' => 'employee', 'action' => 'logout')));
        $router->addRoute('plan', new Zend_Controller_Router_Route('subscription/plan', array('module' => 'subscription', 'controller' => 'plan', 'action' => 'index')));
        $router->addRoute('addPlan', new Zend_Controller_Router_Route('subscription/plan/add', array('module' => 'subscription', 'controller' => 'plan', 'action' => 'add')));
        $router->addRoute('editPlan', new Zend_Controller_Router_Route('subscription/plan/edit/:plan_id', array('module' => 'subscription', 'controller' => 'plan', 'action' => 'edit')));
        $router->addRoute('viewPlan', new Zend_Controller_Router_Route('subscription/plan/view/:plan_id', array('module' => 'subscription', 'controller' => 'plan', 'action' => 'view')));


        $router->addRoute('planTypeList', new Zend_Controller_Router_Route('subscription/plan-type', array('module' => 'subscription', 'controller' => 'plan-type', 'action' => 'index')));
        $router->addRoute('addPlanType', new Zend_Controller_Router_Route('subscription/planType/add', array('module' => 'subscription', 'controller' => 'plan-type', 'action' => 'add')));
        $router->addRoute('editPlanType', new Zend_Controller_Router_Route('subscription/planType/edit/:plan_type_id', array('module' => 'subscription', 'controller' => 'plan-type', 'action' => 'edit')));
        $router->addRoute('deletePlanType', new Zend_Controller_Router_Route('subscription/planType/delete/:plan_type_id', array('module' => 'subscription', 'controller' => 'plan-type', 'action' => 'delete')));
        $router->addRoute('planTypeCredentialList', new Zend_Controller_Router_Route('subscription/plan-type-credential/:plan_type_id', array('module' => 'subscription', 'controller' => 'plan-type-credential', 'action' => 'index')));

        $router->addRoute('coutriesjson', new Zend_Controller_Router_Route('coutriesjson', array('module' => 'subscription', 'controller' => 'index', 'action' => 'coutries-json')));
        $router->addRoute('testtest5', new Zend_Controller_Router_Route('testtttt', array('module' => 'default', 'controller' => 'index', 'action' => 'test-system')));
        $router->addRoute('gottestt', new Zend_Controller_Router_Route('gottestt', array('module' => 'subscription', 'controller' => 'index', 'action' => 'for-test')));
        $router->addRoute('signupupwithcode', new Zend_Controller_Router_Route(':id/:code/signup', array('module' => 'subscription', 'controller' => 'index', 'action' => 'signup-code')));
        $router->addRoute('signup', new Zend_Controller_Router_Route('sign-up/:id', array('module' => 'subscription', 'controller' => 'index', 'action' => 'index')));
        $router->addRoute('signupupdate', new Zend_Controller_Router_Route('sign-up/:id/:update', array('module' => 'subscription', 'controller' => 'index', 'action' => 'index')));

        //$router->addRoute('settingsUserApproveAddUserPayment', new Zend_Controller_Router_Route('settings/user/add/:isApproved', array('module' => 'settings', 'controller' => 'user', 'action' => 'add')));
        //$router->addRoute('forcheckaddorpaytousers', new Zend_Controller_Router_Route('forcheckaddorpaytousers', array('module' => 'settings', 'controller' => 'user', 'action' => 'check-add-pay')));
        //$router->addRoute('upgradetousers', new Zend_Controller_Router_Route('upgradetousers', array('module' => 'settings', 'controller' => 'user', 'action' => 'go-upgrade')));
        //$router->addRoute('calculateRealAccountSubscription', new Zend_Controller_Router_Route('subscription/claculate/', array('module' => 'subscription', 'controller' => 'index', 'action' => 'add-user-change')));
        //$router->addRoute('calculateRealChangeAccountSubscription', new Zend_Controller_Router_Route('subscription/claculateChangePlan/', array('module' => 'subscription', 'controller' => 'index', 'action' => 'change-plan-view')));
        $router->addRoute('changePlanSubscription', new Zend_Controller_Router_Route('subscription/Payment/', array('module' => 'subscription', 'controller' => 'index', 'action' => 'change-plan')));
        $router->addRoute('CheckAvailabilityTwilioNumber', new Zend_Controller_Router_Route('subscription/AjaxAvailable/:id', array('module' => 'subscription', 'controller' => 'index', 'action' => 'check-available-twilio-number')));
        $router->addRoute('changePlanTrialSubscription', new Zend_Controller_Router_Route('subscription/ChangePlanTrial/', array('module' => 'subscription', 'controller' => 'index', 'action' => 'change-plan-trial')));
        $router->addRoute('checkMaxUserForSelectedPlan', new Zend_Controller_Router_Route('subscription/CheckMaxUserForSelected/', array('module' => 'subscription', 'controller' => 'index', 'action' => 'check-max-for-selected-plan')));
        //$router->addRoute('CheckAvailabilityTwilioNumber', new Zend_Controller_Router_Route('subscription/CheckTwilioNumber/', array('module' => 'subscription', 'controller' => 'index', 'action' => 'check-available-twilio-number')));
        //$router->addRoute('calculateRealAccountSubscriptionSubbb', new Zend_Controller_Router_Route('subscription/claculate/:isSubscribe', array('module' => 'subscription', 'controller' => 'index', 'action' => 'add-user-change')));
        //$router->addRoute('changePlanMessegeRealAccount', new Zend_Controller_Router_Route('subscription/informmsg/:isGreaterThan', array('module' => 'subscription', 'controller' => 'index', 'action' => 'change-plan-messege')));
        $router->addRoute('approvePaymentSubscription', new Zend_Controller_Router_Route('subscription/approvePaymentSubscription/:newPlanId/:isApprove/:isGreaterThan', array('module' => 'subscription', 'controller' => 'index', 'action' => 'approve-payment')));

        $router->addRoute('testemail', new Zend_Controller_Router_Route('testemail', array('module' => 'subscription', 'controller' => 'index', 'action' => 'testemail')));
        $router->addRoute('signupemailsent', new Zend_Controller_Router_Route('signupemailsent', array('module' => 'subscription', 'controller' => 'index', 'action' => 'sentemail')));
        $router->addRoute('checkvalidateaccount', new Zend_Controller_Router_Route('checkvalidateaccount/:email/:hashcode', array('module' => 'subscription', 'controller' => 'index', 'action' => 'check-validate-account')));
        $router->addRoute('pagesubscription', new Zend_Controller_Router_Route('subscription-page', array('module' => 'subscription', 'controller' => 'index', 'action' => 'subscription-page')));
        $router->addRoute('panelFirstLogin', new Zend_Controller_Router_Route('panel-first-login', array('module' => 'subscription', 'controller' => 'index', 'action' => 'panel-first-login')));
        $router->addRoute('panellastone', new Zend_Controller_Router_Route('panel-settings', array('module' => 'subscription', 'controller' => 'index', 'action' => 'panel-settings')));
        $router->addRoute('panelSecondCom', new Zend_Controller_Router_Route('panel-second-com', array('module' => 'subscription', 'controller' => 'index', 'action' => 'panel-second-com')));
        $router->addRoute('invitationform', new Zend_Controller_Router_Route('getinvitation/:email/:hashcode', array('module' => 'subscription', 'controller' => 'index', 'action' => 'get-invitation')));
        $router->addRoute('completeadminpanel', new Zend_Controller_Router_Route('complete-user-panel', array('module' => 'subscription', 'controller' => 'index', 'action' => 'complete-user-panel')));
        $router->addRoute('manageBillingInformation', new Zend_Controller_Router_Route('subscription-payment/billingInformation', array('module' => 'subscription', 'controller' => 'payment', 'action' => 'billing-information-view')));

        $router->addRoute('processBillingInformation', new Zend_Controller_Router_Route('subscription-payment/Process-Billing-Information', array('module' => 'subscription', 'controller' => 'payment', 'action' => 'process-billing-information-view')));

        $router->addRoute('completecompanypanel', new Zend_Controller_Router_Route('complete-company', array('module' => 'subscription', 'controller' => 'index', 'action' => 'complete-company')));

        $router->addRoute('completeadminaccount', new Zend_Controller_Router_Route('complete-admin-account', array('module' => 'subscription', 'controller' => 'index', 'action' => 'complete-admin-account')));

        $router->addRoute('checkcompleteness', new Zend_Controller_Router_Route('subscription/checkcomplete', array('module' => 'subscription', 'controller' => 'index', 'action' => 'check-complete')));
        $router->addRoute('checkcompletenessnext', new Zend_Controller_Router_Route('subscription/checkcompletenext', array('module' => 'subscription', 'controller' => 'index', 'action' => 'check-complete-next')));

        //added by mohamed subscription control panel   
        $router->addRoute('account', new Zend_Controller_Router_Route('subscription/account', array('module' => 'subscription', 'controller' => 'account', 'action' => 'index')));
        $router->addRoute('subscriptionView', new Zend_Controller_Router_Route('subscription/view/:id', array('module' => 'subscription', 'controller' => 'account', 'action' => 'view')));
        $router->addRoute('SuspendSubscription', new Zend_Controller_Router_Route('subscription/account/:id/:is_suspended', array('module' => 'subscription', 'controller' => 'account', 'action' => 'suspend')));
        $router->addRoute('changePlanMessege', new Zend_Controller_Router_Route('subscription/inform', array('module' => 'subscription', 'controller' => 'account', 'action' => 'change-plan-messege')));
        $router->addRoute('changePlan', new Zend_Controller_Router_Route('subscription/changePlan', array('module' => 'subscription', 'controller' => 'account', 'action' => 'change-plan')));
        $router->addRoute('subscriptionAdd', new Zend_Controller_Router_Route('subscription/add', array('module' => 'subscription', 'controller' => 'account', 'action' => 'add')));
        $router->addRoute('addUserSubscription', new Zend_Controller_Router_Route('subscription/addUser/:company_id', array('module' => 'subscription', 'controller' => 'account', 'action' => 'add-user')));
        $router->addRoute('viewUserSubscription', new Zend_Controller_Router_Route('subscription/viewUser/:user_id', array('module' => 'subscription', 'controller' => 'account', 'action' => 'view-user')));
        $router->addRoute('editUserSubscription', new Zend_Controller_Router_Route('subscription/editUser/:user_id', array('module' => 'subscription', 'controller' => 'account', 'action' => 'edit-user')));
        $router->addRoute('deleteUserSubscription', new Zend_Controller_Router_Route('subscription/deleteUser/:user_id', array('module' => 'subscription', 'controller' => 'account', 'action' => 'delete-user')));
        //$router->addRoute('calculateTotalAmountSubscription', new Zend_Controller_Router_Route('subscription/totalamount/', array('module' => 'subscription', 'controller' => 'account', 'action' => 'add-user-change')));
        //$router->addRoute('calculateTotalChangePlanSubscription', new Zend_Controller_Router_Route('subscription/changeplan/', array('module' => 'subscription', 'controller' => 'account', 'action' => 'change-plan-view')));
        $router->addRoute('toaddusers', new Zend_Controller_Router_Route('add-users-company', array('module' => 'subscription', 'controller' => 'index', 'action' => 'add-users-page')));
        //added by mohamed



        $router->addRoute('bookingContactHistoryAddReminder', new Zend_Controller_Router_Route('booking/contact_history/add/:booking_id', array('module' => 'booking', 'controller' => 'contact-history', 'action' => 'contact-history-add-reminder')));

        $router->addRoute('bookingContactHistoryDeleteReminder', new Zend_Controller_Router_Route('booking/contact_history/delete/:id/:contact_history_id', array('module' => 'booking', 'controller' => 'contact-history', 'action' => 'contact-history-delete-reminder')));

        $router->addRoute('inquiryReminderDeleteHistory', new Zend_Controller_Router_Route('inquiry/reminder/delete/:booking_id/:id/:reminder_id', array('module' => 'inquiry', 'controller' => 'reminder', 'action' => 'reminder-delete-history')));

        $router->addRoute('inquiryReminderAddHistory', new Zend_Controller_Router_Route('inquiry/reminder/add/:inquiry_id', array('module' => 'inquiry', 'controller' => 'reminder', 'action' => 'reminder-add-history')));
        $router->addRoute('bookingAttendancePerContractor', new Zend_Controller_Router_Route('booking-attendance-per-contractor', array('module' => 'default', 'controller' => 'booking-attendance', 'action' => 'index-contractor')));

        $router->addRoute('bookingReminderAdd_CustomerContact', new Zend_Controller_Router_Route('booking/reminder/add/:booking_id/:customer_contact_id', array('module' => 'booking', 'controller' => 'reminder', 'action' => 'customer-contact-reminder-add')));


        $router->addRoute('bookingReminderDelete_CustomerContact', new Zend_Controller_Router_Route('booking/reminder/delete/:id/:reminder_id/:customer_contact_id', array('module' => 'booking', 'controller' => 'reminder', 'action' => 'customer-contact-reminder-delete')));


        $router->addRoute('sendEmailToContractor', new Zend_Controller_Router_Route('email/contractor/:contractor_id/:type/:booking_id', array('module' => 'booking', 'controller' => 'index', 'action' => 'send-email-to-contractor')));
    }

    protected function _initAutoLoad() {

        $loader = new Zend_Application_Module_Autoloader(array(
            'namespace' => '',
            'basePath' => APPLICATION_PATH . '/modules/default'
        ));

        $autoloader = Zend_Loader_Autoloader::getInstance();
        $autoloader->pushAutoloader(new Zend_Application_Module_Autoloader(array(
            'namespace' => 'Settings_',
            'basePath' => APPLICATION_PATH . '/modules/settings'
        )));
        $autoloader->pushAutoloader(new Zend_Application_Module_Autoloader(array(
            'namespace' => 'Calendar_',
            'basePath' => APPLICATION_PATH . '/modules/calendar'
        )));
        $autoloader->pushAutoloader(new Zend_Application_Module_Autoloader(array(
            'namespace' => 'Booking_',
            'basePath' => APPLICATION_PATH . '/modules/booking'
        )));

        $autoloader->pushAutoloader(new Zend_Application_Module_Autoloader(array(
            'namespace' => 'Complaint_',
            'basePath' => APPLICATION_PATH . '/modules/complaint'
        )));

        $autoloader->pushAutoloader(new Zend_Application_Module_Autoloader(array(
            'namespace' => 'Reports_',
            'basePath' => APPLICATION_PATH . '/modules/reports'
        )));

        $autoloader->pushAutoloader(new Zend_Application_Module_Autoloader(array(
            'namespace' => 'Inquiry_',
            'basePath' => APPLICATION_PATH . '/modules/inquiry'
        )));

        $autoloader->pushAutoloader(new Zend_Application_Module_Autoloader(array(
            'namespace' => 'Estimates_',
            'basePath' => APPLICATION_PATH . '/modules/estimates'
        )));

        $autoloader->pushAutoloader(new Zend_Application_Module_Autoloader(array(
            'namespace' => 'Invoices_',
            'basePath' => APPLICATION_PATH . '/modules/invoices'
        )));

        $autoloader->pushAutoloader(new Zend_Application_Module_Autoloader(array(
            'namespace' => 'Customer_',
            'basePath' => APPLICATION_PATH . '/modules/customer'
        )));

        $autoloader->pushAutoloader(new Zend_Application_Module_Autoloader(array(
            'namespace' => 'Websites_',
            'basePath' => APPLICATION_PATH . '/modules/websites'
        )));

        $autoloader->pushAutoloader(new Zend_Application_Module_Autoloader(array(
            'namespace' => 'WebService_',
            'basePath' => APPLICATION_PATH . '/modules/webservice'
        )));
        $autoloader = Zend_Loader_Autoloader::getInstance();
        $autoloader->pushAutoloader(new Zend_Application_Module_Autoloader(array(
            'namespace' => 'Subscription_',
            'basePath' => APPLICATION_PATH . '/modules/subscription'
        )));

        Zend_Loader::loadClass('FilterLink', APPLICATION_PATH . '/../library');
        Zend_Loader::loadClass('SortOrder', APPLICATION_PATH . '/../library');
        Zend_Loader::loadClass('CheckAuth', APPLICATION_PATH . '/../library');
        Zend_Loader::loadClass('BreadCrumbs', APPLICATION_PATH . '/../library');
        Zend_Loader::loadClass('EmailNotification', APPLICATION_PATH . '/../library');
        Zend_Loader::loadClass('MobileNotification', APPLICATION_PATH . '/../library');
        Zend_Loader::loadClass('MobileNotificationNew', APPLICATION_PATH . '/../library');

        Zend_Loader::loadClass('ImageMagick', APPLICATION_PATH . '/../library');
        Zend_Loader::loadClass('ViewHelper', APPLICATION_PATH . '/../library');
        Zend_Loader::loadClass('GdataCalendar', APPLICATION_PATH . '/../library');
        Zend_Loader::loadClass('PHPExcel', APPLICATION_PATH . '/../library');


        Zend_Loader::loadFile('Client.php', APPLICATION_PATH . '/../library/google-api-php-client-master/src/Google');
        Zend_Loader::loadFile('Calendar.php', APPLICATION_PATH . '/../library/google-api-php-client-master/src/Google/Service');
        //Zend_Loader::loadFile('Gmail.php', APPLICATION_PATH . '/../library/google-api-php-client-master/src/Google/Service');
        Zend_Loader::loadFile('Twilio.php', APPLICATION_PATH . '/../library/twilio-php-master/Services');



        Zend_Loader::loadClass('GoogleMapAPI', APPLICATION_PATH . '/../library/GoogleMap');
        Zend_Loader::loadClass('JSMin', APPLICATION_PATH . '/../library/GoogleMap');


        Zend_Loader::loadFile('commonHelpers.php', APPLICATION_PATH . '/modules/default/views/helpers');
        Zend_Loader::loadFile('attributeFunctions.php', APPLICATION_PATH . '/modules/default/views/helpers');
        Zend_Loader::loadFile('wkhtmltopdf.php', APPLICATION_PATH . '/modules/default/views/helpers');

        //Rand
        //Zend_Loader::loadFile('Client.php', APPLICATION_PATH . '/../library/google-api-php-client/src/Google');
        //Zend_Loader::loadFile('autoload.php', APPLICATION_PATH . '/../library/google-api-php-client/vendor/');

        $frontController = Zend_Controller_Front::getInstance();
        $frontController->registerPlugin(new Plugin_UpdateCheck());

        return $loader;
    }

    protected function _initViewHelpers() {



        $this->bootstrap('layout');
        $layout = $this->getResource('layout');
        //$layout->setLayout('main');

        $view = $layout->getView();



        //ZendX_JQuery::enableView($view);
        $flashMessenger = new Zend_Controller_Action_Helper_FlashMessenger();
        $view->messages = $flashMessenger->getMessages();

        //$view->doctype('XHTML1_TRANSITIONAL');
        $view->headMeta()->appendHttpEquiv('Content-Type', 'text/html; charset=utf-8');

        //$view->headTitle()->setSeparator(' | ');
        //$view->headTitle('Cleaning Service');

        $view->page_title = "";

        BreadCrumbs::setModules(array('settings', 'booking', 'calendar'));
        BreadCrumbs::setSeparator('/');

        //setBaseUrl
        $httpHost = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : get_config('http_host');
        $httpHost = 'http://' . $httpHost;
        Zend_Controller_Front::getInstance()->setBaseUrl($httpHost);


        //
        // default Style Sheet and JavaScript
        //
        ViewHelper::setVerision(1);
        ViewHelper::setBaseUrl($httpHost);

        ////////
        /* $view->addHelperPath('ZendX/JQuery/View/Helper/', 'ZendX_JQuery_View_Helper');
          ZendX_JQuery_View_Helper_JQuery::enableNoConflictMode();
          $view->jQuery()->setLocalPath(ViewHelper::getBaseUrl().'/js/date picker jquery/jquery.js')
          ->setUiLocalPath(ViewHelper::getBaseUrl().'/js/date picker jquery/jquery-ui.min.js')
          ->addStyleSheet(ViewHelper::getBaseUrl().'/js/date picker jquery/jquery-ui.min.css');

          $view->jQuery()->enable()
          ->uiEnable(); */
        ////////
        //
        //ViewHelper::setStyleSheet('/js/jquery-ui-1.8.16.custom/css/redmond/jquery-ui-1.8.16.custom.css');
        //ViewHelper::setStyleSheet('/js/thickbox/thickbox.css');
        // by mona for text editor
        // photo gallary
        //ViewHelper::setStyleSheet('/css/SimpleSlider.css');
        ViewHelper::setStyleSheet('/css/SimpleSliderCustom.css');

        // by Abdallah for reposnive style
        ViewHelper::setStyleSheet('/css/bootstrap.css');
        ViewHelper::setStyleSheet('/css/font-awesome.css');
        ViewHelper::setStyleSheet('/css/fontello.css');
        ViewHelper::setStyleSheet('/css/style-new.css');
        ViewHelper::setStyleSheet('/css/animate.css');
        ViewHelper::setStyleSheet('/css/login.css');

        //ViewHelper::setStyleSheet('/css/summernote-bs3.css'); 
        ViewHelper::setStyleSheet('/css/summernote.css');
        ViewHelper::setStyleSheet('/css/datepicker3.css');
        ViewHelper::setStyleSheet('/css/bootstrap-timepicker.css');
        ViewHelper::setStyleSheet('/css/responsive-table.css');
        ViewHelper::setStyleSheet('/css/basic.css');
        ViewHelper::setStyleSheet('/css/dropzone.css');
        ViewHelper::setStyleSheet('/css/bootstrap-clockpicker.min.css');
        ViewHelper::setStyleSheet('/css/bootstrap-datetimepicker.min.css');
        ViewHelper::setStyleSheet('/css/switchery.css');

        ViewHelper::setStyleSheet('/css/activity-log.css');
        ViewHelper::setStyleSheet('/css/bootstrap-select.min.css');
        //By Mona
        ///End By Mona
        // JavaScript
        //
		ViewHelper::setJavaScript('/js/switchery.js');
        ViewHelper::setJavaScript('/js/jquery-2.1.1.js');
        ViewHelper::setJavaScript('/js/date picker jquery/jquery-ui.min.js');
        ViewHelper::setJavaScript('/js/inspinia.js');
        ViewHelper::setJavaScript('/js/bootstrap.min.js');
        ViewHelper::setJavaScript('/js/full_calendar/moment.min.js');
        ViewHelper::setJavaScript('/js/jquery.metisMenu.js');
        ViewHelper::setJavaScript('/js/pace.min.js');
        ViewHelper::setJavaScript('/js/classie.js');
        ViewHelper::setJavaScript('/js/bootstrap-tabdrop.js');
        //ViewHelper::setJavaScript('/js/thickbox/thickbox.js');
        ViewHelper::setJavaScript('/js/tiny_mce/tiny_mce.js');
        ViewHelper::setJavaScript('/js/js.js');
        // by mona for text editor
        ViewHelper::setJavaScript('/js/summernote.min.js');

        //by islam for table sort  
        ViewHelper::setJavaScript('/js/jquery.tablesorter.js');
        //for number format datepicker clocktime picker & datetimepicker 
        ViewHelper::setJavaScript('/js/jquery.format-1.3.min.js');
        ViewHelper::setJavaScript('/js/bootstrap-datepicker.js');
        ViewHelper::setJavaScript('/js/bootstrap-datetimepicker.js');
        ViewHelper::setJavaScript('/js/bootstrap-clockpicker.min.js');
        ViewHelper::setJavaScript('/js/nav-dropdown.js');
        //By Mona
        //ViewHelper::setJavaScript('/js/Am2_SimpleSlider.js');
        ViewHelper::setJavaScript('/plugins/dropzone/dropzone.js');
        ViewHelper::setJavaScript('/js/bootstrap-select.min.js');
        ViewHelper::setJavaScript('/js/jquery.form.js');

        //Rand
        ViewHelper::setStyleSheet('/js/material-datetimepicker/css/bootstrap-material-datetimepicker.css');
        ViewHelper::setJavaScript('/js/material-datetimepicker/js/bootstrap-material-datetimepicker.js');
        //ViewHelper::setJavaScript('/js/moment-master/moment.js');
    }

}
