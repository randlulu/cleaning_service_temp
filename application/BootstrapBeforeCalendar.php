<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {

    protected function _initRoutes() {


        //create a router
        $router = Zend_Controller_Front::getInstance()->getRouter();

        //not loged user
        $router->addRoute('pageNotFound', new Zend_Controller_Router_Route('page-not-found', array('module' => 'default', 'controller' => 'index', 'action' => 'page-not-found')));
        $router->addRoute('cancelBooking', new Zend_Controller_Router_Route('cancel/:cancel_hashcode/:booking_id/:status_id', array('module' => 'default', 'controller' => 'index', 'action' => 'cancel-booking')));
        $router->addRoute('feedbackBooking', new Zend_Controller_Router_Route('booking/feedback/:feedback_hashcode/:booking_id/:status_id', array('module' => 'default', 'controller' => 'index', 'action' => 'feedback-booking')));
        //
        //test
        //
        $router->addRoute('test', new Zend_Controller_Router_Route('test', array('module' => 'test', 'controller' => 'index', 'action' => 'index')));

        //
        // create settings routers
        //
        $router->addRoute('settings', new Zend_Controller_Router_Route('settings', array('module' => 'settings', 'controller' => 'settings', 'action' => 'index')));
        $router->addRoute('settingsLocation', new Zend_Controller_Router_Route('settings/location', array('module' => 'settings', 'controller' => 'settings', 'action' => 'settings-location')));
        $router->addRoute('settingsPayment', new Zend_Controller_Router_Route('settings/payment', array('module' => 'settings', 'controller' => 'settings', 'action' => 'settings-payment')));
        $router->addRoute('settingsUser', new Zend_Controller_Router_Route('settings/user', array('module' => 'settings', 'controller' => 'settings', 'action' => 'settings-user')));
        $router->addRoute('settingsServices', new Zend_Controller_Router_Route('settings/service', array('module' => 'settings', 'controller' => 'settings', 'action' => 'settings-services')));
        $router->addRoute('settingsType', new Zend_Controller_Router_Route('settings/type', array('module' => 'settings', 'controller' => 'settings', 'action' => 'settings-type')));
        $router->addRoute('settingsEmail', new Zend_Controller_Router_Route('settings/email', array('module' => 'settings', 'controller' => 'settings', 'action' => 'settings-email')));
        $router->addRoute('settingsLabel', new Zend_Controller_Router_Route('settings/label', array('module' => 'settings', 'controller' => 'settings', 'action' => 'settings-label')));

		$router->addRoute('settingsSecurity', new Zend_Controller_Router_Route('settings/security', array('module' => 'settings', 'controller' => 'settings', 'action' => 'settings-security')));
		$router->addRoute('settingsSecurityBannedIpsList', new Zend_Controller_Router_Route('settings/security_banned_ips', array('module' => 'settings', 'controller' => 'security-banned-ips', 'action' => 'index')));
        
        $router->addRoute('settingsBookingStatusList', new Zend_Controller_Router_Route('settings/booking-status', array('module' => 'settings', 'controller' => 'booking-status', 'action' => 'index')));
        $router->addRoute('settingsBookingStatusAdd', new Zend_Controller_Router_Route('settings/booking-status/add', array('module' => 'settings', 'controller' => 'booking-status', 'action' => 'add')));
        $router->addRoute('settingsBookingStatusEdit', new Zend_Controller_Router_Route('settings/booking-status/edit/:id', array('module' => 'settings', 'controller' => 'booking-status', 'action' => 'edit')));
        $router->addRoute('settingsBookingStatusDelete', new Zend_Controller_Router_Route('settings/booking-status/delete/:id', array('module' => 'settings', 'controller' => 'booking-status', 'action' => 'delete')));
        $router->addRoute('settingsBookingStatusDeleteBtn', new Zend_Controller_Router_Route('settings/booking-status/delete', array('module' => 'settings', 'controller' => 'booking-status', 'action' => 'delete')));
        $router->addRoute('settingsBookingStatusChangeColor', new Zend_Controller_Router_Route('settings/booking-status/change-color/:id', array('module' => 'settings', 'controller' => 'booking-status', 'action' => 'change-color')));
        $router->addRoute('settingsBookingStatusAddDescription', new Zend_Controller_Router_Route('settings/booking-status/add-desription/:id', array('module' => 'settings', 'controller' => 'booking-status', 'action' => 'add-description')));
        $router->addRoute('settingsBookingStatusShowDescription', new Zend_Controller_Router_Route('settings/booking-status/show-desription/:id', array('module' => 'settings', 'controller' => 'booking-status', 'action' => 'show-description')));
        $router->addRoute('settingsBookingStatusGoogleCalender', new Zend_Controller_Router_Route('settings/booking-status/google-calender/:id', array('module' => 'settings', 'controller' => 'booking-status', 'action' => 'google-calender')));

        $router->addRoute('settingsImageAttachmentList', new Zend_Controller_Router_Route('settings/image-attachment', array('module' => 'settings', 'controller' => 'image-attachment', 'action' => 'index')));
        $router->addRoute('settingsImageAttachmentAdd', new Zend_Controller_Router_Route('settings/image-attachment/add', array('module' => 'settings', 'controller' => 'image-attachment', 'action' => 'add')));
        $router->addRoute('settingsImageAttachmentEdit', new Zend_Controller_Router_Route('settings/image-attachment/edit/:id', array('module' => 'settings', 'controller' => 'image-attachment', 'action' => 'edit')));
        $router->addRoute('settingsImageAttachmentDescription', new Zend_Controller_Router_Route('settings/image-attachment/description/:id', array('module' => 'settings', 'controller' => 'image-attachment', 'action' => 'description')));
        $router->addRoute('settingsImageAttachmentDelete', new Zend_Controller_Router_Route('settings/image-attachment/delete/:id', array('module' => 'settings', 'controller' => 'image-attachment', 'action' => 'delete')));
        $router->addRoute('settingsImageAttachmentDeleteBtn', new Zend_Controller_Router_Route('settings/image-attachment/delete', array('module' => 'settings', 'controller' => 'image-attachment', 'action' => 'delete')));

        $router->addRoute('settingsCompaniesList', new Zend_Controller_Router_Route('settings/companies', array('module' => 'settings', 'controller' => 'companies', 'action' => 'index')));
        $router->addRoute('settingsCompaniesAdd', new Zend_Controller_Router_Route('settings/companies/add', array('module' => 'settings', 'controller' => 'companies', 'action' => 'add')));
        $router->addRoute('settingsCompaniesLogoUpload', new Zend_Controller_Router_Route('settings/companies/logo-upload/:id', array('module' => 'settings', 'controller' => 'companies', 'action' => 'logo-upload')));
        $router->addRoute('settingsCompaniesInvoiceLogoUpload', new Zend_Controller_Router_Route('settings/companies/invoice-logo-upload/:id', array('module' => 'settings', 'controller' => 'companies', 'action' => 'invoice-logo-upload')));
        $router->addRoute('settingsCompaniesEdit', new Zend_Controller_Router_Route('settings/companies/edit/:id', array('module' => 'settings', 'controller' => 'companies', 'action' => 'edit')));
        $router->addRoute('settingsCompaniesDelete', new Zend_Controller_Router_Route('settings/companies/delete/:id', array('module' => 'settings', 'controller' => 'companies', 'action' => 'delete')));
        $router->addRoute('settingsCompaniesDeleteBtn', new Zend_Controller_Router_Route('settings/companies/delete', array('module' => 'settings', 'controller' => 'companies', 'action' => 'delete')));
        $router->addRoute('settingsCompaniesSignatureLogoUpload', new Zend_Controller_Router_Route('settings/companies/signature-logo-upload/:id', array('module' => 'settings', 'controller' => 'companies', 'action' => 'signature-logo-upload')));

        $router->addRoute('settingsCompanyInvoiceNoteList', new Zend_Controller_Router_Route('settings/company-invoice-note/:company_id', array('module' => 'settings', 'controller' => 'company-invoice-note', 'action' => 'index')));
        $router->addRoute('settingsCompanyInvoiceNoteAdd', new Zend_Controller_Router_Route('settings/company-invoice-note/add/:company_id', array('module' => 'settings', 'controller' => 'company-invoice-note', 'action' => 'add')));
        $router->addRoute('settingsCompanyInvoiceNoteEdit', new Zend_Controller_Router_Route('settings/company-invoice-note/edit/:company_id/:id', array('module' => 'settings', 'controller' => 'company-invoice-note', 'action' => 'edit')));
        $router->addRoute('settingsCompanyInvoiceNoteDelete', new Zend_Controller_Router_Route('settings/company-invoice-note/delete/:company_id/:id', array('module' => 'settings', 'controller' => 'company-invoice-note', 'action' => 'delete')));
        $router->addRoute('settingsCompanyInvoiceNoteDeleteBtn', new Zend_Controller_Router_Route('settings/company-invoice-note/delete/:company_id', array('module' => 'settings', 'controller' => 'company-invoice-note', 'action' => 'delete')));

        $router->addRoute('settingsInquiryTypeList', new Zend_Controller_Router_Route('settings/inquiry-type', array('module' => 'settings', 'controller' => 'inquiry-type', 'action' => 'index')));
        $router->addRoute('settingsInquiryTypeAdd', new Zend_Controller_Router_Route('settings/inquiry-type/add', array('module' => 'settings', 'controller' => 'inquiry-type', 'action' => 'add')));
        $router->addRoute('settingsInquiryTypeEdit', new Zend_Controller_Router_Route('settings/inquiry-type/edit/:id', array('module' => 'settings', 'controller' => 'inquiry-type', 'action' => 'edit')));
        $router->addRoute('settingsInquiryTypeDelete', new Zend_Controller_Router_Route('settings/inquiry-type/delete/:id', array('module' => 'settings', 'controller' => 'inquiry-type', 'action' => 'delete')));
        $router->addRoute('settingsInquiryTypeDeleteBtn', new Zend_Controller_Router_Route('settings/inquiry-type/delete', array('module' => 'settings', 'controller' => 'inquiry-type', 'action' => 'delete')));

        $router->addRoute('settingsPropertyTypes', new Zend_Controller_Router_Route('settings/property-type', array('module' => 'settings', 'controller' => 'property-type', 'action' => 'index')));
        $router->addRoute('settingsPropertyTypeAdd', new Zend_Controller_Router_Route('settings/property-type/add', array('module' => 'settings', 'controller' => 'property-type', 'action' => 'add')));
        $router->addRoute('settingsPropertyTypeDelete', new Zend_Controller_Router_Route('settings/property-type/delete/:id', array('module' => 'settings', 'controller' => 'property-type', 'action' => 'delete')));
        $router->addRoute('settingsPropertyTypeEdit', new Zend_Controller_Router_Route('settings/property-type/edit/:id', array('module' => 'settings', 'controller' => 'property-type', 'action' => 'edit')));

        $router->addRoute('settingsInquiryRequiredTypes', new Zend_Controller_Router_Route('settings/required-type', array('module' => 'settings', 'controller' => 'inquiry-required-type', 'action' => 'index')));
        $router->addRoute('settingsInquiryRequiredTypeAdd', new Zend_Controller_Router_Route('settings/required-type/add', array('module' => 'settings', 'controller' => 'inquiry-required-type', 'action' => 'add')));
        $router->addRoute('settingsInquiryRequiredTypeDelete', new Zend_Controller_Router_Route('settings/required-type/delete/:id', array('module' => 'settings', 'controller' => 'inquiry-required-type', 'action' => 'delete')));
        $router->addRoute('settingsInquiryRequiredTypeEdit', new Zend_Controller_Router_Route('settings/required-type/edit/:id', array('module' => 'settings', 'controller' => 'inquiry-required-type', 'action' => 'edit')));

		///By Islam Update booking Qestions
		$router->addRoute('settingsBookingUpdateQuestions', new Zend_Controller_Router_Route('settings/booking-update-questions', array('module' => 'settings', 'controller' => 'booking-update-questions', 'action' => 'index')));
		$router->addRoute('settingsBookingUpdateQuestionsViewStatus', new Zend_Controller_Router_Route('settings/booking-update-questions/view-booking-status/:question_id', array('module' => 'settings', 'controller' => 'booking-update-questions', 'action' => 'view-booking-status')));
		$router->addRoute('settingsBookingUpdateQuestionsAdd', new Zend_Controller_Router_Route('settings/booking-update-questions/add-edit-update-question', array('module' => 'settings', 'controller' => 'booking-update-questions', 'action' => 'add-update-question')));
		$router->addRoute('settingsBookingUpdateQuestionsEdit', new Zend_Controller_Router_Route('settings/booking-update-questions/add-edit-update-question/:question_id', array('module' => 'settings', 'controller' => 'booking-update-questions', 'action' => 'edit-update-question')));
		$router->addRoute('settingsBookingUpdateQuestionsDelete', new Zend_Controller_Router_Route('settings/booking-update-questions/delete/:question_id', array('module' => 'settings', 'controller' => 'booking-update-questions', 'action' => 'delete-update-question')));
        
        $router->addRoute('settingsComplaintTypeList', new Zend_Controller_Router_Route('settings/complaint-type', array('module' => 'settings', 'controller' => 'complaint-type', 'action' => 'index')));
        $router->addRoute('settingsComplaintTypeAdd', new Zend_Controller_Router_Route('settings/complaint-type/add', array('module' => 'settings', 'controller' => 'complaint-type', 'action' => 'add')));
        $router->addRoute('settingsComplaintTypeEdit', new Zend_Controller_Router_Route('settings/complaint-type/edit/:id', array('module' => 'settings', 'controller' => 'complaint-type', 'action' => 'edit')));
        $router->addRoute('settingsComplaintTypeDelete', new Zend_Controller_Router_Route('settings/complaint-type/delete/:id', array('module' => 'settings', 'controller' => 'complaint-type', 'action' => 'delete')));
        $router->addRoute('settingsComplaintTypeDeleteBtn', new Zend_Controller_Router_Route('settings/complaint-type/delete', array('module' => 'settings', 'controller' => 'complaint-type', 'action' => 'delete')));

        $router->addRoute('settingsCountriesList', new Zend_Controller_Router_Route('settings/countries', array('module' => 'settings', 'controller' => 'countries', 'action' => 'index')));
        $router->addRoute('settingsCountriesAdd', new Zend_Controller_Router_Route('settings/countries/add', array('module' => 'settings', 'controller' => 'countries', 'action' => 'add')));
        $router->addRoute('settingsCountriesEdit', new Zend_Controller_Router_Route('settings/countries/edit/:id', array('module' => 'settings', 'controller' => 'countries', 'action' => 'edit')));
        $router->addRoute('settingsCountriesDelete', new Zend_Controller_Router_Route('settings/countries/delete/:id', array('module' => 'settings', 'controller' => 'countries', 'action' => 'delete')));
        $router->addRoute('settingsCountriesDeleteBtn', new Zend_Controller_Router_Route('settings/countries/delete', array('module' => 'settings', 'controller' => 'countries', 'action' => 'delete')));

        $router->addRoute('settingsCitiesList', new Zend_Controller_Router_Route('settings/cities', array('module' => 'settings', 'controller' => 'cities', 'action' => 'index')));
        $router->addRoute('settingsCitiesAdd', new Zend_Controller_Router_Route('settings/cities/add', array('module' => 'settings', 'controller' => 'cities', 'action' => 'add')));
        $router->addRoute('settingsCitiesEdit', new Zend_Controller_Router_Route('settings/cities/edit/:id', array('module' => 'settings', 'controller' => 'cities', 'action' => 'edit')));
        $router->addRoute('settingsCitiesDelete', new Zend_Controller_Router_Route('settings/cities/delete/:id', array('module' => 'settings', 'controller' => 'cities', 'action' => 'delete')));
        $router->addRoute('settingsCitiesDeleteBtn', new Zend_Controller_Router_Route('settings/cities/delete', array('module' => 'settings', 'controller' => 'cities', 'action' => 'delete')));

        $router->addRoute('settingsBankList', new Zend_Controller_Router_Route('settings/bank', array('module' => 'settings', 'controller' => 'bank', 'action' => 'index')));
        $router->addRoute('settingsBankAdd', new Zend_Controller_Router_Route('settings/bank/add', array('module' => 'settings', 'controller' => 'bank', 'action' => 'add')));
        $router->addRoute('settingsBankEdit', new Zend_Controller_Router_Route('settings/bank/edit/:id', array('module' => 'settings', 'controller' => 'bank', 'action' => 'edit')));
        $router->addRoute('settingsBankDelete', new Zend_Controller_Router_Route('settings/bank/delete/:id', array('module' => 'settings', 'controller' => 'bank', 'action' => 'delete')));
        $router->addRoute('settingsBankDeleteBtn', new Zend_Controller_Router_Route('settings/bank/delete', array('module' => 'settings', 'controller' => 'bank', 'action' => 'delete')));

        $router->addRoute('settingsAttributeList', new Zend_Controller_Router_Route('settings/attribute', array('module' => 'settings', 'controller' => 'attribute', 'action' => 'index')));
        $router->addRoute('settingsAttributeAdd', new Zend_Controller_Router_Route('settings/attribute/add', array('module' => 'settings', 'controller' => 'attribute', 'action' => 'add')));
        $router->addRoute('settingsAttributeEdit', new Zend_Controller_Router_Route('settings/attribute/edit/:id', array('module' => 'settings', 'controller' => 'attribute', 'action' => 'edit')));
        $router->addRoute('settingsAttributeDelete', new Zend_Controller_Router_Route('settings/attribute/delete/:id', array('module' => 'settings', 'controller' => 'attribute', 'action' => 'delete')));
        $router->addRoute('settingsAttributeDeleteBtn', new Zend_Controller_Router_Route('settings/attribute/delete', array('module' => 'settings', 'controller' => 'attribute', 'action' => 'delete')));

        $router->addRoute('settingsServicesList', new Zend_Controller_Router_Route('settings/services', array('module' => 'settings', 'controller' => 'services', 'action' => 'index')));
        $router->addRoute('settingsServicesAdd', new Zend_Controller_Router_Route('settings/services/add', array('module' => 'settings', 'controller' => 'services', 'action' => 'add')));
        $router->addRoute('settingsServicesEdit', new Zend_Controller_Router_Route('settings/services/edit/:id', array('module' => 'settings', 'controller' => 'services', 'action' => 'edit')));
        $router->addRoute('settingsServicesDelete', new Zend_Controller_Router_Route('settings/services/delete/:id', array('module' => 'settings', 'controller' => 'services', 'action' => 'delete')));
        $router->addRoute('settingsServicesDeleteBtn', new Zend_Controller_Router_Route('settings/services/delete', array('module' => 'settings', 'controller' => 'services', 'action' => 'delete')));

        $router->addRoute('settingsAuthRoleList', new Zend_Controller_Router_Route('settings/auth-role', array('module' => 'settings', 'controller' => 'auth-role', 'action' => 'index')));
        $router->addRoute('settingsAuthRoleAdd', new Zend_Controller_Router_Route('settings/auth-role/add', array('module' => 'settings', 'controller' => 'auth-role', 'action' => 'add')));
        $router->addRoute('settingsAuthRoleEdit', new Zend_Controller_Router_Route('settings/auth-role/edit/:id', array('module' => 'settings', 'controller' => 'auth-role', 'action' => 'edit')));
        $router->addRoute('settingsAuthRoleDelete', new Zend_Controller_Router_Route('settings/auth-role/delete/:id', array('module' => 'settings', 'controller' => 'auth-role', 'action' => 'delete')));
        $router->addRoute('settingsAuthRoleDeleteBtn', new Zend_Controller_Router_Route('settings/auth-role/delete', array('module' => 'settings', 'controller' => 'auth-role', 'action' => 'delete')));

        $router->addRoute('settingsAuthCredentialList', new Zend_Controller_Router_Route('settings/auth-credential', array('module' => 'settings', 'controller' => 'auth-credential', 'action' => 'index')));
        $router->addRoute('settingsAuthCredentialAdd', new Zend_Controller_Router_Route('settings/auth-credential/add', array('module' => 'settings', 'controller' => 'auth-credential', 'action' => 'add')));
        $router->addRoute('settingsAuthCredentialAddParent', new Zend_Controller_Router_Route('settings/auth-credential/add/:parent_id', array('module' => 'settings', 'controller' => 'auth-credential', 'action' => 'add')));
        $router->addRoute('settingsAuthCredentialEdit', new Zend_Controller_Router_Route('settings/auth-credential/edit/:id', array('module' => 'settings', 'controller' => 'auth-credential', 'action' => 'edit')));
        $router->addRoute('settingsAuthCredentialDelete', new Zend_Controller_Router_Route('settings/auth-credential/delete/:id', array('module' => 'settings', 'controller' => 'auth-credential', 'action' => 'delete')));
        $router->addRoute('settingsAuthCredentialDeleteBtn', new Zend_Controller_Router_Route('settings/auth-credential/delete', array('module' => 'settings', 'controller' => 'auth-credential', 'action' => 'delete')));

        $router->addRoute('settingsUserList', new Zend_Controller_Router_Route('settings/users', array('module' => 'settings', 'controller' => 'user', 'action' => 'index')));
        $router->addRoute('settingsUserAdd', new Zend_Controller_Router_Route('settings/user/add', array('module' => 'settings', 'controller' => 'user', 'action' => 'add')));
        $router->addRoute('settingsGetCompanyInquiryEmail', new Zend_Controller_Router_Route('settings/user/get_company_inquiry_email', array('module' => 'settings', 'controller' => 'user', 'action' => 'get-company-inquiry-email')));
        
		$router->addRoute('settingsUserEdit', new Zend_Controller_Router_Route('settings/user/edit/:id', array('module' => 'settings', 'controller' => 'user', 'action' => 'edit')));
        $router->addRoute('settingsUserPassword', new Zend_Controller_Router_Route('settings/user/change-password/:id', array('module' => 'settings', 'controller' => 'user', 'action' => 'change-password')));
        $router->addRoute('settingsUserLoginAsThisUser', new Zend_Controller_Router_Route('settings/user/login_as_this_user/:id', array('module' => 'settings', 'controller' => 'user', 'action' => 'login-as-this-user')));
        $router->addRoute('settingsUserDelete', new Zend_Controller_Router_Route('settings/user/delete/:id', array('module' => 'settings', 'controller' => 'user', 'action' => 'delete')));
        $router->addRoute('settingsUserDeleteBtn', new Zend_Controller_Router_Route('settings/user/delete', array('module' => 'settings', 'controller' => 'user', 'action' => 'delete')));
        $router->addRoute('activateOrDeactivateUsers', new Zend_Controller_Router_Route('settings/user/active/:id', array('module' => 'settings', 'controller' => 'user', 'action' => 'activate-or-deactivate-users')));

        $router->addRoute('settingsAttributeValueList', new Zend_Controller_Router_Route('settings/attribute-value/:attribute_id', array('module' => 'settings', 'controller' => 'attribute-value', 'action' => 'index')));
        $router->addRoute('settingsAttributeValueAdd', new Zend_Controller_Router_Route('settings/attribute-value/add/:attribute_id', array('module' => 'settings', 'controller' => 'attribute-value', 'action' => 'add')));
        $router->addRoute('settingsAttributeValueDelete', new Zend_Controller_Router_Route('settings/attribute-value/delete/:attribute_id/:id', array('module' => 'settings', 'controller' => 'attribute-value', 'action' => 'delete')));
        $router->addRoute('settingsAttributeValueDeleteBtn', new Zend_Controller_Router_Route('settings/attribute-value/delete/:attribute_id', array('module' => 'settings', 'controller' => 'attribute-value', 'action' => 'delete')));
        $router->addRoute('settingsAttributeValueEdit', new Zend_Controller_Router_Route('settings/attribute-value/edit/:id', array('module' => 'settings', 'controller' => 'attribute-value', 'action' => 'edit')));

        $router->addRoute('settingsServiceAttributeList', new Zend_Controller_Router_Route('settings/service-attribute/:service_id', array('module' => 'settings', 'controller' => 'service-attribute', 'action' => 'index')));
        $router->addRoute('settingsServiceAttributeAdd', new Zend_Controller_Router_Route('settings/service-attribute/add/:service_id', array('module' => 'settings', 'controller' => 'service-attribute', 'action' => 'add')));
        $router->addRoute('settingsServiceAttributeDelete', new Zend_Controller_Router_Route('settings/service-attribute/delete/:service_id/:id', array('module' => 'settings', 'controller' => 'service-attribute', 'action' => 'delete')));
        $router->addRoute('settingsServiceAttributeEdit', new Zend_Controller_Router_Route('settings/service-attribute/edit/:service_id/:id', array('module' => 'settings', 'controller' => 'service-attribute', 'action' => 'edit')));
        $router->addRoute('settingsServiceAttributeDeleteBtn', new Zend_Controller_Router_Route('settings/service-attribute/delete/:service_id', array('module' => 'settings', 'controller' => 'service-attribute', 'action' => 'delete')));

        $router->addRoute('settingsContractorServiceList', new Zend_Controller_Router_Route('settings/contractor-service/:contractor_id', array('module' => 'settings', 'controller' => 'contractor-service', 'action' => 'index')));
        $router->addRoute('settingsContractorServiceAdd', new Zend_Controller_Router_Route('settings/contractor-service/add/:contractor_id', array('module' => 'settings', 'controller' => 'contractor-service', 'action' => 'add')));
        $router->addRoute('settingsContractorServiceDelete', new Zend_Controller_Router_Route('settings/contractor-service/delete/:contractor_id/:id', array('module' => 'settings', 'controller' => 'contractor-service', 'action' => 'delete')));
        $router->addRoute('settingsContractorServiceDeleteBtn', new Zend_Controller_Router_Route('settings/contractor-service/delete/:contractor_id', array('module' => 'settings', 'controller' => 'contractor-service', 'action' => 'delete')));

        $router->addRoute('settingsContractorOwnerList', new Zend_Controller_Router_Route('settings/contractor-owner/:contractor_info_id', array('module' => 'settings', 'controller' => 'contractor-owner', 'action' => 'index')));
        $router->addRoute('settingsContractorOwnerAdd', new Zend_Controller_Router_Route('settings/contractor-owner/add/:contractor_info_id', array('module' => 'settings', 'controller' => 'contractor-owner', 'action' => 'add')));
        $router->addRoute('settingsContractorOwnerDelete', new Zend_Controller_Router_Route('settings/contractor-owner/delete/:contractor_info_id/:id', array('module' => 'settings', 'controller' => 'contractor-owner', 'action' => 'delete')));
        $router->addRoute('settingsContractorOwnerDeleteBtn', new Zend_Controller_Router_Route('settings/contractor-owner/delete/:contractor_info_id', array('module' => 'settings', 'controller' => 'contractor-owner', 'action' => 'delete')));
        $router->addRoute('settingsContractorOwnerEdit', new Zend_Controller_Router_Route('settings/contractor-owner/edit/:id', array('module' => 'settings', 'controller' => 'contractor-owner', 'action' => 'edit')));
        $router->addRoute('settingsContractorOwnerPhotoUpload', new Zend_Controller_Router_Route('settings/contractor-owner/photo_upload/:id', array('module' => 'settings', 'controller' => 'contractor-owner', 'action' => 'photo-upload')));
        $router->addRoute('settingsContractorOwnerview', new Zend_Controller_Router_Route('settings/contractor-owner/view/:id', array('module' => 'settings', 'controller' => 'contractor-owner', 'action' => 'view')));

        $router->addRoute('settingsContractorEmployeeList', new Zend_Controller_Router_Route('settings/contractor-employee/:contractor_info_id', array('module' => 'settings', 'controller' => 'contractor-employee', 'action' => 'index')));
        $router->addRoute('settingsContractorEmployeeAdd', new Zend_Controller_Router_Route('settings/contractor-employee/add/:contractor_info_id', array('module' => 'settings', 'controller' => 'contractor-employee', 'action' => 'add')));
        $router->addRoute('settingsContractorEmployeeDelete', new Zend_Controller_Router_Route('settings/contractor-employee/delete/:contractor_info_id/:id', array('module' => 'settings', 'controller' => 'contractor-employee', 'action' => 'delete')));
        $router->addRoute('settingsContractorEmployeeDeleteBtn', new Zend_Controller_Router_Route('settings/contractor-employee/delete/:contractor_info_id', array('module' => 'settings', 'controller' => 'contractor-employee', 'action' => 'delete')));
        $router->addRoute('settingsContractorEmployeeEdit', new Zend_Controller_Router_Route('settings/contractor-employee/edit/:id', array('module' => 'settings', 'controller' => 'contractor-employee', 'action' => 'edit')));
        $router->addRoute('settingsContractorEmployeePhotoUpload', new Zend_Controller_Router_Route('settings/contractor-employee/photo_upload/:id', array('module' => 'settings', 'controller' => 'contractor-employee', 'action' => 'photo-upload')));
        $router->addRoute('settingsContractorEmployeeview', new Zend_Controller_Router_Route('settings/contractor-employeer/view/:id', array('module' => 'settings', 'controller' => 'contractor-employee', 'action' => 'view')));

        $router->addRoute('settingsDeclarationOfChemicalsList', new Zend_Controller_Router_Route('settings/declaration-of-chemicals/:contractor_info_id', array('module' => 'settings', 'controller' => 'declaration-of-chemicals', 'action' => 'index')));
        $router->addRoute('settingsDeclarationOfChemicalsAdd', new Zend_Controller_Router_Route('settings/declaration-of-chemicals/add/:contractor_info_id', array('module' => 'settings', 'controller' => 'declaration-of-chemicals', 'action' => 'add')));
        $router->addRoute('settingsDeclarationOfChemicalsDelete', new Zend_Controller_Router_Route('settings/declaration-of-chemicals/delete/:contractor_info_id/:id', array('module' => 'settings', 'controller' => 'declaration-of-chemicals', 'action' => 'delete')));
        $router->addRoute('settingsDeclarationOfChemicalsDeleteBtn', new Zend_Controller_Router_Route('settings/declaration-of-chemicals/delete/:contractor_info_id', array('module' => 'settings', 'controller' => 'declaration-of-chemicals', 'action' => 'delete')));

        $router->addRoute('settingsDeclarationOfEquipmentList', new Zend_Controller_Router_Route('settings/declaration-of-equipment/:contractor_info_id', array('module' => 'settings', 'controller' => 'declaration-of-equipment', 'action' => 'index')));
        $router->addRoute('settingsDeclarationOfEquipmentAdd', new Zend_Controller_Router_Route('settings/declaration-of-equipment/add/:contractor_info_id', array('module' => 'settings', 'controller' => 'declaration-of-equipment', 'action' => 'add')));
        $router->addRoute('settingsDeclarationOfEquipmentDelete', new Zend_Controller_Router_Route('settings/declaration-of-equipment/delete/:contractor_info_id/:id', array('module' => 'settings', 'controller' => 'declaration-of-equipment', 'action' => 'delete')));
        $router->addRoute('settingsDeclarationOfEquipmentDeleteBtn', new Zend_Controller_Router_Route('settings/declaration-of-equipment/delete/:contractor_info_id', array('module' => 'settings', 'controller' => 'declaration-of-equipment', 'action' => 'delete')));

        $router->addRoute('settingsDeclarationOfOtherApparatusList', new Zend_Controller_Router_Route('settings/declaration-of-other-apparatus/:contractor_info_id', array('module' => 'settings', 'controller' => 'declaration-of-other-apparatus', 'action' => 'index')));
        $router->addRoute('settingsDeclarationOfOtherApparatusAdd', new Zend_Controller_Router_Route('settings/declaration-of-other-apparatus/add/:contractor_info_id', array('module' => 'settings', 'controller' => 'declaration-of-other-apparatus', 'action' => 'add')));
        $router->addRoute('settingsDeclarationOfOtherApparatusDelete', new Zend_Controller_Router_Route('settings/declaration-of-other-apparatus/delete/:contractor_info_id/:id', array('module' => 'settings', 'controller' => 'declaration-of-other-apparatus', 'action' => 'delete')));
        $router->addRoute('settingsDeclarationOfOtherApparatusDeleteBtn', new Zend_Controller_Router_Route('settings/declaration-of-other-apparatus/delete/:contractor_info_id', array('module' => 'settings', 'controller' => 'declaration-of-other-apparatus', 'action' => 'delete')));

        $router->addRoute('settingsContractorVehicleList', new Zend_Controller_Router_Route('settings/contractor-vehicle/:contractor_info_id', array('module' => 'settings', 'controller' => 'contractor-vehicle', 'action' => 'index')));
        $router->addRoute('settingsContractorVehicleAdd', new Zend_Controller_Router_Route('settings/contractor-vehicle/add/:contractor_info_id', array('module' => 'settings', 'controller' => 'contractor-vehicle', 'action' => 'add')));
        $router->addRoute('settingsContractorVehicleDelete', new Zend_Controller_Router_Route('settings/contractor-vehicle/delete/:contractor_info_id/:id', array('module' => 'settings', 'controller' => 'contractor-vehicle', 'action' => 'delete')));
        $router->addRoute('settingsContractorVehicleDeleteBtn', new Zend_Controller_Router_Route('settings/contractor-vehicle/delete/:contractor_info_id', array('module' => 'settings', 'controller' => 'contractor-vehicle', 'action' => 'delete')));

        $router->addRoute('settingsContractorInfoList', new Zend_Controller_Router_Route('settings/contractor-info/:contractor_id', array('module' => 'settings', 'controller' => 'contractor-info', 'action' => 'index')));
        $router->addRoute('settingsContractorInfoAdd', new Zend_Controller_Router_Route('settings/contractor-info/add/:contractor_id', array('module' => 'settings', 'controller' => 'contractor-info', 'action' => 'add')));
        $router->addRoute('settingsContractorInfoDelete', new Zend_Controller_Router_Route('settings/contractor-info/delete/:contractor_id/:id', array('module' => 'settings', 'controller' => 'contractor-info', 'action' => 'delete')));
        $router->addRoute('settingsContractorInfoEdit', new Zend_Controller_Router_Route('settings/contractor-info/edit/:contractor_id/:id', array('module' => 'settings', 'controller' => 'contractor-info', 'action' => 'edit')));

        $router->addRoute('settingsAuthRoleCredentialList', new Zend_Controller_Router_Route('settings/auth-role-credential/:role_id', array('module' => 'settings', 'controller' => 'auth-role-credential', 'action' => 'index')));
        $router->addRoute('updateGeneralContractor', new Zend_Controller_Router_Route('settings/update-general-contractor', array('module' => 'settings', 'controller' => 'user', 'action' => 'update-general-contractor')));
        $router->addRoute('settingsContractorGmailAdd', new Zend_Controller_Router_Route('settings/contractor-info/add-gmail-accounts/:contractor_id', array('module' => 'settings', 'controller' => 'contractor-info', 'action' => 'add-gmail-accounts')));
        $router->addRoute('settingsContractorGmailEdit', new Zend_Controller_Router_Route('settings/contractor-info/edit-gmail-accounts/:contractor_id/:id', array('module' => 'settings', 'controller' => 'contractor-info', 'action' => 'edit-gmail-accounts')));

        $router->addRoute('settingsUserInfoList', new Zend_Controller_Router_Route('settings/user-info/:user_id', array('module' => 'settings', 'controller' => 'user-info', 'action' => 'index')));
        $router->addRoute('settingsUserInfoAdd', new Zend_Controller_Router_Route('settings/user-info/add/:user_id', array('module' => 'settings', 'controller' => 'user-info', 'action' => 'add')));
        $router->addRoute('settingsUserInfoDelete', new Zend_Controller_Router_Route('settings/user-info/delete/:user_id/:id', array('module' => 'settings', 'controller' => 'user-info', 'action' => 'delete')));
        $router->addRoute('settingsUserInfoEdit', new Zend_Controller_Router_Route('settings/user-info/edit/:user_id/:id', array('module' => 'settings', 'controller' => 'user-info', 'action' => 'edit')));

        $router->addRoute('settingsContractorServiceAvailabilityList', new Zend_Controller_Router_Route('settings/contractor-service-availability/:contractor_service_id', array('module' => 'settings', 'controller' => 'contractor-service-availability', 'action' => 'index')));
        $router->addRoute('settingsContractorServiceAvailabilityAdd', new Zend_Controller_Router_Route('settings/contractor-service-availability/add/:contractor_service_id', array('module' => 'settings', 'controller' => 'contractor-service-availability', 'action' => 'add')));
        $router->addRoute('settingsContractorServiceAvailabilityDelete', new Zend_Controller_Router_Route('settings/contractor-service-availability/delete/:contractor_service_id/:id', array('module' => 'settings', 'controller' => 'contractor-service-availability', 'action' => 'delete')));
        $router->addRoute('settingsContractorServiceAvailabilityDeleteBtn', new Zend_Controller_Router_Route('settings/contractor-service-availability/delete/:contractor_service_id', array('module' => 'settings', 'controller' => 'contractor-service-availability', 'action' => 'delete')));

        $router->addRoute('settingsInquiryTypeAttributeList', new Zend_Controller_Router_Route('settings/inquiry-type-attribute/:inquiry_type_id', array('module' => 'settings', 'controller' => 'inquiry-type-attribute', 'action' => 'index')));
        $router->addRoute('settingsInquiryTypeAttributeAdd', new Zend_Controller_Router_Route('settings/inquiry-type-attribute/add/:inquiry_type_id', array('module' => 'settings', 'controller' => 'inquiry-type-attribute', 'action' => 'add')));
        $router->addRoute('settingsInquiryTypeAttributeDelete', new Zend_Controller_Router_Route('settings/inquiry-type-attribute/delete/:inquiry_type_id/:id', array('module' => 'settings', 'controller' => 'inquiry-type-attribute', 'action' => 'delete')));
        $router->addRoute('settingsInquiryTypeAttributeDeleteBtn', new Zend_Controller_Router_Route('settings/inquiry-type-attribute/delete/:inquiry_type_id', array('module' => 'settings', 'controller' => 'inquiry-type-attribute', 'action' => 'delete')));

        $router->addRoute('settingsPaymentTypeList', new Zend_Controller_Router_Route('settings/payment-type', array('module' => 'settings', 'controller' => 'payment-type', 'action' => 'index')));
        $router->addRoute('settingsPaymentTypeAdd', new Zend_Controller_Router_Route('settings/payment-type/add', array('module' => 'settings', 'controller' => 'payment-type', 'action' => 'add')));
        $router->addRoute('settingsPaymentTypeEdit', new Zend_Controller_Router_Route('settings/payment-type/edit/:id', array('module' => 'settings', 'controller' => 'payment-type', 'action' => 'edit')));
        $router->addRoute('settingsPaymentTypeDelete', new Zend_Controller_Router_Route('settings/payment-type/delete/:id', array('module' => 'settings', 'controller' => 'payment-type', 'action' => 'delete')));
        $router->addRoute('settingsPaymentTypeDeleteBtn', new Zend_Controller_Router_Route('settings/payment-type/delete', array('module' => 'settings', 'controller' => 'payment-type', 'action' => 'delete')));

        $router->addRoute('settingsCustomerTypeList', new Zend_Controller_Router_Route('settings/customer-type', array('module' => 'settings', 'controller' => 'customer-type', 'action' => 'index')));
        $router->addRoute('settingsCustomerTypeAdd', new Zend_Controller_Router_Route('settings/customer-type/add', array('module' => 'settings', 'controller' => 'customer-type', 'action' => 'add')));
        $router->addRoute('settingsCustomerTypeEdit', new Zend_Controller_Router_Route('settings/customer-type/edit/:id', array('module' => 'settings', 'controller' => 'customer-type', 'action' => 'edit')));
        $router->addRoute('settingsCustomerTypeDelete', new Zend_Controller_Router_Route('settings/customer-type/delete/:id', array('module' => 'settings', 'controller' => 'customer-type', 'action' => 'delete')));
        $router->addRoute('settingsCustomerTypeDeleteBtn', new Zend_Controller_Router_Route('settings/customer-type/delete', array('module' => 'settings', 'controller' => 'customer-type', 'action' => 'delete')));
        $router->addRoute('settingsCustomerTypeChangeWorkOrder', new Zend_Controller_Router_Route('settings/customer-type/change-work-order/:id', array('module' => 'settings', 'controller' => 'customer-type', 'action' => 'change-work-order')));
        $router->addRoute('settingsCustomerTypeAsBookingAddress', new Zend_Controller_Router_Route('settings/customer-type/as-booking-address/:id', array('module' => 'settings', 'controller' => 'customer-type', 'action' => 'as-booking-address')));

        $router->addRoute('settingsLabelTypeList', new Zend_Controller_Router_Route('settings/label-type', array('module' => 'settings', 'controller' => 'label-type', 'action' => 'index')));
        $router->addRoute('settingsLabelTypeAdd', new Zend_Controller_Router_Route('settings/label-type/add', array('module' => 'settings', 'controller' => 'label-type', 'action' => 'add')));
        $router->addRoute('settingsLabelTypeEdit', new Zend_Controller_Router_Route('settings/label-type/edit/:id', array('module' => 'settings', 'controller' => 'label-type', 'action' => 'edit')));
        $router->addRoute('settingsLabelTypeDelete', new Zend_Controller_Router_Route('settings/label-type/delete/:id', array('module' => 'settings', 'controller' => 'label-type', 'action' => 'delete')));
        $router->addRoute('settingsLabelTypeDeleteBtn', new Zend_Controller_Router_Route('settings/label-type/delete', array('module' => 'settings', 'controller' => 'label-type', 'action' => 'delete')));

        $router->addRoute('settingsCustomerContactLabelList', new Zend_Controller_Router_Route('settings/customer_contact_label', array('module' => 'settings', 'controller' => 'customer-contact-label', 'action' => 'index')));
        $router->addRoute('settingsCustomerContactLabelAdd', new Zend_Controller_Router_Route('settings/customer_contact_label/add', array('module' => 'settings', 'controller' => 'customer-contact-label', 'action' => 'add')));
        $router->addRoute('settingsCustomerContactLabelEdit', new Zend_Controller_Router_Route('settings/customer_contact_label/edit/:id', array('module' => 'settings', 'controller' => 'customer-contact-label', 'action' => 'edit')));
        $router->addRoute('settingsCustomerContactLabelDelete', new Zend_Controller_Router_Route('settings/customer_contact_label/delete/:id', array('module' => 'settings', 'controller' => 'customer-contact-label', 'action' => 'delete')));
        $router->addRoute('settingsCustomerContactLabelDeleteBtn', new Zend_Controller_Router_Route('settings/customer_contact_label/delete', array('module' => 'settings', 'controller' => 'customer-contact-label', 'action' => 'delete')));

        $router->addRoute('settingsDueDateList', new Zend_Controller_Router_Route('settings/due-date', array('module' => 'settings', 'controller' => 'due-date', 'action' => 'index')));
        $router->addRoute('settingsDueDateAdd', new Zend_Controller_Router_Route('settings/due-date/add', array('module' => 'settings', 'controller' => 'due-date', 'action' => 'add')));
        $router->addRoute('settingsDueDateEdit', new Zend_Controller_Router_Route('settings/due-date/edit/:id', array('module' => 'settings', 'controller' => 'due-date', 'action' => 'edit')));
        $router->addRoute('settingsDueDateDelete', new Zend_Controller_Router_Route('settings/due-date/delete/:id', array('module' => 'settings', 'controller' => 'due-date', 'action' => 'delete')));
        $router->addRoute('settingsDueDateDeleteBtn', new Zend_Controller_Router_Route('settings/due-date/delete', array('module' => 'settings', 'controller' => 'due-date', 'action' => 'delete')));
        $router->addRoute('settingsDueDateAsDefault', new Zend_Controller_Router_Route('settings/due-date/set-as-default/:id', array('module' => 'settings', 'controller' => 'due-date', 'action' => ' set-as-default')));
        $router->addRoute('settingsCallOutFee', new Zend_Controller_Router_Route('settings/call-out-fee', array('module' => 'settings', 'controller' => 'call-out-fee', 'action' => 'index')));

        $router->addRoute('settingsProductList', new Zend_Controller_Router_Route('settings/product', array('module' => 'settings', 'controller' => 'product', 'action' => 'index')));
        $router->addRoute('settingsProductAdd', new Zend_Controller_Router_Route('settings/product/add', array('module' => 'settings', 'controller' => 'product', 'action' => 'add')));
        $router->addRoute('settingsProductEdit', new Zend_Controller_Router_Route('settings/product/edit/:id', array('module' => 'settings', 'controller' => 'product', 'action' => 'edit')));
        $router->addRoute('settingsProductDelete', new Zend_Controller_Router_Route('settings/product/delete/:id', array('module' => 'settings', 'controller' => 'product', 'action' => 'delete')));
        $router->addRoute('settingsProductDeleteBtn', new Zend_Controller_Router_Route('settings/product/delete', array('module' => 'settings', 'controller' => 'product', 'action' => 'delete')));

        $router->addRoute('settingsEmailTemplateList', new Zend_Controller_Router_Route('settings/email-template', array('module' => 'settings', 'controller' => 'email-template', 'action' => 'index')));
        $router->addRoute('settingsEmailTemplateAdd', new Zend_Controller_Router_Route('settings/email-template/add', array('module' => 'settings', 'controller' => 'email-template', 'action' => 'add')));
        $router->addRoute('settingsEmailTemplateEdit', new Zend_Controller_Router_Route('settings/email-template/edit/:id', array('module' => 'settings', 'controller' => 'email-template', 'action' => 'edit')));
        $router->addRoute('settingsEmailTemplateDelete', new Zend_Controller_Router_Route('settings/email-template/delete/:id', array('module' => 'settings', 'controller' => 'email-template', 'action' => 'delete')));
        $router->addRoute('settingsEmailTemplateDeleteBtn', new Zend_Controller_Router_Route('settings/email-template/delete', array('module' => 'settings', 'controller' => 'email-template', 'action' => 'delete')));

        $router->addRoute('settingsCannedResponsesList', new Zend_Controller_Router_Route('settings/canned-responses', array('module' => 'settings', 'controller' => 'canned-responses', 'action' => 'index')));
        $router->addRoute('settingsCannedResponsesAdd', new Zend_Controller_Router_Route('settings/canned-responses/add', array('module' => 'settings', 'controller' => 'canned-responses', 'action' => 'add')));
        $router->addRoute('settingsCannedResponsesEdit', new Zend_Controller_Router_Route('settings/canned-responses/edit/:id', array('module' => 'settings', 'controller' => 'canned-responses', 'action' => 'edit')));
        $router->addRoute('settingsCannedResponsesDelete', new Zend_Controller_Router_Route('settings/canned-responses/delete/:id', array('module' => 'settings', 'controller' => 'canned-responses', 'action' => 'delete')));
        $router->addRoute('settingsCannedResponsesDeleteBtn', new Zend_Controller_Router_Route('settings/canned-responses/delete', array('module' => 'settings', 'controller' => 'canned-responses', 'action' => 'delete')));

        $router->addRoute('settingsCronJobList', new Zend_Controller_Router_Route('settings/cron-job', array('module' => 'settings', 'controller' => 'cron-job', 'action' => 'index')));
        $router->addRoute('settingsCronJobAdd', new Zend_Controller_Router_Route('settings/cron-job/add', array('module' => 'settings', 'controller' => 'cron-job', 'action' => 'add')));
        $router->addRoute('settingsCronJobEdit', new Zend_Controller_Router_Route('settings/cron-job/edit/:id', array('module' => 'settings', 'controller' => 'cron-job', 'action' => 'edit')));
        $router->addRoute('settingsCronJobDelete', new Zend_Controller_Router_Route('settings/cron-job/delete/:id', array('module' => 'settings', 'controller' => 'cron-job', 'action' => 'delete')));
        $router->addRoute('settingsCronJobDeleteBtn', new Zend_Controller_Router_Route('settings/cron-job/delete', array('module' => 'settings', 'controller' => 'cron-job', 'action' => 'delete')));
        $router->addRoute('settingsCronjobAddDescription', new Zend_Controller_Router_Route('settings/cron-job/add-desription/:id', array('module' => 'settings', 'controller' => 'cron-job', 'action' => 'add-description')));
        $router->addRoute('settingsCronJobView', new Zend_Controller_Router_Route('settings/cron-job/view/:id', array('module' => 'settings', 'controller' => 'cron-job', 'action' => 'view')));
		$router->addRoute('settingsCronJobRunNow', new Zend_Controller_Router_Route('settings/cron-job/:id', array('module' => 'settings', 'controller' => 'cron-job', 'action' => 'run-now')));

		//
		//Contractor
		//
		$router->addRoute('contractors', new Zend_Controller_Router_Route('contractor', array('module' => 'contractor', 'controller' => 'index', 'action' => 'index')));
        
        //
        //customer
        //
        $router->addRoute('customerList', new Zend_Controller_Router_Route('customers', array('module' => 'customer', 'controller' => 'index', 'action' => 'index')));
        $router->addRoute('customerAdd', new Zend_Controller_Router_Route('customer/add', array('module' => 'customer', 'controller' => 'index', 'action' => 'add')));
        $router->addRoute('customerEdit', new Zend_Controller_Router_Route('customer/edit/:id', array('module' => 'customer', 'controller' => 'index', 'action' => 'edit')));
        $router->addRoute('customerDelete', new Zend_Controller_Router_Route('customer/delete/:id', array('module' => 'customer', 'controller' => 'index', 'action' => 'delete')));
        $router->addRoute('customerUndelete', new Zend_Controller_Router_Route('customer/undelete/:id', array('module' => 'customer', 'controller' => 'index', 'action' => 'undelete')));
        $router->addRoute('customerDeleteBtn', new Zend_Controller_Router_Route('customer/delete', array('module' => 'customer', 'controller' => 'index', 'action' => 'delete')));
        $router->addRoute('customerView', new Zend_Controller_Router_Route('customer/view/:id', array('module' => 'customer', 'controller' => 'index', 'action' => 'view')));
        $router->addRoute('businessView', new Zend_Controller_Router_Route('customer/view-business/:name', array('module' => 'customer', 'controller' => 'index', 'action' => 'view-business')));
        
		$router->addRoute('customerLocation', new Zend_Controller_Router_Route('customer/location/:id', array('module' => 'customer', 'controller' => 'index', 'action' => 'location')));
        $router->addRoute('bookingLocation', new Zend_Controller_Router_Route('booking/location/:id', array('module' => 'customer', 'controller' => 'index', 'action' => 'booking-location')));
        $router->addRoute('showDeletedCustomer', new Zend_Controller_Router_Route('customer/deleted', array('module' => 'customer', 'controller' => 'index', 'action' => 'show-deleted')));
        $router->addRoute('showDeletedInquiry', new Zend_Controller_Router_Route('inquiry/deleted', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'show-deleted')));
        $router->addRoute('customerDeleteForever', new Zend_Controller_Router_Route('customer/delete-forever/:id', array('module' => 'customer', 'controller' => 'index', 'action' => 'delete-forever')));
        $router->addRoute('customerDeleteForeverBtn', new Zend_Controller_Router_Route('customer/delete-forever', array('module' => 'customer', 'controller' => 'index', 'action' => 'delete-forever')));
        $router->addRoute('inquiryLocation', new Zend_Controller_Router_Route('inquiry/location/:id', array('module' => 'customer', 'controller' => 'index', 'action' => 'inquiry-location')));
        $router->addRoute('customerNotes', new Zend_Controller_Router_Route('customer/notes/:id', array('module' => 'customer', 'controller' => 'index', 'action' => 'customer-notes')));
        $router->addRoute('customerInfoAsJson', new Zend_Controller_Router_Route('customer/info_as_json', array('module' => 'customer', 'controller' => 'index', 'action' => 'customer-info-as-json')));
        $router->addRoute('findDuplicateCustomer', new Zend_Controller_Router_Route('customer/find_duplicate_customer', array('module' => 'customer', 'controller' => 'duplicate-customer', 'action' => 'find-duplicate-customer')));
        $router->addRoute('fixDuplicateCustomer', new Zend_Controller_Router_Route('customer/fix_duplicate_customer/:id', array('module' => 'customer', 'controller' => 'duplicate-customer', 'action' => 'fix-duplicate-customer')));
        $router->addRoute('getCustomerContact', new Zend_Controller_Router_Route('customer/get_customer_contact', array('module' => 'customer', 'controller' => 'index', 'action' => 'get-customer-contact')));


        $router->addRoute('Login', new Zend_Controller_Router_Route('login', array('module' => 'default', 'controller' => 'index', 'action' => 'index')));
        $router->addRoute('Logout', new Zend_Controller_Router_Route('logout', array('module' => 'default', 'controller' => 'index', 'action' => 'logout')));
        $router->addRoute('myAccount', new Zend_Controller_Router_Route('myAccount', array('module' => 'default', 'controller' => 'index', 'action' => 'my-account')));
        $router->addRoute('forgetPassword', new Zend_Controller_Router_Route('forget/password', array('module' => 'default', 'controller' => 'index', 'action' => 'forget-password')));
        $router->addRoute('forgetPasswordStep2', new Zend_Controller_Router_Route('reset/password/:code/:id', array('module' => 'default', 'controller' => 'index', 'action' => 'forget-password-step2')));
        $router->addRoute('changeEmail', new Zend_Controller_Router_Route('change/email', array('module' => 'default', 'controller' => 'index', 'action' => 'change-email')));
        $router->addRoute('changePassword', new Zend_Controller_Router_Route('change/password', array('module' => 'default', 'controller' => 'index', 'action' => 'change-password')));
        $router->addRoute('changeEmailStep2', new Zend_Controller_Router_Route('change/email/step2/:code/:id', array('module' => 'default', 'controller' => 'index', 'action' => 'change-email-step2')));
        $router->addRoute('changeAccountInfo', new Zend_Controller_Router_Route('change/account', array('module' => 'default', 'controller' => 'index', 'action' => 'change-account-info')));
        $router->addRoute('modfyUserInfo', new Zend_Controller_Router_Route('myAccount/userInfo', array('module' => 'default', 'controller' => 'index', 'action' => 'modfy-user-info')));
        $router->addRoute('modfyContractorInfo', new Zend_Controller_Router_Route('myAccount/contractorInfo', array('module' => 'default', 'controller' => 'index', 'action' => 'modfy-contractor-info')));

        $router->addRoute('dashboard', new Zend_Controller_Router_Route('dashboard', array('module' => 'default', 'controller' => 'dashboard', 'action' => 'index')));
        $router->addRoute('recentDiscussions', new Zend_Controller_Router_Route('dashboard/recentDiscussions', array('module' => 'default', 'controller' => 'dashboard', 'action' => 'recent-discussions')));
        $router->addRoute('recentHistory', new Zend_Controller_Router_Route('dashboard/recentHistory', array('module' => 'default', 'controller' => 'dashboard', 'action' => 'recent-history')));
        $router->addRoute('recentComplaints', new Zend_Controller_Router_Route('dashboard/recentComplaints', array('module' => 'default', 'controller' => 'dashboard', 'action' => 'recent-complaints')));
        $router->addRoute('acceptServices', new Zend_Controller_Router_Route('dashboard/acceptServices/:bookingId', array('module' => 'default', 'controller' => 'dashboard', 'action' => 'accept-services')));
        $router->addRoute('rejectServices', new Zend_Controller_Router_Route('dashboard/rejectServices/:bookingId', array('module' => 'default', 'controller' => 'dashboard', 'action' => 'reject-services')));
        $router->addRoute('acceptAdminServices', new Zend_Controller_Router_Route('dashboard/acceptAdminServices/:bookingId', array('module' => 'default', 'controller' => 'dashboard', 'action' => 'accept-admin-services')));
        /////By Islam reject services
		$router->addRoute('rejectAdminServices', new Zend_Controller_Router_Route('dashboard/rejectAdminServices/:bookingId', array('module' => 'default', 'controller' => 'dashboard', 'action' => 'reject-admin-services')));
        
		
		$router->addRoute('resendAdminServices', new Zend_Controller_Router_Route('dashboard/resendAdminServices/:bookingId', array('module' => 'default', 'controller' => 'dashboard', 'action' => 'resend-admin-services')));
        $router->addRoute('recentByConvertStatus', new Zend_Controller_Router_Route('dashboard/recentByConvertStatus/:convert_status', array('module' => 'default', 'controller' => 'dashboard', 'action' => 'recent-by-convert-status')));
        $router->addRoute('recentByBookingStatus', new Zend_Controller_Router_Route('dashboard/recentByBookingStatus/:status_id', array('module' => 'default', 'controller' => 'dashboard', 'action' => 'recent-by-booking-status')));
        $router->addRoute('showStatusDiscussion', new Zend_Controller_Router_Route('dashboard/show-status-discussion/:id/:status_id', array('module' => 'default', 'controller' => 'dashboard', 'action' => 'show-status-discussion')));
        $router->addRoute('logUserActivity', new Zend_Controller_Router_Route('dashboard/log_user_activity', array('module' => 'default', 'controller' => 'dashboard', 'action' => 'log-user-activity')));
        $router->addRoute('recentEmails', new Zend_Controller_Router_Route('dashboard/recentEmails', array('module' => 'default', 'controller' => 'dashboard', 'action' => 'recent-emails')));
        $router->addRoute('cronjobHistory', new Zend_Controller_Router_Route('dashboard/cronjobHistory', array('module' => 'default', 'controller' => 'dashboard', 'action' => 'cronjob-history')));
        
		$router->addRoute('replayRecentEmail', new Zend_Controller_Router_Route('dashboard/replayRecentEmail/:id', array('module' => 'default', 'controller' => 'dashboard', 'action' => 'replay-recent-emails')));
        $router->addRoute('downloadEmailPdfFile', new Zend_Controller_Router_Route('dashboard/downloadEmailPdfFile/:id', array('module' => 'default', 'controller' => 'dashboard', 'action' => 'download-email-pdf-file')));

        $router->addRoute('unapproved', new Zend_Controller_Router_Route('approve-booking/unapproved', array('module' => 'default', 'controller' => 'approve-booking', 'action' => 'unapproved')));
        $router->addRoute('approvedService', new Zend_Controller_Router_Route('approve-booking/accept/service/:id', array('module' => 'default', 'controller' => 'approve-booking', 'action' => 'approved-service')));
        $router->addRoute('ignoreAll', new Zend_Controller_Router_Route('approve-booking/ignore/all/:id', array('module' => 'default', 'controller' => 'approve-booking', 'action' => 'ignore-all')));
        $router->addRoute('ignoreBookingAddress', new Zend_Controller_Router_Route('approve-booking/ignore/aooking-address/:id', array('module' => 'default', 'controller' => 'approve-booking', 'action' => 'ignore-booking-address')));
        $router->addRoute('ignoreTotalDiscount', new Zend_Controller_Router_Route('approve-booking/ignore/discount/:id', array('module' => 'default', 'controller' => 'approve-booking', 'action' => 'ignore-total-discount')));
        $router->addRoute('ignoreService', new Zend_Controller_Router_Route('approve-booking/ignore/service/:id/:service_id/:clone', array('module' => 'default', 'controller' => 'approve-booking', 'action' => 'ignore-service')));
        $router->addRoute('ignoreCallOutFee', new Zend_Controller_Router_Route('approve-booking/ignore/call_out_fee/:id', array('module' => 'default', 'controller' => 'approve-booking', 'action' => 'ignore-call-out-fee')));

        $router->addRoute('contractorOwnerList', new Zend_Controller_Router_Route('myAccount/contractor-owner', array('module' => 'default', 'controller' => 'contractor-owner', 'action' => 'index')));
        $router->addRoute('contractorOwnerAdd', new Zend_Controller_Router_Route('myAccount/contractor-owner/add', array('module' => 'default', 'controller' => 'contractor-owner', 'action' => 'add')));
        $router->addRoute('contractorOwnerDelete', new Zend_Controller_Router_Route('/contractor-owner/delete/:id', array('module' => 'default', 'controller' => 'contractor-owner', 'action' => 'delete')));
        $router->addRoute('contractorOwnerDeleteBtn', new Zend_Controller_Router_Route('myAccount/contractor-owner/delete', array('module' => 'default', 'controller' => 'contractor-owner', 'action' => 'delete')));
        $router->addRoute('contractorOwnerEdit', new Zend_Controller_Router_Route('myAccount/contractor-owner/edit/:id', array('module' => 'default', 'controller' => 'contractor-owner', 'action' => 'edit')));
        $router->addRoute('contractorOwnerPhotoUpload', new Zend_Controller_Router_Route('myAccount/contractor-owner/photo_upload/:id', array('module' => 'default', 'controller' => 'contractor-owner', 'action' => 'photo-upload')));
        $router->addRoute('contractorOwnerview', new Zend_Controller_Router_Route('myAccount/contractor-owner/view/:id', array('module' => 'default', 'controller' => 'contractor-owner', 'action' => 'view')));

        $router->addRoute('contractorEmployeeList', new Zend_Controller_Router_Route('myAccount/contractor-employee', array('module' => 'default', 'controller' => 'contractor-employee', 'action' => 'index')));
        $router->addRoute('contractorEmployeeAdd', new Zend_Controller_Router_Route('myAccount/contractor-employee/add', array('module' => 'default', 'controller' => 'contractor-employee', 'action' => 'add')));
        $router->addRoute('contractorEmployeeDelete', new Zend_Controller_Router_Route('myAccount/contractor-employee/delete/:id', array('module' => 'default', 'controller' => 'contractor-employee', 'action' => 'delete')));
        $router->addRoute('contractorEmployeeDeleteBtn', new Zend_Controller_Router_Route('myAccount/contractor-employee/delete', array('module' => 'default', 'controller' => 'contractor-employee', 'action' => 'delete')));
        $router->addRoute('contractorEmployeePhotoUpload', new Zend_Controller_Router_Route('myAccount/contractor-employee/photo_upload/:id', array('module' => 'default', 'controller' => 'contractor-employee', 'action' => 'photo-upload')));
        $router->addRoute('contractorEmployeeEdit', new Zend_Controller_Router_Route('myAccount/contractor-employee/edit/:id', array('module' => 'default', 'controller' => 'contractor-employee', 'action' => 'edit')));
        $router->addRoute('contractorEmployeeview', new Zend_Controller_Router_Route('myAccount/contractor-employeer/view/:id', array('module' => 'default', 'controller' => 'contractor-employee', 'action' => 'view')));

        $router->addRoute('declarationOfChemicalsList', new Zend_Controller_Router_Route('myAccount/declaration-of-chemicals', array('module' => 'default', 'controller' => 'declaration-of-chemicals', 'action' => 'index')));
        $router->addRoute('declarationOfChemicalsAdd', new Zend_Controller_Router_Route('myAccount/declaration-of-chemicals/add', array('module' => 'default', 'controller' => 'declaration-of-chemicals', 'action' => 'add')));
        $router->addRoute('declarationOfChemicalsDelete', new Zend_Controller_Router_Route('myAccount/declaration-of-chemicals/delete/:id', array('module' => 'default', 'controller' => 'declaration-of-chemicals', 'action' => 'delete')));
        $router->addRoute('declarationOfChemicalsDeleteBtn', new Zend_Controller_Router_Route('myAccount/declaration-of-chemicals/delete', array('module' => 'default', 'controller' => 'declaration-of-chemicals', 'action' => 'delete')));

        $router->addRoute('declarationOfEquipmentList', new Zend_Controller_Router_Route('myAccount/declaration-of-equipment', array('module' => 'default', 'controller' => 'declaration-of-equipment', 'action' => 'index')));
        $router->addRoute('declarationOfEquipmentAdd', new Zend_Controller_Router_Route('myAccount/declaration-of-equipment/add', array('module' => 'default', 'controller' => 'declaration-of-equipment', 'action' => 'add')));
        $router->addRoute('declarationOfEquipmentDelete', new Zend_Controller_Router_Route('myAccount/declaration-of-equipment/delete/:id', array('module' => 'default', 'controller' => 'declaration-of-equipment', 'action' => 'delete')));
        $router->addRoute('declarationOfEquipmentDeleteBtn', new Zend_Controller_Router_Route('myAccount/declaration-of-equipment/delete', array('module' => 'default', 'controller' => 'declaration-of-equipment', 'action' => 'delete')));

        $router->addRoute('declarationOfOtherApparatusList', new Zend_Controller_Router_Route('myAccount/declaration-of-other-apparatus', array('module' => 'default', 'controller' => 'declaration-of-other-apparatus', 'action' => 'index')));
        $router->addRoute('declarationOfOtherApparatusAdd', new Zend_Controller_Router_Route('myAccount/declaration-of-other-apparatus/add', array('module' => 'default', 'controller' => 'declaration-of-other-apparatus', 'action' => 'add')));
        $router->addRoute('declarationOfOtherApparatusDelete', new Zend_Controller_Router_Route('myAccount/declaration-of-other-apparatus/delete/:id', array('module' => 'default', 'controller' => 'declaration-of-other-apparatus', 'action' => 'delete')));
        $router->addRoute('declarationOfOtherApparatusDeleteBtn', new Zend_Controller_Router_Route('myAccount/declaration-of-other-apparatus/delete', array('module' => 'default', 'controller' => 'declaration-of-other-apparatus', 'action' => 'delete')));

        $router->addRoute('contractorVehicleList', new Zend_Controller_Router_Route('myAccount/contractor-vehicle', array('module' => 'default', 'controller' => 'contractor-vehicle', 'action' => 'index')));
        $router->addRoute('contractorVehicleAdd', new Zend_Controller_Router_Route('myAccount/contractor-vehicle/add', array('module' => 'default', 'controller' => 'contractor-vehicle', 'action' => 'add')));
        $router->addRoute('contractorVehicleDelete', new Zend_Controller_Router_Route('myAccount/contractor-vehicle/delete/:id', array('module' => 'default', 'controller' => 'contractor-vehicle', 'action' => 'delete')));
        $router->addRoute('contractorVehicleDeleteBtn', new Zend_Controller_Router_Route('myAccount/contractor-vehicle/delete', array('module' => 'default', 'controller' => 'contractor-vehicle', 'action' => 'delete')));


        $router->addRoute('calendar', new Zend_Controller_Router_Route('calendar', array('module' => 'calendar', 'controller' => 'index', 'action' => 'index')));
        $router->addRoute('addBooking', new Zend_Controller_Router_Route('booking-add', array('module' => 'calendar', 'controller' => 'index', 'action' => 'add')));
        $router->addRoute('editBooking', new Zend_Controller_Router_Route('booking-edit', array('module' => 'calendar', 'controller' => 'index', 'action' => 'edit')));
		$router->addRoute('copyBooking', new Zend_Controller_Router_Route('calendar/index/booking-copy/:booking_id', array('module' => 'calendar', 'controller' => 'index', 'action' => 'copy')));
        $router->addRoute('manageCalendar', new Zend_Controller_Router_Route('calendar-manage', array('module' => 'calendar', 'controller' => 'index', 'action' => 'manage')));

        //
        //booking
        //
		//////////Islam
		$router->addRoute('defultPage', new Zend_Controller_Router_Route('booking/index/defult_page/:contractor_id', array('module' => 'booking', 'controller' => 'index', 'action' => 'defult-page')));
        
		///////the flag parameter indicates if this process is payment(1) or just assign(0)
		$router->addRoute('paymentFileUpload', new Zend_Controller_Router_Route('reports/file-upload/:flag', array('module' => 'reports', 'controller' => 'attachment', 'action' => 'file-upload')));
		$router->addRoute('notVisitedBooking', new Zend_Controller_Router_Route('booking/index/not-visited-booking/:id', array('module' => 'booking', 'controller' => 'index', 'action' => 'not-visited-booking')));
		$router->addRoute('visitedBooking', new Zend_Controller_Router_Route('booking/index/visited-booking/:id', array('module' => 'booking', 'controller' => 'index', 'action' => 'visited-booking')));
		
		////////////
        $router->addRoute('booking', new Zend_Controller_Router_Route('booking', array('module' => 'booking', 'controller' => 'index', 'action' => 'index')));
        $router->addRoute('isDeletedBooking', new Zend_Controller_Router_Route('booking/:is_deleted', array('module' => 'booking', 'controller' => 'index', 'action' => 'index')));
        $router->addRoute('bookingEdit', new Zend_Controller_Router_Route('booking/edit/:id', array('module' => 'booking', 'controller' => 'index', 'action' => 'edit')));
        ///////////By Islam
		$router->addRoute('bookingCopy', new Zend_Controller_Router_Route('booking/copy/:booking_id', array('module' => 'booking', 'controller' => 'index', 'action' => 'copy')));
		$router->addRoute('followUpBooking', new Zend_Controller_Router_Route('booking/follow-up/:id', array('module' => 'booking', 'controller' => 'index', 'action' => 'follow-up-booking')));
        $router->addRoute('removeFollowUpBooking', new Zend_Controller_Router_Route('booking/remove_follow_up/:id', array('module' => 'booking', 'controller' => 'index', 'action' => 'remove-follow-up-booking')));
        $router->addRoute('pauseResumeBookingEmails', new Zend_Controller_Router_Route('pause-resume-booking-emails/:id/:process', array('module' => 'booking', 'controller' => 'index', 'action' => 'pause-resume-booking-emails')));
        
		
		$router->addRoute('bookingView', new Zend_Controller_Router_Route('booking/view/:id', array('module' => 'booking', 'controller' => 'index', 'action' => 'view')));
        $router->addRoute('bookingHistory', new Zend_Controller_Router_Route('booking/history/:id', array('module' => 'booking', 'controller' => 'index', 'action' => 'history')));
        $router->addRoute('bookingDiscussion', new Zend_Controller_Router_Route('booking/discussion/:id', array('module' => 'booking', 'controller' => 'discussion', 'action' => 'index')));
        $router->addRoute('submitBookingDiscussion', new Zend_Controller_Router_Route('booking/discussion/send/:id', array('module' => 'booking', 'controller' => 'discussion', 'action' => 'submit')));
        $router->addRoute('allBookingDiscussion', new Zend_Controller_Router_Route('booking/discussion/all/:id', array('module' => 'booking', 'controller' => 'discussion', 'action' => 'get-all-discussion')));
        $router->addRoute('bookingAdd', new Zend_Controller_Router_Route('booking/add', array('module' => 'booking', 'controller' => 'index', 'action' => 'add')));
        
		$router->addRoute('addBookingToCustomerId', new Zend_Controller_Router_Route('booking-add/:customer_id', array('module' => 'calendar', 'controller' => 'index', 'action' => 'add')));
        $router->addRoute('bookingAddToCustomerId', new Zend_Controller_Router_Route('booking/add/:customer_id', array('module' => 'booking', 'controller' => 'index', 'action' => 'add-booking-to-customer-id')));
        
		$router->addRoute('bookingDelete', new Zend_Controller_Router_Route('booking/delete/:id', array('module' => 'booking', 'controller' => 'index', 'action' => 'delete')));
        $router->addRoute('bookingDeleteBtn', new Zend_Controller_Router_Route('booking/delete', array('module' => 'booking', 'controller' => 'index', 'action' => 'delete')));
        $router->addRoute('bookingUndelete', new Zend_Controller_Router_Route('booking/undelete/:id', array('module' => 'booking', 'controller' => 'index', 'action' => 'undelete')));
        $router->addRoute('sendDiscussion', new Zend_Controller_Router_Route('booking/discussion/send', array('module' => 'booking', 'controller' => 'discussion', 'action' => 'index')));
        $router->addRoute('bookingAddLabel', new Zend_Controller_Router_Route('booking/label/add-label/:id', array('module' => 'booking', 'controller' => 'label', 'action' => 'add-label')));
        $router->addRoute('bookingSelectLabel', new Zend_Controller_Router_Route('booking/label/select-label/:id', array('module' => 'booking', 'controller' => 'label', 'action' => 'select-label')));
        $router->addRoute('filterBookingLabel', new Zend_Controller_Router_Route('booking/label/filter-label', array('module' => 'booking', 'controller' => 'label', 'action' => 'filter-label')));
        $router->addRoute('bookingSearchLabel', new Zend_Controller_Router_Route('booking/label/search-label', array('module' => 'booking', 'controller' => 'label', 'action' => 'search-label')));
        $router->addRoute('bookingAttachment', new Zend_Controller_Router_Route('booking/attachment/:id', array('module' => 'booking', 'controller' => 'attachment', 'action' => 'index')));
        $router->addRoute('bookingFileUpload', new Zend_Controller_Router_Route('booking/file_upload/:booking_id', array('module' => 'booking', 'controller' => 'attachment', 'action' => 'file-upload')));
        $router->addRoute('bookingFileUploadDelete', new Zend_Controller_Router_Route('booking/file_upload/delete/:id', array('module' => 'booking', 'controller' => 'attachment', 'action' => 'delete')));
        $router->addRoute('bookingFileUploadDeleteBtn', new Zend_Controller_Router_Route('booking/file_upload/delete', array('module' => 'booking', 'controller' => 'attachment', 'action' => 'delete')));
        $router->addRoute('bookingFileDownload', new Zend_Controller_Router_Route('booking/file_download/:id', array('module' => 'booking', 'controller' => 'attachment', 'action' => 'download')));
        $router->addRoute('bookingFileEditWorkOrder', new Zend_Controller_Router_Route('booking/file_upload/edit_work_order/:id/:work_order', array('module' => 'booking', 'controller' => 'attachment', 'action' => 'edit-work-order')));
        $router->addRoute('bookingFileEditDescription', new Zend_Controller_Router_Route('booking/file_upload/edit_desription/:id', array('module' => 'booking', 'controller' => 'attachment', 'action' => 'edit-description')));
        $router->addRoute('bookingFileViewDescription', new Zend_Controller_Router_Route('booking/file_upload/view_desription/:id', array('module' => 'booking', 'controller' => 'attachment', 'action' => 'view-description')));
        $router->addRoute('setContractorShare', new Zend_Controller_Router_Route('booking/contractor_share/:id/:flag', array('module' => 'booking', 'controller' => 'index', 'action' => 'set-contractor-share')));
       // $router->addRoute('setContractorShareAjaxPost', new Zend_Controller_Router_Route('booking/contractor_share/:id', array('module' => 'booking', 'controller' => 'index', 'action' => 'set-contractor-share-ajax-post')));
        
		
		
		$router->addRoute('feedbackList', new Zend_Controller_Router_Route('booking/feedback', array('module' => 'booking', 'controller' => 'feedback', 'action' => 'feedback-list')));
        $router->addRoute('feedbackView', new Zend_Controller_Router_Route('booking/feedback_view/:id', array('module' => 'booking', 'controller' => 'feedback', 'action' => 'feedback-view')));
        $router->addRoute('sendClaimOwner', new Zend_Controller_Router_Route('claim_owner/send/:id', array('module' => 'booking', 'controller' => 'claim-owner', 'action' => 'send-claim-owner')));
        $router->addRoute('claimOwner', new Zend_Controller_Router_Route('claim_owner', array('module' => 'booking', 'controller' => 'claim-owner', 'action' => 'index')));
        $router->addRoute('submitClaimOwner', new Zend_Controller_Router_Route('claim_owner/submit/:id', array('module' => 'booking', 'controller' => 'claim-owner', 'action' => 'submit-claim-owner')));
        $router->addRoute('contractorBookingLocation', new Zend_Controller_Router_Route('contractor_booking_location/:booking_id/:contractor_id', array('module' => 'booking', 'controller' => 'index', 'action' => 'contractor-booking-location')));
        $router->addRoute('modifyFollowDate', new Zend_Controller_Router_Route('/ajax/modifyFollowDate', array('module' => 'booking', 'controller' => 'index', 'action' => 'modify-follow-date')));
        $router->addRoute('logBookingView', new Zend_Controller_Router_Route('log-booking/view/:id', array('module' => 'booking', 'controller' => 'log', 'action' => 'view')));

        $router->addRoute('sendReminderTentativeBookingAsEmail', new Zend_Controller_Router_Route('email/reminder-tentative-booking/:id', array('module' => 'booking', 'controller' => 'index', 'action' => 'send-reminder-tentative-booking-as-email')));
        $router->addRoute('sendReminderOnHoldBookingAsEmail', new Zend_Controller_Router_Route('email/reminder-on-hold-booking/:id', array('module' => 'booking', 'controller' => 'index', 'action' => 'send-reminder-on-hold-booking-as-email')));
        $router->addRoute('sendBookingConfirmationAsEmail', new Zend_Controller_Router_Route('email/booking-confirmation', array('module' => 'booking', 'controller' => 'index', 'action' => 'send-booking-confirmation-as-email')));
        $router->addRoute('sendBookingAsEmail', new Zend_Controller_Router_Route('email/booking/:id', array('module' => 'booking', 'controller' => 'index', 'action' => 'send-booking-as-email')));
        $router->addRoute('sendBookingAsEmailToContractor', new Zend_Controller_Router_Route('email/booking_to_contractor/:id', array('module' => 'booking', 'controller' => 'index', 'action' => 'send-booking-as-email-to-contractor')));
        $router->addRoute('sendWorkOrderRequestEmail', new Zend_Controller_Router_Route('work_order_request_email/booking/:id', array('module' => 'booking', 'controller' => 'index', 'action' => 'send-work-order-request-email')));
        $router->addRoute('sendBookingAsSms', new Zend_Controller_Router_Route('sms/booking/:id', array('module' => 'booking', 'controller' => 'index', 'action' => 'send-booking-as-sms')));
        $router->addRoute('sendBookingAsSmsToContractor', new Zend_Controller_Router_Route('sms/booking/contractor/:id', array('module' => 'booking', 'controller' => 'index', 'action' => 'send-booking-as-sms-to-contractor')));
        $router->addRoute('bookingPreview', new Zend_Controller_Router_Route('booking/preview/:id', array('module' => 'booking', 'controller' => 'index', 'action' => 'preview')));
        $router->addRoute('bookingCustomerPreview', new Zend_Controller_Router_Route('booking/customer_preview/:id', array('module' => 'booking', 'controller' => 'index', 'action' => 'booking-customer-preview')));
        $router->addRoute('downloadBooking', new Zend_Controller_Router_Route('download/booking/:id', array('module' => 'booking', 'controller' => 'index', 'action' => 'download-booking')));
        $router->addRoute('downloadContractorBooking', new Zend_Controller_Router_Route('download/contractor_booking/:id', array('module' => 'booking', 'controller' => 'index', 'action' => 'download-contractor-booking')));
        $router->addRoute('bookingDeleteForever', new Zend_Controller_Router_Route('booking/delete-forever/:id', array('module' => 'booking', 'controller' => 'index', 'action' => 'delete-forever')));
        $router->addRoute('bookingDeleteForeverBtn', new Zend_Controller_Router_Route('booking/delete-forever', array('module' => 'booking', 'controller' => 'index', 'action' => 'delete-forever')));
        $router->addRoute('awaitingUpdateBooking', new Zend_Controller_Router_Route('booking/awaiting_update', array('module' => 'booking', 'controller' => 'index', 'action' => 'get-awaiting-update-booking')));
        $router->addRoute('bookingReminderAdd', new Zend_Controller_Router_Route('booking/reminder/add/:booking_id', array('module' => 'booking', 'controller' => 'reminder', 'action' => 'reminder-add')));
        $router->addRoute('bookingReminderView', new Zend_Controller_Router_Route('booking/reminder/view/:id', array('module' => 'booking', 'controller' => 'reminder', 'action' => 'reminder-view')));
        $router->addRoute('bookingReminderDelete', new Zend_Controller_Router_Route('booking/reminder/delete/:id/:reminder_id', array('module' => 'booking', 'controller' => 'reminder', 'action' => 'reminder-delete')));
        $router->addRoute('sendRequestBookingFeedback', new Zend_Controller_Router_Route('booking/sendRequestBookingFeedback/:id', array('module' => 'booking', 'controller' => 'index', 'action' => 'send-request-booking-feedback')));
		$router->addRoute('awaitingAcceptBooking', new Zend_Controller_Router_Route('booking/awaiting_accept', array('module' => 'booking', 'controller' => 'index', 'action' => 'get-awaiting-accept-booking')));
        
		
        $router->addRoute('bookingContactHistoryAdd', new Zend_Controller_Router_Route('booking/contact_history/add/:booking_id', array('module' => 'booking', 'controller' => 'contact-history', 'action' => 'contact-history-add')));
        $router->addRoute('bookingContactHistoryView', new Zend_Controller_Router_Route('booking/contact_history/view/:id', array('module' => 'booking', 'controller' => 'contact-history', 'action' => 'contact-history-view')));
        $router->addRoute('bookingContactHistoryDelete', new Zend_Controller_Router_Route('booking/contact_history/delete/:id/:contact_history_id', array('module' => 'booking', 'controller' => 'contact-history', 'action' => 'contact-history-delete')));
        $router->addRoute('sendReminderUpdateBookingToContractor', new Zend_Controller_Router_Route('email/reminder_booking_to_contractor/:id', array('module' => 'booking', 'controller' => 'index', 'action' => 'send-reminder-update-booking-to-contractor')));
        $router->addRoute('getEmailTemplate', new Zend_Controller_Router_Route('ajax/get_email_template', array('module' => 'booking', 'controller' => 'index', 'action' => 'get-email-template')));


        //
        //calendar service
        //
        $router->addRoute('getRelatedAddress', new Zend_Controller_Router_Route('ajax/getRelatedAddress', array('module' => 'default', 'controller' => 'common', 'action' => 'get-related-address')));
        $router->addRoute('findAvailability', new Zend_Controller_Router_Route('ajax/findAvailability', array('module' => 'default', 'controller' => 'common', 'action' => 'find-availability')));
        $router->addRoute('setCompanySession', new Zend_Controller_Router_Route('ajax/setCompanySession', array('module' => 'default', 'controller' => 'common', 'action' => 'set-company-session')));
        $router->addRoute('saveAsXls', new Zend_Controller_Router_Route('ajax/saveAsXls', array('module' => 'default', 'controller' => 'common', 'action' => 'save-as-xls')));
        $router->addRoute('dropDownCity', new Zend_Controller_Router_Route('drop_down/city', array('module' => 'default', 'controller' => 'common', 'action' => 'cities-by-country-id')));
        $router->addRoute('dropDownState', new Zend_Controller_Router_Route('drop_down/state', array('module' => 'default', 'controller' => 'common', 'action' => 'get-by-state')));
        $router->addRoute('bookingAddressBlock', new Zend_Controller_Router_Route('common/booking-address', array('module' => 'default', 'controller' => 'common', 'action' => 'get-booking-address')));
        $router->addRoute('bookingState', new Zend_Controller_Router_Route('common/booking-state', array('module' => 'default', 'controller' => 'common', 'action' => 'get-booking-state')));
        $router->addRoute('InquiryAddressBlock', new Zend_Controller_Router_Route('common/inquiry-address', array('module' => 'default', 'controller' => 'common', 'action' => 'get-inquiry-address')));
        $router->addRoute('servicesAvailable', new Zend_Controller_Router_Route('common/services_available', array('module' => 'default', 'controller' => 'common', 'action' => 'services-available-by-city-id')));
        $router->addRoute('searchServices', new Zend_Controller_Router_Route('common/search_services', array('module' => 'default', 'controller' => 'common', 'action' => 'search-services')));
        $router->addRoute('contractor', new Zend_Controller_Router_Route('common/contractor', array('module' => 'default', 'controller' => 'common', 'action' => 'contractor-available-by-service-id')));
        $router->addRoute('drowAttribute', new Zend_Controller_Router_Route('common/drow_attribute', array('module' => 'default', 'controller' => 'attribute', 'action' => 'drow-attribute')));
        $router->addRoute('countServicePrice', new Zend_Controller_Router_Route('common/count_service_price', array('module' => 'default', 'controller' => 'attribute', 'action' => 'count-service-price')));
        $router->addRoute('dropDownCustomerList', new Zend_Controller_Router_Route('common/customerList', array('module' => 'default', 'controller' => 'common', 'action' => 'customer-list')));
        $router->addRoute('customerSearch', new Zend_Controller_Router_Route('common/customerSearch', array('module' => 'default', 'controller' => 'common', 'action' => 'customer-search')));
        $router->addRoute('checkBookingStatus', new Zend_Controller_Router_Route('common/checkBookingStatus', array('module' => 'default', 'controller' => 'common', 'action' => 'check-booking-status')));
        $router->addRoute('setMessage', new Zend_Controller_Router_Route('common/setMessage', array('module' => 'default', 'controller' => 'common', 'action' => 'set-message')));
        $router->addRoute('inquiryTypeAttributes', new Zend_Controller_Router_Route('common/inquiry_type_attributes', array('module' => 'default', 'controller' => 'common', 'action' => 'inquiry-type-attributes')));
        $router->addRoute('fillCannedResponse', new Zend_Controller_Router_Route('ajax/fill_canned_response', array('module' => 'default', 'controller' => 'common', 'action' => 'fill-canned-response')));
        $router->addRoute('customerLike', new Zend_Controller_Router_Route('common/customerLike', array('module' => 'default', 'controller' => 'common', 'action' => 'customer-like')));
        $router->addRoute('search', new Zend_Controller_Router_Route('search', array('module' => 'default', 'controller' => 'search', 'action' => 'index')));
        
		$router->addRoute('updateFullTextSearch', new Zend_Controller_Router_Route('ajax/update_full_text_search', array('module' => 'default', 'controller' => 'search', 'action' => 'update-full-text-search')));
        $router->addRoute('updateFullTextSearchByTypeAndTypeId', new Zend_Controller_Router_Route('ajax/update_full_text_search_by_type_and_type_id/:type/:type_id', array('module' => 'default', 'controller' => 'search', 'action' => 'update-full-text-search-by-type-and-type-id')));
        
		
		$router->addRoute('setDashboardStatusSession', new Zend_Controller_Router_Route('ajax/setDashboardStatusSession', array('module' => 'default', 'controller' => 'common', 'action' => 'set-dashboard-status-session')));
        $router->addRoute('autoCompleteCustomer', new Zend_Controller_Router_Route('ajax/autoCompleteCustomer', array('module' => 'default', 'controller' => 'common', 'action' => 'auto-complete-customer')));

        $router->addRoute('missedCallAdd', new Zend_Controller_Router_Route('missedCallAdd', array('module' => 'default', 'controller' => 'missed-calls', 'action' => 'add')));
        $router->addRoute('missedCall', new Zend_Controller_Router_Route('missedCall', array('module' => 'default', 'controller' => 'missed-calls', 'action' => 'index')));
        $router->addRoute('missedCallView', new Zend_Controller_Router_Route('missedCallView/:id', array('module' => 'default', 'controller' => 'missed-calls', 'action' => 'view')));
        $router->addRoute('missedCallSent', new Zend_Controller_Router_Route('missedCall/sent', array('module' => 'default', 'controller' => 'missed-calls', 'action' => 'missed-call-sent')));

        //
        //complaint
        //
        $router->addRoute('complaint', new Zend_Controller_Router_Route('complaint', array('module' => 'complaint', 'controller' => 'index', 'action' => 'index')));
        $router->addRoute('complaintAdd', new Zend_Controller_Router_Route('complaint/add/:booking_id', array('module' => 'complaint', 'controller' => 'index', 'action' => 'add')));
        $router->addRoute('complaintAddFullScreen', new Zend_Controller_Router_Route('complaint/complaint_add_full_screen/:booking_id', array('module' => 'complaint', 'controller' => 'index', 'action' => 'complaint-add-full-screen')));
        $router->addRoute('complaintDelete', new Zend_Controller_Router_Route('complaint/delete/:id', array('module' => 'complaint', 'controller' => 'index', 'action' => 'delete')));
        $router->addRoute('complaintEdit', new Zend_Controller_Router_Route('complaint/edit/:id', array('module' => 'complaint', 'controller' => 'index', 'action' => 'edit')));
        $router->addRoute('complaintView', new Zend_Controller_Router_Route('complaint/view/:id', array('module' => 'complaint', 'controller' => 'index', 'action' => 'view')));
        $router->addRoute('complaintDeleteBtn', new Zend_Controller_Router_Route('complaint/delete', array('module' => 'complaint', 'controller' => 'index', 'action' => 'delete')));
        $router->addRoute('complaintReplay', new Zend_Controller_Router_Route('complaint/replay/:id', array('module' => 'complaint', 'controller' => 'index', 'action' => 'replay-complaint')));
        $router->addRoute('complaintReplayToContractor', new Zend_Controller_Router_Route('complaint/replay_to_contractor/:id', array('module' => 'complaint', 'controller' => 'index', 'action' => 'replay-complaint-to-contractor')));
        $router->addRoute('convertStatus', new Zend_Controller_Router_Route('complaint/convertStatus/:id', array('module' => 'complaint', 'controller' => 'index', 'action' => 'convert-status')));
        $router->addRoute('convertStatusByAjax', new Zend_Controller_Router_Route('complaint/convertStatus/:id', array('module' => 'complaint', 'controller' => 'index', 'action' => 'convert-status-by-ajax')));
        
		$router->addRoute('complaintDisscussion', new Zend_Controller_Router_Route('complaint/discussion/:id/:process', array('module' => 'complaint', 'controller' => 'discussion', 'action' => 'index')));
        $router->addRoute('submitComplaintDiscussion', new Zend_Controller_Router_Route('complaint/discussion/send/:id/:process', array('module' => 'complaint', 'controller' => 'discussion', 'action' => 'submit')));
        $router->addRoute('allComplaintDiscussion', new Zend_Controller_Router_Route('complaint/discussion/all/:id', array('module' => 'complaint', 'controller' => 'discussion', 'action' => 'get-all-discussion')));
        $router->addRoute('createComplaint', new Zend_Controller_Router_Route('complaint/create', array('module' => 'complaint', 'controller' => 'index', 'action' => 'create-complaint')));
        $router->addRoute('sendComplaintAcknowledgementAsEmail', new Zend_Controller_Router_Route('complaint/acknowledgement_as_email/:id', array('module' => 'complaint', 'controller' => 'index', 'action' => 'send-complaint-acknowledgement-as-email')));

        //
        //banned ip address
        //
		$router->addRoute('addBannedIp', new Zend_Controller_Router_Route('inquiry/add_banned_ip/:ip', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'add-banned-ip')));
		$router->addRoute('removeBannedIp', new Zend_Controller_Router_Route('inquiry/remove_banned_ip/:ip', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'remove-banned-ip')));
        
		
		
		//
        //inquiry
        //
        $router->addRoute('inquiry', new Zend_Controller_Router_Route('inquiry', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'index')));
        $router->addRoute('inquiryDelete', new Zend_Controller_Router_Route('inquiry/delete/:id', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'delete')));
        $router->addRoute('inquiryMarkAsSpam', new Zend_Controller_Router_Route('inquiry/mark_as_spam/', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'mark-as-spam')));
        $router->addRoute('inquiryDeleteSpam', new Zend_Controller_Router_Route('inquiry/delete_spam/', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'delete-spam')));
        
		
		$router->addRoute('inquiryUndelete', new Zend_Controller_Router_Route('inquiry/undelete/:id', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'undelete')));
        $router->addRoute('inquiryDeleteBtn', new Zend_Controller_Router_Route('inquiry/delete', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'delete')));
        $router->addRoute('inquiryEdit', new Zend_Controller_Router_Route('inquiry/edit/:id', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'edit')));
        $router->addRoute('inquiryView', new Zend_Controller_Router_Route('inquiry/view/:id', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'view')));
        $router->addRoute('inquiryAdd', new Zend_Controller_Router_Route('inquiry/add', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'add')));
        $router->addRoute('inquiryAddToCustomerID', new Zend_Controller_Router_Route('inquiry/add/:customer_id', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'add')));
        
		$router->addRoute('inquiryOutComing', new Zend_Controller_Router_Route('inquiry/out-coming', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'out-coming')));
        $router->addRoute('inquiryConvertToBooking', new Zend_Controller_Router_Route('inquiry/convert/booking/:id', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'convert-to-booking')));
        $router->addRoute('inquiryConvertToEstimate', new Zend_Controller_Router_Route('inquiry/convert/estimate/:id', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'convert-to-estimate')));
        $router->addRoute('inquiryDeleteForever', new Zend_Controller_Router_Route('inquiry/delete-forever/:id', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'delete-forever')));
        $router->addRoute('inquiryDeleteForeverBtn', new Zend_Controller_Router_Route('inquiry/delete-forever', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'delete-forever')));
        $router->addRoute('inquirySelectLabel', new Zend_Controller_Router_Route('inquiry/label/select-label/:inquiry_id', array('module' => 'inquiry', 'controller' => 'label', 'action' => 'select-label')));
        $router->addRoute('inquirySearchLabel', new Zend_Controller_Router_Route('inquiry/label/search-label', array('module' => 'inquiry', 'controller' => 'label', 'action' => 'search-label')));
        $router->addRoute('inquiryAddLabel', new Zend_Controller_Router_Route('inquiry/label/add-label/:inquiry_id', array('module' => 'inquiry', 'controller' => 'label', 'action' => 'add-label')));
        $router->addRoute('filterInquiryLabel', new Zend_Controller_Router_Route('inquiry/label/filter-label', array('module' => 'inquiry', 'controller' => 'label', 'action' => 'filter-label')));
        $router->addRoute('inquiryDisscussion', new Zend_Controller_Router_Route('inquiry/discussion/:id', array('module' => 'inquiry', 'controller' => 'discussion', 'action' => 'index')));
        $router->addRoute('submitInquiryDiscussion', new Zend_Controller_Router_Route('inquiry/discussion/send/:id', array('module' => 'inquiry', 'controller' => 'discussion', 'action' => 'submit')));
        $router->addRoute('allInquiryDiscussion', new Zend_Controller_Router_Route('inquiry/discussion/all/:id', array('module' => 'inquiry', 'controller' => 'discussion', 'action' => 'get-all-discussion')));
        $router->addRoute('invoiceConditionReport', new Zend_Controller_Router_Route('inquiry/condition-report/:id', array('module' => 'inquiry', 'controller' => 'condition-report', 'action' => 'index')));
        $router->addRoute('inquiryReminderAdd', new Zend_Controller_Router_Route('inquiry/reminder/add/:inquiry_id', array('module' => 'inquiry', 'controller' => 'reminder', 'action' => 'reminder-add')));
        $router->addRoute('inquiryReminderView', new Zend_Controller_Router_Route('inquiry/reminder/view/:id', array('module' => 'inquiry', 'controller' => 'reminder', 'action' => 'reminder-view')));
        $router->addRoute('inquiryReminderDelete', new Zend_Controller_Router_Route('inquiry/reminder/delete/:id/:reminder_id', array('module' => 'inquiry', 'controller' => 'reminder', 'action' => 'reminder-delete')));
        $router->addRoute('inquiryAttachment', new Zend_Controller_Router_Route('inquiry/attachment/:id', array('module' => 'inquiry', 'controller' => 'attachment', 'action' => 'index')));
        $router->addRoute('inquiryFileUpload', new Zend_Controller_Router_Route('inquiry/file_upload/:inquiry_id', array('module' => 'inquiry', 'controller' => 'attachment', 'action' => 'file-upload')));
        
		$router->addRoute('inquiryFileUploadDelete', new Zend_Controller_Router_Route('inquiry/file_upload/delete/:id', array('module' => 'inquiry', 'controller' => 'attachment', 'action' => 'delete')));
        $router->addRoute('inquiryFileUploadDeleteBtn', new Zend_Controller_Router_Route('inquiry/file_upload/delete', array('module' => 'inquiry', 'controller' => 'attachment', 'action' => 'delete')));
        $router->addRoute('inquiryFileDownload', new Zend_Controller_Router_Route('inquiry/file_download/:id', array('module' => 'inquiry', 'controller' => 'attachment', 'action' => 'download')));
        $router->addRoute('inquiryFileEditDescription', new Zend_Controller_Router_Route('inquiry/file_upload/edit_desription/:id', array('module' => 'inquiry', 'controller' => 'attachment', 'action' => 'edit-description')));
        $router->addRoute('inquirygFileEditWorkOrder', new Zend_Controller_Router_Route('inquiry/file_upload/edit_work_order/:id/:work_order', array('module' => 'inquiry', 'controller' => 'attachment', 'action' => 'edit-work-order')));
        $router->addRoute('inquiryFileViewDescription', new Zend_Controller_Router_Route('inquiry/file_upload/view_desription/:id', array('module' => 'inquiry', 'controller' => 'attachment', 'action' => 'view-description')));
        $router->addRoute('sendInquiryAdvertisingEmail', new Zend_Controller_Router_Route('inquiry/advertising_email/:id', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'send-inquiry-advertising-email')));
        $router->addRoute('removeFollowUpInquiry', new Zend_Controller_Router_Route('inquiry/remove_follow_up/:id', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'remove-follow-up-inquiry')));
        $router->addRoute('followUpInquiry', new Zend_Controller_Router_Route('inquiry/follow_up/:id', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'follow-up-inquiry')));
        $router->addRoute('modifyInquiryFollowDate', new Zend_Controller_Router_Route('/ajax/modifyInquiryFollowDate', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'modify-inquiry-follow-date')));
        $router->addRoute('getAllContractorDistances', new Zend_Controller_Router_Route('ajax/get_all_contractor_distances/:inquiry_id/:service_ids', array('module' => 'inquiry', 'controller' => 'index', 'action' => 'get-all-contractor-distances')));
        
		//
        //estimates
        //
        $router->addRoute('estimates', new Zend_Controller_Router_Route('estimates', array('module' => 'estimates', 'controller' => 'index', 'action' => 'index')));
        $router->addRoute('convertBookingToEstimate', new Zend_Controller_Router_Route('booking/convert/estimate/:id', array('module' => 'estimates', 'controller' => 'index', 'action' => 'convert-booking-to-estimate')));
        $router->addRoute('convertBookingToEstimateBtn', new Zend_Controller_Router_Route('booking/convert/estimate', array('module' => 'estimates', 'controller' => 'index', 'action' => 'convert-booking-to-estimate')));
        $router->addRoute('estimateView', new Zend_Controller_Router_Route('estimate/view/:id', array('module' => 'estimates', 'controller' => 'index', 'action' => 'view')));
        $router->addRoute('estimateViewAll', new Zend_Controller_Router_Route('estimate/view/', array('module' => 'estimates', 'controller' => 'index', 'action' => 'view')));
        
		$router->addRoute('estimateDelete', new Zend_Controller_Router_Route('estimate/delete/:id', array('module' => 'estimates', 'controller' => 'index', 'action' => 'delete')));
        $router->addRoute('removeFollowUpEstimate', new Zend_Controller_Router_Route('estimate/remove_follow_up/:id', array('module' => 'estimates', 'controller' => 'index', 'action' => 'remove-follow-up-estimate')));
        $router->addRoute('followUpEstimate', new Zend_Controller_Router_Route('estimate/follow-up/:id', array('module' => 'estimates', 'controller' => 'index', 'action' => 'follow-up-estimate')));
        $router->addRoute('estimateUndelete', new Zend_Controller_Router_Route('estimate/undelete/:id', array('module' => 'estimates', 'controller' => 'index', 'action' => 'undelete')));
        $router->addRoute('estimateDeleteForEver', new Zend_Controller_Router_Route('estimate/deleteForEver/:id', array('module' => 'estimates', 'controller' => 'index', 'action' => 'delete-for-ever')));
        $router->addRoute('editEstimateNumber', new Zend_Controller_Router_Route('estimate/edit-estimate-number/:id', array('module' => 'estimates', 'controller' => 'index', 'action' => 'edit-estimate-number')));
        $router->addRoute('estimateAdd', new Zend_Controller_Router_Route('estimates/add', array('module' => 'estimates', 'controller' => 'index', 'action' => 'add')));
        $router->addRoute('estimateAddToCustomerId', new Zend_Controller_Router_Route('estimates/add/:customer_id', array('module' => 'estimates', 'controller' => 'index', 'action' => 'add-to-customer-id')));
        
		$router->addRoute('estimateCoppy', new Zend_Controller_Router_Route('estimates/coppy', array('module' => 'estimates', 'controller' => 'index', 'action' => 'coppy')));
        
		
		$router->addRoute('convertEstimateToBooking', new Zend_Controller_Router_Route('estimate/convert/booking/:id', array('module' => 'estimates', 'controller' => 'index', 'action' => 'convert-estimate-to-booking')));
        $router->addRoute('sendEstimateAsEmail', new Zend_Controller_Router_Route('email/estimate/:id', array('module' => 'estimates', 'controller' => 'index', 'action' => 'send-estimate-as-email')));
        $router->addRoute('sendReminderEstimateAsEmail', new Zend_Controller_Router_Route('email/reminder-estimate/:id', array('module' => 'estimates', 'controller' => 'index', 'action' => 'send-reminder-estimate-as-email')));
        $router->addRoute('sendEmail', new Zend_Controller_Router_Route('email/customer/:id/:type/:reference_id', array('module' => 'customer', 'controller' => 'index', 'action' => 'send-email')));
        $router->addRoute('downloadEstimate', new Zend_Controller_Router_Route('download/estimate/:id', array('module' => 'estimates', 'controller' => 'index', 'action' => 'download-estimate')));
        $router->addRoute('estimateSelectLabel', new Zend_Controller_Router_Route('estimate/label/select-label/:id', array('module' => 'estimates', 'controller' => 'label', 'action' => 'select-label')));
        $router->addRoute('estimateAddLabel', new Zend_Controller_Router_Route('estimate/label/add-label/:id', array('module' => 'estimates', 'controller' => 'label', 'action' => 'add-label')));
        $router->addRoute('filterEstimateLabel', new Zend_Controller_Router_Route('estimate/label/filter-label', array('module' => 'estimates', 'controller' => 'label', 'action' => 'filter-label')));
        $router->addRoute('estimateSearchLabel', new Zend_Controller_Router_Route('estimate/label/search-label', array('module' => 'estimates', 'controller' => 'label', 'action' => 'search-label')));
        $router->addRoute('estimateDisscussion', new Zend_Controller_Router_Route('estimate/discussion/:id', array('module' => 'estimates', 'controller' => 'discussion', 'action' => 'index')));
        $router->addRoute('submitEstimateDiscussion', new Zend_Controller_Router_Route('estimate/discussion/send/:id', array('module' => 'estimates', 'controller' => 'discussion', 'action' => 'submit')));
        $router->addRoute('allEstimateDiscussion', new Zend_Controller_Router_Route('estimate/discussion/all/:id', array('module' => 'estimates', 'controller' => 'discussion', 'action' => 'get-all-discussion')));
        $router->addRoute('emailDetails', new Zend_Controller_Router_Route('email/details/:id', array('module' => 'default', 'controller' => 'email-details', 'action' => 'index')));
        $router->addRoute('isDeletedEstimate', new Zend_Controller_Router_Route('estimate/:is_deleted', array('module' => 'estimates', 'controller' => 'index', 'action' => 'index')));
        $router->addRoute('estimatePreview', new Zend_Controller_Router_Route('estimate/preview/:id', array('module' => 'estimates', 'controller' => 'index', 'action' => 'preview')));
        $router->addRoute('estimateDeleteBtn', new Zend_Controller_Router_Route('estimate/delete', array('module' => 'estimates', 'controller' => 'index', 'action' => 'delete')));
        $router->addRoute('sendAdvertisingEmail', new Zend_Controller_Router_Route('estimate/advertising_email/:id', array('module' => 'estimates', 'controller' => 'index', 'action' => 'send-advertising-email')));

        //
        //invoices
        //
        $router->addRoute('invoices', new Zend_Controller_Router_Route('invoices', array('module' => 'invoices', 'controller' => 'index', 'action' => 'index')));
        $router->addRoute('duplicatedInvoices', new Zend_Controller_Router_Route('invoices/duplicated-invoices', array('module' => 'invoices', 'controller' => 'index', 'action' => 'duplicated-invoices')));
        
		$router->addRoute('convertBookingToInvoice', new Zend_Controller_Router_Route('booking/convert/invoice/:id', array('module' => 'invoices', 'controller' => 'index', 'action' => 'convert-booking-to-invoice')));
        $router->addRoute('convertBookingToInvoiceBtn', new Zend_Controller_Router_Route('booking/convert/invoice', array('module' => 'invoices', 'controller' => 'index', 'action' => 'convert-booking-to-invoice')));
        $router->addRoute('invoiceView', new Zend_Controller_Router_Route('invoice/:id', array('module' => 'invoices', 'controller' => 'index', 'action' => 'view')));
        $router->addRoute('editInvoiceNumber', new Zend_Controller_Router_Route('invoice/edit-invoice-number/:id', array('module' => 'invoices', 'controller' => 'index', 'action' => 'edit-invoice-number')));
        $router->addRoute('checkDuplicateInvoice', new Zend_Controller_Router_Route('invoice', array('module' => 'invoices', 'controller' => 'index', 'action' => 'check-duplicate-invoice')));
        
		$router->addRoute('invoicePreview', new Zend_Controller_Router_Route('invoice/preview/:id', array('module' => 'invoices', 'controller' => 'index', 'action' => 'preview')));
        $router->addRoute('invoiceDelete', new Zend_Controller_Router_Route('invoice/delete/:id', array('module' => 'invoices', 'controller' => 'index', 'action' => 'delete')));
        $router->addRoute('invoiceDeleteBtn', new Zend_Controller_Router_Route('invoice/delete', array('module' => 'invoices', 'controller' => 'index', 'action' => 'delete')));
        $router->addRoute('convertToOpen', new Zend_Controller_Router_Route('invoice/convert/open', array('module' => 'invoices', 'controller' => 'index', 'action' => 'convert-to-open')));
        $router->addRoute('convertToOpenLink', new Zend_Controller_Router_Route('invoice/convert-link/open/:id', array('module' => 'invoices', 'controller' => 'index', 'action' => 'convert-to-open')));
        $router->addRoute('convert', new Zend_Controller_Router_Route('invoice/convert/:type/:id', array('module' => 'invoices', 'controller' => 'index', 'action' => 'convert')));
        $router->addRoute('sendInvoiceAsEmail', new Zend_Controller_Router_Route('email/invoice/:id', array('module' => 'invoices', 'controller' => 'index', 'action' => 'send-invoice-as-email')));
        $router->addRoute('sendReminderOverdueInvoiceAsEmail', new Zend_Controller_Router_Route('email/reminder-overdue-invoice/:id', array('module' => 'invoices', 'controller' => 'index', 'action' => 'send-reminder-overdue-invoice-as-email')));
        $router->addRoute('downloadInvoice', new Zend_Controller_Router_Route('download/invoice/:id', array('module' => 'invoices', 'controller' => 'index', 'action' => 'download-invoice')));
        $router->addRoute('setDueDate', new Zend_Controller_Router_Route('invoices/set-due-date/:booking_id', array('module' => 'invoices', 'controller' => 'index', 'action' => 'set-due-date')));
        $router->addRoute('invoiceAddLabel', new Zend_Controller_Router_Route('invoice/label/add-label/:id', array('module' => 'invoices', 'controller' => 'label', 'action' => 'add-label')));
        $router->addRoute('filterInvoiceLabel', new Zend_Controller_Router_Route('invoice/label/filter-label', array('module' => 'invoices', 'controller' => 'label', 'action' => 'filter-label')));
        $router->addRoute('invoiceSearchLabel', new Zend_Controller_Router_Route('invoice/label/search-label', array('module' => 'invoices', 'controller' => 'label', 'action' => 'search-label')));
        $router->addRoute('invoiceSelectLabel', new Zend_Controller_Router_Route('invoice/label/select-label/:id', array('module' => 'invoices', 'controller' => 'label', 'action' => 'select-label')));
        $router->addRoute('invoiceDisscussion', new Zend_Controller_Router_Route('invoice/discussion/:id', array('module' => 'invoices', 'controller' => 'discussion', 'action' => 'index')));
        $router->addRoute('submitInvoiceDiscussion', new Zend_Controller_Router_Route('invoice/discussion/send/:id', array('module' => 'invoices', 'controller' => 'discussion', 'action' => 'submit')));
        $router->addRoute('allInvoiceDiscussion', new Zend_Controller_Router_Route('invoice/discussion/all/:id', array('module' => 'invoices', 'controller' => 'discussion', 'action' => 'get-all-discussion')));
        $router->addRoute('invoiceConditionReport', new Zend_Controller_Router_Route('invoice/condition-report/:id', array('module' => 'invoices', 'controller' => 'condition-report', 'action' => 'index')));

        //
        //payment
        //
		/////by islam edit & delete payment to contractor report
		//$router->addRoute('editPaymentToContractors', new Zend_Controller_Router_Route('reports/edit-contractor-invoice-number/:payment_to_contractors_id/:attachment_id', array('module' => 'reports', 'controller' => 'attachment', 'action' => 'edit-contractor-invoice-number')));
        
		$router->addRoute('paymentAttachmentDownload', new Zend_Controller_Router_Route('reports/file_download/:id/:file_name', array('module' => 'reports', 'controller' => 'attachment', 'action' => 'download')));
       
		
        $router->addRoute('paymentList', new Zend_Controller_Router_Route('payment', array('module' => 'invoices', 'controller' => 'payment', 'action' => 'index')));
        $router->addRoute('bookingPaymentList', new Zend_Controller_Router_Route('payment/:booking_id', array('module' => 'invoices', 'controller' => 'payment', 'action' => 'index')));
        $router->addRoute('statment', new Zend_Controller_Router_Route('statment/:id', array('module' => 'invoices', 'controller' => 'payment', 'action' => 'statment')));
        $router->addRoute('sendStatmentsAsEmail', new Zend_Controller_Router_Route('email/statment/:id', array('module' => 'invoices', 'controller' => 'payment', 'action' => 'send-statments-as-email')));
        $router->addRoute('paymentAdd', new Zend_Controller_Router_Route('payment/add/:booking_id', array('module' => 'invoices', 'controller' => 'payment', 'action' => 'add')));
        $router->addRoute('paymentDelete', new Zend_Controller_Router_Route('payment/delete/:booking_id/:id', array('module' => 'invoices', 'controller' => 'payment', 'action' => 'delete')));
        $router->addRoute('paymentApprove', new Zend_Controller_Router_Route('payment/approve/:booking_id/:id', array('module' => 'invoices', 'controller' => 'payment', 'action' => 'payment-approve')));
        $router->addRoute('paymentUnapprove', new Zend_Controller_Router_Route('payment/unapprove/:booking_id/:id', array('module' => 'invoices', 'controller' => 'payment', 'action' => 'payment-unapprove')));
        $router->addRoute('paymentDeleteBtn', new Zend_Controller_Router_Route('payment/delete/:booking_id', array('module' => 'invoices', 'controller' => 'payment', 'action' => 'delete')));
        $router->addRoute('checkStatusToAddPayment', new Zend_Controller_Router_Route('payment/checkStatus/:booking_id', array('module' => 'invoices', 'controller' => 'payment', 'action' => 'check-status-to-add-payment')));
        $router->addRoute('editPayment', new Zend_Controller_Router_Route('payment/edit/:id', array('module' => 'invoices', 'controller' => 'payment', 'action' => 'edit')));
        $router->addRoute('markPayment', new Zend_Controller_Router_Route('markPayment/:payment_id', array('module' => 'invoices', 'controller' => 'payment', 'action' => 'mark-payment')));
        $router->addRoute('unmarkPayment', new Zend_Controller_Router_Route('/ajax/unmarkPayment', array('module' => 'invoices', 'controller' => 'payment', 'action' => 'unmark-payment')));
        $router->addRoute('modifyMarkComment', new Zend_Controller_Router_Route('modify_mark_comment/:payment_id', array('module' => 'invoices', 'controller' => 'payment', 'action' => 'modify-mark-comment')));
        $router->addRoute('markPaymentView', new Zend_Controller_Router_Route('mark_payment_view/:payment_id', array('module' => 'invoices', 'controller' => 'payment', 'action' => 'mark-payment-view')));
        $router->addRoute('changeReceivedDate', new Zend_Controller_Router_Route('/ajax/changeReceivedDate', array('module' => 'invoices', 'controller' => 'payment', 'action' => 'change-received-date')));

        //
        //refund
        //
        $router->addRoute('refundAdd', new Zend_Controller_Router_Route('refund/add/:booking_id', array('module' => 'invoices', 'controller' => 'refund', 'action' => 'add')));
        $router->addRoute('bookingRefundList', new Zend_Controller_Router_Route('refund/:booking_id', array('module' => 'invoices', 'controller' => 'refund', 'action' => 'index')));
        $router->addRoute('refundList', new Zend_Controller_Router_Route('refund', array('module' => 'invoices', 'controller' => 'refund', 'action' => 'index')));
        $router->addRoute('refundDelete', new Zend_Controller_Router_Route('refund/delete/:booking_id/:id', array('module' => 'invoices', 'controller' => 'refund', 'action' => 'delete')));
        $router->addRoute('refundApprove', new Zend_Controller_Router_Route('refund/approve/:booking_id/:id', array('module' => 'invoices', 'controller' => 'refund', 'action' => 'refund-approve')));
        $router->addRoute('refundUnapprove', new Zend_Controller_Router_Route('refund/unapprove/:booking_id/:id', array('module' => 'invoices', 'controller' => 'refund', 'action' => 'refund-unapprove')));
        $router->addRoute('refundDeleteBtn', new Zend_Controller_Router_Route('refund/delete/:booking_id', array('module' => 'invoices', 'controller' => 'refund', 'action' => 'delete')));
        $router->addRoute('editRefund', new Zend_Controller_Router_Route('refund/edit/:id', array('module' => 'invoices', 'controller' => 'refund', 'action' => 'edit')));
		
		
        $router->addRoute('reports', new Zend_Controller_Router_Route('reports', array('module' => 'reports', 'controller' => 'index', 'action' => 'index')));
        $router->addRoute('addPaymentToContractor', new Zend_Controller_Router_Route('ajax/addPaymentToContractor', array('module' => 'reports', 'controller' => 'contractor-report', 'action' => 'add-payment-to-contractor')));
        
		
		$router->addRoute('addContractorInvoiceNumber', new Zend_Controller_Router_Route('ajax/addContractorInvoiceNumber/:flag', array('module' => 'reports', 'controller' => 'contractor-report', 'action' => 'add-contractor-invoice-number')));
		$router->addRoute('contractorInvoiceNumbersByContractorId', new Zend_Controller_Router_Route('ajax/contractorInvoiceNumbersByContractorId/', array('module' => 'reports', 'controller' => 'contractor-report', 'action' => 'contractor-invoice-numbers-by-contractor-id')));
		$router->addRoute('paymentToContractorById', new Zend_Controller_Router_Route('ajax/paymentToContractorById/', array('module' => 'reports', 'controller' => 'contractor-report', 'action' => 'payment-to-contractor-by-id')));
		
		$router->addRoute('editContractorInvoiceNumberOnly', new Zend_Controller_Router_Route('contractor-report/edit-invoice-number/:payment_to_contractor_id', array('module' => 'reports', 'controller' => 'contractor-report', 'action' => 'edit-contractor-invoice-number-only')));
        
		/////////////// By Islam
		$router->addRoute('editContractorInvoiceNumber', new Zend_Controller_Router_Route('ajax/editContractorInvoiceNumber', array('module' => 'reports', 'controller' => 'contractor-report', 'action' => 'edit-contractor-invoice-number')));
        $router->addRoute('editPaymentToContractor', new Zend_Controller_Router_Route('ajax/editPaymentToContractor', array('module' => 'reports', 'controller' => 'contractor-report', 'action' => 'edit-payment-to-contractor')));
        $router->addRoute('deletePaymentToContractors', new Zend_Controller_Router_Route('ajax/payment_to_contractors/:id', array('module' => 'reports', 'controller' => 'payment-report', 'action' => 'delete-payment-to-contractors')));
        $router->addRoute('deletePaymentToContractorAttachment', new Zend_Controller_Router_Route('reports/payment_to_contractors/:attachment_id', array('module' => 'reports', 'controller' => 'payment-report', 'action' => 'delete-payment-to-contractor-attachment')));
        $router->addRoute('uploadDocumentToGoogleDrive', new Zend_Controller_Router_Route('reports/payment_to_contractors/:contractor_id/:attachment_id/:file_name', array('module' => 'reports', 'controller' => 'payment-report', 'action' => 'upload-document-to-google-drive')));
       
		
		$router->addRoute('unPaidContractor', new Zend_Controller_Router_Route('reports/un_paid_contractor/:id', array('module' => 'reports', 'controller' => 'contractor-report', 'action' => 'un-paid-contractor')));
        
		//$router->addRoute('setPaidAmount', new Zend_Controller_Router_Route('reports/set_paid_amount', array('module' => 'reports', 'controller' => 'contractor-report', 'action' => 'set-paid-amount')));
        $router->addRoute('editPaidAmount', new Zend_Controller_Router_Route('reports/edit_paid_amount/:id/:contractor_id/:old_paid_amount', array('module' => 'reports', 'controller' => 'contractor-report', 'action' => 'edit-paid-amount')));
        
		$router->addRoute('setContractorShareFromReport', new Zend_Controller_Router_Route('reports/set_contractor_share_from_report/:id/:contractor_id/:old_contractor_share', array('module' => 'reports', 'controller' => 'contractor-report', 'action' => 'set-contractor-share-from-report')));
        
		
		$router->addRoute('reportContractorsBookingsSummary', new Zend_Controller_Router_Route('reports/contractors_bookings_summary', array('module' => 'reports', 'controller' => 'contractor-report', 'action' => 'contractors-bookings-summary')));
		
		
		$router->addRoute('reportContractorsDetailedSummary', new Zend_Controller_Router_Route('reports/contractors_detailed_summary', array('module' => 'reports', 'controller' => 'contractor-report', 'action' => 'contractors-detailed-summary')));
        $router->addRoute('reportContractorsSummary', new Zend_Controller_Router_Route('reports/contractors_summary', array('module' => 'reports', 'controller' => 'contractor-report', 'action' => 'contractors-summary')));
        $router->addRoute('contractorsBookingPayment', new Zend_Controller_Router_Route('reports/contractors_booking_payment', array('module' => 'reports', 'controller' => 'contractor-report', 'action' => 'contractors-booking-payment')));
        $router->addRoute('reportContractorsPaymentDetailedSummary', new Zend_Controller_Router_Route('reports/contractors_payment_detailed_summary', array('module' => 'reports', 'controller' => 'contractor-report', 'action' => 'contractors-payment-detailed-summary')));
        $router->addRoute('bookingSummary', new Zend_Controller_Router_Route('reports/booking_summary', array('module' => 'reports', 'controller' => 'common-report', 'action' => 'booking-summary')));
       
		$router->addRoute('bookingSummaryByChart', new Zend_Controller_Router_Route('reports/booking_summary_by_chart', array('module' => 'reports', 'controller' => 'common-report', 'action' => 'booking-summary-by-chart')));
        $router->addRoute('convertToBookingDuration', new Zend_Controller_Router_Route('reports/convert_to_booking_duration', array('module' => 'reports', 'controller' => 'common-report', 'action' => 'convert-to-booking-duration')));
        
		$router->addRoute('bookingServiceTileSummary', new Zend_Controller_Router_Route('reports/booking_service_tile_summary', array('module' => 'reports', 'controller' => 'common-report', 'action' => 'booking-service-tile-summary')));
        $router->addRoute('reportSalesByCustomer', new Zend_Controller_Router_Route('reports/sales_by_customer', array('module' => 'reports', 'controller' => 'customer-report', 'action' => 'sales-by-customer')));
        $router->addRoute('reportPaymentReceived', new Zend_Controller_Router_Route('reports/payment_received', array('module' => 'reports', 'controller' => 'payment-report', 'action' => 'payment-received')));
        //////* by islam link to payment to contractors report
		$router->addRoute('reportPaymentToContractors', new Zend_Controller_Router_Route('reports/payment_to_contractors', array('module' => 'reports', 'controller' => 'payment-report', 'action' => 'payment-to-contractors')));
       	$router->addRoute('sendpaymentAsEmail', new Zend_Controller_Router_Route('payment_report/send_payment_as_email/:id/:contractor_id', array('module' => 'reports', 'controller' => 'payment-report', 'action' => 'send-payment-as-email')));
        
		$router->addRoute('reportSalesByEmployee', new Zend_Controller_Router_Route('reports/sales_By_Employee', array('module' => 'reports', 'controller' => 'employee-report', 'action' => 'sales-by-employee')));
        $router->addRoute('reportOverdueInvoices', new Zend_Controller_Router_Route('reports/overdue_invoices', array('module' => 'reports', 'controller' => 'payment-report', 'action' => 'overdue-invoices')));
        $router->addRoute('reportRefundHistory', new Zend_Controller_Router_Route('reports/refund_history', array('module' => 'reports', 'controller' => 'payment-report', 'action' => 'refund-history')));

        $router->addRoute('savedReport', new Zend_Controller_Router_Route('reports/saved-report', array('module' => 'reports', 'controller' => 'index', 'action' => 'saved-report')));
        $router->addRoute('savedReportDeleteBtn', new Zend_Controller_Router_Route('reports/saved-report/delete', array('module' => 'reports', 'controller' => 'index', 'action' => 'delete')));
        $router->addRoute('savedReportDelete', new Zend_Controller_Router_Route('reports/saved-report/delete/:id', array('module' => 'reports', 'controller' => 'index', 'action' => 'delete')));
        $router->addRoute('savedReportDownload', new Zend_Controller_Router_Route('reports/saved-report/download/:id', array('module' => 'reports', 'controller' => 'index', 'action' => 'download')));

        //the websites module
        $router->addRoute('websites', new Zend_Controller_Router_Route('websites', array('module' => 'websites', 'controller' => 'index', 'action' => 'index')));
        $router->addRoute('companyWebsite', new Zend_Controller_Router_Route('company_websites/:id', array('module' => 'websites', 'controller' => 'index', 'action' => 'company-website')));

        $router->addRoute('pageAdd', new Zend_Controller_Router_Route('add_page', array('module' => 'websites', 'controller' => 'index', 'action' => 'page-add')));
        $router->addRoute('pageEdit', new Zend_Controller_Router_Route('edit_page/:id', array('module' => 'websites', 'controller' => 'index', 'action' => 'page-edit')));
        $router->addRoute('pageDelete', new Zend_Controller_Router_Route('delete_page/:id', array('module' => 'websites', 'controller' => 'index', 'action' => 'page-delete')));

        $router->addRoute('pageBlock', new Zend_Controller_Router_Route('page_block/:id', array('module' => 'websites', 'controller' => 'block', 'action' => 'index')));
        $router->addRoute('blockAdd', new Zend_Controller_Router_Route('add/:page_id', array('module' => 'websites', 'controller' => 'block', 'action' => 'add')));
        $router->addRoute('blockEdit', new Zend_Controller_Router_Route('edit', array('module' => 'websites', 'controller' => 'block', 'action' => 'edit')));
        $router->addRoute('getType', new Zend_Controller_Router_Route('get_type', array('module' => 'websites', 'controller' => 'block', 'action' => 'get-type')));

        //
        //Web Services
        //
        $router->addRoute('webservice', new Zend_Controller_Router_Route('webservice', array('module' => 'webservice', 'controller' => 'index', 'action' => 'index')));
        $router->addRoute('registerMobileIos', new Zend_Controller_Router_Route('webservice/register-mobile-ios', array('module' => 'webservice', 'controller' => 'index', 'action' => 'register-mobile-ios')));
        $router->addRoute('getIosNotificationSettings', new Zend_Controller_Router_Route('webservice/get-ios-notification-settings', array('module' => 'webservice', 'controller' => 'index', 'action' => 'get-ios-notification-settings')));
        $router->addRoute('iosUserNotificationSettings', new Zend_Controller_Router_Route('webservice/ios-user-notification-settings', array('module' => 'webservice', 'controller' => 'index', 'action' => 'ios-user-notification-settings')));
        $router->addRoute('iosUserNotification', new Zend_Controller_Router_Route('webservice/ios-user-notification', array('module' => 'webservice', 'controller' => 'index', 'action' => 'ios-user-notification')));
        $router->addRoute('iosBookingDistance', new Zend_Controller_Router_Route('webservice/ios-booking-distance', array('module' => 'webservice', 'controller' => 'index', 'action' => 'ios-booking-distance')));
        
		$router->addRoute('receiveEmailsParameters', new Zend_Controller_Router_Route('webservice/receive-emails-parameters', array('module' => 'webservice', 'controller' => 'index', 'action' => 'receive-emails-parameters')));
        $router->addRoute('updateSentEmails', new Zend_Controller_Router_Route('webservice/update-sent-emails', array('module' => 'webservice', 'controller' => 'index', 'action' => 'update-sent-emails')));
        
		//$router->addRoute('webservicePrepareToEdit', new Zend_Controller_Router_Route('webservice/prepareToEdit/:booking_id', array('module' => 'webservice', 'controller' => 'index', 'action' => 'prepare-to-edit')));
    }

    protected function _initAutoLoad() {

        $loader = new Zend_Application_Module_Autoloader(array(
                    'namespace' => '',
                    'basePath' => APPLICATION_PATH . '/modules/default'
                ));

        $autoloader = Zend_Loader_Autoloader::getInstance();
        $autoloader->pushAutoloader(new Zend_Application_Module_Autoloader(array(
                    'namespace' => 'Settings_',
                    'basePath' => APPLICATION_PATH . '/modules/settings'
                )));
        $autoloader->pushAutoloader(new Zend_Application_Module_Autoloader(array(
                    'namespace' => 'Calendar_',
                    'basePath' => APPLICATION_PATH . '/modules/calendar'
                )));
        $autoloader->pushAutoloader(new Zend_Application_Module_Autoloader(array(
                    'namespace' => 'Booking_',
                    'basePath' => APPLICATION_PATH . '/modules/booking'
                )));

        $autoloader->pushAutoloader(new Zend_Application_Module_Autoloader(array(
                    'namespace' => 'Complaint_',
                    'basePath' => APPLICATION_PATH . '/modules/complaint'
                )));

        $autoloader->pushAutoloader(new Zend_Application_Module_Autoloader(array(
                    'namespace' => 'Reports_',
                    'basePath' => APPLICATION_PATH . '/modules/reports'
                )));

        $autoloader->pushAutoloader(new Zend_Application_Module_Autoloader(array(
                    'namespace' => 'Inquiry_',
                    'basePath' => APPLICATION_PATH . '/modules/inquiry'
                )));

        $autoloader->pushAutoloader(new Zend_Application_Module_Autoloader(array(
                    'namespace' => 'Estimates_',
                    'basePath' => APPLICATION_PATH . '/modules/estimates'
                )));

        $autoloader->pushAutoloader(new Zend_Application_Module_Autoloader(array(
                    'namespace' => 'Invoices_',
                    'basePath' => APPLICATION_PATH . '/modules/invoices'
                )));

        $autoloader->pushAutoloader(new Zend_Application_Module_Autoloader(array(
                    'namespace' => 'Customer_',
                    'basePath' => APPLICATION_PATH . '/modules/customer'
                )));

        $autoloader->pushAutoloader(new Zend_Application_Module_Autoloader(array(
                    'namespace' => 'Websites_',
                    'basePath' => APPLICATION_PATH . '/modules/websites'
                )));

        $autoloader->pushAutoloader(new Zend_Application_Module_Autoloader(array(
                    'namespace' => 'WebService_',
                    'basePath' => APPLICATION_PATH . '/modules/webservice'
                )));

        Zend_Loader::loadClass('FilterLink', APPLICATION_PATH . '/../library');
        Zend_Loader::loadClass('SortOrder', APPLICATION_PATH . '/../library');
        Zend_Loader::loadClass('CheckAuth', APPLICATION_PATH . '/../library');
        Zend_Loader::loadClass('BreadCrumbs', APPLICATION_PATH . '/../library');
        Zend_Loader::loadClass('EmailNotification', APPLICATION_PATH . '/../library');
		Zend_Loader::loadClass('MailingServer', APPLICATION_PATH . '/../library');
        Zend_Loader::loadClass('ImageMagick', APPLICATION_PATH . '/../library');
        Zend_Loader::loadClass('ViewHelper', APPLICATION_PATH . '/../library');
        Zend_Loader::loadClass('GdataCalendar', APPLICATION_PATH . '/../library');
		
		//Zend_Loader::loadFile('Client.php', APPLICATION_PATH . '/../library/google-api-php-client-master/src/Google');
        //Zend_Loader::loadFile('Calendar.php', APPLICATION_PATH . '/../library/google-api-php-client-master/src/Google/Service');
        
		
        Zend_Loader::loadClass('GoogleMapAPI', APPLICATION_PATH . '/../library/GoogleMap');
        Zend_Loader::loadClass('JSMin', APPLICATION_PATH . '/../library/GoogleMap');

        Zend_Loader::loadFile('Apns.php', APPLICATION_PATH . '/../library/Zend/Mobile/Push/Message');
		Zend_Loader::loadFile('commonHelpers.php', APPLICATION_PATH . '/modules/default/views/helpers');
        Zend_Loader::loadFile('attributeFunctions.php', APPLICATION_PATH . '/modules/default/views/helpers');
        Zend_Loader::loadFile('wkhtmltopdf.php', APPLICATION_PATH . '/modules/default/views/helpers');

        $frontController = Zend_Controller_Front::getInstance();
        $frontController->registerPlugin(new Plugin_UpdateCheck());

        return $loader;
    }

    protected function _initViewHelpers() {
        $this->bootstrap('layout');
        $layout = $this->getResource('layout');
        //$layout->setLayout('main');

        $view = $layout->getView();


        $flashMessenger = new Zend_Controller_Action_Helper_FlashMessenger();
        $view->messages = $flashMessenger->getMessages();

        $view->doctype('XHTML1_TRANSITIONAL');
        $view->headMeta()->appendHttpEquiv('Content-Type', 'text/html; charset=utf-8');

        $view->headTitle()->setSeparator(' | ');
        $view->headTitle('Cleaning Service');

        BreadCrumbs::setModules(array('settings'));
        BreadCrumbs::setSeparator('>');

        //setBaseUrl
        $httpHost = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : get_config('http_host');
        $httpHost = 'http://' . $httpHost;
        Zend_Controller_Front::getInstance()->setBaseUrl($httpHost);


        //
        // default Style Sheet and JavaScript
        //
        ViewHelper::setVerision(1);
        ViewHelper::setBaseUrl($httpHost);

        //
        // JavaScript
        //
        ViewHelper::setJavaScript('/js/jquery-1.7.1.min.js');
        ViewHelper::setJavaScript('/js/jquery-ui-1.8.16.custom/js/jquery-ui-1.8.16.custom.min.js');
        ViewHelper::setJavaScript('/js/jquery-ui-1.8.16.custom/js/jquery-ui-timepicker-addon.js');
        ViewHelper::setJavaScript('/js/thickbox/thickbox.js');
        ViewHelper::setJavaScript('/js/tiny_mce/tiny_mce.js');
        ViewHelper::setJavaScript('/js/menu/fg.menu.js');
        ViewHelper::setJavaScript('/js/js.js', true);
		///////by islam for table sort  
		ViewHelper::setJavaScript('/js/jquery.tablesorter.js');
		///////for number formate
		ViewHelper::setJavaScript('/js/jquery.format-1.3.min.js');
		
		

        //
        // Style Sheet
        //
        ViewHelper::setStyleSheet('/js/jquery-ui-1.8.16.custom/css/redmond/jquery-ui-1.8.16.custom.css');
        ViewHelper::setStyleSheet('/js/thickbox/thickbox.css');
        ViewHelper::setStyleSheet('/js/menu/fg.menu.css');
        ViewHelper::setStyleSheet('/css/style.css', true);
    }

}

